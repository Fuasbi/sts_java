/*     */ package com.megacrit.cardcrawl.random;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.badlogic.gdx.math.RandomXS128;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Random
/*     */ {
/*  14 */   private static final Logger logger = LogManager.getLogger(Random.class.getName());
/*     */   public RandomXS128 random;
/*  16 */   public int counter = 0;
/*     */   
/*     */   public Random() {
/*  19 */     this(Long.valueOf(MathUtils.random(9999L)), MathUtils.random(99));
/*     */   }
/*     */   
/*     */   public Random(Long seed) {
/*  23 */     this.random = new RandomXS128(seed.longValue());
/*     */   }
/*     */   
/*     */   public Random(Long seed, int counter) {
/*  27 */     this.random = new RandomXS128(seed.longValue());
/*  28 */     for (int i = 0; i < counter; i++) {
/*  29 */       random(999);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCounter(int targetCounter)
/*     */   {
/*  39 */     if (this.counter < targetCounter) {
/*  40 */       int count = targetCounter - this.counter;
/*  41 */       for (int i = 0; i < count; i++) {
/*  42 */         randomBoolean();
/*     */       }
/*     */     } else {
/*  45 */       logger.info("ERROR: Counter is already higher than target counter!");
/*     */     }
/*     */   }
/*     */   
/*     */   public int random(int range)
/*     */   {
/*  51 */     this.counter += 1;
/*  52 */     return this.random.nextInt(range + 1);
/*     */   }
/*     */   
/*     */   public int random(int start, int end)
/*     */   {
/*  57 */     this.counter += 1;
/*  58 */     return start + this.random.nextInt(end - start + 1);
/*     */   }
/*     */   
/*     */   public long random(long range)
/*     */   {
/*  63 */     this.counter += 1;
/*  64 */     return (this.random.nextDouble() * range);
/*     */   }
/*     */   
/*     */   public long random(long start, long end)
/*     */   {
/*  69 */     this.counter += 1;
/*  70 */     return start + (this.random.nextDouble() * (end - start));
/*     */   }
/*     */   
/*     */   public long randomLong()
/*     */   {
/*  75 */     this.counter += 1;
/*  76 */     return this.random.nextLong();
/*     */   }
/*     */   
/*     */   public boolean randomBoolean()
/*     */   {
/*  81 */     this.counter += 1;
/*  82 */     return this.random.nextBoolean();
/*     */   }
/*     */   
/*     */   public boolean randomBoolean(float chance)
/*     */   {
/*  87 */     this.counter += 1;
/*  88 */     return this.random.nextFloat() < chance;
/*     */   }
/*     */   
/*     */   public float random()
/*     */   {
/*  93 */     this.counter += 1;
/*  94 */     return this.random.nextFloat();
/*     */   }
/*     */   
/*     */   public float random(float range)
/*     */   {
/*  99 */     this.counter += 1;
/* 100 */     return this.random.nextFloat() * range;
/*     */   }
/*     */   
/*     */   public float random(float start, float end)
/*     */   {
/* 105 */     this.counter += 1;
/* 106 */     return start + this.random.nextFloat() * (end - start);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\random\Random.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */