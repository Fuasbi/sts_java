/*     */ package com.megacrit.cardcrawl.metrics;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Net.HttpRequest;
/*     */ import com.badlogic.gdx.Net.HttpResponse;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.badlogic.gdx.net.HttpRequestBuilder;
/*     */ import com.google.gson.Gson;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.screens.DeathScreen;
/*     */ import java.io.File;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Calendar;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Metrics implements Runnable
/*     */ {
/*  28 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(Metrics.class.getName());
/*  29 */   private HashMap<Object, Object> params = new HashMap();
/*  30 */   private Gson gson = new Gson();
/*     */   
/*     */   private long lastPlaytimeEnd;
/*  33 */   public static final SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
/*     */   
/*     */   public boolean death;
/*     */   
/*  37 */   public MonsterGroup monsters = null;
/*     */   public MetricRequestType type;
/*     */   
/*     */   public static enum MetricRequestType {
/*  41 */     UPLOAD_METRICS,  UPLOAD_CRASH,  NONE;
/*     */     
/*     */     private MetricRequestType() {} }
/*     */   
/*  45 */   public void setValues(boolean death, MonsterGroup monsters, MetricRequestType type) { this.death = death;
/*  46 */     this.monsters = monsters;
/*  47 */     this.type = type;
/*     */   }
/*     */   
/*     */   private void sendPost(String fileName)
/*     */   {
/*  52 */     String url = "https://a79d.metrics.megacrit.com/input/playthrough";
/*  53 */     sendPost(url, fileName);
/*     */   }
/*     */   
/*     */   private void addData(Object key, Object value) {
/*  57 */     this.params.put(key, value);
/*     */   }
/*     */   
/*     */   private void sendPost(String url, final String fileToDelete) {
/*  61 */     HashMap<String, java.io.Serializable> event = new HashMap();
/*  62 */     event.put("event", this.params);
/*  63 */     if (Settings.isBeta) {
/*  64 */       event.put("host", CardCrawlGame.playerName);
/*     */     } else {
/*  66 */       event.put("host", CardCrawlGame.alias);
/*     */     }
/*  68 */     event.put("time", Long.valueOf(System.currentTimeMillis() / 1000L));
/*  69 */     String data = this.gson.toJson(event);
/*  70 */     logger.info("UPLOADING METRICS TO: url=" + url + ",data=" + data);
/*  71 */     HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  79 */     Net.HttpRequest httpRequest = requestBuilder.newRequest().method("POST").url(url).header("Content-Type", "application/json").header("Accept", "application/json").header("User-Agent", "curl/7.43.0").build();
/*  80 */     httpRequest.setContent(data);
/*  81 */     Gdx.net.sendHttpRequest(httpRequest, new com.badlogic.gdx.Net.HttpResponseListener()
/*     */     {
/*     */       public void handleHttpResponse(Net.HttpResponse httpResponse) {
/*  84 */         Metrics.logger.info("Metrics: http request response: " + httpResponse.getResultAsString());
/*  85 */         if (fileToDelete != null) {
/*  86 */           Gdx.files.local(fileToDelete).delete();
/*     */         }
/*     */       }
/*     */       
/*     */       public void failed(Throwable t)
/*     */       {
/*  92 */         Metrics.logger.info("Metrics: http request failed: " + t.toString());
/*     */       }
/*     */       
/*     */       public void cancelled()
/*     */       {
/*  97 */         Metrics.logger.info("Metrics: http request cancelled.");
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   private void gatherAllData(boolean death, MonsterGroup monsters)
/*     */   {
/* 104 */     addData("play_id", java.util.UUID.randomUUID().toString());
/* 105 */     addData("build_version", CardCrawlGame.TRUE_VERSION_NUM);
/* 106 */     addData("seed_played", Settings.seed.toString());
/* 107 */     addData("chose_seed", Boolean.valueOf(Settings.seedSet));
/* 108 */     addData("seed_source_timestamp", Long.valueOf(Settings.seedSourceTimestamp));
/* 109 */     addData("is_daily", Boolean.valueOf(Settings.isDailyRun));
/* 110 */     addData("special_seed", Settings.specialSeed);
/* 111 */     if ((Settings.dailyMods != null) && (Settings.dailyMods.getEnabledModIDs().size() > 0)) {
/* 112 */       addData("daily_mods", Settings.dailyMods.getEnabledModIDs());
/*     */     }
/*     */     
/*     */ 
/* 116 */     addData("is_trial", Boolean.valueOf(Settings.isTrial));
/* 117 */     addData("is_endless", Boolean.valueOf(Settings.isEndless));
/*     */     
/* 119 */     if (death) {
/* 120 */       AbstractPlayer player = AbstractDungeon.player;
/* 121 */       CardCrawlGame.metricData.current_hp_per_floor.add(Integer.valueOf(player.currentHealth));
/* 122 */       CardCrawlGame.metricData.max_hp_per_floor.add(Integer.valueOf(player.maxHealth));
/* 123 */       CardCrawlGame.metricData.gold_per_floor.add(Integer.valueOf(player.gold));
/*     */     }
/*     */     
/*     */ 
/* 127 */     addData("is_ascension_mode", Boolean.valueOf(AbstractDungeon.isAscensionMode));
/* 128 */     addData("ascension_level", Integer.valueOf(AbstractDungeon.ascensionLevel));
/*     */     
/* 130 */     addData("neow_bonus", CardCrawlGame.metricData.neowBonus);
/* 131 */     addData("neow_cost", CardCrawlGame.metricData.neowCost);
/*     */     
/* 133 */     addData("is_beta", Boolean.valueOf(Settings.isBeta));
/* 134 */     addData("is_prod", Boolean.valueOf(Settings.isDemo));
/* 135 */     addData("victory", Boolean.valueOf(!death));
/* 136 */     addData("floor_reached", Integer.valueOf(AbstractDungeon.floorNum));
/* 137 */     addData("score", Integer.valueOf(DeathScreen.calcScore(!death)));
/* 138 */     this.lastPlaytimeEnd = (System.currentTimeMillis() / 1000L);
/* 139 */     addData("timestamp", Long.valueOf(this.lastPlaytimeEnd));
/* 140 */     addData("local_time", timestampFormatter.format(Calendar.getInstance().getTime()));
/* 141 */     addData("playtime", Long.valueOf(CardCrawlGame.playtime));
/* 142 */     addData("player_experience", Long.valueOf(Settings.totalPlayTime));
/* 143 */     addData("master_deck", AbstractDungeon.player.masterDeck.getCardIdsForMetrics());
/* 144 */     addData("relics", AbstractDungeon.player.getRelicNames());
/* 145 */     addData("gold", Integer.valueOf(AbstractDungeon.player.gold));
/* 146 */     addData("campfire_rested", Integer.valueOf(CardCrawlGame.metricData.campfire_rested));
/* 147 */     addData("campfire_upgraded", Integer.valueOf(CardCrawlGame.metricData.campfire_upgraded));
/* 148 */     addData("purchased_purges", Integer.valueOf(CardCrawlGame.metricData.purchased_purges));
/* 149 */     addData("potions_floor_spawned", CardCrawlGame.metricData.potions_floor_spawned);
/* 150 */     addData("potions_floor_usage", CardCrawlGame.metricData.potions_floor_usage);
/* 151 */     addData("current_hp_per_floor", CardCrawlGame.metricData.current_hp_per_floor);
/* 152 */     addData("max_hp_per_floor", CardCrawlGame.metricData.max_hp_per_floor);
/* 153 */     addData("gold_per_floor", CardCrawlGame.metricData.gold_per_floor);
/* 154 */     addData("path_per_floor", CardCrawlGame.metricData.path_per_floor);
/* 155 */     addData("path_taken", CardCrawlGame.metricData.path_taken);
/* 156 */     addData("items_purchased", CardCrawlGame.metricData.items_purchased);
/* 157 */     addData("item_purchase_floors", CardCrawlGame.metricData.item_purchase_floors);
/* 158 */     addData("items_purged", CardCrawlGame.metricData.items_purged);
/* 159 */     addData("items_purged_floors", CardCrawlGame.metricData.items_purged_floors);
/* 160 */     addData("character_chosen", AbstractDungeon.player.chosenClass.name());
/* 161 */     addData("card_choices", CardCrawlGame.metricData.card_choices);
/* 162 */     addData("event_choices", CardCrawlGame.metricData.event_choices);
/* 163 */     addData("boss_relics", CardCrawlGame.metricData.boss_relics);
/* 164 */     addData("damage_taken", CardCrawlGame.metricData.damage_taken);
/* 165 */     addData("potions_obtained", CardCrawlGame.metricData.potions_obtained);
/* 166 */     addData("relics_obtained", CardCrawlGame.metricData.relics_obtained);
/* 167 */     addData("campfire_choices", CardCrawlGame.metricData.campfire_choices);
/*     */     
/*     */ 
/* 170 */     addData("circlet_count", Integer.valueOf(AbstractDungeon.player.getCircletCount()));
/*     */     Prefs pref;
/*     */     Prefs pref;
/*     */     Prefs pref;
/* 174 */     Prefs pref; switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 176 */       pref = CardCrawlGame.ironcladPrefs;
/* 177 */       break;
/*     */     case THE_SILENT: 
/* 179 */       pref = CardCrawlGame.silentPrefs;
/* 180 */       break;
/*     */     case DEFECT: 
/* 182 */       pref = CardCrawlGame.defectPrefs;
/* 183 */       break;
/*     */     default: 
/* 185 */       pref = CardCrawlGame.ironcladPrefs;
/*     */     }
/*     */     
/*     */     
/* 189 */     int numVictory = pref.getInteger("WIN_COUNT", 0);
/* 190 */     int numDeath = pref.getInteger("LOSE_COUNT", 0);
/*     */     
/* 192 */     if (numVictory <= 0) {
/* 193 */       addData("win_rate", Float.valueOf(0.0F));
/*     */     } else {
/* 195 */       addData("win_rate", Integer.valueOf(numVictory / (numDeath + numVictory)));
/*     */     }
/*     */     
/* 198 */     if ((death) && (monsters != null)) {
/* 199 */       addData("killed_by", AbstractDungeon.lastCombatMetricKey);
/*     */     } else {
/* 201 */       addData("killed_by", null);
/*     */     }
/*     */   }
/*     */   
/*     */   private void gatherAllDataAndSend(boolean death, MonsterGroup monsters) {
/* 206 */     if (DeathScreen.shouldUploadMetricData()) {
/* 207 */       gatherAllData(death, monsters);
/* 208 */       sendPost(null);
/*     */     }
/*     */   }
/*     */   
/*     */   public void gatherAllDataAndSave(boolean death, MonsterGroup monsters) {
/* 213 */     gatherAllData(death, monsters);
/*     */     
/* 215 */     String data = this.gson.toJson(this.params);
/*     */     FileHandle file;
/*     */     FileHandle file;
/* 218 */     if (!Settings.isDailyRun) {
/* 219 */       String local_runs_save_path = "runs" + File.separator;
/*     */       
/* 221 */       switch (CardCrawlGame.saveSlot)
/*     */       {
/*     */       case 0: 
/*     */         break;
/*     */       default: 
/* 226 */         local_runs_save_path = local_runs_save_path + CardCrawlGame.saveSlot + "_";
/*     */       }
/*     */       
/*     */       
/* 230 */       local_runs_save_path = local_runs_save_path + AbstractDungeon.player.chosenClass.name() + File.separator + this.lastPlaytimeEnd + ".run";
/*     */       
/*     */ 
/* 233 */       file = Gdx.files.local(local_runs_save_path);
/*     */     } else {
/* 235 */       String tmpPath = "runs" + File.separator;
/*     */       
/* 237 */       switch (CardCrawlGame.saveSlot)
/*     */       {
/*     */       case 0: 
/*     */         break;
/*     */       default: 
/* 242 */         tmpPath = tmpPath + CardCrawlGame.saveSlot + "_";
/*     */       }
/*     */       
/*     */       
/* 246 */       file = Gdx.files.local(tmpPath + "DAILY" + File.separator + this.lastPlaytimeEnd + ".run");
/*     */     }
/*     */     
/* 249 */     file.writeString(data, false);
/*     */   }
/*     */   
/*     */   public void run()
/*     */   {
/* 254 */     switch (this.type) {
/*     */     case UPLOAD_CRASH: 
/* 256 */       gatherAllDataAndSend(this.death, this.monsters);
/* 257 */       break;
/*     */     case UPLOAD_METRICS: 
/* 259 */       if (!Settings.isModded) {
/* 260 */         gatherAllDataAndSend(this.death, this.monsters);
/*     */       }
/*     */       break;
/*     */     default: 
/* 264 */       logger.info("Unspecified MetricRequestType: " + this.type.name() + " in run()");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\metrics\Metrics.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */