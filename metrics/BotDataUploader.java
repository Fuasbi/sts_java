/*     */ package com.megacrit.cardcrawl.metrics;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Net;
/*     */ import com.badlogic.gdx.Net.HttpRequest;
/*     */ import com.badlogic.gdx.Net.HttpResponse;
/*     */ import com.badlogic.gdx.Net.HttpResponseListener;
/*     */ import com.badlogic.gdx.net.HttpRequestBuilder;
/*     */ import com.google.gson.Gson;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import java.io.Serializable;
/*     */ import java.net.InetAddress;
/*     */ import java.net.UnknownHostException;
/*     */ import java.security.MessageDigest;
/*     */ import java.security.NoSuchAlgorithmException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class BotDataUploader implements Runnable
/*     */ {
/*  23 */   private static final Logger logger = LogManager.getLogger(BotDataUploader.class.getName());
/*  24 */   private Gson gson = new Gson();
/*  25 */   private String url = System.getenv("STS_DATA_UPLOAD_URL");
/*     */   
/*     */   private GameDataType type;
/*     */   
/*     */   private String header;
/*     */   private ArrayList<String> data;
/*     */   
/*     */   public static enum GameDataType
/*     */   {
/*  34 */     BANDITS,  CRASH_DATA,  DAILY_DATA,  DEMO_EMBARK,  VICTORY_DATA,  CARD_DATA,  ENEMY_DATA,  RELIC_DATA,  POTION_DATA,  DAILY_MOD_DATA,  BLIGHT_DATA;
/*     */     
/*     */     private GameDataType() {} }
/*     */   
/*  38 */   public void setValues(GameDataType type, String header, ArrayList<String> data) { this.type = type;
/*  39 */     this.header = header;
/*  40 */     this.data = data;
/*     */   }
/*     */   
/*     */   public void sendPost(HashMap<String, Serializable> eventData)
/*     */   {
/*  45 */     if (this.url == null) {
/*  46 */       return;
/*     */     }
/*  48 */     HashMap<String, Serializable> event = new HashMap();
/*     */     try
/*     */     {
/*  51 */       String hostname = InetAddress.getLocalHost().getHostName();
/*  52 */       MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
/*  53 */       messageDigest.update(hostname.getBytes());
/*     */     } catch (UnknownHostException|NoSuchAlgorithmException e) {
/*  55 */       e.printStackTrace();
/*  56 */       com.megacrit.cardcrawl.core.ExceptionHandler.handleException(e, logger);
/*     */     }
/*  58 */     eventData.put("STS_DATA_UPLOAD_KEY", System.getenv("STS_DATA_UPLOAD_KEY"));
/*  59 */     event.putAll(eventData);
/*     */     
/*  61 */     String data = this.gson.toJson(event);
/*  62 */     logger.info("UPLOADING TO LEADER BOARD: " + data);
/*  63 */     HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  70 */     Net.HttpRequest httpRequest = requestBuilder.newRequest().method("POST").url(this.url).header("Content-Type", "application/json").header("Accept", "application/json").build();
/*  71 */     httpRequest.setContent(data);
/*  72 */     Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener()
/*     */     {
/*     */       public void handleHttpResponse(Net.HttpResponse httpResponse) {
/*  75 */         if (Settings.isDev) {
/*  76 */           BotDataUploader.logger.info("Bot Data Upload: http request response: " + httpResponse.getResultAsString());
/*     */         }
/*     */       }
/*     */       
/*     */       public void failed(Throwable t)
/*     */       {
/*  82 */         if (Settings.isDev) {
/*  83 */           BotDataUploader.logger.info("Bot Data Upload: http request failed: " + t.toString());
/*     */         }
/*     */       }
/*     */       
/*     */       public void cancelled()
/*     */       {
/*  89 */         if (Settings.isDev) {
/*  90 */           BotDataUploader.logger.info("Bot Data Upload: http request cancelled.");
/*     */         }
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   private void sendCardData(String header, ArrayList<String> cardData) {
/*  97 */     sendData("card", header, cardData);
/*     */   }
/*     */   
/*     */   private void sendRelicData(String header, ArrayList<String> relicData) {
/* 101 */     sendData("relic", header, relicData);
/*     */   }
/*     */   
/*     */   private void sendEnemyData(String header, ArrayList<String> enemyData) {
/* 105 */     sendData("enemy", header, enemyData);
/*     */   }
/*     */   
/*     */   private void sendPotionData(String header, ArrayList<String> potionData) {
/* 109 */     sendData("potion", header, potionData);
/*     */   }
/*     */   
/*     */   private void sendDailyModData(String header, ArrayList<String> dailyModData) {
/* 113 */     sendData("daily mod", header, dailyModData);
/*     */   }
/*     */   
/*     */   private void sendBlightData(String header, ArrayList<String> blightData) {
/* 117 */     sendData("blight", header, blightData);
/*     */   }
/*     */   
/*     */   private void sendData(String table, String header, ArrayList<String> entries) {
/* 121 */     HashMap<String, Serializable> data = new HashMap(entries.size() + 1);
/* 122 */     data.put("event type", table + " data update");
/* 123 */     data.put("header", header);
/* 124 */     data.put("data", entries);
/* 125 */     sendPost(data);
/*     */   }
/*     */   
/*     */   public static void uploadDataAsync(GameDataType type, String header, ArrayList<String> data) {
/* 129 */     BotDataUploader poster = new BotDataUploader();
/* 130 */     poster.setValues(type, header, data);
/* 131 */     Thread t = new Thread(poster);
/* 132 */     t.setName("LeaderboardPoster");
/* 133 */     t.run();
/*     */   }
/*     */   
/*     */   public void run()
/*     */   {
/* 138 */     switch (this.type) {
/*     */     case CARD_DATA: 
/* 140 */       sendCardData(this.header, this.data);
/* 141 */       break;
/*     */     case RELIC_DATA: 
/* 143 */       sendRelicData(this.header, this.data);
/* 144 */       break;
/*     */     case ENEMY_DATA: 
/* 146 */       sendEnemyData(this.header, this.data);
/* 147 */       break;
/*     */     case POTION_DATA: 
/* 149 */       sendPotionData(this.header, this.data);
/* 150 */       break;
/*     */     case DAILY_MOD_DATA: 
/* 152 */       sendDailyModData(this.header, this.data);
/* 153 */       break;
/*     */     case BLIGHT_DATA: 
/* 155 */       sendBlightData(this.header, this.data);
/* 156 */       break;
/*     */     default: 
/* 158 */       logger.info("Unspecified/deprecated LeaderboardPosterType: " + this.type.name() + " in run()");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\metrics\BotDataUploader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */