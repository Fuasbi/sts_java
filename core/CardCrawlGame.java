/*      */ package com.megacrit.cardcrawl.core;
/*      */ 
/*      */ import com.badlogic.gdx.Application;
/*      */ import com.badlogic.gdx.ApplicationListener;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.Preferences;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.FPSLogger;
/*      */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*      */ import com.badlogic.gdx.graphics.Pixmap;
/*      */ import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.Interpolation;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.badlogic.gdx.math.Vector3;
/*      */ import com.badlogic.gdx.utils.viewport.FitViewport;
/*      */ import com.codedisaster.steamworks.SteamAPI;
/*      */ import com.codedisaster.steamworks.SteamController;
/*      */ import com.codedisaster.steamworks.SteamController.SourceMode;
/*      */ import com.codedisaster.steamworks.SteamControllerAnalogActionData;
/*      */ import com.codedisaster.steamworks.SteamControllerDigitalActionData;
/*      */ import com.codedisaster.steamworks.SteamControllerHandle;
/*      */ import com.codedisaster.steamworks.SteamNativeHandle;
/*      */ import com.codedisaster.steamworks.SteamUtils;
/*      */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.CardSave;
/*      */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.characters.Ironclad;
/*      */ import com.megacrit.cardcrawl.characters.TheSilent;
/*      */ import com.megacrit.cardcrawl.credits.CreditsScreen;
/*      */ import com.megacrit.cardcrawl.daily.DailyMods;
/*      */ import com.megacrit.cardcrawl.daily.TimeHelper;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.dungeons.Exordium;
/*      */ import com.megacrit.cardcrawl.dungeons.TheBeyond;
/*      */ import com.megacrit.cardcrawl.dungeons.TheCity;
/*      */ import com.megacrit.cardcrawl.helpers.AsyncSaver;
/*      */ import com.megacrit.cardcrawl.helpers.BlightHelper;
/*      */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*      */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.DrawMaster;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*      */ import com.megacrit.cardcrawl.helpers.GameTips;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.ModHelper;
/*      */ import com.megacrit.cardcrawl.helpers.PotionHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Prefs;
/*      */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*      */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*      */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*      */ import com.megacrit.cardcrawl.helpers.ShaderHelper;
/*      */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*      */ import com.megacrit.cardcrawl.helpers.controller.SteamInputHandler;
/*      */ import com.megacrit.cardcrawl.helpers.input.DevInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.metrics.MetricData;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*      */ import com.megacrit.cardcrawl.potions.PotionSlot;
/*      */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*      */ import com.megacrit.cardcrawl.random.Random;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.BottledFlame;
/*      */ import com.megacrit.cardcrawl.relics.BottledLightning;
/*      */ import com.megacrit.cardcrawl.relics.BottledTornado;
/*      */ import com.megacrit.cardcrawl.rewards.RewardSave;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*      */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*      */ import com.megacrit.cardcrawl.scenes.TitleBackground;
/*      */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*      */ import com.megacrit.cardcrawl.screens.DungeonTransitionScreen;
/*      */ import com.megacrit.cardcrawl.screens.SingleCardViewPopup;
/*      */ import com.megacrit.cardcrawl.screens.SingleRelicViewPopup;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*      */ import com.megacrit.cardcrawl.screens.splash.SplashScreen;
/*      */ import com.megacrit.cardcrawl.shop.ShopScreen;
/*      */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*      */ import com.megacrit.cardcrawl.trials.AbstractTrial;
/*      */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import de.robojumper.ststwitch.TwitchConfig;
/*      */ import java.io.PrintStream;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Iterator;
/*      */ import java.util.Map;
/*      */ import java.util.Map.Entry;
/*      */ import java.util.Scanner;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class CardCrawlGame implements ApplicationListener
/*      */ {
/*  111 */   public static String VERSION_NUM = "[EARLY_ACCESS] (08-30-2018)";
/*  112 */   public static String TRUE_VERSION_NUM = "2018-08-30";
/*      */   private OrthographicCamera camera;
/*      */   public static FitViewport viewport;
/*      */   public static PolygonSpriteBatch psb;
/*      */   private SpriteBatch sb;
/*      */   public static GameCursor cursor;
/*  118 */   public static boolean isPopupOpen = false;
/*      */   public static int popupMX;
/*  120 */   public static int popupMY; public static ScreenShake screenShake = new ScreenShake();
/*      */   public static AbstractDungeon dungeon;
/*      */   public static MainMenuScreen mainMenuScreen;
/*      */   public static SplashScreen splashScreen;
/*      */   public static DungeonTransitionScreen dungeonTransitionScreen;
/*      */   public static CancelButton cancelButton;
/*      */   public static MusicMaster music;
/*      */   public static SoundMaster sound;
/*      */   public static GameTips tips;
/*      */   public static SingleCardViewPopup cardPopup;
/*      */   public static SingleRelicViewPopup relicPopup;
/*  131 */   private FPSLogger fpsLogger = new FPSLogger();
/*  132 */   public boolean prevDebugKeyDown = false;
/*      */   public static String nextDungeon;
/*  134 */   public static GameMode mode = GameMode.CHAR_SELECT;
/*  135 */   public static boolean startOver = false;
/*  136 */   private static boolean queueCredits = false;
/*  137 */   public static boolean MUTE_IF_BG = false;
/*      */   
/*      */ 
/*  140 */   public static AbstractPlayer.PlayerClass chosenCharacter = null;
/*      */   public static boolean loadingSave;
/*  142 */   public static SaveFile saveFile = null;
/*      */   public static Prefs saveSlotPref;
/*      */   public static Prefs playerPref;
/*  145 */   public static Prefs ironcladPrefs; public static Prefs silentPrefs; public static Prefs defectPrefs; public static int saveSlot = 0;
/*      */   
/*      */ 
/*      */ 
/*      */   public static String playerName;
/*      */   
/*      */ 
/*      */   public static String alias;
/*      */   
/*      */ 
/*  155 */   public static int monstersSlain = 0;
/*  156 */   public static int elites1Slain = 0;
/*  157 */   public static int elites2Slain = 0;
/*  158 */   public static int elites3Slain = 0;
/*  159 */   public static int champion = 0;
/*  160 */   public static int perfect = 0;
/*  161 */   public static boolean overkill = false;
/*  162 */   public static boolean combo = false;
/*  163 */   public static boolean cheater = false;
/*  164 */   public static int goldGained = 0;
/*  165 */   public static int cardsPurged = 0;
/*  166 */   public static int potionsBought = 0;
/*  167 */   public static int mysteryMachine = 0;
/*  168 */   public static float playtime = 0.0F;
/*  169 */   public static boolean stopClock = false;
/*      */   
/*      */   public static com.esotericsoftware.spine.SkeletonRendererDebug debugRenderer;
/*      */   
/*  173 */   public static AbstractTrial trial = null;
/*      */   
/*      */   public static Scanner sControllerScanner;
/*      */   
/*  177 */   SteamInputHandler sInputHandler = null;
/*  178 */   Thread sInputThread = null;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private SteamUtils clientUtils;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  196 */   private static Color screenColor = Color.BLACK.cpy();
/*  197 */   public static float screenTimer = 2.0F; public static float screenTime = 2.0F;
/*  198 */   private static boolean fadeIn = true;
/*      */   
/*      */ 
/*      */   public static MetricData metricData;
/*      */   
/*      */ 
/*      */   public static LocalizedStrings languagePack;
/*      */   
/*      */ 
/*  207 */   private boolean displayCursor = true;
/*  208 */   public static boolean displayVersion = true;
/*      */   
/*      */ 
/*  211 */   public static String preferenceDir = null;
/*  212 */   private static final Logger logger = LogManager.getLogger(CardCrawlGame.class.getName());
/*      */   
/*      */   public CardCrawlGame(String prefDir) {
/*  215 */     preferenceDir = prefDir;
/*      */   }
/*      */   
/*      */   public void create()
/*      */   {
/*  220 */     if (Settings.isBeta) {
/*  221 */       VERSION_NUM += " BETA";
/*      */     }
/*      */     try {
/*  224 */       TwitchConfig.createConfig();
/*  225 */       SteamSaveSync.initializeSteamApi();
/*  226 */       if (SteamAPI.isSteamRunning()) {
/*  227 */         SteamSaveSync.requestGlobalStats(365);
/*      */       }
/*      */       
/*      */ 
/*  231 */       saveMigration();
/*  232 */       checkIfFilesExist();
/*      */       
/*  234 */       saveSlotPref = SaveHelper.getPrefs("STSSaveSlots");
/*  235 */       saveSlot = saveSlotPref.getInteger("DEFAULT_SLOT", 0);
/*  236 */       playerPref = SaveHelper.getPrefs("STSPlayer");
/*  237 */       playerName = saveSlotPref.getString(SaveHelper.slotName("PROFILE_NAME", saveSlot), "");
/*  238 */       if (playerName.equals("")) {
/*  239 */         playerName = playerPref.getString("name", "");
/*      */       }
/*  241 */       alias = playerPref.getString("alias", "");
/*      */       
/*  243 */       if (alias.equals("")) {
/*  244 */         alias = generateRandomAlias();
/*  245 */         playerPref.putString("alias", alias);
/*  246 */         playerPref.flush();
/*      */       }
/*      */       
/*      */ 
/*  250 */       Settings.initialize(false);
/*      */       
/*      */ 
/*  253 */       this.camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
/*  254 */       if ((Settings.VERT_LETTERBOX_AMT != 0) || (Settings.HORIZ_LETTERBOX_AMT != 0)) {
/*  255 */         this.camera.position.set(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 0.0F);
/*  256 */         this.camera.update();
/*  257 */         viewport = new FitViewport(Settings.WIDTH, Settings.M_H - Settings.HEIGHT / 2, this.camera);
/*      */       } else {
/*  259 */         this.camera.position.set(this.camera.viewportWidth / 2.0F, this.camera.viewportHeight / 2.0F, 0.0F);
/*  260 */         this.camera.update();
/*  261 */         viewport = new FitViewport(Settings.WIDTH, Settings.HEIGHT, this.camera);
/*  262 */         viewport.apply();
/*      */       }
/*      */       
/*  265 */       languagePack = new LocalizedStrings();
/*  266 */       cardPopup = new SingleCardViewPopup();
/*  267 */       relicPopup = new SingleRelicViewPopup();
/*      */       
/*  269 */       if (Settings.IS_FULLSCREEN) {
/*  270 */         resize(Settings.M_W, Settings.M_H);
/*      */       }
/*  272 */       Gdx.graphics.setCursor(Gdx.graphics.newCursor(new Pixmap(Gdx.files.internal("images/blank.png")), 0, 0));
/*  273 */       this.sb = new SpriteBatch();
/*  274 */       psb = new PolygonSpriteBatch();
/*      */       
/*      */ 
/*  277 */       music = new MusicMaster();
/*  278 */       sound = new SoundMaster();
/*      */       
/*  280 */       SaveAndContinue.initialize();
/*  281 */       AbstractCreature.initialize();
/*  282 */       AbstractCard.initialize();
/*  283 */       GameDictionary.initialize();
/*  284 */       ImageMaster.initialize();
/*  285 */       AbstractPower.initialize();
/*  286 */       FontHelper.initialize();
/*  287 */       UnlockTracker.initialize();
/*  288 */       CardLibrary.initialize();
/*  289 */       RelicLibrary.initialize();
/*  290 */       InputHelper.initialize();
/*  291 */       TipTracker.initialize();
/*  292 */       ModHelper.initialize();
/*  293 */       ShaderHelper.initializeShaders();
/*      */       
/*  295 */       if (!Settings.isDev) {
/*  296 */         UnlockTracker.retroactiveUnlock();
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  305 */       cursor = new GameCursor();
/*  306 */       metricData = new MetricData();
/*  307 */       cancelButton = new CancelButton();
/*      */       
/*  309 */       tips = new GameTips();
/*      */       
/*  311 */       splashScreen = new SplashScreen();
/*  312 */       mode = GameMode.SPLASH;
/*  313 */       CInputHelper.loadSettings();
/*  314 */       logger.info("[GC] BEFORE: " + String.valueOf(SystemStats.getUsedMemory()));
/*  315 */       System.gc();
/*  316 */       logger.info("[GC] AFTER: " + String.valueOf(SystemStats.getUsedMemory()));
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  320 */       ExceptionHandler.handleException(e, logger);
/*  321 */       LogManager.shutdown();
/*  322 */       Gdx.app.exit();
/*      */     }
/*      */   }
/*      */   
/*      */   public static void reloadPrefs() {
/*  327 */     playerPref = SaveHelper.getPrefs("STSPlayer");
/*      */     
/*  329 */     alias = playerPref.getString("alias", "");
/*  330 */     if (alias.equals("")) {
/*  331 */       alias = generateRandomAlias();
/*  332 */       playerPref.putString("alias", alias);
/*      */     }
/*      */     
/*  335 */     ironcladPrefs = SaveHelper.getPrefs("STSDataVagabond");
/*  336 */     silentPrefs = SaveHelper.getPrefs("STSDataTheSilent");
/*  337 */     defectPrefs = SaveHelper.getPrefs("STSDataDefect");
/*  338 */     music.fadeOutBGM();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  348 */     InputActionSet.prefs = SaveHelper.getPrefs("STSInputSettings");
/*  349 */     InputActionSet.load();
/*  350 */     Settings.initialize(true);
/*  351 */     SaveAndContinue.initialize();
/*  352 */     UnlockTracker.initialize();
/*  353 */     CardLibrary.resetForReload();
/*  354 */     CardLibrary.initialize();
/*  355 */     RelicLibrary.resetForReload();
/*  356 */     RelicLibrary.initialize();
/*      */     
/*  358 */     TipTracker.initialize();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  363 */     screenColor.a = 0.0F;
/*  364 */     screenTime = 0.01F;
/*  365 */     screenTimer = 0.01F;
/*  366 */     fadeIn = false;
/*  367 */     startOver = true;
/*      */   }
/*      */   
/*      */   public void saveMigration()
/*      */   {
/*  372 */     if (!SaveHelper.saveExists()) {
/*  373 */       Preferences p = Gdx.app.getPreferences("STSPlayer");
/*      */       
/*      */ 
/*  376 */       if ((p.getString("name", "").equals("")) && (Gdx.app.getPreferences("STSDataVagabond").getLong("PLAYTIME") == 0L))
/*      */       {
/*  378 */         System.out.println("New player, no migration.");
/*  379 */         return;
/*      */       }
/*  381 */       System.out.println("Migrating Save...");
/*  382 */       migrateHelper("STSPlayer");
/*  383 */       migrateHelper("STSUnlocks");
/*  384 */       migrateHelper("STSUnlockProgress");
/*  385 */       migrateHelper("STSTips");
/*  386 */       migrateHelper("STSSound");
/*  387 */       migrateHelper("STSSeenRelics");
/*  388 */       migrateHelper("STSSeenCards");
/*  389 */       migrateHelper("STSSeenBosses");
/*  390 */       migrateHelper("STSGameplaySettings");
/*  391 */       migrateHelper("STSDataVagabond");
/*  392 */       migrateHelper("STSDataTheSilent");
/*  393 */       migrateHelper("STSAchievements");
/*      */       
/*  395 */       if (MathUtils.randomBoolean(0.5F)) {
/*  396 */         logger.warn("Save Migration");
/*      */       }
/*      */     }
/*      */     else {
/*  400 */       System.out.println("No migration");
/*      */     }
/*      */   }
/*      */   
/*      */   public void checkIfFilesExist() {
/*  405 */     if ((Settings.isBeta) && 
/*  406 */       (SaveHelper.doesPrefExist("STSDataTheSilent")) && (!SaveHelper.doesPrefExist("STSDataVagabond"))) {
/*  407 */       logger.warn("Silent Save Exists but NOT Ironclad.");
/*      */     }
/*      */   }
/*      */   
/*      */   public void migrateHelper(String file)
/*      */   {
/*  413 */     Preferences p = Gdx.app.getPreferences(file);
/*  414 */     Prefs p2 = SaveHelper.getPrefs(file);
/*      */     
/*  416 */     Map<String, ?> map = p.get();
/*  417 */     for (Map.Entry<String, ?> c : map.entrySet()) {
/*  418 */       p2.putString((String)c.getKey(), p.getString((String)c.getKey()));
/*      */     }
/*  420 */     p2.flush();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void render()
/*      */   {
/*      */     try
/*      */     {
/*      */       
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  447 */       if (Gdx.graphics.getRawDeltaTime() > 0.1F) {
/*  448 */         return;
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  462 */       CInputHelper.initializeIfAble();
/*      */       
/*      */ 
/*      */ 
/*  466 */       update();
/*      */       
/*  468 */       this.sb.setProjectionMatrix(this.camera.combined);
/*  469 */       psb.setProjectionMatrix(this.camera.combined);
/*  470 */       Gdx.gl.glClear(16384);
/*  471 */       this.sb.begin();
/*  472 */       this.sb.setColor(Color.WHITE);
/*      */       
/*  474 */       switch (mode) {
/*      */       case SPLASH: 
/*  476 */         splashScreen.render(this.sb);
/*  477 */         break;
/*      */       case CHAR_SELECT: 
/*  479 */         mainMenuScreen.render(this.sb);
/*  480 */         break;
/*      */       case GAMEPLAY: 
/*  482 */         if (dungeonTransitionScreen != null) {
/*  483 */           dungeonTransitionScreen.render(this.sb);
/*  484 */         } else if (dungeon != null) {
/*  485 */           dungeon.render(this.sb);
/*      */         }
/*      */         
/*      */         break;
/*      */       case DUNGEON_TRANSITION: 
/*      */         break;
/*      */       default: 
/*  492 */         logger.info("Unknown Game Mode: " + mode.name());
/*      */       }
/*      */       
/*      */       
/*  496 */       DrawMaster.draw(this.sb);
/*      */       
/*  498 */       if (cardPopup.isOpen) {
/*  499 */         cardPopup.render(this.sb);
/*      */       }
/*      */       
/*  502 */       if (relicPopup.isOpen) {
/*  503 */         relicPopup.render(this.sb);
/*      */       }
/*      */       
/*  506 */       com.megacrit.cardcrawl.helpers.TipHelper.render(this.sb);
/*      */       
/*  508 */       if (mode != GameMode.SPLASH) {
/*  509 */         renderBlackFadeScreen(this.sb);
/*  510 */         if (this.displayCursor) {
/*  511 */           if ((isPopupOpen) && 
/*  512 */             (isPopupOpen)) {
/*  513 */             InputHelper.mX = popupMX;
/*  514 */             InputHelper.mY = popupMY;
/*      */           }
/*      */           
/*  517 */           cursor.render(this.sb);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*  524 */       if (Settings.HORIZ_LETTERBOX_AMT != 0) {
/*  525 */         this.sb.setColor(Color.BLACK);
/*  526 */         this.sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, -Settings.HORIZ_LETTERBOX_AMT);
/*  527 */         this.sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, Settings.HEIGHT, Settings.WIDTH, Settings.HORIZ_LETTERBOX_AMT);
/*  528 */       } else if (Settings.VERT_LETTERBOX_AMT != 0) {
/*  529 */         this.sb.setColor(Color.BLACK);
/*  530 */         this.sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, -Settings.VERT_LETTERBOX_AMT, Settings.HEIGHT);
/*  531 */         this.sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH, 0.0F, Settings.VERT_LETTERBOX_AMT, Settings.HEIGHT);
/*      */       }
/*      */       
/*  534 */       this.sb.end();
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  539 */       ExceptionHandler.handleException(e, logger);
/*  540 */       LogManager.shutdown();
/*  541 */       Gdx.app.exit();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBlackFadeScreen(SpriteBatch sb)
/*      */   {
/*  574 */     sb.setColor(screenColor);
/*  575 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*      */   }
/*      */   
/*      */   public void updateFade() {
/*  579 */     if (screenTimer != 0.0F) {
/*  580 */       screenTimer -= Gdx.graphics.getDeltaTime();
/*  581 */       if (screenTimer < 0.0F) {
/*  582 */         screenTimer = 0.0F;
/*      */       }
/*      */       
/*  585 */       if (fadeIn) {
/*  586 */         screenColor.a = Interpolation.fade.apply(1.0F, 0.0F, 1.0F - screenTimer / screenTime);
/*      */       } else {
/*  588 */         screenColor.a = Interpolation.fade.apply(0.0F, 1.0F, 1.0F - screenTimer / screenTime);
/*      */         
/*      */ 
/*  591 */         if ((startOver) && (screenTimer == 0.0F)) {
/*  592 */           if (AbstractDungeon.scene != null) {
/*  593 */             AbstractDungeon.scene.fadeOutAmbiance();
/*      */           }
/*  595 */           long startTime = System.currentTimeMillis();
/*  596 */           AbstractDungeon.screen = AbstractDungeon.CurrentScreen.NONE;
/*  597 */           AbstractDungeon.reset();
/*  598 */           AbstractRelic.relicPage = 0;
/*  599 */           com.megacrit.cardcrawl.ui.panels.SeedPanel.textField = "";
/*  600 */           DailyMods.setModsFalse();
/*  601 */           Settings.seed = null;
/*  602 */           Settings.specialSeed = null;
/*  603 */           Settings.isTrial = false;
/*  604 */           Settings.isDailyRun = false;
/*  605 */           Settings.isEndless = false;
/*  606 */           trial = null;
/*      */           
/*  608 */           logger.info("Dungeon Reset: " + (System.currentTimeMillis() - startTime) + "ms");
/*  609 */           startTime = System.currentTimeMillis();
/*      */           
/*  611 */           ShopScreen.resetPurgeCost();
/*  612 */           tips.initialize();
/*  613 */           metricData.clearData();
/*      */           
/*  615 */           logger.info("Shop Screen Rest, Tips Initialize, Metric Data Clear: " + (
/*  616 */             System.currentTimeMillis() - startTime) + "ms");
/*      */           
/*  618 */           startTime = System.currentTimeMillis();
/*      */           
/*  620 */           UnlockTracker.refresh();
/*      */           
/*  622 */           logger.info("Unlock Tracker Refresh:  " + (System.currentTimeMillis() - startTime) + "ms");
/*  623 */           startTime = System.currentTimeMillis();
/*      */           
/*  625 */           mainMenuScreen = new MainMenuScreen();
/*  626 */           mainMenuScreen.bg.slideDownInstantly();
/*      */           
/*      */ 
/*  629 */           saveSlotPref.putFloat(
/*  630 */             SaveHelper.slotName("COMPLETION", saveSlot), 
/*  631 */             UnlockTracker.getCompletionPercentage());
/*      */           
/*      */ 
/*  634 */           saveSlotPref.putLong(
/*  635 */             SaveHelper.slotName("PLAYTIME", saveSlot), 
/*  636 */             UnlockTracker.getTotalPlaytime());
/*      */           
/*      */ 
/*  639 */           saveSlotPref.flush();
/*      */           
/*  641 */           logger.info("New Main Menu Screen: " + (System.currentTimeMillis() - startTime) + "ms");
/*  642 */           startTime = System.currentTimeMillis();
/*      */           
/*  644 */           CardHelper.clear();
/*  645 */           mode = GameMode.CHAR_SELECT;
/*  646 */           nextDungeon = "Exordium";
/*  647 */           dungeonTransitionScreen = new DungeonTransitionScreen("Exordium");
/*  648 */           TipTracker.refresh();
/*      */           
/*  650 */           logger.info("[GC] BEFORE: " + String.valueOf(SystemStats.getUsedMemory()));
/*  651 */           System.gc();
/*  652 */           logger.info("[GC] AFTER: " + String.valueOf(SystemStats.getUsedMemory()));
/*      */           
/*  654 */           logger.info("New Transition Screen, Tip Tracker Refresh: " + (
/*  655 */             System.currentTimeMillis() - startTime) + "ms");
/*      */           
/*  657 */           startTime = System.currentTimeMillis();
/*      */           
/*  659 */           fadeIn(2.0F);
/*      */           
/*      */ 
/*  662 */           if (queueCredits) {
/*  663 */             queueCredits = false;
/*  664 */             mainMenuScreen.creditsScreen.open();
/*  665 */             mainMenuScreen.hideMenuButtons();
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public static void fadeIn(float duration) {
/*  673 */     screenColor.a = 1.0F;
/*  674 */     screenTime = duration;
/*  675 */     screenTimer = duration;
/*  676 */     fadeIn = true;
/*      */   }
/*      */   
/*      */   public static void fadeToBlack(float duration) {
/*  680 */     screenColor.a = 0.0F;
/*  681 */     screenTime = duration;
/*  682 */     screenTimer = duration;
/*  683 */     fadeIn = false;
/*      */   }
/*      */   
/*      */   public static void startOver() {
/*  687 */     startOver = true;
/*  688 */     fadeToBlack(2.0F);
/*      */   }
/*      */   
/*      */   public static void startOverButShowCredits() {
/*  692 */     startOver = true;
/*  693 */     queueCredits = true;
/*  694 */     fadeToBlack(2.0F);
/*      */   }
/*      */   
/*      */   public static void startNewGame(AbstractPlayer.PlayerClass c)
/*      */   {
/*  699 */     if (AbstractDungeon.scene != null) {
/*  700 */       AbstractDungeon.scene.fadeOutAmbiance();
/*      */     }
/*      */     
/*  703 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.NONE;
/*  704 */     AbstractDungeon.reset();
/*  705 */     ShopScreen.resetPurgeCost();
/*  706 */     tips.initialize();
/*  707 */     metricData.clearData();
/*  708 */     mainMenuScreen = new MainMenuScreen(false);
/*  709 */     mainMenuScreen.bg.slideDownInstantly();
/*  710 */     CardHelper.clear();
/*  711 */     mode = GameMode.CHAR_SELECT;
/*  712 */     nextDungeon = "Exordium";
/*  713 */     dungeonTransitionScreen = new DungeonTransitionScreen("Exordium");
/*  714 */     UnlockTracker.refresh();
/*  715 */     TipTracker.refresh();
/*      */     
/*      */ 
/*  718 */     chosenCharacter = c;
/*  719 */     AbstractDungeon.player = createCharacter(c);
/*      */     
/*  721 */     for (AbstractCard i : AbstractDungeon.player.masterDeck.group) {
/*  722 */       if (i.rarity != AbstractCard.CardRarity.BASIC) {
/*  723 */         CardHelper.obtain(i.cardID, i.rarity, i.color);
/*      */       }
/*      */     }
/*      */     
/*  727 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  728 */       r.updateDescription(AbstractDungeon.player.chosenClass);
/*  729 */       r.onEquip();
/*      */     }
/*      */     
/*  732 */     mode = GameMode.GAMEPLAY;
/*  733 */     nextDungeon = "Exordium";
/*  734 */     dungeonTransitionScreen = new DungeonTransitionScreen("Exordium");
/*      */   }
/*      */   
/*      */   public static void resetScoreVars() {
/*  738 */     monstersSlain = 0;
/*  739 */     elites1Slain = 0;
/*  740 */     elites2Slain = 0;
/*  741 */     elites3Slain = 0;
/*  742 */     if (dungeon != null) {
/*  743 */       AbstractDungeon.bossCount = 0;
/*      */     }
/*  745 */     champion = 0;
/*  746 */     perfect = 0;
/*  747 */     overkill = false;
/*  748 */     combo = false;
/*  749 */     goldGained = 0;
/*  750 */     cardsPurged = 0;
/*  751 */     potionsBought = 0;
/*  752 */     mysteryMachine = 0;
/*  753 */     playtime = 0.0F;
/*  754 */     stopClock = false;
/*      */   }
/*      */   
/*      */   public void update() {
/*  758 */     cursor.update();
/*  759 */     screenShake.update(viewport);
/*  760 */     if (mode != GameMode.SPLASH) {
/*  761 */       updateFade();
/*      */     }
/*  763 */     music.update();
/*  764 */     sound.update();
/*      */     
/*  766 */     if (Settings.isDebug) {
/*  767 */       if (DevInputActionSet.toggleCursor.isJustPressed()) {
/*  768 */         this.displayCursor = (!this.displayCursor);
/*  769 */       } else if (DevInputActionSet.toggleVersion.isJustPressed()) {
/*  770 */         displayVersion = !displayVersion;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  775 */     if (this.sInputHandler != null) {
/*  776 */       processUpdate();
/*  777 */     } else if (CInputHelper.controller != null) {
/*  778 */       CInputHelper.updateFirst();
/*      */     }
/*  780 */     InputHelper.updateFirst();
/*      */     
/*  782 */     if (cardPopup.isOpen) {
/*  783 */       cardPopup.update();
/*      */     }
/*  785 */     if (relicPopup.isOpen) {
/*  786 */       relicPopup.update();
/*      */     }
/*      */     
/*  789 */     if (isPopupOpen) {
/*  790 */       popupMX = InputHelper.mX;
/*  791 */       popupMY = InputHelper.mY;
/*  792 */       InputHelper.mX = 55537;
/*  793 */       InputHelper.mY = 55537;
/*      */     }
/*      */     
/*  796 */     switch (mode) {
/*      */     case SPLASH: 
/*  798 */       splashScreen.update();
/*  799 */       if (splashScreen.isDone) {
/*  800 */         mode = GameMode.CHAR_SELECT;
/*  801 */         splashScreen = null;
/*      */         
/*  803 */         if (ironcladPrefs == null) {
/*  804 */           ironcladPrefs = SaveHelper.getPrefs("STSDataVagabond");
/*  805 */           silentPrefs = SaveHelper.getPrefs("STSDataTheSilent");
/*  806 */           defectPrefs = SaveHelper.getPrefs("STSDataDefect");
/*      */         }
/*      */         
/*  809 */         mainMenuScreen = new MainMenuScreen();
/*      */       }
/*      */       break;
/*      */     case CHAR_SELECT: 
/*  813 */       mainMenuScreen.update();
/*  814 */       if (mainMenuScreen.fadedOut) {
/*  815 */         AbstractDungeon.pathX = new ArrayList();
/*  816 */         AbstractDungeon.pathY = new ArrayList();
/*      */         
/*  818 */         if ((trial == null) && (Settings.specialSeed != null)) {
/*  819 */           trial = com.megacrit.cardcrawl.helpers.TrialHelper.getTrialForSeed(SeedHelper.getString(Settings.specialSeed.longValue()));
/*      */         }
/*      */         
/*  822 */         if (loadingSave) {
/*  823 */           DailyMods.setModsFalse();
/*  824 */           AbstractDungeon.player = createCharacter(chosenCharacter);
/*  825 */           loadPlayerSave(AbstractDungeon.player);
/*      */         }
/*  827 */         else if (trial == null) {
/*  828 */           if (Settings.isDailyRun) {
/*  829 */             AbstractDungeon.ascensionLevel = 0;
/*  830 */             AbstractDungeon.isAscensionMode = false;
/*      */           }
/*  832 */           AbstractDungeon.player = createCharacter(chosenCharacter);
/*  833 */           for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  834 */             r.updateDescription(AbstractDungeon.player.chosenClass);
/*  835 */             r.onEquip();
/*      */           }
/*      */           
/*      */ 
/*  839 */           for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  840 */             if (c.rarity != AbstractCard.CardRarity.BASIC) {
/*  841 */               CardHelper.obtain(c.cardID, c.rarity, c.color);
/*      */             }
/*      */           }
/*      */         }
/*      */         else {
/*  846 */           Settings.isTrial = true;
/*  847 */           Settings.isDailyRun = false;
/*      */           
/*  849 */           setupTrialMods(trial);
/*  850 */           setupTrialPlayer(trial);
/*      */         }
/*      */         
/*  853 */         mode = GameMode.GAMEPLAY;
/*  854 */         nextDungeon = "Exordium";
/*  855 */         dungeonTransitionScreen = new DungeonTransitionScreen("Exordium");
/*      */         
/*  857 */         if (loadingSave) {
/*  858 */           dungeonTransitionScreen.isComplete = true;
/*      */         }
/*      */         else {
/*  861 */           monstersSlain = 0;
/*  862 */           elites1Slain = 0;
/*  863 */           elites2Slain = 0;
/*  864 */           elites3Slain = 0;
/*      */         }
/*      */       }
/*      */       break;
/*      */     case GAMEPLAY: 
/*  869 */       if (dungeonTransitionScreen != null) {
/*  870 */         dungeonTransitionScreen.update();
/*      */         
/*  872 */         if (dungeonTransitionScreen.isComplete) {
/*  873 */           dungeonTransitionScreen = null;
/*      */           
/*      */ 
/*  876 */           if (loadingSave) {
/*  877 */             getDungeon(saveFile.level_name, AbstractDungeon.player, saveFile);
/*  878 */             loadPostCombat(saveFile);
/*  879 */             if (!saveFile.post_combat) {
/*  880 */               loadingSave = false;
/*      */             }
/*      */           } else {
/*  883 */             getDungeon(nextDungeon, AbstractDungeon.player);
/*      */             
/*  885 */             if ((!nextDungeon.equals("Exordium")) || (Settings.isShowBuild) || (!((Boolean)TipTracker.tips.get("NEOW_SKIP")).booleanValue()))
/*      */             {
/*  887 */               AbstractDungeon.dungeonMapScreen.open(true);
/*  888 */               TipTracker.neverShowAgain("NEOW_SKIP");
/*      */             }
/*      */           }
/*      */         }
/*  892 */       } else if (dungeon != null) {
/*  893 */         dungeon.update();
/*      */       } else {
/*  895 */         logger.info("Eh-?");
/*      */       }
/*      */       
/*      */ 
/*  899 */       if ((dungeon != null) && (AbstractDungeon.isDungeonBeaten) && (AbstractDungeon.fadeColor.a == 1.0F)) {
/*  900 */         dungeon = null;
/*  901 */         AbstractDungeon.scene.fadeOutAmbiance();
/*  902 */         dungeonTransitionScreen = new DungeonTransitionScreen(nextDungeon);
/*      */       }
/*      */       
/*      */       break;
/*      */     case DUNGEON_TRANSITION: 
/*      */       break;
/*      */     default: 
/*  909 */       logger.info("Unknown Game Mode: " + mode.name());
/*      */     }
/*      */     
/*      */     
/*  913 */     updateDebugSwitch();
/*  914 */     InputHelper.updateLast();
/*  915 */     if (CInputHelper.controller != null) {
/*  916 */       CInputHelper.updateLast();
/*      */     }
/*      */     
/*  919 */     if (Settings.isInfo) {
/*  920 */       this.fpsLogger.log();
/*      */     }
/*      */   }
/*      */   
/*      */   private void processUpdate() {
/*  925 */     if (SteamInputHandler.setHandle == null) {
/*  926 */       return;
/*      */     }
/*      */     
/*  929 */     for (int i = 0; i < SteamInputHandler.numControllers; i++) {
/*  930 */       SteamControllerHandle handle = SteamInputHandler.controllerHandles[i];
/*  931 */       SteamInputHandler.controller.activateActionSet(handle, SteamInputHandler.setHandle);
/*      */       
/*  933 */       if (SteamInputHandler.digitalActionHandle != null) {
/*  934 */         SteamInputHandler.controller.getDigitalActionData(handle, SteamInputHandler.digitalActionHandle, SteamInputHandler.digitalActionData);
/*      */         
/*      */ 
/*      */ 
/*  938 */         if ((SteamInputHandler.digitalActionData.getActive()) && (SteamInputHandler.digitalActionData.getState())) {
/*  939 */           logger.info("  digital action: " + SteamNativeHandle.getNativeHandle(SteamInputHandler.digitalActionHandle));
/*      */         }
/*      */       }
/*      */       
/*  943 */       if (SteamInputHandler.analogActionHandle != null) {
/*  944 */         SteamInputHandler.controller.getAnalogActionData(handle, SteamInputHandler.analogActionHandle, SteamInputHandler.analogActionData);
/*      */         
/*      */ 
/*      */ 
/*  948 */         if (SteamInputHandler.analogActionData.getActive()) {
/*  949 */           float x = SteamInputHandler.analogActionData.getX();
/*  950 */           float y = SteamInputHandler.analogActionData.getY();
/*  951 */           SteamController.SourceMode mode = SteamInputHandler.analogActionData.getMode();
/*  952 */           if ((Math.abs(x) > 1.0E-4F) && (Math.abs(y) > 0.001F)) {
/*  953 */             logger.info("  analog action: " + SteamInputHandler.analogActionData
/*  954 */               .getX() + ", " + SteamInputHandler.analogActionData
/*  955 */               .getY() + ", " + mode.name());
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void setupTrialMods(AbstractTrial trial) {
/*  963 */     if (trial.useRandomDailyMods()) {
/*  964 */       long sourceTime = System.nanoTime();
/*  965 */       Random rng = new Random(Long.valueOf(sourceTime));
/*  966 */       Settings.seed = Long.valueOf(SeedHelper.generateUnoffensiveSeed(rng));
/*  967 */       Settings.dailyMods.initialize(Settings.seed.longValue());
/*      */     }
/*  969 */     else if (trial.dailyModIDs() != null) {
/*  970 */       Settings.dailyMods.setMods(trial.dailyModIDs());
/*      */     }
/*      */   }
/*      */   
/*      */   private void setupTrialPlayer(AbstractTrial trial)
/*      */   {
/*  976 */     AbstractDungeon.player = trial.setupPlayer(createCharacter(chosenCharacter));
/*      */     
/*      */ 
/*  979 */     if (!trial.keepStarterRelic()) {
/*  980 */       AbstractDungeon.player.relics.clear();
/*      */     }
/*  982 */     for (String relicID : trial.extraStartingRelicIDs()) {
/*  983 */       AbstractRelic relic = RelicLibrary.getRelic(relicID);
/*  984 */       relic.instantObtain(AbstractDungeon.player, AbstractDungeon.player.relics.size(), false);
/*  985 */       AbstractDungeon.relicsToRemoveOnStart.add(relic.relicId);
/*      */     }
/*      */     
/*  988 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  989 */       r.updateDescription(AbstractDungeon.player.chosenClass);
/*  990 */       r.onEquip();
/*      */     }
/*      */     
/*      */ 
/*  994 */     if (!trial.keepsStarterCards()) {
/*  995 */       AbstractDungeon.player.masterDeck.clear();
/*      */     }
/*  997 */     for (String cardID : trial.extraStartingCardIDs()) {
/*  998 */       AbstractDungeon.player.masterDeck.addToTop(CardLibrary.getCard(cardID).makeCopy());
/*      */     }
/*      */   }
/*      */   
/*      */   private void loadPostCombat(SaveFile saveFile) {
/* 1003 */     if (saveFile.post_combat) {
/* 1004 */       AbstractDungeon.getCurrRoom().isBattleOver = true;
/* 1005 */       AbstractDungeon.overlayMenu.hideCombatPanels();
/* 1006 */       AbstractDungeon.loading_post_combat = true;
/* 1007 */       AbstractDungeon.getCurrRoom().smoked = saveFile.smoked;
/* 1008 */       AbstractDungeon.getCurrRoom().mugged = saveFile.mugged;
/*      */       
/* 1010 */       if (AbstractDungeon.getCurrRoom().event != null) {
/* 1011 */         AbstractDungeon.getCurrRoom().event.postCombatLoad();
/*      */       }
/*      */       
/*      */ 
/* 1015 */       if (AbstractDungeon.getCurrRoom().monsters != null) {
/* 1016 */         AbstractDungeon.getCurrRoom().monsters.monsters.clear();
/* 1017 */         AbstractDungeon.actionManager.actions.clear();
/*      */       }
/*      */       
/* 1020 */       if (!saveFile.smoked) {
/* 1021 */         for (RewardSave i : saveFile.combat_rewards) {
/* 1022 */           switch (i.type)
/*      */           {
/*      */           case "CARD": 
/*      */             break;
/*      */           case "GOLD": 
/* 1027 */             AbstractDungeon.getCurrRoom().addGoldToRewards(i.amount);
/* 1028 */             break;
/*      */           case "RELIC": 
/* 1030 */             AbstractDungeon.getCurrRoom().addRelicToRewards(RelicLibrary.getRelic(i.id).makeCopy());
/* 1031 */             break;
/*      */           case "POTION": 
/* 1033 */             AbstractDungeon.getCurrRoom().addPotionToRewards(PotionHelper.getPotion(i.id));
/* 1034 */             break;
/*      */           case "STOLEN_GOLD": 
/* 1036 */             AbstractDungeon.getCurrRoom().addStolenGoldToRewards(i.amount);
/* 1037 */             break;
/*      */           default: 
/* 1039 */             System.out.println("Loading unknown type: " + i.type);
/*      */           }
/*      */           
/*      */         }
/*      */       }
/*      */       
/* 1045 */       if ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 1046 */         AbstractDungeon.scene.fadeInAmbiance();
/* 1047 */         music.silenceTempBgmInstantly();
/* 1048 */         music.silenceBGMInstantly();
/*      */         
/* 1050 */         AbstractMonster.playBossStinger();
/* 1051 */       } else if ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomElite)) {
/* 1052 */         AbstractDungeon.scene.fadeInAmbiance();
/* 1053 */         music.fadeOutTempBGM();
/*      */       }
/* 1055 */       saveFile.post_combat = false;
/*      */     }
/*      */   }
/*      */   
/*      */   private void loadPlayerSave(AbstractPlayer p) {
/* 1060 */     saveFile = SaveAndContinue.loadSaveFile(p.chosenClass);
/*      */     
/*      */ 
/* 1063 */     Settings.seed = Long.valueOf(saveFile.seed);
/* 1064 */     Settings.isDailyRun = saveFile.is_daily;
/* 1065 */     if (Settings.isDailyRun) {
/* 1066 */       Settings.dailyDate = saveFile.daily_date;
/*      */     }
/* 1068 */     Settings.specialSeed = Long.valueOf(saveFile.special_seed);
/* 1069 */     Settings.isTrial = saveFile.is_trial;
/*      */     
/* 1071 */     if (Settings.isTrial) {
/* 1072 */       Settings.dailyMods.initialize(Settings.seed.longValue());
/* 1073 */       AbstractPlayer.customMods = saveFile.custom_mods;
/* 1074 */     } else if (Settings.isDailyRun) {
/* 1075 */       Settings.dailyMods.initialize(Settings.specialSeed.longValue());
/*      */     }
/*      */     
/* 1078 */     AbstractPlayer.customMods = saveFile.custom_mods;
/*      */     
/* 1080 */     if (AbstractPlayer.customMods == null) {
/* 1081 */       AbstractPlayer.customMods = new ArrayList();
/*      */     }
/*      */     
/*      */ 
/* 1085 */     p.currentHealth = saveFile.current_health;
/* 1086 */     p.maxHealth = saveFile.max_health;
/* 1087 */     p.gold = saveFile.gold;
/* 1088 */     p.displayGold = p.gold;
/* 1089 */     p.masterHandSize = saveFile.hand_size;
/* 1090 */     p.potionSlots = saveFile.potion_slots;
/* 1091 */     if (p.potionSlots == 0) {
/* 1092 */       p.potionSlots = 3;
/*      */     }
/* 1094 */     p.potions.clear();
/* 1095 */     for (int i = 0; i < p.potionSlots; i++) {
/* 1096 */       p.potions.add(new PotionSlot(i));
/*      */     }
/* 1098 */     p.masterMaxOrbs = saveFile.max_orbs;
/* 1099 */     p.energy = new EnergyManager(saveFile.red + saveFile.green + saveFile.blue);
/*      */     
/*      */ 
/* 1102 */     monstersSlain = saveFile.monsters_killed;
/* 1103 */     elites1Slain = saveFile.elites1_killed;
/* 1104 */     elites2Slain = saveFile.elites2_killed;
/* 1105 */     elites3Slain = saveFile.elites3_killed;
/* 1106 */     goldGained = saveFile.gold_gained;
/* 1107 */     champion = saveFile.champions;
/* 1108 */     perfect = saveFile.perfect;
/* 1109 */     combo = saveFile.combo;
/* 1110 */     overkill = saveFile.overkill;
/* 1111 */     mysteryMachine = saveFile.mystery_machine;
/* 1112 */     playtime = (float)saveFile.play_time;
/* 1113 */     AbstractDungeon.ascensionLevel = saveFile.ascension_level;
/* 1114 */     AbstractDungeon.isAscensionMode = saveFile.is_ascension_mode;
/*      */     
/*      */ 
/* 1117 */     p.masterDeck.clear();
/* 1118 */     for (i = saveFile.cards.iterator(); i.hasNext();) { s = (CardSave)i.next();
/* 1119 */       logger.info(s.id + ", " + s.upgrades);
/* 1120 */       p.masterDeck.addToTop(CardLibrary.getCopy(s.id, s.upgrades, s.misc));
/*      */     }
/*      */     
/*      */     CardSave s;
/* 1124 */     Settings.isEndless = saveFile.is_endless_mode;
/*      */     
/* 1126 */     int index = 0;
/* 1127 */     p.blights.clear();
/*      */     
/* 1129 */     if (saveFile.blights != null) {
/* 1130 */       for (String b : saveFile.blights)
/*      */       {
/* 1132 */         AbstractBlight blight = BlightHelper.getBlight(b);
/* 1133 */         if (blight != null)
/*      */         {
/* 1135 */           int incrementAmount = ((Integer)saveFile.endless_increments.get(index)).intValue();
/* 1136 */           for (int i = 0; i < incrementAmount; i++) {
/* 1137 */             blight.incrementUp();
/*      */           }
/* 1139 */           blight.setIncrement(incrementAmount);
/* 1140 */           blight.instantObtain(AbstractDungeon.player, index, false);
/*      */         }
/* 1142 */         index++;
/*      */       }
/*      */       
/* 1145 */       if (saveFile.blight_counters != null)
/*      */       {
/* 1147 */         index = 0;
/* 1148 */         for (Integer i : saveFile.blight_counters) {
/* 1149 */           ((AbstractBlight)p.blights.get(index)).setCounter(i.intValue());
/* 1150 */           ((AbstractBlight)p.blights.get(index)).updateDescription(p.chosenClass);
/* 1151 */           index++;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1157 */     p.relics.clear();
/* 1158 */     index = 0;
/* 1159 */     for (String s : saveFile.relics) {
/* 1160 */       AbstractRelic r = RelicLibrary.getRelic(s).makeCopy();
/* 1161 */       r.instantObtain(p, index, false);
/* 1162 */       r.setCounter(((Integer)saveFile.relic_counters.get(index)).intValue());
/* 1163 */       r.updateDescription(p.chosenClass);
/* 1164 */       index++;
/*      */     }
/*      */     
/*      */ 
/* 1168 */     index = 0;
/* 1169 */     for (String s : saveFile.potions) {
/* 1170 */       AbstractPotion potion = PotionHelper.getPotion(s);
/* 1171 */       if (potion != null) {
/* 1172 */         AbstractDungeon.player.obtainPotion(index, potion);
/*      */       }
/* 1174 */       index++;
/*      */     }
/*      */     
/*      */ 
/* 1178 */     if (saveFile.bottled_flame != null) {
/* 1179 */       AbstractCard c = AbstractDungeon.player.masterDeck.findCardByName(saveFile.bottled_flame);
/* 1180 */       if (c != null) {
/* 1181 */         c.inBottleFlame = true;
/* 1182 */         ((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).card = c;
/* 1183 */         ((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).setDescriptionAfterLoading();
/*      */       } else {
/* 1185 */         logger.warn("This didn't match any cardID we know: saveFile.bottled_flame=" + saveFile.bottled_flame);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1190 */     if (saveFile.bottled_lightning != null) {
/* 1191 */       AbstractCard c = AbstractDungeon.player.masterDeck.findCardByName(saveFile.bottled_lightning);
/* 1192 */       if (c != null) {
/* 1193 */         c.inBottleLightning = true;
/* 1194 */         ((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).card = c;
/* 1195 */         ((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).setDescriptionAfterLoading();
/*      */       } else {
/* 1197 */         logger.warn("This didn't match any cardID we know: saveFile.bottled_lightning=" + saveFile.bottled_lightning);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1203 */     if (saveFile.bottled_tornado != null) {
/* 1204 */       AbstractCard c = AbstractDungeon.player.masterDeck.findCardByName(saveFile.bottled_tornado);
/* 1205 */       if (c != null) {
/* 1206 */         c.inBottleTornado = true;
/* 1207 */         ((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).card = c;
/* 1208 */         ((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).setDescriptionAfterLoading();
/*      */       } else {
/* 1210 */         logger.warn("This didn't match any cardID we know: saveFile.bottled_tornado=" + saveFile.bottled_tornado);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1215 */     if ((saveFile.daily_mods != null) && (saveFile.daily_mods.size() > 0)) {
/* 1216 */       Settings.dailyMods.setMods(saveFile.daily_mods);
/*      */     }
/*      */     
/*      */ 
/* 1220 */     metricData.clearData();
/* 1221 */     metricData.campfire_rested = saveFile.metric_campfire_rested;
/* 1222 */     metricData.campfire_upgraded = saveFile.metric_campfire_upgraded;
/* 1223 */     metricData.purchased_purges = saveFile.metric_purchased_purges;
/* 1224 */     metricData.potions_floor_spawned = saveFile.metric_potions_floor_spawned;
/* 1225 */     metricData.current_hp_per_floor = saveFile.metric_current_hp_per_floor;
/* 1226 */     metricData.max_hp_per_floor = saveFile.metric_max_hp_per_floor;
/* 1227 */     metricData.gold_per_floor = saveFile.metric_gold_per_floor;
/* 1228 */     metricData.path_per_floor = saveFile.metric_path_per_floor;
/* 1229 */     metricData.path_taken = saveFile.metric_path_taken;
/* 1230 */     metricData.items_purchased = saveFile.metric_items_purchased;
/* 1231 */     metricData.items_purged = saveFile.metric_items_purged;
/* 1232 */     metricData.card_choices = saveFile.metric_card_choices;
/* 1233 */     metricData.event_choices = saveFile.metric_event_choices;
/* 1234 */     metricData.damage_taken = saveFile.metric_damage_taken;
/* 1235 */     metricData.boss_relics = saveFile.metric_boss_relics;
/*      */     
/*      */ 
/* 1238 */     if (saveFile.metric_potions_obtained != null) {
/* 1239 */       metricData.potions_obtained = saveFile.metric_potions_obtained;
/*      */     }
/* 1241 */     if (saveFile.metric_relics_obtained != null) {
/* 1242 */       metricData.relics_obtained = saveFile.metric_relics_obtained;
/*      */     }
/* 1244 */     if (saveFile.metric_campfire_choices != null) {
/* 1245 */       metricData.campfire_choices = saveFile.metric_campfire_choices;
/*      */     }
/* 1247 */     if (saveFile.metric_item_purchase_floors != null) {
/* 1248 */       metricData.item_purchase_floors = saveFile.metric_item_purchase_floors;
/*      */     }
/* 1250 */     if (saveFile.metric_items_purged_floors != null) {
/* 1251 */       metricData.items_purged_floors = saveFile.metric_items_purged_floors;
/*      */     }
/* 1253 */     if (saveFile.neow_bonus != null) {
/* 1254 */       metricData.neowBonus = saveFile.neow_bonus;
/*      */     }
/* 1256 */     if (saveFile.neow_cost != null) {
/* 1257 */       metricData.neowCost = saveFile.neow_cost;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private static AbstractPlayer createCharacter(AbstractPlayer.PlayerClass selection)
/*      */   {
/* 1266 */     AbstractPlayer p = null;
/* 1267 */     switch (selection) {
/*      */     case IRONCLAD: 
/* 1269 */       p = new Ironclad(playerName, AbstractPlayer.PlayerClass.IRONCLAD);
/* 1270 */       break;
/*      */     case THE_SILENT: 
/* 1272 */       p = new TheSilent(playerName, AbstractPlayer.PlayerClass.THE_SILENT);
/* 1273 */       break;
/*      */     case DEFECT: 
/* 1275 */       p = new com.megacrit.cardcrawl.characters.Defect(playerName, AbstractPlayer.PlayerClass.DEFECT);
/*      */     }
/*      */     
/*      */     
/* 1279 */     for (AbstractCard c : p.masterDeck.group) {
/* 1280 */       UnlockTracker.markCardAsSeen(c.cardID);
/*      */     }
/*      */     
/* 1283 */     return p;
/*      */   }
/*      */   
/*      */   private void updateDebugSwitch() {
/* 1287 */     if (!Settings.isDev) {
/* 1288 */       return;
/*      */     }
/*      */     
/* 1291 */     if (DevInputActionSet.toggleDebug.isJustPressed()) {
/* 1292 */       Settings.isDebug = !Settings.isDebug;
/* 1293 */       return;
/*      */     }
/*      */     
/* 1296 */     if (DevInputActionSet.toggleInfo.isJustPressed()) {
/* 1297 */       Settings.isInfo = !Settings.isInfo;
/* 1298 */       return;
/*      */     }
/*      */     
/* 1301 */     if (DevInputActionSet.uploadData.isJustPressed()) {
/* 1302 */       RelicLibrary.uploadRelicData();
/* 1303 */       CardLibrary.uploadCardData();
/* 1304 */       com.megacrit.cardcrawl.helpers.MonsterHelper.uploadEnemyData();
/* 1305 */       PotionHelper.uploadPotionData();
/* 1306 */       ModHelper.uploadModData();
/* 1307 */       BlightHelper.uploadBlightData();
/* 1308 */       return;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1313 */     if (!Settings.isDebug) {
/* 1314 */       return;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1319 */     if (DevInputActionSet.hideTopBar.isJustPressed()) {
/* 1320 */       Settings.hideTopBar = !Settings.hideTopBar;
/* 1321 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1325 */     if (DevInputActionSet.hidePopUps.isJustPressed()) {
/* 1326 */       Settings.hidePopupDetails = !Settings.hidePopupDetails;
/* 1327 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1331 */     if (DevInputActionSet.hideRelics.isJustPressed()) {
/* 1332 */       Settings.hideRelics = !Settings.hideRelics;
/* 1333 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1337 */     if (DevInputActionSet.hideCombatLowUI.isJustPressed()) {
/* 1338 */       Settings.hideLowerElements = !Settings.hideLowerElements;
/* 1339 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1343 */     if (DevInputActionSet.hideCards.isJustPressed()) {
/* 1344 */       Settings.hideCards = !Settings.hideCards;
/* 1345 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1349 */     if (DevInputActionSet.hideEndTurnButton.isJustPressed()) {
/* 1350 */       Settings.hideEndTurn = !Settings.hideEndTurn;
/* 1351 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 1352 */         m.damage(new DamageInfo(AbstractDungeon.player, m.currentHealth, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS));
/*      */       }
/* 1354 */       return;
/*      */     }
/*      */     
/*      */ 
/* 1358 */     if (DevInputActionSet.hideCombatInfo.isJustPressed()) {
/* 1359 */       Settings.hideCombatElements = !Settings.hideCombatElements;
/*      */     }
/*      */   }
/*      */   
/*      */   public void resize(int width, int height) {}
/*      */   
/*      */   public AbstractDungeon getDungeon(String key, AbstractPlayer p)
/*      */   {
/* 1367 */     switch (key) {
/*      */     case "Exordium": 
/* 1369 */       ArrayList<String> emptyList = new ArrayList();
/* 1370 */       return new Exordium(p, emptyList);
/*      */     case "TheCity": 
/* 1372 */       return new TheCity(p, AbstractDungeon.specialOneTimeEventList);
/*      */     case "TheBeyond": 
/* 1374 */       return new TheBeyond(p, AbstractDungeon.specialOneTimeEventList);
/*      */     }
/* 1376 */     return null;
/*      */   }
/*      */   
/*      */   public AbstractDungeon getDungeon(String key, AbstractPlayer p, SaveFile saveFile)
/*      */   {
/* 1381 */     switch (key) {
/*      */     case "Exordium": 
/* 1383 */       return new Exordium(p, saveFile);
/*      */     case "TheCity": 
/* 1385 */       return new TheCity(p, saveFile);
/*      */     case "TheBeyond": 
/* 1387 */       return new TheBeyond(p, saveFile);
/*      */     }
/* 1389 */     return null;
/*      */   }
/*      */   
/*      */   public static enum GameMode
/*      */   {
/* 1394 */     CHAR_SELECT,  GAMEPLAY,  DUNGEON_TRANSITION,  SPLASH;
/*      */     
/*      */     private GameMode() {}
/*      */   }
/*      */   
/* 1399 */   public void pause() { logger.info("PAUSE()");
/* 1400 */     Settings.isControllerMode = false;
/*      */     
/* 1402 */     if ((MUTE_IF_BG) && 
/* 1403 */       (mainMenuScreen != null)) {
/* 1404 */       Settings.isBackgrounded = true;
/* 1405 */       if (mode == GameMode.CHAR_SELECT) {
/* 1406 */         mainMenuScreen.muteAmbienceVolume();
/* 1407 */       } else if (AbstractDungeon.scene != null) {
/* 1408 */         AbstractDungeon.scene.muteAmbienceVolume();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void resume()
/*      */   {
/* 1416 */     logger.info("RESUME()");
/* 1417 */     if ((MUTE_IF_BG) && 
/* 1418 */       (mainMenuScreen != null)) {
/* 1419 */       Settings.isBackgrounded = false;
/* 1420 */       if (mode == GameMode.CHAR_SELECT) {
/* 1421 */         mainMenuScreen.updateAmbienceVolume();
/* 1422 */       } else if (AbstractDungeon.scene != null) {
/* 1423 */         AbstractDungeon.scene.updateAmbienceVolume();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void dispose()
/*      */   {
/* 1431 */     logger.info("Game shutting down...");
/* 1432 */     AsyncSaver.shutdownSaveThread();
/* 1433 */     if (SteamInputHandler.alive) {
/* 1434 */       SteamInputHandler.alive = false;
/* 1435 */       this.clientUtils.dispose();
/* 1436 */       SteamInputHandler.controller.shutdown();
/*      */     }
/* 1438 */     SteamAPI.shutdown();
/*      */   }
/*      */   
/*      */   public static String generateRandomAlias()
/*      */   {
/* 1443 */     String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
/* 1444 */     StringBuilder retVal = new StringBuilder();
/* 1445 */     for (int i = 0; i < 16; i++) {
/* 1446 */       retVal.append(alphabet.charAt(MathUtils.random(0, alphabet.length() - 1)));
/*      */     }
/* 1448 */     return retVal.toString();
/*      */   }
/*      */   
/*      */   public static boolean isInARun() {
/* 1452 */     return (mode == GameMode.GAMEPLAY) && (AbstractDungeon.player != null) && (!AbstractDungeon.player.isDead);
/*      */   }
/*      */   
/*      */   public static com.badlogic.gdx.graphics.Texture getSaveSlotImg()
/*      */   {
/* 1457 */     switch (saveSlot) {
/*      */     case 0: 
/* 1459 */       return ImageMaster.PROFILE_A;
/*      */     case 1: 
/* 1461 */       return ImageMaster.PROFILE_B;
/*      */     case 2: 
/* 1463 */       return ImageMaster.PROFILE_C;
/*      */     }
/* 1465 */     return ImageMaster.PROFILE_A;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\CardCrawlGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */