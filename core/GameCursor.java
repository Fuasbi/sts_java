/*     */ package com.megacrit.cardcrawl.core;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ 
/*     */ public class GameCursor
/*     */ {
/*     */   private com.badlogic.gdx.graphics.Texture img;
/*     */   private com.badlogic.gdx.graphics.Texture mImg;
/*     */   public static final int W = 64;
/*  12 */   public static boolean hidden = false;
/*     */   
/*     */   private float rotation;
/*  15 */   private static final float OFFSET_X = 24.0F * Settings.scale;
/*  16 */   private static final float OFFSET_Y = -24.0F * Settings.scale;
/*  17 */   private static final float SHADOW_OFFSET_X = -10.0F * Settings.scale;
/*  18 */   private static final float SHADOW_OFFSET_Y = 8.0F * Settings.scale;
/*  19 */   private static final Color SHADOW_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.15F);
/*     */   private static final float TILT_ANGLE = 6.0F;
/*  21 */   private CursorType type = CursorType.NORMAL;
/*     */   
/*     */   public static enum CursorType {
/*  24 */     NORMAL,  INSPECT;
/*     */     
/*     */     private CursorType() {} }
/*     */   
/*  28 */   public GameCursor() { this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/ui/cursors/gold2.png");
/*  29 */     this.mImg = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/ui/cursors/magGlass2.png");
/*     */   }
/*     */   
/*     */   public void update() {
/*  33 */     if (InputHelper.isMouseDown) {
/*  34 */       this.rotation = 6.0F;
/*     */     } else {
/*  36 */       this.rotation = 0.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeType(CursorType type) {
/*  41 */     this.type = type;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  45 */     if ((hidden) || (Settings.isControllerMode)) {
/*  46 */       return;
/*     */     }
/*  48 */     switch (this.type) {
/*     */     case NORMAL: 
/*  50 */       sb.setColor(SHADOW_COLOR);
/*  51 */       sb.draw(this.img, InputHelper.mX - 32.0F - SHADOW_OFFSET_X + OFFSET_X, InputHelper.mY - 32.0F - SHADOW_OFFSET_Y + OFFSET_Y, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, this.rotation, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  69 */       sb.setColor(Color.WHITE);
/*  70 */       sb.draw(this.img, InputHelper.mX - 32.0F + OFFSET_X, InputHelper.mY - 32.0F + OFFSET_Y, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, this.rotation, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  87 */       break;
/*     */     case INSPECT: 
/*  89 */       sb.setColor(SHADOW_COLOR);
/*  90 */       sb.draw(this.mImg, InputHelper.mX - 32.0F - SHADOW_OFFSET_X + OFFSET_X, InputHelper.mY - 32.0F - SHADOW_OFFSET_Y + OFFSET_Y, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, this.rotation, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 108 */       sb.setColor(Color.WHITE);
/* 109 */       sb.draw(this.mImg, InputHelper.mX - 32.0F + OFFSET_X, InputHelper.mY - 32.0F + OFFSET_Y, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, this.rotation, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 126 */       break;
/*     */     }
/*     */     
/*     */     
/* 130 */     changeType(CursorType.NORMAL);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\GameCursor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */