/*     */ package com.megacrit.cardcrawl.core;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.EndTurnButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.BottomBgPanel;
/*     */ import com.megacrit.cardcrawl.ui.panels.BottomBgPanel.Mode;
/*     */ import com.megacrit.cardcrawl.ui.panels.DiscardPilePanel;
/*     */ import com.megacrit.cardcrawl.ui.panels.DrawPilePanel;
/*     */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*     */ import com.megacrit.cardcrawl.ui.panels.ExhaustPanel;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class OverlayMenu
/*     */ {
/*     */   private AbstractPlayer player;
/*  28 */   public static final float HAND_HIDE_Y = 300.0F * Settings.scale;
/*  29 */   public boolean combatPanelsShown = true;
/*  30 */   public float tipHoverDuration = 0.0F;
/*     */   private static final float HOVER_TIP_TIME = 0.01F;
/*  32 */   public boolean hoveredTip = false;
/*  33 */   public ArrayList<AbstractRelic> relicQueue = new ArrayList();
/*     */   
/*     */ 
/*  36 */   public BottomBgPanel bottomBgPanel = new BottomBgPanel();
/*  37 */   public EnergyPanel energyPanel = new EnergyPanel();
/*  38 */   private Color blackScreenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  39 */   private float blackScreenTarget = 0.0F;
/*     */   
/*     */ 
/*  42 */   public DrawPilePanel combatDeckPanel = new DrawPilePanel();
/*  43 */   public DiscardPilePanel discardPilePanel = new DiscardPilePanel();
/*  44 */   public ExhaustPanel exhaustPanel = new ExhaustPanel();
/*     */   
/*     */ 
/*  47 */   public EndTurnButton endTurnButton = new EndTurnButton();
/*  48 */   public ProceedButton proceedButton = new ProceedButton();
/*  49 */   public CancelButton cancelButton = new CancelButton();
/*     */   
/*     */   public OverlayMenu(AbstractPlayer player) {
/*  52 */     this.player = player;
/*     */   }
/*     */   
/*     */   public void update() {
/*  56 */     this.hoveredTip = false;
/*  57 */     this.bottomBgPanel.updatePositions();
/*  58 */     this.energyPanel.updatePositions();
/*  59 */     this.energyPanel.update();
/*  60 */     this.player.hand.update();
/*  61 */     this.combatDeckPanel.updatePositions();
/*  62 */     this.discardPilePanel.updatePositions();
/*  63 */     this.exhaustPanel.updatePositions();
/*  64 */     this.endTurnButton.update();
/*  65 */     this.proceedButton.update();
/*  66 */     this.cancelButton.update();
/*  67 */     updateBlackScreen();
/*     */     
/*  69 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  70 */       if (r != null) {
/*  71 */         r.update();
/*     */       }
/*     */     }
/*     */     
/*  75 */     for (AbstractBlight b : AbstractDungeon.player.blights) {
/*  76 */       if (b != null) {
/*  77 */         b.update();
/*     */       }
/*     */     }
/*     */     
/*  81 */     if (!this.relicQueue.isEmpty()) {
/*  82 */       for (AbstractRelic r : this.relicQueue) {
/*  83 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, r);
/*     */       }
/*  85 */       this.relicQueue.clear();
/*     */     }
/*     */     
/*  88 */     if (this.hoveredTip) {
/*  89 */       this.tipHoverDuration += Gdx.graphics.getDeltaTime();
/*  90 */       if (this.tipHoverDuration > 0.01F) {
/*  91 */         this.tipHoverDuration = 0.02F;
/*     */       }
/*     */     } else {
/*  94 */       this.tipHoverDuration -= Gdx.graphics.getDeltaTime();
/*  95 */       if (this.tipHoverDuration < 0.0F) {
/*  96 */         this.tipHoverDuration = 0.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void showCombatPanels() {
/* 102 */     this.combatPanelsShown = true;
/* 103 */     this.bottomBgPanel.changeMode(BottomBgPanel.Mode.NORMAL);
/* 104 */     this.combatDeckPanel.show();
/* 105 */     this.discardPilePanel.show();
/* 106 */     this.exhaustPanel.show();
/* 107 */     this.energyPanel.show();
/* 108 */     this.endTurnButton.show();
/* 109 */     if (AbstractDungeon.ftue == null) {
/* 110 */       this.proceedButton.hide();
/*     */     }
/*     */     
/* 113 */     this.player.hand.refreshHandLayout();
/*     */   }
/*     */   
/*     */   public void hideCombatPanels() {
/* 117 */     this.combatPanelsShown = false;
/* 118 */     this.bottomBgPanel.changeMode(BottomBgPanel.Mode.HIDDEN);
/* 119 */     this.combatDeckPanel.hide();
/* 120 */     this.discardPilePanel.hide();
/* 121 */     this.exhaustPanel.hide();
/* 122 */     this.endTurnButton.hide();
/* 123 */     this.energyPanel.hide();
/*     */     
/* 125 */     for (AbstractCard c : this.player.hand.group) {
/* 126 */       c.target_y = (-AbstractCard.IMG_HEIGHT);
/*     */     }
/*     */   }
/*     */   
/*     */   public void showBlackScreen(float target) {
/* 131 */     this.blackScreenTarget = target;
/*     */   }
/*     */   
/*     */   public void showBlackScreen() {
/* 135 */     if (this.blackScreenTarget < 0.85F) {
/* 136 */       this.blackScreenTarget = 0.85F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void hideBlackScreen() {
/* 141 */     this.blackScreenTarget = 0.0F;
/*     */   }
/*     */   
/*     */   private void updateBlackScreen() {
/* 145 */     if (this.blackScreenColor.a != this.blackScreenTarget) {
/* 146 */       if (this.blackScreenTarget > this.blackScreenColor.a) {
/* 147 */         this.blackScreenColor.a += Gdx.graphics.getDeltaTime() * 2.0F;
/* 148 */         if (this.blackScreenColor.a > this.blackScreenTarget) {
/* 149 */           this.blackScreenColor.a = this.blackScreenTarget;
/*     */         }
/*     */       } else {
/* 152 */         this.blackScreenColor.a -= Gdx.graphics.getDeltaTime() * 2.0F;
/* 153 */         if (this.blackScreenColor.a < this.blackScreenTarget) {
/* 154 */           this.blackScreenColor.a = this.blackScreenTarget;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void renderBgPanels(SpriteBatch sb) {}
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 167 */     this.endTurnButton.render(sb);
/* 168 */     this.proceedButton.render(sb);
/* 169 */     this.cancelButton.render(sb);
/*     */     
/*     */ 
/* 172 */     this.energyPanel.render(sb);
/*     */     
/*     */ 
/* 175 */     this.combatDeckPanel.render(sb);
/*     */     
/*     */ 
/* 178 */     this.discardPilePanel.render(sb);
/*     */     
/*     */ 
/* 181 */     this.exhaustPanel.render(sb);
/*     */     
/*     */ 
/* 184 */     this.player.renderHand(sb);
/* 185 */     this.player.hand.renderTip(sb);
/*     */   }
/*     */   
/*     */ 
/*     */   public void renderBlackScreen(SpriteBatch sb)
/*     */   {
/* 191 */     if (this.blackScreenColor.a != 0.0F) {
/* 192 */       sb.setColor(this.blackScreenColor);
/* 193 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\OverlayMenu.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */