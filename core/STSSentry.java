/*    */ package com.megacrit.cardcrawl.core;
/*    */ 
/*    */ import io.sentry.Sentry;
/*    */ import io.sentry.SentryClient;
/*    */ import io.sentry.context.Context;
/*    */ import io.sentry.dsn.InvalidDsnException;
/*    */ import io.sentry.event.UserBuilder;
/*    */ import java.io.File;
/*    */ import java.net.InetAddress;
/*    */ import java.net.UnknownHostException;
/*    */ import java.nio.charset.Charset;
/*    */ import java.nio.charset.StandardCharsets;
/*    */ import java.security.MessageDigest;
/*    */ import java.security.NoSuchAlgorithmException;
/*    */ import java.util.Locale;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class STSSentry
/*    */ {
/* 21 */   private static final Logger logger = LogManager.getLogger(STSSentry.class.getName());
/*    */   
/*    */ 
/*    */ 
/*    */   public static void setup()
/*    */   {
/* 27 */     String dsn = "https://cc194dc3d6214afbb87f21a3c96094ce:ee868ee67dc043a999868758abebd305@sentry.io/226216";
/* 28 */     String steamPath = "Steam" + File.separator + "steamapps" + File.separator + "common";
/* 29 */     String cwd = System.getProperty("user.dir");
/* 30 */     SentryClient client = null;
/* 31 */     long freeMem = SystemStats.getFreeMemory();
/*    */     try
/*    */     {
/* 34 */       boolean sendToSentry = (!Settings.isModded) && (cwd.contains(steamPath)) && (freeMem > 100000000L);
/* 35 */       if (sendToSentry) {
/* 36 */         client = Sentry.init(dsn);
/*    */       }
/*    */       else {
/* 39 */         client = Sentry.init("https://0e18:c64c98@megacrit.com/226216");
/*    */       }
/*    */     } catch (InvalidDsnException e) {
/* 42 */       logger.error(e);
/*    */     }
/*    */     
/* 45 */     if (client != null) {
/* 46 */       client.addTag("isBeta", String.valueOf(Settings.isBeta));
/* 47 */       client.addTag("isModded", String.valueOf(Settings.isModded));
/* 48 */       client.addTag("isDemo", String.valueOf(Settings.isDemo));
/* 49 */       client.addTag("isPublisherBuild", String.valueOf(Settings.isPublisherBuild));
/* 50 */       client.addTag("java_version", System.getProperty("java.version"));
/* 51 */       client.addTag("os_arch", System.getProperty("os.arch"));
/* 52 */       client.addTag("os_name", System.getProperty("os.name"));
/* 53 */       client.addTag("os_version", System.getProperty("os.version"));
/* 54 */       client.addTag("mem_max", String.valueOf(SystemStats.getMaxMemory()));
/* 55 */       client.addTag("mem_free", String.valueOf(freeMem));
/* 56 */       client.addTag("mem_alloc", String.valueOf(SystemStats.getAllocatedMemory()));
/* 57 */       client.addTag("mem_unalloc", String.valueOf(SystemStats.getUnallocatedMemory()));
/* 58 */       client.addTag("mem_total_free", String.valueOf(SystemStats.getTotalFreeMemory()));
/* 59 */       client.addTag("mem_used", String.valueOf(SystemStats.getUsedMemory()));
/* 60 */       client.addTag("disk_total", String.valueOf(SystemStats.getDiskSpaceTotal()));
/* 61 */       client.addTag("disk_free", String.valueOf(SystemStats.getDiskSpaceFree()));
/* 62 */       client.addTag("disk_usable", String.valueOf(SystemStats.getDiskSpaceUsable()));
/* 63 */       client.addTag("locale: ", String.valueOf(Locale.getDefault()));
/* 64 */       client.addTag("charset: ", String.valueOf(Charset.defaultCharset()));
/* 65 */       client.addTag("file_encoding: ", System.getProperty("file.encoding"));
/* 66 */       String machine_id = String.valueOf(getAnonymizedMachineName());
/* 67 */       Sentry.getContext().setUser(new UserBuilder().setUsername(machine_id).build());
/* 68 */       client.setServerName(machine_id);
/* 69 */       client.setRelease(CardCrawlGame.TRUE_VERSION_NUM);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void attachToEvent(String key, Object o)
/*    */   {
/* 80 */     Sentry.getStoredClient().addExtra(key, o);
/*    */   }
/*    */   
/*    */   public static String getAnonymizedMachineName()
/*    */   {
/* 85 */     String machineID = null;
/*    */     try {
/* 87 */       String hostname = InetAddress.getLocalHost().getHostName();
/* 88 */       MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
/* 89 */       messageDigest.update(hostname.getBytes(StandardCharsets.UTF_8));
/* 90 */       machineID = org.apache.commons.codec.binary.Hex.encodeHexString(messageDigest.digest()).substring(0, 8);
/*    */     }
/*    */     catch (UnknownHostException|NoSuchAlgorithmException e) {
/* 93 */       logger.info(e);
/*    */     }
/* 95 */     return machineID;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\STSSentry.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */