/*     */ package com.megacrit.cardcrawl.core;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.screens.DisplayOption;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Settings
/*     */ {
/*  16 */   private static final Logger logger = LogManager.getLogger(Settings.class.getName());
/*     */   
/*     */ 
/*  19 */   public static boolean isDev = false; public static boolean isBeta = false; public static boolean isTestingNeow = false; public static boolean isModded = false;
/*  20 */   public static boolean isDemo = false; public static boolean isShowBuild = false; public static boolean isPublisherBuild = false;
/*  21 */   public static boolean isDebug = false; public static boolean isInfo = false; public static boolean isControllerMode = false;
/*     */   
/*     */   public static GameLanguage language;
/*     */   
/*     */   public static final String PARAM_CHAR_LOC = "paramChars.txt";
/*  26 */   public static boolean lineBreakViaCharacter = false; public static boolean usesOrdinal = true;
/*     */   
/*     */   public static Prefs soundPref;
/*     */   public static Prefs dailyPref;
/*     */   public static Prefs gamePref;
/*     */   public static boolean isDailyRun;
/*     */   public static boolean hasDoneDailyToday;
/*  33 */   public static long dailyDate = 0L;
/*     */   public static DailyMods dailyMods;
/*     */   public static long totalPlayTime;
/*     */   public static boolean isEndless;
/*     */   public static boolean isTrial;
/*     */   public static Long specialSeed;
/*     */   public static String trialName;
/*     */   public static boolean IS_FULLSCREEN;
/*     */   public static boolean IS_W_FULLSCREEN;
/*     */   public static boolean IS_V_SYNC;
/*     */   public static int MAX_FPS;
/*     */   public static int M_W;
/*     */   public static int M_H;
/*     */   public static int SAVED_WIDTH;
/*     */   public static int SAVED_HEIGHT;
/*  48 */   public static int WIDTH; public static int HEIGHT; public static boolean isSixteenByTen = false;
/*  49 */   public static int HORIZ_LETTERBOX_AMT = 0; public static int VERT_LETTERBOX_AMT = 0;
/*  50 */   public static ArrayList<DisplayOption> displayOptions = null;
/*  51 */   public static int displayIndex = 0;
/*     */   
/*     */   public static float scale;
/*     */   
/*     */   public static Long seed;
/*  56 */   public static boolean seedSet = false;
/*     */   
/*     */   public static long seedSourceTimestamp;
/*     */   
/*  60 */   public static boolean isBackgrounded = false;
/*  61 */   public static float bgVolume = 0.0F;
/*     */   public static final String MASTER_VOLUME_PREF = "Master Volume";
/*     */   public static final String MUSIC_VOLUME_PREF = "Music Volume";
/*     */   public static final String SOUND_VOLUME_PREF = "Sound Volume";
/*     */   public static final String AMBIENCE_ON_PREF = "Ambience On";
/*     */   public static final String MUTE_IF_BG_PREF = "Mute in Bg";
/*     */   public static final float DEFAULT_MASTER_VOLUME = 0.5F;
/*     */   public static final float DEFAULT_MUSIC_VOLUME = 0.5F;
/*     */   public static final float DEFAULT_SOUND_VOLUME = 0.5F;
/*     */   public static float MASTER_VOLUME;
/*     */   public static float MUSIC_VOLUME;
/*     */   public static float SOUND_VOLUME;
/*     */   public static boolean AMBIANCE_ON;
/*     */   public static final String SCREEN_SHAKE_PREF = "Screen Shake";
/*     */   public static final String SUM_DMG_PREF = "Summed Damage";
/*     */   public static final String BLOCKED_DMG_PREF = "Blocked Damage";
/*     */   public static final String HAND_CONF_PREF = "Hand Confirmation";
/*     */   public static final String FAST_MODE_PREF = "Fast Mode";
/*     */   public static final String UPLOAD_PREF = "Upload Data";
/*     */   public static final String PLAYTESTER_ART = "Playtester Art";
/*     */   public static final String SHOW_CARD_HOTKEYS_PREF = "Show Card keys";
/*     */   public static final String CONTROLLER_ENABLED_PREF = "Controller Enabled";
/*     */   public static final String LAST_DAILY = "LAST_DAILY";
/*     */   public static boolean SHOW_DMG_SUM;
/*     */   public static boolean SHOW_DMG_BLOCK;
/*     */   public static boolean FAST_HAND_CONF;
/*     */   public static boolean FAST_MODE;
/*  88 */   public static boolean CONTROLLER_ENABLED; public static boolean UPLOAD_DATA; public static boolean SCREEN_SHAKE; public static boolean PLAYTESTER_ART_MODE; public static boolean SHOW_CARD_HOTKEYS; public static final Color CREAM_COLOR = new Color(-597249);
/*  89 */   public static final Color LIGHT_YELLOW_COLOR = new Color(-1202177);
/*  90 */   public static final Color RED_TEXT_COLOR = new Color(-10132481);
/*  91 */   public static final Color GREEN_TEXT_COLOR = new Color(2147418367);
/*  92 */   public static final Color BLUE_TEXT_COLOR = new Color(-2016482305);
/*  93 */   public static final Color GOLD_COLOR = new Color(-272084481);
/*  94 */   public static final Color PURPLE_COLOR = new Color(-293409025);
/*  95 */   public static final Color TOP_PANEL_SHADOW_COLOR = new Color(64);
/*     */   
/*     */   public static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*     */   
/*     */   public static final float WAIT_BEFORE_BATTLE_TIME = 1.0F;
/* 100 */   public static float ACTION_DUR_XFAST = 0.1F;
/* 101 */   public static float ACTION_DUR_FASTER = 0.2F;
/* 102 */   public static float ACTION_DUR_FAST = 0.25F;
/* 103 */   public static float ACTION_DUR_MED = 0.5F;
/* 104 */   public static float ACTION_DUR_LONG = 1.0F;
/* 105 */   public static float ACTION_DUR_XLONG = 1.5F;
/*     */   public static float CARD_DROP_END_Y;
/*     */   public static float SCROLL_SPEED;
/*     */   public static float MAP_SCROLL_SPEED;
/*     */   public static final float SCROLL_LERP_SPEED = 12.0F;
/*     */   public static final float SCROLL_SNAP_BACK_SPEED = 10.0F;
/*     */   public static float DEFAULT_SCROLL_LIMIT;
/*     */   public static float MAP_DST_Y;
/*     */   public static final float CLICK_SPEED_THRESHOLD = 0.4F;
/*     */   public static float CLICK_DIST_THRESHOLD;
/*     */   public static float POTION_W;
/*     */   public static float POTION_Y;
/* 117 */   public static final Color BLACK_SCREEN_OVERLAY_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.7F);
/* 118 */   public static final Color GLOW_COLOR = Color.SCARLET.cpy();
/* 119 */   public static final Color DISCARD_COLOR = Color.valueOf("8a769bff");
/* 120 */   public static final Color DISCARD_GLOW_COLOR = Color.valueOf("553a66ff");
/* 121 */   public static final Color SHADOW_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*     */   
/*     */   public static final float CARD_SOUL_SCALE = 0.12F;
/*     */   
/*     */   public static final float CARD_LERP_SPEED = 6.0F;
/*     */   
/*     */   public static float CARD_SNAP_THRESHOLD;
/*     */   
/*     */   public static float UI_SNAP_THRESHOLD;
/*     */   
/*     */   public static final float CARD_SCALE_LERP_SPEED = 7.5F;
/*     */   
/*     */   public static final float CARD_SCALE_SNAP_THRESHOLD = 0.003F;
/*     */   
/*     */   public static final float UI_LERP_SPEED = 9.0F;
/*     */   
/*     */   public static final float ORB_LERP_SPEED = 6.0F;
/*     */   
/*     */   public static final float MOUSE_LERP_SPEED = 20.0F;
/*     */   
/*     */   public static float POP_AMOUNT;
/*     */   
/*     */   public static final float POP_LERP_SPEED = 8.0F;
/*     */   
/*     */   public static final float FADE_LERP_SPEED = 12.0F;
/*     */   
/*     */   public static final float SLOW_COLOR_LERP_SPEED = 3.0F;
/*     */   
/*     */   public static final float FADE_SNAP_THRESHOLD = 0.01F;
/*     */   
/*     */   public static final float ROTATE_LERP_SPEED = 12.0F;
/*     */   public static final float SCALE_LERP_SPEED = 3.0F;
/*     */   public static final float SCALE_SNAP_THRESHOLD = 0.003F;
/*     */   public static final float HEALTH_BAR_WAIT_TIME = 1.5F;
/*     */   public static float HOVER_BUTTON_RISE_AMOUNT;
/*     */   public static final float HOVER_BUTTON_SCALE_AMOUNT = 1.2F;
/*     */   public static final float CARD_VIEW_SCALE = 0.75F;
/*     */   public static float CARD_VIEW_PAD_X;
/*     */   public static float CARD_VIEW_PAD_Y;
/*     */   public static float OPTION_Y;
/*     */   public static float EVENT_Y;
/*     */   public static final int MAX_ASCENSION_LEVEL = 20;
/*     */   public static final float POST_COMBAT_WAIT_TIME = 0.25F;
/*     */   public static final int MAX_HAND_SIZE = 10;
/*     */   public static final int NUM_POTIONS = 3;
/*     */   public static final int NORMAL_POTION_DROP_RATE = 40;
/*     */   public static final int ELITE_POTION_DROP_RATE = 40;
/*     */   public static final int BOSS_GOLD_AMT = 100;
/*     */   public static final int BOSS_GOLD_JITTER = 5;
/*     */   public static final int NORMAL_RARE_DROP_RATE = 3;
/*     */   public static final int NORMAL_UNCOMMON_DROP_RATE = 40;
/*     */   public static final int ELITE_RARE_DROP_RATE = 10;
/*     */   public static final int ELITE_UNCOMMON_DROP_RATE = 50;
/*     */   public static final int UNLOCK_PER_CHAR_COUNT = 5;
/*     */   public static final int CHEATER_SCORE_CUTOFF = 10000;
/* 176 */   public static boolean hideTopBar = false;
/* 177 */   public static boolean hidePopupDetails = false;
/* 178 */   public static boolean hideRelics = false;
/* 179 */   public static boolean hideLowerElements = false;
/* 180 */   public static boolean hideCards = false;
/* 181 */   public static boolean hideEndTurn = false;
/* 182 */   public static boolean hideCombatElements = false;
/*     */   
/*     */   public static final String SENDTODEVS = "sendToDevs";
/*     */   
/*     */   public static enum GameLanguage
/*     */   {
/* 188 */     ENG,  EPO,  PTB,  ZHS,  ZHT,  FRA,  DEU,  GRE,  IND,  ITA,  JPN,  KOR,  NOR,  POL,  RUS,  SPA,  SRP,  SRB,  THA,  TUR,  UKR,  WWW;
/*     */     
/*     */     private GameLanguage() {} }
/*     */   
/* 192 */   public static void initialize(boolean reloaded) { if (!reloaded) {
/* 193 */       DisplayConfig displayConf = DisplayConfig.readConfig();
/* 194 */       M_W = Gdx.graphics.getWidth();
/* 195 */       M_H = Gdx.graphics.getHeight();
/* 196 */       WIDTH = displayConf.getWidth();
/* 197 */       HEIGHT = displayConf.getHeight();
/*     */       
/* 199 */       float ratio = WIDTH / HEIGHT;
/* 200 */       if (ratio < 1.59F) {
/* 201 */         HEIGHT = (int)(WIDTH * 0.625F);
/* 202 */         HORIZ_LETTERBOX_AMT = (M_H - HEIGHT) / 2;
/* 203 */       } else if (ratio > 1.78F) {
/* 204 */         WIDTH = (int)(HEIGHT * 1.7777778F);
/* 205 */         VERT_LETTERBOX_AMT = (M_W - WIDTH) / 2;
/*     */       }
/*     */       
/* 208 */       MAX_FPS = displayConf.getMaxFPS();
/* 209 */       SAVED_WIDTH = WIDTH;
/* 210 */       SAVED_HEIGHT = HEIGHT;
/* 211 */       IS_FULLSCREEN = displayConf.getIsFullscreen();
/* 212 */       IS_W_FULLSCREEN = displayConf.getWFS();
/* 213 */       IS_V_SYNC = displayConf.getIsVsync();
/*     */       
/* 215 */       if (WIDTH / HEIGHT < 1.7F) {
/* 216 */         isSixteenByTen = true;
/*     */       }
/*     */       
/* 219 */       scale = WIDTH / 1920.0F;
/* 220 */       SCROLL_SPEED = 75.0F * scale;
/* 221 */       MAP_SCROLL_SPEED = 75.0F * scale;
/* 222 */       DEFAULT_SCROLL_LIMIT = 50.0F * scale;
/* 223 */       MAP_DST_Y = 150.0F * scale;
/* 224 */       CLICK_DIST_THRESHOLD = 30.0F * scale;
/* 225 */       CARD_DROP_END_Y = HEIGHT * 0.81F;
/* 226 */       POTION_W = 56.0F * scale;
/* 227 */       POTION_Y = HEIGHT - 30.0F * scale;
/* 228 */       OPTION_Y = HEIGHT / 2.0F - 32.0F * scale;
/* 229 */       EVENT_Y = HEIGHT / 2.0F - 128.0F * scale;
/* 230 */       CARD_VIEW_PAD_X = 40.0F * scale;
/* 231 */       CARD_VIEW_PAD_Y = 40.0F * scale;
/* 232 */       HOVER_BUTTON_RISE_AMOUNT = 8.0F * scale;
/* 233 */       POP_AMOUNT = 1.75F * scale;
/* 234 */       CARD_SNAP_THRESHOLD = 1.0F * scale;
/* 235 */       UI_SNAP_THRESHOLD = 1.0F * scale;
/*     */     }
/*     */     
/* 238 */     dailyMods = new DailyMods();
/* 239 */     soundPref = SaveHelper.getPrefs("STSSound");
/* 240 */     AMBIANCE_ON = soundPref.getBoolean("Ambience On", true);
/* 241 */     CardCrawlGame.MUTE_IF_BG = soundPref.getBoolean("Mute in Bg", true);
/* 242 */     soundPref.putBoolean("Ambience On", AMBIANCE_ON);
/* 243 */     soundPref.putBoolean("Mute in Bg", CardCrawlGame.MUTE_IF_BG);
/* 244 */     soundPref.flush();
/*     */     
/* 246 */     gamePref = SaveHelper.getPrefs("STSGameplaySettings");
/* 247 */     SHOW_DMG_SUM = gamePref.getBoolean("Summed Damage", false);
/* 248 */     SHOW_DMG_BLOCK = gamePref.getBoolean("Blocked Damage", false);
/* 249 */     FAST_HAND_CONF = gamePref.getBoolean("Hand Confirmation", false);
/* 250 */     FAST_MODE = gamePref.getBoolean("Fast Mode", false);
/* 251 */     SHOW_CARD_HOTKEYS = gamePref.getBoolean("Show Card keys", false);
/* 252 */     UPLOAD_DATA = gamePref.getBoolean("Upload Data", true);
/* 253 */     SCREEN_SHAKE = gamePref.getBoolean("Screen Shake", true);
/* 254 */     if (!reloaded) {
/* 255 */       setLanguage(gamePref.getString("LANGUAGE", GameLanguage.ENG.name()), true);
/*     */     }
/* 257 */     PLAYTESTER_ART_MODE = gamePref.getBoolean("Playtester Art", false);
/* 258 */     CONTROLLER_ENABLED = gamePref.getBoolean("Controller Enabled", true);
/*     */     
/* 260 */     dailyPref = SaveHelper.getPrefs("STSDaily");
/*     */     
/* 262 */     gamePref.putBoolean("Summed Damage", SHOW_DMG_SUM);
/* 263 */     gamePref.putBoolean("Blocked Damage", SHOW_DMG_BLOCK);
/* 264 */     gamePref.putBoolean("Hand Confirmation", FAST_HAND_CONF);
/* 265 */     gamePref.putBoolean("Upload Data", UPLOAD_DATA);
/* 266 */     gamePref.putBoolean("Fast Mode", FAST_MODE);
/* 267 */     gamePref.putBoolean("Show Card keys", SHOW_CARD_HOTKEYS);
/* 268 */     gamePref.putBoolean("Screen Shake", SCREEN_SHAKE);
/* 269 */     gamePref.putBoolean("Playtester Art", PLAYTESTER_ART_MODE);
/* 270 */     gamePref.flush();
/*     */   }
/*     */   
/*     */   public static void setLanguage(GameLanguage key, boolean initial) {
/* 274 */     language = key;
/* 275 */     if (initial) {
/* 276 */       switch (language) {
/*     */       case ZHS: 
/*     */       case ZHT: 
/*     */       case JPN: 
/* 280 */         lineBreakViaCharacter = true;
/* 281 */         usesOrdinal = false;
/* 282 */         break;
/*     */       case ENG: 
/* 284 */         lineBreakViaCharacter = false;
/* 285 */         usesOrdinal = true;
/* 286 */         break;
/*     */       case EPO: 
/*     */       case PTB: 
/*     */       case FRA: 
/*     */       case DEU: 
/*     */       case GRE: 
/*     */       case IND: 
/*     */       case ITA: 
/*     */       case KOR: 
/*     */       case NOR: 
/*     */       case POL: 
/*     */       case RUS: 
/*     */       case SPA: 
/*     */       case SRP: 
/*     */       case SRB: 
/*     */       case THA: 
/*     */       case UKR: 
/*     */       case TUR: 
/* 304 */         lineBreakViaCharacter = false;
/* 305 */         usesOrdinal = false;
/* 306 */         break;
/*     */       default: 
/* 308 */         logger.info("[ERROR] Unspecified language: " + key.toString());
/* 309 */         lineBreakViaCharacter = false;
/* 310 */         usesOrdinal = true;
/*     */       }
/*     */       
/*     */     }
/* 314 */     gamePref.putString("LANGUAGE", key.name());
/* 315 */     gamePref.flush();
/*     */   }
/*     */   
/*     */   public static void setLanguage(String langStr, boolean initial) {
/*     */     try {
/* 320 */       GameLanguage langKey = GameLanguage.valueOf(langStr);
/* 321 */       setLanguage(langKey, initial);
/*     */     } catch (IllegalArgumentException ex) {
/* 323 */       setLanguageLegacy(langStr, initial);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void setLanguageLegacy(String key, boolean initial) {
/* 328 */     switch (key) {
/*     */     case "English": 
/* 330 */       language = GameLanguage.ENG;
/* 331 */       if (initial) {
/* 332 */         lineBreakViaCharacter = false;
/* 333 */         usesOrdinal = true;
/*     */       }
/*     */       break;
/*     */     case "Brazilian Portuguese": 
/* 337 */       language = GameLanguage.PTB;
/* 338 */       if (initial) {
/* 339 */         lineBreakViaCharacter = false;
/* 340 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Chinese (Simplified)": 
/* 344 */       language = GameLanguage.ZHS;
/* 345 */       if (initial) {
/* 346 */         lineBreakViaCharacter = true;
/* 347 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Chinese (Traditional)": 
/* 351 */       language = GameLanguage.ZHT;
/* 352 */       if (initial) {
/* 353 */         lineBreakViaCharacter = true;
/* 354 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "French": 
/* 358 */       language = GameLanguage.FRA;
/* 359 */       if (initial) {
/* 360 */         lineBreakViaCharacter = false;
/* 361 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "German": 
/* 365 */       language = GameLanguage.DEU;
/* 366 */       if (initial) {
/* 367 */         lineBreakViaCharacter = false;
/* 368 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Greek": 
/* 372 */       language = GameLanguage.GRE;
/* 373 */       if (initial) {
/* 374 */         lineBreakViaCharacter = false;
/* 375 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Italian": 
/* 379 */       language = GameLanguage.ITA;
/* 380 */       if (initial) {
/* 381 */         lineBreakViaCharacter = false;
/* 382 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Indonesian": 
/* 386 */       language = GameLanguage.ITA;
/* 387 */       if (initial) {
/* 388 */         lineBreakViaCharacter = false;
/* 389 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Japanese": 
/* 393 */       language = GameLanguage.JPN;
/* 394 */       if (initial) {
/* 395 */         lineBreakViaCharacter = true;
/* 396 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Korean": 
/* 400 */       language = GameLanguage.KOR;
/* 401 */       if (initial) {
/* 402 */         lineBreakViaCharacter = false;
/* 403 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Norwegian": 
/* 407 */       language = GameLanguage.NOR;
/* 408 */       if (initial) {
/* 409 */         lineBreakViaCharacter = false;
/* 410 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Polish": 
/* 414 */       language = GameLanguage.POL;
/* 415 */       if (initial) {
/* 416 */         lineBreakViaCharacter = false;
/* 417 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Russian": 
/* 421 */       language = GameLanguage.RUS;
/* 422 */       if (initial) {
/* 423 */         lineBreakViaCharacter = false;
/* 424 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Spanish": 
/* 428 */       language = GameLanguage.SPA;
/* 429 */       if (initial) {
/* 430 */         lineBreakViaCharacter = false;
/* 431 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Serbian-Cyrillic": 
/* 435 */       language = GameLanguage.SRP;
/* 436 */       if (initial) {
/* 437 */         lineBreakViaCharacter = false;
/* 438 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Serbian-Latin": 
/* 442 */       language = GameLanguage.SRB;
/* 443 */       if (initial) {
/* 444 */         lineBreakViaCharacter = false;
/* 445 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Thai": 
/* 449 */       language = GameLanguage.THA;
/* 450 */       if (initial) {
/* 451 */         lineBreakViaCharacter = false;
/* 452 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Turkish": 
/* 456 */       language = GameLanguage.TUR;
/* 457 */       if (initial) {
/* 458 */         lineBreakViaCharacter = false;
/* 459 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     case "Ukrainian": 
/* 463 */       language = GameLanguage.UKR;
/* 464 */       if (initial) {
/* 465 */         lineBreakViaCharacter = false;
/* 466 */         usesOrdinal = false;
/*     */       }
/*     */       break;
/*     */     default: 
/* 470 */       language = GameLanguage.ENG;
/* 471 */       if (initial) {
/* 472 */         lineBreakViaCharacter = false;
/* 473 */         usesOrdinal = true;
/*     */       }
/*     */       break;
/*     */     }
/*     */     
/* 478 */     gamePref.putString("LANGUAGE", key);
/* 479 */     gamePref.flush();
/*     */   }
/*     */   
/*     */   public static boolean isStandardRun() {
/* 483 */     return (!isDailyRun) && (!isTrial);
/*     */   }
/*     */   
/*     */   public static boolean dailyModsEnabled() {
/* 487 */     return dailyMods.areEnabled();
/*     */   }
/*     */   
/*     */   public static boolean treatEverythingAsUnlocked() {
/* 491 */     return (isDailyRun) || (isTrial);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\Settings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */