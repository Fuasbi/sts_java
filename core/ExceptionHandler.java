/*    */ package com.megacrit.cardcrawl.core;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ 
/*    */ public class ExceptionHandler
/*    */ {
/*    */   public static void handleException(Exception e, Logger logger)
/*    */   {
/* 11 */     System.out.println("Exception: " + e.toString());
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 16 */     logger.error("Exception caught", e);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\ExceptionHandler.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */