/*      */ package com.megacrit.cardcrawl.core;
/*      */ 
/*      */ import com.badlogic.gdx.Files;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*      */ import com.badlogic.gdx.math.Interpolation;
/*      */ import com.badlogic.gdx.math.Interpolation.ElasticOut;
/*      */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*      */ import com.badlogic.gdx.math.Interpolation.Pow;
/*      */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*      */ import com.esotericsoftware.spine.AnimationState;
/*      */ import com.esotericsoftware.spine.AnimationStateData;
/*      */ import com.esotericsoftware.spine.Skeleton;
/*      */ import com.esotericsoftware.spine.SkeletonJson;
/*      */ import com.esotericsoftware.spine.SkeletonMeshRenderer;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*      */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*      */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*      */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*      */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.TextAboveCreatureEffect;
/*      */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.BlockedNumberEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.HbBlockBrokenEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.HealEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Iterator;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public abstract class AbstractCreature
/*      */ {
/*   51 */   private static final Logger logger = LogManager.getLogger(AbstractCreature.class.getName());
/*      */   public String name;
/*   53 */   public String id; public ArrayList<AbstractPower> powers = new ArrayList();
/*      */   public boolean isPlayer;
/*      */   public boolean isBloodied;
/*      */   public float drawX;
/*      */   public float drawY;
/*      */   public float dialogX;
/*      */   public float dialogY;
/*      */   public Hitbox hb;
/*   61 */   public int gold; public int displayGold; public static final int DAMAGE_FLASH_FRAMES = 4; public boolean damageFlash = false;
/*      */   public int damageFlashFrames;
/*   63 */   public boolean isDying = false; public boolean isDead = false; public boolean halfDead = false;
/*      */   
/*      */ 
/*   66 */   protected boolean flipHorizontal = false; protected boolean flipVertical = false;
/*   67 */   public float escapeTimer = 0.0F;
/*   68 */   public boolean isEscaping = false;
/*      */   
/*      */ 
/*   71 */   protected static final float TIP_X_THRESHOLD = 1544.0F * Settings.scale;
/*   72 */   protected static final float MULTI_TIP_Y_OFFSET = 80.0F * Settings.scale;
/*   73 */   protected static final float TIP_OFFSET_R_X = 20.0F * Settings.scale;
/*   74 */   protected static final float TIP_OFFSET_L_X = -380.0F * Settings.scale;
/*   75 */   protected static final float TIP_OFFSET_Y = 80.0F * Settings.scale;
/*      */   
/*      */ 
/*   78 */   private static UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractCreature");
/*   79 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   
/*      */   public Hitbox healthHb;
/*   82 */   private float healthHideTimer = 0.0F;
/*      */   public float hb_x;
/*      */   public float hb_y;
/*      */   public float hb_w;
/*      */   public float hb_h;
/*   87 */   public int currentHealth; public int maxHealth; public int currentBlock; private float healthBarWidth; private float targetHealthBarWidth; private float hbShowTimer = 0.0F;
/*   88 */   private float healthBarAnimTimer = 0.0F;
/*   89 */   private float blockAnimTimer = 0.0F;
/*   90 */   private float blockOffset = 0.0F;
/*   91 */   private float blockScale = 1.0F;
/*   92 */   public float hbAlpha = 0.0F;
/*   93 */   private float hbYOffset = HB_Y_OFFSET_DIST * 5.0F;
/*   94 */   private Color hbBgColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*   95 */   private Color hbShadowColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*   96 */   private Color blockColor = new Color(0.6F, 0.93F, 0.98F, 0.0F);
/*   97 */   private Color blockOutlineColor = new Color(0.6F, 0.93F, 0.98F, 0.0F);
/*   98 */   private Color blockTextColor = new Color(0.9F, 0.9F, 0.9F, 0.0F);
/*   99 */   private Color redHbBarColor = new Color(0.8F, 0.05F, 0.05F, 0.0F);
/*  100 */   private Color greenHbBarColor = Color.valueOf("78c13c00");
/*  101 */   private Color blueHbBarColor = Color.valueOf("31568c00");
/*  102 */   private Color orangeHbBarColor = new Color(1.0F, 0.5F, 0.0F, 0.0F);
/*  103 */   private Color hbTextColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*      */   private static final float BLOCK_ANIM_TIME = 0.7F;
/*  105 */   private static final float BLOCK_OFFSET_DIST = 12.0F * Settings.scale;
/*      */   private static final float SHOW_HB_TIME = 0.7F;
/*  107 */   private static final float HB_Y_OFFSET_DIST = 12.0F * Settings.scale;
/*  108 */   protected static final float BLOCK_ICON_X = -14.0F * Settings.scale;
/*  109 */   protected static final float BLOCK_ICON_Y = -14.0F * Settings.scale;
/*      */   private static final int BLOCK_W = 64;
/*      */   private static final float HEALTH_BAR_PAUSE_DURATION = 1.2F;
/*  112 */   private static final float HEALTH_BAR_HEIGHT = 20.0F * Settings.scale;
/*  113 */   private static final float HEALTH_BAR_OFFSET_Y = -28.0F * Settings.scale;
/*  114 */   private static final float HEALTH_TEXT_OFFSET_Y = 6.0F * Settings.scale;
/*  115 */   private static final float POWER_ICON_PADDING_X = 48.0F * Settings.scale;
/*  116 */   private static final float HEALTH_BG_OFFSET_X = 31.0F * Settings.scale;
/*      */   
/*      */ 
/*  119 */   public TintEffect tint = new TintEffect();
/*      */   public static SkeletonMeshRenderer sr;
/*  121 */   private boolean shakeToggle = true;
/*  122 */   private static final float SHAKE_THRESHOLD = Settings.scale * 8.0F; private static final float SHAKE_SPEED = 150.0F * Settings.scale;
/*      */   public float animX;
/*      */   public float animY;
/*      */   protected float vX;
/*  126 */   protected float vY; protected CreatureAnimation animation; protected float animationTimer = 0.0F;
/*      */   protected static final float SLOW_ATTACK_ANIM_DUR = 1.0F;
/*      */   protected static final float STAGGER_ANIM_DUR = 0.3F;
/*  129 */   protected static final float FAST_ATTACK_ANIM_DUR = 0.4F; protected static final float HOP_ANIM_DURATION = 0.7F; private static final float STAGGER_MOVE_SPEED = 20.0F * Settings.scale;
/*  130 */   protected TextureAtlas atlas = null;
/*      */   
/*      */   protected Skeleton skeleton;
/*      */   
/*      */   public AnimationState state;
/*      */   protected AnimationStateData stateData;
/*      */   private static final int RETICLE_W = 36;
/*  137 */   public float reticleAlpha = 0.0F;
/*  138 */   public boolean reticleRendered = false;
/*  139 */   private float reticleOffset = 0.0F;
/*  140 */   private float reticleAnimTimer = 0.0F;
/*  141 */   private static final float RETICLE_OFFSET_DIST = 15.0F * Settings.scale;
/*      */   public abstract void damage(DamageInfo paramDamageInfo);
/*      */   
/*  144 */   public static enum CreatureAnimation { FAST_SHAKE,  SHAKE,  ATTACK_FAST,  ATTACK_SLOW,  STAGGER,  HOP,  JUMP;
/*      */     
/*      */     private CreatureAnimation() {}
/*      */   }
/*      */   
/*      */   private void brokeBlock()
/*      */   {
/*  151 */     if ((this instanceof AbstractMonster)) {
/*  152 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  153 */         r.onBlockBroken(this);
/*      */       }
/*      */     }
/*      */     
/*  157 */     AbstractDungeon.effectList.add(new HbBlockBrokenEffect(this.hb.cX - this.hb.width / 2.0F + BLOCK_ICON_X, this.hb.cY - this.hb.height / 2.0F + BLOCK_ICON_Y));
/*      */     
/*  159 */     CardCrawlGame.sound.play("BLOCK_BREAK");
/*      */   }
/*      */   
/*      */   protected int decrementBlock(DamageInfo info, int damageAmount) {
/*  163 */     if ((info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS) && (this.currentBlock > 0)) {
/*  164 */       CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/*      */       
/*      */ 
/*  167 */       if (damageAmount > this.currentBlock) {
/*  168 */         damageAmount -= this.currentBlock;
/*  169 */         if (Settings.SHOW_DMG_BLOCK) {
/*  170 */           AbstractDungeon.effectList.add(new BlockedNumberEffect(this.hb.cX, this.hb.cY + this.hb.height / 2.0F, 
/*  171 */             Integer.toString(this.currentBlock)));
/*      */         }
/*  173 */         loseBlock();
/*  174 */         brokeBlock();
/*      */       }
/*  176 */       else if (damageAmount == this.currentBlock) {
/*  177 */         damageAmount = 0;
/*  178 */         loseBlock();
/*  179 */         brokeBlock();
/*  180 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.BlockedWordEffect(this, this.hb.cX, this.hb.cY, TEXT[1]));
/*      */       }
/*      */       else
/*      */       {
/*  184 */         CardCrawlGame.sound.play("BLOCK_ATTACK");
/*  185 */         loseBlock(damageAmount);
/*  186 */         for (int i = 0; i < 18; i++) {
/*  187 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.BlockImpactLineEffect(this.hb.cX, this.hb.cY));
/*      */         }
/*  189 */         if (Settings.SHOW_DMG_BLOCK) {
/*  190 */           AbstractDungeon.effectList.add(new BlockedNumberEffect(this.hb.cX, this.hb.cY + this.hb.height / 2.0F, 
/*  191 */             Integer.toString(damageAmount)));
/*      */         }
/*  193 */         damageAmount = 0;
/*      */       }
/*      */     }
/*      */     
/*  197 */     return damageAmount;
/*      */   }
/*      */   
/*      */   public void increaseMaxHp(int amount, boolean showEffect) {
/*  201 */     if ((!Settings.isEndless) || (!AbstractDungeon.player.hasBlight("FullBelly")))
/*      */     {
/*      */ 
/*  204 */       if (amount < 0) {
/*  205 */         logger.info("Why are we decreasing health with increaseMaxHealth()?");
/*      */       }
/*      */       
/*  208 */       this.maxHealth += amount;
/*  209 */       AbstractDungeon.effectsQueue.add(new TextAboveCreatureEffect(this.hb.cX - this.animX, this.hb.cY, TEXT[2] + 
/*      */       
/*      */ 
/*      */ 
/*  213 */         Integer.toString(amount), Settings.GREEN_TEXT_COLOR));
/*      */       
/*  215 */       heal(amount, true);
/*  216 */       healthBarUpdatedEvent();
/*      */     }
/*      */   }
/*      */   
/*      */   public void decreaseMaxHealth(int amount) {
/*  221 */     if (amount < 0) {
/*  222 */       logger.info("Why are we increasing health with decreaseMaxHealth()?");
/*      */     }
/*      */     
/*  225 */     this.maxHealth -= amount;
/*  226 */     if (this.maxHealth <= 1) {
/*  227 */       this.maxHealth = 1;
/*      */     }
/*  229 */     if (this.currentHealth > this.maxHealth) {
/*  230 */       this.currentHealth = this.maxHealth;
/*      */     }
/*  232 */     healthBarUpdatedEvent();
/*      */   }
/*      */   
/*      */   protected void refreshHitboxLocation() {
/*  236 */     this.hb.move(this.drawX + this.hb_x + this.animX, this.drawY + this.hb_y + this.hb_h / 2.0F);
/*  237 */     this.healthHb.move(this.hb.cX, this.hb.cY - this.hb_h / 2.0F - this.healthHb.height / 2.0F);
/*      */   }
/*      */   
/*      */   public void updateAnimations() {
/*  241 */     if (this.animationTimer != 0.0F) {
/*  242 */       switch (this.animation) {
/*      */       case ATTACK_FAST: 
/*  244 */         updateFastAttackAnimation();
/*  245 */         break;
/*      */       case ATTACK_SLOW: 
/*  247 */         updateSlowAttackAnimation();
/*  248 */         break;
/*      */       case FAST_SHAKE: 
/*  250 */         updateFastShakeAnimation();
/*  251 */         break;
/*      */       case HOP: 
/*  253 */         updateHopAnimation();
/*  254 */         break;
/*      */       case JUMP: 
/*  256 */         updateJumpAnimation();
/*  257 */         break;
/*      */       case SHAKE: 
/*  259 */         updateShakeAnimation();
/*  260 */         break;
/*      */       case STAGGER: 
/*  262 */         updateStaggerAnimation();
/*  263 */         break;
/*      */       }
/*      */       
/*      */     }
/*      */     
/*  268 */     refreshHitboxLocation();
/*  269 */     if (!this.isPlayer) {
/*  270 */       ((AbstractMonster)this).refreshIntentHbLocation();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void updateFastAttackAnimation()
/*      */   {
/*  278 */     this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  279 */     float targetPos = 90.0F * Settings.scale;
/*  280 */     if (!this.isPlayer) {
/*  281 */       targetPos = -targetPos;
/*      */     }
/*      */     
/*      */ 
/*  285 */     if (this.animationTimer > 0.5F) {
/*  286 */       this.animX = Interpolation.exp5In.apply(0.0F, targetPos, (1.0F - this.animationTimer / 1.0F) * 2.0F);
/*      */ 
/*      */     }
/*  289 */     else if (this.animationTimer < 0.0F) {
/*  290 */       this.animationTimer = 0.0F;
/*  291 */       this.animX = 0.0F;
/*      */     }
/*      */     else
/*      */     {
/*  295 */       this.animX = Interpolation.fade.apply(0.0F, targetPos, this.animationTimer / 1.0F * 2.0F);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void updateSlowAttackAnimation()
/*      */   {
/*  303 */     this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  304 */     float targetPos = 90.0F * Settings.scale;
/*  305 */     if (!this.isPlayer) {
/*  306 */       targetPos = -targetPos;
/*      */     }
/*      */     
/*      */ 
/*  310 */     if (this.animationTimer > 0.5F) {
/*  311 */       this.animX = Interpolation.exp10In.apply(0.0F, targetPos, (1.0F - this.animationTimer / 1.0F) * 2.0F);
/*      */ 
/*      */     }
/*  314 */     else if (this.animationTimer < 0.0F) {
/*  315 */       this.animationTimer = 0.0F;
/*  316 */       this.animX = 0.0F;
/*      */     }
/*      */     else
/*      */     {
/*  320 */       this.animX = Interpolation.fade.apply(0.0F, targetPos, this.animationTimer / 1.0F * 2.0F);
/*      */     }
/*      */   }
/*      */   
/*      */   protected void updateFastShakeAnimation() {
/*  325 */     this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  326 */     if (this.animationTimer < 0.0F) {
/*  327 */       this.animationTimer = 0.0F;
/*  328 */       this.animX = 0.0F;
/*      */     }
/*  330 */     else if (this.shakeToggle) {
/*  331 */       this.animX += SHAKE_SPEED * Gdx.graphics.getDeltaTime();
/*  332 */       if (this.animX > SHAKE_THRESHOLD / 2.0F) {
/*  333 */         this.shakeToggle = (!this.shakeToggle);
/*      */       }
/*      */     } else {
/*  336 */       this.animX -= SHAKE_SPEED * Gdx.graphics.getDeltaTime();
/*  337 */       if (this.animX < -SHAKE_THRESHOLD / 2.0F) {
/*  338 */         this.shakeToggle = (!this.shakeToggle);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   protected void updateHopAnimation()
/*      */   {
/*  345 */     this.vY -= 17.0F * Settings.scale;
/*  346 */     this.animY += this.vY * Gdx.graphics.getDeltaTime();
/*  347 */     if (this.animY < 0.0F) {
/*  348 */       this.animationTimer = 0.0F;
/*  349 */       this.animY = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   protected void updateJumpAnimation() {
/*  354 */     this.vY -= 17.0F * Settings.scale;
/*  355 */     this.animY += this.vY * Gdx.graphics.getDeltaTime();
/*  356 */     if (this.animY < 0.0F) {
/*  357 */       this.animationTimer = 0.0F;
/*  358 */       this.animY = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   protected void updateStaggerAnimation() {
/*  363 */     if (this.animationTimer != 0.0F) {
/*  364 */       this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  365 */       if (!this.isPlayer) {
/*  366 */         this.animX = Interpolation.pow2.apply(STAGGER_MOVE_SPEED, 0.0F, 1.0F - this.animationTimer / 0.3F);
/*      */       } else {
/*  368 */         this.animX = Interpolation.pow2.apply(-STAGGER_MOVE_SPEED, 0.0F, 1.0F - this.animationTimer / 0.3F);
/*      */       }
/*  370 */       if (this.animationTimer < 0.0F) {
/*  371 */         this.animationTimer = 0.0F;
/*  372 */         this.animX = 0.0F;
/*  373 */         this.vX = STAGGER_MOVE_SPEED;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   protected void updateShakeAnimation() {
/*  379 */     this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  380 */     if (this.animationTimer < 0.0F) {
/*  381 */       this.animationTimer = 0.0F;
/*  382 */       this.animX = 0.0F;
/*      */     }
/*  384 */     else if (this.shakeToggle) {
/*  385 */       this.animX += SHAKE_SPEED * Gdx.graphics.getDeltaTime();
/*  386 */       if (this.animX > SHAKE_THRESHOLD) {
/*  387 */         this.shakeToggle = (!this.shakeToggle);
/*      */       }
/*      */     } else {
/*  390 */       this.animX -= SHAKE_SPEED * Gdx.graphics.getDeltaTime();
/*  391 */       if (this.animX < -SHAKE_THRESHOLD) {
/*  392 */         this.shakeToggle = (!this.shakeToggle);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   protected void loadAnimation(String atlasUrl, String skeletonUrl, float scale)
/*      */   {
/*  399 */     this.atlas = new TextureAtlas(Gdx.files.internal(atlasUrl));
/*  400 */     SkeletonJson json = new SkeletonJson(this.atlas);
/*  401 */     json.setScale(Settings.scale / scale);
/*  402 */     com.esotericsoftware.spine.SkeletonData skeletonData = json.readSkeletonData(Gdx.files.internal(skeletonUrl));
/*  403 */     this.skeleton = new Skeleton(skeletonData);
/*  404 */     this.skeleton.setColor(Color.WHITE);
/*  405 */     this.stateData = new AnimationStateData(skeletonData);
/*  406 */     this.state = new AnimationState(this.stateData);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void heal(int healAmount, boolean showEffect)
/*      */   {
/*  415 */     if ((Settings.isEndless) && (this.isPlayer) && (AbstractDungeon.player.hasBlight("FullBelly"))) {
/*  416 */       healAmount /= 2;
/*  417 */       if (healAmount < 1) {
/*  418 */         healAmount = 1;
/*      */       }
/*      */     }
/*      */     
/*  422 */     if (this.isDying) {
/*  423 */       return;
/*      */     }
/*      */     
/*  426 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  427 */       if (this.isPlayer) {
/*  428 */         healAmount = r.onPlayerHeal(healAmount);
/*      */       }
/*      */     }
/*      */     
/*  432 */     for (AbstractPower p : this.powers) {
/*  433 */       healAmount = p.onHeal(healAmount);
/*      */     }
/*      */     
/*  436 */     this.currentHealth += healAmount;
/*  437 */     if (this.currentHealth > this.maxHealth) {
/*  438 */       this.currentHealth = this.maxHealth;
/*      */     }
/*      */     
/*  441 */     if (healAmount > 0) {
/*  442 */       if (showEffect) {
/*  443 */         AbstractDungeon.effectsQueue.add(new HealEffect(this.hb.cX - this.animX, this.hb.cY, healAmount));
/*      */       }
/*  445 */       healthBarUpdatedEvent();
/*      */     }
/*      */   }
/*      */   
/*      */   public void heal(int amount) {
/*  450 */     heal(amount, true);
/*      */   }
/*      */   
/*      */   public void addBlock(int blockAmount) {
/*  454 */     float tmp = blockAmount;
/*  455 */     if (this.isPlayer) {
/*  456 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  457 */         tmp = r.onPlayerGainedBlock(tmp);
/*      */       }
/*  459 */       if (tmp > 0.0F)
/*  460 */         for (??? = this.powers.iterator(); ???.hasNext();) { p = (AbstractPower)???.next();
/*  461 */           p.onGainedBlock(tmp);
/*      */         }
/*      */     }
/*      */     AbstractPower p;
/*  465 */     boolean effect = false;
/*  466 */     if (this.currentBlock == 0) {
/*  467 */       effect = true;
/*      */     }
/*  469 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/*  470 */       for (AbstractPower p : m.powers) {
/*  471 */         tmp = p.onPlayerGainedBlock(tmp);
/*      */       }
/*      */     }
/*  474 */     this.currentBlock += com.badlogic.gdx.math.MathUtils.floor(tmp);
/*      */     
/*      */ 
/*  477 */     if ((this.currentBlock >= 99) && (this.isPlayer)) {
/*  478 */       UnlockTracker.unlockAchievement("IMPERVIOUS");
/*      */     }
/*      */     
/*      */ 
/*  482 */     if (this.currentBlock > 999) {
/*  483 */       this.currentBlock = 999;
/*      */     }
/*      */     
/*      */ 
/*  487 */     if (this.currentBlock == 999) {
/*  488 */       UnlockTracker.unlockAchievement("BARRICADED");
/*      */     }
/*      */     
/*  491 */     if ((effect) && (this.currentBlock > 0)) {
/*  492 */       gainBlockAnimation();
/*  493 */     } else if ((blockAmount > 0) && 
/*  494 */       (blockAmount > 0)) {
/*  495 */       Color tmpCol = Settings.GOLD_COLOR.cpy();
/*  496 */       tmpCol.a = this.blockTextColor.a;
/*  497 */       this.blockTextColor = tmpCol;
/*  498 */       this.blockScale = 5.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   public void loseBlock(int amount, boolean noAnimation)
/*      */   {
/*  504 */     boolean effect = false;
/*  505 */     if (this.currentBlock != 0) {
/*  506 */       effect = true;
/*      */     }
/*      */     
/*  509 */     this.currentBlock -= amount;
/*  510 */     if (this.currentBlock < 0) {
/*  511 */       this.currentBlock = 0;
/*      */     }
/*      */     
/*  514 */     if ((this.currentBlock == 0) && (effect)) {
/*  515 */       if (!noAnimation) {
/*  516 */         AbstractDungeon.effectList.add(new HbBlockBrokenEffect(this.hb.cX - this.hb.width / 2.0F + BLOCK_ICON_X, this.hb.cY - this.hb.height / 2.0F + BLOCK_ICON_Y));
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  521 */     else if ((this.currentBlock > 0) && (amount > 0)) {
/*  522 */       Color tmp = Color.SCARLET.cpy();
/*  523 */       tmp.a = this.blockTextColor.a;
/*  524 */       this.blockTextColor = tmp;
/*  525 */       this.blockScale = 5.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   public void loseBlock() {
/*  530 */     loseBlock(this.currentBlock);
/*      */   }
/*      */   
/*      */   public void loseBlock(boolean noAnimation) {
/*  534 */     loseBlock(this.currentBlock, noAnimation);
/*      */   }
/*      */   
/*      */   public void loseBlock(int amount) {
/*  538 */     loseBlock(amount, false);
/*      */   }
/*      */   
/*      */   public void showHealthBar() {
/*  542 */     this.hbShowTimer = 0.7F;
/*  543 */     this.hbAlpha = 0.0F;
/*      */   }
/*      */   
/*      */   public void hideHealthBar() {
/*  547 */     this.hbAlpha = 0.0F;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void addPower(AbstractPower powerToApply)
/*      */   {
/*  557 */     boolean hasBuffAlready = false;
/*      */     
/*  559 */     for (Iterator localIterator = this.powers.iterator(); localIterator.hasNext();) { p = (AbstractPower)localIterator.next();
/*  560 */       if (p.ID.equals(powerToApply.ID)) {
/*  561 */         p.stackPower(powerToApply.amount);
/*  562 */         p.updateDescription();
/*  563 */         hasBuffAlready = true;
/*      */       }
/*      */     }
/*      */     AbstractPower p;
/*  567 */     if (!hasBuffAlready) {
/*  568 */       this.powers.add(powerToApply);
/*      */       
/*      */ 
/*  571 */       if (this.isPlayer) {
/*  572 */         int buffCount = 0;
/*  573 */         for (AbstractPower p : this.powers) {
/*  574 */           if (p.type == com.megacrit.cardcrawl.powers.AbstractPower.PowerType.BUFF) {
/*  575 */             buffCount++;
/*      */           }
/*      */         }
/*  578 */         if (buffCount >= 10) {
/*  579 */           UnlockTracker.unlockAchievement("POWERFUL");
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnPowers() {
/*  586 */     for (AbstractPower p : this.powers) {
/*  587 */       p.atStartOfTurn();
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyTurnPowers() {
/*  592 */     for (AbstractPower p : this.powers) {
/*  593 */       p.duringTurn();
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnPostDrawPowers() {
/*  598 */     for (AbstractPower p : this.powers) {
/*  599 */       p.atStartOfTurnPostDraw();
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyEndOfTurnTriggers() {
/*  604 */     for (AbstractPower p : this.powers) {
/*  605 */       p.atEndOfTurn(this.isPlayer);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void healthBarUpdatedEvent()
/*      */   {
/*  613 */     this.healthBarAnimTimer = 1.2F;
/*  614 */     this.targetHealthBarWidth = (this.hb.width * this.currentHealth / this.maxHealth);
/*  615 */     if (this.maxHealth == this.currentHealth) {
/*  616 */       this.healthBarWidth = this.targetHealthBarWidth;
/*  617 */     } else if (this.currentHealth == 0) {
/*  618 */       this.healthBarWidth = 0.0F;
/*  619 */       this.targetHealthBarWidth = 0.0F;
/*      */     }
/*      */     
/*  622 */     if (this.targetHealthBarWidth > this.healthBarWidth) {
/*  623 */       this.healthBarWidth = this.targetHealthBarWidth;
/*      */     }
/*      */   }
/*      */   
/*      */   public void healthBarRevivedEvent() {
/*  628 */     this.healthBarAnimTimer = 1.2F;
/*  629 */     this.targetHealthBarWidth = (this.hb.width * this.currentHealth / this.maxHealth);
/*  630 */     this.healthBarWidth = this.targetHealthBarWidth;
/*  631 */     this.hbBgColor.a = 0.75F;
/*  632 */     this.hbShadowColor.a = 0.5F;
/*  633 */     this.hbTextColor.a = 1.0F;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void updateHealthBar()
/*      */   {
/*  640 */     updateHbHoverFade();
/*  641 */     updateBlockAnimations();
/*  642 */     updateHbPopInAnimation();
/*  643 */     updateHbDamageAnimation();
/*  644 */     updateHbAlpha();
/*      */   }
/*      */   
/*      */   private void updateHbHoverFade() {
/*  648 */     if (this.healthHb.hovered) {
/*  649 */       this.healthHideTimer -= Gdx.graphics.getDeltaTime() * 4.0F;
/*  650 */       if (this.healthHideTimer < 0.2F) {
/*  651 */         this.healthHideTimer = 0.2F;
/*      */       }
/*      */     } else {
/*  654 */       this.healthHideTimer += Gdx.graphics.getDeltaTime() * 4.0F;
/*  655 */       if (this.healthHideTimer > 1.0F) {
/*  656 */         this.healthHideTimer = 1.0F;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void updateHbAlpha()
/*      */   {
/*  666 */     if (((this instanceof AbstractMonster)) && (((AbstractMonster)this).isEscaping)) {
/*  667 */       this.hbAlpha = MathHelper.fadeLerpSnap(this.hbAlpha, 0.0F);
/*  668 */       this.targetHealthBarWidth = 0.0F;
/*  669 */       this.hbBgColor.a = (this.hbAlpha * 0.75F);
/*  670 */       this.hbShadowColor.a = (this.hbAlpha * 0.5F);
/*  671 */       this.hbTextColor.a = this.hbAlpha;
/*  672 */       this.orangeHbBarColor.a = this.hbAlpha;
/*  673 */       this.redHbBarColor.a = this.hbAlpha;
/*  674 */       this.greenHbBarColor.a = this.hbAlpha;
/*  675 */       this.blueHbBarColor.a = this.hbAlpha;
/*  676 */       this.blockOutlineColor.a = this.hbAlpha;
/*      */     }
/*  678 */     else if ((this.targetHealthBarWidth == 0.0F) && (this.healthBarAnimTimer <= 0.0F)) {
/*  679 */       this.hbShadowColor.a = MathHelper.fadeLerpSnap(this.hbShadowColor.a, 0.0F);
/*  680 */       this.hbBgColor.a = MathHelper.fadeLerpSnap(this.hbBgColor.a, 0.0F);
/*  681 */       this.hbTextColor.a = MathHelper.fadeLerpSnap(this.hbTextColor.a, 0.0F);
/*  682 */       this.blockOutlineColor.a = MathHelper.fadeLerpSnap(this.blockOutlineColor.a, 0.0F);
/*      */     }
/*      */     else {
/*  685 */       this.hbBgColor.a = (this.hbAlpha * 0.5F);
/*  686 */       this.hbShadowColor.a = (this.hbAlpha * 0.2F);
/*  687 */       this.hbTextColor.a = this.hbAlpha;
/*  688 */       this.orangeHbBarColor.a = this.hbAlpha;
/*  689 */       this.redHbBarColor.a = this.hbAlpha;
/*  690 */       this.greenHbBarColor.a = this.hbAlpha;
/*  691 */       this.blueHbBarColor.a = this.hbAlpha;
/*  692 */       this.blockOutlineColor.a = this.hbAlpha;
/*      */     }
/*      */   }
/*      */   
/*      */   protected void gainBlockAnimation() {
/*  697 */     this.blockAnimTimer = 0.7F;
/*  698 */     this.blockTextColor.a = 0.0F;
/*  699 */     this.blockColor.a = 0.0F;
/*      */   }
/*      */   
/*      */   private void updateBlockAnimations()
/*      */   {
/*  704 */     if (this.currentBlock > 0) {
/*  705 */       if (this.blockAnimTimer > 0.0F) {
/*  706 */         this.blockAnimTimer -= Gdx.graphics.getDeltaTime();
/*  707 */         if (this.blockAnimTimer < 0.0F) {
/*  708 */           this.blockAnimTimer = 0.0F;
/*      */         }
/*      */         
/*      */ 
/*  712 */         this.blockOffset = Interpolation.swingOut.apply(BLOCK_OFFSET_DIST * 3.0F, 0.0F, 1.0F - this.blockAnimTimer / 0.7F);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  718 */         this.blockScale = Interpolation.pow3In.apply(3.0F, 1.0F, 1.0F - this.blockAnimTimer / 0.7F);
/*      */         
/*      */ 
/*  721 */         this.blockColor.a = Interpolation.pow2Out.apply(0.0F, 1.0F, 1.0F - this.blockAnimTimer / 0.7F);
/*  722 */         this.blockTextColor.a = Interpolation.pow5In.apply(0.0F, 1.0F, 1.0F - this.blockAnimTimer / 0.7F);
/*      */       }
/*  724 */       else if (this.blockScale != 1.0F) {
/*  725 */         this.blockScale = MathHelper.scaleLerpSnap(this.blockScale, 1.0F);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  730 */       if (this.blockTextColor.r != 1.0F) {
/*  731 */         this.blockTextColor.r = MathHelper.slowColorLerpSnap(this.blockTextColor.r, 1.0F);
/*      */       }
/*  733 */       if (this.blockTextColor.g != 1.0F) {
/*  734 */         this.blockTextColor.g = MathHelper.slowColorLerpSnap(this.blockTextColor.g, 1.0F);
/*      */       }
/*  736 */       if (this.blockTextColor.b != 1.0F) {
/*  737 */         this.blockTextColor.b = MathHelper.slowColorLerpSnap(this.blockTextColor.b, 1.0F);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateHbPopInAnimation()
/*      */   {
/*  744 */     if (this.hbShowTimer > 0.0F) {
/*  745 */       this.hbShowTimer -= Gdx.graphics.getDeltaTime();
/*  746 */       if (this.hbShowTimer < 0.0F) {
/*  747 */         this.hbShowTimer = 0.0F;
/*      */       }
/*      */       
/*  750 */       this.hbAlpha = Interpolation.fade.apply(0.0F, 1.0F, 1.0F - this.hbShowTimer / 0.7F);
/*  751 */       this.hbYOffset = Interpolation.exp10Out.apply(HB_Y_OFFSET_DIST * 5.0F, 0.0F, 1.0F - this.hbShowTimer / 0.7F);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void updateHbDamageAnimation()
/*      */   {
/*  761 */     if (this.healthBarAnimTimer > 0.0F) {
/*  762 */       this.healthBarAnimTimer -= Gdx.graphics.getDeltaTime();
/*      */     }
/*      */     
/*      */ 
/*  766 */     if ((this.healthBarWidth != this.targetHealthBarWidth) && (this.healthBarAnimTimer <= 0.0F) && (this.targetHealthBarWidth < this.healthBarWidth))
/*      */     {
/*  768 */       this.healthBarWidth = MathHelper.uiLerpSnap(this.healthBarWidth, this.targetHealthBarWidth);
/*      */     }
/*      */   }
/*      */   
/*      */   public void updatePowers() {
/*  773 */     for (int i = 0; i < this.powers.size(); i++) {
/*  774 */       ((AbstractPower)this.powers.get(i)).update(i);
/*      */     }
/*      */   }
/*      */   
/*      */   public static void initialize() {
/*  779 */     sr = new SkeletonMeshRenderer();
/*  780 */     sr.setPremultipliedAlpha(true);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderPowerTips(SpriteBatch sb)
/*      */   {
/*  789 */     ArrayList<PowerTip> tips = new ArrayList();
/*  790 */     for (AbstractPower p : this.powers) {
/*  791 */       if (p.region48 != null) {
/*  792 */         tips.add(new PowerTip(p.name, p.description, p.region48));
/*      */       } else {
/*  794 */         tips.add(new PowerTip(p.name, p.description, p.img));
/*      */       }
/*      */     }
/*      */     
/*  798 */     if (!tips.isEmpty()) {
/*  799 */       float offsetY = (tips.size() - 1) * MULTI_TIP_Y_OFFSET + TIP_OFFSET_Y;
/*      */       
/*      */ 
/*  802 */       if (this.hb.cX + this.hb.width / 2.0F < TIP_X_THRESHOLD) {
/*  803 */         TipHelper.queuePowerTips(this.hb.cX + this.hb.width / 2.0F + TIP_OFFSET_R_X, this.hb.cY + offsetY, tips);
/*      */       } else {
/*  805 */         TipHelper.queuePowerTips(this.hb.cX - this.hb.width / 2.0F + TIP_OFFSET_L_X, this.hb.cY + offsetY, tips);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void useFastAttackAnimation() {
/*  811 */     this.animX = 0.0F;
/*  812 */     this.animY = 0.0F;
/*  813 */     this.animationTimer = 0.4F;
/*  814 */     this.animation = CreatureAnimation.ATTACK_FAST;
/*      */   }
/*      */   
/*      */   public void useSlowAttackAnimation() {
/*  818 */     this.animX = 0.0F;
/*  819 */     this.animY = 0.0F;
/*  820 */     this.animationTimer = 1.0F;
/*  821 */     this.animation = CreatureAnimation.ATTACK_SLOW;
/*      */   }
/*      */   
/*      */   public void useHopAnimation() {
/*  825 */     this.animX = 0.0F;
/*  826 */     this.animY = 0.0F;
/*  827 */     this.vY = (300.0F * Settings.scale);
/*  828 */     this.animationTimer = 0.7F;
/*  829 */     this.animation = CreatureAnimation.HOP;
/*      */   }
/*      */   
/*      */   public void useJumpAnimation() {
/*  833 */     this.animX = 0.0F;
/*  834 */     this.animY = 0.0F;
/*  835 */     this.vY = (500.0F * Settings.scale);
/*  836 */     this.animationTimer = 0.7F;
/*  837 */     this.animation = CreatureAnimation.JUMP;
/*      */   }
/*      */   
/*      */   public void useStaggerAnimation() {
/*  841 */     if (this.animY == 0.0F) {
/*  842 */       this.animX = 0.0F;
/*  843 */       this.animationTimer = 0.3F;
/*  844 */       this.animation = CreatureAnimation.STAGGER;
/*      */     }
/*      */   }
/*      */   
/*      */   public void useFastShakeAnimation(float duration) {
/*  849 */     if (this.animY == 0.0F) {
/*  850 */       this.animX = 0.0F;
/*  851 */       this.animationTimer = duration;
/*  852 */       this.animation = CreatureAnimation.FAST_SHAKE;
/*      */     }
/*      */   }
/*      */   
/*      */   public void useShakeAnimation(float duration) {
/*  857 */     if (this.animY == 0.0F) {
/*  858 */       this.animX = 0.0F;
/*  859 */       this.animationTimer = duration;
/*  860 */       this.animation = CreatureAnimation.SHAKE;
/*      */     }
/*      */   }
/*      */   
/*      */   public AbstractPower getPower(String targetID) {
/*  865 */     for (AbstractPower p : this.powers) {
/*  866 */       if (p.ID.equals(targetID)) {
/*  867 */         return p;
/*      */       }
/*      */     }
/*  870 */     return null;
/*      */   }
/*      */   
/*      */   public boolean hasPower(String targetID) {
/*  874 */     for (AbstractPower p : this.powers) {
/*  875 */       if (p.ID.equals(targetID)) {
/*  876 */         return true;
/*      */       }
/*      */     }
/*  879 */     return false;
/*      */   }
/*      */   
/*      */   public boolean isDeadOrEscaped() {
/*  883 */     if ((this.isDying) || (this.halfDead)) {
/*  884 */       return true;
/*      */     }
/*      */     
/*  887 */     if (!this.isPlayer) {
/*  888 */       AbstractMonster m = (AbstractMonster)this;
/*  889 */       if (m.isEscaping) {
/*  890 */         return true;
/*      */       }
/*      */     }
/*      */     
/*  894 */     return false;
/*      */   }
/*      */   
/*      */   public void loseGold(int goldAmount)
/*      */   {
/*  899 */     if (goldAmount > 0) {
/*  900 */       this.gold -= goldAmount;
/*  901 */       if (this.gold < 0) {
/*  902 */         this.gold = 0;
/*      */       }
/*      */     } else {
/*  905 */       logger.info("NEGATIVE MONEY???");
/*      */     }
/*      */   }
/*      */   
/*      */   public void gainGold(int amount)
/*      */   {
/*  911 */     if (amount < 0) {
/*  912 */       logger.info("NEGATIVE MONEY???");
/*      */     } else {
/*  914 */       this.gold += amount;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderReticle(SpriteBatch sb)
/*      */   {
/*  924 */     this.reticleRendered = true;
/*      */     
/*      */ 
/*  927 */     renderReticleCorner(sb, -this.hb.width / 2.0F + this.reticleOffset, this.hb.height / 2.0F - this.reticleOffset, false, false);
/*  928 */     renderReticleCorner(sb, this.hb.width / 2.0F - this.reticleOffset, this.hb.height / 2.0F - this.reticleOffset, true, false);
/*  929 */     renderReticleCorner(sb, -this.hb.width / 2.0F + this.reticleOffset, -this.hb.height / 2.0F + this.reticleOffset, false, true);
/*  930 */     renderReticleCorner(sb, this.hb.width / 2.0F - this.reticleOffset, -this.hb.height / 2.0F + this.reticleOffset, true, true);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderReticle(SpriteBatch sb, Hitbox hb)
/*      */   {
/*  939 */     this.reticleRendered = true;
/*      */     
/*      */ 
/*  942 */     renderReticleCorner(sb, -hb.width / 2.0F + this.reticleOffset, hb.height / 2.0F - this.reticleOffset, hb, false, false);
/*  943 */     renderReticleCorner(sb, hb.width / 2.0F - this.reticleOffset, hb.height / 2.0F - this.reticleOffset, hb, true, false);
/*  944 */     renderReticleCorner(sb, -hb.width / 2.0F + this.reticleOffset, -hb.height / 2.0F + this.reticleOffset, hb, false, true);
/*  945 */     renderReticleCorner(sb, hb.width / 2.0F - this.reticleOffset, -hb.height / 2.0F + this.reticleOffset, hb, true, true);
/*      */   }
/*      */   
/*      */   protected void updateReticle()
/*      */   {
/*  950 */     if (this.reticleRendered) {
/*  951 */       this.reticleRendered = false;
/*  952 */       this.reticleAlpha += Gdx.graphics.getDeltaTime() * 3.0F;
/*  953 */       if (this.reticleAlpha > 1.0F) {
/*  954 */         this.reticleAlpha = 1.0F;
/*      */       }
/*      */       
/*      */ 
/*  958 */       this.reticleAnimTimer += Gdx.graphics.getDeltaTime();
/*  959 */       if (this.reticleAnimTimer > 1.0F) {
/*  960 */         this.reticleAnimTimer = 1.0F;
/*      */       }
/*  962 */       this.reticleOffset = Interpolation.elasticOut.apply(RETICLE_OFFSET_DIST, 0.0F, this.reticleAnimTimer);
/*      */     }
/*      */     else
/*      */     {
/*  966 */       this.reticleAlpha = 0.0F;
/*  967 */       this.reticleAnimTimer = 0.0F;
/*  968 */       this.reticleOffset = RETICLE_OFFSET_DIST;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderHealth(SpriteBatch sb)
/*      */   {
/*  978 */     if (Settings.hideCombatElements) {
/*  979 */       return;
/*      */     }
/*      */     
/*  982 */     float x = this.hb.cX - this.hb.width / 2.0F;
/*  983 */     float y = this.hb.cY - this.hb.height / 2.0F + this.hbYOffset;
/*      */     
/*  985 */     renderHealthBg(sb, x, y);
/*      */     
/*  987 */     if (this.targetHealthBarWidth != 0.0F) {
/*  988 */       renderOrangeHealthBar(sb, x, y);
/*  989 */       if (hasPower("Poison")) {
/*  990 */         renderGreenHealthBar(sb, x, y);
/*      */       }
/*  992 */       renderRedHealthBar(sb, x, y);
/*      */     }
/*      */     
/*      */ 
/*  996 */     if ((this.currentBlock != 0) && (this.hbAlpha != 0.0F)) {
/*  997 */       renderBlockOutline(sb, x, y);
/*      */     }
/*      */     
/* 1000 */     renderHealthText(sb, y);
/*      */     
/*      */ 
/* 1003 */     if ((this.currentBlock != 0) && (this.hbAlpha != 0.0F)) {
/* 1004 */       renderBlockIconAndValue(sb, x, y);
/*      */     }
/*      */     
/* 1007 */     renderPowerIcons(sb, x, y);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBlockOutline(SpriteBatch sb, float x, float y)
/*      */   {
/* 1016 */     sb.setColor(this.blockOutlineColor);
/* 1017 */     sb.setBlendFunction(770, 1);
/*      */     
/*      */ 
/* 1020 */     sb.draw(ImageMaster.BLOCK_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1028 */     sb.draw(ImageMaster.BLOCK_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.hb.width, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/* 1031 */     sb.draw(ImageMaster.BLOCK_BAR_R, x + this.hb.width, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/* 1032 */     sb.setBlendFunction(770, 771);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBlockIconAndValue(SpriteBatch sb, float x, float y)
/*      */   {
/* 1041 */     sb.setColor(this.blockColor);
/* 1042 */     sb.draw(ImageMaster.BLOCK_ICON, x + BLOCK_ICON_X - 32.0F, y + BLOCK_ICON_Y - 32.0F + this.blockOffset, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1060 */     FontHelper.renderFontCentered(sb, FontHelper.blockInfoFont, 
/*      */     
/*      */ 
/* 1063 */       Integer.toString(this.currentBlock), x + BLOCK_ICON_X, y - 16.0F * Settings.scale, this.blockTextColor, this.blockScale);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderHealthBg(SpriteBatch sb, float x, float y)
/*      */   {
/* 1077 */     sb.setColor(this.hbShadowColor);
/*      */     
/* 1079 */     sb.draw(ImageMaster.HB_SHADOW_L, x - HEALTH_BAR_HEIGHT, y - HEALTH_BG_OFFSET_X + 3.0F * Settings.scale, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1087 */     sb.draw(ImageMaster.HB_SHADOW_B, x, y - HEALTH_BG_OFFSET_X + 3.0F * Settings.scale, this.hb.width, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/* 1090 */     sb.draw(ImageMaster.HB_SHADOW_R, x + this.hb.width, y - HEALTH_BG_OFFSET_X + 3.0F * Settings.scale, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1098 */     sb.setColor(this.hbBgColor);
/* 1099 */     if (this.currentHealth != this.maxHealth)
/*      */     {
/*      */ 
/* 1102 */       sb.draw(ImageMaster.HEALTH_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1110 */       sb.draw(ImageMaster.HEALTH_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.hb.width, HEALTH_BAR_HEIGHT);
/*      */       
/*      */ 
/* 1113 */       sb.draw(ImageMaster.HEALTH_BAR_R, x + this.hb.width, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderOrangeHealthBar(SpriteBatch sb, float x, float y)
/*      */   {
/* 1129 */     sb.setColor(this.orangeHbBarColor);
/*      */     
/*      */ 
/* 1132 */     sb.draw(ImageMaster.HEALTH_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1140 */     sb.draw(ImageMaster.HEALTH_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.healthBarWidth, HEALTH_BAR_HEIGHT);
/* 1141 */     sb.draw(ImageMaster.HEALTH_BAR_R, x + this.healthBarWidth, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderGreenHealthBar(SpriteBatch sb, float x, float y)
/*      */   {
/* 1156 */     sb.setColor(this.greenHbBarColor);
/*      */     
/*      */ 
/* 1159 */     if (this.currentHealth > 0) {
/* 1160 */       sb.draw(ImageMaster.HEALTH_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1169 */     sb.draw(ImageMaster.HEALTH_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.targetHealthBarWidth, HEALTH_BAR_HEIGHT);
/*      */     
/*      */ 
/* 1172 */     sb.draw(ImageMaster.HEALTH_BAR_R, x + this.targetHealthBarWidth, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderRedHealthBar(SpriteBatch sb, float x, float y)
/*      */   {
/* 1187 */     if (this.currentBlock > 0) {
/* 1188 */       sb.setColor(this.blueHbBarColor);
/*      */     } else {
/* 1190 */       sb.setColor(this.redHbBarColor);
/*      */     }
/*      */     
/* 1193 */     if (!hasPower("Poison"))
/*      */     {
/* 1195 */       if (this.currentHealth > 0) {
/* 1196 */         sb.draw(ImageMaster.HEALTH_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1205 */       sb.draw(ImageMaster.HEALTH_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.targetHealthBarWidth, HEALTH_BAR_HEIGHT);
/*      */       
/*      */ 
/* 1208 */       sb.draw(ImageMaster.HEALTH_BAR_R, x + this.targetHealthBarWidth, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1215 */       int poisonAmt = getPower("Poison").amount;
/* 1216 */       if ((poisonAmt > 0) && (hasPower("Intangible"))) {
/* 1217 */         poisonAmt = 1;
/*      */       }
/* 1219 */       if (this.currentHealth > poisonAmt) {
/* 1220 */         float w = 1.0F - (this.currentHealth - poisonAmt) / this.currentHealth;
/* 1221 */         w *= this.targetHealthBarWidth;
/*      */         
/* 1223 */         if (this.currentHealth > 0) {
/* 1224 */           sb.draw(ImageMaster.HEALTH_BAR_L, x - HEALTH_BAR_HEIGHT, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1233 */         sb.draw(ImageMaster.HEALTH_BAR_B, x, y + HEALTH_BAR_OFFSET_Y, this.targetHealthBarWidth - w, HEALTH_BAR_HEIGHT);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1241 */         sb.draw(ImageMaster.HEALTH_BAR_R, x + this.targetHealthBarWidth - w, y + HEALTH_BAR_OFFSET_Y, HEALTH_BAR_HEIGHT, HEALTH_BAR_HEIGHT);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderHealthText(SpriteBatch sb, float y)
/*      */   {
/* 1257 */     if (this.targetHealthBarWidth != 0.0F) {
/* 1258 */       float tmp = this.hbTextColor.a;
/* 1259 */       this.hbTextColor.a *= this.healthHideTimer;
/*      */       
/* 1261 */       FontHelper.renderFontCentered(sb, FontHelper.healthInfoFont, 
/*      */       
/*      */ 
/* 1264 */         Integer.toString(this.currentHealth) + "/" + Integer.toString(this.maxHealth), this.hb.cX, y + HEALTH_BAR_OFFSET_Y + HEALTH_TEXT_OFFSET_Y + 5.0F * Settings.scale, this.hbTextColor);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1269 */       this.hbTextColor.a = tmp;
/*      */     } else {
/* 1271 */       FontHelper.renderFontCentered(sb, FontHelper.healthInfoFont, TEXT[0], this.hb.cX, y + HEALTH_BAR_OFFSET_Y + HEALTH_TEXT_OFFSET_Y - 1.0F * Settings.scale, this.hbTextColor);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderPowerIcons(SpriteBatch sb, float x, float y)
/*      */   {
/* 1287 */     float offset = 10.0F * Settings.scale;
/*      */     
/* 1289 */     for (AbstractPower p : this.powers) {
/* 1290 */       p.renderIcons(sb, x + offset, y - 48.0F * Settings.scale, this.hbTextColor);
/* 1291 */       offset += POWER_ICON_PADDING_X;
/*      */     }
/*      */     
/* 1294 */     offset = 0.0F * Settings.scale;
/*      */     
/* 1296 */     for (AbstractPower p : this.powers) {
/* 1297 */       p.renderAmount(sb, x + offset + 32.0F * Settings.scale, y - 66.0F * Settings.scale, this.hbTextColor);
/* 1298 */       offset += POWER_ICON_PADDING_X;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderReticleCorner(SpriteBatch sb, float x, float y, Hitbox hb, boolean flipX, boolean flipY)
/*      */   {
/* 1313 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.reticleAlpha / 4.0F));
/* 1314 */     sb.draw(ImageMaster.RETICLE_CORNER, hb.cX + x - 18.0F + 4.0F * Settings.scale, hb.cY + y - 18.0F - 4.0F * Settings.scale, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1333 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.reticleAlpha));
/* 1334 */     sb.draw(ImageMaster.RETICLE_CORNER, hb.cX + x - 18.0F, hb.cY + y - 18.0F, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderReticleCorner(SpriteBatch sb, float x, float y, boolean flipX, boolean flipY)
/*      */   {
/* 1364 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.reticleAlpha / 4.0F));
/* 1365 */     sb.draw(ImageMaster.RETICLE_CORNER, this.hb.cX + x - 18.0F + 4.0F * Settings.scale, this.hb.cY + y - 18.0F - 4.0F * Settings.scale, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1384 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.reticleAlpha));
/* 1385 */     sb.draw(ImageMaster.RETICLE_CORNER, this.hb.cX + x - 18.0F, this.hb.cY + y - 18.0F, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*      */   }
/*      */   
/*      */   public abstract void render(SpriteBatch paramSpriteBatch);
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\core\AbstractCreature.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */