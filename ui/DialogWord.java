/*     */ package com.megacrit.cardcrawl.ui;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ 
/*     */ public class DialogWord
/*     */ {
/*     */   private BitmapFont font;
/*     */   private WordEffect effect;
/*     */   private WordColor wColor;
/*     */   public String word;
/*  17 */   public int line = 0;
/*     */   private float x;
/*  19 */   private float y; private float target_x; private float target_y; private float offset_x; private float offset_y; private float timer = 0.0F;
/*     */   private Color color;
/*  21 */   private Color targetColor; private float scale = 1.0F; private float targetScale = 1.0F;
/*  22 */   private static final float BUMP_OFFSET = 20.0F * Settings.scale;
/*     */   private static com.badlogic.gdx.graphics.g2d.GlyphLayout gl;
/*     */   private static final float COLOR_LERP_SPEED = 8.0F;
/*  25 */   private static final float SHAKE_AMT = 2.0F * Settings.scale;
/*  26 */   private static final float DIALOG_FADE_Y = 50.0F * Settings.scale;
/*     */   private static final float WAVY_SPEED = 6.0F;
/*     */   private static final float WAVY_DIST = 3.0F;
/*     */   private static final float SHAKE_INTERVAL = 0.02F;
/*     */   
/*     */   public static enum AppearEffect
/*     */   {
/*  33 */     NONE,  FADE_IN,  GROW_IN,  BUMP_IN;
/*     */     
/*     */     private AppearEffect() {} }
/*     */   
/*  37 */   public static enum WordEffect { NONE,  WAVY,  SHAKY,  PULSE;
/*     */     
/*     */     private WordEffect() {} }
/*     */   
/*  41 */   public static enum WordColor { DEFAULT,  RED,  GREEN,  BLUE,  GOLD,  PURPLE;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     private WordColor() {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public DialogWord(BitmapFont font, String word, AppearEffect a_effect, WordEffect effect, WordColor wColor, float x, float y, int line)
/*     */   {
/*  54 */     if (gl == null) {
/*  55 */       gl = new com.badlogic.gdx.graphics.g2d.GlyphLayout();
/*     */     }
/*     */     
/*  58 */     this.font = font;
/*  59 */     this.effect = effect;
/*  60 */     this.wColor = wColor;
/*  61 */     this.word = word;
/*  62 */     this.x = x;
/*  63 */     this.y = y;
/*  64 */     this.target_x = x;
/*  65 */     this.target_y = y;
/*  66 */     this.targetColor = getColor();
/*  67 */     this.line = line;
/*  68 */     this.color = new Color(this.targetColor.r, this.targetColor.g, this.targetColor.b, 0.0F);
/*     */     
/*  70 */     if (effect == WordEffect.WAVY) {
/*  71 */       this.timer = MathUtils.random(1.5707964F);
/*     */     }
/*     */     
/*  74 */     switch (a_effect) {
/*     */     case FADE_IN: 
/*     */       break;
/*     */     case GROW_IN: 
/*  78 */       this.y -= BUMP_OFFSET;
/*  79 */       this.scale = 0.0F;
/*  80 */       break;
/*     */     case BUMP_IN: 
/*  82 */       this.y -= BUMP_OFFSET;
/*  83 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   private Color getColor()
/*     */   {
/*  91 */     switch (this.wColor) {
/*     */     case RED: 
/*  93 */       return Settings.RED_TEXT_COLOR.cpy();
/*     */     case GREEN: 
/*  95 */       return Settings.GREEN_TEXT_COLOR.cpy();
/*     */     case BLUE: 
/*  97 */       return Settings.BLUE_TEXT_COLOR.cpy();
/*     */     case GOLD: 
/*  99 */       return Settings.GOLD_COLOR.cpy();
/*     */     case PURPLE: 
/* 101 */       return Settings.PURPLE_COLOR.cpy();
/*     */     }
/* 103 */     return Settings.CREAM_COLOR.cpy();
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/* 109 */     if (this.x != this.target_x) {
/* 110 */       this.x = MathUtils.lerp(this.x, this.target_x, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/* 112 */     if (this.y != this.target_y) {
/* 113 */       this.y = MathUtils.lerp(this.y, this.target_y, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/*     */     
/*     */ 
/* 117 */     this.color = this.color.lerp(this.targetColor, Gdx.graphics.getDeltaTime() * 8.0F);
/*     */     
/*     */ 
/* 120 */     if (this.scale != this.targetScale) {
/* 121 */       this.scale = com.megacrit.cardcrawl.helpers.MathHelper.scaleLerpSnap(this.scale, this.targetScale);
/*     */     }
/*     */     
/* 124 */     applyEffects();
/*     */   }
/*     */   
/*     */   private void applyEffects() {
/* 128 */     switch (this.effect) {
/*     */     case SHAKY: 
/* 130 */       this.timer -= Gdx.graphics.getDeltaTime();
/* 131 */       if (this.timer < 0.0F) {
/* 132 */         this.offset_x = MathUtils.random(-SHAKE_AMT, SHAKE_AMT);
/* 133 */         this.offset_y = MathUtils.random(-SHAKE_AMT, SHAKE_AMT);
/* 134 */         this.timer = 0.02F;
/*     */       }
/*     */       break;
/*     */     case WAVY: 
/* 138 */       this.timer += Gdx.graphics.getDeltaTime() * 6.0F;
/* 139 */       this.offset_y = ((float)Math.cos(this.timer) * Settings.scale * 3.0F);
/* 140 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void fadeOut()
/*     */   {
/* 148 */     this.targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*     */   }
/*     */   
/*     */   public void dialogFadeOut() {
/* 152 */     this.targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/* 153 */     this.target_y -= DIALOG_FADE_Y;
/*     */   }
/*     */   
/*     */   public void shiftY(float shiftAmount) {
/* 157 */     this.target_y += shiftAmount;
/*     */   }
/*     */   
/*     */   public void shiftX(float shiftAmount) {
/* 161 */     this.target_x += shiftAmount;
/*     */   }
/*     */   
/*     */   public void setX(float newX) {
/* 165 */     this.target_x = newX;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 169 */     this.font.setColor(this.color);
/* 170 */     this.font.getData().setScale(this.scale);
/* 171 */     this.font.draw(sb, this.word, this.x + this.offset_x, this.y + this.offset_y);
/* 172 */     this.font.getData().setScale(1.0F);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float y2) {
/* 176 */     this.font.setColor(this.color);
/* 177 */     this.font.getData().setScale(this.scale);
/* 178 */     this.font.draw(sb, this.word, this.x + this.offset_x, this.y + this.offset_y + y2);
/* 179 */     this.font.getData().setScale(1.0F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static WordEffect identifyWordEffect(String word)
/*     */   {
/* 189 */     if (word.length() > 2) {
/* 190 */       if ((word.charAt(0) == '@') && (word.charAt(word.length() - 1) == '@'))
/* 191 */         return WordEffect.SHAKY;
/* 192 */       if ((word.charAt(0) == '~') && (word.charAt(word.length() - 1) == '~')) {
/* 193 */         return WordEffect.WAVY;
/*     */       }
/*     */     }
/*     */     
/* 197 */     return WordEffect.NONE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static WordColor identifyWordColor(String word)
/*     */   {
/* 207 */     if (word.charAt(0) == '#') {
/* 208 */       switch (word.charAt(1)) {
/*     */       case 'r': 
/* 210 */         return WordColor.RED;
/*     */       case 'g': 
/* 212 */         return WordColor.GREEN;
/*     */       case 'b': 
/* 214 */         return WordColor.BLUE;
/*     */       case 'y': 
/* 216 */         return WordColor.GOLD;
/*     */       case 'p': 
/* 218 */         return WordColor.PURPLE;
/*     */       }
/*     */       
/*     */     }
/* 222 */     return WordColor.DEFAULT;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\DialogWord.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */