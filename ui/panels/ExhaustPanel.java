/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ 
/*     */ public class ExhaustPanel extends AbstractPanel
/*     */ {
/*  23 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Exhaust Tip");
/*  24 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  25 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  26 */   public static float fontScale = 1.0F;
/*     */   public static final float FONT_POP_SCALE = 2.0F;
/*  28 */   private static final float COUNT_CIRCLE_W = 128.0F * Settings.scale;
/*  29 */   public static int totalCount = 0;
/*  30 */   private GlyphLayout gl = new GlyphLayout();
/*  31 */   private Hitbox hb = new Hitbox(0.0F, 0.0F, 100.0F * Settings.scale, 100.0F * Settings.scale);
/*  32 */   public static float energyVfxTimer = 0.0F;
/*     */   public static final float ENERGY_VFX_TIME = 2.0F;
/*     */   
/*     */   public ExhaustPanel() {
/*  36 */     super(Settings.WIDTH - 70.0F * Settings.scale, 184.0F * Settings.scale, Settings.WIDTH + 100.0F * Settings.scale, 184.0F * Settings.scale, 0.0F, 0.0F, null, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void updatePositions()
/*     */   {
/*  49 */     super.updatePositions();
/*     */     
/*  51 */     if ((!this.isHidden) && 
/*  52 */       (AbstractDungeon.player.exhaustPile.size() > 0)) {
/*  53 */       this.hb.update();
/*  54 */       updateVfx();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  59 */     if ((this.hb.hovered) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.EXHAUST_VIEW) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.HAND_SELECT)))
/*     */     {
/*  61 */       AbstractDungeon.overlayMenu.hoveredTip = true;
/*  62 */       if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/*  63 */         this.hb.clickStarted = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  68 */     if (((this.hb.clicked) || (InputActionSet.exhaustPile.isJustPressed()) || 
/*  69 */       (CInputActionSet.pageRightViewExhaust.isJustPressed())) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.EXHAUST_VIEW)) {
/*  70 */       this.hb.clicked = false;
/*  71 */       this.hb.hovered = false;
/*  72 */       CardCrawlGame.sound.play("DECK_CLOSE");
/*  73 */       AbstractDungeon.closeCurrentScreen();
/*  74 */       return;
/*     */     }
/*     */     
/*     */ 
/*  78 */     if (((this.hb.clicked) || (InputActionSet.exhaustPile.isJustPressed()) || 
/*  79 */       (CInputActionSet.pageRightViewExhaust.isJustPressed())) && (AbstractDungeon.overlayMenu.combatPanelsShown) && (AbstractDungeon.getMonsters() != null) && 
/*  80 */       (!AbstractDungeon.getMonsters().areMonstersDead()) && (!AbstractDungeon.player.isDead))
/*     */     {
/*  82 */       this.hb.clicked = false;
/*  83 */       this.hb.hovered = false;
/*     */       
/*  85 */       if (AbstractDungeon.isScreenUp) {
/*  86 */         if (AbstractDungeon.previousScreen == null) {
/*  87 */           AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*     */         }
/*     */       } else {
/*  90 */         AbstractDungeon.previousScreen = null;
/*     */       }
/*     */       
/*  93 */       openExhaustPile();
/*     */     }
/*     */   }
/*     */   
/*     */   private void openExhaustPile() {
/*  98 */     AbstractDungeon.exhaustPileViewScreen.open();
/*  99 */     this.hb.hovered = false;
/* 100 */     com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*     */   }
/*     */   
/*     */   private void updateVfx() {
/* 104 */     energyVfxTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 105 */     if (energyVfxTimer < 0.0F) {
/* 106 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.ExhaustPileParticle(this.current_x, this.current_y));
/* 107 */       energyVfxTimer = 0.05F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 113 */     if ((!Settings.hideLowerElements) && (!AbstractDungeon.player.exhaustPile.isEmpty())) {
/* 114 */       this.hb.move(this.current_x, this.current_y);
/*     */       
/*     */ 
/* 117 */       String msg = Integer.toString(AbstractDungeon.player.exhaustPile.size());
/* 118 */       this.gl.setText(FontHelper.deckCountFont, msg);
/* 119 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.8F));
/* 120 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.DECK_COUNT_CIRCLE, this.current_x - COUNT_CIRCLE_W / 2.0F, this.current_y - COUNT_CIRCLE_W / 2.0F, COUNT_CIRCLE_W, COUNT_CIRCLE_W);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 127 */       FontHelper.renderFontCentered(sb, FontHelper.deckCountFont, msg, this.current_x, this.current_y + 2.0F * Settings.scale, Settings.PURPLE_COLOR
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 133 */         .cpy());
/*     */       
/* 135 */       if (Settings.isControllerMode) {
/* 136 */         sb.setColor(Color.WHITE);
/* 137 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 138 */           .getKeyImg(), this.current_x - 32.0F + 30.0F * Settings.scale, this.current_y - 32.0F - 30.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 156 */       this.hb.render(sb);
/* 157 */       if ((this.hb.hovered) && (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && (!AbstractDungeon.isScreenUp)) {
/* 158 */         com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(1550.0F * Settings.scale, 450.0F * Settings.scale, LABEL[0] + " (" + InputActionSet.exhaustPile
/*     */         
/*     */ 
/* 161 */           .getKeyString() + ")", MSG[0]);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\ExhaustPanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */