/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*     */ import com.megacrit.cardcrawl.core.EnergyManager;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.RefreshEnergyEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class EnergyPanel extends AbstractPanel
/*     */ {
/*  26 */   private static final TutorialStrings tutorialStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getTutorialString("Energy Panel Tip");
/*     */   
/*  28 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  29 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*     */   private static final int RAW_W = 256;
/*     */   private static final int ORB_W = 128;
/*  32 */   private static final Color ENERGY_TEXT_COLOR = new Color(1.0F, 1.0F, 0.86F, 1.0F);
/*     */   
/*  34 */   public static float fontScale = 1.0F;
/*     */   public static final float FONT_POP_SCALE = 2.0F;
/*  36 */   private static final float ORB_IMG_SCALE = 1.15F * Settings.scale;
/*  37 */   public static int totalCount = 0;
/*  38 */   private Hitbox tipHitbox = new Hitbox(0.0F, 0.0F, 120.0F * Settings.scale, 120.0F * Settings.scale);
/*     */   
/*     */ 
/*  41 */   private com.badlogic.gdx.graphics.Texture gainEnergyImg = null;
/*  42 */   private float energyVfxAngle = 0.0F; private float energyVfxScale = Settings.scale;
/*  43 */   private Color energyVfxColor = Color.WHITE.cpy();
/*  44 */   public static float energyVfxTimer = 0.0F;
/*     */   
/*     */   public static final float ENERGY_VFX_TIME = 2.0F;
/*     */   private static final float VFX_ROTATE_SPEED = -30.0F;
/*     */   private float angle5;
/*     */   
/*     */   public EnergyPanel()
/*     */   {
/*  52 */     super(198.0F * Settings.scale, 190.0F * Settings.scale, -480.0F * Settings.scale, 200.0F * Settings.scale, 12.0F * Settings.scale, -12.0F * Settings.scale, null, true);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  62 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/*  64 */       this.gainEnergyImg = ImageMaster.RED_ORB_FLASH_VFX;
/*  65 */       break;
/*     */     case THE_SILENT: 
/*  67 */       this.gainEnergyImg = ImageMaster.GREEN_ORB_FLASH_VFX;
/*  68 */       break;
/*     */     case DEFECT: 
/*  70 */       this.gainEnergyImg = ImageMaster.BLUE_ORB_FLASH_VFX;
/*  71 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public static void setEnergy(int energy)
/*     */   {
/*  79 */     totalCount = energy;
/*     */     
/*  81 */     AbstractDungeon.effectsQueue.add(new RefreshEnergyEffect());
/*  82 */     energyVfxTimer = 2.0F;
/*  83 */     fontScale = 2.0F;
/*     */   }
/*     */   
/*     */   public static void addEnergy(int e) {
/*  87 */     totalCount += e;
/*     */     
/*     */ 
/*  90 */     if (totalCount >= 9) {
/*  91 */       UnlockTracker.unlockAchievement("ADRENALINE");
/*     */     }
/*     */     
/*  94 */     if (totalCount > 999) {
/*  95 */       totalCount = 999;
/*     */     }
/*     */     
/*  98 */     AbstractDungeon.effectsQueue.add(new RefreshEnergyEffect());
/*  99 */     fontScale = 2.0F;
/* 100 */     energyVfxTimer = 2.0F;
/*     */   }
/*     */   
/*     */   public static void useEnergy(int e) {
/* 104 */     totalCount -= e;
/*     */     
/* 106 */     if (totalCount < 0) {
/* 107 */       totalCount = 0;
/*     */     }
/*     */     
/* 110 */     if (e != 0) {
/* 111 */       fontScale = 2.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/* 116 */     updateRed();
/* 117 */     updateVfx();
/*     */     
/* 119 */     if (fontScale != 1.0F) {
/* 120 */       fontScale = com.megacrit.cardcrawl.helpers.MathHelper.scaleLerpSnap(fontScale, 1.0F);
/*     */     }
/*     */     
/* 123 */     this.tipHitbox.update();
/* 124 */     if ((this.tipHitbox.hovered) && (!AbstractDungeon.isScreenUp)) {
/* 125 */       AbstractDungeon.overlayMenu.hoveredTip = true;
/*     */     }
/*     */     
/* 128 */     if (Settings.isDebug) {
/* 129 */       if (InputHelper.scrolledDown) {
/* 130 */         addEnergy(1);
/* 131 */       } else if ((InputHelper.scrolledUp) && (totalCount > 0)) {
/* 132 */         useEnergy(1);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateRed() {
/* 138 */     if (totalCount == 0) {
/* 139 */       this.angle5 += Gdx.graphics.getDeltaTime() * -5.0F;
/* 140 */       this.angle4 += Gdx.graphics.getDeltaTime() * 5.0F;
/* 141 */       this.angle3 += Gdx.graphics.getDeltaTime() * -8.0F;
/* 142 */       this.angle2 += Gdx.graphics.getDeltaTime() * 8.0F;
/* 143 */       this.angle1 += Gdx.graphics.getDeltaTime() * 72.0F;
/*     */     } else {
/* 145 */       this.angle5 += Gdx.graphics.getDeltaTime() * -20.0F;
/* 146 */       this.angle4 += Gdx.graphics.getDeltaTime() * 20.0F;
/* 147 */       this.angle3 += Gdx.graphics.getDeltaTime() * -40.0F;
/* 148 */       this.angle2 += Gdx.graphics.getDeltaTime() * 40.0F;
/* 149 */       this.angle1 += Gdx.graphics.getDeltaTime() * 360.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateVfx() {
/* 154 */     if (energyVfxTimer != 0.0F) {
/* 155 */       this.energyVfxColor.a = Interpolation.exp10In.apply(0.5F, 0.0F, 1.0F - energyVfxTimer / 2.0F);
/* 156 */       this.energyVfxAngle += Gdx.graphics.getDeltaTime() * -30.0F;
/* 157 */       this.energyVfxScale = (Settings.scale * Interpolation.exp10In.apply(1.0F, 0.1F, 1.0F - energyVfxTimer / 2.0F));
/*     */       
/*     */ 
/*     */ 
/* 161 */       energyVfxTimer -= Gdx.graphics.getDeltaTime();
/* 162 */       if (energyVfxTimer < 0.0F) {
/* 163 */         energyVfxTimer = 0.0F;
/* 164 */         this.energyVfxColor.a = 0.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 171 */     if (!Settings.hideLowerElements) {
/* 172 */       this.tipHitbox.move(this.current_x, this.current_y);
/*     */       
/*     */ 
/* 175 */       renderOrb(sb);
/* 176 */       renderVfx(sb);
/*     */       
/*     */ 
/*     */ 
/* 180 */       String energyMsg = totalCount + "/" + AbstractDungeon.player.energy.energy;
/*     */       
/* 182 */       switch (AbstractDungeon.player.chosenClass) {
/*     */       case IRONCLAD: 
/* 184 */         FontHelper.energyNumFontRed.getData().setScale(fontScale);
/* 185 */         FontHelper.renderFontCentered(sb, FontHelper.energyNumFontRed, energyMsg, this.current_x, this.current_y, ENERGY_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 192 */         break;
/*     */       case THE_SILENT: 
/* 194 */         FontHelper.energyNumFontGreen.getData().setScale(fontScale);
/* 195 */         FontHelper.renderFontCentered(sb, FontHelper.energyNumFontGreen, energyMsg, this.current_x, this.current_y, ENERGY_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 202 */         break;
/*     */       case DEFECT: 
/* 204 */         FontHelper.energyNumFontBlue.getData().setScale(fontScale);
/* 205 */         FontHelper.renderFontCentered(sb, FontHelper.energyNumFontBlue, energyMsg, this.current_x, this.current_y, ENERGY_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 212 */         break;
/*     */       default: 
/* 214 */         FontHelper.energyNumFontRed.getData().setScale(fontScale);
/* 215 */         FontHelper.renderFontCentered(sb, FontHelper.energyNumFontRed, energyMsg, this.current_x, this.current_y, ENERGY_TEXT_COLOR);
/*     */       }
/*     */       
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 225 */       this.tipHitbox.render(sb);
/* 226 */       if ((this.tipHitbox.hovered) && (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && (!AbstractDungeon.isScreenUp))
/*     */       {
/* 228 */         com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(50.0F * Settings.scale, 380.0F * Settings.scale, LABEL[0], MSG[0]);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderOrb(SpriteBatch sb) {
/* 234 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 236 */       if (totalCount == 0) {
/* 237 */         renderRedOrbDisabled(sb);
/*     */       } else {
/* 239 */         renderRedOrb(sb);
/*     */       }
/* 241 */       break;
/*     */     case THE_SILENT: 
/* 243 */       if (totalCount == 0) {
/* 244 */         renderGreenOrbDisabled(sb);
/*     */       } else {
/* 246 */         renderGreenOrb(sb);
/*     */       }
/* 248 */       break;
/*     */     case DEFECT: 
/* 250 */       if (totalCount == 0) {
/* 251 */         renderBlueOrbDisabled(sb);
/*     */       } else {
/* 253 */         renderBlueOrb(sb);
/*     */       }
/* 255 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void renderGreenOrb(SpriteBatch sb)
/*     */   {
/* 262 */     sb.setColor(Color.WHITE);
/*     */     
/* 264 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER2, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 282 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER3, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 300 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER4, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 318 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER5, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 336 */     sb.setBlendFunction(770, 1);
/* 337 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 338 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER1, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 355 */     sb.setBlendFunction(770, 771);
/*     */     
/* 357 */     sb.setColor(Color.WHITE);
/*     */     
/* 359 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER6, this.current_x - 128.0F, this.current_y - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 256, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderGreenOrbDisabled(SpriteBatch sb)
/*     */   {
/* 379 */     sb.setColor(Color.WHITE);
/* 380 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER2D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle2, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 398 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER3D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 416 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER4D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 435 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER5D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 453 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER1D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 472 */     sb.draw(ImageMaster.ENERGY_GREEN_LAYER6, this.current_x - 128.0F, this.current_y - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 256, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderRedOrb(SpriteBatch sb)
/*     */   {
/* 492 */     sb.setColor(Color.WHITE);
/* 493 */     sb.draw(ImageMaster.ENERGY_RED_LAYER1, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle1, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 511 */     sb.draw(ImageMaster.ENERGY_RED_LAYER2, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle2, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 529 */     sb.draw(ImageMaster.ENERGY_RED_LAYER3, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 547 */     sb.draw(ImageMaster.ENERGY_RED_LAYER4, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 566 */     sb.draw(ImageMaster.ENERGY_RED_LAYER5, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle5, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 585 */     sb.draw(ImageMaster.ENERGY_RED_LAYER6, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderRedOrbDisabled(SpriteBatch sb)
/*     */   {
/* 605 */     sb.setColor(Color.WHITE);
/* 606 */     sb.draw(ImageMaster.ENERGY_RED_LAYER1D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle1, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 624 */     sb.draw(ImageMaster.ENERGY_RED_LAYER2D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle2, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 642 */     sb.draw(ImageMaster.ENERGY_RED_LAYER3D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 660 */     sb.draw(ImageMaster.ENERGY_RED_LAYER4D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 679 */     sb.draw(ImageMaster.ENERGY_RED_LAYER5D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle5, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 698 */     sb.draw(ImageMaster.ENERGY_RED_LAYER6, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderBlueOrb(SpriteBatch sb)
/*     */   {
/* 718 */     sb.setColor(Color.WHITE);
/* 719 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER1, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 737 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER2, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle2, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 755 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER3, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 773 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER4, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 792 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER5, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle5, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 811 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER6, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderBlueOrbDisabled(SpriteBatch sb)
/*     */   {
/* 831 */     sb.setColor(Color.WHITE);
/* 832 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER1D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 850 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER2D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle2, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 868 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER3D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle3, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 886 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER4D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle4, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 905 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER5D, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, this.angle5, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 924 */     sb.draw(ImageMaster.ENERGY_BLUE_LAYER6, this.current_x - 64.0F, this.current_y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, ORB_IMG_SCALE, ORB_IMG_SCALE, 0.0F, 0, 0, 128, 128, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private float angle4;
/*     */   
/*     */ 
/*     */ 
/*     */   private float angle3;
/*     */   
/*     */ 
/*     */ 
/*     */   private float angle2;
/*     */   
/*     */ 
/*     */ 
/*     */   private float angle1;
/*     */   
/*     */ 
/*     */ 
/*     */   private void renderVfx(SpriteBatch sb)
/*     */   {
/* 949 */     if (energyVfxTimer != 0.0F) {
/* 950 */       sb.setBlendFunction(770, 1);
/* 951 */       sb.setColor(this.energyVfxColor);
/* 952 */       sb.draw(this.gainEnergyImg, this.current_x - 128.0F, this.current_y - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.energyVfxScale, this.energyVfxScale, -this.energyVfxAngle + 50.0F, 0, 0, 256, 256, true, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 969 */       sb.draw(this.gainEnergyImg, this.current_x - 128.0F, this.current_y - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.energyVfxScale, this.energyVfxScale, this.energyVfxAngle, 0, 0, 256, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 986 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */   }
/*     */   
/*     */   public static int getCurrentEnergy() {
/* 991 */     if (AbstractDungeon.player == null) {
/* 992 */       return 0;
/*     */     }
/*     */     
/* 995 */     return totalCount;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\EnergyPanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */