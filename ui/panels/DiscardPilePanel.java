/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.DiscardGlowEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class DiscardPilePanel extends AbstractPanel
/*     */ {
/*  32 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Discard Tip");
/*  33 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  34 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  35 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DiscardPilePanel");
/*  36 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final int RAW_W = 128;
/*  39 */   private float scale = 1.0F;
/*  40 */   private static final float COUNT_CIRCLE_W = 128.0F * Settings.scale;
/*  41 */   private static final float DECK_X = 180.0F * Settings.scale - 64.0F;
/*  42 */   private static final float DECK_Y = 70.0F * Settings.scale - 64.0F;
/*  43 */   private static final float COUNT_X = 134.0F * Settings.scale;
/*  44 */   private static final float COUNT_Y = 48.0F * Settings.scale;
/*  45 */   private static final float COUNT_OFFSET_X = 70.0F * Settings.scale;
/*  46 */   private static final float COUNT_OFFSET_Y = -18.0F * Settings.scale;
/*     */   
/*  48 */   private Color glowColor = Color.WHITE.cpy();
/*  49 */   private float glowAlpha = 0.0F;
/*  50 */   private GlyphLayout gl = new GlyphLayout();
/*  51 */   private BobEffect bob = new BobEffect(1.0F);
/*     */   
/*  53 */   private ArrayList<DiscardGlowEffect> vfxAbove = new ArrayList();
/*  54 */   private ArrayList<DiscardGlowEffect> vfxBelow = new ArrayList();
/*     */   
/*  56 */   private static final float DECK_TIP_X = 1550.0F * Settings.scale;
/*  57 */   private static final float DECK_TIP_Y = 470.0F * Settings.scale;
/*     */   
/*     */ 
/*  60 */   private static final float HITBOX_W = 120.0F * Settings.scale;
/*  61 */   private static final float HITBOX_W2 = 450.0F * Settings.scale;
/*     */   
/*  63 */   private Hitbox hb = new Hitbox(Settings.WIDTH - HITBOX_W, 0.0F, HITBOX_W, HITBOX_W);
/*  64 */   private Hitbox bannerHb = new Hitbox(Settings.WIDTH - HITBOX_W2, 0.0F, HITBOX_W2, HITBOX_W);
/*     */   
/*     */   public DiscardPilePanel() {
/*  67 */     super(Settings.WIDTH - 256.0F * Settings.scale, 0.0F, Settings.WIDTH, -300.0F * Settings.scale, 8.0F * Settings.scale, 0.0F, null, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void updatePositions()
/*     */   {
/*  80 */     super.updatePositions();
/*  81 */     this.bob.update();
/*  82 */     updateVfx();
/*     */     
/*  84 */     if (!this.isHidden) {
/*  85 */       this.hb.update();
/*  86 */       this.bannerHb.update();
/*  87 */       updatePop();
/*     */     }
/*     */     
/*     */ 
/*  91 */     if ((this.hb.hovered) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.DISCARD_VIEW) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.HAND_SELECT)))
/*     */     {
/*  93 */       AbstractDungeon.overlayMenu.hoveredTip = true;
/*  94 */       if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/*  95 */         this.hb.clickStarted = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 100 */     if (((this.hb.clicked) || (InputActionSet.discardPile.isJustPressed()) || (CInputActionSet.discardPile.isJustPressed())) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.DISCARD_VIEW))
/*     */     {
/*     */ 
/* 103 */       this.hb.clicked = false;
/* 104 */       this.hb.hovered = false;
/* 105 */       this.bannerHb.hovered = false;
/* 106 */       CardCrawlGame.sound.play("DECK_CLOSE");
/* 107 */       if (AbstractDungeon.previousScreen == AbstractDungeon.CurrentScreen.DISCARD_VIEW) {
/* 108 */         AbstractDungeon.previousScreen = null;
/*     */       }
/* 110 */       AbstractDungeon.closeCurrentScreen();
/* 111 */       return;
/*     */     }
/*     */     
/*     */ 
/* 115 */     this.glowAlpha += Gdx.graphics.getDeltaTime() * 3.0F;
/* 116 */     if (this.glowAlpha < 0.0F) {
/* 117 */       this.glowAlpha *= -1.0F;
/*     */     }
/* 119 */     float tmp = MathUtils.cos(this.glowAlpha);
/* 120 */     if (tmp < 0.0F) {
/* 121 */       this.glowColor.a = (-tmp / 2.0F);
/*     */     } else {
/* 123 */       this.glowColor.a = (tmp / 2.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 128 */     if (((this.hb.clicked) || (InputActionSet.discardPile.isJustPressed()) || (CInputActionSet.discardPile.isJustPressed())) && (AbstractDungeon.overlayMenu.combatPanelsShown) && 
/* 129 */       (AbstractDungeon.getMonsters() != null) && 
/* 130 */       (!AbstractDungeon.getMonsters().areMonstersDead()) && (!AbstractDungeon.player.isDead))
/*     */     {
/* 132 */       this.hb.clicked = false;
/* 133 */       this.hb.hovered = false;
/* 134 */       this.bannerHb.hovered = false;
/*     */       
/* 136 */       if (AbstractDungeon.isScreenUp) {
/* 137 */         if (AbstractDungeon.previousScreen == null) {
/* 138 */           AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*     */         }
/*     */       } else {
/* 141 */         AbstractDungeon.previousScreen = null;
/*     */       }
/*     */       
/* 144 */       openDiscardPile();
/*     */     }
/*     */   }
/*     */   
/*     */   private void openDiscardPile() {
/* 149 */     AbstractPlayer p = AbstractDungeon.player;
/*     */     
/* 151 */     if (p.discardPile.size() != 0) {
/* 152 */       AbstractDungeon.discardPileViewScreen.open();
/*     */     } else {
/* 154 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.ThoughtBubble(p.dialogX, p.dialogY, 3.0F, TEXT[0], true));
/*     */     }
/*     */     
/* 157 */     this.hb.hovered = false;
/* 158 */     com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateVfx()
/*     */   {
/* 165 */     for (Iterator<DiscardGlowEffect> i = this.vfxAbove.iterator(); i.hasNext();) {
/* 166 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 167 */       e.update();
/* 168 */       if (e.isDone) {
/* 169 */         i.remove();
/*     */       }
/*     */     }
/* 172 */     for (Iterator<DiscardGlowEffect> i = this.vfxBelow.iterator(); i.hasNext();) {
/* 173 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 174 */       e.update();
/* 175 */       if (e.isDone) {
/* 176 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 180 */     if (this.vfxAbove.size() < 9) {
/* 181 */       this.vfxAbove.add(new DiscardGlowEffect(true));
/*     */     }
/* 183 */     if (this.vfxBelow.size() < 9) {
/* 184 */       this.vfxBelow.add(new DiscardGlowEffect(false));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updatePop() {
/* 189 */     if (this.scale != 1.0F) {
/* 190 */       this.scale = MathUtils.lerp(this.scale, 1.0F, Gdx.graphics.getDeltaTime() * 8.0F);
/* 191 */       if (Math.abs(this.scale - 1.0F) < 0.003F) {
/* 192 */         this.scale = 1.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void pop() {
/* 198 */     this.scale = Settings.POP_AMOUNT;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 203 */     if (!Settings.hideLowerElements) {
/* 204 */       renderButton(sb);
/*     */       
/*     */ 
/* 207 */       String msg = Integer.toString(AbstractDungeon.player.discardPile.size());
/* 208 */       this.gl.setText(FontHelper.deckCountFont, msg);
/* 209 */       sb.setColor(Color.WHITE);
/* 210 */       sb.draw(ImageMaster.DECK_COUNT_CIRCLE, this.current_x + COUNT_OFFSET_X, this.current_y + COUNT_OFFSET_Y, COUNT_CIRCLE_W, COUNT_CIRCLE_W);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 217 */       if (Settings.isControllerMode) {
/* 218 */         sb.draw(CInputActionSet.discardPile
/* 219 */           .getKeyImg(), this.current_x - 32.0F + 220.0F * Settings.scale, this.current_y - 32.0F + 40.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 237 */       FontHelper.renderFontCentered(sb, FontHelper.deckCountFont, msg, this.current_x + COUNT_X, this.current_y + COUNT_Y);
/*     */       
/* 239 */       if (!this.isHidden) {
/* 240 */         this.hb.render(sb);
/* 241 */         if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.DISCARD_VIEW) {
/* 242 */           this.bannerHb.render(sb);
/*     */         }
/*     */       }
/*     */       
/* 246 */       if ((!this.isHidden) && (this.hb != null) && (this.hb.hovered) && (!AbstractDungeon.isScreenUp) && 
/* 247 */         (AbstractDungeon.getMonsters() != null) && (!AbstractDungeon.getMonsters().areMonstersDead())) {
/* 248 */         com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(DECK_TIP_X, DECK_TIP_Y, LABEL[0] + " (" + InputActionSet.discardPile
/*     */         
/*     */ 
/* 251 */           .getKeyString() + ")", MSG[0]);
/*     */       }
/*     */       else {
/* 254 */         this.hb.hovered = false;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderButton(SpriteBatch sb) {
/* 260 */     if ((this.hb.hovered) || ((this.bannerHb.hovered) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.DISCARD_VIEW))) {
/* 261 */       this.scale = 1.2F;
/*     */     }
/*     */     
/* 264 */     for (DiscardGlowEffect e : this.vfxBelow) {
/* 265 */       e.render(sb, this.current_x - 1664.0F * Settings.scale, this.current_y + this.bob.y * 0.5F);
/*     */     }
/*     */     
/* 268 */     sb.setColor(Color.WHITE);
/* 269 */     sb.draw(ImageMaster.DISCARD_BTN_BASE, this.current_x + DECK_X, this.current_y + DECK_Y + this.bob.y / 2.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * Settings.scale, this.scale * Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 287 */     for (DiscardGlowEffect e : this.vfxAbove) {
/* 288 */       e.render(sb, this.current_x - 1664.0F * Settings.scale, this.current_y + this.bob.y * 0.5F);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\DiscardPilePanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */