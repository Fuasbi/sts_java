/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.GameDeckGlowEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class DrawPilePanel extends AbstractPanel
/*     */ {
/*  36 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Draw Tip");
/*  37 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  38 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  39 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DrawPilePanel");
/*  40 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final int RAW_W = 128;
/*  43 */   private float scale = 1.0F;
/*  44 */   private static final float COUNT_CIRCLE_W = 128.0F * Settings.scale;
/*  45 */   private static final float DECK_X = 76.0F * Settings.scale - 64.0F;
/*  46 */   private static final float DECK_Y = 74.0F * Settings.scale - 64.0F;
/*  47 */   private static final float COUNT_X = 118.0F * Settings.scale;
/*  48 */   private static final float COUNT_Y = 48.0F * Settings.scale;
/*  49 */   private static final float COUNT_OFFSET_X = 54.0F * Settings.scale;
/*  50 */   private static final float COUNT_OFFSET_Y = -18.0F * Settings.scale;
/*  51 */   private Color glowColor = Color.WHITE.cpy();
/*  52 */   private float glowAlpha = 0.0F;
/*  53 */   private GlyphLayout gl = new GlyphLayout();
/*  54 */   private static final float DECK_TIP_X = 50.0F * Settings.scale;
/*  55 */   private static final float DECK_TIP_Y = 470.0F * Settings.scale;
/*  56 */   private BobEffect bob = new BobEffect(1.0F);
/*  57 */   private ArrayList<GameDeckGlowEffect> vfxBelow = new ArrayList();
/*     */   
/*  59 */   private static final float HITBOX_W = 120.0F * Settings.scale;
/*  60 */   private static final float HITBOX_W2 = 450.0F * Settings.scale;
/*     */   
/*  62 */   private Hitbox hb = new Hitbox(0.0F, 0.0F, HITBOX_W, HITBOX_W);
/*  63 */   private Hitbox bannerHb = new Hitbox(0.0F, 0.0F, HITBOX_W2, HITBOX_W);
/*     */   
/*     */   public DrawPilePanel() {
/*  66 */     super(0.0F, 0.0F, -300.0F * Settings.scale, -300.0F * Settings.scale, null, true);
/*     */   }
/*     */   
/*     */   public void updatePositions()
/*     */   {
/*  71 */     super.updatePositions();
/*  72 */     this.bob.update();
/*  73 */     updateVfx();
/*     */     
/*  75 */     if (!this.isHidden) {
/*  76 */       this.hb.update();
/*  77 */       this.bannerHb.update();
/*  78 */       updatePop();
/*     */     }
/*     */     
/*     */ 
/*  82 */     if ((this.hb.hovered) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GAME_DECK_VIEW) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.HAND_SELECT)))
/*     */     {
/*  84 */       AbstractDungeon.overlayMenu.hoveredTip = true;
/*  85 */       if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/*  86 */         this.hb.clickStarted = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  91 */     if (((this.hb.clicked) || (InputActionSet.drawPile.isJustPressed()) || (CInputActionSet.drawPile.isJustPressed())) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GAME_DECK_VIEW))
/*     */     {
/*  93 */       this.hb.clicked = false;
/*  94 */       this.hb.hovered = false;
/*  95 */       this.bannerHb.hovered = false;
/*  96 */       CardCrawlGame.sound.play("DECK_CLOSE");
/*  97 */       if (AbstractDungeon.previousScreen == AbstractDungeon.CurrentScreen.GAME_DECK_VIEW) {
/*  98 */         AbstractDungeon.previousScreen = null;
/*     */       }
/* 100 */       AbstractDungeon.closeCurrentScreen();
/* 101 */       return;
/*     */     }
/*     */     
/*     */ 
/* 105 */     this.glowAlpha += Gdx.graphics.getDeltaTime() * 3.0F;
/* 106 */     if (this.glowAlpha < 0.0F) {
/* 107 */       this.glowAlpha *= -1.0F;
/*     */     }
/* 109 */     float tmp = MathUtils.cos(this.glowAlpha);
/* 110 */     if (tmp < 0.0F) {
/* 111 */       this.glowColor.a = (-tmp / 2.0F);
/*     */     } else {
/* 113 */       this.glowColor.a = (tmp / 2.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 118 */     if (((this.hb.clicked) || (InputActionSet.drawPile.isJustPressed()) || (CInputActionSet.drawPile.isJustPressed())) && (AbstractDungeon.overlayMenu.combatPanelsShown) && 
/* 119 */       (AbstractDungeon.getMonsters() != null) && 
/* 120 */       (!AbstractDungeon.getMonsters().areMonstersDead()) && (!AbstractDungeon.player.isDead))
/*     */     {
/* 122 */       this.hb.clicked = false;
/* 123 */       this.hb.hovered = false;
/* 124 */       this.bannerHb.hovered = false;
/*     */       
/* 126 */       if (AbstractDungeon.isScreenUp) {
/* 127 */         if (AbstractDungeon.previousScreen == null) {
/* 128 */           AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*     */         }
/*     */       } else {
/* 131 */         AbstractDungeon.previousScreen = null;
/*     */       }
/*     */       
/* 134 */       openDrawPile();
/*     */     }
/*     */   }
/*     */   
/*     */   private void openDrawPile() {
/* 139 */     AbstractPlayer p = AbstractDungeon.player;
/*     */     
/* 141 */     if (!p.drawPile.isEmpty()) {
/* 142 */       AbstractDungeon.gameDeckViewScreen.open();
/*     */     } else {
/* 144 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.ThoughtBubble(p.dialogX, p.dialogY, 3.0F, TEXT[0], true));
/*     */     }
/*     */     
/* 147 */     this.hb.hovered = false;
/* 148 */     com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateVfx()
/*     */   {
/* 155 */     for (Iterator<GameDeckGlowEffect> i = this.vfxBelow.iterator(); i.hasNext();) {
/* 156 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 157 */       e.update();
/* 158 */       if (e.isDone) {
/* 159 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 163 */     if (this.vfxBelow.size() < 25) {
/* 164 */       this.vfxBelow.add(new GameDeckGlowEffect(false));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updatePop() {
/* 169 */     if (this.scale != 1.0F) {
/* 170 */       this.scale = MathUtils.lerp(this.scale, 1.0F, Gdx.graphics.getDeltaTime() * 8.0F);
/* 171 */       if (Math.abs(this.scale - 1.0F) < 0.003F) {
/* 172 */         this.scale = 1.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void pop() {
/* 178 */     this.scale = Settings.POP_AMOUNT;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 183 */     if (!Settings.hideLowerElements) {
/* 184 */       if ((this.hb.hovered) || ((this.bannerHb.hovered) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GAME_DECK_VIEW))) {
/* 185 */         this.scale = 1.2F;
/*     */       }
/*     */       
/* 188 */       for (GameDeckGlowEffect e : this.vfxBelow) {
/* 189 */         e.render(sb, this.current_x, this.current_y + this.bob.y * 0.5F);
/*     */       }
/*     */       
/*     */ 
/* 193 */       sb.setColor(Color.WHITE);
/* 194 */       sb.draw(ImageMaster.DECK_BTN_BASE, this.current_x + DECK_X, this.current_y + DECK_Y + this.bob.y / 2.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * Settings.scale, this.scale * Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 213 */       String msg = Integer.toString(AbstractDungeon.player.drawPile.size());
/* 214 */       this.gl.setText(FontHelper.deckCountFont, msg);
/* 215 */       sb.setColor(Color.WHITE);
/* 216 */       sb.draw(ImageMaster.DECK_COUNT_CIRCLE, this.current_x + COUNT_OFFSET_X, this.current_y + COUNT_OFFSET_Y, COUNT_CIRCLE_W, COUNT_CIRCLE_W);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 224 */       if ((!AbstractDungeon.isScreenUp) && (this.hb.hovered) && (AbstractDungeon.player.hasRelic("Frozen Eye OLD"))) {
/* 225 */         AbstractRelic r = AbstractDungeon.player.getRelic("Frozen Eye OLD");
/* 226 */         if (r.flashTimer == 0.0F) {
/* 227 */           r.flash();
/* 228 */           r.flashTimer = 1.0F;
/*     */         }
/*     */         
/* 231 */         if (AbstractDungeon.player.drawPile.size() != 0) {
/* 232 */           AbstractCard c = AbstractDungeon.player.drawPile.getTopCard();
/* 233 */           c.setAngle(0.0F, true);
/* 234 */           c.lighten(true);
/* 235 */           if (this.hb.justHovered) {
/* 236 */             c.current_x = (170.0F * Settings.scale);
/* 237 */             c.current_y = (210.0F * Settings.scale);
/*     */           } else {
/* 239 */             c.current_x = MathHelper.cardLerpSnap(c.current_x, 170.0F * Settings.scale);
/* 240 */             c.current_y = MathHelper.cardLerpSnap(c.current_y, 300.0F * Settings.scale);
/*     */           }
/* 242 */           c.drawScale = 0.75F;
/* 243 */           c.render(sb);
/*     */         }
/*     */       }
/*     */       
/* 247 */       if (Settings.isControllerMode) {
/* 248 */         sb.draw(CInputActionSet.drawPile
/* 249 */           .getKeyImg(), this.current_x - 32.0F + 30.0F * Settings.scale, this.current_y - 32.0F + 40.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 267 */       FontHelper.renderFontCentered(sb, FontHelper.deckCountFont, msg, this.current_x + COUNT_X, this.current_y + COUNT_Y);
/*     */       
/* 269 */       if (!this.isHidden) {
/* 270 */         this.hb.render(sb);
/* 271 */         if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GAME_DECK_VIEW) {
/* 272 */           this.bannerHb.render(sb);
/*     */         }
/*     */       }
/*     */       
/* 276 */       if ((this.hb.hovered) && 
/* 277 */         (!AbstractDungeon.player.hasRelic("Frozen Eye OLD")) && (!AbstractDungeon.isScreenUp) && 
/* 278 */         (AbstractDungeon.getMonsters() != null) && (!AbstractDungeon.getMonsters().areMonstersDead())) {
/* 279 */         com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(DECK_TIP_X, DECK_TIP_Y, LABEL[0] + " (" + InputActionSet.drawPile
/*     */         
/*     */ 
/* 282 */           .getKeyString() + ")", MSG[0] + AbstractDungeon.player.gameHandSize + MSG[1]);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\DrawPilePanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */