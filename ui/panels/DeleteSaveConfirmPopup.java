/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.SaveSlotScreen.CurrentPopup;
/*     */ import com.megacrit.cardcrawl.vfx.WarningSignEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class DeleteSaveConfirmPopup
/*     */ {
/*  26 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DeletePopup");
/*  27 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  30 */   public boolean shown = false;
/*  31 */   private int slot = -1;
/*     */   
/*     */ 
/*  34 */   private Hitbox yesHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale); private Hitbox noHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*     */   private static final int CONFIRM_W = 360;
/*     */   private static final int CONFIRM_H = 414;
/*     */   private static final int YES_W = 173;
/*     */   private static final int NO_W = 161;
/*  39 */   private static final int BUTTON_H = 74; private Color uiColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  40 */   private Color faderColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*     */   
/*     */ 
/*  43 */   private ArrayList<WarningSignEffect> effects = new ArrayList();
/*     */   
/*     */   public DeleteSaveConfirmPopup() {
/*  46 */     this.yesHb.move(854.0F * Settings.scale, Settings.OPTION_Y - 120.0F * Settings.scale);
/*  47 */     this.noHb.move(1066.0F * Settings.scale, Settings.OPTION_Y - 120.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  51 */     if (!this.shown) {
/*  52 */       this.faderColor.a = MathHelper.fadeLerpSnap(this.faderColor.a, 0.0F);
/*  53 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 0.0F);
/*     */     } else {
/*  55 */       if (this.effects.isEmpty()) {
/*  56 */         this.effects.add(new WarningSignEffect(Settings.WIDTH / 2.0F, Settings.OPTION_Y + 275.0F * Settings.scale));
/*     */       }
/*     */       
/*  59 */       this.faderColor.a = MathHelper.fadeLerpSnap(this.faderColor.a, 0.75F);
/*  60 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 1.0F);
/*  61 */       updateButtons();
/*     */     }
/*     */     
/*     */ 
/*  65 */     for (Iterator<WarningSignEffect> i = this.effects.iterator(); i.hasNext();) {
/*  66 */       WarningSignEffect e = (WarningSignEffect)i.next();
/*  67 */       e.update();
/*  68 */       if (e.isDone) {
/*  69 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateButtons()
/*     */   {
/*  76 */     this.yesHb.update();
/*  77 */     if (this.yesHb.justHovered) {
/*  78 */       CardCrawlGame.sound.play("UI_HOVER");
/*  79 */     } else if ((this.yesHb.hovered) && (InputHelper.justClickedLeft)) {
/*  80 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  81 */       this.yesHb.clickStarted = true;
/*  82 */     } else if (this.yesHb.clicked) {
/*  83 */       deleteSaves();
/*  84 */       this.yesHb.clicked = false;
/*     */     }
/*     */     
/*     */ 
/*  88 */     this.noHb.update();
/*  89 */     if (this.noHb.justHovered) {
/*  90 */       CardCrawlGame.sound.play("UI_HOVER");
/*  91 */     } else if ((this.noHb.hovered) && (InputHelper.justClickedLeft)) {
/*  92 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  93 */       this.noHb.clickStarted = true;
/*  94 */     } else if (this.noHb.clicked) {
/*  95 */       cancel();
/*  96 */       this.noHb.clicked = false;
/*     */     }
/*     */     
/*     */ 
/* 100 */     if (CInputActionSet.proceed.isJustPressed()) {
/* 101 */       CInputActionSet.proceed.unpress();
/* 102 */       deleteSaves();
/* 103 */     } else if ((CInputActionSet.cancel.isJustPressed()) || (InputActionSet.cancel.isJustPressed())) {
/* 104 */       CInputActionSet.cancel.unpress();
/* 105 */       cancel();
/*     */     }
/*     */   }
/*     */   
/*     */   public void open(int slot) {
/* 110 */     this.slot = slot;
/* 111 */     this.shown = true;
/*     */   }
/*     */   
/*     */   private void deleteSaves() {
/* 115 */     com.megacrit.cardcrawl.helpers.SaveHelper.deletePrefs(this.slot);
/* 116 */     CardCrawlGame.mainMenuScreen.saveSlotScreen.deleteSlot(this.slot);
/* 117 */     CardCrawlGame.mainMenuScreen.saveSlotScreen.curPop = SaveSlotScreen.CurrentPopup.NONE;
/* 118 */     CardCrawlGame.playerName = "";
/* 119 */     this.shown = false;
/*     */   }
/*     */   
/*     */   private void cancel() {
/* 123 */     CardCrawlGame.mainMenuScreen.saveSlotScreen.curPop = SaveSlotScreen.CurrentPopup.NONE;
/* 124 */     this.shown = false;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 128 */     sb.setColor(this.faderColor);
/* 129 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 130 */     sb.setColor(this.uiColor);
/*     */     
/* 132 */     renderPopupBg(sb);
/* 133 */     renderText(sb);
/* 134 */     renderButtons(sb);
/* 135 */     renderWarning(sb);
/*     */   }
/*     */   
/*     */   private void renderWarning(SpriteBatch sb) {
/* 139 */     for (WarningSignEffect e : this.effects) {
/* 140 */       e.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderButtons(SpriteBatch sb) {
/* 145 */     sb.setColor(this.uiColor);
/* 146 */     Color textColor = Settings.GOLD_COLOR.cpy();
/* 147 */     textColor.a = this.uiColor.a;
/*     */     
/*     */ 
/* 150 */     if (this.yesHb.clickStarted) {
/* 151 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.9F));
/* 152 */       sb.draw(ImageMaster.OPTION_YES, this.yesHb.cX - 86.5F, this.yesHb.cY - 37.0F, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 169 */       sb.setColor(new Color(this.uiColor));
/*     */     } else {
/* 171 */       sb.draw(ImageMaster.OPTION_YES, this.yesHb.cX - 86.5F, this.yesHb.cY - 37.0F, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 190 */     if ((!this.yesHb.clickStarted) && (this.yesHb.hovered)) {
/* 191 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 192 */       sb.setBlendFunction(770, 1);
/* 193 */       sb.draw(ImageMaster.OPTION_YES, this.yesHb.cX - 86.5F, this.yesHb.cY - 37.0F, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 210 */       sb.setBlendFunction(770, 771);
/* 211 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 214 */     if (this.yesHb.clickStarted) {
/* 215 */       textColor = Color.LIGHT_GRAY.cpy();
/* 216 */     } else if (this.yesHb.hovered) {
/* 217 */       textColor = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 219 */       textColor = Settings.GOLD_COLOR.cpy();
/*     */     }
/*     */     
/* 222 */     textColor.a = this.uiColor.a;
/* 223 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[1], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, textColor, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 233 */     sb.draw(ImageMaster.OPTION_NO, this.noHb.cX - 80.5F, this.noHb.cY - 37.0F, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 251 */     if ((!this.noHb.clickStarted) && (this.noHb.hovered)) {
/* 252 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 253 */       sb.setBlendFunction(770, 1);
/* 254 */       sb.draw(ImageMaster.OPTION_NO, this.noHb.cX - 80.5F, this.noHb.cY - 37.0F, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 271 */       sb.setBlendFunction(770, 771);
/* 272 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 275 */     if (this.noHb.clickStarted) {
/* 276 */       textColor = Color.LIGHT_GRAY.cpy();
/* 277 */     } else if (this.noHb.hovered) {
/* 278 */       textColor = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 280 */       textColor = Settings.GOLD_COLOR.cpy();
/*     */     }
/*     */     
/* 283 */     textColor.a = this.uiColor.a;
/* 284 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[2], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, textColor, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 293 */     if (Settings.isControllerMode) {
/* 294 */       sb.draw(CInputActionSet.proceed
/* 295 */         .getKeyImg(), 770.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 311 */       sb.draw(CInputActionSet.cancel
/* 312 */         .getKeyImg(), 1150.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 330 */     if (this.shown) {
/* 331 */       this.yesHb.render(sb);
/* 332 */       this.noHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderText(SpriteBatch sb) {
/* 337 */     Color c = Settings.GOLD_COLOR.cpy();
/* 338 */     c.a = this.uiColor.a;
/* 339 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[0], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 150.0F * Settings.scale, c);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 347 */     c = Settings.CREAM_COLOR.cpy();
/* 348 */     c.a = this.uiColor.a;
/* 349 */     FontHelper.renderWrappedText(sb, FontHelper.tipBodyFont, TEXT[3], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 20.0F * Settings.scale, 240.0F * Settings.scale, c, 1.0F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderPopupBg(SpriteBatch sb)
/*     */   {
/* 361 */     sb.draw(ImageMaster.OPTION_CONFIRM, Settings.WIDTH / 2.0F - 180.0F, Settings.OPTION_Y - 207.0F, 180.0F, 207.0F, 360.0F, 414.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 360, 414, false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\DeleteSaveConfirmPopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */