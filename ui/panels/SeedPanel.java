/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.utils.Clipboard;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ 
/*     */ public class SeedPanel
/*     */ {
/*  26 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SeedPanel");
/*  27 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public String title;
/*  30 */   public static String textField = "";
/*  31 */   public Hitbox yesHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale); public Hitbox noHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*     */   private static final int CONFIRM_W = 360;
/*     */   private static final int CONFIRM_H = 414;
/*     */   private static final int YES_W = 173;
/*     */   private static final int NO_W = 161;
/*  36 */   private static final int BUTTON_H = 74; private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  37 */   private Color uiColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  38 */   private float animTimer = 0.0F; private float waitTimer = 0.0F;
/*     */   private static final float ANIM_TIME = 0.25F;
/*  40 */   public boolean shown = false;
/*     */   private static final float SCREEN_DARKNESS = 0.8F;
/*  42 */   public MainMenuScreen.CurScreen sourceScreen = null;
/*     */   
/*     */   public void update() {
/*  45 */     if ((Gdx.input.isKeyPressed(67)) && (!textField.equals("")) && (this.waitTimer <= 0.0F)) {
/*  46 */       textField = textField.substring(0, textField.length() - 1);
/*  47 */       this.waitTimer = 0.09F;
/*     */     }
/*  49 */     if (this.waitTimer > 0.0F) {
/*  50 */       this.waitTimer -= Gdx.graphics.getDeltaTime();
/*     */     }
/*  52 */     if (this.shown) {
/*  53 */       if (this.animTimer != 0.0F) {
/*  54 */         this.animTimer -= Gdx.graphics.getDeltaTime();
/*  55 */         if (this.animTimer < 0.0F) {
/*  56 */           this.animTimer = 0.0F;
/*     */         }
/*  58 */         this.screenColor.a = Interpolation.fade.apply(0.8F, 0.0F, this.animTimer * 1.0F / 0.25F);
/*  59 */         this.uiColor.a = Interpolation.fade.apply(1.0F, 0.0F, this.animTimer * 1.0F / 0.25F);
/*     */       } else {
/*  61 */         updateYes();
/*  62 */         updateNo();
/*     */         
/*     */ 
/*  65 */         if (com.megacrit.cardcrawl.helpers.input.InputActionSet.confirm.isJustPressed()) {
/*  66 */           confirm();
/*  67 */         } else if (InputHelper.pressedEscape) {
/*  68 */           InputHelper.pressedEscape = false;
/*  69 */           cancel();
/*  70 */         } else if (InputHelper.isPasteJustPressed()) {
/*  71 */           Clipboard clipBoard = Gdx.app.getClipboard();
/*  72 */           String pasteText = clipBoard.getContents();
/*  73 */           String seedText = SeedHelper.sterilizeString(pasteText);
/*  74 */           if (!seedText.isEmpty()) {
/*  75 */             textField = seedText;
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*  80 */     else if (this.animTimer != 0.0F) {
/*  81 */       this.animTimer -= Gdx.graphics.getDeltaTime();
/*  82 */       if (this.animTimer < 0.0F) {
/*  83 */         this.animTimer = 0.0F;
/*     */       }
/*  85 */       this.screenColor.a = Interpolation.fade.apply(0.0F, 0.8F, this.animTimer * 1.0F / 0.25F);
/*  86 */       this.uiColor.a = Interpolation.fade.apply(0.0F, 1.0F, this.animTimer * 1.0F / 0.25F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateYes()
/*     */   {
/*  92 */     this.yesHb.update();
/*  93 */     if (this.yesHb.justHovered) {
/*  94 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/*  97 */     if ((InputHelper.justClickedLeft) && (this.yesHb.hovered)) {
/*  98 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  99 */       this.yesHb.clickStarted = true;
/*     */     }
/*     */     
/* 102 */     if (CInputActionSet.proceed.isJustPressed()) {
/* 103 */       CInputActionSet.proceed.unpress();
/* 104 */       this.yesHb.clicked = true;
/*     */     }
/*     */     
/* 107 */     if (this.yesHb.clicked) {
/* 108 */       this.yesHb.clicked = false;
/* 109 */       confirm();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateNo() {
/* 114 */     this.noHb.update();
/* 115 */     if (this.noHb.justHovered) {
/* 116 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/* 119 */     if ((InputHelper.justClickedLeft) && (this.noHb.hovered)) {
/* 120 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 121 */       this.noHb.clickStarted = true;
/*     */     }
/*     */     
/* 124 */     if (CInputActionSet.cancel.isJustPressed()) {
/* 125 */       this.noHb.clicked = true;
/*     */     }
/*     */     
/* 128 */     if (this.noHb.clicked) {
/* 129 */       this.noHb.clicked = false;
/* 130 */       cancel();
/*     */     }
/*     */   }
/*     */   
/*     */   public void show() {
/* 135 */     Gdx.input.setInputProcessor(new com.megacrit.cardcrawl.helpers.TypeHelper(true));
/* 136 */     this.yesHb.move(860.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/* 137 */     this.noHb.move(1062.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/* 138 */     this.shown = true;
/* 139 */     this.animTimer = 0.25F;
/* 140 */     textField = SeedHelper.getUserFacingSeedString();
/*     */   }
/*     */   
/*     */   public void show(MainMenuScreen.CurScreen sourceScreen) {
/* 144 */     show();
/* 145 */     this.sourceScreen = sourceScreen;
/*     */   }
/*     */   
/*     */   public void confirm() {
/* 149 */     textField = textField.trim();
/*     */     try {
/* 151 */       SeedHelper.setSeed(textField);
/*     */     }
/*     */     catch (NumberFormatException e) {
/* 154 */       Settings.seed = Long.valueOf(Long.MAX_VALUE);
/*     */     }
/* 156 */     close();
/*     */   }
/*     */   
/*     */   public void cancel() {
/* 160 */     close();
/*     */   }
/*     */   
/*     */   public void close() {
/* 164 */     this.yesHb.move(-1000.0F, -1000.0F);
/* 165 */     this.noHb.move(-1000.0F, -1000.0F);
/* 166 */     this.shown = false;
/* 167 */     this.animTimer = 0.25F;
/* 168 */     Gdx.input.setInputProcessor(new com.megacrit.cardcrawl.helpers.input.ScrollInputProcessor());
/*     */     
/* 170 */     if (this.sourceScreen == null) {
/* 171 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.CHAR_SELECT;
/*     */     } else {
/* 173 */       CardCrawlGame.mainMenuScreen.screen = this.sourceScreen;
/* 174 */       this.sourceScreen = null;
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean isFull() {
/* 179 */     return textField.length() >= SeedHelper.SEED_DEFAULT_LENGTH;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 183 */     sb.setColor(this.screenColor);
/* 184 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 186 */     sb.setColor(this.uiColor);
/* 187 */     sb.draw(ImageMaster.OPTION_CONFIRM, Settings.WIDTH / 2.0F - 180.0F, Settings.OPTION_Y - 207.0F, 180.0F, 207.0F, 360.0F, 414.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 360, 414, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 206 */     sb.draw(ImageMaster.RENAME_BOX, Settings.WIDTH / 2.0F - 160.0F, Settings.OPTION_Y - 160.0F, 160.0F, 160.0F, 320.0F, 320.0F, Settings.scale * 1.1F, Settings.scale * 1.1F, 0.0F, 0, 0, 320, 320, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 224 */     FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_small_N, textField, Settings.WIDTH / 2.0F - 120.0F * Settings.scale, Settings.OPTION_Y + 4.0F * Settings.scale, 100000.0F, 0.0F, this.uiColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 235 */     if (!isFull()) {
/* 236 */       float tmpAlpha = (com.badlogic.gdx.math.MathUtils.cosDeg((float)(System.currentTimeMillis() / 3L % 360L)) + 1.25F) / 3.0F * this.uiColor.a;
/* 237 */       FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_small_N, "_", Settings.WIDTH / 2.0F - 122.0F * Settings.scale + 
/*     */       
/*     */ 
/*     */ 
/* 241 */         FontHelper.getSmartWidth(FontHelper.cardTitleFont_small_N, textField, 1000000.0F, 0.0F), Settings.OPTION_Y + 4.0F * Settings.scale, 100000.0F, 0.0F, new Color(1.0F, 1.0F, 1.0F, tmpAlpha));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 252 */     Color c = Settings.GOLD_COLOR.cpy();
/* 253 */     c.a = this.uiColor.a;
/* 254 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[1], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 126.0F * Settings.scale, c);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 263 */     if (this.yesHb.clickStarted) {
/* 264 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.9F));
/* 265 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 282 */       sb.setColor(new Color(this.uiColor));
/*     */     } else {
/* 284 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 303 */     if ((!this.yesHb.clickStarted) && (this.yesHb.hovered)) {
/* 304 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 305 */       sb.setBlendFunction(770, 1);
/* 306 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 323 */       sb.setBlendFunction(770, 771);
/* 324 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 327 */     if (this.yesHb.clickStarted) {
/* 328 */       c = Color.LIGHT_GRAY.cpy();
/* 329 */     } else if (this.yesHb.hovered) {
/* 330 */       c = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 332 */       c = Settings.GOLD_COLOR.cpy();
/*     */     }
/* 334 */     c.a = this.uiColor.a;
/* 335 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 345 */     sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 363 */     if ((!this.noHb.clickStarted) && (this.noHb.hovered)) {
/* 364 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 365 */       sb.setBlendFunction(770, 1);
/* 366 */       sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 383 */       sb.setBlendFunction(770, 771);
/* 384 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 387 */     if (this.noHb.clickStarted) {
/* 388 */       c = Color.LIGHT_GRAY.cpy();
/* 389 */     } else if (this.noHb.hovered) {
/* 390 */       c = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 392 */       c = Settings.GOLD_COLOR.cpy();
/*     */     }
/* 394 */     c.a = this.uiColor.a;
/* 395 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 404 */     if (this.shown) {
/* 405 */       if (Settings.isControllerMode) {
/* 406 */         sb.draw(CInputActionSet.proceed
/* 407 */           .getKeyImg(), 770.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 423 */         sb.draw(CInputActionSet.cancel
/* 424 */           .getKeyImg(), 1150.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 441 */       this.yesHb.render(sb);
/* 442 */       this.noHb.render(sb);
/*     */       
/* 444 */       FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT[4], Settings.WIDTH / 2.0F, 100.0F * Settings.scale, new Color(1.0F, 0.3F, 0.3F, c.a));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\SeedPanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */