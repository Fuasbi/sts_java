/*    */ package com.megacrit.cardcrawl.ui.panels;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ 
/*    */ public class BottomBgPanel
/*    */ {
/* 14 */   private float normal_y = 72.0F * Settings.scale; private float hide_y = 0.0F; private float overlay_y = Settings.HEIGHT * 0.5F;
/* 15 */   public boolean doneAnimating = true;
/*    */   private static final float SNAP_THRESHOLD = 0.3F;
/*    */   
/* 18 */   public BottomBgPanel() { this.current_y = this.normal_y;
/* 19 */     this.target_y = this.current_y;
/*    */   }
/*    */   
/*    */   public void changeMode(Mode mode) {
/* 23 */     switch (mode) {
/*    */     case NORMAL: 
/* 25 */       this.target_y = this.normal_y;
/* 26 */       this.doneAnimating = false;
/* 27 */       break;
/*    */     case OVERLAY: 
/* 29 */       this.target_y = this.overlay_y;
/* 30 */       this.doneAnimating = false;
/* 31 */       break;
/*    */     case HIDDEN: 
/* 33 */       this.target_y = this.hide_y;
/* 34 */       this.doneAnimating = false;
/* 35 */       break;
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   public void updatePositions()
/*    */   {
/* 42 */     if (this.current_y != this.target_y) {
/* 43 */       this.current_y = MathUtils.lerp(this.current_y, this.target_y, Gdx.graphics.getDeltaTime() * 7.0F);
/* 44 */       if (Math.abs(this.current_y - this.target_y) < 0.3F) {
/* 45 */         this.current_y = this.target_y;
/* 46 */         this.doneAnimating = true;
/*    */       } else {
/* 48 */         this.doneAnimating = false;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   private static final float LERP_SPEED = 7.0F;
/*    */   
/*    */   private float current_y;
/*    */   private float target_y;
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 60 */     sb.setColor(new Color(0.2F, 0.3F, 0.34F, 1.0F));
/* 61 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.8F));
/* 62 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, this.current_y);
/*    */   }
/*    */   
/*    */   public static enum Mode {
/* 66 */     NORMAL,  OVERLAY,  HIDDEN;
/*    */     
/*    */     private Mode() {}
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\BottomBgPanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */