/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.ScrollInputProcessor;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.SaveSlotScreen.CurrentPopup;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class RenamePopup
/*     */ {
/*  29 */   private static final Logger logger = LogManager.getLogger(RenamePopup.class.getName());
/*     */   
/*     */ 
/*  32 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("RenamePanel");
/*  33 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  36 */   private int slot = 0;
/*  37 */   private boolean newSave = false; private boolean shown = false;
/*  38 */   public static String textField = "";
/*     */   
/*     */ 
/*  41 */   public Hitbox yesHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale); public Hitbox noHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*     */   private static final int CONFIRM_W = 360;
/*     */   private static final int CONFIRM_H = 414;
/*     */   private static final int YES_W = 173;
/*     */   private static final int NO_W = 161;
/*  46 */   private static final int BUTTON_H = 74; private Color faderColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  47 */   private Color uiColor = new Color(1.0F, 0.965F, 0.886F, 0.0F);
/*  48 */   private float waitTimer = 0.0F;
/*     */   
/*     */   public RenamePopup() {
/*  51 */     this.yesHb.move(854.0F * Settings.scale, Settings.OPTION_Y - 120.0F * Settings.scale);
/*  52 */     this.noHb.move(1066.0F * Settings.scale, Settings.OPTION_Y - 120.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  56 */     if ((Gdx.input.isKeyPressed(67)) && (!textField.equals("")) && (this.waitTimer <= 0.0F)) {
/*  57 */       textField = textField.substring(0, textField.length() - 1);
/*  58 */       this.waitTimer = 0.09F;
/*     */     }
/*     */     
/*  61 */     if (this.waitTimer > 0.0F) {
/*  62 */       this.waitTimer -= Gdx.graphics.getDeltaTime();
/*     */     }
/*     */     
/*  65 */     if (this.shown) {
/*  66 */       this.faderColor.a = MathHelper.fadeLerpSnap(this.faderColor.a, 0.75F);
/*  67 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 1.0F);
/*  68 */       updateButtons();
/*     */       
/*     */ 
/*  71 */       if (Gdx.input.isKeyJustPressed(66)) {
/*  72 */         confirm();
/*  73 */       } else if (InputHelper.pressedEscape) {
/*  74 */         InputHelper.pressedEscape = false;
/*  75 */         cancel();
/*     */       }
/*     */     } else {
/*  78 */       this.faderColor.a = MathHelper.fadeLerpSnap(this.faderColor.a, 0.0F);
/*  79 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateButtons()
/*     */   {
/*  85 */     this.yesHb.update();
/*  86 */     if (this.yesHb.justHovered) {
/*  87 */       CardCrawlGame.sound.play("UI_HOVER");
/*  88 */     } else if ((this.yesHb.hovered) && (InputHelper.justClickedLeft)) {
/*  89 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  90 */       this.yesHb.clickStarted = true;
/*  91 */     } else if (this.yesHb.clicked) {
/*  92 */       confirm();
/*  93 */       this.yesHb.clicked = false;
/*     */     }
/*     */     
/*     */ 
/*  97 */     this.noHb.update();
/*  98 */     if (this.noHb.justHovered) {
/*  99 */       CardCrawlGame.sound.play("UI_HOVER");
/* 100 */     } else if ((this.noHb.hovered) && (InputHelper.justClickedLeft)) {
/* 101 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 102 */       this.noHb.clickStarted = true;
/* 103 */     } else if (this.noHb.clicked) {
/* 104 */       cancel();
/* 105 */       this.noHb.clicked = false;
/*     */     }
/*     */     
/*     */ 
/* 109 */     if (CInputActionSet.proceed.isJustPressed()) {
/* 110 */       CInputActionSet.proceed.unpress();
/* 111 */       confirm();
/* 112 */     } else if ((CInputActionSet.cancel.isJustPressed()) || (com.megacrit.cardcrawl.helpers.input.InputActionSet.cancel.isJustPressed())) {
/* 113 */       CInputActionSet.cancel.unpress();
/* 114 */       cancel();
/*     */     }
/*     */   }
/*     */   
/*     */   public void confirm() {
/* 119 */     textField = textField.trim();
/* 120 */     if (textField.equals("")) {
/* 121 */       return;
/*     */     }
/*     */     
/* 124 */     CardCrawlGame.mainMenuScreen.saveSlotScreen.curPop = SaveSlotScreen.CurrentPopup.NONE;
/* 125 */     this.shown = false;
/* 126 */     Gdx.input.setInputProcessor(new ScrollInputProcessor());
/*     */     
/*     */ 
/* 129 */     if (this.newSave) {
/* 130 */       boolean saveSlotPrefSave = false;
/*     */       
/*     */ 
/* 133 */       logger.info("UPDATING DEFAULT SLOT: ", Integer.valueOf(this.slot));
/* 134 */       CardCrawlGame.saveSlotPref.putInteger("DEFAULT_SLOT", this.slot);
/* 135 */       saveSlotPrefSave = true;
/* 136 */       CardCrawlGame.reloadPrefs();
/*     */       
/*     */ 
/* 139 */       if (!CardCrawlGame.saveSlotPref.getString(SaveHelper.slotName("PROFILE_NAME", this.slot), "").equals(textField))
/*     */       {
/* 141 */         logger.info("NAME CHANGE IN SLOT " + this.slot + ": " + textField);
/* 142 */         CardCrawlGame.saveSlotPref.putString(SaveHelper.slotName("PROFILE_NAME", this.slot), textField);
/* 143 */         saveSlotPrefSave = true;
/*     */       }
/*     */       
/* 146 */       if (saveSlotPrefSave) {
/* 147 */         CardCrawlGame.saveSlotPref.flush();
/*     */       }
/*     */       
/*     */ 
/* 151 */       if (CardCrawlGame.playerPref.getString("alias", "").equals("")) {
/* 152 */         CardCrawlGame.playerPref.putString("alias", CardCrawlGame.generateRandomAlias());
/*     */       }
/*     */       
/* 155 */       CardCrawlGame.alias = CardCrawlGame.playerPref.getString("alias", "");
/* 156 */       CardCrawlGame.playerPref.putString("name", textField);
/* 157 */       CardCrawlGame.playerPref.flush();
/* 158 */       CardCrawlGame.playerName = textField;
/*     */ 
/*     */ 
/*     */     }
/* 162 */     else if (!CardCrawlGame.saveSlotPref.getString(SaveHelper.slotName("PROFILE_NAME", this.slot), "").equals(textField))
/*     */     {
/* 164 */       logger.info("RENAMING " + this.slot + ": " + textField);
/* 165 */       CardCrawlGame.saveSlotPref.putString(SaveHelper.slotName("PROFILE_NAME", this.slot), textField);
/* 166 */       CardCrawlGame.saveSlotPref.flush();
/* 167 */       CardCrawlGame.mainMenuScreen.saveSlotScreen.rename(this.slot, textField);
/* 168 */       CardCrawlGame.playerName = textField;
/*     */     }
/*     */   }
/*     */   
/*     */   public void cancel()
/*     */   {
/* 174 */     CardCrawlGame.mainMenuScreen.saveSlotScreen.curPop = SaveSlotScreen.CurrentPopup.NONE;
/* 175 */     this.shown = false;
/* 176 */     Gdx.input.setInputProcessor(new ScrollInputProcessor());
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 180 */     sb.setColor(this.faderColor);
/* 181 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 183 */     renderPopupBg(sb);
/* 184 */     renderTextbox(sb);
/* 185 */     renderHeader(sb);
/* 186 */     renderButtons(sb);
/*     */   }
/*     */   
/*     */   private void renderHeader(SpriteBatch sb) {
/* 190 */     Color c = Settings.GOLD_COLOR.cpy();
/* 191 */     c.a = this.uiColor.a;
/* 192 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[1], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 150.0F * Settings.scale, c);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderPopupBg(SpriteBatch sb)
/*     */   {
/* 203 */     sb.setColor(this.uiColor);
/* 204 */     sb.draw(ImageMaster.OPTION_CONFIRM, Settings.WIDTH / 2.0F - 180.0F, Settings.OPTION_Y - 207.0F, 180.0F, 207.0F, 360.0F, 414.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 360, 414, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderTextbox(SpriteBatch sb)
/*     */   {
/* 225 */     sb.draw(ImageMaster.RENAME_BOX, Settings.WIDTH / 2.0F - 160.0F, Settings.OPTION_Y + 20.0F * Settings.scale - 160.0F, 160.0F, 160.0F, 320.0F, 320.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 320, 320, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 244 */     FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_small_N, textField, Settings.WIDTH / 2.0F - 120.0F * Settings.scale, Settings.OPTION_Y + 24.0F * Settings.scale, 100000.0F, 0.0F, this.uiColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 255 */     float tmpAlpha = (com.badlogic.gdx.math.MathUtils.cosDeg((float)(System.currentTimeMillis() / 3L % 360L)) + 1.25F) / 3.0F * this.uiColor.a;
/* 256 */     FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_small_N, "_", Settings.WIDTH / 2.0F - 122.0F * Settings.scale + 
/*     */     
/*     */ 
/*     */ 
/* 260 */       FontHelper.getSmartWidth(FontHelper.cardTitleFont_small_N, textField, 1000000.0F, 0.0F), Settings.OPTION_Y + 24.0F * Settings.scale, 100000.0F, 0.0F, new Color(1.0F, 1.0F, 1.0F, tmpAlpha));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderButtons(SpriteBatch sb)
/*     */   {
/* 272 */     sb.setColor(this.uiColor);
/* 273 */     Color c = Settings.GOLD_COLOR.cpy();
/* 274 */     c.a = this.uiColor.a;
/*     */     
/*     */ 
/* 277 */     if (this.yesHb.clickStarted) {
/* 278 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.9F));
/* 279 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 296 */       sb.setColor(new Color(this.uiColor));
/*     */     } else {
/* 298 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 317 */     if ((!this.yesHb.clickStarted) && (this.yesHb.hovered)) {
/* 318 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 319 */       sb.setBlendFunction(770, 1);
/* 320 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 337 */       sb.setBlendFunction(770, 771);
/* 338 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 341 */     if ((this.yesHb.clickStarted) || (textField.trim().equals(""))) {
/* 342 */       c = Color.LIGHT_GRAY.cpy();
/* 343 */     } else if (this.yesHb.hovered) {
/* 344 */       c = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 346 */       c = Settings.GOLD_COLOR.cpy();
/*     */     }
/* 348 */     c.a = this.uiColor.a;
/* 349 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 359 */     sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 377 */     if ((!this.noHb.clickStarted) && (this.noHb.hovered)) {
/* 378 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 379 */       sb.setBlendFunction(770, 1);
/* 380 */       sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 397 */       sb.setBlendFunction(770, 771);
/* 398 */       sb.setColor(this.uiColor);
/*     */     }
/*     */     
/* 401 */     if (this.noHb.clickStarted) {
/* 402 */       c = Color.LIGHT_GRAY.cpy();
/* 403 */     } else if (this.noHb.hovered) {
/* 404 */       c = Settings.CREAM_COLOR.cpy();
/*     */     } else {
/* 406 */       c = Settings.GOLD_COLOR.cpy();
/*     */     }
/*     */     
/* 409 */     c.a = this.uiColor.a;
/* 410 */     FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 419 */     if (Settings.isControllerMode) {
/* 420 */       sb.draw(CInputActionSet.proceed
/* 421 */         .getKeyImg(), 770.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 437 */       sb.draw(CInputActionSet.cancel
/* 438 */         .getKeyImg(), 1150.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 456 */     if (this.shown) {
/* 457 */       this.yesHb.render(sb);
/* 458 */       this.noHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void open(int slot, boolean isNewSave) {
/* 463 */     Gdx.input.setInputProcessor(new com.megacrit.cardcrawl.helpers.TypeHelper());
/* 464 */     this.slot = slot;
/* 465 */     this.newSave = isNewSave;
/* 466 */     this.shown = true;
/* 467 */     textField = "";
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\RenamePopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */