/*     */ package com.megacrit.cardcrawl.ui.panels;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.potions.FruitJuice;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class PotionPopUp
/*     */ {
/*  30 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Potion Panel Tip");
/*     */   
/*  32 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  33 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  34 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("PotionPopUp");
/*  35 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private int slot;
/*     */   
/*     */   private AbstractPotion potion;
/*  40 */   public boolean isHidden = true; public boolean targetMode = false;
/*     */   private static final int RAW_W = 282;
/*     */   private static final int RAW_H = 286;
/*     */   private static final float HB_W = 184.0F;
/*     */   private static final float HB_H = 52.0F;
/*     */   private Hitbox hbTop;
/*     */   private Hitbox hbBot;
/*     */   private float x;
/*     */   private float y;
/*     */   private static final int SEGMENTS = 20;
/*  50 */   private Vector2[] points = new Vector2[20];
/*     */   private Vector2 controlPoint;
/*  52 */   private float arrowScale; private float arrowScaleTimer = 0.0F;
/*     */   private static final float ARROW_TARGET_SCALE = 1.2F;
/*     */   private static final int TARGET_ARROW_W = 256;
/*  55 */   private AbstractMonster hoveredMonster = null;
/*  56 */   private boolean autoTargetFirst = false;
/*     */   
/*     */ 
/*  59 */   private static final String THROW_LABEL = TEXT[0];
/*  60 */   private static final String DRINK_LABEL = TEXT[1];
/*  61 */   private static final String DISCARD_LABEL = TEXT[2];
/*     */   
/*     */   public PotionPopUp() {
/*  64 */     this.hbTop = new Hitbox(184.0F, 52.0F);
/*  65 */     this.hbBot = new Hitbox(184.0F, 52.0F);
/*     */     
/*  67 */     for (int i = 0; i < this.points.length; i++) {
/*  68 */       this.points[i] = new Vector2();
/*     */     }
/*     */   }
/*     */   
/*     */   public void open(int slot, AbstractPotion potion) {
/*  73 */     AbstractDungeon.topPanel.selectPotionMode = false;
/*  74 */     this.slot = slot;
/*  75 */     this.potion = potion;
/*  76 */     this.x = potion.posX;
/*  77 */     this.y = (Settings.HEIGHT - 186.0F * Settings.scale);
/*  78 */     this.isHidden = false;
/*     */     
/*  80 */     this.hbTop.move(this.x, this.y + 44.0F * Settings.scale);
/*  81 */     this.hbBot.move(this.x, this.y - 14.0F * Settings.scale);
/*     */     
/*  83 */     this.hbTop.clickStarted = false;
/*  84 */     this.hbBot.clickStarted = false;
/*  85 */     this.hbTop.clicked = false;
/*  86 */     this.hbBot.clicked = false;
/*     */   }
/*     */   
/*     */   public void close()
/*     */   {
/*  91 */     this.isHidden = true;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  96 */     if (!this.isHidden) {
/*  97 */       updateControllerInput();
/*  98 */       this.hbTop.update();
/*  99 */       this.hbBot.update();
/* 100 */       updateInput();
/* 101 */     } else if (this.targetMode) {
/* 102 */       updateControllerTargetInput();
/* 103 */       updateTargetMode();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerTargetInput() {
/* 108 */     if (!Settings.isControllerMode) {
/* 109 */       return;
/*     */     }
/*     */     
/* 112 */     int offsetEnemyIndex = 0;
/*     */     
/* 114 */     if (this.autoTargetFirst) {
/* 115 */       this.autoTargetFirst = false;
/* 116 */       offsetEnemyIndex++;
/*     */     }
/*     */     
/* 119 */     if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 120 */       offsetEnemyIndex--;
/*     */     }
/* 122 */     if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 123 */       offsetEnemyIndex++;
/*     */     }
/*     */     
/* 126 */     if (offsetEnemyIndex != 0)
/*     */     {
/*     */ 
/*     */ 
/* 130 */       ArrayList<AbstractMonster> prefiltered = AbstractDungeon.getCurrRoom().monsters.monsters;
/*     */       
/* 132 */       ArrayList<AbstractMonster> sortedMonsters = new ArrayList(AbstractDungeon.getCurrRoom().monsters.monsters);
/*     */       
/* 134 */       for (AbstractMonster mons : prefiltered) {
/* 135 */         if (mons.isDying) {
/* 136 */           sortedMonsters.remove(mons);
/*     */         }
/*     */       }
/*     */       
/* 140 */       sortedMonsters.sort(AbstractMonster.sortByHitbox);
/*     */       
/* 142 */       if (sortedMonsters.isEmpty()) {
/* 143 */         return;
/*     */       }
/*     */       
/*     */ 
/* 147 */       for (AbstractMonster m : sortedMonsters) {
/* 148 */         if (m.hb.hovered) {
/* 149 */           this.hoveredMonster = m;
/* 150 */           break;
/*     */         }
/*     */       }
/*     */       AbstractMonster newTarget;
/*     */       AbstractMonster newTarget;
/* 155 */       if (this.hoveredMonster == null) {
/*     */         AbstractMonster newTarget;
/* 157 */         if (offsetEnemyIndex == 1)
/*     */         {
/* 159 */           newTarget = (AbstractMonster)sortedMonsters.get(0);
/*     */         }
/*     */         else {
/* 162 */           newTarget = (AbstractMonster)sortedMonsters.get(sortedMonsters.size() - 1);
/*     */         }
/*     */       }
/*     */       else {
/* 166 */         int currentTargetIndex = sortedMonsters.indexOf(this.hoveredMonster);
/* 167 */         int newTargetIndex = currentTargetIndex + offsetEnemyIndex;
/* 168 */         newTargetIndex = (newTargetIndex + sortedMonsters.size()) % sortedMonsters.size();
/* 169 */         newTarget = (AbstractMonster)sortedMonsters.get(newTargetIndex);
/*     */       }
/*     */       
/* 172 */       if (newTarget != null) {
/* 173 */         Hitbox target = newTarget.hb;
/* 174 */         Gdx.input.setCursorPosition((int)target.cX, Settings.HEIGHT - (int)target.cY);
/* 175 */         this.hoveredMonster = newTarget;
/*     */       }
/*     */       
/* 178 */       if (this.hoveredMonster.halfDead) {
/* 179 */         this.hoveredMonster = null;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 185 */     if (!Settings.isControllerMode) {
/* 186 */       return;
/*     */     }
/*     */     
/* 189 */     if (CInputActionSet.cancel.isJustPressed()) {
/* 190 */       CInputActionSet.cancel.unpress();
/* 191 */       close();
/* 192 */       return;
/*     */     }
/*     */     
/* 195 */     if ((!this.hbTop.hovered) && (!this.hbBot.hovered)) {
/* 196 */       if (this.potion.canUse()) {
/* 197 */         Gdx.input.setCursorPosition((int)this.hbTop.cX, Settings.HEIGHT - (int)this.hbTop.cY);
/*     */       } else {
/* 199 */         Gdx.input.setCursorPosition((int)this.hbBot.cX, Settings.HEIGHT - (int)this.hbBot.cY);
/*     */       }
/* 201 */     } else if (this.hbTop.hovered) {
/* 202 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.down.isJustPressed()) || 
/* 203 */         (CInputActionSet.altUp.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 204 */         Gdx.input.setCursorPosition((int)this.hbBot.cX, Settings.HEIGHT - (int)this.hbBot.cY);
/*     */       }
/* 206 */     } else if ((this.hbBot.hovered) && 
/* 207 */       (this.potion.canUse()) && ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.down.isJustPressed()) || 
/* 208 */       (CInputActionSet.altUp.isJustPressed()) || (CInputActionSet.altDown.isJustPressed()))) {
/* 209 */       Gdx.input.setCursorPosition((int)this.hbTop.cX, Settings.HEIGHT - (int)this.hbTop.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateTargetMode()
/*     */   {
/* 215 */     if ((InputHelper.justClickedRight) || (AbstractDungeon.isScreenUp) || (InputHelper.mY > Settings.HEIGHT - 80.0F * Settings.scale) || (AbstractDungeon.player.hoveredCard != null) || (InputHelper.mY < 140.0F * Settings.scale) || 
/*     */     
/* 217 */       (CInputActionSet.cancel.isJustPressed())) {
/* 218 */       CInputActionSet.cancel.unpress();
/* 219 */       this.targetMode = false;
/* 220 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*     */     }
/*     */     
/* 223 */     this.hoveredMonster = null;
/* 224 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 225 */       if ((m.hb.hovered) && (!m.isDying)) {
/* 226 */         this.hoveredMonster = m;
/* 227 */         break;
/*     */       }
/*     */     }
/*     */     
/* 231 */     if ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) {
/* 232 */       InputHelper.justClickedLeft = false;
/* 233 */       CInputActionSet.select.unpress();
/* 234 */       if (this.hoveredMonster != null) {
/* 235 */         CardCrawlGame.metricData.potions_floor_usage.add(Integer.valueOf(AbstractDungeon.floorNum));
/* 236 */         this.potion.use(this.hoveredMonster);
/* 237 */         for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 238 */           r.onUsePotion();
/*     */         }
/* 240 */         AbstractDungeon.topPanel.destroyPotion(this.slot);
/* 241 */         this.targetMode = false;
/* 242 */         com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateInput()
/*     */   {
/* 250 */     if (InputHelper.justClickedLeft) {
/* 251 */       InputHelper.justClickedLeft = false;
/* 252 */       if (this.hbTop.hovered) {
/* 253 */         this.hbTop.clickStarted = true;
/* 254 */       } else if (this.hbBot.hovered) {
/* 255 */         this.hbBot.clickStarted = true;
/*     */       } else {
/* 257 */         close();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 262 */     if (((this.hbTop.clicked) || ((this.hbTop.hovered) && (CInputActionSet.select.isJustPressed()))) && ((!AbstractDungeon.isScreenUp) || ((this.potion instanceof FruitJuice))))
/*     */     {
/* 264 */       CInputActionSet.select.unpress();
/* 265 */       this.hbTop.clicked = false;
/* 266 */       if (this.potion.canUse()) {
/* 267 */         if (!this.potion.targetRequired) {
/* 268 */           CardCrawlGame.metricData.potions_floor_usage.add(Integer.valueOf(AbstractDungeon.floorNum));
/* 269 */           this.potion.use(null);
/* 270 */           for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 271 */             r.onUsePotion();
/*     */           }
/* 273 */           CardCrawlGame.sound.play("POTION_1");
/* 274 */           AbstractDungeon.topPanel.destroyPotion(this.slot);
/*     */         } else {
/* 276 */           this.targetMode = true;
/* 277 */           com.megacrit.cardcrawl.core.GameCursor.hidden = true;
/* 278 */           this.autoTargetFirst = true;
/*     */         }
/* 280 */         close();
/*     */       }
/*     */     }
/* 283 */     else if (((this.hbBot.clicked) || ((this.hbBot.hovered) && (CInputActionSet.select.isJustPressed()))) && 
/* 284 */       (this.potion.canDiscard())) {
/* 285 */       CInputActionSet.select.unpress();
/* 286 */       this.hbBot.clicked = false;
/* 287 */       CardCrawlGame.sound.play("POTION_DROP_2");
/* 288 */       AbstractDungeon.topPanel.destroyPotion(this.slot);
/* 289 */       this.slot = -1;
/* 290 */       this.potion = null;
/* 291 */       close();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 297 */     if (!this.isHidden) {
/* 298 */       sb.setColor(Color.WHITE);
/*     */       
/*     */ 
/* 301 */       sb.draw(ImageMaster.POTION_UI_SHADOW, this.x - 141.0F, this.y - 143.0F, 141.0F, 143.0F, 282.0F, 286.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 282, 286, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 319 */       sb.draw(ImageMaster.POTION_UI_BG, this.x - 141.0F, this.y - 143.0F, 141.0F, 143.0F, 282.0F, 286.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 282, 286, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 337 */       if (this.hbTop.hovered) {
/* 338 */         sb.draw(ImageMaster.POTION_UI_TOP, this.x - 141.0F, this.y - 143.0F, 141.0F, 143.0F, 282.0F, 286.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 282, 286, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/* 355 */       else if (this.hbBot.hovered) {
/* 356 */         sb.draw(ImageMaster.POTION_UI_MID, this.x - 141.0F, this.y - 143.0F, 141.0F, 143.0F, 282.0F, 286.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 282, 286, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 375 */       sb.draw(ImageMaster.POTION_UI_OVERLAY, this.x - 141.0F, this.y - 143.0F, 141.0F, 143.0F, 282.0F, 286.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 282, 286, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 393 */       Color c = Settings.CREAM_COLOR;
/* 394 */       if ((!this.potion.canUse()) || (AbstractDungeon.isScreenUp)) {
/* 395 */         c = Color.GRAY;
/*     */       }
/*     */       
/* 398 */       if ((this.potion instanceof FruitJuice)) {
/* 399 */         if (AbstractDungeon.getCurrRoom().event != null) {
/* 400 */           if (!(AbstractDungeon.getCurrRoom().event instanceof com.megacrit.cardcrawl.events.shrines.WeMeetAgain)) {
/* 401 */             c = Settings.CREAM_COLOR;
/*     */           }
/*     */         } else {
/* 404 */           c = Settings.CREAM_COLOR;
/*     */         }
/*     */       }
/*     */       
/* 408 */       String label = DRINK_LABEL;
/* 409 */       if (this.potion.isThrown) {
/* 410 */         label = THROW_LABEL;
/*     */       }
/*     */       
/* 413 */       FontHelper.renderFontCenteredWidth(sb, FontHelper.topPanelInfoFont, label, this.x, this.y + 55.0F * Settings.scale, c);
/*     */       
/* 415 */       FontHelper.renderFontCenteredWidth(sb, FontHelper.topPanelInfoFont, DISCARD_LABEL, this.x, this.y - 2.0F * Settings.scale, Color.SALMON);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 423 */       this.hbTop.render(sb);
/* 424 */       this.hbBot.render(sb);
/*     */       
/* 426 */       if (this.hbTop.hovered) {
/* 427 */         if (this.potion.isThrown) {
/* 428 */           TipHelper.renderGenericTip(this.x + 124.0F * Settings.scale, this.y + 50.0F * Settings.scale, LABEL[0], MSG[0]);
/*     */         } else {
/* 430 */           TipHelper.renderGenericTip(this.x + 124.0F * Settings.scale, this.y + 50.0F * Settings.scale, LABEL[1], MSG[1]);
/*     */         }
/* 432 */       } else if (this.hbBot.hovered) {
/* 433 */         TipHelper.renderGenericTip(this.x + 124.0F * Settings.scale, this.y + 50.0F * Settings.scale, LABEL[2], MSG[2]);
/*     */       }
/*     */     }
/*     */     
/* 437 */     if (this.targetMode) {
/* 438 */       if (this.hoveredMonster != null) {
/* 439 */         this.hoveredMonster.renderReticle(sb);
/*     */       }
/* 441 */       renderTargetingUi(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderTargetingUi(SpriteBatch sb) {
/* 446 */     float x = InputHelper.mX;float y = InputHelper.mY;
/* 447 */     this.controlPoint = new Vector2(this.potion.posX - (x - this.potion.posX) / 4.0F, y + (y - this.potion.posY - 40.0F * Settings.scale) / 2.0F);
/*     */     
/*     */ 
/*     */ 
/* 451 */     if (this.hoveredMonster == null) {
/* 452 */       this.arrowScale = Settings.scale;
/* 453 */       this.arrowScaleTimer = 0.0F;
/* 454 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 1.0F));
/*     */     } else {
/* 456 */       this.arrowScaleTimer += Gdx.graphics.getDeltaTime();
/* 457 */       if (this.arrowScaleTimer > 1.0F) {
/* 458 */         this.arrowScaleTimer = 1.0F;
/*     */       }
/*     */       
/* 461 */       this.arrowScale = com.badlogic.gdx.math.Interpolation.elasticOut.apply(Settings.scale, Settings.scale * 1.2F, this.arrowScaleTimer);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 466 */       sb.setColor(new Color(1.0F, 0.2F, 0.3F, 1.0F));
/*     */     }
/*     */     
/*     */ 
/* 470 */     Vector2 tmp = new Vector2(this.controlPoint.x - x, this.controlPoint.y - y);
/* 471 */     tmp.nor();
/*     */     
/* 473 */     drawCurvedLine(sb, new Vector2(this.potion.posX, this.potion.posY - 40.0F * Settings.scale), new Vector2(x, y), this.controlPoint);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 478 */     sb.draw(ImageMaster.TARGET_UI_ARROW, x - 128.0F, y - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.arrowScale, this.arrowScale, tmp
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 488 */       .angle() + 90.0F, 0, 0, 256, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void drawCurvedLine(SpriteBatch sb, Vector2 start, Vector2 end, Vector2 control)
/*     */   {
/* 498 */     float radius = 7.0F * Settings.scale;
/*     */     
/* 500 */     for (int i = 0; i < this.points.length - 1; i++) {
/* 501 */       this.points[i] = ((Vector2)com.badlogic.gdx.math.Bezier.quadratic(this.points[i], i / 20.0F, start, control, end, new Vector2()));
/* 502 */       radius += 0.4F * Settings.scale;
/*     */       
/*     */       float angle;
/*     */       
/*     */       float angle;
/* 507 */       if (i != 0) {
/* 508 */         Vector2 tmp = new Vector2(this.points[(i - 1)].x - this.points[i].x, this.points[(i - 1)].y - this.points[i].y);
/* 509 */         angle = tmp.nor().angle() + 90.0F;
/*     */       } else {
/* 511 */         Vector2 tmp = new Vector2(this.controlPoint.x - this.points[i].x, this.controlPoint.y - this.points[i].y);
/* 512 */         angle = tmp.nor().angle() + 270.0F;
/*     */       }
/*     */       
/* 515 */       sb.draw(ImageMaster.TARGET_UI_CIRCLE, this.points[i].x - 64.0F, this.points[i].y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, radius / 18.0F, radius / 18.0F, angle, 0, 0, 128, 128, false, false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\panels\PotionPopUp.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */