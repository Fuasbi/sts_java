/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.DrawMaster;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.unlock.AbstractUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockCharacterScreen;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DynamicButton
/*     */ {
/*     */   public static final int RAW_W = 512;
/*  23 */   private static final float Y_OFFSET = -100.0F * Settings.scale;
/*  24 */   private static final float BUTTON_W = 240.0F * Settings.scale;
/*  25 */   private static final float BUTTON_H = 160.0F * Settings.scale;
/*     */   private static final float ANIM_TIME = 0.3F;
/*     */   private static final float LERP_SPEED = 9.0F;
/*  28 */   private static final Color TEXT_SHOW_COLOR = new Color(0.9F, 0.9F, 0.9F, 1.0F);
/*  29 */   private static final Color HIGHLIGHT_COLOR = new Color(1.0F, 1.0F, 1.0F, 1.0F);
/*  30 */   private static final Color IDLE_COLOR = new Color(0.7F, 0.7F, 0.7F, 1.0F);
/*  31 */   private static final Color FADE_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*     */   
/*     */   public String label;
/*  34 */   private float animateTimer = 0.0F;
/*     */   public float x;
/*     */   public float y;
/*  37 */   public float targetX; public float targetY; public Hitbox hb; protected TintEffect tint = new TintEffect();
/*  38 */   protected TintEffect textTint = new TintEffect();
/*  39 */   public boolean pressed = false; public boolean isMoving = false; public boolean show = false; public boolean isLarge = false;
/*     */   public int height;
/*     */   public int width;
/*     */   
/*     */   public DynamicButton() {
/*  44 */     this.tint.color.a = 0.0F;
/*  45 */     this.textTint.color.a = 0.0F;
/*  46 */     this.hb = new Hitbox(-10000.0F, -10000.0F, BUTTON_W, BUTTON_H);
/*     */   }
/*     */   
/*     */   public void appear(float x, float y, String label, boolean isLarge) {
/*  50 */     this.x = x;
/*  51 */     this.y = (y + Y_OFFSET);
/*  52 */     this.targetX = x;
/*  53 */     this.targetY = y;
/*  54 */     this.label = label;
/*  55 */     this.pressed = false;
/*  56 */     this.isMoving = true;
/*  57 */     this.show = true;
/*  58 */     this.animateTimer = 0.3F;
/*  59 */     this.tint.changeColor(IDLE_COLOR, 9.0F);
/*  60 */     this.textTint.changeColor(TEXT_SHOW_COLOR, 9.0F);
/*  61 */     this.isLarge = isLarge;
/*     */   }
/*     */   
/*     */   public void hide() {
/*  65 */     this.show = false;
/*  66 */     this.isMoving = false;
/*  67 */     this.tint.changeColor(FADE_COLOR, 9.0F);
/*  68 */     this.textTint.changeColor(FADE_COLOR, 9.0F);
/*     */   }
/*     */   
/*     */   public void update() {
/*  72 */     this.tint.update();
/*  73 */     this.textTint.update();
/*     */     
/*  75 */     if (this.show)
/*     */     {
/*  77 */       if (this.animateTimer != 0.0F) {
/*  78 */         this.animateTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  79 */         if (this.animateTimer < 0.0F) {
/*  80 */           this.animateTimer = 0.0F;
/*  81 */           this.y = this.targetY;
/*  82 */           this.isMoving = false;
/*     */         } else {
/*  84 */           this.y = com.badlogic.gdx.math.Interpolation.fade.apply(this.y, this.targetY, this.animateTimer / 0.3F);
/*     */         }
/*  86 */         this.hb.move(this.x, this.y);
/*     */       }
/*     */       
/*  89 */       this.hb.update();
/*  90 */       if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.hb.hovered)) {
/*  91 */         this.hb.clickStarted = true;
/*  92 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*     */       }
/*  94 */       if (this.hb.justHovered) {
/*  95 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*     */       
/*     */ 
/*  99 */       if ((this.hb.hovered) && (this.animateTimer == 0.0F)) {
/* 100 */         this.tint.changeColor(HIGHLIGHT_COLOR, 18.0F);
/*     */         
/* 102 */         if ((AbstractDungeon.screen != AbstractDungeon.CurrentScreen.DEATH) && (this.hb.clicked) && (!this.pressed)) {
/* 103 */           CardCrawlGame.sound.play("END_TURN");
/* 104 */           this.hb.clicked = false;
/*     */           
/* 106 */           if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.VICTORY)
/*     */           {
/* 108 */             this.pressed = true;
/* 109 */             com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 110 */             hide();
/*     */             
/* 112 */             if ((AbstractDungeon.unlocks.isEmpty()) || (Settings.isDemo)) {
/* 113 */               CardCrawlGame.startOver();
/*     */             } else {
/* 115 */               AbstractDungeon.unlocks.clear();
/* 116 */               if (UnlockTracker.isCharacterLocked("The Silent")) {
/* 117 */                 AbstractDungeon.unlocks.add(new com.megacrit.cardcrawl.unlock.misc.TheSilentUnlock());
/* 118 */                 AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/* 119 */               } else if (UnlockTracker.isCharacterLocked("Defect")) {
/* 120 */                 AbstractDungeon.unlocks.add(new com.megacrit.cardcrawl.unlock.misc.DefectUnlock());
/* 121 */                 AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/*     */               } else {
/* 123 */                 CardCrawlGame.startOver();
/*     */               }
/*     */               
/*     */             }
/*     */           }
/* 128 */           else if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.UNLOCK) {
/* 129 */             UnlockTracker.hardUnlock(AbstractDungeon.unlockScreen.unlock.key);
/* 130 */             this.pressed = true;
/* 131 */             com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 132 */             hide();
/*     */             
/* 134 */             if ((AbstractDungeon.unlocks.isEmpty()) || (Settings.isDemo)) {
/* 135 */               CardCrawlGame.startOver();
/*     */             } else {
/* 137 */               AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/*     */             }
/*     */             
/*     */           }
/* 141 */           else if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD) {
/* 142 */             AbstractDungeon.combatRewardScreen.setupItemReward();
/* 143 */             this.pressed = true;
/*     */           } else {
/* 145 */             AbstractDungeon.closeCurrentScreen();
/* 146 */             this.pressed = true;
/* 147 */             com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 148 */             hide();
/*     */           }
/*     */         }
/*     */       } else {
/* 152 */         this.tint.changeColor(IDLE_COLOR, 9.0F);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 157 */     if (this.textTint.color.a != 0.0F) {
/* 158 */       if (this.hb.clickStarted) {
/* 159 */         DrawMaster.queue(FontHelper.panelEndTurnFont, this.label, this.x, this.y, 650, 1.0F, Color.LIGHT_GRAY);
/*     */       } else {
/* 161 */         DrawMaster.queue(FontHelper.panelEndTurnFont, this.label, this.x, this.y, 650, 1.0F, this.textTint.color);
/*     */       }
/* 163 */       if (this.isLarge) {
/* 164 */         if (this.hb.clickStarted) {
/* 165 */           DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG2, this.x, this.y, 600, Color.LIGHT_GRAY);
/*     */         } else {
/* 167 */           DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG2, this.x, this.y, 600, this.tint.color);
/*     */         }
/*     */       }
/* 170 */       else if (this.hb.clickStarted) {
/* 171 */         DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG, this.x, this.y, 600, Color.GRAY);
/*     */       } else {
/* 173 */         DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG, this.x, this.y, 600, this.tint.color);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 180 */     if ((!this.pressed) && (this.show)) {
/* 181 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\DynamicButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */