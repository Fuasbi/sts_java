/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.events.beyond.MindBloom;
/*     */ import com.megacrit.cardcrawl.events.beyond.MysteriousSphere;
/*     */ import com.megacrit.cardcrawl.events.exordium.Mushrooms;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.neow.NeowRoom;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem.RewardType;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.rooms.TreasureRoomBoss;
/*     */ import com.megacrit.cardcrawl.rooms.VictoryRoom;
/*     */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*     */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*     */ import com.megacrit.cardcrawl.screens.select.BossRelicSelectScreen;
/*     */ import com.megacrit.cardcrawl.ui.FtueTip;
/*     */ import com.megacrit.cardcrawl.ui.FtueTip.TipType;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ProceedButton
/*     */ {
/*  43 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Rewards Tip");
/*  44 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  45 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  46 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("Proceed Button");
/*  47 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  50 */   private static final Color HOVER_BLEND_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.3F);
/*  51 */   private static final float SHOW_X = 1670.0F * Settings.scale; private static final float DRAW_Y = 320.0F * Settings.scale;
/*  52 */   private static final float HIDE_X = SHOW_X + 500.0F * Settings.scale;
/*  53 */   private float current_x = HIDE_X; private float current_y = DRAW_Y;
/*  54 */   private float target_x = this.current_x;
/*  55 */   private float wavyTimer = 0.0F;
/*  56 */   private boolean isHidden = true;
/*  57 */   private String label = TEXT[0];
/*     */   private static final int W = 512;
/*  59 */   private com.badlogic.gdx.graphics.g2d.BitmapFont font = FontHelper.buttonLabelFont;
/*  60 */   private boolean callingBellCheck = true;
/*     */   
/*     */ 
/*  63 */   private static final float HITBOX_W = 280.0F * Settings.scale;
/*  64 */   private static final float HITBOX_H = 156.0F * Settings.scale;
/*     */   
/*     */ 
/*  67 */   private Hitbox hb = new Hitbox(SHOW_X, this.current_y, HITBOX_W, HITBOX_H);
/*  68 */   private static final float CLICKABLE_DIST = 25.0F * Settings.scale;
/*     */   
/*  70 */   public boolean isHovered = false;
/*     */   
/*     */   public ProceedButton() {
/*  73 */     this.hb.move(SHOW_X, this.current_y);
/*     */   }
/*     */   
/*     */   public void setLabel(String newLabel) {
/*  77 */     this.label = newLabel;
/*  78 */     if (FontHelper.getSmartWidth(FontHelper.buttonLabelFont, this.label, 9999.0F, 0.0F) > 160.0F * Settings.scale) {
/*  79 */       this.font = FontHelper.topPanelInfoFont;
/*     */     } else {
/*  81 */       this.font = FontHelper.buttonLabelFont;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/*  86 */     if (!this.isHidden) {
/*  87 */       this.wavyTimer += Gdx.graphics.getDeltaTime() * 3.0F;
/*  88 */       if (this.current_x - SHOW_X < CLICKABLE_DIST) {
/*  89 */         this.hb.update();
/*     */       }
/*     */       
/*  92 */       this.isHovered = this.hb.hovered;
/*     */       
/*  94 */       if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  95 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*  96 */         this.hb.clickStarted = true;
/*     */       }
/*     */       
/*     */ 
/* 100 */       if ((this.hb.justHovered) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD)) {
/* 101 */         for (RewardItem i : AbstractDungeon.combatRewardScreen.rewards) {
/* 102 */           i.flash();
/*     */         }
/*     */       }
/*     */       
/* 106 */       if (((this.hb.hovered) && (this.hb.clicked)) || (CInputActionSet.proceed.isJustPressed())) {
/* 107 */         this.hb.hovered = false;
/* 108 */         this.hb.clicked = false;
/* 109 */         AbstractRoom currentRoom = AbstractDungeon.getCurrRoom();
/*     */         
/* 111 */         if (((currentRoom instanceof MonsterRoomBoss)) && ((CardCrawlGame.dungeon instanceof com.megacrit.cardcrawl.dungeons.TheBeyond)) && (!Settings.isEndless))
/*     */         {
/*     */ 
/* 114 */           if ((AbstractDungeon.ascensionLevel >= 20) && (AbstractDungeon.bossList.size() == 2))
/*     */           {
/* 116 */             AbstractDungeon.bossKey = (String)AbstractDungeon.bossList.get(0);
/* 117 */             CardCrawlGame.music.fadeOutBGM();
/* 118 */             CardCrawlGame.music.fadeOutTempBGM();
/* 119 */             MapRoomNode node = new MapRoomNode(-1, 15);
/* 120 */             node.room = new MonsterRoomBoss();
/* 121 */             AbstractDungeon.nextRoom = node;
/* 122 */             AbstractDungeon.closeCurrentScreen();
/* 123 */             AbstractDungeon.nextRoomTransitionStart();
/* 124 */             hide();
/*     */           }
/*     */           else {
/* 127 */             CardCrawlGame.music.fadeOutBGM();
/* 128 */             CardCrawlGame.music.fadeOutTempBGM();
/* 129 */             MapRoomNode node = new MapRoomNode(-1, 15);
/* 130 */             node.room = new VictoryRoom();
/* 131 */             AbstractDungeon.nextRoom = node;
/* 132 */             AbstractDungeon.closeCurrentScreen();
/* 133 */             AbstractDungeon.nextRoomTransitionStart();
/* 134 */             hide();
/*     */           }
/*     */         }
/*     */         
/* 138 */         if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD) && 
/* 139 */           (!(AbstractDungeon.getCurrRoom() instanceof TreasureRoomBoss))) {
/* 140 */           if ((currentRoom instanceof MonsterRoomBoss)) {
/* 141 */             CardCrawlGame.music.fadeOutTempBGM();
/* 142 */             MapRoomNode node = new MapRoomNode(-1, 15);
/*     */             
/* 144 */             node.room = new TreasureRoomBoss();
/*     */             
/* 146 */             AbstractDungeon.nextRoom = node;
/* 147 */             AbstractDungeon.closeCurrentScreen();
/* 148 */             AbstractDungeon.nextRoomTransitionStart();
/* 149 */             hide();
/* 150 */           } else if ((currentRoom instanceof com.megacrit.cardcrawl.rooms.EventRoom)) {
/* 151 */             if ((!(currentRoom.event instanceof Mushrooms)) && (!(currentRoom.event instanceof com.megacrit.cardcrawl.events.city.MaskedBandits)) && (!(currentRoom.event instanceof com.megacrit.cardcrawl.events.exordium.DeadAdventurer)) && (!(currentRoom.event instanceof com.megacrit.cardcrawl.events.shrines.Lab)) && (!(currentRoom.event instanceof com.megacrit.cardcrawl.events.city.Colosseum)) && (!(currentRoom.event instanceof MysteriousSphere)) && (!(currentRoom.event instanceof MindBloom)))
/*     */             {
/*     */ 
/*     */ 
/*     */ 
/* 156 */               AbstractDungeon.closeCurrentScreen();
/* 157 */               hide();
/*     */             } else {
/* 159 */               AbstractDungeon.closeCurrentScreen();
/* 160 */               AbstractDungeon.dungeonMapScreen.open(false);
/* 161 */               AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/*     */             }
/*     */           }
/*     */           else {
/* 165 */             if ((!((Boolean)TipTracker.tips.get("CARD_REWARD_TIP")).booleanValue()) && 
/* 166 */               (!AbstractDungeon.combatRewardScreen.rewards.isEmpty())) {
/* 167 */               AbstractDungeon.ftue = new FtueTip(LABEL[0], MSG[0], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, FtueTip.TipType.CARD_REWARD);
/*     */               
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 173 */               TipTracker.neverShowAgain("CARD_REWARD_TIP");
/* 174 */               return;
/*     */             }
/*     */             
/*     */ 
/*     */ 
/*     */ 
/* 180 */             int relicCount = 0;
/* 181 */             for (RewardItem i : AbstractDungeon.combatRewardScreen.rewards) {
/* 182 */               if (i.type == RewardItem.RewardType.RELIC) {
/* 183 */                 relicCount++;
/*     */               }
/*     */             }
/*     */             
/* 187 */             if ((relicCount == 3) && (this.callingBellCheck)) {
/* 188 */               this.callingBellCheck = false;
/* 189 */               if (!AbstractDungeon.combatRewardScreen.rewards.isEmpty()) {
/* 190 */                 AbstractDungeon.ftue = new FtueTip(LABEL[0], MSG[0], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, FtueTip.TipType.CARD_REWARD);
/*     */                 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 196 */                 return;
/*     */               }
/*     */             }
/*     */             
/*     */ 
/* 201 */             if ((AbstractDungeon.getCurrRoom() instanceof NeowRoom)) {
/* 202 */               AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/*     */             }
/*     */             
/* 205 */             if (!AbstractDungeon.combatRewardScreen.hasTakenAll) {
/* 206 */               for (RewardItem item : AbstractDungeon.combatRewardScreen.rewards) {
/* 207 */                 if (item.type == RewardItem.RewardType.CARD) {
/* 208 */                   item.recordCardSkipMetrics();
/*     */                 }
/*     */               }
/*     */             }
/* 212 */             AbstractDungeon.closeCurrentScreen();
/* 213 */             AbstractDungeon.dungeonMapScreen.open(false);
/* 214 */             AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/*     */           }
/* 216 */         } else if ((currentRoom instanceof TreasureRoomBoss)) {
/* 217 */           if (Settings.isDemo) {
/* 218 */             MapRoomNode node = new MapRoomNode(-1, 15);
/* 219 */             node.room = new VictoryRoom();
/* 220 */             AbstractDungeon.nextRoom = node;
/* 221 */             AbstractDungeon.closeCurrentScreen();
/* 222 */             AbstractDungeon.nextRoomTransitionStart();
/* 223 */             hide();
/*     */           } else {
/* 225 */             if (!((TreasureRoomBoss)currentRoom).choseRelic) {
/* 226 */               AbstractDungeon.bossRelicScreen.noPick();
/*     */             }
/*     */             
/*     */ 
/* 230 */             int relicCount = 0;
/* 231 */             for (RewardItem i : AbstractDungeon.combatRewardScreen.rewards) {
/* 232 */               if (i.type == RewardItem.RewardType.RELIC) {
/* 233 */                 relicCount++;
/*     */               }
/*     */             }
/*     */             
/* 237 */             if ((relicCount == 3) && (this.callingBellCheck)) {
/* 238 */               this.callingBellCheck = false;
/* 239 */               if (!AbstractDungeon.combatRewardScreen.rewards.isEmpty()) {
/* 240 */                 AbstractDungeon.ftue = new FtueTip(LABEL[0], MSG[0], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, FtueTip.TipType.CARD_REWARD);
/*     */                 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 246 */                 return;
/*     */               }
/*     */             }
/*     */             
/*     */ 
/* 251 */             CardCrawlGame.music.fadeOutBGM();
/* 252 */             CardCrawlGame.music.fadeOutTempBGM();
/* 253 */             AbstractDungeon.fadeOut();
/* 254 */             AbstractDungeon.isDungeonBeaten = true;
/* 255 */             hide();
/*     */           }
/* 257 */         } else if (!(currentRoom instanceof MonsterRoomBoss)) {
/* 258 */           AbstractDungeon.dungeonMapScreen.open(false);
/* 259 */           hide();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 264 */     if (this.current_x != this.target_x) {
/* 265 */       this.current_x = MathUtils.lerp(this.current_x, this.target_x, Gdx.graphics.getDeltaTime() * 9.0F);
/* 266 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/* 267 */         this.current_x = this.target_x;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void hideInstantly() {
/* 273 */     this.current_x = HIDE_X;
/* 274 */     this.target_x = HIDE_X;
/* 275 */     this.isHidden = true;
/*     */   }
/*     */   
/*     */   public void hide() {
/* 279 */     if (!this.isHidden) {
/* 280 */       this.target_x = HIDE_X;
/* 281 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void show() {
/* 286 */     if (this.isHidden) {
/* 287 */       this.target_x = SHOW_X;
/* 288 */       this.isHidden = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 293 */     sb.setColor(Color.WHITE);
/* 294 */     renderShadow(sb);
/*     */     
/*     */ 
/* 297 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD) {
/* 298 */       sb.setColor(new Color(1.0F, 0.9F, 0.2F, MathUtils.cos(this.wavyTimer) / 5.0F + 0.6F));
/*     */     } else {
/* 300 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/*     */     }
/* 302 */     sb.draw(ImageMaster.PROCEED_BUTTON_OUTLINE, this.current_x - 256.0F, this.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale * 1.1F + 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 310 */       MathUtils.cos(this.wavyTimer) / 50.0F, Settings.scale * 1.1F + 
/* 311 */       MathUtils.cos(this.wavyTimer) / 50.0F, 0.0F, 0, 0, 512, 512, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 320 */     sb.setColor(Color.WHITE);
/* 321 */     renderButton(sb);
/*     */     
/* 323 */     if (Settings.isControllerMode) {
/* 324 */       sb.setColor(Color.WHITE);
/* 325 */       sb.draw(CInputActionSet.proceed
/* 326 */         .getKeyImg(), this.current_x - 32.0F - 38.0F * Settings.scale - 
/* 327 */         FontHelper.getSmartWidth(this.font, this.label, 99999.0F, 0.0F) / 2.0F, this.current_y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 344 */     if (this.hb.hovered) {
/* 345 */       sb.setBlendFunction(770, 1);
/* 346 */       sb.setColor(HOVER_BLEND_COLOR);
/* 347 */       renderButton(sb);
/* 348 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/*     */ 
/* 352 */     if ((this.hb.hovered) && (!this.hb.clickStarted)) {
/* 353 */       FontHelper.renderFontCentered(sb, this.font, this.label, this.current_x, this.current_y, Settings.CREAM_COLOR);
/* 354 */     } else if (this.hb.clickStarted) {
/* 355 */       FontHelper.renderFontCentered(sb, this.font, this.label, this.current_x, this.current_y, Color.LIGHT_GRAY);
/*     */     } else {
/* 357 */       FontHelper.renderFontCentered(sb, this.font, this.label, this.current_x, this.current_y, Settings.LIGHT_YELLOW_COLOR);
/*     */     }
/*     */     
/* 360 */     if (!this.isHidden) {
/* 361 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderShadow(SpriteBatch sb) {
/* 366 */     sb.draw(ImageMaster.PROCEED_BUTTON_SHADOW, this.current_x - 256.0F, this.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale * 1.1F + 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 374 */       MathUtils.cos(this.wavyTimer) / 50.0F, Settings.scale * 1.1F + 
/* 375 */       MathUtils.cos(this.wavyTimer) / 50.0F, 0.0F, 0, 0, 512, 512, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderButton(SpriteBatch sb)
/*     */   {
/* 386 */     sb.draw(ImageMaster.PROCEED_BUTTON, this.current_x - 256.0F, this.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale * 1.1F + 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 394 */       MathUtils.cos(this.wavyTimer) / 50.0F, Settings.scale * 1.1F + 
/* 395 */       MathUtils.cos(this.wavyTimer) / 50.0F, 0.0F, 0, 0, 512, 512, false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\ProceedButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */