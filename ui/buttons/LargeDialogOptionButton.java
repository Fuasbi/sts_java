/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent.EventType;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class LargeDialogOptionButton
/*     */ {
/*  20 */   private static final float OPTION_SPACING_Y = -82.0F * Settings.scale;
/*  21 */   private static final float TEXT_ADJUST_X = -400.0F * Settings.scale; private static final float TEXT_ADJUST_Y = 10.0F * Settings.scale;
/*     */   public String msg;
/*  23 */   private Color textColor = new Color(0.0F, 0.0F, 0.0F, 0.0F); private Color boxColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  24 */   private float x; private float y = -9999.0F * Settings.scale;
/*     */   public Hitbox hb;
/*     */   private static final float ANIM_TIME = 0.5F;
/*  27 */   private float animTimer = 0.5F; private float alpha = 0.0F;
/*  28 */   private static final Color TEXT_ACTIVE_COLOR = Color.WHITE.cpy();
/*  29 */   private static final Color TEXT_INACTIVE_COLOR = new Color(0.8F, 0.8F, 0.8F, 1.0F);
/*  30 */   private static final Color TEXT_DISABLED_COLOR = Color.FIREBRICK.cpy();
/*  31 */   private Color boxInactiveColor = new Color(0.2F, 0.25F, 0.25F, 0.0F);
/*  32 */   public boolean pressed = false;
/*     */   public boolean isDisabled;
/*  34 */   public int slot = 0;
/*     */   private AbstractCard cardToPreview;
/*     */   private static final int W = 890;
/*     */   private static final int H = 77;
/*     */   
/*     */   public LargeDialogOptionButton(int slot, String msg, boolean isDisabled, AbstractCard previewCard) {
/*  40 */     switch (AbstractEvent.type) {
/*     */     case TEXT: 
/*  42 */       this.x = (895.0F * Settings.scale);
/*  43 */       break;
/*     */     case IMAGE: 
/*  45 */       this.x = (1260.0F * Settings.scale);
/*  46 */       break;
/*     */     case ROOM: 
/*  48 */       this.x = (620.0F * Settings.scale);
/*     */     }
/*     */     
/*  51 */     this.slot = slot;
/*  52 */     this.isDisabled = isDisabled;
/*  53 */     this.cardToPreview = previewCard;
/*     */     
/*  55 */     if (isDisabled) {
/*  56 */       this.msg = stripColor(msg);
/*     */     } else {
/*  58 */       this.msg = msg;
/*     */     }
/*     */     
/*  61 */     this.hb = new Hitbox(892.0F * Settings.scale, 80.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public LargeDialogOptionButton(int slot, String msg) {
/*  65 */     this(slot, msg, false, null);
/*     */   }
/*     */   
/*     */   public LargeDialogOptionButton(int slot, String msg, boolean isDisabled) {
/*  69 */     this(slot, msg, isDisabled, null);
/*     */   }
/*     */   
/*     */   public LargeDialogOptionButton(int slot, String msg, AbstractCard previewCard) {
/*  73 */     this(slot, msg, false, previewCard);
/*     */   }
/*     */   
/*     */   private String stripColor(String input) {
/*  77 */     input = input.replace("#r", "");
/*  78 */     input = input.replace("#g", "");
/*  79 */     input = input.replace("#b", "");
/*  80 */     input = input.replace("#y", "");
/*  81 */     return input;
/*     */   }
/*     */   
/*     */   public void calculateY(int size) {
/*  85 */     if (AbstractEvent.type != AbstractEvent.EventType.ROOM) {
/*  86 */       this.y = (Settings.OPTION_Y - 424.0F * Settings.scale);
/*  87 */       this.y += this.slot * OPTION_SPACING_Y;
/*  88 */       this.y -= size * OPTION_SPACING_Y;
/*     */     } else {
/*  90 */       this.y = (Settings.OPTION_Y - 500.0F * Settings.scale);
/*  91 */       this.y += this.slot * OPTION_SPACING_Y;
/*  92 */       this.y -= RoomEventDialog.optionList.size() * OPTION_SPACING_Y;
/*     */     }
/*  94 */     this.hb.move(this.x, this.y);
/*     */   }
/*     */   
/*     */   public void update(int size) {
/*  98 */     calculateY(size);
/*  99 */     hoverAndClickLogic();
/* 100 */     updateAnimation();
/*     */   }
/*     */   
/*     */   private void updateAnimation() {
/* 104 */     if (this.animTimer != 0.0F) {
/* 105 */       this.animTimer -= Gdx.graphics.getDeltaTime();
/* 106 */       if (this.animTimer < 0.0F) {
/* 107 */         this.animTimer = 0.0F;
/*     */       }
/* 109 */       this.alpha = com.badlogic.gdx.math.Interpolation.exp5In.apply(0.0F, 1.0F, 1.0F - this.animTimer / 0.5F);
/*     */     }
/* 111 */     this.textColor.a = this.alpha;
/* 112 */     this.boxColor.a = this.alpha;
/*     */   }
/*     */   
/*     */   private void hoverAndClickLogic()
/*     */   {
/* 117 */     this.hb.update();
/* 118 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (!this.isDisabled) && (this.animTimer < 0.1F)) {
/* 119 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 120 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/* 123 */     if ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()) && (!this.isDisabled)) {
/* 124 */       this.hb.clicked = true;
/*     */     }
/*     */     
/* 127 */     if (this.hb.clicked) {
/* 128 */       this.hb.clicked = false;
/* 129 */       this.pressed = true;
/*     */     }
/*     */     
/* 132 */     if (!this.isDisabled) {
/* 133 */       if (this.hb.hovered) {
/* 134 */         this.textColor = TEXT_ACTIVE_COLOR;
/* 135 */         this.boxColor = Color.WHITE.cpy();
/*     */       } else {
/* 137 */         this.textColor = TEXT_INACTIVE_COLOR;
/* 138 */         this.boxColor = new Color(0.4F, 0.4F, 0.4F, 1.0F);
/*     */       }
/*     */     } else {
/* 141 */       this.textColor = TEXT_DISABLED_COLOR;
/* 142 */       this.boxColor = this.boxInactiveColor;
/*     */     }
/*     */     
/*     */ 
/* 146 */     if (this.hb.hovered) {
/* 147 */       if (!this.isDisabled) {
/* 148 */         this.textColor = TEXT_ACTIVE_COLOR;
/*     */       } else {
/* 150 */         this.textColor = Color.GRAY.cpy();
/*     */       }
/*     */     }
/* 153 */     else if (!this.isDisabled) {
/* 154 */       this.textColor = TEXT_ACTIVE_COLOR;
/*     */     } else {
/* 156 */       this.textColor = Color.GRAY.cpy();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 162 */     float scale = Settings.scale;
/* 163 */     if (this.hb.clickStarted) {
/* 164 */       scale *= 0.99F;
/*     */     }
/*     */     
/* 167 */     if (this.isDisabled) {
/* 168 */       sb.setColor(Color.WHITE);
/* 169 */       sb.draw(ImageMaster.EVENT_BUTTON_DISABLED, this.x - 445.0F, this.y - 38.5F, 445.0F, 38.5F, 890.0F, 77.0F, scale, scale, 0.0F, 0, 0, 890, 77, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 187 */       sb.setColor(this.boxColor);
/* 188 */       sb.draw(ImageMaster.EVENT_BUTTON_ENABLED, this.x - 445.0F, this.y - 38.5F, 445.0F, 38.5F, 890.0F, 77.0F, scale, scale, 0.0F, 0, 0, 890, 77, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */       sb.setBlendFunction(770, 1);
/* 206 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.15F));
/* 207 */       sb.draw(ImageMaster.EVENT_BUTTON_ENABLED, this.x - 445.0F, this.y - 38.5F, 445.0F, 38.5F, 890.0F, 77.0F, scale, scale, 0.0F, 0, 0, 890, 77, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 224 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 227 */     if (FontHelper.getSmartWidth(FontHelper.largeDialogOptionFont, this.msg, Settings.WIDTH, 0.0F) > 800.0F * Settings.scale)
/*     */     {
/* 229 */       FontHelper.renderSmartText(sb, FontHelper.smallDialogOptionFont, this.msg, this.x + TEXT_ADJUST_X, this.y + TEXT_ADJUST_Y, Settings.WIDTH, 0.0F, this.textColor);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 239 */       FontHelper.renderSmartText(sb, FontHelper.largeDialogOptionFont, this.msg, this.x + TEXT_ADJUST_X, this.y + TEXT_ADJUST_Y, Settings.WIDTH, 0.0F, this.textColor);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 250 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void renderCardPreview(SpriteBatch sb)
/*     */   {
/* 255 */     if ((this.cardToPreview != null) && (this.hb.hovered)) {
/* 256 */       this.cardToPreview.current_x = (this.x + this.hb.width / 1.75F);
/* 257 */       this.cardToPreview.current_y = this.y;
/* 258 */       this.cardToPreview.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\LargeDialogOptionButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */