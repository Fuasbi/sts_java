/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.DrawMaster;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*     */ 
/*     */ public class RetryButton
/*     */ {
/*     */   public static final int RAW_W = 512;
/*  17 */   private static final float BUTTON_W = 240.0F * Settings.scale;
/*  18 */   private static final float BUTTON_H = 160.0F * Settings.scale;
/*     */   private static final float LERP_SPEED = 9.0F;
/*  20 */   private static final Color TEXT_SHOW_COLOR = new Color(0.9F, 0.9F, 0.9F, 1.0F);
/*  21 */   private static final Color HIGHLIGHT_COLOR = new Color(1.0F, 1.0F, 1.0F, 1.0F);
/*  22 */   private static final Color IDLE_COLOR = new Color(0.7F, 0.7F, 0.7F, 1.0F);
/*  23 */   private static final Color FADE_COLOR = new Color(0.3F, 0.3F, 0.3F, 1.0F);
/*     */   public String label;
/*     */   public float x;
/*     */   public float y;
/*     */   public Hitbox hb;
/*  28 */   protected TintEffect tint = new TintEffect();
/*  29 */   protected TintEffect textTint = new TintEffect();
/*  30 */   public boolean pressed = false; public boolean isMoving = false; public boolean show = false;
/*     */   public int height;
/*     */   public int width;
/*     */   
/*     */   public RetryButton() {
/*  35 */     this.tint.color.a = 0.0F;
/*  36 */     this.textTint.color.a = 0.0F;
/*  37 */     this.hb = new Hitbox(-10000.0F, -10000.0F, BUTTON_W, BUTTON_H);
/*     */   }
/*     */   
/*     */   public void appear(float x, float y, String label) {
/*  41 */     this.x = x;
/*  42 */     this.y = y;
/*  43 */     this.label = label;
/*  44 */     this.pressed = false;
/*  45 */     this.isMoving = true;
/*  46 */     this.show = true;
/*  47 */     this.tint.changeColor(IDLE_COLOR, 9.0F);
/*  48 */     this.textTint.changeColor(TEXT_SHOW_COLOR, 9.0F);
/*     */   }
/*     */   
/*     */   public void hide() {
/*  52 */     this.show = false;
/*  53 */     this.isMoving = false;
/*  54 */     this.tint.changeColor(FADE_COLOR, 9.0F);
/*  55 */     this.textTint.changeColor(FADE_COLOR, 9.0F);
/*     */   }
/*     */   
/*     */   public void update() {
/*  59 */     this.tint.update();
/*  60 */     this.textTint.update();
/*     */     
/*  62 */     if (this.show) {
/*  63 */       this.hb.move(this.x, this.y);
/*  64 */       this.hb.update();
/*     */       
/*  66 */       if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.hb.hovered)) {
/*  67 */         this.hb.clickStarted = true;
/*  68 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*     */       }
/*  70 */       if (this.hb.justHovered) {
/*  71 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*     */       
/*  74 */       if (this.hb.hovered) {
/*  75 */         this.tint.changeColor(HIGHLIGHT_COLOR, 18.0F);
/*     */       } else {
/*  77 */         this.tint.changeColor(IDLE_COLOR, 9.0F);
/*     */       }
/*     */       
/*  80 */       if (this.hb.clicked) {
/*  81 */         this.hb.clicked = false;
/*     */         
/*  83 */         CardCrawlGame.startNewGame(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.chosenClass);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  88 */     if ((this.textTint.color.a != 0.0F) && (this.label != null)) {
/*  89 */       if (this.hb.clickStarted) {
/*  90 */         DrawMaster.queue(FontHelper.panelEndTurnFont, this.label, this.x, this.y, 650, 1.0F, Color.LIGHT_GRAY);
/*     */       } else {
/*  92 */         DrawMaster.queue(FontHelper.panelEndTurnFont, this.label, this.x, this.y, 650, 1.0F, this.textTint.color);
/*     */       }
/*  94 */       if (this.hb.clickStarted) {
/*  95 */         DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG3, this.x, this.y, 600, Color.LIGHT_GRAY);
/*     */       } else {
/*  97 */         DrawMaster.queue(ImageMaster.DYNAMIC_BTN_IMG3, this.x, this.y, 600, this.tint.color);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 103 */     if ((!this.pressed) && (this.show)) {
/* 104 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\RetryButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */