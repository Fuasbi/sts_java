/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SingingBowlButton
/*     */ {
/*  23 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CardRewardScreen");
/*  24 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final int W = 512;
/*     */   private static final int H = 256;
/*  28 */   private static final float TAKE_Y = 220.0F * Settings.scale;
/*  29 */   private static final float SHOW_X = Settings.WIDTH / 2.0F + 165.0F * Settings.scale;
/*  30 */   private static final float HIDE_X = Settings.WIDTH / 2.0F;
/*  31 */   private float current_x = HIDE_X;
/*  32 */   private float target_x = this.current_x;
/*  33 */   private Color textColor = Color.WHITE.cpy();
/*  34 */   private Color btnColor = Color.WHITE.cpy();
/*  35 */   private boolean isHidden = true;
/*  36 */   private RewardItem rItem = null;
/*     */   
/*     */ 
/*  39 */   private static final float HITBOX_W = 260.0F * Settings.scale; private static final float HITBOX_H = 80.0F * Settings.scale;
/*  40 */   public Hitbox hb = new Hitbox(0.0F, 0.0F, HITBOX_W, HITBOX_H);
/*     */   
/*     */   public SingingBowlButton() {
/*  43 */     this.hb.move(Settings.WIDTH / 2.0F, TAKE_Y);
/*     */   }
/*     */   
/*     */   public void update() {
/*  47 */     if (this.isHidden) {
/*  48 */       return;
/*     */     }
/*     */     
/*  51 */     this.hb.update();
/*     */     
/*  53 */     if (this.hb.justHovered) {
/*  54 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*  56 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  57 */       this.hb.clickStarted = true;
/*  58 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*     */     }
/*  60 */     if ((this.hb.clicked) || (com.megacrit.cardcrawl.helpers.input.InputActionSet.cancel.isJustPressed()) || (CInputActionSet.proceed.isJustPressed())) {
/*  61 */       CInputActionSet.proceed.unpress();
/*  62 */       this.hb.clicked = false;
/*  63 */       onClick();
/*  64 */       AbstractDungeon.cardRewardScreen.closeFromBowlButton();
/*  65 */       AbstractDungeon.closeCurrentScreen();
/*  66 */       hide();
/*     */     }
/*     */     
/*  69 */     if (this.current_x != this.target_x) {
/*  70 */       this.current_x = MathUtils.lerp(this.current_x, this.target_x, com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 9.0F);
/*  71 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/*  72 */         this.current_x = this.target_x;
/*  73 */         this.hb.move(this.current_x, TAKE_Y);
/*     */       }
/*     */     }
/*     */     
/*  77 */     this.textColor.a = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.textColor.a, 1.0F);
/*  78 */     this.btnColor.a = this.textColor.a;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void onClick()
/*     */   {
/*  85 */     AbstractDungeon.player.getRelic("Singing Bowl").flash();
/*  86 */     CardCrawlGame.sound.playA("SINGING_BOWL", MathUtils.random(-0.2F, 0.1F));
/*  87 */     AbstractDungeon.player.increaseMaxHp(2, true);
/*  88 */     AbstractDungeon.combatRewardScreen.rewards.remove(this.rItem);
/*     */   }
/*     */   
/*     */   public void hide() {
/*  92 */     if (!this.isHidden) {
/*  93 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void show(RewardItem rItem) {
/*  98 */     this.isHidden = false;
/*  99 */     this.textColor.a = 0.0F;
/* 100 */     this.btnColor.a = 0.0F;
/* 101 */     this.current_x = HIDE_X;
/* 102 */     this.target_x = SHOW_X;
/* 103 */     this.rItem = rItem;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 107 */     if (this.isHidden) {
/* 108 */       return;
/*     */     }
/*     */     
/* 111 */     renderButton(sb);
/* 112 */     FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, TEXT[2], this.current_x, TAKE_Y, this.textColor);
/*     */   }
/*     */   
/*     */   public boolean isHidden() {
/* 116 */     return this.isHidden;
/*     */   }
/*     */   
/*     */   private void renderButton(SpriteBatch sb) {
/* 120 */     sb.setColor(this.btnColor);
/* 121 */     sb.draw(ImageMaster.REWARD_SCREEN_TAKE_BUTTON, this.current_x - 256.0F, TAKE_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 139 */     if ((this.hb.hovered) && (!this.hb.clickStarted)) {
/* 140 */       sb.setBlendFunction(770, 1);
/* 141 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.3F));
/* 142 */       sb.draw(ImageMaster.REWARD_SCREEN_TAKE_BUTTON, this.current_x - 256.0F, TAKE_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 159 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 162 */     if (Settings.isControllerMode) {
/* 163 */       sb.setColor(Color.WHITE);
/* 164 */       sb.draw(CInputActionSet.proceed
/* 165 */         .getKeyImg(), this.current_x - 32.0F - 80.0F * Settings.scale, TAKE_Y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 183 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\SingingBowlButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */