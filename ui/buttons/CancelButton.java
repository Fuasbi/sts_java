/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*     */ import com.megacrit.cardcrawl.rooms.TreasureRoomBoss;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ 
/*     */ public class CancelButton
/*     */ {
/*  25 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("Cancel Button");
/*  26 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final int W = 512;
/*     */   private static final int H = 256;
/*  30 */   private static final Color HOVER_BLEND_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.4F);
/*  31 */   private static final float SHOW_X = 256.0F * Settings.scale; private static final float DRAW_Y = 128.0F * Settings.scale;
/*  32 */   public static final float HIDE_X = SHOW_X - 400.0F * Settings.scale;
/*  33 */   public float current_x = HIDE_X;
/*  34 */   private float target_x = this.current_x;
/*  35 */   public boolean isHidden = true;
/*  36 */   private float glowAlpha = 0.0F;
/*  37 */   private Color glowColor = Settings.GOLD_COLOR.cpy();
/*     */   
/*     */ 
/*  40 */   public String buttonText = "NOT_SET";
/*  41 */   private static final float TEXT_OFFSET_X = -136.0F * Settings.scale;
/*  42 */   private static final float TEXT_OFFSET_Y = 57.0F * Settings.scale;
/*     */   
/*     */ 
/*  45 */   private static final float HITBOX_W = 300.0F * Settings.scale; private static final float HITBOX_H = 100.0F * Settings.scale;
/*  46 */   public Hitbox hb = new Hitbox(0.0F, 0.0F, HITBOX_W, HITBOX_H);
/*     */   
/*     */   public CancelButton() {
/*  49 */     this.hb.move(SHOW_X - 106.0F * Settings.scale, DRAW_Y + 60.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  53 */     if (!this.isHidden) {
/*  54 */       updateGlow();
/*  55 */       this.hb.update();
/*     */       
/*  57 */       if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.hb.hovered)) {
/*  58 */         this.hb.clickStarted = true;
/*  59 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*     */       }
/*  61 */       if (this.hb.justHovered) {
/*  62 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*     */       
/*  65 */       if ((this.hb.clicked) || (((com.megacrit.cardcrawl.helpers.input.InputHelper.pressedEscape) || (CInputActionSet.cancel.isJustPressed())) && (this.current_x != HIDE_X)))
/*     */       {
/*  67 */         com.megacrit.cardcrawl.helpers.input.InputHelper.pressedEscape = false;
/*  68 */         this.hb.clicked = false;
/*  69 */         hide();
/*     */         
/*  71 */         if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/*  72 */           this.hb.clicked = false;
/*     */           
/*  74 */           if (CardCrawlGame.mainMenuScreen.statsScreen.screenUp) {
/*  75 */             CardCrawlGame.mainMenuScreen.statsScreen.hide();
/*  76 */             return; }
/*  77 */           if (CardCrawlGame.mainMenuScreen.isSettingsUp) {
/*  78 */             CardCrawlGame.mainMenuScreen.lighten();
/*  79 */             CardCrawlGame.mainMenuScreen.isSettingsUp = false;
/*  80 */             CardCrawlGame.mainMenuScreen.screen = com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen.MAIN_MENU;
/*     */             
/*  82 */             if (!CardCrawlGame.mainMenuScreen.panelScreen.panels.isEmpty()) {
/*  83 */               CardCrawlGame.mainMenuScreen.panelScreen.open(com.megacrit.cardcrawl.screens.mainMenu.MenuPanelScreen.PanelScreen.SETTINGS);
/*     */             }
/*  85 */             hide();
/*  86 */             return; }
/*  87 */           if (this.buttonText.equals(TEXT[0])) {
/*  88 */             return;
/*     */           }
/*     */         }
/*     */         
/*  92 */         if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) {
/*  93 */           CardCrawlGame.sound.play("MAP_CLOSE", 0.05F);
/*     */         }
/*     */         
/*  96 */         if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GRID) && ((AbstractDungeon.gridSelectScreen.forUpgrade) || (AbstractDungeon.gridSelectScreen.forTransform) || (AbstractDungeon.gridSelectScreen.forPurge)))
/*     */         {
/*     */ 
/*  99 */           if (AbstractDungeon.gridSelectScreen.confirmScreenUp) {
/* 100 */             AbstractDungeon.gridSelectScreen.cancelUpgrade();
/*     */           } else {
/* 102 */             AbstractDungeon.closeCurrentScreen();
/* 103 */             if ((AbstractDungeon.getCurrRoom() instanceof RestRoom)) {
/* 104 */               RestRoom r = (RestRoom)AbstractDungeon.getCurrRoom();
/* 105 */               r.campfireUI.reopen();
/*     */             }
/*     */           }
/*     */         }
/*     */         else {
/* 110 */           if (((AbstractDungeon.getCurrRoom() instanceof TreasureRoomBoss)) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD))
/*     */           {
/* 112 */             TreasureRoomBoss r = (TreasureRoomBoss)AbstractDungeon.getCurrRoom();
/* 113 */             r.chest.close();
/*     */           }
/* 115 */           AbstractDungeon.closeCurrentScreen();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 120 */     if (this.current_x != this.target_x) {
/* 121 */       this.current_x = MathUtils.lerp(this.current_x, this.target_x, Gdx.graphics.getDeltaTime() * 9.0F);
/* 122 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/* 123 */         this.current_x = this.target_x;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateGlow() {
/* 129 */     this.glowAlpha += Gdx.graphics.getDeltaTime() * 3.0F;
/* 130 */     if (this.glowAlpha < 0.0F) {
/* 131 */       this.glowAlpha *= -1.0F;
/*     */     }
/* 133 */     float tmp = MathUtils.cos(this.glowAlpha);
/* 134 */     if (tmp < 0.0F) {
/* 135 */       this.glowColor.a = (-tmp / 2.0F + 0.3F);
/*     */     } else {
/* 137 */       this.glowColor.a = (tmp / 2.0F + 0.3F);
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean hovered() {
/* 142 */     return this.hb.hovered;
/*     */   }
/*     */   
/*     */   public void hide() {
/* 146 */     if (!this.isHidden) {
/* 147 */       this.hb.hovered = false;
/* 148 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 149 */       this.target_x = HIDE_X;
/* 150 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void hideInstantly() {
/* 155 */     if (!this.isHidden) {
/* 156 */       this.hb.hovered = false;
/* 157 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 158 */       this.target_x = HIDE_X;
/* 159 */       this.current_x = this.target_x;
/* 160 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void show(String buttonText) {
/* 165 */     if (this.isHidden) {
/* 166 */       this.glowAlpha = 0.0F;
/* 167 */       this.current_x = HIDE_X;
/* 168 */       this.target_x = SHOW_X;
/* 169 */       this.isHidden = false;
/* 170 */       this.buttonText = buttonText;
/*     */     } else {
/* 172 */       this.current_x = HIDE_X;
/* 173 */       this.buttonText = buttonText;
/*     */     }
/* 175 */     this.hb.hovered = false;
/*     */   }
/*     */   
/*     */   public void showInstantly(String buttonText) {
/* 179 */     this.current_x = SHOW_X;
/* 180 */     this.target_x = SHOW_X;
/* 181 */     this.isHidden = false;
/* 182 */     this.buttonText = buttonText;
/* 183 */     this.hb.hovered = false;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 187 */     sb.setColor(Color.WHITE);
/* 188 */     renderShadow(sb);
/* 189 */     sb.setColor(this.glowColor);
/* 190 */     renderOutline(sb);
/* 191 */     sb.setColor(Color.WHITE);
/* 192 */     renderButton(sb);
/*     */     
/* 194 */     if ((this.hb.hovered) && (!this.hb.clickStarted)) {
/* 195 */       sb.setBlendFunction(770, 1);
/* 196 */       sb.setColor(HOVER_BLEND_COLOR);
/* 197 */       renderButton(sb);
/* 198 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 201 */     Color tmpColor = Settings.LIGHT_YELLOW_COLOR;
/* 202 */     if (this.hb.clickStarted) {
/* 203 */       tmpColor = Color.LIGHT_GRAY;
/*     */     }
/* 205 */     if (Settings.isControllerMode) {
/* 206 */       FontHelper.renderFontLeft(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X - 30.0F * Settings.scale, DRAW_Y + TEXT_OFFSET_Y, tmpColor);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 214 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X, DRAW_Y + TEXT_OFFSET_Y, tmpColor);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 223 */     renderControllerUi(sb);
/*     */     
/* 225 */     if (!this.isHidden) {
/* 226 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderShadow(SpriteBatch sb) {
/* 231 */     sb.draw(ImageMaster.CANCEL_BUTTON_SHADOW, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderOutline(SpriteBatch sb)
/*     */   {
/* 251 */     sb.draw(ImageMaster.CANCEL_BUTTON_OUTLINE, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderButton(SpriteBatch sb)
/*     */   {
/* 271 */     sb.draw(ImageMaster.CANCEL_BUTTON, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderControllerUi(SpriteBatch sb)
/*     */   {
/* 291 */     if (Settings.isControllerMode) {
/* 292 */       sb.setColor(Color.WHITE);
/* 293 */       sb.draw(CInputActionSet.cancel
/* 294 */         .getKeyImg(), this.current_x - 32.0F - 210.0F * Settings.scale, DRAW_Y - 32.0F + 57.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\CancelButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */