/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation.SwingOut;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*     */ 
/*     */ public class DynamicBanner
/*     */ {
/*     */   public static final int RAW_W = 1112;
/*     */   public static final int RAW_H = 238;
/*  14 */   private static final float Y_OFFSET = -50.0F * Settings.scale;
/*     */   private static final float ANIM_TIME = 0.5F;
/*     */   private static final float LERP_SPEED = 9.0F;
/*  17 */   private static final Color TEXT_SHOW_COLOR = new Color(0.9F, 0.9F, 0.9F, 1.0F);
/*  18 */   private static final Color IDLE_COLOR = new Color(0.7F, 0.7F, 0.7F, 1.0F);
/*  19 */   private static final Color FADE_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*     */   
/*     */   private String label;
/*  22 */   private float animateTimer = 0.0F;
/*     */   public float y;
/*  24 */   public float targetY; public float startY; public float scale; private static final float Y = Settings.HEIGHT - 280.0F * Settings.scale;
/*  25 */   protected TintEffect tint = new TintEffect();
/*  26 */   protected TintEffect textTint = new TintEffect();
/*  27 */   public boolean pressed = false; public boolean isMoving = false; public boolean show = false; public boolean isLarge = false;
/*     */   public int height;
/*     */   public int width;
/*     */   
/*     */   public DynamicBanner() {
/*  32 */     this.tint.color.a = 0.0F;
/*  33 */     this.textTint.color.a = 0.0F;
/*     */   }
/*     */   
/*     */   public void appear(String label) {
/*  37 */     appear(Y, label);
/*     */   }
/*     */   
/*     */   public void appearInstantly(String label) {
/*  41 */     appearInstantly(Y, label);
/*     */   }
/*     */   
/*     */   public void appear(float y, String label) {
/*  45 */     this.startY = (y + Y_OFFSET);
/*  46 */     this.y = (y + Y_OFFSET);
/*  47 */     this.targetY = y;
/*  48 */     this.label = label;
/*  49 */     this.scale = 0.2F;
/*  50 */     this.pressed = false;
/*  51 */     this.isMoving = true;
/*  52 */     this.show = true;
/*  53 */     this.animateTimer = 0.5F;
/*  54 */     this.tint.changeColor(IDLE_COLOR, 9.0F);
/*  55 */     this.textTint.changeColor(TEXT_SHOW_COLOR, 9.0F);
/*     */   }
/*     */   
/*     */   public void appearInstantly(float y, String label) {
/*  59 */     this.isMoving = false;
/*  60 */     this.animateTimer = 0.0F;
/*  61 */     this.y = y;
/*  62 */     this.targetY = y;
/*  63 */     this.scale = 1.0F;
/*  64 */     this.label = label;
/*  65 */     this.pressed = false;
/*  66 */     this.show = true;
/*  67 */     this.tint.changeColor(IDLE_COLOR, 9.0F);
/*  68 */     this.textTint.changeColor(TEXT_SHOW_COLOR, 9.0F);
/*     */   }
/*     */   
/*     */   public void hide() {
/*  72 */     this.show = false;
/*  73 */     this.isMoving = false;
/*  74 */     this.tint.changeColor(FADE_COLOR, 18.0F);
/*  75 */     this.textTint.changeColor(FADE_COLOR, 18.0F);
/*     */   }
/*     */   
/*     */   public void update() {
/*  79 */     this.tint.update();
/*  80 */     this.textTint.update();
/*     */     
/*  82 */     if (this.show)
/*     */     {
/*  84 */       this.animateTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  85 */       if (this.animateTimer < 0.0F) {
/*  86 */         this.animateTimer = 0.0F;
/*  87 */         this.isMoving = false;
/*     */       } else {
/*  89 */         this.y = com.badlogic.gdx.math.Interpolation.swingOut.apply(this.startY, this.targetY, (0.5F - this.animateTimer) * 2.0F);
/*  90 */         this.scale = com.badlogic.gdx.math.Interpolation.swingOut.apply(0.0F, 1.0F, (0.5F - this.animateTimer) * 2.0F);
/*  91 */         if (this.scale <= 0.0F) {
/*  92 */           this.scale = 0.01F;
/*     */         }
/*     */       }
/*     */       
/*  96 */       this.tint.changeColor(IDLE_COLOR, 9.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 101 */     if (this.textTint.color.a != 0.0F) {
/* 102 */       sb.setColor(this.tint.color);
/* 103 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.VICTORY_BANNER, Settings.WIDTH / 2.0F - 556.0F, this.y - 119.0F, 556.0F, 119.0F, 1112.0F, 238.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1112, 238, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 121 */       FontHelper.renderFontCentered(sb, FontHelper.bannerFont, this.label, Settings.WIDTH / 2.0F, this.y + 22.0F * Settings.scale, this.textTint.color, this.scale);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\DynamicBanner.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */