/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.ShaderHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.EndTurnGlowEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class EndTurnButton
/*     */ {
/*  32 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("End Turn Tip");
/*  33 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  34 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*  35 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("End Turn Button");
/*  36 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  39 */   private String label = TEXT[0];
/*  40 */   public static final String END_TURN_MSG = TEXT[0];
/*  41 */   public static final String ENEMY_TURN_MSG = TEXT[1];
/*  42 */   private static final Color DISABLED_COLOR = new Color(0.7F, 0.7F, 0.7F, 1.0F);
/*  43 */   private static final float SHOW_X = 1640.0F * Settings.scale; private static final float SHOW_Y = 210.0F * Settings.scale;
/*  44 */   private static final float HIDE_X = SHOW_X + 500.0F * Settings.scale;
/*  45 */   private float current_x = HIDE_X; private float current_y = SHOW_Y;
/*  46 */   private float target_x = this.current_x;
/*  47 */   private boolean isHidden = true;
/*  48 */   public boolean enabled = false;
/*  49 */   private boolean isDisabled = false;
/*     */   
/*     */   private Color textColor;
/*     */   
/*  53 */   private ArrayList<EndTurnGlowEffect> glowList = new ArrayList();
/*     */   private static final float GLOW_INTERVAL = 1.2F;
/*  55 */   private float glowTimer = 0.0F;
/*  56 */   public boolean isGlowing = false; public boolean isWarning = false;
/*     */   
/*     */ 
/*  59 */   private Hitbox hb = new Hitbox(0.0F, 0.0F, 230.0F * Settings.scale, 110.0F * Settings.scale);
/*     */   
/*     */   public void update() {
/*  62 */     glow();
/*  63 */     if (this.current_x != this.target_x) {
/*  64 */       this.current_x = com.badlogic.gdx.math.MathUtils.lerp(this.current_x, this.target_x, Gdx.graphics.getDeltaTime() * 9.0F);
/*  65 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/*  66 */         this.current_x = this.target_x;
/*     */       }
/*     */     }
/*     */     
/*  70 */     this.hb.move(this.current_x, this.current_y);
/*     */     
/*  72 */     if (this.enabled) {
/*  73 */       this.isDisabled = false;
/*  74 */       if ((AbstractDungeon.isScreenUp) || (AbstractDungeon.player.isDraggingCard) || (AbstractDungeon.player.inSingleTargetMode))
/*     */       {
/*  76 */         this.isDisabled = true;
/*     */       }
/*     */       
/*  79 */       if (AbstractDungeon.player.hoveredCard == null) {
/*  80 */         this.hb.update();
/*     */       }
/*     */       
/*  83 */       if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.hb.hovered) && (!this.isDisabled) && (!AbstractDungeon.isScreenUp)) {
/*  84 */         this.hb.clickStarted = true;
/*  85 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*     */       }
/*     */       
/*  88 */       if ((this.hb.hovered) && (!this.isDisabled) && (!AbstractDungeon.isScreenUp)) {
/*  89 */         this.isWarning = showWarning();
/*  90 */         if (this.hb.justHovered) {
/*  91 */           CardCrawlGame.sound.play("UI_HOVER");
/*  92 */           for (AbstractCard c : AbstractDungeon.player.hand.group) {
/*  93 */             if (c.isGlowing) {
/*  94 */               c.superFlash(Color.SKY.cpy());
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 101 */     if ((this.hb.clicked) || (((InputActionSet.endTurn.isJustPressed()) || (CInputActionSet.proceed.isJustPressed())) && (!this.isDisabled) && (this.enabled)))
/*     */     {
/* 103 */       this.hb.clicked = false;
/*     */       
/* 105 */       if ((!this.isDisabled) && (!AbstractDungeon.isScreenUp)) {
/* 106 */         disable(true);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private boolean showWarning() {
/* 112 */     for (AbstractCard card : AbstractDungeon.player.hand.group) {
/* 113 */       if (card.isGlowing) {
/* 114 */         return true;
/*     */       }
/*     */     }
/* 117 */     return false;
/*     */   }
/*     */   
/*     */   public void enable() {
/* 121 */     this.enabled = true;
/* 122 */     updateText(END_TURN_MSG);
/*     */   }
/*     */   
/*     */   public void disable(boolean isEnemyTurn) {
/* 126 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.QueueCardAction());
/*     */     
/* 128 */     this.enabled = false;
/* 129 */     this.hb.hovered = false;
/* 130 */     this.isGlowing = false;
/*     */     
/* 132 */     if (isEnemyTurn) {
/* 133 */       updateText(ENEMY_TURN_MSG);
/* 134 */       CardCrawlGame.sound.play("END_TURN");
/* 135 */       AbstractDungeon.player.endTurnQueued = true;
/* 136 */       AbstractDungeon.player.releaseCard();
/*     */     } else {
/* 138 */       updateText(END_TURN_MSG);
/*     */     }
/*     */   }
/*     */   
/*     */   public void disable() {
/* 143 */     this.enabled = false;
/* 144 */     this.hb.hovered = false;
/* 145 */     this.isGlowing = false;
/*     */   }
/*     */   
/*     */   public void updateText(String msg) {
/* 149 */     this.label = msg;
/*     */   }
/*     */   
/*     */   private void glow() {
/* 153 */     if ((this.isGlowing) && (!this.isHidden)) {
/* 154 */       if (this.glowTimer < 0.0F) {
/* 155 */         this.glowList.add(new EndTurnGlowEffect());
/* 156 */         this.glowTimer = 1.2F;
/*     */       } else {
/* 158 */         this.glowTimer -= Gdx.graphics.getDeltaTime();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 163 */     for (Iterator<EndTurnGlowEffect> i = this.glowList.iterator(); i.hasNext();) {
/* 164 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 165 */       e.update();
/* 166 */       if (e.isDone) {
/* 167 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/* 173 */     if (!this.isHidden) {
/* 174 */       this.target_x = HIDE_X;
/* 175 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void show() {
/* 180 */     if (this.isHidden) {
/* 181 */       this.target_x = SHOW_X;
/* 182 */       this.isHidden = false;
/* 183 */       if (this.isGlowing) {
/* 184 */         this.glowTimer = -1.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 190 */     if (!Settings.hideEndTurn) {
/* 191 */       float tmpY = this.current_y;
/*     */       
/* 193 */       if ((this.isDisabled) || (!this.enabled)) {
/* 194 */         if (this.label.equals(ENEMY_TURN_MSG)) {
/* 195 */           this.textColor = Settings.CREAM_COLOR;
/*     */         } else {
/* 197 */           this.textColor = Color.LIGHT_GRAY;
/*     */         }
/*     */       } else {
/* 200 */         if (this.hb.hovered) {
/* 201 */           if (this.isWarning) {
/* 202 */             this.textColor = Settings.RED_TEXT_COLOR;
/*     */           } else {
/* 204 */             this.textColor = Color.CYAN;
/*     */           }
/*     */         }
/* 207 */         else if (this.isGlowing) {
/* 208 */           this.textColor = Settings.GOLD_COLOR;
/*     */         } else {
/* 210 */           this.textColor = Settings.CREAM_COLOR;
/*     */         }
/*     */         
/*     */ 
/* 214 */         if ((this.hb.hovered) && (!AbstractDungeon.isScreenUp)) {
/* 215 */           com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(this.current_x - 90.0F * Settings.scale, this.current_y + 300.0F * Settings.scale, LABEL[0] + " (" + InputActionSet.endTurn
/*     */           
/*     */ 
/* 218 */             .getKeyString() + ")", MSG[0] + AbstractDungeon.player.gameHandSize + MSG[1]);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 223 */       if ((this.hb.clickStarted) && (!AbstractDungeon.isScreenUp)) {
/* 224 */         tmpY -= 2.0F * Settings.scale;
/* 225 */       } else if ((this.hb.hovered) && (!AbstractDungeon.isScreenUp)) {
/* 226 */         tmpY += 2.0F * Settings.scale;
/*     */       }
/*     */       
/* 229 */       if (this.enabled) {
/* 230 */         if ((this.isDisabled) || ((this.hb.clickStarted) && (this.hb.hovered))) {
/* 231 */           sb.setColor(DISABLED_COLOR);
/*     */         } else {
/* 233 */           sb.setColor(Color.WHITE);
/*     */         }
/*     */       } else {
/* 236 */         ShaderHelper.setShader(sb, com.megacrit.cardcrawl.helpers.ShaderHelper.Shader.GRAYSCALE);
/*     */       }
/*     */       com.badlogic.gdx.graphics.Texture buttonImg;
/*     */       com.badlogic.gdx.graphics.Texture buttonImg;
/* 240 */       if ((this.isGlowing) && (!this.hb.clickStarted)) {
/* 241 */         buttonImg = ImageMaster.END_TURN_BUTTON_GLOW;
/*     */       } else {
/* 243 */         buttonImg = ImageMaster.END_TURN_BUTTON;
/*     */       }
/* 245 */       if ((this.hb.hovered) && (!this.isDisabled) && (!AbstractDungeon.isScreenUp)) {
/* 246 */         sb.draw(ImageMaster.END_TURN_HOVER, this.current_x - 128.0F, tmpY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 265 */       sb.draw(buttonImg, this.current_x - 128.0F, tmpY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 283 */       if (!this.enabled) {
/* 284 */         ShaderHelper.setShader(sb, com.megacrit.cardcrawl.helpers.ShaderHelper.Shader.DEFAULT);
/*     */       }
/*     */       
/* 287 */       renderGlowEffect(sb, this.current_x, this.current_y);
/*     */       
/* 289 */       if ((this.hb.hovered) && (!this.isDisabled) && (!AbstractDungeon.isScreenUp)) {
/* 290 */         sb.setBlendFunction(770, 1);
/* 291 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 292 */         sb.draw(buttonImg, this.current_x - 128.0F, tmpY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 309 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */       
/* 312 */       if ((Settings.isControllerMode) && (this.enabled)) {
/* 313 */         sb.setColor(Color.WHITE);
/* 314 */         sb.draw(CInputActionSet.proceed
/* 315 */           .getKeyImg(), this.current_x - 32.0F - 42.0F * Settings.scale - 
/* 316 */           FontHelper.getSmartWidth(FontHelper.panelEndTurnFont, this.label, 99999.0F, 0.0F) / 2.0F, tmpY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 337 */       FontHelper.renderFontCentered(sb, FontHelper.panelEndTurnFont, this.label, this.current_x - 0.0F * Settings.scale, tmpY - 3.0F * Settings.scale, this.textColor);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 345 */       if (!this.isHidden) {
/* 346 */         this.hb.render(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderGlowEffect(SpriteBatch sb, float x, float y) {
/* 352 */     for (EndTurnGlowEffect e : this.glowList) {
/* 353 */       e.render(sb, x, y);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\EndTurnButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */