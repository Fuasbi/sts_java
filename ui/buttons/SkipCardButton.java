/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ 
/*     */ public class SkipCardButton
/*     */ {
/*  21 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CardRewardScreen");
/*  22 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final int W = 512;
/*     */   private static final int H = 256;
/*  26 */   private static final float TAKE_Y = 220.0F * Settings.scale;
/*  27 */   private static final float SHOW_X = Settings.WIDTH / 2.0F;
/*  28 */   private static final float HIDE_X = Settings.WIDTH / 2.0F;
/*  29 */   private float current_x = HIDE_X;
/*  30 */   private float target_x = this.current_x;
/*  31 */   private boolean isHidden = true;
/*  32 */   private Color textColor = Color.WHITE.cpy();
/*  33 */   private Color btnColor = Color.WHITE.cpy();
/*     */   
/*     */ 
/*  36 */   private static final float HITBOX_W = 260.0F * Settings.scale; private static final float HITBOX_H = 80.0F * Settings.scale;
/*  37 */   public Hitbox hb = new Hitbox(0.0F, 0.0F, HITBOX_W, HITBOX_H);
/*     */   
/*     */   public SkipCardButton() {
/*  40 */     this.hb.move(Settings.WIDTH / 2.0F, TAKE_Y);
/*     */   }
/*     */   
/*     */   public void update() {
/*  44 */     if (this.isHidden) {
/*  45 */       return;
/*     */     }
/*     */     
/*  48 */     this.hb.update();
/*     */     
/*  50 */     if (this.hb.justHovered) {
/*  51 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*  53 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  54 */       this.hb.clickStarted = true;
/*  55 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*     */     }
/*     */     
/*  58 */     if ((this.hb.clicked) || (com.megacrit.cardcrawl.helpers.input.InputActionSet.cancel.isJustPressed()) || (CInputActionSet.cancel.isJustPressed())) {
/*  59 */       this.hb.clicked = false;
/*  60 */       AbstractDungeon.closeCurrentScreen();
/*     */     }
/*     */     
/*  63 */     if (this.current_x != this.target_x) {
/*  64 */       this.current_x = MathUtils.lerp(this.current_x, this.target_x, Gdx.graphics.getDeltaTime() * 9.0F);
/*  65 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/*  66 */         this.current_x = this.target_x;
/*  67 */         this.hb.move(this.current_x, TAKE_Y);
/*     */       }
/*     */     }
/*     */     
/*  71 */     this.textColor.a = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.textColor.a, 1.0F);
/*  72 */     this.btnColor.a = this.textColor.a;
/*     */   }
/*     */   
/*     */   public void hideInstantly() {
/*  76 */     this.current_x = HIDE_X;
/*  77 */     this.target_x = HIDE_X;
/*  78 */     this.isHidden = true;
/*  79 */     this.textColor.a = 0.0F;
/*  80 */     this.btnColor.a = 0.0F;
/*     */   }
/*     */   
/*     */   public void hide() {
/*  84 */     this.isHidden = true;
/*     */   }
/*     */   
/*     */   public void show() {
/*  88 */     this.isHidden = false;
/*  89 */     this.textColor.a = 0.0F;
/*  90 */     this.btnColor.a = 0.0F;
/*  91 */     this.current_x = HIDE_X;
/*  92 */     this.target_x = SHOW_X;
/*  93 */     this.hb.move(SHOW_X, TAKE_Y);
/*     */   }
/*     */   
/*     */   public void show(boolean singingBowl) {
/*  97 */     this.isHidden = false;
/*  98 */     this.textColor.a = 0.0F;
/*  99 */     this.btnColor.a = 0.0F;
/* 100 */     this.current_x = HIDE_X;
/* 101 */     this.target_x = (SHOW_X - 165.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 105 */     if (this.isHidden) {
/* 106 */       return;
/*     */     }
/*     */     
/* 109 */     renderButton(sb);
/* 110 */     if (FontHelper.getSmartWidth(FontHelper.buttonLabelFont, TEXT[0], 9999.0F, 0.0F) > 200.0F * Settings.scale) {
/* 111 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, TEXT[0], this.current_x, TAKE_Y, this.textColor, 0.8F);
/*     */     } else {
/* 113 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, TEXT[0], this.current_x, TAKE_Y, this.textColor);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderButton(SpriteBatch sb) {
/* 118 */     sb.setColor(this.btnColor);
/* 119 */     sb.draw(ImageMaster.REWARD_SCREEN_TAKE_BUTTON, this.current_x - 256.0F, TAKE_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 137 */     if ((this.hb.hovered) && (!this.hb.clickStarted)) {
/* 138 */       sb.setBlendFunction(770, 1);
/* 139 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.3F));
/* 140 */       sb.draw(ImageMaster.REWARD_SCREEN_TAKE_BUTTON, this.current_x - 256.0F, TAKE_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 157 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 160 */     if (Settings.isControllerMode) {
/* 161 */       sb.setColor(Color.WHITE);
/* 162 */       sb.draw(CInputActionSet.cancel
/* 163 */         .getKeyImg(), this.current_x - 32.0F - 38.0F * Settings.scale - 
/* 164 */         FontHelper.getSmartWidth(FontHelper.buttonLabelFont, TEXT[0], 99999.0F, 0.0F) / 2.0F, TAKE_Y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 187 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\SkipCardButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */