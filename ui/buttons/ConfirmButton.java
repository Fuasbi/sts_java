/*     */ package com.megacrit.cardcrawl.ui.buttons;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ 
/*     */ public class ConfirmButton
/*     */ {
/*     */   private static final int W = 512;
/*     */   private static final int H = 256;
/*  19 */   private static final Color HOVER_BLEND_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.3F);
/*  20 */   private static final Color TEXT_DISABLED_COLOR = new Color(0.6F, 0.6F, 0.6F, 1.0F);
/*  21 */   private static final float SHOW_X = Settings.WIDTH - 256.0F * Settings.scale; private static final float DRAW_Y = 128.0F * Settings.scale;
/*  22 */   private static final float HIDE_X = SHOW_X + 400.0F * Settings.scale;
/*  23 */   private float current_x = HIDE_X;
/*  24 */   private float target_x = this.current_x;
/*  25 */   private boolean isHidden = true;
/*  26 */   public boolean isDisabled = true; public boolean isHovered = false;
/*  27 */   private float glowAlpha = 0.0F;
/*  28 */   private Color glowColor = Color.WHITE.cpy();
/*     */   
/*     */ 
/*  31 */   private String buttonText = "NOT_SET";
/*  32 */   private static final float TEXT_OFFSET_X = 136.0F * Settings.scale;
/*  33 */   private static final float TEXT_OFFSET_Y = 57.0F * Settings.scale;
/*     */   
/*     */ 
/*  36 */   private static final float HITBOX_W = 300.0F * Settings.scale; private static final float HITBOX_H = 100.0F * Settings.scale;
/*  37 */   public Hitbox hb = new Hitbox(0.0F, 0.0F, HITBOX_W, HITBOX_H);
/*     */   
/*     */   public ConfirmButton(String label) {
/*  40 */     this.buttonText = label;
/*  41 */     this.hb.move(SHOW_X + 106.0F * Settings.scale, DRAW_Y + 60.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void updateText(String label) {
/*  45 */     this.buttonText = label;
/*     */   }
/*     */   
/*     */   public void update() {
/*  49 */     if (!this.isHidden) {
/*  50 */       updateGlow();
/*  51 */       this.hb.update();
/*     */       
/*  53 */       if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.hb.hovered) && (!this.isDisabled)) {
/*  54 */         this.hb.clickStarted = true;
/*  55 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*     */       }
/*  57 */       if ((this.hb.justHovered) && (!this.isDisabled)) {
/*  58 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*  60 */       this.isHovered = this.hb.hovered;
/*     */       
/*  62 */       if (CInputActionSet.select.isJustPressed()) {
/*  63 */         CInputActionSet.select.unpress();
/*  64 */         this.hb.clicked = true;
/*     */       }
/*     */     }
/*     */     
/*  68 */     if (this.current_x != this.target_x) {
/*  69 */       this.current_x = MathUtils.lerp(this.current_x, this.target_x, Gdx.graphics.getDeltaTime() * 9.0F);
/*  70 */       if (Math.abs(this.current_x - this.target_x) < Settings.UI_SNAP_THRESHOLD) {
/*  71 */         this.current_x = this.target_x;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateGlow() {
/*  77 */     this.glowAlpha += Gdx.graphics.getDeltaTime() * 3.0F;
/*  78 */     if (this.glowAlpha < 0.0F) {
/*  79 */       this.glowAlpha *= -1.0F;
/*     */     }
/*  81 */     float tmp = MathUtils.cos(this.glowAlpha);
/*  82 */     if (tmp < 0.0F) {
/*  83 */       this.glowColor.a = (-tmp / 2.0F + 0.3F);
/*     */     } else {
/*  85 */       this.glowColor.a = (tmp / 2.0F + 0.3F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void hideInstantly() {
/*  90 */     this.current_x = HIDE_X;
/*  91 */     this.target_x = HIDE_X;
/*  92 */     this.isHidden = true;
/*     */   }
/*     */   
/*     */   public void hide() {
/*  96 */     if (!this.isHidden) {
/*  97 */       this.target_x = HIDE_X;
/*  98 */       this.isHidden = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void show() {
/* 103 */     if (this.isHidden) {
/* 104 */       this.glowAlpha = 0.0F;
/* 105 */       this.target_x = SHOW_X;
/* 106 */       this.isHidden = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 111 */     sb.setColor(Color.WHITE);
/* 112 */     renderShadow(sb);
/* 113 */     sb.setColor(this.glowColor);
/* 114 */     renderOutline(sb);
/* 115 */     sb.setColor(Color.WHITE);
/* 116 */     renderButton(sb);
/*     */     
/* 118 */     if ((this.hb.hovered) && (!this.isDisabled) && (!this.hb.clickStarted)) {
/* 119 */       sb.setBlendFunction(770, 1);
/* 120 */       sb.setColor(HOVER_BLEND_COLOR);
/* 121 */       renderButton(sb);
/* 122 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/*     */ 
/* 126 */     if (this.isDisabled) {
/* 127 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X, DRAW_Y + TEXT_OFFSET_Y, TEXT_DISABLED_COLOR);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 134 */     else if (this.hb.clickStarted) {
/* 135 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X, DRAW_Y + TEXT_OFFSET_Y, Color.LIGHT_GRAY);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 142 */     else if (this.hb.hovered) {
/* 143 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X, DRAW_Y + TEXT_OFFSET_Y, Settings.LIGHT_YELLOW_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 151 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.buttonText, this.current_x + TEXT_OFFSET_X, DRAW_Y + TEXT_OFFSET_Y, Settings.LIGHT_YELLOW_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 160 */     renderControllerUi(sb);
/*     */     
/* 162 */     if (!this.isHidden) {
/* 163 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderShadow(SpriteBatch sb) {
/* 168 */     sb.draw(ImageMaster.CONFIRM_BUTTON_SHADOW, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderOutline(SpriteBatch sb)
/*     */   {
/* 188 */     sb.draw(ImageMaster.CONFIRM_BUTTON_OUTLINE, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderButton(SpriteBatch sb)
/*     */   {
/* 208 */     sb.draw(ImageMaster.CONFIRM_BUTTON, this.current_x - 256.0F, DRAW_Y - 128.0F, 256.0F, 128.0F, 512.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 256, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderControllerUi(SpriteBatch sb)
/*     */   {
/* 228 */     if (Settings.isControllerMode) {
/* 229 */       sb.setColor(Color.WHITE);
/* 230 */       sb.draw(CInputActionSet.select
/* 231 */         .getKeyImg(), this.current_x - 32.0F + 47.0F * Settings.scale, DRAW_Y - 32.0F + 57.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\buttons\ConfirmButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */