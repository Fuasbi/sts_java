/*     */ package com.megacrit.cardcrawl.ui;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ 
/*     */ public class SpeechWord
/*     */ {
/*     */   private BitmapFont font;
/*     */   private DialogWord.WordEffect effect;
/*     */   private DialogWord.WordColor wColor;
/*     */   public String word;
/*  20 */   public int line = 0;
/*     */   private float x;
/*  22 */   private float y; private float target_x; private float target_y; private float offset_x; private float offset_y; private float timer = 0.0F;
/*     */   private Color color;
/*  24 */   private Color targetColor; private float scale = 1.0F; private float targetScale = 1.0F;
/*  25 */   private static final float BUMP_OFFSET = 20.0F * Settings.scale;
/*     */   private static GlyphLayout gl;
/*     */   private static final float COLOR_LERP_SPEED = 8.0F;
/*  28 */   private static final float SHAKE_AMT = 2.0F * Settings.scale;
/*  29 */   private static final float DIALOG_FADE_Y = 50.0F * Settings.scale;
/*     */   
/*     */   private static final float WAVY_SPEED = 4.5F;
/*  32 */   private static final float WAVY_DIST = 3.0F * Settings.scale;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final float SHAKE_INTERVAL = 0.02F;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SpeechWord(BitmapFont font, String word, DialogWord.AppearEffect a_effect, DialogWord.WordEffect effect, DialogWord.WordColor wColor, float x, float y, int line)
/*     */   {
/*  45 */     if (gl == null) {
/*  46 */       gl = new GlyphLayout();
/*     */     }
/*     */     
/*  49 */     this.font = font;
/*  50 */     this.effect = effect;
/*  51 */     this.wColor = wColor;
/*  52 */     this.word = word;
/*  53 */     this.x = x;
/*  54 */     this.y = y;
/*  55 */     this.target_x = x;
/*  56 */     this.target_y = y;
/*  57 */     this.targetColor = getColor();
/*  58 */     this.line = line;
/*  59 */     this.color = new Color(this.targetColor.r, this.targetColor.g, this.targetColor.b, 0.0F);
/*     */     
/*  61 */     if (effect == DialogWord.WordEffect.WAVY) {
/*  62 */       this.timer = MathUtils.random(1.5707964F);
/*     */     }
/*     */     
/*  65 */     switch (a_effect) {
/*     */     case FADE_IN: 
/*     */       break;
/*     */     case GROW_IN: 
/*  69 */       this.y -= BUMP_OFFSET;
/*  70 */       this.scale = 0.0F;
/*  71 */       break;
/*     */     case BUMP_IN: 
/*  73 */       this.y -= BUMP_OFFSET;
/*  74 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   private Color getColor()
/*     */   {
/*  82 */     switch (this.wColor) {
/*     */     case RED: 
/*  84 */       return new Color(1.0F, 0.2F, 0.3F, 1.0F);
/*     */     case GREEN: 
/*  86 */       return new Color(0.3F, 1.0F, 0.1F, 1.0F);
/*     */     case BLUE: 
/*  88 */       return Settings.BLUE_TEXT_COLOR.cpy();
/*     */     case GOLD: 
/*  90 */       return Settings.GOLD_COLOR.cpy();
/*     */     }
/*  92 */     return Color.DARK_GRAY.cpy();
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/*  98 */     if (this.x != this.target_x) {
/*  99 */       this.x = MathUtils.lerp(this.x, this.target_x, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/* 101 */     if (this.y != this.target_y) {
/* 102 */       this.y = MathUtils.lerp(this.y, this.target_y, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/*     */     
/*     */ 
/* 106 */     this.color = this.color.lerp(this.targetColor, Gdx.graphics.getDeltaTime() * 8.0F);
/*     */     
/*     */ 
/* 109 */     if (this.scale != this.targetScale) {
/* 110 */       this.scale = MathHelper.scaleLerpSnap(this.scale, this.targetScale);
/*     */     }
/*     */     
/* 113 */     applyEffects();
/*     */   }
/*     */   
/*     */   private void applyEffects() {
/* 117 */     switch (this.effect) {
/*     */     case SHAKY: 
/* 119 */       this.timer -= Gdx.graphics.getDeltaTime();
/* 120 */       if (this.timer < 0.0F) {
/* 121 */         this.offset_x = MathUtils.random(-SHAKE_AMT, SHAKE_AMT);
/* 122 */         this.offset_y = MathUtils.random(-SHAKE_AMT, SHAKE_AMT);
/* 123 */         this.timer = 0.02F;
/*     */       }
/*     */       break;
/*     */     case WAVY: 
/* 127 */       this.timer += Gdx.graphics.getDeltaTime() * 4.5F;
/*     */       
/* 129 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void fadeOut()
/*     */   {
/* 137 */     this.targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*     */   }
/*     */   
/*     */   public void dialogFadeOut() {
/* 141 */     this.targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/* 142 */     this.target_y -= DIALOG_FADE_Y;
/*     */   }
/*     */   
/*     */   public void shiftY(float shiftAmount) {
/* 146 */     this.target_y += shiftAmount;
/*     */   }
/*     */   
/*     */   public void shiftX(float shiftAmount) {
/* 150 */     this.target_x += shiftAmount;
/*     */   }
/*     */   
/*     */   public void setX(float newX) {
/* 154 */     this.target_x = newX;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DialogWord.WordEffect identifyWordEffect(String word)
/*     */   {
/* 164 */     if (word.length() > 2) {
/* 165 */       if ((word.charAt(0) == '@') && (word.charAt(word.length() - 1) == '@'))
/* 166 */         return DialogWord.WordEffect.SHAKY;
/* 167 */       if ((word.charAt(0) == '~') && (word.charAt(word.length() - 1) == '~')) {
/* 168 */         return DialogWord.WordEffect.WAVY;
/*     */       }
/*     */     }
/*     */     
/* 172 */     return DialogWord.WordEffect.NONE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static DialogWord.WordColor identifyWordColor(String word)
/*     */   {
/* 182 */     if (word.charAt(0) == '#') {
/* 183 */       switch (word.charAt(1)) {
/*     */       case 'r': 
/* 185 */         return DialogWord.WordColor.RED;
/*     */       case 'g': 
/* 187 */         return DialogWord.WordColor.GREEN;
/*     */       case 'b': 
/* 189 */         return DialogWord.WordColor.BLUE;
/*     */       case 'y': 
/* 191 */         return DialogWord.WordColor.GOLD;
/*     */       }
/*     */       
/*     */     }
/* 195 */     return DialogWord.WordColor.DEFAULT;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 199 */     this.font.setColor(this.color);
/* 200 */     this.font.getData().setScale(this.scale);
/*     */     char c1;
/* 202 */     char c; switch (this.effect) {
/*     */     case WAVY: 
/* 204 */       float charOffset = 0.0F;
/* 205 */       int j = 0;
/*     */       
/* 207 */       char[] arrayOfChar1 = this.word.toCharArray();int i = arrayOfChar1.length; for (c1 = '\000'; c1 < i; c1++) { c = arrayOfChar1[c1];
/* 208 */         String i = Character.toString(c);
/* 209 */         gl.setText(this.font, i);
/* 210 */         this.font.draw(sb, i, this.x + this.offset_x + charOffset, this.y + 
/*     */         
/*     */ 
/*     */ 
/* 214 */           MathUtils.cosDeg((float)((System.currentTimeMillis() + j * 70) / 4L % 360L)) * WAVY_DIST);
/* 215 */         charOffset += gl.width;
/* 216 */         j++;
/*     */       }
/* 218 */       break;
/*     */     case SHAKY: 
/* 220 */       float charOffset2 = 0.0F;
/* 221 */       char[] arrayOfChar2 = this.word.toCharArray();c1 = arrayOfChar2.length; for (c = '\000'; c < c1; c++) { char c = arrayOfChar2[c];
/* 222 */         String i = Character.toString(c);
/* 223 */         gl.setText(this.font, i);
/* 224 */         this.font.draw(sb, i, this.x + 
/*     */         
/*     */ 
/* 227 */           MathUtils.random(-2.0F, 2.0F) * Settings.scale + charOffset2, this.y + 
/* 228 */           MathUtils.random(-2.0F, 2.0F) * Settings.scale);
/* 229 */         charOffset2 += gl.width;
/*     */       }
/* 231 */       break;
/*     */     default: 
/* 233 */       this.font.draw(sb, this.word, this.x + this.offset_x, this.y + this.offset_y);
/*     */     }
/*     */     
/*     */     
/* 237 */     this.font.getData().setScale(1.0F);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float y2) {
/* 241 */     this.font.setColor(this.color);
/* 242 */     this.font.getData().setScale(this.scale);
/* 243 */     this.font.draw(sb, this.word, this.x + this.offset_x, this.y + this.offset_y + y2);
/* 244 */     this.font.getData().setScale(1.0F);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\SpeechWord.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */