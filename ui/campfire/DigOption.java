/*    */ package com.megacrit.cardcrawl.ui.campfire;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.vfx.campfire.CampfireDigEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DigOption extends AbstractCampfireOption
/*    */ {
/* 10 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("Dig Option");
/* 11 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public DigOption() {
/* 14 */     this.label = TEXT[0];
/* 15 */     this.description = TEXT[1];
/* 16 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.CAMPFIRE_DIG_BUTTON;
/*    */   }
/*    */   
/*    */   public void useOption()
/*    */   {
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new CampfireDigEffect());
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\campfire\DigOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */