/*     */ package com.megacrit.cardcrawl.ui.campfire;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.rooms.CampfireUI;
/*     */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*     */ 
/*     */ public abstract class AbstractCampfireOption
/*     */ {
/*  21 */   private static final float SHDW_X = 11.0F * Settings.scale; private static final float SHDW_Y = -8.0F * Settings.scale;
/*  22 */   private Color color = Color.WHITE.cpy();
/*  23 */   private static final float NORM_SCALE = 0.9F * Settings.scale;
/*  24 */   private static final float HOVER_SCALE = Settings.scale;
/*  25 */   private float scale = NORM_SCALE;
/*  26 */   public Hitbox hb = new Hitbox(216.0F * Settings.scale, 140.0F * Settings.scale);
/*  27 */   public boolean usable = true;
/*     */   protected String label;
/*     */   
/*  30 */   public void setPosition(float x, float y) { this.hb.move(x, y); }
/*     */   
/*     */   protected String description;
/*     */   
/*  34 */   public void update() { this.hb.update();
/*  35 */     boolean canClick = (!((RestRoom)AbstractDungeon.getCurrRoom()).campfireUI.somethingSelected) && (this.usable);
/*  36 */     if ((this.hb.hovered) && (canClick)) {
/*  37 */       if (this.hb.justHovered) {
/*  38 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*  40 */       if (InputHelper.justClickedLeft) {
/*  41 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*  42 */         this.hb.clickStarted = true;
/*     */       }
/*  44 */       if (!this.hb.clickStarted) {
/*  45 */         this.scale = MathHelper.scaleLerpSnap(this.scale, HOVER_SCALE);
/*  46 */         this.scale = MathHelper.scaleLerpSnap(this.scale, HOVER_SCALE);
/*     */       } else {
/*  48 */         this.scale = MathHelper.scaleLerpSnap(this.scale, NORM_SCALE);
/*     */       }
/*     */     } else {
/*  51 */       this.scale = MathHelper.scaleLerpSnap(this.scale, NORM_SCALE);
/*     */     }
/*     */     
/*  54 */     if ((this.hb.clicked) || ((CInputActionSet.select.isJustPressed()) && (canClick) && (this.hb.hovered))) {
/*  55 */       this.hb.clicked = false;
/*  56 */       useOption();
/*  57 */       ((RestRoom)AbstractDungeon.getCurrRoom()).campfireUI.somethingSelected = true;
/*     */     } }
/*     */   
/*     */   protected com.badlogic.gdx.graphics.Texture img;
/*     */   private static final int W = 256;
/*     */   public abstract void useOption();
/*     */   
/*  64 */   public void render(SpriteBatch sb) { float scaler = (this.scale - NORM_SCALE) * 10.0F / Settings.scale;
/*     */     
/*  66 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.color.a / 5.0F));
/*  67 */     sb.draw(this.img, this.hb.cX - 128.0F + SHDW_X, this.hb.cY - 128.0F + SHDW_Y, 128.0F, 128.0F, 256.0F, 256.0F, this.scale, this.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  85 */     sb.setColor(new Color(1.0F, 0.93F, 0.45F, scaler));
/*  86 */     sb.draw(ImageMaster.CAMPFIRE_HOVER_BUTTON, this.hb.cX - 128.0F, this.hb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.scale * 1.075F, this.scale * 1.075F, 0.0F, 0, 0, 256, 256, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 104 */     sb.setColor(this.color);
/* 105 */     sb.draw(this.img, this.hb.cX - 128.0F, this.hb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.scale, this.scale, 0.0F, 0, 0, 256, 256, false, false);
/* 106 */     FontHelper.renderFontCenteredTopAligned(sb, FontHelper.topPanelInfoFont, this.label, this.hb.cX, this.hb.cY - 60.0F * Settings.scale - 50.0F * Settings.scale * this.scale, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 114 */     FontHelper.renderFontCenteredTopAligned(sb, FontHelper.topPanelInfoFont, this.description, 950.0F * Settings.scale, 560.0F * Settings.scale, new Color(1.0F, 0.95F, 0.95F, scaler));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 121 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\campfire\AbstractCampfireOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */