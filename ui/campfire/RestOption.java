/*    */ package com.megacrit.cardcrawl.ui.campfire;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.metrics.MetricData;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import com.megacrit.cardcrawl.vfx.campfire.CampfireSleepEffect;
/*    */ import java.util.ArrayList;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class RestOption extends AbstractCampfireOption
/*    */ {
/* 16 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("Rest Option");
/* 17 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public RestOption() {
/* 20 */     this.label = TEXT[0];
/*    */     int healAmt;
/*    */     int healAmt;
/* 23 */     if (((Boolean)DailyMods.negativeMods.get("Night Terrors")).booleanValue()) {
/* 24 */       healAmt = (int)(AbstractDungeon.player.maxHealth * 1.0F);
/*    */     } else {
/* 26 */       healAmt = (int)(AbstractDungeon.player.maxHealth * 0.3F);
/*    */     }
/*    */     
/* 29 */     if ((com.megacrit.cardcrawl.core.Settings.isEndless) && (AbstractDungeon.player.hasBlight("FullBelly"))) {
/* 30 */       healAmt /= 2;
/*    */     }
/*    */     
/* 33 */     if (((Boolean)DailyMods.negativeMods.get("Night Terrors")).booleanValue()) {
/* 34 */       this.description = (TEXT[1] + healAmt + ").");
/* 35 */       if (AbstractDungeon.player.hasRelic("Regal Pillow")) {
/* 36 */         this.description = (this.description + "\n+15" + TEXT[2] + AbstractDungeon.player.getRelic("Regal Pillow").name + ".");
/*    */       }
/*    */     }
/*    */     else {
/* 40 */       this.description = (TEXT[3] + healAmt + ").");
/* 41 */       if (AbstractDungeon.player.hasRelic("Regal Pillow")) {
/* 42 */         this.description = (this.description + "\n+15" + TEXT[2] + AbstractDungeon.player.getRelic("Regal Pillow").name + ".");
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 47 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.CAMPFIRE_REST_BUTTON;
/*    */   }
/*    */   
/*    */   public void useOption()
/*    */   {
/* 52 */     CardCrawlGame.sound.play("SLEEP_BLANKET");
/* 53 */     AbstractDungeon.effectList.add(new CampfireSleepEffect());
/*    */     
/* 55 */     for (int i = 0; i < 30; i++) {
/* 56 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.campfire.CampfireSleepScreenCoverEffect());
/*    */     }
/* 58 */     CardCrawlGame.metricData.campfire_rested += 1;
/* 59 */     CardCrawlGame.metricData.addCampfireChoiceData("REST");
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\campfire\RestOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */