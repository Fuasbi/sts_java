/*    */ package com.megacrit.cardcrawl.ui.campfire;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.vfx.campfire.CampfireLiftEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LiftOption extends AbstractCampfireOption
/*    */ {
/* 10 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("Lift Option");
/* 11 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public LiftOption() {
/* 14 */     this.label = TEXT[0];
/* 15 */     this.description = TEXT[1];
/* 16 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.CAMPFIRE_TRAIN_BUTTON;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void useOption()
/*    */   {
/* 23 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new CampfireLiftEffect());
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\campfire\LiftOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */