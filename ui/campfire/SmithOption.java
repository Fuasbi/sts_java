/*    */ package com.megacrit.cardcrawl.ui.campfire;
/*    */ 
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SmithOption extends AbstractCampfireOption
/*    */ {
/* 10 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("Smith Option");
/* 11 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public SmithOption(boolean active) {
/* 14 */     this.label = TEXT[0];
/* 15 */     this.usable = active;
/* 16 */     if (active) {
/* 17 */       this.description = TEXT[1];
/* 18 */       this.img = ImageMaster.CAMPFIRE_SMITH_BUTTON;
/*    */     } else {
/* 20 */       this.description = TEXT[2];
/* 21 */       this.img = ImageMaster.CAMPFIRE_SMITH_DISABLE_BUTTON;
/*    */     }
/*    */   }
/*    */   
/*    */   public void useOption()
/*    */   {
/* 27 */     if (this.usable) {
/* 28 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.campfire.CampfireSmithEffect());
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\ui\campfire\SmithOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */