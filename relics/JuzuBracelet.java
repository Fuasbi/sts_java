/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class JuzuBracelet extends AbstractRelic {
/*    */   public static final String ID = "Juzu Bracelet";
/*    */   
/*    */   public JuzuBracelet() {
/*  7 */     super("Juzu Bracelet", "juzuBracelet.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new JuzuBracelet();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\JuzuBracelet.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */