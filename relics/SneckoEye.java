/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class SneckoEye extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Snecko Eye";
/*    */   public static final int HAND_MODIFICATION = 2;
/*    */   
/*    */   public SneckoEye()
/*    */   {
/* 12 */     super("Snecko Eye", "sneckoEye.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 22 */     AbstractDungeon.player.masterHandSize += 2;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 27 */     AbstractDungeon.player.masterHandSize -= 2;
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 32 */     flash();
/* 33 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.ConfusionPower(AbstractDungeon.player)));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 42 */     return new SneckoEye();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SneckoEye.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */