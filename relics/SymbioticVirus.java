/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.orbs.Dark;
/*    */ 
/*    */ public class SymbioticVirus extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Symbiotic Virus";
/*    */   
/*    */   public SymbioticVirus() {
/* 10 */     super("Symbiotic Virus", "virus.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 20 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.channelOrb(new Dark());
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 25 */     return new SymbioticVirus();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SymbioticVirus.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */