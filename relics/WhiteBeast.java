/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class WhiteBeast extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "White Beast Statue";
/*    */   
/*    */   public WhiteBeast()
/*    */   {
/*  9 */     super("White Beast Statue", "whiteBeast.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 19 */     return new WhiteBeast();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\WhiteBeast.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */