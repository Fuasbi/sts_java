/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class PhilosopherStone extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Philosopher's Stone";
/*    */   public static final int STR = 2;
/*    */   
/*    */   public PhilosopherStone()
/*    */   {
/* 16 */     super("Philosopher's Stone", "philosopherStone.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     if (AbstractDungeon.player != null) {
/* 22 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 24 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 29 */     if (c == null) {
/* 30 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     }
/* 32 */     switch (c) {
/*    */     case IRONCLAD: 
/* 34 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     case THE_SILENT: 
/* 36 */       return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0];
/*    */     case DEFECT: 
/* 38 */       return this.DESCRIPTIONS[3] + this.DESCRIPTIONS[0];
/*    */     }
/* 40 */     return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 46 */     this.description = setDescription(c);
/* 47 */     this.tips.clear();
/* 48 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 49 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 54 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 55 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(m, this));
/* 56 */       m.addPower(new com.megacrit.cardcrawl.powers.StrengthPower(m, 2));
/*    */     }
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 62 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 67 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 72 */     return new PhilosopherStone();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\PhilosopherStone.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */