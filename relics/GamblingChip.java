/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class GamblingChip extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Gambling Chip";
/*    */   
/*    */   public GamblingChip()
/*    */   {
/* 11 */     super("Gambling Chip", "gamblingChip.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 21 */     flash();
/* 22 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 23 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.GamblingChipAction(AbstractDungeon.player));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 28 */     return new GamblingChip();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\GamblingChip.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */