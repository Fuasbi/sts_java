/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class TheSpecimen extends AbstractRelic
/*    */ {
/* 13 */   private static final Logger logger = LogManager.getLogger(TheSpecimen.class.getName());
/*    */   public static final String ID = "The Specimen";
/*    */   
/*    */   public TheSpecimen() {
/* 17 */     super("The Specimen", "specimen.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 22 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onMonsterDeath(AbstractMonster m)
/*    */   {
/* 27 */     if (m.hasPower("Poison")) {
/* 28 */       int amount = m.getPower("Poison").amount;
/* 29 */       AbstractMonster target = AbstractDungeon.getRandomMonster(m);
/* 30 */       if (target != null) {
/* 31 */         flash();
/*    */         
/* 33 */         AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(m, this));
/* 34 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, target, new PoisonPower(target, target, amount), amount));
/*    */       }
/*    */       else {
/* 37 */         logger.info("no target for the specimen");
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 44 */     return new TheSpecimen();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\TheSpecimen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */