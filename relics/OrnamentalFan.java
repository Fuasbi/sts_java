/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class OrnamentalFan extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Ornamental Fan";
/*    */   private static final int BLOCK = 4;
/*    */   
/*    */   public OrnamentalFan()
/*    */   {
/* 15 */     super("Ornamental Fan", "ornamentalFan.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 4 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 25 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 30 */     if (card.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 31 */       this.counter += 1;
/*    */       
/* 33 */       if (this.counter % 3 == 0) {
/* 34 */         flash();
/* 35 */         this.counter = 0;
/* 36 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 37 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, 4));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void onVictory()
/*    */   {
/* 45 */     this.counter = -1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 50 */     return new OrnamentalFan();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\OrnamentalFan.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */