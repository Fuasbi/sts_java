/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class RedMask extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Red Mask";
/*    */   private static final int WEAK = 1;
/*    */   
/*    */   public RedMask()
/*    */   {
/* 15 */     super("Red Mask", "redMask.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 1 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     flash();
/* 26 */     for (AbstractMonster mo : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 27 */       AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(mo, this));
/* 28 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(mo, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.WeakPower(mo, 1, false), 1, true));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 35 */     return new RedMask();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RedMask.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */