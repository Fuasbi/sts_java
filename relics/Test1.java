/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Test1 extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Test 1";
/*    */   private static final int ENERGY_AMT = 1;
/*    */   
/*    */   public Test1()
/*    */   {
/* 15 */     super("Test 1", "test1.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     if (AbstractDungeon.player != null) {
/* 21 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 23 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 28 */     if (c == null) {
/* 29 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     }
/* 31 */     switch (c) {
/*    */     case IRONCLAD: 
/* 33 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 35 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 37 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[3];
/*    */     }
/* 39 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 45 */     this.description = setDescription(c);
/* 46 */     this.tips.clear();
/* 47 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 48 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onUsePotion()
/*    */   {
/* 53 */     flash();
/* 54 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 55 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 60 */     return new Test1();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Test1.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */