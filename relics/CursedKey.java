/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CursedKey extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Cursed Key";
/*    */   
/*    */   public CursedKey()
/*    */   {
/* 16 */     super("Cursed Key", "cursedKey.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     if (AbstractDungeon.player != null) {
/* 22 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 24 */     return setDescription(null);
/*    */   }
/*    */   
/*    */ 
/*    */   public void justEnteredRoom(AbstractRoom room)
/*    */   {
/* 30 */     if ((room instanceof com.megacrit.cardcrawl.rooms.TreasureRoom)) {
/* 31 */       flash();
/* 32 */       this.pulse = true;
/*    */     } else {
/* 34 */       this.pulse = false;
/*    */     }
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c) {
/* 39 */     if (c == null) {
/* 40 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     }
/* 42 */     switch (c) {
/*    */     case IRONCLAD: 
/* 44 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     case THE_SILENT: 
/* 46 */       return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0];
/*    */     case DEFECT: 
/* 48 */       return this.DESCRIPTIONS[3] + this.DESCRIPTIONS[0];
/*    */     }
/* 50 */     return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */   public void onChestOpen(boolean bossChest)
/*    */   {
/* 56 */     if (!bossChest) {
/* 57 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*    */       
/* 59 */         AbstractDungeon.returnRandomCurse(), com.megacrit.cardcrawl.core.Settings.WIDTH / 2, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2));
/*    */       
/*    */ 
/* 62 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 68 */     this.description = setDescription(c);
/* 69 */     this.tips.clear();
/* 70 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 71 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 76 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 81 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 86 */     return new CursedKey();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\CursedKey.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */