/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class Waffle extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Lee's Waffle";
/*    */   private static final int HP_AMT = 7;
/*    */   
/*    */   public Waffle() {
/* 11 */     super("Lee's Waffle", "waffle.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0] + 7 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxHp(7, false);
/* 22 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.heal(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.maxHealth);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 27 */     return new Waffle();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Waffle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */