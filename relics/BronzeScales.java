/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BronzeScales extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Bronze Scales";
/*    */   private static final int DAMAGE = 3;
/*    */   
/*    */   public BronzeScales()
/*    */   {
/* 13 */     super("Bronze Scales", "bronzeScales.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.ThornsPower(AbstractDungeon.player, 3), 3));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 34 */     return new BronzeScales();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BronzeScales.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */