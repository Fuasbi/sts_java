/*    */ package com.megacrit.cardcrawl.relics.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class FrozenEyeOLD extends AbstractRelic {
/*    */   public static final String ID = "Frozen Eye OLD";
/*    */   
/*    */   public FrozenEyeOLD() {
/*  9 */     super("Frozen Eye OLD", "frozenEye.png", com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier.STARTER, com.megacrit.cardcrawl.relics.AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 19 */     return new FrozenEyeOLD();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\deprecated\FrozenEyeOLD.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */