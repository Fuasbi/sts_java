/*    */ package com.megacrit.cardcrawl.relics.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class DerpRock extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Derp Rock";
/*    */   public static final int CHARGE_AMT = 1;
/*    */   
/*    */   public DerpRock()
/*    */   {
/* 13 */     super("Derp Rock", "derpRock.png", com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier.STARTER, com.megacrit.cardcrawl.relics.AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 1 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void atPreBattle()
/*    */   {
/* 24 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 29 */     return new DerpRock();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\deprecated\DerpRock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */