/*    */ package com.megacrit.cardcrawl.relics.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class NullstonePeriapt extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Nullstone Periapt";
/*    */   private static final int NUM_TURNS = 10;
/*    */   
/*    */   public NullstonePeriapt()
/*    */   {
/* 12 */     super("Nullstone Periapt", "nullStone.png", com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier.RARE, com.megacrit.cardcrawl.relics.AbstractRelic.LandingSound.MAGICAL);
/* 13 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 23 */     this.counter = -1;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 28 */     if (this.counter != -1) {
/* 29 */       this.pulse = false;
/* 30 */       this.counter += 1;
/*    */     }
/*    */     
/* 33 */     if (this.counter == 10) {
/* 34 */       this.counter = -1;
/* 35 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/nullStone.png");
/* 36 */       flash();
/* 37 */       if (!this.pulse) {
/* 38 */         beginPulse();
/* 39 */         this.pulse = true;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 46 */     if (this.counter == -1) {
/* 47 */       beginPulse();
/* 48 */       this.pulse = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 54 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public void setCounter(int counter)
/*    */   {
/* 59 */     this.counter = counter;
/* 60 */     if (counter != -1) {
/* 61 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/nullStoneUsed.png");
/*    */     } else {
/* 63 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/nullStone.png");
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 69 */     return new NullstonePeriapt();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\deprecated\NullstonePeriapt.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */