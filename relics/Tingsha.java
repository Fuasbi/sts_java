/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageRandomEnemyAction;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Tingsha extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Tingsha";
/*    */   private static final int DMG_AMT = 3;
/*    */   
/*    */   public Tingsha()
/*    */   {
/* 19 */     super("Tingsha", "tingsha.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 24 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onManualDiscard()
/*    */   {
/* 29 */     flash();
/* 30 */     CardCrawlGame.sound.play("TINGSHA");
/* 31 */     AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 32 */     AbstractDungeon.actionManager.addToBottom(new DamageRandomEnemyAction(new DamageInfo(AbstractDungeon.player, 3, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 40 */     super.update();
/*    */     
/* 42 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/* 43 */       CardCrawlGame.sound.playA("TINGSHA", MathUtils.random(-0.2F, 0.1F));
/* 44 */       flash();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 50 */     return new Tingsha();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Tingsha.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */