/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class EmotionChip extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Emotion Chip";
/*    */   
/*    */   public EmotionChip()
/*    */   {
/* 11 */     super("Emotion Chip", "emotionChip.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/* 12 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 22 */     if (this.pulse) {
/* 23 */       this.pulse = false;
/* 24 */       flash();
/* 25 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.ImpulseAction());
/*    */     }
/*    */   }
/*    */   
/*    */   public void onLoseHp(int damageAmount)
/*    */   {
/* 31 */     if ((AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && 
/* 32 */       (damageAmount > 0)) {
/* 33 */       flash();
/* 34 */       if (!this.pulse) {
/* 35 */         beginPulse();
/* 36 */         this.pulse = true;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 44 */     return new EmotionChip();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\EmotionChip.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */