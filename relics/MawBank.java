/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class MawBank extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "MawBank";
/*    */   private static final int GOLD_AMT = 12;
/*    */   
/*    */   public MawBank()
/*    */   {
/* 13 */     super("MawBank", "bank.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 12 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 23 */     if (!this.usedUp) {
/* 24 */       flash();
/* 25 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainGold(12);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onSpendGold()
/*    */   {
/* 31 */     if (!this.usedUp) {
/* 32 */       flash();
/* 33 */       setCounter(-2);
/*    */     }
/*    */   }
/*    */   
/*    */   public void setCounter(int counter)
/*    */   {
/* 39 */     this.counter = counter;
/* 40 */     if (counter == -2) {
/* 41 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/usedBank.png");
/* 42 */       usedUp();
/* 43 */       this.counter = -2;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 49 */     return new MawBank();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MawBank.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */