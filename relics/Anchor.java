/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Anchor extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Anchor";
/*    */   private static final int BLOCK_AMT = 10;
/*    */   
/*    */   public Anchor()
/*    */   {
/* 13 */     super("Anchor", "anchor.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, 10));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 31 */     return new Anchor();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Anchor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */