/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class RunicCapacitor extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Runic Capacitor";
/* 10 */   private boolean firstTurn = true;
/*    */   
/*    */   public RunicCapacitor() {
/* 13 */     super("Runic Capacitor", "runicCapacitor.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 23 */     this.firstTurn = true;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 28 */     if (this.firstTurn) {
/* 29 */       flash();
/* 30 */       AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 31 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.IncreaseMaxOrbAction(3));
/* 32 */       this.firstTurn = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 38 */     return new RunicCapacitor();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RunicCapacitor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */