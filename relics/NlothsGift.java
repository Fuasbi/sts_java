/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class NlothsGift extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Nloth's Gift";
/*    */   public static final float MULTIPLIER = 3.0F;
/*    */   
/*    */   public NlothsGift() {
/*  9 */     super("Nloth's Gift", "nlothsGift.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 19 */     return new NlothsGift();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\NlothsGift.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */