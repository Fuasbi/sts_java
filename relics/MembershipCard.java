/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class MembershipCard extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Membership Card";
/*    */   public static final float MULTIPLIER = 0.5F;
/*    */   public static final float ROOM_MULTIPLIER = 1.5F;
/*    */   
/*    */   public MembershipCard()
/*    */   {
/* 13 */     super("Membership Card", "membershipCard.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 23 */     if ((room instanceof com.megacrit.cardcrawl.rooms.ShopRoom)) {
/* 24 */       flash();
/* 25 */       this.pulse = true;
/*    */     } else {
/* 27 */       this.pulse = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new MembershipCard();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MembershipCard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */