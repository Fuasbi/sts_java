/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class DreamCatcher extends AbstractRelic {
/*    */   public static final String ID = "Dream Catcher";
/*    */   
/*    */   public DreamCatcher() {
/*  7 */     super("Dream Catcher", "dreamCatcher.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new DreamCatcher();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\DreamCatcher.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */