/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class RingOfTheSerpent extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Ring of the Serpent";
/*    */   
/*    */   public RingOfTheSerpent() {
/* 10 */     super("Ring of the Serpent", "serpentRing.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 20 */     flash();
/* 21 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new RingOfTheSerpent();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RingOfTheSerpent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */