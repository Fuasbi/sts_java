/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BlackBlood extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Black Blood";
/*    */   private static final int HEALTH_AMT = 10;
/*    */   
/*    */   public BlackBlood()
/*    */   {
/* 13 */     super("Black Blood", "blackBlood.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractPlayer p = AbstractDungeon.player;
/* 26 */     if (p.currentHealth > 0) {
/* 27 */       p.heal(10);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new BlackBlood();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BlackBlood.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */