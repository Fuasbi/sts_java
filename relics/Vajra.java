/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Vajra extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Vajra";
/*    */   private static final int STR = 1;
/*    */   
/*    */   public Vajra()
/*    */   {
/* 14 */     super("Vajra", "vajra.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 1 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     flash();
/* 25 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, 1), 1));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 31 */     AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 36 */     return new Vajra();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Vajra.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */