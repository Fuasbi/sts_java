/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BagOfPreparation extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Bag of Preparation";
/*    */   private static final int NUM_CARDS = 2;
/*    */   
/*    */   public BagOfPreparation()
/*    */   {
/* 13 */     super("Bag of Preparation", "bagOfPreparation.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 2 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 2));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 30 */     return new BagOfPreparation();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BagOfPreparation.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */