/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class Strawberry extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Strawberry";
/*    */   private static final int HP_AMT = 7;
/*    */   
/*    */   public Strawberry() {
/* 11 */     super("Strawberry", "strawberry.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0] + 7 + ".";
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxHp(7, true);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new Strawberry();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Strawberry.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */