/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.orbs.Lightning;
/*    */ 
/*    */ public class CrackedCore extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Cracked Core";
/*    */   
/*    */   public CrackedCore() {
/* 10 */     super("Cracked Core", "crackedOrb.png", AbstractRelic.RelicTier.STARTER, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 20 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.channelOrb(new Lightning());
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 25 */     return new CrackedCore();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\CrackedCore.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */