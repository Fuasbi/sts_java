/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class HandDrill extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "HandDrill";
/*    */   public static final int VULNERABLE_AMT = 2;
/*    */   
/*    */   public HandDrill()
/*    */   {
/* 15 */     super("HandDrill", "drill.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onBlockBroken(AbstractCreature m)
/*    */   {
/* 25 */     flash();
/* 26 */     AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(m, this));
/* 27 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.VulnerablePower(m, 2, false), 2));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 37 */     return new HandDrill();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\HandDrill.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */