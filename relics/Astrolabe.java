/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ public class Astrolabe extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Astrolabe";
/* 16 */   private boolean cardsSelected = true;
/*    */   
/*    */   public Astrolabe() {
/* 19 */     super("Astrolabe", "astrolabe.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 24 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 29 */     this.cardsSelected = false;
/* 30 */     CardGroup tmp = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/* 31 */     for (AbstractCard card : AbstractDungeon.player.masterDeck.group) {
/* 32 */       if ((!card.cardID.equals("Necronomicurse")) && (!card.cardID.equals("AscendersBane"))) {
/* 33 */         tmp.addToTop(card);
/*    */       }
/*    */     }
/*    */     
/* 37 */     if (tmp.group.isEmpty()) {
/* 38 */       this.cardsSelected = true;
/* 39 */       return; }
/* 40 */     if (tmp.group.size() <= 3) {
/* 41 */       AbstractDungeon.player.masterDeck.group.clear();
/* 42 */       for (AbstractCard card : tmp.group) {
/* 43 */         AbstractDungeon.player.masterDeck.removeCard(card);
/* 44 */         AbstractDungeon.transformCard(card, true, AbstractDungeon.miscRng);
/*    */       }
/* 46 */       this.cardsSelected = true;
/*    */     }
/* 48 */     else if (!AbstractDungeon.isScreenUp) {
/* 49 */       AbstractDungeon.gridSelectScreen.open(tmp, 3, this.DESCRIPTIONS[1] + this.name + ".", false, false, false, false);
/*    */     } else {
/* 51 */       AbstractDungeon.dynamicBanner.hide();
/* 52 */       AbstractDungeon.previousScreen = AbstractDungeon.screen;
/* 53 */       AbstractDungeon.gridSelectScreen.open(tmp, 3, this.DESCRIPTIONS[1] + this.name + ".", false, false, false, false);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 60 */     super.update();
/* 61 */     if ((!this.cardsSelected) && 
/* 62 */       (AbstractDungeon.gridSelectScreen.selectedCards.size() == 3)) {
/* 63 */       this.cardsSelected = true;
/* 64 */       float displayCount = 0.0F;
/* 65 */       Iterator<AbstractCard> i = AbstractDungeon.gridSelectScreen.selectedCards.iterator();
/* 66 */       while (i.hasNext()) {
/* 67 */         AbstractCard card = (AbstractCard)i.next();
/* 68 */         card.untip();
/* 69 */         card.unhover();
/* 70 */         AbstractDungeon.player.masterDeck.removeCard(card);
/* 71 */         AbstractDungeon.transformCard(card, true, AbstractDungeon.miscRng);
/*    */         
/* 73 */         if ((AbstractDungeon.screen != AbstractDungeon.CurrentScreen.TRANSFORM) && (AbstractDungeon.transformedCard != null))
/*    */         {
/* 75 */           AbstractDungeon.topLevelEffectsQueue.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*    */           
/* 77 */             AbstractDungeon.getTransformedCard(), Settings.WIDTH / 3.0F + displayCount, Settings.HEIGHT / 2.0F, false));
/*    */           
/*    */ 
/*    */ 
/* 81 */           displayCount += Settings.WIDTH / 6.0F;
/*    */         }
/*    */       }
/* 84 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 85 */       AbstractDungeon.getCurrRoom().rewardPopOutTimer = 0.25F;
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 92 */     return new Astrolabe();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Astrolabe.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */