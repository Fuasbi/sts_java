/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class ChemicalX extends AbstractRelic {
/*    */   public static final String ID = "Chemical X";
/*    */   public static final int BOOST = 2;
/*    */   
/*    */   public ChemicalX() {
/*  8 */     super("Chemical X", "chemicalX.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 13 */     return this.DESCRIPTIONS[0] + 2 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 18 */     return new ChemicalX();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ChemicalX.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */