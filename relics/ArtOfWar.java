/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ArtOfWar extends AbstractRelic
/*    */ {
/* 15 */   private boolean firstTurn = false; private boolean gainEnergyNext = false;
/*    */   public static final String ID = "Art of War";
/*    */   
/* 18 */   public ArtOfWar() { super("Art of War", "artOfWar.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/* 19 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 24 */     if (AbstractDungeon.player != null) {
/* 25 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 27 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 32 */     if (c == null) {
/* 33 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     }
/* 35 */     switch (c) {
/*    */     case IRONCLAD: 
/* 37 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 39 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 41 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[3];
/*    */     }
/* 43 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 49 */     this.description = setDescription(c);
/* 50 */     this.tips.clear();
/* 51 */     this.tips.add(new PowerTip(this.name, this.description));
/* 52 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 57 */     flash();
/* 58 */     this.firstTurn = true;
/* 59 */     this.gainEnergyNext = true;
/* 60 */     if (!this.pulse) {
/* 61 */       beginPulse();
/* 62 */       this.pulse = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 68 */     beginPulse();
/* 69 */     this.pulse = true;
/* 70 */     if ((this.gainEnergyNext) && (!this.firstTurn)) {
/* 71 */       flash();
/* 72 */       AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 73 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/*    */     }
/* 75 */     this.firstTurn = false;
/* 76 */     this.gainEnergyNext = true;
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 81 */     if (card.type == AbstractCard.CardType.ATTACK) {
/* 82 */       this.gainEnergyNext = false;
/* 83 */       this.pulse = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 89 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 94 */     return new ArtOfWar();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ArtOfWar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */