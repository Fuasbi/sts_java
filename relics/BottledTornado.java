/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class BottledTornado extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Bottled Tornado";
/* 13 */   private boolean cardSelected = true;
/* 14 */   public AbstractCard card = null;
/*    */   
/*    */   public BottledTornado() {
/* 17 */     super("Bottled Tornado", "bottledTornado.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 22 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractCard getCard() {
/* 26 */     return this.card.makeCopy();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 31 */     this.cardSelected = false;
/* 32 */     if (AbstractDungeon.isScreenUp) {
/* 33 */       AbstractDungeon.dynamicBanner.hide();
/* 34 */       AbstractDungeon.overlayMenu.cancelButton.hide();
/* 35 */       AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*    */     }
/* 37 */     AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.INCOMPLETE;
/* 38 */     AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 39 */       .getPowers(), 1, this.DESCRIPTIONS[1] + this.name + ".", false, false, false, false);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onUnequip()
/*    */   {
/* 50 */     if (this.card != null) {
/* 51 */       AbstractCard cardInDeck = AbstractDungeon.player.masterDeck.getSpecificCard(this.card);
/* 52 */       if (cardInDeck != null) {
/* 53 */         cardInDeck.inBottleTornado = false;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 60 */     super.update();
/* 61 */     if ((!this.cardSelected) && 
/* 62 */       (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 63 */       this.cardSelected = true;
/* 64 */       this.card = ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 65 */       this.card.inBottleTornado = true;
/* 66 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/*    */       
/* 68 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 69 */       this.description = (this.DESCRIPTIONS[2] + FontHelper.colorString(this.card.name, "y") + this.DESCRIPTIONS[3]);
/* 70 */       this.tips.clear();
/* 71 */       this.tips.add(new PowerTip(this.name, this.description));
/* 72 */       initializeTips();
/*    */     }
/*    */   }
/*    */   
/*    */   public void setDescriptionAfterLoading()
/*    */   {
/* 78 */     this.description = (this.DESCRIPTIONS[2] + FontHelper.colorString(this.card.name, "y") + this.DESCRIPTIONS[3]);
/* 79 */     this.tips.clear();
/* 80 */     this.tips.add(new PowerTip(this.name, this.description));
/* 81 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 86 */     flash();
/* 87 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 92 */     return new BottledTornado();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BottledTornado.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */