/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.CardQueueItem;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class MummifiedHand extends AbstractRelic
/*    */ {
/* 16 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(MummifiedHand.class.getName());
/*    */   public static final String ID = "Mummified Hand";
/*    */   
/*    */   public MummifiedHand() {
/* 20 */     super("Mummified Hand", "mummifiedHand.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 25 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 30 */     if (card.type == AbstractCard.CardType.POWER) {
/* 31 */       flash();
/* 32 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */       
/* 34 */       ArrayList<AbstractCard> groupCopy = new ArrayList();
/* 35 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 36 */         if ((c.cost > 0) && (c.costForTurn > 0)) {
/* 37 */           groupCopy.add(c);
/*    */         } else {
/* 39 */           logger.info("COST IS 0: " + c.name);
/*    */         }
/*    */       }
/*    */       
/* 43 */       for (??? = AbstractDungeon.actionManager.cardQueue.iterator(); ???.hasNext();) { i = (CardQueueItem)???.next();
/* 44 */         if (i.card != null) {
/* 45 */           logger.info("INVALID: " + i.card.name);
/* 46 */           groupCopy.remove(i.card);
/*    */         }
/*    */       }
/*    */       CardQueueItem i;
/* 50 */       AbstractCard c = null;
/* 51 */       if (!groupCopy.isEmpty()) {
/* 52 */         logger.info("VALID CARDS: ");
/* 53 */         for (AbstractCard cc : groupCopy) {
/* 54 */           logger.info(cc.name);
/*    */         }
/*    */         
/* 57 */         c = (AbstractCard)groupCopy.get(AbstractDungeon.cardRandomRng.random(0, groupCopy.size() - 1));
/*    */       } else {
/* 59 */         logger.info("NO VALID CARDS");
/*    */       }
/*    */       
/* 62 */       if (c != null) {
/* 63 */         logger.info("Mummified hand: " + c.name);
/* 64 */         c.setCostForTurn(0);
/*    */       } else {
/* 66 */         logger.info("ERROR: MUMMIFIED HAND NOT WORKING");
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 73 */     return new MummifiedHand();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MummifiedHand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */