/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*    */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CallingBell extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Calling Bell";
/* 16 */   private boolean cardsReceived = true;
/*    */   
/*    */   public CallingBell() {
/* 19 */     super("Calling Bell", "bell.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 24 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 29 */     this.cardsReceived = false;
/* 30 */     CardGroup group = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/*    */     
/* 32 */     for (int i = 0; i < 3; i++) {
/* 33 */       if ((AbstractDungeon.player.hasRelic("Omamori")) && (AbstractDungeon.player.getRelic("Omamori").counter != 0))
/*    */       {
/* 35 */         ((Omamori)AbstractDungeon.player.getRelic("Omamori")).use();
/*    */       } else {
/* 37 */         AbstractCard bellCurse = AbstractDungeon.getCard(com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.CURSE);
/* 38 */         com.megacrit.cardcrawl.unlock.UnlockTracker.markCardAsSeen(bellCurse.cardID);
/* 39 */         group.addToBottom(bellCurse.makeCopy());
/*    */       }
/*    */     }
/*    */     
/* 43 */     AbstractDungeon.gridSelectScreen.openConfirmationGrid(group, this.DESCRIPTIONS[1]);
/* 44 */     CardCrawlGame.sound.playA("BELL", MathUtils.random(-0.2F, -0.3F));
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 49 */     super.update();
/* 50 */     if ((!this.cardsReceived) && (!AbstractDungeon.isScreenUp)) {
/* 51 */       AbstractDungeon.combatRewardScreen.open();
/* 52 */       AbstractDungeon.combatRewardScreen.rewards.clear();
/*    */       
/* 54 */       AbstractDungeon.combatRewardScreen.rewards.add(new RewardItem(
/* 55 */         AbstractDungeon.returnRandomScreenlessRelic(AbstractRelic.RelicTier.COMMON)));
/* 56 */       AbstractDungeon.combatRewardScreen.rewards.add(new RewardItem(
/* 57 */         AbstractDungeon.returnRandomScreenlessRelic(AbstractRelic.RelicTier.UNCOMMON)));
/* 58 */       AbstractDungeon.combatRewardScreen.rewards.add(new RewardItem(
/* 59 */         AbstractDungeon.returnRandomScreenlessRelic(AbstractRelic.RelicTier.RARE)));
/*    */       
/* 61 */       AbstractDungeon.combatRewardScreen.positionRewards();
/* 62 */       AbstractDungeon.overlayMenu.proceedButton.setLabel(this.DESCRIPTIONS[2]);
/*    */       
/* 64 */       this.cardsReceived = true;
/* 65 */       AbstractDungeon.getCurrRoom().rewardPopOutTimer = 0.25F;
/*    */     }
/*    */     
/* 68 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/* 69 */       CardCrawlGame.sound.playA("BELL", MathUtils.random(-0.2F, -0.3F));
/* 70 */       flash();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 76 */     return new CallingBell();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\CallingBell.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */