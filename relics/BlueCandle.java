/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class BlueCandle extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Blue Candle";
/*    */   public static final int HP_LOSS = 1;
/*    */   
/*    */   public BlueCandle() {
/*  9 */     super("Blue Candle", "blueCandle.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0] + 1 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 19 */     return new BlueCandle();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BlueCandle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */