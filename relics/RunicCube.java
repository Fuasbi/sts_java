/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class RunicCube extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Runic Cube";
/*    */   private static final int NUM_CARDS = 1;
/*    */   
/*    */   public RunicCube()
/*    */   {
/* 14 */     super("Runic Cube", "runicCube.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 1 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onLoseHp(int damageAmount)
/*    */   {
/* 24 */     if ((AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && 
/* 25 */       (damageAmount > 0)) {
/* 26 */       flash();
/* 27 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/* 28 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 35 */     return new RunicCube();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RunicCube.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */