/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class StoneCalendar extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "StoneCalendar";
/*    */   private static final int TURNS = 7;
/*    */   private static final int DMG = 40;
/*    */   
/*    */   public StoneCalendar()
/*    */   {
/* 15 */     super("StoneCalendar", "calendar.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 7 + this.DESCRIPTIONS[1] + 40 + this.DESCRIPTIONS[2];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 30 */     this.counter += 1;
/* 31 */     if (this.counter == 6) {
/* 32 */       beginLongPulse();
/* 33 */     } else if (this.counter == 7) {
/* 34 */       flash();
/* 35 */       AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 36 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */       
/*    */ 
/* 39 */         com.megacrit.cardcrawl.cards.DamageInfo.createDamageMatrix(40, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       
/*    */ 
/* 42 */       stopPulse();
/*    */     }
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 48 */     this.counter = -1;
/* 49 */     stopPulse();
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 54 */     return new StoneCalendar();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\StoneCalendar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */