/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class NlothsMask extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "NlothsMask";
/*    */   
/*    */   public NlothsMask()
/*    */   {
/* 11 */     super("NlothsMask", "nloth.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.FLAT);
/* 12 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void onChestOpenAfter(boolean bossChest)
/*    */   {
/* 17 */     if ((!bossChest) && 
/* 18 */       (this.counter > 0)) {
/* 19 */       this.counter -= 1;
/*    */       
/* 21 */       flash();
/* 22 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 23 */       AbstractDungeon.getCurrRoom().removeRelicFromRewards();
/*    */       
/* 25 */       if (this.counter == 0) {
/* 26 */         setCounter(-2);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void setCounter(int counter)
/*    */   {
/* 34 */     this.counter = counter;
/* 35 */     if (counter == -2) {
/* 36 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/nlothUsed.png");
/* 37 */       usedUp();
/* 38 */       this.counter = -2;
/*    */     }
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 44 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 49 */     return new NlothsMask();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\NlothsMask.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */