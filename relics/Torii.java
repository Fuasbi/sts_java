/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Torii extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Torii";
/*    */   private static final int THRESHOLD = 5;
/*    */   
/*    */   public Torii()
/*    */   {
/* 14 */     super("Torii", "torii.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 5 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 24 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.type != DamageInfo.DamageType.THORNS) && (damageAmount > 1) && (damageAmount <= 5))
/*    */     {
/* 26 */       flash();
/* 27 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 28 */       return 1;
/*    */     }
/* 30 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 35 */     return new Torii();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Torii.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */