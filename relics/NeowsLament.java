/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class NeowsLament extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "NeowsBlessing";
/*    */   
/*    */   public NeowsLament()
/*    */   {
/* 13 */     super("NeowsBlessing", "lament.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.FLAT);
/* 14 */     this.counter = 3;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     if (this.counter > 0) {
/* 25 */       this.counter -= 1;
/* 26 */       if (this.counter == 0) {
/* 27 */         this.counter = -2;
/* 28 */         this.description = this.DESCRIPTIONS[1];
/* 29 */         this.tips.clear();
/* 30 */         this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 31 */         initializeTips();
/*    */       }
/* 33 */       flash();
/* 34 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 35 */         m.currentHealth = 1;
/* 36 */         m.healthBarUpdatedEvent();
/*    */       }
/* 38 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void setCounter(int counter)
/*    */   {
/* 45 */     this.counter = counter;
/* 46 */     if (counter == -2) {
/* 47 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/lamentUsed.png");
/* 48 */       usedUp();
/* 49 */       this.counter = -2;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 55 */     return new NeowsLament();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\NeowsLament.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */