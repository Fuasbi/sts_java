/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Pantograph extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Pantograph";
/*    */   private static final int HEAL_AMT = 25;
/*    */   
/*    */   public Pantograph()
/*    */   {
/* 14 */     super("Pantograph", "pantograph.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 25 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 25 */       if (m.type == com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS) {
/* 26 */         flash();
/* 27 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 28 */         AbstractDungeon.player.heal(25, true);
/* 29 */         return;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 36 */     return new Pantograph();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Pantograph.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */