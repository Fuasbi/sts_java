/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class PotionBelt extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Potion Belt";
/*    */   
/*    */   public PotionBelt() {
/* 10 */     super("Potion Belt", "potion_belt.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 20 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.potionSlots += 2;
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.potions.add(new com.megacrit.cardcrawl.potions.PotionSlot(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.potionSlots - 2));
/* 22 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.potions.add(new com.megacrit.cardcrawl.potions.PotionSlot(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.potionSlots - 1));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 27 */     return new PotionBelt();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\PotionBelt.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */