/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ClockworkSouvenir extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "ClockworkSouvenir";
/*    */   
/*    */   public ClockworkSouvenir()
/*    */   {
/* 11 */     super("ClockworkSouvenir", "clockwork.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 21 */     flash();
/* 22 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.ArtifactPower(AbstractDungeon.player, 1), 1));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 32 */     return new ClockworkSouvenir();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ClockworkSouvenir.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */