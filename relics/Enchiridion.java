/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class Enchiridion extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Enchiridion";
/*    */   public static final float RARE_CHANCE = 0.5F;
/*    */   
/*    */   public Enchiridion()
/*    */   {
/* 15 */     super("Enchiridion", "enchiridion.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     flash();
/*    */     
/*    */ 
/* 28 */     AbstractCard c = AbstractDungeon.returnTrulyRandomCard(com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER, AbstractDungeon.cardRandomRng).makeCopy();
/* 29 */     if (c.cost != -1) {
/* 30 */       c.setCostForTurn(0);
/*    */     }
/* 32 */     UnlockTracker.markCardAsSeen(c.cardID);
/* 33 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(c));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 38 */     return new Enchiridion();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Enchiridion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */