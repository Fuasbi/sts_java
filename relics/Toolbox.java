/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Toolbox extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Toolbox";
/*    */   
/*    */   public Toolbox()
/*    */   {
/* 11 */     super("Toolbox", "toolbox.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStartPreDraw()
/*    */   {
/* 21 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(
/* 22 */       AbstractDungeon.returnTrulyRandomColorlessCard().makeCopy(), false));
/* 23 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 28 */     return new Toolbox();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Toolbox.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */