/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Orichalcum extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Orichalcum";
/*    */   private static final int BLOCK_AMT = 6;
/* 11 */   public boolean trigger = false;
/*    */   
/*    */   public Orichalcum() {
/* 14 */     super("Orichalcum", "orichalcum.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 24 */     if ((AbstractDungeon.player.currentBlock == 0) || (this.trigger)) {
/* 25 */       this.trigger = false;
/* 26 */       flash();
/* 27 */       stopPulse();
/* 28 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, 6));
/*    */       
/* 30 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 36 */     this.trigger = false;
/* 37 */     if (AbstractDungeon.player.currentBlock == 0) {
/* 38 */       beginLongPulse();
/*    */     }
/*    */   }
/*    */   
/*    */   public int onPlayerGainedBlock(float blockAmount)
/*    */   {
/* 44 */     if (blockAmount > 0.0F) {
/* 45 */       stopPulse();
/*    */     }
/*    */     
/* 48 */     return com.badlogic.gdx.math.MathUtils.floor(blockAmount);
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 53 */     stopPulse();
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 58 */     return new Orichalcum();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Orichalcum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */