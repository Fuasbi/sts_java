/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MercuryHourglass extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Mercury Hourglass";
/*    */   private static final int DMG = 3;
/*    */   
/*    */   public MercuryHourglass()
/*    */   {
/* 16 */     super("Mercury Hourglass", "hourglass.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 26 */     flash();
/* 27 */     AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 28 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */     
/*    */ 
/* 31 */       DamageInfo.createDamageMatrix(3, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 38 */     return new MercuryHourglass();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MercuryHourglass.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */