/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class HappyFlower extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Happy Flower";
/*    */   private static final int NUM_TURNS = 3;
/*    */   private static final int ENERGY_AMT = 1;
/*    */   
/*    */   public HappyFlower()
/*    */   {
/* 16 */     super("Happy Flower", "sunflower.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     if (AbstractDungeon.player != null) {
/* 22 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 24 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 29 */     if (c == null) {
/* 30 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */     }
/* 32 */     switch (c) {
/*    */     case IRONCLAD: 
/* 34 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 36 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 38 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[3];
/*    */     }
/* 40 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 46 */     this.description = setDescription(c);
/* 47 */     this.tips.clear();
/* 48 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 49 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 54 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 59 */     if (this.counter == -1) {
/* 60 */       this.counter += 2;
/*    */     } else {
/* 62 */       this.counter += 1;
/*    */     }
/*    */     
/* 65 */     if (this.counter == 3) {
/* 66 */       this.counter = 0;
/* 67 */       flash();
/* 68 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 69 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 75 */     return new HappyFlower();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\HappyFlower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */