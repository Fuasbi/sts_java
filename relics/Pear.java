/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class Pear extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Pear";
/*    */   private static final int HP_AMT = 10;
/*    */   
/*    */   public Pear() {
/* 11 */     super("Pear", "pear.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0] + 10 + ".";
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxHp(10, true);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new Pear();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Pear.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */