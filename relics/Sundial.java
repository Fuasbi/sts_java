/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Sundial extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Sundial";
/*    */   private static final int NUM_TURNS = 3;
/*    */   private static final int ENERGY_AMT = 2;
/*    */   
/*    */   public Sundial()
/*    */   {
/* 15 */     super("Sundial", "sundial.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     if (AbstractDungeon.player != null) {
/* 21 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 23 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 28 */     if (c == null) {
/* 29 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */     }
/* 31 */     switch (c) {
/*    */     case IRONCLAD: 
/* 33 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 35 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 37 */       return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[3];
/*    */     }
/* 39 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void onEquip()
/*    */   {
/* 45 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void onShuffle()
/*    */   {
/* 50 */     this.counter += 1;
/*    */     
/* 52 */     if (this.counter == 3) {
/* 53 */       this.counter = 0;
/* 54 */       flash();
/* 55 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 56 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(2));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 62 */     return new Sundial();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Sundial.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */