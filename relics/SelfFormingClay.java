/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class SelfFormingClay extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Self Forming Clay";
/*    */   private static final int BLOCK_AMT = 3;
/*    */   
/*    */   public SelfFormingClay()
/*    */   {
/* 15 */     super("Self Forming Clay", "clay.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 25 */     if ((AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && 
/* 26 */       (damageAmount > 0)) {
/* 27 */       flash();
/* 28 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.NextTurnBlockPower(AbstractDungeon.player, 3, this.name), 3));
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 41 */     return new SelfFormingClay();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SelfFormingClay.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */