/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.rooms.EventRoom;
/*    */ 
/*    */ public class SsserpentHead extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "SsserpentHead";
/*    */   private static final int GOLD_AMT = 50;
/*    */   
/*    */   public SsserpentHead()
/*    */   {
/* 13 */     super("SsserpentHead", "serpentHead.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 50 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 23 */     if ((room instanceof EventRoom)) {
/* 24 */       flash();
/* 25 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainGold(50);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 31 */     return new SsserpentHead();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SsserpentHead.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */