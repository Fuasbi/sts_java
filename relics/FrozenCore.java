/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class FrozenCore extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "FrozenCore";
/*    */   
/*    */   public FrozenCore() {
/* 10 */     super("FrozenCore", "frozenOrb.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 20 */     if (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hasEmptyOrb()) {
/* 21 */       flash();
/* 22 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.channelOrb(new com.megacrit.cardcrawl.orbs.Frost());
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 28 */     return new FrozenCore();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\FrozenCore.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */