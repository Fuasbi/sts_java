/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PandorasBox
/*    */   extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Pandora's Box";
/* 21 */   private int count = 0;
/* 22 */   private boolean calledTransform = true;
/*    */   
/*    */   public PandorasBox() {
/* 25 */     super("Pandora's Box", "pandorasBox.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 30 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 35 */     this.calledTransform = false;
/*    */     
/* 37 */     for (Iterator<AbstractCard> i = AbstractDungeon.player.masterDeck.group.iterator(); i.hasNext();) {
/* 38 */       AbstractCard e = (AbstractCard)i.next();
/* 39 */       if ((e.cardID.equals("Strike_R")) || (e.cardID.equals("Strike_G")) || (e.cardID.equals("Strike_B")) || 
/* 40 */         (e.cardID.equals("Defend_R")) || (e.cardID.equals("Defend_G")) || (e.cardID.equals("Defend_B")))
/*    */       {
/* 42 */         i.remove();
/* 43 */         this.count += 1;
/*    */       }
/*    */     }
/*    */     
/* 47 */     if (this.count > 0) {
/* 48 */       CardGroup group = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/* 49 */       for (int i = 0; i < this.count; i++)
/*    */       {
/*    */ 
/* 52 */         AbstractCard card = AbstractDungeon.getCard(AbstractDungeon.rollRarity(AbstractDungeon.miscRng), AbstractDungeon.miscRng).makeStatEquivalentCopy();
/* 53 */         UnlockTracker.markCardAsSeen(card.cardID);
/* 54 */         card.isSeen = true;
/* 55 */         group.addToBottom(card);
/*    */       }
/* 57 */       AbstractDungeon.gridSelectScreen.openConfirmationGrid(group, this.DESCRIPTIONS[1]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 63 */     super.update();
/* 64 */     if ((!this.calledTransform) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.GRID)) {
/* 65 */       this.calledTransform = true;
/* 66 */       AbstractDungeon.getCurrRoom().rewardPopOutTimer = 0.25F;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 72 */     return new PandorasBox();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\PandorasBox.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */