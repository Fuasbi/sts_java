/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class Courier extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "The Courier";
/*    */   public static final float MULTIPLIER = 0.8F;
/*    */   
/*    */   public Courier()
/*    */   {
/* 12 */     super("The Courier", "courier.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 22 */     if ((room instanceof com.megacrit.cardcrawl.rooms.ShopRoom)) {
/* 23 */       flash();
/* 24 */       this.pulse = true;
/*    */     } else {
/* 26 */       this.pulse = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 32 */     return new Courier();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Courier.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */