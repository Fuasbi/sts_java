/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class RedSkull extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Red Skull";
/*    */   private static final int STR_AMT = 3;
/* 15 */   private boolean isActive = false;
/*    */   
/*    */   public RedSkull() {
/* 18 */     super("Red Skull", "redSkull.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 23 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 28 */     this.isActive = false;
/* 29 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.AbstractGameAction()
/*    */     {
/*    */       public void update() {
/* 32 */         if ((!RedSkull.this.isActive) && (AbstractDungeon.player.isBloodied)) {
/* 33 */           RedSkull.this.flash();
/* 34 */           RedSkull.this.pulse = true;
/* 35 */           AbstractDungeon.player.addPower(new StrengthPower(AbstractDungeon.player, 3));
/* 36 */           AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, RedSkull.this));
/*    */           
/* 38 */           RedSkull.this.isActive = true;
/*    */         }
/* 40 */         this.isDone = true;
/*    */       }
/*    */     });
/*    */   }
/*    */   
/*    */ 
/*    */   public void onBloodied()
/*    */   {
/* 48 */     flash();
/* 49 */     this.pulse = true;
/* 50 */     if ((!this.isActive) && (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT)) {
/* 51 */       AbstractPlayer p = AbstractDungeon.player;
/* 52 */       AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(p, p, new StrengthPower(p, 3), 3));
/* 53 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 54 */       this.isActive = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void onNotBloodied()
/*    */   {
/* 60 */     this.pulse = false;
/* 61 */     if ((this.isActive) && (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT)) {
/* 62 */       AbstractPlayer p = AbstractDungeon.player;
/* 63 */       AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(p, p, new StrengthPower(p, -3), -3));
/*    */       
/* 65 */       this.isActive = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public int onPlayerHeal(int healAmount)
/*    */   {
/* 71 */     return super.onPlayerHeal(healAmount);
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 76 */     this.pulse = false;
/* 77 */     this.isActive = false;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 82 */     return new RedSkull();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RedSkull.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */