/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class Mango extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Mango";
/*    */   private static final int HP_AMT = 14;
/*    */   
/*    */   public Mango() {
/* 11 */     super("Mango", "mango.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0] + 14 + ".";
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 21 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxHp(14, true);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new Mango();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Mango.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */