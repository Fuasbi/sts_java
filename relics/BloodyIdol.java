/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BloodyIdol extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Bloody Idol";
/*    */   private static final int HEAL_AMOUNT = 5;
/*    */   
/*    */   public BloodyIdol()
/*    */   {
/* 12 */     super("Bloody Idol", "bloodyChalice.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0] + 5 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onGainGold()
/*    */   {
/* 22 */     flash();
/* 23 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 24 */     AbstractDungeon.player.heal(5, true);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 29 */     return new BloodyIdol();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BloodyIdol.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */