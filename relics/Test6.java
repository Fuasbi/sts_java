/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Test6 extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Test 6";
/*    */   private static final int GOLD_REQ = 100;
/*    */   private static final int BLOCK_AMT = 3;
/*    */   
/*    */   public Test6()
/*    */   {
/* 14 */     super("Test 6", "test6.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1] + 100 + this.DESCRIPTIONS[2];
/*    */   }
/*    */   
/*    */ 
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 25 */     if (hasEnoughGold()) {
/* 26 */       flash();
/* 27 */       this.pulse = false;
/* 28 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, 3 * (AbstractDungeon.player.gold / 100)));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/* 33 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 39 */     if (hasEnoughGold()) {
/* 40 */       this.pulse = true;
/* 41 */       beginPulse();
/*    */     }
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 47 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   private boolean hasEnoughGold() {
/* 51 */     return AbstractDungeon.player.gold >= 100;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 56 */     return new Test6();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Test6.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */