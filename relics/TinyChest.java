/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.helpers.EventHelper;
/*    */ 
/*    */ public class TinyChest extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Tiny Chest";
/*    */   public static final float MULTIPLIER = 0.1F;
/*    */   private static final int GOLD_AMT = 30;
/*    */   
/*    */   public TinyChest()
/*    */   {
/* 14 */     super("Tiny Chest", "tinyChest.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 30 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 24 */     this.description = this.DESCRIPTIONS[2];
/* 25 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("GOLD_GAIN");
/* 26 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainGold(30);
/* 27 */     EventHelper.TREASURE_CHANCE += 0.1F;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 32 */     return new TinyChest();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\TinyChest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */