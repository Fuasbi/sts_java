/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Shuriken extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Shuriken";
/*    */   private static final int STR_AMT = 1;
/*    */   private static final int NUM_CARDS = 3;
/*    */   
/*    */   public Shuriken()
/*    */   {
/* 18 */     super("Shuriken", "shuriken.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 23 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1] + 1 + this.DESCRIPTIONS[2];
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 28 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 33 */     if (card.type == AbstractCard.CardType.ATTACK) {
/* 34 */       this.counter += 1;
/*    */       
/* 36 */       if (this.counter % 3 == 0) {
/* 37 */         this.counter = 0;
/* 38 */         flash();
/* 39 */         AbstractDungeon.actionManager.addToBottom(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 40 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, 1), 1));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onVictory()
/*    */   {
/* 52 */     this.counter = -1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 57 */     return new Shuriken();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Shuriken.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */