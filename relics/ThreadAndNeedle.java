/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ThreadAndNeedle extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Thread and Needle";
/*    */   private static final int ARMOR_AMT = 4;
/*    */   
/*    */   public ThreadAndNeedle()
/*    */   {
/* 14 */     super("Thread and Needle", "threadAndNeedle.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 4 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     flash();
/* 25 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.PlatedArmorPower(AbstractDungeon.player, 4), 4));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 31 */     AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 36 */     return new ThreadAndNeedle();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ThreadAndNeedle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */