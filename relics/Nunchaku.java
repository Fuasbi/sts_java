/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Nunchaku extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Nunchaku";
/*    */   private static final int NUM_CARDS = 10;
/*    */   
/*    */   public Nunchaku()
/*    */   {
/* 16 */     super("Nunchaku", "nunchaku.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/* 17 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 22 */     if (AbstractDungeon.player != null) {
/* 23 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 25 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 30 */     if (c == null) {
/* 31 */       return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[1];
/*    */     }
/*    */     
/* 34 */     switch (c) {
/*    */     case IRONCLAD: 
/* 36 */       return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 38 */       return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 40 */       return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[3];
/*    */     }
/* 42 */     return this.DESCRIPTIONS[0] + 10 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 48 */     if (card.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 49 */       this.counter += 1;
/*    */       
/* 51 */       if (this.counter % 10 == 0) {
/* 52 */         this.counter = 0;
/* 53 */         flash();
/* 54 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 55 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 62 */     return new Nunchaku();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Nunchaku.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */