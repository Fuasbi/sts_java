/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class DeadBranch extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Dead Branch";
/*    */   
/*    */   public DeadBranch()
/*    */   {
/* 12 */     super("Dead Branch", "deadBranch.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public void onExhaust(AbstractCard card)
/*    */   {
/* 17 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 18 */       flash();
/* 19 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 20 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(
/*    */       
/* 22 */         AbstractDungeon.returnTrulyRandomCard(AbstractDungeon.cardRandomRng).makeCopy(), false));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public String getUpdatedDescription()
/*    */   {
/* 29 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 34 */     return new DeadBranch();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\DeadBranch.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */