/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class WristBlade extends AbstractRelic {
/*    */   public static final String ID = "WristBlade";
/*    */   public static final int DMG_AMT = 3;
/*    */   
/*    */   public WristBlade() {
/*  8 */     super("WristBlade", "wBlade.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 13 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 18 */     return new WristBlade();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\WristBlade.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */