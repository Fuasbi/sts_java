/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class Shovel extends AbstractRelic {
/*    */   public static final String ID = "Shovel";
/*    */   
/*    */   public Shovel() {
/*  7 */     super("Shovel", "shovel.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new Shovel();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Shovel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */