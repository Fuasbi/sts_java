/*     */ package com.megacrit.cardcrawl.relics;
/*     */ 
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Dodecahedron extends AbstractRelic
/*     */ {
/*     */   public static final String ID = "Dodecahedron";
/*     */   private static final int ENERGY_AMT = 1;
/*     */   
/*     */   public Dodecahedron()
/*     */   {
/*  17 */     super("Dodecahedron", "dodecahedron.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.HEAVY);
/*     */   }
/*     */   
/*     */   public String getUpdatedDescription()
/*     */   {
/*  22 */     if (AbstractDungeon.player != null) {
/*  23 */       return setDescription(AbstractDungeon.player.chosenClass);
/*     */     }
/*  25 */     return setDescription(null);
/*     */   }
/*     */   
/*     */   private String setDescription(AbstractPlayer.PlayerClass c)
/*     */   {
/*  30 */     if (c == null) {
/*  31 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*     */     }
/*  33 */     switch (c) {
/*     */     case IRONCLAD: 
/*  35 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*     */     case THE_SILENT: 
/*  37 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[2];
/*     */     case DEFECT: 
/*  39 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[3];
/*     */     }
/*  41 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*     */   }
/*     */   
/*     */ 
/*     */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*     */   {
/*  47 */     this.description = setDescription(c);
/*  48 */     this.tips.clear();
/*  49 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/*  50 */     initializeTips();
/*     */   }
/*     */   
/*     */   public void atBattleStart()
/*     */   {
/*  55 */     controlPulse();
/*     */   }
/*     */   
/*     */   public void onVictory()
/*     */   {
/*  60 */     stopPulse();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void atTurnStart()
/*     */   {
/*  69 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.AbstractGameAction()
/*     */     {
/*     */       public void update() {
/*  72 */         if (Dodecahedron.this.isActive()) {
/*  73 */           Dodecahedron.this.flash();
/*  74 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, Dodecahedron.this));
/*     */           
/*  76 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/*     */         }
/*  78 */         this.isDone = true;
/*     */       }
/*     */     });
/*     */   }
/*     */   
/*     */   public int onPlayerHeal(int healAmount)
/*     */   {
/*  85 */     controlPulse();
/*  86 */     return super.onPlayerHeal(healAmount);
/*     */   }
/*     */   
/*     */   public int onAttacked(DamageInfo info, int damageAmount)
/*     */   {
/*  91 */     if (damageAmount > 0) {
/*  92 */       stopPulse();
/*     */     }
/*  94 */     return super.onAttacked(info, damageAmount);
/*     */   }
/*     */   
/*     */   public AbstractRelic makeCopy()
/*     */   {
/*  99 */     return new Dodecahedron();
/*     */   }
/*     */   
/*     */   private boolean isActive() {
/* 103 */     return AbstractDungeon.player.currentHealth >= AbstractDungeon.player.maxHealth;
/*     */   }
/*     */   
/*     */   private void controlPulse() {
/* 107 */     if (isActive()) {
/* 108 */       beginLongPulse();
/*     */     } else {
/* 110 */       stopPulse();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Dodecahedron.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */