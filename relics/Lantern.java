/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Lantern extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Lantern";
/*    */   private static final int ENERGY_AMT = 1;
/* 13 */   private boolean firstTurn = true;
/*    */   
/*    */   public Lantern() {
/* 16 */     super("Lantern", "lantern.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.SOLID);
/* 17 */     this.energyBased = true;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 22 */     if (AbstractDungeon.player != null) {
/* 23 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 25 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 30 */     if (c == null) {
/* 31 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1] + this.DESCRIPTIONS[4];
/*    */     }
/* 33 */     switch (c) {
/*    */     case IRONCLAD: 
/* 35 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1] + this.DESCRIPTIONS[4];
/*    */     case THE_SILENT: 
/* 37 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[2] + this.DESCRIPTIONS[4];
/*    */     case DEFECT: 
/* 39 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[3] + this.DESCRIPTIONS[4];
/*    */     }
/* 41 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1] + this.DESCRIPTIONS[4];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 47 */     this.description = setDescription(c);
/* 48 */     this.tips.clear();
/* 49 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 50 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 55 */     this.firstTurn = true;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 60 */     if (this.firstTurn) {
/* 61 */       flash();
/* 62 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/* 63 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 64 */       this.firstTurn = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 70 */     return new Lantern();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Lantern.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */