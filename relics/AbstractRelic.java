/*      */ package com.megacrit.cardcrawl.relics;
/*      */ 
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.GameDataStringBuilder;
/*      */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*      */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*      */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.RelicStrings;
/*      */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.rooms.TreasureRoomBoss;
/*      */ import com.megacrit.cardcrawl.screens.SingleRelicViewPopup;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*      */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.FloatyEffect;
/*      */ import java.io.Serializable;
/*      */ import java.util.ArrayList;
/*      */ import java.util.HashMap;
/*      */ import java.util.Scanner;
/*      */ import java.util.TreeMap;
/*      */ 
/*      */ public abstract class AbstractRelic implements Comparable<AbstractRelic>
/*      */ {
/*   53 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Relic Tip");
/*   54 */   public static final String[] MSG = tutorialStrings.TEXT;
/*   55 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*      */   public final String name;
/*      */   public final String relicId;
/*      */   private final RelicStrings relicStrings;
/*      */   public final String[] DESCRIPTIONS;
/*   60 */   public boolean energyBased = false; public boolean usedUp = false;
/*   61 */   public String description; public String flavorText = "missing";
/*   62 */   public int cost; public int counter = -1;
/*      */   public RelicTier tier;
/*   64 */   public ArrayList<PowerTip> tips = new ArrayList();
/*      */   public Texture img;
/*      */   public Texture largeImg;
/*      */   public Texture outlineImg;
/*      */   protected static final String IMG_DIR = "images/relics/";
/*   69 */   private static final String OUTLINE_DIR = "images/relics/outline/"; private static final String L_IMG_DIR = "images/largeRelics/"; public String imgUrl = "";
/*      */   
/*      */   public static final int RAW_W = 128;
/*      */   
/*   73 */   public static int relicPage = 0;
/*   74 */   private static float offsetX = 0.0F;
/*      */   public static final int MAX_RELICS_PER_PAGE = 25;
/*      */   public float currentX;
/*      */   public float currentY;
/*      */   public float targetX;
/*   79 */   public float targetY; private static final float START_X = 64.0F * Settings.scale; private static final float START_Y = Settings.HEIGHT - 102.0F * Settings.scale;
/*   80 */   public static final float PAD_X = 72.0F * Settings.scale;
/*   81 */   private static final Color PASSIVE_OUTLINE_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*   82 */   public boolean isSeen = false;
/*   83 */   public float scale = Settings.scale;
/*      */   
/*      */ 
/*   86 */   protected boolean pulse = false;
/*   87 */   private float animationTimer = 0.0F;
/*   88 */   public float flashTimer = 0.0F;
/*      */   
/*      */   private static final float FLASH_ANIM_TIME = 2.0F;
/*      */   
/*      */   private static final float DEFAULT_ANIM_SCALE = 4.0F;
/*   93 */   private float glowTimer = 0.0F;
/*   94 */   private FloatyEffect f_effect = new FloatyEffect(10.0F, 0.2F);
/*      */   
/*      */ 
/*   97 */   public boolean isDone = false; public boolean isAnimating = false; public boolean isObtained = false;
/*      */   
/*   99 */   private LandingSound landingSFX = LandingSound.CLINK;
/*  100 */   public Hitbox hb = new Hitbox(PAD_X, PAD_X);
/*      */   private static final float OBTAIN_SPEED = 6.0F;
/*      */   private static final float OBTAIN_THRESHOLD = 0.5F;
/*  103 */   private float rotation = 0.0F;
/*  104 */   public boolean discarded = false;
/*      */   private String assetURL;
/*      */   
/*  107 */   public static enum LandingSound { CLINK,  FLAT,  HEAVY,  MAGICAL,  SOLID;
/*      */     
/*      */     private LandingSound() {} }
/*      */   
/*  111 */   public static enum RelicTier { DEPRECATED,  STARTER,  COMMON,  UNCOMMON,  RARE,  SPECIAL,  BOSS,  SHOP;
/*      */     
/*      */     private RelicTier() {}
/*      */   }
/*      */   
/*      */   public AbstractRelic(String setId, String imgName, RelicTier tier, LandingSound sfx) {
/*  117 */     this.imgUrl = imgName;
/*  118 */     this.img = ImageMaster.loadImage("images/relics/" + imgName);
/*  119 */     this.largeImg = ImageMaster.loadImage("images/largeRelics/" + imgName);
/*  120 */     this.outlineImg = ImageMaster.loadImage("images/relics/outline/" + imgName);
/*  121 */     this.relicId = setId;
/*  122 */     this.relicStrings = CardCrawlGame.languagePack.getRelicStrings(this.relicId);
/*  123 */     this.DESCRIPTIONS = this.relicStrings.DESCRIPTIONS;
/*  124 */     this.name = this.relicStrings.NAME;
/*  125 */     this.description = getUpdatedDescription();
/*  126 */     this.flavorText = this.relicStrings.FLAVOR;
/*  127 */     this.tier = tier;
/*  128 */     this.landingSFX = sfx;
/*  129 */     this.assetURL = ("images/relics/" + imgName);
/*  130 */     this.tips.add(new PowerTip(this.name, this.description));
/*  131 */     initializeTips();
/*      */   }
/*      */   
/*      */   public void usedUp() {
/*  135 */     this.usedUp = true;
/*  136 */     this.description = MSG[2];
/*  137 */     this.tips.clear();
/*  138 */     this.tips.add(new PowerTip(this.name, this.description));
/*  139 */     initializeTips();
/*      */   }
/*      */   
/*      */   public void spawn(float x, float y) {
/*  143 */     if (!(AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.ShopRoom)) {
/*  144 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.SmokePuffEffect(x, y));
/*      */     }
/*  146 */     this.currentX = x;
/*  147 */     this.currentY = y;
/*  148 */     this.isAnimating = true;
/*  149 */     this.isObtained = false;
/*      */     
/*  151 */     if (this.tier == RelicTier.BOSS) {
/*  152 */       this.f_effect.x = 0.0F;
/*  153 */       this.f_effect.y = 0.0F;
/*  154 */       this.targetX = x;
/*  155 */       this.targetY = y;
/*  156 */       this.glowTimer = 0.0F;
/*      */     } else {
/*  158 */       this.f_effect.x = 0.0F;
/*  159 */       this.f_effect.y = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   public int getPrice() {
/*  164 */     switch (this.tier) {
/*      */     case STARTER: 
/*  166 */       return 300;
/*      */     case COMMON: 
/*  168 */       return 150;
/*      */     case UNCOMMON: 
/*  170 */       return 250;
/*      */     case RARE: 
/*  172 */       return 300;
/*      */     case SHOP: 
/*  174 */       return 200;
/*      */     case SPECIAL: 
/*  176 */       return 400;
/*      */     case BOSS: 
/*  178 */       return 999;
/*      */     case DEPRECATED: 
/*  180 */       return -1;
/*      */     }
/*  182 */     return -1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void reorganizeObtain(AbstractPlayer p, int slot, boolean callOnEquip, int relicAmount)
/*      */   {
/*  194 */     this.isDone = true;
/*  195 */     this.isObtained = true;
/*  196 */     p.relics.add(this);
/*  197 */     this.currentX = (START_X + slot * PAD_X);
/*  198 */     this.currentY = START_Y;
/*  199 */     this.targetX = this.currentX;
/*  200 */     this.targetY = this.currentY;
/*  201 */     this.hb.move(this.currentX, this.currentY);
/*  202 */     if (callOnEquip) {
/*  203 */       onEquip();
/*  204 */       relicTip();
/*      */     }
/*  206 */     UnlockTracker.markRelicAsSeen(this.relicId);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void instantObtain(AbstractPlayer p, int slot, boolean callOnEquip)
/*      */   {
/*  217 */     if ((this.relicId.equals("Circlet")) && (p != null) && (p.hasRelic("Circlet"))) {
/*  218 */       AbstractRelic circ = p.getRelic("Circlet");
/*  219 */       circ.counter += 1;
/*  220 */       circ.flash();
/*      */       
/*  222 */       this.isDone = true;
/*  223 */       this.isObtained = true;
/*  224 */       this.discarded = true;
/*      */     } else {
/*  226 */       this.isDone = true;
/*  227 */       this.isObtained = true;
/*      */       
/*  229 */       if (slot >= p.relics.size()) {
/*  230 */         p.relics.add(this);
/*      */       } else {
/*  232 */         p.relics.set(slot, this);
/*      */       }
/*      */       
/*  235 */       this.currentX = (START_X + slot * PAD_X);
/*  236 */       this.currentY = START_Y;
/*  237 */       this.targetX = this.currentX;
/*  238 */       this.targetY = this.currentY;
/*  239 */       this.hb.move(this.currentX, this.currentY);
/*      */       
/*  241 */       if (callOnEquip) {
/*  242 */         onEquip();
/*  243 */         relicTip();
/*      */       }
/*      */       
/*  246 */       UnlockTracker.markRelicAsSeen(this.relicId);
/*  247 */       getUpdatedDescription();
/*      */       
/*  249 */       if (AbstractDungeon.topPanel != null) {
/*  250 */         AbstractDungeon.topPanel.adjustRelicHbs();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void instantObtain() {
/*  256 */     if ((this.relicId == "Circlet") && (AbstractDungeon.player.hasRelic("Circlet"))) {
/*  257 */       AbstractRelic circ = AbstractDungeon.player.getRelic("Circlet");
/*  258 */       circ.counter += 1;
/*  259 */       circ.flash();
/*      */     } else {
/*  261 */       playLandingSFX();
/*  262 */       this.isDone = true;
/*  263 */       this.isObtained = true;
/*  264 */       this.currentX = (START_X + AbstractDungeon.player.relics.size() * PAD_X);
/*  265 */       this.currentY = START_Y;
/*  266 */       this.targetX = this.currentX;
/*  267 */       this.targetY = this.currentY;
/*  268 */       flash();
/*      */       
/*  270 */       AbstractDungeon.player.relics.add(this);
/*  271 */       this.hb.move(this.currentX, this.currentY);
/*  272 */       onEquip();
/*  273 */       relicTip();
/*  274 */       UnlockTracker.markRelicAsSeen(this.relicId);
/*      */     }
/*  276 */     if (AbstractDungeon.topPanel != null) {
/*  277 */       AbstractDungeon.topPanel.adjustRelicHbs();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void obtain()
/*      */   {
/*  285 */     if ((this.relicId == "Circlet") && (AbstractDungeon.player.hasRelic("Circlet"))) {
/*  286 */       AbstractRelic circ = AbstractDungeon.player.getRelic("Circlet");
/*  287 */       circ.counter += 1;
/*  288 */       circ.flash();
/*  289 */       this.hb.hovered = false;
/*      */     } else {
/*  291 */       this.hb.hovered = false;
/*  292 */       int row = AbstractDungeon.player.relics.size();
/*  293 */       this.targetX = (START_X + row * PAD_X);
/*  294 */       this.targetY = START_Y;
/*  295 */       AbstractDungeon.player.relics.add(this);
/*  296 */       relicTip();
/*  297 */       UnlockTracker.markRelicAsSeen(this.relicId);
/*      */     }
/*      */   }
/*      */   
/*      */   public int getColumn() {
/*  302 */     return AbstractDungeon.player.relics.indexOf(this);
/*      */   }
/*      */   
/*      */   public void relicTip()
/*      */   {
/*  307 */     if (TipTracker.relicCounter < 20) {
/*  308 */       TipTracker.relicCounter += 1;
/*  309 */       if ((TipTracker.relicCounter >= 1) && (!((Boolean)TipTracker.tips.get("RELIC_TIP")).booleanValue())) {
/*  310 */         AbstractDungeon.ftue = new com.megacrit.cardcrawl.ui.FtueTip(LABEL[0], MSG[0], 360.0F * Settings.scale, 760.0F * Settings.scale, com.megacrit.cardcrawl.ui.FtueTip.TipType.RELIC);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  316 */         TipTracker.neverShowAgain("RELIC_TIP");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void setCounter(int counter)
/*      */   {
/*  323 */     this.counter = counter;
/*      */   }
/*      */   
/*      */   public void update() {
/*  327 */     updateFlash();
/*      */     
/*  329 */     if (!this.isDone)
/*      */     {
/*  331 */       if (this.isAnimating) {
/*  332 */         this.glowTimer -= Gdx.graphics.getDeltaTime();
/*  333 */         if (this.glowTimer < 0.0F) {
/*  334 */           this.glowTimer = 0.5F;
/*  335 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.GlowRelicParticle(this.img, this.currentX + this.f_effect.x, this.currentY + this.f_effect.y, this.rotation));
/*      */         }
/*      */         
/*  338 */         this.f_effect.update();
/*  339 */         if (this.hb.hovered) {
/*  340 */           this.scale = (Settings.scale * 1.5F);
/*      */         } else {
/*  342 */           this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale * 1.1F);
/*      */         }
/*      */       }
/*  345 */       else if (this.hb.hovered) {
/*  346 */         this.scale = (Settings.scale * 1.25F);
/*      */       } else {
/*  348 */         this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  353 */       if (this.isObtained) {
/*  354 */         if (this.rotation != 0.0F) {
/*  355 */           this.rotation = MathUtils.lerp(this.rotation, 0.0F, Gdx.graphics.getDeltaTime() * 6.0F * 2.0F);
/*      */         }
/*      */         
/*  358 */         if (this.currentX != this.targetX) {
/*  359 */           this.currentX = MathUtils.lerp(this.currentX, this.targetX, Gdx.graphics.getDeltaTime() * 6.0F);
/*  360 */           if (Math.abs(this.currentX - this.targetX) < 0.5F) {
/*  361 */             this.currentX = this.targetX;
/*      */           }
/*      */         }
/*  364 */         if (this.currentY != this.targetY) {
/*  365 */           this.currentY = MathUtils.lerp(this.currentY, this.targetY, Gdx.graphics.getDeltaTime() * 6.0F);
/*  366 */           if (Math.abs(this.currentY - this.targetY) < 0.5F) {
/*  367 */             this.currentY = this.targetY;
/*      */           }
/*      */         }
/*      */         
/*  371 */         if ((this.currentY == this.targetY) && (this.currentX == this.targetX)) {
/*  372 */           this.isDone = true;
/*  373 */           if (AbstractDungeon.topPanel != null) {
/*  374 */             AbstractDungeon.topPanel.adjustRelicHbs();
/*      */           }
/*  376 */           this.hb.move(this.currentX, this.currentY);
/*  377 */           if ((this.tier == RelicTier.BOSS) && ((AbstractDungeon.getCurrRoom() instanceof TreasureRoomBoss))) {
/*  378 */             AbstractDungeon.overlayMenu.proceedButton.show();
/*      */           }
/*  380 */           onEquip();
/*      */         }
/*  382 */         this.scale = Settings.scale;
/*      */       }
/*      */       
/*  385 */       if (this.hb != null) {
/*  386 */         this.hb.update();
/*      */         
/*  388 */         if ((this.hb.hovered) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD)) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.NEOW_UNLOCK))
/*      */         {
/*  390 */           if (((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) && (!this.isObtained)) {
/*  391 */             if ((!this.relicId.equals("Black Blood")) && (!this.relicId.equals("Ring of the Serpent")) && (!this.relicId.equals("FrozenCore")))
/*      */             {
/*  393 */               obtain();
/*      */             }
/*  395 */             InputHelper.justClickedLeft = false;
/*  396 */             this.isObtained = true;
/*  397 */             this.f_effect.x = 0.0F;
/*  398 */             this.f_effect.y = 0.0F;
/*      */           }
/*      */         }
/*      */       }
/*  402 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/*  403 */         updateAnimation();
/*      */       }
/*      */     } else {
/*  406 */       if (AbstractDungeon.player.relics.indexOf(this) / 25 == relicPage) {
/*  407 */         this.hb.update();
/*      */       } else {
/*  409 */         this.hb.hovered = false;
/*      */       }
/*      */       
/*  412 */       if ((this.hb.hovered) && (AbstractDungeon.topPanel.potionUi.isHidden)) {
/*  413 */         this.scale = (Settings.scale * 1.25F);
/*  414 */         CardCrawlGame.cursor.changeType(com.megacrit.cardcrawl.core.GameCursor.CursorType.INSPECT);
/*      */       } else {
/*  416 */         this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale);
/*      */       }
/*  418 */       updateRelicPopupClick();
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateRelicPopupClick() {
/*  423 */     if ((this.hb.hovered) && (InputHelper.justClickedLeft)) {
/*  424 */       this.hb.clickStarted = true;
/*      */     }
/*  426 */     if ((this.hb.clicked) || ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/*  427 */       CardCrawlGame.relicPopup.open(this, AbstractDungeon.player.relics);
/*  428 */       CInputActionSet.select.unpress();
/*  429 */       this.hb.clicked = false;
/*  430 */       this.hb.clickStarted = false;
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateDescription(AbstractPlayer.PlayerClass c) {}
/*      */   
/*      */   public String getUpdatedDescription()
/*      */   {
/*  438 */     return "";
/*      */   }
/*      */   
/*      */   public void playLandingSFX() {
/*  442 */     switch (this.landingSFX) {
/*      */     case CLINK: 
/*  444 */       CardCrawlGame.sound.play("RELIC_DROP_CLINK");
/*  445 */       break;
/*      */     case FLAT: 
/*  447 */       CardCrawlGame.sound.play("RELIC_DROP_FLAT");
/*  448 */       break;
/*      */     case SOLID: 
/*  450 */       CardCrawlGame.sound.play("RELIC_DROP_ROCKY");
/*  451 */       break;
/*      */     case HEAVY: 
/*  453 */       CardCrawlGame.sound.play("RELIC_DROP_HEAVY");
/*  454 */       break;
/*      */     case MAGICAL: 
/*  456 */       CardCrawlGame.sound.play("RELIC_DROP_MAGICAL");
/*  457 */       break;
/*      */     default: 
/*  459 */       CardCrawlGame.sound.play("RELIC_DROP_CLINK");
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void updateAnimation()
/*      */   {
/*  468 */     if (this.animationTimer != 0.0F) {
/*  469 */       this.animationTimer -= Gdx.graphics.getDeltaTime();
/*  470 */       if (this.animationTimer < 0.0F) {
/*  471 */         this.animationTimer = 0.0F;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateFlash() {
/*  477 */     if (this.flashTimer != 0.0F) {
/*  478 */       this.flashTimer -= Gdx.graphics.getDeltaTime();
/*  479 */       if (this.flashTimer < 0.0F) {
/*  480 */         if (this.pulse) {
/*  481 */           this.flashTimer = 1.0F;
/*      */         } else {
/*  483 */           this.flashTimer = 0.0F;
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void onEvokeOrb(AbstractOrb ammo) {}
/*      */   
/*      */ 
/*      */   public void onPlayCard(AbstractCard c, AbstractMonster m) {}
/*      */   
/*      */ 
/*      */   public void onObtainCard(AbstractCard c) {}
/*      */   
/*      */ 
/*      */   public void onGainGold() {}
/*      */   
/*      */ 
/*      */   public void onLoseGold() {}
/*      */   
/*      */ 
/*      */   public void onSpendGold() {}
/*      */   
/*      */ 
/*      */   public void onEquip() {}
/*      */   
/*      */ 
/*      */   public void onUnequip() {}
/*      */   
/*      */ 
/*      */   public void atPreBattle() {}
/*      */   
/*      */ 
/*      */   public void atBattleStart() {}
/*      */   
/*      */ 
/*      */   public void atBattleStartPreDraw() {}
/*      */   
/*      */ 
/*      */   public void atTurnStart() {}
/*      */   
/*      */ 
/*      */   public void atTurnStartPostDraw() {}
/*      */   
/*      */ 
/*      */   public void onPlayerEndTurn() {}
/*      */   
/*      */ 
/*      */   public void onBloodied() {}
/*      */   
/*      */ 
/*      */   public void onNotBloodied() {}
/*      */   
/*      */ 
/*      */   public void onManualDiscard() {}
/*      */   
/*      */ 
/*      */   public void onUseCard(AbstractCard targetCard, UseCardAction useCardAction) {}
/*      */   
/*      */ 
/*      */   public void onVictory() {}
/*      */   
/*      */ 
/*      */   public void onMonsterDeath(AbstractMonster m) {}
/*      */   
/*      */   public void onBlockBroken(AbstractCreature m) {}
/*      */   
/*      */   public int onPlayerGainBlock(int blockAmount)
/*      */   {
/*  553 */     return blockAmount;
/*      */   }
/*      */   
/*      */   public int onPlayerGainedBlock(float blockAmount) {
/*  557 */     return MathUtils.floor(blockAmount);
/*      */   }
/*      */   
/*      */   public int onPlayerHeal(int healAmount) {
/*  561 */     return healAmount;
/*      */   }
/*      */   
/*      */ 
/*      */   public void onMeditate() {}
/*      */   
/*      */ 
/*      */   public void onEnergyRecharge() {}
/*      */   
/*      */ 
/*      */   public void onRest() {}
/*      */   
/*      */ 
/*      */   public void onRitual() {}
/*      */   
/*      */ 
/*      */   public void onEnterRestRoom() {}
/*      */   
/*      */ 
/*      */   public void onRefreshHand() {}
/*      */   
/*      */ 
/*      */   public void onShuffle() {}
/*      */   
/*      */ 
/*      */   public void onSmith() {}
/*      */   
/*      */   public void onAttack(DamageInfo info, int damageAmount, AbstractCreature target) {}
/*      */   
/*      */   public int onAttacked(DamageInfo info, int damageAmount)
/*      */   {
/*  592 */     return damageAmount;
/*      */   }
/*      */   
/*      */   public int onAttackedMonster(DamageInfo info, int damageAmount) {
/*  596 */     return damageAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void onExhaust(AbstractCard card) {}
/*      */   
/*      */ 
/*      */   public void onTrigger() {}
/*      */   
/*      */ 
/*      */   public void onTrigger(AbstractCreature target) {}
/*      */   
/*      */ 
/*      */   public boolean checkTrigger()
/*      */   {
/*  612 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */   public void onEnterRoom(AbstractRoom room) {}
/*      */   
/*      */ 
/*      */   public void justEnteredRoom(AbstractRoom room) {}
/*      */   
/*      */ 
/*      */   public void onCardDraw(AbstractCard drawnCard) {}
/*      */   
/*      */ 
/*      */   public void onChestOpen(boolean bossChest) {}
/*      */   
/*      */ 
/*      */   public void onChestOpenAfter(boolean bossChest) {}
/*      */   
/*      */ 
/*      */   public void onDrawOrDiscard() {}
/*      */   
/*      */   public void onMasterDeckChange() {}
/*      */   
/*      */   public void renderInTopPanel(SpriteBatch sb)
/*      */   {
/*  637 */     if (Settings.hideRelics) {
/*  638 */       return;
/*      */     }
/*      */     
/*  641 */     renderOutline(sb, true);
/*  642 */     sb.setColor(Color.WHITE);
/*  643 */     sb.draw(this.img, this.currentX - 64.0F + offsetX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  660 */     renderCounter(sb, true);
/*  661 */     renderFlash(sb, true);
/*  662 */     this.hb.render(sb);
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb) {
/*  666 */     if (Settings.hideRelics) {
/*  667 */       return;
/*      */     }
/*      */     
/*      */ 
/*  671 */     renderOutline(sb, false);
/*  672 */     if ((!this.isObtained) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.SHOP)))
/*      */     {
/*      */ 
/*  675 */       if (this.hb.hovered) {
/*  676 */         renderTip(sb);
/*      */       }
/*      */       
/*  679 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/*  680 */         if (this.hb.hovered) {
/*  681 */           sb.setColor(PASSIVE_OUTLINE_COLOR);
/*  682 */           sb.draw(this.outlineImg, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  700 */           sb.setColor(PASSIVE_OUTLINE_COLOR);
/*  701 */           sb.draw(this.outlineImg, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  722 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/*  723 */       if (!this.isObtained) {
/*  724 */         sb.setColor(Color.WHITE);
/*  725 */         sb.draw(this.img, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  743 */         sb.setColor(Color.WHITE);
/*  744 */         sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  761 */         renderCounter(sb, false);
/*      */       }
/*      */     } else {
/*  764 */       sb.setColor(Color.WHITE);
/*  765 */       sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  783 */       renderCounter(sb, false);
/*      */     }
/*      */     
/*  786 */     if (this.isDone) {
/*  787 */       renderFlash(sb, false);
/*      */     }
/*      */     
/*  790 */     this.hb.render(sb);
/*      */   }
/*      */   
/*      */   public void renderLock(SpriteBatch sb, Color outlineColor) {
/*  794 */     sb.setColor(outlineColor);
/*  795 */     sb.draw(ImageMaster.RELIC_LOCK_OUTLINE, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  813 */     sb.setColor(Color.WHITE);
/*  814 */     sb.draw(ImageMaster.RELIC_LOCK, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  832 */     if (this.hb.hovered) {
/*  833 */       String unlockReq = (String)UnlockTracker.unlockReqs.get(this.relicId);
/*  834 */       if (unlockReq == null) {
/*  835 */         unlockReq = "Missing unlock req.";
/*      */       }
/*  837 */       unlockReq = LABEL[2];
/*      */       
/*  839 */       if (InputHelper.mX < 1400.0F * Settings.scale) {
/*  840 */         if ((CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.RELIC_VIEW) && (InputHelper.mY < Settings.HEIGHT / 5.0F))
/*      */         {
/*  842 */           TipHelper.renderGenericTip(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY + 100.0F * Settings.scale, LABEL[3], unlockReq);
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  848 */           TipHelper.renderGenericTip(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, LABEL[3], unlockReq);
/*      */         }
/*      */         
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*  855 */         TipHelper.renderGenericTip(InputHelper.mX - 350.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, LABEL[3], unlockReq);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  862 */       float tmpX = this.currentX;
/*  863 */       float tmpY = this.currentY;
/*      */       
/*  865 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/*  866 */         tmpX += this.f_effect.x;
/*  867 */         tmpY += this.f_effect.y;
/*      */       }
/*      */       
/*  870 */       sb.setColor(Color.WHITE);
/*  871 */       sb.draw(ImageMaster.RELIC_LOCK, tmpX - 64.0F, tmpY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  890 */     this.hb.render(sb);
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb, boolean renderAmount, Color outlineColor) {
/*  894 */     if (this.isSeen) {
/*  895 */       renderOutline(outlineColor, sb, false);
/*      */     } else {
/*  897 */       renderOutline(Color.LIGHT_GRAY, sb, false);
/*      */     }
/*      */     
/*  900 */     if (this.isSeen) {
/*  901 */       sb.setColor(Color.WHITE);
/*      */     }
/*  903 */     else if (this.hb.hovered) {
/*  904 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
/*      */     } else {
/*  906 */       sb.setColor(Color.BLACK);
/*      */     }
/*      */     
/*      */ 
/*  910 */     if ((AbstractDungeon.screen != null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NEOW_UNLOCK)) {
/*  911 */       if (this.largeImg == null) {
/*  912 */         sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F + 
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  920 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, Settings.scale * 2.0F + 
/*  921 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, this.rotation, 0, 0, 128, 128, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/*  930 */         sb.draw(this.largeImg, this.currentX - 128.0F, this.currentY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 1.0F + 
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  938 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 30.0F, Settings.scale * 1.0F + 
/*  939 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 30.0F, this.rotation, 0, 0, 256, 256, false, false);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  949 */       sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  966 */       if (this.relicId.equals("Circlet")) {
/*  967 */         renderCounter(sb, false);
/*      */       }
/*      */     }
/*      */     
/*  971 */     if (this.hb.hovered) {
/*  972 */       if (!this.isSeen) {
/*  973 */         if (InputHelper.mX < 1400.0F * Settings.scale) {
/*  974 */           TipHelper.renderGenericTip(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, LABEL[1], MSG[1]);
/*      */ 
/*      */         }
/*      */         else
/*      */         {
/*      */ 
/*  980 */           TipHelper.renderGenericTip(InputHelper.mX - 350.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, LABEL[1], MSG[1]);
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*  986 */         return;
/*      */       }
/*  988 */       renderTip(sb);
/*      */     }
/*      */     
/*      */ 
/*  992 */     this.hb.render(sb);
/*      */   }
/*      */   
/*      */   public void renderWithoutAmount(SpriteBatch sb, Color c) {
/*  996 */     renderOutline(c, sb, false);
/*  997 */     sb.setColor(Color.WHITE);
/*  998 */     sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1016 */     if (this.hb.hovered) {
/* 1017 */       renderTip(sb);
/*      */       
/* 1019 */       float tmpX = this.currentX;
/* 1020 */       float tmpY = this.currentY;
/* 1021 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/* 1022 */         tmpX += this.f_effect.x;
/* 1023 */         tmpY += this.f_effect.y;
/*      */       }
/*      */       
/* 1026 */       sb.setColor(Color.WHITE);
/* 1027 */       sb.draw(this.img, tmpX - 64.0F, tmpY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1046 */     this.hb.render(sb);
/*      */   }
/*      */   
/*      */   public void renderCounter(SpriteBatch sb, boolean inTopPanel) {
/* 1050 */     if (this.counter > -1) {
/* 1051 */       if (inTopPanel) {
/* 1052 */         FontHelper.renderFontRightTopAligned(sb, FontHelper.topPanelInfoFont, 
/*      */         
/*      */ 
/* 1055 */           Integer.toString(this.counter), offsetX + this.currentX + 30.0F * Settings.scale, this.currentY - 7.0F * Settings.scale, Color.WHITE);
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/* 1060 */         FontHelper.renderFontRightTopAligned(sb, FontHelper.topPanelInfoFont, 
/*      */         
/*      */ 
/* 1063 */           Integer.toString(this.counter), offsetX + this.currentX + 30.0F * Settings.scale, this.currentY - 7.0F * Settings.scale, Color.WHITE);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void renderOutline(Color c, SpriteBatch sb, boolean inTopPanel)
/*      */   {
/* 1072 */     sb.setColor(c);
/* 1073 */     if ((AbstractDungeon.screen != null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NEOW_UNLOCK)) {
/* 1074 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F + 
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1082 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, Settings.scale * 2.0F + 
/* 1083 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, this.rotation, 0, 0, 128, 128, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/* 1092 */     else if ((this.hb.hovered) && (Settings.isControllerMode)) {
/* 1093 */       sb.setBlendFunction(770, 1);
/* 1094 */       sb.setColor(new Color(1.0F, 0.9F, 0.4F, 0.6F + 
/* 1095 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 1096 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1113 */       sb.setBlendFunction(770, 771);
/*      */     } else {
/* 1115 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderOutline(SpriteBatch sb, boolean inTopPanel)
/*      */   {
/* 1137 */     float tmpX = this.currentX - 64.0F;
/* 1138 */     if (inTopPanel) {
/* 1139 */       tmpX += offsetX;
/*      */     }
/*      */     
/* 1142 */     if ((this.hb.hovered) && (Settings.isControllerMode)) {
/* 1143 */       sb.setBlendFunction(770, 1);
/* 1144 */       sb.setColor(new Color(1.0F, 0.9F, 0.4F, 0.6F + 
/* 1145 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 1146 */       sb.draw(this.outlineImg, tmpX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1163 */       sb.setBlendFunction(770, 771);
/*      */     }
/*      */     else {
/* 1166 */       sb.setColor(PASSIVE_OUTLINE_COLOR);
/* 1167 */       sb.draw(this.outlineImg, tmpX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderFlash(SpriteBatch sb, boolean inTopPanel)
/*      */   {
/* 1188 */     float tmp = com.badlogic.gdx.math.Interpolation.exp10In.apply(0.0F, 4.0F, this.flashTimer / 2.0F);
/*      */     
/* 1190 */     sb.setBlendFunction(770, 1);
/* 1191 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.flashTimer * 0.2F));
/* 1192 */     float tmpX = this.currentX - 64.0F;
/* 1193 */     if (inTopPanel) {
/* 1194 */       tmpX += offsetX;
/*      */     }
/*      */     
/* 1197 */     sb.draw(this.img, tmpX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp, this.scale + tmp, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1215 */     sb.draw(this.img, tmpX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp * 0.66F, this.scale + tmp * 0.66F, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1233 */     sb.draw(this.img, tmpX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp / 3.0F, this.scale + tmp / 3.0F, this.rotation, 0, 0, 128, 128, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1251 */     sb.setBlendFunction(770, 771);
/*      */   }
/*      */   
/*      */   public void beginPulse() {
/* 1255 */     this.flashTimer = 1.0F;
/*      */   }
/*      */   
/*      */   public void beginLongPulse() {
/* 1259 */     this.flashTimer = 1.0F;
/* 1260 */     this.pulse = true;
/*      */   }
/*      */   
/*      */   public void stopPulse() {
/* 1264 */     this.pulse = false;
/*      */   }
/*      */   
/*      */   public void flash() {
/* 1268 */     this.flashTimer = 2.0F;
/*      */   }
/*      */   
/*      */   public void renderTip(SpriteBatch sb) {
/* 1272 */     if (InputHelper.mX < 1400.0F * Settings.scale) {
/* 1273 */       if (CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.RELIC_VIEW) {
/* 1274 */         TipHelper.queuePowerTips(180.0F * Settings.scale, Settings.HEIGHT * 0.7F, this.tips);
/* 1275 */       } else if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.SHOP) && (this.tips.size() > 2) && 
/* 1276 */         (!AbstractDungeon.player.hasRelic(this.relicId))) {
/* 1277 */         TipHelper.queuePowerTips(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY + 180.0F * Settings.scale, this.tips);
/*      */ 
/*      */ 
/*      */       }
/* 1281 */       else if ((AbstractDungeon.player != null) && (AbstractDungeon.player.hasRelic(this.relicId))) {
/* 1282 */         TipHelper.queuePowerTips(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY - 30.0F * Settings.scale, this.tips);
/*      */ 
/*      */ 
/*      */       }
/* 1286 */       else if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD) {
/* 1287 */         TipHelper.queuePowerTips(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, this.tips);
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/* 1292 */         TipHelper.queuePowerTips(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, this.tips);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else {
/* 1298 */       TipHelper.queuePowerTips(InputHelper.mX - 350.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, this.tips);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean canPlay(AbstractCard card)
/*      */   {
/* 1306 */     return true;
/*      */   }
/*      */   
/*      */   public static String gameDataUploadHeader() {
/* 1310 */     GameDataStringBuilder builder = new GameDataStringBuilder();
/*      */     
/* 1312 */     builder.addFieldData("name");
/* 1313 */     builder.addFieldData("relicID");
/* 1314 */     builder.addFieldData("color");
/* 1315 */     builder.addFieldData("description");
/* 1316 */     builder.addFieldData("flavorText");
/* 1317 */     builder.addFieldData("cost");
/* 1318 */     builder.addFieldData("tier");
/* 1319 */     builder.addFieldData("assetURL");
/*      */     
/* 1321 */     return builder.toString();
/*      */   }
/*      */   
/*      */   protected void initializeTips() {
/* 1325 */     Scanner desc = new Scanner(this.description);
/*      */     
/* 1327 */     while (desc.hasNext()) {
/* 1328 */       String s = desc.next();
/* 1329 */       if (s.charAt(0) == '#') {
/* 1330 */         s = s.substring(2);
/*      */       }
/*      */       
/* 1333 */       s = s.replace(',', ' ');
/* 1334 */       s = s.replace('.', ' ');
/* 1335 */       s = s.trim();
/* 1336 */       s = s.toLowerCase();
/*      */       
/* 1338 */       boolean alreadyExists = false;
/* 1339 */       if (GameDictionary.keywords.containsKey(s)) {
/* 1340 */         s = (String)GameDictionary.parentWord.get(s);
/* 1341 */         for (PowerTip t : this.tips) {
/* 1342 */           if (t.header.toLowerCase().equals(s)) {
/* 1343 */             alreadyExists = true;
/* 1344 */             break;
/*      */           }
/*      */         }
/* 1347 */         if (!alreadyExists) {
/* 1348 */           this.tips.add(new PowerTip(TipHelper.capitalize(s), (String)GameDictionary.keywords.get(s)));
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1353 */     desc.close();
/*      */   }
/*      */   
/*      */   public String gameDataUploadData(String color) {
/* 1357 */     GameDataStringBuilder builder = new GameDataStringBuilder();
/*      */     
/* 1359 */     builder.addFieldData(this.name);
/* 1360 */     builder.addFieldData(this.relicId);
/* 1361 */     builder.addFieldData(color);
/* 1362 */     builder.addFieldData(this.description);
/* 1363 */     builder.addFieldData(this.flavorText);
/* 1364 */     builder.addFieldData(this.cost);
/* 1365 */     builder.addFieldData(this.tier.name());
/* 1366 */     builder.addFieldData(this.assetURL);
/*      */     
/* 1368 */     return builder.toString();
/*      */   }
/*      */   
/*      */   public abstract AbstractRelic makeCopy();
/*      */   
/*      */   public String toString()
/*      */   {
/* 1375 */     return this.name;
/*      */   }
/*      */   
/*      */   public int compareTo(AbstractRelic arg0)
/*      */   {
/* 1380 */     return this.name.compareTo(arg0.name);
/*      */   }
/*      */   
/*      */   public String getAssetURL() {
/* 1384 */     return this.assetURL;
/*      */   }
/*      */   
/*      */   public HashMap<String, Serializable> getLocStrings() {
/* 1388 */     HashMap<String, Serializable> relicData = new HashMap();
/* 1389 */     relicData.put("name", this.name);
/* 1390 */     relicData.put("description", this.description);
/* 1391 */     return relicData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void onUsePotion() {}
/*      */   
/*      */ 
/*      */   public void onLoseHp(int damageAmount) {}
/*      */   
/*      */ 
/*      */   public static void updateOffsetX()
/*      */   {
/* 1404 */     float target = -relicPage * Settings.WIDTH + relicPage * (PAD_X + 36.0F * Settings.scale);
/* 1405 */     if (AbstractDungeon.player.relics.size() >= 26) {
/* 1406 */       target += 36.0F * Settings.scale;
/*      */     }
/*      */     
/* 1409 */     if (offsetX != target) {
/* 1410 */       offsetX = MathHelper.uiLerpSnap(offsetX, target);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\AbstractRelic.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */