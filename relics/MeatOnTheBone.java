/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MeatOnTheBone extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Meat on the Bone";
/*    */   private static final int HEAL_AMT = 12;
/*    */   
/*    */   public MeatOnTheBone()
/*    */   {
/* 13 */     super("Meat on the Bone", "meat.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 12 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onTrigger()
/*    */   {
/* 23 */     AbstractPlayer p = AbstractDungeon.player;
/* 24 */     if ((p.currentHealth <= p.maxHealth / 2.0F) && (p.currentHealth > 0)) {
/* 25 */       flash();
/* 26 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 27 */       p.heal(12);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new MeatOnTheBone();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MeatOnTheBone.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */