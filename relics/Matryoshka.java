/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Matryoshka extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Matryoshka";
/*    */   
/*    */   public Matryoshka()
/*    */   {
/* 11 */     super("Matryoshka", "matryoshka.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.SOLID);
/* 12 */     this.counter = 2;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onChestOpen(boolean bossChest)
/*    */   {
/* 22 */     if ((!bossChest) && 
/* 23 */       (this.counter > 0)) {
/* 24 */       this.counter -= 1;
/*    */       
/* 26 */       flash();
/* 27 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */       
/* 29 */       if (AbstractDungeon.relicRng.randomBoolean(0.75F)) {
/* 30 */         AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.COMMON);
/*    */       } else {
/* 32 */         AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.UNCOMMON);
/*    */       }
/* 34 */       if (this.counter == 0) {
/* 35 */         setCounter(-2);
/* 36 */         this.description = this.DESCRIPTIONS[2];
/*    */       } else {
/* 38 */         this.description = this.DESCRIPTIONS[1];
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void setCounter(int counter)
/*    */   {
/* 46 */     this.counter = counter;
/* 47 */     if (counter == -2) {
/* 48 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/usedMatryoshka.png");
/* 49 */       usedUp();
/* 50 */       this.counter = -2;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 56 */     return new Matryoshka();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Matryoshka.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */