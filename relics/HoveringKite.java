/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class HoveringKite extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "HoveringKite";
/*    */   
/*    */   public HoveringKite() {
/* 10 */     super("HoveringKite", "kite.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atTurnStartPostDraw()
/*    */   {
/* 20 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DiscardAction(AbstractDungeon.player, AbstractDungeon.player, 2, false));
/*    */   }
/*    */   
/*    */ 
/*    */   public void onEquip()
/*    */   {
/* 26 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 31 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 36 */     return new HoveringKite();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\HoveringKite.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */