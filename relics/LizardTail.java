/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class LizardTail extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Lizard Tail";
/*    */   
/*    */   public LizardTail()
/*    */   {
/* 12 */     super("Lizard Tail", "lizardTail.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void setCounter(int counter)
/*    */   {
/* 22 */     if (counter == -2) {
/* 23 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/relics/usedLizardTail.png");
/* 24 */       usedUp();
/* 25 */       this.counter = -2;
/*    */     }
/*    */   }
/*    */   
/*    */   public void onTrigger()
/*    */   {
/* 31 */     flash();
/* 32 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 33 */     int healAmt = AbstractDungeon.player.maxHealth / 2;
/*    */     
/*    */ 
/* 36 */     if (AbstractDungeon.player.hasBlight("FullBelly")) {
/* 37 */       healAmt /= 2;
/*    */     }
/*    */     
/* 40 */     if (healAmt < 1) {
/* 41 */       healAmt = 1;
/*    */     }
/*    */     
/* 44 */     AbstractDungeon.player.heal(healAmt, true);
/* 45 */     setCounter(-2);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 50 */     return new LizardTail();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\LizardTail.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */