/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class PrayerWheel extends AbstractRelic {
/*    */   public static final String ID = "Prayer Wheel";
/*    */   
/*    */   public PrayerWheel() {
/*  7 */     super("Prayer Wheel", "prayerWheel.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new PrayerWheel();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\PrayerWheel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */