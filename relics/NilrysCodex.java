/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class NilrysCodex extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Nilry's Codex";
/*    */   
/*    */   public NilrysCodex()
/*    */   {
/* 11 */     super("Nilry's Codex", "codex.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 21 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 22 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.CodexAction());
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 27 */     return new NilrysCodex();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\NilrysCodex.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */