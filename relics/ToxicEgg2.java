/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class ToxicEgg2 extends AbstractRelic {
/*    */   public static final String ID = "Toxic Egg 2";
/*    */   
/*    */   public ToxicEgg2() {
/*  9 */     super("Toxic Egg 2", "toxicEgg.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onObtainCard(AbstractCard c)
/*    */   {
/* 19 */     if ((c.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL) && (c.canUpgrade()) && (!c.upgraded)) {
/* 20 */       c.upgrade();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new ToxicEgg2();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ToxicEgg2.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */