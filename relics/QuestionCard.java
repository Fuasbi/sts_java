/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class QuestionCard extends AbstractRelic {
/*    */   public static final String ID = "Question Card";
/*    */   
/*    */   public QuestionCard() {
/*  7 */     super("Question Card", "questionCard.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new QuestionCard();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\QuestionCard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */