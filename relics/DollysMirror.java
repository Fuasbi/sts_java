/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DollysMirror extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "DollysMirror";
/* 11 */   private boolean cardSelected = true;
/*    */   
/*    */   public DollysMirror() {
/* 14 */     super("DollysMirror", "mirror.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 24 */     this.cardSelected = false;
/* 25 */     if (AbstractDungeon.isScreenUp) {
/* 26 */       AbstractDungeon.dynamicBanner.hide();
/* 27 */       AbstractDungeon.overlayMenu.cancelButton.hide();
/* 28 */       AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*    */     }
/* 30 */     AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.INCOMPLETE;
/*    */     
/* 32 */     AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck, 1, this.DESCRIPTIONS[1], false, false, false, false);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 44 */     super.update();
/* 45 */     if ((!this.cardSelected) && 
/* 46 */       (AbstractDungeon.gridSelectScreen.selectedCards.size() == 1)) {
/* 47 */       this.cardSelected = true;
/*    */       
/* 49 */       AbstractCard c = ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).makeStatEquivalentCopy();
/* 50 */       c.inBottleFlame = false;
/* 51 */       c.inBottleLightning = false;
/* 52 */       c.inBottleTornado = false;
/* 53 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(c, com.megacrit.cardcrawl.core.Settings.WIDTH / 2.0F, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2.0F));
/*    */       
/* 55 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */       
/* 57 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/* 58 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 65 */     return new DollysMirror();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\DollysMirror.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */