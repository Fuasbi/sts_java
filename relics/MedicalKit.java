/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class MedicalKit extends AbstractRelic {
/*    */   public static final String ID = "Medical Kit";
/*    */   
/*    */   public MedicalKit() {
/*  7 */     super("Medical Kit", "medicalKit.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new MedicalKit();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MedicalKit.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */