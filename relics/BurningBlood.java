/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BurningBlood extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Burning Blood";
/*    */   private static final int HEALTH_AMT = 6;
/*    */   
/*    */   public BurningBlood()
/*    */   {
/* 13 */     super("Burning Blood", "burningBlood.png", AbstractRelic.RelicTier.STARTER, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractPlayer p = AbstractDungeon.player;
/* 26 */     if (p.currentHealth > 0) {
/* 27 */       p.heal(6);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new BurningBlood();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BurningBlood.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */