/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class CharonsAshes extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Charon's Ashes";
/*    */   public static final int DMG = 3;
/*    */   
/*    */   public CharonsAshes()
/*    */   {
/* 18 */     super("Charon's Ashes", "ashes.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 23 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onExhaust(AbstractCard card)
/*    */   {
/* 28 */     flash();
/*    */     
/* 30 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */     
/*    */ 
/* 33 */       com.megacrit.cardcrawl.cards.DamageInfo.createDamageMatrix(3, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */     
/*    */ 
/*    */ 
/* 37 */     for (AbstractMonster mo : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 38 */       if (!mo.isDead) {
/* 39 */         AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(mo, this));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 47 */     return new CharonsAshes();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\CharonsAshes.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */