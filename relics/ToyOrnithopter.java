/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ToyOrnithopter extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Toy Ornithopter";
/*    */   public static final int HEAL_AMT = 5;
/*    */   
/*    */   public ToyOrnithopter()
/*    */   {
/* 13 */     super("Toy Ornithopter", "ornithopter.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 5 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onUsePotion()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(AbstractDungeon.player, AbstractDungeon.player, 5));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 31 */     return new ToyOrnithopter();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\ToyOrnithopter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */