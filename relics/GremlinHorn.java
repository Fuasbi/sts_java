/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GremlinHorn extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Gremlin Horn";
/*    */   
/*    */   public GremlinHorn()
/*    */   {
/* 15 */     super("Gremlin Horn", "gremlinHorn.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.HEAVY);
/* 16 */     this.energyBased = true;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     if (AbstractDungeon.player != null) {
/* 22 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 24 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 29 */     if (c == null) {
/* 30 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     }
/* 32 */     switch (c) {
/*    */     case IRONCLAD: 
/* 34 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 36 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[2];
/*    */     case DEFECT: 
/* 38 */       return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[3];
/*    */     }
/* 40 */     return this.DESCRIPTIONS[0] + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 46 */     this.description = setDescription(c);
/* 47 */     this.tips.clear();
/* 48 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 49 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onMonsterDeath(AbstractMonster m)
/*    */   {
/* 54 */     if ((m.currentHealth == 0) && (!AbstractDungeon.getMonsters().areMonstersBasicallyDead())) {
/* 55 */       flash();
/* 56 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(m, this));
/* 57 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(1));
/* 58 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 64 */     return new GremlinHorn();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\GremlinHorn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */