/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class MealTicket extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "MealTicket";
/*    */   private static final int HP_AMT = 7;
/*    */   
/*    */   public MealTicket()
/*    */   {
/* 14 */     super("MealTicket", "mealTicket.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 24 */     if ((room instanceof com.megacrit.cardcrawl.rooms.ShopRoom)) {
/* 25 */       flash();
/* 26 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 27 */       AbstractDungeon.player.heal(7);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new MealTicket();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MealTicket.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */