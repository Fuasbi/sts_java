/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Inserter extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Inserter";
/*    */   private static final int NUM_TURNS = 2;
/*    */   
/*    */   public Inserter()
/*    */   {
/* 13 */     super("Inserter", "inserter.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 23 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 28 */     if (this.counter == -1) {
/* 29 */       this.counter += 2;
/*    */     } else {
/* 31 */       this.counter += 1;
/*    */     }
/*    */     
/* 34 */     if (this.counter == 2) {
/* 35 */       this.counter = 0;
/* 36 */       flash();
/* 37 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 38 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.IncreaseMaxOrbAction(1));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 44 */     return new Inserter();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Inserter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */