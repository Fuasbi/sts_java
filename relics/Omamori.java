/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class Omamori extends AbstractRelic {
/*    */   public static final String ID = "Omamori";
/*    */   
/*    */   public Omamori() {
/*  9 */     super("Omamori", "omamori.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.FLAT);
/* 10 */     this.counter = 2;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void setCounter(int counter)
/*    */   {
/* 20 */     this.counter = counter;
/* 21 */     if (counter == 0) {
/* 22 */       this.img = ImageMaster.loadImage("images/relics/usedOmamori.png");
/* 23 */       usedUp();
/* 24 */       this.description = this.DESCRIPTIONS[2];
/* 25 */     } else if (counter == 1) {
/* 26 */       this.description = this.DESCRIPTIONS[1];
/*    */     }
/*    */   }
/*    */   
/*    */   public void use() {
/* 31 */     flash();
/* 32 */     this.counter -= 1;
/* 33 */     if (this.counter == 0) {
/* 34 */       setCounter(0);
/*    */     } else {
/* 36 */       this.description = this.DESCRIPTIONS[1];
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 42 */     return new Omamori();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Omamori.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */