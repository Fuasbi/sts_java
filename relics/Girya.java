/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Girya extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Girya";
/*    */   public static final int STR_LIMIT = 3;
/*    */   
/*    */   public Girya()
/*    */   {
/* 14 */     super("Girya", "kettlebell.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.HEAVY);
/* 15 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     if (this.counter != 0) {
/* 26 */       flash();
/* 27 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, this.counter), this.counter));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 39 */     return new Girya();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Girya.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */