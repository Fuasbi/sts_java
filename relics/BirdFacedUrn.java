/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BirdFacedUrn extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Bird Faced Urn";
/*    */   private static final int HEAL_AMT = 2;
/*    */   
/*    */   public BirdFacedUrn()
/*    */   {
/* 16 */     super("Bird Faced Urn", "ancientUrn.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     return this.DESCRIPTIONS[0] + 2 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 26 */     if (card.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER) {
/* 27 */       flash();
/* 28 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.HealAction(AbstractDungeon.player, AbstractDungeon.player, 2));
/*    */       
/* 30 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 36 */     return new BirdFacedUrn();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BirdFacedUrn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */