/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class NuclearBattery extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Nuclear Battery";
/*    */   
/*    */   public NuclearBattery() {
/* 10 */     super("Nuclear Battery", "battery.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 15 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 20 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.channelOrb(new com.megacrit.cardcrawl.orbs.Plasma());
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 25 */     return new NuclearBattery();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\NuclearBattery.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */