/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class DarkstonePeriapt extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Darkstone Periapt";
/*    */   private static final int HP_AMT = 6;
/*    */   
/*    */   public DarkstonePeriapt()
/*    */   {
/* 12 */     super("Darkstone Periapt", "darkstone.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public void onObtainCard(AbstractCard card)
/*    */   {
/* 17 */     if (card.color == com.megacrit.cardcrawl.cards.AbstractCard.CardColor.CURSE) {
/* 18 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxHp(6, true);
/*    */     }
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 24 */     return this.DESCRIPTIONS[0] + 6 + ".";
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 29 */     return new DarkstonePeriapt();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\DarkstonePeriapt.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */