/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class VelvetChoker extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Velvet Choker";
/*    */   private static final int PLAY_LIMIT = 6;
/*    */   
/*    */   public VelvetChoker()
/*    */   {
/* 15 */     super("Velvet Choker", "redChoker.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     if (AbstractDungeon.player != null) {
/* 21 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 23 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 28 */     if (c == null) {
/* 29 */       return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */     }
/* 31 */     switch (c) {
/*    */     case IRONCLAD: 
/* 33 */       return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */     case THE_SILENT: 
/* 35 */       return this.DESCRIPTIONS[3] + this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */     case DEFECT: 
/* 37 */       return this.DESCRIPTIONS[4] + this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */     }
/* 39 */     return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0] + 6 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 45 */     this.description = setDescription(c);
/* 46 */     this.tips.clear();
/* 47 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 48 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 53 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 58 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 63 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 68 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void onPlayCard(AbstractCard card, com.megacrit.cardcrawl.monsters.AbstractMonster m)
/*    */   {
/* 73 */     if ((this.counter < 6) && (card.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.CURSE)) {
/* 74 */       this.counter += 1;
/* 75 */       if (this.counter >= 6) {
/* 76 */         flash();
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean canPlay(AbstractCard card)
/*    */   {
/* 83 */     if ((this.counter >= 6) && (card.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.CURSE)) {
/* 84 */       card.cantUseMessage = (this.DESCRIPTIONS[5] + 6 + this.DESCRIPTIONS[1]);
/* 85 */       return false;
/*    */     }
/* 87 */     return true;
/*    */   }
/*    */   
/*    */   public void onVictory()
/*    */   {
/* 92 */     this.counter = -1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 97 */     return new VelvetChoker();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\VelvetChoker.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */