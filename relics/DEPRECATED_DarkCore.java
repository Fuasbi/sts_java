/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class DEPRECATED_DarkCore extends AbstractRelic {
/*    */   public static final String ID = "Dark Core";
/*    */   
/*    */   public DEPRECATED_DarkCore() {
/*  7 */     super("Dark Core", "vCore.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new DEPRECATED_DarkCore();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\DEPRECATED_DarkCore.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */