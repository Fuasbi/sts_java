/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class UnceasingTop extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Unceasing Top";
/* 12 */   private boolean canDraw = false;
/* 13 */   private boolean disabledUntilEndOfTurn = false;
/*    */   
/*    */   public UnceasingTop() {
/* 16 */     super("Unceasing Top", "top.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 26 */     this.canDraw = false;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 31 */     this.canDraw = true;
/* 32 */     this.disabledUntilEndOfTurn = false;
/*    */   }
/*    */   
/*    */   public void disableUntilTurnEnds() {
/* 36 */     this.disabledUntilEndOfTurn = true;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onRefreshHand()
/*    */   {
/* 47 */     if ((AbstractDungeon.actionManager.actions.isEmpty()) && (AbstractDungeon.player.hand.isEmpty()) && (!AbstractDungeon.actionManager.turnHasEnded) && (this.canDraw) && 
/* 48 */       (!AbstractDungeon.player.hasPower("No Draw")) && (!AbstractDungeon.isScreenUp))
/*    */     {
/* 50 */       if ((AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && (!this.disabledUntilEndOfTurn) && (
/* 51 */         (AbstractDungeon.player.discardPile.size() > 0) || (AbstractDungeon.player.drawPile.size() > 0))) {
/* 52 */         flash();
/* 53 */         AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 54 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 61 */     return new UnceasingTop();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\UnceasingTop.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */