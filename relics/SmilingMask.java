/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class SmilingMask extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Smiling Mask";
/*    */   public static final int COST = 50;
/*    */   
/*    */   public SmilingMask()
/*    */   {
/* 12 */     super("Smiling Mask", "merchantMask.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0] + 50 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 22 */     if ((room instanceof com.megacrit.cardcrawl.rooms.ShopRoom)) {
/* 23 */       flash();
/* 24 */       this.pulse = true;
/*    */     } else {
/* 26 */       this.pulse = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 32 */     return new SmilingMask();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SmilingMask.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */