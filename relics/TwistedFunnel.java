/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class TwistedFunnel extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "TwistedFunnel";
/*    */   private static final int POISON_AMT = 4;
/*    */   
/*    */   public TwistedFunnel()
/*    */   {
/* 14 */     super("TwistedFunnel", "funnel.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     flash();
/* 25 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 26 */       if ((!m.isDead) && (!m.isDying)) {
/* 27 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.PoisonPower(m, AbstractDungeon.player, 4), 4));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 38 */     return new TwistedFunnel();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\TwistedFunnel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */