/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class FrozenEgg2 extends AbstractRelic {
/*    */   public static final String ID = "Frozen Egg 2";
/*    */   
/*    */   public FrozenEgg2() {
/*  9 */     super("Frozen Egg 2", "frozenEgg.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onObtainCard(AbstractCard c)
/*    */   {
/* 19 */     if ((c.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER) && (c.canUpgrade()) && (!c.upgraded)) {
/* 20 */       c.upgrade();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 26 */     return new FrozenEgg2();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\FrozenEgg2.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */