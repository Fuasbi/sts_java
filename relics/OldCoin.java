/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class OldCoin extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Old Coin";
/*    */   private static final int GOLD_AMT = 300;
/*    */   
/*    */   public OldCoin()
/*    */   {
/* 12 */     super("Old Coin", "oldCoin.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 17 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 22 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("GOLD_GAIN");
/* 23 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainGold(300);
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 28 */     return new OldCoin();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\OldCoin.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */