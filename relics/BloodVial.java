/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BloodVial extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Blood Vial";
/*    */   private static final int HEAL_AMOUNT = 2;
/*    */   
/*    */   public BloodVial()
/*    */   {
/* 13 */     super("Blood Vial", "bloodVial.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0] + 2 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 23 */     flash();
/* 24 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 25 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.HealAction(AbstractDungeon.player, AbstractDungeon.player, 2));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 31 */     return new BloodVial();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\BloodVial.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */