/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class EmptyCage extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Empty Cage";
/* 11 */   private boolean cardSelected = true;
/*    */   
/*    */   public EmptyCage() {
/* 14 */     super("Empty Cage", "cage.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 24 */     this.cardSelected = false;
/* 25 */     if (AbstractDungeon.isScreenUp) {
/* 26 */       AbstractDungeon.dynamicBanner.hide();
/* 27 */       AbstractDungeon.overlayMenu.cancelButton.hide();
/* 28 */       AbstractDungeon.previousScreen = AbstractDungeon.screen;
/*    */     }
/* 30 */     AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.INCOMPLETE;
/*    */     
/* 32 */     AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 33 */       .getPurgeableCards(), 2, this.DESCRIPTIONS[1], false, false, false, true);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 44 */     super.update();
/* 45 */     if ((!this.cardSelected) && 
/* 46 */       (AbstractDungeon.gridSelectScreen.selectedCards.size() == 2)) {
/* 47 */       this.cardSelected = true;
/*    */       
/* 49 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/*    */       
/* 51 */         (com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), Settings.WIDTH / 2.0F - 30.0F * Settings.scale - com.megacrit.cardcrawl.cards.AbstractCard.IMG_WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */       
/*    */ 
/* 54 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/*    */       
/* 56 */         (com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(1), Settings.WIDTH / 2.0F + 30.0F * Settings.scale + com.megacrit.cardcrawl.cards.AbstractCard.IMG_WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */       
/*    */ 
/*    */ 
/* 60 */       for (com.megacrit.cardcrawl.cards.AbstractCard card : AbstractDungeon.gridSelectScreen.selectedCards) {
/* 61 */         AbstractDungeon.player.masterDeck.removeCard(card);
/* 62 */         AbstractDungeon.transformCard(card, true, AbstractDungeon.miscRng);
/*    */       }
/*    */       
/*    */ 
/* 66 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/* 67 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 74 */     return new EmptyCage();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\EmptyCage.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */