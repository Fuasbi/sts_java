/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class AncientTeaSet extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Ancient Tea Set";
/*    */   private static final int ENERGY_AMT = 2;
/* 13 */   private boolean firstTurn = true;
/*    */   
/*    */   public AncientTeaSet() {
/* 16 */     super("Ancient Tea Set", "teaSet.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.SOLID);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 21 */     if (AbstractDungeon.player != null) {
/* 22 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 24 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 29 */     if (c == null) {
/* 30 */       return this.DESCRIPTIONS[0];
/*    */     }
/* 32 */     switch (c) {
/*    */     case IRONCLAD: 
/* 34 */       return this.DESCRIPTIONS[0];
/*    */     case THE_SILENT: 
/* 36 */       return this.DESCRIPTIONS[1];
/*    */     case DEFECT: 
/* 38 */       return this.DESCRIPTIONS[2];
/*    */     }
/* 40 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(AbstractPlayer.PlayerClass c)
/*    */   {
/* 46 */     this.description = setDescription(c);
/* 47 */     this.tips.clear();
/* 48 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 49 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 54 */     if (this.firstTurn) {
/* 55 */       if (this.counter == -2) {
/* 56 */         this.pulse = false;
/* 57 */         this.counter = -1;
/* 58 */         flash();
/* 59 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(2));
/* 60 */         AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */       }
/* 62 */       this.firstTurn = false;
/*    */     }
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 68 */     this.firstTurn = true;
/*    */   }
/*    */   
/*    */   public void setCounter(int counter)
/*    */   {
/* 73 */     super.setCounter(counter);
/* 74 */     if (counter == -2) {
/* 75 */       this.pulse = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void onEnterRestRoom()
/*    */   {
/* 81 */     flash();
/* 82 */     this.counter = -2;
/* 83 */     this.pulse = true;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 88 */     return new AncientTeaSet();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\AncientTeaSet.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */