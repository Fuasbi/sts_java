/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class PeacePipe extends AbstractRelic {
/*    */   public static final String ID = "Peace Pipe";
/*    */   
/*    */   public PeacePipe() {
/*  7 */     super("Peace Pipe", "peacePipe.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 12 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 17 */     return new PeacePipe();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\PeacePipe.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */