/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class EternalFeather extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Eternal Feather";
/*    */   private static final int NUM_CARDS = 5;
/*    */   private static final int HEAL_AMT = 3;
/*    */   
/*    */   public EternalFeather()
/*    */   {
/* 14 */     super("Eternal Feather", "eternalFeather.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0] + 5 + this.DESCRIPTIONS[1] + 3 + this.DESCRIPTIONS[2];
/*    */   }
/*    */   
/*    */   public void onEnterRoom(AbstractRoom room)
/*    */   {
/* 24 */     if ((room instanceof com.megacrit.cardcrawl.rooms.RestRoom)) {
/* 25 */       flash();
/* 26 */       int amountToGain = com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.masterDeck.size() / 5 * 3;
/* 27 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.heal(amountToGain);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new EternalFeather();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\EternalFeather.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */