/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MutagenicStrength extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "MutagenicStrength";
/*    */   private static final int STR_AMT = 3;
/*    */   
/*    */   public MutagenicStrength()
/*    */   {
/* 15 */     super("MutagenicStrength", "mutagen.png", AbstractRelic.RelicTier.SPECIAL, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     flash();
/* 26 */     AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, 3), 3));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 32 */     AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.LoseStrengthPower(AbstractDungeon.player, 3), 3));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 38 */     AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 43 */     return new MutagenicStrength();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MutagenicStrength.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */