/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*    */ 
/*    */ public class SingingBowl extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Singing Bowl";
/*    */   public static final int HP_AMT = 2;
/*    */   
/*    */   public SingingBowl()
/*    */   {
/* 13 */     super("Singing Bowl", "singingBowl.png", AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.LandingSound.FLAT);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     super.update();
/*    */     
/* 25 */     if ((this.hb.hovered) && (InputHelper.justClickedLeft)) {
/* 26 */       com.megacrit.cardcrawl.core.CardCrawlGame.sound.playA("SINGING_BOWL", MathUtils.random(-0.2F, 0.1F));
/* 27 */       flash();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 33 */     return new SingingBowl();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\SingingBowl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */