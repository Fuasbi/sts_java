/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MarkOfPain extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Mark of Pain";
/*    */   private static final int CARD_AMT = 2;
/*    */   
/*    */   public MarkOfPain()
/*    */   {
/* 14 */     super("Mark of Pain", "markOfPain.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 19 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 24 */     flash();
/* 25 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 26 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(new com.megacrit.cardcrawl.cards.status.Wound(), 2, true, false));
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 31 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 36 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 41 */     return new MarkOfPain();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\MarkOfPain.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */