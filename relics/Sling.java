/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rooms.MonsterRoomElite;
/*    */ 
/*    */ public class Sling extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Sling";
/*    */   private static final int STR = 2;
/*    */   
/*    */   public Sling()
/*    */   {
/* 15 */     super("Sling", "sling.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 2 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atBattleStart()
/*    */   {
/* 25 */     if ((AbstractDungeon.getCurrRoom() instanceof MonsterRoomElite)) {
/* 26 */       flash();
/* 27 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, 2), 2));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 39 */     return new Sling();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Sling.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */