/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class Cauldron extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Cauldron";
/*    */   
/*    */   public Cauldron()
/*    */   {
/* 11 */     super("Cauldron", "cauldron.png", AbstractRelic.RelicTier.SHOP, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */   public void onEquip()
/*    */   {
/* 22 */     for (int i = 0; i < 5; i++) {
/* 23 */       AbstractDungeon.getCurrRoom().addPotionToRewards(com.megacrit.cardcrawl.helpers.PotionHelper.getRandomPotion());
/*    */     }
/*    */     
/* 26 */     AbstractDungeon.combatRewardScreen.open(this.DESCRIPTIONS[1]);
/* 27 */     AbstractDungeon.getCurrRoom().rewardPopOutTimer = 0.0F;
/*    */     
/*    */ 
/* 30 */     int remove = -1;
/* 31 */     for (int i = 0; i < AbstractDungeon.combatRewardScreen.rewards.size(); i++) {
/* 32 */       if (((com.megacrit.cardcrawl.rewards.RewardItem)AbstractDungeon.combatRewardScreen.rewards.get(i)).type == com.megacrit.cardcrawl.rewards.RewardItem.RewardType.CARD) {
/* 33 */         remove = i;
/* 34 */         break;
/*    */       }
/*    */     }
/* 37 */     if (remove != -1) {
/* 38 */       AbstractDungeon.combatRewardScreen.rewards.remove(remove);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 44 */     return new Cauldron();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\Cauldron.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */