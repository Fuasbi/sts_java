/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class CentennialPuzzle extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Centennial Puzzle";
/*    */   private static final int NUM_CARDS = 3;
/* 12 */   private static boolean usedThisCombat = false;
/*    */   
/*    */   public CentennialPuzzle() {
/* 15 */     super("Centennial Puzzle", "centennialPuzzle.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 20 */     return this.DESCRIPTIONS[0] + 3 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public void atPreBattle()
/*    */   {
/* 25 */     usedThisCombat = false;
/* 26 */     this.pulse = true;
/* 27 */     beginPulse();
/*    */   }
/*    */   
/*    */   public void onLoseHp(int damageAmount)
/*    */   {
/* 32 */     if ((damageAmount > 0) && 
/* 33 */       (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) && 
/* 34 */       (!usedThisCombat)) {
/* 35 */       flash();
/* 36 */       this.pulse = false;
/* 37 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 3));
/* 38 */       AbstractDungeon.actionManager.addToTop(new RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 39 */       usedThisCombat = true;
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void onVictory()
/*    */   {
/* 47 */     this.pulse = false;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 52 */     return new CentennialPuzzle();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\CentennialPuzzle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */