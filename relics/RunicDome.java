/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class RunicDome extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Runic Dome";
/*    */   
/*    */   public RunicDome()
/*    */   {
/* 11 */     super("Runic Dome", "runicDome.png", AbstractRelic.RelicTier.BOSS, AbstractRelic.LandingSound.HEAVY);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 16 */     if (AbstractDungeon.player != null) {
/* 17 */       return setDescription(AbstractDungeon.player.chosenClass);
/*    */     }
/* 19 */     return setDescription(null);
/*    */   }
/*    */   
/*    */   private String setDescription(com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass c)
/*    */   {
/* 24 */     if (c == null) {
/* 25 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     }
/* 27 */     switch (c) {
/*    */     case IRONCLAD: 
/* 29 */       return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */     case THE_SILENT: 
/* 31 */       return this.DESCRIPTIONS[2] + this.DESCRIPTIONS[0];
/*    */     case DEFECT: 
/* 33 */       return this.DESCRIPTIONS[3] + this.DESCRIPTIONS[0];
/*    */     }
/* 35 */     return this.DESCRIPTIONS[1] + this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription(com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass c)
/*    */   {
/* 41 */     this.description = setDescription(c);
/* 42 */     this.tips.clear();
/* 43 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 44 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 49 */     AbstractDungeon.player.energy.energyMaster += 1;
/*    */   }
/*    */   
/*    */   public void onUnequip()
/*    */   {
/* 54 */     AbstractDungeon.player.energy.energyMaster -= 1;
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 59 */     return new RunicDome();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RunicDome.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */