/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class IncenseBurner extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Incense Burner";
/*    */   private static final int NUM_TURNS = 6;
/*    */   
/*    */   public IncenseBurner()
/*    */   {
/* 13 */     super("Incense Burner", "incenseBurner.png", AbstractRelic.RelicTier.RARE, AbstractRelic.LandingSound.CLINK);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 18 */     return this.DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 23 */     this.counter = 0;
/*    */   }
/*    */   
/*    */   public void atTurnStart()
/*    */   {
/* 28 */     if (this.counter == -1) {
/* 29 */       this.counter += 2;
/*    */     } else {
/* 31 */       this.counter += 1;
/*    */     }
/*    */     
/* 34 */     if (this.counter == 6) {
/* 35 */       this.counter = 0;
/* 36 */       flash();
/* 37 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction(AbstractDungeon.player, this));
/* 38 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, null, new com.megacrit.cardcrawl.powers.IntangiblePlayerPower(AbstractDungeon.player, 1), 1));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 49 */     return new IncenseBurner();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\IncenseBurner.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */