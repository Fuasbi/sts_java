/*    */ package com.megacrit.cardcrawl.relics;
/*    */ 
/*    */ public class RegalPillow extends AbstractRelic
/*    */ {
/*    */   public static final String ID = "Regal Pillow";
/*    */   public static final int HEAL_AMT = 15;
/*    */   
/*    */   public RegalPillow() {
/*  9 */     super("Regal Pillow", "pillow.png", AbstractRelic.RelicTier.COMMON, AbstractRelic.LandingSound.MAGICAL);
/*    */   }
/*    */   
/*    */   public String getUpdatedDescription()
/*    */   {
/* 14 */     return this.DESCRIPTIONS[0] + 15 + this.DESCRIPTIONS[1];
/*    */   }
/*    */   
/*    */   public AbstractRelic makeCopy()
/*    */   {
/* 19 */     return new RegalPillow();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\relics\RegalPillow.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */