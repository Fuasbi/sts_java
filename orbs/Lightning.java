/*     */ package com.megacrit.cardcrawl.orbs;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.defect.LightningOrbEvokeAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.OrbStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.LightningEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.LightningOrbPassiveEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect.OrbFlareColor;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Lightning extends AbstractOrb
/*     */ {
/*     */   public static final String ORB_ID = "Lightning";
/*  33 */   private static final OrbStrings orbString = CardCrawlGame.languagePack.getOrbString("Lightning");
/*  34 */   public static final String[] DESC = orbString.DESCRIPTION;
/*  35 */   private float vfxTimer = 1.0F; private float vfxIntervalMin = 0.15F; private float vfxIntervalMax = 0.8F;
/*     */   
/*     */   private static final float PI_DIV_16 = 0.19634955F;
/*     */   private static final float ORB_WAVY_DIST = 0.05F;
/*     */   private static final float PI_4 = 12.566371F;
/*     */   private static final float ORB_BORDER_SCALE = 1.2F;
/*     */   
/*     */   public Lightning()
/*     */   {
/*  44 */     this.ID = "Lightning";
/*  45 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.ORB_LIGHTNING;
/*  46 */     this.name = orbString.NAME;
/*  47 */     this.baseEvokeAmount = 8;
/*  48 */     this.evokeAmount = this.baseEvokeAmount;
/*  49 */     this.basePassiveAmount = 3;
/*  50 */     this.passiveAmount = this.basePassiveAmount;
/*  51 */     updateDescription();
/*  52 */     this.angle = MathUtils.random(360.0F);
/*  53 */     this.channelAnimTimer = 0.5F;
/*     */   }
/*     */   
/*     */   public void updateDescription()
/*     */   {
/*  58 */     applyFocus();
/*  59 */     this.description = (DESC[0] + this.passiveAmount + DESC[1] + this.evokeAmount + DESC[2]);
/*     */   }
/*     */   
/*     */   public void onEvoke()
/*     */   {
/*  64 */     if (AbstractDungeon.player.hasPower("Electro")) {
/*  65 */       AbstractDungeon.actionManager.addToTop(new LightningOrbEvokeAction(new DamageInfo(AbstractDungeon.player, this.evokeAmount, DamageInfo.DamageType.THORNS), true));
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*  70 */       AbstractDungeon.actionManager.addToTop(new LightningOrbEvokeAction(new DamageInfo(AbstractDungeon.player, this.evokeAmount, DamageInfo.DamageType.THORNS), false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onEndOfTurn()
/*     */   {
/*  79 */     if (AbstractDungeon.player.hasPower("Electro")) {
/*  80 */       AbstractDungeon.actionManager.addToTop(new LightningOrbEvokeAction(new DamageInfo(AbstractDungeon.player, this.passiveAmount, DamageInfo.DamageType.THORNS), true));
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*  85 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.LightningOrbPassiveAction(new DamageInfo(AbstractDungeon.player, this.passiveAmount, DamageInfo.DamageType.THORNS), this, false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void triggerPassiveEffect(DamageInfo info, boolean hitAll)
/*     */   {
/*     */     float speedTime;
/*     */     
/*  95 */     if (!hitAll) {
/*  96 */       AbstractCreature m = AbstractDungeon.getRandomMonster();
/*     */       
/*  98 */       if (m != null) {
/*  99 */         speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 100 */         if (Settings.FAST_MODE) {
/* 101 */           speedTime = 0.0F;
/*     */         }
/*     */         
/* 104 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, info, AbstractGameAction.AttackEffect.NONE, true));
/* 105 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(new OrbFlareEffect(this, OrbFlareEffect.OrbFlareColor.LIGHTNING), speedTime));
/*     */         
/* 107 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(new LightningEffect(m.drawX, m.drawY), speedTime));
/*     */         
/* 109 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("ORB_LIGHTNING_EVOKE"));
/*     */       }
/*     */     } else {
/* 112 */       float speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 113 */       if (Settings.FAST_MODE) {
/* 114 */         speedTime = 0.0F;
/*     */       }
/*     */       
/* 117 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new OrbFlareEffect(this, OrbFlareEffect.OrbFlareColor.LIGHTNING), speedTime));
/*     */       
/* 119 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 122 */         DamageInfo.createDamageMatrix(info.base, true), DamageInfo.DamageType.THORNS, AbstractGameAction.AttackEffect.NONE));
/*     */       
/*     */ 
/*     */ 
/* 126 */       for (AbstractMonster m3 : AbstractDungeon.getMonsters().monsters) {
/* 127 */         if ((!m3.isDeadOrEscaped()) && (!m3.halfDead)) {
/* 128 */           AbstractDungeon.actionManager.addToBottom(new VFXAction(new LightningEffect(m3.drawX, m3.drawY), speedTime));
/*     */         }
/*     */       }
/*     */       
/* 132 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("ORB_LIGHTNING_EVOKE"));
/*     */     }
/*     */   }
/*     */   
/*     */   public void triggerEvokeAnimation()
/*     */   {
/* 138 */     CardCrawlGame.sound.play("ORB_LIGHTNING_EVOKE", 0.1F);
/* 139 */     AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.LightningOrbActivateEffect(this.cX, this.cY));
/*     */   }
/*     */   
/*     */   public void updateAnimation()
/*     */   {
/* 144 */     super.updateAnimation();
/* 145 */     this.angle += Gdx.graphics.getDeltaTime() * 180.0F;
/*     */     
/* 147 */     this.vfxTimer -= Gdx.graphics.getDeltaTime();
/* 148 */     if (this.vfxTimer < 0.0F) {
/* 149 */       AbstractDungeon.effectList.add(new LightningOrbPassiveEffect(this.cX, this.cY));
/* 150 */       if (MathUtils.randomBoolean()) {
/* 151 */         AbstractDungeon.effectList.add(new LightningOrbPassiveEffect(this.cX, this.cY));
/*     */       }
/* 153 */       this.vfxTimer = MathUtils.random(this.vfxIntervalMin, this.vfxIntervalMax);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 159 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.c.a / 2.0F));
/* 160 */     sb.setBlendFunction(770, 1);
/* 161 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.c.a / 2.0F));
/* 162 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale + 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 170 */       MathUtils.sin(this.angle / 12.566371F) * 0.05F + 0.19634955F, this.scale * 1.2F, this.angle, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 179 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale * 1.2F, this.scale + 
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 188 */       MathUtils.sin(this.angle / 12.566371F) * 0.05F + 0.19634955F, -this.angle, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 196 */     sb.setBlendFunction(770, 771);
/* 197 */     sb.setColor(this.c);
/* 198 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, this.angle / 12.0F, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 215 */     renderText(sb);
/* 216 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void playChannelSFX()
/*     */   {
/* 221 */     CardCrawlGame.sound.play("ORB_LIGHTNING_CHANNEL", 0.1F);
/*     */   }
/*     */   
/*     */   public AbstractOrb makeCopy()
/*     */   {
/* 226 */     return new Lightning();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\orbs\Lightning.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */