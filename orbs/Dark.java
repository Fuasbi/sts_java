/*     */ package com.megacrit.cardcrawl.orbs;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.OrbStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.DarkOrbPassiveEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Dark extends AbstractOrb
/*     */ {
/*     */   public static final String ORB_ID = "Dark";
/*  28 */   private static final OrbStrings orbString = CardCrawlGame.languagePack.getOrbString("Dark");
/*  29 */   public static final String[] DESC = orbString.DESCRIPTION;
/*     */   private static final float ORB_BORDER_SCALE = 1.2F;
/*  31 */   private float vfxTimer = 0.5F;
/*     */   private static final float VFX_INTERVAL_TIME = 0.25F;
/*     */   
/*     */   public Dark() {
/*  35 */     this.ID = "Dark";
/*  36 */     this.img = ImageMaster.ORB_DARK;
/*  37 */     this.name = orbString.NAME;
/*  38 */     this.baseEvokeAmount = 6;
/*  39 */     this.evokeAmount = this.baseEvokeAmount;
/*  40 */     this.basePassiveAmount = 6;
/*  41 */     this.passiveAmount = this.basePassiveAmount;
/*  42 */     updateDescription();
/*  43 */     this.channelAnimTimer = 0.5F;
/*     */   }
/*     */   
/*     */   public void updateDescription()
/*     */   {
/*  48 */     applyFocus();
/*  49 */     this.description = (DESC[0] + this.passiveAmount + DESC[1] + this.evokeAmount + DESC[2]);
/*     */   }
/*     */   
/*     */   public void onEvoke()
/*     */   {
/*  54 */     AbstractMonster weakestMonster = null;
/*  55 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  56 */       if (!m.isDeadOrEscaped()) {
/*  57 */         if (weakestMonster == null) {
/*  58 */           weakestMonster = m;
/*  59 */         } else if (m.currentHealth < weakestMonster.currentHealth) {
/*  60 */           weakestMonster = m;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  65 */     if (weakestMonster != null) {
/*  66 */       AbstractDungeon.actionManager.addToTop(new DamageAction(weakestMonster, new com.megacrit.cardcrawl.cards.DamageInfo(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  69 */         applyLockOn(weakestMonster, this.evokeAmount), DamageInfo.DamageType.THORNS), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void onEndOfTurn()
/*     */   {
/*  76 */     float speedTime = 0.6F / AbstractDungeon.player.orbs.size();
/*  77 */     if (Settings.FAST_MODE) {
/*  78 */       speedTime = 0.0F;
/*     */     }
/*     */     
/*  81 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect(this, com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect.OrbFlareColor.DARK), speedTime));
/*     */     
/*  83 */     this.evokeAmount += this.passiveAmount;
/*  84 */     updateDescription();
/*     */   }
/*     */   
/*     */   public void triggerEvokeAnimation()
/*     */   {
/*  89 */     CardCrawlGame.sound.play("ORB_DARK_EVOKE", 0.1F);
/*  90 */     AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.DarkOrbActivateEffect(this.cX, this.cY));
/*     */   }
/*     */   
/*     */   public void applyFocus()
/*     */   {
/*  95 */     AbstractPower power = AbstractDungeon.player.getPower("Focus");
/*  96 */     if (power != null) {
/*  97 */       this.passiveAmount = Math.max(0, this.basePassiveAmount + power.amount);
/*     */     } else {
/*  99 */       this.passiveAmount = this.basePassiveAmount;
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateAnimation()
/*     */   {
/* 105 */     super.updateAnimation();
/* 106 */     this.angle += Gdx.graphics.getDeltaTime() * 120.0F;
/*     */     
/* 108 */     this.vfxTimer -= Gdx.graphics.getDeltaTime();
/* 109 */     if (this.vfxTimer < 0.0F) {
/* 110 */       AbstractDungeon.effectList.add(new DarkOrbPassiveEffect(this.cX, this.cY));
/* 111 */       this.vfxTimer = 0.25F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 117 */     sb.setColor(this.c);
/* 118 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, this.angle, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 135 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.c.a / 3.0F));
/* 136 */     sb.setBlendFunction(770, 1);
/* 137 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale * 1.2F, this.scale * 1.2F, this.angle / 1.2F, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 154 */     sb.draw(this.img, this.cX - 48.0F, this.cY - 48.0F + this.bobEffect.y, 48.0F, 48.0F, 96.0F, 96.0F, this.scale * 1.5F, this.scale * 1.5F, this.angle / 1.4F, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 171 */     sb.setBlendFunction(770, 771);
/* 172 */     renderText(sb);
/* 173 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   protected void renderText(SpriteBatch sb)
/*     */   {
/* 178 */     FontHelper.renderFontCentered(sb, FontHelper.cardEnergyFont_L, 
/*     */     
/*     */ 
/* 181 */       Integer.toString(this.evokeAmount), this.cX + NUM_X_OFFSET, this.cY + this.bobEffect.y / 2.0F + NUM_Y_OFFSET - 4.0F * Settings.scale, new Color(0.2F, 1.0F, 1.0F, this.c.a), this.fontScale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 186 */     FontHelper.renderFontCentered(sb, FontHelper.cardEnergyFont_L, 
/*     */     
/*     */ 
/* 189 */       Integer.toString(this.passiveAmount), this.cX + NUM_X_OFFSET, this.cY + this.bobEffect.y / 2.0F + NUM_Y_OFFSET + 20.0F * Settings.scale, this.c, this.fontScale);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void playChannelSFX()
/*     */   {
/* 198 */     CardCrawlGame.sound.play("ORB_DARK_CHANNEL", 0.1F);
/*     */   }
/*     */   
/*     */   public AbstractOrb makeCopy()
/*     */   {
/* 203 */     return new Dark();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\orbs\Dark.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */