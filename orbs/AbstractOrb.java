/*     */ package com.megacrit.cardcrawl.orbs;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*     */ import com.badlogic.gdx.math.Interpolation.SwingIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public abstract class AbstractOrb
/*     */ {
/*     */   public String name;
/*     */   public String description;
/*     */   public String ID;
/*  28 */   protected ArrayList<PowerTip> tips = new ArrayList();
/*  29 */   public int evokeAmount = 0; public int passiveAmount = 0;
/*  30 */   protected int baseEvokeAmount = 0; protected int basePassiveAmount = 0;
/*     */   
/*     */ 
/*  33 */   public float cX = 0.0F; public float cY = 0.0F;
/*  34 */   public float tX; public float tY; protected Color c = Settings.CREAM_COLOR.cpy();
/*     */   protected static final int W = 96;
/*  36 */   public Hitbox hb = new Hitbox(96.0F * Settings.scale, 96.0F * Settings.scale);
/*  37 */   protected com.badlogic.gdx.graphics.Texture img = null;
/*  38 */   protected BobEffect bobEffect = new BobEffect(3.0F * Settings.scale, 3.0F);
/*  39 */   protected static final float NUM_X_OFFSET = 20.0F * Settings.scale;
/*  40 */   protected static final float NUM_Y_OFFSET = -12.0F * Settings.scale;
/*  41 */   protected float angle; protected float scale; protected float fontScale = 0.7F;
/*  42 */   protected boolean showEvokeValue = false;
/*     */   protected static final float CHANNEL_IN_TIME = 0.5F;
/*  44 */   protected float channelAnimTimer = 0.5F;
/*     */   
/*     */   public abstract void updateDescription();
/*     */   
/*     */   public abstract void onEvoke();
/*     */   
/*     */   public static AbstractOrb getRandomOrb(boolean useCardRng) {
/*  51 */     ArrayList<AbstractOrb> orbs = new ArrayList();
/*  52 */     orbs.add(new Dark());
/*  53 */     orbs.add(new Frost());
/*  54 */     orbs.add(new Lightning());
/*  55 */     orbs.add(new Plasma());
/*     */     
/*  57 */     if (useCardRng) {
/*  58 */       return (AbstractOrb)orbs.get(AbstractDungeon.cardRng.random(orbs.size() - 1));
/*     */     }
/*  60 */     return (AbstractOrb)orbs.get(MathUtils.random(orbs.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void onStartOfTurn() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onEndOfTurn() {}
/*     */   
/*     */ 
/*     */   public void applyFocus()
/*     */   {
/*  74 */     AbstractPower power = AbstractDungeon.player.getPower("Focus");
/*  75 */     if ((power != null) && (!this.ID.equals("Plasma"))) {
/*  76 */       this.passiveAmount = Math.max(0, this.basePassiveAmount + power.amount);
/*  77 */       this.evokeAmount = Math.max(0, this.baseEvokeAmount + power.amount);
/*     */     } else {
/*  79 */       this.passiveAmount = this.basePassiveAmount;
/*  80 */       this.evokeAmount = this.baseEvokeAmount;
/*     */     }
/*     */   }
/*     */   
/*     */   public static int applyLockOn(AbstractCreature target, int dmg) {
/*  85 */     int retVal = dmg;
/*  86 */     if (target.hasPower("Lockon")) {
/*  87 */       retVal = (int)(retVal * 1.5F);
/*     */     }
/*  89 */     return retVal;
/*     */   }
/*     */   
/*     */   public abstract AbstractOrb makeCopy();
/*     */   
/*     */   public void update() {
/*  95 */     this.hb.update();
/*  96 */     if (this.hb.hovered) {
/*  97 */       com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(this.tX + 96.0F * Settings.scale, this.tY + 64.0F * Settings.scale, this.name, this.description);
/*     */     }
/*  99 */     this.fontScale = MathHelper.scaleLerpSnap(this.fontScale, 0.7F);
/*     */   }
/*     */   
/*     */   public void updateAnimation() {
/* 103 */     this.bobEffect.update();
/* 104 */     this.cX = MathHelper.orbLerpSnap(this.cX, AbstractDungeon.player.animX + this.tX);
/* 105 */     this.cY = MathHelper.orbLerpSnap(this.cY, AbstractDungeon.player.animY + this.tY);
/*     */     
/* 107 */     if (this.channelAnimTimer != 0.0F) {
/* 108 */       this.channelAnimTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 109 */       if (this.channelAnimTimer < 0.0F) {
/* 110 */         this.channelAnimTimer = 0.0F;
/*     */       }
/*     */     }
/*     */     
/* 114 */     this.c.a = Interpolation.pow2In.apply(1.0F, 0.01F, this.channelAnimTimer / 0.5F);
/* 115 */     this.scale = Interpolation.swingIn.apply(Settings.scale, 0.01F, this.channelAnimTimer / 0.5F);
/*     */   }
/*     */   
/*     */   public void setSlot(int slotNum, int maxOrbs) {
/* 119 */     float dist = 160.0F * Settings.scale + maxOrbs * 10.0F * Settings.scale;
/* 120 */     float angle = 100.0F + maxOrbs * 12.0F;
/* 121 */     float offsetAngle = angle / 2.0F;
/* 122 */     angle *= slotNum / (maxOrbs - 1.0F);
/* 123 */     angle += 90.0F - offsetAngle;
/* 124 */     this.tX = (dist * MathUtils.cosDeg(angle) + AbstractDungeon.player.drawX);
/* 125 */     this.tY = (dist * MathUtils.sinDeg(angle) + AbstractDungeon.player.drawY + AbstractDungeon.player.hb_h / 2.0F);
/*     */     
/* 127 */     if (maxOrbs == 1) {
/* 128 */       this.tX = AbstractDungeon.player.drawX;
/* 129 */       this.tY = (160.0F * Settings.scale + AbstractDungeon.player.drawY + AbstractDungeon.player.hb_h / 2.0F);
/*     */     }
/*     */     
/* 132 */     this.hb.move(this.tX, this.tY);
/*     */   }
/*     */   
/*     */   public abstract void render(SpriteBatch paramSpriteBatch);
/*     */   
/*     */   protected void renderText(SpriteBatch sb) {
/* 138 */     if (!(this instanceof EmptyOrbSlot)) {
/* 139 */       if (this.showEvokeValue) {
/* 140 */         FontHelper.renderFontCentered(sb, FontHelper.cardEnergyFont_L, 
/*     */         
/*     */ 
/* 143 */           Integer.toString(this.evokeAmount), this.cX + NUM_X_OFFSET, this.cY + this.bobEffect.y / 2.0F + NUM_Y_OFFSET, new Color(0.2F, 1.0F, 1.0F, this.c.a), this.fontScale);
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 149 */         FontHelper.renderFontCentered(sb, FontHelper.cardEnergyFont_L, 
/*     */         
/*     */ 
/* 152 */           Integer.toString(this.passiveAmount), this.cX + NUM_X_OFFSET, this.cY + this.bobEffect.y / 2.0F + NUM_Y_OFFSET, this.c, this.fontScale);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void triggerEvokeAnimation() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void showEvokeValue()
/*     */   {
/* 165 */     this.showEvokeValue = true;
/* 166 */     this.fontScale = 1.5F;
/*     */   }
/*     */   
/*     */   public void hideEvokeValues() {
/* 170 */     this.showEvokeValue = false;
/*     */   }
/*     */   
/*     */   public abstract void playChannelSFX();
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\orbs\AbstractOrb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */