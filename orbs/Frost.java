/*     */ package com.megacrit.cardcrawl.orbs;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.OrbStrings;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.FrostOrbPassiveEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Frost extends AbstractOrb
/*     */ {
/*     */   public static final String ORB_ID = "Frost";
/*  21 */   private static final OrbStrings orbString = CardCrawlGame.languagePack.getOrbString("Frost");
/*  22 */   public static final String[] DESC = orbString.DESCRIPTION;
/*     */   private static Texture img1;
/*     */   private static Texture img2;
/*  25 */   private static Texture img3; private boolean hFlip1; private boolean hFlip2; private float vfxTimer = 1.0F; private float vfxIntervalMin = 0.15F; private float vfxIntervalMax = 0.8F;
/*     */   
/*     */   public Frost() {
/*  28 */     if (img1 == null) {
/*  29 */       img1 = ImageMaster.loadImage("images/orbs/frostRight.png");
/*  30 */       img2 = ImageMaster.loadImage("images/orbs/frostLeft.png");
/*  31 */       img3 = ImageMaster.loadImage("images/orbs/frostMid.png");
/*     */     }
/*     */     
/*  34 */     this.hFlip1 = MathUtils.randomBoolean();
/*  35 */     this.hFlip2 = MathUtils.randomBoolean();
/*     */     
/*  37 */     this.ID = "Frost";
/*  38 */     this.name = orbString.NAME;
/*  39 */     this.baseEvokeAmount = 5;
/*  40 */     this.evokeAmount = this.baseEvokeAmount;
/*  41 */     this.basePassiveAmount = 2;
/*  42 */     this.passiveAmount = this.basePassiveAmount;
/*  43 */     updateDescription();
/*  44 */     this.channelAnimTimer = 0.5F;
/*     */   }
/*     */   
/*     */   public void updateDescription()
/*     */   {
/*  49 */     applyFocus();
/*  50 */     this.description = (DESC[0] + this.passiveAmount + DESC[1] + this.evokeAmount + DESC[2]);
/*     */   }
/*     */   
/*     */   public void onEvoke()
/*     */   {
/*  55 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.evokeAmount));
/*     */   }
/*     */   
/*     */ 
/*     */   public void updateAnimation()
/*     */   {
/*  61 */     super.updateAnimation();
/*  62 */     this.angle += Gdx.graphics.getDeltaTime() * 180.0F;
/*     */     
/*  64 */     this.vfxTimer -= Gdx.graphics.getDeltaTime();
/*  65 */     if (this.vfxTimer < 0.0F) {
/*  66 */       AbstractDungeon.effectList.add(new FrostOrbPassiveEffect(this.cX, this.cY));
/*  67 */       if (MathUtils.randomBoolean()) {
/*  68 */         AbstractDungeon.effectList.add(new FrostOrbPassiveEffect(this.cX, this.cY));
/*     */       }
/*  70 */       this.vfxTimer = MathUtils.random(this.vfxIntervalMin, this.vfxIntervalMax);
/*     */     }
/*     */   }
/*     */   
/*     */   public void onEndOfTurn()
/*     */   {
/*  76 */     float speedTime = 0.6F / AbstractDungeon.player.orbs.size();
/*  77 */     if (com.megacrit.cardcrawl.core.Settings.FAST_MODE) {
/*  78 */       speedTime = 0.0F;
/*     */     }
/*     */     
/*  81 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect(this, com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect.OrbFlareColor.FROST), speedTime));
/*     */     
/*  83 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.passiveAmount, true));
/*     */   }
/*     */   
/*     */ 
/*     */   public void triggerEvokeAnimation()
/*     */   {
/*  89 */     CardCrawlGame.sound.play("ORB_FROST_EVOKE", 0.1F);
/*  90 */     AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.FrostOrbActivateEffect(this.cX, this.cY));
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  95 */     sb.setColor(this.c);
/*  96 */     sb.draw(img1, this.cX - 48.0F + this.bobEffect.y / 4.0F, this.cY - 48.0F + this.bobEffect.y / 4.0F, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, 0.0F, 0, 0, 96, 96, this.hFlip1, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 113 */     sb.draw(img2, this.cX - 48.0F + this.bobEffect.y / 4.0F, this.cY - 48.0F - this.bobEffect.y / 4.0F, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, 0.0F, 0, 0, 96, 96, this.hFlip1, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 132 */     sb.draw(img3, this.cX - 48.0F - this.bobEffect.y / 4.0F, this.cY - 48.0F + this.bobEffect.y / 2.0F, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, 0.0F, 0, 0, 96, 96, this.hFlip2, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 149 */     renderText(sb);
/* 150 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void playChannelSFX()
/*     */   {
/* 155 */     CardCrawlGame.sound.play("ORB_FROST_CHANNEL", 0.1F);
/*     */   }
/*     */   
/*     */   public AbstractOrb makeCopy()
/*     */   {
/* 160 */     return new Frost();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\orbs\Frost.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */