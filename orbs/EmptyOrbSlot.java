/*     */ package com.megacrit.cardcrawl.orbs;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.OrbStrings;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ 
/*     */ public class EmptyOrbSlot extends AbstractOrb
/*     */ {
/*     */   public static final String ORB_ID = "Empty";
/*  14 */   private static final OrbStrings orbString = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getOrbString("Empty");
/*  15 */   public static final String[] DESC = orbString.DESCRIPTION;
/*     */   private static com.badlogic.gdx.graphics.Texture img1;
/*     */   private static com.badlogic.gdx.graphics.Texture img2;
/*     */   
/*  19 */   public EmptyOrbSlot(float x, float y) { if (img1 == null) {
/*  20 */       img1 = ImageMaster.loadImage("images/orbs/empty2.png");
/*  21 */       img2 = ImageMaster.loadImage("images/orbs/empty3.png");
/*     */     }
/*  23 */     this.angle = MathUtils.random(360.0F);
/*     */     
/*  25 */     this.ID = "Empty";
/*  26 */     this.name = orbString.NAME;
/*  27 */     this.evokeAmount = 0;
/*  28 */     this.cX = x;
/*  29 */     this.cY = y;
/*  30 */     updateDescription();
/*  31 */     this.channelAnimTimer = 0.5F;
/*     */   }
/*     */   
/*     */   public EmptyOrbSlot() {
/*  35 */     if (img1 == null) {
/*  36 */       img1 = ImageMaster.loadImage("images/orbs/empty1.png");
/*  37 */       img2 = ImageMaster.loadImage("images/orbs/empty2.png");
/*     */     }
/*  39 */     this.angle = MathUtils.random(360.0F);
/*     */     
/*  41 */     this.name = orbString.NAME;
/*  42 */     this.evokeAmount = 0;
/*  43 */     this.cX = (AbstractDungeon.player.drawX + AbstractDungeon.player.hb_x);
/*  44 */     this.cY = (AbstractDungeon.player.drawY + AbstractDungeon.player.hb_y + AbstractDungeon.player.hb_h / 2.0F);
/*  45 */     updateDescription();
/*     */   }
/*     */   
/*     */   public void updateDescription()
/*     */   {
/*  50 */     this.description = DESC[0];
/*     */   }
/*     */   
/*     */ 
/*     */   public void onEvoke() {}
/*     */   
/*     */ 
/*     */   public void updateAnimation()
/*     */   {
/*  59 */     super.updateAnimation();
/*  60 */     this.angle += com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 10.0F;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  65 */     sb.setColor(this.c);
/*     */     
/*  67 */     sb.draw(img2, this.cX - 48.0F - this.bobEffect.y / 8.0F, this.cY - 48.0F + this.bobEffect.y / 8.0F, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, 0.0F, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  84 */     sb.draw(img1, this.cX - 48.0F + this.bobEffect.y / 8.0F, this.cY - 48.0F - this.bobEffect.y / 8.0F, 48.0F, 48.0F, 96.0F, 96.0F, this.scale, this.scale, this.angle, 0, 0, 96, 96, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 101 */     renderText(sb);
/* 102 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public AbstractOrb makeCopy()
/*     */   {
/* 107 */     return new EmptyOrbSlot();
/*     */   }
/*     */   
/*     */   public void playChannelSFX() {}
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\orbs\EmptyOrbSlot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */