/*    */ package com.megacrit.cardcrawl.desktop;
/*    */ 
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglGraphics.SetDisplayModeCallback;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class STSDisplayModeCallback
/*    */   extends LwjglApplicationConfiguration
/*    */   implements LwjglGraphics.SetDisplayModeCallback
/*    */ {
/* 16 */   private static final Logger logger = LogManager.getLogger(STSDisplayModeCallback.class.getName());
/*    */   
/*    */   public LwjglApplicationConfiguration onFailure(LwjglApplicationConfiguration initialConfig)
/*    */   {
/* 20 */     logger.error("Failure to display LwjglApplication. InitialConfig=" + initialConfig.toString());
/* 21 */     initialConfig.width = 1280;
/* 22 */     initialConfig.height = 720;
/* 23 */     initialConfig.fullscreen = false;
/* 24 */     return initialConfig;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\desktop\STSDisplayModeCallback.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */