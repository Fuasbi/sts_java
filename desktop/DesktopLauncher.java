/*    */ package com.megacrit.cardcrawl.desktop;
/*    */ 
/*    */ import com.badlogic.gdx.Application;
/*    */ import com.badlogic.gdx.Files.FileType;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics.DisplayMode;
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.DisplayConfig;
/*    */ import com.megacrit.cardcrawl.core.ExceptionHandler;
/*    */ import com.megacrit.cardcrawl.core.STSSentry;
/*    */ import com.megacrit.cardcrawl.core.SystemStats;
/*    */ import java.nio.charset.Charset;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DesktopLauncher
/*    */ {
/* 22 */   static { System.setProperty("log4j.configurationFile", "log4j2.xml"); }
/*    */   
/* 24 */   private static final Logger logger = LogManager.getLogger(DesktopLauncher.class.getName());
/*    */   
/*    */   public static void main(String[] arg) {
/* 27 */     logger.info("time: " + System.currentTimeMillis());
/* 28 */     logger.info("version: " + CardCrawlGame.TRUE_VERSION_NUM);
/* 29 */     logger.info("libgdx:  1.9.5");
/* 30 */     logger.info("default_locale: " + java.util.Locale.getDefault());
/* 31 */     logger.info("default_charset: " + Charset.defaultCharset());
/* 32 */     logger.info("default_encoding: " + System.getProperty("file.encoding"));
/* 33 */     logger.info("java_version: " + System.getProperty("java.version"));
/* 34 */     logger.info("os_arch: " + System.getProperty("os.arch"));
/* 35 */     logger.info("os_name: " + System.getProperty("os.name"));
/* 36 */     logger.info("os_version: " + System.getProperty("os.version"));
/* 37 */     SystemStats.logMemoryStats();
/* 38 */     SystemStats.logDiskStats();
/*    */     
/*    */ 
/* 41 */     STSSentry.setup();
/*    */     
/*    */     try
/*    */     {
/* 45 */       LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
/* 46 */       config.setDisplayModeCallback = new STSDisplayModeCallback();
/*    */       
/* 48 */       config.addIcon("images/ui/icon.png", Files.FileType.Internal);
/* 49 */       config.resizable = false;
/* 50 */       config.title = "Slay the Spire";
/* 51 */       loadSettings(config);
/* 52 */       logger.info("Launching application...");
/* 53 */       new LwjglApplication(new CardCrawlGame(config.preferencesDirectory), config);
/*    */     } catch (Exception e) {
/* 55 */       ExceptionHandler.handleException(e, logger);
/* 56 */       LogManager.shutdown();
/* 57 */       Gdx.app.exit();
/*    */     }
/*    */   }
/*    */   
/*    */   private static void loadSettings(LwjglApplicationConfiguration config) {
/* 62 */     DisplayConfig displayConf = DisplayConfig.readConfig();
/* 63 */     config.width = displayConf.getWidth();
/* 64 */     config.height = displayConf.getHeight();
/* 65 */     config.foregroundFPS = displayConf.getMaxFPS();
/* 66 */     config.backgroundFPS = config.foregroundFPS;
/*    */     
/* 68 */     if (displayConf.getIsFullscreen()) {
/* 69 */       System.setProperty("org.lwjgl.opengl.Display.enableOSXFullscreenModeAPI", "true");
/* 70 */       System.setProperty("org.lwjgl.opengl.Window.undecorated", "false");
/* 71 */       config.fullscreen = true;
/* 72 */       config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
/* 73 */       config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
/*    */     } else {
/* 75 */       config.fullscreen = false;
/*    */       
/* 77 */       if (displayConf.getWFS()) {
/* 78 */         System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
/* 79 */         config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
/* 80 */         config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 85 */     config.vSyncEnabled = displayConf.getIsVsync();
/* 86 */     logger.info("Settings successfully loaded");
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\desktop\DesktopLauncher.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */