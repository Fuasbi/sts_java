/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class FruitJuice extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Fruit Juice";
/* 13 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Fruit Juice");
/* 14 */   public static final String NAME = potionStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */   public FruitJuice() {
/* 18 */     super(NAME, "Fruit Juice", AbstractPotion.PotionRarity.RARE, AbstractPotion.PotionSize.HEART, AbstractPotion.PotionColor.FRUIT);
/* 19 */     this.potency = getPotency();
/* 20 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 21 */     this.isThrown = false;
/* 22 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 27 */     AbstractDungeon.player.increaseMaxHp(this.potency, true);
/*    */   }
/*    */   
/*    */   public boolean canUse()
/*    */   {
/* 32 */     if ((AbstractDungeon.actionManager.turnHasEnded) && 
/* 33 */       (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT)) {
/* 34 */       return false;
/*    */     }
/* 36 */     if ((AbstractDungeon.getCurrRoom().event != null) && 
/* 37 */       ((AbstractDungeon.getCurrRoom().event instanceof com.megacrit.cardcrawl.events.shrines.WeMeetAgain))) {
/* 38 */       return false;
/*    */     }
/*    */     
/* 41 */     return true;
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 46 */     return new FruitJuice();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 51 */     return 5;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\FruitJuice.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */