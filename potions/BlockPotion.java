/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class BlockPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Block Potion";
/* 14 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Block Potion");
/* 15 */   public static final String NAME = potionStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */   public BlockPotion() {
/* 19 */     super(NAME, "Block Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.S, AbstractPotion.PotionColor.BLUE);
/* 20 */     this.potency = getPotency();
/* 21 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 22 */     this.isThrown = false;
/* 23 */     this.tips.add(new PowerTip(this.name, this.description));
/* 24 */     this.tips.add(new PowerTip(
/*    */     
/* 26 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.BLOCK.NAMES[0]), 
/* 27 */       (String)GameDictionary.keywords.get(GameDictionary.BLOCK.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 32 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.potency));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 38 */     return new BlockPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 43 */     return 12;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\BlockPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */