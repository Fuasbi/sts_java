/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ 
/*    */ public class FairyPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "FairyPotion";
/* 12 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("FairyPotion");
/* 13 */   public static final String NAME = potionStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public FairyPotion()
/*    */   {
/* 20 */     super(NAME, "FairyPotion", AbstractPotion.PotionRarity.RARE, AbstractPotion.PotionSize.FAIRY, AbstractPotion.PotionColor.FAIRY);
/* 21 */     this.potency = getPotency();
/* 22 */     this.description = DESCRIPTIONS[0];
/* 23 */     this.isThrown = false;
/* 24 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 29 */     int healAmt = (int)(AbstractDungeon.player.maxHealth * 0.1F);
/*    */     
/*    */ 
/* 32 */     if (AbstractDungeon.player.hasBlight("FullBelly")) {
/* 33 */       healAmt /= 2;
/*    */     }
/*    */     
/* 36 */     if (healAmt < 1) {
/* 37 */       healAmt = 1;
/*    */     }
/*    */     
/* 40 */     AbstractDungeon.player.heal(healAmt, true);
/*    */   }
/*    */   
/*    */   public boolean canUse()
/*    */   {
/* 45 */     return false;
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 50 */     return new FairyPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 55 */     return 10;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\FairyPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */