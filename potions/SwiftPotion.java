/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ 
/*    */ public class SwiftPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Swift Potion";
/* 12 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Swift Potion");
/* 13 */   public static final String NAME = potionStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public SwiftPotion()
/*    */   {
/* 20 */     super(NAME, "Swift Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.H, AbstractPotion.PotionColor.SWIFT);
/* 21 */     this.potency = getPotency();
/* 22 */     this.description = (DESCRIPTIONS[1] + this.potency + DESCRIPTIONS[2]);
/* 23 */     this.isThrown = false;
/* 24 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 29 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, this.potency));
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 34 */     return new SwiftPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 39 */     return 3;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\SwiftPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */