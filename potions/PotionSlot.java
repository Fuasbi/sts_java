/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ 
/*    */ public class PotionSlot extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Potion Slot";
/* 10 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Potion Slot");
/* 11 */   public static final String NAME = potionStrings.NAME;
/* 12 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public PotionSlot(int slot)
/*    */   {
/* 20 */     super(NAME, "Potion Slot", AbstractPotion.PotionRarity.PLACEHOLDER, AbstractPotion.PotionSize.T, AbstractPotion.PotionColor.NONE);
/* 21 */     this.isObtained = true;
/* 22 */     this.description = DESCRIPTIONS[0];
/* 23 */     this.name = DESCRIPTIONS[1];
/* 24 */     this.tips.add(new PowerTip(this.name, this.description));
/* 25 */     adjustPosition(slot);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void use(AbstractCreature target) {}
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 35 */     return null;
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 40 */     return 0;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\PotionSlot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */