/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class PoisonPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Poison Potion";
/* 15 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Poison Potion");
/* 16 */   public static final String NAME = potionStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public PoisonPotion()
/*    */   {
/* 23 */     super(NAME, "Poison Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.BOTTLE, AbstractPotion.PotionColor.POISON);
/* 24 */     this.potency = getPotency();
/* 25 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 26 */     this.isThrown = true;
/* 27 */     this.targetRequired = true;
/* 28 */     this.tips.add(new PowerTip(this.name, this.description));
/* 29 */     this.tips.add(new PowerTip(
/*    */     
/* 31 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.POISON.NAMES[0]), 
/* 32 */       (String)GameDictionary.keywords.get(GameDictionary.POISON.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 37 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new PoisonPower(target, AbstractDungeon.player, this.potency), this.potency));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 47 */     return new PoisonPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 52 */     return 6;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\PoisonPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */