/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SkillPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "SkillPotion";
/* 13 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("SkillPotion");
/* 14 */   public static final String NAME = potionStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */   public SkillPotion() {
/* 18 */     super(NAME, "SkillPotion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.CARD, AbstractPotion.PotionColor.GREEN);
/* 19 */     this.potency = getPotency();
/* 20 */     this.description = DESCRIPTIONS[0];
/* 21 */     this.isThrown = false;
/* 22 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 27 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.DiscoveryAction(AbstractCard.CardType.SKILL));
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 32 */     return new SkillPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 37 */     return 1;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\SkillPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */