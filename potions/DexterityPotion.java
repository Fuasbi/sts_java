/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.DexterityPower;
/*    */ import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DexterityPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Dexterity Potion";
/* 17 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Dexterity Potion");
/* 18 */   public static final String NAME = potionStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public DexterityPotion()
/*    */   {
/* 25 */     super(NAME, "Dexterity Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.S, AbstractPotion.PotionColor.GREEN);
/* 26 */     this.potency = getPotency();
/* 27 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 28 */     this.isThrown = false;
/* 29 */     this.tips.add(new PowerTip(this.name, this.description));
/* 30 */     this.tips.add(new PowerTip(
/*    */     
/* 32 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.DEXTERITY.NAMES[0]), 
/* 33 */       (String)GameDictionary.keywords.get(GameDictionary.DEXTERITY.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 38 */     target = AbstractDungeon.player;
/* 39 */     AbstractDungeon.effectList.add(new FlashAtkImgEffect(target.hb.cX, target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SHIELD));
/* 40 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new DexterityPower(target, this.potency), this.potency));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 46 */     return new DexterityPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 51 */     return 2;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\DexterityPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */