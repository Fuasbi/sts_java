/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class EnergyPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Energy Potion";
/* 12 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Energy Potion");
/* 13 */   public static final String NAME = potionStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public EnergyPotion()
/*    */   {
/* 20 */     super(NAME, "Energy Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.BOLT, AbstractPotion.PotionColor.ENERGY);
/* 21 */     this.potency = getPotency();
/* 22 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 23 */     this.isThrown = false;
/* 24 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 29 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.potency));
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 34 */     return new EnergyPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 39 */     return 2;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\EnergyPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */