/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class AncientPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Ancient Potion";
/* 16 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Ancient Potion");
/* 17 */   public static final String NAME = potionStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */   public AncientPotion() {
/* 21 */     super(NAME, "Ancient Potion", AbstractPotion.PotionRarity.UNCOMMON, AbstractPotion.PotionSize.FAIRY, AbstractPotion.PotionColor.ANCIENT);
/* 22 */     this.potency = getPotency();
/* 23 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 24 */     this.isThrown = false;
/* 25 */     this.tips.add(new PowerTip(this.name, this.description));
/* 26 */     this.tips.add(new PowerTip(
/*    */     
/* 28 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.ARTIFACT.NAMES[0]), 
/* 29 */       (String)GameDictionary.keywords.get(GameDictionary.ARTIFACT.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 34 */     target = AbstractDungeon.player;
/* 35 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 36 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new ArtifactPower(target, this.potency), this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 43 */     return new AncientPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 48 */     return 1;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\AncientPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */