/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class StrengthPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Strength Potion";
/* 16 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Strength Potion");
/* 17 */   public static final String NAME = potionStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public StrengthPotion()
/*    */   {
/* 24 */     super(NAME, "Strength Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.S, AbstractPotion.PotionColor.STRENGTH);
/* 25 */     this.potency = getPotency();
/* 26 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 27 */     this.isThrown = false;
/* 28 */     this.tips.add(new PowerTip(this.name, this.description));
/* 29 */     this.tips.add(new PowerTip(
/*    */     
/* 31 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.STRENGTH.NAMES[0]), 
/* 32 */       (String)GameDictionary.keywords.get(GameDictionary.STRENGTH.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 37 */     target = AbstractDungeon.player;
/* 38 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 39 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new StrengthPower(target, this.potency), this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 46 */     return new StrengthPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 51 */     return 2;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\StrengthPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */