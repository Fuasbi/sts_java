/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GhostInAJar extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "GhostInAJar";
/* 16 */   private static final PotionStrings potionStrings = CardCrawlGame.languagePack.getPotionString("GhostInAJar");
/* 17 */   public static final String NAME = potionStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public GhostInAJar()
/*    */   {
/* 24 */     super(NAME, "GhostInAJar", AbstractPotion.PotionRarity.UNCOMMON, AbstractPotion.PotionSize.GHOST, AbstractPotion.PotionColor.WHITE);
/* 25 */     this.potency = getPotency();
/* 26 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 27 */     this.isThrown = false;
/* 28 */     this.tips.add(new PowerTip(this.name, this.description));
/* 29 */     this.tips.add(new PowerTip(
/*    */     
/* 31 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.INTANGIBLE.NAMES[0]), 
/* 32 */       (String)GameDictionary.keywords.get(GameDictionary.INTANGIBLE.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 37 */     target = AbstractDungeon.player;
/* 38 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 39 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.IntangiblePlayerPower(target, this.potency), this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 50 */     return new GhostInAJar();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 55 */     return 1;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\GhostInAJar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */