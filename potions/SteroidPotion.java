/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.LoseStrengthPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SteroidPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "SteroidPotion";
/* 17 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("SteroidPotion");
/* 18 */   public static final String NAME = potionStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public SteroidPotion()
/*    */   {
/* 25 */     super(NAME, "SteroidPotion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.FAIRY, AbstractPotion.PotionColor.STEROID);
/* 26 */     this.potency = getPotency();
/* 27 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1] + this.potency + DESCRIPTIONS[2]);
/* 28 */     this.isThrown = false;
/* 29 */     this.tips.add(new PowerTip(this.name, this.description));
/* 30 */     this.tips.add(new PowerTip(
/*    */     
/* 32 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.STRENGTH.NAMES[0]), 
/* 33 */       (String)GameDictionary.keywords.get(GameDictionary.STRENGTH.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 38 */     target = AbstractDungeon.player;
/* 39 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(target, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(target, this.potency), this.potency));
/*    */       
/* 42 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(target, AbstractDungeon.player, new LoseStrengthPower(target, this.potency), this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 49 */     return new SteroidPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 54 */     return 5;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\SteroidPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */