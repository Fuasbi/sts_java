/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ 
/*    */ public class EntropicBrew extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "EntropicBrew";
/* 12 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("EntropicBrew");
/* 13 */   public static final String NAME = potionStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public EntropicBrew()
/*    */   {
/* 20 */     super(NAME, "EntropicBrew", AbstractPotion.PotionRarity.RARE, AbstractPotion.PotionSize.BOTTLE, AbstractPotion.PotionColor.ANCIENT);
/* 21 */     this.description = DESCRIPTIONS[0];
/* 22 */     this.potency = getPotency();
/* 23 */     this.isThrown = false;
/* 24 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 29 */     for (int i = 0; i < AbstractDungeon.player.potionSlots; i++) {
/* 30 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ObtainPotionAction(AbstractDungeon.returnRandomPotion(true)));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 36 */     return new EntropicBrew();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 41 */     if (AbstractDungeon.player != null) {
/* 42 */       return AbstractDungeon.player.potionSlots;
/*    */     }
/* 44 */     return 3;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\EntropicBrew.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */