/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.powers.ConfusionPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SneckoOil extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "SneckoOil";
/* 17 */   private static final PotionStrings potionStrings = CardCrawlGame.languagePack.getPotionString("SneckoOil");
/* 18 */   public static final String NAME = potionStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public SneckoOil()
/*    */   {
/* 25 */     super(NAME, "SneckoOil", AbstractPotion.PotionRarity.RARE, AbstractPotion.PotionSize.SNECKO, AbstractPotion.PotionColor.SNECKO);
/* 26 */     this.potency = getPotency();
/* 27 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 28 */     this.isThrown = false;
/* 29 */     this.tips.add(new PowerTip(this.name, this.description));
/* 30 */     this.tips.add(new PowerTip(
/*    */     
/* 32 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.CONFUSED.NAMES[0]), 
/* 33 */       (String)GameDictionary.keywords.get(GameDictionary.CONFUSED.NAMES[0])));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 38 */     target = AbstractDungeon.player;
/* 39 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new ConfusionPower(target)));
/*    */       
/* 42 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 48 */     return new SneckoOil();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 53 */     return 3;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\SneckoOil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */