/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.vfx.combat.ExplosionSmallEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ExplosivePotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Explosive Potion";
/* 19 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Explosive Potion");
/* 20 */   public static final String NAME = potionStrings.NAME;
/* 21 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public ExplosivePotion()
/*    */   {
/* 27 */     super(NAME, "Explosive Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.H, AbstractPotion.PotionColor.EXPLOSIVE);
/* 28 */     this.potency = getPotency();
/* 29 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 30 */     this.isThrown = true;
/* 31 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 36 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 37 */       if (!m.isDeadOrEscaped()) {
/* 38 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(new ExplosionSmallEffect(m.hb.cX, m.hb.cY), 0.1F));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 43 */     AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*    */     
/* 45 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */     
/*    */ 
/* 48 */       com.megacrit.cardcrawl.cards.DamageInfo.createDamageMatrix(this.potency, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 55 */     return new ExplosivePotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 60 */     return 10;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\ExplosivePotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */