/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class RegenPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Regen Potion";
/* 14 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("Regen Potion");
/* 15 */   public static final String NAME = potionStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */   public RegenPotion() {
/* 19 */     super(NAME, "Regen Potion", AbstractPotion.PotionRarity.UNCOMMON, AbstractPotion.PotionSize.HEART, AbstractPotion.PotionColor.WHITE);
/* 20 */     this.potency = getPotency();
/* 21 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 22 */     this.isThrown = false;
/* 23 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 28 */     target = AbstractDungeon.player;
/*    */     
/* 30 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 31 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.RegenPower(target, this.potency), this.potency));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 38 */     return new RegenPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 43 */     return 5;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\RegenPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */