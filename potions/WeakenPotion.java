/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class WeakenPotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Weak Potion";
/* 15 */   private static final PotionStrings potionStrings = CardCrawlGame.languagePack.getPotionString("Weak Potion");
/* 16 */   public static final String NAME = potionStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public WeakenPotion()
/*    */   {
/* 23 */     super(NAME, "Weak Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.H, AbstractPotion.PotionColor.WEAK);
/* 24 */     this.potency = getPotency();
/* 25 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 26 */     this.isThrown = true;
/* 27 */     this.targetRequired = true;
/* 28 */     this.tips.add(new PowerTip(this.name, this.description));
/* 29 */     this.tips.add(new PowerTip(
/*    */     
/* 31 */       com.megacrit.cardcrawl.helpers.TipHelper.capitalize(GameDictionary.WEAK.NAMES[0]), 
/* 32 */       (String)GameDictionary.keywords.get(GameDictionary.WEAK.NAMES[0])));
/*    */   }
/*    */   
/*    */ 
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 38 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.WeakPower(target, this.potency, false), this.potency));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 44 */     return new WeakenPotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 49 */     return 3;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\WeakenPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */