/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*    */ import com.megacrit.cardcrawl.vfx.combat.SmokeBombEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SmokeBomb extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "SmokeBomb";
/* 16 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("SmokeBomb");
/* 17 */   public static final String NAME = potionStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public SmokeBomb()
/*    */   {
/* 24 */     super(NAME, "SmokeBomb", AbstractPotion.PotionRarity.RARE, AbstractPotion.PotionSize.SPHERE, AbstractPotion.PotionColor.SMOKE);
/* 25 */     this.description = DESCRIPTIONS[0];
/* 26 */     this.isThrown = true;
/* 27 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 32 */     target = AbstractDungeon.player;
/* 33 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 34 */       AbstractDungeon.getCurrRoom().smoked = true;
/* 35 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new SmokeBombEffect(target.hb.cX, target.hb.cY)));
/* 36 */       AbstractDungeon.player.hideHealthBar();
/* 37 */       AbstractDungeon.player.isEscaping = true;
/* 38 */       AbstractDungeon.overlayMenu.endTurnButton.disable();
/* 39 */       AbstractDungeon.player.escapeTimer = 2.5F;
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean canUse()
/*    */   {
/* 45 */     if (super.canUse()) {
/* 46 */       boolean fightingBoss = false;
/* 47 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 48 */         if (m.type == com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS) {
/* 49 */           fightingBoss = true;
/* 50 */           break;
/*    */         }
/*    */       }
/* 53 */       if (!fightingBoss) {
/* 54 */         return true;
/*    */       }
/*    */     }
/* 57 */     return false;
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 62 */     return new SmokeBomb();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 67 */     return 0;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\SmokeBomb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */