/*     */ package com.megacrit.cardcrawl.potions;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.shrines.WeMeetAgain;
/*     */ import com.megacrit.cardcrawl.helpers.GameDataStringBuilder;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.vfx.FlashPotionEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public abstract class AbstractPotion
/*     */ {
/*  30 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractPotion");
/*  31 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   public String ID;
/*     */   public String name;
/*  34 */   public String description; public int slot = -1;
/*  35 */   public ArrayList<PowerTip> tips = new ArrayList();
/*     */   private Texture containerImg;
/*     */   private Texture liquidImg;
/*     */   private Texture hybridImg;
/*     */   private Texture spotsImg;
/*     */   private Texture outlineImg;
/*     */   public float posX;
/*     */   public float posY;
/*  43 */   private static final int RAW_W = 64; private ArrayList<FlashPotionEffect> effect = new ArrayList();
/*  44 */   public float scale = Settings.scale;
/*  45 */   public boolean isObtained = false;
/*     */   
/*     */   private static final int FLASH_COUNT = 1;
/*     */   private static final float FLASH_INTERVAL = 0.33F;
/*  49 */   private int flashCount = 0;
/*  50 */   private float flashTimer = 0.0F;
/*     */   public final PotionColor color;
/*     */   public Color liquidColor;
/*  53 */   public Color hybridColor = null; public Color spotsColor = null;
/*     */   public PotionRarity rarity;
/*     */   public PotionSize size;
/*  56 */   protected int potency = 0;
/*  57 */   public Hitbox hb = new Hitbox(64.0F * Settings.scale, 64.0F * Settings.scale);
/*  58 */   private float angle = 0.0F;
/*  59 */   protected boolean canUse = false;
/*  60 */   public boolean discarded = false; public boolean isThrown = false; public boolean targetRequired = false;
/*     */   
/*  62 */   private static final Color PLACEHOLDER_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.75F);
/*     */   
/*     */   public static enum PotionSize {
/*  65 */     T,  S,  M,  SPHERE,  H,  BOTTLE,  HEART,  SNECKO,  FAIRY,  GHOST,  JAR,  BOLT,  CARD;
/*     */     
/*     */     private PotionSize() {} }
/*     */   
/*  69 */   public static enum PotionRarity { PLACEHOLDER,  COMMON,  UNCOMMON,  RARE;
/*     */     
/*     */     private PotionRarity() {} }
/*     */   
/*  73 */   public static enum PotionColor { POISON,  BLUE,  FIRE,  GREEN,  EXPLOSIVE,  WEAK,  FEAR,  STRENGTH,  WHITE,  FAIRY,  ANCIENT,  ELIXIR,  NONE,  ENERGY,  SWIFT,  FRUIT,  SNECKO,  SMOKE,  STEROID,  SKILL,  ATTACK,  POWER;
/*     */     
/*     */     private PotionColor() {} }
/*     */   
/*  77 */   public AbstractPotion(String name, String id, PotionRarity rarity, PotionSize size, PotionColor color) { this.size = size;
/*  78 */     this.ID = id;
/*  79 */     this.name = name;
/*  80 */     this.rarity = rarity;
/*     */     
/*  82 */     switch (size) {
/*     */     case T: 
/*  84 */       this.containerImg = ImageMaster.POTION_T_CONTAINER;
/*  85 */       this.liquidImg = ImageMaster.POTION_T_LIQUID;
/*  86 */       this.hybridImg = ImageMaster.POTION_T_HYBRID;
/*  87 */       this.spotsImg = ImageMaster.POTION_T_SPOTS;
/*  88 */       this.outlineImg = ImageMaster.POTION_T_OUTLINE;
/*  89 */       break;
/*     */     case S: 
/*  91 */       this.containerImg = ImageMaster.POTION_S_CONTAINER;
/*  92 */       this.liquidImg = ImageMaster.POTION_S_LIQUID;
/*  93 */       this.hybridImg = ImageMaster.POTION_S_HYBRID;
/*  94 */       this.spotsImg = ImageMaster.POTION_S_SPOTS;
/*  95 */       this.outlineImg = ImageMaster.POTION_S_OUTLINE;
/*  96 */       break;
/*     */     case M: 
/*  98 */       this.containerImg = ImageMaster.POTION_M_CONTAINER;
/*  99 */       this.liquidImg = ImageMaster.POTION_M_LIQUID;
/* 100 */       this.hybridImg = ImageMaster.POTION_M_HYBRID;
/* 101 */       this.spotsImg = ImageMaster.POTION_M_SPOTS;
/* 102 */       this.outlineImg = ImageMaster.POTION_M_OUTLINE;
/* 103 */       break;
/*     */     case SPHERE: 
/* 105 */       this.containerImg = ImageMaster.POTION_SPHERE_CONTAINER;
/* 106 */       this.liquidImg = ImageMaster.POTION_SPHERE_LIQUID;
/* 107 */       this.hybridImg = ImageMaster.POTION_SPHERE_HYBRID;
/* 108 */       this.spotsImg = ImageMaster.POTION_SPHERE_SPOTS;
/* 109 */       this.outlineImg = ImageMaster.POTION_SPHERE_OUTLINE;
/* 110 */       break;
/*     */     case H: 
/* 112 */       this.containerImg = ImageMaster.POTION_H_CONTAINER;
/* 113 */       this.liquidImg = ImageMaster.POTION_H_LIQUID;
/* 114 */       this.hybridImg = ImageMaster.POTION_H_HYBRID;
/* 115 */       this.spotsImg = ImageMaster.POTION_H_SPOTS;
/* 116 */       this.outlineImg = ImageMaster.POTION_H_OUTLINE;
/* 117 */       break;
/*     */     case BOTTLE: 
/* 119 */       this.containerImg = ImageMaster.POTION_BOTTLE_CONTAINER;
/* 120 */       this.liquidImg = ImageMaster.POTION_BOTTLE_LIQUID;
/* 121 */       this.hybridImg = ImageMaster.POTION_BOTTLE_HYBRID;
/* 122 */       this.spotsImg = ImageMaster.POTION_BOTTLE_SPOTS;
/* 123 */       this.outlineImg = ImageMaster.POTION_BOTTLE_OUTLINE;
/* 124 */       break;
/*     */     case HEART: 
/* 126 */       this.containerImg = ImageMaster.POTION_HEART_CONTAINER;
/* 127 */       this.liquidImg = ImageMaster.POTION_HEART_LIQUID;
/* 128 */       this.hybridImg = ImageMaster.POTION_HEART_HYBRID;
/* 129 */       this.spotsImg = ImageMaster.POTION_HEART_SPOTS;
/* 130 */       this.outlineImg = ImageMaster.POTION_HEART_OUTLINE;
/* 131 */       break;
/*     */     case SNECKO: 
/* 133 */       this.containerImg = ImageMaster.POTION_SNECKO_CONTAINER;
/* 134 */       this.liquidImg = ImageMaster.POTION_SNECKO_LIQUID;
/* 135 */       this.hybridImg = ImageMaster.POTION_SNECKO_HYBRID;
/* 136 */       this.spotsImg = ImageMaster.POTION_SNECKO_SPOTS;
/* 137 */       this.outlineImg = ImageMaster.POTION_SNECKO_OUTLINE;
/* 138 */       break;
/*     */     case FAIRY: 
/* 140 */       this.containerImg = ImageMaster.POTION_FAIRY_CONTAINER;
/* 141 */       this.liquidImg = ImageMaster.POTION_FAIRY_LIQUID;
/* 142 */       this.hybridImg = ImageMaster.POTION_FAIRY_HYBRID;
/* 143 */       this.spotsImg = ImageMaster.POTION_FAIRY_SPOTS;
/* 144 */       this.outlineImg = ImageMaster.POTION_FAIRY_OUTLINE;
/* 145 */       break;
/*     */     case GHOST: 
/* 147 */       this.containerImg = ImageMaster.POTION_GHOST_CONTAINER;
/* 148 */       this.liquidImg = ImageMaster.POTION_GHOST_LIQUID;
/* 149 */       this.hybridImg = ImageMaster.POTION_GHOST_HYBRID;
/* 150 */       this.spotsImg = ImageMaster.POTION_GHOST_SPOTS;
/* 151 */       this.outlineImg = ImageMaster.POTION_GHOST_OUTLINE;
/* 152 */       break;
/*     */     case JAR: 
/* 154 */       this.containerImg = ImageMaster.POTION_JAR_CONTAINER;
/* 155 */       this.liquidImg = ImageMaster.POTION_JAR_LIQUID;
/* 156 */       this.hybridImg = ImageMaster.POTION_JAR_HYBRID;
/* 157 */       this.spotsImg = ImageMaster.POTION_JAR_SPOTS;
/* 158 */       this.outlineImg = ImageMaster.POTION_JAR_OUTLINE;
/* 159 */       break;
/*     */     case BOLT: 
/* 161 */       this.containerImg = ImageMaster.POTION_BOLT_CONTAINER;
/* 162 */       this.liquidImg = ImageMaster.POTION_BOLT_LIQUID;
/* 163 */       this.hybridImg = ImageMaster.POTION_BOLT_HYBRID;
/* 164 */       this.spotsImg = ImageMaster.POTION_BOLT_SPOTS;
/* 165 */       this.outlineImg = ImageMaster.POTION_BOLT_OUTLINE;
/* 166 */       break;
/*     */     case CARD: 
/* 168 */       this.containerImg = ImageMaster.POTION_CARD_CONTAINER;
/* 169 */       this.liquidImg = ImageMaster.POTION_CARD_LIQUID;
/* 170 */       this.hybridImg = ImageMaster.POTION_CARD_HYBRID;
/* 171 */       this.spotsImg = ImageMaster.POTION_CARD_SPOTS;
/* 172 */       this.outlineImg = ImageMaster.POTION_CARD_OUTLINE;
/* 173 */       break;
/*     */     default: 
/* 175 */       this.containerImg = null;
/* 176 */       this.liquidImg = null;
/* 177 */       this.hybridImg = null;
/* 178 */       this.spotsImg = null;
/*     */     }
/*     */     
/*     */     
/* 182 */     this.color = color;
/* 183 */     initializeColor();
/*     */   }
/*     */   
/*     */   public void flash() {
/* 187 */     this.flashCount = 1;
/*     */   }
/*     */   
/*     */   private void initializeColor() {
/* 191 */     switch (this.color) {
/*     */     case BLUE: 
/* 193 */       this.liquidColor = Color.SKY.cpy();
/* 194 */       break;
/*     */     case WHITE: 
/* 196 */       this.liquidColor = Color.WHITE.cpy();
/* 197 */       this.hybridColor = Color.LIGHT_GRAY.cpy();
/* 198 */       break;
/*     */     case FAIRY: 
/* 200 */       this.liquidColor = Color.CLEAR.cpy();
/* 201 */       this.spotsColor = Color.WHITE.cpy();
/* 202 */       break;
/*     */     case ENERGY: 
/* 204 */       this.liquidColor = Color.GOLD.cpy();
/* 205 */       break;
/*     */     case EXPLOSIVE: 
/* 207 */       this.liquidColor = Color.ORANGE.cpy();
/* 208 */       break;
/*     */     case FIRE: 
/* 210 */       this.liquidColor = Color.RED.cpy();
/* 211 */       this.hybridColor = Color.ORANGE.cpy();
/* 212 */       break;
/*     */     case GREEN: 
/* 214 */       this.liquidColor = Color.CHARTREUSE.cpy();
/* 215 */       break;
/*     */     case POISON: 
/* 217 */       this.liquidColor = Color.LIME.cpy();
/* 218 */       this.spotsColor = Color.FOREST.cpy();
/* 219 */       break;
/*     */     case STRENGTH: 
/* 221 */       this.liquidColor = Color.DARK_GRAY.cpy();
/* 222 */       this.spotsColor = Color.CORAL.cpy();
/* 223 */       break;
/*     */     case STEROID: 
/* 225 */       this.liquidColor = Color.DARK_GRAY.cpy();
/* 226 */       this.hybridColor = Color.CORAL.cpy();
/* 227 */       break;
/*     */     case SWIFT: 
/* 229 */       this.liquidColor = Color.valueOf("0d429dff");
/* 230 */       this.spotsColor = Color.CYAN.cpy();
/* 231 */       break;
/*     */     case WEAK: 
/* 233 */       this.liquidColor = Color.VIOLET.cpy();
/* 234 */       this.hybridColor = Color.MAROON.cpy();
/* 235 */       break;
/*     */     case FEAR: 
/* 237 */       this.liquidColor = Color.BLACK.cpy();
/* 238 */       this.hybridColor = Color.SCARLET.cpy();
/* 239 */       break;
/*     */     case ELIXIR: 
/* 241 */       this.liquidColor = Color.GOLD.cpy();
/* 242 */       this.spotsColor = Color.DARK_GRAY.cpy();
/* 243 */       break;
/*     */     case ANCIENT: 
/* 245 */       this.liquidColor = Color.GOLD.cpy();
/* 246 */       this.hybridColor = Color.CYAN.cpy();
/* 247 */       break;
/*     */     case FRUIT: 
/* 249 */       this.liquidColor = Color.ORANGE.cpy();
/* 250 */       this.hybridColor = Color.LIME.cpy();
/* 251 */       break;
/*     */     case SNECKO: 
/* 253 */       this.liquidColor = Settings.GREEN_TEXT_COLOR.cpy();
/* 254 */       this.hybridColor = Settings.GOLD_COLOR.cpy();
/* 255 */       break;
/*     */     case SMOKE: 
/* 257 */       this.liquidColor = Color.GRAY.cpy();
/* 258 */       this.hybridColor = Color.DARK_GRAY.cpy();
/* 259 */       break;
/*     */     case ATTACK: 
/* 261 */       this.liquidColor = Settings.RED_TEXT_COLOR.cpy();
/* 262 */       this.hybridColor = Color.FIREBRICK.cpy();
/* 263 */       break;
/*     */     case SKILL: 
/* 265 */       this.liquidColor = Color.FOREST.cpy();
/* 266 */       this.hybridColor = Color.CHARTREUSE.cpy();
/* 267 */       break;
/*     */     case POWER: 
/* 269 */       this.liquidColor = Color.NAVY.cpy();
/* 270 */       this.hybridColor = Color.SKY.cpy();
/* 271 */       break;
/*     */     default: 
/* 273 */       this.liquidColor = Color.RED.cpy();
/* 274 */       this.spotsColor = Color.RED.cpy();
/*     */     }
/*     */   }
/*     */   
/*     */   public void move(float setX, float setY)
/*     */   {
/* 280 */     this.posX = setX;
/* 281 */     this.posY = setY;
/*     */   }
/*     */   
/*     */   public void adjustPosition(int slot) {
/* 285 */     this.posX = (com.megacrit.cardcrawl.ui.panels.TopPanel.potionX + slot * Settings.POTION_W);
/* 286 */     this.posY = Settings.POTION_Y;
/* 287 */     this.hb.move(this.posX, this.posY);
/*     */   }
/*     */   
/*     */   public int getPrice() {
/* 291 */     switch (this.rarity) {
/*     */     case COMMON: 
/* 293 */       return 50;
/*     */     case UNCOMMON: 
/* 295 */       return 75;
/*     */     case RARE: 
/* 297 */       return 100;
/*     */     }
/* 299 */     return 999;
/*     */   }
/*     */   
/*     */ 
/*     */   public abstract void use(AbstractCreature paramAbstractCreature);
/*     */   
/*     */   public boolean canDiscard()
/*     */   {
/* 307 */     if ((AbstractDungeon.getCurrRoom().event != null) && 
/* 308 */       ((AbstractDungeon.getCurrRoom().event instanceof WeMeetAgain))) {
/* 309 */       return false;
/*     */     }
/*     */     
/* 312 */     return true;
/*     */   }
/*     */   
/*     */   public boolean canUse() {
/* 316 */     if ((AbstractDungeon.getCurrRoom().event != null) && 
/* 317 */       ((AbstractDungeon.getCurrRoom().event instanceof WeMeetAgain))) {
/* 318 */       return false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 323 */     if ((AbstractDungeon.getCurrRoom().monsters == null) || 
/* 324 */       (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) || (AbstractDungeon.actionManager.turnHasEnded) || 
/* 325 */       (AbstractDungeon.getCurrRoom().phase != AbstractRoom.RoomPhase.COMBAT)) {
/* 326 */       return false;
/*     */     }
/* 328 */     return true;
/*     */   }
/*     */   
/*     */   public void update() {
/* 332 */     if (this.isObtained) {
/* 333 */       this.hb.update();
/* 334 */       updateFlash();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFlash() {
/* 339 */     if (this.flashCount != 0) {
/* 340 */       this.flashTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 341 */       if (this.flashTimer < 0.0F) {
/* 342 */         this.flashTimer = 0.33F;
/* 343 */         this.flashCount -= 1;
/* 344 */         this.effect.add(new FlashPotionEffect(this));
/*     */       }
/*     */     }
/*     */     
/* 348 */     for (Iterator<FlashPotionEffect> i = this.effect.iterator(); i.hasNext();) {
/* 349 */       FlashPotionEffect e = (FlashPotionEffect)i.next();
/* 350 */       e.update();
/* 351 */       if (e.isDone) {
/* 352 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void setAsObtained(int potionSlot) {
/* 358 */     this.slot = potionSlot;
/* 359 */     this.isObtained = true;
/* 360 */     adjustPosition(potionSlot);
/*     */   }
/*     */   
/*     */   public static void playPotionSound() {
/* 364 */     int tmp = MathUtils.random(2);
/* 365 */     if (tmp == 0) {
/* 366 */       CardCrawlGame.sound.play("POTION_1");
/* 367 */     } else if (tmp == 1) {
/* 368 */       CardCrawlGame.sound.play("POTION_2");
/*     */     } else {
/* 370 */       CardCrawlGame.sound.play("POTION_3");
/*     */     }
/*     */   }
/*     */   
/*     */   public void renderLightOutline(SpriteBatch sb) {
/* 375 */     if (!(this instanceof PotionSlot)) {
/* 376 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/* 377 */       sb.draw(this.outlineImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderOutline(SpriteBatch sb)
/*     */   {
/* 398 */     if (!(this instanceof PotionSlot)) {
/* 399 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
/* 400 */       sb.draw(this.outlineImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderShiny(SpriteBatch sb)
/*     */   {
/* 421 */     if (!(this instanceof PotionSlot)) {
/* 422 */       sb.setBlendFunction(770, 1);
/* 423 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.1F));
/* 424 */       sb.draw(this.containerImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 441 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 451 */     updateFlash();
/*     */     
/*     */ 
/* 454 */     if ((this instanceof PotionSlot)) {
/* 455 */       sb.setColor(PLACEHOLDER_COLOR);
/* 456 */       sb.draw(ImageMaster.POTION_PLACEHOLDER, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 474 */       sb.setColor(this.liquidColor);
/* 475 */       sb.draw(this.liquidImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 493 */       if (this.hybridColor != null) {
/* 494 */         sb.setColor(this.hybridColor);
/* 495 */         sb.draw(this.hybridImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 514 */       if (this.spotsColor != null) {
/* 515 */         sb.setColor(this.spotsColor);
/* 516 */         sb.draw(this.spotsImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 535 */       sb.setColor(Color.WHITE);
/*     */       
/*     */ 
/*     */ 
/* 539 */       sb.draw(this.containerImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 558 */     for (FlashPotionEffect e : this.effect) {
/* 559 */       e.render(sb, this.posX, this.posY);
/*     */     }
/*     */     
/* 562 */     if (this.hb != null) {
/* 563 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void shopRender(SpriteBatch sb) {
/* 568 */     updateFlash();
/* 569 */     if (this.hb.hovered) {
/* 570 */       com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY, this.name, this.description);
/* 571 */       this.scale = (1.5F * Settings.scale);
/*     */     } else {
/* 573 */       this.scale = MathHelper.scaleLerpSnap(this.scale, 1.2F * Settings.scale);
/*     */     }
/*     */     
/* 576 */     renderOutline(sb);
/*     */     
/* 578 */     sb.setColor(this.liquidColor);
/* 579 */     sb.draw(this.liquidImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 597 */     if (this.hybridColor != null) {
/* 598 */       sb.setColor(this.hybridColor);
/* 599 */       sb.draw(this.hybridImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 618 */     if (this.spotsColor != null) {
/* 619 */       sb.setColor(this.spotsColor);
/* 620 */       sb.draw(this.spotsImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 639 */     sb.setColor(Color.WHITE);
/* 640 */     sb.draw(this.containerImg, this.posX - 32.0F, this.posY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.angle, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 658 */     for (FlashPotionEffect e : this.effect) {
/* 659 */       e.render(sb, this.posX, this.posY);
/*     */     }
/*     */     
/* 662 */     if (this.hb != null) {
/* 663 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public abstract int getPotency(int paramInt);
/*     */   
/*     */   public int getPotency() {
/* 670 */     return getPotency(AbstractDungeon.ascensionLevel);
/*     */   }
/*     */   
/*     */   public abstract AbstractPotion makeCopy();
/*     */   
/*     */   public static String gameDataUploadHeader() {
/* 676 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 677 */     sb.addFieldData("name");
/* 678 */     sb.addFieldData("rarity");
/* 679 */     sb.addFieldData("text");
/* 680 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public String getUploadData() {
/* 684 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 685 */     sb.addFieldData(this.name);
/* 686 */     sb.addFieldData(this.rarity.toString());
/*     */     
/* 688 */     String originalValue = String.valueOf(getPotency(0));
/* 689 */     String comboDesc = this.description;
/* 690 */     if (getPotency(0) != getPotency(15)) {
/* 691 */       comboDesc = this.description.replace(originalValue, String.format("%s(%s)", new Object[] { originalValue, Integer.valueOf(getPotency(15)) }));
/*     */     }
/*     */     
/* 694 */     sb.addFieldData(comboDesc);
/* 695 */     return sb.toString();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\AbstractPotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */