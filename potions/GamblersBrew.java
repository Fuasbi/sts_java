/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ 
/*    */ public class GamblersBrew extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "GamblersBrew";
/* 12 */   private static final PotionStrings potionStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPotionString("GamblersBrew");
/* 13 */   public static final String NAME = potionStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public GamblersBrew()
/*    */   {
/* 20 */     super(NAME, "GamblersBrew", AbstractPotion.PotionRarity.UNCOMMON, AbstractPotion.PotionSize.S, AbstractPotion.PotionColor.SMOKE);
/* 21 */     this.description = DESCRIPTIONS[0];
/* 22 */     this.isThrown = false;
/* 23 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 28 */     if (!AbstractDungeon.player.hand.isEmpty()) {
/* 29 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.GamblingChipAction(AbstractDungeon.player, true));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 35 */     return new GamblersBrew();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 40 */     return 0;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\GamblersBrew.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */