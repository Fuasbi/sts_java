/*    */ package com.megacrit.cardcrawl.potions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.PotionStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class FirePotion extends AbstractPotion
/*    */ {
/*    */   public static final String POTION_ID = "Fire Potion";
/* 15 */   private static final PotionStrings potionStrings = CardCrawlGame.languagePack.getPotionString("Fire Potion");
/* 16 */   public static final String NAME = potionStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = potionStrings.DESCRIPTIONS;
/*    */   
/*    */ 
/*    */ 
/*    */   public FirePotion()
/*    */   {
/* 23 */     super(NAME, "Fire Potion", AbstractPotion.PotionRarity.COMMON, AbstractPotion.PotionSize.SPHERE, AbstractPotion.PotionColor.FIRE);
/* 24 */     this.potency = getPotency();
/* 25 */     this.description = (DESCRIPTIONS[0] + this.potency + DESCRIPTIONS[1]);
/* 26 */     this.isThrown = true;
/* 27 */     this.targetRequired = true;
/* 28 */     this.tips.add(new PowerTip(this.name, this.description));
/*    */   }
/*    */   
/*    */   public void use(AbstractCreature target)
/*    */   {
/* 33 */     DamageInfo info = new DamageInfo(AbstractDungeon.player, this.potency, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS);
/* 34 */     info.applyEnemyPowersOnly(target);
/* 35 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(target, info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */   }
/*    */   
/*    */   public AbstractPotion makeCopy()
/*    */   {
/* 40 */     return new FirePotion();
/*    */   }
/*    */   
/*    */   public int getPotency(int ascensionLevel)
/*    */   {
/* 45 */     return 20;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\potions\FirePotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */