package com.megacrit.cardcrawl.monsters;

import com.megacrit.cardcrawl.localization.*;
import com.badlogic.gdx.graphics.*;
import com.megacrit.cardcrawl.dungeons.*;
import java.util.*;
import com.badlogic.gdx.*;
import com.megacrit.cardcrawl.vfx.*;
import com.megacrit.cardcrawl.characters.*;
import com.megacrit.cardcrawl.relics.*;
import com.megacrit.cardcrawl.core.*;
import com.megacrit.cardcrawl.vfx.combat.*;
import com.megacrit.cardcrawl.rooms.*;
import com.megacrit.cardcrawl.helpers.*;
import com.megacrit.cardcrawl.cards.*;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.megacrit.cardcrawl.vfx.cardManip.*;
import com.megacrit.cardcrawl.screens.stats.*;
import com.megacrit.cardcrawl.daily.*;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.blights.*;
import com.megacrit.cardcrawl.powers.*;
import com.megacrit.cardcrawl.actions.*;
import com.megacrit.cardcrawl.unlock.*;
import java.io.*;
import org.apache.logging.log4j.*;

public abstract class AbstractMonster extends AbstractCreature
{
    private static final Logger logger;
    private static final UIStrings uiStrings;
    public static final String[] TEXT;
    private static final float DEATH_TIME = 1.8f;
    private static final float ESCAPE_TIME = 3.0f;
    protected static final byte ESCAPE = 99;
    protected static final byte ROLL = 98;
    public float deathTimer;
    public Color bgColor;
    protected Texture img;
    public boolean tintFadeOutCalled;
    protected HashMap<Byte, String> moveSet;
    public boolean escaped;
    public boolean escapeNext;
    private PowerTip intentTip;
    public EnemyType type;
    private float hoverTimer;
    public boolean cannotEscape;
    public ArrayList<DamageInfo> damage;
    private EnemyMoveInfo move;
    private float intentParticleTimer;
    private float intentAngle;
    public ArrayList<Byte> moveHistory;
    public ArrayList<AbstractGameEffect> intentFlash;
    private ArrayList<AbstractGameEffect> intentVfx;
    public byte nextMove;
    private static final int INTENT_W = 128;
    private BobEffect bobEffect;
    private static final float INTENT_HB_W;
    public Hitbox intentHb;
    public Intent intent;
    public Intent tipIntent;
    public float intentAlpha;
    public float intentAlphaTarget;
    public float intentOffsetX;
    private Texture intentImg;
    private Texture intentBg;
    private int intentDmg;
    private int intentBaseDmg;
    private int intentMultiAmt;
    private boolean isMultiDmg;
    private Color intentColor;
    public String moveName;
    public static String[] MOVES;
    public static String[] DIALOG;
    public static Comparator<AbstractMonster> sortByHitbox;
    
    public AbstractMonster(final String name, final String id, final int maxHealth, final float hb_x, final float hb_y, final float hb_w, final float hb_h, final String imgUrl, final float offsetX, final float offsetY) {
        this(name, id, maxHealth, hb_x, hb_y, hb_w, hb_h, imgUrl, offsetX, offsetY, false);
    }
    
    public AbstractMonster(final String name, final String id, final int maxHealth, final float hb_x, final float hb_y, final float hb_w, final float hb_h, final String imgUrl, final float offsetX, final float offsetY, final boolean ignoreBlights) {
        this.deathTimer = 0.0f;
        this.bgColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.tintFadeOutCalled = false;
        this.moveSet = new HashMap<Byte, String>();
        this.escaped = false;
        this.escapeNext = false;
        this.intentTip = new PowerTip();
        this.type = EnemyType.NORMAL;
        this.hoverTimer = 0.0f;
        this.cannotEscape = false;
        this.damage = new ArrayList<DamageInfo>();
        this.intentParticleTimer = 0.0f;
        this.intentAngle = 0.0f;
        this.moveHistory = new ArrayList<Byte>();
        this.intentFlash = new ArrayList<AbstractGameEffect>();
        this.intentVfx = new ArrayList<AbstractGameEffect>();
        this.nextMove = -1;
        this.bobEffect = new BobEffect();
        this.intent = Intent.DEBUG;
        this.tipIntent = Intent.DEBUG;
        this.intentAlpha = 0.0f;
        this.intentAlphaTarget = 0.0f;
        this.intentOffsetX = 0.0f;
        this.intentImg = null;
        this.intentBg = null;
        this.intentDmg = 0;
        this.intentBaseDmg = 0;
        this.intentMultiAmt = 0;
        this.isMultiDmg = false;
        this.intentColor = Color.WHITE.cpy();
        this.moveName = null;
        this.isPlayer = false;
        this.name = name;
        this.id = id;
        this.maxHealth = maxHealth;
        if (!ignoreBlights && Settings.isEndless && AbstractDungeon.player.hasBlight("ToughEnemies")) {
            final float mod = AbstractDungeon.player.getBlight("ToughEnemies").effectFloat();
            this.maxHealth = (int)(maxHealth * mod);
        }
        this.currentHealth = this.maxHealth;
        this.currentBlock = 0;
        this.drawX = Settings.WIDTH * 0.75f + offsetX * Settings.scale;
        this.drawY = AbstractDungeon.floorY + offsetY * Settings.scale;
        this.hb_w = hb_w * Settings.scale;
        this.hb_h = hb_h * Settings.scale;
        this.hb_x = hb_x * Settings.scale;
        this.hb_y = hb_y * Settings.scale;
        if (imgUrl != null) {
            this.img = ImageMaster.loadImage(imgUrl);
        }
        this.intentHb = new Hitbox(AbstractMonster.INTENT_HB_W, AbstractMonster.INTENT_HB_W);
        this.hb = new Hitbox(this.hb_w, this.hb_h);
        this.healthHb = new Hitbox(this.hb_w, 72.0f * Settings.scale);
        this.refreshHitboxLocation();
        this.refreshIntentHbLocation();
    }
    
    public AbstractMonster(final String name, final String id, final int maxHealth, final float hb_x, final float hb_y, final float hb_w, final float hb_h, final String imgUrl) {
        this(name, id, maxHealth, hb_x, hb_y, hb_w, hb_h, imgUrl, 0.0f, 0.0f);
    }
    
    public void refreshIntentHbLocation() {
        this.intentHb.move(this.hb.cX + this.intentOffsetX, this.hb.cY + this.hb_h / 2.0f + AbstractMonster.INTENT_HB_W / 2.0f);
    }
    
    public void update() {
        for (final AbstractPower p : this.powers) {
            p.updateParticles();
        }
        this.updateReticle();
        this.updateHealthBar();
        this.updateAnimations();
        this.updateDeathAnimation();
        this.updateEscapeAnimation();
        this.updateIntent();
        this.tint.update();
    }
    
    public void unhover() {
        this.healthHb.hovered = false;
        this.hb.hovered = false;
        this.intentHb.hovered = false;
    }
    
    private void updateIntent() {
        this.bobEffect.update();
        if (this.intentAlpha != this.intentAlphaTarget && this.intentAlphaTarget == 1.0f) {
            this.intentAlpha += Gdx.graphics.getDeltaTime();
            if (this.intentAlpha > this.intentAlphaTarget) {
                this.intentAlpha = this.intentAlphaTarget;
            }
        }
        else if (this.intentAlphaTarget == 0.0f) {
            this.intentAlpha -= Gdx.graphics.getDeltaTime() / 1.5f;
            if (this.intentAlpha < 0.0f) {
                this.intentAlpha = 0.0f;
            }
        }
        if (!this.isDying && !this.isEscaping) {
            this.updateIntentVFX();
        }
        Iterator<AbstractGameEffect> i = this.intentVfx.iterator();
        while (i.hasNext()) {
            final AbstractGameEffect e = i.next();
            e.update();
            if (e.isDone) {
                i.remove();
            }
        }
        i = this.intentFlash.iterator();
        while (i.hasNext()) {
            final AbstractGameEffect e = i.next();
            e.update();
            if (e.isDone) {
                i.remove();
            }
        }
    }
    
    private void updateIntentVFX() {
        if (this.intentAlpha > 0.0f) {
            if (this.intent == Intent.ATTACK_DEBUFF || this.intent == Intent.DEBUFF || this.intent == Intent.STRONG_DEBUFF || this.intent == Intent.DEFEND_DEBUFF) {
                this.intentParticleTimer -= Gdx.graphics.getDeltaTime();
                if (this.intentParticleTimer < 0.0f) {
                    this.intentParticleTimer = 1.0f;
                    this.intentVfx.add((AbstractGameEffect)new DebuffParticleEffect(this.intentHb.cX, this.intentHb.cY));
                }
            }
            else if (this.intent == Intent.ATTACK_BUFF || this.intent == Intent.BUFF || this.intent == Intent.DEFEND_BUFF) {
                this.intentParticleTimer -= Gdx.graphics.getDeltaTime();
                if (this.intentParticleTimer < 0.0f) {
                    this.intentParticleTimer = 0.1f;
                    this.intentVfx.add((AbstractGameEffect)new BuffParticleEffect(this.intentHb.cX, this.intentHb.cY));
                }
            }
            else if (this.intent == Intent.ATTACK_DEFEND) {
                this.intentParticleTimer -= Gdx.graphics.getDeltaTime();
                if (this.intentParticleTimer < 0.0f) {
                    this.intentParticleTimer = 0.5f;
                    this.intentVfx.add((AbstractGameEffect)new ShieldParticleEffect(this.intentHb.cX, this.intentHb.cY));
                }
            }
            else if (this.intent == Intent.UNKNOWN) {
                this.intentParticleTimer -= Gdx.graphics.getDeltaTime();
                if (this.intentParticleTimer < 0.0f) {
                    this.intentParticleTimer = 0.5f;
                    this.intentVfx.add((AbstractGameEffect)new UnknownParticleEffect(this.intentHb.cX, this.intentHb.cY));
                }
            }
            else if (this.intent == Intent.STUN) {
                this.intentParticleTimer -= Gdx.graphics.getDeltaTime();
                if (this.intentParticleTimer < 0.0f) {
                    this.intentParticleTimer = 0.67f;
                    this.intentVfx.add((AbstractGameEffect)new StunStarEffect(this.intentHb.cX, this.intentHb.cY));
                }
            }
        }
    }
    
    public void renderTip(final SpriteBatch sb) {
        final ArrayList<PowerTip> tips = new ArrayList<PowerTip>();
        if (this.intentAlphaTarget == 1.0f && !AbstractDungeon.player.hasRelic("Runic Dome") && this.intent != Intent.NONE) {
            tips.add(this.intentTip);
        }
        for (final AbstractPower p : this.powers) {
            if (p.region48 != null) {
                tips.add(new PowerTip(p.name, p.description, p.region48));
            }
            else {
                tips.add(new PowerTip(p.name, p.description, p.img));
            }
        }
        if (!tips.isEmpty()) {
            final float offsetY = (tips.size() - 1) * AbstractMonster.MULTI_TIP_Y_OFFSET + AbstractMonster.TIP_OFFSET_Y;
            if (this.hb.cX + this.hb.width / 2.0f < AbstractMonster.TIP_X_THRESHOLD) {
                TipHelper.queuePowerTips(this.hb.cX + this.hb.width / 2.0f + AbstractMonster.TIP_OFFSET_R_X, this.hb.cY + offsetY, (ArrayList)tips);
            }
            else {
                TipHelper.queuePowerTips(this.hb.cX - this.hb.width / 2.0f + AbstractMonster.TIP_OFFSET_L_X, this.hb.cY + offsetY, (ArrayList)tips);
            }
        }
    }
    
    private void updateIntentTip() {
        switch (this.intent) {
            case ATTACK: {
                this.intentTip.header = AbstractMonster.TEXT[0];
                if (this.isMultiDmg) {
                    this.intentTip.body = AbstractMonster.TEXT[1] + this.intentDmg + AbstractMonster.TEXT[2] + this.intentMultiAmt + AbstractMonster.TEXT[3];
                }
                else {
                    this.intentTip.body = AbstractMonster.TEXT[4] + this.intentDmg + AbstractMonster.TEXT[5];
                }
                this.intentTip.img = this.getAttackIntentTip();
                break;
            }
            case ATTACK_BUFF: {
                this.intentTip.header = AbstractMonster.TEXT[6];
                if (this.isMultiDmg) {
                    this.intentTip.body = AbstractMonster.TEXT[7] + this.intentDmg + AbstractMonster.TEXT[2] + this.intentMultiAmt + AbstractMonster.TEXT[8];
                }
                else {
                    this.intentTip.body = AbstractMonster.TEXT[9] + this.intentDmg + AbstractMonster.TEXT[5];
                }
                this.intentTip.img = ImageMaster.INTENT_ATTACK_BUFF;
                break;
            }
            case ATTACK_DEBUFF: {
                this.intentTip.header = AbstractMonster.TEXT[10];
                this.intentTip.body = AbstractMonster.TEXT[11] + this.intentDmg + AbstractMonster.TEXT[5];
                this.intentTip.img = ImageMaster.INTENT_ATTACK_DEBUFF;
                break;
            }
            case ATTACK_DEFEND: {
                this.intentTip.header = AbstractMonster.TEXT[0];
                if (this.isMultiDmg) {
                    this.intentTip.body = AbstractMonster.TEXT[12] + this.intentDmg + AbstractMonster.TEXT[2] + this.intentMultiAmt + AbstractMonster.TEXT[3];
                }
                else {
                    this.intentTip.body = AbstractMonster.TEXT[12] + this.intentDmg + AbstractMonster.TEXT[5];
                }
                this.intentTip.img = ImageMaster.INTENT_ATTACK_DEFEND;
                break;
            }
            case BUFF: {
                this.intentTip.header = AbstractMonster.TEXT[10];
                this.intentTip.body = AbstractMonster.TEXT[19];
                this.intentTip.img = ImageMaster.INTENT_BUFF;
                break;
            }
            case DEBUFF: {
                this.intentTip.header = AbstractMonster.TEXT[10];
                this.intentTip.body = AbstractMonster.TEXT[20];
                this.intentTip.img = ImageMaster.INTENT_DEBUFF;
                break;
            }
            case STRONG_DEBUFF: {
                this.intentTip.header = AbstractMonster.TEXT[10];
                this.intentTip.body = AbstractMonster.TEXT[21];
                this.intentTip.img = ImageMaster.INTENT_DEBUFF2;
                break;
            }
            case DEFEND: {
                this.intentTip.header = AbstractMonster.TEXT[13];
                this.intentTip.body = AbstractMonster.TEXT[22];
                this.intentTip.img = ImageMaster.INTENT_DEFEND;
                break;
            }
            case DEFEND_DEBUFF: {
                this.intentTip.header = AbstractMonster.TEXT[13];
                this.intentTip.body = AbstractMonster.TEXT[23];
                this.intentTip.img = ImageMaster.INTENT_DEFEND;
                break;
            }
            case DEFEND_BUFF: {
                this.intentTip.header = AbstractMonster.TEXT[13];
                this.intentTip.body = AbstractMonster.TEXT[24];
                this.intentTip.img = ImageMaster.INTENT_DEFEND_BUFF;
                break;
            }
            case ESCAPE: {
                this.intentTip.header = AbstractMonster.TEXT[14];
                this.intentTip.body = AbstractMonster.TEXT[25];
                this.intentTip.img = ImageMaster.INTENT_ESCAPE;
                break;
            }
            case MAGIC: {
                this.intentTip.header = AbstractMonster.TEXT[15];
                this.intentTip.body = AbstractMonster.TEXT[26];
                this.intentTip.img = ImageMaster.INTENT_MAGIC;
                break;
            }
            case SLEEP: {
                this.intentTip.header = AbstractMonster.TEXT[16];
                this.intentTip.body = AbstractMonster.TEXT[27];
                this.intentTip.img = ImageMaster.INTENT_SLEEP;
                break;
            }
            case STUN: {
                this.intentTip.header = AbstractMonster.TEXT[17];
                this.intentTip.body = AbstractMonster.TEXT[28];
                this.intentTip.img = ImageMaster.INTENT_STUN;
                break;
            }
            case UNKNOWN: {
                this.intentTip.header = AbstractMonster.TEXT[18];
                this.intentTip.body = AbstractMonster.TEXT[29];
                this.intentTip.img = ImageMaster.INTENT_UNKNOWN;
                break;
            }
            case NONE: {
                this.intentTip.header = "";
                this.intentTip.body = "";
                this.intentTip.img = ImageMaster.INTENT_UNKNOWN;
                break;
            }
            default: {
                this.intentTip.header = "NOT SET";
                this.intentTip.body = "NOT SET";
                this.intentTip.img = ImageMaster.INTENT_UNKNOWN;
                break;
            }
        }
    }
    
    public void heal(int healAmount) {
        if (this.isDying) {
            return;
        }
        for (final AbstractPower p : this.powers) {
            healAmount = p.onHeal(healAmount);
        }
        this.currentHealth += healAmount;
        if (this.currentHealth > this.maxHealth) {
            this.currentHealth = this.maxHealth;
        }
        if (healAmount > 0) {
            AbstractDungeon.effectList.add(new HealEffect(this.hb.cX - this.animX, this.hb.cY, healAmount));
            this.healthBarUpdatedEvent();
        }
    }
    
    public void flashIntent() {
        if (this.intentImg != null) {
            AbstractDungeon.effectList.add(new FlashIntentEffect(this.intentImg, this));
        }
        this.intentAlphaTarget = 0.0f;
    }
    
    public void createIntent() {
        this.intent = this.move.intent;
        this.intentParticleTimer = 0.5f;
        this.nextMove = this.move.nextMove;
        this.intentBaseDmg = this.move.baseDamage;
        if (this.move.baseDamage > -1) {
            this.calculateDamage(this.intentBaseDmg);
            if (this.move.isMultiDamage) {
                this.intentMultiAmt = this.move.multiplier;
                this.isMultiDmg = true;
            }
            else {
                this.intentMultiAmt = -1;
                this.isMultiDmg = false;
            }
        }
        this.intentImg = this.getIntentImg();
        this.intentBg = this.getIntentBg();
        this.tipIntent = this.intent;
        this.intentAlpha = 0.0f;
        this.intentAlphaTarget = 1.0f;
        this.updateIntentTip();
    }
    
    public void setMove(final String moveName, final byte nextMove, final Intent intent, final int baseDamage, final int multiplier, final boolean isMultiDamage) {
        this.moveName = moveName;
        if (nextMove != -1) {
            this.moveHistory.add(nextMove);
        }
        this.move = new EnemyMoveInfo(nextMove, intent, baseDamage, multiplier, isMultiDamage);
    }
    
    public void setMove(final byte nextMove, final Intent intent, final int baseDamage, final int multiplier, final boolean isMultiDamage) {
        this.setMove(null, nextMove, intent, baseDamage, multiplier, isMultiDamage);
    }
    
    public void setMove(final byte nextMove, final Intent intent, final int baseDamage) {
        this.setMove(null, nextMove, intent, baseDamage, 0, false);
    }
    
    public void setMove(final String moveName, final byte nextMove, final Intent intent, final int baseDamage) {
        this.setMove(moveName, nextMove, intent, baseDamage, 0, false);
    }
    
    public void setMove(final String moveName, final byte nextMove, final Intent intent) {
        if (intent == Intent.ATTACK || intent == Intent.ATTACK_BUFF || intent == Intent.ATTACK_DEFEND || intent == Intent.ATTACK_DEBUFF) {
            for (int i = 0; i < 8; ++i) {
                AbstractDungeon.effectsQueue.add(new TextAboveCreatureEffect(MathUtils.random(Settings.WIDTH * 0.25f, Settings.WIDTH * 0.75f), MathUtils.random(Settings.HEIGHT * 0.25f, Settings.HEIGHT * 0.75f), "ENEMY MOVE " + moveName + " IS SET INCORRECTLY! REPORT TO DEV", Color.RED.cpy()));
            }
            AbstractMonster.logger.info("ENEMY MOVE " + moveName + " IS SET INCORRECTLY! REPORT TO DEV");
        }
        this.setMove(moveName, nextMove, intent, -1, 0, false);
    }
    
    public void setMove(final byte nextMove, final Intent intent) {
        this.setMove(null, nextMove, intent, -1, 0, false);
    }
    
    public void rollMove() {
        this.getMove(AbstractDungeon.aiRng.random(99));
    }
    
    protected boolean lastMove(final byte move) {
        return !this.moveHistory.isEmpty() && this.moveHistory.get(this.moveHistory.size() - 1) == move;
    }
    
    protected boolean lastMoveBefore(final byte move) {
        return !this.moveHistory.isEmpty() && this.moveHistory.size() >= 2 && this.moveHistory.get(this.moveHistory.size() - 2) == move;
    }
    
    protected boolean lastTwoMoves(final byte move) {
        return this.moveHistory.size() >= 2 && (this.moveHistory.get(this.moveHistory.size() - 1) == move && this.moveHistory.get(this.moveHistory.size() - 2) == move);
    }
    
    private Texture getIntentImg() {
        switch (this.intent) {
            case ATTACK: {
                return this.getAttackIntent();
            }
            case ATTACK_BUFF: {
                return this.getAttackIntent();
            }
            case ATTACK_DEBUFF: {
                return this.getAttackIntent();
            }
            case ATTACK_DEFEND: {
                return this.getAttackIntent();
            }
            case BUFF: {
                return ImageMaster.INTENT_BUFF_L;
            }
            case DEBUFF: {
                return ImageMaster.INTENT_DEBUFF_L;
            }
            case STRONG_DEBUFF: {
                return ImageMaster.INTENT_DEBUFF2_L;
            }
            case DEFEND: {
                return ImageMaster.INTENT_DEFEND_L;
            }
            case DEFEND_DEBUFF: {
                return ImageMaster.INTENT_DEFEND_L;
            }
            case DEFEND_BUFF: {
                return ImageMaster.INTENT_DEFEND_BUFF_L;
            }
            case ESCAPE: {
                return ImageMaster.INTENT_ESCAPE_L;
            }
            case MAGIC: {
                return ImageMaster.INTENT_MAGIC_L;
            }
            case SLEEP: {
                return ImageMaster.INTENT_SLEEP_L;
            }
            case STUN: {
                return null;
            }
            case UNKNOWN: {
                return ImageMaster.INTENT_UNKNOWN_L;
            }
            default: {
                return ImageMaster.INTENT_UNKNOWN_L;
            }
        }
    }
    
    private Texture getIntentBg() {
        switch (this.intent) {
            case ATTACK_DEFEND: {
                return null;
            }
            default: {
                return null;
            }
        }
    }
    
    protected Texture getAttackIntent(final int dmg) {
        if (dmg < 5) {
            return ImageMaster.INTENT_ATK_1;
        }
        if (dmg < 10) {
            return ImageMaster.INTENT_ATK_2;
        }
        if (dmg < 15) {
            return ImageMaster.INTENT_ATK_3;
        }
        if (dmg < 20) {
            return ImageMaster.INTENT_ATK_4;
        }
        if (dmg < 25) {
            return ImageMaster.INTENT_ATK_5;
        }
        if (dmg < 30) {
            return ImageMaster.INTENT_ATK_6;
        }
        return ImageMaster.INTENT_ATK_7;
    }
    
    protected Texture getAttackIntent() {
        int tmp;
        if (this.isMultiDmg) {
            tmp = this.intentDmg * this.intentMultiAmt;
        }
        else {
            tmp = this.intentDmg;
        }
        if (tmp < 5) {
            return ImageMaster.INTENT_ATK_1;
        }
        if (tmp < 10) {
            return ImageMaster.INTENT_ATK_2;
        }
        if (tmp < 15) {
            return ImageMaster.INTENT_ATK_3;
        }
        if (tmp < 20) {
            return ImageMaster.INTENT_ATK_4;
        }
        if (tmp < 25) {
            return ImageMaster.INTENT_ATK_5;
        }
        if (tmp < 30) {
            return ImageMaster.INTENT_ATK_6;
        }
        return ImageMaster.INTENT_ATK_7;
    }
    
    private Texture getAttackIntentTip() {
        int tmp;
        if (this.isMultiDmg) {
            tmp = this.intentDmg * this.intentMultiAmt;
        }
        else {
            tmp = this.intentDmg;
        }
        if (tmp < 5) {
            return ImageMaster.INTENT_ATK_TIP_1;
        }
        if (tmp < 10) {
            return ImageMaster.INTENT_ATK_TIP_2;
        }
        if (tmp < 15) {
            return ImageMaster.INTENT_ATK_TIP_3;
        }
        if (tmp < 20) {
            return ImageMaster.INTENT_ATK_TIP_4;
        }
        if (tmp < 25) {
            return ImageMaster.INTENT_ATK_TIP_5;
        }
        if (tmp < 30) {
            return ImageMaster.INTENT_ATK_TIP_6;
        }
        return ImageMaster.INTENT_ATK_TIP_7;
    }
    
    public void damage(final DamageInfo info) {
        if (info.output > 0 && this.hasPower("IntangiblePlayer")) {
            info.output = 1;
        }
        int damageAmount = info.output;
        if (this.isDying || this.isEscaping) {
            return;
        }
        if (damageAmount < 0) {
            damageAmount = 0;
        }
        boolean hadBlock = true;
        if (this.currentBlock == 0) {
            hadBlock = false;
        }
        final boolean weakenedToZero = damageAmount == 0;
        damageAmount = this.decrementBlock(info, damageAmount);
        if (info.owner instanceof AbstractPlayer) {
            for (final AbstractRelic r : AbstractDungeon.player.relics) {
                r.onAttack(info, damageAmount, (AbstractCreature)this);
            }
        }
        if (info.owner != null) {
            for (final AbstractPower p : info.owner.powers) {
                p.onAttack(info, damageAmount, (AbstractCreature)this);
            }
        }
        for (final AbstractPower p : this.powers) {
            damageAmount = p.onAttacked(info, damageAmount);
        }
        for (final AbstractRelic r : AbstractDungeon.player.relics) {
            damageAmount = r.onAttackedMonster(info, damageAmount);
        }
        if (damageAmount > 0) {
            if (info.owner != this) {
                this.useStaggerAnimation();
            }
            if (damageAmount >= 99 && !CardCrawlGame.overkill) {
                CardCrawlGame.overkill = true;
            }
            this.currentHealth -= damageAmount;
            AbstractDungeon.effectList.add(new StrikeEffect((AbstractCreature)this, this.hb.cX, this.hb.cY, damageAmount));
            if (this.currentHealth < 0) {
                this.currentHealth = 0;
            }
            this.healthBarUpdatedEvent();
        }
        else if (weakenedToZero && this.currentBlock == 0) {
            if (hadBlock) {
                AbstractDungeon.effectList.add(new BlockedWordEffect((AbstractCreature)this, this.hb.cX, this.hb.cY, AbstractMonster.TEXT[30]));
            }
            else {
                AbstractDungeon.effectList.add(new StrikeEffect((AbstractCreature)this, this.hb.cX, this.hb.cY, 0));
            }
        }
        else if (Settings.SHOW_DMG_BLOCK) {
            AbstractDungeon.effectList.add(new BlockedWordEffect((AbstractCreature)this, this.hb.cX, this.hb.cY, AbstractMonster.TEXT[30]));
        }
        if (this.currentHealth <= 0) {
            this.die();
            if (AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
                AbstractDungeon.actionManager.cleanCardQueue();
                AbstractDungeon.effectList.add(new DeckPoofEffect(64.0f * Settings.scale, 64.0f * Settings.scale, true));
                AbstractDungeon.effectList.add(new DeckPoofEffect(Settings.WIDTH - 64.0f * Settings.scale, 64.0f * Settings.scale, false));
                AbstractDungeon.overlayMenu.hideCombatPanels();
            }
            if (this.currentBlock > 0) {
                this.loseBlock();
                AbstractDungeon.effectList.add(new HbBlockBrokenEffect(this.hb.cX - this.hb.width / 2.0f + AbstractMonster.BLOCK_ICON_X, this.hb.cY - this.hb.height / 2.0f + AbstractMonster.BLOCK_ICON_Y));
            }
        }
    }
    
    public void init() {
        this.rollMove();
        this.healthBarUpdatedEvent();
    }
    
    public abstract void takeTurn();
    
    public void render(final SpriteBatch sb) {
        if (!this.isDead && !this.escaped) {
            if (this.damageFlash) {
                ShaderHelper.setShader(sb, ShaderHelper.Shader.WHITE_SILHOUETTE);
            }
            if (this.atlas == null) {
                sb.setColor(this.tint.color);
                sb.draw(this.img, this.drawX - this.img.getWidth() * Settings.scale / 2.0f + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY, this.img.getWidth() * Settings.scale, this.img.getHeight() * Settings.scale, 0, 0, this.img.getWidth(), this.img.getHeight(), this.flipHorizontal, this.flipVertical);
            }
            else {
                this.state.update(Gdx.graphics.getDeltaTime());
                this.state.apply(this.skeleton);
                this.skeleton.updateWorldTransform();
                this.skeleton.setPosition(this.drawX + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY);
                this.skeleton.setColor(this.tint.color);
                this.skeleton.setFlip(this.flipHorizontal, this.flipVertical);
                sb.end();
                CardCrawlGame.psb.begin();
                AbstractMonster.sr.draw(CardCrawlGame.psb, this.skeleton);
                CardCrawlGame.psb.end();
                sb.begin();
                sb.setBlendFunction(770, 771);
            }
            if (this == AbstractDungeon.getCurrRoom().monsters.hoveredMonster && this.atlas == null) {
                sb.setBlendFunction(770, 1);
                sb.setColor(new Color(1.0f, 1.0f, 1.0f, 0.1f));
                sb.draw(this.img, this.drawX - this.img.getWidth() * Settings.scale / 2.0f + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY, this.img.getWidth() * Settings.scale, this.img.getHeight() * Settings.scale, 0, 0, this.img.getWidth(), this.img.getHeight(), this.flipHorizontal, this.flipVertical);
                sb.setBlendFunction(770, 771);
            }
            if (this.damageFlash) {
                ShaderHelper.setShader(sb, ShaderHelper.Shader.DEFAULT);
                --this.damageFlashFrames;
                if (this.damageFlashFrames == 0) {
                    this.damageFlash = false;
                }
            }
            if (!this.isDying && !this.isEscaping && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT && !AbstractDungeon.player.isDead && !AbstractDungeon.player.hasRelic("Runic Dome") && this.intent != Intent.NONE && !Settings.hideCombatElements) {
                this.renderIntentVfxBehind(sb);
                this.renderIntent(sb);
                this.renderIntentVfxAfter(sb);
                this.renderDamageRange(sb);
            }
            this.hb.render(sb);
            this.intentHb.render(sb);
            this.healthHb.render(sb);
        }
        if (!AbstractDungeon.player.isDead) {
            this.renderHealth(sb);
            this.renderName(sb);
        }
    }
    
    protected void setHp(final int minHp, final int maxHp) {
        this.currentHealth = AbstractDungeon.monsterHpRng.random(minHp, maxHp);
        if (Settings.isEndless && AbstractDungeon.player.hasBlight("ToughEnemies")) {
            final float mod = AbstractDungeon.player.getBlight("ToughEnemies").effectFloat();
            this.currentHealth *= (int)mod;
        }
        this.maxHealth = this.currentHealth;
    }
    
    protected void setHp(final int hp) {
        this.setHp(hp, hp);
    }
    
    private void renderDamageRange(final SpriteBatch sb) {
        if (this.intent.name().contains("ATTACK")) {
            if (this.isMultiDmg) {
                FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont, Integer.toString(this.intentDmg) + "x" + Integer.toString(this.intentMultiAmt), this.intentHb.cX - 30.0f * Settings.scale, this.intentHb.cY + this.bobEffect.y - 12.0f * Settings.scale, this.intentColor);
            }
            else {
                FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont, Integer.toString(this.intentDmg), this.intentHb.cX - 30.0f * Settings.scale, this.intentHb.cY + this.bobEffect.y - 12.0f * Settings.scale, this.intentColor);
            }
        }
    }
    
    private void renderIntentVfxBehind(final SpriteBatch sb) {
        for (final AbstractGameEffect e : this.intentVfx) {
            if (e.renderBehind) {
                e.render(sb);
            }
        }
    }
    
    private void renderIntentVfxAfter(final SpriteBatch sb) {
        for (final AbstractGameEffect e : this.intentVfx) {
            if (!e.renderBehind) {
                e.render(sb);
            }
        }
    }
    
    private void renderName(final SpriteBatch sb) {
        if (!this.hb.hovered) {
            this.hoverTimer = MathHelper.fadeLerpSnap(this.hoverTimer, 0.0f);
        }
        else {
            this.hoverTimer += Gdx.graphics.getDeltaTime();
        }
        if ((!AbstractDungeon.player.isDraggingCard || AbstractDungeon.player.hoveredCard.target == AbstractCard.CardTarget.ENEMY) && !this.isDying) {
            final Color c = new Color();
            if (this.hoverTimer != 0.0f) {
                if (this.hoverTimer * 2.0f > 1.0f) {
                    c.a = 1.0f;
                }
                else {
                    c.a = this.hoverTimer * 2.0f;
                }
            }
            else {
                c.a = MathHelper.slowColorLerpSnap(c.a, 0.0f);
            }
            final float tmp = Interpolation.exp5Out.apply(1.5f, 2.0f, this.hoverTimer);
            c.r = Interpolation.fade.apply(Color.DARK_GRAY.r, Settings.CREAM_COLOR.r, this.hoverTimer * 10.0f);
            c.g = Interpolation.fade.apply(Color.DARK_GRAY.g, Settings.CREAM_COLOR.g, this.hoverTimer * 3.0f);
            c.b = Interpolation.fade.apply(Color.DARK_GRAY.b, Settings.CREAM_COLOR.b, this.hoverTimer * 3.0f);
            final float y = Interpolation.exp10Out.apply(this.healthHb.cY, this.healthHb.cY - 8.0f * Settings.scale, c.a);
            final float x = this.hb.cX - this.animX;
            sb.setColor(new Color(0.0f, 0.0f, 0.0f, c.a / 2.0f * this.hbAlpha));
            final TextureAtlas.AtlasRegion img = ImageMaster.MOVE_NAME_BG;
            sb.draw((TextureRegion)img, x - img.packedWidth / 2.0f, y - img.packedHeight / 2.0f, img.packedWidth / 2.0f, img.packedHeight / 2.0f, (float)img.packedWidth, (float)img.packedHeight, Settings.scale * tmp, Settings.scale * 2.0f, 0.0f);
            FontHelper.renderFontCentered(sb, FontHelper.tipHeaderFont, this.name, x, y, new Color(c.r, c.g, c.b, c.a * this.hbAlpha));
        }
    }
    
    private void renderIntent(final SpriteBatch sb) {
        this.intentColor.a = this.intentAlpha;
        sb.setColor(this.intentColor);
        if (this.intentBg != null) {
            sb.setColor(new Color(1.0f, 1.0f, 1.0f, this.intentAlpha / 2.0f));
            sb.draw(this.intentBg, this.intentHb.cX - 64.0f, this.intentHb.cY - 64.0f + this.bobEffect.y, 64.0f, 64.0f, 128.0f, 128.0f, Settings.scale, Settings.scale, 0.0f, 0, 0, 128, 128, false, false);
        }
        if (this.intentImg != null && this.intent != Intent.UNKNOWN && this.intent != Intent.STUN) {
            if (this.intent == Intent.DEBUFF || this.intent == Intent.STRONG_DEBUFF) {
                this.intentAngle += Gdx.graphics.getDeltaTime() * 150.0f;
            }
            else {
                this.intentAngle = 0.0f;
            }
            sb.setColor(this.intentColor);
            sb.draw(this.intentImg, this.intentHb.cX - 64.0f, this.intentHb.cY - 64.0f + this.bobEffect.y, 64.0f, 64.0f, 128.0f, 128.0f, Settings.scale, Settings.scale, this.intentAngle, 0, 0, 128, 128, false, false);
        }
        for (final AbstractGameEffect e : this.intentFlash) {
            e.render(sb, this.intentHb.cX - 64.0f, this.intentHb.cY - 64.0f);
        }
    }
    
    protected void updateHitbox(final float hb_x, final float hb_y, final float hb_w, final float hb_h) {
        this.hb_w = hb_w * Settings.scale;
        this.hb_h = hb_h * Settings.scale;
        this.hb_y = hb_y * Settings.scale;
        this.hb_x = hb_x * Settings.scale;
        (this.hb = new Hitbox(this.hb_w, this.hb_h)).move(this.drawX + this.hb_x + this.animX, this.drawY + this.hb_y + this.hb_h / 2.0f);
        this.healthHb.move(this.hb.cX, this.hb.cY - this.hb_h / 2.0f - this.healthHb.height / 2.0f);
        this.intentHb.move(this.hb.cX + this.intentOffsetX, this.hb.cY + this.hb_h / 2.0f + 32.0f * Settings.scale);
    }
    
    protected abstract void getMove(final int p0);
    
    private void updateDeathAnimation() {
        if (this.isDying) {
            this.deathTimer -= Gdx.graphics.getDeltaTime();
            if (this.deathTimer < 1.8f && !this.tintFadeOutCalled) {
                this.tintFadeOutCalled = true;
                this.tint.fadeOut();
            }
        }
        if (this.deathTimer < 0.0f) {
            this.isDead = true;
            if (AbstractDungeon.getMonsters().areMonstersDead() && !AbstractDungeon.getCurrRoom().isBattleOver && !AbstractDungeon.getCurrRoom().cannotLose) {
                AbstractDungeon.getCurrRoom().endBattle();
            }
            this.dispose();
            this.powers.clear();
        }
    }
    
    public void dispose() {
        if (this.atlas != null) {
            this.atlas.dispose();
            AbstractMonster.logger.info("Disposing Atlas...");
            this.atlas = null;
        }
    }
    
    private void updateEscapeAnimation() {
        if (this.escapeTimer != 0.0f) {
            this.flipHorizontal = true;
            this.escapeTimer -= Gdx.graphics.getDeltaTime();
            this.drawX += Gdx.graphics.getDeltaTime() * 400.0f * Settings.scale;
        }
        if (this.escapeTimer < 0.0f) {
            this.escaped = true;
            if (AbstractDungeon.getMonsters().areMonstersDead() && !AbstractDungeon.getCurrRoom().isBattleOver && !AbstractDungeon.getCurrRoom().cannotLose) {
                AbstractDungeon.getCurrRoom().endBattle();
            }
        }
    }
    
    public void escapeNext() {
        this.escapeNext = true;
    }
    
    public void deathReact() {
    }
    
    public void escape() {
        this.hideHealthBar();
        this.isEscaping = true;
        this.escapeTimer = 3.0f;
    }
    
    public void die() {
        this.die(true);
    }
    
    public void die(final boolean triggerRelics) {
        if (!this.isDying) {
            this.isDying = true;
            if (this.currentHealth <= 0 && triggerRelics) {
                for (final AbstractPower p : this.powers) {
                    p.onDeath();
                }
            }
            if (triggerRelics) {
                for (final AbstractRelic r : AbstractDungeon.player.relics) {
                    r.onMonsterDeath(this);
                }
            }
            if (AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
                AbstractDungeon.overlayMenu.endTurnButton.disable();
                for (final AbstractCard c : AbstractDungeon.player.limbo.group) {
                    AbstractDungeon.effectList.add(new ExhaustCardEffect(c));
                }
                AbstractDungeon.player.limbo.clear();
            }
            if (this.currentHealth < 0) {
                this.currentHealth = 0;
            }
            if (!Settings.FAST_MODE) {
                this.deathTimer += 1.8f;
            }
            else {
                ++this.deathTimer;
            }
            StatsScreen.incrementEnemySlain();
        }
    }
    
    public void usePreBattleAction() {
    }
    
    public void useUniversalPreBattleAction() {
        if (DailyMods.negativeMods.get("Lethality")) {
            AbstractDungeon.actionManager.addToBottom((AbstractGameAction)new ApplyPowerAction((AbstractCreature)this, (AbstractCreature)this, (AbstractPower)new StrengthPower((AbstractCreature)this, 3), 3));
        }
        for (final AbstractBlight b : AbstractDungeon.player.blights) {
            b.onCreateEnemy(this);
        }
        if (DailyMods.negativeMods.get("Time Dilation") && !this.id.equals("GiantHead")) {
            AbstractDungeon.actionManager.addToBottom((AbstractGameAction)new ApplyPowerAction((AbstractCreature)this, (AbstractCreature)this, (AbstractPower)new SlowPower((AbstractCreature)this, 0)));
        }
    }
    
    private void calculateDamage(int dmg) {
        final AbstractPlayer target = AbstractDungeon.player;
        float tmp = (float)dmg;
        if (Settings.isEndless && AbstractDungeon.player.hasBlight("DeadlyEnemies")) {
            final float mod = AbstractDungeon.player.getBlight("DeadlyEnemies").effectFloat();
            tmp *= mod;
        }
        for (final AbstractPower p : this.powers) {
            tmp = p.atDamageGive(tmp, DamageInfo.DamageType.NORMAL);
        }
        for (final AbstractPower p : target.powers) {
            tmp = p.atDamageReceive(tmp, DamageInfo.DamageType.NORMAL);
        }
        for (final AbstractPower p : this.powers) {
            tmp = p.atDamageFinalGive(tmp, DamageInfo.DamageType.NORMAL);
        }
        for (final AbstractPower p : target.powers) {
            tmp = p.atDamageFinalReceive(tmp, DamageInfo.DamageType.NORMAL);
        }
        dmg = MathUtils.floor(tmp);
        if (dmg < 0) {
            dmg = 0;
        }
        this.intentDmg = dmg;
    }
    
    public void applyPowers() {
        for (final DamageInfo dmg : this.damage) {
            dmg.applyPowers((AbstractCreature)this, (AbstractCreature)AbstractDungeon.player);
        }
        if (this.move.baseDamage > -1) {
            this.calculateDamage(this.move.baseDamage);
        }
        this.intentImg = this.getIntentImg();
        this.updateIntentTip();
    }
    
    public void changeState(final String stateName) {
    }
    
    protected void onBossVictoryLogic() {
        AbstractDungeon.scene.fadeInAmbiance();
        if (AbstractDungeon.getCurrRoom().event == null) {
            ++AbstractDungeon.bossCount;
            StatsScreen.incrementBossSlain();
            if (GameActionManager.turn <= 1) {
                UnlockTracker.unlockAchievement("YOU_ARE_NOTHING");
            }
            if (GameActionManager.damageReceivedThisCombat - GameActionManager.hpLossThisCombat <= 0) {
                UnlockTracker.unlockAchievement("PERFECT");
                ++CardCrawlGame.perfect;
            }
        }
        CardCrawlGame.music.silenceTempBgmInstantly();
        CardCrawlGame.music.silenceBGMInstantly();
        playBossStinger();
        for (final AbstractBlight b : AbstractDungeon.player.blights) {
            b.onBossDefeat();
        }
    }
    
    protected void onFinalBossVictoryLogic() {
        if (!Settings.isEndless) {
            CardCrawlGame.stopClock = true;
            if (CardCrawlGame.playtime <= 1200.0f) {
                UnlockTracker.unlockAchievement("SPEED_CLIMBER");
            }
            if (AbstractDungeon.player.masterDeck.size() <= 5) {
                UnlockTracker.unlockAchievement("MINIMALIST");
            }
            boolean commonSenseUnlocked = true;
            for (final AbstractCard c : AbstractDungeon.player.masterDeck.group) {
                if (c.rarity == AbstractCard.CardRarity.UNCOMMON || c.rarity == AbstractCard.CardRarity.RARE) {
                    commonSenseUnlocked = false;
                    break;
                }
            }
            if (commonSenseUnlocked) {
                UnlockTracker.unlockAchievement("COMMON_SENSE");
            }
            if (AbstractDungeon.player.relics.size() == 1) {
                UnlockTracker.unlockAchievement("ONE_RELIC");
            }
            if (Settings.isDailyRun && !Settings.seedSet) {
                UnlockTracker.unlockLuckyDay();
            }
        }
    }
    
    public static void playBossStinger() {
        CardCrawlGame.sound.play("BOSS_VICTORY_STINGER");
        switch (MathUtils.random(0, 3)) {
            case 0: {
                CardCrawlGame.music.playTempBgmInstantly("STS_BossVictoryStinger_1_v3_MUSIC.ogg", false);
                break;
            }
            case 1: {
                CardCrawlGame.music.playTempBgmInstantly("STS_BossVictoryStinger_2_v3_MUSIC.ogg", false);
                break;
            }
            case 2: {
                CardCrawlGame.music.playTempBgmInstantly("STS_BossVictoryStinger_3_v3_MUSIC.ogg", false);
                break;
            }
            case 3: {
                CardCrawlGame.music.playTempBgmInstantly("STS_BossVictoryStinger_4_v3_MUSIC.ogg", false);
                break;
            }
            default: {
                AbstractMonster.logger.info("[ERROR] Attempted to play boss stinger but failed.");
                break;
            }
        }
    }
    
    public HashMap<String, Serializable> getLocStrings() {
        final HashMap<String, Serializable> data = new HashMap<String, Serializable>();
        data.put("name", this.name);
        data.put("moves", AbstractMonster.MOVES);
        data.put("dialogs", AbstractMonster.DIALOG);
        return data;
    }
    
    static {
        logger = LogManager.getLogger(AbstractMonster.class.getName());
        uiStrings = CardCrawlGame.languagePack.getUIString("AbstractMonster");
        TEXT = AbstractMonster.uiStrings.TEXT;
        INTENT_HB_W = 64.0f * Settings.scale;
        AbstractMonster.sortByHitbox = ((o1, o2) -> (int)(o1.hb.cX - o2.hb.cX));
    }
    
    public enum Intent
    {
        ATTACK, 
        ATTACK_BUFF, 
        ATTACK_DEBUFF, 
        ATTACK_DEFEND, 
        BUFF, 
        DEBUFF, 
        STRONG_DEBUFF, 
        DEBUG, 
        DEFEND, 
        DEFEND_DEBUFF, 
        DEFEND_BUFF, 
        ESCAPE, 
        MAGIC, 
        NONE, 
        SLEEP, 
        STUN, 
        UNKNOWN;
    }
    
    public enum EnemyType
    {
        NORMAL, 
        ELITE, 
        BOSS;
    }
}
