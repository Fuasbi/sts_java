/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BanditLeader extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "BanditLeader";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("BanditLeader");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final int HP_MIN = 35;
/*     */   
/*     */   public static final int HP_MAX = 39;
/*     */   public static final int A_2_HP_MIN = 37;
/*     */   public static final int A_2_HP_MAX = 41;
/*     */   private static final int SLASH_DAMAGE = 15;
/*     */   private static final int AGONIZE_DAMAGE = 10;
/*     */   private static final int A_2_SLASH_DAMAGE = 17;
/*     */   private static final int A_2_AGONIZE_DAMAGE = 12;
/*     */   
/*     */   public BanditLeader(float x, float y)
/*     */   {
/*  39 */     super(NAME, "BanditLeader", 39, -10.0F, -7.0F, 180.0F, 285.0F, null, x, y);
/*  40 */     this.dialogX = (0.0F * Settings.scale);
/*  41 */     this.dialogY = (50.0F * Settings.scale);
/*     */     
/*  43 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  44 */       this.weakAmount = 3;
/*     */     } else {
/*  46 */       this.weakAmount = 2;
/*     */     }
/*     */     
/*  49 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  50 */       setHp(37, 41);
/*     */     } else {
/*  52 */       setHp(35, 39);
/*     */     }
/*     */     
/*  55 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  56 */       this.slashDmg = 17;
/*  57 */       this.agonizeDmg = 12;
/*     */     } else {
/*  59 */       this.slashDmg = 15;
/*  60 */       this.agonizeDmg = 10;
/*     */     }
/*     */     
/*  63 */     this.damage.add(new DamageInfo(this, this.slashDmg));
/*  64 */     this.damage.add(new DamageInfo(this, this.agonizeDmg));
/*     */     
/*  66 */     loadAnimation("images/monsters/theCity/romeo/skeleton.atlas", "images/monsters/theCity/romeo/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  70 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  71 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  72 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  73 */     this.state.setTimeScale(0.8F);
/*     */   }
/*     */   
/*     */   public void deathReact()
/*     */   {
/*  78 */     if (!isDeadOrEscaped()) {
/*  79 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[2]));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  85 */     switch (this.nextMove) {
/*     */     case 2: 
/*  87 */       boolean bearLives = true;
/*  88 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/*  89 */         if (((m instanceof BanditBear)) && (m.isDying)) {
/*  90 */           bearLives = false;
/*  91 */           break;
/*     */         }
/*     */       }
/*  94 */       if (bearLives) {
/*  95 */         AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[0]));
/*     */       } else {
/*  97 */         AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[1]));
/*     */       }
/*  99 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, 
/* 100 */         ((DamageInfo)this.damage.get(1)).base));
/* 101 */       break;
/*     */     case 3: 
/* 103 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "STAB"));
/* 104 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 105 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 108 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/* 110 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, this.weakAmount, true), this.weakAmount));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 116 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, 
/* 117 */         ((DamageInfo)this.damage.get(0)).base));
/* 118 */       break;
/*     */     case 1: 
/* 120 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "STAB"));
/* 121 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 122 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 125 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/*     */ 
/* 128 */       if ((AbstractDungeon.ascensionLevel >= 17) && (!lastTwoMoves((byte)1))) {
/* 129 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, 
/* 130 */           ((DamageInfo)this.damage.get(0)).base));
/*     */       } else {
/* 132 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, 
/* 133 */           ((DamageInfo)this.damage.get(1)).base));
/*     */       }
/* 135 */       break; }
/*     */   }
/*     */   
/*     */   private static final int WEAK_AMT = 2;
/*     */   private static final int A_17_WEAK = 3;
/*     */   private int slashDmg;
/*     */   private int agonizeDmg;
/*     */   
/* 143 */   public void changeState(String key) { switch (key) {
/*     */     case "STAB": 
/* 145 */       this.state.setAnimation(0, "Attack", false);
/* 146 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 147 */       break; }
/*     */   }
/*     */   
/*     */   private int weakAmount;
/*     */   private static final byte CROSS_SLASH = 1;
/*     */   private static final byte MOCK = 2;
/*     */   private static final byte AGONIZING_SLASH = 3;
/*     */   public void damage(DamageInfo info) {
/* 155 */     super.damage(info);
/* 156 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 157 */       this.state.setAnimation(0, "Hit", false);
/* 158 */       this.state.setTimeScale(0.8F);
/* 159 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 165 */     setMove((byte)2, AbstractMonster.Intent.UNKNOWN);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\BanditLeader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */