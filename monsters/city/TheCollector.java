/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.Bone;
/*     */ import com.esotericsoftware.spine.Skeleton;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SuicideAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.CollectorCurseEffect;
/*     */ import com.megacrit.cardcrawl.vfx.GlowyFireEyesEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map.Entry;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class TheCollector extends AbstractMonster
/*     */ {
/*  40 */   private static final Logger logger = LogManager.getLogger(TheCollector.class.getName());
/*     */   public static final String ID = "TheCollector";
/*  42 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("TheCollector");
/*  43 */   public static final String NAME = monsterStrings.NAME;
/*  44 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  45 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final int HP = 282;
/*     */   public static final int A_2_HP = 300;
/*     */   private static final int FIREBALL_DMG = 18;
/*     */   private static final int STR_AMT = 3;
/*     */   private static final int BLOCK_AMT = 15;
/*     */   private static final int A_2_FIREBALL_DMG = 21;
/*     */   private static final int A_2_STR_AMT = 4;
/*     */   private static final int A_2_BLOCK_AMT = 18;
/*     */   private int rakeDmg;
/*  55 */   private int strAmt; private int blockAmt; private int megaDebuffAmt; private static final int MEGA_DEBUFF_AMT = 3; private int turnsTaken = 0;
/*  56 */   private float spawnX = -100.0F;
/*  57 */   private float fireTimer = 0.0F;
/*     */   
/*     */   private static final float FIRE_TIME = 0.07F;
/*  60 */   private boolean ultUsed = false;
/*  61 */   private boolean initialSpawn = true;
/*  62 */   private HashMap<Integer, AbstractMonster> enemySlots = new HashMap();
/*     */   private static final byte SPAWN = 1;
/*     */   private static final byte FIREBALL = 2;
/*     */   
/*     */   public TheCollector() {
/*  67 */     super(NAME, "TheCollector", 282, 15.0F, -40.0F, 300.0F, 390.0F, null, 60.0F, 135.0F);
/*     */     
/*  69 */     this.dialogX = (-90.0F * Settings.scale);
/*  70 */     this.dialogY = (10.0F * Settings.scale);
/*  71 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*     */     
/*  73 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  74 */       setHp(300);
/*  75 */       this.blockAmt = 18;
/*     */     } else {
/*  77 */       setHp(282);
/*  78 */       this.blockAmt = 15;
/*     */     }
/*     */     
/*  81 */     if (AbstractDungeon.ascensionLevel >= 19) {
/*  82 */       this.rakeDmg = 21;
/*  83 */       this.strAmt = 5;
/*  84 */       this.megaDebuffAmt = 5;
/*  85 */     } else if (AbstractDungeon.ascensionLevel >= 4) {
/*  86 */       this.rakeDmg = 21;
/*  87 */       this.strAmt = 4;
/*  88 */       this.megaDebuffAmt = 3;
/*     */     } else {
/*  90 */       this.rakeDmg = 18;
/*  91 */       this.strAmt = 3;
/*  92 */       this.megaDebuffAmt = 3;
/*     */     }
/*     */     
/*  95 */     this.damage.add(new DamageInfo(this, this.rakeDmg));
/*     */     
/*     */ 
/*  98 */     loadAnimation("images/monsters/theCity/collector/skeleton.atlas", "images/monsters/theCity/collector/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/* 102 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 103 */     e.setTime(e.getEndTime() * MathUtils.random()); }
/*     */   
/*     */   private static final byte BUFF = 3;
/*     */   private static final byte MEGA_DEBUFF = 4;
/*     */   private static final byte REVIVE = 5;
/* 108 */   public void usePreBattleAction() { CardCrawlGame.music.unsilenceBGM();
/* 109 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 110 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_CITY");
/* 111 */     UnlockTracker.markBossAsSeen("COLLECTOR");
/*     */   }
/*     */   
/*     */   public void takeTurn() {
/*     */     int i;
/* 116 */     switch (this.nextMove) {
/*     */     case 1: 
/* 118 */       for (i = 1; i < 3; i++) {
/* 119 */         AbstractMonster m = new TorchHead(this.spawnX + -185.0F * i, MathUtils.random(-5.0F, 25.0F));
/* 120 */         AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(m, true));
/* 121 */         this.enemySlots.put(Integer.valueOf(i), m);
/*     */       }
/*     */       
/* 124 */       this.initialSpawn = false;
/* 125 */       break;
/*     */     case 2: 
/* 127 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 128 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/* 129 */       break;
/*     */     case 3: 
/* 131 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 132 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, this.blockAmt + 5));
/*     */       } else {
/* 134 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, this.blockAmt));
/*     */       }
/* 136 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 137 */         if ((!m.isDead) && (!m.isDying) && (!m.isEscaping)) {
/* 138 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new com.megacrit.cardcrawl.powers.StrengthPower(m, this.strAmt), this.strAmt));
/*     */         }
/*     */       }
/*     */       
/* 142 */       break;
/*     */     case 4: 
/* 144 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[0]));
/* 145 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new CollectorCurseEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY), 2.0F));
/*     */       
/*     */ 
/*     */ 
/* 149 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, this.megaDebuffAmt, true), this.megaDebuffAmt));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 155 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, this.megaDebuffAmt, true), this.megaDebuffAmt));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 161 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, this.megaDebuffAmt, true), this.megaDebuffAmt));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 168 */       this.ultUsed = true;
/* 169 */       break;
/*     */     
/*     */ 
/*     */     case 5: 
/* 173 */       for (Map.Entry<Integer, AbstractMonster> m : this.enemySlots.entrySet()) {
/* 174 */         if (((AbstractMonster)m.getValue()).isDying) {
/* 175 */           AbstractMonster newMonster = new TorchHead(this.spawnX + -185.0F * ((Integer)m.getKey()).intValue(), MathUtils.random(-5.0F, 25.0F));
/* 176 */           int key = ((Integer)m.getKey()).intValue();
/* 177 */           this.enemySlots.put(Integer.valueOf(key), newMonster);
/* 178 */           AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(newMonster, true));
/*     */         }
/*     */       }
/* 181 */       break;
/*     */     default: 
/* 183 */       logger.info("ERROR: Default Take Turn was called on " + this.name);
/*     */     }
/*     */     
/* 186 */     this.turnsTaken += 1;
/* 187 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 192 */     if (this.initialSpawn) {
/* 193 */       setMove((byte)1, AbstractMonster.Intent.UNKNOWN);
/* 194 */       return;
/*     */     }
/*     */     
/* 197 */     if ((this.turnsTaken >= 3) && (!this.ultUsed)) {
/* 198 */       setMove((byte)4, AbstractMonster.Intent.STRONG_DEBUFF);
/* 199 */       return;
/*     */     }
/*     */     
/*     */ 
/* 203 */     if ((num <= 25) && (isMinionDead()) && (!lastMove((byte)5))) {
/* 204 */       setMove((byte)5, AbstractMonster.Intent.UNKNOWN);
/* 205 */       return;
/*     */     }
/*     */     
/* 208 */     if ((num <= 70) && (!lastTwoMoves((byte)2))) {
/* 209 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 210 */       return;
/*     */     }
/*     */     
/* 213 */     if (!lastMove((byte)3)) {
/* 214 */       setMove((byte)3, AbstractMonster.Intent.DEFEND_BUFF);
/*     */     } else {
/* 216 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     }
/*     */   }
/*     */   
/*     */   private boolean isMinionDead() {
/* 221 */     for (Map.Entry<Integer, AbstractMonster> m : this.enemySlots.entrySet()) {
/* 222 */       if (((AbstractMonster)m.getValue()).isDying) {
/* 223 */         return true;
/*     */       }
/*     */     }
/*     */     
/* 227 */     return false;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 232 */     super.update();
/* 233 */     if (!this.isDying) {
/* 234 */       this.fireTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 235 */       if (this.fireTimer < 0.0F) {
/* 236 */         this.fireTimer = 0.07F;
/* 237 */         AbstractDungeon.effectList.add(new GlowyFireEyesEffect(this.skeleton
/*     */         
/* 239 */           .getX() + this.skeleton.findBone("lefteyefireslot").getX(), this.skeleton
/* 240 */           .getY() + this.skeleton.findBone("lefteyefireslot").getY() + 140.0F * Settings.scale));
/*     */         
/* 242 */         AbstractDungeon.effectList.add(new GlowyFireEyesEffect(this.skeleton
/*     */         
/* 244 */           .getX() + this.skeleton.findBone("righteyefireslot").getX(), this.skeleton
/* 245 */           .getY() + this.skeleton.findBone("righteyefireslot").getY() + 140.0F * Settings.scale));
/*     */         
/* 247 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.StaffFireEffect(this.skeleton
/*     */         
/* 249 */           .getX() + this.skeleton.findBone("fireslot").getX() - 120.0F * Settings.scale, this.skeleton
/* 250 */           .getY() + this.skeleton.findBone("fireslot").getY() + 390.0F * Settings.scale));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 257 */     useFastShakeAnimation(5.0F);
/* 258 */     CardCrawlGame.screenShake.rumble(4.0F);
/* 259 */     this.deathTimer += 1.5F;
/* 260 */     super.die();
/* 261 */     onBossVictoryLogic();
/*     */     
/* 263 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 264 */       if ((!m.isDead) && (!m.isDying)) {
/* 265 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.HideHealthBarAction(m));
/* 266 */         AbstractDungeon.actionManager.addToTop(new SuicideAction(m));
/* 267 */         AbstractDungeon.actionManager.addToTop(new VFXAction(m, new com.megacrit.cardcrawl.vfx.combat.InflameEffect(m), 0.2F));
/*     */       }
/*     */     }
/* 270 */     UnlockTracker.hardUnlockOverride("COLLECTOR");
/* 271 */     UnlockTracker.unlockAchievement("COLLECTOR");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\TheCollector.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */