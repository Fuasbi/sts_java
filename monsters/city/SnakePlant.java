/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.vfx.combat.BiteEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SnakePlant extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SnakePlant";
/*  27 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("SnakePlant");
/*  28 */   public static final String NAME = monsterStrings.NAME;
/*  29 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  30 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 75;
/*     */   private static final int HP_MAX = 79;
/*     */   private static final int A_2_HP_MIN = 78;
/*     */   private static final int A_2_HP_MAX = 82;
/*     */   private static final byte CHOMPY_CHOMPS = 1;
/*     */   private static final byte SPORES = 2;
/*     */   private static final int CHOMPY_AMT = 3;
/*     */   private static final int CHOMPY_DMG = 7;
/*     */   private static final int A_2_CHOMPY_DMG = 8;
/*     */   private int rainBlowsDmg;
/*     */   
/*  42 */   public SnakePlant(float x, float y) { super(NAME, "SnakePlant", 79, 0.0F, -44.0F, 350.0F, 360.0F, null, x, y + 50.0F);
/*  43 */     loadAnimation("images/monsters/theCity/snakePlant/skeleton.atlas", "images/monsters/theCity/snakePlant/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  47 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  48 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  49 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  50 */     e.setTimeScale(0.8F);
/*     */     
/*  52 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  53 */       setHp(78, 82);
/*     */     } else {
/*  55 */       setHp(75, 79);
/*     */     }
/*     */     
/*  58 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  59 */       this.rainBlowsDmg = 8;
/*     */     } else {
/*  61 */       this.rainBlowsDmg = 7;
/*     */     }
/*     */     
/*  64 */     this.damage.add(new DamageInfo(this, this.rainBlowsDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  69 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.MalleablePower(this)));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/*  74 */     switch (stateName) {
/*     */     case "ATTACK": 
/*  76 */       this.state.setAnimation(0, "Attack", false);
/*  77 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/*  84 */     super.damage(info);
/*  85 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/*  86 */       this.state.setAnimation(0, "Hit", false);
/*  87 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  93 */     AbstractCreature p = AbstractDungeon.player;
/*  94 */     switch (this.nextMove) {
/*     */     case 1: 
/*  96 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  97 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*  98 */       int numBlows = 3;
/*     */       
/* 100 */       for (int i = 0; i < numBlows; i++) {
/* 101 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new BiteEffect(AbstractDungeon.player.hb.cX + 
/*     */         
/*     */ 
/* 104 */           MathUtils.random(-50.0F, 50.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/* 105 */           MathUtils.random(-50.0F, 50.0F) * Settings.scale, Color.CHARTREUSE
/* 106 */           .cpy()), 0.2F));
/*     */         
/* 108 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(p, 
/* 109 */           (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE, true));
/*     */       }
/* 111 */       break;
/*     */     case 2: 
/* 113 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 119 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 125 */       break;
/*     */     }
/*     */     
/*     */     
/* 129 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 134 */     if (AbstractDungeon.ascensionLevel >= 17) {
/* 135 */       if (num < 65) {
/* 136 */         if (lastTwoMoves((byte)1)) {
/* 137 */           setMove(MOVES[0], (byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */         } else {
/* 139 */           setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/*     */         }
/*     */       }
/* 142 */       else if ((lastMove((byte)2)) || (lastMoveBefore((byte)2))) {
/* 143 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/*     */       } else {
/* 145 */         setMove(MOVES[0], (byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */       }
/*     */       
/*     */     }
/* 149 */     else if (num < 65) {
/* 150 */       if (lastTwoMoves((byte)1)) {
/* 151 */         setMove(MOVES[0], (byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */       } else {
/* 153 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/*     */       }
/*     */     }
/* 156 */     else if (lastMove((byte)2)) {
/* 157 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/*     */     } else {
/* 159 */       setMove(MOVES[0], (byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\SnakePlant.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */