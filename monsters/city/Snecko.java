/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Snecko extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Snecko";
/*  29 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Snecko");
/*  30 */   public static final String NAME = monsterStrings.NAME;
/*  31 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  32 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final byte GLARE = 1;
/*     */   private static final byte BITE = 2;
/*     */   private static final byte TAIL = 3;
/*     */   private static final int BITE_DAMAGE = 15;
/*     */   private static final int TAIL_DAMAGE = 8;
/*     */   private static final int A_2_BITE_DAMAGE = 18;
/*     */   private static final int A_2_TAIL_DAMAGE = 10;
/*     */   private int biteDmg;
/*     */   private int tailDmg;
/*     */   private static final int VULNERABLE_AMT = 2;
/*     */   private static final int HP_MIN = 114;
/*  44 */   private static final int HP_MAX = 120; private static final int A_2_HP_MIN = 120; private static final int A_2_HP_MAX = 125; private boolean firstTurn = true;
/*     */   
/*     */   public Snecko() {
/*  47 */     this(0.0F, 0.0F);
/*     */   }
/*     */   
/*     */   public Snecko(float x, float y) {
/*  51 */     super(NAME, "Snecko", 120, -30.0F, -20.0F, 310.0F, 305.0F, null, x, y);
/*  52 */     loadAnimation("images/monsters/theCity/reptile/skeleton.atlas", "images/monsters/theCity/reptile/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  56 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  57 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  58 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  59 */     e.setTimeScale(0.8F);
/*     */     
/*  61 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  62 */       setHp(120, 125);
/*     */     } else {
/*  64 */       setHp(114, 120);
/*     */     }
/*     */     
/*  67 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  68 */       this.biteDmg = 18;
/*  69 */       this.tailDmg = 10;
/*     */     } else {
/*  71 */       this.biteDmg = 15;
/*  72 */       this.tailDmg = 8;
/*     */     }
/*     */     
/*  75 */     this.damage.add(new DamageInfo(this, this.biteDmg));
/*  76 */     this.damage.add(new DamageInfo(this, this.tailDmg));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  81 */     switch (this.nextMove) {
/*     */     case 1: 
/*  83 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  84 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.IntimidateEffect(this.hb.cX, this.hb.cY), 0.5F));
/*     */       
/*  86 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.FastShakeAction(AbstractDungeon.player, 1.0F, 1.0F));
/*  87 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.ConfusionPower(AbstractDungeon.player)));
/*     */       
/*  89 */       break;
/*     */     case 2: 
/*  91 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK_2"));
/*  92 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/*  93 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.BiteEffect(AbstractDungeon.player.hb.cX + 
/*     */       
/*     */ 
/*  96 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/*  97 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, Color.CHARTREUSE
/*  98 */         .cpy()), 0.3F));
/*     */       
/* 100 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 101 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.NONE));
/* 102 */       break;
/*     */     case 3: 
/* 104 */       AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/* 105 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 108 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/* 110 */       if (AbstractDungeon.ascensionLevel >= 17) {
/* 111 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 2, true), 2));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 118 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 124 */       break;
/*     */     }
/*     */     
/*     */     
/* 128 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 133 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 135 */       this.state.setAnimation(0, "Attack", false);
/* 136 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 137 */       break;
/*     */     case "ATTACK_2": 
/* 139 */       this.state.setAnimation(0, "Attack_2", false);
/* 140 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 147 */     super.damage(info);
/* 148 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 149 */       this.state.setAnimation(0, "Hit", false);
/* 150 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 157 */     if (this.firstTurn) {
/* 158 */       this.firstTurn = false;
/* 159 */       setMove(MOVES[0], (byte)1, AbstractMonster.Intent.STRONG_DEBUFF);
/* 160 */       return;
/*     */     }
/*     */     
/*     */ 
/* 164 */     if (num < 40) {
/* 165 */       setMove(MOVES[1], (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 166 */       return;
/*     */     }
/*     */     
/*     */ 
/* 170 */     if (lastTwoMoves((byte)2)) {
/* 171 */       setMove(MOVES[1], (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */     } else {
/* 173 */       setMove(MOVES[2], (byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 179 */     super.die();
/* 180 */     CardCrawlGame.sound.play("SNECKO_DEATH");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Snecko.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */