/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BookOfStabbing extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "BookOfStabbing";
/*  21 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("BookOfStabbing");
/*  22 */   public static final String NAME = monsterStrings.NAME;
/*  23 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  24 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 160;
/*     */   private static final int HP_MAX = 164;
/*     */   private static final int A_2_HP_MIN = 168;
/*     */   private static final int A_2_HP_MAX = 172;
/*     */   private static final int STAB_DAMAGE = 6;
/*     */   private static final int BIG_STAB_DAMAGE = 21;
/*     */   private static final int A_2_STAB_DAMAGE = 7;
/*     */   private static final int A_2_BIG_STAB_DAMAGE = 24;
/*     */   private int stabDmg;
/*  34 */   private int bigStabDmg; private static final byte STAB = 1; private static final byte BIG_STAB = 2; private int stabCount = 1;
/*     */   
/*     */   public BookOfStabbing() {
/*  37 */     super(NAME, "BookOfStabbing", 164, 0.0F, -10.0F, 320.0F, 420.0F, null, 0.0F, 5.0F);
/*  38 */     loadAnimation("images/monsters/theCity/stabBook/skeleton.atlas", "images/monsters/theCity/stabBook/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  42 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  43 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  44 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  45 */     e.setTimeScale(0.8F);
/*     */     
/*  47 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*  48 */     this.dialogX = (-70.0F * Settings.scale);
/*  49 */     this.dialogY = (50.0F * Settings.scale);
/*     */     
/*  51 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  52 */       setHp(168, 172);
/*     */     } else {
/*  54 */       setHp(160, 164);
/*     */     }
/*     */     
/*  57 */     if (AbstractDungeon.ascensionLevel >= 3) {
/*  58 */       this.stabDmg = 7;
/*  59 */       this.bigStabDmg = 24;
/*     */     } else {
/*  61 */       this.stabDmg = 6;
/*  62 */       this.bigStabDmg = 21;
/*     */     }
/*     */     
/*  65 */     this.damage.add(new DamageInfo(this, this.stabDmg));
/*  66 */     this.damage.add(new DamageInfo(this, this.bigStabDmg));
/*     */     
/*  68 */     if (AbstractDungeon.ascensionLevel >= 18) {
/*  69 */       this.stabCount += 1;
/*     */     }
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  75 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.PainfulStabsPower(this)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  80 */     switch (this.nextMove) {
/*     */     case 1: 
/*  82 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  83 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*  84 */       for (int i = 0; i < this.stabCount; i++) {
/*  85 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/*  88 */           (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_VERTICAL));
/*     */       }
/*     */       
/*  91 */       break;
/*     */     case 2: 
/*  93 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK_2"));
/*  94 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*  95 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  98 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_VERTICAL));
/*     */       
/* 100 */       break;
/*     */     }
/*     */     
/*     */     
/* 104 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 109 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 111 */       this.state.setAnimation(0, "Attack", false);
/* 112 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 113 */       break;
/*     */     case "ATTACK_2": 
/* 115 */       this.state.setAnimation(0, "Attack_2", false);
/* 116 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 123 */     super.damage(info);
/* 124 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 125 */       this.state.setAnimation(0, "Hit", false);
/* 126 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 132 */     if (AbstractDungeon.ascensionLevel >= 18) {
/* 133 */       this.stabCount += 1;
/* 134 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.stabCount, true);
/*     */     }
/* 136 */     else if (num < 15) {
/* 137 */       if (lastMove((byte)2)) {
/* 138 */         this.stabCount += 1;
/* 139 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.stabCount, true);
/*     */       } else {
/* 141 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */     }
/* 144 */     else if (lastTwoMoves((byte)1)) {
/* 145 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */     } else {
/* 147 */       this.stabCount += 1;
/* 148 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.stabCount, true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 156 */     super.die();
/* 157 */     CardCrawlGame.sound.play("STAB_BOOK_DEATH");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\BookOfStabbing.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */