/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.ShoutAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.MetallicizePower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.powers.VulnerablePower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.combat.InflameEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Champ extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Champ";
/*  38 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Champ");
/*  39 */   public static final String NAME = monsterStrings.NAME;
/*  40 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  41 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final int HP = 420;
/*     */   public static final int A_9_HP = 440;
/*     */   private static final byte HEAVY_SLASH = 1;
/*     */   private static final byte DEFENSIVE_STANCE = 2;
/*     */   private static final byte EXECUTE = 3;
/*     */   private static final byte FACE_SLAP = 4;
/*  48 */   private static final byte GLOAT = 5; private static final byte TAUNT = 6; private static final byte ANGER = 7; private static final String STANCE_NAME = MOVES[0]; private static final String EXECUTE_NAME = MOVES[1]; private static final String SLAP_NAME = MOVES[2];
/*     */   public static final int SLASH_DMG = 16;
/*     */   public static final int EXECUTE_DMG = 10;
/*     */   public static final int SLAP_DMG = 12;
/*     */   public static final int A_2_SLASH_DMG = 18;
/*     */   public static final int A_2_SLAP_DMG = 14;
/*     */   private int slashDmg;
/*     */   private int executeDmg;
/*     */   private int slapDmg;
/*     */   private int blockAmt;
/*     */   private static final int DEBUFF_AMT = 2;
/*     */   private static final int EXEC_COUNT = 2;
/*     */   private static final int FORGE_AMT = 5;
/*     */   private static final int BLOCK_AMT = 15;
/*     */   private static final int A_9_FORGE_AMT = 6;
/*  63 */   private static final int A_9_BLOCK_AMT = 18; private static final int A_19_FORGE_AMT = 7; private static final int A_19_BLOCK_AMT = 20; private static final int STR_AMT = 2; private static final int A_4_STR_AMT = 3; private static final int A_19_STR_AMT = 4; private int strAmt; private int forgeAmt; private int numTurns = 0;
/*  64 */   private int forgeTimes = 0; private int forgeThreshold = 2;
/*  65 */   private boolean thresholdReached = false;
/*     */   
/*     */   public Champ() {
/*  68 */     super(NAME, "Champ", 420, 0.0F, -15.0F, 400.0F, 410.0F, null, -90.0F, 0.0F);
/*  69 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  70 */     this.dialogX = (-100.0F * Settings.scale);
/*  71 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/*  73 */     loadAnimation("images/monsters/theCity/champ/skeleton.atlas", "images/monsters/theCity/champ/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  77 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  78 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  79 */     e.setTimeScale(0.8F);
/*     */     
/*  81 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  82 */       setHp(440);
/*     */     } else {
/*  84 */       setHp(420);
/*     */     }
/*     */     
/*  87 */     if (AbstractDungeon.ascensionLevel >= 19) {
/*  88 */       this.slashDmg = 18;
/*  89 */       this.executeDmg = 10;
/*  90 */       this.slapDmg = 14;
/*  91 */       this.strAmt = 4;
/*  92 */       this.forgeAmt = 7;
/*  93 */       this.blockAmt = 20;
/*  94 */     } else if (AbstractDungeon.ascensionLevel >= 9) {
/*  95 */       this.slashDmg = 18;
/*  96 */       this.executeDmg = 10;
/*  97 */       this.slapDmg = 14;
/*  98 */       this.strAmt = 3;
/*  99 */       this.forgeAmt = 6;
/* 100 */       this.blockAmt = 18;
/* 101 */     } else if (AbstractDungeon.ascensionLevel >= 4) {
/* 102 */       this.slashDmg = 18;
/* 103 */       this.executeDmg = 10;
/* 104 */       this.slapDmg = 14;
/* 105 */       this.strAmt = 3;
/* 106 */       this.forgeAmt = 5;
/* 107 */       this.blockAmt = 15;
/*     */     } else {
/* 109 */       this.slashDmg = 16;
/* 110 */       this.executeDmg = 10;
/* 111 */       this.slapDmg = 12;
/* 112 */       this.strAmt = 2;
/* 113 */       this.forgeAmt = 5;
/* 114 */       this.blockAmt = 15;
/*     */     }
/*     */     
/* 117 */     this.damage.add(new DamageInfo(this, this.slashDmg));
/* 118 */     this.damage.add(new DamageInfo(this, this.executeDmg));
/* 119 */     this.damage.add(new DamageInfo(this, this.slapDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/* 124 */     CardCrawlGame.music.unsilenceBGM();
/* 125 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 126 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_CITY");
/* 127 */     UnlockTracker.markBossAsSeen("CHAMP");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 132 */     switch (this.nextMove) {
/*     */     case 7: 
/* 134 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_CHAMP_1A"));
/* 135 */       AbstractDungeon.actionManager.addToBottom(new ShoutAction(this, getLimitBreak(), 2.0F, 3.0F));
/* 136 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new InflameEffect(this), 0.25F));
/* 137 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new InflameEffect(this), 0.25F));
/* 138 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new InflameEffect(this), 0.25F));
/* 139 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.RemoveDebuffsAction(this));
/* 140 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, this.strAmt * 3), this.strAmt * 3));
/*     */       
/* 142 */       break;
/*     */     case 1: 
/* 144 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 145 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 146 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 147 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/* 148 */       break;
/*     */     case 2: 
/* 150 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, this.blockAmt));
/* 151 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new MetallicizePower(this, this.forgeAmt), this.forgeAmt));
/*     */       
/* 153 */       break;
/*     */     case 3: 
/* 155 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateJumpAction(this));
/* 156 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 157 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 158 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/* 159 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 160 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/* 161 */       break;
/*     */     case 4: 
/* 163 */       AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/* 164 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 165 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/* 166 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 172 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new VulnerablePower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 178 */       break;
/*     */     case 5: 
/* 180 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, this.strAmt), this.strAmt));
/*     */       
/* 182 */       break;
/*     */     case 6: 
/* 184 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_CHAMP_2A"));
/* 185 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, getTaunt()));
/* 186 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 192 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new VulnerablePower(AbstractDungeon.player, 2, true), 2));
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 200 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 205 */     switch (key) {
/*     */     case "ATTACK": 
/* 207 */       this.state.setAnimation(0, "Attack", false);
/* 208 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 209 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 217 */     super.damage(info);
/* 218 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 219 */       this.state.setAnimation(0, "Hit", false);
/* 220 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private String getTaunt() {
/* 225 */     ArrayList<String> derp = new ArrayList();
/* 226 */     derp.add(DIALOG[0]);
/* 227 */     derp.add(DIALOG[1]);
/* 228 */     derp.add(DIALOG[2]);
/* 229 */     derp.add(DIALOG[3]);
/* 230 */     return (String)derp.get(MathUtils.random(derp.size() - 1));
/*     */   }
/*     */   
/*     */   private String getLimitBreak() {
/* 234 */     ArrayList<String> derp = new ArrayList();
/* 235 */     derp.add(DIALOG[4]);
/* 236 */     derp.add(DIALOG[5]);
/* 237 */     return (String)derp.get(MathUtils.random(derp.size() - 1));
/*     */   }
/*     */   
/*     */   private String getDeathQuote() {
/* 241 */     ArrayList<String> derp = new ArrayList();
/* 242 */     derp.add(DIALOG[6]);
/* 243 */     derp.add(DIALOG[7]);
/* 244 */     return (String)derp.get(MathUtils.random(derp.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 250 */     this.numTurns += 1;
/*     */     
/* 252 */     if ((this.currentHealth < this.maxHealth / 2) && (!this.thresholdReached)) {
/* 253 */       this.thresholdReached = true;
/* 254 */       setMove((byte)7, AbstractMonster.Intent.BUFF);
/* 255 */       return;
/*     */     }
/*     */     
/* 258 */     if ((!lastMove((byte)3)) && (!lastMoveBefore((byte)3)) && (this.thresholdReached)) {
/* 259 */       AbstractDungeon.actionManager.addToTop(new TalkAction(this, getDeathQuote(), 2.0F, 2.0F));
/* 260 */       setMove(EXECUTE_NAME, (byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base, 2, true);
/* 261 */       return;
/*     */     }
/*     */     
/* 264 */     if ((this.numTurns == 4) && (!this.thresholdReached)) {
/* 265 */       setMove((byte)6, AbstractMonster.Intent.DEBUFF);
/* 266 */       this.numTurns = 0;
/* 267 */       return;
/*     */     }
/*     */     
/* 270 */     if (AbstractDungeon.ascensionLevel >= 19)
/*     */     {
/* 272 */       if ((!lastMove((byte)2)) && (this.forgeTimes < this.forgeThreshold) && (num <= 30)) {
/* 273 */         this.forgeTimes += 1;
/* 274 */         setMove(STANCE_NAME, (byte)2, AbstractMonster.Intent.DEFEND_BUFF);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 279 */     else if ((!lastMove((byte)2)) && (this.forgeTimes < this.forgeThreshold) && (num <= 15)) {
/* 280 */       this.forgeTimes += 1;
/* 281 */       setMove(STANCE_NAME, (byte)2, AbstractMonster.Intent.DEFEND_BUFF);
/* 282 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 287 */     if ((!lastMove((byte)5)) && (!lastMove((byte)2)) && (num <= 30)) {
/* 288 */       setMove((byte)5, AbstractMonster.Intent.BUFF);
/* 289 */       return;
/*     */     }
/*     */     
/*     */ 
/* 293 */     if ((!lastMove((byte)4)) && (num <= 55)) {
/* 294 */       setMove(SLAP_NAME, (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(2)).base);
/* 295 */       return;
/*     */     }
/*     */     
/* 298 */     if (!lastMove((byte)1)) {
/* 299 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     } else {
/* 301 */       setMove(SLAP_NAME, (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(2)).base);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void die()
/*     */   {
/* 308 */     useFastShakeAnimation(5.0F);
/* 309 */     CardCrawlGame.screenShake.rumble(4.0F);
/* 310 */     this.deathTimer += 1.5F;
/* 311 */     super.die();
/* 312 */     if (MathUtils.randomBoolean()) {
/* 313 */       CardCrawlGame.sound.play("VO_CHAMP_3A");
/*     */     } else {
/* 315 */       CardCrawlGame.sound.play("VO_CHAMP_3B");
/*     */     }
/* 317 */     AbstractDungeon.scene.fadeInAmbiance();
/* 318 */     onBossVictoryLogic();
/* 319 */     UnlockTracker.hardUnlockOverride("CHAMP");
/* 320 */     UnlockTracker.unlockAchievement("CHAMP");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Champ.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */