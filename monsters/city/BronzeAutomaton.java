/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.combat.LaserBeamEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BronzeAutomaton extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "BronzeAutomaton";
/*  33 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("BronzeAutomaton");
/*  34 */   public static final String NAME = monsterStrings.NAME;
/*  35 */   private static final String[] MOVES = monsterStrings.MOVES;
/*     */   private static final int HP = 300;
/*     */   private static final int A_2_HP = 320;
/*     */   private static final byte FLAIL = 1;
/*     */   private static final byte HYPER_BEAM = 2;
/*     */   private static final byte STUNNED = 3;
/*  41 */   private static final byte SPAWN_ORBS = 4; private static final byte BOOST = 5; private static final String BEAM_NAME = MOVES[0];
/*     */   private static final int FLAIL_DMG = 7;
/*     */   private static final int BEAM_DMG = 45;
/*     */   private static final int A_2_FLAIL_DMG = 8;
/*     */   private static final int A_2_BEAM_DMG = 50;
/*     */   private int flailDmg;
/*     */   private int beamDmg;
/*     */   private static final int BLOCK_AMT = 9;
/*     */   private static final int STR_AMT = 3;
/*     */   private static final int A_2_BLOCK_AMT = 12;
/*  51 */   private static final int A_2_STR_AMT = 4; private int strAmt; private int blockAmt; private int numTurns = 0;
/*  52 */   private boolean firstTurn = true;
/*     */   
/*     */   public BronzeAutomaton() {
/*  55 */     super(NAME, "BronzeAutomaton", 300, 0.0F, -30.0F, 270.0F, 400.0F, null, -50.0F, 20.0F);
/*  56 */     loadAnimation("images/monsters/theCity/automaton/skeleton.atlas", "images/monsters/theCity/automaton/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  60 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  61 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */     
/*  63 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  64 */     this.dialogX = (-100.0F * Settings.scale);
/*  65 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/*  67 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  68 */       setHp(320);
/*  69 */       this.blockAmt = 12;
/*     */     } else {
/*  71 */       setHp(300);
/*  72 */       this.blockAmt = 9;
/*     */     }
/*     */     
/*  75 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  76 */       this.flailDmg = 8;
/*  77 */       this.beamDmg = 50;
/*  78 */       this.strAmt = 4;
/*     */     } else {
/*  80 */       this.flailDmg = 7;
/*  81 */       this.beamDmg = 45;
/*  82 */       this.strAmt = 3;
/*     */     }
/*     */     
/*  85 */     this.damage.add(new DamageInfo(this, this.flailDmg));
/*  86 */     this.damage.add(new DamageInfo(this, this.beamDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  91 */     CardCrawlGame.music.unsilenceBGM();
/*  92 */     AbstractDungeon.scene.fadeOutAmbiance();
/*  93 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_CITY");
/*  94 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ArtifactPower(this, 3)));
/*  95 */     UnlockTracker.markBossAsSeen("AUTOMATON");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 100 */     switch (this.nextMove) {
/*     */     case 4: 
/* 102 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("AUTOMATON_ORB_SPAWN", 
/* 103 */         MathUtils.random(-0.1F, 0.1F)));
/* 104 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new BronzeOrb(-300.0F, 200.0F, 0), true, 0));
/*     */       
/* 106 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("AUTOMATON_ORB_SPAWN", 
/* 107 */         MathUtils.random(-0.1F, 0.1F)));
/* 108 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new BronzeOrb(200.0F, 130.0F, 1), true, 2));
/*     */       
/* 110 */       break;
/*     */     case 1: 
/* 112 */       AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/* 113 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 114 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/* 115 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 116 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/* 117 */       break;
/*     */     case 5: 
/* 119 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, this.blockAmt));
/* 120 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.StrengthPower(this, this.strAmt), this.strAmt));
/*     */       
/* 122 */       break;
/*     */     case 2: 
/* 124 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new LaserBeamEffect(this.hb.cX, this.hb.cY + 60.0F * Settings.scale), 1.5F));
/*     */       
/* 126 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 127 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.NONE));
/* 128 */       break;
/*     */     case 3: 
/* 130 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this, TextAboveCreatureAction.TextType.STUNNED));
/*     */     }
/*     */     
/* 133 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 138 */     if (this.firstTurn) {
/* 139 */       setMove((byte)4, AbstractMonster.Intent.UNKNOWN);
/* 140 */       this.firstTurn = false;
/* 141 */       return;
/*     */     }
/*     */     
/* 144 */     if (this.numTurns == 4) {
/* 145 */       setMove(BEAM_NAME, (byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/* 146 */       this.numTurns = 0;
/* 147 */       return;
/*     */     }
/*     */     
/*     */ 
/* 151 */     if (lastMove((byte)2)) {
/* 152 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 153 */         setMove((byte)5, AbstractMonster.Intent.DEFEND_BUFF);
/* 154 */         return;
/*     */       }
/* 156 */       setMove((byte)3, AbstractMonster.Intent.STUN);
/* 157 */       return;
/*     */     }
/*     */     
/*     */ 
/* 161 */     if ((lastMove((byte)3)) || (lastMove((byte)5)) || (lastMove((byte)4))) {
/* 162 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */     } else {
/* 164 */       setMove((byte)5, AbstractMonster.Intent.DEFEND_BUFF);
/*     */     }
/*     */     
/* 167 */     this.numTurns += 1;
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 172 */     useFastShakeAnimation(5.0F);
/* 173 */     CardCrawlGame.screenShake.rumble(4.0F);
/* 174 */     this.deathTimer += 1.5F;
/* 175 */     super.die();
/* 176 */     onBossVictoryLogic();
/*     */     
/* 178 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 179 */       if ((!m.isDead) && (!m.isDying)) {
/* 180 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.HideHealthBarAction(m));
/* 181 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.SuicideAction(m));
/* 182 */         AbstractDungeon.actionManager.addToTop(new VFXAction(m, new com.megacrit.cardcrawl.vfx.combat.InflameEffect(m), 0.2F));
/*     */       }
/*     */     }
/* 185 */     UnlockTracker.hardUnlockOverride("AUTOMATON");
/* 186 */     UnlockTracker.unlockAchievement("AUTOMATON");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\BronzeAutomaton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */