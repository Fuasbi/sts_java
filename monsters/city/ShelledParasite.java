/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateHopAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ShelledParasite extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Shelled Parasite";
/*  31 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Shelled Parasite");
/*  32 */   public static final String NAME = monsterStrings.NAME;
/*  33 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  34 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 68;
/*     */   private static final int HP_MAX = 72;
/*     */   private static final int A_2_HP_MIN = 70;
/*     */   private static final int A_2_HP_MAX = 75;
/*     */   private static final float HB_X_F = 20.0F;
/*     */   private static final float HB_Y_F = -6.0F;
/*     */   private static final float HB_W = 350.0F;
/*     */   private static final float HB_H = 260.0F;
/*     */   private static final int PLATED_ARMOR_AMT = 14;
/*     */   private static final int FELL_DMG = 18;
/*     */   private static final int DOUBLE_STRIKE_DMG = 6;
/*     */   private static final int SUCK_DMG = 10;
/*  47 */   private static final int A_2_FELL_DMG = 21; private static final int A_2_DOUBLE_STRIKE_DMG = 7; private static final int A_2_SUCK_DMG = 12; private int fellDmg; private int doubleStrikeDmg; private int suckDmg; private static final int DOUBLE_STRIKE_COUNT = 2; private static final int FELL_FRAIL_AMT = 2; private static final byte FELL = 1; private static final byte DOUBLE_STRIKE = 2; private static final byte LIFE_SUCK = 3; private static final byte STUNNED = 4; private boolean firstMove = true;
/*     */   public static final String ARMOR_BREAK = "ARMOR_BREAK";
/*     */   
/*     */   public ShelledParasite(float x, float y) {
/*  51 */     super(NAME, "Shelled Parasite", 72, 20.0F, -6.0F, 350.0F, 260.0F, null, x, y);
/*     */     
/*  53 */     loadAnimation("images/monsters/theCity/shellMonster/skeleton.atlas", "images/monsters/theCity/shellMonster/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  57 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  58 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  59 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  60 */     e.setTimeScale(0.8F);
/*     */     
/*  62 */     this.dialogX = (-50.0F * Settings.scale);
/*     */     
/*  64 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  65 */       setHp(70, 75);
/*     */     } else {
/*  67 */       setHp(68, 72);
/*     */     }
/*     */     
/*  70 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  71 */       this.doubleStrikeDmg = 7;
/*  72 */       this.fellDmg = 21;
/*  73 */       this.suckDmg = 12;
/*     */     } else {
/*  75 */       this.doubleStrikeDmg = 6;
/*  76 */       this.fellDmg = 18;
/*  77 */       this.suckDmg = 10;
/*     */     }
/*     */     
/*  80 */     this.damage.add(new DamageInfo(this, this.doubleStrikeDmg));
/*  81 */     this.damage.add(new DamageInfo(this, this.fellDmg));
/*  82 */     this.damage.add(new DamageInfo(this, this.suckDmg));
/*     */   }
/*     */   
/*     */   public ShelledParasite() {
/*  86 */     this(-20.0F, 10.0F);
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  91 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.PlatedArmorPower(this, 14)));
/*     */     
/*  93 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, 14));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  98 */     switch (this.nextMove) {
/*     */     case 1: 
/* 100 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 101 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 102 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 105 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*     */ 
/* 108 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 114 */       break;
/*     */     case 2: 
/* 116 */       for (int i = 0; i < 2; i++) {
/* 117 */         AbstractDungeon.actionManager.addToBottom(new AnimateHopAction(this));
/* 118 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.2F));
/* 119 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/* 122 */           (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       }
/*     */       
/* 125 */       break;
/*     */     case 3: 
/* 127 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 128 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.4F));
/* 129 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.BiteEffect(AbstractDungeon.player.hb.cX + 
/*     */       
/*     */ 
/* 132 */         MathUtils.random(-25.0F, 25.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/* 133 */         MathUtils.random(-25.0F, 25.0F) * Settings.scale, Color.GOLD
/* 134 */         .cpy()), 0.0F));
/*     */       
/* 136 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.VampireDamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 139 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.NONE));
/*     */       
/* 141 */       break;
/*     */     case 4: 
/* 143 */       setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 144 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this, TextAboveCreatureAction.TextType.STUNNED));
/*     */     }
/*     */     
/* 147 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 152 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 154 */       this.state.setAnimation(0, "Attack", false);
/* 155 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 156 */       break;
/*     */     case "ARMOR_BREAK": 
/* 158 */       AbstractDungeon.actionManager.addToBottom(new AnimateHopAction(this));
/* 159 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 160 */       AbstractDungeon.actionManager.addToBottom(new AnimateHopAction(this));
/* 161 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 162 */       AbstractDungeon.actionManager.addToBottom(new AnimateHopAction(this));
/* 163 */       setMove((byte)4, AbstractMonster.Intent.STUN);
/* 164 */       createIntent();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 171 */     super.damage(info);
/* 172 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 173 */       this.state.setAnimation(0, "Hit", false);
/* 174 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 181 */     if (this.firstMove) {
/* 182 */       this.firstMove = false;
/* 183 */       if (AbstractDungeon.ascensionLevel >= 17) {
/* 184 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 185 */         return; }
/* 186 */       if (AbstractDungeon.aiRng.randomBoolean()) {
/* 187 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */       } else {
/* 189 */         setMove((byte)3, AbstractMonster.Intent.ATTACK_BUFF, ((DamageInfo)this.damage.get(2)).base);
/*     */       }
/* 191 */       return;
/*     */     }
/*     */     
/*     */ 
/* 195 */     if (num < 20) {
/* 196 */       if (!lastMove((byte)1)) {
/* 197 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */       } else {
/* 199 */         getMove(AbstractDungeon.aiRng.random(20, 99));
/*     */       }
/*     */     }
/* 202 */     else if (num < 60) {
/* 203 */       if (!lastTwoMoves((byte)2)) {
/* 204 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */       } else {
/* 206 */         setMove((byte)3, AbstractMonster.Intent.ATTACK_BUFF, ((DamageInfo)this.damage.get(2)).base);
/*     */       }
/*     */     }
/* 209 */     else if (!lastTwoMoves((byte)3)) {
/* 210 */       setMove((byte)3, AbstractMonster.Intent.ATTACK_BUFF, ((DamageInfo)this.damage.get(2)).base);
/*     */     } else {
/* 212 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\ShelledParasite.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */