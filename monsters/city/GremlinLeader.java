/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.ShoutAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.SummonGremlinAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinLeader extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinLeader";
/*  28 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("GremlinLeader");
/*  29 */   public static final String NAME = monsterStrings.NAME;
/*  30 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  31 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 140;
/*     */   private static final int HP_MAX = 148;
/*     */   private static final int A_2_HP_MIN = 145;
/*     */   private static final int A_2_HP_MAX = 155;
/*     */   public static final String ENC_NAME = "Gremlin Leader Combat";
/*  37 */   public AbstractMonster[] gremlins = new AbstractMonster[3];
/*     */   
/*     */   public static final float POSX1 = -366.0F;
/*     */   
/*     */   public static final float POSY1 = -4.0F;
/*     */   public static final float POSX2 = -170.0F;
/*     */   public static final float POSY2 = 6.0F;
/*     */   public static final float POSX3 = -532.0F;
/*     */   public static final float POSY3 = 0.0F;
/*     */   private static final byte RALLY = 2;
/*  47 */   private static final String RALLY_NAME = MOVES[0];
/*     */   
/*     */   private static final byte ENCOURAGE = 3;
/*     */   private static final int STR_AMT = 3;
/*     */   private static final int BLOCK_AMT = 6;
/*     */   private static final int A_2_STR_AMT = 4;
/*     */   private static final int A_18_STR_AMT = 5;
/*     */   private static final int A_18_BLOCK_AMT = 10;
/*     */   private int strAmt;
/*     */   private int blockAmt;
/*     */   private static final byte STAB = 4;
/*  58 */   private int STAB_DMG = 6; private int STAB_AMT = 3;
/*     */   
/*     */   public GremlinLeader() {
/*  61 */     super(NAME, "GremlinLeader", 148, 0.0F, -15.0F, 200.0F, 310.0F, null, 35.0F, 0.0F);
/*  62 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*     */     
/*  64 */     loadAnimation("images/monsters/theCity/gremlinleader/skeleton.atlas", "images/monsters/theCity/gremlinleader/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  68 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  69 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  70 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  71 */     e.setTimeScale(0.8F);
/*     */     
/*  73 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  74 */       setHp(145, 155);
/*     */     } else {
/*  76 */       setHp(140, 148);
/*     */     }
/*     */     
/*  79 */     if (AbstractDungeon.ascensionLevel >= 18) {
/*  80 */       this.strAmt = 5;
/*  81 */       this.blockAmt = 10;
/*  82 */     } else if (AbstractDungeon.ascensionLevel >= 3) {
/*  83 */       this.strAmt = 4;
/*  84 */       this.blockAmt = 6;
/*     */     } else {
/*  86 */       this.strAmt = 3;
/*  87 */       this.blockAmt = 6;
/*     */     }
/*     */     
/*  90 */     this.dialogX = (-80.0F * Settings.scale);
/*  91 */     this.dialogY = (50.0F * Settings.scale);
/*  92 */     this.damage.add(new DamageInfo(this, this.STAB_DMG));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  97 */     this.gremlins[0] = ((AbstractMonster)AbstractDungeon.getMonsters().monsters.get(0));
/*  98 */     this.gremlins[1] = ((AbstractMonster)AbstractDungeon.getMonsters().monsters.get(1));
/*  99 */     this.gremlins[2] = null;
/*     */     
/* 101 */     for (AbstractMonster m : this.gremlins) {
/* 102 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.MinionPower(this)));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 108 */     switch (this.nextMove) {
/*     */     case 2: 
/* 110 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "CALL"));
/* 111 */       AbstractDungeon.actionManager.addToBottom(new SummonGremlinAction(this.gremlins));
/* 112 */       AbstractDungeon.actionManager.addToBottom(new SummonGremlinAction(this.gremlins));
/* 113 */       break;
/*     */     case 3: 
/* 115 */       AbstractDungeon.actionManager.addToBottom(new ShoutAction(this, getEncourageQuote()));
/* 116 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 117 */         if (m == this) {
/* 118 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new StrengthPower(m, this.strAmt), this.strAmt));
/*     */ 
/*     */         }
/* 121 */         else if (!m.isDying) {
/* 122 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new StrengthPower(m, this.strAmt), this.strAmt));
/*     */           
/* 124 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(m, this, this.blockAmt));
/*     */         }
/*     */       }
/*     */       
/* 128 */       break;
/*     */     case 4: 
/* 130 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 131 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 132 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 133 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_HORIZONTAL, true));
/* 134 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 135 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_VERTICAL, true));
/* 136 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 137 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */     }
/*     */     
/* 140 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private String getEncourageQuote() {
/* 144 */     ArrayList<String> list = new ArrayList();
/* 145 */     list.add(DIALOG[0]);
/* 146 */     list.add(DIALOG[1]);
/* 147 */     list.add(DIALOG[2]);
/*     */     
/* 149 */     return (String)list.get(AbstractDungeon.aiRng.random(0, list.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 156 */     if (numAliveGremlins() == 0) {
/* 157 */       if (num < 75) {
/* 158 */         if (!lastMove((byte)2)) {
/* 159 */           setMove(RALLY_NAME, (byte)2, AbstractMonster.Intent.UNKNOWN);
/*     */         } else {
/* 161 */           setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */         }
/*     */       }
/* 164 */       else if (!lastMove((byte)4)) {
/* 165 */         setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */       } else {
/* 167 */         setMove(RALLY_NAME, (byte)2, AbstractMonster.Intent.UNKNOWN);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 172 */     else if (numAliveGremlins() < 2) {
/* 173 */       if (num < 50) {
/* 174 */         if (!lastMove((byte)2)) {
/* 175 */           setMove(RALLY_NAME, (byte)2, AbstractMonster.Intent.UNKNOWN);
/*     */         } else {
/* 177 */           getMove(AbstractDungeon.aiRng.random(50, 99));
/*     */         }
/* 179 */       } else if (num < 80) {
/* 180 */         if (!lastMove((byte)3)) {
/* 181 */           setMove((byte)3, AbstractMonster.Intent.DEFEND_BUFF);
/*     */         } else {
/* 183 */           setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */         }
/*     */       }
/* 186 */       else if (!lastMove((byte)4)) {
/* 187 */         setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */       } else {
/* 189 */         getMove(AbstractDungeon.aiRng.random(0, 80));
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 194 */     else if (numAliveGremlins() > 1) {
/* 195 */       if (num < 66) {
/* 196 */         if (!lastMove((byte)3)) {
/* 197 */           setMove((byte)3, AbstractMonster.Intent.DEFEND_BUFF);
/*     */         } else {
/* 199 */           setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */         }
/*     */       }
/* 202 */       else if (!lastMove((byte)4)) {
/* 203 */         setMove((byte)4, AbstractMonster.Intent.ATTACK, this.STAB_DMG, this.STAB_AMT, true);
/*     */       } else {
/* 205 */         setMove((byte)3, AbstractMonster.Intent.DEFEND_BUFF);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private int numAliveGremlins()
/*     */   {
/* 212 */     int count = 0;
/* 213 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 214 */       if ((m != null) && (m != this) && (!m.isDying)) {
/* 215 */         count++;
/*     */       }
/*     */     }
/* 218 */     return count;
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 223 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 225 */       this.state.setAnimation(0, "Attack", false);
/* 226 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 227 */       break;
/*     */     case "CALL": 
/* 229 */       this.state.setAnimation(0, "Call", false);
/* 230 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 237 */     super.damage(info);
/* 238 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 239 */       this.state.setAnimation(0, "Hit", false);
/* 240 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 246 */     super.die();
/* 247 */     boolean first = true;
/* 248 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 249 */       if (!m.isDying) {
/* 250 */         if (first) {
/* 251 */           AbstractDungeon.actionManager.addToBottom(new ShoutAction(m, DIALOG[3], 0.5F, 1.2F));
/* 252 */           first = false;
/*     */         } else {
/* 254 */           AbstractDungeon.actionManager.addToBottom(new ShoutAction(m, DIALOG[4], 0.5F, 1.2F));
/*     */         }
/*     */       }
/*     */     }
/* 258 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 259 */       if (!m.isDying) {
/* 260 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(m));
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\GremlinLeader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */