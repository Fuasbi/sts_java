/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Wound;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Taskmaster extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SlaverBoss";
/*  22 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("SlaverBoss");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 54;
/*     */   private static final int HP_MAX = 60;
/*     */   private static final int A_2_HP_MIN = 57;
/*     */   private static final int A_2_HP_MAX = 64;
/*     */   private static final int WHIP_DMG = 4;
/*     */   private static final int SCOURING_WHIP_DMG = 7;
/*     */   private static final int WOUNDS = 1;
/*     */   private static final int A_2_WOUNDS = 2;
/*     */   private int woundCount;
/*     */   private static final byte SCOURING_WHIP = 2;
/*     */   
/*     */   public Taskmaster(float x, float y)
/*     */   {
/*  39 */     super(NAME, "SlaverBoss", AbstractDungeon.monsterHpRng.random(54, 60), -10.0F, -8.0F, 200.0F, 280.0F, null, x, y);
/*  40 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*     */     
/*  42 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  43 */       setHp(57, 64);
/*     */     } else {
/*  45 */       setHp(54, 60);
/*     */     }
/*     */     
/*  48 */     if (AbstractDungeon.ascensionLevel >= 18) {
/*  49 */       this.woundCount = 3;
/*  50 */     } else if (AbstractDungeon.ascensionLevel >= 3) {
/*  51 */       this.woundCount = 2;
/*     */     } else {
/*  53 */       this.woundCount = 1;
/*     */     }
/*     */     
/*  56 */     this.damage.add(new DamageInfo(this, 4));
/*  57 */     this.damage.add(new DamageInfo(this, 7));
/*     */     
/*  59 */     loadAnimation("images/monsters/theCity/slaverMaster/skeleton.atlas", "images/monsters/theCity/slaverMaster/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  63 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  64 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  69 */     switch (this.nextMove) {
/*     */     case 2: 
/*  71 */       playSfx();
/*  72 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  73 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  76 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */       
/*  78 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new Wound(), this.woundCount));
/*  79 */       if (AbstractDungeon.ascensionLevel >= 18) {
/*  80 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.StrengthPower(this, 1), 1));
/*     */       }
/*     */       
/*     */       break;
/*     */     }
/*     */     
/*     */     
/*  87 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/*  92 */     setMove((byte)2, com.megacrit.cardcrawl.monsters.AbstractMonster.Intent.ATTACK_DEBUFF, 7);
/*     */   }
/*     */   
/*     */   private void playSfx() {
/*  96 */     int roll = MathUtils.random(1);
/*  97 */     if (roll == 0) {
/*  98 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_SLAVERLEADER_1A"));
/*     */     } else {
/* 100 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_SLAVERLEADER_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 105 */     int roll = MathUtils.random(1);
/* 106 */     if (roll == 0) {
/* 107 */       CardCrawlGame.sound.play("VO_SLAVERLEADER_2A");
/*     */     } else {
/* 109 */       CardCrawlGame.sound.play("VO_SLAVERLEADER_2B");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 115 */     super.die();
/* 116 */     playDeathSfx();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Taskmaster.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */