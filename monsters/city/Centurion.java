/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Centurion extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Centurion";
/*  20 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Centurion");
/*  21 */   public static final String NAME = monsterStrings.NAME;
/*  22 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  23 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final float IDLE_TIMESCALE = 0.8F;
/*     */   private static final int HP_MIN = 76;
/*     */   private static final int HP_MAX = 80;
/*     */   private static final int A_2_HP_MIN = 78;
/*     */   private static final int A_2_HP_MAX = 83;
/*     */   private static final int SLASH_DMG = 12;
/*     */   private static final int FURY_DMG = 6;
/*     */   private static final int FURY_HITS = 3;
/*  32 */   private static final int A_2_SLASH_DMG = 14; private static final int A_2_FURY_DMG = 7; private int slashDmg; private int furyDmg; private int furyHits; private int blockAmount; private int BLOCK_AMOUNT = 15; private int A_17_BLOCK_AMOUNT = 20;
/*     */   private static final byte SLASH = 1;
/*     */   
/*     */   public Centurion(float x, float y) {
/*  36 */     super(NAME, "Centurion", 80, -14.0F, -20.0F, 250.0F, 330.0F, null, x, y);
/*     */     
/*  38 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  39 */       setHp(78, 83);
/*     */     } else {
/*  41 */       setHp(76, 80);
/*     */     }
/*     */     
/*  44 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  45 */       this.blockAmount = this.A_17_BLOCK_AMOUNT;
/*     */     } else {
/*  47 */       this.blockAmount = this.BLOCK_AMOUNT;
/*     */     }
/*     */     
/*  50 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  51 */       this.slashDmg = 14;
/*  52 */       this.furyDmg = 7;
/*  53 */       this.furyHits = 3;
/*     */     } else {
/*  55 */       this.slashDmg = 12;
/*  56 */       this.furyDmg = 6;
/*  57 */       this.furyHits = 3;
/*     */     }
/*     */     
/*  60 */     this.damage.add(new DamageInfo(this, this.slashDmg));
/*  61 */     this.damage.add(new DamageInfo(this, this.furyDmg));
/*     */     
/*  63 */     loadAnimation("images/monsters/theCity/tank/skeleton.atlas", "images/monsters/theCity/tank/skeleton.json", 1.0F);
/*  64 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  65 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  66 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  67 */     this.state.setTimeScale(0.8F);
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  72 */     switch (this.nextMove) {
/*     */     case 1: 
/*  74 */       playSfx();
/*  75 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "MACE_HIT"));
/*  76 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/*  77 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  80 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/*  82 */       break;
/*     */     case 2: 
/*  84 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.25F));
/*  85 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.GainBlockRandomMonsterAction(this, this.blockAmount));
/*  86 */       break;
/*     */     case 3: 
/*  88 */       for (int i = 0; i < this.furyHits; i++) {
/*  89 */         playSfx();
/*  90 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "MACE_HIT"));
/*  91 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/*  92 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/*  95 */           (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       }
/*     */       
/*  98 */       break;
/*     */     }
/*     */     
/*     */     
/* 102 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 106 */     int roll = MathUtils.random(1);
/* 107 */     if (roll == 0) {
/* 108 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_TANK_1A"));
/* 109 */     } else if (roll == 1) {
/* 110 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_TANK_1B"));
/*     */     } else {
/* 112 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_TANK_1C"));
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 118 */     switch (key)
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */     case "MACE_HIT": 
/* 124 */       this.state.setAnimation(0, "Attack", false);
/* 125 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 126 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 134 */     if ((num >= 65) && (!lastTwoMoves((byte)2)) && (!lastTwoMoves((byte)3))) {
/* 135 */       int aliveCount = 0;
/*     */       
/*     */ 
/* 138 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 139 */         if ((!m.isDying) && (!m.isEscaping)) {
/* 140 */           aliveCount++;
/*     */         }
/*     */       }
/*     */       
/* 144 */       if (aliveCount > 1) {
/* 145 */         setMove((byte)2, AbstractMonster.Intent.DEFEND);
/* 146 */         return;
/*     */       }
/* 148 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base, this.furyHits, true);
/* 149 */       return;
/*     */     }
/*     */     
/*     */ 
/* 153 */     if (!lastTwoMoves((byte)1)) {
/* 154 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 155 */       return;
/*     */     }
/* 157 */     int aliveCount = 0;
/*     */     
/*     */ 
/* 160 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 161 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 162 */         aliveCount++;
/*     */       }
/*     */     }
/*     */     
/* 166 */     if (aliveCount > 1) {
/* 167 */       setMove((byte)2, AbstractMonster.Intent.DEFEND);
/* 168 */       return;
/*     */     }
/* 170 */     setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base, this.furyHits, true);
/*     */   }
/*     */   
/*     */ 
/*     */   private static final byte PROTECT = 2;
/*     */   private static final byte FURY = 3;
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 178 */     super.damage(info);
/* 179 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 180 */       this.state.setAnimation(0, "Hit", false);
/* 181 */       this.state.setTimeScale(0.8F);
/* 182 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 188 */     this.state.setTimeScale(0.1F);
/* 189 */     useShakeAnimation(5.0F);
/* 190 */     super.die();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Centurion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */