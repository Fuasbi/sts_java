/*    */ package com.megacrit.cardcrawl.monsters.city;
/*    */ 
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.esotericsoftware.spine.Bone;
/*    */ import com.esotericsoftware.spine.Skeleton;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class TorchHead extends AbstractMonster
/*    */ {
/*    */   public static final String ID = "TorchHead";
/* 20 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("TorchHead");
/* 21 */   public static final String NAME = monsterStrings.NAME;
/* 22 */   public static final String[] MOVES = monsterStrings.MOVES;
/* 23 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*    */   
/*    */   public static final int HP_MIN = 38;
/*    */   
/*    */   public static final int HP_MAX = 40;
/*    */   public static final int A_2_HP_MIN = 40;
/*    */   public static final int A_2_HP_MAX = 45;
/*    */   public static final int ATTACK_DMG = 7;
/*    */   private static final byte TACKLE = 1;
/* 32 */   private float fireTimer = 0.0F;
/*    */   private static final float FIRE_TIME = 0.04F;
/*    */   
/*    */   public TorchHead(float x, float y) {
/* 36 */     super(NAME, "TorchHead", AbstractDungeon.monsterHpRng.random(38, 40), -5.0F, -20.0F, 145.0F, 240.0F, null, x, y);
/* 37 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, 7);
/* 38 */     this.damage.add(new DamageInfo(this, 7));
/*    */     
/*    */ 
/* 41 */     loadAnimation("images/monsters/theCity/torchHead/skeleton.atlas", "images/monsters/theCity/torchHead/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/* 45 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 46 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*    */     
/* 48 */     if (AbstractDungeon.ascensionLevel >= 9) {
/* 49 */       setHp(40, 45);
/*    */     } else {
/* 51 */       setHp(38, 40);
/*    */     }
/*    */   }
/*    */   
/*    */   public void takeTurn()
/*    */   {
/* 57 */     switch (this.nextMove) {
/*    */     case 1: 
/* 59 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 60 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 63 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*    */       
/* 65 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, 7));
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 73 */     super.update();
/* 74 */     if (!this.isDying) {
/* 75 */       this.fireTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 76 */       if (this.fireTimer < 0.0F) {
/* 77 */         this.fireTimer = 0.04F;
/* 78 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.TorchHeadFireEffect(this.skeleton
/*    */         
/* 80 */           .getX() + this.skeleton.findBone("fireslot").getX() + 10.0F * Settings.scale, this.skeleton
/* 81 */           .getY() + this.skeleton.findBone("fireslot").getY() + 110.0F * Settings.scale));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   protected void getMove(int num)
/*    */   {
/* 88 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, 7);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\TorchHead.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */