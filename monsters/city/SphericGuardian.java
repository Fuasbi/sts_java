/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SphericGuardian extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SphericGuardian";
/*  26 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("SphericGuardian");
/*  27 */   public static final String NAME = monsterStrings.NAME;
/*  28 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  29 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final float IDLE_TIMESCALE = 0.8F;
/*     */   private static final float HB_X = 0.0F;
/*     */   private static final float HB_Y = 10.0F;
/*     */   private static final float HB_W = 280.0F;
/*     */   private static final float HB_H = 280.0F;
/*     */   private static final int DMG = 10;
/*     */   private static final int A_2_DMG = 11;
/*     */   private int dmg;
/*  38 */   private static final int SLAM_AMT = 2; private static final int HARDEN_BLOCK = 15; private static final int FRAIL_AMT = 5; private static final int ACTIVATE_BLOCK = 25; private static final int ARTIFACT_AMT = 3; private static final int STARTING_BLOCK_AMT = 40; private static final byte BIG_ATTACK = 1; private static final byte INITIAL_BLOCK_GAIN = 2; private static final byte BLOCK_ATTACK = 3; private static final byte FRAIL_ATTACK = 4; private boolean firstMove = true; private boolean secondMove = true;
/*     */   
/*     */   public SphericGuardian() {
/*  41 */     this(0.0F, 0.0F);
/*     */   }
/*     */   
/*     */   public SphericGuardian(float x, float y) {
/*  45 */     super(NAME, "SphericGuardian", 20, 0.0F, 10.0F, 280.0F, 280.0F, null, x, y);
/*     */     
/*  47 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  48 */       this.dmg = 11;
/*     */     } else {
/*  50 */       this.dmg = 10;
/*     */     }
/*     */     
/*  53 */     this.damage.add(new DamageInfo(this, this.dmg));
/*     */     
/*  55 */     loadAnimation("images/monsters/theCity/sphere/skeleton.atlas", "images/monsters/theCity/sphere/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  60 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  61 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  62 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  63 */     this.stateData.setMix("Idle", "Attack", 0.1F);
/*  64 */     this.state.setTimeScale(0.8F);
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  69 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.BarricadePower(this)));
/*  70 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.ArtifactPower(this, 3)));
/*     */     
/*  72 */     AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, 40));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  77 */     switch (this.nextMove) {
/*     */     case 1: 
/*  79 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  80 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.4F));
/*  81 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  84 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY, true));
/*     */       
/*     */ 
/*  87 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  90 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  92 */       break;
/*     */     case 2: 
/*  94 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  95 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, 35));
/*     */       } else {
/*  97 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, 25));
/*     */       }
/*  99 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.2F));
/* 100 */       if (MathUtils.randomBoolean()) {
/* 101 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("SPHERE_DETECT_VO_1"));
/*     */       } else {
/* 103 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("SPHERE_DETECT_VO_2"));
/*     */       }
/* 105 */       break;
/*     */     case 3: 
/* 107 */       AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, 15));
/* 108 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction(this));
/* 109 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 112 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 114 */       break;
/*     */     case 4: 
/* 116 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 117 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 120 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/* 122 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 5, true), 5));
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 130 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 135 */     switch (key) {
/*     */     case "ATTACK": 
/* 137 */       this.state.setAnimation(0, "Attack", false);
/* 138 */       this.state.setTimeScale(0.8F);
/* 139 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 140 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 148 */     super.damage(info);
/* 149 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 150 */       this.state.setAnimation(0, "Hit", false);
/* 151 */       this.state.setTimeScale(0.8F);
/* 152 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 158 */     if (this.firstMove) {
/* 159 */       this.firstMove = false;
/* 160 */       setMove((byte)2, AbstractMonster.Intent.DEFEND);
/* 161 */       return;
/*     */     }
/*     */     
/* 164 */     if (this.secondMove) {
/* 165 */       this.secondMove = false;
/* 166 */       setMove((byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/* 167 */       return;
/*     */     }
/*     */     
/* 170 */     if (lastMove((byte)1)) {
/* 171 */       setMove((byte)3, AbstractMonster.Intent.ATTACK_DEFEND, ((DamageInfo)this.damage.get(0)).base);
/*     */     } else {
/* 173 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 179 */     super.die();
/* 180 */     if (MathUtils.randomBoolean()) {
/* 181 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("SPHERE_DETECT_VO_1"));
/*     */     } else {
/* 183 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("SPHERE_DETECT_VO_2"));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\SphericGuardian.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */