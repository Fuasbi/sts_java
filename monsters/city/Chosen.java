/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Chosen extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Chosen";
/*  27 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Chosen");
/*  28 */   public static final String NAME = monsterStrings.NAME;
/*  29 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  30 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final float IDLE_TIMESCALE = 0.8F;
/*     */   private static final int HP_MIN = 95;
/*     */   private static final int HP_MAX = 99;
/*     */   private static final int A_2_HP_MIN = 98;
/*     */   private static final int A_2_HP_MAX = 103;
/*     */   private static final float HB_X = 5.0F;
/*     */   private static final float HB_Y = -10.0F;
/*     */   private static final float HB_W = 200.0F;
/*     */   private static final float HB_H = 280.0F;
/*     */   private static final int ZAP_DMG = 18;
/*     */   private static final int A_2_ZAP_DMG = 21;
/*     */   private static final int DEBILITATE_DMG = 10;
/*     */   private static final int A_2_DEBILITATE_DMG = 12;
/*     */   private static final int POKE_DMG = 5;
/*     */   private static final int A_2_POKE_DMG = 6;
/*     */   private int zapDmg;
/*     */   private int debilitateDmg;
/*     */   private int pokeDmg;
/*     */   private static final int DEBILITATE_VULN = 2;
/*     */   private static final int DRAIN_STR = 3;
/*     */   private static final int DRAIN_WEAK = 3;
/*  52 */   private static final byte ZAP = 1; private static final byte DRAIN = 2; private static final byte DEBILITATE = 3; private static final byte HEX = 4; private static final byte POKE = 5; private static final int HEX_AMT = 1; private boolean firstTurn = true; private boolean usedHex = false;
/*     */   
/*     */   public Chosen() {
/*  55 */     this(0.0F, 0.0F);
/*     */   }
/*     */   
/*     */   public Chosen(float x, float y) {
/*  59 */     super(NAME, "Chosen", 99, 5.0F, -10.0F, 200.0F, 280.0F, null, x, -20.0F + y);
/*  60 */     this.dialogX = (-30.0F * Settings.scale);
/*  61 */     this.dialogY = (50.0F * Settings.scale);
/*     */     
/*  63 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  64 */       setHp(98, 103);
/*     */     } else {
/*  66 */       setHp(95, 99);
/*     */     }
/*     */     
/*  69 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  70 */       this.zapDmg = 21;
/*  71 */       this.debilitateDmg = 12;
/*  72 */       this.pokeDmg = 6;
/*     */     } else {
/*  74 */       this.zapDmg = 18;
/*  75 */       this.debilitateDmg = 10;
/*  76 */       this.pokeDmg = 5;
/*     */     }
/*     */     
/*  79 */     this.damage.add(new DamageInfo(this, this.zapDmg));
/*  80 */     this.damage.add(new DamageInfo(this, this.debilitateDmg));
/*  81 */     this.damage.add(new DamageInfo(this, this.pokeDmg));
/*     */     
/*  83 */     loadAnimation("images/monsters/theCity/chosen/skeleton.atlas", "images/monsters/theCity/chosen/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  87 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  88 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  89 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  90 */     this.stateData.setMix("Attack", "Idle", 0.2F);
/*  91 */     this.state.setTimeScale(0.8F);
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  96 */     switch (this.nextMove) {
/*     */     case 5: 
/*  98 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  99 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 102 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/* 104 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 107 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.SLASH_VERTICAL));
/*     */       
/* 109 */       break;
/*     */     case 1: 
/* 111 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.FastShakeAction(this, 0.3F, 0.5F));
/* 112 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 113 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.FIRE));
/* 114 */       break;
/*     */     case 2: 
/* 116 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 3, true), 3));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 122 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.StrengthPower(this, 3), 3));
/*     */       
/* 124 */       break;
/*     */     case 3: 
/* 126 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 127 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 130 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */       
/* 132 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */       break;
/*     */     case 4: 
/* 140 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[0]));
/* 141 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 142 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.2F));
/* 143 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.HexPower(AbstractDungeon.player, 1)));
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 150 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 155 */     switch (key) {
/*     */     case "ATTACK": 
/* 157 */       this.state.setAnimation(0, "Attack", false);
/* 158 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 159 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 167 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 169 */       if (!this.usedHex) {
/* 170 */         this.usedHex = true;
/* 171 */         setMove((byte)4, AbstractMonster.Intent.STRONG_DEBUFF);
/* 172 */         return;
/*     */       }
/*     */       
/* 175 */       if ((!lastMove((byte)3)) && (!lastMove((byte)2))) {
/* 176 */         if (num < 50) {
/* 177 */           setMove((byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 178 */           return;
/*     */         }
/* 180 */         setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/* 181 */         return;
/*     */       }
/*     */       
/*     */ 
/* 185 */       if (num < 40) {
/* 186 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 187 */         return;
/*     */       }
/* 189 */       setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(2)).base, 2, true);
/* 190 */       return;
/*     */     }
/*     */     
/*     */ 
/* 194 */     if (this.firstTurn) {
/* 195 */       this.firstTurn = false;
/* 196 */       setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(2)).base, 2, true);
/* 197 */       return;
/*     */     }
/*     */     
/*     */ 
/* 201 */     if (!this.usedHex) {
/* 202 */       this.usedHex = true;
/* 203 */       setMove((byte)4, AbstractMonster.Intent.STRONG_DEBUFF);
/* 204 */       return;
/*     */     }
/*     */     
/* 207 */     if ((!lastMove((byte)3)) && (!lastMove((byte)2))) {
/* 208 */       if (num < 50) {
/* 209 */         setMove((byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 210 */         return;
/*     */       }
/* 212 */       setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/* 213 */       return;
/*     */     }
/*     */     
/*     */ 
/* 217 */     if (num < 40) {
/* 218 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 219 */       return;
/*     */     }
/* 221 */     setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(2)).base, 2, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 231 */     super.damage(info);
/* 232 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 233 */       this.state.setAnimation(0, "Hit", false);
/* 234 */       this.state.setTimeScale(0.8F);
/* 235 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 241 */     super.die();
/* 242 */     CardCrawlGame.sound.play("CHOSEN_DEATH");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Chosen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */