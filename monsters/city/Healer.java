/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Healer extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Healer";
/*  24 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Healer");
/*  25 */   public static final String NAME = monsterStrings.NAME;
/*  26 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  27 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   private static final float IDLE_TIMESCALE = 0.8F;
/*     */   
/*     */   public static final String ENC_NAME = "HealerTank";
/*     */   private static final int HP_MIN = 48;
/*     */   private static final int HP_MAX = 56;
/*     */   private static final int A_2_HP_MIN = 50;
/*     */   private static final int A_2_HP_MAX = 58;
/*     */   private static final int MAGIC_DMG = 8;
/*     */   private static final int HEAL_AMT = 16;
/*     */   
/*     */   public Healer(float x, float y)
/*     */   {
/*  41 */     super(NAME, "Healer", 56, 0.0F, -20.0F, 230.0F, 250.0F, null, x, y);
/*     */     
/*  43 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  44 */       setHp(50, 58);
/*     */     } else {
/*  46 */       setHp(48, 56);
/*     */     }
/*     */     
/*  49 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  50 */       this.magicDmg = 9;
/*  51 */       this.strAmt = 4;
/*  52 */       this.healAmt = 20;
/*  53 */     } else if (AbstractDungeon.ascensionLevel >= 2) {
/*  54 */       this.magicDmg = 9;
/*  55 */       this.strAmt = 3;
/*  56 */       this.healAmt = 16;
/*     */     } else {
/*  58 */       this.magicDmg = 8;
/*  59 */       this.strAmt = 2;
/*  60 */       this.healAmt = 16;
/*     */     }
/*     */     
/*  63 */     this.damage.add(new DamageInfo(this, this.magicDmg));
/*     */     
/*  65 */     loadAnimation("images/monsters/theCity/healer/skeleton.atlas", "images/monsters/theCity/healer/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  69 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  70 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  71 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  72 */     this.state.setTimeScale(0.8F);
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  77 */     switch (this.nextMove) {
/*     */     case 1: 
/*  79 */       playSfx();
/*  80 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  81 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  84 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/*  86 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  92 */       break;
/*     */     case 2: 
/*  94 */       playSfx();
/*  95 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "STAFF_RAISE"));
/*  96 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.25F));
/*  97 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  98 */         if ((!m.isDying) && (!m.isEscaping)) {
/*  99 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(m, this, this.healAmt));
/*     */         }
/*     */       }
/* 102 */       break;
/*     */     case 3: 
/* 104 */       playSfx();
/* 105 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "STAFF_RAISE"));
/* 106 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.25F));
/* 107 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 108 */         if ((!m.isDying) && (!m.isEscaping)) {
/* 109 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new com.megacrit.cardcrawl.powers.StrengthPower(m, this.strAmt), this.strAmt));
/*     */         }
/*     */       }
/*     */       
/* 113 */       break;
/*     */     }
/*     */     
/*     */     
/* 117 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 121 */     if (MathUtils.randomBoolean()) {
/* 122 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_HEALER_1A"));
/*     */     } else {
/* 124 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_HEALER_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 129 */     int roll = MathUtils.random(2);
/* 130 */     if (roll == 0) {
/* 131 */       CardCrawlGame.sound.play("VO_HEALER_2A");
/* 132 */     } else if (roll == 1) {
/* 133 */       CardCrawlGame.sound.play("VO_HEALER_2B");
/*     */     } else {
/* 135 */       CardCrawlGame.sound.play("VO_HEALER_2C");
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 141 */     switch (key) {
/*     */     case "STAFF_RAISE": 
/* 143 */       this.state.setAnimation(0, "Attack", false);
/* 144 */       this.state.setTimeScale(0.8F);
/* 145 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 146 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 154 */     int needToHeal = 0;
/*     */     
/* 156 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 157 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 158 */         needToHeal += m.maxHealth - m.currentHealth;
/*     */       }
/*     */     }
/*     */     
/* 162 */     if (AbstractDungeon.ascensionLevel >= 17) {
/* 163 */       if ((needToHeal > 20) && (!lastTwoMoves((byte)2))) {
/* 164 */         setMove((byte)2, AbstractMonster.Intent.BUFF);
/*     */       }
/*     */       
/*     */     }
/* 168 */     else if ((needToHeal > 15) && (!lastTwoMoves((byte)2))) {
/* 169 */       setMove((byte)2, AbstractMonster.Intent.BUFF);
/* 170 */       return;
/*     */     }
/*     */     
/*     */ 
/* 174 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 176 */       if ((num >= 40) && (!lastMove((byte)1))) {
/* 177 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 182 */     else if ((num >= 40) && (!lastTwoMoves((byte)1))) {
/* 183 */       setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/* 184 */       return;
/*     */     }
/*     */     
/*     */ 
/* 188 */     if (!lastTwoMoves((byte)3)) {
/* 189 */       setMove((byte)3, AbstractMonster.Intent.BUFF);
/* 190 */       return;
/*     */     }
/* 192 */     setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base); }
/*     */   
/*     */   private static final int STR_AMOUNT = 2;
/*     */   private static final int A_2_MAGIC_DMG = 9;
/*     */   private static final int A_2_STR_AMOUNT = 3;
/*     */   private int magicDmg;
/*     */   
/* 199 */   public void damage(DamageInfo info) { super.damage(info);
/* 200 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 201 */       this.state.setAnimation(0, "Hit", false);
/* 202 */       this.state.setTimeScale(0.8F);
/* 203 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     } }
/*     */   
/*     */   private int strAmt;
/*     */   private int healAmt;
/*     */   
/* 209 */   public void die() { playDeathSfx();
/* 210 */     this.state.setTimeScale(0.1F);
/* 211 */     useShakeAnimation(5.0F);
/* 212 */     super.die();
/*     */   }
/*     */   
/*     */   private static final byte ATTACK = 1;
/*     */   private static final byte HEAL = 2;
/*     */   private static final byte BUFF = 3;
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Healer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */