/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BronzeOrb extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "BronzeOrb";
/*  23 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("BronzeOrb");
/*  24 */   public static final String NAME = monsterStrings.NAME;
/*  25 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  26 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 52;
/*     */   private static final int HP_MAX = 58;
/*     */   private static final int A_2_HP_MIN = 54;
/*     */   private static final int A_2_HP_MAX = 60;
/*     */   private static final int BEAM_DMG = 8;
/*     */   private static final int BLOCK_AMT = 12;
/*  33 */   private static final byte BEAM = 1; private static final byte SUPPORT_BEAM = 2; private static final byte STASIS = 3; private boolean usedStasis = false;
/*     */   private int count;
/*     */   
/*     */   public BronzeOrb(float x, float y, int count) {
/*  37 */     super(NAME, "BronzeOrb", AbstractDungeon.monsterHpRng
/*     */     
/*     */ 
/*  40 */       .random(52, 58), 0.0F, 0.0F, 160.0F, 160.0F, "images/monsters/theCity/automaton/orb.png", x, y);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  49 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  50 */       setHp(54, 60);
/*     */     } else {
/*  52 */       setHp(52, 58);
/*     */     }
/*     */     
/*  55 */     this.count = count;
/*  56 */     this.damage.add(new DamageInfo(this, 8));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  61 */     switch (this.nextMove) {
/*     */     case 1: 
/*  63 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("ATTACK_MAGIC_BEAM_SHORT", 0.5F));
/*  64 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.BorderFlashEffect(Color.SKY)));
/*  65 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.SmallLaserEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, this.hb.cX, this.hb.cY), 0.3F));
/*     */       
/*     */ 
/*     */ 
/*  69 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*  70 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*  71 */       break;
/*     */     case 2: 
/*  73 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(
/*  74 */         AbstractDungeon.getMonsters().getMonster("BronzeAutomaton"), this, 12));
/*  75 */       break;
/*     */     case 3: 
/*  77 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.ApplyStasisAction(this));
/*     */     }
/*     */     
/*  80 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  85 */     super.update();
/*  86 */     if (this.count % 2 == 0) {
/*  87 */       this.animY = (MathUtils.cosDeg((float)(System.currentTimeMillis() / 6L % 360L)) * 6.0F * Settings.scale);
/*     */     } else {
/*  89 */       this.animY = (-MathUtils.cosDeg((float)(System.currentTimeMillis() / 6L % 360L)) * 6.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 114 */     if ((!this.usedStasis) && (num >= 25)) {
/* 115 */       setMove((byte)3, AbstractMonster.Intent.STRONG_DEBUFF);
/* 116 */       this.usedStasis = true;
/* 117 */       return;
/*     */     }
/*     */     
/* 120 */     if ((num >= 70) && (!lastTwoMoves((byte)2))) {
/* 121 */       setMove((byte)2, AbstractMonster.Intent.DEFEND);
/* 122 */       return; }
/* 123 */     if (!lastTwoMoves((byte)1)) {
/* 124 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, 8);
/* 125 */       return;
/*     */     }
/*     */     
/* 128 */     setMove((byte)2, AbstractMonster.Intent.DEFEND);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\BronzeOrb.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */