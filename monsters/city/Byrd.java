/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.FlightPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Byrd extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Byrd";
/*  26 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Byrd");
/*  27 */   public static final String NAME = monsterStrings.NAME;
/*  28 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  29 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final String THREE_BYRDS = "3_Byrds";
/*     */   public static final String FOUR_BYRDS = "4_Byrds";
/*     */   public static final String IMAGE = "images/monsters/theCity/byrdFlying.png";
/*     */   private static final int HP_MIN = 25;
/*     */   private static final int HP_MAX = 31;
/*     */   private static final int A_2_HP_MIN = 26;
/*     */   private static final int A_2_HP_MAX = 33;
/*     */   private static final float HB_X_F = 0.0F;
/*     */   private static final float HB_X_G = 10.0F;
/*     */   private static final float HB_Y_F = 50.0F;
/*     */   private static final float HB_Y_G = -50.0F;
/*     */   private static final float HB_W = 240.0F;
/*     */   private static final float HB_H = 180.0F;
/*     */   private static final int PECK_DMG = 1;
/*     */   private static final int PECK_COUNT = 5;
/*     */   private static final int SWOOP_DMG = 12;
/*     */   private static final int A_2_PECK_COUNT = 6;
/*     */   private static final int A_2_SWOOP_DMG = 14;
/*  48 */   private int peckDmg; private int peckCount; private int swoopDmg; private int flightAmt; private static final int HEADBUTT_DMG = 3; private static final int CAW_STR = 1; private static final byte PECK = 1; private static final byte GO_AIRBORNE = 2; private static final byte SWOOP = 3; private static final byte STUNNED = 4; private static final byte HEADBUTT = 5; private static final byte CAW = 6; private boolean firstMove = true; private boolean isFlying = true;
/*     */   public static final String FLY_STATE = "FLYING";
/*     */   public static final String GROUND_STATE = "GROUNDED";
/*     */   
/*  52 */   public Byrd(float x, float y) { super(NAME, "Byrd", 31, 0.0F, 50.0F, 240.0F, 180.0F, null, x, y);
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  55 */       setHp(26, 33);
/*     */     } else {
/*  57 */       setHp(25, 31);
/*     */     }
/*     */     
/*  60 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  61 */       this.flightAmt = 4;
/*     */     } else {
/*  63 */       this.flightAmt = 3;
/*     */     }
/*     */     
/*  66 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  67 */       this.peckDmg = 1;
/*  68 */       this.peckCount = 6;
/*  69 */       this.swoopDmg = 14;
/*     */     } else {
/*  71 */       this.peckDmg = 1;
/*  72 */       this.peckCount = 5;
/*  73 */       this.swoopDmg = 12;
/*     */     }
/*     */     
/*  76 */     this.damage.add(new DamageInfo(this, this.peckDmg));
/*  77 */     this.damage.add(new DamageInfo(this, this.swoopDmg));
/*  78 */     this.damage.add(new DamageInfo(this, 3));
/*     */     
/*  80 */     loadAnimation("images/monsters/theCity/byrd/flying.atlas", "images/monsters/theCity/byrd/flying.json", 1.0F);
/*  81 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle_flap", true);
/*  82 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  87 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new FlightPower(this, this.flightAmt)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  92 */     switch (this.nextMove) {
/*     */     case 1: 
/*  94 */       AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/*  95 */       for (int i = 0; i < this.peckCount; i++) {
/*  96 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/*  99 */           (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_LIGHT, true));
/*     */       }
/*     */       
/*     */ 
/* 103 */       break;
/*     */     case 5: 
/* 105 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 106 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 109 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 111 */       setMove((byte)2, AbstractMonster.Intent.UNKNOWN);
/* 112 */       return;
/*     */     case 2: 
/* 114 */       this.isFlying = true;
/* 115 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "FLYING"));
/* 116 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new FlightPower(this, this.flightAmt)));
/*     */       
/* 118 */       break;
/*     */     case 6: 
/* 120 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.TalkAction(this, DIALOG[0], 1.2F, 1.2F));
/* 121 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.StrengthPower(this, 1), 1));
/*     */       
/* 123 */       break;
/*     */     case 3: 
/* 125 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 126 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 129 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */       
/* 131 */       break;
/*     */     case 4: 
/* 133 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.SetAnimationAction(this, "head_lift"));
/* 134 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction(this, com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType.STUNNED));
/*     */     }
/*     */     
/* 137 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */ 
/*     */   public void changeState(String stateName)
/*     */   {
/* 143 */     switch (stateName) {
/*     */     case "FLYING": 
/* 145 */       loadAnimation("images/monsters/theCity/byrd/flying.atlas", "images/monsters/theCity/byrd/flying.json", 1.0F);
/*     */       
/*     */ 
/*     */ 
/* 149 */       AnimationState.TrackEntry e = this.state.setAnimation(0, "idle_flap", true);
/* 150 */       e.setTime(e.getEndTime() * MathUtils.random());
/* 151 */       updateHitbox(0.0F, 50.0F, 240.0F, 180.0F);
/* 152 */       break;
/*     */     case "GROUNDED": 
/* 154 */       setMove((byte)4, AbstractMonster.Intent.STUN);
/* 155 */       createIntent();
/* 156 */       this.isFlying = false;
/* 157 */       loadAnimation("images/monsters/theCity/byrd/grounded.atlas", "images/monsters/theCity/byrd/grounded.json", 1.0F);
/*     */       
/*     */ 
/*     */ 
/* 161 */       AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 162 */       e.setTime(e.getEndTime() * MathUtils.random());
/* 163 */       updateHitbox(10.0F, -50.0F, 240.0F, 180.0F);
/* 164 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 175 */     if (this.firstMove) {
/* 176 */       this.firstMove = false;
/* 177 */       if (AbstractDungeon.aiRng.randomBoolean(0.375F)) {
/* 178 */         setMove((byte)6, AbstractMonster.Intent.BUFF);
/*     */       } else {
/* 180 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.peckCount, true);
/*     */       }
/* 182 */       return;
/*     */     }
/*     */     
/* 185 */     if (this.isFlying)
/*     */     {
/*     */ 
/* 188 */       if (num < 50)
/*     */       {
/* 190 */         if (lastTwoMoves((byte)1)) {
/* 191 */           if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 192 */             setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */           } else {
/* 194 */             setMove((byte)6, AbstractMonster.Intent.BUFF);
/*     */           }
/*     */         } else {
/* 197 */           setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.peckCount, true);
/*     */         }
/*     */         
/*     */       }
/* 201 */       else if (num < 70)
/*     */       {
/* 203 */         if (lastMove((byte)3)) {
/* 204 */           if (AbstractDungeon.aiRng.randomBoolean(0.375F)) {
/* 205 */             setMove((byte)6, AbstractMonster.Intent.BUFF);
/*     */           } else {
/* 207 */             setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.peckCount, true);
/*     */           }
/*     */         } else {
/* 210 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 215 */       else if (lastMove((byte)6)) {
/* 216 */         if (AbstractDungeon.aiRng.randomBoolean(0.2857F)) {
/* 217 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 219 */           setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.peckCount, true);
/*     */         }
/*     */       } else {
/* 222 */         setMove((byte)6, AbstractMonster.Intent.BUFF);
/*     */       }
/*     */       
/*     */     }
/*     */     else {
/* 227 */       setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(2)).base);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 233 */     super.die();
/* 234 */     CardCrawlGame.sound.play("BYRD_DEATH");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\Byrd.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */