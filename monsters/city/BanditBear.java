/*     */ package com.megacrit.cardcrawl.monsters.city;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.DexterityPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BanditBear extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "BanditBear";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("BanditBear");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final int HP_MIN = 38;
/*     */   
/*     */   public static final int HP_MAX = 42;
/*     */   
/*     */   public static final int A_2_HP_MIN = 40;
/*     */   public static final int A_2_HP_MAX = 44;
/*     */   private static final int MAUL_DMG = 18;
/*     */   private static final int A_2_MAUL_DMG = 20;
/*     */   private static final int LUNGE_DMG = 9;
/*     */   private static final int A_2_LUNGE_DMG = 10;
/*     */   
/*     */   public BanditBear(float x, float y)
/*     */   {
/*  40 */     super(NAME, "BanditBear", 42, -5.0F, -4.0F, 180.0F, 280.0F, null, x, y);
/*     */     
/*  42 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  43 */       setHp(40, 44);
/*     */     } else {
/*  45 */       setHp(38, 42);
/*     */     }
/*     */     
/*  48 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  49 */       this.maulDmg = 20;
/*  50 */       this.lungeDmg = 10;
/*     */     } else {
/*  52 */       this.maulDmg = 18;
/*  53 */       this.lungeDmg = 9;
/*     */     }
/*     */     
/*  56 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  57 */       this.con_reduction = -4;
/*     */     } else {
/*  59 */       this.con_reduction = -2;
/*     */     }
/*     */     
/*  62 */     this.damage.add(new DamageInfo(this, this.maulDmg));
/*  63 */     this.damage.add(new DamageInfo(this, this.lungeDmg));
/*     */     
/*  65 */     loadAnimation("images/monsters/theCity/bear/skeleton.atlas", "images/monsters/theCity/bear/skeleton.json", 1.0F);
/*  66 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  67 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  68 */     this.stateData.setMix("Hit", "Idle", 0.2F);
/*  69 */     this.state.setTimeScale(1.0F);
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  74 */     switch (this.nextMove) {
/*     */     case 2: 
/*  76 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  77 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new DexterityPower(AbstractDungeon.player, this.con_reduction), this.con_reduction));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  83 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ATTACK_DEFEND, 
/*  84 */         ((DamageInfo)this.damage.get(1)).base));
/*  85 */       break;
/*     */     case 1: 
/*  87 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "MAUL"));
/*  88 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.3F));
/*  89 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  92 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  94 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ATTACK_DEFEND, 
/*  95 */         ((DamageInfo)this.damage.get(1)).base));
/*  96 */       break;
/*     */     case 3: 
/*  98 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  99 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 102 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/* 104 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, 9));
/* 105 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, 
/* 106 */         ((DamageInfo)this.damage.get(0)).base));
/* 107 */       break; }
/*     */   }
/*     */   
/*     */   private static final int LUNGE_DEFENSE = 9;
/*     */   private static final int CON_AMT = -2;
/*     */   private static final int A_17_CON_AMT = -4;
/*     */   private int maulDmg;
/*     */   
/* 115 */   public void changeState(String key) { switch (key) {
/*     */     case "MAUL": 
/* 117 */       this.state.setAnimation(0, "Attack", false);
/* 118 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 119 */       break; }
/*     */   }
/*     */   
/*     */   private int lungeDmg;
/*     */   private int con_reduction;
/*     */   private static final byte MAUL = 1;
/*     */   private static final byte BEAR_HUG = 2;
/*     */   private static final byte LUNGE = 3;
/* 127 */   public void damage(DamageInfo info) { super.damage(info);
/* 128 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 129 */       this.state.setAnimation(0, "Hit", false);
/* 130 */       this.state.setTimeScale(1.0F);
/* 131 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 137 */     super.die();
/* 138 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 139 */       if ((!m.isDead) && (!m.isDying)) {
/* 140 */         m.deathReact();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 147 */     setMove((byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\city\BanditBear.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */