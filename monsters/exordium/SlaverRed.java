/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.VulnerablePower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SlaverRed extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SlaverRed";
/*  22 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("SlaverRed");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 46;
/*     */   private static final int HP_MAX = 50;
/*     */   private static final int A_2_HP_MIN = 48;
/*     */   private static final int A_2_HP_MAX = 52;
/*     */   private static final int STAB_DMG = 13;
/*     */   private static final int A_2_STAB_DMG = 14;
/*     */   private static final int SCRAPE_DMG = 8;
/*     */   private static final int A_2_SCRAPE_DMG = 9;
/*     */   private int stabDmg;
/*  35 */   private int scrapeDmg; private int VULN_AMT = 1;
/*     */   private static final byte STAB = 1;
/*  37 */   private static final byte ENTANGLE = 2; private static final byte SCRAPE = 3; private static final String SCRAPE_NAME = MOVES[0]; private static final String ENTANGLE_NAME = MOVES[1];
/*  38 */   private boolean usedEntangle = false; private boolean firstTurn = true;
/*     */   
/*     */   public SlaverRed(float x, float y) {
/*  41 */     super(NAME, "SlaverRed", 50, 0.0F, 0.0F, 170.0F, 230.0F, null, x, y);
/*     */     
/*  43 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  44 */       setHp(48, 52);
/*     */     } else {
/*  46 */       setHp(46, 50);
/*     */     }
/*     */     
/*  49 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  50 */       this.stabDmg = 14;
/*  51 */       this.scrapeDmg = 9;
/*     */     } else {
/*  53 */       this.stabDmg = 13;
/*  54 */       this.scrapeDmg = 8;
/*     */     }
/*     */     
/*  57 */     this.damage.add(new DamageInfo(this, this.stabDmg));
/*  58 */     this.damage.add(new DamageInfo(this, this.scrapeDmg));
/*     */     
/*  60 */     loadAnimation("images/monsters/theBottom/redSlaver/skeleton.atlas", "images/monsters/theBottom/redSlaver/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  64 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  65 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  66 */     this.firstTurn = true;
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  71 */     switch (this.nextMove) {
/*     */     case 2: 
/*  73 */       playSfx();
/*  74 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "Use Net"));
/*  75 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.EntanglePower(AbstractDungeon.player)));
/*     */       
/*  77 */       this.usedEntangle = true;
/*  78 */       break;
/*     */     case 1: 
/*  80 */       playSfx();
/*  81 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  82 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  85 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/*  87 */       break;
/*     */     case 3: 
/*  89 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  90 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  93 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/*  95 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  96 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new VulnerablePower(AbstractDungeon.player, this.VULN_AMT + 1, true), this.VULN_AMT + 1));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 103 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new VulnerablePower(AbstractDungeon.player, this.VULN_AMT, true), this.VULN_AMT));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 110 */       break;
/*     */     }
/*     */     
/*     */     
/* 114 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 118 */     int roll = MathUtils.random(1);
/* 119 */     if (roll == 0) {
/* 120 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("VO_SLAVERRED_1A"));
/*     */     } else {
/* 122 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("VO_SLAVERRED_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 127 */     int roll = MathUtils.random(1);
/* 128 */     if (roll == 0) {
/* 129 */       CardCrawlGame.sound.play("VO_SLAVERRED_2A");
/*     */     } else {
/* 131 */       CardCrawlGame.sound.play("VO_SLAVERRED_2B");
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 137 */     float tmp = this.state.getCurrent(0).getTime();
/* 138 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idleNoNet", true);
/* 139 */     e.setTime(tmp);
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 144 */     if (this.firstTurn) {
/* 145 */       this.firstTurn = false;
/* 146 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, this.stabDmg);
/* 147 */       return;
/*     */     }
/*     */     
/*     */ 
/* 151 */     if ((num >= 75) && (!this.usedEntangle)) {
/* 152 */       setMove(ENTANGLE_NAME, (byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/* 153 */       return;
/*     */     }
/*     */     
/*     */ 
/* 157 */     if ((num >= 55) && (this.usedEntangle) && (!lastTwoMoves((byte)1))) {
/* 158 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 159 */       return;
/*     */     }
/*     */     
/* 162 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 164 */       if (!lastMove((byte)3)) {
/* 165 */         setMove(SCRAPE_NAME, (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 166 */         return;
/*     */       }
/* 168 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 169 */       return;
/*     */     }
/*     */     
/*     */ 
/* 173 */     if (!lastTwoMoves((byte)3)) {
/* 174 */       setMove(SCRAPE_NAME, (byte)3, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 175 */       return;
/*     */     }
/* 177 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 185 */     super.die();
/* 186 */     playDeathSfx();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\SlaverRed.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */