/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.powers.ModeShiftPower;
/*     */ import com.megacrit.cardcrawl.powers.SharpHidePower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class TheGuardian extends AbstractMonster
/*     */ {
/*  35 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(TheGuardian.class.getName());
/*     */   public static final String ID = "TheGuardian";
/*  37 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("TheGuardian");
/*  38 */   public static final String NAME = monsterStrings.NAME;
/*  39 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  40 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   private static final String DEFENSIVE_MODE = "Defensive Mode";
/*     */   
/*     */   private static final String OFFENSIVE_MODE = "Offensive Mode";
/*     */   
/*     */   private static final String RESET_THRESH = "Reset Threshold";
/*     */   public static final int HP = 240;
/*     */   public static final int A_2_HP = 250;
/*     */   private static final int DMG_THRESHOLD = 30;
/*     */   private static final int A_2_DMG_THRESHOLD = 35;
/*     */   private static final int A_19_DMG_THRESHOLD = 40;
/*     */   private int dmgThreshold;
/*  53 */   private int dmgThresholdIncrease = 10;
/*     */   private int dmgTaken;
/*     */   private static final int FIERCE_BASH_DMG = 32;
/*     */   private static final int A_2_FIERCE_BASH_DMG = 36;
/*     */   private static final int ROLL_DMG = 9;
/*     */   private static final int A_2_ROLL_DMG = 10;
/*     */   private int fierceBashDamage;
/*  60 */   private int whirlwindDamage = 5; private int twinSlamDamage = 8; private int rollDamage; private int whirlwindCount = 4; private int DEFENSIVE_BLOCK = 20;
/*     */   
/*  62 */   private int blockAmount = 9; private int thornsDamage = 3; private int VENT_DEBUFF = 2;
/*  63 */   private boolean isOpen = true;
/*  64 */   private boolean closeUpTriggered = false;
/*     */   private static final byte CLOSE_UP = 1;
/*     */   private static final byte FIERCE_BASH = 2;
/*  67 */   private static final byte ROLL_ATTACK = 3; private static final byte TWIN_SLAM = 4; private static final byte WHIRLWIND = 5; private static final byte CHARGE_UP = 6; private static final byte VENT_STEAM = 7; private static final String CLOSEUP_NAME = MOVES[0]; private static final String FIERCEBASH_NAME = MOVES[1]; private static final String TWINSLAM_NAME = MOVES[3];
/*  68 */   private static final String WHIRLWIND_NAME = MOVES[4]; private static final String CHARGEUP_NAME = MOVES[5]; private static final String VENTSTEAM_NAME = MOVES[6];
/*     */   
/*     */   public TheGuardian() {
/*  71 */     super(NAME, "TheGuardian", 240, 0.0F, 95.0F, 440.0F, 350.0F, null, -50.0F, -100.0F);
/*  72 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  73 */     this.dialogX = (-100.0F * Settings.scale);
/*  74 */     this.dialogY = (50.0F * Settings.scale);
/*     */     
/*  76 */     if (AbstractDungeon.ascensionLevel >= 19) {
/*  77 */       setHp(250);
/*  78 */       this.dmgThreshold = 40;
/*  79 */     } else if (AbstractDungeon.ascensionLevel >= 9) {
/*  80 */       setHp(250);
/*  81 */       this.dmgThreshold = 35;
/*     */     } else {
/*  83 */       setHp(240);
/*  84 */       this.dmgThreshold = 30;
/*     */     }
/*     */     
/*  87 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  88 */       this.fierceBashDamage = 36;
/*  89 */       this.rollDamage = 10;
/*     */     } else {
/*  91 */       this.fierceBashDamage = 32;
/*  92 */       this.rollDamage = 9;
/*     */     }
/*     */     
/*  95 */     this.damage.add(new DamageInfo(this, this.fierceBashDamage));
/*  96 */     this.damage.add(new DamageInfo(this, this.rollDamage));
/*  97 */     this.damage.add(new DamageInfo(this, this.whirlwindDamage));
/*  98 */     this.damage.add(new DamageInfo(this, this.twinSlamDamage));
/*     */     
/* 100 */     loadAnimation("images/monsters/theBottom/boss/guardian/skeleton.atlas", "images/monsters/theBottom/boss/guardian/skeleton.json", 2.0F);
/*     */     
/*     */ 
/*     */ 
/* 104 */     this.state.setAnimation(0, "idle", true);
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/* 109 */     CardCrawlGame.music.unsilenceBGM();
/* 110 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 111 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BOTTOM");
/* 112 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ModeShiftPower(this, this.dmgThreshold)));
/*     */     
/* 114 */     AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Reset Threshold"));
/* 115 */     UnlockTracker.markBossAsSeen("GUARDIAN");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 120 */     switch (this.nextMove) {
/*     */     case 1: 
/* 122 */       useCloseUp();
/* 123 */       break;
/*     */     case 2: 
/* 125 */       useFierceBash();
/* 126 */       break;
/*     */     case 7: 
/* 128 */       useVentSteam();
/* 129 */       break;
/*     */     case 3: 
/* 131 */       useRollAttack();
/* 132 */       break;
/*     */     case 4: 
/* 134 */       useTwinSmash();
/* 135 */       break;
/*     */     case 5: 
/* 137 */       useWhirlwind();
/* 138 */       break;
/*     */     case 6: 
/* 140 */       useChargeUp();
/* 141 */       break;
/*     */     default: 
/* 143 */       logger.info("ERROR");
/*     */     }
/*     */   }
/*     */   
/*     */   private void useFierceBash()
/*     */   {
/* 149 */     AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 150 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 151 */       (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/* 152 */     setMove(VENTSTEAM_NAME, (byte)7, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */   }
/*     */   
/*     */   private void useVentSteam() {
/* 156 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, this.VENT_DEBUFF, true), this.VENT_DEBUFF));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 162 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, this.VENT_DEBUFF, true), this.VENT_DEBUFF));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 168 */     setMove(WHIRLWIND_NAME, (byte)5, AbstractMonster.Intent.ATTACK, this.whirlwindDamage, this.whirlwindCount, true);
/*     */   }
/*     */   
/*     */   private void useCloseUp() {
/* 172 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction(this, DIALOG[1]));
/* 173 */     if (AbstractDungeon.ascensionLevel >= 19) {
/* 174 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new SharpHidePower(this, this.thornsDamage + 1)));
/*     */     }
/*     */     else {
/* 177 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new SharpHidePower(this, this.thornsDamage)));
/*     */     }
/*     */     
/* 180 */     setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */   }
/*     */   
/*     */   private void useTwinSmash() {
/* 184 */     AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Offensive Mode"));
/* 185 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 186 */       (DamageInfo)this.damage.get(3), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/* 187 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 188 */       (DamageInfo)this.damage.get(3), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/* 189 */     AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this, this, "Sharp Hide"));
/* 190 */     setMove(WHIRLWIND_NAME, (byte)5, AbstractMonster.Intent.ATTACK, this.whirlwindDamage, this.whirlwindCount, true);
/*     */   }
/*     */   
/*     */   private void useRollAttack() {
/* 194 */     AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 195 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 196 */       (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/* 197 */     setMove(TWINSLAM_NAME, (byte)4, AbstractMonster.Intent.ATTACK_BUFF, this.twinSlamDamage, 2, true);
/*     */   }
/*     */   
/*     */   private void useWhirlwind() {
/* 201 */     AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 202 */     for (int i = 0; i < this.whirlwindCount; i++) {
/* 203 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("ATTACK_HEAVY"));
/* 204 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.CleaveEffect(true), 0.15F));
/* 205 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 206 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.NONE, true));
/*     */     }
/*     */     
/* 209 */     setMove(CHARGEUP_NAME, (byte)6, AbstractMonster.Intent.DEFEND);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void useChargeUp()
/*     */   {
/* 216 */     AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, this.blockAmount));
/* 217 */     AbstractDungeon.actionManager.addToBottom(new SFXAction("MONSTER_GUARDIAN_DESTROY"));
/* 218 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.TalkAction(this, DIALOG[2], 1.0F, 2.5F));
/*     */     
/* 220 */     setMove(FIERCEBASH_NAME, (byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 225 */     if (this.isOpen) {
/* 226 */       setMove(CHARGEUP_NAME, (byte)6, AbstractMonster.Intent.DEFEND);
/*     */     } else {
/* 228 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void changeState(String stateName)
/*     */   {
/* 237 */     switch (stateName) {
/*     */     case "Defensive Mode": 
/* 239 */       AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this, this, "Mode Shift"));
/*     */       
/* 241 */       CardCrawlGame.sound.play("GUARDIAN_ROLL_UP");
/* 242 */       AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, this.DEFENSIVE_BLOCK));
/* 243 */       this.stateData.setMix("idle", "transition", 0.1F);
/* 244 */       this.state.setTimeScale(2.0F);
/* 245 */       this.state.setAnimation(0, "transition", false);
/* 246 */       this.state.addAnimation(0, "defensive", true, 0.0F);
/* 247 */       this.dmgThreshold += this.dmgThresholdIncrease;
/* 248 */       setMove(CLOSEUP_NAME, (byte)1, AbstractMonster.Intent.BUFF);
/* 249 */       createIntent();
/* 250 */       this.isOpen = false;
/* 251 */       updateHitbox(0.0F, 95.0F, 440.0F, 250.0F);
/* 252 */       healthBarUpdatedEvent();
/* 253 */       break;
/*     */     case "Offensive Mode": 
/* 255 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ModeShiftPower(this, this.dmgThreshold)));
/*     */       
/* 257 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Reset Threshold"));
/* 258 */       if (this.currentBlock != 0) {
/* 259 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.LoseBlockAction(this, this, this.currentBlock));
/*     */       }
/* 261 */       this.stateData.setMix("defensive", "idle", 0.2F);
/* 262 */       this.state.setTimeScale(1.0F);
/* 263 */       this.state.setAnimation(0, "idle", true);
/* 264 */       this.isOpen = true;
/* 265 */       this.closeUpTriggered = false;
/* 266 */       updateHitbox(0.0F, 95.0F, 440.0F, 350.0F);
/* 267 */       healthBarUpdatedEvent();
/* 268 */       break;
/*     */     case "Reset Threshold": 
/* 270 */       this.dmgTaken = 0;
/* 271 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 283 */     int tmpHealth = this.currentHealth;
/* 284 */     super.damage(info);
/*     */     
/* 286 */     if ((this.isOpen) && (!this.closeUpTriggered) && 
/* 287 */       (tmpHealth > this.currentHealth) && (!this.isDying)) {
/* 288 */       this.dmgTaken += tmpHealth - this.currentHealth;
/* 289 */       if (getPower("Mode Shift") != null) {
/* 290 */         getPower("Mode Shift").amount -= tmpHealth - this.currentHealth;
/* 291 */         getPower("Mode Shift").updateDescription();
/*     */       }
/*     */       
/* 294 */       if (this.dmgTaken >= this.dmgThreshold) {
/* 295 */         this.dmgTaken = 0;
/* 296 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.IntenseZoomEffect(this.hb.cX, this.hb.cY, false), 0.05F, true));
/*     */         
/* 298 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Defensive Mode"));
/* 299 */         this.closeUpTriggered = true;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 310 */     super.render(sb);
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 315 */     useFastShakeAnimation(5.0F);
/* 316 */     CardCrawlGame.screenShake.rumble(4.0F);
/* 317 */     this.deathTimer += 1.5F;
/* 318 */     super.die();
/* 319 */     onBossVictoryLogic();
/* 320 */     UnlockTracker.hardUnlockOverride("GUARDIAN");
/* 321 */     UnlockTracker.unlockAchievement("GUARDIAN");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\TheGuardian.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */