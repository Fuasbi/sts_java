/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.CurlUpPower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class LouseNormal extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "FuzzyLouseNormal";
/*     */   public static final String THREE_LOUSE = "ThreeLouse";
/*  23 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("FuzzyLouseNormal");
/*  24 */   public static final String NAME = monsterStrings.NAME;
/*  25 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  26 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 10;
/*     */   private static final int HP_MAX = 15;
/*     */   private static final int A_2_HP_MIN = 11;
/*     */   private static final int A_2_HP_MAX = 16;
/*     */   private static final byte BITE = 3;
/*  32 */   private static final byte STRENGTHEN = 4; private boolean isOpen = true;
/*     */   private static final String CLOSED_STATE = "CLOSED";
/*     */   private static final String OPEN_STATE = "OPEN";
/*     */   private static final String REAR_IDLE = "REAR_IDLE";
/*     */   private int biteDamage;
/*     */   private static final int STR_AMOUNT = 3;
/*     */   
/*  39 */   public LouseNormal(float x, float y) { super(NAME, "FuzzyLouseNormal", 15, 0.0F, -5.0F, 180.0F, 140.0F, null, x, y);
/*     */     
/*  41 */     loadAnimation("images/monsters/theBottom/louseRed/skeleton.atlas", "images/monsters/theBottom/louseRed/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  45 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  46 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*     */     
/*  48 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  49 */       setHp(11, 16);
/*     */     } else {
/*  51 */       setHp(10, 15);
/*     */     }
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  55 */       this.biteDamage = AbstractDungeon.monsterHpRng.random(6, 8);
/*     */     } else {
/*  57 */       this.biteDamage = AbstractDungeon.monsterHpRng.random(5, 7);
/*     */     }
/*     */     
/*  60 */     this.damage.add(new DamageInfo(this, this.biteDamage));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  65 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  66 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  67 */         .random(9, 12))));
/*  68 */     } else if (AbstractDungeon.ascensionLevel >= 7) {
/*  69 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  70 */         .random(4, 8))));
/*     */     } else {
/*  72 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  73 */         .random(3, 7))));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  79 */     switch (this.nextMove) {
/*     */     case 3: 
/*  81 */       if (!this.isOpen) {
/*  82 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "OPEN"));
/*  83 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*     */       }
/*  85 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  86 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  89 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/*  91 */       break;
/*     */     case 4: 
/*  93 */       if (!this.isOpen) {
/*  94 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REAR"));
/*  95 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(1.2F));
/*     */       } else {
/*  97 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REAR_IDLE"));
/*  98 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.9F));
/*     */       }
/* 100 */       if (AbstractDungeon.ascensionLevel >= 17) {
/* 101 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 4), 4));
/*     */       }
/*     */       else {
/* 104 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 3), 3));
/*     */       }
/*     */       
/* 107 */       break;
/*     */     }
/*     */     
/*     */     
/* 111 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 116 */     if (stateName.equals("CLOSED")) {
/* 117 */       this.state.setAnimation(0, "transitiontoclosed", false);
/* 118 */       this.state.addAnimation(0, "idle closed", true, 0.0F);
/* 119 */       this.isOpen = false;
/* 120 */     } else if (stateName.equals("OPEN")) {
/* 121 */       this.state.setAnimation(0, "transitiontoopened", false);
/* 122 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 123 */       this.isOpen = true;
/* 124 */     } else if (stateName.equals("REAR_IDLE")) {
/* 125 */       this.state.setAnimation(0, "rear", false);
/* 126 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 127 */       this.isOpen = true;
/*     */     } else {
/* 129 */       this.state.setAnimation(0, "transitiontoopened", false);
/* 130 */       this.state.addAnimation(0, "rear", false, 0.0F);
/* 131 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 132 */       this.isOpen = true;
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 138 */     if (AbstractDungeon.ascensionLevel >= 17) {
/* 139 */       if (num < 25) {
/* 140 */         if (lastMove((byte)4)) {
/* 141 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 143 */           setMove(MOVES[0], (byte)4, AbstractMonster.Intent.BUFF);
/*     */         }
/*     */       }
/* 146 */       else if (lastTwoMoves((byte)3)) {
/* 147 */         setMove(MOVES[0], (byte)4, AbstractMonster.Intent.BUFF);
/*     */       } else {
/* 149 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */     }
/* 153 */     else if (num < 25) {
/* 154 */       if (lastTwoMoves((byte)4)) {
/* 155 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 157 */         setMove(MOVES[0], (byte)4, AbstractMonster.Intent.BUFF);
/*     */       }
/*     */     }
/* 160 */     else if (lastTwoMoves((byte)3)) {
/* 161 */       setMove(MOVES[0], (byte)4, AbstractMonster.Intent.BUFF);
/*     */     } else {
/* 163 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\LouseNormal.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */