/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Slimed;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SpikeSlime_M extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SpikeSlime_M";
/*  25 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("SpikeSlime_M");
/*  26 */   public static final String NAME = monsterStrings.NAME;
/*  27 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  28 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final int HP_MIN = 28;
/*     */   public static final int HP_MAX = 32;
/*     */   public static final int A_2_HP_MIN = 29;
/*     */   public static final int A_2_HP_MAX = 34;
/*     */   public static final int TACKLE_DAMAGE = 8;
/*     */   public static final int WOUND_COUNT = 1;
/*     */   public static final int A_2_TACKLE_DAMAGE = 10;
/*     */   public static final int FRAIL_TURNS = 1;
/*     */   private static final byte FLAME_TACKLE = 1;
/*  38 */   private static final byte FRAIL_LICK = 4; private static final String FRAIL_NAME = MOVES[0];
/*     */   
/*     */   public SpikeSlime_M(float x, float y) {
/*  41 */     this(x, y, 0, 32);
/*     */     
/*  43 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  44 */       setHp(29, 34);
/*     */     } else {
/*  46 */       setHp(28, 32);
/*     */     }
/*     */   }
/*     */   
/*     */   public SpikeSlime_M(float x, float y, int poisonAmount, int newHealth) {
/*  51 */     super(NAME, "SpikeSlime_M", newHealth, 0.0F, -25.0F, 170.0F, 130.0F, null, x, y, true);
/*     */     
/*  53 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  54 */       this.damage.add(new DamageInfo(this, 10));
/*     */     } else {
/*  56 */       this.damage.add(new DamageInfo(this, 8));
/*     */     }
/*     */     
/*  59 */     if (poisonAmount >= 1) {
/*  60 */       this.powers.add(new PoisonPower(this, this, poisonAmount));
/*     */     }
/*     */     
/*  63 */     loadAnimation("images/monsters/theBottom/slimeAltM/skeleton.atlas", "images/monsters/theBottom/slimeAltM/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  67 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  68 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  73 */     switch (this.nextMove) {
/*     */     case 4: 
/*  75 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  76 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  82 */       break;
/*     */     case 1: 
/*  84 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  85 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  88 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  90 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Slimed(), 1));
/*     */     }
/*     */     
/*  93 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/*  99 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 101 */       if (num < 30) {
/* 102 */         if (lastTwoMoves((byte)1)) {
/* 103 */           setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         } else {
/* 105 */           setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         }
/*     */       }
/* 108 */       else if (lastMove((byte)4)) {
/* 109 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 111 */         setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 116 */     else if (num < 30) {
/* 117 */       if (lastTwoMoves((byte)1)) {
/* 118 */         setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/* 120 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */     }
/* 123 */     else if (lastTwoMoves((byte)4)) {
/* 124 */       setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */     } else {
/* 126 */       setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 134 */     super.die();
/*     */     
/* 136 */     if ((AbstractDungeon.getMonsters().areMonstersBasicallyDead()) && 
/* 137 */       ((AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss))) {
/* 138 */       onBossVictoryLogic();
/* 139 */       UnlockTracker.hardUnlockOverride("SLIME");
/* 140 */       UnlockTracker.unlockAchievement("SLIME_BOSS");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\SpikeSlime_M.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */