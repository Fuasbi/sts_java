/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class AcidSlime_M extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "AcidSlime_M";
/*  26 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("AcidSlime_M");
/*  27 */   public static final String NAME = monsterStrings.NAME;
/*  28 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  29 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*  31 */   private static final String WOUND_NAME = MOVES[0]; private static final String WEAK_NAME = MOVES[1];
/*     */   
/*     */   public static final int HP_MIN = 28;
/*     */   public static final int HP_MAX = 32;
/*     */   public static final int A_2_HP_MIN = 29;
/*     */   public static final int A_2_HP_MAX = 34;
/*     */   public static final int W_TACKLE_DMG = 7;
/*     */   public static final int WOUND_COUNT = 1;
/*     */   
/*     */   public AcidSlime_M(float x, float y)
/*     */   {
/*  42 */     this(x, y, 0, 32);
/*     */     
/*  44 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  45 */       setHp(29, 34);
/*     */     } else {
/*  47 */       setHp(28, 32);
/*     */     }
/*     */   }
/*     */   
/*     */   public AcidSlime_M(float x, float y, int poisonAmount, int newHealth) {
/*  52 */     super(NAME, "AcidSlime_M", newHealth, 0.0F, 0.0F, 170.0F, 130.0F, null, x, y, true);
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  55 */       this.damage.add(new DamageInfo(this, 8));
/*  56 */       this.damage.add(new DamageInfo(this, 12));
/*     */     } else {
/*  58 */       this.damage.add(new DamageInfo(this, 7));
/*  59 */       this.damage.add(new DamageInfo(this, 10));
/*     */     }
/*     */     
/*  62 */     if (poisonAmount >= 1) {
/*  63 */       this.powers.add(new PoisonPower(this, this, poisonAmount));
/*     */     }
/*     */     
/*  66 */     loadAnimation("images/monsters/theBottom/slimeM/skeleton.atlas", "images/monsters/theBottom/slimeM/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  70 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  71 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  72 */     this.state.addListener(new com.megacrit.cardcrawl.helpers.SlimeAnimListener());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  77 */     switch (this.nextMove) {
/*     */     case 4: 
/*  79 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  80 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  86 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*  87 */       break;
/*     */     case 1: 
/*  89 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  90 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  93 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  95 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Slimed(), 1));
/*  96 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*  97 */       break;
/*     */     case 2: 
/*  99 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 100 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 103 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 105 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 112 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 114 */       if (num < 40) {
/* 115 */         if (lastTwoMoves((byte)1)) {
/* 116 */           if (AbstractDungeon.aiRng.randomBoolean()) {
/* 117 */             setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */           } else {
/* 119 */             setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */           }
/*     */         } else {
/* 122 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         }
/*     */         
/*     */       }
/* 126 */       else if (num < 80) {
/* 127 */         if (lastTwoMoves((byte)2)) {
/* 128 */           if (AbstractDungeon.aiRng.randomBoolean(0.5F)) {
/* 129 */             setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */           } else {
/* 131 */             setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */           }
/*     */         } else {
/* 134 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 139 */       else if (lastMove((byte)4)) {
/* 140 */         if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 141 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 143 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */       } else {
/* 146 */         setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 151 */     else if (num < 30) {
/* 152 */       if (lastTwoMoves((byte)1)) {
/* 153 */         if (AbstractDungeon.aiRng.randomBoolean()) {
/* 154 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 156 */           setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       } else {
/* 159 */         setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */     }
/* 163 */     else if (num < 70) {
/* 164 */       if (lastMove((byte)2)) {
/* 165 */         if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 166 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 168 */           setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       } else {
/* 171 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 176 */     else if (lastTwoMoves((byte)4)) {
/* 177 */       if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 178 */         setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 180 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */     } else
/* 183 */       setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */   }
/*     */   
/*     */   public static final int N_TACKLE_DMG = 10;
/*     */   public static final int A_2_W_TACKLE_DMG = 8;
/*     */   public static final int A_2_N_TACKLE_DMG = 12;
/*     */   
/*     */   public void die() {
/* 191 */     super.die();
/*     */     
/* 193 */     if ((AbstractDungeon.getMonsters().areMonstersBasicallyDead()) && 
/* 194 */       ((AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss))) {
/* 195 */       onBossVictoryLogic();
/* 196 */       UnlockTracker.hardUnlockOverride("SLIME");
/* 197 */       UnlockTracker.unlockAchievement("SLIME_BOSS");
/*     */     }
/*     */   }
/*     */   
/*     */   public static final int WEAK_TURNS = 1;
/*     */   private static final byte WOUND_TACKLE = 1;
/*     */   private static final byte NORMAL_TACKLE = 2;
/*     */   private static final byte WEAK_LICK = 4;
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\AcidSlime_M.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */