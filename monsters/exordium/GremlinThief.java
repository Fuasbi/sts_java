/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinThief extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinThief";
/*  22 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("GremlinThief");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int THIEF_DAMAGE = 9;
/*     */   private static final int A_2_THIEF_DAMAGE = 10;
/*     */   private static final byte PUNCTURE = 1;
/*     */   private static final int HP_MIN = 10;
/*     */   private static final int HP_MAX = 14;
/*     */   private static final int A_2_HP_MIN = 11;
/*     */   private static final int A_2_HP_MAX = 15;
/*     */   private int thiefDamage;
/*     */   
/*  35 */   public GremlinThief(float x, float y) { super(NAME, "GremlinThief", 14, 0.0F, 0.0F, 120.0F, 160.0F, null, x, y);
/*  36 */     this.dialogY = (50.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*     */     
/*  38 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  39 */       setHp(11, 15);
/*     */     } else {
/*  41 */       setHp(10, 14);
/*     */     }
/*     */     
/*  44 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  45 */       this.thiefDamage = 10;
/*     */     } else {
/*  47 */       this.thiefDamage = 9;
/*     */     }
/*     */     
/*  50 */     this.damage.add(new DamageInfo(this, this.thiefDamage));
/*     */     
/*  52 */     loadAnimation("images/monsters/theBottom/thiefGremlin/skeleton.atlas", "images/monsters/theBottom/thiefGremlin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  56 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "animation", true);
/*  57 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  62 */     switch (this.nextMove) {
/*     */     case 1: 
/*  64 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  65 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  68 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/*     */ 
/*  71 */       if (!this.escapeNext) {
/*  72 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, this.thiefDamage));
/*     */       }
/*     */       else {
/*  75 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*     */       }
/*  77 */       break;
/*     */     case 99: 
/*  79 */       playSfx();
/*  80 */       AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.5F, DIALOG[1], false));
/*     */       
/*  82 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(this));
/*  83 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*  84 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void playSfx()
/*     */   {
/*  91 */     int roll = MathUtils.random(1);
/*  92 */     if (roll == 0) {
/*  93 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINSPAZZY_1A"));
/*     */     } else {
/*  95 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINSPAZZY_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 100 */     int roll = MathUtils.random(2);
/* 101 */     if (roll == 0) {
/* 102 */       CardCrawlGame.sound.play("VO_GREMLINSPAZZY_2A");
/* 103 */     } else if (roll == 1) {
/* 104 */       CardCrawlGame.sound.play("VO_GREMLINSPAZZY_2B");
/*     */     } else {
/* 106 */       CardCrawlGame.sound.play("VO_GREMLINSPAZZY_2C");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 112 */     super.die();
/* 113 */     playDeathSfx();
/*     */   }
/*     */   
/*     */   public void escapeNext()
/*     */   {
/* 118 */     if ((!this.cannotEscape) && 
/* 119 */       (!this.escapeNext)) {
/* 120 */       this.escapeNext = true;
/* 121 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 128 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */   public void deathReact()
/*     */   {
/* 133 */     if ((this.intent != AbstractMonster.Intent.ESCAPE) && (!this.isDying)) {
/* 134 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/* 135 */       setMove((byte)99, AbstractMonster.Intent.ESCAPE);
/* 136 */       createIntent();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\GremlinThief.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */