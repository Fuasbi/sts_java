/*    */ package com.megacrit.cardcrawl.monsters.exordium;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class AcidSlime_S extends AbstractMonster
/*    */ {
/*    */   public static final String ID = "AcidSlime_S";
/* 20 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("AcidSlime_S");
/* 21 */   public static final String NAME = monsterStrings.NAME;
/* 22 */   public static final String[] MOVES = monsterStrings.MOVES;
/* 23 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*    */   public static final int HP_MIN = 8;
/*    */   public static final int HP_MAX = 12;
/*    */   public static final int A_2_HP_MIN = 9;
/*    */   public static final int A_2_HP_MAX = 13;
/*    */   public static final int TACKLE_DAMAGE = 3;
/*    */   public static final int WEAK_TURNS = 1;
/*    */   public static final int A_2_TACKLE_DAMAGE = 4;
/*    */   private static final byte TACKLE = 1;
/*    */   private static final byte DEBUFF = 2;
/*    */   
/* 34 */   public AcidSlime_S(float x, float y, int poisonAmount) { super(NAME, "AcidSlime_S", 12, 0.0F, -4.0F, 130.0F, 100.0F, null, x, y);
/*    */     
/* 36 */     if (AbstractDungeon.ascensionLevel >= 7) {
/* 37 */       setHp(9, 13);
/*    */     } else {
/* 39 */       setHp(8, 12);
/*    */     }
/*    */     
/* 42 */     if (AbstractDungeon.ascensionLevel >= 2) {
/* 43 */       this.damage.add(new DamageInfo(this, 4));
/*    */     } else {
/* 45 */       this.damage.add(new DamageInfo(this, 3));
/*    */     }
/*    */     
/* 48 */     if (poisonAmount >= 1) {
/* 49 */       this.powers.add(new PoisonPower(this, this, poisonAmount));
/*    */     }
/*    */     
/* 52 */     loadAnimation("images/monsters/theBottom/slimeS/skeleton.atlas", "images/monsters/theBottom/slimeS/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/* 56 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 57 */     e.setTime(e.getEndTime() * MathUtils.random());
/* 58 */     this.state.addListener(new com.megacrit.cardcrawl.helpers.SlimeAnimListener());
/*    */   }
/*    */   
/*    */   public void takeTurn()
/*    */   {
/* 63 */     switch (this.nextMove) {
/*    */     case 1: 
/* 65 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 66 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 69 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       
/* 71 */       setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/* 72 */       break;
/*    */     case 2: 
/* 74 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 75 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 1, true), 1));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 81 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   protected void getMove(int num)
/*    */   {
/* 88 */     if (AbstractDungeon.ascensionLevel >= 17) {
/* 89 */       if (lastTwoMoves((byte)1)) {
/* 90 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*    */       } else {
/* 92 */         setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/*    */       }
/*    */     }
/* 95 */     else if (AbstractDungeon.aiRng.randomBoolean()) {
/* 96 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*    */     } else {
/* 98 */       setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\AcidSlime_S.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */