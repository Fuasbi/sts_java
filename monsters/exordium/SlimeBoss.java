/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateJumpAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateShakeAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SuicideAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.CannotLoseAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.HideHealthBarAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Slimed;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.SplitPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SlimeBoss extends AbstractMonster
/*     */ {
/*  43 */   private static final Logger logger = LogManager.getLogger(SlimeBoss.class.getName());
/*     */   public static final String ID = "SlimeBoss";
/*  45 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("SlimeBoss");
/*  46 */   public static final String NAME = monsterStrings.NAME;
/*  47 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  48 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final int HP = 140;
/*     */   public static final int A_2_HP = 150;
/*     */   public static final int TACKLE_DAMAGE = 9;
/*     */   public static final int SLAM_DAMAGE = 35;
/*     */   public static final int A_2_TACKLE_DAMAGE = 10;
/*     */   public static final int A_2_SLAM_DAMAGE = 38;
/*     */   private int tackleDmg;
/*     */   private int slamDmg;
/*     */   public static final int STICKY_TURNS = 3;
/*     */   private static final byte SLAM = 1;
/*     */   private static final byte PREP_SLAM = 2;
/*  60 */   private static final byte SPLIT = 3; private static final byte STICKY = 4; private static final String SLAM_NAME = MOVES[0]; private static final String PREP_NAME = MOVES[1]; private static final String SPLIT_NAME = MOVES[2];
/*  61 */   private static final String STICKY_NAME = MOVES[3];
/*  62 */   private boolean firstTurn = true;
/*     */   
/*     */   public SlimeBoss() {
/*  65 */     super(NAME, "SlimeBoss", 140, 0.0F, -30.0F, 400.0F, 350.0F, null, 0.0F, 28.0F);
/*  66 */     this.type = AbstractMonster.EnemyType.BOSS;
/*     */     
/*  68 */     this.dialogX = (-150.0F * Settings.scale);
/*  69 */     this.dialogY = (-70.0F * Settings.scale);
/*     */     
/*  71 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  72 */       setHp(150);
/*     */     } else {
/*  74 */       setHp(140);
/*     */     }
/*     */     
/*  77 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  78 */       this.tackleDmg = 10;
/*  79 */       this.slamDmg = 38;
/*     */     } else {
/*  81 */       this.tackleDmg = 9;
/*  82 */       this.slamDmg = 35;
/*     */     }
/*     */     
/*  85 */     this.damage.add(new DamageInfo(this, this.tackleDmg));
/*  86 */     this.damage.add(new DamageInfo(this, this.slamDmg));
/*  87 */     this.powers.add(new SplitPower(this));
/*     */     
/*  89 */     loadAnimation("images/monsters/theBottom/boss/slime/skeleton.atlas", "images/monsters/theBottom/boss/slime/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  93 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  94 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  99 */     CardCrawlGame.music.unsilenceBGM();
/* 100 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 101 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BOTTOM");
/* 102 */     UnlockTracker.markBossAsSeen("SLIME");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 107 */     switch (this.nextMove) {
/*     */     case 4: 
/* 109 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/* 110 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("MONSTER_SLIME_ATTACK"));
/* 111 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 112 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Slimed(), 5));
/*     */       }
/*     */       else {
/* 115 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Slimed(), 3));
/*     */       }
/*     */       
/* 118 */       setMove(PREP_NAME, (byte)2, AbstractMonster.Intent.UNKNOWN);
/* 119 */       break;
/*     */     case 2: 
/* 121 */       playSfx();
/* 122 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.ShoutAction(this, DIALOG[0], 1.0F, 2.0F));
/* 123 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.ShakeScreenAction(0.3F, ScreenShake.ShakeDur.LONG, ScreenShake.ShakeIntensity.LOW));
/*     */       
/* 125 */       setMove(SLAM_NAME, (byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/* 126 */       break;
/*     */     case 1: 
/* 128 */       AbstractDungeon.actionManager.addToBottom(new AnimateJumpAction(this));
/* 129 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.WeightyImpactEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, new Color(0.1F, 1.0F, 0.1F, 0.0F))));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 135 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.8F));
/* 136 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 137 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.POISON));
/* 138 */       setMove(STICKY_NAME, (byte)4, AbstractMonster.Intent.STRONG_DEBUFF);
/* 139 */       break;
/*     */     case 3: 
/* 141 */       AbstractDungeon.actionManager.addToBottom(new CannotLoseAction());
/* 142 */       AbstractDungeon.actionManager.addToBottom(new AnimateShakeAction(this, 1.0F, 0.1F));
/* 143 */       AbstractDungeon.actionManager.addToBottom(new HideHealthBarAction(this));
/* 144 */       AbstractDungeon.actionManager.addToBottom(new SuicideAction(this, false));
/* 145 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(1.0F));
/* 146 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("SLIME_SPLIT"));
/*     */       
/* 148 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new SpikeSlime_L(-385.0F, 20.0F, 0, this.currentHealth), false));
/*     */       
/* 150 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new AcidSlime_L(120.0F, -8.0F, 0, this.currentHealth), false));
/*     */       
/* 152 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.CanLoseAction());
/* 153 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/*     */     }
/*     */   }
/*     */   
/*     */   private void playSfx()
/*     */   {
/* 159 */     int roll = MathUtils.random(1);
/* 160 */     if (roll == 0) {
/* 161 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_SLIMEBOSS_1A"));
/*     */     } else {
/* 163 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_SLIMEBOSS_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 169 */     super.damage(info);
/*     */     
/* 171 */     if ((!this.isDying) && (this.currentHealth <= this.maxHealth / 2.0F) && (this.nextMove != 3)) {
/* 172 */       logger.info("SPLIT");
/* 173 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/* 174 */       createIntent();
/* 175 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction(this, com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType.INTERRUPTED));
/* 176 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetMoveAction(this, SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN));
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 182 */     if (this.firstTurn) {
/* 183 */       this.firstTurn = false;
/* 184 */       setMove(STICKY_NAME, (byte)4, AbstractMonster.Intent.STRONG_DEBUFF);
/* 185 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 191 */     super.die();
/* 192 */     CardCrawlGame.sound.play("VO_SLIMEBOSS_2A");
/*     */     
/* 194 */     for (AbstractGameAction a : AbstractDungeon.actionManager.actions) {
/* 195 */       if ((a instanceof SpawnMonsterAction)) {
/* 196 */         return;
/*     */       }
/*     */     }
/*     */     
/* 200 */     if (this.currentHealth <= 0) {
/* 201 */       useFastShakeAnimation(5.0F);
/* 202 */       CardCrawlGame.screenShake.rumble(4.0F);
/* 203 */       this.deathTimer += 1.5F;
/* 204 */       onBossVictoryLogic();
/* 205 */       UnlockTracker.hardUnlockOverride("SLIME");
/* 206 */       UnlockTracker.unlockAchievement("SLIME_BOSS");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\SlimeBoss.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */