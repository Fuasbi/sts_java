/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.AngryPower;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinWarrior extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinWarrior";
/*  24 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("GremlinWarrior");
/*  25 */   public static final String NAME = monsterStrings.NAME;
/*  26 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  27 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int SCRATCH_DAMAGE = 4;
/*     */   private static final int A_2_SCRATCH_DAMAGE = 5;
/*     */   private static final byte SCRATCH = 1;
/*     */   private static final int HP_MIN = 20;
/*     */   private static final int HP_MAX = 24;
/*     */   private static final int A_2_HP_MIN = 21;
/*     */   private static final int A_2_HP_MAX = 25;
/*     */   
/*     */   public GremlinWarrior(float x, float y) {
/*  37 */     super(NAME, "GremlinWarrior", 24, -4.0F, 12.0F, 130.0F, 194.0F, null, x, y);
/*  38 */     this.dialogY = (30.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*     */     
/*  40 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  41 */       setHp(21, 25);
/*     */     } else {
/*  43 */       setHp(20, 24);
/*     */     }
/*     */     
/*  46 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  47 */       this.damage.add(new DamageInfo(this, 5));
/*     */     } else {
/*  49 */       this.damage.add(new DamageInfo(this, 4));
/*     */     }
/*     */     
/*  52 */     loadAnimation("images/monsters/theBottom/angryGremlin/skeleton.atlas", "images/monsters/theBottom/angryGremlin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  56 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  57 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  62 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  63 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new AngryPower(this, 2)));
/*     */     } else {
/*  65 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new AngryPower(this, 1)));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  71 */     switch (this.nextMove) {
/*     */     case 1: 
/*  73 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  74 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  77 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/*     */ 
/*  80 */       if (this.escapeNext) {
/*  81 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*     */       } else {
/*  83 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)1, AbstractMonster.Intent.ATTACK, 
/*  84 */           ((DamageInfo)this.damage.get(0)).base));
/*     */       }
/*  86 */       break;
/*     */     case 99: 
/*  88 */       playSfx();
/*  89 */       AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.5F, DIALOG[1], false));
/*     */       
/*  91 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(this));
/*  92 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*  93 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void playSfx()
/*     */   {
/* 100 */     int roll = MathUtils.random(2);
/* 101 */     if (roll == 0) {
/* 102 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINANGRY_1A"));
/* 103 */     } else if (roll == 1) {
/* 104 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINANGRY_1B"));
/*     */     } else {
/* 106 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINANGRY_1C"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 111 */     int roll = MathUtils.random(1);
/* 112 */     if (roll == 0) {
/* 113 */       CardCrawlGame.sound.play("VO_GREMLINANGRY_2A");
/*     */     } else {
/* 115 */       CardCrawlGame.sound.play("VO_GREMLINANGRY_2B");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 121 */     super.die();
/* 122 */     playDeathSfx();
/*     */   }
/*     */   
/*     */   public void escapeNext()
/*     */   {
/* 127 */     if ((!this.cannotEscape) && 
/* 128 */       (!this.escapeNext)) {
/* 129 */       this.escapeNext = true;
/* 130 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 137 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */   public void deathReact()
/*     */   {
/* 142 */     if ((this.intent != AbstractMonster.Intent.ESCAPE) && (!this.isDying)) {
/* 143 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/* 144 */       setMove((byte)99, AbstractMonster.Intent.ESCAPE);
/* 145 */       createIntent();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\GremlinWarrior.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */