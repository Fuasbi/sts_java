/*    */ package com.megacrit.cardcrawl.monsters.exordium;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ApologySlime extends AbstractMonster
/*    */ {
/*    */   public static final String ID = "Apology Slime";
/* 20 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Apology Slime");
/* 21 */   public static final String NAME = monsterStrings.NAME;
/* 22 */   public static final String[] MOVES = monsterStrings.MOVES;
/* 23 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*    */   public static final int HP_MIN = 8;
/*    */   public static final int HP_MAX = 12;
/*    */   public static final int TACKLE_DAMAGE = 3;
/*    */   public static final int WEAK_TURNS = 1;
/*    */   private static final byte TACKLE = 1;
/*    */   private static final byte DEBUFF = 2;
/*    */   
/* 31 */   public ApologySlime() { super(NAME, "Apology Slime", AbstractDungeon.monsterHpRng.random(8, 12), 0.0F, -4.0F, 130.0F, 100.0F, null);
/*    */     
/* 33 */     this.damage.add(new DamageInfo(this, 3));
/*    */     
/* 35 */     loadAnimation("images/monsters/theBottom/slimeS/skeleton.atlas", "images/monsters/theBottom/slimeS/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/* 39 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 40 */     e.setTime(e.getEndTime() * MathUtils.random());
/* 41 */     this.state.addListener(new com.megacrit.cardcrawl.helpers.SlimeAnimListener());
/*    */   }
/*    */   
/*    */   public void usePreBattleAction()
/*    */   {
/* 46 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.TalkAction(this, "Aw, something went wrong... NL please let the devs know!", 4.0F, 4.0F));
/*    */   }
/*    */   
/*    */ 
/*    */   public void takeTurn()
/*    */   {
/* 52 */     switch (this.nextMove) {
/*    */     case 1: 
/* 54 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 55 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 58 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       
/* 60 */       setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/* 61 */       break;
/*    */     case 2: 
/* 63 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 64 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 1, true), 1));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 70 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   protected void getMove(int num)
/*    */   {
/* 77 */     if (AbstractDungeon.aiRng.randomBoolean()) {
/* 78 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*    */     } else {
/* 80 */       setMove((byte)2, AbstractMonster.Intent.DEBUFF);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\ApologySlime.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */