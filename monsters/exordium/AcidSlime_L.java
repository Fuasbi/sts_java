/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateShakeAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SuicideAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.CannotLoseAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*     */ import com.megacrit.cardcrawl.powers.SplitPower;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class AcidSlime_L extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "AcidSlime_L";
/*  38 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("AcidSlime_L");
/*  39 */   public static final String NAME = monsterStrings.NAME;
/*  40 */   public static final String[] MOVES = monsterStrings.MOVES;
/*     */   
/*  42 */   private static final String WOUND_NAME = MOVES[0]; private static final String SPLIT_NAME = MOVES[1]; private static final String WEAK_NAME = MOVES[2];
/*     */   
/*     */   public static final int HP_MIN = 65;
/*     */   public static final int HP_MAX = 69;
/*     */   public static final int A_2_HP_MIN = 68;
/*     */   public static final int A_2_HP_MAX = 72;
/*     */   public static final int W_TACKLE_DMG = 11;
/*     */   public static final int N_TACKLE_DMG = 16;
/*     */   public static final int A_2_W_TACKLE_DMG = 12;
/*     */   public static final int A_2_N_TACKLE_DMG = 18;
/*     */   
/*     */   public AcidSlime_L(float x, float y)
/*     */   {
/*  55 */     this(x, y, 0, 69);
/*     */     
/*  57 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  58 */       setHp(68, 72);
/*     */     } else {
/*  60 */       setHp(65, 69);
/*     */     }
/*     */   }
/*     */   
/*     */   public AcidSlime_L(float x, float y, int poisonAmount, int newHealth) {
/*  65 */     super(NAME, "AcidSlime_L", newHealth, 0.0F, 0.0F, 300.0F, 180.0F, null, x, y, true);
/*     */     
/*  67 */     this.saveX = x;
/*  68 */     this.saveY = y;
/*  69 */     this.splitTriggered = false;
/*     */     
/*  71 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  72 */       this.damage.add(new DamageInfo(this, 12));
/*  73 */       this.damage.add(new DamageInfo(this, 18));
/*     */     } else {
/*  75 */       this.damage.add(new DamageInfo(this, 11));
/*  76 */       this.damage.add(new DamageInfo(this, 16));
/*     */     }
/*     */     
/*  79 */     this.powers.add(new SplitPower(this));
/*     */     
/*  81 */     if (poisonAmount >= 1) {
/*  82 */       this.powers.add(new PoisonPower(this, this, poisonAmount));
/*     */     }
/*     */     
/*  85 */     loadAnimation("images/monsters/theBottom/slimeL/skeleton.atlas", "images/monsters/theBottom/slimeL/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  89 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  90 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  91 */     this.state.addListener(new com.megacrit.cardcrawl.helpers.SlimeAnimListener());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  96 */     switch (this.nextMove) {
/*     */     case 4: 
/*  98 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  99 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 105 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 106 */       break;
/*     */     case 1: 
/* 108 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 109 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("MONSTER_SLIME_ATTACK"));
/* 110 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 113 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 115 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Slimed(), 2));
/* 116 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 117 */       break;
/*     */     case 2: 
/* 119 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 120 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 123 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 125 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 126 */       break;
/*     */     case 3: 
/* 128 */       AbstractDungeon.actionManager.addToBottom(new CannotLoseAction());
/* 129 */       AbstractDungeon.actionManager.addToBottom(new AnimateShakeAction(this, 1.0F, 0.1F));
/* 130 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.HideHealthBarAction(this));
/* 131 */       AbstractDungeon.actionManager.addToBottom(new SuicideAction(this, false));
/* 132 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(1.0F));
/* 133 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("SLIME_SPLIT"));
/*     */       
/* 135 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new AcidSlime_M(this.saveX - 134.0F, this.saveY + 
/*     */       
/* 137 */         MathUtils.random(-4.0F, 4.0F), 0, this.currentHealth), false));
/*     */       
/* 139 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new AcidSlime_M(this.saveX + 134.0F, this.saveY + 
/*     */       
/* 141 */         MathUtils.random(-4.0F, 4.0F), 0, this.currentHealth), false));
/*     */       
/*     */ 
/* 144 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.CanLoseAction());
/* 145 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 152 */     super.damage(info);
/*     */     
/* 154 */     if ((!this.isDying) && (this.currentHealth <= this.maxHealth / 2.0F) && (this.nextMove != 3) && (!this.splitTriggered)) {
/* 155 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/* 156 */       createIntent();
/* 157 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this, TextAboveCreatureAction.TextType.INTERRUPTED));
/* 158 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN));
/* 159 */       this.splitTriggered = true;
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 165 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 167 */       if (num < 40) {
/* 168 */         if (lastTwoMoves((byte)1)) {
/* 169 */           if (AbstractDungeon.aiRng.randomBoolean(0.6F)) {
/* 170 */             setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */           } else {
/* 172 */             setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */           }
/*     */         } else {
/* 175 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         }
/*     */         
/*     */       }
/* 179 */       else if (num < 70) {
/* 180 */         if (lastTwoMoves((byte)2)) {
/* 181 */           if (AbstractDungeon.aiRng.randomBoolean(0.6F)) {
/* 182 */             setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */           } else {
/* 184 */             setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */           }
/*     */         } else {
/* 187 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 192 */       else if (lastMove((byte)4)) {
/* 193 */         if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 194 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 196 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */       } else {
/* 199 */         setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 205 */     else if (num < 30) {
/* 206 */       if (lastTwoMoves((byte)1)) {
/* 207 */         if (AbstractDungeon.aiRng.randomBoolean()) {
/* 208 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 210 */           setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       } else {
/* 213 */         setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */     }
/* 217 */     else if (num < 70) {
/* 218 */       if (lastMove((byte)2)) {
/* 219 */         if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 220 */           setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 222 */           setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       } else {
/* 225 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 230 */     else if (lastTwoMoves((byte)4)) {
/* 231 */       if (AbstractDungeon.aiRng.randomBoolean(0.4F)) {
/* 232 */         setMove(WOUND_NAME, (byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 234 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */     } else
/* 237 */       setMove(WEAK_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */   }
/*     */   
/*     */   public static final int WEAK_TURNS = 2;
/*     */   public static final int WOUND_COUNT = 2;
/*     */   private static final byte SLIME_TACKLE = 1;
/*     */   private static final byte NORMAL_TACKLE = 2;
/*     */   
/* 245 */   public void die() { super.die();
/*     */     
/* 247 */     for (com.megacrit.cardcrawl.actions.AbstractGameAction a : AbstractDungeon.actionManager.actions) {
/* 248 */       if ((a instanceof SpawnMonsterAction)) {
/* 249 */         return;
/*     */       }
/*     */     }
/*     */     
/* 253 */     if ((AbstractDungeon.getMonsters().areMonstersBasicallyDead()) && 
/* 254 */       ((AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss))) {
/* 255 */       onBossVictoryLogic();
/* 256 */       UnlockTracker.hardUnlockOverride("SLIME");
/* 257 */       UnlockTracker.unlockAchievement("SLIME_BOSS");
/*     */     }
/*     */   }
/*     */   
/*     */   private static final byte SPLIT = 3;
/*     */   private static final byte WEAK_LICK = 4;
/*     */   private float saveX;
/*     */   private float saveY;
/*     */   private boolean splitTriggered;
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\AcidSlime_L.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */