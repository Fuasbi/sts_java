/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.CurlUpPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class LouseDefensive extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "FuzzyLouseDefensive";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("FuzzyLouseDefensive");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 11;
/*     */   private static final int HP_MAX = 17;
/*     */   private static final int A_2_HP_MIN = 12;
/*     */   private static final int A_2_HP_MAX = 18;
/*  30 */   private static final byte BITE = 3; private static final byte WEAKEN = 4; private boolean isOpen = true;
/*     */   private static final String CLOSED_STATE = "CLOSED";
/*     */   private static final String OPEN_STATE = "OPEN";
/*     */   private static final String REAR_IDLE = "REAR_IDLE";
/*     */   private final int biteDamage;
/*     */   private static final int WEAK_AMT = 2;
/*     */   
/*     */   public LouseDefensive(float x, float y)
/*     */   {
/*  39 */     super(NAME, "FuzzyLouseDefensive", 17, 0.0F, -5.0F, 180.0F, 140.0F, null, x, y);
/*     */     
/*  41 */     loadAnimation("images/monsters/theBottom/louseGreen/skeleton.atlas", "images/monsters/theBottom/louseGreen/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  45 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  46 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */     
/*  48 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  49 */       setHp(12, 18);
/*     */     } else {
/*  51 */       setHp(11, 17);
/*     */     }
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  55 */       this.biteDamage = AbstractDungeon.monsterHpRng.random(6, 8);
/*     */     } else {
/*  57 */       this.biteDamage = AbstractDungeon.monsterHpRng.random(5, 7);
/*     */     }
/*     */     
/*  60 */     this.damage.add(new DamageInfo(this, this.biteDamage));
/*     */   }
/*     */   
/*     */ 
/*     */   public void usePreBattleAction()
/*     */   {
/*  66 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  67 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  68 */         .random(9, 12))));
/*  69 */     } else if (AbstractDungeon.ascensionLevel >= 7) {
/*  70 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  71 */         .random(4, 8))));
/*     */     } else {
/*  73 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CurlUpPower(this, AbstractDungeon.monsterHpRng
/*  74 */         .random(3, 7))));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  80 */     switch (this.nextMove) {
/*     */     case 3: 
/*  82 */       if (!this.isOpen) {
/*  83 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "OPEN"));
/*  84 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/*     */       }
/*  86 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  87 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  90 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/*  92 */       break;
/*     */     case 4: 
/*  94 */       if (!this.isOpen) {
/*  95 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REAR"));
/*  96 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(1.2F));
/*     */       } else {
/*  98 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REAR_IDLE"));
/*  99 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.9F));
/*     */       }
/* 101 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 2, true), 2));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 107 */       break;
/*     */     }
/*     */     
/*     */     
/* 111 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 116 */     if (stateName.equals("CLOSED")) {
/* 117 */       this.state.setAnimation(0, "transitiontoclosed", false);
/* 118 */       this.state.addAnimation(0, "idle closed", true, 0.0F);
/* 119 */       this.isOpen = false;
/* 120 */     } else if (stateName.equals("OPEN")) {
/* 121 */       this.state.setAnimation(0, "transitiontoopened", false);
/* 122 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 123 */       this.isOpen = true;
/* 124 */     } else if (stateName.equals("REAR_IDLE")) {
/* 125 */       this.state.setAnimation(0, "rear", false);
/* 126 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 127 */       this.isOpen = true;
/*     */     } else {
/* 129 */       this.state.setAnimation(0, "transitiontoopened", false);
/* 130 */       this.state.addAnimation(0, "rear", false, 0.0F);
/* 131 */       this.state.addAnimation(0, "idle", true, 0.0F);
/* 132 */       this.isOpen = true;
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 138 */     if (AbstractDungeon.ascensionLevel >= 17) {
/* 139 */       if (num < 25) {
/* 140 */         if (lastMove((byte)4)) {
/* 141 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */         } else {
/* 143 */           setMove(MOVES[0], (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       }
/* 146 */       else if (lastTwoMoves((byte)3)) {
/* 147 */         setMove(MOVES[0], (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/* 149 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */     }
/* 153 */     else if (num < 25) {
/* 154 */       if (lastTwoMoves((byte)4)) {
/* 155 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 157 */         setMove(MOVES[0], (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */     }
/* 160 */     else if (lastTwoMoves((byte)3)) {
/* 161 */       setMove(MOVES[0], (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */     } else {
/* 163 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\LouseDefensive.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */