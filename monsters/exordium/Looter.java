/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Looter extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Looter";
/*  27 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Looter");
/*  28 */   public static final String NAME = monsterStrings.NAME;
/*  29 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  30 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 44;
/*     */   private static final int HP_MAX = 48;
/*     */   private static final int A_2_HP_MIN = 46;
/*     */   private static final int A_2_HP_MAX = 50;
/*  35 */   private int swipeDmg; private int lungeDmg; private int escapeDef = 6;
/*     */   private int goldAmt;
/*  37 */   private static final byte MUG = 1; private static final byte SMOKE_BOMB = 2; private static final byte ESCAPE = 3; private static final byte LUNGE = 4; private static final String SLASH_MSG1 = DIALOG[0];
/*  38 */   private static final String DEATH_MSG1 = DIALOG[1];
/*  39 */   private static final String SMOKE_BOMB_MSG = DIALOG[2];
/*  40 */   private static final String RUN_MSG = DIALOG[3];
/*  41 */   private int slashCount = 0;
/*  42 */   private int stolenGold = 0;
/*     */   
/*     */   public Looter(float x, float y) {
/*  45 */     super(NAME, "Looter", 48, 0.0F, 0.0F, 200.0F, 220.0F, null, x, y);
/*     */     
/*  47 */     this.dialogX = (-30.0F * Settings.scale);
/*  48 */     this.dialogY = (50.0F * Settings.scale);
/*     */     
/*  50 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  51 */       this.goldAmt = 20;
/*     */     } else {
/*  53 */       this.goldAmt = 15;
/*     */     }
/*     */     
/*  56 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  57 */       setHp(46, 50);
/*     */     } else {
/*  59 */       setHp(44, 48);
/*     */     }
/*     */     
/*  62 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  63 */       this.swipeDmg = 11;
/*  64 */       this.lungeDmg = 14;
/*     */     } else {
/*  66 */       this.swipeDmg = 10;
/*  67 */       this.lungeDmg = 12;
/*     */     }
/*     */     
/*  70 */     this.damage.add(new DamageInfo(this, this.swipeDmg));
/*  71 */     this.damage.add(new DamageInfo(this, this.lungeDmg));
/*     */     
/*  73 */     loadAnimation("images/monsters/theBottom/looter/skeleton.atlas", "images/monsters/theBottom/looter/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  77 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  78 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  83 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.ThieveryPower(this, this.goldAmt)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  88 */     switch (this.nextMove) {
/*     */     case 1: 
/*  90 */       if ((this.slashCount == 0) && (AbstractDungeon.aiRng.randomBoolean(0.6F)))
/*     */       {
/*  92 */         AbstractDungeon.actionManager.addToBottom(new TalkAction(this, SLASH_MSG1, 0.3F, 2.0F));
/*     */       }
/*     */       
/*  95 */       playSfx();
/*  96 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  97 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.AbstractGameAction()
/*     */       {
/*     */         public void update()
/*     */         {
/* 101 */           Looter.this.stolenGold = (Looter.this.stolenGold + Math.min(Looter.this.goldAmt, AbstractDungeon.player.gold));
/* 102 */           this.isDone = true;
/*     */         }
/* 104 */       });
/* 105 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 106 */         (DamageInfo)this.damage.get(0), this.goldAmt));
/*     */       
/* 108 */       this.slashCount += 1;
/* 109 */       if (this.slashCount == 2) {
/* 110 */         if (AbstractDungeon.aiRng.randomBoolean(0.5F)) {
/* 111 */           setMove((byte)2, AbstractMonster.Intent.DEFEND);
/*     */         } else {
/* 113 */           AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, MOVES[0], (byte)4, AbstractMonster.Intent.ATTACK, 
/* 114 */             ((DamageInfo)this.damage.get(1)).base));
/*     */         }
/*     */       } else {
/* 117 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, MOVES[1], (byte)1, AbstractMonster.Intent.ATTACK, 
/* 118 */           ((DamageInfo)this.damage.get(0)).base));
/*     */       }
/* 120 */       break;
/*     */     case 4: 
/* 122 */       playSfx();
/* 123 */       this.slashCount += 1;
/* 124 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 125 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.AbstractGameAction()
/*     */       {
/*     */         public void update()
/*     */         {
/* 129 */           Looter.this.stolenGold = (Looter.this.stolenGold + Math.min(Looter.this.goldAmt, AbstractDungeon.player.gold));
/* 130 */           this.isDone = true;
/*     */         }
/* 132 */       });
/* 133 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 134 */         (DamageInfo)this.damage.get(1), this.goldAmt));
/* 135 */       setMove((byte)2, AbstractMonster.Intent.DEFEND);
/* 136 */       break;
/*     */     case 2: 
/* 138 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, SMOKE_BOMB_MSG, 0.75F, 2.5F));
/* 139 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, this.escapeDef));
/* 140 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ESCAPE));
/* 141 */       break;
/*     */     case 3: 
/* 143 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, RUN_MSG, 0.3F, 2.5F));
/* 144 */       AbstractDungeon.getCurrRoom().mugged = true;
/* 145 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.SmokeBombEffect(this.hb.cX, this.hb.cY)));
/* 146 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(this));
/* 147 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)3, AbstractMonster.Intent.ESCAPE));
/* 148 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void playSfx()
/*     */   {
/* 155 */     int roll = MathUtils.random(2);
/* 156 */     if (roll == 0) {
/* 157 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_LOOTER_1A"));
/* 158 */     } else if (roll == 1) {
/* 159 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_LOOTER_1B"));
/*     */     } else {
/* 161 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_LOOTER_1C"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 166 */     int roll = MathUtils.random(2);
/* 167 */     if (roll == 0) {
/* 168 */       CardCrawlGame.sound.play("VO_LOOTER_2A");
/* 169 */     } else if (roll == 1) {
/* 170 */       CardCrawlGame.sound.play("VO_LOOTER_2B");
/*     */     } else {
/* 172 */       CardCrawlGame.sound.play("VO_LOOTER_2C");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 178 */     playDeathSfx();
/* 179 */     this.state.setTimeScale(0.1F);
/* 180 */     useShakeAnimation(5.0F);
/* 181 */     if (MathUtils.randomBoolean(0.3F)) {
/* 182 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.0F, DEATH_MSG1, false));
/* 183 */       this.deathTimer += 1.5F;
/*     */     }
/* 185 */     if (this.stolenGold > 0) {
/* 186 */       AbstractDungeon.getCurrRoom().addStolenGoldToRewards(this.stolenGold);
/*     */     }
/* 188 */     super.die();
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 193 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\Looter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */