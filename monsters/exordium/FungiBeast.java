/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class FungiBeast extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "FungiBeast";
/*     */   public static final String DOUBLE_ENCOUNTER = "TwoFungiBeasts";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("FungiBeast");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   private static final int HP_MIN = 22;
/*     */   
/*     */   private static final int HP_MAX = 28;
/*     */   
/*     */   private static final int A_2_HP_MIN = 24;
/*     */   
/*     */   private static final int A_2_HP_MAX = 28;
/*     */   
/*     */   private static final float HB_X = 0.0F;
/*     */   private static final float HB_Y = -16.0F;
/*     */   private static final float HB_W = 260.0F;
/*     */   private static final float HB_H = 170.0F;
/*     */   
/*     */   public FungiBeast(float x, float y)
/*     */   {
/*  42 */     super(NAME, "FungiBeast", 28, 0.0F, -16.0F, 260.0F, 170.0F, null, x, y);
/*     */     
/*  44 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  45 */       setHp(24, 28);
/*     */     } else {
/*  47 */       setHp(22, 28);
/*     */     }
/*     */     
/*  50 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  51 */       this.strAmt = 4;
/*  52 */       this.biteDamage = 6;
/*     */     } else {
/*  54 */       this.strAmt = 3;
/*  55 */       this.biteDamage = 6;
/*     */     }
/*     */     
/*  58 */     loadAnimation("images/monsters/theBottom/fungi/skeleton.atlas", "images/monsters/theBottom/fungi/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  62 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  63 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  64 */     e.setTimeScale(MathUtils.random(0.7F, 1.0F));
/*     */     
/*  66 */     this.damage.add(new DamageInfo(this, this.biteDamage));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  71 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.SporeCloudPower(this, 2)));
/*     */   }
/*     */   
/*     */ 
/*     */   public void takeTurn()
/*     */   {
/*  77 */     switch (this.nextMove) {
/*     */     case 1: 
/*  79 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  80 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.5F));
/*  81 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  84 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/*  86 */       break;
/*     */     case 2: 
/*  88 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  89 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, this.strAmt + 1), this.strAmt + 1));
/*     */       }
/*     */       else {
/*  92 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, this.strAmt), this.strAmt));
/*     */       }
/*     */       
/*     */       break;
/*     */     }
/*     */     
/*  98 */     AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this)); }
/*     */   
/*     */   private int biteDamage;
/*     */   private int strAmt;
/*     */   private static final int BITE_DMG = 6;
/*     */   private static final int GROW_STR = 3;
/*     */   
/* 105 */   protected void getMove(int num) { if (num < 60) {
/* 106 */       if (lastTwoMoves((byte)1)) {
/* 107 */         setMove(MOVES[0], (byte)2, AbstractMonster.Intent.BUFF);
/*     */       } else {
/* 109 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 114 */     else if (lastMove((byte)2)) {
/* 115 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     } else
/* 117 */       setMove(MOVES[0], (byte)2, AbstractMonster.Intent.BUFF);
/*     */   }
/*     */   
/*     */   private static final int A_2_GROW_STR = 4;
/*     */   private static final byte BITE = 1;
/*     */   private static final byte GROW = 2;
/*     */   private static final int VULN_AMT = 2;
/* 124 */   public void changeState(String key) { switch (key) {
/*     */     case "ATTACK": 
/* 126 */       this.state.setAnimation(0, "Attack", false);
/* 127 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 128 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 136 */     super.damage(info);
/* 137 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 138 */       this.state.setAnimation(0, "Hit", false);
/* 139 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\FungiBeast.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */