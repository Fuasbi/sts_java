/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect.ShockWaveType;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Sentry extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Sentry";
/*  28 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Sentry");
/*  29 */   public static final String NAME = monsterStrings.NAME;
/*  30 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  31 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final String ENC_NAME = "Sentries";
/*     */   private static final int HP_MIN = 38;
/*     */   private static final int HP_MAX = 42;
/*     */   private static final int A_2_HP_MIN = 39;
/*     */   private static final int A_2_HP_MAX = 45;
/*     */   private static final byte BOLT = 3;
/*     */   private static final byte BEAM = 4;
/*     */   private int beamDmg;
/*  40 */   private int dazedAmt; private static final int DAZED_AMT = 2; private static final int A_18_DAZED_AMT = 3; private boolean firstMove = true;
/*     */   
/*     */   public Sentry(float x, float y) {
/*  43 */     super(NAME, "Sentry", 42, 0.0F, -5.0F, 180.0F, 310.0F, null, x, y);
/*  44 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*     */     
/*  46 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  47 */       setHp(39, 45);
/*     */     } else {
/*  49 */       setHp(38, 42);
/*     */     }
/*     */     
/*  52 */     if (AbstractDungeon.ascensionLevel >= 3) {
/*  53 */       this.beamDmg = 10;
/*     */     } else {
/*  55 */       this.beamDmg = 9;
/*     */     }
/*     */     
/*  58 */     if (AbstractDungeon.ascensionLevel >= 18) {
/*  59 */       this.dazedAmt = 3;
/*     */     } else {
/*  61 */       this.dazedAmt = 2;
/*     */     }
/*     */     
/*  64 */     this.damage.add(new DamageInfo(this, this.beamDmg));
/*     */     
/*  66 */     loadAnimation("images/monsters/theBottom/sentry/skeleton.atlas", "images/monsters/theBottom/sentry/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  70 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  71 */     e.setTimeScale(2.0F);
/*  72 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  73 */     this.stateData.setMix("idle", "attack", 0.1F);
/*  74 */     this.stateData.setMix("idle", "spaz1", 0.1F);
/*  75 */     this.stateData.setMix("idle", "hit", 0.1F);
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  80 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this, this, new ArtifactPower(this, 1)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  85 */     switch (this.nextMove) {
/*     */     case 3: 
/*  87 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("THUNDERCLAP"));
/*  88 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new ShockWaveEffect(this.hb.cX, this.hb.cY, Color.ROYAL, ShockWaveEffect.ShockWaveType.ADDITIVE), 0.5F));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  93 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.FastShakeAction(AbstractDungeon.player, 0.6F, 0.2F));
/*  94 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Dazed(), this.dazedAmt));
/*  95 */       break;
/*     */     case 4: 
/*  97 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  98 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("ATTACK_MAGIC_BEAM_SHORT", 0.5F));
/*  99 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.BorderFlashEffect(Color.SKY)));
/* 100 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.SmallLaserEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, this.hb.cX, this.hb.cY), 0.3F));
/*     */       
/*     */ 
/*     */ 
/* 104 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 105 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/* 106 */       break;
/*     */     }
/*     */     
/*     */     
/* 110 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 115 */     super.damage(info);
/* 116 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 117 */       this.state.setAnimation(0, "hit", false);
/* 118 */       this.state.addAnimation(0, "idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 124 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 126 */       this.state.setAnimation(0, "attack", false);
/* 127 */       this.state.addAnimation(0, "idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 134 */     if (this.firstMove) {
/* 135 */       if (AbstractDungeon.getMonsters().monsters.lastIndexOf(this) % 2 == 0) {
/* 136 */         setMove((byte)3, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/* 138 */         setMove((byte)4, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/* 140 */       this.firstMove = false;
/* 141 */       return;
/*     */     }
/*     */     
/* 144 */     if (lastMove((byte)4)) {
/* 145 */       setMove((byte)3, AbstractMonster.Intent.DEBUFF);
/*     */     } else {
/* 147 */       setMove((byte)4, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\Sentry.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */