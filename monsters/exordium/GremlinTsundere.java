/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.GainBlockRandomMonsterAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinTsundere extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinTsundere";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("GremlinTsundere");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 12;
/*     */   private static final int HP_MAX = 15;
/*     */   private static final int A_2_HP_MIN = 13;
/*     */   private static final int A_2_HP_MAX = 17;
/*     */   private static final int BLOCK_AMOUNT = 7;
/*     */   private static final int A_2_BLOCK_AMOUNT = 8;
/*     */   private static final int A_17_BLOCK_AMOUNT = 11;
/*     */   private static final int BASH_DAMAGE = 6;
/*     */   private static final int A_2_BASH_DAMAGE = 8;
/*     */   private int blockAmt;
/*     */   private int bashDmg;
/*     */   private static final byte PROTECT = 1;
/*     */   private static final byte BASH = 2;
/*     */   
/*  40 */   public GremlinTsundere(float x, float y) { super(NAME, "GremlinTsundere", 15, 0.0F, 0.0F, 120.0F, 200.0F, null, x, y);
/*  41 */     this.dialogY = (60.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*     */     
/*  43 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  44 */       setHp(13, 17);
/*  45 */       this.blockAmt = 11;
/*  46 */     } else if (AbstractDungeon.ascensionLevel >= 7) {
/*  47 */       setHp(13, 17);
/*  48 */       this.blockAmt = 8;
/*     */     } else {
/*  50 */       setHp(12, 15);
/*  51 */       this.blockAmt = 7;
/*     */     }
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  55 */       this.bashDmg = 8;
/*     */     } else {
/*  57 */       this.bashDmg = 6;
/*     */     }
/*     */     
/*  60 */     this.damage.add(new DamageInfo(this, this.bashDmg));
/*     */     
/*  62 */     loadAnimation("images/monsters/theBottom/femaleGremlin/skeleton.atlas", "images/monsters/theBottom/femaleGremlin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  66 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  67 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  72 */     switch (this.nextMove)
/*     */     {
/*     */     case 1: 
/*  75 */       AbstractDungeon.actionManager.addToBottom(new GainBlockRandomMonsterAction(this, this.blockAmt));
/*     */       
/*  77 */       int aliveCount = 0;
/*     */       
/*     */ 
/*  80 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  81 */         if ((!m.isDying) && (!m.isEscaping)) {
/*  82 */           aliveCount++;
/*     */         }
/*     */       }
/*     */       
/*  86 */       if (this.escapeNext) {
/*  87 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*     */       }
/*  89 */       else if (aliveCount > 1) {
/*  90 */         setMove(MOVES[0], (byte)1, AbstractMonster.Intent.DEFEND);
/*     */       } else {
/*  92 */         setMove(MOVES[1], (byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */       
/*  95 */       break;
/*     */     case 2: 
/*  97 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  98 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 101 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/* 103 */       if (this.escapeNext) {
/* 104 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*     */       } else {
/* 106 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, MOVES[1], (byte)2, AbstractMonster.Intent.ATTACK, 
/* 107 */           ((DamageInfo)this.damage.get(0)).base));
/*     */       }
/* 109 */       break;
/*     */     case 99: 
/* 111 */       AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.5F, DIALOG[1], false));
/*     */       
/* 113 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(this));
/* 114 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/* 115 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 124 */     super.die();
/*     */   }
/*     */   
/*     */   public void escapeNext()
/*     */   {
/* 129 */     if ((!this.cannotEscape) && 
/* 130 */       (!this.escapeNext)) {
/* 131 */       this.escapeNext = true;
/* 132 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 139 */     setMove(MOVES[0], (byte)1, AbstractMonster.Intent.DEFEND);
/*     */   }
/*     */   
/*     */   public void deathReact()
/*     */   {
/* 144 */     if ((this.intent != AbstractMonster.Intent.ESCAPE) && (!this.isDying)) {
/* 145 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/* 146 */       setMove((byte)99, AbstractMonster.Intent.ESCAPE);
/* 147 */       createIntent();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\GremlinTsundere.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */