/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SlaverBlue extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SlaverBlue";
/*  20 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("SlaverBlue");
/*  21 */   public static final String NAME = monsterStrings.NAME;
/*  22 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  23 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   private static final int HP_MIN = 46;
/*     */   
/*     */   private static final int HP_MAX = 50;
/*     */   private static final int A_2_HP_MIN = 48;
/*     */   private static final int A_2_HP_MAX = 52;
/*     */   private static final int STAB_DMG = 12;
/*     */   private static final int A_2_STAB_DMG = 13;
/*     */   private static final int RAKE_DMG = 7;
/*     */   private static final int A_2_RAKE_DMG = 8;
/*  34 */   private int stabDmg = 12; private int rakeDmg = 7; private int weakAmt = 1;
/*     */   private static final byte STAB = 1;
/*     */   private static final byte RAKE = 4;
/*     */   
/*  38 */   public SlaverBlue(float x, float y) { super(NAME, "SlaverBlue", 50, 0.0F, 0.0F, 170.0F, 230.0F, null, x, y);
/*     */     
/*  40 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  41 */       setHp(48, 52);
/*     */     } else {
/*  43 */       setHp(46, 50);
/*     */     }
/*     */     
/*  46 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  47 */       this.stabDmg = 13;
/*  48 */       this.rakeDmg = 8;
/*     */     } else {
/*  50 */       this.stabDmg = 12;
/*  51 */       this.rakeDmg = 7;
/*     */     }
/*     */     
/*  54 */     this.damage.add(new DamageInfo(this, this.stabDmg));
/*  55 */     this.damage.add(new DamageInfo(this, this.rakeDmg));
/*     */     
/*  57 */     loadAnimation("images/monsters/theBottom/blueSlaver/skeleton.atlas", "images/monsters/theBottom/blueSlaver/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  61 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  62 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  67 */     switch (this.nextMove) {
/*     */     case 1: 
/*  69 */       playSfx();
/*  70 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  71 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  74 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/*  76 */       break;
/*     */     case 4: 
/*  78 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  79 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  82 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*     */       
/*     */ 
/*  85 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  86 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, this.weakAmt + 1, true), this.weakAmt + 1));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*  93 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, this.weakAmt, true), this.weakAmt));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 100 */       break;
/*     */     }
/*     */     
/*     */     
/* 104 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 108 */     int roll = MathUtils.random(1);
/* 109 */     if (roll == 0) {
/* 110 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("VO_SLAVERBLUE_1A"));
/*     */     } else {
/* 112 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("VO_SLAVERBLUE_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 117 */     int roll = MathUtils.random(1);
/* 118 */     if (roll == 0) {
/* 119 */       CardCrawlGame.sound.play("VO_SLAVERBLUE_2A");
/*     */     } else {
/* 121 */       CardCrawlGame.sound.play("VO_SLAVERBLUE_2B");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 129 */     if ((num >= 40) && (!lastTwoMoves((byte)1))) {
/* 130 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 131 */       return;
/*     */     }
/*     */     
/* 134 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 136 */       if (!lastMove((byte)4)) {
/* 137 */         setMove(MOVES[0], (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 138 */         return;
/*     */       }
/* 140 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 141 */       return;
/*     */     }
/*     */     
/*     */ 
/* 145 */     if (!lastTwoMoves((byte)4)) {
/* 146 */       setMove(MOVES[0], (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 147 */       return;
/*     */     }
/* 149 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 157 */     super.die();
/* 158 */     playDeathSfx();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\SlaverBlue.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */