/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Burn;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.BorderFlashEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ScreenOnFireEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Hexaghost extends AbstractMonster
/*     */ {
/*  39 */   private static final Logger logger = LogManager.getLogger(Hexaghost.class.getName());
/*     */   public static final String ID = "Hexaghost";
/*  41 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Hexaghost");
/*  42 */   public static final String NAME = monsterStrings.NAME;
/*  43 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  44 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final String IMAGE = "images/monsters/theBottom/boss/ghost/core.png";
/*     */   
/*     */   private static final int HP = 250;
/*     */   
/*     */   private static final int A_2_HP = 264;
/*  51 */   private ArrayList<HexaghostOrb> orbs = new ArrayList();
/*     */   private static final int SEAR_DMG = 6;
/*     */   private static final int INFERNO_DMG = 2;
/*     */   private static final int FIRE_TACKLE_DMG = 5;
/*     */   private static final int BURN_COUNT = 1;
/*     */   private static final int STR_AMT = 2;
/*     */   private static final int A_4_INFERNO_DMG = 3;
/*  58 */   private static final int A_4_FIRE_TACKLE_DMG = 6; private static final int A_19_BURN_COUNT = 2; private static final int A_19_STR_AMT = 3; private int searDmg; private int strengthenBlockAmt = 12; private int strAmount; private int searBurnCount; private int fireTackleDmg; private int fireTackleCount = 2; private int infernoDmg; private int infernoHits = 6;
/*     */   private static final byte DIVIDER = 1;
/*     */   private static final byte TACKLE = 2;
/*  61 */   private static final byte INFLAME = 3; private static final byte SEAR = 4; private static final byte ACTIVATE = 5; private static final byte INFERNO = 6; private static final String STRENGTHEN_NAME = MOVES[0]; private static final String SEAR_NAME = MOVES[1]; private static final String BURN_NAME = MOVES[2];
/*     */   
/*     */   private static final String ACTIVATE_STATE = "Activate";
/*     */   private static final String ACTIVATE_ORB = "Activate Orb";
/*     */   private static final String DEACTIVATE_ALL_ORBS = "Deactivate";
/*  66 */   private boolean activated = false; private boolean burnUpgraded = false;
/*  67 */   private int orbActiveCount = 0;
/*     */   private HexaghostBody body;
/*     */   
/*     */   public Hexaghost() {
/*  71 */     super(NAME, "Hexaghost", 250, 20.0F, 0.0F, 450.0F, 450.0F, "images/monsters/theBottom/boss/ghost/core.png");
/*  72 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  73 */     this.body = new HexaghostBody(this);
/*  74 */     createOrbs();
/*     */     
/*  76 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  77 */       setHp(264);
/*     */     } else {
/*  79 */       setHp(250);
/*     */     }
/*     */     
/*  82 */     if (AbstractDungeon.ascensionLevel >= 19) {
/*  83 */       this.strAmount = 3;
/*  84 */       this.searBurnCount = 2;
/*  85 */       this.fireTackleDmg = 6;
/*  86 */       this.infernoDmg = 3;
/*  87 */     } else if (AbstractDungeon.ascensionLevel >= 4) {
/*  88 */       this.strAmount = 2;
/*  89 */       this.searBurnCount = 1;
/*  90 */       this.fireTackleDmg = 6;
/*  91 */       this.infernoDmg = 3;
/*     */     } else {
/*  93 */       this.strAmount = 2;
/*  94 */       this.searBurnCount = 1;
/*  95 */       this.fireTackleDmg = 5;
/*  96 */       this.infernoDmg = 2;
/*     */     }
/*     */     
/*  99 */     this.searDmg = 6;
/* 100 */     this.damage.add(new DamageInfo(this, this.fireTackleDmg));
/* 101 */     this.damage.add(new DamageInfo(this, this.searDmg));
/* 102 */     this.damage.add(new DamageInfo(this, -1));
/* 103 */     this.damage.add(new DamageInfo(this, this.infernoDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/* 108 */     UnlockTracker.markBossAsSeen("GHOST");
/*     */   }
/*     */   
/*     */   private void createOrbs() {
/* 112 */     this.orbs.add(new HexaghostOrb(-90.0F, 380.0F, this.orbs.size()));
/* 113 */     this.orbs.add(new HexaghostOrb(90.0F, 380.0F, this.orbs.size()));
/* 114 */     this.orbs.add(new HexaghostOrb(160.0F, 250.0F, this.orbs.size()));
/* 115 */     this.orbs.add(new HexaghostOrb(90.0F, 120.0F, this.orbs.size()));
/* 116 */     this.orbs.add(new HexaghostOrb(-90.0F, 120.0F, this.orbs.size()));
/* 117 */     this.orbs.add(new HexaghostOrb(-160.0F, 250.0F, this.orbs.size()));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 122 */     switch (this.nextMove)
/*     */     {
/*     */     case 5: 
/* 125 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Activate"));
/* 126 */       int d = AbstractDungeon.player.currentHealth / 12 + 1;
/*     */       
/* 128 */       ((DamageInfo)this.damage.get(2)).base = d;
/*     */       
/* 130 */       applyPowers();
/* 131 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(2)).base, 6, true);
/* 132 */       break;
/*     */     
/*     */ 
/*     */     case 1: 
/* 136 */       for (int i = 0; i < 6; i++) {
/* 137 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.GhostIgniteEffect(AbstractDungeon.player.hb.cX + 
/*     */         
/*     */ 
/*     */ 
/* 141 */           MathUtils.random(-120.0F, 120.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/* 142 */           MathUtils.random(-120.0F, 120.0F) * Settings.scale), 0.05F));
/*     */         
/* 144 */         if (MathUtils.randomBoolean()) {
/* 145 */           AbstractDungeon.actionManager.addToBottom(new SFXAction("GHOST_ORB_IGNITE_1", 0.3F));
/*     */         } else {
/* 147 */           AbstractDungeon.actionManager.addToBottom(new SFXAction("GHOST_ORB_IGNITE_2", 0.3F));
/*     */         }
/* 149 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/* 152 */           (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.BLUNT_HEAVY, true));
/*     */       }
/*     */       
/*     */ 
/* 156 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Deactivate"));
/* 157 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 158 */       break;
/*     */     
/*     */ 
/*     */     case 2: 
/* 162 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new BorderFlashEffect(Color.CHARTREUSE)));
/* 163 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 164 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 165 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.FIRE));
/* 166 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 167 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.FIRE));
/* 168 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Activate Orb"));
/* 169 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 170 */       break;
/*     */     
/*     */ 
/*     */     case 4: 
/* 174 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.FireballEffect(this.hb.cX, this.hb.cY, AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY), 0.5F));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 182 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 183 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.FIRE));
/* 184 */       Burn c = new Burn();
/* 185 */       if (this.burnUpgraded) {
/* 186 */         c.upgrade();
/*     */       }
/* 188 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(c, this.searBurnCount));
/* 189 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Activate Orb"));
/* 190 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 191 */       break;
/*     */     
/*     */ 
/*     */     case 3: 
/* 195 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.InflameEffect(this), 0.5F));
/* 196 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, this.strengthenBlockAmt));
/* 197 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, this.strAmount), this.strAmount));
/*     */       
/* 199 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Activate Orb"));
/* 200 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 201 */       break;
/*     */     
/*     */ 
/*     */     case 6: 
/* 205 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new ScreenOnFireEffect(), 1.0F));
/* 206 */       for (int i = 0; i < this.infernoHits; i++) {
/* 207 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 208 */           (DamageInfo)this.damage.get(3), AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 210 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.BurnIncreaseAction());
/* 211 */       if (!this.burnUpgraded) {
/* 212 */         this.burnUpgraded = true;
/*     */       }
/* 214 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "Deactivate"));
/* 215 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 216 */       break;
/*     */     default: 
/* 218 */       logger.info("ERROR: Default Take Turn was called on " + this.name);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 225 */     if (!this.activated) {
/* 226 */       this.activated = true;
/* 227 */       setMove((byte)5, AbstractMonster.Intent.UNKNOWN);
/*     */     }
/*     */     else {
/* 230 */       switch (this.orbActiveCount) {
/*     */       case 0: 
/* 232 */         setMove(SEAR_NAME, (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 233 */         break;
/*     */       case 1: 
/* 235 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.fireTackleCount, true);
/* 236 */         break;
/*     */       case 2: 
/* 238 */         setMove(SEAR_NAME, (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 239 */         break;
/*     */       case 3: 
/* 241 */         setMove(STRENGTHEN_NAME, (byte)3, AbstractMonster.Intent.DEFEND_BUFF);
/* 242 */         break;
/*     */       case 4: 
/* 244 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, this.fireTackleCount, true);
/* 245 */         break;
/*     */       case 5: 
/* 247 */         setMove(SEAR_NAME, (byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 248 */         break;
/*     */       case 6: 
/* 250 */         setMove(BURN_NAME, (byte)6, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(3)).base, this.infernoHits, true);
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void changeState(String stateName)
/*     */   {
/* 262 */     switch (stateName)
/*     */     {
/*     */     case "Activate": 
/* 265 */       CardCrawlGame.music.unsilenceBGM();
/* 266 */       AbstractDungeon.scene.fadeOutAmbiance();
/* 267 */       AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BOTTOM");
/*     */       
/* 269 */       for (HexaghostOrb orb : this.orbs) {
/* 270 */         orb.activate(this.drawX + this.animX, this.drawY + this.animY);
/*     */       }
/* 272 */       this.orbActiveCount = 6;
/* 273 */       this.body.targetRotationSpeed = 120.0F;
/* 274 */       break;
/*     */     
/*     */ 
/*     */     case "Activate Orb": 
/* 278 */       for (HexaghostOrb orb : this.orbs) {
/* 279 */         if (!orb.activated) {
/* 280 */           orb.activate(this.drawX + this.animX, this.drawY + this.animY);
/* 281 */           break;
/*     */         }
/*     */       }
/* 284 */       this.orbActiveCount += 1;
/* 285 */       if (this.orbActiveCount == 6) {
/* 286 */         setMove(BURN_NAME, (byte)6, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(3)).base, this.infernoHits, true);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       break;
/*     */     case "Deactivate": 
/* 293 */       for (HexaghostOrb orb : this.orbs) {
/* 294 */         orb.deactivate();
/*     */       }
/* 296 */       CardCrawlGame.sound.play("CARD_EXHAUST", 0.2F);
/* 297 */       CardCrawlGame.sound.play("CARD_EXHAUST", 0.2F);
/* 298 */       this.orbActiveCount = 0;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 305 */     useFastShakeAnimation(5.0F);
/* 306 */     CardCrawlGame.screenShake.rumble(4.0F);
/* 307 */     this.deathTimer += 1.5F;
/* 308 */     super.die();
/*     */     
/* 310 */     for (HexaghostOrb orb : this.orbs) {
/* 311 */       orb.hide();
/*     */     }
/*     */     
/* 314 */     onBossVictoryLogic();
/* 315 */     UnlockTracker.hardUnlockOverride("GHOST");
/* 316 */     UnlockTracker.unlockAchievement("GHOST_GUARDIAN");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 324 */     super.update();
/* 325 */     this.body.update();
/*     */     
/* 327 */     for (HexaghostOrb orb : this.orbs) {
/* 328 */       orb.update(this.drawX + this.animX, this.drawY + this.animY);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 337 */     this.body.render(sb);
/* 338 */     super.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\Hexaghost.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */