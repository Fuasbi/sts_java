/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateShakeAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SuicideAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.CannotLoseAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Slimed;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.FrailPower;
/*     */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*     */ import com.megacrit.cardcrawl.powers.SplitPower;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SpikeSlime_L extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "SpikeSlime_L";
/*  37 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("SpikeSlime_L");
/*  38 */   public static final String NAME = monsterStrings.NAME;
/*  39 */   public static final String[] MOVES = monsterStrings.MOVES;
/*     */   public static final int HP_MIN = 64;
/*     */   public static final int HP_MAX = 70;
/*     */   public static final int A_2_HP_MIN = 67;
/*     */   public static final int A_2_HP_MAX = 73;
/*     */   public static final int TACKLE_DAMAGE = 16;
/*     */   public static final int A_2_TACKLE_DAMAGE = 18;
/*     */   public static final int FRAIL_TURNS = 2;
/*     */   public static final int WOUND_COUNT = 2;
/*  48 */   private static final byte FLAME_TACKLE = 1; private static final byte SPLIT = 3; private static final byte FRAIL_LICK = 4; private static final String FRAIL_NAME = MOVES[0]; private static final String SPLIT_NAME = MOVES[1];
/*     */   private float saveX;
/*     */   private float saveY;
/*     */   private boolean splitTriggered;
/*     */   
/*  53 */   public SpikeSlime_L(float x, float y) { this(x, y, 0, 70);
/*     */     
/*  55 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  56 */       setHp(67, 73);
/*     */     } else {
/*  58 */       setHp(64, 70);
/*     */     }
/*     */   }
/*     */   
/*     */   public SpikeSlime_L(float x, float y, int poisonAmount, int newHealth) {
/*  63 */     super(NAME, "SpikeSlime_L", newHealth, 0.0F, -30.0F, 300.0F, 180.0F, null, x, y, true);
/*     */     
/*  65 */     this.saveX = x;
/*  66 */     this.saveY = y;
/*  67 */     this.splitTriggered = false;
/*     */     
/*  69 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  70 */       this.damage.add(new DamageInfo(this, 18));
/*     */     } else {
/*  72 */       this.damage.add(new DamageInfo(this, 16));
/*     */     }
/*     */     
/*  75 */     this.powers.add(new SplitPower(this));
/*     */     
/*  77 */     if (poisonAmount >= 1) {
/*  78 */       this.powers.add(new PoisonPower(this, this, poisonAmount));
/*     */     }
/*     */     
/*  81 */     loadAnimation("images/monsters/theBottom/slimeAltL/skeleton.atlas", "images/monsters/theBottom/slimeAltL/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  85 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  86 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */ 
/*     */   public void takeTurn()
/*     */   {
/*  92 */     switch (this.nextMove) {
/*     */     case 4: 
/*  94 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  95 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  96 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new FrailPower(AbstractDungeon.player, 3, true), 3));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 103 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new FrailPower(AbstractDungeon.player, 2, true), 2));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 110 */       break;
/*     */     case 1: 
/* 112 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 113 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 116 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 118 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Slimed(), 2));
/* 119 */       break;
/*     */     case 3: 
/* 121 */       AbstractDungeon.actionManager.addToBottom(new CannotLoseAction());
/* 122 */       AbstractDungeon.actionManager.addToBottom(new AnimateShakeAction(this, 1.0F, 0.1F));
/* 123 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.HideHealthBarAction(this));
/* 124 */       AbstractDungeon.actionManager.addToBottom(new SuicideAction(this, false));
/* 125 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(1.0F));
/* 126 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("SLIME_SPLIT"));
/*     */       
/* 128 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new SpikeSlime_M(this.saveX - 134.0F, this.saveY + 
/*     */       
/* 130 */         MathUtils.random(-4.0F, 4.0F), 0, this.currentHealth), false));
/*     */       
/*     */ 
/* 133 */       AbstractDungeon.actionManager.addToBottom(new SpawnMonsterAction(new SpikeSlime_M(this.saveX + 134.0F, this.saveY + 
/*     */       
/* 135 */         MathUtils.random(-4.0F, 4.0F), 0, this.currentHealth), false));
/*     */       
/*     */ 
/* 138 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.CanLoseAction());
/* 139 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/*     */     }
/*     */     
/* 142 */     AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 147 */     super.damage(info);
/*     */     
/* 149 */     if ((!this.isDying) && (this.currentHealth <= this.maxHealth / 2.0F) && (this.nextMove != 3) && (!this.splitTriggered)) {
/* 150 */       setMove(SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN);
/* 151 */       createIntent();
/* 152 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this, TextAboveCreatureAction.TextType.INTERRUPTED));
/* 153 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, SPLIT_NAME, (byte)3, AbstractMonster.Intent.UNKNOWN));
/* 154 */       this.splitTriggered = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 161 */     if (AbstractDungeon.ascensionLevel >= 17)
/*     */     {
/* 163 */       if (num < 30) {
/* 164 */         if (lastTwoMoves((byte)1)) {
/* 165 */           setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         } else {
/* 167 */           setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */         }
/*     */       }
/* 170 */       else if (lastMove((byte)4)) {
/* 171 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       } else {
/* 173 */         setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 178 */     else if (num < 30) {
/* 179 */       if (lastTwoMoves((byte)1)) {
/* 180 */         setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/* 182 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */     }
/* 185 */     else if (lastTwoMoves((byte)4)) {
/* 186 */       setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */     } else {
/* 188 */       setMove(FRAIL_NAME, (byte)4, AbstractMonster.Intent.DEBUFF);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 196 */     super.die();
/*     */     
/* 198 */     for (com.megacrit.cardcrawl.actions.AbstractGameAction a : AbstractDungeon.actionManager.actions) {
/* 199 */       if ((a instanceof SpawnMonsterAction)) {
/* 200 */         return;
/*     */       }
/*     */     }
/*     */     
/* 204 */     if ((AbstractDungeon.getMonsters().areMonstersBasicallyDead()) && 
/* 205 */       ((AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss))) {
/* 206 */       onBossVictoryLogic();
/* 207 */       UnlockTracker.hardUnlockOverride("SLIME");
/* 208 */       UnlockTracker.unlockAchievement("SLIME_BOSS");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\SpikeSlime_L.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */