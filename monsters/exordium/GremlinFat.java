/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.SetMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinFat extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinFat";
/*  26 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("GremlinFat");
/*  27 */   public static final String NAME = monsterStrings.NAME;
/*  28 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  29 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 13;
/*     */   private static final int HP_MAX = 17;
/*     */   private static final int A_2_HP_MIN = 14;
/*     */   private static final int A_2_HP_MAX = 18;
/*     */   private static final int BLUNT_DAMAGE = 4;
/*     */   private static final int A_2_BLUNT_DAMAGE = 5;
/*     */   private static final int WEAK_AMT = 1;
/*     */   private static final byte BLUNT = 2;
/*     */   
/*     */   public GremlinFat(float x, float y)
/*     */   {
/*  41 */     super(NAME, "GremlinFat", 17, 0.0F, 0.0F, 110.0F, 220.0F, null, x, y);
/*  42 */     this.dialogY = (30.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*     */     
/*  44 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  45 */       setHp(14, 18);
/*     */     } else {
/*  47 */       setHp(13, 17);
/*     */     }
/*     */     
/*  50 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  51 */       this.damage.add(new DamageInfo(this, 5));
/*     */     } else {
/*  53 */       this.damage.add(new DamageInfo(this, 4));
/*     */     }
/*     */     
/*  56 */     loadAnimation("images/monsters/theBottom/fatGremlin/skeleton.atlas", "images/monsters/theBottom/fatGremlin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  60 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "animation", true);
/*  61 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  66 */     switch (this.nextMove) {
/*     */     case 2: 
/*  68 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  69 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  72 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/*  74 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  80 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  81 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 1, true), 1));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  88 */       if (this.escapeNext) {
/*  89 */         AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/*     */       } else {
/*  91 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */       }
/*  93 */       break;
/*     */     case 99: 
/*  95 */       playSfx();
/*  96 */       AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.5F, DIALOG[1], false));
/*     */       
/*  98 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(this));
/*  99 */       AbstractDungeon.actionManager.addToBottom(new SetMoveAction(this, (byte)99, AbstractMonster.Intent.ESCAPE));
/* 100 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void playSfx()
/*     */   {
/* 107 */     int roll = MathUtils.random(2);
/* 108 */     if (roll == 0) {
/* 109 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINFAT_1A"));
/* 110 */     } else if (roll == 1) {
/* 111 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINFAT_1B"));
/*     */     } else {
/* 113 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINFAT_1C"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 118 */     int roll = MathUtils.random(2);
/* 119 */     if (roll == 0) {
/* 120 */       CardCrawlGame.sound.play("VO_GREMLINFAT_2A");
/* 121 */     } else if (roll == 1) {
/* 122 */       CardCrawlGame.sound.play("VO_GREMLINFAT_2B");
/*     */     } else {
/* 124 */       CardCrawlGame.sound.play("VO_GREMLINFAT_2C");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 130 */     super.die();
/* 131 */     playDeathSfx();
/*     */   }
/*     */   
/*     */   public void escapeNext()
/*     */   {
/* 136 */     if ((!this.cannotEscape) && 
/* 137 */       (!this.escapeNext)) {
/* 138 */       this.escapeNext = true;
/* 139 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 147 */     setMove(MOVES[0], (byte)2, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */ 
/*     */   public void deathReact()
/*     */   {
/* 153 */     if ((this.intent != AbstractMonster.Intent.ESCAPE) && (!this.isDying)) {
/* 154 */       AbstractDungeon.effectList.add(new SpeechBubble(this.dialogX, this.dialogY, 3.0F, DIALOG[2], false));
/* 155 */       setMove((byte)99, AbstractMonster.Intent.ESCAPE);
/* 156 */       createIntent();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\GremlinFat.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */