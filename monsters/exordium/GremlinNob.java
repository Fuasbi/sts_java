/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.AngerPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinNob extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "GremlinNob";
/*  23 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("GremlinNob");
/*  24 */   public static final String NAME = monsterStrings.NAME;
/*  25 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  26 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 82;
/*     */   private static final int HP_MAX = 86;
/*     */   private static final int A_2_HP_MIN = 85;
/*     */   private static final int A_2_HP_MAX = 90;
/*     */   private static final int BASH_DMG = 6;
/*     */   private static final int RUSH_DMG = 14;
/*     */   private static final int A_2_BASH_DMG = 8;
/*     */   private static final int A_2_RUSH_DMG = 16;
/*     */   private static final int DEBUFF_AMT = 2;
/*     */   private int bashDmg;
/*     */   private int rushDmg;
/*     */   private static final byte BULL_RUSH = 1;
/*  39 */   private static final byte SKULL_BASH = 2; private static final byte BELLOW = 3; private static final int ANGRY_LEVEL = 2; private boolean usedBellow = false;
/*     */   private boolean canVuln;
/*     */   
/*     */   public GremlinNob(float x, float y) {
/*  43 */     this(x, y, true);
/*     */   }
/*     */   
/*     */   public GremlinNob(float x, float y, boolean setVuln) {
/*  47 */     super(NAME, "GremlinNob", 86, -70.0F, -10.0F, 270.0F, 380.0F, null, x, y);
/*  48 */     this.intentOffsetX = (-30.0F * Settings.scale);
/*  49 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*  50 */     this.dialogX = (-60.0F * Settings.scale);
/*  51 */     this.dialogY = (50.0F * Settings.scale);
/*  52 */     this.canVuln = setVuln;
/*     */     
/*  54 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  55 */       setHp(85, 90);
/*     */     } else {
/*  57 */       setHp(82, 86);
/*     */     }
/*     */     
/*  60 */     if (AbstractDungeon.ascensionLevel >= 3) {
/*  61 */       this.bashDmg = 8;
/*  62 */       this.rushDmg = 16;
/*     */     } else {
/*  64 */       this.bashDmg = 6;
/*  65 */       this.rushDmg = 14;
/*     */     }
/*     */     
/*  68 */     this.damage.add(new DamageInfo(this, this.rushDmg));
/*  69 */     this.damage.add(new DamageInfo(this, this.bashDmg));
/*     */     
/*  71 */     loadAnimation("images/monsters/theBottom/nobGremlin/skeleton.atlas", "images/monsters/theBottom/nobGremlin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  75 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "animation", true);
/*  76 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  81 */     CardCrawlGame.music.unsilenceBGM();
/*  82 */     AbstractDungeon.scene.fadeOutAmbiance();
/*  83 */     AbstractDungeon.getCurrRoom().playBgmInstantly("ELITE");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  88 */     switch (this.nextMove) {
/*     */     case 3: 
/*  90 */       playSfx();
/*  91 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.TalkAction(this, DIALOG[0], 1.0F, 3.0F));
/*  92 */       if (AbstractDungeon.ascensionLevel >= 18) {
/*  93 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new AngerPower(this, 3), 3));
/*     */       }
/*     */       else {
/*  96 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new AngerPower(this, 2), 2));
/*     */       }
/*     */       
/*  99 */       break;
/*     */     case 2: 
/* 101 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 102 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 105 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 107 */       if (this.canVuln) {
/* 108 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, 2, true), 2));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       break;
/*     */     case 1: 
/* 117 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 118 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 121 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 123 */       break;
/*     */     }
/*     */     
/*     */     
/* 127 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 131 */     int roll = MathUtils.random(2);
/* 132 */     if (roll == 0) {
/* 133 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINNOB_1A"));
/* 134 */     } else if (roll == 1) {
/* 135 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINNOB_1B"));
/*     */     } else {
/* 137 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_GREMLINNOB_1C"));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 145 */     if (!this.usedBellow) {
/* 146 */       this.usedBellow = true;
/* 147 */       setMove((byte)3, AbstractMonster.Intent.BUFF);
/* 148 */       return;
/*     */     }
/*     */     
/* 151 */     if (AbstractDungeon.ascensionLevel >= 18) {
/* 152 */       if ((!lastMove((byte)2)) && (!lastMoveBefore((byte)2))) {
/* 153 */         if (this.canVuln) {
/* 154 */           setMove(MOVES[0], (byte)2, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 156 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/* 158 */         return;
/*     */       }
/* 160 */       if (lastTwoMoves((byte)1)) {
/* 161 */         if (this.canVuln) {
/* 162 */           setMove(MOVES[0], (byte)2, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 164 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */       } else {
/* 167 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */     }
/*     */     else {
/* 171 */       if (num < 33) {
/* 172 */         if (this.canVuln) {
/* 173 */           setMove(MOVES[0], (byte)2, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 175 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/* 177 */         return;
/*     */       }
/*     */       
/*     */ 
/* 181 */       if (lastTwoMoves((byte)1)) {
/* 182 */         if (this.canVuln) {
/* 183 */           setMove(MOVES[0], (byte)2, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/*     */         } else {
/* 185 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */         }
/*     */       } else {
/* 188 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void die()
/*     */   {
/* 196 */     super.die();
/* 197 */     AbstractDungeon.scene.fadeInAmbiance();
/* 198 */     CardCrawlGame.music.fadeOutTempBGM();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\GremlinNob.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */