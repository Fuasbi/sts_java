/*     */ package com.megacrit.cardcrawl.monsters.exordium;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction.TextType;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.DexterityPower;
/*     */ import com.megacrit.cardcrawl.powers.MetallicizePower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Lagavulin extends AbstractMonster
/*     */ {
/*  31 */   private static final Logger logger = LogManager.getLogger(Lagavulin.class.getName());
/*     */   public static final String ID = "Lagavulin";
/*  33 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Lagavulin");
/*  34 */   public static final String NAME = monsterStrings.NAME;
/*  35 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  36 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 109;
/*     */   private static final int HP_MAX = 111;
/*     */   private static final int A_2_HP_MIN = 112;
/*     */   private static final int A_2_HP_MAX = 115;
/*     */   private static final byte DEBUFF = 1;
/*  42 */   private static final byte STRONG_ATK = 3; private static final byte OPEN = 4; private static final byte IDLE = 5; private static final byte OPEN_NATURAL = 6; private static final String DEBUFF_NAME = MOVES[0];
/*     */   private static final int STRONG_ATK_DMG = 18;
/*     */   private static final int DEBUFF_AMT = -1;
/*     */   private static final int A_18_DEBUFF_AMT = -2;
/*     */   private static final int A_2_STRONG_ATK_DMG = 20;
/*  47 */   private int attackDmg; private int debuff; private static final int ARMOR_AMT = 8; private boolean isOut = false;
/*  48 */   private boolean asleep; private boolean isOutTriggered = false;
/*  49 */   private int idleCount = 0; private int debuffTurnCount = 0;
/*     */   
/*     */   public Lagavulin(boolean setAsleep) {
/*  52 */     super(NAME, "Lagavulin", 111, 0.0F, -25.0F, 320.0F, 220.0F, null, 0.0F, 20.0F);
/*  53 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*  54 */     this.dialogX = (-100.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*     */     
/*  56 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  57 */       setHp(112, 115);
/*     */     } else {
/*  59 */       setHp(109, 111);
/*     */     }
/*     */     
/*  62 */     if (AbstractDungeon.ascensionLevel >= 3) {
/*  63 */       this.attackDmg = 20;
/*     */     } else {
/*  65 */       this.attackDmg = 18;
/*     */     }
/*     */     
/*  68 */     if (AbstractDungeon.ascensionLevel >= 18) {
/*  69 */       this.debuff = -2;
/*     */     } else {
/*  71 */       this.debuff = -1;
/*     */     }
/*     */     
/*  74 */     this.damage.add(new DamageInfo(this, this.attackDmg));
/*  75 */     this.asleep = setAsleep;
/*     */     
/*  77 */     loadAnimation("images/monsters/theBottom/lagavulin/skeleton.atlas", "images/monsters/theBottom/lagavulin/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  81 */     AnimationState.TrackEntry e = null;
/*  82 */     if (!this.asleep) {
/*  83 */       this.isOut = true;
/*  84 */       this.isOutTriggered = true;
/*  85 */       e = this.state.setAnimation(0, "Idle_2", true);
/*  86 */       updateHitbox(0.0F, -25.0F, 320.0F, 370.0F);
/*     */     } else {
/*  88 */       e = this.state.setAnimation(0, "Idle_1", true);
/*     */     }
/*  90 */     this.stateData.setMix("Attack", "Idle_2", 0.25F);
/*  91 */     this.stateData.setMix("Hit", "Idle_2", 0.25F);
/*  92 */     this.stateData.setMix("Idle_1", "Idle_2", 0.5F);
/*     */     
/*  94 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  99 */     if (this.asleep) {
/* 100 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, 8));
/* 101 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new MetallicizePower(this, 8), 8));
/*     */     }
/*     */     else {
/* 104 */       CardCrawlGame.music.unsilenceBGM();
/* 105 */       AbstractDungeon.scene.fadeOutAmbiance();
/* 106 */       AbstractDungeon.getCurrRoom().playBgmInstantly("ELITE");
/* 107 */       setMove(DEBUFF_NAME, (byte)1, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 113 */     switch (this.nextMove)
/*     */     {
/*     */ 
/*     */     case 1: 
/* 117 */       this.debuffTurnCount = 0;
/* 118 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "DEBUFF"));
/* 119 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 120 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new DexterityPower(AbstractDungeon.player, this.debuff), this.debuff));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 126 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, this.debuff), this.debuff));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 132 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 133 */       break;
/*     */     case 3: 
/* 135 */       this.debuffTurnCount += 1;
/* 136 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 137 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 138 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 141 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 143 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 144 */       break;
/*     */     case 5: 
/* 146 */       this.idleCount += 1;
/* 147 */       if (this.idleCount >= 3) {
/* 148 */         logger.info("idle happened");
/* 149 */         this.isOutTriggered = true;
/* 150 */         AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "OPEN"));
/* 151 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetMoveAction(this, (byte)3, AbstractMonster.Intent.ATTACK, 
/* 152 */           ((DamageInfo)this.damage.get(0)).base));
/*     */       } else {
/* 154 */         setMove((byte)5, AbstractMonster.Intent.SLEEP);
/*     */       }
/* 156 */       switch (this.idleCount) {
/*     */       case 1: 
/* 158 */         AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[1], 0.5F, 2.0F));
/* 159 */         AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 160 */         break;
/*     */       case 2: 
/* 162 */         AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[2], 0.5F, 2.0F));
/* 163 */         AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*     */       }
/*     */       
/* 166 */       break;
/*     */     
/*     */ 
/*     */     case 4: 
/* 170 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this, TextAboveCreatureAction.TextType.STUNNED));
/* 171 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 172 */       break;
/*     */     case 6: 
/* 174 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "OPEN"));
/* 175 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 176 */       createIntent();
/* 177 */       this.isOutTriggered = true;
/* 178 */       AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/* 179 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void changeState(String stateName)
/*     */   {
/* 190 */     if (stateName.equals("ATTACK")) {
/* 191 */       this.state.setAnimation(0, "Attack", false);
/* 192 */       this.state.addAnimation(0, "Idle_2", true, 0.0F);
/* 193 */     } else if (stateName.equals("DEBUFF")) {
/* 194 */       this.state.setAnimation(0, "Debuff", false);
/* 195 */       this.state.addAnimation(0, "Idle_2", true, 0.0F);
/* 196 */     } else if (stateName.equals("OPEN")) {
/* 197 */       this.isOut = true;
/* 198 */       updateHitbox(0.0F, -25.0F, 320.0F, 360.0F);
/* 199 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[3], 0.5F, 2.0F));
/* 200 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this, this, "Metallicize", 8));
/*     */       
/* 202 */       CardCrawlGame.music.unsilenceBGM();
/* 203 */       AbstractDungeon.scene.fadeOutAmbiance();
/* 204 */       AbstractDungeon.getCurrRoom().playBgmInstantly("ELITE");
/* 205 */       this.state.setAnimation(0, "Coming_out", false);
/* 206 */       this.state.addAnimation(0, "Idle_2", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 212 */     super.damage(info);
/*     */     
/* 214 */     if ((this.currentHealth != this.maxHealth) && (!this.isOutTriggered)) {
/* 215 */       setMove((byte)4, AbstractMonster.Intent.STUN);
/* 216 */       createIntent();
/* 217 */       this.isOutTriggered = true;
/* 218 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "OPEN"));
/* 219 */     } else if ((this.isOutTriggered) && (info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0))
/*     */     {
/* 221 */       this.state.setAnimation(0, "Hit", false);
/* 222 */       this.state.addAnimation(0, "Idle_2", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 228 */     if (this.isOut) {
/* 229 */       if (this.debuffTurnCount < 2) {
/* 230 */         if (lastTwoMoves((byte)3)) {
/* 231 */           setMove(DEBUFF_NAME, (byte)1, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */         } else {
/* 233 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */         }
/*     */       } else {
/* 236 */         setMove(DEBUFF_NAME, (byte)1, AbstractMonster.Intent.STRONG_DEBUFF);
/*     */       }
/*     */     } else {
/* 239 */       setMove((byte)5, AbstractMonster.Intent.SLEEP);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 245 */     super.die();
/* 246 */     AbstractDungeon.scene.fadeInAmbiance();
/* 247 */     CardCrawlGame.music.fadeOutTempBGM();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\exordium\Lagavulin.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */