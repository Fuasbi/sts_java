/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SnakeDagger extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Dagger";
/*  21 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Dagger");
/*  22 */   public static final String NAME = monsterStrings.NAME;
/*  23 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  24 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 16;
/*     */   private static final int HP_MAX = 20;
/*     */   private static final int STAB_DMG = 9;
/*     */   private static final int SACRIFICE_DMG = 25;
/*  29 */   private static final byte WOUND = 1; private static final byte EXPLODE = 2; public boolean firstMove = true;
/*     */   
/*     */   public SnakeDagger(float x, float y) {
/*  32 */     super(NAME, "Dagger", AbstractDungeon.monsterHpRng.random(16, 20), 0.0F, -50.0F, 140.0F, 130.0F, null, x, y + 25.0F);
/*  33 */     initializeAnimation();
/*     */     
/*     */ 
/*  36 */     this.damage.add(new DamageInfo(this, 9));
/*     */     
/*     */ 
/*  39 */     this.damage.add(new DamageInfo(this, 25));
/*  40 */     this.damage.add(new DamageInfo(this, 25, DamageInfo.DamageType.HP_LOSS));
/*     */   }
/*     */   
/*     */   public void initializeAnimation() {
/*  44 */     loadAnimation("images/monsters/theForest/mage_dagger/skeleton.atlas", "images/monsters/theForest/mage_dagger/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  49 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  50 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  55 */     switch (this.nextMove) {
/*     */     case 1: 
/*  57 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  58 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/*  59 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  62 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/*  64 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Wound(), 1));
/*  65 */       break;
/*     */     case 2: 
/*  67 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "SUICIDE"));
/*  68 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.4F));
/*  69 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  72 */         (DamageInfo)this.damage.get(1), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */       
/*  74 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.LoseHPAction(this, this, this.currentHealth));
/*     */     }
/*     */     
/*  77 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/*  82 */     super.damage(info);
/*  83 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/*  84 */       this.state.setAnimation(0, "Hurt", false);
/*  85 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*  86 */       this.stateData.setMix("Hurt", "Idle", 0.1F);
/*  87 */       this.stateData.setMix("Idle", "Hurt", 0.1F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/*  93 */     if (this.firstMove) {
/*  94 */       this.firstMove = false;
/*  95 */       setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, 9);
/*  96 */       return;
/*     */     }
/*     */     
/*  99 */     setMove((byte)2, AbstractMonster.Intent.ATTACK, 25);
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 104 */     switch (key) {
/*     */     case "ATTACK": 
/* 106 */       this.state.setAnimation(0, "Attack", false);
/* 107 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 108 */       break;
/*     */     case "SUICIDE": 
/* 110 */       this.state.setAnimation(0, "Attack2", false);
/* 111 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 112 */       break;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\SnakeDagger.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */