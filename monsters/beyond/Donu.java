/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Donu extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Donu";
/*  24 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Donu");
/*  25 */   public static final String NAME = monsterStrings.NAME;
/*  26 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  27 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final int HP = 250;
/*     */   public static final int A_2_HP = 265;
/*     */   private static final byte BEAM = 0;
/*     */   private static final byte CIRCLE_OF_PROTECTION = 2;
/*     */   private static final int ARTIFACT_AMT = 2;
/*     */   private static final int BEAM_DMG = 10;
/*     */   private static final int BEAM_AMT = 2;
/*     */   private static final int A_2_BEAM_DMG = 12;
/*     */   private int beamDmg;
/*  38 */   private static final String CIRCLE_NAME = MOVES[0];
/*     */   private static final int CIRCLE_STR_AMT = 3;
/*     */   private boolean isAttacking;
/*     */   
/*     */   public Donu()
/*     */   {
/*  44 */     super(NAME, "Donu", 250, 0.0F, -20.0F, 390.0F, 390.0F, null, 100.0F, 20.0F);
/*  45 */     loadAnimation("images/monsters/theForest/donu/skeleton.atlas", "images/monsters/theForest/donu/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  49 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  50 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  51 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  52 */     this.stateData.setMix("Attack_2", "Idle", 0.1F);
/*     */     
/*  54 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  55 */     this.dialogX = (-200.0F * Settings.scale);
/*  56 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/*  58 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  59 */       setHp(265);
/*     */     } else {
/*  61 */       setHp(250);
/*     */     }
/*     */     
/*  64 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  65 */       this.beamDmg = 12;
/*     */     } else {
/*  67 */       this.beamDmg = 10;
/*     */     }
/*     */     
/*  70 */     this.damage.add(new DamageInfo(this, this.beamDmg));
/*  71 */     this.isAttacking = false;
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/*  76 */     switch (stateName) {
/*     */     case "ATTACK": 
/*  78 */       this.state.setAnimation(0, "Attack_2", false);
/*  79 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/*  86 */     super.damage(info);
/*  87 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/*  88 */       this.state.setAnimation(0, "Hit", false);
/*  89 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  95 */     if (AbstractDungeon.ascensionLevel >= 19) {
/*  96 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ArtifactPower(this, 3)));
/*     */     }
/*     */     else {
/*  99 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ArtifactPower(this, 2)));
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*     */     int i;
/* 106 */     switch (this.nextMove) {
/*     */     case 0: 
/* 108 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 109 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.5F));
/* 110 */       for (i = 0; i < 2; i++) {
/* 111 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/* 112 */           (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 114 */       this.isAttacking = false;
/* 115 */       break;
/*     */     case 2: 
/* 117 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 118 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new com.megacrit.cardcrawl.powers.StrengthPower(m, 3), 3));
/*     */       }
/*     */       
/* 121 */       this.isAttacking = true;
/*     */     }
/*     */     
/* 124 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 129 */     if (this.isAttacking) {
/* 130 */       setMove((byte)0, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */     } else {
/* 132 */       setMove(CIRCLE_NAME, (byte)2, AbstractMonster.Intent.BUFF);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 138 */     super.die();
/* 139 */     if (AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 140 */       useFastShakeAnimation(5.0F);
/* 141 */       CardCrawlGame.screenShake.rumble(4.0F);
/* 142 */       this.deathTimer += 1.5F;
/* 143 */       onBossVictoryLogic();
/* 144 */       UnlockTracker.hardUnlockOverride("DONUT");
/* 145 */       UnlockTracker.unlockAchievement("SHAPES");
/* 146 */       onFinalBossVictoryLogic();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Donu.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */