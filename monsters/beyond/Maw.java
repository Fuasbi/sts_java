/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.ShoutAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Maw extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Maw";
/*  24 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Maw");
/*  25 */   public static final String NAME = monsterStrings.NAME;
/*  26 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  27 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP = 300;
/*     */   private static final float HB_X = 0.0F;
/*     */   private static final float HB_Y = -40.0F;
/*     */   private static final float HB_W = 430.0F;
/*     */   private static final float HB_H = 360.0F;
/*     */   private static final int SLAM_DMG = 25;
/*     */   private static final int NOM_DMG = 5;
/*     */   private static final int A_2_SLAM_DMG = 30;
/*  36 */   private int slamDmg; private int nomDmg; private static final byte ROAR = 2; private static final byte SLAM = 3; private static final byte DROOL = 4; private static final byte NOMNOMNOM = 5; private boolean roared = false;
/*  37 */   private int turnCount = 1;
/*     */   private int strUp;
/*     */   
/*  40 */   public Maw(float x, float y) { super(NAME, "Maw", 300, 0.0F, -40.0F, 430.0F, 360.0F, null, x, y);
/*  41 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*     */     
/*  43 */     loadAnimation("images/monsters/theForest/maw/skeleton.atlas", "images/monsters/theForest/maw/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  47 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  48 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*     */     
/*  50 */     this.dialogX = (-160.0F * Settings.scale);
/*  51 */     this.dialogY = (40.0F * Settings.scale);
/*     */     
/*  53 */     this.strUp = 3;
/*  54 */     this.terrifyDur = 3;
/*  55 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  56 */       this.strUp += 2;
/*  57 */       this.terrifyDur += 2;
/*     */     }
/*     */     
/*  60 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  61 */       this.slamDmg = 30;
/*  62 */       this.nomDmg = 5;
/*     */     } else {
/*  64 */       this.slamDmg = 25;
/*  65 */       this.nomDmg = 5;
/*     */     }
/*     */     
/*  68 */     this.damage.add(new DamageInfo(this, this.slamDmg));
/*  69 */     this.damage.add(new DamageInfo(this, this.nomDmg));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  74 */     switch (this.nextMove) {
/*     */     case 2: 
/*  76 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("MAW_DEATH", 0.1F));
/*  77 */       AbstractDungeon.actionManager.addToBottom(new ShoutAction(this, DIALOG[0], 1.0F, 2.0F));
/*  78 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, this.terrifyDur, true), this.terrifyDur));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  84 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, this.terrifyDur, true), this.terrifyDur));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  90 */       this.roared = true;
/*  91 */       break;
/*     */     case 3: 
/*  93 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  94 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  97 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  99 */       break;
/*     */     case 4: 
/* 101 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.StrengthPower(this, this.strUp), this.strUp));
/*     */       
/* 103 */       break;
/*     */     case 5: 
/* 105 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 106 */       for (int i = 0; i < this.turnCount / 2; i++) {
/* 107 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */         
/*     */ 
/* 110 */           (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       }
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 116 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 121 */     this.turnCount += 1;
/* 122 */     if (!this.roared) {
/* 123 */       setMove((byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/* 124 */       return;
/*     */     }
/*     */     
/* 127 */     if ((num < 50) && (!lastMove((byte)5))) {
/* 128 */       if (this.turnCount / 2 <= 1) {
/* 129 */         setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       } else {
/* 131 */         setMove((byte)5, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base, this.turnCount / 2, true);
/*     */       }
/* 133 */       return;
/*     */     }
/*     */     
/* 136 */     if ((lastMove((byte)3)) || (lastMove((byte)5))) {
/* 137 */       setMove((byte)4, AbstractMonster.Intent.BUFF);
/* 138 */       return;
/*     */     }
/* 140 */     setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */ 
/*     */   private int terrifyDur;
/*     */   public void die()
/*     */   {
/* 147 */     super.die();
/* 148 */     CardCrawlGame.sound.play("MAW_DEATH");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Maw.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */