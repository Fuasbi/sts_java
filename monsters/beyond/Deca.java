/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Deca extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Deca";
/*  27 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Deca");
/*  28 */   public static final String NAME = monsterStrings.NAME;
/*  29 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  30 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final String ENC_NAME = "Donu and Deca";
/*     */   
/*     */   public static final int HP = 250;
/*     */   public static final int A_2_HP = 265;
/*     */   private static final byte BEAM = 0;
/*     */   private static final byte SQUARE_OF_PROTECTION = 2;
/*     */   private static final int ARTIFACT_AMT = 2;
/*     */   private static final int BEAM_DMG = 10;
/*     */   private static final int BEAM_AMT = 2;
/*     */   private static final int A_2_BEAM_DMG = 12;
/*     */   private int beamDmg;
/*     */   private static final int BEAM_DAZE_AMT = 2;
/*     */   private static final int PROTECT_BLOCK = 16;
/*     */   private boolean isAttacking;
/*     */   
/*     */   public Deca()
/*     */   {
/*  49 */     super(NAME, "Deca", 250, 0.0F, -26.0F, 390.0F, 390.0F, null, -350.0F, 30.0F);
/*  50 */     loadAnimation("images/monsters/theForest/deca/skeleton.atlas", "images/monsters/theForest/deca/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  54 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  55 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  56 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  57 */     this.stateData.setMix("Attack_2", "Idle", 0.1F);
/*     */     
/*  59 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  60 */     this.dialogX = (-200.0F * Settings.scale);
/*  61 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/*  63 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  64 */       setHp(265);
/*     */     } else {
/*  66 */       setHp(250);
/*     */     }
/*     */     
/*  69 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  70 */       this.beamDmg = 12;
/*     */     } else {
/*  72 */       this.beamDmg = 10;
/*     */     }
/*     */     
/*  75 */     this.damage.add(new DamageInfo(this, this.beamDmg));
/*  76 */     this.isAttacking = true;
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/*  81 */     switch (stateName) {
/*     */     case "ATTACK": 
/*  83 */       this.state.setAnimation(0, "Attack_2", false);
/*  84 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/*  91 */     super.damage(info);
/*  92 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/*  93 */       this.state.setAnimation(0, "Hit", false);
/*  94 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/* 100 */     CardCrawlGame.music.unsilenceBGM();
/* 101 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 102 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BEYOND");
/* 103 */     if (AbstractDungeon.ascensionLevel >= 19) {
/* 104 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ArtifactPower(this, 3)));
/*     */     }
/*     */     else {
/* 107 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ArtifactPower(this, 2)));
/*     */     }
/*     */     
/* 110 */     UnlockTracker.markBossAsSeen("DONUT");
/*     */   }
/*     */   
/*     */   public void takeTurn() {
/*     */     int i;
/* 115 */     switch (this.nextMove) {
/*     */     case 0: 
/* 117 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 118 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 119 */       for (i = 0; i < 2; i++) {
/* 120 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/* 121 */           (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 123 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Dazed(), 2));
/* 124 */       this.isAttacking = false;
/* 125 */       break;
/*     */     case 2: 
/* 127 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 128 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(m, this, 16));
/* 129 */         if (AbstractDungeon.ascensionLevel >= 19) {
/* 130 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, this, new com.megacrit.cardcrawl.powers.PlatedArmorPower(m, 3), 3));
/*     */         }
/*     */       }
/*     */       
/* 134 */       this.isAttacking = true;
/*     */     }
/*     */     
/* 137 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 142 */     if (this.isAttacking) {
/* 143 */       setMove((byte)0, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */     }
/* 145 */     else if (AbstractDungeon.ascensionLevel >= 19) {
/* 146 */       setMove((byte)2, AbstractMonster.Intent.DEFEND_BUFF);
/*     */     } else {
/* 148 */       setMove((byte)2, AbstractMonster.Intent.DEFEND);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void die()
/*     */   {
/* 155 */     super.die();
/* 156 */     if (AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 157 */       useFastShakeAnimation(5.0F);
/* 158 */       CardCrawlGame.screenShake.rumble(4.0F);
/* 159 */       onBossVictoryLogic();
/* 160 */       UnlockTracker.hardUnlockOverride("DONUT");
/* 161 */       UnlockTracker.unlockAchievement("SHAPES");
/* 162 */       onFinalBossVictoryLogic();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Deca.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */