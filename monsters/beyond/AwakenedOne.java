/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.esotericsoftware.spine.Bone;
/*     */ import com.esotericsoftware.spine.Skeleton;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.ShoutAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Void;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.exordium.Cultist;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.powers.CuriosityPower;
/*     */ import com.megacrit.cardcrawl.powers.RegenerateMonsterPower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.powers.UnawakenedPower;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.AwakenedEyeParticle;
/*     */ import com.megacrit.cardcrawl.vfx.AwakenedWingParticle;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect.ShockWaveType;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class AwakenedOne extends AbstractMonster
/*     */ {
/*  58 */   private static final Logger logger = LogManager.getLogger(AwakenedOne.class.getName());
/*     */   public static final String ID = "AwakenedOne";
/*  60 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("AwakenedOne");
/*  61 */   public static final String NAME = monsterStrings.NAME;
/*  62 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  63 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*  64 */   private boolean form1 = true; private boolean firstTurn = true; private boolean saidPower = false;
/*     */   public static final int STAGE_1_HP = 300;
/*     */   public static final int STAGE_2_HP = 300;
/*     */   public static final int A_9_STAGE_1_HP = 320;
/*     */   public static final int A_9_STAGE_2_HP = 320;
/*     */   private static final int A_4_STR = 2;
/*     */   private static final byte SLASH = 1;
/*     */   private static final byte SOUL_STRIKE = 2;
/*     */   private static final byte REBIRTH = 3;
/*  73 */   private static final String SS_NAME = MOVES[0];
/*     */   private static final int SLASH_DMG = 20;
/*     */   private static final int SS_DMG = 6;
/*     */   private static final int SS_AMT = 4;
/*     */   private static final int REGEN_AMT = 10;
/*  78 */   private static final int STR_AMT = 1; private static final byte DARK_ECHO = 5; private static final byte SLUDGE = 6; private static final byte TACKLE = 8; private static final String DARK_ECHO_NAME = MOVES[1]; private static final String SLUDGE_NAME = MOVES[3];
/*     */   private static final int ECHO_DMG = 40;
/*     */   private static final int SLUDGE_DMG = 18;
/*     */   private static final int TACKLE_DMG = 10;
/*  82 */   private static final int TACKLE_AMT = 3; private float fireTimer = 0.0F;
/*     */   private static final float FIRE_TIME = 0.1F;
/*     */   private Bone eye;
/*  85 */   private Bone back; private boolean animateParticles = false;
/*  86 */   private ArrayList<AwakenedWingParticle> wParticles = new ArrayList();
/*     */   
/*     */   public AwakenedOne(float x, float y) {
/*  89 */     super(NAME, "AwakenedOne", 300, 40.0F, -30.0F, 460.0F, 250.0F, null, x, y);
/*     */     
/*  91 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  92 */       setHp(320);
/*     */     } else {
/*  94 */       setHp(300);
/*     */     }
/*     */     
/*  97 */     loadAnimation("images/monsters/theForest/awakenedOne/skeleton.atlas", "images/monsters/theForest/awakenedOne/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/* 101 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle_1", true);
/* 102 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/* 103 */     this.stateData.setMix("Hit", "Idle_1", 0.3F);
/* 104 */     this.stateData.setMix("Hit", "Idle_2", 0.2F);
/* 105 */     this.stateData.setMix("Attack_1", "Idle_1", 0.2F);
/* 106 */     this.stateData.setMix("Attack_2", "Idle_2", 0.2F);
/* 107 */     this.state.getData().setMix("Idle_1", "Idle_2", 1.0F);
/*     */     
/* 109 */     this.eye = this.skeleton.findBone("Eye");
/*     */     
/* 111 */     for (Bone b : this.skeleton.getBones()) {
/* 112 */       logger.info(b.getData().getName());
/*     */     }
/* 114 */     this.back = this.skeleton.findBone("Hips");
/*     */     
/* 116 */     this.type = AbstractMonster.EnemyType.BOSS;
/* 117 */     this.dialogX = (-200.0F * Settings.scale);
/* 118 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/* 120 */     this.damage.add(new DamageInfo(this, 20));
/* 121 */     this.damage.add(new DamageInfo(this, 6));
/* 122 */     this.damage.add(new DamageInfo(this, 40));
/* 123 */     this.damage.add(new DamageInfo(this, 18));
/* 124 */     this.damage.add(new DamageInfo(this, 10));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/* 129 */     CardCrawlGame.music.unsilenceBGM();
/* 130 */     AbstractDungeon.scene.fadeOutAmbiance();
/* 131 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BEYOND");
/* 132 */     AbstractDungeon.getCurrRoom().cannotLose = true;
/*     */     
/* 134 */     if (AbstractDungeon.ascensionLevel >= 19) {
/* 135 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new RegenerateMonsterPower(this, 15)));
/*     */       
/* 137 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CuriosityPower(this, 2)));
/*     */     }
/*     */     else {
/* 140 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new RegenerateMonsterPower(this, 10)));
/*     */       
/* 142 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new CuriosityPower(this, 1)));
/*     */     }
/*     */     
/* 145 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new UnawakenedPower(this)));
/*     */     
/* 147 */     if (AbstractDungeon.ascensionLevel >= 4) {
/* 148 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 2), 2));
/*     */     }
/*     */     
/*     */ 
/* 152 */     UnlockTracker.markBossAsSeen("CROW");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 157 */     switch (this.nextMove) {
/*     */     case 1: 
/* 159 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK_1"));
/* 160 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 161 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 162 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/* 163 */       break;
/*     */     case 2: 
/* 165 */       for (int i = 0; i < 4; i++) {
/* 166 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 167 */           (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 169 */       break;
/*     */     case 3: 
/* 171 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_AWAKENEDONE_1"));
/* 172 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new com.megacrit.cardcrawl.vfx.combat.IntenseZoomEffect(this.hb.cX, this.hb.cY, true), 0.05F, true));
/*     */       
/* 174 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REBIRTH"));
/* 175 */       break;
/*     */     case 5: 
/* 177 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK_2"));
/* 178 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.1F));
/* 179 */       this.firstTurn = false;
/* 180 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_AWAKENEDONE_3"));
/* 181 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new ShockWaveEffect(this.hb.cX, this.hb.cY, new Color(0.1F, 0.0F, 0.2F, 1.0F), ShockWaveEffect.ShockWaveType.CHAOTIC), 0.3F));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 186 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(this, new ShockWaveEffect(this.hb.cX, this.hb.cY, new Color(0.3F, 0.2F, 0.4F, 1.0F), ShockWaveEffect.ShockWaveType.CHAOTIC), 1.0F));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 191 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 192 */         (DamageInfo)this.damage.get(2), AbstractGameAction.AttackEffect.SMASH));
/* 193 */       break;
/*     */     case 6: 
/* 195 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK_2"));
/* 196 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/* 197 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 198 */         (DamageInfo)this.damage.get(3), AbstractGameAction.AttackEffect.POISON));
/* 199 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(new Void(), 1, true, true));
/* 200 */       break;
/*     */     case 8: 
/* 202 */       for (int i = 0; i < 3; i++) {
/* 203 */         AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/* 204 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(0.06F));
/* 205 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 206 */           (DamageInfo)this.damage.get(4), AbstractGameAction.AttackEffect.FIRE, true));
/*     */       }
/*     */     }
/*     */     
/* 210 */     AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 215 */     switch (key) {
/*     */     case "REBIRTH": 
/* 217 */       if (AbstractDungeon.ascensionLevel >= 9) {
/* 218 */         this.maxHealth = 320;
/*     */       } else {
/* 220 */         this.maxHealth = 300;
/*     */       }
/* 222 */       if ((Settings.isEndless) && (AbstractDungeon.player.hasBlight("ToughEnemies"))) {
/* 223 */         float mod = AbstractDungeon.player.getBlight("ToughEnemies").effectFloat();
/* 224 */         this.maxHealth = ((int)(this.maxHealth * mod));
/*     */       }
/*     */       
/* 227 */       this.state.setAnimation(0, "Idle_2", true);
/* 228 */       this.halfDead = false;
/* 229 */       this.animateParticles = true;
/*     */       
/* 231 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(this, this, this.maxHealth));
/* 232 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.CanLoseAction());
/* 233 */       break;
/*     */     case "ATTACK_1": 
/* 235 */       this.state.setAnimation(0, "Attack_1", false);
/* 236 */       this.state.addAnimation(0, "Idle_1", true, 0.0F);
/* 237 */       break;
/*     */     case "ATTACK_2": 
/* 239 */       this.state.setAnimation(0, "Attack_2", false);
/* 240 */       this.state.addAnimation(0, "Idle_2", true, 0.0F);
/* 241 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 251 */     if (this.form1) {
/* 252 */       if (this.firstTurn) {
/* 253 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, 20);
/* 254 */         this.firstTurn = false;
/* 255 */         return;
/*     */       }
/*     */       
/*     */ 
/* 259 */       if (num < 25) {
/* 260 */         if (!lastMove((byte)2)) {
/* 261 */           setMove(SS_NAME, (byte)2, AbstractMonster.Intent.ATTACK, 6, 4, true);
/*     */         } else {
/* 263 */           setMove((byte)1, AbstractMonster.Intent.ATTACK, 20);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 268 */       else if (!lastTwoMoves((byte)1)) {
/* 269 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, 20);
/*     */       } else {
/* 271 */         setMove(SS_NAME, (byte)2, AbstractMonster.Intent.ATTACK, 6, 4, true);
/*     */       }
/*     */       
/*     */     }
/*     */     else
/*     */     {
/* 277 */       if (this.firstTurn) {
/* 278 */         setMove(DARK_ECHO_NAME, (byte)5, AbstractMonster.Intent.ATTACK, 40);
/* 279 */         return;
/*     */       }
/*     */       
/* 282 */       if (num < 50) {
/* 283 */         if (!lastTwoMoves((byte)6)) {
/* 284 */           setMove(SLUDGE_NAME, (byte)6, AbstractMonster.Intent.ATTACK_DEBUFF, 18);
/*     */         } else {
/* 286 */           setMove((byte)8, AbstractMonster.Intent.ATTACK, 10, 3, true);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/* 291 */       else if (!lastTwoMoves((byte)8)) {
/* 292 */         setMove((byte)8, AbstractMonster.Intent.ATTACK, 10, 3, true);
/*     */       } else {
/* 294 */         setMove(SLUDGE_NAME, (byte)6, AbstractMonster.Intent.ATTACK_DEBUFF, 18);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 302 */     super.damage(info);
/*     */     
/* 304 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 305 */       this.state.setAnimation(0, "Hit", false);
/* 306 */       if (this.form1) {
/* 307 */         this.state.addAnimation(0, "Idle_1", true, 0.0F);
/*     */       } else {
/* 309 */         this.state.addAnimation(0, "Idle_2", true, 0.0F);
/*     */       }
/*     */     }
/*     */     
/* 313 */     if ((this.currentHealth <= 0) && (!this.halfDead)) {
/* 314 */       this.halfDead = true;
/* 315 */       for (AbstractPower p : this.powers) {
/* 316 */         p.onDeath();
/*     */       }
/* 318 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 319 */         r.onMonsterDeath(this);
/*     */       }
/* 321 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.ClearCardQueueAction());
/*     */       
/* 323 */       for (Object s = this.powers.iterator(); ((Iterator)s).hasNext();) {
/* 324 */         AbstractPower p = (AbstractPower)((Iterator)s).next();
/* 325 */         if ((p.type == com.megacrit.cardcrawl.powers.AbstractPower.PowerType.DEBUFF) || (p.ID.equals("Curiosity")) || (p.ID.equals("Unawakened")))
/*     */         {
/* 327 */           ((Iterator)s).remove();
/*     */         }
/*     */       }
/*     */       
/* 331 */       setMove((byte)3, AbstractMonster.Intent.UNKNOWN);
/* 332 */       createIntent();
/* 333 */       AbstractDungeon.actionManager.addToBottom(new ShoutAction(this, DIALOG[0]));
/* 334 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetMoveAction(this, (byte)3, AbstractMonster.Intent.UNKNOWN));
/* 335 */       applyPowers();
/* 336 */       this.firstTurn = true;
/* 337 */       this.form1 = false;
/*     */       
/*     */ 
/* 340 */       if (GameActionManager.turn <= 1) {
/* 341 */         UnlockTracker.unlockAchievement("YOU_ARE_NOTHING");
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 348 */     super.update();
/* 349 */     if ((!this.isDying) && (this.animateParticles)) {
/* 350 */       this.fireTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 351 */       if (this.fireTimer < 0.0F) {
/* 352 */         this.fireTimer = 0.1F;
/* 353 */         AbstractDungeon.effectList.add(new AwakenedEyeParticle(this.skeleton
/* 354 */           .getX() + this.eye.getWorldX(), this.skeleton.getY() + this.eye.getWorldY()));
/* 355 */         this.wParticles.add(new AwakenedWingParticle());
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 360 */     for (Iterator<AwakenedWingParticle> p = this.wParticles.iterator(); p.hasNext();) {
/* 361 */       AwakenedWingParticle e = (AwakenedWingParticle)p.next();
/* 362 */       e.update();
/* 363 */       if (e.isDone) {
/* 364 */         p.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 371 */     for (AwakenedWingParticle p : this.wParticles) {
/* 372 */       if (p.renderBehind) {
/* 373 */         p.render(sb, this.skeleton.getX() + this.back.getWorldX(), this.skeleton.getY() + this.back.getWorldY());
/*     */       }
/*     */     }
/*     */     
/* 377 */     super.render(sb);
/*     */     
/* 379 */     for (AwakenedWingParticle p : this.wParticles) {
/* 380 */       if (!p.renderBehind) {
/* 381 */         p.render(sb, this.skeleton.getX() + this.back.getWorldX(), this.skeleton.getY() + this.back.getWorldY());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 388 */     if (!AbstractDungeon.getCurrRoom().cannotLose) {
/* 389 */       super.die();
/* 390 */       useFastShakeAnimation(5.0F);
/* 391 */       CardCrawlGame.screenShake.rumble(4.0F);
/* 392 */       if (this.saidPower) {
/* 393 */         AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + this.dialogX, this.hb.cY + this.dialogY, 2.5F, DIALOG[1], false));
/*     */         
/* 395 */         this.deathTimer += 1.5F;
/* 396 */         this.saidPower = true;
/*     */       }
/*     */       
/* 399 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 400 */         if ((!m.isDying) && ((m instanceof Cultist))) {
/* 401 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EscapeAction(m));
/*     */         }
/*     */       }
/*     */       
/* 405 */       onBossVictoryLogic();
/* 406 */       UnlockTracker.hardUnlockOverride("CROW");
/* 407 */       UnlockTracker.unlockAchievement("CROW");
/* 408 */       onFinalBossVictoryLogic();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\AwakenedOne.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */