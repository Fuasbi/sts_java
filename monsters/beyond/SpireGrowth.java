/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.ConstrictedPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SpireGrowth extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Serpent";
/*  22 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Serpent");
/*  23 */   public static final String NAME = monsterStrings.NAME;
/*  24 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  25 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   private static final int START_HP = 170;
/*     */   private static final int A_2_START_HP = 190;
/*  29 */   private int tackleDmg = 16; private int smashDmg = 22; private int constrictDmg = 10;
/*  30 */   private int A_2_tackleDmg = 18; private int A_2_smashDmg = 25;
/*     */   private int tackleDmgActual;
/*     */   private int smashDmgActual;
/*     */   
/*     */   public SpireGrowth() {
/*  35 */     super(NAME, "Serpent", 170, -10.0F, -35.0F, 480.0F, 430.0F, null, 0.0F, 10.0F);
/*  36 */     loadAnimation("images/monsters/theForest/spireGrowth/skeleton.atlas", "images/monsters/theForest/spireGrowth/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  41 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  42 */       setHp(190);
/*     */     } else {
/*  44 */       setHp(170);
/*     */     }
/*     */     
/*  47 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  48 */       this.tackleDmgActual = this.A_2_tackleDmg;
/*  49 */       this.smashDmgActual = this.A_2_smashDmg;
/*     */     } else {
/*  51 */       this.tackleDmgActual = this.tackleDmg;
/*  52 */       this.smashDmgActual = this.smashDmg;
/*     */     }
/*     */     
/*  55 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  56 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  57 */     e.setTimeScale(1.3F);
/*  58 */     this.stateData.setMix("Hurt", "Idle", 0.2F);
/*  59 */     this.stateData.setMix("Idle", "Hurt", 0.2F);
/*     */     
/*  61 */     this.damage.add(new DamageInfo(this, this.tackleDmgActual));
/*  62 */     this.damage.add(new DamageInfo(this, this.smashDmgActual));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  67 */     switch (this.nextMove) {
/*     */     case 1: 
/*  69 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction(this));
/*  70 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  73 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*  75 */       break;
/*     */     case 2: 
/*  77 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction(this));
/*  78 */       if (AbstractDungeon.ascensionLevel >= 17) {
/*  79 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new ConstrictedPower(AbstractDungeon.player, this, this.constrictDmg + 5)));
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*  85 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new ConstrictedPower(AbstractDungeon.player, this, this.constrictDmg)));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  91 */       break;
/*     */     case 3: 
/*  93 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "ATTACK"));
/*  94 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.4F));
/*  95 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  98 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */     }
/*     */     
/*     */     
/* 102 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 108 */     if ((AbstractDungeon.ascensionLevel >= 17) && 
/* 109 */       (!AbstractDungeon.player.hasPower("Constricted")) && (!lastMove((byte)2))) {
/* 110 */       setMove((byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/* 111 */       return;
/*     */     }
/*     */     
/*     */ 
/* 115 */     if ((num < 50) && (!lastTwoMoves((byte)1))) {
/* 116 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 117 */       return;
/*     */     }
/*     */     
/* 120 */     if ((!AbstractDungeon.player.hasPower("Constricted")) && (!lastMove((byte)2))) {
/* 121 */       setMove((byte)2, AbstractMonster.Intent.STRONG_DEBUFF);
/* 122 */       return;
/*     */     }
/*     */     
/* 125 */     if (!lastTwoMoves((byte)3)) {
/* 126 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/* 127 */       return;
/*     */     }
/* 129 */     setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/*     */   }
/*     */   
/*     */   private static final byte QUICK_TACKLE = 1;
/*     */   private static final byte CONSTRICT = 2;
/*     */   private static final byte SMASH = 3;
/*     */   public void damage(DamageInfo info) {
/* 136 */     super.damage(info);
/* 137 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 138 */       this.state.setAnimation(0, "Hurt", false);
/* 139 */       this.state.setTimeScale(1.3F);
/* 140 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 146 */     switch (key) {
/*     */     case "ATTACK": 
/* 148 */       this.state.setAnimation(0, "Attack", false);
/* 149 */       this.state.setTimeScale(1.3F);
/* 150 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 151 */       break;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\SpireGrowth.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */