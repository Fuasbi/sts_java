/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.Bone;
/*     */ import com.esotericsoftware.spine.Skeleton;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.status.Burn;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.vfx.NemesisFireParticle;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Nemesis extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Nemesis";
/*  30 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("Nemesis");
/*  31 */   public static final String NAME = monsterStrings.NAME;
/*  32 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  33 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP = 185;
/*     */   private static final int A_2_HP = 200;
/*     */   private static final int SCYTHE_COOLDOWN_TURNS = 2;
/*     */   private static final float HB_X = 5.0F;
/*     */   private static final float HB_Y = -10.0F;
/*     */   private static final int SCYTHE_DMG = 45;
/*     */   private static final int FIRE_DMG = 6;
/*     */   private static final int FIRE_TIMES = 3;
/*     */   private static final int A_2_FIRE_DMG = 7;
/*     */   private static final int BURN_AMT = 3;
/*     */   private int fireDmg;
/*  45 */   private int scytheCooldown = 0;
/*     */   private static final byte TRI_ATTACK = 2;
/*     */   private static final byte SCYTHE = 3;
/*  48 */   private static final byte TRI_BURN = 4; private float fireTimer = 0.0F;
/*     */   private static final float FIRE_TIME = 0.05F;
/*     */   private Bone eye1;
/*  51 */   private Bone eye2; private Bone eye3; private boolean firstMove = true;
/*     */   
/*     */   public Nemesis() {
/*  54 */     super(NAME, "Nemesis", 185, 5.0F, -10.0F, 350.0F, 440.0F, null, 0.0F, 0.0F);
/*  55 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.ELITE;
/*     */     
/*  57 */     loadAnimation("images/monsters/theForest/nemesis/skeleton.atlas", "images/monsters/theForest/nemesis/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  61 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  62 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  63 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  64 */     e.setTimeScale(0.8F);
/*  65 */     this.eye1 = this.skeleton.findBone("eye0");
/*  66 */     this.eye2 = this.skeleton.findBone("eye1");
/*  67 */     this.eye3 = this.skeleton.findBone("eye2");
/*     */     
/*  69 */     if (AbstractDungeon.ascensionLevel >= 8) {
/*  70 */       setHp(200);
/*     */     } else {
/*  72 */       setHp(185);
/*     */     }
/*     */     
/*  75 */     if (AbstractDungeon.ascensionLevel >= 3) {
/*  76 */       this.fireDmg = 7;
/*     */     }
/*     */     else {
/*  79 */       this.fireDmg = 6;
/*     */     }
/*     */     
/*  82 */     this.damage.add(new DamageInfo(this, 45));
/*  83 */     this.damage.add(new DamageInfo(this, this.fireDmg));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  88 */     if (!hasPower("Intangible")) {
/*  89 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this, this, new com.megacrit.cardcrawl.powers.IntangiblePower(this, 1)));
/*     */     }
/*     */     
/*  92 */     switch (this.nextMove) {
/*     */     case 3: 
/*  94 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ChangeStateAction(this, "ATTACK"));
/*  95 */       playSfx();
/*  96 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.4F));
/*  97 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 100 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*     */       
/* 102 */       break;
/*     */     case 2: 
/* 104 */       for (int i = 0; i < 3; i++) {
/* 105 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 106 */           (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 108 */       break;
/*     */     case 4: 
/* 110 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_NEMESIS_1C"));
/* 111 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(this, new ShockWaveEffect(this.hb.cX, this.hb.cY, com.megacrit.cardcrawl.core.Settings.GREEN_TEXT_COLOR, com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect.ShockWaveType.CHAOTIC), 1.5F));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 116 */       if (AbstractDungeon.ascensionLevel >= 18) {
/* 117 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Burn(), 5));
/*     */       }
/*     */       else {
/* 120 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new Burn(), 3));
/*     */       }
/*     */       break;
/*     */     }
/*     */     
/* 125 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 130 */     if ((info.output > 0) && (hasPower("Intangible"))) {
/* 131 */       info.output = 1;
/*     */     }
/*     */     
/* 134 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 135 */       AnimationState.TrackEntry e = this.state.setAnimation(0, "Hit", false);
/* 136 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 137 */       e.setTimeScale(0.8F);
/*     */     }
/* 139 */     super.damage(info);
/*     */   }
/*     */   
/*     */   public void changeState(String key)
/*     */   {
/* 144 */     switch (key) {
/*     */     case "ATTACK": 
/* 146 */       AnimationState.TrackEntry e = this.state.setAnimation(0, "Attack", false);
/* 147 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 148 */       e.setTimeScale(0.8F);
/* 149 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   protected void getMove(int num)
/*     */   {
/* 157 */     this.scytheCooldown -= 1;
/* 158 */     if (this.firstMove) {
/* 159 */       this.firstMove = false;
/*     */       
/* 161 */       if (num < 50) {
/* 162 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, this.fireDmg, 3, true);
/*     */       } else {
/* 164 */         setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */       
/* 167 */       return;
/*     */     }
/*     */     
/* 170 */     if (num < 30) {
/* 171 */       if ((!lastMove((byte)3)) && (this.scytheCooldown <= 0)) {
/* 172 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, 45);
/* 173 */         this.scytheCooldown = 2;
/*     */       }
/* 175 */       else if (AbstractDungeon.aiRng.randomBoolean()) {
/* 176 */         if (!lastTwoMoves((byte)2)) {
/* 177 */           setMove((byte)2, AbstractMonster.Intent.ATTACK, this.fireDmg, 3, true);
/*     */         } else {
/* 179 */           setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         }
/*     */       }
/* 182 */       else if (!lastMove((byte)4)) {
/* 183 */         setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/* 185 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, this.fireDmg, 3, true);
/*     */       }
/*     */       
/*     */     }
/* 189 */     else if (num < 65) {
/* 190 */       if (!lastTwoMoves((byte)2)) {
/* 191 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, this.fireDmg, 3, true);
/*     */       }
/* 193 */       else if (AbstractDungeon.aiRng.randomBoolean()) {
/* 194 */         if (this.scytheCooldown > 0) {
/* 195 */           setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */         } else {
/* 197 */           setMove((byte)3, AbstractMonster.Intent.ATTACK, 45);
/* 198 */           this.scytheCooldown = 2;
/*     */         }
/*     */       } else {
/* 201 */         setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       }
/*     */       
/*     */     }
/* 205 */     else if (!lastMove((byte)4)) {
/* 206 */       setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */     }
/* 208 */     else if ((AbstractDungeon.aiRng.randomBoolean()) && (this.scytheCooldown <= 0)) {
/* 209 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, 45);
/* 210 */       this.scytheCooldown = 2;
/*     */     } else {
/* 212 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, this.fireDmg, 3, true);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void playSfx()
/*     */   {
/* 219 */     int roll = MathUtils.random(1);
/* 220 */     if (roll == 0) {
/* 221 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_NEMESIS_1A"));
/*     */     } else {
/* 223 */       AbstractDungeon.actionManager.addToBottom(new SFXAction("VO_NEMESIS_1B"));
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDeathSfx() {
/* 228 */     int roll = MathUtils.random(1);
/* 229 */     if (roll == 0) {
/* 230 */       CardCrawlGame.sound.play("VO_NEMESIS_2A");
/*     */     } else {
/* 232 */       CardCrawlGame.sound.play("VO_NEMESIS_2B");
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 238 */     playDeathSfx();
/* 239 */     super.die();
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 244 */     super.update();
/* 245 */     if (!this.isDying) {
/* 246 */       this.fireTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 247 */       if (this.fireTimer < 0.0F) {
/* 248 */         this.fireTimer = 0.05F;
/* 249 */         AbstractDungeon.effectList.add(new NemesisFireParticle(this.skeleton
/* 250 */           .getX() + this.eye1.getWorldX(), this.skeleton.getY() + this.eye1.getWorldY()));
/* 251 */         AbstractDungeon.effectList.add(new NemesisFireParticle(this.skeleton
/* 252 */           .getX() + this.eye2.getWorldX(), this.skeleton.getY() + this.eye2.getWorldY()));
/* 253 */         AbstractDungeon.effectList.add(new NemesisFireParticle(this.skeleton
/* 254 */           .getX() + this.eye3.getWorldX(), this.skeleton.getY() + this.eye3.getWorldY()));
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Nemesis.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */