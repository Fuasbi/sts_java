/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.esotericsoftware.spine.AnimationStateData;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.unique.SpawnDaggerAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.vfx.combat.BiteEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Reptomancer extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Reptomancer";
/*  31 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Reptomancer");
/*  32 */   public static final String NAME = monsterStrings.NAME;
/*  33 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  34 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   private static final int HP_MIN = 120;
/*     */   private static final int HP_MAX = 130;
/*     */   private static final int A_2_HP_MIN = 130;
/*     */   private static final int A_2_HP_MAX = 140;
/*     */   private static final int HEAL_AMT = 30;
/*     */   private static final int SNAKE_STRIKE_DMG = 11;
/*  41 */   private static final int A_2_SNAKE_STRIKE_DMG = 14; private static final byte SNAKE_STRIKE = 1; private static final byte SPAWN_DAGGER = 2; private static final byte BITE = 3; private boolean firstMove = true;
/*     */   private int strikeDmg;
/*     */   
/*     */   public Reptomancer() {
/*  45 */     super(NAME, "Reptomancer", AbstractDungeon.monsterHpRng.random(120, 130), 0.0F, -30.0F, 220.0F, 320.0F, null, -20.0F, 10.0F);
/*  46 */     loadAnimation("images/monsters/theForest/mage/skeleton.atlas", "images/monsters/theForest/mage/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  51 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  52 */       setHp(130, 140);
/*     */     } else {
/*  54 */       setHp(120, 130);
/*     */     }
/*     */     
/*  57 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  58 */       this.strikeDmg = 14;
/*     */     } else {
/*  60 */       this.strikeDmg = 11;
/*     */     }
/*     */     
/*  63 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  64 */     this.stateData.setMix("Idle", "Sumon", 0.1F);
/*  65 */     this.stateData.setMix("Sumon", "Idle", 0.1F);
/*  66 */     this.stateData.setMix("Hurt", "Idle", 0.1F);
/*  67 */     this.stateData.setMix("Idle", "Hurt", 0.1F);
/*  68 */     this.stateData.setMix("Attack", "Idle", 0.1F);
/*  69 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */     
/*  71 */     this.damage.add(new DamageInfo(this, this.strikeDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  76 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  77 */       if (!m.id.equals(this.id)) {
/*  78 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.MinionPower(this)));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  85 */     switch (this.nextMove) {
/*     */     case 1: 
/*  87 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  88 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.3F));
/*  89 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new BiteEffect(AbstractDungeon.player.hb.cX + 
/*     */       
/*     */ 
/*  92 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/*  93 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, Color.ORANGE
/*  94 */         .cpy()), 0.1F));
/*     */       
/*  96 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*  97 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*  98 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new BiteEffect(AbstractDungeon.player.hb.cX + 
/*     */       
/*     */ 
/* 101 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, AbstractDungeon.player.hb.cY + 
/* 102 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, Color.ORANGE
/* 103 */         .cpy()), 0.1F));
/*     */       
/* 105 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 106 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/* 107 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 113 */       break;
/*     */     case 2: 
/* 115 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "SUMMON"));
/* 116 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.5F));
/* 117 */       if (AbstractDungeon.ascensionLevel >= 17) {
/* 118 */         int aliveCount = 0;
/* 119 */         for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 120 */           if ((m != this) && (!m.isDying)) {
/* 121 */             aliveCount++;
/*     */           }
/*     */         }
/* 124 */         if (aliveCount <= 2) {
/* 125 */           AbstractDungeon.actionManager.addToBottom(new SpawnDaggerAction(this));
/*     */         }
/* 127 */         AbstractDungeon.actionManager.addToBottom(new SpawnDaggerAction(this));
/*     */       } else {
/* 129 */         AbstractDungeon.actionManager.addToBottom(new SpawnDaggerAction(this));
/*     */       }
/*     */       
/* 132 */       break;
/*     */     case 3: 
/* 134 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction(this));
/* 135 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(this, this, 30));
/*     */     }
/*     */     
/* 138 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   private boolean canSpawn() {
/* 142 */     int aliveCount = 0;
/* 143 */     for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 144 */       if ((m != this) && (!m.isDying)) {
/* 145 */         aliveCount++;
/*     */       }
/*     */     }
/* 148 */     if (aliveCount > 3) {
/* 149 */       return false;
/*     */     }
/* 151 */     return true;
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 156 */     super.damage(info);
/* 157 */     if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 158 */       this.state.setAnimation(0, "Hurt", false);
/* 159 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 165 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 166 */       if ((!m.isDead) && (!m.isDying)) {
/* 167 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.HideHealthBarAction(m));
/* 168 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.SuicideAction(m));
/* 169 */         AbstractDungeon.actionManager.addToTop(new VFXAction(m, new com.megacrit.cardcrawl.vfx.combat.InflameEffect(m), 0.2F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 176 */     if (this.firstMove) {
/* 177 */       this.firstMove = false;
/* 178 */       setMove((byte)2, AbstractMonster.Intent.UNKNOWN);
/* 179 */       return;
/*     */     }
/*     */     
/*     */ 
/* 183 */     if (num < 33) {
/* 184 */       if (!lastMove((byte)1)) {
/* 185 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, this.strikeDmg, 2, true);
/*     */       } else {
/* 187 */         getMove(AbstractDungeon.aiRng.random(33, 99));
/*     */       }
/*     */       
/*     */     }
/* 191 */     else if (num < 66) {
/* 192 */       if (!lastTwoMoves((byte)2)) {
/* 193 */         if (canSpawn()) {
/* 194 */           setMove((byte)2, AbstractMonster.Intent.UNKNOWN);
/*     */         } else {
/* 196 */           setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, this.strikeDmg, 2, true);
/*     */         }
/*     */       } else {
/* 199 */         setMove((byte)1, AbstractMonster.Intent.ATTACK_DEBUFF, this.strikeDmg, 2, true);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 204 */     else if ((!lastTwoMoves((byte)3)) && (this.currentHealth < this.maxHealth / 2)) {
/* 205 */       setMove((byte)3, AbstractMonster.Intent.BUFF);
/*     */     } else {
/* 207 */       getMove(AbstractDungeon.aiRng.random(65));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void changeState(String key)
/*     */   {
/* 214 */     switch (key) {
/*     */     case "ATTACK": 
/* 216 */       this.state.setAnimation(0, "Attack", false);
/* 217 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 218 */       break;
/*     */     case "SUMMON": 
/* 220 */       this.state.setAnimation(0, "Sumon", false);
/* 221 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 222 */       break;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Reptomancer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */