/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.ShoutAction;
/*     */ import com.megacrit.cardcrawl.actions.animations.TalkAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.DrawReductionPower;
/*     */ import com.megacrit.cardcrawl.powers.TimeWarpPower;
/*     */ import com.megacrit.cardcrawl.powers.WeakPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect.ShockWaveType;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class TimeEater extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "TimeEater";
/*  37 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("TimeEater");
/*  38 */   public static final String NAME = monsterStrings.NAME;
/*  39 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  40 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final int HP = 456;
/*     */   public static final int A_2_HP = 480;
/*     */   private static final byte REVERBERATE = 2;
/*     */   private static final byte RIPPLE = 3;
/*     */   private static final byte HEAD_SLAM = 4;
/*     */   private static final byte HASTE = 5;
/*     */   private static final int REVERB_DMG = 7;
/*     */   private static final int REVERB_AMT = 3;
/*     */   private static final int A_2_REVERB_DMG = 8;
/*     */   private static final int RIPPLE_BLOCK = 20;
/*     */   private static final int HEAD_SLAM_DMG = 26;
/*     */   private static final int A_2_HEAD_SLAM_DMG = 32;
/*     */   private int reverbDmg;
/*     */   private int headSlamDmg;
/*     */   private static final int HEAD_SLAM_STICKY = 1;
/*     */   private static final int RIPPLE_DEBUFF_TURNS = 1;
/*  57 */   private boolean usedHaste = false; private boolean firstTurn = true;
/*     */   
/*     */   public TimeEater() {
/*  60 */     super(NAME, "TimeEater", 456, -10.0F, -30.0F, 476.0F, 410.0F, null, -50.0F, 30.0F);
/*     */     
/*  62 */     if (AbstractDungeon.ascensionLevel >= 9) {
/*  63 */       setHp(480);
/*     */     } else {
/*  65 */       setHp(456);
/*     */     }
/*     */     
/*  68 */     loadAnimation("images/monsters/theForest/timeEater/skeleton.atlas", "images/monsters/theForest/timeEater/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  72 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  73 */     e.setTime(e.getEndTime() * com.badlogic.gdx.math.MathUtils.random());
/*  74 */     this.stateData.setMix("Hit", "Idle", 0.1F);
/*  75 */     e.setTimeScale(0.8F);
/*     */     
/*  77 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  78 */     this.dialogX = (-200.0F * Settings.scale);
/*  79 */     this.dialogY = (10.0F * Settings.scale);
/*     */     
/*  81 */     if (AbstractDungeon.ascensionLevel >= 4) {
/*  82 */       this.reverbDmg = 8;
/*  83 */       this.headSlamDmg = 32;
/*     */     } else {
/*  85 */       this.reverbDmg = 7;
/*  86 */       this.headSlamDmg = 26;
/*     */     }
/*     */     
/*  89 */     this.damage.add(new DamageInfo(this, this.reverbDmg, DamageInfo.DamageType.NORMAL));
/*  90 */     this.damage.add(new DamageInfo(this, this.headSlamDmg, DamageInfo.DamageType.NORMAL));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  95 */     CardCrawlGame.music.unsilenceBGM();
/*  96 */     AbstractDungeon.scene.fadeOutAmbiance();
/*  97 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BEYOND");
/*  98 */     UnlockTracker.markBossAsSeen("WIZARD");
/*  99 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new TimeWarpPower(this)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/* 104 */     if (this.firstTurn) {
/* 105 */       AbstractDungeon.actionManager.addToBottom(new TalkAction(this, DIALOG[0], 0.5F, 2.0F));
/* 106 */       this.firstTurn = false;
/*     */     }
/* 108 */     switch (this.nextMove) {
/*     */     case 2: 
/* 110 */       for (int i = 0; i < 3; i++) {
/* 111 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(this, new ShockWaveEffect(this.hb.cX, this.hb.cY, Settings.BLUE_TEXT_COLOR, ShockWaveEffect.ShockWaveType.CHAOTIC), 0.75F));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 120 */         AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 121 */           (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.FIRE));
/*     */       }
/* 123 */       break;
/*     */     case 3: 
/* 125 */       AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, 20));
/* 126 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.VulnerablePower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 132 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new WeakPower(AbstractDungeon.player, 1, true), 1));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 139 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new com.megacrit.cardcrawl.powers.FrailPower(AbstractDungeon.player, 1, true), 1));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       break;
/*     */     case 4: 
/* 148 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/* 149 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.4F));
/* 150 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/* 151 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.POISON));
/* 152 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(AbstractDungeon.player, this, new DrawReductionPower(AbstractDungeon.player, 1)));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 157 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 158 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Slimed(), 2));
/*     */       }
/*     */       break;
/*     */     case 5: 
/* 162 */       AbstractDungeon.actionManager.addToBottom(new ShoutAction(this, DIALOG[1], 0.5F, 2.0F));
/* 163 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.RemoveDebuffsAction(this));
/* 164 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(this, this, this.maxHealth / 2 - this.currentHealth));
/* 165 */       if (AbstractDungeon.ascensionLevel >= 19) {
/* 166 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this, this, this.headSlamDmg));
/*     */       }
/*     */       break;
/*     */     }
/* 170 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   public void changeState(String stateName)
/*     */   {
/* 175 */     switch (stateName) {
/*     */     case "ATTACK": 
/* 177 */       this.state.setAnimation(0, "Attack", false);
/* 178 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 185 */     super.damage(info);
/* 186 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 187 */       this.state.setAnimation(0, "Hit", false);
/* 188 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 194 */     if ((this.currentHealth < this.maxHealth / 2) && (!this.usedHaste)) {
/* 195 */       this.usedHaste = true;
/* 196 */       setMove((byte)5, AbstractMonster.Intent.BUFF);
/* 197 */       return;
/*     */     }
/*     */     
/* 200 */     if (num < 45) {
/* 201 */       if (!lastTwoMoves((byte)2)) {
/* 202 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/* 203 */         return;
/*     */       }
/* 205 */       getMove(AbstractDungeon.aiRng.random(50, 99));
/* 206 */       return;
/*     */     }
/*     */     
/*     */ 
/* 210 */     if (num < 80) {
/* 211 */       if (!lastMove((byte)4)) {
/* 212 */         setMove((byte)4, AbstractMonster.Intent.ATTACK_DEBUFF, ((DamageInfo)this.damage.get(1)).base);
/* 213 */         return;
/*     */       }
/* 215 */       if (AbstractDungeon.aiRng.randomBoolean(0.66F)) {
/* 216 */         setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 3, true);
/* 217 */         return;
/*     */       }
/* 219 */       setMove((byte)3, AbstractMonster.Intent.DEFEND_DEBUFF);
/* 220 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 225 */     if (!lastMove((byte)3)) {
/* 226 */       setMove((byte)3, AbstractMonster.Intent.DEFEND_DEBUFF);
/* 227 */       return;
/*     */     }
/* 229 */     getMove(AbstractDungeon.aiRng.random(74));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void die()
/*     */   {
/* 236 */     if (!AbstractDungeon.getCurrRoom().cannotLose) {
/* 237 */       useFastShakeAnimation(5.0F);
/* 238 */       CardCrawlGame.screenShake.rumble(4.0F);
/* 239 */       this.deathTimer += 1.5F;
/* 240 */       super.die();
/* 241 */       onBossVictoryLogic();
/* 242 */       UnlockTracker.hardUnlockOverride("WIZARD");
/* 243 */       UnlockTracker.unlockAchievement("TIME_EATER");
/* 244 */       onFinalBossVictoryLogic();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\TimeEater.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */