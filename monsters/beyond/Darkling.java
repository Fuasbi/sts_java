/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateFastAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.powers.RegrowPower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Darkling extends AbstractMonster
/*     */ {
/*  33 */   private static final Logger logger = LogManager.getLogger(Darkling.class.getName());
/*     */   public static final String ID = "Darkling";
/*  35 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Darkling");
/*  36 */   public static final String NAME = monsterStrings.NAME;
/*  37 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  38 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   public static final String DARKLING_ENCOUNTER = "Darkling Encounter";
/*     */   public static final int HP_MIN = 48;
/*     */   public static final int HP_MAX = 56;
/*     */   public static final int A_2_HP_MIN = 50;
/*     */   public static final int A_2_HP_MAX = 59;
/*     */   private static final float HB_X = 0.0F;
/*     */   private static final float HB_Y = -20.0F;
/*     */   private static final float HB_W = 260.0F;
/*     */   private static final float HB_H = 200.0F;
/*     */   private static final int BITE_DMG = 8;
/*     */   private static final int A_2_BITE_DMG = 9;
/*     */   private int chompDmg;
/*     */   private int nipDmg;
/*  52 */   private static final int BLOCK_AMT = 12; private static final int CHOMP_AMT = 2; private static final byte CHOMP = 1; private static final byte HARDEN = 2; private static final byte NIP = 3; private static final byte COUNT = 4; private static final byte REINCARNATE = 5; private boolean firstMove = true;
/*     */   
/*     */   public Darkling(float x, float y) {
/*  55 */     super(NAME, "Darkling", 56, 0.0F, -20.0F, 260.0F, 200.0F, null, x, y + 20.0F);
/*  56 */     loadAnimation("images/monsters/theForest/darkling/skeleton.atlas", "images/monsters/theForest/darkling/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  61 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/*  62 */     e.setTime(e.getEndTime() * MathUtils.random());
/*  63 */     e.setTimeScale(MathUtils.random(0.75F, 1.0F));
/*     */     
/*  65 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  66 */       setHp(50, 59);
/*     */     } else {
/*  68 */       setHp(48, 56);
/*     */     }
/*     */     
/*  71 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  72 */       this.chompDmg = 9;
/*  73 */       this.nipDmg = AbstractDungeon.monsterHpRng.random(9, 13);
/*     */     } else {
/*  75 */       this.chompDmg = 8;
/*  76 */       this.nipDmg = AbstractDungeon.monsterHpRng.random(7, 11);
/*     */     }
/*     */     
/*  79 */     this.dialogX = (-50.0F * com.megacrit.cardcrawl.core.Settings.scale);
/*  80 */     this.damage.add(new DamageInfo(this, this.chompDmg));
/*  81 */     this.damage.add(new DamageInfo(this, this.nipDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  86 */     AbstractDungeon.getCurrRoom().cannotLose = true;
/*  87 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new RegrowPower(this)));
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  92 */     switch (this.nextMove) {
/*     */     case 1: 
/*  94 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "ATTACK"));
/*  95 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.5F));
/*  96 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  99 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 101 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 104 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/* 106 */       break;
/*     */     case 2: 
/* 108 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, 12));
/* 109 */       if (AbstractDungeon.ascensionLevel >= 17) {
/* 110 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 2), 2));
/*     */       }
/*     */       
/*     */       break;
/*     */     case 3: 
/* 115 */       AbstractDungeon.actionManager.addToBottom(new AnimateFastAttackAction(this));
/* 116 */       AbstractDungeon.actionManager.addToBottom(new DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/* 119 */         (DamageInfo)this.damage.get(1), AbstractGameAction.AttackEffect.BLUNT_LIGHT));
/*     */       
/* 121 */       break;
/*     */     case 4: 
/* 123 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction(this, DIALOG[0]));
/* 124 */       break;
/*     */     case 5: 
/* 126 */       if (MathUtils.randomBoolean()) {
/* 127 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("DARKLING_REGROW_2", 
/* 128 */           MathUtils.random(-0.1F, 0.1F)));
/*     */       } else {
/* 130 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("DARKLING_REGROW_1", 
/* 131 */           MathUtils.random(-0.1F, 0.1F)));
/*     */       }
/* 133 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(this, this, this.maxHealth / 2));
/* 134 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "REVIVE"));
/* 135 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new RegrowPower(this), 1));
/* 136 */       if (AbstractDungeon.player.hasRelic("Philosopher's Stone")) {
/* 137 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 2), 2));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 147 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 152 */     System.out.println("GET MOVE");
/*     */     
/* 154 */     if (this.halfDead) {
/* 155 */       setMove((byte)5, AbstractMonster.Intent.BUFF);
/* 156 */       return;
/*     */     }
/*     */     
/* 159 */     if (this.firstMove) {
/* 160 */       if (num < 50) {
/* 161 */         if (AbstractDungeon.ascensionLevel >= 17) {
/* 162 */           setMove((byte)2, AbstractMonster.Intent.DEFEND_BUFF);
/*     */         } else {
/* 164 */           setMove((byte)2, AbstractMonster.Intent.DEFEND);
/*     */         }
/*     */       } else {
/* 167 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */       
/* 170 */       this.firstMove = false;
/* 171 */       return;
/*     */     }
/*     */     
/*     */ 
/* 175 */     if (num < 40) {
/* 176 */       if ((!lastMove((byte)1)) && (AbstractDungeon.getMonsters().monsters.lastIndexOf(this) % 2 == 0)) {
/* 177 */         setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base, 2, true);
/*     */       } else {
/* 179 */         getMove(AbstractDungeon.aiRng.random(40, 99));
/*     */       }
/* 181 */     } else if (num < 70) {
/* 182 */       if (!lastMove((byte)2)) {
/* 183 */         if (AbstractDungeon.ascensionLevel >= 17) {
/* 184 */           setMove((byte)2, AbstractMonster.Intent.DEFEND_BUFF);
/*     */         } else {
/* 186 */           setMove((byte)2, AbstractMonster.Intent.DEFEND);
/*     */         }
/*     */       } else {
/* 189 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */       }
/*     */     }
/* 192 */     else if (!lastTwoMoves((byte)3)) {
/* 193 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(1)).base);
/*     */     } else {
/* 195 */       getMove(AbstractDungeon.aiRng.random(0, 99));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void changeState(String key)
/*     */   {
/* 202 */     switch (key) {
/*     */     case "ATTACK": 
/* 204 */       this.state.setAnimation(0, "Attack", false);
/* 205 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/* 206 */       break;
/*     */     case "REVIVE": 
/* 208 */       this.halfDead = false;
/* 209 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void damage(DamageInfo info)
/*     */   {
/* 217 */     super.damage(info);
/* 218 */     AbstractRelic r; if ((this.currentHealth <= 0) && (!this.halfDead)) {
/* 219 */       this.halfDead = true;
/* 220 */       for (AbstractPower p : this.powers) {
/* 221 */         p.onDeath();
/*     */       }
/* 223 */       for (??? = AbstractDungeon.player.relics.iterator(); ???.hasNext();) { r = (AbstractRelic)???.next();
/* 224 */         r.onMonsterDeath(this);
/*     */       }
/* 226 */       this.powers.clear();
/*     */       
/* 228 */       logger.info("This monster is now half dead.");
/* 229 */       boolean allDead = true;
/* 230 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 231 */         if (!m.halfDead) {
/* 232 */           allDead = false;
/*     */         }
/*     */       }
/*     */       
/* 236 */       logger.info("All dead: " + allDead);
/* 237 */       if (!allDead) {
/* 238 */         if (this.nextMove != 4) {
/* 239 */           setMove((byte)4, AbstractMonster.Intent.UNKNOWN);
/* 240 */           createIntent();
/* 241 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetMoveAction(this, (byte)4, AbstractMonster.Intent.UNKNOWN));
/*     */         }
/*     */       } else {
/* 244 */         AbstractDungeon.getCurrRoom().cannotLose = false;
/* 245 */         this.halfDead = false;
/* 246 */         for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 247 */           m.die();
/*     */         }
/*     */       }
/* 250 */     } else if ((info.owner != null) && (info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (info.output > 0)) {
/* 251 */       this.state.setAnimation(0, "Hit", false);
/* 252 */       this.state.addAnimation(0, "Idle", true, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 258 */     if (!AbstractDungeon.getCurrRoom().cannotLose) {
/* 259 */       super.die();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Darkling.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */