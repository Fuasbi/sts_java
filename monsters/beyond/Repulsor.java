/*    */ package com.megacrit.cardcrawl.monsters.beyond;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*    */ import com.megacrit.cardcrawl.actions.common.RollMoveAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Repulsor extends AbstractMonster
/*    */ {
/*    */   public static final String ID = "Repulsor";
/* 19 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Repulsor");
/* 20 */   public static final String NAME = monsterStrings.NAME;
/* 21 */   public static final String[] MOVES = monsterStrings.MOVES;
/* 22 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*    */   public static final String ENCOUNTER_NAME_W = "Ancient Shapes Weak";
/*    */   public static final String ENCOUNTER_NAME = "Ancient Shapes";
/*    */   private static final float HB_X = -8.0F;
/*    */   private static final float HB_Y = -10.0F;
/*    */   private static final float HB_W = 150.0F;
/*    */   private static final float HB_H = 150.0F;
/*    */   private static final byte DAZE = 1;
/*    */   private static final byte ATTACK = 2;
/*    */   private int attackDmg;
/*    */   private int dazeAmt;
/*    */   
/* 34 */   public Repulsor(float x, float y) { super(NAME, "Repulsor", 35, -8.0F, -10.0F, 150.0F, 150.0F, null, x, y + 10.0F);
/*    */     
/* 36 */     loadAnimation("images/monsters/theForest/repulser/skeleton.atlas", "images/monsters/theForest/repulser/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/* 40 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 41 */     e.setTime(e.getEndTime() * MathUtils.random());
/*    */     
/* 43 */     this.dazeAmt = 2;
/*    */     
/* 45 */     if (AbstractDungeon.ascensionLevel >= 7) {
/* 46 */       setHp(31, 38);
/*    */     } else {
/* 48 */       setHp(29, 35);
/*    */     }
/*    */     
/* 51 */     if (AbstractDungeon.ascensionLevel >= 2) {
/* 52 */       this.attackDmg = 13;
/*    */     } else {
/* 54 */       this.attackDmg = 11;
/*    */     }
/*    */     
/* 57 */     this.damage.add(new DamageInfo(this, this.attackDmg));
/*    */   }
/*    */   
/*    */   public void takeTurn()
/*    */   {
/* 62 */     switch (this.nextMove) {
/*    */     case 2: 
/* 64 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/* 65 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 68 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*    */       
/* 70 */       break;
/*    */     case 1: 
/* 72 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(new com.megacrit.cardcrawl.cards.status.Dazed(), this.dazeAmt, true, true));
/*    */       
/* 74 */       break;
/*    */     }
/*    */     
/*    */     
/*    */ 
/* 79 */     AbstractDungeon.actionManager.addToBottom(new RollMoveAction(this));
/*    */   }
/*    */   
/*    */   protected void getMove(int num)
/*    */   {
/* 84 */     if ((num < 20) && (!lastMove((byte)2))) {
/* 85 */       setMove((byte)2, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 86 */       return;
/*    */     }
/*    */     
/* 89 */     setMove((byte)1, AbstractMonster.Intent.DEBUFF);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Repulsor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */