/*     */ package com.megacrit.cardcrawl.monsters.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.animations.AnimateSlowAttackAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.ThornsPower;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Spiker extends AbstractMonster
/*     */ {
/*     */   public static final String ID = "Spiker";
/*  19 */   private static final MonsterStrings monsterStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getMonsterStrings("Spiker");
/*  20 */   public static final String NAME = monsterStrings.NAME;
/*  21 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  22 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */   public static final String ENCOUNTER_NAME = "Ancient Shapes";
/*     */   
/*     */   private static final int HP_MIN = 42;
/*     */   private static final int HP_MAX = 56;
/*     */   private static final int A_2_HP_MIN = 44;
/*     */   private static final int A_2_HP_MAX = 60;
/*     */   private static final float HB_X = -8.0F;
/*     */   private static final float HB_Y = -10.0F;
/*     */   private static final float HB_W = 150.0F;
/*     */   private static final float HB_H = 150.0F;
/*     */   private static final int STARTING_THORNS = 3;
/*     */   private static final int A_2_STARTING_THORNS = 4;
/*     */   private int startingThorns;
/*     */   private static final byte ATTACK = 1;
/*     */   private static final int ATTACK_DMG = 7;
/*     */   private static final int A_2_ATTACK_DMG = 9;
/*     */   private int attackDmg;
/*     */   private static final byte BUFF_THORNS = 2;
/*     */   private static final int BUFF_AMT = 2;
/*  43 */   private int thornsCount = 0;
/*     */   
/*     */   public Spiker(float x, float y) {
/*  46 */     super(NAME, "Spiker", 56, -8.0F, -10.0F, 150.0F, 150.0F, null, x, y + 10.0F);
/*     */     
/*  48 */     loadAnimation("images/monsters/theForest/spiker/skeleton.atlas", "images/monsters/theForest/spiker/skeleton.json", 1.0F);
/*     */     
/*     */ 
/*     */ 
/*  52 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/*  53 */     e.setTime(e.getEndTime() * MathUtils.random());
/*     */     
/*  55 */     if (AbstractDungeon.ascensionLevel >= 7) {
/*  56 */       setHp(44, 60);
/*     */     } else {
/*  58 */       setHp(42, 56);
/*     */     }
/*     */     
/*  61 */     if (AbstractDungeon.ascensionLevel >= 2) {
/*  62 */       this.startingThorns = 4;
/*  63 */       this.attackDmg = 9;
/*     */     } else {
/*  65 */       this.startingThorns = 3;
/*  66 */       this.attackDmg = 7;
/*     */     }
/*     */     
/*  69 */     this.damage.add(new DamageInfo(this, this.attackDmg));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  74 */     if (AbstractDungeon.ascensionLevel >= 17) {
/*  75 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ThornsPower(this, this.startingThorns + 3)));
/*     */     }
/*     */     else {
/*  78 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ThornsPower(this, this.startingThorns)));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void takeTurn()
/*     */   {
/*  85 */     switch (this.nextMove) {
/*     */     case 1: 
/*  87 */       AbstractDungeon.actionManager.addToBottom(new AnimateSlowAttackAction(this));
/*  88 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*     */       
/*     */ 
/*  91 */         (DamageInfo)this.damage.get(0), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*     */       
/*  93 */       break;
/*     */     case 2: 
/*  95 */       this.thornsCount += 1;
/*  96 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new ThornsPower(this, 2), 2));
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/* 102 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/* 107 */     if (this.thornsCount > 5) {
/* 108 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 109 */       return;
/*     */     }
/*     */     
/* 112 */     if ((num < 50) && (!lastMove((byte)1))) {
/* 113 */       setMove((byte)1, AbstractMonster.Intent.ATTACK, ((DamageInfo)this.damage.get(0)).base);
/* 114 */       return;
/*     */     }
/*     */     
/* 117 */     setMove((byte)2, AbstractMonster.Intent.BUFF);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\beyond\Spiker.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */