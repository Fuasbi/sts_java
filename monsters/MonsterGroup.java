/*     */ package com.megacrit.cardcrawl.monsters;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.dungeons.TheCity;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MonsterGroup
/*     */ {
/*  23 */   private static final Logger logger = LogManager.getLogger(MonsterGroup.class.getName());
/*  24 */   public ArrayList<AbstractMonster> monsters = new ArrayList();
/*  25 */   public AbstractMonster hoveredMonster = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MonsterGroup(AbstractMonster[] input)
/*     */   {
/*  33 */     Collections.addAll(this.monsters, input);
/*     */   }
/*     */   
/*     */   public void addMonster(AbstractMonster m) {
/*  37 */     this.monsters.add(m);
/*     */   }
/*     */   
/*     */   public void addMonster(int newIndex, AbstractMonster m) {
/*  41 */     this.monsters.add(newIndex, m);
/*     */   }
/*     */   
/*     */   public void addSpawnedMonster(AbstractMonster m) {
/*  45 */     this.monsters.add(0, m);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MonsterGroup(AbstractMonster m)
/*     */   {
/*  54 */     this(new AbstractMonster[] { m });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void showIntent()
/*     */   {
/*  61 */     for (AbstractMonster m : this.monsters) {
/*  62 */       m.createIntent();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void init()
/*     */   {
/*  70 */     for (AbstractMonster m : this.monsters) {
/*  71 */       m.init();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void add(AbstractMonster m)
/*     */   {
/*  79 */     this.monsters.add(m);
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  84 */     if (AbstractDungeon.loading_post_combat) {
/*  85 */       return;
/*     */     }
/*     */     
/*  88 */     for (AbstractMonster m : this.monsters) {
/*  89 */       m.usePreBattleAction();
/*  90 */       m.useUniversalPreBattleAction();
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean areMonstersDead() {
/*  95 */     for (AbstractMonster m : this.monsters) {
/*  96 */       if ((!m.isDead) && (!m.escaped)) {
/*  97 */         return false;
/*     */       }
/*     */     }
/* 100 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean areMonstersBasicallyDead()
/*     */   {
/* 109 */     for (AbstractMonster m : this.monsters) {
/* 110 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 111 */         return false;
/*     */       }
/*     */     }
/* 114 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void applyPreTurnLogic()
/*     */   {
/* 121 */     for (AbstractMonster m : this.monsters) {
/* 122 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 123 */         if (!m.hasPower("Barricade")) {
/* 124 */           m.loseBlock();
/*     */         }
/* 126 */         m.applyStartOfTurnPowers();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AbstractMonster getMonster(String id)
/*     */   {
/* 140 */     for (AbstractMonster m : this.monsters) {
/* 141 */       if (m.id.equals(id)) {
/* 142 */         return m;
/*     */       }
/*     */     }
/*     */     
/* 146 */     logger.info("MONSTER GROUP getMonster(): Could not find monster: " + id);
/* 147 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void queueMonsters()
/*     */   {
/* 154 */     for (AbstractMonster m : this.monsters) {
/* 155 */       if ((!m.isDeadOrEscaped()) || (m.halfDead)) {
/* 156 */         AbstractDungeon.actionManager.monsterQueue.add(new MonsterQueueItem(m));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean haveMonstersEscaped()
/*     */   {
/* 167 */     for (AbstractMonster m : this.monsters) {
/* 168 */       if (!m.escaped) {
/* 169 */         return false;
/*     */       }
/*     */     }
/*     */     
/* 173 */     return true;
/*     */   }
/*     */   
/*     */   public boolean isMonsterEscaping() {
/* 177 */     for (AbstractMonster m : this.monsters) {
/* 178 */       if (m.nextMove == 99) {
/* 179 */         return true;
/*     */       }
/*     */     }
/* 182 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public boolean hasMonsterEscaped()
/*     */   {
/* 189 */     for (AbstractMonster m : this.monsters) {
/* 190 */       if (m.isEscaping) {
/* 191 */         return true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 196 */     if ((CardCrawlGame.dungeon instanceof TheCity)) {
/* 197 */       return true;
/*     */     }
/* 199 */     return false;
/*     */   }
/*     */   
/*     */   public AbstractMonster getRandomMonster() {
/* 203 */     return getRandomMonster(null, false);
/*     */   }
/*     */   
/*     */   public AbstractMonster getRandomMonster(boolean aliveOnly) {
/* 207 */     return getRandomMonster(null, aliveOnly);
/*     */   }
/*     */   
/*     */   public AbstractMonster getRandomMonster(AbstractMonster exception, boolean aliveOnly, Random rng) {
/* 211 */     if (areMonstersBasicallyDead()) {
/* 212 */       return null;
/*     */     }
/*     */     
/*     */ 
/* 216 */     if (exception == null) {
/* 217 */       if (aliveOnly) {
/* 218 */         ArrayList<AbstractMonster> tmp = new ArrayList();
/* 219 */         for (AbstractMonster m : this.monsters) {
/* 220 */           if ((!m.halfDead) && (!m.isDying) && (!m.isEscaping)) {
/* 221 */             tmp.add(m);
/*     */           }
/*     */         }
/*     */         
/* 225 */         if (tmp.size() <= 0) {
/* 226 */           return null;
/*     */         }
/* 228 */         return (AbstractMonster)tmp.get(rng.random(0, tmp.size() - 1));
/*     */       }
/*     */       
/* 231 */       return (AbstractMonster)this.monsters.get(rng.random(0, this.monsters.size() - 1));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 238 */     if (this.monsters.size() == 1) {
/* 239 */       return (AbstractMonster)this.monsters.get(0);
/*     */     }
/*     */     
/* 242 */     if (aliveOnly) {
/* 243 */       ArrayList<AbstractMonster> tmp = new ArrayList();
/* 244 */       for (AbstractMonster m : this.monsters) {
/* 245 */         if ((!m.halfDead) && (!m.isDying) && (!m.isEscaping) && (!exception.equals(m))) {
/* 246 */           tmp.add(m);
/*     */         }
/*     */       }
/* 249 */       if (tmp.size() == 0) {
/* 250 */         return null;
/*     */       }
/*     */       
/* 253 */       return (AbstractMonster)tmp.get(rng.random(0, tmp.size() - 1));
/*     */     }
/*     */     
/* 256 */     ArrayList<AbstractMonster> tmp = new ArrayList();
/* 257 */     for (AbstractMonster m : this.monsters) {
/* 258 */       if (!exception.equals(m)) {
/* 259 */         tmp.add(m);
/*     */       }
/*     */     }
/* 262 */     return (AbstractMonster)tmp.get(rng.random(0, tmp.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public AbstractMonster getRandomMonster(AbstractMonster exception, boolean aliveOnly)
/*     */   {
/* 269 */     if (areMonstersBasicallyDead()) {
/* 270 */       return null;
/*     */     }
/*     */     
/*     */ 
/* 274 */     if (exception == null) {
/* 275 */       if (aliveOnly) {
/* 276 */         ArrayList<AbstractMonster> tmp = new ArrayList();
/* 277 */         for (AbstractMonster m : this.monsters) {
/* 278 */           if ((!m.halfDead) && (!m.isDying) && (!m.isEscaping)) {
/* 279 */             tmp.add(m);
/*     */           }
/*     */         }
/*     */         
/* 283 */         if (tmp.size() <= 0) {
/* 284 */           return null;
/*     */         }
/* 286 */         return (AbstractMonster)tmp.get(MathUtils.random(0, tmp.size() - 1));
/*     */       }
/*     */       
/* 289 */       return (AbstractMonster)this.monsters.get(MathUtils.random(0, this.monsters.size() - 1));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 296 */     if (this.monsters.size() == 1) {
/* 297 */       return (AbstractMonster)this.monsters.get(0);
/*     */     }
/*     */     
/* 300 */     if (aliveOnly) {
/* 301 */       ArrayList<AbstractMonster> tmp = new ArrayList();
/* 302 */       for (AbstractMonster m : this.monsters) {
/* 303 */         if ((!m.halfDead) && (!m.isDying) && (!m.isEscaping) && (!exception.equals(m))) {
/* 304 */           tmp.add(m);
/*     */         }
/*     */       }
/* 307 */       if (tmp.size() == 0) {
/* 308 */         return null;
/*     */       }
/*     */       
/* 311 */       return (AbstractMonster)tmp.get(MathUtils.random(0, tmp.size() - 1));
/*     */     }
/*     */     
/* 314 */     ArrayList<AbstractMonster> tmp = new ArrayList();
/* 315 */     for (AbstractMonster m : this.monsters) {
/* 316 */       if (!exception.equals(m)) {
/* 317 */         tmp.add(m);
/*     */       }
/*     */     }
/* 320 */     return (AbstractMonster)tmp.get(MathUtils.random(0, tmp.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 329 */     for (AbstractMonster m : this.monsters) {
/* 330 */       m.update();
/*     */     }
/*     */     
/* 333 */     if (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.DEATH)
/*     */     {
/* 335 */       this.hoveredMonster = null;
/* 336 */       for (AbstractMonster m : this.monsters) {
/* 337 */         if ((!m.isDying) && (!m.isEscaping)) {
/* 338 */           m.hb.update();
/* 339 */           m.intentHb.update();
/* 340 */           m.healthHb.update();
/* 341 */           if (((m.hb.hovered) || (m.intentHb.hovered) || (m.healthHb.hovered)) && (!AbstractDungeon.player.isDraggingCard))
/*     */           {
/* 343 */             this.hoveredMonster = m;
/* 344 */             break;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 349 */       if (this.hoveredMonster == null) {
/* 350 */         AbstractDungeon.player.hoverEnemyWaitTimer = -1.0F;
/*     */       }
/*     */     } else {
/* 353 */       this.hoveredMonster = null;
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateAnimations() {
/* 358 */     for (AbstractMonster m : this.monsters) {
/* 359 */       m.updatePowers();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void escape()
/*     */   {
/* 367 */     for (AbstractMonster m : this.monsters) {
/* 368 */       m.escape();
/*     */     }
/*     */   }
/*     */   
/*     */   public void unhover() {
/* 373 */     for (AbstractMonster m : this.monsters) {
/* 374 */       m.unhover();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 379 */     if ((this.hoveredMonster != null) && (!this.hoveredMonster.isDead) && (!this.hoveredMonster.escaped) && (AbstractDungeon.player.hoverEnemyWaitTimer < 0.0F) && (!AbstractDungeon.isScreenUp))
/*     */     {
/* 381 */       this.hoveredMonster.renderTip(sb);
/*     */     }
/*     */     
/* 384 */     for (AbstractMonster m : this.monsters) {
/* 385 */       m.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void applyEndOfTurnPowers() {
/* 390 */     for (AbstractMonster m : this.monsters) {
/* 391 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 392 */         m.applyEndOfTurnTriggers();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 397 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/* 398 */       p.atEndOfRound();
/*     */     }
/*     */     
/* 401 */     for (AbstractMonster m : this.monsters) {
/* 402 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 403 */         for (AbstractPower p : m.powers) {
/* 404 */           p.atEndOfRound();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void renderReticle(SpriteBatch sb) {
/* 411 */     for (AbstractMonster m : this.monsters) {
/* 412 */       if ((!m.isDying) && (!m.isEscaping)) {
/* 413 */         m.renderReticle(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public ArrayList<String> getMonsterNames() {
/* 419 */     ArrayList<String> arr = new ArrayList();
/* 420 */     for (AbstractMonster m : this.monsters) {
/* 421 */       arr.add(m.id);
/*     */     }
/* 423 */     return arr;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\MonsterGroup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */