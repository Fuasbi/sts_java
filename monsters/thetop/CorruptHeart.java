/*     */ package com.megacrit.cardcrawl.monsters.thetop;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.MonsterStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class CorruptHeart extends AbstractMonster
/*     */ {
/*  25 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(CorruptHeart.class.getName());
/*     */   public static final String ID = "CorruptHeart";
/*  27 */   private static final MonsterStrings monsterStrings = CardCrawlGame.languagePack.getMonsterStrings("CorruptHeart");
/*  28 */   public static final String NAME = monsterStrings.NAME;
/*  29 */   public static final String[] MOVES = monsterStrings.MOVES;
/*  30 */   public static final String[] DIALOG = monsterStrings.DIALOG;
/*     */   
/*     */ 
/*     */   private static final int MAX_HP = 3333;
/*     */   
/*  35 */   private int beatSpeed = 4;
/*  36 */   private float beatTimer = 4.0F;
/*  37 */   private int points = 0;
/*     */   private static final byte START_BEATING = 1;
/*     */   private static final byte BLOCK = 2;
/*     */   private static final byte ATTACK = 3;
/*     */   private static final byte REMOVE_POWER = 4;
/*     */   public static final int ECHO_DMG = 16;
/*     */   private static final int BLOCK_AMT = 12;
/*     */   
/*  45 */   public CorruptHeart() { super(NAME, "CorruptHeart", 3333, 0.0F, 50.0F, 550.0F, 500.0F, "images/monsters/heart.png", -200.0F, -60.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  50 */     this.type = com.megacrit.cardcrawl.monsters.AbstractMonster.EnemyType.BOSS;
/*  51 */     this.damage.add(new DamageInfo(this, 16));
/*     */   }
/*     */   
/*     */   public void usePreBattleAction()
/*     */   {
/*  56 */     CardCrawlGame.music.unsilenceBGM();
/*  57 */     AbstractDungeon.scene.fadeOutAmbiance();
/*  58 */     AbstractDungeon.getCurrRoom().playBgmInstantly("BOSS_BOTTOM");
/*     */   }
/*     */   
/*     */   public void takeTurn()
/*     */   {
/*  63 */     switch (this.nextMove) {
/*     */     case 1: 
/*  65 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction(this, "BEAT"));
/*  66 */       break;
/*     */     case 2: 
/*  68 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this, this, 12));
/*  69 */       break;
/*     */     case 3: 
/*  71 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, 
/*  72 */         (DamageInfo)this.damage.get(0), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*  73 */       break;
/*     */     case 4: 
/*  75 */       if (!AbstractDungeon.player.powers.isEmpty()) {
/*  76 */         String toRemove = ((AbstractPower)AbstractDungeon.player.powers.get(
/*  77 */           MathUtils.random(0, AbstractDungeon.player.powers.size() - 1))).ID;
/*  78 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(AbstractDungeon.player, this, toRemove));
/*     */       }
/*     */       break;
/*     */     }
/*     */     
/*  83 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RollMoveAction(this));
/*     */   }
/*     */   
/*     */   protected void getMove(int num)
/*     */   {
/*  88 */     if ((num < 50) && (!AbstractDungeon.player.powers.isEmpty())) {
/*  89 */       if (!lastMove((byte)4)) {
/*  90 */         setMove((byte)4, AbstractMonster.Intent.DEBUFF);
/*     */       } else {
/*  92 */         getMove(MathUtils.random(51, 99));
/*     */       }
/*  94 */     } else if (num < 75) {
/*  95 */       if (!lastTwoMoves((byte)3)) {
/*  96 */         setMove((byte)3, AbstractMonster.Intent.ATTACK, 16);
/*     */       } else {
/*  98 */         setMove((byte)2, AbstractMonster.Intent.DEFEND, 12);
/*     */       }
/*     */     }
/* 101 */     else if (!lastTwoMoves((byte)2)) {
/* 102 */       setMove((byte)2, AbstractMonster.Intent.DEFEND, 12);
/*     */     } else {
/* 104 */       setMove((byte)3, AbstractMonster.Intent.ATTACK, 16);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/* 111 */     super.update();
/* 112 */     updateBeat();
/*     */   }
/*     */   
/*     */   private void updateBeat() {
/* 116 */     if (!this.isDying) {
/* 117 */       this.beatTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 118 */       if (this.beatTimer < 0.0F) {
/* 119 */         if (this.beatSpeed == 0) {
/* 120 */           this.beatTimer = 0.66F;
/*     */         } else {
/* 122 */           this.beatTimer = this.beatSpeed;
/*     */         }
/* 124 */         beat();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void beat() {
/* 130 */     if (AbstractDungeon.player.currentHealth > 0) {
/* 131 */       CardCrawlGame.sound.play("HEART_BEAT");
/* 132 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */       AbstractDungeon.player.damage(new DamageInfo(this, 1, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS));
/*     */     }
/*     */   }
/*     */   
/*     */   public void incrementPoints(int amount) {
/* 143 */     this.points += amount;
/* 144 */     if (this.points > 100) {
/* 145 */       this.points = 0;
/* 146 */       if (this.beatSpeed != 0) {
/* 147 */         this.beatSpeed -= 1;
/*     */       }
/*     */     }
/* 150 */     logger.info("POINTS: " + this.points);
/*     */   }
/*     */   
/*     */   public void die()
/*     */   {
/* 155 */     super.die();
/* 156 */     onBossVictoryLogic();
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 160 */     super.render(sb);
/* 161 */     if (this.beatTimer < 1.0F) {
/* 162 */       sb.setColor(this.tint.color);
/* 163 */       sb.draw(this.img, this.drawX - this.img
/*     */       
/* 165 */         .getWidth() * Settings.scale / 2.0F + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY, this.img
/*     */         
/* 167 */         .getWidth() * Settings.scale + this.beatTimer * 30.0F, this.img
/* 168 */         .getHeight() * Settings.scale + this.beatTimer * 30.0F, 0, 0, this.img
/*     */         
/*     */ 
/* 171 */         .getWidth(), this.img
/* 172 */         .getHeight(), this.flipHorizontal, this.flipVertical);
/*     */       
/*     */ 
/*     */ 
/* 176 */       sb.draw(this.img, this.drawX - this.img
/*     */       
/* 178 */         .getWidth() * Settings.scale / 2.0F + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY, this.img
/*     */         
/* 180 */         .getWidth() * Settings.scale + this.beatTimer * 10.0F, this.img
/* 181 */         .getHeight() * Settings.scale + this.beatTimer * 10.0F, 0, 0, this.img
/*     */         
/*     */ 
/* 184 */         .getWidth(), this.img
/* 185 */         .getHeight(), this.flipHorizontal, this.flipVertical);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\monsters\thetop\CorruptHeart.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */