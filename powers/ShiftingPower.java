/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ShiftingPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Shifting";
/* 14 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Shifting");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public ShiftingPower(AbstractCreature owner) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Shifting";
/* 21 */     this.owner = owner;
/* 22 */     updateDescription();
/* 23 */     this.isPostActionPower = true;
/* 24 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/fading.png");
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 29 */     AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, -damageAmount), -damageAmount));
/*    */     
/*    */ 
/* 32 */     if (!this.owner.hasPower("Artifact")) {
/* 33 */       AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.owner, this.owner, new GainStrengthPower(this.owner, damageAmount), damageAmount));
/*    */     }
/*    */     
/* 36 */     flash();
/*    */     
/* 38 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 43 */     this.description = (DESCRIPTIONS[0] + FontHelper.colorString(this.owner.name, "y") + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ShiftingPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */