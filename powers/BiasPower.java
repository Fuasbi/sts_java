/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class BiasPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Bias";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Bias");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public BiasPower(AbstractCreature owner, int setAmount) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Bias";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = setAmount;
/* 20 */     updateDescription();
/* 21 */     loadRegion("bias");
/* 22 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 27 */     flash();
/* 28 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new FocusPower(this.owner, -this.amount), -this.amount));
/*    */   }
/*    */   
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 34 */     this.fontScale = 8.0F;
/* 35 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\BiasPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */