/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.PoisonLoseHpAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class PoisonPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Poison";
/* 16 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Poison");
/* 17 */   public static final String NAME = powerStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private AbstractCreature source;
/*    */   
/*    */   public PoisonPower(AbstractCreature owner, AbstractCreature source, int poisonAmt)
/*    */   {
/* 23 */     this.name = NAME;
/* 24 */     this.ID = "Poison";
/* 25 */     this.owner = owner;
/* 26 */     this.source = source;
/* 27 */     this.amount = poisonAmt;
/*    */     
/* 29 */     if (this.amount >= 9999) {
/* 30 */       this.amount = 9999;
/*    */     }
/*    */     
/* 33 */     updateDescription();
/* 34 */     loadRegion("poison");
/* 35 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */     
/* 37 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 42 */     if ((this.owner == null) || (this.owner.isPlayer)) {
/* 43 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 45 */       this.description = (DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[1]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 51 */     super.stackPower(stackAmount);
/*    */     
/* 53 */     if ((this.amount > 98) && (AbstractDungeon.player.chosenClass == com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass.THE_SILENT)) {
/* 54 */       UnlockTracker.unlockAchievement("CATALYST");
/*    */     }
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 60 */     if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT)
/*    */     {
/* 62 */       if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 63 */         flashWithoutSound();
/* 64 */         AbstractDungeon.actionManager.addToBottom(new PoisonLoseHpAction(this.owner, this.source, this.amount, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.POISON));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PoisonPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */