/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class WraithFormPower2 extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Wraith Form v2";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Wraith Form v2");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public WraithFormPower2(AbstractCreature owner, int amount) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Wraith Form v2";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = amount;
/* 20 */     updateDescription();
/* 21 */     loadRegion("wraithForm");
/* 22 */     this.canGoNegative = true;
/* 23 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 28 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new DexterityPower(AbstractDungeon.player, this.amount), this.amount));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 38 */     this.fontScale = 8.0F;
/* 39 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 44 */     this.description = (DESCRIPTIONS[0] + -this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\WraithFormPower2.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */