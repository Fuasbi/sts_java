/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class StaticDischargePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "StaticDischarge";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("StaticDischarge");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public StaticDischargePower(AbstractCreature owner, int lightningAmount) {
/* 20 */     this.name = NAME;
/* 21 */     this.ID = "StaticDischarge";
/* 22 */     this.owner = owner;
/* 23 */     this.amount = lightningAmount;
/* 24 */     updateDescription();
/* 25 */     loadRegion("static_discharge");
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 30 */     if ((info.type != DamageInfo.DamageType.THORNS) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.owner != null) && (info.owner != this.owner) && (damageAmount > 0) && 
/* 31 */       (!this.owner.hasPower("Buffer"))) {
/* 32 */       flash();
/* 33 */       for (int i = 0; i < this.amount; i++) {
/* 34 */         AbstractDungeon.actionManager.addToTop(new ChannelAction(new com.megacrit.cardcrawl.orbs.Lightning()));
/*    */       }
/*    */     }
/* 37 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 42 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\StaticDischargePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */