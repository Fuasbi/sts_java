/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class RegenPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Regeneration";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Regeneration");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public RegenPower(AbstractCreature owner, int heal) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Regeneration";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = heal;
/* 20 */     updateDescription();
/* 21 */     loadRegion("regen");
/* 22 */     this.type = AbstractPower.PowerType.BUFF;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 27 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 32 */     flashWithoutSound();
/* 33 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.unique.RegenAction(this.owner, this.amount));
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 38 */     super.stackPower(stackAmount);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RegenPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */