/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class MalleablePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Malleable";
/*    */   
/*    */   public MalleablePower(AbstractCreature owner)
/*    */   {
/* 20 */     this(owner, 3);
/*    */   }
/*    */   
/*    */   public MalleablePower(AbstractCreature owner, int amt) {
/* 24 */     this.name = NAME;
/* 25 */     this.ID = "Malleable";
/* 26 */     this.owner = owner;
/* 27 */     this.amount = amt;
/* 28 */     this.basePower = amt;
/* 29 */     updateDescription();
/* 30 */     this.img = ImageMaster.loadImage("images/powers/32/malleable.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 35 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1] + NAME + DESCRIPTIONS[2] + this.basePower + DESCRIPTIONS[3]);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 42 */     if (this.owner.isPlayer)
/* 43 */       return;
/* 44 */     this.amount = this.basePower;
/* 45 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 50 */     if (!this.owner.isPlayer)
/* 51 */       return;
/* 52 */     this.amount = this.basePower;
/* 53 */     updateDescription();
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 58 */     if ((damageAmount < this.owner.currentHealth) && (damageAmount > 0) && (info.owner != null) && (info.type == DamageInfo.DamageType.NORMAL) && (info.type != DamageInfo.DamageType.HP_LOSS))
/*    */     {
/* 60 */       flash();
/* 61 */       if (this.owner.isPlayer) {
/* 62 */         AbstractDungeon.actionManager.addToTop(new GainBlockAction(this.owner, this.owner, this.amount));
/*    */       } else {
/* 64 */         AbstractDungeon.actionManager.addToBottom(new GainBlockAction(this.owner, this.owner, this.amount));
/*    */       }
/* 66 */       this.amount += 1;
/* 67 */       updateDescription();
/*    */     }
/* 69 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 74 */     this.amount += stackAmount;
/* 75 */     this.basePower += stackAmount;
/*    */   }
/*    */   
/*    */ 
/* 79 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Malleable");
/* 80 */   public static final String NAME = powerStrings.NAME;
/* 81 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private static final int STARTING_BLOCK = 3;
/*    */   private int basePower;
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\MalleablePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */