/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class FlameBarrierPower extends AbstractPower
/*    */ {
/* 17 */   private static final Logger logger = LogManager.getLogger(FlameBarrierPower.class.getName());
/*    */   public static final String POWER_ID = "Flame Barrier";
/* 19 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Flame Barrier");
/* 20 */   public static final String NAME = powerStrings.NAME;
/* 21 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private DamageInfo thornsInfo;
/*    */   
/*    */   public FlameBarrierPower(AbstractCreature owner, int thornsDamage)
/*    */   {
/* 26 */     this.name = NAME;
/* 27 */     this.ID = "Flame Barrier";
/* 28 */     this.owner = owner;
/* 29 */     this.amount = thornsDamage;
/* 30 */     updateDescription();
/* 31 */     loadRegion("flameBarrier");
/*    */     
/* 33 */     this.thornsInfo = new DamageInfo(owner, this.amount, DamageInfo.DamageType.THORNS);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount) {
/* 37 */     if (this.amount == -1) {
/* 38 */       logger.info(this.name + " does not stack");
/* 39 */       return;
/*    */     }
/* 41 */     this.fontScale = 8.0F;
/* 42 */     this.amount += stackAmount;
/* 43 */     this.thornsInfo = new DamageInfo(this.owner, this.amount, DamageInfo.DamageType.THORNS);
/* 44 */     updateDescription();
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 49 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.THORNS) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.owner != this.owner))
/*    */     {
/* 51 */       flash();
/* 52 */       AbstractDungeon.actionManager.addToTop(new DamageAction(info.owner, this.thornsInfo, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */     }
/*    */     
/* 55 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 60 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(AbstractDungeon.player, AbstractDungeon.player, "Flame Barrier"));
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription()
/*    */   {
/* 66 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\FlameBarrierPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */