/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class FireBreathingPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Fire Breathing";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Fire Breathing");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public FireBreathingPower(AbstractCreature owner, int newAmount) {
/* 20 */     this.name = NAME;
/* 21 */     this.ID = "Fire Breathing";
/* 22 */     this.owner = owner;
/* 23 */     this.amount = newAmount;
/* 24 */     updateDescription();
/* 25 */     loadRegion("firebreathing");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 30 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 36 */     int count = 0;
/* 37 */     for (AbstractCard c : AbstractDungeon.actionManager.cardsPlayedThisTurn) {
/* 38 */       if (c.type == AbstractCard.CardType.ATTACK) {
/* 39 */         count++;
/*    */       }
/*    */     }
/* 42 */     if (count > 0) {
/* 43 */       flash();
/* 44 */       for (int i = 0; i < count; i++) {
/* 45 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */         
/*    */ 
/* 48 */           DamageInfo.createDamageMatrix(this.amount, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE, true));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\FireBreathingPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */