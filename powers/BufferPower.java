/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class BufferPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Buffer";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Buffer");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public BufferPower(AbstractCreature owner, int bufferAmt) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Buffer";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = bufferAmt;
/* 20 */     updateDescription();
/* 21 */     loadRegion("buffer");
/*    */   }
/*    */   
/*    */   public int onLoseHp(int damageAmount)
/*    */   {
/* 26 */     if (damageAmount > 0) {
/* 27 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, this.ID, 1));
/*    */     }
/* 29 */     return 0;
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 34 */     this.fontScale = 8.0F;
/* 35 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     if (this.amount <= 1) {
/* 41 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 43 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\BufferPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */