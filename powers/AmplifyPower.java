/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class AmplifyPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Amplify";
/* 18 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Amplify");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public AmplifyPower(AbstractCreature owner, int amount) {
/* 23 */     this.name = NAME;
/* 24 */     this.ID = "Amplify";
/* 25 */     this.owner = owner;
/* 26 */     this.amount = amount;
/* 27 */     updateDescription();
/* 28 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/doubleTap.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 33 */     if (this.amount == 1) {
/* 34 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 36 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 42 */     if ((!card.purgeOnUse) && (card.type == AbstractCard.CardType.POWER) && (this.amount > 0)) {
/* 43 */       flash();
/* 44 */       AbstractMonster m = null;
/*    */       
/* 46 */       if (action.target != null) {
/* 47 */         m = (AbstractMonster)action.target;
/*    */       }
/*    */       
/* 50 */       AbstractCard tmp = card.makeStatEquivalentCopy();
/* 51 */       AbstractDungeon.player.limbo.addToBottom(tmp);
/* 52 */       tmp.current_x = card.current_x;
/* 53 */       tmp.current_y = card.current_y;
/* 54 */       tmp.target_x = (Settings.WIDTH / 2.0F - 300.0F * Settings.scale);
/* 55 */       tmp.target_y = (Settings.HEIGHT / 2.0F);
/* 56 */       tmp.freeToPlayOnce = true;
/*    */       
/* 58 */       if (m != null) {
/* 59 */         tmp.calculateCardDamage(m);
/*    */       }
/*    */       
/* 62 */       tmp.purgeOnUse = true;
/* 63 */       AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(tmp, m, card.energyOnUse));
/* 64 */       this.amount -= 1;
/* 65 */       if (this.amount == 0) {
/* 66 */         AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this.owner, this.owner, "Amplify"));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 73 */     if (isPlayer) {
/* 74 */       AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this.owner, this.owner, "Amplify"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\AmplifyPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */