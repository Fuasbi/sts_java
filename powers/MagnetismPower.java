/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class MagnetismPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Magnetism";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Magnetism");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String SINGULAR_DESCRIPTION = powerStrings.DESCRIPTIONS[0];
/* 16 */   public static final String PLURAL_DESCRIPTION = powerStrings.DESCRIPTIONS[1];
/*    */   
/*    */   public MagnetismPower(AbstractCreature owner, int cardAmount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Magnetism";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = cardAmount;
/* 23 */     updateDescription();
/* 24 */     this.img = ImageMaster.loadImage("images/powers/32/infiniteBlades.png");
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 29 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 30 */       flash();
/* 31 */       for (int i = 0; i < this.amount; i++) {
/* 32 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(
/*    */         
/* 34 */           AbstractDungeon.returnTrulyRandomColorlessCard("Bandage Up").makeCopy(), 1, false));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 43 */     this.fontScale = 8.0F;
/* 44 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 49 */     if (this.amount > 1) {
/* 50 */       this.description = String.format(PLURAL_DESCRIPTION, new Object[] { Integer.valueOf(this.amount) });
/*    */     } else {
/* 52 */       this.description = String.format(SINGULAR_DESCRIPTION, new Object[] { Integer.valueOf(this.amount) });
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\MagnetismPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */