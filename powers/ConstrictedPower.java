/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ConstrictedPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Constricted";
/* 12 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Constricted");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   public AbstractCreature source;
/*    */   
/*    */   public ConstrictedPower(AbstractCreature target, AbstractCreature source, int fadeAmt)
/*    */   {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Constricted";
/* 21 */     this.owner = target;
/* 22 */     this.source = source;
/* 23 */     this.amount = fadeAmt;
/* 24 */     updateDescription();
/* 25 */     loadRegion("constricted");
/* 26 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */     
/*    */ 
/* 29 */     this.priority = 105;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 34 */     CardCrawlGame.sound.play("POWER_CONSTRICTED", 0.05F);
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 39 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 44 */     flashWithoutSound();
/* 45 */     playApplyPowerSfx();
/* 46 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(this.owner, new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS)));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ConstrictedPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */