/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DoubleDamagePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Double Damage";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Double Damage");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/* 18 */   private boolean justApplied = false;
/*    */   
/*    */   public DoubleDamagePower(AbstractCreature owner, int amount, boolean isSourceMonster) {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Double Damage";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = amount;
/* 25 */     updateDescription();
/* 26 */     this.img = ImageMaster.loadImage("images/powers/32/doubleDamage.png");
/* 27 */     this.justApplied = isSourceMonster;
/* 28 */     this.type = AbstractPower.PowerType.BUFF;
/* 29 */     this.isTurnBased = true;
/* 30 */     this.priority = 6;
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 35 */     if (this.justApplied) {
/* 36 */       this.justApplied = false;
/* 37 */       return;
/*    */     }
/*    */     
/* 40 */     if (this.amount == 0) {
/* 41 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Double Damage"));
/*    */     } else {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Double Damage", 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 49 */     if (this.amount == 1) {
/* 50 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 52 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public float atDamageGive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 58 */     if (type == DamageInfo.DamageType.NORMAL) {
/* 59 */       return damage * 2.0F;
/*    */     }
/* 61 */     return damage;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DoubleDamagePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */