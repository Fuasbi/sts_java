/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class SlowPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Slow";
/* 16 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Slow");
/* 17 */   public static final String NAME = powerStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public SlowPower(AbstractCreature owner, int amount) {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Slow";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = amount;
/* 25 */     updateDescription();
/* 26 */     this.img = ImageMaster.loadImage("images/powers/32/slow.png");
/* 27 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 32 */     this.amount = 0;
/* 33 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 38 */     this.description = (DESCRIPTIONS[0] + FontHelper.colorString(this.owner.name, "y") + DESCRIPTIONS[1]);
/*    */     
/* 40 */     if (this.amount != 0) {
/* 41 */       this.description = (this.description + DESCRIPTIONS[2] + this.amount * 10 + DESCRIPTIONS[3]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onAfterUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 47 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new SlowPower(this.owner, 1), 1));
/*    */   }
/*    */   
/*    */   public float atDamageReceive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 52 */     if (type == DamageInfo.DamageType.NORMAL) {
/* 53 */       return damage * (1.0F + this.amount * 0.1F);
/*    */     }
/* 55 */     return damage;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\SlowPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */