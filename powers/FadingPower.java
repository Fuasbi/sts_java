/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.vfx.combat.ExplosionSmallEffect;
/*    */ 
/*    */ public class FadingPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Fading";
/* 15 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Fading");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public FadingPower(AbstractCreature owner, int turns) {
/* 20 */     this.name = NAME;
/* 21 */     this.ID = "Fading";
/* 22 */     this.owner = owner;
/* 23 */     this.amount = turns;
/* 24 */     updateDescription();
/* 25 */     loadRegion("fading");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 30 */     if (this.amount == 1) {
/* 31 */       this.description = DESCRIPTIONS[2];
/*    */     } else {
/* 33 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void duringTurn()
/*    */   {
/* 39 */     if ((this.amount == 1) && (!this.owner.isDying)) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new ExplosionSmallEffect(this.owner.hb.cX, this.owner.hb.cY), 0.1F));
/*    */       
/* 42 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SuicideAction((AbstractMonster)this.owner));
/*    */     } else {
/* 44 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Fading", 1));
/* 45 */       updateDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\FadingPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */