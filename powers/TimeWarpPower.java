/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class TimeWarpPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Time Warp";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Time Warp");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESC = powerStrings.DESCRIPTIONS;
/*    */   private static final int STR_AMT = 2;
/*    */   private static final int COUNTDOWN_AMT = 12;
/*    */   
/* 21 */   public TimeWarpPower(AbstractCreature owner) { this.name = NAME;
/* 22 */     this.ID = "Time Warp";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = 0;
/* 25 */     updateDescription();
/* 26 */     loadRegion("time");
/* 27 */     this.type = AbstractPower.PowerType.BUFF;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 32 */     CardCrawlGame.sound.play("POWER_TIME_WARP", 0.05F);
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 37 */     this.description = (DESC[0] + 12 + DESC[1] + 2 + DESC[2]);
/*    */   }
/*    */   
/*    */   public void onAfterUseCard(AbstractCard card, com.megacrit.cardcrawl.actions.utility.UseCardAction action)
/*    */   {
/* 42 */     flashWithoutSound();
/* 43 */     this.amount += 1;
/* 44 */     if (this.amount == 12) {
/* 45 */       this.amount = 0;
/* 46 */       playApplyPowerSfx();
/* 47 */       AbstractDungeon.actionManager.cardQueue.clear();
/* 48 */       for (AbstractCard c : AbstractDungeon.player.limbo.group) {
/* 49 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect(c));
/*    */       }
/* 51 */       AbstractDungeon.player.limbo.group.clear();
/* 52 */       AbstractDungeon.player.releaseCard();
/* 53 */       AbstractDungeon.overlayMenu.endTurnButton.disable(true);
/*    */       
/* 55 */       for (com.megacrit.cardcrawl.monsters.AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 56 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, m, new StrengthPower(m, 2), 2));
/*    */       }
/*    */     }
/*    */     
/* 60 */     updateDescription();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\TimeWarpPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */