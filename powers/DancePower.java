/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DancePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Dance Puppet";
/* 18 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Dance Puppet");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/* 22 */   private AbstractCard.CardType typeToCompare = AbstractCard.CardType.SKILL;
/* 23 */   private boolean firstTurnApplied = true;
/*    */   
/*    */   public DancePower(AbstractCreature owner, int amount) {
/* 26 */     this.name = NAME;
/* 27 */     this.ID = "Dance Puppet";
/* 28 */     this.owner = owner;
/* 29 */     this.amount = amount;
/* 30 */     updateDescription();
/* 31 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 36 */     if (this.typeToCompare == AbstractCard.CardType.SKILL) {
/* 37 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/* 38 */       this.img = ImageMaster.loadImage("images/powers/32/puppetSkill.png");
/*    */     } else {
/* 40 */       this.img = ImageMaster.loadImage("images/powers/32/puppetAttack2.png");
/* 41 */       this.description = (DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[1]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 47 */     if (card.type != this.typeToCompare) {
/* 48 */       flash();
/* 49 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(this.owner, new DamageInfo(this.owner, this.amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */       
/* 51 */       this.amount += 1;
/* 52 */       AbstractDungeon.actionManager.addToBottom(new TextAboveCreatureAction(this.owner, DESCRIPTIONS[3]));
/* 53 */       updateDescription();
/*    */     }
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 59 */     if (this.firstTurnApplied) {
/* 60 */       this.firstTurnApplied = false;
/*    */     } else {
/* 62 */       flash();
/* 63 */       if (this.typeToCompare == AbstractCard.CardType.SKILL) {
/* 64 */         this.typeToCompare = AbstractCard.CardType.ATTACK;
/* 65 */         updateDescription();
/*    */       } else {
/* 67 */         this.typeToCompare = AbstractCard.CardType.SKILL;
/* 68 */         updateDescription();
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DancePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */