/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class RitualPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Ritual";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Ritual");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/* 15 */   private boolean skipFirst = true;
/*    */   
/*    */   public RitualPower(AbstractCreature owner, int strAmt) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Ritual";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = strAmt;
/* 22 */     updateDescription();
/* 23 */     loadRegion("ritual");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 33 */     if (!this.skipFirst) {
/* 34 */       flash();
/* 35 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, this.amount), this.amount));
/*    */     }
/*    */     else {
/* 38 */       this.skipFirst = false;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RitualPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */