/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class SpiritConversionPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Spirit Conversion";
/* 10 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Spirit Conversion");
/* 11 */   public static final String NAME = powerStrings.NAME;
/* 12 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public SpiritConversionPower(AbstractCreature owner) {
/* 15 */     this.name = NAME;
/* 16 */     this.ID = "Spirit Conversion";
/* 17 */     this.owner = owner;
/* 18 */     updateDescription();
/* 19 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/noxiousFumes.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 24 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\SpiritConversionPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */