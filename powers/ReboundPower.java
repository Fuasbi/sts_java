/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ReboundPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Rebound";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Rebound");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/* 18 */   private boolean justEvoked = true;
/*    */   
/*    */   public ReboundPower(AbstractCreature owner) {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Rebound";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = 1;
/* 25 */     updateDescription();
/* 26 */     loadRegion("rebound");
/* 27 */     this.isTurnBased = true;
/* 28 */     this.type = AbstractPower.PowerType.BUFF;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 33 */     if (this.amount > 1) {
/* 34 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     } else {
/* 36 */       this.description = DESCRIPTIONS[0];
/*    */     }
/*    */   }
/*    */   
/*    */   public void onAfterUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 42 */     if (this.justEvoked) {
/* 43 */       this.justEvoked = false;
/* 44 */       return;
/*    */     }
/*    */     
/* 47 */     if ((card.type != AbstractCard.CardType.POWER) && (!card.exhaust) && (!card.exhaustOnUseOnce)) {
/* 48 */       flash();
/* 49 */       action.reboundCard = true;
/*    */     }
/*    */     
/* 52 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Rebound", 1));
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 57 */     if (isPlayer) {
/* 58 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Rebound"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ReboundPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */