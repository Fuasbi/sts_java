/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DEPRECATED_PhaseShiftPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Phase Shift";
/* 12 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Phase Shift");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String DESCRIPTION = powerStrings.DESCRIPTIONS[0];
/*    */   private int TURNS_FOR_INTANGIBLE;
/*    */   
/*    */   public DEPRECATED_PhaseShiftPower(AbstractCreature owner, int amt)
/*    */   {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Phase Shift";
/* 21 */     this.owner = owner;
/* 22 */     this.TURNS_FOR_INTANGIBLE = amt;
/* 23 */     this.amount = this.TURNS_FOR_INTANGIBLE;
/* 24 */     updateDescription();
/* 25 */     loadRegion("platedarmor");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 30 */     this.description = String.format(DESCRIPTION, new Object[] { Integer.valueOf(this.amount) });
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 35 */     this.amount -= 1;
/* 36 */     if (this.amount == 0) {
/* 37 */       this.amount = this.TURNS_FOR_INTANGIBLE;
/* 38 */       com.megacrit.cardcrawl.characters.AbstractPlayer p = AbstractDungeon.player;
/* 39 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(p, p, new IntangiblePlayerPower(p, 1), 1));
/*    */     }
/* 41 */     updateDescription();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DEPRECATED_PhaseShiftPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */