/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class FlightPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Flight";
/* 16 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Flight");
/* 17 */   public static final String NAME = powerStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private int storedAmount;
/*    */   
/*    */   public FlightPower(AbstractCreature owner, int amount)
/*    */   {
/* 23 */     this.name = NAME;
/* 24 */     this.ID = "Flight";
/* 25 */     this.owner = owner;
/* 26 */     this.amount = amount;
/* 27 */     this.storedAmount = amount;
/* 28 */     updateDescription();
/* 29 */     loadRegion("flight");
/* 30 */     this.priority = 50;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 35 */     CardCrawlGame.sound.play("POWER_FLIGHT", 0.05F);
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 45 */     this.amount = this.storedAmount;
/*    */   }
/*    */   
/*    */   public float atDamageFinalReceive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 50 */     return calculateDamageTakenAmount(damage, type);
/*    */   }
/*    */   
/*    */   private float calculateDamageTakenAmount(float damage, DamageInfo.DamageType type) {
/* 54 */     if ((type != DamageInfo.DamageType.HP_LOSS) && (type != DamageInfo.DamageType.THORNS)) {
/* 55 */       return damage / 2.0F;
/*    */     }
/* 57 */     return damage;
/*    */   }
/*    */   
/*    */ 
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 63 */     Boolean willLive = Boolean.valueOf(calculateDamageTakenAmount(damageAmount, info.type) < this.owner.currentHealth);
/* 64 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.type != DamageInfo.DamageType.THORNS) && (damageAmount > 0) && 
/* 65 */       (willLive.booleanValue())) {
/* 66 */       flash();
/* 67 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Flight", 1));
/*    */     }
/* 69 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void onRemove()
/*    */   {
/* 74 */     AbstractDungeon.actionManager.addToBottom(new ChangeStateAction((AbstractMonster)this.owner, "GROUNDED"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\FlightPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */