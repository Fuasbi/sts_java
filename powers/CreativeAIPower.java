/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class CreativeAIPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Creative AI";
/* 14 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Creative AI");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public CreativeAIPower(AbstractCreature owner, int amt) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Creative AI";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = amt;
/* 23 */     updateDescription();
/* 24 */     loadRegion("ai");
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 29 */     for (int i = 0; i < this.amount; i++)
/*    */     {
/*    */ 
/* 32 */       AbstractCard card = AbstractDungeon.returnTrulyRandomCard("Self Repair", com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER, AbstractDungeon.cardRandomRng).makeCopy();
/*    */       
/* 34 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInHandAction(card));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     if (this.amount > 1) {
/* 41 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     } else {
/* 43 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\CreativeAIPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */