/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class EchoPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Echo Form";
/* 19 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Echo Form");
/* 20 */   public static final String NAME = powerStrings.NAME;
/* 21 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/* 22 */   private int cardsDoubledThisTurn = 0;
/*    */   
/*    */   public EchoPower(AbstractCreature owner, int amount) {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Echo Form";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = amount;
/* 29 */     updateDescription();
/* 30 */     loadRegion("echo");
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription()
/*    */   {
/* 36 */     if (this.amount == 1) {
/* 37 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 39 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 45 */     this.cardsDoubledThisTurn = 0;
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 50 */     if ((!card.purgeOnUse) && (this.amount > 0) && (AbstractDungeon.actionManager.cardsPlayedThisTurn.size() - this.cardsDoubledThisTurn <= this.amount))
/*    */     {
/* 52 */       this.cardsDoubledThisTurn += 1;
/* 53 */       flash();
/* 54 */       AbstractMonster m = null;
/*    */       
/* 56 */       if (action.target != null) {
/* 57 */         m = (AbstractMonster)action.target;
/*    */       }
/*    */       
/* 60 */       AbstractCard tmp = card.makeStatEquivalentCopy();
/* 61 */       AbstractDungeon.player.limbo.addToBottom(tmp);
/* 62 */       tmp.current_x = card.current_x;
/* 63 */       tmp.current_y = card.current_y;
/* 64 */       tmp.target_x = (Settings.WIDTH / 2.0F - 300.0F * Settings.scale);
/* 65 */       tmp.target_y = (Settings.HEIGHT / 2.0F);
/* 66 */       tmp.freeToPlayOnce = true;
/*    */       
/* 68 */       if (m != null) {
/* 69 */         tmp.calculateCardDamage(m);
/*    */       }
/*    */       
/* 72 */       tmp.purgeOnUse = true;
/* 73 */       AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(tmp, m, card.energyOnUse));
/*    */       
/*    */ 
/* 76 */       if (tmp.cardID.equals("Rampage")) {
/* 77 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ModifyDamageAction(card, tmp.magicNumber));
/* 78 */       } else if (tmp.cardID.equals("Genetic Algorithm")) {
/* 79 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.IncreaseMiscAction(card.cardID, card.misc + card.magicNumber, card.magicNumber));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\EchoPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */