/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.vfx.combat.CleaveEffect;
/*    */ 
/*    */ public class ThousandCutsPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Thousand Cuts";
/* 18 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Thousand Cuts");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public ThousandCutsPower(AbstractCreature owner, int amount) {
/* 23 */     this.name = NAME;
/* 24 */     this.ID = "Thousand Cuts";
/* 25 */     this.owner = owner;
/* 26 */     this.amount = amount;
/* 27 */     updateDescription();
/* 28 */     this.img = ImageMaster.loadImage("images/powers/32/thousandCuts.png");
/*    */   }
/*    */   
/*    */   public void onAfterCardPlayed(AbstractCard card)
/*    */   {
/* 33 */     flash();
/* 34 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("ATTACK_HEAVY"));
/* 35 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(this.owner, new CleaveEffect(), 0.25F));
/* 36 */     AbstractDungeon.actionManager.addToBottom(new DamageAllEnemiesAction(this.owner, 
/*    */     
/*    */ 
/* 39 */       DamageInfo.createDamageMatrix(this.amount, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 46 */     this.fontScale = 8.0F;
/* 47 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 52 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ThousandCutsPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */