/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class PlatedArmorPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Plated Armor";
/* 18 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Plated Armor");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private static final int DECREMENT_AMT = 1;
/*    */   
/*    */   public PlatedArmorPower(AbstractCreature owner, int amt)
/*    */   {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Plated Armor";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = amt;
/* 29 */     updateDescription();
/* 30 */     loadRegion("platedarmor");
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 35 */     CardCrawlGame.sound.play("POWER_PLATED", 0.05F);
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     if (this.owner.isPlayer) {
/* 41 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 43 */       this.description = (DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[3]);
/*    */     }
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 49 */     if ((info.owner != null) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.type != DamageInfo.DamageType.THORNS) && (damageAmount > 0) && 
/* 50 */       (!this.owner.hasPower("Buffer"))) {
/* 51 */       flash();
/* 52 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Plated Armor", 1));
/*    */     }
/* 54 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void onRemove()
/*    */   {
/* 59 */     if (!this.owner.isPlayer) {
/* 60 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction((AbstractMonster)this.owner, "ARMOR_BREAK"));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 67 */     flash();
/* 68 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this.owner, this.owner, this.amount));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PlatedArmorPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */