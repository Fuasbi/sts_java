/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class ThornsPower extends AbstractPower
/*    */ {
/* 16 */   private static final Logger logger = LogManager.getLogger(ThornsPower.class.getName());
/*    */   public static final String POWER_ID = "Thorns";
/* 18 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Thorns");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private DamageInfo thornsInfo;
/*    */   
/*    */   public ThornsPower(AbstractCreature owner, int thornsDamage)
/*    */   {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Thorns";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = thornsDamage;
/* 29 */     updateDescription();
/* 30 */     loadRegion("thorns");
/* 31 */     this.thornsInfo = new DamageInfo(this.owner, this.amount, DamageInfo.DamageType.THORNS);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount) {
/* 35 */     if (this.amount == -1) {
/* 36 */       logger.info(this.name + " does not stack");
/* 37 */       return;
/*    */     }
/* 39 */     this.fontScale = 8.0F;
/* 40 */     this.amount += stackAmount;
/* 41 */     this.thornsInfo = new DamageInfo(null, this.amount, DamageInfo.DamageType.THORNS);
/* 42 */     updateDescription();
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 47 */     if ((info.type != DamageInfo.DamageType.THORNS) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.owner != null) && (info.owner != this.owner))
/*    */     {
/* 49 */       flash();
/* 50 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(info.owner, this.thornsInfo, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL, true));
/*    */     }
/*    */     
/* 53 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 58 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ThornsPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */