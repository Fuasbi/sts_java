/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DrawDownPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Draw Down";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Draw Down");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DrawDownPower(AbstractCreature owner, int amount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Draw Down";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amount;
/* 21 */     updateDescription();
/* 22 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/exhaustion.png");
/* 23 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void onInitialApplication()
/*    */   {
/* 28 */     AbstractDungeon.player.gameHandSize -= this.amount;
/*    */   }
/*    */   
/*    */ 
/*    */   public void stackPower(int amt)
/*    */   {
/* 34 */     super.stackPower(amt);
/* 35 */     AbstractDungeon.player.gameHandSize -= amt;
/* 36 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw() {
/* 40 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Draw Down"));
/*    */   }
/*    */   
/*    */   public void onRemove()
/*    */   {
/* 45 */     AbstractDungeon.player.gameHandSize += this.amount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 50 */     if (this.amount == 1) {
/* 51 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 53 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DrawDownPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */