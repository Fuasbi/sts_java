/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DexterityPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Dexterity";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Dexterity");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DexterityPower(AbstractCreature owner, int amount) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Dexterity";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = amount;
/* 20 */     if (this.amount >= 999) {
/* 21 */       this.amount = 999;
/*    */     }
/*    */     
/* 24 */     if (this.amount <= 64537) {
/* 25 */       this.amount = 64537;
/*    */     }
/* 27 */     updateDescription();
/* 28 */     loadRegion("dexterity");
/* 29 */     this.canGoNegative = true;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 34 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("POWER_DEXTERITY", 0.05F);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 39 */     this.fontScale = 8.0F;
/* 40 */     this.amount += stackAmount;
/*    */     
/* 42 */     if (this.amount == 0) {
/* 43 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, "Dexterity"));
/*    */     }
/*    */     
/* 46 */     if (this.amount >= 999) {
/* 47 */       this.amount = 999;
/*    */     }
/*    */     
/* 50 */     if (this.amount <= 64537) {
/* 51 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void reducePower(int reduceAmount)
/*    */   {
/* 57 */     this.fontScale = 8.0F;
/* 58 */     this.amount -= reduceAmount;
/*    */     
/* 60 */     if (this.amount == 0) {
/* 61 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, "Dexterity"));
/*    */     }
/*    */     
/* 64 */     if (this.amount >= 999) {
/* 65 */       this.amount = 999;
/*    */     }
/*    */     
/* 68 */     if (this.amount <= 64537) {
/* 69 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 75 */     if (this.amount > 0) {
/* 76 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/* 77 */       this.type = AbstractPower.PowerType.BUFF;
/*    */     } else {
/* 79 */       int tmp = -this.amount;
/* 80 */       this.description = (DESCRIPTIONS[1] + tmp + DESCRIPTIONS[2]);
/* 81 */       this.type = AbstractPower.PowerType.DEBUFF;
/*    */     }
/*    */   }
/*    */   
/*    */   public float modifyBlock(float blockAmount)
/*    */   {
/* 87 */     if (blockAmount += this.amount < 0.0F) {
/* 88 */       return 0.0F;
/*    */     }
/* 90 */     return blockAmount;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DexterityPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */