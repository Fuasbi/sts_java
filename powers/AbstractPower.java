/*     */ package com.megacrit.cardcrawl.powers;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.FlashPowerEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.GainPowerEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.SilentGainPowerEffect;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public abstract class AbstractPower implements Comparable<AbstractPower>
/*     */ {
/*  36 */   private static final Logger logger = LogManager.getLogger(AbstractPower.class.getName());
/*     */   public static TextureAtlas atlas;
/*     */   public TextureAtlas.AtlasRegion region48;
/*     */   public TextureAtlas.AtlasRegion region128;
/*     */   private static final int RAW_W = 32;
/*     */   protected static final float POWER_STACK_FONT_SCALE = 8.0F;
/*     */   private static final float FONT_LERP = 10.0F;
/*     */   private static final float FONT_SNAP_THRESHOLD = 0.05F;
/*  44 */   protected float fontScale = 1.0F;
/*  45 */   private Color color = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  46 */   private Color redColor = new Color(1.0F, 0.0F, 0.0F, 1.0F);
/*  47 */   private Color greenColor = new Color(0.0F, 1.0F, 0.0F, 1.0F);
/*  48 */   public static final float TIP_OFFSET_X_M = -360.0F * Settings.scale;
/*  49 */   public static final float TIP_OFFSET_X_P = 90.0F * Settings.scale;
/*  50 */   public static final float TIP_OFFSET_Y = -50.0F * Settings.scale;
/*  51 */   private ArrayList<AbstractGameEffect> effect = new ArrayList();
/*     */   public AbstractCreature owner;
/*     */   public String name;
/*     */   public String description;
/*     */   public String ID;
/*  56 */   public com.badlogic.gdx.graphics.Texture img; public int amount; public int priority = 5;
/*  57 */   public PowerType type = PowerType.BUFF;
/*  58 */   protected boolean isTurnBased = false;
/*  59 */   public boolean isHovered = false; public boolean isPostActionPower = false; public boolean isSelected = false; public boolean finished = false;
/*  60 */   public boolean canGoNegative = false;
/*     */   
/*     */   public static String[] DESCRIPTIONS;
/*     */   
/*     */   public static enum PowerType
/*     */   {
/*  66 */     BUFF,  DEBUFF;
/*     */     
/*     */     private PowerType() {} }
/*     */   
/*  70 */   public static void initialize() { atlas = new TextureAtlas(Gdx.files.internal("powers/powers.atlas")); }
/*     */   
/*     */   protected void loadRegion(String fileName)
/*     */   {
/*  74 */     this.region48 = atlas.findRegion("48/" + fileName);
/*  75 */     this.region128 = atlas.findRegion("128/" + fileName);
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/*  80 */     return "[" + this.name + "]: " + this.description;
/*     */   }
/*     */   
/*     */   public void playApplyPowerSfx() {
/*  84 */     if (this.type == PowerType.BUFF) {
/*  85 */       int roll = MathUtils.random(0, 2);
/*  86 */       if (roll == 0) {
/*  87 */         CardCrawlGame.sound.play("BUFF_1");
/*  88 */       } else if (roll == 1) {
/*  89 */         CardCrawlGame.sound.play("BUFF_2");
/*     */       } else {
/*  91 */         CardCrawlGame.sound.play("BUFF_3");
/*     */       }
/*     */     }
/*     */     else {
/*  95 */       int roll = MathUtils.random(0, 2);
/*  96 */       if (roll == 0) {
/*  97 */         CardCrawlGame.sound.play("DEBUFF_1");
/*  98 */       } else if (roll == 1) {
/*  99 */         CardCrawlGame.sound.play("DEBUFF_2");
/*     */       } else {
/* 101 */         CardCrawlGame.sound.play("DEBUFF_3");
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void updateParticles() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void update(int slot)
/*     */   {
/* 114 */     updateFlash();
/* 115 */     updateFontScale();
/* 116 */     updateColor();
/*     */   }
/*     */   
/*     */   private void updateFlash() {
/* 120 */     for (Iterator<AbstractGameEffect> i = this.effect.iterator(); i.hasNext();) {
/* 121 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 122 */       e.update();
/* 123 */       if (e.isDone) {
/* 124 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateColor() {
/* 130 */     if (this.color.a != 1.0F) {
/* 131 */       this.color.a = MathHelper.fadeLerpSnap(this.color.a, 1.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFontScale() {
/* 136 */     if (this.fontScale != 1.0F) {
/* 137 */       this.fontScale = MathUtils.lerp(this.fontScale, 1.0F, Gdx.graphics.getDeltaTime() * 10.0F);
/* 138 */       if (this.fontScale - 1.0F < 0.05F) {
/* 139 */         this.fontScale = 1.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateDescription() {}
/*     */   
/*     */   public void stackPower(int stackAmount)
/*     */   {
/* 148 */     if (this.amount == -1) {
/* 149 */       logger.info(this.name + " does not stack");
/* 150 */       return;
/*     */     }
/* 152 */     this.fontScale = 8.0F;
/* 153 */     this.amount += stackAmount;
/*     */   }
/*     */   
/*     */   public void reducePower(int reduceAmount) {
/* 157 */     if (this.amount - reduceAmount <= 0) {
/* 158 */       this.fontScale = 8.0F;
/* 159 */       this.amount = 0;
/*     */     }
/*     */     else {
/* 162 */       this.fontScale = 8.0F;
/* 163 */       this.amount -= reduceAmount;
/*     */     }
/*     */   }
/*     */   
/*     */   public String getHoverMessage() {
/* 168 */     return this.name + ":\n" + this.description;
/*     */   }
/*     */   
/*     */   public void renderIcons(SpriteBatch sb, float x, float y, Color c) {
/* 172 */     if (this.img != null) {
/* 173 */       sb.setColor(c);
/* 174 */       sb.draw(this.img, x - 12.0F, y - 12.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale * 1.5F, Settings.scale * 1.5F, 0.0F, 0, 0, 32, 32, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 192 */       sb.setColor(c);
/* 193 */       sb.draw(this.region48, x - this.region48.packedWidth / 2.0F, y - this.region48.packedHeight / 2.0F, this.region48.packedWidth / 2.0F, this.region48.packedHeight / 2.0F, this.region48.packedWidth, this.region48.packedHeight, Settings.scale, Settings.scale, 0.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 206 */     for (AbstractGameEffect e : this.effect) {
/* 207 */       e.render(sb, x, y);
/*     */     }
/*     */   }
/*     */   
/*     */   public void renderAmount(SpriteBatch sb, float x, float y, Color c) {
/* 212 */     if (this.amount > 0) {
/* 213 */       if (!this.isTurnBased) {
/* 214 */         this.greenColor.a = c.a;
/* 215 */         c = this.greenColor;
/*     */       }
/* 217 */       FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, 
/*     */       
/*     */ 
/* 220 */         Integer.toString(this.amount), x, y, this.fontScale, c);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 225 */     else if ((this.amount < 0) && (this.canGoNegative)) {
/* 226 */       this.redColor.a = c.a;
/* 227 */       c = this.redColor;
/*     */       
/* 229 */       FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, 
/*     */       
/*     */ 
/* 232 */         Integer.toString(this.amount), x, y, this.fontScale, c);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public float atDamageGive(float damage, DamageInfo.DamageType type)
/*     */   {
/* 241 */     return damage;
/*     */   }
/*     */   
/*     */   public float atDamageFinalGive(float damage, DamageInfo.DamageType type) {
/* 245 */     return damage;
/*     */   }
/*     */   
/*     */   public float atDamageFinalReceive(float damage, DamageInfo.DamageType type) {
/* 249 */     return damage;
/*     */   }
/*     */   
/*     */   public float atDamageReceive(float damage, DamageInfo.DamageType damageType) {
/* 253 */     return damage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void atStartOfTurn() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void duringTurn() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void atStartOfTurnPostDraw() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void atEndOfTurn(boolean isPlayer) {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void atEndOfRound() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onDamageAllEnemies(int[] damage) {}
/*     */   
/*     */ 
/*     */   public int onHeal(int healAmount)
/*     */   {
/* 283 */     return healAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int onAttacked(DamageInfo info, int damageAmount)
/*     */   {
/* 293 */     return damageAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onAttack(DamageInfo info, int damageAmount, AbstractCreature target) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onEvokeOrb(AbstractOrb orb) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onPlayCard(AbstractCard card, AbstractMonster m) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onUseCard(AbstractCard card, UseCardAction action) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onAfterUseCard(AbstractCard card, UseCardAction action) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onSpecificTrigger() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onDeath() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onChannel(AbstractOrb orb) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void atEnergyGain() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onExhaust(AbstractCard card) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public float modifyBlock(float blockAmount)
/*     */   {
/* 364 */     return blockAmount;
/*     */   }
/*     */   
/*     */   public void onGainedBlock(float blockAmount) {}
/*     */   
/*     */   public int onPlayerGainedBlock(float blockAmount)
/*     */   {
/* 371 */     return MathUtils.floor(blockAmount);
/*     */   }
/*     */   
/*     */   public int onPlayerGainedBlock(int blockAmount) {
/* 375 */     return blockAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onGainCharge(int chargeAmount) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onRemove() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onEnergyRecharge() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onDrawOrDiscard() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onAfterCardPlayed(AbstractCard usedCard) {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onInitialApplication() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public int compareTo(AbstractPower other)
/*     */   {
/* 410 */     return this.priority - other.priority;
/*     */   }
/*     */   
/*     */   public void flash() {
/* 414 */     this.effect.add(new GainPowerEffect(this));
/* 415 */     AbstractDungeon.effectList.add(new FlashPowerEffect(this));
/*     */   }
/*     */   
/*     */   public void flashWithoutSound() {
/* 419 */     this.effect.add(new SilentGainPowerEffect(this));
/* 420 */     AbstractDungeon.effectList.add(new FlashPowerEffect(this));
/*     */   }
/*     */   
/*     */   public void onApplyPower(AbstractPower power, AbstractCreature target, AbstractCreature source) {}
/*     */   
/*     */   public HashMap<String, Serializable> getLocStrings()
/*     */   {
/* 427 */     HashMap<String, Serializable> powerData = new HashMap();
/* 428 */     powerData.put("name", this.name);
/* 429 */     powerData.put("description", DESCRIPTIONS);
/* 430 */     return powerData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int onLoseHp(int damageAmount)
/*     */   {
/* 440 */     return damageAmount;
/*     */   }
/*     */   
/*     */   public void onVictory() {}
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\AbstractPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */