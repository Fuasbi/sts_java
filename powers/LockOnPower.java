/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class LockOnPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Lockon";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Lockon");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   public static final float MULTIPLIER = 1.5F;
/*    */   private static final int MULTI_STR = 50;
/*    */   
/*    */   public LockOnPower(AbstractCreature owner, int amount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Lockon";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = amount;
/* 23 */     updateDescription();
/* 24 */     loadRegion("lockon");
/* 25 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 26 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 31 */     if (this.amount == 0) {
/* 32 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Lockon"));
/*    */     } else {
/* 34 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Lockon", 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     if (this.owner != null) {
/* 41 */       if (this.amount == 1) {
/* 42 */         this.description = (DESCRIPTIONS[0] + 50 + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */       } else {
/* 44 */         this.description = (DESCRIPTIONS[0] + 50 + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[3]);
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\LockOnPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */