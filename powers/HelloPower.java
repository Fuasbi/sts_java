/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class HelloPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Hello";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Hello");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public HelloPower(AbstractCreature owner, int cardAmt) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Hello";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = cardAmt;
/* 21 */     updateDescription();
/* 22 */     loadRegion("hello");
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 27 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 28 */       flash();
/* 29 */       for (int i = 0; i < this.amount; i++) {
/* 30 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(
/*    */         
/*    */ 
/* 33 */           AbstractDungeon.getCard(com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, AbstractDungeon.cardRandomRng)
/* 34 */           .makeCopy(), 1, false));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 43 */     this.fontScale = 8.0F;
/* 44 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 49 */     if (this.amount > 1) {
/* 50 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 52 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\HelloPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */