/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DrawCardNextTurnPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Draw Card";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Draw Card");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DrawCardNextTurnPower(AbstractCreature owner, int drawAmount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Draw Card";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = drawAmount;
/* 21 */     updateDescription();
/* 22 */     loadRegion("carddraw");
/* 23 */     this.priority = 20;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     if (this.amount > 1) {
/* 29 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 31 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 37 */     flash();
/* 38 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.owner, this.amount));
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Draw Card"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DrawCardNextTurnPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */