/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ public class TimeDilationPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "TimeDilation";
/* 16 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("TimeDilation");
/* 17 */   public static final String NAME = powerStrings.NAME;
/* 18 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/* 19 */   private ArrayList<AbstractPower> savedPowers = new ArrayList();
/*    */   
/*    */   public TimeDilationPower(AbstractCreature owner, int amount) {
/* 22 */     this.name = NAME;
/* 23 */     this.ID = "TimeDilation";
/* 24 */     this.owner = owner;
/* 25 */     this.amount = amount;
/* 26 */     updateDescription();
/* 27 */     loadRegion("time");
/* 28 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 29 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */ 
/*    */   public void onInitialApplication()
/*    */   {
/* 35 */     for (AbstractPower p : this.owner.powers) {
/* 36 */       if (!p.ID.equals("TimeDilation")) {
/* 37 */         this.savedPowers.add(p);
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 42 */     for (Object p = this.owner.powers.iterator(); ((Iterator)p).hasNext();) {
/* 43 */       AbstractPower e = (AbstractPower)((Iterator)p).next();
/* 44 */       if (!e.ID.equals("TimeDilation")) {
/* 45 */         ((Iterator)p).remove();
/*    */       }
/*    */     }
/* 48 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 53 */     if (this.amount == 0) {
/* 54 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "TimeDilation"));
/*    */     } else {
/* 56 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "TimeDilation", 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 62 */     if (this.amount <= 1) {
/* 63 */       if (this.savedPowers.size() == 1) {
/* 64 */         this.description = (DESCRIPTIONS[0] + this.savedPowers.size() + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[3]);
/*    */       } else {
/* 66 */         this.description = (DESCRIPTIONS[0] + this.savedPowers.size() + DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[3]);
/*    */       }
/*    */     }
/* 69 */     else if (this.savedPowers.size() == 1) {
/* 70 */       this.description = (DESCRIPTIONS[0] + this.savedPowers.size() + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[4]);
/*    */     } else {
/* 72 */       this.description = (DESCRIPTIONS[0] + this.savedPowers.size() + DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[4]);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void onRemove()
/*    */   {
/* 79 */     if (!this.owner.isDying) {
/* 80 */       for (AbstractPower power : this.savedPowers) {
/* 81 */         AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.owner, AbstractDungeon.player, power, power.amount));
/*    */       }
/*    */       
/* 84 */       this.savedPowers.clear();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\TimeDilationPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */