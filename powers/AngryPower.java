/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class AngryPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Angry";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Angry");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public AngryPower(AbstractCreature owner, int attackAmount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Angry";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = attackAmount;
/* 23 */     updateDescription();
/* 24 */     this.isPostActionPower = true;
/* 25 */     loadRegion("anger");
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 30 */     if ((info.owner != null) && (damageAmount > 0) && (info.type != DamageInfo.DamageType.HP_LOSS) && (info.type != DamageInfo.DamageType.THORNS))
/*    */     {
/* 32 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, this.amount), this.amount));
/*    */       
/* 34 */       flash();
/*    */     }
/* 36 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 41 */     this.description = (DESCRIPTIONS[0] + FontHelper.colorString(this.owner.name, "y") + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\AngryPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */