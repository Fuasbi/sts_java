/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class NoBlockPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "NoBlockPower";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("NoBlockPower");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/* 15 */   private boolean justApplied = false;
/*    */   
/*    */   public NoBlockPower(AbstractCreature owner, int amount, boolean isSourceMonster) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "NoBlockPower";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = amount;
/* 22 */     updateDescription();
/* 23 */     loadRegion("frail");
/*    */     
/* 25 */     if ((AbstractDungeon.actionManager.turnHasEnded) && (isSourceMonster)) {
/* 26 */       this.justApplied = true;
/*    */     }
/*    */     
/* 29 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 30 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 35 */     if (this.justApplied) {
/* 36 */       this.justApplied = false;
/* 37 */       return;
/*    */     }
/*    */     
/* 40 */     if (this.amount == 0) {
/* 41 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "NoBlockPower"));
/*    */     } else {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "NoBlockPower", 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 49 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public float modifyBlock(float blockAmount)
/*    */   {
/* 54 */     return 0.0F;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\NoBlockPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */