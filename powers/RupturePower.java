/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class RupturePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Rupture";
/* 13 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Rupture");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public RupturePower(AbstractCreature owner, int strAmt) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Rupture";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = strAmt;
/* 22 */     updateDescription();
/* 23 */     this.isPostActionPower = true;
/* 24 */     this.img = ImageMaster.loadImage("images/powers/32/rupture.png");
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount)
/*    */   {
/* 29 */     if ((damageAmount > 0) && (info.owner == this.owner)) {
/* 30 */       flash();
/* 31 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, this.amount), this.amount));
/*    */     }
/*    */     
/* 34 */     return damageAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 39 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RupturePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */