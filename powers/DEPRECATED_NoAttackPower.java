/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DEPRECATED_NoAttackPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "No Attack";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("No Attack");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DEPRECATED_NoAttackPower(AbstractCreature owner) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "No Attack";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = 1;
/* 20 */     updateDescription();
/* 21 */     loadRegion("noattack");
/* 22 */     this.isTurnBased = true;
/* 23 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount) {}
/*    */   
/*    */ 
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 38 */     if (isPlayer) {
/* 39 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "No Attack"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DEPRECATED_NoAttackPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */