/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class IntangiblePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Intangible";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Intangible");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private boolean justApplied;
/*    */   
/*    */   public IntangiblePower(AbstractCreature owner, int turns) {
/* 20 */     this.name = NAME;
/* 21 */     this.ID = "Intangible";
/* 22 */     this.owner = owner;
/* 23 */     this.amount = turns;
/* 24 */     updateDescription();
/* 25 */     this.img = ImageMaster.loadImage("images/powers/32/ghost.png");
/* 26 */     this.priority = 99;
/* 27 */     this.justApplied = true;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 32 */     CardCrawlGame.sound.play("POWER_INTANGIBLE", 0.05F);
/*    */   }
/*    */   
/*    */   public float atDamageReceive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 37 */     if (damage > 1.0F) {
/* 38 */       damage = 1.0F;
/*    */     }
/* 40 */     return damage;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 45 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 50 */     if (this.justApplied) {
/* 51 */       this.justApplied = false;
/* 52 */       return;
/*    */     }
/* 54 */     flash();
/*    */     
/* 56 */     if (this.amount == 0) {
/* 57 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Intangible"));
/*    */     } else {
/* 59 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Intangible", 1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\IntangiblePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */