/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DarkEmbracePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Dark Embrace";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Dark Embrace");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DarkEmbracePower(AbstractCreature owner, int amount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Dark Embrace";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amount;
/* 21 */     updateDescription();
/* 22 */     loadRegion("darkembrace");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 27 */     if (this.amount == 1) {
/* 28 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 30 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onExhaust(AbstractCard card)
/*    */   {
/* 36 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 37 */       flash();
/* 38 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.owner, this.amount));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DarkEmbracePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */