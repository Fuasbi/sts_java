/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.SwordBoomerangAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class JuggernautPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Juggernaut";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Juggernaut");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public JuggernautPower(AbstractCreature owner, int newAmount) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Juggernaut";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = newAmount;
/* 22 */     updateDescription();
/* 23 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/exoskeleton.png");
/*    */   }
/*    */   
/*    */   public void onGainedBlock(float blockAmount)
/*    */   {
/* 28 */     if (blockAmount > 0.0F) {
/* 29 */       flash();
/* 30 */       AbstractDungeon.actionManager.addToBottom(new SwordBoomerangAction(
/*    */       
/* 32 */         AbstractDungeon.getMonsters().getRandomMonster(true), new com.megacrit.cardcrawl.cards.DamageInfo(this.owner, this.amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS), 1));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void updateDescription()
/*    */   {
/* 40 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\JuggernautPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */