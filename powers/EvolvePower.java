/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class EvolvePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Evolve";
/*  9 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Evolve");
/* 10 */   public static final String NAME = powerStrings.NAME;
/* 11 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public EvolvePower(AbstractCreature owner, int drawAmt) {
/* 14 */     this.name = NAME;
/* 15 */     this.ID = "Evolve";
/* 16 */     this.owner = owner;
/* 17 */     this.amount = drawAmt;
/* 18 */     updateDescription();
/* 19 */     loadRegion("evolve");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 24 */     if (this.amount == 1) {
/* 25 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 27 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\EvolvePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */