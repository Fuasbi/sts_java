/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.LoseBlockAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DEPRECATED_OverloadedPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Overloaded";
/* 13 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Overloaded");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private boolean isAllBlock;
/*    */   
/*    */   public DEPRECATED_OverloadedPower(AbstractCreature owner, int loseAmount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Overloaded";
/* 21 */     this.owner = owner;
/* 22 */     this.isAllBlock = (loseAmount > 1000);
/* 23 */     this.amount = (this.isAllBlock ? 0 : loseAmount);
/* 24 */     updateDescription();
/* 25 */     loadRegion("frail");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 30 */     this.description = (this.isAllBlock ? DESCRIPTIONS[0] : String.format(DESCRIPTIONS[1], new Object[] { Integer.valueOf(this.amount) }));
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 35 */     flash();
/* 36 */     if (this.isAllBlock) {
/* 37 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveAllBlockAction(AbstractDungeon.player, AbstractDungeon.player));
/*    */     }
/*    */     else {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new LoseBlockAction(this.owner, this.owner, this.amount));
/*    */     }
/* 42 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Overloaded"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DEPRECATED_OverloadedPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */