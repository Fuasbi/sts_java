/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DoubleTapPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Double Tap";
/* 20 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Double Tap");
/* 21 */   public static final String NAME = powerStrings.NAME;
/* 22 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DoubleTapPower(AbstractCreature owner, int amount) {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Double Tap";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = amount;
/* 29 */     updateDescription();
/* 30 */     this.img = ImageMaster.loadImage("images/powers/32/doubleTap.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 35 */     if (this.amount == 1) {
/* 36 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 38 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 44 */     if ((!card.purgeOnUse) && (card.type == AbstractCard.CardType.ATTACK) && (this.amount > 0)) {
/* 45 */       flash();
/* 46 */       AbstractMonster m = null;
/*    */       
/* 48 */       if (action.target != null) {
/* 49 */         m = (AbstractMonster)action.target;
/*    */       }
/*    */       
/* 52 */       AbstractCard tmp = card.makeStatEquivalentCopy();
/* 53 */       AbstractDungeon.player.limbo.addToBottom(tmp);
/* 54 */       tmp.current_x = card.current_x;
/* 55 */       tmp.current_y = card.current_y;
/* 56 */       tmp.target_x = (Settings.WIDTH / 2.0F - 300.0F * Settings.scale);
/* 57 */       tmp.target_y = (Settings.HEIGHT / 2.0F);
/* 58 */       tmp.freeToPlayOnce = true;
/*    */       
/* 60 */       if (m != null) {
/* 61 */         tmp.calculateCardDamage(m);
/*    */       }
/*    */       
/* 64 */       tmp.purgeOnUse = true;
/* 65 */       AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(tmp, m, card.energyOnUse));
/* 66 */       if (tmp.cardID.equals("Rampage")) {
/* 67 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ModifyDamageAction(card, tmp.magicNumber));
/*    */       }
/* 69 */       this.amount -= 1;
/* 70 */       if (this.amount == 0) {
/* 71 */         AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this.owner, this.owner, "Double Tap"));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 78 */     if (isPlayer) {
/* 79 */       AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this.owner, this.owner, "Double Tap"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DoubleTapPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */