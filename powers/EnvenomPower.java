/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class EnvenomPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Envenom";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Envenom");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public EnvenomPower(AbstractCreature owner, int newAmount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Envenom";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = newAmount;
/* 23 */     updateDescription();
/* 24 */     this.img = ImageMaster.loadImage("images/powers/32/envenom.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 29 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onAttack(DamageInfo info, int damageAmount, AbstractCreature target)
/*    */   {
/* 34 */     if ((damageAmount > 0) && (target != this.owner) && (info.type == com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL)) {
/* 35 */       flash();
/* 36 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(target, this.owner, new PoisonPower(target, this.owner, this.amount), this.amount, true));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\EnvenomPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */