/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class AttackBurnPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Attack Burn";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Attack Burn");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/* 18 */   private boolean justApplied = true;
/*    */   
/*    */   public AttackBurnPower(AbstractCreature owner, int amount) {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Attack Burn";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = amount;
/* 25 */     updateDescription();
/* 26 */     loadRegion("attackBurn");
/* 27 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 28 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 33 */     if (this.justApplied) {
/* 34 */       this.justApplied = false;
/* 35 */       return;
/*    */     }
/*    */     
/* 38 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Attack Burn", 1));
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 43 */     if (this.amount == 1) {
/* 44 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 46 */       this.description = (DESCRIPTIONS[2] + this.amount + DESCRIPTIONS[3]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 52 */     if (card.type == AbstractCard.CardType.ATTACK) {
/* 53 */       action.exhaustCard = true;
/* 54 */       flash();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\AttackBurnPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */