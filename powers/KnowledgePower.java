/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class KnowledgePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Knowledge";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Knowledge");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public KnowledgePower(AbstractCreature owner, int newAmount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Knowledge";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = newAmount;
/* 23 */     updateDescription();
/* 24 */     this.img = ImageMaster.loadImage("images/powers/32/knowledge.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 29 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onAfterCardPlayed(AbstractCard card)
/*    */   {
/* 34 */     if (card.type == AbstractCard.CardType.POWER) {
/* 35 */       flash();
/* 36 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.owner, this.amount));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\KnowledgePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */