/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class RechargingCorePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "RechargingCore";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("RechargingCore");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private int turnTimer;
/*    */   
/*    */   public RechargingCorePower(AbstractCreature owner, int amount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "RechargingCore";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amount;
/* 21 */     this.turnTimer = 3;
/* 22 */     updateDescription();
/* 23 */     loadRegion("conserve");
/* 24 */     this.type = AbstractPower.PowerType.BUFF;
/* 25 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */ 
/*    */   public void updateDescription()
/*    */   {
/* 31 */     this.description = (DESCRIPTIONS[0] + this.turnTimer);
/* 32 */     if (this.turnTimer == 1) {
/* 33 */       this.description += DESCRIPTIONS[1];
/*    */     } else {
/* 35 */       this.description += DESCRIPTIONS[2];
/*    */     }
/* 37 */     for (int i = 0; i < this.amount; i++) {
/* 38 */       this.description += DESCRIPTIONS[3];
/*    */     }
/* 40 */     this.description += " .";
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 45 */     updateDescription();
/* 46 */     if (this.turnTimer == 1) {
/* 47 */       flash();
/* 48 */       this.turnTimer = 3;
/* 49 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.amount));
/*    */     } else {
/* 51 */       this.turnTimer -= 1;
/*    */     }
/* 53 */     updateDescription();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RechargingCorePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */