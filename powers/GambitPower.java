/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class GambitPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Gambit";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Gambit");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private int energyAmt;
/*    */   
/*    */   public GambitPower(AbstractCreature owner, int energyAmt)
/*    */   {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Gambit";
/* 23 */     this.owner = owner;
/* 24 */     this.energyAmt = energyAmt;
/* 25 */     this.amount = 3;
/* 26 */     updateDescription();
/* 27 */     this.img = ImageMaster.loadImage("images/powers/32/xanatos.png");
/* 28 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 33 */     if (this.amount == 1) {
/* 34 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1] + this.energyAmt + DESCRIPTIONS[3]);
/*    */     } else {
/* 36 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2] + this.energyAmt + DESCRIPTIONS[3]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 42 */     if (this.amount == 0) {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Gambit"));
/*    */     } else {
/* 45 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Gambit", 1));
/*    */     }
/*    */   }
/*    */   
/*    */   public void onRemove()
/*    */   {
/* 51 */     flash();
/* 52 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.energyAmt));
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 57 */     this.energyAmt += stackAmount;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\GambitPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */