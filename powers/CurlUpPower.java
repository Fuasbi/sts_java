/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ChangeStateAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class CurlUpPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Curl Up";
/* 18 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Curl Up");
/* 19 */   public static final String NAME = powerStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/* 22 */   private boolean triggered = false;
/*    */   
/*    */   public CurlUpPower(AbstractCreature owner, int amount) {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Curl Up";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = amount;
/* 29 */     this.description = (DESCRIPTIONS[0] + FontHelper.colorString(Integer.toString(amount), "b") + DESCRIPTIONS[1]);
/* 30 */     this.img = ImageMaster.loadImage("images/powers/32/closeUp.png");
/*    */   }
/*    */   
/*    */   public int onAttacked(DamageInfo info, int damageAmount) {
/* 34 */     if ((!this.triggered) && (damageAmount < this.owner.currentHealth) && (damageAmount > 0) && (info.owner != null) && (info.type == com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL))
/*    */     {
/* 36 */       flash();
/* 37 */       this.triggered = true;
/* 38 */       AbstractDungeon.actionManager.addToBottom(new ChangeStateAction((AbstractMonster)this.owner, "CLOSED"));
/* 39 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this.owner, this.owner, this.amount));
/* 40 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Curl Up"));
/*    */     }
/* 42 */     return damageAmount;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\CurlUpPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */