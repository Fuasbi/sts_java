/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class NightmarePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Night Terror";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Night Terror");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private AbstractCard card;
/*    */   
/*    */   public NightmarePower(AbstractCreature owner, int cardAmt, AbstractCard copyMe) {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Night Terror";
/* 23 */     this.owner = owner;
/* 24 */     this.amount = cardAmt;
/* 25 */     this.img = ImageMaster.loadImage("images/powers/32/nightTerror.png");
/* 26 */     this.card = copyMe.makeStatEquivalentCopy();
/* 27 */     this.card.resetAttributes();
/* 28 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 33 */     this.description = (DESCRIPTIONS[0] + this.amount + " " + FontHelper.colorString(this.card.name, "y") + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 38 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(this.card, this.amount));
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Night Terror"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\NightmarePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */