/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class PrimitivePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Primitive";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Primitive");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public PrimitivePower(AbstractCreature owner, int amount) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "Primitive";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = amount;
/* 23 */     updateDescription();
/* 24 */     loadRegion("primitive");
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 29 */     if ((card.rarity == AbstractCard.CardRarity.COMMON) || (card.rarity == AbstractCard.CardRarity.BASIC)) {
/* 30 */       flash();
/* 31 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.owner, this.amount));
/*    */     }
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 37 */     this.fontScale = 8.0F;
/* 38 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 43 */     if (this.amount <= 1) {
/* 44 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 46 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PrimitivePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */