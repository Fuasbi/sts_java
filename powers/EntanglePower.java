/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class EntanglePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Entangled";
/* 11 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Entangled");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public EntanglePower(AbstractCreature owner) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Entangled";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = 1;
/* 20 */     updateDescription();
/* 21 */     loadRegion("entangle");
/* 22 */     this.isTurnBased = true;
/* 23 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 28 */     CardCrawlGame.sound.play("POWER_ENTANGLED", 0.05F);
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 33 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 38 */     if (isPlayer) {
/* 39 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Entangled"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\EntanglePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */