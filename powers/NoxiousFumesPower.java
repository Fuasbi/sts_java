/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class NoxiousFumesPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Noxious Fumes";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Noxious Fumes");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public NoxiousFumesPower(AbstractCreature owner, int poisonAmount) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Noxious Fumes";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = poisonAmount;
/* 22 */     updateDescription();
/* 23 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/noxiousFumes.png");
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 28 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 29 */       flash();
/* 30 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 31 */         if ((!m.isDead) && (!m.isDying)) {
/* 32 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, this.owner, new PoisonPower(m, this.owner, this.amount), this.amount));
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 40 */     this.fontScale = 8.0F;
/* 41 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 46 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\NoxiousFumesPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */