/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DEPRECATED_CachePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Cache";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Cache");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DEPRECATED_CachePower(AbstractCreature owner, int amt) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Cache";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = amt;
/* 20 */     updateDescription();
/* 21 */     loadRegion("cache");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 26 */     if (this.amount <= 1) {
/* 27 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 29 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 35 */     if (isPlayer) {
/* 36 */       flash();
/* 37 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.CacheAction(this.amount));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DEPRECATED_CachePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */