/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class PainfulStabsPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Painful Stabs";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Painful Stabs");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public PainfulStabsPower(AbstractCreature owner) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Painful Stabs";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = -1;
/* 22 */     updateDescription();
/* 23 */     loadRegion("painfulStabs");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onAttack(DamageInfo info, int damageAmount, AbstractCreature target) {
/* 32 */     if (damageAmount > 0) {
/* 33 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(new com.megacrit.cardcrawl.cards.status.Wound(), 1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PainfulStabsPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */