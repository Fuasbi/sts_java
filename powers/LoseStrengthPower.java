/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class LoseStrengthPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Flex";
/* 12 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Flex");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public LoseStrengthPower(AbstractCreature owner, int newAmount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Flex";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = newAmount;
/* 21 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 22 */     updateDescription();
/* 23 */     loadRegion("flex");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 33 */     flash();
/* 34 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, -this.amount), -this.amount));
/*    */     
/* 36 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Flex"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\LoseStrengthPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */