/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class CombustPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Combust";
/* 15 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Combust");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private int hpLoss;
/*    */   
/*    */   public CombustPower(AbstractCreature owner, int hpLoss, int damageAmount)
/*    */   {
/* 22 */     this.name = NAME;
/* 23 */     this.ID = "Combust";
/* 24 */     this.owner = owner;
/* 25 */     this.amount = damageAmount;
/* 26 */     this.hpLoss = hpLoss;
/* 27 */     updateDescription();
/* 28 */     loadRegion("combust");
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 33 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 34 */       flash();
/* 35 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.LoseHPAction(this.owner, this.owner, this.hpLoss, AbstractGameAction.AttackEffect.FIRE));
/*    */       
/* 37 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(null, 
/*    */       
/*    */ 
/* 40 */         DamageInfo.createDamageMatrix(this.amount, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, AbstractGameAction.AttackEffect.FIRE));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 48 */     this.fontScale = 8.0F;
/* 49 */     this.amount += stackAmount;
/* 50 */     this.hpLoss += 1;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 55 */     this.description = (DESCRIPTIONS[0] + this.hpLoss + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\CombustPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */