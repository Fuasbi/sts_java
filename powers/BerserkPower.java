/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class BerserkPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Berserk";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Berserk");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public BerserkPower(String name, AbstractCreature owner, int amount) {
/* 17 */     this.name = name;
/* 18 */     this.ID = "Berserk";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amount;
/* 21 */     updateDescription();
/* 22 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/thrillseeker.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 27 */     StringBuilder sb = new StringBuilder();
/* 28 */     sb.append(DESCRIPTIONS[0]);
/* 29 */     for (int i = 0; i < this.amount; i++) {
/* 30 */       sb.append("[R] ");
/*    */     }
/* 32 */     sb.append(DESCRIPTIONS[1]);
/* 33 */     this.description = sb.toString();
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 38 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new GainEnergyAction(this.amount));
/* 39 */     flash();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\BerserkPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */