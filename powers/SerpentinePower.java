/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class SerpentinePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Serpentine";
/* 10 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Serpentine");
/* 11 */   public static final String NAME = powerStrings.NAME;
/* 12 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public SerpentinePower(AbstractCreature owner) {
/* 15 */     this.name = NAME;
/* 16 */     this.ID = "Serpentine";
/* 17 */     this.owner = owner;
/* 18 */     this.amount = -1;
/* 19 */     updateDescription();
/* 20 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/split.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 25 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\SerpentinePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */