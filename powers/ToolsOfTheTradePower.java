/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ToolsOfTheTradePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Tools Of The Trade";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Tools Of The Trade");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public ToolsOfTheTradePower(AbstractCreature owner, int drawAmount) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Tools Of The Trade";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = drawAmount;
/* 22 */     updateDescription();
/* 23 */     this.img = ImageMaster.loadImage("images/powers/32/drawCard.png");
/* 24 */     this.priority = 25;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 29 */     if (this.amount == 1) {
/* 30 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     } else {
/* 32 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[3] + this.amount + DESCRIPTIONS[4]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 38 */     flash();
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.owner, this.amount));
/* 40 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DiscardAction(this.owner, this.owner, this.amount, false));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ToolsOfTheTradePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */