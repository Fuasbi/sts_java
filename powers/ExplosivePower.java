/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.actions.common.SuicideAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.vfx.combat.ExplosionSmallEffect;
/*    */ 
/*    */ public class ExplosivePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Explosive";
/* 19 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Explosive");
/* 20 */   public static final String NAME = powerStrings.NAME;
/* 21 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private static final int DAMAGE_AMOUNT = 30;
/*    */   
/*    */   public ExplosivePower(AbstractCreature owner, int damage) {
/* 25 */     this.name = NAME;
/* 26 */     this.ID = "Explosive";
/* 27 */     this.owner = owner;
/* 28 */     this.amount = damage;
/* 29 */     updateDescription();
/* 30 */     loadRegion("explosive");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 35 */     if (this.amount == 1) {
/* 36 */       this.description = (DESCRIPTIONS[3] + 30 + DESCRIPTIONS[2]);
/*    */     } else {
/* 38 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1] + 30 + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void duringTurn()
/*    */   {
/* 44 */     if ((this.amount == 1) && (!this.owner.isDying)) {
/* 45 */       AbstractDungeon.actionManager.addToBottom(new VFXAction(new ExplosionSmallEffect(this.owner.hb.cX, this.owner.hb.cY), 0.1F));
/*    */       
/* 47 */       AbstractDungeon.actionManager.addToBottom(new SuicideAction((AbstractMonster)this.owner));
/* 48 */       DamageInfo damageInfo = new DamageInfo(this.owner, 30, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS);
/* 49 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, damageInfo, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE, true));
/*    */     }
/*    */     else {
/* 52 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Explosive", 1));
/* 53 */       updateDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ExplosivePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */