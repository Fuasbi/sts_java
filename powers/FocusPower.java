/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class FocusPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Focus";
/* 13 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Focus");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public FocusPower(AbstractCreature owner, int amount) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Focus";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = amount;
/* 22 */     updateDescription();
/* 23 */     loadRegion("focus");
/* 24 */     this.canGoNegative = true;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 29 */     CardCrawlGame.sound.play("POWER_FOCUS", 0.05F);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 34 */     this.fontScale = 8.0F;
/* 35 */     this.amount += stackAmount;
/*    */     
/* 37 */     if (this.amount == 0) {
/* 38 */       AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, "Focus"));
/*    */     }
/*    */     
/* 41 */     if (this.amount >= 25) {
/* 42 */       com.megacrit.cardcrawl.unlock.UnlockTracker.unlockAchievement("FOCUSED");
/*    */     }
/*    */     
/* 45 */     if (this.amount >= 999) {
/* 46 */       this.amount = 999;
/*    */     }
/*    */     
/* 49 */     if (this.amount <= 64537) {
/* 50 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void reducePower(int reduceAmount)
/*    */   {
/* 56 */     this.fontScale = 8.0F;
/* 57 */     this.amount -= reduceAmount;
/*    */     
/* 59 */     if (this.amount == 0) {
/* 60 */       AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, NAME));
/*    */     }
/*    */     
/* 63 */     if (this.amount >= 999) {
/* 64 */       this.amount = 999;
/*    */     }
/*    */     
/* 67 */     if (this.amount <= 64537) {
/* 68 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 74 */     if (this.amount > 0) {
/* 75 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/* 76 */       this.type = AbstractPower.PowerType.BUFF;
/*    */     } else {
/* 78 */       int tmp = -this.amount;
/* 79 */       this.description = (DESCRIPTIONS[1] + tmp + DESCRIPTIONS[2]);
/* 80 */       this.type = AbstractPower.PowerType.DEBUFF;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\FocusPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */