/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.colorless.Shiv;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class InfiniteBladesPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Infinite Blades";
/* 13 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Infinite Blades");
/* 14 */   public static final String NAME = powerStrings.NAME;
/* 15 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public InfiniteBladesPower(AbstractCreature owner, int bladeAmt) {
/* 18 */     this.name = NAME;
/* 19 */     this.ID = "Infinite Blades";
/* 20 */     this.owner = owner;
/* 21 */     this.amount = bladeAmt;
/* 22 */     updateDescription();
/* 23 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/infiniteBlades.png");
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 28 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 29 */       flash();
/* 30 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(new Shiv(), this.amount, false));
/*    */     }
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 36 */     this.fontScale = 8.0F;
/* 37 */     this.amount += stackAmount;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 42 */     if (this.amount > 1) {
/* 43 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */     } else {
/* 45 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\InfiniteBladesPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */