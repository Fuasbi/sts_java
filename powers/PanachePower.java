/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class PanachePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Panache";
/* 17 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Panache");
/* 18 */   public static final String NAME = powerStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   public static final int CARD_AMT = 5;
/*    */   private int damage;
/*    */   
/*    */   public PanachePower(AbstractCreature owner, int damage) {
/* 24 */     this.name = NAME;
/* 25 */     this.ID = "Panache";
/* 26 */     this.owner = owner;
/* 27 */     this.amount = 5;
/* 28 */     this.damage = damage;
/* 29 */     updateDescription();
/* 30 */     this.img = ImageMaster.loadImage("images/powers/32/panache.png");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 35 */     if (this.amount == 1) {
/* 36 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1] + this.damage + DESCRIPTIONS[2]);
/*    */     } else {
/* 38 */       this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[3] + this.damage + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 44 */     this.fontScale = 8.0F;
/* 45 */     this.damage += stackAmount;
/* 46 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 51 */     this.amount -= 1;
/* 52 */     if (this.amount == 0) {
/* 53 */       flash();
/* 54 */       this.amount = 5;
/* 55 */       AbstractDungeon.actionManager.addToBottom(new DamageAllEnemiesAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 58 */         DamageInfo.createDamageMatrix(this.damage, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */     }
/*    */     
/*    */ 
/* 62 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void atStartOfTurn()
/*    */   {
/* 67 */     this.amount = 5;
/* 68 */     updateDescription();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PanachePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */