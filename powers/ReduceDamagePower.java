/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ReduceDamagePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Reduce Damage";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Reduce Damage");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public ReduceDamagePower(AbstractCreature owner, int armorAmt) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Reduce Damage";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = armorAmt;
/* 20 */     updateDescription();
/* 21 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/exoskeleton.png");
/* 22 */     this.priority = 99;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 27 */     this.description = (DESCRIPTIONS[0] + this.amount + ".");
/*    */   }
/*    */   
/*    */   public float atDamageReceive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 32 */     if (type != DamageInfo.DamageType.HP_LOSS) {
/* 33 */       flash();
/* 34 */       damage -= this.amount;
/*    */     }
/* 36 */     if (damage < 0.0F) {
/* 37 */       damage = 0.0F;
/*    */     }
/* 39 */     return damage;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ReduceDamagePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */