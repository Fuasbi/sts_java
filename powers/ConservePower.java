/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class ConservePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Conserve";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Conserve");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public ConservePower(AbstractCreature owner, int amount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Conserve";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amount;
/* 21 */     this.description = DESCRIPTIONS[0];
/* 22 */     loadRegion("conserve");
/* 23 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     if (this.amount == 1) {
/* 29 */       this.description = DESCRIPTIONS[0];
/*    */     } else {
/* 31 */       this.description = (DESCRIPTIONS[1] + this.amount + DESCRIPTIONS[2]);
/*    */     }
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 37 */     if (this.amount == 0) {
/* 38 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Conserve"));
/*    */     } else {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "Conserve", 1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\ConservePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */