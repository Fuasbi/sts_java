/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class RegenerateMonsterPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Regenerate";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Regenerate");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public RegenerateMonsterPower(AbstractMonster owner, int regenAmt) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Regenerate";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = regenAmt;
/* 20 */     updateDescription();
/* 21 */     loadRegion("regen");
/* 22 */     this.type = AbstractPower.PowerType.BUFF;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 27 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 32 */     flash();
/* 33 */     if ((!this.owner.halfDead) && (!this.owner.isDying) && (!this.owner.isDead)) {
/* 34 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.HealAction(this.owner, this.owner, this.amount));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RegenerateMonsterPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */