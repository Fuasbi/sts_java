/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class NoDrawPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "No Draw";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("No Draw");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public NoDrawPower(AbstractCreature owner) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "No Draw";
/* 18 */     this.owner = owner;
/* 19 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 20 */     this.amount = -1;
/* 21 */     this.description = DESCRIPTIONS[0];
/* 22 */     loadRegion("noDraw");
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 27 */     if (isPlayer) {
/* 28 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "No Draw"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\NoDrawPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */