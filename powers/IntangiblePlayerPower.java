/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class IntangiblePlayerPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "IntangiblePlayer";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("IntangiblePlayer");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public IntangiblePlayerPower(AbstractCreature owner, int turns) {
/* 19 */     this.name = NAME;
/* 20 */     this.ID = "IntangiblePlayer";
/* 21 */     this.owner = owner;
/* 22 */     this.amount = turns;
/* 23 */     updateDescription();
/* 24 */     this.img = ImageMaster.loadImage("images/powers/32/ghost.png");
/* 25 */     this.priority = 99;
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 30 */     CardCrawlGame.sound.play("POWER_INTANGIBLE", 0.05F);
/*    */   }
/*    */   
/*    */   public float atDamageReceive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 35 */     if (damage > 1.0F) {
/* 36 */       damage = 1.0F;
/*    */     }
/* 38 */     return damage;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 43 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void atEndOfRound()
/*    */   {
/* 48 */     flash();
/*    */     
/* 50 */     if (this.amount == 0) {
/* 51 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "IntangiblePlayer"));
/*    */     } else {
/* 53 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ReducePowerAction(this.owner, this.owner, "IntangiblePlayer", 1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\IntangiblePlayerPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */