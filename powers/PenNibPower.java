/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class PenNibPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Pen Nib";
/* 15 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Pen Nib");
/* 16 */   public static final String NAME = powerStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public PenNibPower(AbstractCreature owner, int amount) {
/* 20 */     this.name = NAME;
/* 21 */     this.ID = "Pen Nib";
/* 22 */     this.owner = owner;
/* 23 */     this.amount = amount;
/* 24 */     updateDescription();
/* 25 */     this.img = ImageMaster.loadImage("images/powers/32/penNibPower.png");
/* 26 */     this.type = AbstractPower.PowerType.BUFF;
/* 27 */     this.isTurnBased = true;
/* 28 */     this.priority = 6;
/*    */   }
/*    */   
/*    */   public void onUseCard(AbstractCard card, UseCardAction action)
/*    */   {
/* 33 */     if (card.type == AbstractCard.CardType.ATTACK) {
/* 34 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(this.owner, this.owner, "Pen Nib"));
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 40 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public float atDamageGive(float damage, DamageInfo.DamageType type)
/*    */   {
/* 45 */     if (type == DamageInfo.DamageType.NORMAL) {
/* 46 */       return damage * 2.0F;
/*    */     }
/* 48 */     return damage;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\PenNibPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */