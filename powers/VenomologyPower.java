/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class VenomologyPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Venomology";
/* 10 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Venomology");
/* 11 */   public static final String NAME = powerStrings.NAME;
/* 12 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public VenomologyPower(AbstractCreature owner, int newAmount) {
/* 15 */     this.name = NAME;
/* 16 */     this.ID = "Venomology";
/* 17 */     this.owner = owner;
/* 18 */     this.amount = newAmount;
/* 19 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/* 20 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/venomology.png");
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\VenomologyPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */