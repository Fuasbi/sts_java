/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class GainStrengthPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Shackled";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Shackled");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public GainStrengthPower(AbstractCreature owner, int newAmount) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Shackled";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = newAmount;
/* 21 */     this.type = AbstractPower.PowerType.DEBUFF;
/* 22 */     updateDescription();
/* 23 */     loadRegion("shackle");
/* 24 */     if (this.amount >= 999) {
/* 25 */       this.amount = 999;
/*    */     }
/*    */     
/* 28 */     if (this.amount <= 64537) {
/* 29 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void playApplyPowerSfx()
/*    */   {
/* 35 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("POWER_SHACKLE", 0.05F);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 40 */     this.fontScale = 8.0F;
/* 41 */     this.amount += stackAmount;
/* 42 */     if (this.amount == 0) {
/* 43 */       AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, "Shackled"));
/*    */     }
/*    */     
/* 46 */     if (this.amount >= 999) {
/* 47 */       this.amount = 999;
/*    */     }
/*    */     
/* 50 */     if (this.amount <= 64537) {
/* 51 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void reducePower(int reduceAmount)
/*    */   {
/* 57 */     this.fontScale = 8.0F;
/* 58 */     this.amount -= reduceAmount;
/*    */     
/* 60 */     if (this.amount == 0) {
/* 61 */       AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.owner, this.owner, NAME));
/*    */     }
/*    */     
/* 64 */     if (this.amount >= 999) {
/* 65 */       this.amount = 999;
/*    */     }
/*    */     
/* 68 */     if (this.amount <= 64537) {
/* 69 */       this.amount = 64537;
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 75 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atEndOfTurn(boolean isPlayer)
/*    */   {
/* 80 */     flash();
/* 81 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, this.amount), this.amount));
/*    */     
/* 83 */     AbstractDungeon.actionManager.addToBottom(new RemoveSpecificPowerAction(this.owner, this.owner, "Shackled"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\GainStrengthPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */