/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.LoseEnergyAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DEPRECATED_DarknessPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Darkness";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Darkness");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DEPRECATED_DarknessPower(int energyAmt) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Darkness";
/* 19 */     this.owner = AbstractDungeon.player;
/* 20 */     this.amount = energyAmt;
/* 21 */     updateDescription();
/*    */     
/* 23 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/darkness.png");
/* 24 */     this.type = AbstractPower.PowerType.DEBUFF;
/*    */     
/* 26 */     this.isTurnBased = true;
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 31 */     this.description = DESCRIPTIONS[0];
/*    */   }
/*    */   
/*    */   public void onEnergyRecharge()
/*    */   {
/* 36 */     AbstractDungeon.actionManager.addToBottom(new LoseEnergyAction(1));
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 41 */     flash();
/* 42 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction(AbstractDungeon.player, AbstractDungeon.player, "Darkness"));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DEPRECATED_DarknessPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */