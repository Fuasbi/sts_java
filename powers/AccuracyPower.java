/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.colorless.Shiv;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class AccuracyPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Accuracy";
/* 12 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Accuracy");
/* 13 */   public static final String NAME = powerStrings.NAME;
/* 14 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public AccuracyPower(com.megacrit.cardcrawl.core.AbstractCreature owner, int amt) {
/* 17 */     this.name = NAME;
/* 18 */     this.ID = "Accuracy";
/* 19 */     this.owner = owner;
/* 20 */     this.amount = amt;
/* 21 */     updateDescription();
/* 22 */     loadRegion("accuracy");
/* 23 */     updateShivsInHand();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void stackPower(int stackAmount)
/*    */   {
/* 33 */     this.fontScale = 8.0F;
/* 34 */     this.amount += stackAmount;
/* 35 */     updateShivsInHand();
/*    */   }
/*    */   
/*    */   private void updateShivsInHand() {
/* 39 */     for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.group) {
/* 40 */       if ((c instanceof Shiv)) {
/* 41 */         if (!c.upgraded) {
/* 42 */           c.baseDamage = (4 + this.amount);
/*    */         } else {
/* 44 */           c.baseDamage = (6 + this.amount);
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void onDrawOrDiscard()
/*    */   {
/* 52 */     for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.group) {
/* 53 */       if ((c instanceof Shiv)) {
/* 54 */         if (!c.upgraded) {
/* 55 */           c.baseDamage = (4 + this.amount);
/*    */         } else {
/* 57 */           c.baseDamage = (6 + this.amount);
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\AccuracyPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */