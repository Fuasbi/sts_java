/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class DemonFormPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Demon Form";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Demon Form");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public DemonFormPower(AbstractCreature owner, int strengthAmount) {
/* 16 */     this.name = NAME;
/* 17 */     this.ID = "Demon Form";
/* 18 */     this.owner = owner;
/* 19 */     this.amount = strengthAmount;
/* 20 */     updateDescription();
/* 21 */     loadRegion("demonForm");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 26 */     this.description = (DESCRIPTIONS[0] + this.amount + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void atStartOfTurnPostDraw()
/*    */   {
/* 31 */     flash();
/* 32 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new StrengthPower(this.owner, this.amount), this.amount));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\DemonFormPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */