/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInDiscardAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class StasisPower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Stasis";
/* 14 */   private static final PowerStrings powerStrings = CardCrawlGame.languagePack.getPowerStrings("Stasis");
/* 15 */   public static final String NAME = powerStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   private AbstractCard card;
/*    */   
/*    */   public StasisPower(AbstractCreature owner, AbstractCard card)
/*    */   {
/* 21 */     this.name = NAME;
/* 22 */     this.ID = "Stasis";
/* 23 */     this.owner = owner;
/* 24 */     this.card = card;
/* 25 */     this.amount = -1;
/* 26 */     updateDescription();
/* 27 */     loadRegion("stasis");
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 32 */     this.description = (DESCRIPTIONS[0] + this.card.name + DESCRIPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onDeath()
/*    */   {
/* 37 */     if (AbstractDungeon.player.hand.size() != 10) {
/* 38 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(this.card, 1, false));
/*    */     } else {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new MakeTempCardInDiscardAction(this.card, 1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\StasisPower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */