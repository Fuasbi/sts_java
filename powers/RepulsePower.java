/*    */ package com.megacrit.cardcrawl.powers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.PowerStrings;
/*    */ 
/*    */ public class RepulsePower extends AbstractPower
/*    */ {
/*    */   public static final String POWER_ID = "Repulse";
/* 11 */   private static final PowerStrings powerStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getPowerStrings("Repulse");
/* 12 */   public static final String NAME = powerStrings.NAME;
/* 13 */   public static final String[] DESCRIPTIONS = powerStrings.DESCRIPTIONS;
/*    */   
/*    */   public RepulsePower(AbstractCreature owner) {
/* 16 */     this.name = NAME;
/* 17 */     this.owner = owner;
/* 18 */     this.amount = -1;
/* 19 */     updateDescription();
/* 20 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/powers/32/generator.png");
/* 21 */     this.ID = "Repulse";
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 26 */     this.description = (DESCRIPTIONS[0] + FontHelper.colorString(this.owner.name, "y") + DESCRIPTIONS[1]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\powers\RepulsePower.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */