/*     */ package com.megacrit.cardcrawl.saveAndContinue;
/*     */ 
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.BottledFlame;
/*     */ import com.megacrit.cardcrawl.relics.BottledLightning;
/*     */ import com.megacrit.cardcrawl.relics.BottledTornado;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem.RewardType;
/*     */ import com.megacrit.cardcrawl.rewards.RewardSave;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Calendar;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SaveFile
/*     */ {
/*  29 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SaveFile.class.getName());
/*     */   
/*     */   public String name;
/*     */   
/*     */   public String loadout;
/*     */   
/*     */   public int current_health;
/*     */   
/*     */   public int max_health;
/*     */   
/*     */   public int max_orbs;
/*     */   
/*     */   public int gold;
/*     */   
/*     */   public int hand_size;
/*     */   
/*     */   public int potion_slots;
/*     */   
/*     */   public int red;
/*     */   
/*     */   public int green;
/*     */   
/*     */   public int blue;
/*     */   
/*     */   public ArrayList<com.megacrit.cardcrawl.cards.CardSave> cards;
/*     */   
/*     */   public HashMap<String, Integer> obtained_cards;
/*     */   
/*     */   public ArrayList<String> relics;
/*     */   
/*     */   public ArrayList<Integer> relic_counters;
/*     */   
/*     */   public ArrayList<String> blights;
/*     */   
/*     */   public ArrayList<Integer> blight_counters;
/*     */   
/*     */   public ArrayList<String> potions;
/*     */   
/*     */   public boolean is_ascension_mode;
/*     */   
/*     */   public int ascension_level;
/*     */   public boolean chose_neow_reward;
/*     */   public String level_name;
/*     */   public long play_time;
/*     */   public long save_date;
/*     */   public long daily_date;
/*     */   public int floor_num;
/*     */   public int act_num;
/*     */   public long seed;
/*     */   public long special_seed;
/*     */   public boolean is_trial;
/*     */   public boolean is_daily;
/*     */   public ArrayList<String> custom_mods;
/*     */   public ArrayList<String> daily_mods;
/*     */   public int monster_seed_count;
/*     */   public int event_seed_count;
/*     */   public int merchant_seed_count;
/*     */   public int card_seed_count;
/*     */   public int treasure_seed_count;
/*     */   public int relic_seed_count;
/*     */   public int potion_seed_count;
/*     */   public int monster_hp_seed_count;
/*     */   public int ai_seed_count;
/*     */   public int shuffle_seed_count;
/*     */   public int card_random_seed_count;
/*     */   public int card_random_seed_randomizer;
/*     */   public int potion_chance;
/*     */   public int purgeCost;
/*     */   public ArrayList<String> monster_list;
/*     */   public ArrayList<String> elite_monster_list;
/*     */   public ArrayList<String> boss_list;
/*     */   public ArrayList<String> event_list;
/*     */   public ArrayList<String> one_time_event_list;
/*     */   public ArrayList<Float> event_chances;
/*     */   public ArrayList<Integer> path_x;
/*     */   public ArrayList<Integer> path_y;
/*     */   public int room_x;
/*     */   public int room_y;
/*     */   public int spirit_count;
/*     */   public String boss;
/*     */   public String current_room;
/*     */   public ArrayList<String> common_relics;
/*     */   public ArrayList<String> uncommon_relics;
/*     */   public ArrayList<String> rare_relics;
/*     */   public ArrayList<String> shop_relics;
/*     */   public ArrayList<String> boss_relics;
/*     */   public String bottled_flame;
/*     */   public String bottled_lightning;
/*     */   public String bottled_tornado;
/*     */   public boolean is_endless_mode;
/*     */   public ArrayList<Integer> endless_increments;
/*     */   public boolean post_combat;
/*     */   public boolean mugged;
/*     */   public boolean smoked;
/*     */   public ArrayList<RewardSave> combat_rewards;
/*     */   public int monsters_killed;
/*     */   public int elites1_killed;
/*     */   public int elites2_killed;
/*     */   public int elites3_killed;
/*     */   public int champions;
/*     */   public int perfect;
/*     */   public boolean overkill;
/*     */   public boolean combo;
/*     */   public boolean cheater;
/*     */   public int gold_gained;
/*     */   public int mystery_machine;
/*     */   public int metric_campfire_rested;
/*     */   public int metric_campfire_upgraded;
/*     */   public int metric_campfire_rituals;
/*     */   public int metric_campfire_meditates;
/*     */   public int metric_purchased_purges;
/*     */   public ArrayList<Integer> metric_potions_floor_spawned;
/*     */   public ArrayList<Integer> metric_potions_floor_usage;
/*     */   public ArrayList<Integer> metric_current_hp_per_floor;
/*     */   public ArrayList<Integer> metric_max_hp_per_floor;
/*     */   public ArrayList<Integer> metric_gold_per_floor;
/*     */   public ArrayList<String> metric_path_per_floor;
/*     */   public ArrayList<String> metric_path_taken;
/*     */   public ArrayList<String> metric_items_purchased;
/*     */   public ArrayList<Integer> metric_item_purchase_floors;
/*     */   public ArrayList<String> metric_items_purged;
/*     */   public ArrayList<Integer> metric_items_purged_floors;
/*     */   public ArrayList<HashMap> metric_card_choices;
/*     */   public ArrayList<HashMap> metric_event_choices;
/*     */   public ArrayList<HashMap> metric_boss_relics;
/*     */   public ArrayList<HashMap> metric_damage_taken;
/*     */   public ArrayList<HashMap> metric_potions_obtained;
/*     */   public ArrayList<HashMap> metric_relics_obtained;
/*     */   public ArrayList<HashMap> metric_campfire_choices;
/*     */   public String metric_build_version;
/*     */   public String metric_seed_played;
/*     */   public int metric_floor_reached;
/*     */   public long metric_playtime;
/*     */   public String neow_bonus;
/*     */   public String neow_cost;
/*     */   public SaveFile() {}
/*     */   
/*     */   public static enum SaveType
/*     */   {
/* 168 */     ENTER_ROOM,  POST_NEOW,  POST_COMBAT,  AFTER_BOSS_RELIC,  ENDLESS_NEOW;
/*     */     
/*     */ 
/*     */     private SaveType() {}
/*     */   }
/*     */   
/*     */ 
/*     */   public SaveFile(SaveType type)
/*     */   {
/* 177 */     AbstractPlayer p = AbstractDungeon.player;
/* 178 */     this.name = p.name;
/* 179 */     this.current_health = p.currentHealth;
/* 180 */     this.max_health = p.maxHealth;
/* 181 */     this.max_orbs = p.masterMaxOrbs;
/* 182 */     this.gold = p.gold;
/* 183 */     this.hand_size = p.masterHandSize;
/* 184 */     this.red = p.energy.energyMaster;
/* 185 */     this.green = 0;
/* 186 */     this.blue = 0;
/*     */     
/*     */ 
/* 189 */     this.monsters_killed = CardCrawlGame.monstersSlain;
/* 190 */     this.elites1_killed = CardCrawlGame.elites1Slain;
/* 191 */     this.elites2_killed = CardCrawlGame.elites2Slain;
/* 192 */     this.elites3_killed = CardCrawlGame.elites3Slain;
/* 193 */     this.champions = CardCrawlGame.champion;
/* 194 */     this.perfect = CardCrawlGame.perfect;
/* 195 */     this.overkill = CardCrawlGame.overkill;
/* 196 */     this.combo = CardCrawlGame.combo;
/* 197 */     this.cheater = CardCrawlGame.cheater;
/* 198 */     this.gold_gained = CardCrawlGame.goldGained;
/* 199 */     this.mystery_machine = CardCrawlGame.mysteryMachine;
/* 200 */     this.play_time = (CardCrawlGame.playtime);
/*     */     
/*     */ 
/* 203 */     this.cards = p.masterDeck.getCardDeck();
/* 204 */     this.obtained_cards = CardHelper.obtainedCards;
/*     */     
/*     */ 
/* 207 */     this.relics = new ArrayList();
/* 208 */     this.relic_counters = new ArrayList();
/* 209 */     for (AbstractRelic r : p.relics) {
/* 210 */       this.relics.add(r.relicId);
/* 211 */       this.relic_counters.add(Integer.valueOf(r.counter));
/*     */     }
/*     */     
/*     */ 
/* 215 */     this.is_endless_mode = Settings.isEndless;
/*     */     
/*     */ 
/* 218 */     this.blights = new ArrayList();
/* 219 */     this.blight_counters = new ArrayList();
/* 220 */     for (AbstractBlight b : p.blights) {
/* 221 */       this.blights.add(b.blightID);
/* 222 */       this.blight_counters.add(Integer.valueOf(b.counter));
/*     */     }
/*     */     
/* 225 */     this.endless_increments = new ArrayList();
/* 226 */     for (AbstractBlight b : p.blights) {
/* 227 */       this.endless_increments.add(Integer.valueOf(b.increment));
/*     */     }
/*     */     
/*     */ 
/* 231 */     this.potion_slots = AbstractDungeon.player.potionSlots;
/* 232 */     this.potions = new ArrayList();
/* 233 */     for (AbstractPotion pot : AbstractDungeon.player.potions) {
/* 234 */       this.potions.add(pot.ID);
/*     */     }
/*     */     
/*     */ 
/* 238 */     this.is_ascension_mode = AbstractDungeon.isAscensionMode;
/* 239 */     this.ascension_level = AbstractDungeon.ascensionLevel;
/* 240 */     this.chose_neow_reward = false;
/* 241 */     this.level_name = AbstractDungeon.id;
/* 242 */     this.floor_num = AbstractDungeon.floorNum;
/* 243 */     this.act_num = AbstractDungeon.actNum;
/* 244 */     this.monster_list = AbstractDungeon.monsterList;
/* 245 */     this.elite_monster_list = AbstractDungeon.eliteMonsterList;
/* 246 */     this.boss_list = AbstractDungeon.bossList;
/* 247 */     this.event_list = AbstractDungeon.eventList;
/* 248 */     this.one_time_event_list = AbstractDungeon.specialOneTimeEventList;
/* 249 */     this.potion_chance = AbstractRoom.blizzardPotionMod;
/* 250 */     this.event_chances = com.megacrit.cardcrawl.helpers.EventHelper.getChances();
/* 251 */     this.save_date = Calendar.getInstance().getTimeInMillis();
/* 252 */     this.seed = Settings.seed.longValue();
/* 253 */     if (Settings.specialSeed != null) {
/* 254 */       this.special_seed = Settings.specialSeed.longValue();
/*     */     }
/* 256 */     this.is_daily = Settings.isDailyRun;
/* 257 */     this.daily_date = Settings.dailyDate;
/* 258 */     this.is_trial = Settings.isTrial;
/* 259 */     this.daily_mods = Settings.dailyMods.getEnabledModIDs();
/*     */     
/* 261 */     if (AbstractPlayer.customMods == null) {
/* 262 */       if (CardCrawlGame.trial != null) {
/* 263 */         AbstractPlayer.customMods = CardCrawlGame.trial.dailyModIDs();
/*     */       } else {
/* 265 */         AbstractPlayer.customMods = new ArrayList();
/*     */       }
/*     */     }
/*     */     
/* 269 */     this.custom_mods = AbstractPlayer.customMods;
/*     */     
/* 271 */     this.boss = AbstractDungeon.bossKey;
/* 272 */     this.purgeCost = com.megacrit.cardcrawl.shop.ShopScreen.purgeCost;
/* 273 */     this.monster_seed_count = AbstractDungeon.monsterRng.counter;
/* 274 */     this.event_seed_count = AbstractDungeon.eventRng.counter;
/* 275 */     this.merchant_seed_count = AbstractDungeon.merchantRng.counter;
/* 276 */     this.card_seed_count = AbstractDungeon.cardRng.counter;
/* 277 */     this.card_random_seed_randomizer = AbstractDungeon.cardBlizzRandomizer;
/* 278 */     this.treasure_seed_count = AbstractDungeon.treasureRng.counter;
/* 279 */     this.relic_seed_count = AbstractDungeon.relicRng.counter;
/* 280 */     this.potion_seed_count = AbstractDungeon.potionRng.counter;
/* 281 */     this.path_x = AbstractDungeon.pathX;
/* 282 */     this.path_y = AbstractDungeon.pathY;
/*     */     
/*     */ 
/* 285 */     if ((AbstractDungeon.nextRoom == null) || (type == SaveType.ENDLESS_NEOW)) {
/* 286 */       this.room_x = AbstractDungeon.getCurrMapNode().x;
/* 287 */       this.room_y = AbstractDungeon.getCurrMapNode().y;
/* 288 */       this.current_room = AbstractDungeon.getCurrRoom().getClass().getName();
/*     */     } else {
/* 290 */       this.room_x = AbstractDungeon.nextRoom.x;
/* 291 */       this.room_y = AbstractDungeon.nextRoom.y;
/* 292 */       this.current_room = AbstractDungeon.nextRoom.room.getClass().getName();
/*     */     }
/*     */     
/* 295 */     this.spirit_count = AbstractDungeon.bossCount;
/* 296 */     logger.info("Next Room: " + this.current_room);
/* 297 */     this.common_relics = AbstractDungeon.commonRelicPool;
/* 298 */     this.uncommon_relics = AbstractDungeon.uncommonRelicPool;
/* 299 */     this.rare_relics = AbstractDungeon.rareRelicPool;
/* 300 */     this.shop_relics = AbstractDungeon.shopRelicPool;
/* 301 */     this.boss_relics = AbstractDungeon.bossRelicPool;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 331 */     this.post_combat = false;
/* 332 */     this.mugged = false;
/* 333 */     this.smoked = false;
/* 334 */     switch (type) {
/*     */     case AFTER_BOSS_RELIC: 
/*     */       break;
/*     */     case ENTER_ROOM: 
/*     */       break;
/*     */     case POST_COMBAT: 
/* 340 */       this.post_combat = true;
/* 341 */       this.mugged = AbstractDungeon.getCurrRoom().mugged;
/* 342 */       this.smoked = AbstractDungeon.getCurrRoom().smoked;
/* 343 */       this.combat_rewards = new ArrayList();
/* 344 */       for (RewardItem i : AbstractDungeon.getCurrRoom().rewards) {
/* 345 */         switch (i.type) {
/*     */         case CARD: 
/* 347 */           this.combat_rewards.add(new RewardSave(i.type.toString(), null));
/* 348 */           break;
/*     */         case GOLD: 
/* 350 */           this.combat_rewards.add(new RewardSave(i.type.toString(), null, i.goldAmt, i.bonusGold));
/* 351 */           break;
/*     */         case POTION: 
/* 353 */           this.combat_rewards.add(new RewardSave(i.type.toString(), i.potion.ID));
/* 354 */           break;
/*     */         case RELIC: 
/* 356 */           this.combat_rewards.add(new RewardSave(i.type.toString(), i.relic.relicId));
/* 357 */           break;
/*     */         case STOLEN_GOLD: 
/* 359 */           this.combat_rewards.add(new RewardSave(i.type.toString(), null, i.goldAmt, 0));
/*     */         }
/*     */         
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 366 */       break;
/*     */     case POST_NEOW: 
/* 368 */       this.chose_neow_reward = true;
/* 369 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/* 375 */     if (AbstractDungeon.player.hasRelic("Bottled Flame")) {
/* 376 */       if (((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).card != null) {
/* 377 */         this.bottled_flame = ((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).card.cardID;
/*     */       } else {
/* 379 */         this.bottled_flame = null;
/*     */       }
/*     */     } else {
/* 382 */       this.bottled_flame = null;
/*     */     }
/*     */     
/*     */ 
/* 386 */     if (AbstractDungeon.player.hasRelic("Bottled Lightning")) {
/* 387 */       if (((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).card != null) {
/* 388 */         this.bottled_lightning = ((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).card.cardID;
/*     */       }
/*     */       else {
/* 391 */         this.bottled_lightning = null;
/*     */       }
/*     */     } else {
/* 394 */       this.bottled_lightning = null;
/*     */     }
/*     */     
/*     */ 
/* 398 */     if (AbstractDungeon.player.hasRelic("Bottled Tornado")) {
/* 399 */       if (((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).card != null) {
/* 400 */         this.bottled_tornado = ((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).card.cardID;
/*     */       } else {
/* 402 */         this.bottled_tornado = null;
/*     */       }
/*     */     } else {
/* 405 */       this.bottled_tornado = null;
/*     */     }
/*     */     
/*     */ 
/* 409 */     this.metric_campfire_rested = CardCrawlGame.metricData.campfire_rested;
/* 410 */     this.metric_campfire_upgraded = CardCrawlGame.metricData.campfire_upgraded;
/* 411 */     this.metric_purchased_purges = CardCrawlGame.metricData.purchased_purges;
/* 412 */     this.metric_potions_floor_spawned = CardCrawlGame.metricData.potions_floor_spawned;
/* 413 */     this.metric_potions_floor_usage = CardCrawlGame.metricData.potions_floor_usage;
/* 414 */     this.metric_current_hp_per_floor = CardCrawlGame.metricData.current_hp_per_floor;
/* 415 */     this.metric_max_hp_per_floor = CardCrawlGame.metricData.max_hp_per_floor;
/* 416 */     this.metric_gold_per_floor = CardCrawlGame.metricData.gold_per_floor;
/* 417 */     this.metric_path_per_floor = CardCrawlGame.metricData.path_per_floor;
/* 418 */     this.metric_path_taken = CardCrawlGame.metricData.path_taken;
/* 419 */     this.metric_items_purchased = CardCrawlGame.metricData.items_purchased;
/* 420 */     this.metric_item_purchase_floors = CardCrawlGame.metricData.item_purchase_floors;
/* 421 */     this.metric_items_purged = CardCrawlGame.metricData.items_purged;
/* 422 */     this.metric_items_purged_floors = CardCrawlGame.metricData.items_purged_floors;
/* 423 */     this.metric_card_choices = CardCrawlGame.metricData.card_choices;
/* 424 */     this.metric_event_choices = CardCrawlGame.metricData.event_choices;
/* 425 */     this.metric_boss_relics = CardCrawlGame.metricData.boss_relics;
/* 426 */     this.metric_potions_obtained = CardCrawlGame.metricData.potions_obtained;
/* 427 */     this.metric_relics_obtained = CardCrawlGame.metricData.relics_obtained;
/* 428 */     this.metric_campfire_choices = CardCrawlGame.metricData.campfire_choices;
/* 429 */     this.metric_damage_taken = CardCrawlGame.metricData.damage_taken;
/* 430 */     this.metric_build_version = CardCrawlGame.TRUE_VERSION_NUM;
/* 431 */     this.metric_seed_played = Settings.seed.toString();
/* 432 */     this.metric_floor_reached = AbstractDungeon.floorNum;
/* 433 */     this.metric_playtime = (CardCrawlGame.playtime);
/* 434 */     this.neow_bonus = CardCrawlGame.metricData.neowBonus;
/* 435 */     this.neow_cost = CardCrawlGame.metricData.neowCost;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\saveAndContinue\SaveFile.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */