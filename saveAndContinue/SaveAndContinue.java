/*     */ package com.megacrit.cardcrawl.saveAndContinue;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.GsonBuilder;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.STSSentry;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.exceptions.SaveFileLoadError;
/*     */ import com.megacrit.cardcrawl.helpers.AsyncSaver;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.relics.BottledFlame;
/*     */ import com.megacrit.cardcrawl.relics.BottledLightning;
/*     */ import com.megacrit.cardcrawl.relics.BottledTornado;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SaveAndContinue
/*     */ {
/*  28 */   private static final Logger logger = LogManager.getLogger(SaveAndContinue.class.getName());
/*  29 */   public static final String SAVE_PATH = "saves" + java.io.File.separator;
/*     */   public static String IRONCLAD_FILE;
/*     */   public static String THE_SILENT_FILE;
/*     */   public static String DEFECT_FILE;
/*     */   
/*  34 */   private static String getPlayerSavePath(AbstractPlayer.PlayerClass c) { StringBuilder sb = new StringBuilder();
/*  35 */     sb.append(SAVE_PATH);
/*     */     
/*  37 */     switch (CardCrawlGame.saveSlot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/*     */     case 2: 
/*     */     case 3: 
/*     */     default: 
/*  48 */       sb.append(CardCrawlGame.saveSlot + "_");
/*     */     }
/*     */     
/*     */     
/*  52 */     sb.append(c.name() + ".autosave");
/*  53 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public static void initialize()
/*     */   {
/*  58 */     String tmp = SAVE_PATH;
/*     */     
/*  60 */     switch (CardCrawlGame.saveSlot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/*     */     case 2: 
/*     */     case 3: 
/*     */     default: 
/*  71 */       tmp = tmp + CardCrawlGame.saveSlot + "_";
/*     */     }
/*     */     
/*     */     
/*  75 */     IRONCLAD_FILE = tmp + AbstractPlayer.PlayerClass.IRONCLAD.name() + ".autosave";
/*  76 */     THE_SILENT_FILE = tmp + AbstractPlayer.PlayerClass.THE_SILENT.name() + ".autosave";
/*  77 */     DEFECT_FILE = tmp + AbstractPlayer.PlayerClass.DEFECT.name() + ".autosave";
/*     */   }
/*     */   
/*     */   private static boolean saveExistsAndNotCorrupted(AbstractPlayer.PlayerClass c) {
/*  81 */     String filepath = getPlayerSavePath(c);
/*  82 */     boolean fileExists = Gdx.files.local(filepath).exists();
/*     */     
/*  84 */     if (fileExists) {
/*     */       try {
/*  86 */         loadSaveFile(getPlayerSavePath(c));
/*     */       } catch (SaveFileLoadError saveFileLoadError) {
/*  88 */         deleteSave(c);
/*  89 */         logger.info(c.name() + " save INVALID!");
/*  90 */         return false;
/*     */       }
/*  92 */       logger.info(c.name() + " save exists and is valid.");
/*  93 */       return true;
/*     */     }
/*  95 */     logger.info(c.name() + " save does NOT exist!");
/*  96 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void checkForSaves()
/*     */   {
/* 103 */     ironcladSaveExists = saveExistsAndNotCorrupted(AbstractPlayer.PlayerClass.IRONCLAD);
/* 104 */     silentSaveExists = saveExistsAndNotCorrupted(AbstractPlayer.PlayerClass.THE_SILENT);
/* 105 */     defectSaveExists = saveExistsAndNotCorrupted(AbstractPlayer.PlayerClass.DEFECT);
/*     */   }
/*     */   
/*     */   public static String loadSaveString(AbstractPlayer.PlayerClass c) {
/* 109 */     return loadSaveString(getPlayerSavePath(c));
/*     */   }
/*     */   
/*     */   private static String loadSaveString(String filePath) {
/* 113 */     FileHandle file = Gdx.files.local(filePath);
/* 114 */     String data = file.readString();
/*     */     
/* 116 */     if (SaveFileObfuscator.isObfuscated(data)) {
/* 117 */       return SaveFileObfuscator.decode(data, "key");
/*     */     }
/* 119 */     return data;
/*     */   }
/*     */   
/*     */   public static SaveFile loadSaveFile(AbstractPlayer.PlayerClass c)
/*     */   {
/* 124 */     String fileName = getPlayerSavePath(c);
/*     */     try {
/* 126 */       return loadSaveFile(fileName);
/*     */     } catch (SaveFileLoadError e) {
/* 128 */       com.megacrit.cardcrawl.core.ExceptionHandler.handleException(e, logger);
/* 129 */       LogManager.shutdown();
/*     */       
/*     */ 
/* 132 */       Gdx.app.exit();
/*     */     }
/* 134 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public static boolean ironcladSaveExists;
/*     */   
/*     */   public static boolean silentSaveExists;
/*     */   public static boolean crowbotSaveExists;
/*     */   public static boolean defectSaveExists;
/*     */   private static SaveFile loadSaveFile(String filePath)
/*     */     throws SaveFileLoadError
/*     */   {
/* 146 */     SaveFile saveFile = null;
/* 147 */     Gson gson = new Gson();
/* 148 */     String savestr = null;
/* 149 */     Exception err = null;
/*     */     try {
/* 151 */       savestr = loadSaveString(filePath);
/* 152 */       saveFile = (SaveFile)gson.fromJson(savestr, SaveFile.class);
/*     */     } catch (Exception e) {
/* 154 */       STSSentry.attachToEvent("savefile", saveFile);
/* 155 */       STSSentry.attachToEvent("savestr", savestr);
/* 156 */       if (Gdx.files.local(filePath).exists())
/*     */       {
/* 158 */         SaveHelper.preserveCorruptFile(filePath);
/*     */       }
/* 160 */       err = e;
/* 161 */       if (!filePath.endsWith(".backUp")) {
/* 162 */         logger.info(filePath + " was corrupt, loading backup...");
/* 163 */         return loadSaveFile(filePath + ".backUp");
/*     */       }
/*     */     }
/* 166 */     if (saveFile == null) {
/* 167 */       throw new SaveFileLoadError("Unable to load save file: " + filePath, err);
/*     */     }
/* 169 */     logger.info(filePath + " save file was successfully loaded.");
/* 170 */     return saveFile;
/*     */   }
/*     */   
/*     */   public static void save(SaveFile save) {
/* 174 */     CardCrawlGame.loadingSave = false;
/* 175 */     HashMap<Object, Object> params = new HashMap();
/*     */     
/*     */ 
/* 178 */     params.put("name", save.name);
/* 179 */     params.put("loadout", save.loadout);
/* 180 */     params.put("current_health", Integer.valueOf(save.current_health));
/* 181 */     params.put("max_health", Integer.valueOf(save.max_health));
/* 182 */     params.put("max_orbs", Integer.valueOf(save.max_orbs));
/* 183 */     params.put("gold", Integer.valueOf(save.gold));
/* 184 */     params.put("hand_size", Integer.valueOf(save.hand_size));
/* 185 */     params.put("red", Integer.valueOf(save.red));
/* 186 */     params.put("green", Integer.valueOf(save.green));
/* 187 */     params.put("blue", Integer.valueOf(save.blue));
/*     */     
/*     */ 
/* 190 */     params.put("monsters_killed", Integer.valueOf(save.monsters_killed));
/* 191 */     params.put("elites1_killed", Integer.valueOf(save.elites1_killed));
/* 192 */     params.put("elites2_killed", Integer.valueOf(save.elites2_killed));
/* 193 */     params.put("elites3_killed", Integer.valueOf(save.elites3_killed));
/* 194 */     params.put("gold_gained", Integer.valueOf(save.gold_gained));
/* 195 */     params.put("mystery_machine", Integer.valueOf(save.mystery_machine));
/* 196 */     params.put("champions", Integer.valueOf(save.champions));
/* 197 */     params.put("perfect", Integer.valueOf(save.perfect));
/* 198 */     params.put("overkill", Boolean.valueOf(save.overkill));
/* 199 */     params.put("combo", Boolean.valueOf(save.combo));
/*     */     
/*     */ 
/* 202 */     params.put("cards", save.cards);
/* 203 */     params.put("obtained_cards", save.obtained_cards);
/* 204 */     params.put("relics", save.relics);
/* 205 */     params.put("relic_counters", save.relic_counters);
/* 206 */     params.put("potions", save.potions);
/* 207 */     params.put("potion_slots", Integer.valueOf(save.potion_slots));
/*     */     
/*     */ 
/* 210 */     params.put("is_endless_mode", Boolean.valueOf(save.is_endless_mode));
/* 211 */     params.put("blights", save.blights);
/* 212 */     params.put("blight_counters", save.blight_counters);
/* 213 */     params.put("endless_increments", save.endless_increments);
/*     */     
/*     */ 
/* 216 */     params.put("chose_neow_reward", Boolean.valueOf(save.chose_neow_reward));
/* 217 */     params.put("neow_bonus", save.neow_bonus);
/* 218 */     params.put("neow_cost", save.neow_cost);
/* 219 */     params.put("is_ascension_mode", Boolean.valueOf(save.is_ascension_mode));
/* 220 */     params.put("ascension_level", Integer.valueOf(save.ascension_level));
/* 221 */     params.put("level_name", save.level_name);
/* 222 */     params.put("floor_num", Integer.valueOf(save.floor_num));
/* 223 */     params.put("act_num", Integer.valueOf(save.act_num));
/* 224 */     params.put("event_list", save.event_list);
/* 225 */     params.put("one_time_event_list", save.one_time_event_list);
/* 226 */     params.put("potion_chance", Integer.valueOf(save.potion_chance));
/* 227 */     params.put("event_chances", save.event_chances);
/* 228 */     params.put("monster_list", save.monster_list);
/* 229 */     params.put("elite_monster_list", save.elite_monster_list);
/* 230 */     params.put("boss_list", save.boss_list);
/* 231 */     params.put("play_time", Long.valueOf(save.play_time));
/* 232 */     params.put("save_date", Long.valueOf(save.save_date));
/* 233 */     params.put("seed", Long.valueOf(save.seed));
/* 234 */     params.put("special_seed", Long.valueOf(save.special_seed));
/* 235 */     params.put("is_daily", Boolean.valueOf(save.is_daily));
/* 236 */     params.put("daily_date", Long.valueOf(save.daily_date));
/* 237 */     params.put("is_trial", Boolean.valueOf(save.is_trial));
/* 238 */     params.put("daily_mods", save.daily_mods);
/* 239 */     params.put("custom_mods", save.custom_mods);
/* 240 */     params.put("boss", save.boss);
/* 241 */     params.put("purgeCost", Integer.valueOf(save.purgeCost));
/* 242 */     params.put("monster_seed_count", Integer.valueOf(save.monster_seed_count));
/* 243 */     params.put("event_seed_count", Integer.valueOf(save.event_seed_count));
/* 244 */     params.put("merchant_seed_count", Integer.valueOf(save.merchant_seed_count));
/* 245 */     params.put("card_seed_count", Integer.valueOf(save.card_seed_count));
/* 246 */     params.put("treasure_seed_count", Integer.valueOf(save.treasure_seed_count));
/* 247 */     params.put("relic_seed_count", Integer.valueOf(save.relic_seed_count));
/* 248 */     params.put("potion_seed_count", Integer.valueOf(save.potion_seed_count));
/* 249 */     params.put("ai_seed_count", Integer.valueOf(save.ai_seed_count));
/* 250 */     params.put("shuffle_seed_count", Integer.valueOf(save.shuffle_seed_count));
/* 251 */     params.put("card_random_seed_count", Integer.valueOf(save.card_random_seed_count));
/* 252 */     params.put("card_random_seed_randomizer", Integer.valueOf(save.card_random_seed_randomizer));
/* 253 */     params.put("path_x", save.path_x);
/* 254 */     params.put("path_y", save.path_y);
/* 255 */     params.put("room_x", Integer.valueOf(save.room_x));
/* 256 */     params.put("room_y", Integer.valueOf(save.room_y));
/* 257 */     params.put("spirit_count", Integer.valueOf(save.spirit_count));
/* 258 */     params.put("current_room", save.current_room);
/* 259 */     params.put("common_relics", save.common_relics);
/* 260 */     params.put("uncommon_relics", save.uncommon_relics);
/* 261 */     params.put("rare_relics", save.rare_relics);
/* 262 */     params.put("shop_relics", save.shop_relics);
/* 263 */     params.put("boss_relics", save.boss_relics);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 275 */     params.put("post_combat", Boolean.valueOf(save.post_combat));
/* 276 */     params.put("mugged", Boolean.valueOf(save.mugged));
/* 277 */     params.put("smoked", Boolean.valueOf(save.smoked));
/* 278 */     params.put("combat_rewards", save.combat_rewards);
/*     */     
/*     */ 
/* 281 */     if (AbstractDungeon.player.hasRelic("Bottled Flame")) {
/* 282 */       if (((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).card != null) {
/* 283 */         params.put("bottled_flame", 
/*     */         
/* 285 */           ((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).card.cardID);
/*     */       } else {
/* 287 */         params.put("bottled_flame", null);
/*     */       }
/*     */     }
/*     */     else {
/* 291 */       params.put("bottled_flame", null);
/*     */     }
/*     */     
/*     */ 
/* 295 */     if (AbstractDungeon.player.hasRelic("Bottled Lightning")) {
/* 296 */       if (((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).card != null) {
/* 297 */         params.put("bottled_lightning", 
/*     */         
/* 299 */           ((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).card.cardID);
/*     */       } else {
/* 301 */         params.put("bottled_lightning", null);
/*     */       }
/*     */     } else {
/* 304 */       params.put("bottled_lightning", null);
/*     */     }
/*     */     
/*     */ 
/* 308 */     if (AbstractDungeon.player.hasRelic("Bottled Tornado")) {
/* 309 */       if (((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).card != null) {
/* 310 */         params.put("bottled_tornado", 
/*     */         
/* 312 */           ((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).card.cardID);
/*     */       } else {
/* 314 */         params.put("bottled_tornado", null);
/*     */       }
/*     */     } else {
/* 317 */       params.put("bottled_tornado", null);
/*     */     }
/*     */     
/*     */ 
/* 321 */     params.put("metric_campfire_rested", Integer.valueOf(save.metric_campfire_rested));
/* 322 */     params.put("metric_campfire_upgraded", Integer.valueOf(save.metric_campfire_upgraded));
/* 323 */     params.put("metric_campfire_rituals", Integer.valueOf(save.metric_campfire_rituals));
/* 324 */     params.put("metric_campfire_meditates", Integer.valueOf(save.metric_campfire_meditates));
/* 325 */     params.put("metric_purchased_purges", Integer.valueOf(save.metric_purchased_purges));
/* 326 */     params.put("metric_potions_floor_spawned", save.metric_potions_floor_spawned);
/* 327 */     params.put("metric_potions_floor_usage", save.metric_potions_floor_usage);
/* 328 */     params.put("metric_current_hp_per_floor", save.metric_current_hp_per_floor);
/* 329 */     params.put("metric_max_hp_per_floor", save.metric_max_hp_per_floor);
/* 330 */     params.put("metric_gold_per_floor", save.metric_gold_per_floor);
/* 331 */     params.put("metric_path_per_floor", save.metric_path_per_floor);
/* 332 */     params.put("metric_path_taken", save.metric_path_taken);
/* 333 */     params.put("metric_items_purchased", save.metric_items_purchased);
/* 334 */     params.put("metric_item_purchase_floors", save.metric_item_purchase_floors);
/* 335 */     params.put("metric_items_purged", save.metric_items_purged);
/* 336 */     params.put("metric_items_purged_floors", save.metric_items_purged_floors);
/* 337 */     params.put("metric_card_choices", save.metric_card_choices);
/* 338 */     params.put("metric_event_choices", save.metric_event_choices);
/* 339 */     params.put("metric_boss_relics", save.metric_boss_relics);
/* 340 */     params.put("metric_damage_taken", save.metric_damage_taken);
/* 341 */     params.put("metric_potions_obtained", save.metric_potions_obtained);
/* 342 */     params.put("metric_relics_obtained", save.metric_relics_obtained);
/* 343 */     params.put("metric_campfire_choices", save.metric_campfire_choices);
/* 344 */     params.put("metric_build_version", save.metric_build_version);
/* 345 */     params.put("metric_seed_played", save.metric_seed_played);
/* 346 */     params.put("metric_floor_reached", Integer.valueOf(save.metric_floor_reached));
/* 347 */     params.put("metric_playtime", Long.valueOf(save.metric_playtime));
/*     */     
/* 349 */     Gson gson = new GsonBuilder().setPrettyPrinting().create();
/* 350 */     String data = gson.toJson(params);
/* 351 */     String filepath = getPlayerSavePath(AbstractDungeon.player.chosenClass);
/*     */     
/*     */ 
/* 354 */     if (Settings.isBeta) {
/* 355 */       AsyncSaver.save(filepath + "BETA", data);
/*     */     }
/*     */     
/*     */ 
/* 359 */     AsyncSaver.save(filepath, SaveFileObfuscator.encode(data, "key"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void deleteSave(AbstractPlayer.PlayerClass c)
/*     */   {
/* 369 */     StringBuilder sb = new StringBuilder();
/* 370 */     sb.append("DELETING ");
/*     */     
/* 372 */     switch (CardCrawlGame.saveSlot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/*     */     case 2: 
/*     */     case 3: 
/*     */     default: 
/* 383 */       sb.append(CardCrawlGame.saveSlot + "_");
/*     */     }
/*     */     
/*     */     
/* 387 */     sb.append(c.name() + " SAVE");
/* 388 */     logger.info(sb.toString());
/* 389 */     Gdx.files.local(getPlayerSavePath(c)).delete();
/* 390 */     Gdx.files.local(getPlayerSavePath(c) + ".backUp").delete();
/*     */   }
/*     */   
/*     */   public static boolean isLoadingAutoSave(AbstractPlayer.PlayerClass selection) {
/* 394 */     switch (selection) {
/*     */     case IRONCLAD: 
/* 396 */       return ironcladSaveExists;
/*     */     case THE_SILENT: 
/* 398 */       return silentSaveExists;
/*     */     case DEFECT: 
/* 400 */       return defectSaveExists;
/*     */     }
/* 402 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\saveAndContinue\SaveAndContinue.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */