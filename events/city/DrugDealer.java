/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.colorless.JAX;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.Circlet;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class DrugDealer extends AbstractImageEvent
/*     */ {
/*  25 */   private static final Logger logger = LogManager.getLogger(DrugDealer.class.getName());
/*     */   public static final String ID = "Drug Dealer";
/*  27 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Drug Dealer");
/*  28 */   public static final String NAME = eventStrings.NAME;
/*  29 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  30 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  32 */   private int screenNum = 0;
/*     */   
/*     */   public DrugDealer() {
/*  35 */     super(NAME, DESCRIPTIONS[0], "images/events/drugDealer.jpg");
/*     */     
/*  37 */     this.imageEventText.setDialogOption(OPTIONS[0], CardLibrary.getCopy("J.A.X."));
/*  38 */     if (AbstractDungeon.player.masterDeck.getPurgeableCards().size() >= 3) {
/*  39 */       this.imageEventText.setDialogOption(OPTIONS[1]);
/*     */     } else {
/*  41 */       this.imageEventText.setDialogOption(OPTIONS[4], true);
/*     */     }
/*  43 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  48 */     switch (this.screenNum) {
/*     */     case 0: 
/*  50 */       switch (buttonPressed) {
/*     */       case 0: 
/*  52 */         logMetric("Got JAX");
/*  53 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/*  54 */         UnlockTracker.markCardAsSeen("J.A.X.");
/*  55 */         AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new JAX(), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */         
/*  57 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  58 */         this.imageEventText.clearRemainingOptions();
/*  59 */         break;
/*     */       case 1: 
/*  61 */         logMetric("Became Test Subject");
/*  62 */         this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/*  63 */         transform();
/*  64 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  65 */         this.imageEventText.clearRemainingOptions();
/*  66 */         break;
/*     */       case 2: 
/*  68 */         logMetric("Inject Mutagens");
/*  69 */         this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/*  70 */         if (!AbstractDungeon.player.hasRelic("MutagenicStrength")) {
/*  71 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.drawX, this.drawY, new com.megacrit.cardcrawl.relics.MutagenicStrength());
/*     */         } else {
/*  73 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.drawX, this.drawY, new Circlet());
/*     */         }
/*  75 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  76 */         this.imageEventText.clearRemainingOptions();
/*  77 */         break;
/*     */       default: 
/*  79 */         logger.info("ERROR: Unhandled case " + buttonPressed);
/*     */       }
/*     */       
/*  82 */       this.screenNum = 1;
/*  83 */       break;
/*     */     case 1: 
/*  85 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void transform()
/*     */   {
/*  91 */     ArrayList<AbstractCard> transformableCards = new ArrayList();
/*  92 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.getPurgeableCards().group) {
/*  93 */       transformableCards.add(c);
/*     */     }
/*     */     
/*  96 */     java.util.Collections.shuffle(transformableCards, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     float displayCount;
/*  98 */     if (!transformableCards.isEmpty())
/*     */     {
/*     */ 
/*     */ 
/* 102 */       CardGroup tmp = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/* 103 */       tmp.addToBottom((AbstractCard)transformableCards.get(0));
/* 104 */       tmp.addToBottom((AbstractCard)transformableCards.get(1));
/* 105 */       tmp.addToBottom((AbstractCard)transformableCards.get(2));
/*     */       
/* 107 */       displayCount = 0.0F;
/* 108 */       for (AbstractCard card : tmp.group) {
/* 109 */         AbstractDungeon.player.masterDeck.removeCard(card);
/* 110 */         AbstractDungeon.transformCard(card, false, AbstractDungeon.miscRng);
/*     */         
/* 112 */         if ((AbstractDungeon.screen != AbstractDungeon.CurrentScreen.TRANSFORM) && (AbstractDungeon.transformedCard != null))
/*     */         {
/* 114 */           AbstractDungeon.topLevelEffectsQueue.add(new ShowCardAndObtainEffect(
/*     */           
/* 116 */             AbstractDungeon.getTransformedCard(), Settings.WIDTH / 3.0F + displayCount, Settings.HEIGHT / 2.0F, false));
/*     */           
/*     */ 
/*     */ 
/* 120 */           displayCount += Settings.WIDTH / 6.0F;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 127 */     AbstractEvent.logMetric("Drug Dealer", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\DrugDealer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */