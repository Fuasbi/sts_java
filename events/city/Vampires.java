/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.blue.Strike_Blue;
/*     */ import com.megacrit.cardcrawl.cards.colorless.Bite;
/*     */ import com.megacrit.cardcrawl.cards.red.Strike_Red;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.BloodVial;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class Vampires extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Vampires";
/*  24 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Vampires");
/*  25 */   public static final String NAME = eventStrings.NAME;
/*  26 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  27 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*  28 */   private static final String ACCEPT_BODY = DESCRIPTIONS[2];
/*  29 */   private static final String EXIT_BODY = DESCRIPTIONS[3];
/*  30 */   private static final String GIVE_VIAL = DESCRIPTIONS[4];
/*     */   private static final int HP_DRAIN_PERCENT = 30;
/*     */   private static final float HP_PERCENT = 0.7F;
/*  33 */   private int screenNum = 0;
/*     */   private boolean hasVial;
/*     */   
/*     */   public Vampires() {
/*  37 */     super(NAME, "test", "images/events/vampires.jpg");
/*     */     
/*  39 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case THE_SILENT: 
/*  41 */       this.body = DESCRIPTIONS[1];
/*  42 */       break;
/*     */     case IRONCLAD: 
/*  44 */       this.body = DESCRIPTIONS[0];
/*  45 */       break;
/*     */     case DEFECT: 
/*  47 */       this.body = DESCRIPTIONS[5];
/*     */     default: 
/*  49 */       this.body = DESCRIPTIONS[5];
/*     */     }
/*     */     
/*     */     
/*  53 */     this.hasVial = AbstractDungeon.player.hasRelic("Blood Vial");
/*     */     
/*  55 */     this.imageEventText.setDialogOption(OPTIONS[0] + 30 + OPTIONS[1], new Bite());
/*  56 */     if (this.hasVial) {
/*  57 */       String vialName = new BloodVial().name;
/*  58 */       this.imageEventText.setDialogOption(OPTIONS[3] + vialName + OPTIONS[4], new Bite());
/*     */     }
/*     */     
/*  61 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  66 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  69 */       switch (buttonPressed)
/*     */       {
/*     */ 
/*     */       case 0: 
/*  73 */         CardCrawlGame.sound.play("EVENT_VAMP_BITE");
/*  74 */         this.imageEventText.updateBodyText(ACCEPT_BODY);
/*     */         
/*     */ 
/*  77 */         AbstractDungeon.player.maxHealth = ((int)(AbstractDungeon.player.maxHealth * 0.7F));
/*  78 */         if (AbstractDungeon.player.maxHealth <= 0) {
/*  79 */           AbstractDungeon.player.maxHealth = 1;
/*     */         }
/*  81 */         logMetric("Became a vampire");
/*  82 */         if (AbstractDungeon.player.currentHealth > AbstractDungeon.player.maxHealth) {
/*  83 */           AbstractDungeon.player.currentHealth = AbstractDungeon.player.maxHealth;
/*     */         }
/*     */         
/*  86 */         replaceAttacks();
/*     */         
/*     */ 
/*  89 */         this.screenNum = 1;
/*  90 */         this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/*  91 */         this.imageEventText.clearRemainingOptions();
/*  92 */         break;
/*     */       
/*     */ 
/*     */       case 1: 
/*  96 */         if (this.hasVial) {
/*  97 */           CardCrawlGame.sound.play("EVENT_VAMP_BITE");
/*  98 */           this.imageEventText.updateBodyText(GIVE_VIAL);
/*  99 */           logMetric("Became a vampire (Vial)");
/* 100 */           AbstractDungeon.player.loseRelic("Blood Vial");
/* 101 */           replaceAttacks();
/* 102 */           this.screenNum = 1;
/* 103 */           this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 104 */           this.imageEventText.clearRemainingOptions(); }
/* 105 */         break;
/*     */       }
/*     */       
/*     */       
/*     */ 
/* 110 */       logMetric("Ignored");
/* 111 */       this.imageEventText.updateBodyText(EXIT_BODY);
/* 112 */       this.screenNum = 2;
/* 113 */       this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 114 */       this.imageEventText.clearRemainingOptions();
/* 115 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/* 120 */       openMap();
/* 121 */       break;
/*     */     
/*     */     default: 
/* 124 */       openMap();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void replaceAttacks()
/*     */   {
/* 131 */     for (Iterator<AbstractCard> i = AbstractDungeon.player.masterDeck.group.iterator(); i.hasNext();) {
/* 132 */       AbstractCard e = (AbstractCard)i.next();
/* 133 */       if (((e instanceof Strike_Red)) || ((e instanceof com.megacrit.cardcrawl.cards.green.Strike_Green)) || ((e instanceof Strike_Blue))) {
/* 134 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 138 */     for (int i = 0; i < 5; i++) {
/* 139 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new Bite(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/* 141 */       UnlockTracker.markCardAsSeen("Bite");
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 147 */     AbstractEvent.logMetric("Vampires", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\Vampires.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */