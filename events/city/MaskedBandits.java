/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ 
/*     */ public class MaskedBandits extends AbstractEvent
/*     */ {
/*     */   public static final String ID = "Masked Bandits";
/*  17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Masked Bandits");
/*  18 */   public static final String NAME = eventStrings.NAME;
/*  19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  22 */   private static final String PAID_MSG_1 = DESCRIPTIONS[0];
/*  23 */   private static final String PAID_MSG_2 = DESCRIPTIONS[1];
/*  24 */   private static final String PAID_MSG_3 = DESCRIPTIONS[2];
/*  25 */   private static final String PAID_MSG_4 = DESCRIPTIONS[3];
/*     */   
/*  27 */   private CurScreen screen = CurScreen.INTRO;
/*     */   
/*     */   private static enum CurScreen {
/*  30 */     INTRO,  PAID_1,  PAID_2,  PAID_3,  END;
/*     */     
/*     */     private CurScreen() {}
/*     */   }
/*     */   
/*  35 */   public MaskedBandits() { this.body = DESCRIPTIONS[4];
/*     */     
/*  37 */     this.roomEventText.addDialogOption(OPTIONS[0]);
/*  38 */     this.roomEventText.addDialogOption(OPTIONS[1]);
/*     */     
/*  40 */     this.hasDialog = true;
/*  41 */     this.hasFocus = true;
/*  42 */     AbstractDungeon.getCurrRoom().monsters = com.megacrit.cardcrawl.helpers.MonsterHelper.getEncounter("Masked Bandits");
/*     */   }
/*     */   
/*     */   public void update() {
/*  46 */     super.update();
/*     */     
/*  48 */     if (!RoomEventDialog.waitForInput) {
/*  49 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  55 */     switch (this.screen) {
/*     */     case INTRO: 
/*  57 */       switch (buttonPressed) {
/*     */       case 0: 
/*  59 */         logMetric("Paid Fearfully");
/*     */         
/*  61 */         stealGold();
/*  62 */         AbstractDungeon.player.loseGold(AbstractDungeon.player.gold);
/*  63 */         this.roomEventText.updateBodyText(PAID_MSG_1);
/*  64 */         this.roomEventText.updateDialogOption(0, OPTIONS[2]);
/*  65 */         this.roomEventText.clearRemainingOptions();
/*  66 */         this.screen = CurScreen.PAID_1;
/*  67 */         return;
/*     */       
/*     */       case 1: 
/*  70 */         logMetric("Fought Bandits");
/*     */         
/*  72 */         if (com.megacrit.cardcrawl.core.Settings.isDailyRun) {
/*  73 */           AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(30));
/*     */         } else {
/*  75 */           AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(25, 35));
/*     */         }
/*  77 */         if (AbstractDungeon.player.hasRelic("Red Mask")) {
/*  78 */           AbstractDungeon.getCurrRoom().addRelicToRewards(new com.megacrit.cardcrawl.relics.Circlet());
/*     */         } else {
/*  80 */           AbstractDungeon.getCurrRoom().addRelicToRewards(new com.megacrit.cardcrawl.relics.RedMask());
/*     */         }
/*     */         
/*  83 */         enterCombat();
/*  84 */         AbstractDungeon.lastCombatMetricKey = "Masked Bandits";
/*  85 */         return;
/*     */       }
/*  87 */       break;
/*     */     case PAID_1: 
/*  89 */       this.roomEventText.updateBodyText(PAID_MSG_2);
/*  90 */       this.screen = CurScreen.PAID_2;
/*  91 */       this.roomEventText.updateDialogOption(0, OPTIONS[2]);
/*  92 */       break;
/*     */     case PAID_2: 
/*  94 */       this.roomEventText.updateBodyText(PAID_MSG_3);
/*  95 */       this.screen = CurScreen.PAID_3;
/*  96 */       this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/*  97 */       break;
/*     */     case PAID_3: 
/*  99 */       this.roomEventText.updateBodyText(PAID_MSG_4);
/* 100 */       this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/* 101 */       this.screen = CurScreen.END;
/* 102 */       openMap();
/* 103 */       break;
/*     */     case END: 
/* 105 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void stealGold()
/*     */   {
/* 111 */     AbstractCreature target = AbstractDungeon.player;
/* 112 */     if (target.gold == 0)
/*     */     {
/* 114 */       return;
/*     */     }
/*     */     
/* 117 */     CardCrawlGame.sound.play("GOLD_JINGLE");
/*     */     
/* 119 */     for (int i = 0; i < target.gold; i++) {
/* 120 */       AbstractCreature source = AbstractDungeon.getCurrRoom().monsters.getRandomMonster();
/* 121 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.GainPennyEffect(source, target.hb.cX, target.hb.cY, source.hb.cX, source.hb.cY, false));
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 127 */     AbstractEvent.logMetric("Masked Bandits", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\MaskedBandits.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */