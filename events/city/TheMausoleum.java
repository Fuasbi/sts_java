/*    */ package com.megacrit.cardcrawl.events.city;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class TheMausoleum extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "The Mausoleum";
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("The Mausoleum");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String CURSED_RESULT = DESCRIPTIONS[1];
/* 24 */   private static final String NORMAL_RESULT = DESCRIPTIONS[2];
/* 25 */   private static final String NOPE_RESULT = DESCRIPTIONS[3];
/* 26 */   private CurScreen screen = CurScreen.INTRO;
/*    */   private static final int PERCENT = 50;
/*    */   private static final int A_2_PERCENT = 100;
/*    */   private int percent;
/*    */   
/*    */   private static enum CurScreen {
/* 32 */     INTRO,  RESULT;
/*    */     
/*    */     private CurScreen() {} }
/*    */   
/* 36 */   public TheMausoleum() { super(NAME, DIALOG_1, "images/events/mausoleum.jpg");
/*    */     
/* 38 */     if (AbstractDungeon.ascensionLevel >= 15) {
/* 39 */       this.percent = 100;
/*    */     } else {
/* 41 */       this.percent = 50;
/*    */     }
/*    */     
/* 44 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.percent + OPTIONS[1], com.megacrit.cardcrawl.helpers.CardLibrary.getCopy("Writhe"));
/* 45 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 50 */     super.update();
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 55 */     switch (this.screen) {
/*    */     case INTRO: 
/* 57 */       switch (buttonPressed)
/*    */       {
/*    */       case 0: 
/* 60 */         boolean result = AbstractDungeon.eventRng.randomBoolean();
/* 61 */         if (AbstractDungeon.ascensionLevel >= 15) {
/* 62 */           result = true;
/*    */         }
/*    */         
/* 65 */         if (result) {
/* 66 */           this.imageEventText.updateBodyText(CURSED_RESULT);
/* 67 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Writhe(), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*    */           
/* 69 */           UnlockTracker.markCardAsSeen("Writhe");
/*    */         } else {
/* 71 */           this.imageEventText.updateBodyText(NORMAL_RESULT);
/*    */         }
/*    */         
/* 74 */         CardCrawlGame.sound.play("BLUNT_HEAVY");
/* 75 */         CardCrawlGame.screenShake.rumble(2.0F);
/* 76 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*    */         
/*    */ 
/* 79 */           AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 80 */         logMetric("Yes");
/* 81 */         break;
/*    */       default: 
/* 83 */         this.imageEventText.updateBodyText(NOPE_RESULT);
/* 84 */         logMetric("No");
/*    */       }
/*    */       
/* 87 */       this.imageEventText.clearAllDialogs();
/* 88 */       this.imageEventText.setDialogOption(OPTIONS[2]);
/* 89 */       this.screen = CurScreen.RESULT;
/* 90 */       break;
/*    */     default: 
/* 92 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 98 */     AbstractEvent.logMetric("The Mausoleum", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\TheMausoleum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */