/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.blue.Defend_Blue;
/*     */ import com.megacrit.cardcrawl.cards.blue.Strike_Blue;
/*     */ import com.megacrit.cardcrawl.cards.red.Defend_Red;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BackToBasics extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Back to Basics";
/*  24 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Back to Basics");
/*  25 */   public static final String NAME = eventStrings.NAME;
/*  26 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  27 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  29 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  30 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  31 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*  32 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  35 */     INTRO,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  39 */   public BackToBasics() { super(NAME, DIALOG_1, "images/events/backToBasics.jpg");
/*  40 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  41 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  46 */     if (Settings.AMBIANCE_ON) {
/*  47 */       CardCrawlGame.sound.play("EVENT_ANCIENT");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  53 */     super.update();
/*     */     
/*     */ 
/*  56 */     if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/*  57 */       AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/*  58 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(c));
/*  59 */       AbstractDungeon.player.masterDeck.removeCard(c);
/*  60 */       AbstractDungeon.gridSelectScreen.selectedCards.remove(c);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  66 */     switch (this.screen)
/*     */     {
/*     */     case INTRO: 
/*  69 */       if (buttonPressed == 0) {
/*  70 */         this.imageEventText.updateBodyText(DIALOG_2);
/*  71 */         logMetric("Elegance");
/*  72 */         AbstractDungeon.gridSelectScreen.open(
/*  73 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck.getPurgeableCards()), 1, OPTIONS[2], false);
/*     */         
/*     */ 
/*     */ 
/*  77 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  78 */         this.imageEventText.clearRemainingOptions();
/*     */       } else {
/*  80 */         this.imageEventText.updateBodyText(DIALOG_3);
/*  81 */         logMetric("Simplicity");
/*  82 */         upgradeStrikeAndDefends();
/*  83 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  84 */         this.imageEventText.clearRemainingOptions();
/*     */       }
/*  86 */       this.screen = CUR_SCREEN.COMPLETE;
/*  87 */       break;
/*     */     case COMPLETE: 
/*  89 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void upgradeStrikeAndDefends()
/*     */   {
/*  95 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  96 */       if (((c instanceof com.megacrit.cardcrawl.cards.red.Strike_Red)) || ((c instanceof Defend_Red)) || ((c instanceof com.megacrit.cardcrawl.cards.green.Strike_Green)) || ((c instanceof com.megacrit.cardcrawl.cards.green.Defend_Green)) || ((c instanceof Strike_Blue)) || ((c instanceof Defend_Blue)))
/*     */       {
/*  98 */         if (c.canUpgrade()) {
/*  99 */           c.upgrade();
/* 100 */           AbstractDungeon.player.bottledCardUpgradeCheck(c);
/* 101 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect(c
/*     */           
/* 103 */             .makeStatEquivalentCopy(), 
/* 104 */             MathUtils.random(0.1F, 0.9F) * Settings.WIDTH, 
/* 105 */             MathUtils.random(0.2F, 0.8F) * Settings.HEIGHT));
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 112 */     AbstractEvent.logMetric("Back to Basics", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\BackToBasics.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */