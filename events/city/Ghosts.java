/*    */ package com.megacrit.cardcrawl.events.city;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.colorless.Apparition;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Ghosts extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Ghosts";
/* 17 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Ghosts");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/* 21 */   private static final String INTRO_BODY_M = DESCRIPTIONS[0];
/* 22 */   private static final String ACCEPT_BODY = DESCRIPTIONS[2];
/* 23 */   private static final String EXIT_BODY = DESCRIPTIONS[3];
/*    */   private static final int HP_DRAIN_PERCENT = 50;
/*    */   private static final float HP_PERCENT = 0.5F;
/* 26 */   private int screenNum = 0;
/*    */   
/*    */   public Ghosts() {
/* 29 */     super(NAME, "test", "images/events/ghost.jpg");
/* 30 */     this.body = INTRO_BODY_M;
/*    */     
/* 32 */     this.imageEventText.setDialogOption(OPTIONS[0] + 50 + OPTIONS[1], new Apparition());
/* 33 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 38 */     switch (this.screenNum)
/*    */     {
/*    */     case 0: 
/* 41 */       switch (buttonPressed)
/*    */       {
/*    */ 
/*    */       case 0: 
/* 45 */         this.imageEventText.updateBodyText(ACCEPT_BODY);
/*    */         
/*    */ 
/* 48 */         AbstractDungeon.player.maxHealth = ((int)(AbstractDungeon.player.maxHealth * 0.5F));
/* 49 */         if (AbstractDungeon.player.maxHealth <= 0) {
/* 50 */           AbstractDungeon.player.maxHealth = 1;
/*    */         }
/* 52 */         logMetric("Became a Ghost");
/* 53 */         if (AbstractDungeon.player.currentHealth > AbstractDungeon.player.maxHealth) {
/* 54 */           AbstractDungeon.player.currentHealth = AbstractDungeon.player.maxHealth;
/*    */         }
/*    */         
/* 57 */         becomeGhost();
/*    */         
/*    */ 
/* 60 */         this.screenNum = 1;
/* 61 */         this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 62 */         this.imageEventText.clearRemainingOptions();
/* 63 */         break;
/*    */       
/*    */       default: 
/* 66 */         logMetric("Ignored");
/* 67 */         this.imageEventText.updateBodyText(EXIT_BODY);
/* 68 */         this.screenNum = 2;
/* 69 */         this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 70 */         this.imageEventText.clearRemainingOptions(); }
/* 71 */       break;
/*    */     
/*    */ 
/*    */ 
/*    */     case 1: 
/* 76 */       openMap();
/* 77 */       break;
/*    */     
/*    */     default: 
/* 80 */       openMap();
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   private void becomeGhost()
/*    */   {
/* 87 */     for (int i = 0; i < 5; i++) {
/* 88 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new Apparition(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */       
/* 90 */       UnlockTracker.markCardAsSeen("Ghostly");
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 97 */     AbstractEvent.logMetric("Ghosts", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\Ghosts.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */