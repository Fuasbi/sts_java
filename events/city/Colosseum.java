/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Colosseum extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Colosseum";
/*  15 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Colosseum");
/*  16 */   public static final String NAME = eventStrings.NAME;
/*  17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  20 */   private CurScreen screen = CurScreen.INTRO;
/*     */   
/*     */   private static enum CurScreen {
/*  23 */     INTRO,  FIGHT,  LEAVE,  POST_COMBAT;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  27 */   public Colosseum() { super(NAME, DESCRIPTIONS[0], "images/events/colosseum.jpg");
/*     */     
/*     */ 
/*  30 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  35 */     switch (this.screen) {
/*     */     case INTRO: 
/*  37 */       switch (buttonPressed) {
/*     */       case 0: 
/*  39 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1] + DESCRIPTIONS[2] + 4200 + DESCRIPTIONS[3]);
/*  40 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/*  41 */         this.screen = CurScreen.FIGHT;
/*     */       }
/*     */       
/*  44 */       break;
/*     */     case FIGHT: 
/*  46 */       switch (buttonPressed) {
/*     */       case 0: 
/*  48 */         this.screen = CurScreen.POST_COMBAT;
/*  49 */         logMetric("Fight");
/*  50 */         AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter("Colosseum Slavers");
/*  51 */         AbstractDungeon.getCurrRoom().rewards.clear();
/*  52 */         AbstractDungeon.getCurrRoom().rewardAllowed = false;
/*  53 */         enterCombatFromImage();
/*  54 */         AbstractDungeon.lastCombatMetricKey = "Colosseum Slavers";
/*  55 */         break;
/*     */       }
/*     */       
/*     */       
/*  59 */       this.imageEventText.clearRemainingOptions();
/*  60 */       break;
/*     */     case POST_COMBAT: 
/*  62 */       AbstractDungeon.getCurrRoom().rewardAllowed = true;
/*  63 */       switch (buttonPressed) {
/*     */       case 1: 
/*  65 */         this.screen = CurScreen.LEAVE;
/*  66 */         logMetric("Fought Nobs");
/*  67 */         AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter("Colosseum Nobs");
/*  68 */         AbstractDungeon.getCurrRoom().rewards.clear();
/*  69 */         AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.RARE);
/*  70 */         AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.UNCOMMON);
/*  71 */         AbstractDungeon.getCurrRoom().addGoldToRewards(100);
/*  72 */         enterCombatFromImage();
/*  73 */         AbstractDungeon.lastCombatMetricKey = "Colosseum Nobs";
/*  74 */         break;
/*     */       default: 
/*  76 */         logMetric("Fled From Nobs");
/*  77 */         openMap(); }
/*  78 */       break;
/*     */     
/*     */ 
/*     */     case LEAVE: 
/*  82 */       openMap();
/*  83 */       break;
/*     */     default: 
/*  85 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/*  91 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Colosseum", actionTaken);
/*     */   }
/*     */   
/*     */   public void reopen()
/*     */   {
/*  96 */     if (this.screen != CurScreen.LEAVE) {
/*  97 */       enterImageFromCombat();
/*  98 */       this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/*  99 */       this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/* 100 */       this.imageEventText.setDialogOption(OPTIONS[3]);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\Colosseum.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */