/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class CursedTome extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Cursed Tome";
/*  23 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Cursed Tome");
/*  24 */   public static final String NAME = eventStrings.NAME;
/*  25 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  26 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */ 
/*  29 */   private static final String INTRO_MSG = DESCRIPTIONS[0];
/*  30 */   private static final String READ_1 = DESCRIPTIONS[1];
/*  31 */   private static final String READ_2 = DESCRIPTIONS[2];
/*  32 */   private static final String READ_3 = DESCRIPTIONS[3];
/*  33 */   private static final String READ_4 = DESCRIPTIONS[4];
/*  34 */   private static final String OBTAIN_MSG = DESCRIPTIONS[5];
/*  35 */   private static final String IGNORE_MSG = DESCRIPTIONS[6];
/*  36 */   private static final String STOP_MSG = DESCRIPTIONS[7];
/*     */   
/*     */ 
/*  39 */   private static final String OPT_READ = OPTIONS[0];
/*  40 */   private static final String OPT_CONTINUE_1 = OPTIONS[1];
/*  41 */   private static final String OPT_CONTINUE_2 = OPTIONS[2];
/*  42 */   private static final String OPT_CONTINUE_3 = OPTIONS[3];
/*  43 */   private static final String OPT_STOP = OPTIONS[4];
/*  44 */   private static final String OPT_LEAVE = OPTIONS[7];
/*     */   
/*     */   private static final int DMG_BOOK_OPEN = 1;
/*     */   
/*     */   private static final int DMG_SECOND_PAGE = 2;
/*     */   
/*     */   private static final int DMG_THIRD_PAGE = 3;
/*     */   private static final int DMG_STOP_READING = 3;
/*     */   private static final int DMG_OBTAIN_BOOK = 10;
/*     */   private static final int A_2_DMG_OBTAIN_BOOK = 15;
/*     */   private int finalDmg;
/*  55 */   private CurScreen screen = CurScreen.INTRO;
/*     */   
/*     */   private static enum CurScreen {
/*  58 */     INTRO,  PAGE_1,  PAGE_2,  PAGE_3,  LAST_PAGE,  END;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  62 */   public CursedTome() { super(NAME, INTRO_MSG, "images/events/cursedTome.jpg");
/*  63 */     this.noCardsInRewards = true;
/*  64 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  65 */       this.finalDmg = 15;
/*     */     } else {
/*  67 */       this.finalDmg = 10;
/*     */     }
/*     */     
/*  70 */     this.imageEventText.setDialogOption(OPT_READ);
/*  71 */     this.imageEventText.setDialogOption(OPT_LEAVE);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  76 */     switch (this.screen) {
/*     */     case INTRO: 
/*  78 */       this.imageEventText.clearAllDialogs();
/*  79 */       if (buttonPressed == 0) {
/*  80 */         this.imageEventText.clearAllDialogs();
/*  81 */         this.imageEventText.setDialogOption(OPT_CONTINUE_1);
/*  82 */         this.imageEventText.updateBodyText(READ_1);
/*  83 */         this.screen = CurScreen.PAGE_1;
/*     */       } else {
/*  85 */         this.imageEventText.clearAllDialogs();
/*  86 */         this.imageEventText.setDialogOption(OPT_LEAVE);
/*  87 */         this.imageEventText.updateBodyText(IGNORE_MSG);
/*  88 */         this.screen = CurScreen.END;
/*  89 */         logMetric("Ignored");
/*     */       }
/*  91 */       break;
/*     */     case PAGE_1: 
/*  93 */       AbstractDungeon.player.damage(new DamageInfo(null, 1, DamageInfo.DamageType.HP_LOSS));
/*  94 */       this.imageEventText.clearAllDialogs();
/*  95 */       this.imageEventText.setDialogOption(OPT_CONTINUE_2);
/*  96 */       this.imageEventText.updateBodyText(READ_2);
/*  97 */       this.screen = CurScreen.PAGE_2;
/*  98 */       break;
/*     */     case PAGE_2: 
/* 100 */       AbstractDungeon.player.damage(new DamageInfo(null, 2, DamageInfo.DamageType.HP_LOSS));
/* 101 */       this.imageEventText.clearAllDialogs();
/* 102 */       this.imageEventText.setDialogOption(OPT_CONTINUE_3);
/* 103 */       this.imageEventText.updateBodyText(READ_3);
/* 104 */       this.screen = CurScreen.PAGE_3;
/* 105 */       break;
/*     */     case PAGE_3: 
/* 107 */       AbstractDungeon.player.damage(new DamageInfo(null, 3, DamageInfo.DamageType.HP_LOSS));
/* 108 */       this.imageEventText.clearAllDialogs();
/* 109 */       this.imageEventText.setDialogOption(OPTIONS[5] + this.finalDmg + OPTIONS[6]);
/* 110 */       this.imageEventText.setDialogOption(OPT_STOP);
/* 111 */       this.imageEventText.updateBodyText(READ_4);
/* 112 */       this.screen = CurScreen.LAST_PAGE;
/* 113 */       break;
/*     */     case LAST_PAGE: 
/* 115 */       if (buttonPressed == 0) {
/* 116 */         AbstractDungeon.player.damage(new DamageInfo(null, this.finalDmg, DamageInfo.DamageType.HP_LOSS));
/* 117 */         this.imageEventText.updateBodyText(OBTAIN_MSG);
/* 118 */         logMetric("Obtained Book");
/* 119 */         randomBook();
/* 120 */         this.imageEventText.clearAllDialogs();
/* 121 */         this.imageEventText.setDialogOption(OPT_LEAVE);
/*     */       } else {
/* 123 */         AbstractDungeon.player.damage(new DamageInfo(null, 3, DamageInfo.DamageType.HP_LOSS));
/* 124 */         this.imageEventText.updateBodyText(STOP_MSG);
/* 125 */         logMetric("Stopped");
/* 126 */         this.imageEventText.clearAllDialogs();
/* 127 */         this.imageEventText.setDialogOption(OPT_LEAVE);
/* 128 */         this.screen = CurScreen.END;
/*     */       }
/* 130 */       break;
/*     */     case END: 
/* 132 */       openMap();
/* 133 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   private void randomBook()
/*     */   {
/* 141 */     ArrayList<AbstractRelic> possibleBooks = new ArrayList();
/*     */     
/* 143 */     if (!AbstractDungeon.player.hasRelic("Necronomicon")) {
/* 144 */       possibleBooks.add(RelicLibrary.getRelic("Necronomicon").makeCopy());
/*     */     }
/*     */     
/* 147 */     if (!AbstractDungeon.player.hasRelic("Enchiridion")) {
/* 148 */       possibleBooks.add(RelicLibrary.getRelic("Enchiridion").makeCopy());
/*     */     }
/*     */     
/* 151 */     if (!AbstractDungeon.player.hasRelic("Nilry's Codex")) {
/* 152 */       possibleBooks.add(RelicLibrary.getRelic("Nilry's Codex").makeCopy());
/*     */     }
/*     */     
/* 155 */     if (possibleBooks.size() == 0) {
/* 156 */       possibleBooks.add(RelicLibrary.getRelic("Circlet").makeCopy());
/*     */     }
/*     */     
/* 159 */     AbstractRelic r = (AbstractRelic)possibleBooks.get(AbstractDungeon.miscRng.random.nextInt(possibleBooks.size()));
/*     */     
/* 161 */     AbstractDungeon.getCurrRoom().rewards.clear();
/* 162 */     AbstractDungeon.getCurrRoom().addRelicToRewards(r);
/* 163 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 164 */     AbstractDungeon.combatRewardScreen.open();
/* 165 */     this.screen = CurScreen.END;
/*     */   }
/*     */   
/*     */   public void logMetric(String msg) {
/* 169 */     AbstractEvent.logMetric("Cursed Tome", msg);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\CursedTome.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */