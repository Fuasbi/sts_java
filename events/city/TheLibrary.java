/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class TheLibrary extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "The Library";
/*  24 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("The Library");
/*  25 */   public static final String NAME = eventStrings.NAME;
/*  26 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  27 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  29 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  30 */   private static final String SLEEP_RESULT = DESCRIPTIONS[1];
/*  31 */   private int screenNum = 0;
/*  32 */   private boolean pickCard = false;
/*     */   private static final float HP_HEAL_PERCENT = 0.33F;
/*     */   private static final float A_2_HP_HEAL_PERCENT = 0.2F;
/*     */   private int healAmt;
/*     */   
/*     */   public TheLibrary()
/*     */   {
/*  39 */     super(NAME, DIALOG_1, "images/events/library.jpg");
/*     */     
/*  41 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  42 */       this.healAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.2F);
/*     */     } else {
/*  44 */       this.healAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.33F);
/*     */     }
/*     */     
/*  47 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  48 */     this.imageEventText.setDialogOption(OPTIONS[1] + this.healAmt + OPTIONS[2]);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  53 */     super.update();
/*  54 */     if ((this.pickCard) && 
/*  55 */       (!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  56 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*     */       
/*  58 */         ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).makeCopy(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/*     */ 
/*  61 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  68 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  71 */       switch (buttonPressed) {
/*     */       case 0: 
/*  73 */         this.imageEventText.updateBodyText(getBook());
/*  74 */         logMetric("Read");
/*  75 */         this.screenNum = 1;
/*  76 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  77 */         this.imageEventText.clearRemainingOptions();
/*  78 */         this.pickCard = true;
/*  79 */         CardGroup group = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  80 */         AbstractCard card = null;
/*     */         
/*  82 */         for (int i = 0; i < 20; i++) {
/*  83 */           card = AbstractDungeon.getCard(AbstractDungeon.rollRarity());
/*  84 */           if (!group.contains(card)) {
/*  85 */             if ((card.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2")))
/*     */             {
/*  87 */               card.upgrade();
/*  88 */             } else if ((card.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2")))
/*     */             {
/*  90 */               card.upgrade();
/*  91 */             } else if ((card.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2")))
/*     */             {
/*  93 */               card.upgrade();
/*     */             }
/*  95 */             group.addToBottom(card);
/*     */           } else {
/*  97 */             i--;
/*     */           }
/*     */         }
/*     */         
/* 101 */         for (AbstractCard c : group.group) {
/* 102 */           UnlockTracker.markCardAsSeen(c.cardID);
/*     */         }
/* 104 */         AbstractDungeon.gridSelectScreen.open(group, 1, OPTIONS[4], false);
/* 105 */         break;
/*     */       default: 
/* 107 */         this.imageEventText.updateBodyText(SLEEP_RESULT);
/* 108 */         AbstractDungeon.player.heal(this.healAmt, true);
/* 109 */         logMetric("Heal");
/* 110 */         this.screenNum = 1;
/* 111 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/* 112 */         this.imageEventText.clearRemainingOptions(); }
/* 113 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     default: 
/* 118 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private String getBook()
/*     */   {
/* 124 */     ArrayList<String> list = new ArrayList();
/* 125 */     list.add(DESCRIPTIONS[2]);
/* 126 */     list.add(DESCRIPTIONS[3]);
/* 127 */     list.add(DESCRIPTIONS[4]);
/* 128 */     return (String)list.get(MathUtils.random(2));
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 132 */     AbstractEvent.logMetric("The Library", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\TheLibrary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */