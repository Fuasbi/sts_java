/*    */ package com.megacrit.cardcrawl.events.city;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class Addict extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Addict";
/* 17 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Addict");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/*    */   private static final int GOLD_COST = 85;
/* 23 */   private int screenNum = 0;
/*    */   
/*    */   public Addict() {
/* 26 */     super(NAME, DESCRIPTIONS[0], "images/events/addict.jpg");
/*    */     
/* 28 */     if (AbstractDungeon.player.gold >= 85) {
/* 29 */       this.imageEventText.setDialogOption(OPTIONS[0] + 85 + OPTIONS[1], AbstractDungeon.player.gold < 85);
/*    */     }
/*    */     else
/*    */     {
/* 33 */       this.imageEventText.setDialogOption(OPTIONS[2] + 85 + OPTIONS[3], AbstractDungeon.player.gold < 85);
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 38 */     this.imageEventText.setDialogOption(OPTIONS[4], CardLibrary.getCopy("Shame"));
/* 39 */     this.imageEventText.setDialogOption(OPTIONS[5]);
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 44 */     switch (this.screenNum) {
/*    */     case 0: 
/* 46 */       switch (buttonPressed) {
/*    */       case 0: 
/* 48 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/* 49 */         if (AbstractDungeon.player.gold >= 85) {
/* 50 */           logMetric("Obtained Relic");
/* 51 */           AbstractDungeon.player.loseGold(85);
/* 52 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.drawX, this.drawY, 
/*    */           
/*    */ 
/* 55 */             AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 56 */           this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 57 */           this.imageEventText.clearRemainingOptions();
/*    */         }
/*    */         break;
/*    */       case 1: 
/* 61 */         this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/* 62 */         logMetric("Stole Relic");
/* 63 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.drawX, this.drawY, 
/*    */         
/*    */ 
/* 66 */           AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 67 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*    */         
/* 69 */           CardLibrary.getCopy("Shame"), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*    */         
/*    */ 
/* 72 */         UnlockTracker.markCardAsSeen("Shame");
/* 73 */         this.imageEventText.updateDialogOption(0, OPTIONS[5]);
/* 74 */         this.imageEventText.clearRemainingOptions();
/*    */         
/* 76 */         break;
/*    */       default: 
/* 78 */         openMap();
/*    */       }
/*    */       
/* 81 */       this.screenNum = 1;
/* 82 */       break;
/*    */     case 1: 
/* 84 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 90 */     AbstractEvent.logMetric("Addict", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\Addict.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */