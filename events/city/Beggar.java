/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Beggar extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Beggar";
/*  14 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Beggar");
/*  15 */   public static final String NAME = eventStrings.NAME;
/*  16 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  17 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   private CurScreen screen;
/*     */   private static final int GOLD_COST = 75;
/*  21 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  22 */   private static final String CANCEL_DIALOG = DESCRIPTIONS[1];
/*  23 */   private static final String PURGE_DIALOG = DESCRIPTIONS[2];
/*  24 */   private static final String POST_PURGE_DIALOG = DESCRIPTIONS[3];
/*     */   
/*     */   public static enum CurScreen {
/*  27 */     INTRO,  LEAVE,  GAVE_MONEY;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  31 */   public Beggar() { super(NAME, DIALOG_1, "images/events/beggar.jpg");
/*     */     
/*  33 */     if (AbstractDungeon.player.gold >= 75) {
/*  34 */       this.imageEventText.setDialogOption(OPTIONS[0] + 75 + OPTIONS[1], AbstractDungeon.player.gold < 75);
/*     */     }
/*     */     else
/*     */     {
/*  38 */       this.imageEventText.setDialogOption(OPTIONS[2] + 75 + OPTIONS[3], AbstractDungeon.player.gold < 75);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  43 */     this.imageEventText.setDialogOption(OPTIONS[5]);
/*  44 */     this.hasDialog = true;
/*  45 */     this.hasFocus = true;
/*  46 */     this.screen = CurScreen.INTRO;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  51 */     super.update();
/*     */     
/*     */ 
/*  54 */     if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  55 */       com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CARD_EXHAUST");
/*  56 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/*     */       
/*  58 */         (com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), com.megacrit.cardcrawl.core.Settings.WIDTH / 2, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2));
/*     */       
/*     */ 
/*  61 */       AbstractDungeon.player.masterDeck.removeCard((com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*  62 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*     */       
/*  64 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  70 */     switch (this.screen) {
/*     */     case INTRO: 
/*  72 */       if (buttonPressed == 0) {
/*  73 */         this.imageEventText.loadImage("images/events/cleric.jpg");
/*  74 */         this.imageEventText.updateBodyText(PURGE_DIALOG);
/*  75 */         AbstractDungeon.player.loseGold(75);
/*  76 */         this.imageEventText.clearAllDialogs();
/*  77 */         this.imageEventText.setDialogOption(OPTIONS[4]);
/*  78 */         logMetric("Gave Gold");
/*  79 */         this.screen = CurScreen.GAVE_MONEY;
/*     */       } else {
/*  81 */         this.imageEventText.updateBodyText(CANCEL_DIALOG);
/*  82 */         this.imageEventText.clearAllDialogs();
/*  83 */         this.imageEventText.setDialogOption(OPTIONS[5]);
/*  84 */         this.screen = CurScreen.LEAVE;
/*  85 */         logMetric("Ignored");
/*     */       }
/*  87 */       break;
/*     */     case GAVE_MONEY: 
/*  89 */       AbstractDungeon.gridSelectScreen.open(
/*  90 */         com.megacrit.cardcrawl.cards.CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck.getPurgeableCards()), 1, OPTIONS[6], false, false, false, true);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  97 */       this.imageEventText.updateBodyText(POST_PURGE_DIALOG);
/*  98 */       this.imageEventText.clearAllDialogs();
/*  99 */       this.imageEventText.setDialogOption(OPTIONS[5]);
/* 100 */       this.screen = CurScreen.LEAVE;
/* 101 */       break;
/*     */     case LEAVE: 
/* 103 */       openMap();
/* 104 */       break;
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 110 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Beggar", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\Beggar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */