/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ForgottenAltar extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Forgotten Altar";
/*  26 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Forgotten Altar");
/*  27 */   public static final String NAME = eventStrings.NAME;
/*  28 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  29 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  31 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  32 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  33 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*  34 */   private static final String DIALOG_4 = DESCRIPTIONS[3];
/*     */   private static final float HP_LOSS_PERCENT = 0.25F;
/*     */   private static final float A_2_HP_LOSS_PERCENT = 0.35F;
/*     */   private int hpLoss;
/*     */   private static final int MAX_HP_GAIN = 5;
/*     */   
/*     */   public ForgottenAltar() {
/*  41 */     super(NAME, DIALOG_1, "images/events/forgottenAltar.jpg");
/*     */     
/*  43 */     if (AbstractDungeon.player.hasRelic("Golden Idol")) {
/*  44 */       this.imageEventText.setDialogOption(OPTIONS[0], !AbstractDungeon.player.hasRelic("Golden Idol"));
/*     */     } else {
/*  46 */       this.imageEventText.setDialogOption(OPTIONS[1], !AbstractDungeon.player.hasRelic("Golden Idol"));
/*     */     }
/*     */     
/*  49 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  50 */       this.hpLoss = MathUtils.round(AbstractDungeon.player.maxHealth * 0.35F);
/*     */     } else {
/*  52 */       this.hpLoss = MathUtils.round(AbstractDungeon.player.maxHealth * 0.25F);
/*     */     }
/*     */     
/*  55 */     this.imageEventText.setDialogOption(OPTIONS[2] + 5 + OPTIONS[3] + this.hpLoss + OPTIONS[4]);
/*  56 */     this.imageEventText.setDialogOption(OPTIONS[6], CardLibrary.getCopy("Decay"));
/*     */   }
/*     */   
/*     */ 
/*     */   public void onEnterRoom()
/*     */   {
/*  62 */     if (Settings.AMBIANCE_ON) {
/*  63 */       CardCrawlGame.sound.play("EVENT_FORGOTTEN");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  69 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  72 */       switch (buttonPressed) {
/*     */       case 0: 
/*  74 */         gainChalice();
/*  75 */         showProceedScreen(DIALOG_2);
/*  76 */         CardCrawlGame.sound.play("HEAL_1");
/*  77 */         logMetric("Gave Idol");
/*  78 */         break;
/*     */       case 1: 
/*  80 */         AbstractDungeon.player.increaseMaxHp(5, false);
/*  81 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, this.hpLoss));
/*  82 */         CardCrawlGame.sound.play("HEAL_3");
/*  83 */         showProceedScreen(DIALOG_3);
/*  84 */         logMetric("Shed Blood");
/*  85 */         break;
/*     */       case 2: 
/*  87 */         CardCrawlGame.sound.play("BLUNT_HEAVY");
/*  88 */         CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.MED, true);
/*  89 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Decay(), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */         
/*  91 */         UnlockTracker.markCardAsSeen("Decay");
/*  92 */         showProceedScreen(DIALOG_4);
/*  93 */         logMetric("Smashed Altar");
/*     */       }
/*     */       
/*  96 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     default: 
/* 101 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void gainChalice()
/*     */   {
/* 107 */     int relicAtIndex = 0;
/* 108 */     for (int i = 0; i < AbstractDungeon.player.relics.size(); i++) {
/* 109 */       if (((AbstractRelic)AbstractDungeon.player.relics.get(i)).relicId.equals("Golden Idol")) {
/* 110 */         relicAtIndex = i;
/* 111 */         break;
/*     */       }
/*     */     }
/* 114 */     if (AbstractDungeon.player.hasRelic("Bloody Idol")) {
/* 115 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */       
/*     */ 
/* 118 */         RelicLibrary.getRelic("Circlet").makeCopy());
/*     */     } else {
/* 120 */       ((AbstractRelic)AbstractDungeon.player.relics.get(relicAtIndex)).onUnequip();
/* 121 */       AbstractRelic bloodyIdol = RelicLibrary.getRelic("Bloody Idol").makeCopy();
/* 122 */       bloodyIdol.instantObtain(AbstractDungeon.player, relicAtIndex, false);
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String result)
/*     */   {
/* 128 */     AbstractEvent.logMetric("Forgotten Altar", result);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\ForgottenAltar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */