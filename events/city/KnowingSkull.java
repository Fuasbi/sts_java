/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class KnowingSkull extends AbstractImageEvent
/*     */ {
/*  23 */   private static final Logger logger = LogManager.getLogger(KnowingSkull.class.getName());
/*     */   public static final String ID = "Knowing Skull";
/*  25 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Knowing Skull");
/*  26 */   public static final String NAME = eventStrings.NAME;
/*  27 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  28 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  30 */   private static final String INTRO_MSG = DESCRIPTIONS[0];
/*  31 */   private static final String INTRO_2_MSG = DESCRIPTIONS[1];
/*  32 */   private static final String ASK_AGAIN_MSG = DESCRIPTIONS[2];
/*  33 */   private static final String POTION_MSG = DESCRIPTIONS[4];
/*  34 */   private static final String CARD_MSG = DESCRIPTIONS[5];
/*  35 */   private static final String GOLD_MSG = DESCRIPTIONS[6];
/*  36 */   private static final String LEAVE_MSG = DESCRIPTIONS[7];
/*     */   
/*     */   private int hpCost;
/*     */   
/*     */   private static final int GOLD_REWARD = 90;
/*  41 */   private CurScreen screen = CurScreen.INTRO_1;
/*  42 */   private String optionsChosen = "";
/*     */   
/*     */ 
/*  45 */   private ArrayList<Reward> options = new ArrayList();
/*     */   
/*     */   private static enum CurScreen {
/*  48 */     INTRO_1,  ASK,  COMPLETE;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  52 */   private static enum Reward { POTION,  LEAVE,  GOLD,  CARD;
/*     */     
/*     */     private Reward() {}
/*     */   }
/*     */   
/*     */   public KnowingSkull()
/*     */   {
/*  59 */     super(NAME, INTRO_MSG, "images/events/knowingSkull.jpg");
/*  60 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  61 */     this.options.add(Reward.CARD);
/*  62 */     this.options.add(Reward.GOLD);
/*  63 */     this.options.add(Reward.POTION);
/*  64 */     this.options.add(Reward.LEAVE);
/*     */     
/*  66 */     this.hpCost = (AbstractDungeon.player.maxHealth / 10);
/*  67 */     if (this.hpCost <= 6) {
/*  68 */       this.hpCost = 6;
/*     */     }
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  74 */     if (Settings.AMBIANCE_ON) {
/*  75 */       CardCrawlGame.sound.play("EVENT_SKULL");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  81 */     switch (this.screen) {
/*     */     case INTRO_1: 
/*  83 */       this.imageEventText.updateBodyText(INTRO_2_MSG);
/*  84 */       this.imageEventText.clearAllDialogs();
/*  85 */       this.imageEventText.setDialogOption(OPTIONS[4] + this.hpCost + OPTIONS[1]);
/*  86 */       this.imageEventText.setDialogOption(OPTIONS[5] + 90 + OPTIONS[6] + this.hpCost + OPTIONS[1]);
/*  87 */       this.imageEventText.setDialogOption(OPTIONS[3] + this.hpCost + OPTIONS[1]);
/*  88 */       this.imageEventText.setDialogOption(OPTIONS[7] + this.hpCost + OPTIONS[1]);
/*  89 */       this.screen = CurScreen.ASK;
/*  90 */       break;
/*     */     case ASK: 
/*  92 */       AbstractDungeon.player.damage(new DamageInfo(null, this.hpCost, DamageInfo.DamageType.HP_LOSS));
/*  93 */       CardCrawlGame.sound.play("DEBUFF_2");
/*  94 */       switch (buttonPressed) {
/*     */       case 0: 
/*  96 */         obtainReward(0);
/*  97 */         break;
/*     */       case 1: 
/*  99 */         obtainReward(1);
/* 100 */         break;
/*     */       case 2: 
/* 102 */         obtainReward(2);
/* 103 */         break;
/*     */       default: 
/* 105 */         setLeave(); }
/* 106 */       break;
/*     */     
/*     */ 
/*     */     case COMPLETE: 
/* 110 */       logMetric(this.optionsChosen);
/* 111 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void obtainReward(int slot)
/*     */   {
/* 117 */     switch (slot) {
/*     */     case 0: 
/* 119 */       this.optionsChosen += "POTION ";
/* 120 */       this.imageEventText.updateBodyText(POTION_MSG + ASK_AGAIN_MSG);
/* 121 */       AbstractDungeon.player.obtainPotion(com.megacrit.cardcrawl.helpers.PotionHelper.getRandomPotion());
/* 122 */       break;
/*     */     case 1: 
/* 124 */       this.optionsChosen += "GOLD ";
/* 125 */       this.imageEventText.updateBodyText(GOLD_MSG + ASK_AGAIN_MSG);
/* 126 */       AbstractDungeon.effectList.add(new RainingGoldEffect(90));
/* 127 */       AbstractDungeon.player.gainGold(90);
/* 128 */       break;
/*     */     case 2: 
/* 130 */       this.optionsChosen += "CARD ";
/* 131 */       this.imageEventText.updateBodyText(CARD_MSG + ASK_AGAIN_MSG);
/* 132 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*     */       
/* 134 */         AbstractDungeon.returnColorlessCard(AbstractCard.CardRarity.UNCOMMON).makeCopy(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/*     */ 
/* 137 */       break;
/*     */     default: 
/* 139 */       logger.info("This should never happen.");
/*     */     }
/*     */   }
/*     */   
/*     */   private void setLeave()
/*     */   {
/* 145 */     this.imageEventText.updateBodyText(LEAVE_MSG);
/* 146 */     this.imageEventText.clearAllDialogs();
/* 147 */     this.imageEventText.setDialogOption(OPTIONS[8]);
/* 148 */     this.screen = CurScreen.COMPLETE;
/*     */   }
/*     */   
/*     */   public void logMetric(String msg) {
/* 152 */     AbstractEvent.logMetric("Knowing Skull", msg);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\KnowingSkull.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */