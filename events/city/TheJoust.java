/*     */ package com.megacrit.cardcrawl.events.city;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ 
/*     */ public class TheJoust extends com.megacrit.cardcrawl.events.AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "The Joust";
/*  15 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("The Joust");
/*  16 */   public static final String NAME = eventStrings.NAME;
/*  17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  20 */   private static final String HALT_MSG = DESCRIPTIONS[0];
/*  21 */   private static final String EXPL_MSG = DESCRIPTIONS[1];
/*  22 */   private static final String BET_AGAINST = DESCRIPTIONS[2];
/*  23 */   private static final String BET_FOR = DESCRIPTIONS[3];
/*  24 */   private static final String COMBAT_MSG = DESCRIPTIONS[4];
/*  25 */   private static final String NOODLES_WIN = DESCRIPTIONS[5];
/*  26 */   private static final String NOODLES_LOSE = DESCRIPTIONS[6];
/*  27 */   private static final String BET_WON_MSG = DESCRIPTIONS[7];
/*  28 */   private static final String BET_LOSE_MSG = DESCRIPTIONS[8];
/*     */   private boolean betFor;
/*     */   private boolean ownerWins;
/*     */   private static final int WIN_OWNER = 250;
/*  32 */   private static final int WIN_MURDERER = 100; private static final int BET_AMT = 50; private CUR_SCREEN screen = CUR_SCREEN.HALT;
/*     */   
/*     */ 
/*  35 */   private float joustTimer = 0.0F;
/*  36 */   private int clangCount = 0;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  39 */     HALT,  EXPLANATION,  PRE_JOUST,  JOUST,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  43 */   public TheJoust() { super(NAME, HALT_MSG, "images/events/joust.jpg");
/*  44 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  49 */     super.update();
/*  50 */     if (this.joustTimer != 0.0F) {
/*  51 */       this.joustTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  52 */       if (this.joustTimer < 0.0F) {
/*  53 */         this.clangCount += 1;
/*  54 */         if (this.clangCount == 1) {
/*  55 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.MED, false);
/*  56 */           CardCrawlGame.sound.play("BLUNT_HEAVY");
/*  57 */           this.joustTimer = 1.0F;
/*  58 */         } else if (this.clangCount == 2) {
/*  59 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/*  60 */           CardCrawlGame.sound.play("BLUNT_FAST");
/*  61 */           this.joustTimer = 0.25F;
/*  62 */         } else if (this.clangCount == 3) {
/*  63 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.LONG, false);
/*  64 */           CardCrawlGame.sound.play("BLUNT_HEAVY");
/*  65 */           this.joustTimer = 0.0F;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  73 */     switch (this.screen) {
/*     */     case HALT: 
/*  75 */       this.imageEventText.updateBodyText(EXPL_MSG);
/*  76 */       this.imageEventText.updateDialogOption(0, OPTIONS[1] + 50 + OPTIONS[2] + 100 + OPTIONS[3]);
/*  77 */       this.imageEventText.setDialogOption(OPTIONS[4] + 50 + OPTIONS[5] + 250 + OPTIONS[3]);
/*  78 */       this.screen = CUR_SCREEN.EXPLANATION;
/*  79 */       break;
/*     */     case EXPLANATION: 
/*  81 */       if (buttonPressed == 0) {
/*  82 */         this.betFor = false;
/*  83 */         this.imageEventText.updateBodyText(BET_AGAINST);
/*     */       } else {
/*  85 */         this.betFor = true;
/*  86 */         this.imageEventText.updateBodyText(BET_FOR);
/*     */       }
/*  88 */       AbstractDungeon.player.loseGold(50);
/*  89 */       this.imageEventText.updateDialogOption(0, OPTIONS[6]);
/*  90 */       this.imageEventText.clearRemainingOptions();
/*  91 */       this.screen = CUR_SCREEN.PRE_JOUST;
/*  92 */       break;
/*     */     case PRE_JOUST: 
/*  94 */       this.imageEventText.updateBodyText(COMBAT_MSG);
/*  95 */       this.imageEventText.updateDialogOption(0, OPTIONS[6]);
/*  96 */       this.ownerWins = AbstractDungeon.eventRng.randomBoolean(0.3F);
/*  97 */       this.screen = CUR_SCREEN.JOUST;
/*  98 */       this.joustTimer = 0.01F;
/*  99 */       break;
/*     */     case JOUST: 
/* 101 */       this.imageEventText.updateDialogOption(0, OPTIONS[7]);
/*     */       
/*     */       String tmp;
/* 104 */       if (this.ownerWins) {
/* 105 */         String tmp = NOODLES_WIN;
/* 106 */         if (this.betFor) {
/* 107 */           tmp = tmp + BET_WON_MSG;
/* 108 */           AbstractDungeon.player.gainGold(250);
/* 109 */           CardCrawlGame.sound.play("GOLD_GAIN");
/* 110 */           logMetric("Bet on Owner");
/*     */         } else {
/* 112 */           tmp = tmp + BET_LOSE_MSG;
/* 113 */           logMetric("Bet on Owner");
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 118 */         tmp = NOODLES_LOSE;
/* 119 */         if (this.betFor) {
/* 120 */           tmp = tmp + BET_LOSE_MSG;
/* 121 */           logMetric("Bet on Murderer");
/*     */         } else {
/* 123 */           tmp = tmp + BET_WON_MSG;
/* 124 */           AbstractDungeon.player.gainGold(100);
/* 125 */           CardCrawlGame.sound.play("GOLD_GAIN");
/* 126 */           logMetric("Bet on Murderer");
/*     */         }
/*     */       }
/* 129 */       this.imageEventText.updateBodyText(tmp);
/* 130 */       this.screen = CUR_SCREEN.COMPLETE;
/* 131 */       break;
/*     */     case COMPLETE: 
/* 133 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 139 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("The Joust", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\city\TheJoust.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */