/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SensoryStone extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "SensoryStone";
/*  19 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("SensoryStone");
/*  20 */   public static final String NAME = eventStrings.NAME;
/*  21 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  22 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  24 */   private static final String INTRO_TEXT = DESCRIPTIONS[0];
/*  25 */   private static final String INTRO_TEXT_2 = DESCRIPTIONS[1];
/*  26 */   private static final String MEMORY_1_TEXT = DESCRIPTIONS[2];
/*  27 */   private static final String MEMORY_2_TEXT = DESCRIPTIONS[3];
/*  28 */   private static final String MEMORY_3_TEXT = DESCRIPTIONS[4];
/*     */   
/*  30 */   private CurScreen screen = CurScreen.INTRO;
/*     */   private int choice;
/*     */   
/*     */   private static enum CurScreen {
/*  34 */     INTRO,  INTRO_2,  ACCEPT,  LEAVE;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  38 */   public SensoryStone() { super(NAME, INTRO_TEXT, "images/events/sensoryStone.jpg");
/*  39 */     this.noCardsInRewards = true;
/*  40 */     this.imageEventText.setDialogOption(OPTIONS[5]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  45 */     switch (this.screen) {
/*     */     case INTRO: 
/*  47 */       this.imageEventText.updateBodyText(INTRO_TEXT_2);
/*  48 */       this.imageEventText.updateDialogOption(0, OPTIONS[0]);
/*  49 */       this.imageEventText.setDialogOption(OPTIONS[1] + 5 + OPTIONS[3]);
/*  50 */       this.imageEventText.setDialogOption(OPTIONS[2] + 10 + OPTIONS[3]);
/*  51 */       this.screen = CurScreen.INTRO_2;
/*  52 */       break;
/*     */     case INTRO_2: 
/*  54 */       getRandomMemory();
/*  55 */       switch (buttonPressed) {
/*     */       case 0: 
/*  57 */         this.screen = CurScreen.ACCEPT;
/*  58 */         logMetric("Memory 1");
/*  59 */         this.choice = 1;
/*  60 */         reward(this.choice);
/*  61 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  62 */         break;
/*     */       case 1: 
/*  64 */         this.screen = CurScreen.ACCEPT;
/*  65 */         logMetric("Memory 2");
/*  66 */         this.choice = 2;
/*  67 */         reward(this.choice);
/*  68 */         AbstractDungeon.player.damage(new DamageInfo(null, 5, DamageInfo.DamageType.HP_LOSS));
/*  69 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  70 */         break;
/*     */       case 2: 
/*  72 */         this.screen = CurScreen.ACCEPT;
/*  73 */         logMetric("Memory 3");
/*  74 */         this.choice = 3;
/*  75 */         reward(this.choice);
/*  76 */         AbstractDungeon.player.damage(new DamageInfo(null, 10, DamageInfo.DamageType.HP_LOSS));
/*  77 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*     */       }
/*     */       
/*     */       
/*  81 */       this.imageEventText.clearRemainingOptions();
/*  82 */       break;
/*     */     case ACCEPT: 
/*  84 */       reward(this.choice);
/*     */     default: 
/*  86 */       openMap();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void getRandomMemory()
/*     */   {
/*  95 */     ArrayList<String> memories = new ArrayList();
/*  96 */     memories.add(MEMORY_1_TEXT);
/*  97 */     memories.add(MEMORY_2_TEXT);
/*  98 */     memories.add(MEMORY_3_TEXT);
/*  99 */     java.util.Collections.shuffle(memories, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/* 100 */     this.imageEventText.updateBodyText((String)memories.get(0));
/*     */   }
/*     */   
/*     */   private void reward(int num) {
/* 104 */     AbstractDungeon.getCurrRoom().rewards.clear();
/* 105 */     for (int i = 0; i < num; i++) {
/* 106 */       AbstractDungeon.getCurrRoom().addCardReward(new com.megacrit.cardcrawl.rewards.RewardItem(com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS));
/*     */     }
/* 108 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 109 */     AbstractDungeon.combatRewardScreen.open();
/* 110 */     this.screen = CurScreen.LEAVE;
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 114 */     AbstractEvent.logMetric("SensoryStone", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\SensoryStone.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */