/*    */ package com.megacrit.cardcrawl.events.beyond;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class MysteriousSphere extends AbstractEvent
/*    */ {
/*    */   public static final String ID = "Mysterious Sphere";
/* 15 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Mysterious Sphere");
/* 16 */   public static final String NAME = eventStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 20 */   private static final String INTRO_MSG = DESCRIPTIONS[0];
/* 21 */   private CurScreen screen = CurScreen.INTRO;
/*    */   
/*    */   private static enum CurScreen {
/* 24 */     INTRO,  PRE_COMBAT,  END;
/*    */     
/*    */     private CurScreen() {}
/*    */   }
/*    */   
/* 29 */   public MysteriousSphere() { initializeImage("images/events/sphereClosed.png", 1120.0F * Settings.scale, AbstractDungeon.floorY - 50.0F * Settings.scale);
/*    */     
/*    */ 
/*    */ 
/* 33 */     this.body = INTRO_MSG;
/* 34 */     this.roomEventText.addDialogOption(OPTIONS[0]);
/* 35 */     this.roomEventText.addDialogOption(OPTIONS[1]);
/*    */     
/* 37 */     this.hasDialog = true;
/* 38 */     this.hasFocus = true;
/* 39 */     AbstractDungeon.getCurrRoom().monsters = com.megacrit.cardcrawl.helpers.MonsterHelper.getEncounter("Mysterious Sphere");
/*    */   }
/*    */   
/*    */   public void update() {
/* 43 */     super.update();
/*    */     
/* 45 */     if (!RoomEventDialog.waitForInput) {
/* 46 */       buttonEffect(this.roomEventText.getSelectedOption());
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 52 */     switch (this.screen) {
/*    */     case INTRO: 
/* 54 */       switch (buttonPressed) {
/*    */       case 0: 
/* 56 */         this.screen = CurScreen.PRE_COMBAT;
/* 57 */         this.roomEventText.updateBodyText(DESCRIPTIONS[1]);
/* 58 */         this.roomEventText.updateDialogOption(0, OPTIONS[2]);
/* 59 */         this.roomEventText.clearRemainingOptions();
/* 60 */         logMetric("Fight");
/* 61 */         return;
/*    */       case 1: 
/* 63 */         this.screen = CurScreen.END;
/* 64 */         this.roomEventText.updateBodyText(DESCRIPTIONS[2]);
/* 65 */         this.roomEventText.updateDialogOption(0, OPTIONS[1]);
/* 66 */         this.roomEventText.clearRemainingOptions();
/* 67 */         logMetric("Ignore");
/* 68 */         return;
/*    */       }
/* 70 */       break;
/*    */     case PRE_COMBAT: 
/* 72 */       if (Settings.isDailyRun) {
/* 73 */         AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(50));
/*    */       } else {
/* 75 */         AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(45, 55));
/*    */       }
/* 77 */       AbstractDungeon.getCurrRoom().addRelicToRewards(
/* 78 */         AbstractDungeon.returnRandomScreenlessRelic(com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier.RARE));
/* 79 */       this.img = ImageMaster.loadImage("images/events/sphereOpen.png");
/*    */       
/* 81 */       enterCombat();
/* 82 */       AbstractDungeon.lastCombatMetricKey = "Mysterious Sphere";
/* 83 */       break;
/*    */     case END: 
/* 85 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 91 */     AbstractEvent.logMetric("Mysterious Sphere", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\MysteriousSphere.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */