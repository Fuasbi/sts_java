/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.curses.Doubt;
/*     */ import com.megacrit.cardcrawl.cards.curses.Normality;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*     */ import com.megacrit.cardcrawl.vfx.UpgradeShineEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class MindBloom extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "MindBloom";
/*  29 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("MindBloom");
/*  30 */   public static final String NAME = eventStrings.NAME;
/*  31 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  32 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  34 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  35 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  36 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*     */   
/*  38 */   private CurScreen screen = CurScreen.INTRO;
/*     */   
/*     */   private static enum CurScreen {
/*  41 */     INTRO,  FIGHT,  LEAVE;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  45 */   public MindBloom() { super(NAME, DIALOG_1, "images/events/mindBloom.jpg");
/*     */     
/*     */ 
/*  48 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */     
/*     */ 
/*  51 */     this.imageEventText.setDialogOption(OPTIONS[3]);
/*     */     
/*     */ 
/*  54 */     if (AbstractDungeon.floorNum % 50 <= 40) {
/*  55 */       this.imageEventText.setDialogOption(OPTIONS[1]);
/*     */     } else {
/*  57 */       this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  63 */     switch (this.screen) {
/*     */     case INTRO: 
/*  65 */       switch (buttonPressed) {
/*     */       case 0: 
/*  67 */         this.imageEventText.updateBodyText(DIALOG_2);
/*  68 */         this.screen = CurScreen.FIGHT;
/*  69 */         logMetric("Fight");
/*     */         
/*  71 */         ArrayList<String> list = new ArrayList();
/*  72 */         list.add("The Guardian");
/*  73 */         list.add("Hexaghost");
/*  74 */         list.add("Slime Boss");
/*  75 */         java.util.Collections.shuffle(list, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*  76 */         AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter((String)list.get(0));
/*  77 */         AbstractDungeon.getCurrRoom().rewards.clear();
/*  78 */         AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.RARE);
/*  79 */         enterCombatFromImage();
/*  80 */         AbstractDungeon.lastCombatMetricKey = "Mind Bloom Boss Battle";
/*  81 */         break;
/*     */       case 1: 
/*  83 */         this.imageEventText.updateBodyText(DIALOG_3);
/*  84 */         this.screen = CurScreen.LEAVE;
/*  85 */         int effectCount = 0;
/*  86 */         for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  87 */           if (c.canUpgrade()) {
/*  88 */             effectCount++;
/*  89 */             if (effectCount <= 20) {
/*  90 */               float x = MathUtils.random(0.1F, 0.9F) * Settings.WIDTH;
/*  91 */               float y = MathUtils.random(0.2F, 0.8F) * Settings.HEIGHT;
/*     */               
/*  93 */               AbstractDungeon.effectList.add(new ShowCardBrieflyEffect(c
/*  94 */                 .makeStatEquivalentCopy(), x, y));
/*  95 */               AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(x, y));
/*     */             }
/*  97 */             c.upgrade();
/*     */           }
/*     */         }
/* 100 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 
/*     */         
/*     */ 
/* 103 */           RelicLibrary.getRelic("Mark of the Bloom").makeCopy());
/* 104 */         logMetric("Upgrade");
/* 105 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/* 106 */         break;
/*     */       case 2: 
/* 108 */         if (AbstractDungeon.floorNum % 50 <= 40) {
/* 109 */           this.imageEventText.updateBodyText(DIALOG_2);
/* 110 */           this.screen = CurScreen.LEAVE;
/* 111 */           logMetric("Gold");
/* 112 */           AbstractDungeon.effectList.add(new RainingGoldEffect(999));
/* 113 */           AbstractDungeon.player.gainGold(999);
/* 114 */           AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new Normality(), Settings.WIDTH * 0.6F, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/* 119 */           AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new Normality(), Settings.WIDTH * 0.3F, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/* 124 */           UnlockTracker.markCardAsSeen("Normality");
/* 125 */           this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*     */         }
/*     */         else {
/* 128 */           this.imageEventText.updateBodyText(DIALOG_2);
/* 129 */           this.screen = CurScreen.LEAVE;
/* 130 */           logMetric("Heal");
/* 131 */           AbstractDungeon.player.heal(AbstractDungeon.player.maxHealth);
/* 132 */           AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new Doubt(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */           
/* 134 */           UnlockTracker.markCardAsSeen("Doubt");
/* 135 */           this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*     */         }
/* 137 */         break;
/*     */       }
/*     */       
/*     */       
/*     */ 
/* 142 */       this.imageEventText.clearRemainingOptions();
/* 143 */       break;
/*     */     case LEAVE: 
/* 145 */       openMap();
/* 146 */       break;
/*     */     default: 
/* 148 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 154 */     AbstractEvent.logMetric("MindBloom", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\MindBloom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */