/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ 
/*     */ public class MoaiHead extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "The Moai Head";
/*  17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("The Moai Head");
/*  18 */   public static final String NAME = eventStrings.NAME;
/*  19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   private static final float HP_LOSS_PERCENT = 0.125F;
/*     */   private static final float A_2_HP_LOSS_PERCENT = 0.18F;
/*  24 */   private int hpAmt = 0;
/*     */   
/*     */   private static final int goldAmount = 333;
/*     */   
/*  28 */   private static final String INTRO_BODY = DESCRIPTIONS[0];
/*  29 */   private int screenNum = 0;
/*     */   
/*     */   public MoaiHead() {
/*  32 */     super(NAME, INTRO_BODY, "images/events/moaiHead.jpg");
/*     */     
/*  34 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  35 */       this.hpAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.18F);
/*     */     } else {
/*  37 */       this.hpAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.125F);
/*     */     }
/*     */     
/*  40 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.hpAmt + OPTIONS[1]);
/*  41 */     if (AbstractDungeon.player.hasRelic("Golden Idol")) {
/*  42 */       this.imageEventText.setDialogOption(OPTIONS[2], !AbstractDungeon.player.hasRelic("Golden Idol"));
/*     */     } else {
/*  44 */       this.imageEventText.setDialogOption(OPTIONS[3], !AbstractDungeon.player.hasRelic("Golden Idol"));
/*     */     }
/*  46 */     this.imageEventText.setDialogOption(OPTIONS[4]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  51 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  54 */       switch (buttonPressed)
/*     */       {
/*     */ 
/*     */       case 0: 
/*  58 */         logMetric("Heal");
/*  59 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/*     */         
/*     */ 
/*  62 */         CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.MED, true);
/*  63 */         CardCrawlGame.sound.play("BLUNT_HEAVY");
/*  64 */         AbstractDungeon.player.maxHealth -= this.hpAmt;
/*  65 */         if (AbstractDungeon.player.currentHealth > AbstractDungeon.player.maxHealth) {
/*  66 */           AbstractDungeon.player.currentHealth = AbstractDungeon.player.maxHealth;
/*     */         }
/*  68 */         if (AbstractDungeon.player.maxHealth < 1) {
/*  69 */           AbstractDungeon.player.maxHealth = 1;
/*     */         }
/*  71 */         AbstractDungeon.player.heal(AbstractDungeon.player.maxHealth);
/*     */         
/*  73 */         this.screenNum = 1;
/*  74 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  75 */         this.imageEventText.clearRemainingOptions();
/*  76 */         break;
/*     */       
/*     */       case 1: 
/*  79 */         logMetric("Gave Idol");
/*  80 */         this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/*  81 */         this.screenNum = 1;
/*  82 */         AbstractDungeon.player.loseRelic("Golden Idol");
/*  83 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.RainingGoldEffect(333));
/*  84 */         AbstractDungeon.player.gainGold(333);
/*  85 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  86 */         this.imageEventText.clearRemainingOptions();
/*  87 */         break;
/*     */       
/*     */       default: 
/*  90 */         logMetric("Ignored");
/*  91 */         this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/*  92 */         this.screenNum = 1;
/*  93 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  94 */         this.imageEventText.clearRemainingOptions(); }
/*  95 */       break;
/*     */     
/*     */ 
/*     */     default: 
/*  99 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 105 */     AbstractEvent.logMetric("The Moai Head", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\MoaiHead.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */