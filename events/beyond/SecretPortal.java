/*    */ package com.megacrit.cardcrawl.events.beyond;
/*    */ 
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*    */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SecretPortal extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "SecretPortal";
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("SecretPortal");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 24 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*    */   
/* 26 */   private CurScreen screen = CurScreen.INTRO;
/*    */   
/*    */   private static enum CurScreen {
/* 29 */     INTRO,  ACCEPT,  LEAVE;
/*    */     
/*    */     private CurScreen() {} }
/*    */   
/* 33 */   public SecretPortal() { super(NAME, DIALOG_1, "images/events/secretPortal.jpg");
/* 34 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/* 35 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 40 */     if (com.megacrit.cardcrawl.core.Settings.AMBIANCE_ON) {
/* 41 */       CardCrawlGame.sound.play("EVENT_PORTAL");
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 47 */     switch (this.screen) {
/*    */     case INTRO: 
/* 49 */       switch (buttonPressed) {
/*    */       case 0: 
/* 51 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 52 */         this.screen = CurScreen.ACCEPT;
/* 53 */         logMetric("Took Portal.");
/* 54 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 55 */         CardCrawlGame.screenShake.mildRumble(5.0F);
/* 56 */         CardCrawlGame.sound.play("ATTACK_MAGIC_SLOW_2");
/* 57 */         break;
/*    */       case 1: 
/* 59 */         this.imageEventText.updateBodyText(DIALOG_3);
/* 60 */         this.screen = CurScreen.LEAVE;
/* 61 */         logMetric("Rejected Portal.");
/* 62 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/*    */       }
/*    */       
/*    */       
/*    */ 
/* 67 */       this.imageEventText.clearRemainingOptions();
/* 68 */       break;
/*    */     
/*    */     case ACCEPT: 
/* 71 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/* 72 */       MapRoomNode node = new MapRoomNode(-1, 15);
/* 73 */       node.room = new MonsterRoomBoss();
/* 74 */       AbstractDungeon.nextRoom = node;
/* 75 */       CardCrawlGame.music.fadeOutTempBGM();
/* 76 */       AbstractDungeon.pathX.add(Integer.valueOf(1));
/* 77 */       AbstractDungeon.pathY.add(Integer.valueOf(15));
/* 78 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.FadeWipeParticle());
/* 79 */       AbstractDungeon.nextRoomTransitionStart();
/* 80 */       break;
/*    */     default: 
/* 82 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 88 */     AbstractEvent.logMetric("SecretPortal", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\SecretPortal.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */