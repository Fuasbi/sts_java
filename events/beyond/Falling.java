/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Falling extends com.megacrit.cardcrawl.events.AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Falling";
/*  17 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Falling");
/*  18 */   public static final String NAME = eventStrings.NAME;
/*  19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  23 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*     */   
/*  25 */   private CurScreen screen = CurScreen.INTRO;
/*     */   private boolean attack;
/*     */   private boolean skill;
/*     */   private boolean power;
/*     */   
/*  30 */   private static enum CurScreen { INTRO,  CHOICE,  RESULT;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  34 */   public Falling() { super(NAME, DIALOG_1, "images/events/falling.jpg");
/*  35 */     setCards();
/*  36 */     this.imageEventText.setDialogOption(OPTIONS[0]); }
/*     */   
/*     */   private AbstractCard attackCard;
/*     */   private AbstractCard skillCard;
/*     */   private AbstractCard powerCard;
/*  41 */   public void onEnterRoom() { if (com.megacrit.cardcrawl.core.Settings.AMBIANCE_ON) {
/*  42 */       com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("EVENT_FALLING");
/*     */     }
/*     */   }
/*     */   
/*     */   private void setCards() {
/*  47 */     this.attack = CardHelper.hasCardWithType(AbstractCard.CardType.ATTACK);
/*  48 */     this.skill = CardHelper.hasCardWithType(AbstractCard.CardType.SKILL);
/*  49 */     this.power = CardHelper.hasCardWithType(AbstractCard.CardType.POWER);
/*     */     
/*  51 */     if (this.attack) {
/*  52 */       this.attackCard = CardHelper.returnCardOfType(AbstractCard.CardType.ATTACK);
/*     */     }
/*  54 */     if (this.skill) {
/*  55 */       this.skillCard = CardHelper.returnCardOfType(AbstractCard.CardType.SKILL);
/*     */     }
/*  57 */     if (this.power) {
/*  58 */       this.powerCard = CardHelper.returnCardOfType(AbstractCard.CardType.POWER);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  64 */     switch (this.screen) {
/*     */     case INTRO: 
/*  66 */       this.screen = CurScreen.CHOICE;
/*  67 */       this.imageEventText.updateBodyText(DIALOG_2);
/*     */       
/*  69 */       this.imageEventText.clearAllDialogs();
/*     */       
/*  71 */       if ((!this.skill) && (!this.power) && (!this.attack)) {
/*  72 */         this.imageEventText.setDialogOption(OPTIONS[8]);
/*     */       } else {
/*  74 */         if (this.skill) {
/*  75 */           this.imageEventText.setDialogOption(OPTIONS[1] + 
/*  76 */             FontHelper.colorString(this.skillCard.name, "r"), this.skillCard
/*  77 */             .makeStatEquivalentCopy());
/*     */         } else {
/*  79 */           this.imageEventText.setDialogOption(OPTIONS[2], true);
/*     */         }
/*  81 */         if (this.power) {
/*  82 */           this.imageEventText.setDialogOption(OPTIONS[3] + 
/*  83 */             FontHelper.colorString(this.powerCard.name, "r"), this.powerCard
/*  84 */             .makeStatEquivalentCopy());
/*     */         } else {
/*  86 */           this.imageEventText.setDialogOption(OPTIONS[4], true);
/*     */         }
/*  88 */         if (this.attack) {
/*  89 */           this.imageEventText.setDialogOption(OPTIONS[5] + 
/*  90 */             FontHelper.colorString(this.attackCard.name, "r"), this.attackCard
/*  91 */             .makeStatEquivalentCopy());
/*     */         } else {
/*  93 */           this.imageEventText.setDialogOption(OPTIONS[6], true);
/*     */         }
/*     */       }
/*  96 */       break;
/*     */     case CHOICE: 
/*  98 */       this.screen = CurScreen.RESULT;
/*  99 */       this.imageEventText.clearAllDialogs();
/* 100 */       this.imageEventText.setDialogOption(OPTIONS[7]);
/* 101 */       switch (buttonPressed) {
/*     */       case 0: 
/* 103 */         if ((!this.skill) && (!this.power) && (!this.attack)) {
/* 104 */           this.imageEventText.updateBodyText(DESCRIPTIONS[5]);
/* 105 */           logMetric("Had nothing to remove!");
/*     */         } else {
/* 107 */           this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/* 108 */           AbstractDungeon.effectList.add(new PurgeCardEffect(this.skillCard));
/* 109 */           AbstractDungeon.player.masterDeck.removeCard(this.skillCard);
/* 110 */           logMetric("Removed Skill");
/*     */         }
/* 112 */         break;
/*     */       case 1: 
/* 114 */         this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/* 115 */         AbstractDungeon.effectList.add(new PurgeCardEffect(this.powerCard));
/* 116 */         AbstractDungeon.player.masterDeck.removeCard(this.powerCard);
/* 117 */         logMetric("Removed Power");
/* 118 */         break;
/*     */       case 2: 
/* 120 */         this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/* 121 */         AbstractDungeon.effectList.add(new PurgeCardEffect(this.attackCard));
/* 122 */         AbstractDungeon.player.masterDeck.removeCard(this.attackCard);
/* 123 */         logMetric("Removed Attack");
/*     */       }
/*     */       
/*     */       
/* 127 */       break;
/*     */     default: 
/* 129 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 135 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Falling", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\Falling.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */