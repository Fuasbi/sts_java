/*    */ package com.megacrit.cardcrawl.events.beyond;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class TombRedMask extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Tomb of Lord Red Mask";
/* 15 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Tomb of Lord Red Mask");
/* 16 */   public static final String NAME = eventStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/*    */   private static final int GOLD_AMT = 222;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String MASK_RESULT = DESCRIPTIONS[1];
/* 24 */   private static final String RELIC_RESULT = DESCRIPTIONS[2];
/* 25 */   private CurScreen screen = CurScreen.INTRO;
/*    */   
/*    */   private static enum CurScreen {
/* 28 */     INTRO,  RESULT;
/*    */     
/*    */     private CurScreen() {} }
/*    */   
/* 32 */   public TombRedMask() { super(NAME, DIALOG_1, "images/events/redMaskTomb.jpg");
/*    */     
/* 34 */     if (AbstractDungeon.player.hasRelic("Red Mask")) {
/* 35 */       this.imageEventText.setDialogOption(OPTIONS[0]);
/*    */     } else {
/* 37 */       this.imageEventText.setDialogOption(OPTIONS[1], true);
/* 38 */       this.imageEventText.setDialogOption(OPTIONS[2] + AbstractDungeon.player.gold + OPTIONS[3]);
/*    */     }
/* 40 */     this.imageEventText.setDialogOption(OPTIONS[4]);
/*    */   }
/*    */   
/*    */ 
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 46 */     switch (this.screen) {
/*    */     case INTRO: 
/* 48 */       if (buttonPressed == 0) {
/* 49 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.RainingGoldEffect(222));
/* 50 */         AbstractDungeon.player.gainGold(222);
/* 51 */         this.imageEventText.updateBodyText(MASK_RESULT);
/* 52 */         logMetric("Wore Mask");
/* 53 */       } else if ((buttonPressed == 1) && (!AbstractDungeon.player.hasRelic("Red Mask"))) {
/* 54 */         AbstractDungeon.player.loseGold(AbstractDungeon.player.gold);
/* 55 */         logMetric("Paid");
/* 56 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, new com.megacrit.cardcrawl.relics.RedMask());
/*    */         
/*    */ 
/*    */ 
/* 60 */         this.imageEventText.updateBodyText(RELIC_RESULT);
/*    */       } else {
/* 62 */         openMap();
/* 63 */         this.imageEventText.clearAllDialogs();
/* 64 */         this.imageEventText.setDialogOption(OPTIONS[4]);
/* 65 */         logMetric("Ignored");
/*    */       }
/* 67 */       this.imageEventText.clearAllDialogs();
/* 68 */       this.imageEventText.setDialogOption(OPTIONS[4]);
/* 69 */       this.screen = CurScreen.RESULT;
/* 70 */       break;
/*    */     default: 
/* 72 */       openMap();
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 79 */     AbstractEvent.logMetric("Tomb of Lord Red Mask", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\TombRedMask.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */