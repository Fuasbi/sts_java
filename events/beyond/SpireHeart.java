/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.characters.AnimatedNpc;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.HeartAnimListener;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.DeathScreen;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.vfx.BorderFlashEffect;
/*     */ import java.text.NumberFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Locale;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SpireHeart extends AbstractEvent
/*     */ {
/*  27 */   private static final Logger logger = LogManager.getLogger(SpireHeart.class.getName());
/*     */   public static final String ID = "Spire Heart";
/*  29 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Spire Heart");
/*  30 */   public static final String NAME = eventStrings.NAME;
/*  31 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  32 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*  33 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */ 
/*     */   private AnimatedNpc npc;
/*     */   
/*  38 */   private float x = 1300.0F * Settings.scale; private float y = Settings.HEIGHT - 500.0F * Settings.scale;
/*     */   
/*     */   private long globalDamageDealt;
/*     */   
/*     */   private long totalDamageDealt;
/*     */   
/*     */   private int damageDealt;
/*     */   private int winstreak;
/*     */   private static final String HEART_DMG_KEY = "test_stat";
/*     */   public static final String WIN_STREAK_IRONCLAD = "win_streak_ironclad";
/*     */   public static final String WIN_STREAK_SILENT = "win_streak_silent";
/*     */   public static final String WIN_STREAK_DEFECT = "win_streak_defect";
/*     */   public static final String WIN_STREAK_IRONCLAD_BETA = "win_streak_ironclad_BETA";
/*     */   public static final String WIN_STREAK_SILENT_BETA = "win_streak_silent_BETA";
/*     */   public static final String WIN_STREAK_DEFECT_BETA = "win_streak_defect_BETA";
/*     */   
/*     */   private static enum CUR_SCREEN
/*     */   {
/*  56 */     INTRO,  MIDDLE,  MIDDLE_2,  END;
/*     */     
/*     */     private CUR_SCREEN() {}
/*     */   }
/*     */   
/*  61 */   public SpireHeart() { this.npc = new AnimatedNpc(1300.0F * Settings.scale, Settings.HEIGHT - 860.0F * Settings.scale, "images/npcs/heart/skeleton.atlas", "images/npcs/heart/skeleton.json", "idle");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  67 */     this.npc.setTimeScale(1.5F);
/*  68 */     this.npc.addListener(new HeartAnimListener());
/*     */     
/*  70 */     this.body = DESCRIPTIONS[0];
/*  71 */     this.roomEventText.clear();
/*  72 */     this.roomEventText.addDialogOption(OPTIONS[0]);
/*  73 */     this.hasDialog = true;
/*  74 */     this.hasFocus = true;
/*     */     
/*     */ 
/*  77 */     this.globalDamageDealt = SteamSaveSync.getGlobalStat("test_stat");
/*  78 */     DeathScreen.resetScoreChecks();
/*  79 */     this.damageDealt = DeathScreen.calcScore(true);
/*  80 */     SteamSaveSync.incrementStat("test_stat", this.damageDealt);
/*  81 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/*  83 */       if (Settings.isBeta) {
/*  84 */         SteamSaveSync.incrementStat("win_streak_ironclad_BETA", 1);
/*  85 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad_BETA"));
/*     */       } else {
/*  87 */         SteamSaveSync.incrementStat("win_streak_ironclad", 1);
/*  88 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad"));
/*     */       }
/*  90 */       break;
/*     */     case THE_SILENT: 
/*  92 */       if (Settings.isBeta) {
/*  93 */         SteamSaveSync.incrementStat("win_streak_silent_BETA", 1);
/*  94 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent_BETA"));
/*     */       } else {
/*  96 */         SteamSaveSync.incrementStat("win_streak_silent", 1);
/*  97 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent"));
/*     */       }
/*  99 */       break;
/*     */     case DEFECT: 
/* 101 */       if (Settings.isBeta) {
/* 102 */         SteamSaveSync.incrementStat("win_streak_defect_BETA", 1);
/* 103 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_defect_BETA"));
/*     */       } else {
/* 105 */         SteamSaveSync.incrementStat("win_streak_defect", 1);
/* 106 */         logger.info("WIN STREAK  " + SteamSaveSync.getStat("win_streak_defect"));
/*     */       }
/* 108 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 113 */     this.totalDamageDealt = SteamSaveSync.getStat("test_stat");
/*     */     
/*     */ 
/* 116 */     boolean skipUpload = (Settings.isModded) || (!Settings.isStandardRun());
/* 117 */     if (!skipUpload) {
/* 118 */       switch (AbstractDungeon.player.chosenClass) {
/*     */       case IRONCLAD: 
/* 120 */         if (Settings.isBeta) {
/* 121 */           this.winstreak = SteamSaveSync.getStat("win_streak_ironclad_BETA");
/* 122 */           SteamSaveSync.uploadLeaderboardScore("IRONCLAD_CONSECUTIVE_WINS_BETA", this.winstreak);
/*     */         } else {
/* 124 */           this.winstreak = SteamSaveSync.getStat("win_streak_ironclad");
/* 125 */           SteamSaveSync.uploadLeaderboardScore("IRONCLAD_CONSECUTIVE_WINS", this.winstreak);
/*     */         }
/* 127 */         break;
/*     */       case THE_SILENT: 
/* 129 */         this.winstreak = SteamSaveSync.getStat("win_streak_silent");
/* 130 */         if (Settings.isBeta) {
/* 131 */           this.winstreak = SteamSaveSync.getStat("win_streak_silent_BETA");
/* 132 */           SteamSaveSync.uploadLeaderboardScore("SILENT_CONSECUTIVE_WINS_BETA", this.winstreak);
/*     */         } else {
/* 134 */           this.winstreak = SteamSaveSync.getStat("win_streak_silent");
/* 135 */           SteamSaveSync.uploadLeaderboardScore("SILENT_CONSECUTIVE_WINS", this.winstreak);
/*     */         }
/* 137 */         break;
/*     */       case DEFECT: 
/* 139 */         this.winstreak = SteamSaveSync.getStat("win_streak_defect");
/* 140 */         if (Settings.isBeta) {
/* 141 */           this.winstreak = SteamSaveSync.getStat("win_streak_defect_BETA");
/* 142 */           SteamSaveSync.uploadLeaderboardScore("DEFECT_CONSECUTIVE_WINS_BETA", this.winstreak);
/*     */         } else {
/* 144 */           this.winstreak = SteamSaveSync.getStat("win_streak_defect");
/* 145 */           SteamSaveSync.uploadLeaderboardScore("DEFECT_CONSECUTIVE_WINS", this.winstreak);
/*     */         }
/* 147 */         break;
/*     */       
/*     */       default: 
/* 150 */         this.winstreak = 0;
/*     */       }
/*     */       
/*     */     }
/*     */     
/*     */ 
/* 156 */     CardCrawlGame.playerPref.putInteger("DMG_DEALT", this.damageDealt + CardCrawlGame.playerPref
/*     */     
/* 158 */       .getInteger("DMG_DEALT", 0));
/* 159 */     if (this.totalDamageDealt == -1L) {
/* 160 */       this.totalDamageDealt = CardCrawlGame.playerPref.getInteger("DMG_DEALT", 0);
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 166 */     super.update();
/* 167 */     if (!RoomEventDialog.waitForInput) {
/* 168 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 174 */     switch (this.screen) {
/*     */     case INTRO: 
/* 176 */       this.screen = CUR_SCREEN.MIDDLE;
/* 177 */       switch (AbstractDungeon.player.chosenClass) {
/*     */       case IRONCLAD: 
/* 179 */         this.roomEventText.updateBodyText(DESCRIPTIONS[8]);
/* 180 */         break;
/*     */       case THE_SILENT: 
/* 182 */         this.roomEventText.updateBodyText(DESCRIPTIONS[9]);
/* 183 */         break;
/*     */       case DEFECT: 
/* 185 */         this.roomEventText.updateBodyText(DESCRIPTIONS[10]);
/* 186 */         break;
/*     */       }
/*     */       
/*     */       
/* 190 */       this.roomEventText.updateDialogOption(0, OPTIONS[1]);
/* 191 */       break;
/*     */     case MIDDLE: 
/* 193 */       this.screen = CUR_SCREEN.MIDDLE_2;
/* 194 */       this.roomEventText.updateBodyText(DESCRIPTIONS[1] + this.damageDealt + DESCRIPTIONS[2]);
/* 195 */       this.roomEventText.updateDialogOption(0, OPTIONS[0]);
/*     */       AbstractGameAction.AttackEffect[] effects;
/*     */       AbstractGameAction.AttackEffect[] effects;
/*     */       AbstractGameAction.AttackEffect[] effects;
/* 199 */       Color color; AbstractGameAction.AttackEffect[] effects; switch (AbstractDungeon.player.chosenClass) {
/*     */       case IRONCLAD: 
/* 201 */         Color color = Color.RED;
/* 202 */         effects = new AbstractGameAction.AttackEffect[] { AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.FIRE, AbstractGameAction.AttackEffect.BLUNT_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.FIRE, AbstractGameAction.AttackEffect.BLUNT_HEAVY };
/*     */         
/*     */ 
/* 205 */         break;
/*     */       case THE_SILENT: 
/* 207 */         Color color = Color.GREEN;
/* 208 */         effects = new AbstractGameAction.AttackEffect[] { AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.POISON, AbstractGameAction.AttackEffect.SLASH_DIAGONAL, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.POISON, AbstractGameAction.AttackEffect.SLASH_DIAGONAL };
/*     */         
/*     */ 
/* 211 */         break;
/*     */       case DEFECT: 
/* 213 */         Color color = Color.SKY;
/* 214 */         effects = new AbstractGameAction.AttackEffect[] { AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.FIRE, AbstractGameAction.AttackEffect.SLASH_DIAGONAL, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.FIRE, AbstractGameAction.AttackEffect.SLASH_DIAGONAL };
/*     */         
/*     */ 
/* 217 */         break;
/*     */       default: 
/* 219 */         color = Color.WHITE;
/* 220 */         effects = new AbstractGameAction.AttackEffect[] { AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY, AbstractGameAction.AttackEffect.SLASH_HEAVY };
/*     */       }
/*     */       
/*     */       
/*     */ 
/* 225 */       int HITS = effects.length;
/* 226 */       int damagePerTick = this.damageDealt / HITS;
/* 227 */       int remainder = this.damageDealt % HITS;
/*     */       
/* 229 */       int[] damages = new int[HITS];
/* 230 */       for (int i = 0; i < HITS; i++) {
/* 231 */         damages[i] = damagePerTick;
/* 232 */         if (remainder > 0) {
/* 233 */           damages[i] += 1;
/* 234 */           remainder--;
/*     */         }
/*     */       }
/*     */       
/* 238 */       float tmp = 0.0F;
/* 239 */       AbstractDungeon.effectList.add(new BorderFlashEffect(color, true));
/* 240 */       for (int i = 0; i < HITS; i++) {
/* 241 */         tmp += MathUtils.random(0.05F, 0.2F);
/* 242 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.DamageHeartEffect(tmp, this.x, this.y, effects[i], damages[i]));
/*     */       }
/* 244 */       break;
/*     */     case MIDDLE_2: 
/* 246 */       this.screen = CUR_SCREEN.END;
/*     */       
/* 248 */       NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
/*     */       
/* 250 */       if (this.globalDamageDealt <= 0L) {
/* 251 */         this.roomEventText.updateBodyText(DESCRIPTIONS[3] + numberFormat
/* 252 */           .format(this.totalDamageDealt) + DESCRIPTIONS[4] + DESCRIPTIONS[7]);
/*     */       }
/*     */       else {
/* 255 */         this.roomEventText.updateBodyText(DESCRIPTIONS[3] + numberFormat
/* 256 */           .format(this.totalDamageDealt) + DESCRIPTIONS[4] + DESCRIPTIONS[5] + numberFormat
/* 257 */           .format(this.globalDamageDealt) + DESCRIPTIONS[6] + DESCRIPTIONS[7]);
/*     */       }
/* 259 */       this.roomEventText.updateDialogOption(0, OPTIONS[2]);
/* 260 */       break;
/*     */     case END: 
/* 262 */       AbstractDungeon.player.isDying = true;
/* 263 */       this.hasFocus = false;
/* 264 */       this.roomEventText.hide();
/* 265 */       AbstractDungeon.player.isDead = true;
/* 266 */       AbstractDungeon.deathScreen = new DeathScreen(null);
/* 267 */       break;
/*     */     default: 
/* 269 */       logger.info("WHY YOU CALLED?");
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 280 */     this.npc.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\SpireHeart.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */