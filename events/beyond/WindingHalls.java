/*     */ package com.megacrit.cardcrawl.events.beyond;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.colorless.Madness;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class WindingHalls extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Winding Halls";
/*  21 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Winding Halls");
/*  22 */   public static final String NAME = eventStrings.NAME;
/*  23 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  24 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   private static final float HP_LOSS_PERCENT = 0.125F;
/*     */   private static final float HP_MAX_LOSS_PERCENT = 0.05F;
/*     */   private static final float A_2_HP_LOSS_PERCENT = 0.18F;
/*     */   private static final float HEAL_AMT = 0.25F;
/*     */   private static final float A_2_HEAL_AMT = 0.2F;
/*     */   private int hpAmt;
/*  31 */   private int healAmt; private int maxHPAmt; private static final String INTRO_BODY1 = DESCRIPTIONS[0];
/*  32 */   private static final String INTRO_BODY2 = DESCRIPTIONS[1];
/*  33 */   private static final String CHOICE_1_TEXT = DESCRIPTIONS[2];
/*  34 */   private static final String CHOICE_2_TEXT = DESCRIPTIONS[3];
/*     */   
/*  36 */   private int screenNum = 0;
/*     */   
/*     */   public WindingHalls() {
/*  39 */     super(NAME, INTRO_BODY1, "images/events/winding.jpg");
/*     */     
/*  41 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  42 */       this.hpAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.18F);
/*  43 */       this.healAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.2F);
/*     */     } else {
/*  45 */       this.hpAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.125F);
/*  46 */       this.healAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.25F);
/*     */     }
/*  48 */     this.maxHPAmt = MathUtils.round(AbstractDungeon.player.maxHealth * 0.05F);
/*     */     
/*  50 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  55 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  58 */       this.imageEventText.updateBodyText(INTRO_BODY2);
/*  59 */       this.screenNum = 1;
/*  60 */       this.imageEventText.updateDialogOption(0, OPTIONS[1] + this.hpAmt + OPTIONS[2], CardLibrary.getCopy("Madness"));
/*  61 */       this.imageEventText.setDialogOption(OPTIONS[3] + this.healAmt + OPTIONS[5], CardLibrary.getCopy("Writhe"));
/*  62 */       this.imageEventText.setDialogOption(OPTIONS[6] + this.maxHPAmt + OPTIONS[7]);
/*  63 */       break;
/*     */     case 1: 
/*  65 */       switch (buttonPressed) {
/*     */       case 0: 
/*  67 */         logMetric("Embrace Madness");
/*  68 */         this.imageEventText.updateBodyText(CHOICE_1_TEXT);
/*  69 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, this.hpAmt));
/*  70 */         CardCrawlGame.sound.play("ATTACK_MAGIC_SLOW_1");
/*  71 */         AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new Madness(), Settings.WIDTH / 2.0F - 350.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  76 */         AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new Madness(), Settings.WIDTH / 2.0F + 350.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  81 */         UnlockTracker.markCardAsSeen("Madness");
/*  82 */         this.screenNum = 2;
/*  83 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  84 */         this.imageEventText.clearRemainingOptions();
/*  85 */         break;
/*     */       case 1: 
/*  87 */         this.imageEventText.updateBodyText(CHOICE_2_TEXT);
/*  88 */         AbstractDungeon.player.heal(this.healAmt);
/*  89 */         AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Writhe(), Settings.WIDTH / 2.0F + 10.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  94 */         UnlockTracker.markCardAsSeen("Writhe");
/*  95 */         logMetric("Writhe");
/*  96 */         this.screenNum = 2;
/*  97 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  98 */         this.imageEventText.clearRemainingOptions();
/*  99 */         break;
/*     */       case 2: 
/* 101 */         this.screenNum = 2;
/* 102 */         this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/* 103 */         logMetric("Max HP");
/* 104 */         AbstractDungeon.player.decreaseMaxHealth(this.maxHPAmt);
/* 105 */         CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT, true);
/* 106 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/* 107 */         this.imageEventText.clearRemainingOptions();
/*     */       }
/*     */       
/* 110 */       break;
/*     */     
/*     */ 
/*     */     default: 
/* 114 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 120 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Winding Halls", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\beyond\WindingHalls.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */