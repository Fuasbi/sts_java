/*     */ package com.megacrit.cardcrawl.events;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.RenderScene;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ 
/*     */ public abstract class AbstractEvent
/*     */ {
/*  25 */   protected Texture img = null;
/*  26 */   public RoomEventDialog roomEventText = new RoomEventDialog();
/*  27 */   public GenericEventDialog imageEventText = new GenericEventDialog();
/*     */   protected float drawX;
/*     */   protected float drawY;
/*     */   protected float imgWidth;
/*  31 */   protected float imgHeight; protected Color imgColor = Color.WHITE.cpy();
/*  32 */   protected Hitbox hb = null;
/*  33 */   public float panelAlpha = 0.0F;
/*  34 */   public boolean hideAlpha = false;
/*     */   
/*  36 */   public boolean hasFocus = false;
/*  37 */   protected String body = null;
/*  38 */   public float waitTimer = 1.5F;
/*  39 */   protected boolean waitForInput = false;
/*  40 */   public boolean hasDialog = false;
/*  41 */   protected int screenNum = 0;
/*  42 */   public static EventType type = EventType.IMAGE;
/*     */   
/*     */   public static String NAME;
/*     */   
/*     */   public static String[] DESCRIPTIONS;
/*     */   public static String[] OPTIONS;
/*  48 */   public boolean combatTime = false;
/*  49 */   public boolean noCardsInRewards = false;
/*     */   
/*     */ 
/*  52 */   public ArrayList<Integer> optionsSelected = new ArrayList();
/*     */   
/*     */   public static enum EventType {
/*  55 */     TEXT,  IMAGE,  ROOM;
/*     */     
/*     */     private EventType() {} }
/*     */   
/*  59 */   public AbstractEvent() { type = EventType.ROOM;
/*  60 */     if (Settings.FAST_MODE) {
/*  61 */       this.waitTimer = 0.1F;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void initializeImage(String imgUrl, float x, float y)
/*     */   {
/*  73 */     this.img = ImageMaster.loadImage(imgUrl);
/*  74 */     this.drawX = x;
/*  75 */     this.drawY = y;
/*  76 */     this.imgWidth = (this.img.getWidth() * Settings.scale);
/*  77 */     this.imgHeight = (this.img.getHeight() * Settings.scale);
/*     */   }
/*     */   
/*     */   public void onEnterRoom() {}
/*     */   
/*     */   public void enterCombat()
/*     */   {
/*  84 */     this.roomEventText.clear();
/*  85 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMBAT;
/*  86 */     AbstractDungeon.getCurrRoom().monsters.init();
/*  87 */     AbstractRoom.waitTimer = 0.1F;
/*  88 */     AbstractDungeon.player.preBattlePrep();
/*  89 */     this.hasFocus = false;
/*  90 */     this.roomEventText.hide();
/*     */   }
/*     */   
/*     */   protected abstract void buttonEffect(int paramInt);
/*     */   
/*     */   public void updateDialog() {
/*  96 */     this.imageEventText.update();
/*  97 */     this.roomEventText.update();
/*     */   }
/*     */   
/*     */   public void update() {
/* 101 */     if (this.waitTimer > 0.0F) {
/* 102 */       this.waitTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 103 */       if ((this.waitTimer < 0.0F) && (this.hasDialog)) {
/* 104 */         this.roomEventText.show(this.body);
/* 105 */         this.waitTimer = 0.0F;
/*     */       }
/*     */     }
/* 108 */     else if ((AbstractDungeon.getCurrRoom().phase != AbstractRoom.RoomPhase.COMBAT) && (!this.hideAlpha)) {
/* 109 */       this.panelAlpha = MathHelper.fadeLerpSnap(this.panelAlpha, 0.66F);
/*     */     } else {
/* 111 */       this.panelAlpha = MathHelper.fadeLerpSnap(this.panelAlpha, 0.0F);
/*     */     }
/*     */     
/*     */ 
/* 115 */     if (!RoomEventDialog.waitForInput) {
/* 116 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   public void logInput(int buttonPressed) {
/* 121 */     this.optionsSelected.add(Integer.valueOf(buttonPressed));
/*     */   }
/*     */   
/*     */   protected void openMap() {
/* 125 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 126 */     AbstractDungeon.dungeonMapScreen.open(false);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 130 */     if (this.img != null) {
/* 131 */       sb.setColor(this.imgColor);
/* 132 */       sb.draw(this.img, this.drawX, this.drawY, this.imgWidth, this.imgHeight);
/*     */     }
/*     */     
/* 135 */     if (this.hb != null) {
/* 136 */       this.hb.render(sb);
/* 137 */       if ((this.img != null) && (this.hb.hovered)) {
/* 138 */         sb.setBlendFunction(770, 1);
/* 139 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.3F));
/* 140 */         sb.draw(this.img, this.drawX, this.drawY, this.imgWidth, this.imgHeight);
/* 141 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void renderText(SpriteBatch sb) {
/* 147 */     this.roomEventText.render(sb);
/* 148 */     this.imageEventText.render(sb);
/*     */   }
/*     */   
/*     */   public void renderRoomEventPanel(SpriteBatch sb) {
/* 152 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.panelAlpha));
/* 153 */     sb.draw(ImageMaster.EVENT_ROOM_PANEL, 0.0F, Settings.HEIGHT - 475.0F * Settings.scale, Settings.WIDTH, 300.0F * Settings.scale);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void showProceedScreen(String bodyText)
/*     */   {
/* 162 */     this.roomEventText.updateBodyText(bodyText);
/* 163 */     this.roomEventText.updateDialogOption(0, "[ #bProceed ]");
/* 164 */     this.roomEventText.clearRemainingOptions();
/* 165 */     this.screenNum = 99;
/*     */   }
/*     */   
/*     */ 
/*     */   public void renderAboveTopPanel(SpriteBatch sb) {}
/*     */   
/*     */   public void reopen() {}
/*     */   
/*     */   public void postCombatLoad()
/*     */   {
/* 175 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMBAT;
/* 176 */     AbstractDungeon.getCurrRoom().isBattleOver = true;
/* 177 */     AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter("Colosseum Nobs");
/* 178 */     this.hasFocus = false;
/* 179 */     GenericEventDialog.hide();
/* 180 */     AbstractDungeon.rs = AbstractDungeon.RenderScene.NORMAL;
/*     */   }
/*     */   
/*     */   public static void logMetric(String eventName, String playerChoice) {
/* 184 */     logMetric(eventName, playerChoice, 0);
/*     */   }
/*     */   
/*     */   public static void logMetric(String eventName, String playerChoice, int damageTaken)
/*     */   {
/* 189 */     HashMap<String, Object> choice = new HashMap();
/* 190 */     choice.put("event_name", eventName);
/* 191 */     choice.put("player_choice", playerChoice);
/* 192 */     choice.put("damage_taken", Integer.valueOf(damageTaken));
/* 193 */     choice.put("floor", Integer.valueOf(AbstractDungeon.floorNum));
/* 194 */     com.megacrit.cardcrawl.core.CardCrawlGame.metricData.event_choices.add(choice);
/*     */   }
/*     */   
/*     */   public HashMap<String, Serializable> getLocStrings() {
/* 198 */     HashMap<String, Serializable> data = new HashMap();
/* 199 */     data.put("name", NAME);
/* 200 */     data.put("moves", DESCRIPTIONS);
/* 201 */     data.put("dialogs", OPTIONS);
/* 202 */     return data;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\AbstractEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */