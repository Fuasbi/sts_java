/*    */ package com.megacrit.cardcrawl.events;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.RenderScene;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*    */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public abstract class AbstractImageEvent extends AbstractEvent
/*    */ {
/*    */   protected String title;
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Proceed Screen");
/* 18 */   private static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*    */   
/*    */   public AbstractImageEvent(String title, String body, String imgUrl) {
/* 21 */     this.imageEventText.clear();
/* 22 */     this.roomEventText.clear();
/* 23 */     this.title = title;
/* 24 */     this.body = body;
/* 25 */     this.imageEventText.loadImage(imgUrl);
/* 26 */     type = AbstractEvent.EventType.IMAGE;
/* 27 */     this.noCardsInRewards = false;
/*    */   }
/*    */   
/*    */   public void update() {
/* 31 */     if (!this.combatTime) {
/* 32 */       this.hasFocus = true;
/* 33 */       if (com.badlogic.gdx.math.MathUtils.randomBoolean(0.1F)) {
/* 34 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.scene.EventBgParticle());
/*    */       }
/*    */       
/* 37 */       if (this.waitTimer > 0.0F) {
/* 38 */         this.waitTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 39 */         if (this.waitTimer < 0.0F) {
/* 40 */           this.imageEventText.show(this.title, this.body);
/* 41 */           this.waitTimer = 0.0F;
/*    */         }
/*    */       }
/*    */       
/* 45 */       if (!GenericEventDialog.waitForInput) {
/* 46 */         buttonEffect(GenericEventDialog.getSelectedOption());
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void showProceedScreen(String bodyText)
/*    */   {
/* 53 */     this.imageEventText.updateBodyText(bodyText);
/* 54 */     this.imageEventText.updateDialogOption(0, DESCRIPTIONS[0]);
/* 55 */     this.imageEventText.clearRemainingOptions();
/* 56 */     this.screenNum = 99;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */   
/*    */   protected void openMap()
/*    */   {
/* 63 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 64 */     AbstractDungeon.dungeonMapScreen.open(false);
/*    */   }
/*    */   
/*    */   public void enterCombatFromImage() {
/* 68 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMBAT;
/* 69 */     AbstractDungeon.getCurrRoom().monsters.init();
/* 70 */     AbstractRoom.waitTimer = 0.1F;
/* 71 */     AbstractDungeon.player.preBattlePrep();
/* 72 */     this.hasFocus = false;
/* 73 */     GenericEventDialog.hide();
/* 74 */     CardCrawlGame.fadeIn(1.5F);
/* 75 */     AbstractDungeon.rs = AbstractDungeon.RenderScene.NORMAL;
/* 76 */     this.combatTime = true;
/*    */   }
/*    */   
/*    */   public void enterImageFromCombat() {
/* 80 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.EVENT;
/* 81 */     AbstractDungeon.getCurrRoom().isBattleOver = false;
/* 82 */     AbstractDungeon.getCurrRoom().monsters.monsters.clear();
/* 83 */     AbstractDungeon.getCurrRoom().rewards.clear();
/* 84 */     this.hasDialog = true;
/* 85 */     this.hasFocus = true;
/* 86 */     this.combatTime = false;
/* 87 */     GenericEventDialog.show();
/* 88 */     CardCrawlGame.fadeIn(1.5F);
/* 89 */     AbstractDungeon.rs = AbstractDungeon.RenderScene.EVENT;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\AbstractImageEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */