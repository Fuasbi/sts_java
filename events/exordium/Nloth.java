/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.NlothsGift;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Nloth extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "N'loth";
/*  20 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("N'loth");
/*  21 */   public static final String NAME = eventStrings.NAME;
/*  22 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  23 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  25 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  26 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  27 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*     */   
/*  29 */   private int screenNum = 0;
/*     */   private AbstractRelic choice1;
/*     */   private AbstractRelic choice2;
/*     */   
/*  33 */   public Nloth() { super(NAME, DIALOG_1, "images/events/nloth.jpg");
/*     */     
/*     */ 
/*  36 */     ArrayList<AbstractRelic> relics = new ArrayList();
/*  37 */     relics.addAll(AbstractDungeon.player.relics);
/*  38 */     java.util.Collections.shuffle(relics, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/*  40 */     this.choice1 = ((AbstractRelic)relics.get(0));
/*  41 */     this.choice2 = ((AbstractRelic)relics.get(1));
/*     */     
/*  43 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.choice1.name + OPTIONS[1]);
/*  44 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.choice2.name + OPTIONS[1]);
/*  45 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  50 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  53 */       switch (buttonPressed) {
/*     */       case 0: 
/*  55 */         this.imageEventText.updateBodyText(DIALOG_2);
/*  56 */         logMetric("Traded a " + this.choice1.relicId);
/*     */         
/*  58 */         if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/*  59 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */           
/*     */ 
/*  62 */             RelicLibrary.getRelic("Circlet").makeCopy());
/*     */         } else {
/*  64 */           AbstractDungeon.player.loseRelic(this.choice1.relicId);
/*  65 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, new NlothsGift());
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  71 */         this.screenNum = 1;
/*  72 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  73 */         this.imageEventText.clearRemainingOptions();
/*  74 */         break;
/*     */       case 1: 
/*  76 */         this.imageEventText.updateBodyText(DIALOG_2);
/*  77 */         logMetric("Traded a " + this.choice2.relicId);
/*  78 */         AbstractDungeon.player.loseRelic(this.choice2.relicId);
/*  79 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, new NlothsGift());
/*     */         
/*     */ 
/*     */ 
/*  83 */         this.screenNum = 1;
/*  84 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  85 */         this.imageEventText.clearRemainingOptions();
/*  86 */         break;
/*     */       case 2: 
/*  88 */         logMetric("Ignored");
/*  89 */         this.imageEventText.updateBodyText(DIALOG_3);
/*  90 */         this.screenNum = 1;
/*  91 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  92 */         this.imageEventText.clearRemainingOptions();
/*  93 */         break;
/*     */       default: 
/*  95 */         this.imageEventText.updateBodyText(DIALOG_3);
/*  96 */         this.screenNum = 1;
/*  97 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  98 */         this.imageEventText.clearRemainingOptions(); }
/*  99 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/* 104 */       openMap();
/* 105 */       break;
/*     */     
/*     */     default: 
/* 108 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 114 */     AbstractEvent.logMetric("N'loth", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\Nloth.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */