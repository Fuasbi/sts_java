/*    */ package com.megacrit.cardcrawl.events.exordium;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GoopPuddle extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "World of Goop";
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("World of Goop");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String GOLD_DIALOG = DESCRIPTIONS[1];
/* 24 */   private static final String LEAVE_DIALOG = DESCRIPTIONS[2];
/*    */   
/* 26 */   private CurScreen screen = CurScreen.INTRO;
/* 27 */   private int damage = 11;
/* 28 */   private int gold = 75;
/*    */   private int goldLoss;
/*    */   
/*    */   private static enum CurScreen
/*    */   {
/* 33 */     INTRO,  RESULT;
/*    */     
/*    */     private CurScreen() {} }
/*    */   
/* 37 */   public GoopPuddle() { super(NAME, DIALOG_1, "images/events/goopPuddle.jpg");
/*    */     
/* 39 */     if (AbstractDungeon.ascensionLevel >= 15) {
/* 40 */       this.goldLoss = AbstractDungeon.miscRng.random(35, 75);
/*    */     } else {
/* 42 */       this.goldLoss = AbstractDungeon.miscRng.random(20, 50);
/*    */     }
/*    */     
/* 45 */     if (this.goldLoss > AbstractDungeon.player.gold) {
/* 46 */       this.goldLoss = AbstractDungeon.player.gold;
/*    */     }
/* 48 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.gold + OPTIONS[1] + this.damage + OPTIONS[2]);
/* 49 */     this.imageEventText.setDialogOption(OPTIONS[3] + this.goldLoss + OPTIONS[4]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 54 */     if (com.megacrit.cardcrawl.core.Settings.AMBIANCE_ON) {
/* 55 */       CardCrawlGame.sound.play("EVENT_SPIRITS");
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 61 */     switch (this.screen) {
/*    */     case INTRO: 
/* 63 */       switch (buttonPressed) {
/*    */       case 0: 
/* 65 */         this.imageEventText.updateBodyText(GOLD_DIALOG);
/* 66 */         this.imageEventText.clearAllDialogs();
/* 67 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(AbstractDungeon.player, this.damage));
/* 68 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */         
/*    */ 
/*    */ 
/*    */ 
/* 73 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.RainingGoldEffect(this.gold));
/* 74 */         AbstractDungeon.player.gainGold(this.gold);
/* 75 */         this.imageEventText.setDialogOption(OPTIONS[5]);
/* 76 */         this.screen = CurScreen.RESULT;
/* 77 */         logMetric("Gather Gold");
/* 78 */         break;
/*    */       case 1: 
/* 80 */         this.imageEventText.updateBodyText(LEAVE_DIALOG);
/* 81 */         AbstractDungeon.player.loseGold(this.goldLoss);
/* 82 */         this.imageEventText.clearAllDialogs();
/* 83 */         this.imageEventText.setDialogOption(OPTIONS[5]);
/* 84 */         this.screen = CurScreen.RESULT;
/* 85 */         logMetric("Left");
/*    */       }
/*    */       
/* 88 */       break;
/*    */     default: 
/* 90 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 96 */     AbstractEvent.logMetric("World of Goop", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\GoopPuddle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */