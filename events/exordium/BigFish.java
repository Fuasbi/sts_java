/*    */ package com.megacrit.cardcrawl.events.exordium;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class BigFish extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Big Fish";
/* 17 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Big Fish");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String BANANA_RESULT = DESCRIPTIONS[1];
/* 24 */   private static final String DONUT_RESULT = DESCRIPTIONS[2];
/*    */   
/* 26 */   private static final String BOX_RESULT = DESCRIPTIONS[4];
/* 27 */   private static final String BOX_BAD = DESCRIPTIONS[5];
/*    */   
/* 29 */   private int healAmt = 0;
/*    */   private static final int MAX_HP_AMT = 5;
/* 31 */   private CurScreen screen = CurScreen.INTRO;
/*    */   
/*    */   private static enum CurScreen {
/* 34 */     INTRO,  RESULT;
/*    */     
/*    */     private CurScreen() {} }
/*    */   
/* 38 */   public BigFish() { super(NAME, DIALOG_1, "images/events/fishing.jpg");
/* 39 */     this.healAmt = (AbstractDungeon.player.maxHealth / 3);
/* 40 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.healAmt + OPTIONS[1]);
/* 41 */     this.imageEventText.setDialogOption(OPTIONS[2] + 5 + OPTIONS[3]);
/* 42 */     this.imageEventText.setDialogOption(OPTIONS[4], CardLibrary.getCopy("Regret"));
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 47 */     switch (this.screen) {
/*    */     case INTRO: 
/* 49 */       switch (buttonPressed)
/*    */       {
/*    */       case 0: 
/* 52 */         AbstractDungeon.player.heal(this.healAmt, true);
/* 53 */         this.imageEventText.updateBodyText(BANANA_RESULT);
/* 54 */         logMetric("Banana");
/* 55 */         break;
/*    */       
/*    */       case 1: 
/* 58 */         AbstractDungeon.player.increaseMaxHp(5, true);
/* 59 */         this.imageEventText.updateBodyText(DONUT_RESULT);
/* 60 */         logMetric("Donut");
/* 61 */         break;
/*    */       
/*    */ 
/*    */       default: 
/* 65 */         this.imageEventText.updateBodyText(BOX_RESULT + BOX_BAD);
/* 66 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*    */         
/* 68 */           CardLibrary.getCopy("Regret"), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*    */         
/*    */ 
/* 71 */         UnlockTracker.markCardAsSeen("Regret");
/* 72 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*    */         
/*    */ 
/* 75 */           AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 76 */         logMetric("Box");
/*    */       }
/*    */       
/* 79 */       this.imageEventText.clearAllDialogs();
/* 80 */       this.imageEventText.setDialogOption(OPTIONS[5]);
/* 81 */       this.screen = CurScreen.RESULT;
/* 82 */       break;
/*    */     default: 
/* 84 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String actionTaken)
/*    */   {
/* 90 */     AbstractEvent.logMetric("Big Fish", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\BigFish.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */