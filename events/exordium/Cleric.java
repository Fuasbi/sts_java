/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Cleric extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "The Cleric";
/*  16 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("The Cleric");
/*  17 */   public static final String NAME = eventStrings.NAME;
/*  18 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  19 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   private static final int HEAL_COST = 35;
/*     */   private static final int PURIFY_COST = 50;
/*     */   private static final int A_2_PURIFY_COST = 75;
/*  23 */   private int purifyCost = 0;
/*     */   
/*     */   private static final float HEAL_AMT = 0.25F;
/*     */   
/*  27 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  28 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  29 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*  30 */   private static final String DIALOG_4 = DESCRIPTIONS[3];
/*     */   private int healAmt;
/*     */   
/*     */   public Cleric()
/*     */   {
/*  35 */     super(NAME, DIALOG_1, "images/events/cleric.jpg");
/*     */     
/*  37 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  38 */       this.purifyCost = 75;
/*     */     } else {
/*  40 */       this.purifyCost = 50;
/*     */     }
/*     */     
/*  43 */     int gold = AbstractDungeon.player.gold;
/*  44 */     if (gold >= 35) {
/*  45 */       this.healAmt = ((int)(AbstractDungeon.player.maxHealth * 0.25F));
/*  46 */       this.imageEventText.setDialogOption(OPTIONS[0] + this.healAmt + OPTIONS[8], gold < 35);
/*     */     } else {
/*  48 */       this.imageEventText.setDialogOption(OPTIONS[1] + 35 + OPTIONS[2], gold < 35);
/*     */     }
/*     */     
/*  51 */     if (gold >= 50) {
/*  52 */       this.imageEventText.setDialogOption(OPTIONS[3] + this.purifyCost + OPTIONS[4], gold < this.purifyCost);
/*     */     } else {
/*  54 */       this.imageEventText.setDialogOption(OPTIONS[5], gold < this.purifyCost);
/*     */     }
/*     */     
/*  57 */     this.imageEventText.setDialogOption(OPTIONS[6]);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  62 */     super.update();
/*     */     
/*     */ 
/*  65 */     if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/*  66 */       AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/*  67 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(c, com.megacrit.cardcrawl.core.Settings.WIDTH / 2, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2));
/*  68 */       AbstractDungeon.player.masterDeck.removeCard(c);
/*  69 */       AbstractDungeon.gridSelectScreen.selectedCards.remove(c);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  75 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  78 */       switch (buttonPressed) {
/*     */       case 0: 
/*  80 */         AbstractDungeon.player.loseGold(35);
/*  81 */         AbstractDungeon.player.heal(this.healAmt);
/*  82 */         showProceedScreen(DIALOG_2);
/*  83 */         logMetric("Healed");
/*  84 */         break;
/*     */       case 1: 
/*  86 */         AbstractDungeon.player.loseGold(this.purifyCost);
/*  87 */         AbstractDungeon.gridSelectScreen.open(
/*  88 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/*  89 */           .getPurgeableCards()), 1, OPTIONS[7], false, false, false, true);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  96 */         showProceedScreen(DIALOG_3);
/*  97 */         logMetric("Card Removal");
/*  98 */         break;
/*     */       default: 
/* 100 */         showProceedScreen(DIALOG_4);
/* 101 */         logMetric("Leave"); }
/* 102 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     default: 
/* 107 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 113 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("The Cleric", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\Cleric.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */