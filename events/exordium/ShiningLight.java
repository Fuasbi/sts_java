/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ShiningLight extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Shining Light";
/*  23 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Shining Light");
/*  24 */   public static final String NAME = eventStrings.NAME;
/*  25 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  26 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  28 */   private static final String INTRO = DESCRIPTIONS[0];
/*  29 */   private static final String AGREE_DIALOG = DESCRIPTIONS[1];
/*  30 */   private static final String DISAGREE_DIALOG = DESCRIPTIONS[2];
/*     */   
/*  32 */   private int damage = 0;
/*     */   
/*     */   private static final float HP_LOSS_PERCENT = 0.2F;
/*     */   private static final float A_2_HP_LOSS_PERCENT = 0.3F;
/*  36 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  39 */     INTRO,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  43 */   public ShiningLight() { super(NAME, INTRO, "images/events/shiningLight.jpg");
/*     */     
/*  45 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  46 */       this.damage = MathUtils.round(AbstractDungeon.player.maxHealth * 0.3F);
/*     */     } else {
/*  48 */       this.damage = MathUtils.round(AbstractDungeon.player.maxHealth * 0.2F);
/*     */     }
/*     */     
/*  51 */     if (AbstractDungeon.player.masterDeck.hasUpgradableCards().booleanValue()) {
/*  52 */       this.imageEventText.setDialogOption(OPTIONS[0] + this.damage + OPTIONS[1]);
/*     */     } else {
/*  54 */       this.imageEventText.setDialogOption(OPTIONS[3], true);
/*     */     }
/*     */     
/*  57 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  62 */     if (Settings.AMBIANCE_ON) {
/*  63 */       CardCrawlGame.sound.play("EVENT_SHINING");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  69 */     switch (this.screen) {
/*     */     case INTRO: 
/*  71 */       if (buttonPressed == 0) {
/*  72 */         this.imageEventText.updateBodyText(AGREE_DIALOG);
/*  73 */         this.imageEventText.removeDialogOption(1);
/*  74 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  75 */         this.screen = CUR_SCREEN.COMPLETE;
/*  76 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(AbstractDungeon.player, this.damage));
/*  77 */         AbstractDungeon.effectList.add(new FlashAtkImgEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  82 */         upgradeCards();
/*  83 */         logMetric("Entered Light");
/*     */       } else {
/*  85 */         this.imageEventText.updateBodyText(DISAGREE_DIALOG);
/*  86 */         this.imageEventText.removeDialogOption(1);
/*  87 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  88 */         this.screen = CUR_SCREEN.COMPLETE;
/*  89 */         logMetric("Ignored");
/*     */       }
/*  91 */       break;
/*     */     default: 
/*  93 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void upgradeCards()
/*     */   {
/*  99 */     AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/* 100 */     ArrayList<AbstractCard> upgradableCards = new ArrayList();
/* 101 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/* 102 */       if (c.canUpgrade()) {
/* 103 */         upgradableCards.add(c);
/*     */       }
/*     */     }
/*     */     
/* 107 */     java.util.Collections.shuffle(upgradableCards, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/* 109 */     if (!upgradableCards.isEmpty())
/*     */     {
/* 111 */       if (upgradableCards.size() == 1) {
/* 112 */         ((AbstractCard)upgradableCards.get(0)).upgrade();
/* 113 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(0));
/* 114 */         AbstractDungeon.effectList.add(new ShowCardBrieflyEffect(((AbstractCard)upgradableCards.get(0)).makeStatEquivalentCopy()));
/*     */       } else {
/* 116 */         ((AbstractCard)upgradableCards.get(0)).upgrade();
/* 117 */         ((AbstractCard)upgradableCards.get(1)).upgrade();
/* 118 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(0));
/* 119 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(1));
/* 120 */         AbstractDungeon.effectList.add(new ShowCardBrieflyEffect(
/*     */         
/* 122 */           ((AbstractCard)upgradableCards.get(0)).makeStatEquivalentCopy(), Settings.WIDTH / 2.0F - 190.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/* 125 */         AbstractDungeon.effectList.add(new ShowCardBrieflyEffect(
/*     */         
/* 127 */           ((AbstractCard)upgradableCards.get(1)).makeStatEquivalentCopy(), Settings.WIDTH / 2.0F + 190.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 134 */     AbstractEvent.logMetric("Shining Light", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\ShiningLight.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */