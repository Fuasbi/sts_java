/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class LivingWall extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Living Wall";
/*  18 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Living Wall");
/*  19 */   public static final String NAME = eventStrings.NAME;
/*  20 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  21 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  23 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  24 */   private static final String RESULT_DIALOG = DESCRIPTIONS[1];
/*     */   
/*  26 */   private CurScreen screen = CurScreen.INTRO;
/*  27 */   private boolean pickCard = false;
/*  28 */   private Choice choice = null;
/*     */   
/*     */   private static enum CurScreen {
/*  31 */     INTRO,  RESULT;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  35 */   private static enum Choice { FORGET,  CHANGE,  GROW;
/*     */     
/*     */     private Choice() {} }
/*     */   
/*  39 */   public LivingWall() { super(NAME, DIALOG_1, "images/events/livingWall.jpg");
/*     */     
/*  41 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  42 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*  43 */     if (AbstractDungeon.player.masterDeck.hasUpgradableCards().booleanValue()) {
/*  44 */       this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */     } else {
/*  46 */       this.imageEventText.setDialogOption(OPTIONS[7], true);
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  52 */     super.update();
/*  53 */     if ((this.pickCard) && 
/*  54 */       (!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  55 */       switch (this.choice) {
/*     */       case FORGET: 
/*  57 */         CardCrawlGame.sound.play("CARD_EXHAUST");
/*  58 */         AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/*     */         
/*  60 */           (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */         
/*     */ 
/*  63 */         AbstractDungeon.player.masterDeck.removeCard(
/*  64 */           (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*  65 */         break;
/*     */       case CHANGE: 
/*  67 */         AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/*  68 */         AbstractDungeon.player.masterDeck.removeCard(c);
/*  69 */         AbstractDungeon.transformCard(c, false, AbstractDungeon.miscRng);
/*  70 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*     */         
/*  72 */           AbstractDungeon.getTransformedCard(), c.current_x, c.current_y));
/*     */         
/*     */ 
/*  75 */         break;
/*     */       case GROW: 
/*  77 */         AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */         
/*  79 */         ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).upgrade();
/*  80 */         AbstractDungeon.player.bottledCardUpgradeCheck(
/*  81 */           (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*     */       }
/*     */       
/*  84 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*  85 */       this.pickCard = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  92 */     switch (this.screen) {
/*     */     case INTRO: 
/*  94 */       switch (buttonPressed) {
/*     */       case 0: 
/*  96 */         this.choice = Choice.FORGET;
/*  97 */         AbstractDungeon.gridSelectScreen.open(
/*  98 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/*  99 */           .getPurgeableCards()), 1, OPTIONS[3], false, false, false, true);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 106 */         logMetric("Forget");
/* 107 */         break;
/*     */       case 1: 
/* 109 */         this.choice = Choice.CHANGE;
/* 110 */         AbstractDungeon.gridSelectScreen.open(
/* 111 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 112 */           .getPurgeableCards()), 1, OPTIONS[4], false, true, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 119 */         logMetric("Change");
/* 120 */         break;
/*     */       default: 
/* 122 */         AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 123 */           .getUpgradableCards(), 1, OPTIONS[5], true, false, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 130 */         this.choice = Choice.GROW;
/* 131 */         logMetric("Grow");
/*     */       }
/*     */       
/* 134 */       this.pickCard = true;
/* 135 */       this.imageEventText.updateBodyText(RESULT_DIALOG);
/* 136 */       this.imageEventText.clearAllDialogs();
/* 137 */       this.imageEventText.setDialogOption(OPTIONS[6]);
/* 138 */       this.screen = CurScreen.RESULT;
/* 139 */       break;
/*     */     default: 
/* 141 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 147 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Living Wall", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\LivingWall.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */