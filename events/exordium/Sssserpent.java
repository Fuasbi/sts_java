/*    */ package com.megacrit.cardcrawl.events.exordium;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Sssserpent extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Liars Game";
/* 18 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Liars Game");
/* 19 */   public static final String NAME = eventStrings.NAME;
/* 20 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 21 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 23 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 24 */   private static final String AGREE_DIALOG = DESCRIPTIONS[1];
/* 25 */   private static final String DISAGREE_DIALOG = DESCRIPTIONS[2];
/* 26 */   private static final String GOLD_RAIN_MSG = DESCRIPTIONS[3];
/* 27 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*    */   private static final int GOLD_REWARD = 175;
/*    */   private static final int A_2_GOLD_REWARD = 150;
/* 30 */   private int goldReward = 0;
/*    */   
/*    */   private static enum CUR_SCREEN {
/* 33 */     INTRO,  AGREE,  DISAGREE,  COMPLETE;
/*    */     
/*    */     private CUR_SCREEN() {} }
/*    */   
/* 37 */   public Sssserpent() { super(NAME, DIALOG_1, "images/events/liarsGame.jpg");
/*    */     
/* 39 */     if (AbstractDungeon.ascensionLevel >= 15) {
/* 40 */       this.goldReward = 150;
/*    */     } else {
/* 42 */       this.goldReward = 175;
/*    */     }
/*    */     
/* 45 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.goldReward + OPTIONS[1], CardLibrary.getCopy("Doubt"));
/* 46 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 51 */     switch (this.screen) {
/*    */     case INTRO: 
/* 53 */       if (buttonPressed == 0) {
/* 54 */         this.imageEventText.updateBodyText(AGREE_DIALOG);
/* 55 */         this.imageEventText.removeDialogOption(1);
/* 56 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/* 57 */         this.screen = CUR_SCREEN.AGREE;
/* 58 */         logMetric(true);
/*    */       } else {
/* 60 */         this.imageEventText.updateBodyText(DISAGREE_DIALOG);
/* 61 */         this.imageEventText.removeDialogOption(1);
/* 62 */         this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/* 63 */         this.screen = CUR_SCREEN.DISAGREE;
/* 64 */         logMetric(false);
/*    */       }
/* 66 */       break;
/*    */     case AGREE: 
/* 68 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Doubt(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */       
/* 70 */       UnlockTracker.markCardAsSeen("Doubt");
/* 71 */       AbstractDungeon.effectList.add(new RainingGoldEffect(this.goldReward));
/* 72 */       AbstractDungeon.player.gainGold(this.goldReward);
/* 73 */       this.imageEventText.updateBodyText(GOLD_RAIN_MSG);
/* 74 */       this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/* 75 */       this.screen = CUR_SCREEN.COMPLETE;
/* 76 */       break;
/*    */     default: 
/* 78 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(boolean agreed)
/*    */   {
/* 84 */     String actionTaken = "agreed";
/* 85 */     if (!agreed) {
/* 86 */       actionTaken = "disagreed";
/*    */     }
/* 88 */     AbstractEvent.logMetric("Liars Game", actionTaken);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\Sssserpent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */