/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.Circlet;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class Mushrooms extends AbstractEvent
/*     */ {
/*  26 */   private static final Logger logger = LogManager.getLogger(Mushrooms.class.getName());
/*     */   public static final String ID = "Mushrooms";
/*  28 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Mushrooms");
/*  29 */   public static final String NAME = eventStrings.NAME;
/*  30 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  31 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   public static final String ENC_NAME = "The Mushroom Lair";
/*  34 */   private Texture fgImg = ImageMaster.loadImage("images/events/fgShrooms.png");
/*  35 */   private Texture bgImg = ImageMaster.loadImage("images/events/bgShrooms.png");
/*     */   private static final float HEAL_AMT = 0.25F;
/*  37 */   private static final String HEAL_MSG = DESCRIPTIONS[0];
/*  38 */   private static final String FIGHT_MSG = DESCRIPTIONS[1];
/*  39 */   private int screenNum = 0;
/*     */   
/*     */   public Mushrooms()
/*     */   {
/*  43 */     this.body = DESCRIPTIONS[2];
/*     */     
/*  45 */     this.roomEventText.addDialogOption(OPTIONS[0]);
/*  46 */     int temp = (int)(AbstractDungeon.player.maxHealth * 0.25F);
/*  47 */     this.roomEventText.addDialogOption(OPTIONS[1] + temp + OPTIONS[2], CardLibrary.getCopy("Parasite"));
/*  48 */     AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.EVENT;
/*  49 */     this.hasDialog = true;
/*  50 */     this.hasFocus = true;
/*     */   }
/*     */   
/*     */   public void update() {
/*  54 */     super.update();
/*  55 */     if (!RoomEventDialog.waitForInput) {
/*  56 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  62 */     switch (buttonPressed)
/*     */     {
/*     */     case 0: 
/*  65 */       if (this.screenNum == 0) {
/*  66 */         AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter("The Mushroom Lair");
/*     */         
/*  68 */         this.roomEventText.updateBodyText(FIGHT_MSG);
/*  69 */         this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/*  70 */         this.roomEventText.removeDialogOption(1);
/*  71 */         logMetric("Fought Mushrooms");
/*  72 */         this.screenNum += 2;
/*  73 */       } else if (this.screenNum == 1)
/*     */       {
/*  75 */         openMap();
/*  76 */       } else if (this.screenNum == 2) {
/*  77 */         if (Settings.isDailyRun) {
/*  78 */           AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(25));
/*     */         } else {
/*  80 */           AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(20, 30));
/*     */         }
/*  82 */         if (AbstractDungeon.player.hasRelic("Odd Mushroom")) {
/*  83 */           AbstractDungeon.getCurrRoom().addRelicToRewards(new Circlet());
/*     */         } else {
/*  85 */           AbstractDungeon.getCurrRoom().addRelicToRewards(new com.megacrit.cardcrawl.relics.OddMushroom());
/*     */         }
/*     */         
/*  88 */         enterCombat();
/*  89 */         AbstractDungeon.lastCombatMetricKey = "The Mushroom Lair";
/*     */       }
/*  91 */       return;
/*     */     
/*     */     case 1: 
/*  94 */       logMetric("Healed and dodged fight");
/*  95 */       AbstractDungeon.player.heal((int)(AbstractDungeon.player.maxHealth * 0.25F));
/*  96 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Parasite(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/*  98 */       UnlockTracker.markCardAsSeen("Parasite");
/*  99 */       this.roomEventText.updateBodyText(HEAL_MSG);
/* 100 */       this.roomEventText.updateDialogOption(0, OPTIONS[4]);
/* 101 */       this.roomEventText.removeDialogOption(1);
/* 102 */       this.screenNum = 1;
/* 103 */       return;
/*     */     }
/* 105 */     logger.info("ERROR: case " + buttonPressed + " should never be called");
/*     */   }
/*     */   
/*     */ 
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 111 */     AbstractEvent.logMetric("Mushrooms", actionTaken);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 116 */     sb.setColor(Color.WHITE);
/* 117 */     sb.draw(this.bgImg, 0.0F, -10.0F, Settings.WIDTH, 1080.0F * Settings.scale);
/* 118 */     sb.draw(this.fgImg, 0.0F, -20.0F * Settings.scale, Settings.WIDTH, 1080.0F * Settings.scale);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\Mushrooms.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */