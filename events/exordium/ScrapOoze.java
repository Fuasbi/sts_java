/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class ScrapOoze extends AbstractImageEvent
/*     */ {
/*  15 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ScrapOoze.class.getName());
/*     */   public static final String ID = "Scrap Ooze";
/*  17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Scrap Ooze");
/*  18 */   public static final String NAME = eventStrings.NAME;
/*  19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  22 */   private int relicObtainChance = 25;
/*  23 */   private int dmg = 3;
/*  24 */   private int totalDamageDealt = 0;
/*  25 */   private int screenNum = 0;
/*  26 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  27 */   private static final String FAIL_MSG = DESCRIPTIONS[1];
/*  28 */   private static final String SUCCESS_MSG = DESCRIPTIONS[2];
/*  29 */   private static final String ESCAPE_MSG = DESCRIPTIONS[3];
/*     */   
/*     */   public ScrapOoze() {
/*  32 */     super(NAME, DIALOG_1, "images/events/scrapOoze.jpg");
/*     */     
/*  34 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  35 */       this.dmg = 5;
/*     */     }
/*     */     
/*  38 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.dmg + OPTIONS[1] + this.relicObtainChance + OPTIONS[2]);
/*  39 */     this.imageEventText.setDialogOption(OPTIONS[3]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  44 */     if (Settings.AMBIANCE_ON) {
/*  45 */       CardCrawlGame.sound.play("EVENT_OOZE");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  51 */     switch (this.screenNum) {
/*     */     case 0: 
/*  53 */       switch (buttonPressed) {
/*     */       case 0: 
/*  55 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, this.dmg));
/*  56 */         CardCrawlGame.sound.play("ATTACK_POISON");
/*  57 */         this.totalDamageDealt += this.dmg;
/*  58 */         int random = AbstractDungeon.miscRng.random(0, 99);
/*     */         
/*  60 */         if (random >= 99 - this.relicObtainChance) {
/*  61 */           this.imageEventText.updateBodyText(SUCCESS_MSG);
/*  62 */           logMetric(true, this.totalDamageDealt);
/*  63 */           this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  64 */           this.imageEventText.removeDialogOption(1);
/*  65 */           this.screenNum = 1;
/*  66 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 
/*     */           
/*     */ 
/*  69 */             AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/*     */         } else {
/*  71 */           this.imageEventText.updateBodyText(FAIL_MSG);
/*  72 */           this.relicObtainChance += 10;
/*  73 */           this.dmg += 1;
/*  74 */           this.imageEventText.updateDialogOption(0, OPTIONS[4] + this.dmg + OPTIONS[1] + this.relicObtainChance + OPTIONS[2]);
/*     */           
/*     */ 
/*  77 */           this.imageEventText.updateDialogOption(1, OPTIONS[3]);
/*     */         }
/*     */         
/*  80 */         break;
/*     */       case 1: 
/*  82 */         logMetric(false, this.totalDamageDealt);
/*  83 */         this.imageEventText.updateBodyText(ESCAPE_MSG);
/*  84 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  85 */         this.imageEventText.removeDialogOption(1);
/*  86 */         this.screenNum = 1;
/*  87 */         break;
/*     */       default: 
/*  89 */         logger.info("ERROR: case " + buttonPressed + " should never be called"); }
/*  90 */       break;
/*     */     
/*     */ 
/*     */     case 1: 
/*  94 */       openMap();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void logMetric(boolean success, int damageTaken)
/*     */   {
/* 102 */     String actionTaken = "success";
/* 103 */     if (!success) {
/* 104 */       actionTaken = "unsuccessful";
/*     */     }
/*     */     
/* 107 */     AbstractEvent.logMetric("Scrap Ooze", actionTaken, damageTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\ScrapOoze.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */