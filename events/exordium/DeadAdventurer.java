/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.EffectHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class DeadAdventurer extends AbstractEvent
/*     */ {
/*  24 */   private static final Logger logger = LogManager.getLogger(DeadAdventurer.class.getName());
/*     */   public static final String ID = "Dead Adventurer";
/*  26 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Dead Adventurer");
/*  27 */   public static final String NAME = eventStrings.NAME;
/*  28 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  29 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   private static final int GOLD_REWARD = 30;
/*     */   private static final int ENCOUNTER_CHANCE_START = 25;
/*     */   private static final int A_2_CHANCE_START = 35;
/*     */   private static final int ENCOUNTER_CHANCE_RAMP = 25;
/*  35 */   private static final String FIGHT_MSG = DESCRIPTIONS[0];
/*  36 */   private static final String ESCAPE_MSG = DESCRIPTIONS[1];
/*     */   
/*  38 */   private int numRewards = 0;
/*  39 */   private int encounterChance = 0;
/*  40 */   private ArrayList<String> rewards = new ArrayList();
/*  41 */   private float x = 800.0F * Settings.scale; private float y = AbstractDungeon.floorY;
/*  42 */   private int enemy = 0;
/*  43 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*  44 */   private static final Color DARKEN_COLOR = new Color(0.5F, 0.5F, 0.5F, 1.0F);
/*     */   public static final String LAGAVULIN_FIGHT = "Lagavulin Dead Adventurers Fight";
/*     */   private com.badlogic.gdx.graphics.Texture adventurerImg;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  49 */     INTRO,  FAIL,  SUCCESS,  ESCAPE;
/*     */     
/*     */     private CUR_SCREEN() {}
/*     */   }
/*     */   
/*  54 */   public DeadAdventurer() { this.rewards.add("GOLD");
/*  55 */     this.rewards.add("POTION");
/*  56 */     this.rewards.add("RELIC");
/*  57 */     Collections.shuffle(this.rewards, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/*  59 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  60 */       this.encounterChance = 35;
/*     */     } else {
/*  62 */       this.encounterChance = 25;
/*     */     }
/*     */     
/*  65 */     this.enemy = AbstractDungeon.eventRng.random(0, 2);
/*  66 */     this.adventurerImg = ImageMaster.loadImage("images/npcs/nopants.png");
/*  67 */     this.body = DESCRIPTIONS[2];
/*     */     
/*  69 */     switch (this.enemy) {
/*     */     case 0: 
/*  71 */       this.body += DESCRIPTIONS[3];
/*  72 */       break;
/*     */     case 1: 
/*  74 */       this.body += DESCRIPTIONS[4];
/*  75 */       break;
/*     */     default: 
/*  77 */       this.body += DESCRIPTIONS[5];
/*     */     }
/*     */     
/*  80 */     this.body += DESCRIPTIONS[6];
/*     */     
/*  82 */     this.roomEventText.addDialogOption(OPTIONS[0] + this.encounterChance + OPTIONS[4]);
/*  83 */     this.roomEventText.addDialogOption(OPTIONS[1]);
/*     */     
/*  85 */     this.hasDialog = true;
/*  86 */     this.hasFocus = true;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  91 */     super.update();
/*  92 */     if ((AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.EVENT) || 
/*  93 */       (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMPLETE)) {
/*  94 */       this.imgColor = Color.WHITE.cpy();
/*     */     } else {
/*  96 */       this.imgColor = DARKEN_COLOR;
/*     */     }
/*     */     
/*  99 */     if (!RoomEventDialog.waitForInput) {
/* 100 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 106 */     switch (this.screen) {
/*     */     case INTRO: 
/* 108 */       switch (buttonPressed) {
/*     */       case 0: 
/* 110 */         if (AbstractDungeon.eventRng.random(0, 99) < this.encounterChance)
/*     */         {
/* 112 */           this.screen = CUR_SCREEN.FAIL;
/* 113 */           this.roomEventText.updateBodyText(FIGHT_MSG);
/* 114 */           this.roomEventText.updateDialogOption(0, OPTIONS[2]);
/* 115 */           this.roomEventText.removeDialogOption(1);
/* 116 */           if (Settings.isDailyRun) {
/* 117 */             AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(30));
/*     */           } else {
/* 119 */             AbstractDungeon.getCurrRoom().addGoldToRewards(AbstractDungeon.eventRng.random(25, 35));
/*     */           }
/*     */           
/* 122 */           AbstractDungeon.getCurrRoom().monsters = MonsterHelper.getEncounter(getMonster());
/* 123 */           AbstractDungeon.getCurrRoom().eliteTrigger = true;
/*     */         }
/*     */         else
/*     */         {
/* 127 */           randomReward();
/*     */         }
/* 129 */         break;
/*     */       case 1: 
/* 131 */         this.screen = CUR_SCREEN.ESCAPE;
/* 132 */         this.roomEventText.updateBodyText(ESCAPE_MSG);
/* 133 */         this.roomEventText.updateDialogOption(0, OPTIONS[1]);
/* 134 */         this.roomEventText.removeDialogOption(1);
/*     */       }
/*     */       
/* 137 */       break;
/*     */     case SUCCESS: 
/* 139 */       openMap();
/* 140 */       break;
/*     */     case FAIL: 
/* 142 */       for (String s : this.rewards) {
/* 143 */         if (s.equals("GOLD")) {
/* 144 */           AbstractDungeon.getCurrRoom().addGoldToRewards(30);
/* 145 */         } else if (s.equals("POTION")) {
/* 146 */           AbstractDungeon.getCurrRoom().addPotionToRewards();
/* 147 */         } else if (s.equals("RELIC")) {
/* 148 */           AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractDungeon.returnRandomRelicTier());
/*     */         }
/*     */       }
/* 151 */       enterCombat();
/* 152 */       AbstractDungeon.lastCombatMetricKey = getMonster();
/* 153 */       this.numRewards += 1;
/* 154 */       logMetric(this.numRewards);
/* 155 */       break;
/*     */     case ESCAPE: 
/* 157 */       logMetric(this.numRewards);
/* 158 */       openMap();
/* 159 */       break;
/*     */     default: 
/* 161 */       logger.info("WHY YOU CALLED?");
/*     */     }
/*     */   }
/*     */   
/*     */   private String getMonster()
/*     */   {
/* 167 */     switch (this.enemy) {
/*     */     case 0: 
/* 169 */       return "3 Sentries";
/*     */     case 1: 
/* 171 */       return "Gremlin Nob";
/*     */     }
/* 173 */     return "Lagavulin Event";
/*     */   }
/*     */   
/*     */ 
/*     */   private void randomReward()
/*     */   {
/* 179 */     this.numRewards += 1;
/* 180 */     this.encounterChance += 25;
/* 181 */     switch ((String)this.rewards.remove(0)) {
/*     */     case "GOLD": 
/* 183 */       this.roomEventText.updateBodyText(DESCRIPTIONS[7]);
/* 184 */       EffectHelper.gainGold(AbstractDungeon.player, this.x, this.y, 30);
/* 185 */       break;
/*     */     case "POTION": 
/* 187 */       this.roomEventText.updateBodyText(DESCRIPTIONS[8]);
/* 188 */       break;
/*     */     case "RELIC": 
/* 190 */       this.roomEventText.updateBodyText(DESCRIPTIONS[9]);
/* 191 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.x, this.y, 
/*     */       
/*     */ 
/* 194 */         AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 195 */       break;
/*     */     default: 
/* 197 */       logger.info("HOW IS THIS POSSSIBLLEEEE");
/*     */     }
/*     */     
/*     */     
/* 201 */     if (this.numRewards == 3) {
/* 202 */       this.roomEventText.updateBodyText(DESCRIPTIONS[10]);
/* 203 */       this.roomEventText.updateDialogOption(0, OPTIONS[1]);
/* 204 */       this.roomEventText.removeDialogOption(1);
/* 205 */       this.screen = CUR_SCREEN.SUCCESS;
/* 206 */       logMetric(this.numRewards);
/*     */     } else {
/* 208 */       logger.info("SHOULD NOT DISMISS");
/* 209 */       this.roomEventText.updateDialogOption(0, OPTIONS[3] + this.encounterChance + OPTIONS[4]);
/* 210 */       this.roomEventText.updateDialogOption(1, OPTIONS[1]);
/* 211 */       this.screen = CUR_SCREEN.INTRO;
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(int numAttempts) {
/* 216 */     AbstractEvent.logMetric("Dead Adventurer", "Searched '" + numAttempts + "' times");
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 221 */     super.render(sb);
/* 222 */     sb.setColor(Color.WHITE);
/* 223 */     sb.draw(this.adventurerImg, this.x - 146.0F, this.y - 86.5F, 146.0F, 86.5F, 292.0F, 173.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 292, 173, false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\DeadAdventurer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */