/*     */ package com.megacrit.cardcrawl.events.exordium;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ 
/*     */ public class GoldenIdolEvent extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Golden Idol";
/*  23 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Golden Idol");
/*  24 */   public static final String NAME = eventStrings.NAME;
/*  25 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  26 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  28 */   private static final String DIALOG_START = DESCRIPTIONS[0];
/*  29 */   private static final String DIALOG_BOULDER = DESCRIPTIONS[1];
/*  30 */   private static final String DIALOG_CHOSE_RUN = DESCRIPTIONS[2];
/*  31 */   private static final String DIALOG_CHOSE_FIGHT = DESCRIPTIONS[3];
/*  32 */   private static final String DIALOG_CHOSE_FLAT = DESCRIPTIONS[4];
/*  33 */   private static final String DIALOG_IGNORE = DESCRIPTIONS[5];
/*     */   
/*  35 */   private int screenNum = 0;
/*     */   private static final float HP_LOSS_PERCENT = 0.25F;
/*     */   private static final float MAX_HP_LOSS_PERCENT = 0.08F;
/*     */   private static final float A_2_HP_LOSS_PERCENT = 0.35F;
/*     */   private static final float A_2_MAX_HP_LOSS_PERCENT = 0.1F;
/*     */   private int hpLoss;
/*     */   private int maxHpLoss;
/*     */   
/*     */   public GoldenIdolEvent() {
/*  44 */     super(NAME, DIALOG_START, "images/events/goldenIdol.jpg");
/*     */     
/*  46 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  47 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*     */     
/*  49 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  50 */       this.hpLoss = ((int)(AbstractDungeon.player.maxHealth * 0.35F));
/*  51 */       this.maxHpLoss = ((int)(AbstractDungeon.player.maxHealth * 0.1F));
/*     */     } else {
/*  53 */       this.hpLoss = ((int)(AbstractDungeon.player.maxHealth * 0.25F));
/*  54 */       this.maxHpLoss = ((int)(AbstractDungeon.player.maxHealth * 0.08F));
/*     */     }
/*     */     
/*  57 */     if (this.maxHpLoss < 1) {
/*  58 */       this.maxHpLoss = 1;
/*     */     }
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  64 */     if (Settings.AMBIANCE_ON) {
/*  65 */       CardCrawlGame.sound.play("EVENT_GOLDEN");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  71 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  74 */       switch (buttonPressed) {
/*     */       case 0: 
/*  76 */         this.imageEventText.updateBodyText(DIALOG_BOULDER);
/*     */         
/*  78 */         if (AbstractDungeon.player.hasRelic("Golden Idol")) {
/*  79 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */           
/*     */ 
/*  82 */             RelicLibrary.getRelic("Circlet").makeCopy());
/*     */         } else {
/*  84 */           AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */           
/*     */ 
/*  87 */             RelicLibrary.getRelic("Golden Idol").makeCopy());
/*     */         }
/*     */         
/*  90 */         CardCrawlGame.screenShake.mildRumble(5.0F);
/*  91 */         CardCrawlGame.sound.play("BLUNT_HEAVY");
/*  92 */         this.screenNum = 1;
/*  93 */         this.imageEventText.updateDialogOption(0, OPTIONS[2], com.megacrit.cardcrawl.helpers.CardLibrary.getCopy("Injury"));
/*  94 */         this.imageEventText.updateDialogOption(1, OPTIONS[3] + this.hpLoss + OPTIONS[4]);
/*  95 */         this.imageEventText.setDialogOption(OPTIONS[5] + this.maxHpLoss + OPTIONS[6]);
/*     */         
/*  97 */         break;
/*     */       default: 
/*  99 */         this.imageEventText.updateBodyText(DIALOG_IGNORE);
/* 100 */         this.screenNum = 2;
/* 101 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 102 */         this.imageEventText.clearRemainingOptions();
/* 103 */         logMetric("Ignored"); }
/* 104 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/* 109 */       switch (buttonPressed)
/*     */       {
/*     */       case 0: 
/* 112 */         CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.MED, false);
/* 113 */         this.imageEventText.updateBodyText(DIALOG_CHOSE_RUN);
/* 114 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Injury(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */         
/* 116 */         UnlockTracker.markCardAsSeen("Injury");
/* 117 */         this.screenNum = 2;
/* 118 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 119 */         logMetric("Take Wound");
/* 120 */         this.imageEventText.clearRemainingOptions();
/* 121 */         break;
/*     */       
/*     */       case 1: 
/* 124 */         this.imageEventText.updateBodyText(DIALOG_CHOSE_FIGHT);
/* 125 */         CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.MED, false);
/* 126 */         CardCrawlGame.sound.play("BLUNT_FAST");
/* 127 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, this.hpLoss));
/* 128 */         this.screenNum = 2;
/* 129 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 130 */         logMetric("Take Damage");
/* 131 */         this.imageEventText.clearRemainingOptions();
/* 132 */         break;
/*     */       
/*     */       case 2: 
/* 135 */         this.imageEventText.updateBodyText(DIALOG_CHOSE_FLAT);
/* 136 */         AbstractDungeon.player.decreaseMaxHealth(this.maxHpLoss);
/* 137 */         CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.MED, false);
/* 138 */         CardCrawlGame.sound.play("BLUNT_FAST");
/* 139 */         this.screenNum = 2;
/* 140 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 141 */         logMetric("Lose Max HP");
/* 142 */         this.imageEventText.clearRemainingOptions();
/* 143 */         break;
/*     */       default: 
/* 145 */         openMap(); }
/* 146 */       break;
/*     */     
/*     */ 
/*     */     case 2: 
/* 150 */       openMap();
/* 151 */       break;
/*     */     default: 
/* 153 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 159 */     AbstractEvent.logMetric("Golden Idol", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\exordium\GoldenIdolEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */