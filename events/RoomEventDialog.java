/*     */ package com.megacrit.cardcrawl.events;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.AppearEffect;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordColor;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordEffect;
/*     */ import com.megacrit.cardcrawl.ui.buttons.LargeDialogOptionButton;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Scanner;
/*     */ 
/*     */ public class RoomEventDialog
/*     */ {
/*  22 */   private Color color = new Color(0.0F, 0.0F, 0.0F, 0.0F); private Color targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  23 */   private static final Color PANEL_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*     */   
/*  25 */   private boolean isMoving = false;
/*  26 */   private float animateTimer = 0.0F;
/*     */   
/*  28 */   private boolean show = false;
/*     */   
/*     */ 
/*  31 */   private float curLineWidth = 0.0F;
/*  32 */   private int curLine = 0;
/*     */   
/*     */ 
/*  35 */   private GlyphLayout gl = new GlyphLayout();
/*  36 */   private ArrayList<DialogWord> words = new ArrayList();
/*  37 */   private boolean textDone = true;
/*  38 */   private float wordTimer = 0.0F;
/*     */   
/*  40 */   private static final float CHAR_SPACING = 8.0F * Settings.scale;
/*  41 */   private static final float LINE_SPACING = 30.0F * Settings.scale;
/*  42 */   private static final float DIALOG_MSG_X = Settings.WIDTH * 0.1F;
/*  43 */   private static final float DIALOG_MSG_Y = 250.0F * Settings.scale;
/*  44 */   private static final float DIALOG_MSG_W = Settings.WIDTH * 0.8F;
/*     */   
/*     */ 
/*  47 */   public static ArrayList<LargeDialogOptionButton> optionList = new ArrayList();
/*  48 */   public static int selectedOption = -1;
/*  49 */   public static boolean waitForInput = true;
/*     */   private static final float COLOR_FADE_SPEED = 8.0F;
/*     */   
/*  52 */   public void update() { if (this.isMoving) {
/*  53 */       this.animateTimer -= Gdx.graphics.getDeltaTime();
/*  54 */       if (this.animateTimer < 0.0F) {
/*  55 */         this.animateTimer = 0.0F;
/*  56 */         this.isMoving = false;
/*     */       }
/*     */     }
/*     */     
/*  60 */     this.color = this.color.lerp(this.targetColor, Gdx.graphics.getDeltaTime() * 8.0F);
/*     */     int i;
/*  62 */     if (this.show) {
/*  63 */       for (i = 0; i < optionList.size(); i++) {
/*  64 */         ((LargeDialogOptionButton)optionList.get(i)).update(optionList.size());
/*  65 */         if ((((LargeDialogOptionButton)optionList.get(i)).pressed) && (waitForInput)) {
/*  66 */           selectedOption = i;
/*  67 */           ((LargeDialogOptionButton)optionList.get(i)).pressed = false;
/*  68 */           waitForInput = false;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  73 */     if (Settings.lineBreakViaCharacter) {
/*  74 */       bodyTextEffectCN();
/*     */     } else {
/*  76 */       bodyTextEffect();
/*     */     }
/*  78 */     for (DialogWord w : this.words)
/*  79 */       w.update(); }
/*     */   
/*     */   private static final float ANIM_SPEED = 0.5F;
/*     */   private DialogWord.AppearEffect a_effect;
/*     */   
/*  84 */   public int getSelectedOption() { waitForInput = true;
/*  85 */     return selectedOption; }
/*     */   
/*     */   private Scanner s;
/*     */   private static final float WORD_TIME = 0.02F;
/*  89 */   public void clear() { optionList.clear();
/*  90 */     this.words.clear();
/*  91 */     waitForInput = true;
/*     */   }
/*     */   
/*     */   public void show() {
/*  95 */     this.targetColor = PANEL_COLOR;
/*  96 */     if (Settings.FAST_MODE) {
/*  97 */       this.animateTimer = 0.125F;
/*     */     } else {
/*  99 */       this.animateTimer = 0.5F;
/*     */     }
/* 101 */     this.show = true;
/* 102 */     this.isMoving = true;
/*     */   }
/*     */   
/*     */   public void show(String text) {
/* 106 */     updateBodyText(text);
/* 107 */     show();
/*     */   }
/*     */   
/*     */   public void hide() {
/* 111 */     this.targetColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/* 112 */     if (Settings.FAST_MODE) {
/* 113 */       this.animateTimer = 0.125F;
/*     */     } else {
/* 115 */       this.animateTimer = 0.5F;
/*     */     }
/* 117 */     this.show = false;
/* 118 */     this.isMoving = true;
/* 119 */     for (DialogWord w : this.words) {
/* 120 */       w.dialogFadeOut();
/*     */     }
/* 122 */     optionList.clear();
/*     */   }
/*     */   
/*     */   public void removeDialogOption(int slot) {
/* 126 */     optionList.remove(slot);
/*     */   }
/*     */   
/*     */   public void addDialogOption(String text) {
/* 130 */     optionList.add(new LargeDialogOptionButton(optionList.size(), text));
/*     */   }
/*     */   
/*     */   public void addDialogOption(String text, AbstractCard previewCard) {
/* 134 */     optionList.add(new LargeDialogOptionButton(optionList.size(), text, previewCard));
/*     */   }
/*     */   
/*     */   public void addDialogOption(String text, boolean isDisabled) {
/* 138 */     optionList.add(new LargeDialogOptionButton(optionList.size(), text, isDisabled));
/*     */   }
/*     */   
/*     */   public void addDialogOption(String text, boolean isDisabled, AbstractCard previewCard) {
/* 142 */     optionList.add(new LargeDialogOptionButton(optionList.size(), text, isDisabled, previewCard));
/*     */   }
/*     */   
/*     */   public void updateDialogOption(int slot, String text) {
/* 146 */     optionList.set(slot, new LargeDialogOptionButton(slot, text));
/*     */   }
/*     */   
/*     */   public void updateBodyText(String text) {
/* 150 */     updateBodyText(text, DialogWord.AppearEffect.BUMP_IN);
/*     */   }
/*     */   
/*     */   public void updateBodyText(String text, DialogWord.AppearEffect ae) {
/* 154 */     this.s = new Scanner(text);
/* 155 */     this.words.clear();
/* 156 */     this.textDone = false;
/* 157 */     this.a_effect = ae;
/* 158 */     this.curLineWidth = 0.0F;
/* 159 */     this.curLine = 0;
/*     */   }
/*     */   
/*     */   public void clearRemainingOptions() {
/* 163 */     for (int i = optionList.size() - 1; i > 0; i--) {
/* 164 */       optionList.remove(i);
/*     */     }
/*     */   }
/*     */   
/*     */   private void bodyTextEffectCN() {
/* 169 */     this.wordTimer -= Gdx.graphics.getDeltaTime();
/* 170 */     if ((this.wordTimer < 0.0F) && (!this.textDone)) {
/* 171 */       if (Settings.FAST_MODE) {
/* 172 */         this.wordTimer = 0.005F;
/*     */       } else {
/* 174 */         this.wordTimer = 0.02F;
/*     */       }
/*     */       
/* 177 */       if (this.s.hasNext()) {
/* 178 */         String word = this.s.next();
/*     */         
/* 180 */         if (word.equals("NL")) {
/* 181 */           this.curLine += 1;
/* 182 */           this.curLineWidth = 0.0F;
/* 183 */           return;
/*     */         }
/*     */         
/* 186 */         DialogWord.WordColor color = DialogWord.identifyWordColor(word);
/* 187 */         if (color != DialogWord.WordColor.DEFAULT) {
/* 188 */           word = word.substring(2, word.length());
/*     */         }
/*     */         
/* 191 */         DialogWord.WordEffect effect = DialogWord.identifyWordEffect(word);
/* 192 */         if (effect != DialogWord.WordEffect.NONE) {
/* 193 */           word = word.substring(1, word.length() - 1);
/*     */         }
/*     */         
/*     */ 
/* 197 */         for (int i = 0; i < word.length(); i++) {
/* 198 */           String tmp = Character.toString(word.charAt(i));
/*     */           
/* 200 */           this.gl.setText(FontHelper.eventBodyText, tmp);
/* 201 */           if (this.curLineWidth + this.gl.width > DIALOG_MSG_W) {
/* 202 */             this.curLine += 1;
/* 203 */             this.curLineWidth = this.gl.width;
/*     */           } else {
/* 205 */             this.curLineWidth += this.gl.width;
/*     */           }
/*     */           
/* 208 */           this.words.add(new DialogWord(FontHelper.eventBodyText, tmp, this.a_effect, effect, color, DIALOG_MSG_X + this.curLineWidth - this.gl.width, DIALOG_MSG_Y - LINE_SPACING * this.curLine, this.curLine));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 219 */           if (!this.show) {
/* 220 */             ((DialogWord)this.words.get(this.words.size() - 1)).dialogFadeOut();
/*     */           }
/*     */         }
/*     */       } else {
/* 224 */         this.textDone = true;
/* 225 */         this.s.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void bodyTextEffect() {
/* 231 */     this.wordTimer -= Gdx.graphics.getDeltaTime();
/* 232 */     if ((this.wordTimer < 0.0F) && (!this.textDone)) {
/* 233 */       if (Settings.FAST_MODE) {
/* 234 */         this.wordTimer = 0.005F;
/*     */       } else {
/* 236 */         this.wordTimer = 0.02F;
/*     */       }
/*     */       
/* 239 */       if (this.s.hasNext()) {
/* 240 */         String word = this.s.next();
/*     */         
/* 242 */         if (word.equals("NL")) {
/* 243 */           this.curLine += 1;
/* 244 */           this.curLineWidth = 0.0F;
/* 245 */           return;
/*     */         }
/*     */         
/* 248 */         DialogWord.WordColor color = DialogWord.identifyWordColor(word);
/* 249 */         if (color != DialogWord.WordColor.DEFAULT) {
/* 250 */           word = word.substring(2, word.length());
/*     */         }
/*     */         
/* 253 */         DialogWord.WordEffect effect = DialogWord.identifyWordEffect(word);
/* 254 */         if (effect != DialogWord.WordEffect.NONE) {
/* 255 */           word = word.substring(1, word.length() - 1);
/*     */         }
/*     */         
/* 258 */         this.gl.setText(FontHelper.textOnlyEventBody, word);
/* 259 */         if (this.curLineWidth + this.gl.width > DIALOG_MSG_W) {
/* 260 */           this.curLine += 1;
/* 261 */           this.curLineWidth = (this.gl.width + CHAR_SPACING);
/*     */         } else {
/* 263 */           this.curLineWidth += this.gl.width + CHAR_SPACING;
/*     */         }
/*     */         
/* 266 */         this.words.add(new DialogWord(FontHelper.textOnlyEventBody, word, this.a_effect, effect, color, DIALOG_MSG_X + this.curLineWidth - this.gl.width, DIALOG_MSG_Y - LINE_SPACING * this.curLine, this.curLine));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 277 */         if (!this.show) {
/* 278 */           ((DialogWord)this.words.get(this.words.size() - 1)).dialogFadeOut();
/*     */         }
/*     */       }
/*     */       else {
/* 282 */         this.textDone = true;
/* 283 */         this.s.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 289 */     if (!AbstractDungeon.player.isDead) {
/* 290 */       for (DialogWord w : this.words) {
/* 291 */         w.render(sb, Settings.HEIGHT - 525.0F * Settings.scale);
/*     */       }
/*     */       
/* 294 */       for (LargeDialogOptionButton b : optionList) {
/* 295 */         b.render(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\RoomEventDialog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */