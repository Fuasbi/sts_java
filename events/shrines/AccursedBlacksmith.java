/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class AccursedBlacksmith extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Accursed Blacksmith";
/*  20 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Accursed Blacksmith");
/*  21 */   public static final String NAME = eventStrings.NAME;
/*  22 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  23 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  25 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  26 */   private static final String FORGE_RESULT = DESCRIPTIONS[1];
/*  27 */   private static final String RUMMAGE_RESULT = DESCRIPTIONS[2];
/*  28 */   private static final String CURSE_RESULT1 = DESCRIPTIONS[3];
/*  29 */   private static final String CURSE_RESULT2 = DESCRIPTIONS[4];
/*  30 */   private static final String LEAVE_RESULT = DESCRIPTIONS[5];
/*     */   
/*  32 */   private int screenNum = 0;
/*  33 */   private boolean pickCard = false;
/*     */   
/*     */   public AccursedBlacksmith() {
/*  36 */     super(NAME, DIALOG_1, "images/events/blacksmith.jpg");
/*     */     
/*  38 */     if (AbstractDungeon.player.masterDeck.hasUpgradableCards().booleanValue()) {
/*  39 */       this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */     } else {
/*  41 */       this.imageEventText.setDialogOption(OPTIONS[4], true);
/*     */     }
/*     */     
/*  44 */     this.imageEventText.setDialogOption(OPTIONS[1], com.megacrit.cardcrawl.helpers.CardLibrary.getCopy("Normality"));
/*  45 */     this.imageEventText.setDialogOption(OPTIONS[2]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  50 */     if (Settings.AMBIANCE_ON) {
/*  51 */       CardCrawlGame.sound.play("EVENT_FORGE");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  57 */     super.update();
/*     */     
/*     */ 
/*  60 */     if ((this.pickCard) && 
/*  61 */       (!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  62 */       ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).upgrade();
/*  63 */       AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*  64 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect(
/*     */       
/*  66 */         ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).makeStatEquivalentCopy()));
/*  67 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*  68 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*  69 */       this.pickCard = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  76 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/*  79 */       switch (buttonPressed) {
/*     */       case 0: 
/*  81 */         this.pickCard = true;
/*  82 */         AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/*  83 */           .getUpgradableCards(), 1, OPTIONS[3], true, false, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  90 */         this.imageEventText.updateBodyText(FORGE_RESULT);
/*  91 */         this.screenNum = 2;
/*     */         
/*  93 */         logMetric("Forge");
/*  94 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  95 */         break;
/*     */       
/*     */       case 1: 
/*  98 */         this.screenNum = 2;
/*  99 */         this.imageEventText.updateBodyText(RUMMAGE_RESULT + CURSE_RESULT2);
/* 100 */         AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Normality(), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */         
/* 102 */         UnlockTracker.markCardAsSeen("Normality");
/*     */         
/* 104 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */         
/*     */ 
/* 107 */           AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/* 108 */         logMetric("Rummage");
/* 109 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/* 110 */         break;
/*     */       
/*     */       case 2: 
/* 113 */         this.screenNum = 2;
/* 114 */         logMetric("Ignore");
/* 115 */         this.imageEventText.updateBodyText(LEAVE_RESULT);
/* 116 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*     */       }
/*     */       
/* 119 */       this.imageEventText.clearRemainingOptions();
/* 120 */       break;
/*     */     
/*     */     case 1: 
/* 123 */       this.imageEventText.updateBodyText(CURSE_RESULT1);
/* 124 */       AbstractDungeon.effectList.add(new ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Injury(), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */       
/* 126 */       UnlockTracker.markCardAsSeen("Injury");
/* 127 */       switch (buttonPressed) {
/*     */       }
/* 129 */       this.screenNum = 2;
/* 130 */       this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*     */       
/* 132 */       break;
/*     */     
/*     */     default: 
/* 135 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken)
/*     */   {
/* 141 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Accursed Blacksmith", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\AccursedBlacksmith.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */