/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.vfx.UpgradeShineEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Designer extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Designer";
/*  23 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Designer");
/*  24 */   public static final String NAME = eventStrings.NAME;
/*  25 */   public static final String[] DESC = eventStrings.DESCRIPTIONS;
/*  26 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*  27 */   private CurrentScreen curScreen = CurrentScreen.INTRO;
/*  28 */   private OptionChosen option = null;
/*     */   public static final int GOLD_REQ = 75;
/*     */   public static final int UPG_AMT = 2;
/*     */   public static final int REMOVE_AMT = 2;
/*     */   private boolean adjustmentUpgradesOne;
/*     */   
/*  34 */   private static enum CurrentScreen { INTRO,  MAIN,  DONE;
/*     */     
/*     */     private CurrentScreen() {} }
/*     */   
/*  38 */   private static enum OptionChosen { UPGRADE,  REMOVE,  REMOVE_AND_UPGRADE,  TRANSFORM,  NONE;
/*     */     
/*     */     private OptionChosen() {} }
/*     */   
/*  42 */   public Designer() { super(NAME, DESC[0], "images/events/designer2.jpg");
/*  43 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  44 */     this.option = OptionChosen.NONE;
/*     */     
/*     */ 
/*  47 */     this.adjustmentUpgradesOne = AbstractDungeon.miscRng.randomBoolean();
/*  48 */     this.cleanUpRemovesCards = AbstractDungeon.miscRng.randomBoolean();
/*     */     
/*  50 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  51 */       this.adjustCost = 50;
/*  52 */       this.cleanUpCost = 75;
/*  53 */       this.fullServiceCost = 110;
/*  54 */       this.hpLoss = 5;
/*     */     } else {
/*  56 */       this.adjustCost = 40;
/*  57 */       this.cleanUpCost = 60;
/*  58 */       this.fullServiceCost = 90;
/*  59 */       this.hpLoss = 3;
/*     */     } }
/*     */   
/*     */   private boolean cleanUpRemovesCards;
/*     */   private int adjustCost;
/*     */   
/*  65 */   public void update() { super.update();
/*     */     
/*     */ 
/*  68 */     if (this.option != OptionChosen.NONE) {
/*  69 */       switch (this.option) {
/*     */       case REMOVE: 
/*  71 */         if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  72 */           CardCrawlGame.sound.play("CARD_EXHAUST");
/*  73 */           AbstractDungeon.topLevelEffects.add(new PurgeCardEffect(
/*     */           
/*  75 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/*  78 */           AbstractDungeon.player.masterDeck.removeCard(
/*  79 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*  80 */           AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*  81 */           this.option = OptionChosen.NONE;
/*     */         }
/*     */         break;
/*     */       case REMOVE_AND_UPGRADE: 
/*  85 */         if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  86 */           CardCrawlGame.sound.play("CARD_EXHAUST");
/*  87 */           AbstractDungeon.topLevelEffects.add(new PurgeCardEffect(
/*     */           
/*  89 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH - 20.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/*  92 */           AbstractDungeon.player.masterDeck.removeCard(
/*  93 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/*  94 */           AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*  95 */           upgradeRandomCard();
/*  96 */           this.option = OptionChosen.NONE;
/*     */         }
/*     */         break;
/*     */       case TRANSFORM: 
/* 100 */         if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 101 */           if (AbstractDungeon.gridSelectScreen.selectedCards.size() == 2) {
/* 102 */             AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 103 */             AbstractDungeon.player.masterDeck.removeCard(c);
/* 104 */             AbstractDungeon.transformCard(c);
/* 105 */             AbstractDungeon.effectsQueue.add(new ShowCardAndObtainEffect(
/*     */             
/* 107 */               AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH / 2.0F - 20.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */             
/*     */ 
/* 110 */             c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(1);
/* 111 */             AbstractDungeon.player.masterDeck.removeCard(c);
/* 112 */             AbstractDungeon.transformCard(c);
/* 113 */             AbstractDungeon.effectsQueue.add(new ShowCardAndObtainEffect(
/*     */             
/* 115 */               AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH / 2.0F + 20.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */             
/*     */ 
/* 118 */             AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*     */           } else {
/* 120 */             AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 121 */             AbstractDungeon.player.masterDeck.removeCard(c);
/* 122 */             AbstractDungeon.transformCard(c);
/* 123 */             AbstractDungeon.effectsQueue.add(new ShowCardAndObtainEffect(
/*     */             
/* 125 */               AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */             
/*     */ 
/* 128 */             AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*     */           }
/* 130 */           this.option = OptionChosen.NONE;
/*     */         }
/*     */         break;
/*     */       case UPGRADE: 
/* 134 */         if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 135 */           ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).upgrade();
/* 136 */           AbstractDungeon.player.bottledCardUpgradeCheck(
/* 137 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 138 */           AbstractDungeon.effectsQueue.add(new ShowCardBrieflyEffect(
/*     */           
/* 140 */             ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).makeStatEquivalentCopy()));
/* 141 */           AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */           
/* 143 */           AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 144 */           this.option = OptionChosen.NONE;
/*     */         }
/*     */         break;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private int cleanUpCost;
/*     */   private int fullServiceCost;
/*     */   private int hpLoss;
/*     */   protected void buttonEffect(int buttonPressed) {
/* 155 */     switch (this.curScreen) {
/*     */     case INTRO: 
/* 157 */       this.imageEventText.updateBodyText(DESC[1]);
/*     */       
/* 159 */       this.imageEventText.removeDialogOption(0);
/*     */       
/*     */ 
/* 162 */       if (this.adjustmentUpgradesOne) {
/* 163 */         this.imageEventText.updateDialogOption(0, OPTIONS[1] + this.adjustCost + OPTIONS[6] + OPTIONS[9], (AbstractDungeon.player.gold < this.adjustCost) || 
/*     */         
/*     */ 
/* 166 */           (!AbstractDungeon.player.masterDeck
/* 167 */           .hasUpgradableCards().booleanValue()));
/*     */       }
/*     */       else {
/* 169 */         this.imageEventText.updateDialogOption(0, OPTIONS[1] + this.adjustCost + OPTIONS[6] + OPTIONS[7] + 2 + OPTIONS[8], (AbstractDungeon.player.gold < this.adjustCost) || 
/*     */         
/*     */ 
/* 172 */           (!AbstractDungeon.player.masterDeck
/* 173 */           .hasUpgradableCards().booleanValue()));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 177 */       if (this.cleanUpRemovesCards) {
/* 178 */         this.imageEventText.setDialogOption(OPTIONS[2] + this.cleanUpCost + OPTIONS[6] + OPTIONS[10], (AbstractDungeon.player.gold < this.cleanUpCost) || 
/*     */         
/*     */ 
/*     */ 
/* 182 */           (CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).size() == 0));
/*     */       } else {
/* 184 */         this.imageEventText.setDialogOption(OPTIONS[2] + this.cleanUpCost + OPTIONS[6] + OPTIONS[11] + 2 + OPTIONS[12], (AbstractDungeon.player.gold < this.cleanUpCost) || 
/*     */         
/*     */ 
/*     */ 
/* 188 */           (CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).size() < 2));
/*     */       }
/*     */       
/*     */ 
/* 192 */       this.imageEventText.setDialogOption(OPTIONS[3] + this.fullServiceCost + OPTIONS[6] + OPTIONS[13], (AbstractDungeon.player.gold < this.fullServiceCost) || 
/*     */       
/*     */ 
/*     */ 
/* 196 */         (CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).size() == 0));
/*     */       
/*     */ 
/* 199 */       this.imageEventText.setDialogOption(OPTIONS[4] + this.hpLoss + OPTIONS[5]);
/*     */       
/* 201 */       this.curScreen = CurrentScreen.MAIN;
/* 202 */       break;
/*     */     case MAIN: 
/* 204 */       switch (buttonPressed)
/*     */       {
/*     */       case 0: 
/* 207 */         this.imageEventText.updateBodyText(DESC[2]);
/* 208 */         AbstractDungeon.player.loseGold(this.adjustCost);
/*     */         
/* 210 */         if (this.adjustmentUpgradesOne) {
/* 211 */           logMetric("Upgrade Card");
/* 212 */           this.option = OptionChosen.UPGRADE;
/* 213 */           AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 214 */             .getUpgradableCards(), 1, OPTIONS[15], true, false, false, false);
/*     */ 
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/*     */ 
/* 222 */           logMetric("Upgrade 2 Random Cards");
/* 223 */           upgradeTwoRandomCards();
/*     */         }
/* 225 */         break;
/*     */       
/*     */ 
/*     */       case 1: 
/* 229 */         this.imageEventText.updateBodyText(DESC[2]);
/* 230 */         AbstractDungeon.player.loseGold(this.cleanUpCost);
/*     */         
/* 232 */         if (this.cleanUpRemovesCards) {
/* 233 */           logMetric("Remove Card");
/* 234 */           this.option = OptionChosen.REMOVE;
/* 235 */           AbstractDungeon.gridSelectScreen.open(
/* 236 */             CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 237 */             .getPurgeableCards()), 1, OPTIONS[17], false, false, false, true);
/*     */ 
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/*     */ 
/* 245 */           logMetric("Transform 2 Cards");
/* 246 */           this.option = OptionChosen.TRANSFORM;
/* 247 */           AbstractDungeon.gridSelectScreen.open(
/* 248 */             CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 249 */             .getPurgeableCards()), 2, OPTIONS[16], false, false, false, false);
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 257 */         break;
/*     */       
/*     */ 
/*     */       case 2: 
/* 261 */         this.imageEventText.updateBodyText(DESC[2]);
/* 262 */         logMetric("Full Service");
/* 263 */         AbstractDungeon.player.loseGold(this.fullServiceCost);
/* 264 */         this.option = OptionChosen.REMOVE_AND_UPGRADE;
/* 265 */         AbstractDungeon.gridSelectScreen.open(
/* 266 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 267 */           .getPurgeableCards()), 1, OPTIONS[17], false, false, false, true);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 274 */         break;
/*     */       
/*     */ 
/*     */       case 3: 
/* 278 */         this.imageEventText.loadImage("images/events/designerPunched2.jpg");
/* 279 */         this.imageEventText.updateBodyText(DESC[3]);
/* 280 */         logMetric("Punch");
/* 281 */         CardCrawlGame.sound.play("BLUNT_FAST");
/* 282 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, this.hpLoss, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS));
/* 283 */         break;
/*     */       }
/*     */       
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 290 */       this.imageEventText.updateDialogOption(0, OPTIONS[14]);
/* 291 */       this.imageEventText.clearRemainingOptions();
/* 292 */       this.curScreen = CurrentScreen.DONE;
/* 293 */       break;
/*     */     
/*     */     case DONE: 
/*     */     default: 
/* 297 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void upgradeTwoRandomCards()
/*     */   {
/* 303 */     ArrayList<AbstractCard> upgradableCards = new ArrayList();
/* 304 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/* 305 */       if (c.canUpgrade()) {
/* 306 */         upgradableCards.add(c);
/*     */       }
/*     */     }
/*     */     
/* 310 */     java.util.Collections.shuffle(upgradableCards, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/* 312 */     if (!upgradableCards.isEmpty())
/*     */     {
/* 314 */       if (upgradableCards.size() == 1) {
/* 315 */         ((AbstractCard)upgradableCards.get(0)).upgrade();
/* 316 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(0));
/* 317 */         AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect(
/* 318 */           ((AbstractCard)upgradableCards.get(0)).makeStatEquivalentCopy()));
/* 319 */         AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       } else {
/* 321 */         ((AbstractCard)upgradableCards.get(0)).upgrade();
/* 322 */         ((AbstractCard)upgradableCards.get(1)).upgrade();
/* 323 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(0));
/* 324 */         AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(1));
/* 325 */         AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect(
/*     */         
/* 327 */           ((AbstractCard)upgradableCards.get(0)).makeStatEquivalentCopy(), Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH / 2.0F - 20.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/* 330 */         AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect(
/*     */         
/* 332 */           ((AbstractCard)upgradableCards.get(1)).makeStatEquivalentCopy(), Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH / 2.0F + 20.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */         
/*     */ 
/* 335 */         AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       } }
/*     */   }
/*     */   
/*     */   private void upgradeRandomCard() {
/* 340 */     ArrayList<AbstractCard> upgradableCards = new ArrayList();
/* 341 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/* 342 */       if (c.canUpgrade()) {
/* 343 */         upgradableCards.add(c);
/*     */       }
/*     */     }
/*     */     
/* 347 */     java.util.Collections.shuffle(upgradableCards, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/* 349 */     if (!upgradableCards.isEmpty()) {
/* 350 */       ((AbstractCard)upgradableCards.get(0)).upgrade();
/* 351 */       AbstractDungeon.player.bottledCardUpgradeCheck((AbstractCard)upgradableCards.get(0));
/* 352 */       AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect(
/* 353 */         ((AbstractCard)upgradableCards.get(0)).makeStatEquivalentCopy()));
/* 354 */       AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 359 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Designer", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\Designer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */