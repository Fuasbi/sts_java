/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Bonfire extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Bonfire Elementals";
/*  20 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Bonfire Elementals");
/*  21 */   public static final String NAME = eventStrings.NAME;
/*  22 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  23 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  25 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  26 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  27 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*     */   
/*  29 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*  30 */   private AbstractCard offeredCard = null;
/*  31 */   private boolean cardSelect = false;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  34 */     INTRO,  CHOOSE,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  38 */   public Bonfire() { super(NAME, DIALOG_1, "images/events/bonfire.jpg");
/*  39 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  44 */     if (Settings.AMBIANCE_ON) {
/*  45 */       CardCrawlGame.sound.play("EVENT_GOOP");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  51 */     super.update();
/*     */     
/*  53 */     if ((this.cardSelect) && 
/*  54 */       (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  55 */       this.offeredCard = ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.remove(0));
/*  56 */       setReward(this.offeredCard.rarity);
/*  57 */       logMetric(this.offeredCard.rarity.toString());
/*  58 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(this.offeredCard, Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */       
/*  60 */       AbstractDungeon.player.masterDeck.removeCard(this.offeredCard);
/*  61 */       this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/*  62 */       this.screen = CUR_SCREEN.COMPLETE;
/*  63 */       this.cardSelect = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  70 */     switch (this.screen)
/*     */     {
/*     */     case INTRO: 
/*  73 */       this.imageEventText.updateBodyText(DIALOG_2);
/*  74 */       this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/*  75 */       this.screen = CUR_SCREEN.CHOOSE;
/*  76 */       break;
/*     */     case CHOOSE: 
/*  78 */       AbstractDungeon.gridSelectScreen.open(
/*  79 */         CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck.getPurgeableCards()), 1, OPTIONS[3], false, false, false, true);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  86 */       this.cardSelect = true;
/*  87 */       break;
/*     */     case COMPLETE: 
/*  89 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void setReward(AbstractCard.CardRarity rarity)
/*     */   {
/*  95 */     String dialog = DIALOG_3;
/*  96 */     switch (rarity) {
/*     */     case CURSE: 
/*  98 */       dialog = dialog + DESCRIPTIONS[3];
/*  99 */       if (!AbstractDungeon.player.hasRelic("Spirit Poop")) {
/* 100 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 
/*     */         
/*     */ 
/* 103 */           com.megacrit.cardcrawl.helpers.RelicLibrary.getRelic("Spirit Poop").makeCopy());
/*     */       } else {
/* 105 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(this.drawX, this.drawY, new com.megacrit.cardcrawl.relics.Circlet());
/*     */       }
/* 107 */       break;
/*     */     case BASIC: 
/* 109 */       dialog = dialog + DESCRIPTIONS[4];
/* 110 */       break;
/*     */     case COMMON: 
/* 112 */       dialog = dialog + DESCRIPTIONS[5];
/* 113 */       AbstractDungeon.player.heal(5);
/* 114 */       break;
/*     */     case UNCOMMON: 
/* 116 */       dialog = dialog + DESCRIPTIONS[6];
/* 117 */       AbstractDungeon.player.heal(AbstractDungeon.player.maxHealth);
/* 118 */       break;
/*     */     case RARE: 
/* 120 */       dialog = dialog + DESCRIPTIONS[7];
/* 121 */       AbstractDungeon.player.increaseMaxHp(10, false);
/* 122 */       AbstractDungeon.player.heal(AbstractDungeon.player.maxHealth);
/* 123 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 128 */     this.imageEventText.updateBodyText(dialog);
/*     */   }
/*     */   
/*     */   public void logMetric(String cardGiven) {
/* 132 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Bonfire Elementals", cardGiven);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\Bonfire.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */