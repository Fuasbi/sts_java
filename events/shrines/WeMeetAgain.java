/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class WeMeetAgain extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "WeMeetAgain";
/*  23 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("WeMeetAgain");
/*  24 */   public static final String NAME = eventStrings.NAME;
/*  25 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  26 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   private static final int MAX_GOLD = 150;
/*  29 */   private AbstractPotion potionOption = null;
/*  30 */   private AbstractCard cardOption = null;
/*  31 */   private int goldAmount = 0;
/*     */   
/*  33 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  34 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  37 */     INTRO,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  41 */   public WeMeetAgain() { super(NAME, DIALOG_1, "images/events/weMeetAgain.jpg");
/*     */     
/*     */ 
/*  44 */     this.potionOption = AbstractDungeon.player.getRandomPotion();
/*  45 */     if (this.potionOption != null) {
/*  46 */       this.imageEventText.setDialogOption(OPTIONS[0] + FontHelper.colorString(this.potionOption.name, "r") + OPTIONS[6]);
/*     */     } else {
/*  48 */       this.imageEventText.setDialogOption(OPTIONS[1], true);
/*     */     }
/*     */     
/*     */ 
/*  52 */     this.goldAmount = getGoldAmount();
/*  53 */     if (this.goldAmount != 0) {
/*  54 */       this.imageEventText.setDialogOption(OPTIONS[2] + this.goldAmount + OPTIONS[9] + OPTIONS[6]);
/*     */     } else {
/*  56 */       this.imageEventText.setDialogOption(OPTIONS[3], true);
/*     */     }
/*     */     
/*     */ 
/*  60 */     this.cardOption = getRandomNonBasicCard();
/*  61 */     if (this.cardOption != null) {
/*  62 */       this.imageEventText.setDialogOption(OPTIONS[4] + this.cardOption.name + OPTIONS[6], this.cardOption
/*     */       
/*  64 */         .makeStatEquivalentCopy());
/*     */     } else {
/*  66 */       this.imageEventText.setDialogOption(OPTIONS[5], true);
/*     */     }
/*     */     
/*     */ 
/*  70 */     this.imageEventText.setDialogOption(OPTIONS[7]);
/*     */   }
/*     */   
/*     */   private AbstractCard getRandomNonBasicCard() {
/*  74 */     ArrayList<AbstractCard> list = new ArrayList();
/*  75 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  76 */       if ((c.rarity != com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.BASIC) && (c.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.CURSE)) {
/*  77 */         list.add(c);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  82 */     if (list.isEmpty()) {
/*  83 */       return null;
/*     */     }
/*     */     
/*  86 */     java.util.Collections.shuffle(list, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*  87 */     return (AbstractCard)list.get(0);
/*     */   }
/*     */   
/*     */   private int getGoldAmount() {
/*  91 */     if (AbstractDungeon.player.gold < 50) {
/*  92 */       return 0;
/*     */     }
/*  94 */     if (AbstractDungeon.player.gold > 150) {
/*  95 */       return AbstractDungeon.miscRng.random(50, 150);
/*     */     }
/*  97 */     return AbstractDungeon.miscRng.random(50, AbstractDungeon.player.gold);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 104 */     switch (this.screen)
/*     */     {
/*     */     case INTRO: 
/* 107 */       this.screen = CUR_SCREEN.COMPLETE;
/* 108 */       switch (buttonPressed)
/*     */       {
/*     */       case 0: 
/* 111 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1] + DESCRIPTIONS[5]);
/* 112 */         AbstractDungeon.player.removePotion(this.potionOption);
/* 113 */         relicReward();
/* 114 */         logMetric("Potion");
/* 115 */         break;
/*     */       
/*     */       case 1: 
/* 118 */         this.imageEventText.updateBodyText(DESCRIPTIONS[2] + DESCRIPTIONS[5]);
/* 119 */         AbstractDungeon.player.loseGold(this.goldAmount);
/* 120 */         relicReward();
/* 121 */         logMetric("Gold");
/* 122 */         break;
/*     */       
/*     */       case 2: 
/* 125 */         this.imageEventText.updateBodyText(DESCRIPTIONS[3] + DESCRIPTIONS[5]);
/* 126 */         AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(this.cardOption, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */         
/* 128 */         AbstractDungeon.player.masterDeck.removeCard(this.cardOption);
/* 129 */         relicReward();
/* 130 */         logMetric("Card");
/* 131 */         break;
/*     */       
/*     */       case 3: 
/* 134 */         this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/* 135 */         CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/* 136 */         CardCrawlGame.sound.play("BLUNT_HEAVY");
/* 137 */         logMetric("Attack");
/* 138 */         break;
/*     */       }
/*     */       
/*     */       
/*     */ 
/*     */ 
/* 144 */       this.imageEventText.updateDialogOption(0, OPTIONS[8]);
/* 145 */       this.imageEventText.clearRemainingOptions();
/* 146 */       break;
/*     */     case COMPLETE: 
/* 148 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void relicReward()
/*     */   {
/* 154 */     AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH * 0.28F, Settings.HEIGHT / 2.0F, 
/*     */     
/*     */ 
/* 157 */       AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier()));
/*     */   }
/*     */   
/*     */   public void logMetric(String result) {
/* 161 */     AbstractEvent.logMetric("WeMeetAgain", result);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\WeMeetAgain.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */