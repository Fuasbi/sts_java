/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class FountainOfCurseRemoval extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Fountain of Cleansing";
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Fountain of Cleansing");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 24 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*    */   
/* 26 */   private int screenNum = 0;
/*    */   
/*    */   public FountainOfCurseRemoval() {
/* 29 */     super(NAME, DIALOG_1, "images/events/fountain.jpg");
/*    */     
/* 31 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/* 32 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void onEnterRoom()
/*    */   {
/* 38 */     CardCrawlGame.music.playTempBGM("SHRINE");
/* 39 */     if (com.megacrit.cardcrawl.core.Settings.AMBIANCE_ON) {
/* 40 */       CardCrawlGame.sound.play("EVENT_FOUNTAIN");
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 46 */     switch (this.screenNum)
/*    */     {
/*    */     case 0: 
/* 49 */       switch (buttonPressed) {
/*    */       case 0: 
/* 51 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 52 */         this.screenNum = 1;
/*    */         
/* 54 */         for (int i = AbstractDungeon.player.masterDeck.group.size() - 1; i >= 0; i--)
/*    */         {
/* 56 */           if ((((AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)).type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.CURSE) && 
/* 57 */             (!((AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)).inBottleFlame) && 
/* 58 */             (!((AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)).inBottleLightning) && 
/* 59 */             (((AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)).cardID != "AscendersBane") && 
/* 60 */             (((AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)).cardID != "Necronomicurse"))
/*    */           {
/* 62 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/* 63 */               (AbstractCard)AbstractDungeon.player.masterDeck.group.get(i)));
/* 64 */             AbstractDungeon.player.masterDeck.removeCard(
/* 65 */               (AbstractCard)AbstractDungeon.player.masterDeck.group.get(i));
/*    */           }
/*    */         }
/*    */         
/* 69 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 70 */         this.imageEventText.clearRemainingOptions();
/* 71 */         logMetric("Removed Curse");
/* 72 */         break;
/*    */       default: 
/* 74 */         logMetric("Ignored");
/* 75 */         this.imageEventText.updateBodyText(DIALOG_3);
/* 76 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 77 */         this.imageEventText.clearRemainingOptions();
/* 78 */         this.screenNum = 1; }
/* 79 */       break;
/*    */     
/*    */ 
/*    */ 
/*    */     case 1: 
/* 84 */       openMap();
/* 85 */       break;
/*    */     
/*    */     default: 
/* 88 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String cardGiven)
/*    */   {
/* 94 */     AbstractEvent.logMetric("Fountain of Cleansing", cardGiven);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\FountainOfCurseRemoval.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */