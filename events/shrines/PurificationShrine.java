/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class PurificationShrine extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Purifier";
/* 15 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Purifier");
/* 16 */   public static final String NAME = eventStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 20 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 21 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 22 */   private static final String IGNORE = DESCRIPTIONS[2];
/* 23 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*    */   
/*    */   private static enum CUR_SCREEN {
/* 26 */     INTRO,  COMPLETE;
/*    */     
/*    */     private CUR_SCREEN() {} }
/*    */   
/* 30 */   public PurificationShrine() { super(NAME, DIALOG_1, "images/events/shrine3.jpg");
/* 31 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/* 32 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 37 */     CardCrawlGame.music.playTempBGM("SHRINE");
/*    */   }
/*    */   
/*    */   public void update() {
/* 41 */     super.update();
/* 42 */     if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 43 */       CardCrawlGame.sound.play("CARD_EXHAUST");
/* 44 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(
/*    */       
/* 46 */         (com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), com.megacrit.cardcrawl.core.Settings.WIDTH / 2, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2));
/*    */       
/*    */ 
/* 49 */       AbstractDungeon.player.masterDeck.removeCard((com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 50 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 56 */     switch (this.screen)
/*    */     {
/*    */     case INTRO: 
/* 59 */       switch (buttonPressed) {
/*    */       case 0: 
/* 61 */         this.screen = CUR_SCREEN.COMPLETE;
/* 62 */         logMetric("Purifier", "One Purge");
/* 63 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 64 */         AbstractDungeon.gridSelectScreen.open(
/* 65 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 66 */           .getPurgeableCards()), 1, OPTIONS[2], false, false, false, true);
/*    */         
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 73 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 74 */         this.imageEventText.clearRemainingOptions();
/* 75 */         break;
/*    */       case 1: 
/* 77 */         this.screen = CUR_SCREEN.COMPLETE;
/* 78 */         logMetric("Purifier", "Skipped");
/* 79 */         this.imageEventText.updateBodyText(IGNORE);
/* 80 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 81 */         this.imageEventText.clearRemainingOptions();
/*    */       }
/*    */       
/* 84 */       break;
/*    */     case COMPLETE: 
/* 86 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String result)
/*    */   {
/* 92 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Purifier", result);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\PurificationShrine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */