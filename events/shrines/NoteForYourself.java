/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class NoteForYourself extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "NoteForYourself";
/*  18 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("NoteForYourself");
/*  19 */   public static final String NAME = eventStrings.NAME;
/*  20 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  21 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*  22 */   private AbstractCard obtainCard = null;
/*  23 */   public AbstractCard saveCard = null;
/*  24 */   private boolean cardSelect = false;
/*     */   
/*  26 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  27 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  30 */     INTRO,  CHOOSE,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  34 */   public NoteForYourself() { super(NAME, DIALOG_1, "images/events/selfNote.jpg");
/*  35 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  36 */     initializeObtainCard();
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  41 */     switch (this.screen)
/*     */     {
/*     */     case INTRO: 
/*  44 */       this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/*  45 */       this.screen = CUR_SCREEN.CHOOSE;
/*  46 */       this.imageEventText.updateDialogOption(0, OPTIONS[1] + this.obtainCard.name + OPTIONS[2], this.obtainCard);
/*  47 */       this.imageEventText.setDialogOption(OPTIONS[3]);
/*  48 */       break;
/*     */     case CHOOSE: 
/*  50 */       this.screen = CUR_SCREEN.COMPLETE;
/*  51 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/*  52 */       switch (buttonPressed) {
/*     */       case 0: 
/*  54 */         obtainCard();
/*  55 */         storeCard();
/*  56 */         logMetric("Took Card");
/*  57 */         break;
/*     */       default: 
/*  59 */         logMetric("Ignored");
/*     */       }
/*     */       
/*     */       
/*  63 */       this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/*  64 */       this.imageEventText.updateDialogOption(0, OPTIONS[4]);
/*  65 */       this.imageEventText.clearRemainingOptions();
/*  66 */       this.screen = CUR_SCREEN.COMPLETE;
/*  67 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/*  68 */       break;
/*     */     case COMPLETE: 
/*  70 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private void obtainCard()
/*     */   {
/*  76 */     AbstractDungeon.player.masterDeck.addToTop(this.obtainCard);
/*     */     
/*  78 */     for (com.megacrit.cardcrawl.relics.AbstractRelic r : AbstractDungeon.player.relics) {
/*  79 */       r.onMasterDeckChange();
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  85 */     super.update();
/*     */     
/*  87 */     if ((this.cardSelect) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/*  88 */       AbstractCard storeCard = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.remove(0);
/*  89 */       AbstractDungeon.player.masterDeck.removeCard(storeCard);
/*  90 */       this.saveCard = storeCard;
/*  91 */       this.cardSelect = false;
/*     */     }
/*     */   }
/*     */   
/*     */   private void initializeObtainCard() {
/*  96 */     this.obtainCard = com.megacrit.cardcrawl.helpers.CardLibrary.getCard(CardCrawlGame.playerPref.getString("NOTE_CARD", "Iron Wave")).makeCopy();
/*  97 */     for (int i = 0; i < CardCrawlGame.playerPref.getInteger("NOTE_UPGRADE", 0); i++) {
/*  98 */       this.obtainCard.upgrade();
/*     */     }
/*     */   }
/*     */   
/*     */   private void storeCard() {
/* 103 */     this.cardSelect = true;
/* 104 */     AbstractDungeon.gridSelectScreen.open(
/* 105 */       CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck.getPurgeableCards()), 1, DESCRIPTIONS[2], false, false, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void logMetric(String result)
/*     */   {
/* 115 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("NoteForYourself", result);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\NoteForYourself.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */