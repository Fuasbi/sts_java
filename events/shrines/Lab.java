/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.helpers.PotionHelper;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Lab extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Lab";
/* 17 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Lab");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*    */   
/*    */   private static enum CUR_SCREEN {
/* 26 */     INTRO,  COMPLETE;
/*    */     
/*    */     private CUR_SCREEN() {} }
/*    */   
/* 30 */   public Lab() { super(NAME, DIALOG_1, "images/events/lab.jpg");
/* 31 */     this.noCardsInRewards = true;
/* 32 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 37 */     if (com.megacrit.cardcrawl.core.Settings.AMBIANCE_ON) {
/* 38 */       CardCrawlGame.sound.play("EVENT_LAB");
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 44 */     switch (this.screen)
/*    */     {
/*    */     case INTRO: 
/* 47 */       GenericEventDialog.hide();
/* 48 */       AbstractDungeon.getCurrRoom().rewards.clear();
/*    */       
/* 50 */       AbstractDungeon.getCurrRoom().rewards.add(new RewardItem(PotionHelper.getRandomPotion()));
/* 51 */       AbstractDungeon.getCurrRoom().rewards.add(new RewardItem(PotionHelper.getRandomPotion()));
/* 52 */       if (AbstractDungeon.ascensionLevel < 15) {
/* 53 */         AbstractDungeon.getCurrRoom().rewards.add(new RewardItem(PotionHelper.getRandomPotion()));
/*    */       }
/*    */       
/* 56 */       this.screen = CUR_SCREEN.COMPLETE;
/* 57 */       AbstractDungeon.getCurrRoom().phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/* 58 */       AbstractDungeon.combatRewardScreen.open();
/* 59 */       logMetric("The Lab", "Got Potions");
/* 60 */       break;
/*    */     case COMPLETE: 
/* 62 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String result)
/*    */   {
/* 68 */     AbstractEvent.logMetric("Lab", result);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\Lab.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */