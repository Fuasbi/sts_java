/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Transmogrifier extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Transmorgrifier";
/* 15 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Transmorgrifier");
/* 16 */   public static final String NAME = eventStrings.NAME;
/* 17 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 18 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 20 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 21 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 22 */   private static final String IGNORE = DESCRIPTIONS[2];
/* 23 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*    */   
/*    */   private static enum CUR_SCREEN {
/* 26 */     INTRO,  COMPLETE;
/*    */     
/*    */     private CUR_SCREEN() {} }
/*    */   
/* 30 */   public Transmogrifier() { super(NAME, DIALOG_1, "images/events/shrine1.jpg");
/* 31 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/* 32 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 37 */     CardCrawlGame.music.playTempBGM("SHRINE");
/*    */   }
/*    */   
/*    */   public void update() {
/* 41 */     super.update();
/* 42 */     if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 43 */       com.megacrit.cardcrawl.cards.AbstractCard c = (com.megacrit.cardcrawl.cards.AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 44 */       AbstractDungeon.player.masterDeck.removeCard(c);
/* 45 */       AbstractDungeon.transformCard(c);
/* 46 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*    */       
/* 48 */         AbstractDungeon.getTransformedCard(), com.megacrit.cardcrawl.core.Settings.WIDTH / 2.0F, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2.0F));
/*    */       
/*    */ 
/* 51 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 57 */     switch (this.screen)
/*    */     {
/*    */     case INTRO: 
/* 60 */       switch (buttonPressed) {
/*    */       case 0: 
/* 62 */         this.screen = CUR_SCREEN.COMPLETE;
/* 63 */         logMetric("Transmorgrifier", "Transform");
/* 64 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 65 */         AbstractDungeon.gridSelectScreen.open(
/* 66 */           CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 67 */           .getPurgeableCards()), 1, OPTIONS[2], false, true, false, false);
/*    */         
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 74 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 75 */         this.imageEventText.clearRemainingOptions();
/* 76 */         break;
/*    */       case 1: 
/* 78 */         this.screen = CUR_SCREEN.COMPLETE;
/* 79 */         logMetric("Transmorgrifier", "Skipped");
/* 80 */         this.imageEventText.updateBodyText(IGNORE);
/* 81 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 82 */         this.imageEventText.clearRemainingOptions();
/*    */       }
/*    */       
/* 85 */       break;
/*    */     case COMPLETE: 
/* 87 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String result)
/*    */   {
/* 93 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Transmorgrifier", result);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\Transmogrifier.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */