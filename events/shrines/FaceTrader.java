/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class FaceTrader extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "FaceTrader";
/*  20 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("FaceTrader");
/*  21 */   public static final String NAME = eventStrings.NAME;
/*  22 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  23 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   private static int goldReward;
/*     */   private static int damage;
/*  26 */   private CurScreen screen = CurScreen.INTRO;
/*     */   
/*     */   private static enum CurScreen {
/*  29 */     INTRO,  MAIN,  RESULT;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/*  33 */   public FaceTrader() { super(NAME, DESCRIPTIONS[0], "images/events/facelessTrader.jpg");
/*     */     
/*  35 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  36 */       goldReward = 50;
/*     */     } else {
/*  38 */       goldReward = 75;
/*     */     }
/*     */     
/*  41 */     damage = AbstractDungeon.player.maxHealth / 10;
/*  42 */     if (damage == 0) {
/*  43 */       damage = 1;
/*     */     }
/*     */     
/*  46 */     this.imageEventText.setDialogOption(OPTIONS[4]);
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  51 */     switch (this.screen) {
/*     */     case INTRO: 
/*  53 */       switch (buttonPressed) {
/*     */       case 0: 
/*  55 */         this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/*  56 */         this.imageEventText.updateDialogOption(0, OPTIONS[0] + damage + OPTIONS[5] + goldReward + OPTIONS[1]);
/*     */         
/*     */ 
/*  59 */         this.imageEventText.setDialogOption(OPTIONS[2]);
/*  60 */         this.imageEventText.setDialogOption(OPTIONS[3]);
/*  61 */         this.screen = CurScreen.MAIN;
/*     */       }
/*     */       
/*  64 */       break;
/*     */     case MAIN: 
/*  66 */       switch (buttonPressed)
/*     */       {
/*     */       case 0: 
/*  69 */         logMetric("Touch");
/*  70 */         this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/*  71 */         AbstractDungeon.effectList.add(new RainingGoldEffect(goldReward));
/*  72 */         AbstractDungeon.player.gainGold(goldReward);
/*     */         
/*  74 */         AbstractDungeon.player.damage(new com.megacrit.cardcrawl.cards.DamageInfo(null, damage));
/*  75 */         CardCrawlGame.sound.play("ATTACK_POISON");
/*  76 */         break;
/*     */       
/*     */ 
/*     */       case 1: 
/*  80 */         logMetric("Trade");
/*     */         
/*  82 */         AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 
/*     */         
/*     */ 
/*  85 */           getRandomFace());
/*  86 */         this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/*  87 */         break;
/*     */       
/*     */ 
/*     */       case 2: 
/*  91 */         logMetric("Leave");
/*  92 */         this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/*  93 */         break;
/*     */       }
/*     */       
/*     */       
/*  97 */       this.imageEventText.clearAllDialogs();
/*  98 */       this.imageEventText.setDialogOption(OPTIONS[3]);
/*  99 */       this.screen = CurScreen.RESULT;
/* 100 */       break;
/*     */     default: 
/* 102 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   private AbstractRelic getRandomFace()
/*     */   {
/* 108 */     ArrayList<String> ids = new ArrayList();
/* 109 */     if (!AbstractDungeon.player.hasRelic("CultistMask")) {
/* 110 */       ids.add("CultistMask");
/*     */     }
/* 112 */     if (!AbstractDungeon.player.hasRelic("FaceOfCleric")) {
/* 113 */       ids.add("FaceOfCleric");
/*     */     }
/* 115 */     if (!AbstractDungeon.player.hasRelic("GremlinMask")) {
/* 116 */       ids.add("GremlinMask");
/*     */     }
/* 118 */     if (!AbstractDungeon.player.hasRelic("NlothsMask")) {
/* 119 */       ids.add("NlothsMask");
/*     */     }
/* 121 */     if (!AbstractDungeon.player.hasRelic("SsserpentHead")) {
/* 122 */       ids.add("SsserpentHead");
/*     */     }
/* 124 */     if (ids.size() <= 0) {
/* 125 */       ids.add("Circlet");
/*     */     }
/*     */     
/* 128 */     java.util.Collections.shuffle(ids, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/* 129 */     return com.megacrit.cardcrawl.helpers.RelicLibrary.getRelic((String)ids.get(0)).makeCopy();
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 133 */     AbstractEvent.logMetric("FaceTrader", actionTaken);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\FaceTrader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */