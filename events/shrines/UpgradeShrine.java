/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class UpgradeShrine extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Upgrade Shrine";
/* 17 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Upgrade Shrine");
/* 18 */   public static final String NAME = eventStrings.NAME;
/* 19 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 20 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/* 22 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 23 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 24 */   private static final String IGNORE = DESCRIPTIONS[2];
/*    */   
/* 26 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*    */   
/*    */   private static enum CUR_SCREEN {
/* 29 */     INTRO,  COMPLETE;
/*    */     
/*    */     private CUR_SCREEN() {} }
/*    */   
/* 33 */   public UpgradeShrine() { super(NAME, DIALOG_1, "images/events/shrine2.jpg");
/* 34 */     if (AbstractDungeon.player.masterDeck.hasUpgradableCards().booleanValue()) {
/* 35 */       this.imageEventText.setDialogOption(OPTIONS[0]);
/*    */     } else {
/* 37 */       this.imageEventText.setDialogOption(OPTIONS[3], true);
/*    */     }
/* 39 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 44 */     com.megacrit.cardcrawl.core.CardCrawlGame.music.playTempBGM("SHRINE");
/*    */   }
/*    */   
/*    */   public void update() {
/* 48 */     super.update();
/* 49 */     if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 50 */       AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 51 */       c.upgrade();
/* 52 */       AbstractDungeon.player.bottledCardUpgradeCheck(c);
/* 53 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect(c.makeStatEquivalentCopy()));
/* 54 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/* 55 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 61 */     switch (this.screen)
/*    */     {
/*    */     case INTRO: 
/* 64 */       switch (buttonPressed) {
/*    */       case 0: 
/* 66 */         this.screen = CUR_SCREEN.COMPLETE;
/* 67 */         AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 68 */         logMetric("Upgrade Shrine", "Upgraded");
/* 69 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 70 */         AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 71 */           .getUpgradableCards(), 1, OPTIONS[2], true, false, false, false);
/*    */         
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 78 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 79 */         this.imageEventText.clearRemainingOptions();
/* 80 */         break;
/*    */       case 1: 
/* 82 */         this.screen = CUR_SCREEN.COMPLETE;
/* 83 */         AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 84 */         logMetric("Upgrade Shrine", "Skipped");
/* 85 */         this.imageEventText.updateBodyText(IGNORE);
/* 86 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 87 */         this.imageEventText.clearRemainingOptions();
/*    */       }
/*    */       
/* 90 */       break;
/*    */     case COMPLETE: 
/* 92 */       openMap();
/*    */     }
/*    */   }
/*    */   
/*    */   public void logMetric(String result)
/*    */   {
/* 98 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Upgrade Shrine", result);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\UpgradeShrine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */