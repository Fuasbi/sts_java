/*    */ package com.megacrit.cardcrawl.events.shrines;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*    */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*    */ import com.megacrit.cardcrawl.localization.EventStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Duplicator extends AbstractImageEvent
/*    */ {
/*    */   public static final String ID = "Duplicator";
/* 14 */   private static final EventStrings eventStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getEventString("Duplicator");
/* 15 */   public static final String NAME = eventStrings.NAME;
/* 16 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/* 17 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*    */   
/*    */ 
/* 20 */   private int screenNum = 0;
/* 21 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/* 22 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/* 23 */   private static final String IGNORE = DESCRIPTIONS[2];
/*    */   
/*    */   public Duplicator() {
/* 26 */     super(NAME, DIALOG_1, "images/events/shrine4.jpg");
/* 27 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/* 28 */     this.imageEventText.setDialogOption(OPTIONS[1]);
/*    */   }
/*    */   
/*    */   public void onEnterRoom()
/*    */   {
/* 33 */     com.megacrit.cardcrawl.core.CardCrawlGame.music.playTempBGM("SHRINE");
/*    */   }
/*    */   
/*    */   public void update() {
/* 37 */     super.update();
/* 38 */     if ((!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) && 
/* 39 */       (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 40 */       AbstractCard c = ((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0)).makeStatEquivalentCopy();
/* 41 */       c.inBottleFlame = false;
/* 42 */       c.inBottleLightning = false;
/* 43 */       c.inBottleTornado = false;
/* 44 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(c, com.megacrit.cardcrawl.core.Settings.WIDTH / 2.0F, com.megacrit.cardcrawl.core.Settings.HEIGHT / 2.0F));
/*    */       
/* 46 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   protected void buttonEffect(int buttonPressed)
/*    */   {
/* 53 */     switch (this.screenNum) {
/*    */     case 0: 
/* 55 */       switch (buttonPressed) {
/*    */       case 0: 
/* 57 */         this.imageEventText.updateBodyText(DIALOG_2);
/* 58 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 59 */         this.imageEventText.clearRemainingOptions();
/* 60 */         use();
/* 61 */         this.screenNum = 2;
/* 62 */         logMetric("One dupe");
/* 63 */         break;
/*    */       case 1: 
/* 65 */         this.screenNum = 2;
/* 66 */         this.imageEventText.updateBodyText(IGNORE);
/* 67 */         this.imageEventText.updateDialogOption(0, OPTIONS[1]);
/* 68 */         this.imageEventText.clearRemainingOptions();
/* 69 */         logMetric("Ignored");
/*    */       }
/*    */       
/* 72 */       break;
/*    */     case 1: 
/* 74 */       this.screenNum = 2;
/* 75 */       break;
/*    */     case 2: 
/* 77 */       openMap();
/* 78 */       break;
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   public void use()
/*    */   {
/* 85 */     AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck, 1, OPTIONS[2], false, false, false, false);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void logMetric(String result)
/*    */   {
/* 96 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Duplicator", result);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\Duplicator.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */