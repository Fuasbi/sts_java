/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.ElasticIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Objects;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class GremlinWheelGame extends AbstractImageEvent
/*     */ {
/*  43 */   private static final Logger logger = LogManager.getLogger(GremlinWheelGame.class.getName());
/*     */   public static final String ID = "Wheel of Change";
/*  45 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Wheel of Change");
/*  46 */   public static final String NAME = eventStrings.NAME;
/*  47 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  48 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*  50 */   public static final String INTRO_DIALOG = DESCRIPTIONS[0];
/*     */   
/*     */ 
/*  53 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   private int result;
/*     */   private float resultAngle;
/*  56 */   private float tmpAngle; private boolean startSpin = false; private boolean finishSpin = false; private boolean doneSpinning = false; private boolean bounceIn = true;
/*  57 */   private float bounceTimer = 1.0F; private float animTimer = 3.0F;
/*  58 */   private float spinVelocity = 200.0F;
/*     */   private int goldAmount;
/*  60 */   private boolean purgeResult = false; private boolean buttonPressed = false;
/*  61 */   private Hitbox buttonHb = new Hitbox(450.0F * Settings.scale, 300.0F * Settings.scale);
/*     */   private Texture wheelImg;
/*     */   private Texture arrowImg;
/*     */   private Texture buttonImg;
/*  65 */   private static final float START_Y = Settings.OPTION_Y + 1000.0F * Settings.scale; private static final float TARGET_Y = Settings.OPTION_Y;
/*  66 */   private float imgX = Settings.WIDTH / 2.0F; private float imgY = START_Y; private float wheelAngle = 0.0F;
/*     */   private static final int WHEEL_W = 1024;
/*     */   private static final int ARROW_W = 512;
/*  69 */   private static final float ARROW_OFFSET_X = 300.0F * Settings.scale;
/*  70 */   private Color color = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*     */   
/*  72 */   private float hpLossPercent = 0.1F;
/*     */   
/*     */   private static final float A_2_HP_LOSS = 0.15F;
/*     */   
/*     */ 
/*     */   public GremlinWheelGame()
/*     */   {
/*  79 */     super(NAME, INTRO_DIALOG, "images/events/spinTheWheel.jpg");
/*  80 */     this.wheelImg = ImageMaster.loadImage("images/events/wheel.png");
/*  81 */     this.arrowImg = ImageMaster.loadImage("images/events/wheelArrow.png");
/*  82 */     this.buttonImg = ImageMaster.loadImage("images/events/spinButton.png");
/*  83 */     this.noCardsInRewards = true;
/*     */     
/*  85 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  86 */       this.hpLossPercent = 0.15F;
/*     */     }
/*     */     
/*  89 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*  90 */     setGold();
/*     */     
/*  92 */     this.hasDialog = true;
/*  93 */     this.hasFocus = true;
/*     */     
/*  95 */     this.buttonHb.move(500.0F * Settings.scale, -500.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   private void setGold() {
/*  99 */     if (Objects.equals(AbstractDungeon.id, "Exordium")) {
/* 100 */       this.goldAmount = 100;
/* 101 */     } else if (Objects.equals(AbstractDungeon.id, "TheCity")) {
/* 102 */       this.goldAmount = 200;
/* 103 */     } else if (Objects.equals(AbstractDungeon.id, "TheBeyond")) {
/* 104 */       this.goldAmount = 300;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static enum CUR_SCREEN
/*     */   {
/* 112 */     INTRO,  LEAVE,  SPIN,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {}
/*     */   }
/*     */   
/* 117 */   public void update() { super.update();
/* 118 */     updatePosition();
/* 119 */     purgeLogic();
/* 120 */     if ((this.bounceTimer == 0.0F) && (this.startSpin)) {
/* 121 */       if (!this.buttonPressed) {
/* 122 */         this.buttonHb.cY = MathHelper.cardLerpSnap(this.buttonHb.cY, 50.0F * Settings.scale);
/* 123 */         this.buttonHb.move(this.buttonHb.cX, this.buttonHb.cY);
/* 124 */         this.buttonHb.update();
/* 125 */         if (((this.buttonHb.hovered) && (InputHelper.justClickedLeft)) || (CInputActionSet.proceed.isJustPressed())) {
/* 126 */           this.buttonPressed = true;
/* 127 */           this.buttonHb.hovered = false;
/* 128 */           CardCrawlGame.sound.play("WHEEL");
/*     */         }
/*     */       } else {
/* 131 */         this.buttonHb.cY = MathHelper.cardLerpSnap(this.buttonHb.cY, -500.0F * Settings.scale);
/*     */       }
/*     */     }
/*     */     
/* 135 */     if ((this.startSpin) && (this.bounceTimer == 0.0F) && (this.buttonPressed)) {
/* 136 */       this.imgY = TARGET_Y;
/* 137 */       if (this.animTimer > 0.0F) {
/* 138 */         this.animTimer -= Gdx.graphics.getDeltaTime();
/* 139 */         this.wheelAngle += this.spinVelocity * Gdx.graphics.getDeltaTime();
/*     */       } else {
/* 141 */         this.finishSpin = true;
/* 142 */         this.animTimer = 3.0F;
/* 143 */         this.startSpin = false;
/* 144 */         this.tmpAngle = this.resultAngle;
/*     */       }
/* 146 */     } else if (this.finishSpin) {
/* 147 */       if (this.animTimer > 0.0F) {
/* 148 */         this.animTimer -= Gdx.graphics.getDeltaTime();
/* 149 */         if (this.animTimer < 0.0F) {
/* 150 */           this.animTimer = 1.0F;
/* 151 */           this.finishSpin = false;
/* 152 */           this.doneSpinning = true;
/*     */         }
/* 154 */         this.wheelAngle = Interpolation.elasticIn.apply(this.tmpAngle, -180.0F, this.animTimer / 3.0F);
/*     */       }
/* 156 */     } else if (this.doneSpinning) {
/* 157 */       if (this.animTimer > 0.0F) {
/* 158 */         this.animTimer -= Gdx.graphics.getDeltaTime();
/* 159 */         if (this.animTimer <= 0.0F) {
/* 160 */           this.bounceTimer = 1.0F;
/* 161 */           this.bounceIn = false;
/*     */         }
/* 163 */       } else if (this.bounceTimer == 0.0F) {
/* 164 */         this.doneSpinning = false;
/* 165 */         this.imageEventText.clearAllDialogs();
/* 166 */         preApplyResult();
/* 167 */         GenericEventDialog.show();
/* 168 */         this.screen = CUR_SCREEN.COMPLETE;
/*     */       }
/*     */     }
/*     */     
/* 172 */     if (!GenericEventDialog.waitForInput) {
/* 173 */       buttonEffect(GenericEventDialog.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   private void updatePosition() {
/* 178 */     if (this.bounceTimer != 0.0F) {
/* 179 */       this.bounceTimer -= Gdx.graphics.getDeltaTime();
/* 180 */       if (this.bounceTimer < 0.0F) {
/* 181 */         this.bounceTimer = 0.0F;
/*     */       }
/* 183 */       if ((this.bounceIn) && (this.startSpin)) {
/* 184 */         this.color.a = Interpolation.fade.apply(1.0F, 0.0F, this.bounceTimer);
/* 185 */         this.imgY = Interpolation.bounceIn.apply(TARGET_Y, START_Y, this.bounceTimer);
/* 186 */       } else if (this.doneSpinning) {
/* 187 */         this.color.a = Interpolation.fade.apply(0.0F, 1.0F, this.bounceTimer);
/* 188 */         this.imgY = Interpolation.swingOut.apply(START_Y, TARGET_Y, this.bounceTimer);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void preApplyResult()
/*     */   {
/* 199 */     switch (this.result) {
/*     */     case 0: 
/* 201 */       this.imageEventText.updateBodyText(DESCRIPTIONS[1]);
/* 202 */       this.imageEventText.setDialogOption(OPTIONS[1]);
/* 203 */       AbstractDungeon.effectList.add(new RainingGoldEffect(this.goldAmount));
/* 204 */       AbstractDungeon.player.gainGold(this.goldAmount);
/* 205 */       break;
/*     */     case 1: 
/* 207 */       this.imageEventText.updateBodyText(DESCRIPTIONS[2]);
/* 208 */       this.imageEventText.setDialogOption(OPTIONS[2]);
/* 209 */       break;
/*     */     case 2: 
/* 211 */       this.imageEventText.updateBodyText(DESCRIPTIONS[3]);
/* 212 */       this.imageEventText.setDialogOption(OPTIONS[3]);
/* 213 */       break;
/*     */     case 3: 
/* 215 */       this.imageEventText.updateBodyText(DESCRIPTIONS[4]);
/* 216 */       this.imageEventText.setDialogOption(OPTIONS[4]);
/* 217 */       break;
/*     */     case 4: 
/* 219 */       this.imageEventText.updateBodyText(DESCRIPTIONS[5]);
/* 220 */       this.imageEventText.setDialogOption(OPTIONS[5]);
/* 221 */       break;
/*     */     default: 
/* 223 */       this.imageEventText.updateBodyText(DESCRIPTIONS[6]);
/* 224 */       this.imageEventText.setDialogOption(OPTIONS[6] + (int)(AbstractDungeon.player.maxHealth * this.hpLossPercent) + OPTIONS[7]);
/*     */       
/* 226 */       this.color = new Color(0.5F, 0.5F, 0.5F, 1.0F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 238 */     switch (this.screen) {
/*     */     case INTRO: 
/* 240 */       if (buttonPressed == 0) {
/* 241 */         GenericEventDialog.hide();
/* 242 */         this.result = AbstractDungeon.miscRng.random(0, 5);
/* 243 */         this.resultAngle = (this.result * 60.0F + MathUtils.random(-10.0F, 10.0F));
/* 244 */         this.wheelAngle = 0.0F;
/* 245 */         this.startSpin = true;
/* 246 */         this.bounceTimer = 2.0F;
/* 247 */         this.animTimer = 2.0F;
/* 248 */         this.spinVelocity = 1500.0F;
/*     */       }
/*     */       break;
/*     */     case COMPLETE: 
/* 252 */       applyResult();
/* 253 */       this.imageEventText.clearAllDialogs();
/* 254 */       this.imageEventText.setDialogOption(OPTIONS[8]);
/* 255 */       this.screen = CUR_SCREEN.LEAVE;
/* 256 */       break;
/*     */     case LEAVE: 
/* 258 */       openMap();
/* 259 */       break;
/*     */     default: 
/* 261 */       logger.info("UNHANDLED CASE");
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void applyResult()
/*     */   {
/* 270 */     switch (this.result)
/*     */     {
/*     */     case 0: 
/* 273 */       this.hasFocus = false;
/* 274 */       logMetric("Gold");
/* 275 */       break;
/*     */     case 1: 
/* 277 */       AbstractDungeon.getCurrRoom().rewards.clear();
/* 278 */       AbstractRelic r = AbstractDungeon.returnRandomScreenlessRelic(AbstractDungeon.returnRandomRelicTier());
/* 279 */       AbstractDungeon.getCurrRoom().addRelicToRewards(r);
/* 280 */       AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/* 281 */       AbstractDungeon.combatRewardScreen.open();
/* 282 */       this.hasFocus = false;
/* 283 */       logMetric("Relic");
/* 284 */       break;
/*     */     case 2: 
/* 286 */       AbstractDungeon.player.heal(AbstractDungeon.player.maxHealth);
/* 287 */       this.hasFocus = false;
/* 288 */       logMetric("Full Heal");
/* 289 */       break;
/*     */     case 3: 
/* 291 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Decay(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/* 293 */       com.megacrit.cardcrawl.unlock.UnlockTracker.markCardAsSeen("Decay");
/* 294 */       this.hasFocus = false;
/* 295 */       logMetric("Curse");
/* 296 */       break;
/*     */     case 4: 
/* 298 */       AbstractDungeon.gridSelectScreen.open(
/* 299 */         CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck.getPurgeableCards()), 1, OPTIONS[9], false, false, false, true);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 306 */       this.roomEventText.hide();
/* 307 */       this.purgeResult = true;
/* 308 */       logMetric("Card Removal");
/* 309 */       break;
/*     */     default: 
/* 311 */       this.imageEventText.updateBodyText(DESCRIPTIONS[7]);
/* 312 */       CardCrawlGame.sound.play("ATTACK_DAGGER_6");
/* 313 */       CardCrawlGame.sound.play("BLOOD_SPLAT");
/* 314 */       AbstractDungeon.player.damage(new DamageInfo(null, (int)(AbstractDungeon.player.maxHealth * this.hpLossPercent), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS));
/*     */       
/* 316 */       logMetric("Damage");
/*     */     }
/*     */   }
/*     */   
/*     */   private void purgeLogic()
/*     */   {
/* 322 */     if ((this.purgeResult) && (!AbstractDungeon.isScreenUp) && (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty())) {
/* 323 */       AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 324 */       AbstractDungeon.player.masterDeck.removeCard(c);
/* 325 */       AbstractDungeon.effectList.add(new PurgeCardEffect(c));
/* 326 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 327 */       this.hasFocus = false;
/* 328 */       this.purgeResult = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 334 */     sb.setColor(this.color);
/* 335 */     sb.draw(this.wheelImg, this.imgX - 512.0F, this.imgY - 512.0F, 512.0F, 512.0F, 1024.0F, 1024.0F, Settings.scale, Settings.scale, this.wheelAngle, 0, 0, 1024, 1024, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 353 */     sb.draw(this.arrowImg, this.imgX - 256.0F + ARROW_OFFSET_X + 180.0F * Settings.scale, this.imgY - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 512, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 371 */     if (this.buttonHb.hovered) {
/* 372 */       sb.draw(this.buttonImg, this.buttonHb.cX - 256.0F, this.buttonHb.cY - 256.0F + 100.0F * Settings.scale, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale * 1.05F, Settings.scale * 1.05F, 0.0F, 0, 0, 512, 512, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 390 */       sb.draw(this.buttonImg, this.buttonHb.cX - 256.0F, this.buttonHb.cY - 256.0F + 100.0F * Settings.scale, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 512, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 409 */     if (Settings.isControllerMode) {
/* 410 */       sb.draw(CInputActionSet.proceed
/* 411 */         .getKeyImg(), this.buttonHb.cX - 32.0F - 160.0F * Settings.scale, this.buttonHb.cY - 32.0F + 30.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 429 */     sb.setBlendFunction(770, 1);
/* 430 */     if (this.buttonHb.hovered) {
/* 431 */       sb.setColor(1.0F, 1.0F, 1.0F, 0.25F);
/*     */     } else {
/* 433 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, (MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) + 1.25F) / 3.5F));
/*     */     }
/* 435 */     if (this.buttonHb.hovered) {
/* 436 */       sb.draw(this.buttonImg, this.buttonHb.cX - 256.0F, this.buttonHb.cY - 256.0F + 100.0F * Settings.scale, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale * 1.05F, Settings.scale * 1.05F, 0.0F, 0, 0, 512, 512, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 454 */       sb.draw(this.buttonImg, this.buttonHb.cX - 256.0F, this.buttonHb.cY - 256.0F + 100.0F * Settings.scale, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 512, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 472 */     sb.setBlendFunction(770, 771);
/*     */     
/* 474 */     this.buttonHb.render(sb);
/*     */   }
/*     */   
/*     */   public void logMetric(String result) {
/* 478 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Wheel of Change", result);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\GremlinWheelGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */