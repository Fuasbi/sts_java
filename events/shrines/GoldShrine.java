/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.RainingGoldEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GoldShrine extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Golden Shrine";
/*  18 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Golden Shrine");
/*  19 */   public static final String NAME = eventStrings.NAME;
/*  20 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  21 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   
/*     */   private static final int GOLD_AMT = 100;
/*     */   
/*     */   private static final int CURSE_GOLD_AMT = 275;
/*     */   private static final int A_2_GOLD_AMT = 50;
/*     */   private int goldAmt;
/*  28 */   private static final String DIALOG_1 = DESCRIPTIONS[0];
/*  29 */   private static final String DIALOG_2 = DESCRIPTIONS[1];
/*  30 */   private static final String DIALOG_3 = DESCRIPTIONS[2];
/*  31 */   private static final String IGNORE = DESCRIPTIONS[3];
/*     */   
/*  33 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*     */   
/*     */   private static enum CUR_SCREEN {
/*  36 */     INTRO,  COMPLETE;
/*     */     
/*     */     private CUR_SCREEN() {} }
/*     */   
/*  40 */   public GoldShrine() { super(NAME, DIALOG_1, "images/events/goldShrine.jpg");
/*     */     
/*  42 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  43 */       this.goldAmt = 50;
/*     */     } else {
/*  45 */       this.goldAmt = 100;
/*     */     }
/*     */     
/*  48 */     this.imageEventText.setDialogOption(OPTIONS[0] + this.goldAmt + OPTIONS[1]);
/*  49 */     this.imageEventText.setDialogOption(OPTIONS[2], com.megacrit.cardcrawl.helpers.CardLibrary.getCopy("Regret"));
/*  50 */     this.imageEventText.setDialogOption(OPTIONS[3]);
/*     */   }
/*     */   
/*     */   public void onEnterRoom()
/*     */   {
/*  55 */     CardCrawlGame.music.playTempBGM("SHRINE");
/*     */   }
/*     */   
/*     */   public void update() {
/*  59 */     super.update();
/*     */     
/*  61 */     if (!RoomEventDialog.waitForInput) {
/*  62 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/*  68 */     switch (this.screen)
/*     */     {
/*     */     case INTRO: 
/*  71 */       switch (buttonPressed) {
/*     */       case 0: 
/*  73 */         this.screen = CUR_SCREEN.COMPLETE;
/*  74 */         logMetric("Golden Shrine", "Pray");
/*  75 */         this.imageEventText.updateBodyText(DIALOG_2);
/*  76 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  77 */         AbstractDungeon.effectList.add(new RainingGoldEffect(this.goldAmt));
/*  78 */         AbstractDungeon.player.gainGold(this.goldAmt);
/*  79 */         this.imageEventText.clearRemainingOptions();
/*  80 */         break;
/*     */       case 1: 
/*  82 */         this.screen = CUR_SCREEN.COMPLETE;
/*  83 */         logMetric("Golden Shrine", "Desecrate");
/*  84 */         AbstractDungeon.effectList.add(new RainingGoldEffect(275));
/*  85 */         AbstractDungeon.player.gainGold(275);
/*  86 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(new com.megacrit.cardcrawl.cards.curses.Regret(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */         
/*  88 */         UnlockTracker.markCardAsSeen("Regret");
/*  89 */         this.imageEventText.updateBodyText(DIALOG_3);
/*  90 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  91 */         this.imageEventText.clearRemainingOptions();
/*  92 */         break;
/*     */       case 2: 
/*  94 */         this.screen = CUR_SCREEN.COMPLETE;
/*  95 */         logMetric("Golden Shrine", "Skipped");
/*  96 */         this.imageEventText.updateBodyText(IGNORE);
/*  97 */         this.imageEventText.updateDialogOption(0, OPTIONS[3]);
/*  98 */         this.imageEventText.clearRemainingOptions();
/*     */       }
/*     */       
/* 101 */       break;
/*     */     case COMPLETE: 
/* 103 */       openMap();
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String result)
/*     */   {
/* 109 */     com.megacrit.cardcrawl.events.AbstractEvent.logMetric("Golden Shrine", result);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\GoldShrine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */