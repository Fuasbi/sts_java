/*     */ package com.megacrit.cardcrawl.events.shrines;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*     */ import com.megacrit.cardcrawl.cards.blue.Zap;
/*     */ import com.megacrit.cardcrawl.cards.green.Neutralize;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.EventStrings;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GremlinMatchGame extends AbstractImageEvent
/*     */ {
/*     */   public static final String ID = "Match and Keep!";
/*  33 */   private static final EventStrings eventStrings = CardCrawlGame.languagePack.getEventString("Match and Keep!");
/*  34 */   public static final String NAME = eventStrings.NAME;
/*  35 */   public static final String[] DESCRIPTIONS = eventStrings.DESCRIPTIONS;
/*  36 */   public static final String[] OPTIONS = eventStrings.OPTIONS;
/*     */   private AbstractCard chosenCard;
/*     */   private AbstractCard hoveredCard;
/*  39 */   private boolean cardFlipped = false; private boolean gameDone = false; private boolean cleanUpCalled = false;
/*  40 */   private int attemptCount = 5;
/*  41 */   private CardGroup cards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  42 */   private float waitTimer = 0.0F;
/*  43 */   private int cardsMatched = 0;
/*     */   
/*  45 */   private CUR_SCREEN screen = CUR_SCREEN.INTRO;
/*  46 */   private static final String MSG_2 = DESCRIPTIONS[0];
/*  47 */   private static final String MSG_3 = DESCRIPTIONS[1];
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public GremlinMatchGame()
/*     */   {
/*  54 */     super(NAME, DESCRIPTIONS[2], "images/events/matchAndKeep.jpg");
/*  55 */     this.cards.group = initializeCards();
/*  56 */     java.util.Collections.shuffle(this.cards.group, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/*     */     
/*  58 */     this.imageEventText.setDialogOption(OPTIONS[0]);
/*     */   }
/*     */   
/*     */   private ArrayList<AbstractCard> initializeCards() {
/*  62 */     ArrayList<AbstractCard> retVal = new ArrayList();
/*  63 */     ArrayList<AbstractCard> retVal2 = new ArrayList();
/*     */     
/*  65 */     if (AbstractDungeon.ascensionLevel >= 15) {
/*  66 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.RARE).makeCopy());
/*  67 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.UNCOMMON).makeCopy());
/*  68 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.COMMON).makeCopy());
/*  69 */       retVal.add(AbstractDungeon.returnRandomCurse());
/*  70 */       retVal.add(AbstractDungeon.returnRandomCurse());
/*     */     } else {
/*  72 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.RARE).makeCopy());
/*  73 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.UNCOMMON).makeCopy());
/*  74 */       retVal.add(AbstractDungeon.getCard(AbstractCard.CardRarity.COMMON).makeCopy());
/*  75 */       retVal.add(AbstractDungeon.returnColorlessCard(AbstractCard.CardRarity.UNCOMMON).makeCopy());
/*  76 */       retVal.add(AbstractDungeon.returnRandomCurse());
/*     */     }
/*     */     
/*     */ 
/*  80 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/*  82 */       retVal.add(new com.megacrit.cardcrawl.cards.red.Bash());
/*  83 */       break;
/*     */     case THE_SILENT: 
/*  85 */       retVal.add(new Neutralize());
/*  86 */       break;
/*     */     case DEFECT: 
/*  88 */       retVal.add(new Zap());
/*  89 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*  94 */     for (AbstractCard c : retVal) {
/*  95 */       if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/*  96 */         c.upgrade();
/*  97 */       } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2"))) {
/*  98 */         c.upgrade();
/*  99 */       } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2"))) {
/* 100 */         c.upgrade();
/*     */       }
/* 102 */       retVal2.add(c.makeStatEquivalentCopy());
/*     */     }
/*     */     
/* 105 */     retVal.addAll(retVal2);
/*     */     
/* 107 */     for (AbstractCard c : retVal) {
/* 108 */       c.current_x = (Settings.WIDTH / 2.0F);
/* 109 */       c.target_x = c.current_x;
/* 110 */       c.current_y = (-300.0F * Settings.scale);
/* 111 */       c.target_y = c.current_y;
/*     */     }
/*     */     
/* 114 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static enum CUR_SCREEN
/*     */   {
/* 121 */     INTRO,  RULE_EXPLANATION,  PLAY,  COMPLETE,  CLEAN_UP;
/*     */     
/*     */     private CUR_SCREEN() {}
/*     */   }
/*     */   
/* 126 */   public void update() { super.update();
/* 127 */     this.cards.update();
/*     */     
/* 129 */     if (this.screen == CUR_SCREEN.PLAY) {
/* 130 */       updateControllerInput();
/* 131 */       updateMatchGameLogic();
/* 132 */     } else if (this.screen == CUR_SCREEN.CLEAN_UP) {
/* 133 */       if (!this.cleanUpCalled) {
/* 134 */         this.cleanUpCalled = true;
/* 135 */         cleanUpCards();
/*     */       }
/* 137 */       if (this.waitTimer > 0.0F) {
/* 138 */         this.waitTimer -= Gdx.graphics.getDeltaTime();
/* 139 */         if (this.waitTimer < 0.0F) {
/* 140 */           this.waitTimer = 0.0F;
/* 141 */           this.screen = CUR_SCREEN.COMPLETE;
/* 142 */           GenericEventDialog.show();
/* 143 */           this.imageEventText.updateBodyText(MSG_3);
/* 144 */           this.imageEventText.clearRemainingOptions();
/* 145 */           this.imageEventText.setDialogOption(OPTIONS[1]);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 150 */     if (!GenericEventDialog.waitForInput) {
/* 151 */       buttonEffect(GenericEventDialog.getSelectedOption());
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 156 */     if (!Settings.isControllerMode) {
/* 157 */       return;
/*     */     }
/*     */     
/* 160 */     boolean anyHovered = false;
/* 161 */     int index = 0;
/* 162 */     for (AbstractCard c : this.cards.group) {
/* 163 */       if (c.hb.hovered) {
/* 164 */         anyHovered = true;
/* 165 */         break;
/*     */       }
/* 167 */       index++;
/*     */     }
/*     */     
/* 170 */     if (!anyHovered) {
/* 171 */       Gdx.input.setCursorPosition(
/* 172 */         (int)((AbstractCard)this.cards.group.get(0)).hb.cX, Settings.HEIGHT - 
/* 173 */         (int)((AbstractCard)this.cards.group.get(0)).hb.cY);
/*     */     } else {
/* 175 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 176 */         float y = ((AbstractCard)this.cards.group.get(index)).hb.cY + 230.0F * Settings.scale;
/* 177 */         if (y > 865.0F * Settings.scale) {
/* 178 */           y = 290.0F * Settings.scale;
/*     */         }
/* 180 */         Gdx.input.setCursorPosition((int)((AbstractCard)this.cards.group.get(index)).hb.cX, (int)(Settings.HEIGHT - y));
/* 181 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 182 */         float y = ((AbstractCard)this.cards.group.get(index)).hb.cY - 230.0F * Settings.scale;
/* 183 */         if (y < 175.0F * Settings.scale) {
/* 184 */           y = 750.0F * Settings.scale;
/*     */         }
/* 186 */         Gdx.input.setCursorPosition((int)((AbstractCard)this.cards.group.get(index)).hb.cX, (int)(Settings.HEIGHT - y));
/*     */       }
/* 188 */       else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 189 */         float x = ((AbstractCard)this.cards.group.get(index)).hb.cX - 210.0F * Settings.scale;
/* 190 */         if (x < 530.0F * Settings.scale) {
/* 191 */           x = 1270.0F * Settings.scale;
/*     */         }
/* 193 */         Gdx.input.setCursorPosition((int)x, Settings.HEIGHT - (int)((AbstractCard)this.cards.group.get(index)).hb.cY);
/* 194 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 195 */         float x = ((AbstractCard)this.cards.group.get(index)).hb.cX + 210.0F * Settings.scale;
/* 196 */         if (x > 1375.0F * Settings.scale) {
/* 197 */           x = 640.0F * Settings.scale;
/*     */         }
/* 199 */         Gdx.input.setCursorPosition((int)x, Settings.HEIGHT - (int)((AbstractCard)this.cards.group.get(index)).hb.cY);
/*     */       }
/*     */       
/* 202 */       if (CInputActionSet.select.isJustPressed()) {
/* 203 */         CInputActionSet.select.unpress();
/* 204 */         InputHelper.justClickedLeft = true;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void cleanUpCards() {
/* 210 */     for (AbstractCard c : this.cards.group) {
/* 211 */       c.targetDrawScale = 0.5F;
/* 212 */       c.target_x = (Settings.WIDTH / 2.0F);
/* 213 */       c.target_y = (-300.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMatchGameLogic() {
/* 218 */     if (this.waitTimer == 0.0F) {
/* 219 */       this.hoveredCard = null;
/* 220 */       for (AbstractCard c : this.cards.group) {
/* 221 */         c.hb.update();
/* 222 */         if ((this.hoveredCard == null) && (c.hb.hovered)) {
/* 223 */           c.drawScale = 0.7F;
/* 224 */           c.targetDrawScale = 0.7F;
/* 225 */           this.hoveredCard = c;
/* 226 */           if ((InputHelper.justClickedLeft) && (this.hoveredCard.isFlipped)) {
/* 227 */             InputHelper.justClickedLeft = false;
/* 228 */             this.hoveredCard.isFlipped = false;
/* 229 */             if (!this.cardFlipped) {
/* 230 */               this.cardFlipped = true;
/* 231 */               this.chosenCard = this.hoveredCard;
/*     */             } else {
/* 233 */               this.cardFlipped = false;
/* 234 */               if (this.chosenCard.cardID.equals(this.hoveredCard.cardID)) {
/* 235 */                 this.waitTimer = 1.0F;
/* 236 */                 this.chosenCard.targetDrawScale = 0.7F;
/* 237 */                 this.chosenCard.target_x = (Settings.WIDTH / 2.0F);
/* 238 */                 this.chosenCard.target_y = (Settings.HEIGHT / 2.0F);
/* 239 */                 this.hoveredCard.targetDrawScale = 0.7F;
/* 240 */                 this.hoveredCard.target_x = (Settings.WIDTH / 2.0F);
/* 241 */                 this.hoveredCard.target_y = (Settings.HEIGHT / 2.0F);
/*     */               } else {
/* 243 */                 this.waitTimer = 1.25F;
/* 244 */                 this.chosenCard.targetDrawScale = 1.0F;
/* 245 */                 this.hoveredCard.targetDrawScale = 1.0F;
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/* 250 */         else if (c != this.chosenCard) {
/* 251 */           c.targetDrawScale = 0.5F;
/*     */         }
/*     */       }
/*     */     }
/*     */     else {
/* 256 */       this.waitTimer -= Gdx.graphics.getDeltaTime();
/* 257 */       if ((this.waitTimer < 0.0F) && (!this.gameDone)) {
/* 258 */         this.waitTimer = 0.0F;
/*     */         
/*     */ 
/* 261 */         if (this.chosenCard.cardID.equals(this.hoveredCard.cardID)) {
/* 262 */           this.cardsMatched += 1;
/* 263 */           this.cards.group.remove(this.chosenCard);
/* 264 */           this.cards.group.remove(this.hoveredCard);
/* 265 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(this.chosenCard
/* 266 */             .makeCopy(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/* 267 */           this.chosenCard = null;
/* 268 */           this.hoveredCard = null;
/*     */         } else {
/* 270 */           this.chosenCard.isFlipped = true;
/* 271 */           this.hoveredCard.isFlipped = true;
/* 272 */           this.chosenCard.targetDrawScale = 0.5F;
/* 273 */           this.hoveredCard.targetDrawScale = 0.5F;
/* 274 */           this.chosenCard = null;
/* 275 */           this.hoveredCard = null;
/*     */         }
/* 277 */         this.attemptCount -= 1;
/* 278 */         if (this.attemptCount == 0) {
/* 279 */           this.gameDone = true;
/* 280 */           this.waitTimer = 1.0F;
/*     */         }
/* 282 */       } else if (this.gameDone)
/*     */       {
/* 284 */         this.screen = CUR_SCREEN.CLEAN_UP;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 296 */     switch (this.screen) {
/*     */     case INTRO: 
/* 298 */       switch (buttonPressed) {
/*     */       case 0: 
/* 300 */         this.imageEventText.updateBodyText(MSG_2);
/* 301 */         this.imageEventText.updateDialogOption(0, OPTIONS[2]);
/* 302 */         this.screen = CUR_SCREEN.RULE_EXPLANATION;
/*     */       }
/*     */       
/* 305 */       break;
/*     */     
/*     */     case RULE_EXPLANATION: 
/* 308 */       switch (buttonPressed) {
/*     */       case 0: 
/* 310 */         this.imageEventText.removeDialogOption(0);
/* 311 */         GenericEventDialog.hide();
/* 312 */         this.screen = CUR_SCREEN.PLAY;
/* 313 */         placeCards();
/*     */       }
/*     */       
/* 316 */       break;
/*     */     
/*     */     case COMPLETE: 
/* 319 */       logMetric(this.cardsMatched + " cards matched");
/* 320 */       openMap();
/* 321 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void placeCards()
/*     */   {
/* 328 */     for (int i = 0; i < this.cards.size(); i++) {
/* 329 */       ((AbstractCard)this.cards.group.get(i)).target_x = (i % 4 * 210.0F * Settings.scale + 640.0F * Settings.scale);
/* 330 */       ((AbstractCard)this.cards.group.get(i)).target_y = (i % 3 * -230.0F * Settings.scale + 750.0F * Settings.scale);
/* 331 */       ((AbstractCard)this.cards.group.get(i)).targetDrawScale = 0.5F;
/* 332 */       ((AbstractCard)this.cards.group.get(i)).isFlipped = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 338 */     this.cards.render(sb);
/* 339 */     if (this.chosenCard != null) {
/* 340 */       this.chosenCard.render(sb);
/*     */     }
/* 342 */     if (this.hoveredCard != null) {
/* 343 */       this.hoveredCard.render(sb);
/*     */     }
/*     */     
/* 346 */     if (this.screen == CUR_SCREEN.PLAY) {
/* 347 */       FontHelper.renderSmartText(sb, FontHelper.deckBannerFont, OPTIONS[3] + this.attemptCount, 780.0F * Settings.scale, 80.0F * Settings.scale, 2000.0F * Settings.scale, 0.0F, Color.WHITE);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void logMetric(String result)
/*     */   {
/* 360 */     AbstractEvent.logMetric("Match and Keep!", result);
/*     */   }
/*     */   
/*     */   public void renderAboveTopPanel(SpriteBatch sb) {}
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\shrines\GremlinMatchGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */