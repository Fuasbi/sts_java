/*     */ package com.megacrit.cardcrawl.events;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.AppearEffect;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordColor;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordEffect;
/*     */ import com.megacrit.cardcrawl.ui.buttons.LargeDialogOptionButton;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Scanner;
/*     */ 
/*     */ public class GenericEventDialog
/*     */ {
/*  26 */   private com.badlogic.gdx.graphics.Texture img = null;
/*  27 */   private static final float DIALOG_MSG_X_IMAGE = 816.0F * Settings.scale;
/*  28 */   private static final float DIALOG_MSG_W_IMAGE = 900.0F * Settings.scale;
/*     */   
/*     */ 
/*  31 */   private Color panelColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  32 */   private Color titleColor = new Color(1.0F, 0.835F, 0.39F, 0.0F);
/*  33 */   private Color borderColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  34 */   private Color imgColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  35 */   private float animateTimer = 0.0F;
/*     */   private static final float ANIM_SPEED = 0.5F;
/*  37 */   private static boolean show = false;
/*     */   
/*     */ 
/*  40 */   private String title = "";
/*  41 */   private static final float TITLE_X = 570.0F * Settings.scale;
/*  42 */   private static final float TITLE_Y = Settings.EVENT_Y + 408.0F * Settings.scale;
/*     */   
/*     */ 
/*  45 */   private static float curLineWidth = 0.0F;
/*  46 */   private static int curLine = 0;
/*     */   private static DialogWord.AppearEffect a_effect;
/*     */   private static Scanner s;
/*  49 */   private static GlyphLayout gl = new GlyphLayout();
/*  50 */   private static ArrayList<DialogWord> words = new ArrayList();
/*  51 */   private static boolean textDone = true;
/*  52 */   private static float wordTimer = 0.0F;
/*     */   private static final float WORD_TIME = 0.02F;
/*  54 */   private static final float CHAR_SPACING = 8.0F * Settings.scale;
/*  55 */   private static final float LINE_SPACING = 38.0F * Settings.scale;
/*  56 */   private static final float DIALOG_MSG_X_TEXT = 455.0F * Settings.scale;
/*  57 */   private static final float DIALOG_MSG_Y_TEXT = Settings.EVENT_Y + 300.0F * Settings.scale;
/*  58 */   private static final float DIALOG_MSG_W_TEXT = 1000.0F * Settings.scale;
/*  59 */   private static float DIALOG_MSG_X = DIALOG_MSG_X_TEXT;
/*  60 */   private static float DIALOG_MSG_Y = DIALOG_MSG_Y_TEXT;
/*  61 */   private static float DIALOG_MSG_W = DIALOG_MSG_W_TEXT;
/*     */   
/*     */ 
/*  64 */   public ArrayList<LargeDialogOptionButton> optionList = new ArrayList();
/*  65 */   public static int selectedOption = -1;
/*  66 */   public static boolean waitForInput = true;
/*     */   
/*     */   public void loadImage(String imgUrl) {
/*  69 */     this.img = ImageMaster.loadImage(imgUrl);
/*  70 */     DIALOG_MSG_X = DIALOG_MSG_X_IMAGE;
/*  71 */     DIALOG_MSG_W = DIALOG_MSG_W_IMAGE;
/*     */   }
/*     */   
/*     */   public void clearImage() {
/*  75 */     this.img = null;
/*  76 */     DIALOG_MSG_X = DIALOG_MSG_X_TEXT;
/*  77 */     DIALOG_MSG_Y = DIALOG_MSG_Y_TEXT;
/*  78 */     DIALOG_MSG_W = DIALOG_MSG_W_TEXT;
/*     */   }
/*     */   
/*     */   public void update() {
/*  82 */     animateIn();
/*     */     int i;
/*  84 */     if ((show) && (this.animateTimer == 0.0F)) {
/*  85 */       for (i = 0; i < this.optionList.size(); i++) {
/*  86 */         ((LargeDialogOptionButton)this.optionList.get(i)).update(this.optionList.size());
/*  87 */         if ((((LargeDialogOptionButton)this.optionList.get(i)).pressed) && (waitForInput)) {
/*  88 */           selectedOption = i;
/*  89 */           ((LargeDialogOptionButton)this.optionList.get(i)).pressed = false;
/*  90 */           waitForInput = false;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  95 */     if (!Settings.lineBreakViaCharacter) {
/*  96 */       bodyTextEffect();
/*     */     } else {
/*  98 */       bodyTextEffectCN();
/*     */     }
/* 100 */     for (DialogWord w : words) {
/* 101 */       w.update();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void animateIn()
/*     */   {
/* 109 */     if (show) {
/* 110 */       this.animateTimer -= Gdx.graphics.getDeltaTime();
/* 111 */       if (this.animateTimer < 0.0F) {
/* 112 */         this.animateTimer = 0.0F;
/*     */       }
/* 114 */       this.panelColor.a = MathHelper.slowColorLerpSnap(this.panelColor.a, 1.0F);
/*     */       
/* 116 */       if (this.panelColor.a > 0.8F) {
/* 117 */         this.titleColor.a = MathHelper.slowColorLerpSnap(this.titleColor.a, 1.0F);
/* 118 */         this.borderColor.a = this.titleColor.a;
/* 119 */         if (this.borderColor.a > 0.8F) {
/* 120 */           this.imgColor.a = MathHelper.slowColorLerpSnap(this.imgColor.a, 1.0F);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public static int getSelectedOption() {
/* 127 */     waitForInput = true;
/* 128 */     return selectedOption;
/*     */   }
/*     */   
/*     */   public static void hide() {
/* 132 */     show = false;
/*     */   }
/*     */   
/*     */   public static void show() {
/* 136 */     show = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 143 */     show = false;
/* 144 */     clearImage();
/* 145 */     this.animateTimer = 0.0F;
/* 146 */     this.panelColor.a = 0.0F;
/* 147 */     this.titleColor.a = 0.0F;
/* 148 */     this.imgColor.a = 0.0F;
/* 149 */     this.borderColor.a = 0.0F;
/* 150 */     this.optionList.clear();
/* 151 */     words.clear();
/* 152 */     waitForInput = true;
/*     */   }
/*     */   
/*     */   public static void cleanUp() {
/* 156 */     words.clear();
/* 157 */     show = false;
/* 158 */     waitForInput = true;
/*     */   }
/*     */   
/*     */   public void show(String title, String text) {
/* 162 */     this.title = title;
/* 163 */     updateBodyText(text);
/* 164 */     if (Settings.FAST_MODE) {
/* 165 */       this.animateTimer = 0.125F;
/*     */     } else {
/* 167 */       this.animateTimer = 0.5F;
/*     */     }
/* 169 */     show = true;
/*     */   }
/*     */   
/*     */   public void clearAllDialogs() {
/* 173 */     this.optionList.clear();
/*     */   }
/*     */   
/*     */   public void removeDialogOption(int slot) {
/* 177 */     if (slot < this.optionList.size()) {
/* 178 */       this.optionList.remove(slot);
/*     */     }
/* 180 */     for (LargeDialogOptionButton b : this.optionList) {
/* 181 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void clearRemainingOptions() {
/* 186 */     for (int i = this.optionList.size() - 1; i > 0; i--) {
/* 187 */       this.optionList.remove(i);
/*     */     }
/* 189 */     for (LargeDialogOptionButton b : this.optionList) {
/* 190 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void setDialogOption(String text) {
/* 195 */     this.optionList.add(new LargeDialogOptionButton(this.optionList.size(), text));
/* 196 */     for (LargeDialogOptionButton b : this.optionList) {
/* 197 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void setDialogOption(String text, AbstractCard previewCard) {
/* 202 */     this.optionList.add(new LargeDialogOptionButton(this.optionList.size(), text, previewCard));
/* 203 */     for (LargeDialogOptionButton b : this.optionList) {
/* 204 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void setDialogOption(String text, boolean isDisabled) {
/* 209 */     this.optionList.add(new LargeDialogOptionButton(this.optionList.size(), text, isDisabled));
/* 210 */     for (LargeDialogOptionButton b : this.optionList) {
/* 211 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void setDialogOption(String text, boolean isDisabled, AbstractCard previewCard) {
/* 216 */     this.optionList.add(new LargeDialogOptionButton(this.optionList.size(), text, isDisabled, previewCard));
/* 217 */     for (LargeDialogOptionButton b : this.optionList) {
/* 218 */       b.calculateY(this.optionList.size());
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateDialogOption(int slot, String text) {
/* 223 */     if (!this.optionList.isEmpty()) {
/* 224 */       if (this.optionList.size() > slot) {
/* 225 */         this.optionList.set(slot, new LargeDialogOptionButton(slot, text));
/*     */       } else {
/* 227 */         this.optionList.add(new LargeDialogOptionButton(slot, text));
/*     */       }
/*     */     } else {
/* 230 */       this.optionList.add(new LargeDialogOptionButton(slot, text));
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateDialogOption(int slot, String text, boolean isDisabled) {
/* 235 */     if (!this.optionList.isEmpty()) {
/* 236 */       if (this.optionList.size() > slot) {
/* 237 */         this.optionList.set(slot, new LargeDialogOptionButton(slot, text, isDisabled));
/*     */       } else {
/* 239 */         this.optionList.add(new LargeDialogOptionButton(slot, text, isDisabled));
/*     */       }
/*     */     } else {
/* 242 */       this.optionList.add(new LargeDialogOptionButton(slot, text, isDisabled));
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateDialogOption(int slot, String text, AbstractCard previewCard) {
/* 247 */     if (!this.optionList.isEmpty()) {
/* 248 */       if (this.optionList.size() > slot) {
/* 249 */         this.optionList.set(slot, new LargeDialogOptionButton(slot, text, previewCard));
/*     */       } else {
/* 251 */         this.optionList.add(new LargeDialogOptionButton(slot, text, previewCard));
/*     */       }
/*     */     } else {
/* 254 */       this.optionList.add(new LargeDialogOptionButton(slot, text, previewCard));
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateBodyText(String text) {
/* 259 */     updateBodyText(text, DialogWord.AppearEffect.BUMP_IN);
/* 260 */     System.out.println("UPDATE BODY TEXT UPDATE BODY TEXTUPDATE BODY TEXTUPDATE BODY TEXT");
/*     */   }
/*     */   
/*     */   public void updateBodyText(String text, DialogWord.AppearEffect ae) {
/* 264 */     s = new Scanner(text);
/* 265 */     words.clear();
/* 266 */     textDone = false;
/* 267 */     a_effect = ae;
/* 268 */     curLineWidth = 0.0F;
/* 269 */     curLine = 0;
/*     */   }
/*     */   
/*     */   private void bodyTextEffectCN() {
/* 273 */     wordTimer -= Gdx.graphics.getDeltaTime();
/* 274 */     if ((wordTimer < 0.0F) && (!textDone)) {
/* 275 */       if (Settings.FAST_MODE) {
/* 276 */         wordTimer = 0.005F;
/*     */       } else {
/* 278 */         wordTimer = 0.02F;
/*     */       }
/*     */       
/* 281 */       if (s.hasNext()) {
/* 282 */         String word = s.next();
/*     */         
/* 284 */         if (word.equals("NL")) {
/* 285 */           curLine += 1;
/* 286 */           curLineWidth = 0.0F;
/* 287 */           return;
/*     */         }
/*     */         
/* 290 */         DialogWord.WordColor color = DialogWord.identifyWordColor(word);
/* 291 */         if (color != DialogWord.WordColor.DEFAULT) {
/* 292 */           word = word.substring(2, word.length());
/*     */         }
/*     */         
/* 295 */         DialogWord.WordEffect effect = DialogWord.identifyWordEffect(word);
/* 296 */         if (effect != DialogWord.WordEffect.NONE) {
/* 297 */           word = word.substring(1, word.length() - 1);
/*     */         }
/*     */         
/*     */ 
/* 301 */         for (int i = 0; i < word.length(); i++) {
/* 302 */           String tmp = Character.toString(word.charAt(i));
/*     */           
/* 304 */           gl.setText(FontHelper.eventBodyText, tmp);
/* 305 */           if (curLineWidth + gl.width > DIALOG_MSG_W) {
/* 306 */             curLine += 1;
/* 307 */             curLineWidth = gl.width;
/*     */           } else {
/* 309 */             curLineWidth += gl.width;
/*     */           }
/*     */           
/* 312 */           words.add(new DialogWord(FontHelper.eventBodyText, tmp, a_effect, effect, color, DIALOG_MSG_X + curLineWidth - gl.width, DIALOG_MSG_Y - LINE_SPACING * curLine, curLine));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 323 */           if (!show) {
/* 324 */             ((DialogWord)words.get(words.size() - 1)).dialogFadeOut();
/*     */           }
/*     */         }
/*     */       } else {
/* 328 */         textDone = true;
/* 329 */         s.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void bodyTextEffect() {
/* 335 */     wordTimer -= Gdx.graphics.getDeltaTime();
/* 336 */     if ((wordTimer < 0.0F) && (!textDone)) {
/* 337 */       if (Settings.FAST_MODE) {
/* 338 */         wordTimer = 0.005F;
/*     */       } else {
/* 340 */         wordTimer = 0.02F;
/*     */       }
/*     */       
/* 343 */       if (s.hasNext()) {
/* 344 */         String word = s.next();
/*     */         
/* 346 */         if (word.equals("NL")) {
/* 347 */           curLine += 1;
/* 348 */           curLineWidth = 0.0F;
/* 349 */           return;
/*     */         }
/*     */         
/* 352 */         DialogWord.WordColor color = DialogWord.identifyWordColor(word);
/* 353 */         if (color != DialogWord.WordColor.DEFAULT) {
/* 354 */           word = word.substring(2, word.length());
/*     */         }
/*     */         
/* 357 */         DialogWord.WordEffect effect = DialogWord.identifyWordEffect(word);
/* 358 */         if (effect != DialogWord.WordEffect.NONE) {
/* 359 */           word = word.substring(1, word.length() - 1);
/*     */         }
/*     */         
/* 362 */         gl.setText(FontHelper.eventBodyText, word);
/* 363 */         if (curLineWidth + gl.width > DIALOG_MSG_W) {
/* 364 */           curLine += 1;
/* 365 */           curLineWidth = gl.width + CHAR_SPACING;
/*     */         } else {
/* 367 */           curLineWidth += gl.width + CHAR_SPACING;
/*     */         }
/*     */         
/* 370 */         words.add(new DialogWord(FontHelper.eventBodyText, word, a_effect, effect, color, DIALOG_MSG_X + curLineWidth - gl.width, DIALOG_MSG_Y - LINE_SPACING * curLine, curLine));
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 381 */         if (!show) {
/* 382 */           ((DialogWord)words.get(words.size() - 1)).dialogFadeOut();
/*     */         }
/*     */       } else {
/* 385 */         textDone = true;
/* 386 */         s.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 392 */     if ((show) && (!AbstractDungeon.player.isDead)) {
/* 393 */       sb.setColor(this.panelColor);
/*     */       
/* 395 */       sb.draw(AbstractDungeon.eventBackgroundImg, Settings.WIDTH / 2.0F - 881.0F - 12.0F * Settings.scale, Settings.EVENT_Y - 403.0F + 64.0F * Settings.scale, 881.5F, 403.0F, 1763.0F, 806.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1763, 806, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 413 */       if (this.img != null) {
/* 414 */         sb.setColor(this.imgColor);
/* 415 */         sb.draw(this.img, 460.0F * Settings.scale - 300.0F, Settings.EVENT_Y - 300.0F + 16.0F * Settings.scale, 300.0F, 300.0F, 600.0F, 600.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 600, 600, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 433 */         sb.setColor(this.borderColor);
/* 434 */         sb.draw(ImageMaster.EVENT_IMG_FRAME, 460.0F * Settings.scale - 305.0F, Settings.EVENT_Y - 305.0F + 16.0F * Settings.scale, 305.0F, 305.0F, 610.0F, 610.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 610, 610, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 453 */       FontHelper.renderFontCentered(sb, FontHelper.textOnlyEventTitle, this.title, TITLE_X, TITLE_Y, this.titleColor);
/*     */       
/* 455 */       for (DialogWord w : words) {
/* 456 */         w.render(sb);
/*     */       }
/*     */       
/* 459 */       for (LargeDialogOptionButton b : this.optionList) {
/* 460 */         b.render(sb);
/*     */       }
/* 462 */       for (LargeDialogOptionButton b : this.optionList) {
/* 463 */         b.renderCardPreview(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\events\GenericEventDialog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */