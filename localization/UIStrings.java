package com.megacrit.cardcrawl.localization;

import java.util.Map;

public class UIStrings
{
  public String[] TEXT;
  public String[] EXTRA_TEXT;
  public Map<String, String> TEXT_DICT;
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\localization\UIStrings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */