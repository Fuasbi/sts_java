package com.megacrit.cardcrawl.localization;

public class CardStrings
{
  public String NAME;
  public String DESCRIPTION;
  public String UPGRADE_DESCRIPTION;
  public String[] EXTENDED_DESCRIPTION;
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\localization\CardStrings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */