package com.megacrit.cardcrawl.localization;

public class KeywordStrings
{
  public Keyword ARTIFACT;
  public Keyword BLOCK;
  public Keyword BURN;
  public Keyword CHANNEL;
  public Keyword CONFUSED;
  public Keyword CURSE;
  public Keyword DARK;
  public Keyword DAZED;
  public Keyword DEXTERITY;
  public Keyword ETHEREAL;
  public Keyword EVOKE;
  public Keyword EXHAUST;
  public Keyword FOCUS;
  public Keyword FRAIL;
  public Keyword FROST;
  public Keyword INNATE;
  public Keyword INTANGIBLE;
  public Keyword LIGHTNING;
  public Keyword LOCKED;
  public Keyword LOCK_ON;
  public Keyword OPENER;
  public Keyword PLASMA;
  public Keyword POISON;
  public Keyword RECHARGE;
  public Keyword RETAIN;
  public Keyword SHIV;
  public Keyword STATUS;
  public Keyword STRENGTH;
  public Keyword STRIKE;
  public Keyword TODO;
  public Keyword TRANSFORM;
  public Keyword UNKNOWN;
  public Keyword UNPLAYABLE;
  public Keyword UPGRADE;
  public Keyword VOID;
  public Keyword VULNERABLE;
  public Keyword WEAK;
  public Keyword WOUND;
  public Keyword THORNS;
  public String[] TEXT;
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\localization\KeywordStrings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */