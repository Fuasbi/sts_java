/*     */ package com.megacrit.cardcrawl.localization;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.reflect.TypeToken;
/*     */ import java.io.File;
/*     */ import java.lang.reflect.Type;
/*     */ import java.nio.charset.StandardCharsets;
/*     */ import java.util.Map;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LocalizedStrings
/*     */ {
/*  20 */   private static final Logger logger = LogManager.getLogger(LocalizedStrings.class.getName());
/*     */   
/*     */   private static final String LOCALIZATION_DIR = "localization";
/*     */   
/*     */   private static Map<String, MonsterStrings> monsters;
/*     */   
/*     */   private static Map<String, PowerStrings> powers;
/*     */   private static Map<String, CardStrings> cards;
/*     */   private static Map<String, RelicStrings> relics;
/*     */   private static Map<String, EventStrings> events;
/*     */   private static Map<String, PotionStrings> potions;
/*     */   private static Map<String, CreditStrings> credits;
/*     */   private static Map<String, TutorialStrings> tutorials;
/*     */   private static Map<String, KeywordStrings> keywords;
/*     */   private static Map<String, ScoreBonusStrings> scoreBonuses;
/*     */   private static Map<String, CharacterStrings> characters;
/*     */   private static Map<String, UIStrings> ui;
/*     */   private static Map<String, OrbStrings> orb;
/*     */   public static Map<String, RunModStrings> mod;
/*     */   private static Map<String, BlightStrings> blights;
/*     */   private static Map<String, AchievementStrings> achievements;
/*     */   
/*     */   public LocalizedStrings()
/*     */   {
/*  44 */     long startTime = System.currentTimeMillis();
/*     */     
/*  46 */     Gson gson = new Gson();
/*     */     String langPackDir;
/*     */     String langPackDir;
/*  49 */     String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; String langPackDir; switch (com.megacrit.cardcrawl.core.Settings.language) {
/*     */     case ENG: 
/*  51 */       langPackDir = "localization" + File.separator + "eng";
/*  52 */       break;
/*     */     case EPO: 
/*  54 */       langPackDir = "localization" + File.separator + "epo";
/*  55 */       break;
/*     */     case PTB: 
/*  57 */       langPackDir = "localization" + File.separator + "ptb";
/*  58 */       break;
/*     */     case ZHS: 
/*  60 */       langPackDir = "localization" + File.separator + "zhs";
/*  61 */       break;
/*     */     case ZHT: 
/*  63 */       langPackDir = "localization" + File.separator + "zht";
/*  64 */       break;
/*     */     case FRA: 
/*  66 */       langPackDir = "localization" + File.separator + "fra";
/*  67 */       break;
/*     */     case DEU: 
/*  69 */       langPackDir = "localization" + File.separator + "deu";
/*  70 */       break;
/*     */     case GRE: 
/*  72 */       langPackDir = "localization" + File.separator + "gre";
/*  73 */       break;
/*     */     case IND: 
/*  75 */       langPackDir = "localization" + File.separator + "ind";
/*  76 */       break;
/*     */     case ITA: 
/*  78 */       langPackDir = "localization" + File.separator + "ita";
/*  79 */       break;
/*     */     case JPN: 
/*  81 */       langPackDir = "localization" + File.separator + "jpn";
/*  82 */       break;
/*     */     case KOR: 
/*  84 */       langPackDir = "localization" + File.separator + "kor";
/*  85 */       break;
/*     */     case NOR: 
/*  87 */       langPackDir = "localization" + File.separator + "nor";
/*  88 */       break;
/*     */     case POL: 
/*  90 */       langPackDir = "localization" + File.separator + "pol";
/*  91 */       break;
/*     */     case RUS: 
/*  93 */       langPackDir = "localization" + File.separator + "rus";
/*  94 */       break;
/*     */     case SPA: 
/*  96 */       langPackDir = "localization" + File.separator + "spa";
/*  97 */       break;
/*     */     case SRP: 
/*  99 */       langPackDir = "localization" + File.separator + "srp";
/* 100 */       break;
/*     */     case SRB: 
/* 102 */       langPackDir = "localization" + File.separator + "srb";
/* 103 */       break;
/*     */     case THA: 
/* 105 */       langPackDir = "localization" + File.separator + "tha";
/* 106 */       break;
/*     */     case TUR: 
/* 108 */       langPackDir = "localization" + File.separator + "tur";
/* 109 */       break;
/*     */     case UKR: 
/* 111 */       langPackDir = "localization" + File.separator + "ukr";
/* 112 */       break;
/*     */     case WWW: 
/* 114 */       langPackDir = "localization" + File.separator + "www";
/* 115 */       break;
/*     */     default: 
/* 117 */       langPackDir = "localization" + File.separator + "www";
/*     */     }
/*     */     
/* 120 */     String monsterPath = langPackDir + File.separator + "monsters.json";
/*     */     
/* 122 */     Type monstersType = new TypeToken() {}.getType();
/* 123 */     monsters = (Map)gson.fromJson(loadJson(monsterPath), monstersType);
/*     */     
/* 125 */     String powerPath = langPackDir + File.separator + "powers.json";
/*     */     
/* 127 */     Type powerType = new TypeToken() {}.getType();
/* 128 */     powers = (Map)gson.fromJson(loadJson(powerPath), powerType);
/*     */     
/* 130 */     String cardPath = langPackDir + File.separator + "cards.json";
/*     */     
/* 132 */     Type cardType = new TypeToken() {}.getType();
/* 133 */     cards = (Map)gson.fromJson(loadJson(cardPath), cardType);
/*     */     
/* 135 */     String relicPath = langPackDir + File.separator + "relics.json";
/*     */     
/* 137 */     Type relicType = new TypeToken() {}.getType();
/* 138 */     relics = (Map)gson.fromJson(loadJson(relicPath), relicType);
/*     */     
/* 140 */     String eventPath = langPackDir + File.separator + "events.json";
/*     */     
/* 142 */     Type eventType = new TypeToken() {}.getType();
/* 143 */     events = (Map)gson.fromJson(loadJson(eventPath), eventType);
/*     */     
/* 145 */     String potionPath = langPackDir + File.separator + "potions.json";
/*     */     
/* 147 */     Type potionType = new TypeToken() {}.getType();
/* 148 */     potions = (Map)gson.fromJson(loadJson(potionPath), potionType);
/*     */     
/* 150 */     String creditPath = langPackDir + File.separator + "credits.json";
/*     */     
/* 152 */     Type creditType = new TypeToken() {}.getType();
/* 153 */     credits = (Map)gson.fromJson(loadJson(creditPath), creditType);
/*     */     
/* 155 */     String tutorialsPath = langPackDir + File.separator + "tutorials.json";
/*     */     
/* 157 */     Type tutorialType = new TypeToken() {}.getType();
/* 158 */     tutorials = (Map)gson.fromJson(loadJson(tutorialsPath), tutorialType);
/*     */     
/* 160 */     String keywordsPath = langPackDir + File.separator + "keywords.json";
/*     */     
/* 162 */     Type keywordsType = new TypeToken() {}.getType();
/* 163 */     keywords = (Map)gson.fromJson(loadJson(keywordsPath), keywordsType);
/*     */     
/* 165 */     String scoreBonusesPath = langPackDir + File.separator + "score_bonuses.json";
/*     */     
/* 167 */     Type scoreBonusType = new TypeToken() {}.getType();
/* 168 */     scoreBonuses = (Map)gson.fromJson(loadJson(scoreBonusesPath), scoreBonusType);
/*     */     
/* 170 */     String characterPath = langPackDir + File.separator + "characters.json";
/*     */     
/* 172 */     Type characterType = new TypeToken() {}.getType();
/* 173 */     characters = (Map)gson.fromJson(loadJson(characterPath), characterType);
/*     */     
/* 175 */     String uiPath = langPackDir + File.separator + "ui.json";
/*     */     
/* 177 */     Type uiType = new TypeToken() {}.getType();
/* 178 */     ui = (Map)gson.fromJson(loadJson(uiPath), uiType);
/*     */     
/* 180 */     String orbPath = langPackDir + File.separator + "orbs.json";
/*     */     
/* 182 */     Type orbType = new TypeToken() {}.getType();
/* 183 */     orb = (Map)gson.fromJson(loadJson(orbPath), orbType);
/*     */     
/* 185 */     String modPath = langPackDir + File.separator + "run_mods.json";
/*     */     
/* 187 */     Type modType = new TypeToken() {}.getType();
/* 188 */     mod = (Map)gson.fromJson(loadJson(modPath), modType);
/*     */     
/* 190 */     String blightPath = langPackDir + File.separator + "blights.json";
/*     */     
/* 192 */     Type blightType = new TypeToken() {}.getType();
/* 193 */     blights = (Map)gson.fromJson(loadJson(blightPath), blightType);
/*     */     
/* 195 */     String achievePath = langPackDir + File.separator + "achievements.json";
/*     */     
/* 197 */     Type achieveType = new TypeToken() {}.getType();
/* 198 */     achievements = (Map)gson.fromJson(loadJson(achievePath), achieveType);
/*     */     
/* 200 */     logger.info("Loc Strings load time: " + (System.currentTimeMillis() - startTime) + "ms");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PowerStrings getPowerStrings(String powerName)
/*     */   {
/* 209 */     return (PowerStrings)powers.get(powerName);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MonsterStrings getMonsterStrings(String monsterName)
/*     */   {
/* 218 */     return (MonsterStrings)monsters.get(monsterName);
/*     */   }
/*     */   
/*     */   public EventStrings getEventString(String eventName) {
/* 222 */     return (EventStrings)events.get(eventName);
/*     */   }
/*     */   
/*     */   public PotionStrings getPotionString(String potionName) {
/* 226 */     return (PotionStrings)potions.get(potionName);
/*     */   }
/*     */   
/*     */   public CreditStrings getCreditString(String creditName) {
/* 230 */     return (CreditStrings)credits.get(creditName);
/*     */   }
/*     */   
/*     */   public TutorialStrings getTutorialString(String tutorialName) {
/* 234 */     return (TutorialStrings)tutorials.get(tutorialName);
/*     */   }
/*     */   
/*     */   public KeywordStrings getKeywordString(String keywordName) {
/* 238 */     return (KeywordStrings)keywords.get(keywordName);
/*     */   }
/*     */   
/*     */   public CharacterStrings getCharacterString(String characterName) {
/* 242 */     return (CharacterStrings)characters.get(characterName);
/*     */   }
/*     */   
/*     */   public UIStrings getUIString(String uiName) {
/* 246 */     return (UIStrings)ui.get(uiName);
/*     */   }
/*     */   
/*     */   public OrbStrings getOrbString(String orbName) {
/* 250 */     return (OrbStrings)orb.get(orbName);
/*     */   }
/*     */   
/*     */   public RunModStrings getRunModString(String modName) {
/* 254 */     return (RunModStrings)mod.get(modName);
/*     */   }
/*     */   
/*     */   public BlightStrings getBlightString(String blightName) {
/* 258 */     return (BlightStrings)blights.get(blightName);
/*     */   }
/*     */   
/*     */   public ScoreBonusStrings getScoreString(String scoreName) {
/* 262 */     return (ScoreBonusStrings)scoreBonuses.get(scoreName);
/*     */   }
/*     */   
/*     */   public AchievementStrings getAchievementString(String achievementName) {
/* 266 */     return (AchievementStrings)achievements.get(achievementName);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CardStrings getCardStrings(String cardName)
/*     */   {
/* 275 */     return (CardStrings)cards.get(cardName);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RelicStrings getRelicStrings(String relicName)
/*     */   {
/* 284 */     return (RelicStrings)relics.get(relicName);
/*     */   }
/*     */   
/*     */   private static String loadJson(String jsonPath) {
/* 288 */     return Gdx.files.internal(jsonPath).readString(String.valueOf(StandardCharsets.UTF_8));
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\localization\LocalizedStrings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */