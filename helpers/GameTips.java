/*    */ package com.megacrit.cardcrawl.helpers;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Collections;
/*    */ 
/*    */ public class GameTips
/*    */ {
/* 11 */   private static final TutorialStrings tutorialStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getTutorialString("Random Tips");
/* 12 */   public static final String[] MSG = tutorialStrings.TEXT;
/* 13 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*    */   
/* 15 */   private ArrayList<String> tips = new ArrayList();
/*    */   
/*    */   public GameTips() {
/* 18 */     initialize();
/*    */   }
/*    */   
/*    */   public void initialize() {
/* 22 */     Collections.addAll(this.tips, MSG);
/*    */     
/*    */ 
/* 25 */     Collections.shuffle(this.tips);
/*    */   }
/*    */   
/*    */   public String getTip() {
/* 29 */     String retVal = (String)this.tips.remove(MathUtils.random(this.tips.size() - 1));
/* 30 */     if (this.tips.isEmpty()) {
/* 31 */       initialize();
/*    */     }
/*    */     
/* 34 */     return retVal;
/*    */   }
/*    */   
/*    */   public String getPotionTip() {
/* 38 */     return LABEL[0];
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\GameTips.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */