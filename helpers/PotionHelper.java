/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.math.RandomXS128;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.potions.AncientPotion;
/*     */ import com.megacrit.cardcrawl.potions.AttackPotion;
/*     */ import com.megacrit.cardcrawl.potions.BlockPotion;
/*     */ import com.megacrit.cardcrawl.potions.BloodPotion;
/*     */ import com.megacrit.cardcrawl.potions.DexterityPotion;
/*     */ import com.megacrit.cardcrawl.potions.EnergyPotion;
/*     */ import com.megacrit.cardcrawl.potions.EntropicBrew;
/*     */ import com.megacrit.cardcrawl.potions.EssenceOfSteel;
/*     */ import com.megacrit.cardcrawl.potions.ExplosivePotion;
/*     */ import com.megacrit.cardcrawl.potions.FairyPotion;
/*     */ import com.megacrit.cardcrawl.potions.FearPotion;
/*     */ import com.megacrit.cardcrawl.potions.FirePotion;
/*     */ import com.megacrit.cardcrawl.potions.FocusPotion;
/*     */ import com.megacrit.cardcrawl.potions.FruitJuice;
/*     */ import com.megacrit.cardcrawl.potions.GamblersBrew;
/*     */ import com.megacrit.cardcrawl.potions.GhostInAJar;
/*     */ import com.megacrit.cardcrawl.potions.LiquidBronze;
/*     */ import com.megacrit.cardcrawl.potions.PoisonPotion;
/*     */ import com.megacrit.cardcrawl.potions.PowerPotion;
/*     */ import com.megacrit.cardcrawl.potions.RegenPotion;
/*     */ import com.megacrit.cardcrawl.potions.SkillPotion;
/*     */ import com.megacrit.cardcrawl.potions.SmokeBomb;
/*     */ import com.megacrit.cardcrawl.potions.SneckoOil;
/*     */ import com.megacrit.cardcrawl.potions.SpeedPotion;
/*     */ import com.megacrit.cardcrawl.potions.SteroidPotion;
/*     */ import com.megacrit.cardcrawl.potions.StrengthPotion;
/*     */ import com.megacrit.cardcrawl.potions.SwiftPotion;
/*     */ import com.megacrit.cardcrawl.potions.WeakenPotion;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashSet;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ public class PotionHelper
/*     */ {
/*  46 */   private static final Logger logger = LogManager.getLogger(PotionHelper.class.getName());
/*  47 */   public static ArrayList<String> potions = new ArrayList();
/*     */   
/*  49 */   public static int POTION_COMMON_CHANCE = 70;
/*  50 */   public static int POTION_UNCOMMON_CHANCE = 25;
/*     */   
/*     */   public static void initialize(AbstractPlayer.PlayerClass chosenClass)
/*     */   {
/*  54 */     potions.clear();
/*     */     
/*     */ 
/*  57 */     switch (chosenClass) {
/*     */     case IRONCLAD: 
/*  59 */       potions.add("BloodPotion");
/*  60 */       break;
/*     */     case THE_SILENT: 
/*  62 */       potions.add("GhostInAJar");
/*  63 */       break;
/*     */     case DEFECT: 
/*  65 */       potions.add("FocusPotion");
/*  66 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*  72 */     potions.add("Block Potion");
/*  73 */     potions.add("Dexterity Potion");
/*  74 */     potions.add("Energy Potion");
/*  75 */     potions.add("Explosive Potion");
/*  76 */     potions.add("Fire Potion");
/*  77 */     potions.add("Strength Potion");
/*  78 */     potions.add("Swift Potion");
/*  79 */     potions.add("Poison Potion");
/*  80 */     potions.add("Weak Potion");
/*  81 */     potions.add("FearPotion");
/*  82 */     potions.add("SkillPotion");
/*  83 */     potions.add("AttackPotion");
/*  84 */     potions.add("PowerPotion");
/*  85 */     potions.add("SteroidPotion");
/*  86 */     potions.add("SpeedPotion");
/*     */     
/*     */ 
/*  89 */     potions.add("Regen Potion");
/*  90 */     potions.add("Ancient Potion");
/*  91 */     potions.add("LiquidBronze");
/*  92 */     potions.add("GamblersBrew");
/*  93 */     potions.add("EssenceOfSteel");
/*     */     
/*     */ 
/*  96 */     potions.add("Fruit Juice");
/*  97 */     potions.add("SneckoOil");
/*  98 */     potions.add("FairyPotion");
/*  99 */     potions.add("SmokeBomb");
/* 100 */     potions.add("EntropicBrew");
/*     */   }
/*     */   
/*     */   public static AbstractPotion getRandomPotion(ArrayList<String> exclusion) {
/* 104 */     ArrayList<String> potionsTmp = new ArrayList();
/*     */     
/*     */ 
/* 107 */     for (String s : potions) {
/* 108 */       boolean exclude = false;
/* 109 */       for (String s2 : exclusion) {
/* 110 */         if (s.equals(s2)) {
/* 111 */           logger.info(s + " EXCLUDED");
/* 112 */           exclude = true;
/*     */         }
/*     */       }
/*     */       
/* 116 */       if (!exclude) {
/* 117 */         potionsTmp.add(s);
/*     */       }
/*     */     }
/*     */     
/* 121 */     String randomKey = (String)potionsTmp.get(AbstractDungeon.potionRng.random.nextInt(potionsTmp.size()));
/* 122 */     return getPotion(randomKey);
/*     */   }
/*     */   
/*     */   public static AbstractPotion getRandomPotion(Random rng) {
/* 126 */     String randomKey = (String)potions.get(rng.random.nextInt(potions.size()));
/* 127 */     return getPotion(randomKey);
/*     */   }
/*     */   
/*     */   public static AbstractPotion getRandomPotion() {
/* 131 */     String randomKey = (String)potions.get(AbstractDungeon.potionRng.random.nextInt(potions.size()));
/* 132 */     return getPotion(randomKey);
/*     */   }
/*     */   
/*     */   public static boolean isAPotion(String key) {
/* 136 */     switch (key)
/*     */     {
/*     */ 
/*     */     case "BloodPotion": 
/*     */     case "GhostInAJar": 
/*     */     case "FocusPotion": 
/*     */     case "Block Potion": 
/*     */     case "Dexterity Potion": 
/*     */     case "Energy Potion": 
/*     */     case "Explosive Potion": 
/*     */     case "Fire Potion": 
/*     */     case "Strength Potion": 
/*     */     case "Swift Potion": 
/*     */     case "Poison Potion": 
/*     */     case "Weak Potion": 
/*     */     case "FearPotion": 
/*     */     case "SkillPotion": 
/*     */     case "AttackPotion": 
/*     */     case "PowerPotion": 
/*     */     case "SteroidPotion": 
/*     */     case "SpeedPotion": 
/*     */     case "Regen Potion": 
/*     */     case "Ancient Potion": 
/*     */     case "LiquidBronze": 
/*     */     case "GamblersBrew": 
/*     */     case "EssenceOfSteel": 
/*     */     case "Fruit Juice": 
/*     */     case "SneckoOil": 
/*     */     case "FairyPotion": 
/*     */     case "SmokeBomb": 
/*     */     case "EntropicBrew": 
/* 167 */       return true; }
/*     */     
/* 169 */     return false;
/*     */   }
/*     */   
/*     */   public static AbstractPotion getPotion(String name)
/*     */   {
/* 174 */     if ((name == null) || (name.equals(""))) {
/* 175 */       return null;
/*     */     }
/*     */     
/* 178 */     switch (name) {
/*     */     case "Block Potion": 
/* 180 */       return new BlockPotion();
/*     */     case "Dexterity Potion": 
/* 182 */       return new DexterityPotion();
/*     */     case "Energy Potion": 
/* 184 */       return new EnergyPotion();
/*     */     case "Explosive Potion": 
/* 186 */       return new ExplosivePotion();
/*     */     case "Fire Potion": 
/* 188 */       return new FirePotion();
/*     */     case "Strength Potion": 
/* 190 */       return new StrengthPotion();
/*     */     case "Swift Potion": 
/* 192 */       return new SwiftPotion();
/*     */     case "Poison Potion": 
/* 194 */       return new PoisonPotion();
/*     */     case "Weak Potion": 
/* 196 */       return new WeakenPotion();
/*     */     case "FearPotion": 
/* 198 */       return new FearPotion();
/*     */     case "SkillPotion": 
/* 200 */       return new SkillPotion();
/*     */     case "PowerPotion": 
/* 202 */       return new PowerPotion();
/*     */     case "AttackPotion": 
/* 204 */       return new AttackPotion();
/*     */     case "SteroidPotion": 
/* 206 */       return new SteroidPotion();
/*     */     case "SpeedPotion": 
/* 208 */       return new SpeedPotion();
/*     */     
/*     */     case "Ancient Potion": 
/* 211 */       return new AncientPotion();
/*     */     case "Regen Potion": 
/* 213 */       return new RegenPotion();
/*     */     case "GhostInAJar": 
/* 215 */       return new GhostInAJar();
/*     */     case "FocusPotion": 
/* 217 */       return new FocusPotion();
/*     */     case "LiquidBronze": 
/* 219 */       return new LiquidBronze();
/*     */     case "GamblersBrew": 
/* 221 */       return new GamblersBrew();
/*     */     case "EssenceOfSteel": 
/* 223 */       return new EssenceOfSteel();
/*     */     case "BloodPotion": 
/* 225 */       return new BloodPotion();
/*     */     
/*     */     case "Fruit Juice": 
/* 228 */       return new FruitJuice();
/*     */     case "SneckoOil": 
/* 230 */       return new SneckoOil();
/*     */     case "FairyPotion": 
/* 232 */       return new FairyPotion();
/*     */     case "SmokeBomb": 
/* 234 */       return new SmokeBomb();
/*     */     case "EntropicBrew": 
/* 236 */       return new EntropicBrew();
/*     */     
/*     */     case "Potion Slot": 
/* 239 */       return null;
/*     */     }
/* 241 */     logger.info("MISSING KEY: POTIONHELPER 37: " + name);
/* 242 */     return new FirePotion();
/*     */   }
/*     */   
/*     */   public static void uploadPotionData()
/*     */   {
/* 247 */     HashSet<String> potionIDs = new HashSet();
/*     */     
/* 249 */     initialize(AbstractPlayer.PlayerClass.IRONCLAD);
/* 250 */     potionIDs.addAll(potions);
/* 251 */     initialize(AbstractPlayer.PlayerClass.THE_SILENT);
/* 252 */     potionIDs.addAll(potions);
/* 253 */     initialize(AbstractPlayer.PlayerClass.DEFECT);
/* 254 */     potionIDs.addAll(potions);
/* 255 */     potions.clear();
/*     */     
/* 257 */     ArrayList<String> data = new ArrayList();
/* 258 */     for (String id : potionIDs) {
/* 259 */       data.add(getPotion(id).getUploadData());
/*     */     }
/* 261 */     BotDataUploader.uploadDataAsync(BotDataUploader.GameDataType.POTION_DATA, AbstractPotion.gameDataUploadHeader(), data);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\PotionHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */