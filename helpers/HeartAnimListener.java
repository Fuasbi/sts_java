/*    */ package com.megacrit.cardcrawl.helpers;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState.AnimationStateListener;
/*    */ import com.esotericsoftware.spine.Event;
/*    */ import com.esotericsoftware.spine.EventData;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ 
/*    */ public class HeartAnimListener
/*    */   implements AnimationState.AnimationStateListener
/*    */ {
/*    */   public void event(int trackIndex, Event event)
/*    */   {
/* 17 */     if ((!AbstractDungeon.isScreenUp) && (event.getData().getName().equals("maxbeat"))) {
/* 18 */       CardCrawlGame.sound.playA("HEART_BEAT", MathUtils.random(-0.05F, 0.05F));
/* 19 */       CardCrawlGame.sound.playA("HEART_BEAT", MathUtils.random(-0.05F, 0.05F));
/* 20 */       CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT, false);
/*    */     }
/*    */   }
/*    */   
/*    */   public void complete(int trackIndex, int loopCount) {}
/*    */   
/*    */   public void start(int trackIndex) {}
/*    */   
/*    */   public void end(int trackIndex) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\HeartAnimListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */