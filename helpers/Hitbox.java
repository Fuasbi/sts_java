/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ 
/*     */ 
/*     */ public class Hitbox
/*     */ {
/*  14 */   public boolean clicked = false; public boolean clickStarted = false; public boolean justHovered = false; public boolean hovered = false;
/*     */   public float height;
/*     */   
/*  17 */   public Hitbox(float width, float height) { this(-10000.0F, -10000.0F, width, height); }
/*     */   
/*     */   public Hitbox(float x, float y, float width, float height)
/*     */   {
/*  21 */     this.x = x;
/*  22 */     this.y = y;
/*  23 */     this.width = width;
/*  24 */     this.height = height;
/*  25 */     this.cX = (x + width / 2.0F);
/*  26 */     this.cY = (y + height / 2.0F);
/*     */   }
/*     */   
/*     */   public void update() {
/*  30 */     update(this.x, this.y);
/*  31 */     if ((this.clickStarted) && 
/*  32 */       (InputHelper.justReleasedClickLeft)) {
/*  33 */       if (this.hovered) {
/*  34 */         this.clicked = true;
/*     */       }
/*  36 */       this.clickStarted = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update(float x, float y)
/*     */   {
/*  42 */     if (AbstractDungeon.isFadingOut) {
/*  43 */       return;
/*     */     }
/*     */     
/*  46 */     this.x = x;
/*  47 */     this.y = y;
/*     */     
/*  49 */     if (this.justHovered) {
/*  50 */       this.justHovered = false;
/*     */     }
/*     */     
/*  53 */     if (!this.hovered) {
/*  54 */       this.hovered = ((InputHelper.mX > x) && (InputHelper.mX < x + this.width) && (InputHelper.mY > y) && (InputHelper.mY < y + this.height));
/*     */       
/*  56 */       if (this.hovered) {
/*  57 */         this.justHovered = true;
/*     */       }
/*     */     } else {
/*  60 */       this.hovered = ((InputHelper.mX > x) && (InputHelper.mX < x + this.width) && (InputHelper.mY > y) && (InputHelper.mY < y + this.height));
/*     */     }
/*     */   }
/*     */   
/*     */   public void encapsulatedUpdate(HitboxListener listener)
/*     */   {
/*  66 */     update();
/*     */     
/*  68 */     if (this.justHovered) {
/*  69 */       listener.hoverStarted(this);
/*  70 */     } else if ((this.hovered) && (InputHelper.justClickedLeft)) {
/*  71 */       this.clickStarted = true;
/*  72 */       listener.startClicking(this);
/*  73 */     } else if ((this.clicked) || ((this.hovered) && (CInputActionSet.select.isJustPressed()))) {
/*  74 */       CInputActionSet.select.unpress();
/*  75 */       this.clicked = false;
/*  76 */       listener.clicked(this);
/*     */     }
/*     */   }
/*     */   
/*     */   public void unhover() {
/*  81 */     this.hovered = false;
/*  82 */     this.justHovered = false;
/*     */   }
/*     */   
/*     */   public float width;
/*     */   public float cY;
/*     */   public float cX;
/*     */   public float y;
/*     */   public float x;
/*     */   public void move(float cX, float cY)
/*     */   {
/*  92 */     this.cX = cX;
/*  93 */     this.cY = cY;
/*  94 */     this.x = (cX - this.width / 2.0F);
/*  95 */     this.y = (cY - this.height / 2.0F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void translate(float x, float y)
/*     */   {
/* 105 */     this.x = x;
/* 106 */     this.y = y;
/* 107 */     this.cX = (x + this.width / 2.0F);
/* 108 */     this.cY = (y + this.height / 2.0F);
/*     */   }
/*     */   
/*     */   public void resize(float w, float h) {
/* 112 */     this.width = w;
/* 113 */     this.height = h;
/*     */   }
/*     */   
/*     */   public boolean intersects(Hitbox other) {
/* 117 */     return (this.x < other.x + other.width) && (this.x + this.width > other.x) && (this.y < other.y + other.height) && (this.y + this.height > other.y);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 122 */     if ((Settings.isDebug) || (Settings.isInfo)) {
/* 123 */       if (this.clickStarted) {
/* 124 */         sb.setColor(Color.CHARTREUSE);
/*     */       } else {
/* 126 */         sb.setColor(Color.RED);
/*     */       }
/* 128 */       sb.draw(ImageMaster.DEBUG_HITBOX_IMG, this.x, this.y, this.width, this.height);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\Hitbox.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */