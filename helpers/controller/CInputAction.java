/*    */ package com.megacrit.cardcrawl.helpers.controller;
/*    */ 
/*    */ import com.badlogic.gdx.Input.Keys;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class CInputAction
/*    */ {
/* 12 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("InputKeyNames");
/* 13 */   public static final Map<String, String> TEXT_CONVERSIONS = uiStrings.TEXT_DICT;
/*    */   public int keycode;
/* 15 */   boolean justPressed = false; boolean pressed = false; boolean justReleased = false;
/*    */   
/*    */   public CInputAction(int keycode) {
/* 18 */     this.keycode = keycode;
/*    */   }
/*    */   
/*    */   public int getKey() {
/* 22 */     return this.keycode;
/*    */   }
/*    */   
/*    */   public String getKeyString() {
/* 26 */     String keycodeStr = Input.Keys.toString(this.keycode);
/* 27 */     return (String)TEXT_CONVERSIONS.getOrDefault(keycodeStr, keycodeStr);
/*    */   }
/*    */   
/*    */   public com.badlogic.gdx.graphics.Texture getKeyImg() {
/* 31 */     switch (this.keycode) {
/*    */     case 0: 
/* 33 */       return ImageMaster.CONTROLLER_A;
/*    */     case 1: 
/* 35 */       return ImageMaster.CONTROLLER_B;
/*    */     case 2: 
/* 37 */       return ImageMaster.CONTROLLER_X;
/*    */     case 3: 
/* 39 */       return ImageMaster.CONTROLLER_Y;
/*    */     case 4: 
/* 41 */       return ImageMaster.CONTROLLER_LB;
/*    */     case 5: 
/* 43 */       return ImageMaster.CONTROLLER_RB;
/*    */     case 6: 
/* 45 */       return ImageMaster.CONTROLLER_BACK;
/*    */     case 7: 
/* 47 */       return ImageMaster.CONTROLLER_START;
/*    */     case 1000: 
/* 49 */       return ImageMaster.CONTROLLER_LS_DOWN;
/*    */     case -1000: 
/* 51 */       return ImageMaster.CONTROLLER_LS_UP;
/*    */     case 1001: 
/* 53 */       return ImageMaster.CONTROLLER_LS_RIGHT;
/*    */     case -1001: 
/* 55 */       return ImageMaster.CONTROLLER_LS_LEFT;
/*    */     case -1003: 
/*    */     case -1002: 
/*    */     case 1002: 
/*    */     case 1003: 
/* 60 */       return ImageMaster.CONTROLLER_BACK;
/*    */     case 1004: 
/* 62 */       return ImageMaster.CONTROLLER_LT;
/*    */     case -1004: 
/* 64 */       return ImageMaster.CONTROLLER_RT;
/*    */     case 2000: 
/* 66 */       return ImageMaster.CONTROLLER_D_DOWN;
/*    */     case -2000: 
/* 68 */       return ImageMaster.CONTROLLER_D_UP;
/*    */     case 2001: 
/* 70 */       return ImageMaster.CONTROLLER_D_RIGHT;
/*    */     case -2001: 
/* 72 */       return ImageMaster.CONTROLLER_D_LEFT;
/*    */     }
/* 74 */     return ImageMaster.CONTROLLER_A;
/*    */   }
/*    */   
/*    */   public boolean isJustPressed()
/*    */   {
/* 79 */     return this.justPressed;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void unpress()
/*    */   {
/* 86 */     this.justPressed = false;
/*    */   }
/*    */   
/*    */   public boolean isJustReleased() {
/* 90 */     return CInputHelper.isJustReleased(this.keycode);
/*    */   }
/*    */   
/*    */   public void remap(int newKeycode) {
/* 94 */     this.keycode = newKeycode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\controller\CInputAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */