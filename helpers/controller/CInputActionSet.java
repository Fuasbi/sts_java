/*     */ package com.megacrit.cardcrawl.helpers.controller;
/*     */ 
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ 
/*     */ public class CInputActionSet
/*     */ {
/*   7 */   public static Prefs prefs = com.megacrit.cardcrawl.helpers.SaveHelper.getPrefs("STSInputSettings_Controller");
/*     */   
/*     */   public static CInputAction select;
/*     */   
/*     */   public static CInputAction cancel;
/*     */   
/*     */   public static CInputAction topPanel;
/*     */   
/*     */   public static CInputAction proceed;
/*     */   
/*     */   public static CInputAction pageLeftViewDeck;
/*     */   
/*     */   public static CInputAction pageRightViewExhaust;
/*     */   
/*     */   public static CInputAction map;
/*     */   
/*     */   public static CInputAction settings;
/*     */   
/*     */   public static CInputAction drawPile;
/*     */   
/*     */   public static CInputAction discardPile;
/*     */   
/*     */   public static CInputAction up;
/*     */   
/*     */   public static CInputAction down;
/*     */   
/*     */   public static CInputAction left;
/*     */   
/*     */   public static CInputAction right;
/*     */   
/*     */   public static CInputAction inspectUp;
/*     */   
/*     */   public static CInputAction inspectDown;
/*     */   public static CInputAction inspectLeft;
/*     */   public static CInputAction inspectRight;
/*     */   public static CInputAction altUp;
/*     */   public static CInputAction altDown;
/*     */   public static CInputAction altLeft;
/*     */   public static CInputAction altRight;
/*     */   
/*     */   public static void load()
/*     */   {
/*  49 */     select = new CInputAction(prefs.getInteger("SELECT", 0));
/*  50 */     cancel = new CInputAction(prefs.getInteger("CANCEL", 1));
/*  51 */     topPanel = new CInputAction(prefs.getInteger("VIEW", 2));
/*  52 */     proceed = new CInputAction(prefs.getInteger("PROCEED", 3));
/*  53 */     pageLeftViewDeck = new CInputAction(prefs.getInteger("PAGE_LEFT_KEY", 4));
/*  54 */     pageRightViewExhaust = new CInputAction(prefs.getInteger("PAGE_RIGHT_KEY", 5));
/*  55 */     map = new CInputAction(prefs.getInteger("MAP", 6));
/*  56 */     settings = new CInputAction(prefs.getInteger("SETTINGS", 7));
/*     */     
/*     */ 
/*  59 */     up = new CInputAction(prefs.getInteger("LS_Y_POSITIVE", 64536));
/*  60 */     down = new CInputAction(prefs.getInteger("LS_Y_NEGATIVE", 1000));
/*  61 */     left = new CInputAction(prefs.getInteger("LS_X_NEGATIVE", 64535));
/*  62 */     right = new CInputAction(prefs.getInteger("LS_X_POSITIVE", 1001));
/*  63 */     inspectUp = new CInputAction(prefs.getInteger("RS_Y_POSITIVE", 64534));
/*  64 */     inspectDown = new CInputAction(prefs.getInteger("RS_Y_NEGATIVE", 1002));
/*  65 */     inspectLeft = new CInputAction(prefs.getInteger("RS_X_POSITIVE", 64533));
/*  66 */     inspectRight = new CInputAction(prefs.getInteger("RS_X_NEGATIVE", 1003));
/*  67 */     altUp = new CInputAction(prefs.getInteger("D_NORTH", 63536));
/*  68 */     altDown = new CInputAction(prefs.getInteger("D_SOUTH", 2000));
/*  69 */     altLeft = new CInputAction(prefs.getInteger("D_WEST", 63535));
/*  70 */     altRight = new CInputAction(prefs.getInteger("D_EAST", 2001));
/*     */     
/*     */ 
/*  73 */     drawPile = new CInputAction(prefs.getInteger("DRAW_PILE", 1004));
/*  74 */     discardPile = new CInputAction(prefs.getInteger("DISCARD_PILE", 64532));
/*     */     
/*  76 */     CInputHelper.actions.add(select);
/*  77 */     CInputHelper.actions.add(cancel);
/*  78 */     CInputHelper.actions.add(topPanel);
/*  79 */     CInputHelper.actions.add(proceed);
/*  80 */     CInputHelper.actions.add(pageLeftViewDeck);
/*  81 */     CInputHelper.actions.add(pageRightViewExhaust);
/*  82 */     CInputHelper.actions.add(map);
/*  83 */     CInputHelper.actions.add(settings);
/*  84 */     CInputHelper.actions.add(up);
/*  85 */     CInputHelper.actions.add(down);
/*  86 */     CInputHelper.actions.add(left);
/*  87 */     CInputHelper.actions.add(right);
/*  88 */     CInputHelper.actions.add(inspectUp);
/*  89 */     CInputHelper.actions.add(inspectDown);
/*  90 */     CInputHelper.actions.add(inspectLeft);
/*  91 */     CInputHelper.actions.add(inspectRight);
/*  92 */     CInputHelper.actions.add(altUp);
/*  93 */     CInputHelper.actions.add(altDown);
/*  94 */     CInputHelper.actions.add(altLeft);
/*  95 */     CInputHelper.actions.add(altRight);
/*  96 */     CInputHelper.actions.add(drawPile);
/*  97 */     CInputHelper.actions.add(discardPile); }
/*     */   
/*     */   private static final String LS_Y_POSITIVE = "LS_Y_POSITIVE";
/*     */   
/* 101 */   public static void save() { prefs.putInteger("SELECT", select.getKey());
/* 102 */     prefs.putInteger("CANCEL", cancel.getKey());
/* 103 */     prefs.putInteger("VIEW", topPanel.getKey());
/* 104 */     prefs.putInteger("PROCEED", proceed.getKey());
/* 105 */     prefs.putInteger("PAGE_LEFT_KEY", pageLeftViewDeck.getKey());
/* 106 */     prefs.putInteger("PAGE_RIGHT_KEY", pageRightViewExhaust.getKey());
/* 107 */     prefs.putInteger("MAP", map.getKey());
/* 108 */     prefs.putInteger("SETTINGS", settings.getKey());
/* 109 */     prefs.putInteger("LS_Y_POSITIVE", up.getKey());
/* 110 */     prefs.putInteger("LS_Y_NEGATIVE", down.getKey());
/* 111 */     prefs.putInteger("LS_X_NEGATIVE", left.getKey());
/* 112 */     prefs.putInteger("LS_X_POSITIVE", right.getKey());
/* 113 */     prefs.putInteger("RS_Y_POSITIVE", inspectUp.getKey());
/* 114 */     prefs.putInteger("RS_Y_NEGATIVE", inspectDown.getKey());
/* 115 */     prefs.putInteger("RS_X_POSITIVE", inspectLeft.getKey());
/* 116 */     prefs.putInteger("RS_X_NEGATIVE", inspectRight.getKey());
/* 117 */     prefs.putInteger("DRAW_PILE", drawPile.getKey());
/* 118 */     prefs.putInteger("DISCARD_PILE", discardPile.getKey());
/* 119 */     prefs.flush(); }
/*     */   
/*     */   private static final String LS_Y_NEGATIVE = "LS_Y_NEGATIVE";
/*     */   
/* 123 */   public static void resetToDefaults() { select.remap(0);
/* 124 */     cancel.remap(1);
/* 125 */     topPanel.remap(2);
/* 126 */     proceed.remap(3);
/* 127 */     pageLeftViewDeck.remap(4);
/* 128 */     pageRightViewExhaust.remap(5);
/* 129 */     map.remap(6);
/* 130 */     settings.remap(7);
/* 131 */     up.remap(64536);
/* 132 */     down.remap(1000);
/* 133 */     left.remap(64535);
/* 134 */     right.remap(1001);
/* 135 */     inspectUp.remap(1002);
/* 136 */     inspectDown.remap(64534);
/* 137 */     inspectLeft.remap(64533);
/* 138 */     inspectRight.remap(1003);
/* 139 */     altUp.remap(63536);
/* 140 */     altDown.remap(2000);
/* 141 */     altLeft.remap(63535);
/* 142 */     altRight.remap(2001);
/* 143 */     drawPile.remap(1004);
/* 144 */     discardPile.remap(64532);
/*     */   }
/*     */   
/*     */   private static final String LS_X_POSITIVE = "LS_X_POSITIVE";
/*     */   private static final String LS_X_NEGATIVE = "LS_X_NEGATIVE";
/*     */   private static final String RS_Y_POSITIVE = "RS_Y_POSITIVE";
/*     */   private static final String RS_Y_NEGATIVE = "RS_Y_NEGATIVE";
/*     */   private static final String RS_X_POSITIVE = "RS_X_POSITIVE";
/*     */   private static final String RS_X_NEGATIVE = "RS_X_NEGATIVE";
/*     */   private static final String D_NORTH = "D_NORTH";
/*     */   private static final String D_SOUTH = "D_SOUTH";
/*     */   private static final String D_WEST = "D_WEST";
/*     */   private static final String D_EAST = "D_EAST";
/*     */   private static final String SELECT_KEY = "SELECT";
/*     */   private static final String CANCEL_KEY = "CANCEL";
/*     */   private static final String TOP_PANEL_KEY = "VIEW";
/*     */   private static final String PROCEED_KEY = "PROCEED";
/*     */   private static final String PAGE_LEFT_KEY = "PAGE_LEFT_KEY";
/*     */   private static final String PAGE_RIGHT_KEY = "PAGE_RIGHT_KEY";
/*     */   private static final String MAP_KEY = "MAP";
/*     */   private static final String SETTINGS_KEY = "SETTINGS";
/*     */   private static final String DRAW_PILE_KEY = "DRAW_PILE";
/*     */   private static final String DISCARD_PILE_KEY = "DISCARD_PILE";
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\controller\CInputActionSet.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */