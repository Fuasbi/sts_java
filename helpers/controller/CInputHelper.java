/*     */ package com.megacrit.cardcrawl.helpers.controller;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.controllers.Controller;
/*     */ import com.badlogic.gdx.controllers.Controllers;
/*     */ import com.badlogic.gdx.utils.Array;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ import org.lwjgl.opengl.Display;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CInputHelper
/*     */ {
/*  21 */   private static final Logger logger = LogManager.getLogger(CInputHelper.class.getName());
/*  22 */   public static Array<Controller> controllers = null;
/*  23 */   public static Controller controller = null;
/*  24 */   public static ArrayList<CInputAction> actions = new ArrayList();
/*  25 */   public static CInputListener listener = null;
/*  26 */   private static boolean initializedController = false;
/*  27 */   public static ControllerModel model = null;
/*     */   public static void loadSettings() {}
/*     */   
/*  30 */   public static enum ControllerModel { XBOX_360,  XBOX_ONE,  PS3,  PS4,  STEAM,  OTHER;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     private ControllerModel() {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void initializeIfAble()
/*     */   {
/*  42 */     if ((!initializedController) && (Display.isActive())) {
/*  43 */       initializedController = true;
/*  44 */       logger.info("[CONTROLLER] Utilizing DirectInput");
/*  45 */       controllers = Controllers.getControllers();
/*     */       
/*  47 */       if (controllers == null) {
/*  48 */         logger.info("[ERROR] getControllers() returned null");
/*  49 */         return;
/*     */       }
/*     */       
/*  52 */       for (int i = 0; i < controllers.size; i++) {
/*  53 */         logger.info("CONTROLLER[" + i + "] " + ((Controller)controllers.get(i)).getName());
/*     */       }
/*     */       
/*  56 */       if (controllers.size != 0) {
/*  57 */         Settings.isControllerMode = true;
/*  58 */         listener = new CInputListener();
/*  59 */         controller = (Controller)controllers.first();
/*  60 */         controller.addListener(listener);
/*     */         
/*     */ 
/*  63 */         if (controller.getName().contains("360")) {
/*  64 */           model = ControllerModel.XBOX_360;
/*  65 */           ImageMaster.loadControllerImages(ControllerModel.XBOX_360);
/*  66 */         } else if (controller.getName().contains("Xbox One")) {
/*  67 */           model = ControllerModel.XBOX_ONE;
/*  68 */           ImageMaster.loadControllerImages(ControllerModel.XBOX_ONE);
/*     */         }
/*     */         else {
/*  71 */           model = ControllerModel.XBOX_360;
/*  72 */           ImageMaster.loadControllerImages(ControllerModel.XBOX_360);
/*     */         }
/*     */       } else {
/*  75 */         logger.info("[CONTROLLER] No controllers detected");
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public static void updateFirst() {}
/*     */   
/*     */   public static void setCursor(Hitbox hb)
/*     */   {
/*  84 */     Gdx.input.setCursorPosition((int)hb.cX, Settings.HEIGHT - (int)hb.cY);
/*     */   }
/*     */   
/*     */   public static void updateLast() {
/*  88 */     for (CInputAction a : actions) {
/*  89 */       a.justPressed = false;
/*  90 */       a.justReleased = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean listenerPress(int keycode) {
/*  95 */     for (CInputAction a : actions) {
/*  96 */       if (a.keycode == keycode) {
/*  97 */         a.justPressed = true;
/*  98 */         break;
/*     */       }
/*     */     }
/* 101 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean listenerRelease(int keycode) {
/* 105 */     for (CInputAction a : actions) {
/* 106 */       if (a.keycode == keycode) {
/* 107 */         a.justReleased = true;
/* 108 */         break;
/*     */       }
/*     */     }
/* 111 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isJustPressed(int keycode) {
/* 115 */     for (CInputAction a : actions) {
/* 116 */       if (a.keycode == keycode) {
/* 117 */         return a.justPressed;
/*     */       }
/*     */     }
/* 120 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isJustReleased(int keycode) {
/* 124 */     for (CInputAction a : actions) {
/* 125 */       if (a.keycode == keycode) {
/* 126 */         return a.justReleased;
/*     */       }
/*     */     }
/* 129 */     return false;
/*     */   }
/*     */   
/*     */   public static void regainInputFocus() {
/* 133 */     CInputListener.remapping = false;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\controller\CInputHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */