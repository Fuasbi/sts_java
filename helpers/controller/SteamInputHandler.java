/*     */ package com.megacrit.cardcrawl.helpers.controller;
/*     */ 
/*     */ import com.codedisaster.steamworks.SteamAPI;
/*     */ import com.codedisaster.steamworks.SteamController;
/*     */ import com.codedisaster.steamworks.SteamController.Pad;
/*     */ import com.codedisaster.steamworks.SteamControllerAnalogActionData;
/*     */ import com.codedisaster.steamworks.SteamControllerAnalogActionHandle;
/*     */ import com.codedisaster.steamworks.SteamControllerDigitalActionData;
/*     */ import com.codedisaster.steamworks.SteamControllerDigitalActionHandle;
/*     */ import com.codedisaster.steamworks.SteamControllerHandle;
/*     */ import com.codedisaster.steamworks.SteamException;
/*     */ import com.codedisaster.steamworks.SteamNativeHandle;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import java.io.PrintStream;
/*     */ import java.util.Scanner;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SteamInputHandler implements Runnable
/*     */ {
/*  20 */   public static volatile boolean alive = false;
/*  21 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SteamInputHandler.class.getName());
/*     */   
/*     */   public static volatile SteamController controller;
/*     */   
/*  25 */   public static volatile SteamControllerHandle[] controllerHandles = new SteamControllerHandle[0];
/*  26 */   public static volatile int numControllers = 0;
/*     */   public static volatile com.codedisaster.steamworks.SteamControllerActionSetHandle setHandle;
/*     */   public static volatile SteamControllerDigitalActionHandle digitalActionHandle;
/*     */   public static volatile SteamControllerAnalogActionHandle analogActionHandle;
/*  30 */   public static volatile SteamControllerDigitalActionData digitalActionData = new SteamControllerDigitalActionData();
/*  31 */   public static volatile SteamControllerAnalogActionData analogActionData = new SteamControllerAnalogActionData();
/*     */   
/*     */   public SteamInputHandler() {
/*  34 */     alive = true;
/*  35 */     SteamAPI.printDebugInfo(System.out);
/*  36 */     registerInterfaces();
/*     */   }
/*     */   
/*     */   protected void registerInterfaces() {
/*  40 */     logger.info("Register controller API ...");
/*  41 */     controller = new SteamController();
/*  42 */     controller.init();
/*     */     try
/*     */     {
/*  45 */       processInput("c list");
/*     */     } catch (SteamException e) {
/*  47 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public void run()
/*     */   {
/*     */     try {
/*  54 */       while ((alive) && (SteamAPI.isSteamRunning())) {
/*  55 */         System.out.println("Boop");
/*  56 */         if (CardCrawlGame.sControllerScanner.hasNext()) {
/*  57 */           String input = CardCrawlGame.sControllerScanner.next();
/*  58 */           if ((input.equals("quit")) || (input.equals("exit"))) {
/*  59 */             alive = false;
/*     */           } else {
/*     */             try {
/*  62 */               processInput(input);
/*     */             } catch (NumberFormatException e) {
/*  64 */               e.printStackTrace();
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*  69 */       System.out.println("DEATH");
/*     */     } catch (SteamException e) {
/*  71 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean alive() {
/*  76 */     return alive;
/*     */   }
/*     */   
/*     */   protected void processInput(String input) throws SteamException {
/*  80 */     logger.info("[CONTROLLER] processInput: " + input);
/*  81 */     if (input.equals("c list")) {
/*  82 */       controllerHandles = new SteamControllerHandle[16];
/*  83 */       numControllers = controller.getConnectedControllers(controllerHandles);
/*     */       
/*  85 */       logger.info(numControllers + " controllers found");
/*  86 */       logger.info(numControllers + " controllers found");
/*  87 */       logger.info(numControllers + " controllers found");
/*  88 */       logger.info(numControllers + " controllers found");
/*  89 */       logger.info(numControllers + " controllers found");
/*  90 */       logger.info(numControllers + " controllers found");
/*  91 */       logger.info(numControllers + " controllers found");
/*  92 */       logger.info(numControllers + " controllers found");
/*  93 */       logger.info(numControllers + " controllers found");
/*  94 */       logger.info(numControllers + " controllers found");
/*     */       
/*  96 */       for (int i = 0; i < numControllers; i++) {
/*  97 */         logger.info("  " + i + ": " + controllerHandles[i]);
/*     */       }
/*  99 */     } else if (input.startsWith("c pulse ")) {
/* 100 */       String[] params = input.substring("controller pulse ".length()).split(" ");
/* 101 */       if (params.length > 1) {
/* 102 */         SteamController.Pad pad = "left".equals(params[0]) ? SteamController.Pad.Left : SteamController.Pad.Right;
/*     */         
/* 104 */         if (params.length == 2) {
/* 105 */           controller.triggerHapticPulse(controllerHandles[0], pad, Short.parseShort(params[1]));
/* 106 */         } else if (params.length == 4) {
/* 107 */           controller.triggerRepeatedHapticPulse(controllerHandles[0], pad, 
/*     */           
/*     */ 
/* 110 */             Short.parseShort(params[1]), 
/* 111 */             Short.parseShort(params[2]), 
/* 112 */             Short.parseShort(params[3]), 0);
/*     */         }
/*     */       }
/*     */     }
/* 116 */     else if (input.startsWith("c set ")) {
/* 117 */       String setName = input.substring("c set ".length());
/* 118 */       setHandle = controller.getActionSetHandle(setName);
/* 119 */       logger.info("  handle for set '" + setName + "': " + SteamNativeHandle.getNativeHandle(setHandle));
/* 120 */     } else if (input.startsWith("c d action ")) {
/* 121 */       String actionName = input.substring("c d action ".length());
/* 122 */       digitalActionHandle = controller.getDigitalActionHandle(actionName);
/* 123 */       logger.info("  handle for digital '" + actionName + "': " + SteamNativeHandle.getNativeHandle(digitalActionHandle));
/* 124 */     } else if (input.startsWith("c a action ")) {
/* 125 */       String actionName = input.substring("c a action ".length());
/* 126 */       analogActionHandle = controller.getAnalogActionHandle(actionName);
/* 127 */       logger.info("  handle for analog '" + actionName + "': " + SteamNativeHandle.getNativeHandle(analogActionHandle));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\controller\SteamInputHandler.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */