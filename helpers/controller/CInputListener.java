/*     */ package com.megacrit.cardcrawl.helpers.controller;
/*     */ 
/*     */ import com.badlogic.gdx.controllers.Controller;
/*     */ import com.badlogic.gdx.controllers.PovDirection;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.screens.options.RemapInputElement;
/*     */ import com.megacrit.cardcrawl.screens.options.RemapInputElementListener;
/*     */ import java.io.PrintStream;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class CInputListener implements com.badlogic.gdx.controllers.ControllerListener
/*     */ {
/*  15 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(CInputListener.class.getName());
/*     */   private static final float DEADZONE = 0.5F;
/*  17 */   private float[] axisValues = { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F };
/*  18 */   public static boolean remapping = false;
/*  19 */   public static RemapInputElement element = null;
/*     */   
/*     */   public static void listenForRemap(RemapInputElement e) {
/*  22 */     element = e;
/*  23 */     remapping = true;
/*     */   }
/*     */   
/*     */   public void connected(Controller controller)
/*     */   {
/*  28 */     logger.info("CONNECTED: " + controller.getName());
/*     */   }
/*     */   
/*     */   public void disconnected(Controller controller)
/*     */   {
/*  33 */     logger.info("DISCONNECTED: " + controller.getName());
/*     */   }
/*     */   
/*     */   public boolean buttonDown(Controller controller, int buttonCode)
/*     */   {
/*  38 */     if (!Settings.CONTROLLER_ENABLED) {
/*  39 */       return false;
/*     */     }
/*     */     
/*     */ 
/*  43 */     if (remapping) {
/*  44 */       if (element.listener.willRemapController(element, element.cAction.keycode, buttonCode)) {
/*  45 */         element.cAction.remap(buttonCode);
/*     */       }
/*  47 */       remapping = false;
/*  48 */       element.hasInputFocus = false;
/*  49 */       InputHelper.regainInputFocus();
/*  50 */       CInputHelper.regainInputFocus();
/*  51 */       element = null;
/*  52 */       return false;
/*     */     }
/*     */     
/*  55 */     if (!Settings.isControllerMode) {
/*  56 */       Settings.isControllerMode = true;
/*  57 */       System.out.println("Entering controller mode: BUTTON pressed");
/*     */     }
/*  59 */     CInputHelper.listenerPress(buttonCode);
/*  60 */     return false;
/*     */   }
/*     */   
/*     */   public boolean buttonUp(Controller controller, int buttonCode)
/*     */   {
/*  65 */     CInputHelper.listenerRelease(buttonCode);
/*  66 */     return false;
/*     */   }
/*     */   
/*     */   public boolean axisMoved(Controller controller, int axisCode, float value)
/*     */   {
/*  71 */     if (!Settings.CONTROLLER_ENABLED) {
/*  72 */       return false;
/*     */     }
/*     */     
/*     */ 
/*  76 */     if (remapping) {
/*  77 */       if ((value > 0.5F) && (this.axisValues[axisCode] < 0.5F)) {
/*  78 */         if (element.listener.willRemapController(element, element.cAction.keycode, 1000 + axisCode)) {
/*  79 */           element.cAction.remap(1000 + axisCode);
/*     */         }
/*  81 */         remapping = false;
/*  82 */         element.hasInputFocus = false;
/*  83 */         InputHelper.regainInputFocus();
/*  84 */         CInputHelper.regainInputFocus();
/*  85 */         element = null;
/*  86 */         return false; }
/*  87 */       if ((value < -0.5F) && (this.axisValues[axisCode] > -0.5F)) {
/*  88 */         if (element.listener.willRemapController(element, element.cAction.keycode, 64536 - axisCode)) {
/*  89 */           element.cAction.remap(64536 - axisCode);
/*     */         }
/*  91 */         remapping = false;
/*  92 */         element.hasInputFocus = false;
/*  93 */         InputHelper.regainInputFocus();
/*  94 */         CInputHelper.regainInputFocus();
/*  95 */         element = null;
/*  96 */         return false;
/*     */       }
/*     */     }
/*     */     
/* 100 */     if ((value > 0.5F) && (this.axisValues[axisCode] < 0.5F)) {
/* 101 */       CInputHelper.listenerPress(1000 + axisCode);
/* 102 */       if (!Settings.isControllerMode) {
/* 103 */         Settings.isControllerMode = true;
/* 104 */         System.out.println("Entering controller mode: AXIS moved");
/*     */       }
/* 106 */     } else if ((value < -0.5F) && (this.axisValues[axisCode] > -0.5F)) {
/* 107 */       CInputHelper.listenerPress(64536 - axisCode);
/* 108 */       if (!Settings.isControllerMode) {
/* 109 */         Settings.isControllerMode = true;
/* 110 */         System.out.println("Entering controller mode: AXIS moved");
/*     */       }
/*     */     }
/* 113 */     this.axisValues[axisCode] = value;
/*     */     
/* 115 */     return false;
/*     */   }
/*     */   
/*     */   public boolean povMoved(Controller controller, int povCode, PovDirection value)
/*     */   {
/* 120 */     if (!Settings.CONTROLLER_ENABLED) {
/* 121 */       return false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 126 */     if (remapping) {
/* 127 */       int code = 63536;
/* 128 */       switch (value) {
/*     */       case north: 
/* 130 */         code = 63536;
/* 131 */         break;
/*     */       case south: 
/* 133 */         code = 2000;
/* 134 */         break;
/*     */       case northWest: 
/*     */       case southWest: 
/*     */       case west: 
/* 138 */         code = 63535;
/* 139 */         break;
/*     */       case northEast: 
/*     */       case southEast: 
/*     */       case east: 
/* 143 */         code = 2001;
/* 144 */         break;
/*     */       default: 
/* 146 */         code = 63536;
/*     */       }
/*     */       
/*     */       
/* 150 */       if (element.listener.willRemapController(element, element.cAction.keycode, code)) {
/* 151 */         element.cAction.remap(code);
/*     */       }
/* 153 */       remapping = false;
/* 154 */       element.hasInputFocus = false;
/* 155 */       InputHelper.regainInputFocus();
/* 156 */       CInputHelper.regainInputFocus();
/* 157 */       element = null;
/* 158 */       return false;
/*     */     }
/*     */     
/* 161 */     if (!Settings.isControllerMode) {
/* 162 */       Settings.isControllerMode = true;
/* 163 */       System.out.println("Entering controller mode: D-Pad pressed");
/*     */     }
/*     */     
/* 166 */     switch (value) {
/*     */     case north: 
/* 168 */       CInputHelper.listenerPress(63536);
/* 169 */       break;
/*     */     case south: 
/* 171 */       CInputHelper.listenerPress(2000);
/* 172 */       break;
/*     */     case northWest: 
/*     */     case southWest: 
/*     */     case west: 
/* 176 */       CInputHelper.listenerPress(63535);
/* 177 */       break;
/*     */     case northEast: 
/*     */     case southEast: 
/*     */     case east: 
/* 181 */       CInputHelper.listenerPress(2001);
/* 182 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 187 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean xSliderMoved(Controller controller, int sliderCode, boolean value)
/*     */   {
/* 193 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean ySliderMoved(Controller controller, int sliderCode, boolean value)
/*     */   {
/* 199 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value)
/*     */   {
/* 205 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\controller\CInputListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */