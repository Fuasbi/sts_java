/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.JsonSyntaxException;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.STSSentry;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType;
/*     */ import com.megacrit.cardcrawl.vfx.GameSavedEffect;
/*     */ import java.io.File;
/*     */ import java.lang.reflect.Type;
/*     */ import java.nio.charset.StandardCharsets;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SaveHelper
/*     */ {
/*  26 */   private static final Logger logger = LogManager.getLogger(SaveHelper.class.getName());
/*  27 */   private static String DIR = "preferences";
/*  28 */   private static String DIR_BETA = "betaPreferences";
/*     */   
/*     */   public static void initialize() {}
/*     */   
/*     */   public static boolean doesPrefExist(String name) {
/*     */     String filepath;
/*     */     String filepath;
/*  35 */     if (Settings.isBeta) {
/*  36 */       filepath = DIR_BETA + File.separator + name;
/*     */     } else {
/*  38 */       filepath = DIR + File.separator + name;
/*     */     }
/*  40 */     return Gdx.files.local(filepath).exists();
/*     */   }
/*     */   
/*     */   public static void deletePrefs(int slot) {
/*  44 */     String dir = DIR + File.separator;
/*  45 */     if (Settings.isBeta) {
/*  46 */       dir = DIR_BETA + File.separator;
/*     */     }
/*     */     
/*     */ 
/*  50 */     deleteFile(dir + slotName("STSDataVagabond", slot));
/*  51 */     deleteFile(dir + slotName("STSDataTheSilent", slot));
/*  52 */     deleteFile(dir + slotName("STSDataDefect", slot));
/*     */     
/*     */ 
/*  55 */     deleteFile(dir + slotName("STSAchievements", slot));
/*  56 */     deleteFile(dir + slotName("STSDaily", slot));
/*  57 */     deleteFile(dir + slotName("STSSeenBosses", slot));
/*  58 */     deleteFile(dir + slotName("STSSeenCards", slot));
/*  59 */     deleteFile(dir + slotName("STSSeenRelics", slot));
/*  60 */     deleteFile(dir + slotName("STSUnlockProgress", slot));
/*  61 */     deleteFile(dir + slotName("STSUnlocks", slot));
/*     */     
/*     */ 
/*  64 */     deleteFile(dir + slotName("STSGameplaySettings", slot));
/*  65 */     deleteFile(dir + slotName("STSInputSettings", slot));
/*  66 */     deleteFile(dir + slotName("STSInputSettings_Controller", slot));
/*  67 */     deleteFile(dir + slotName("STSSound", slot));
/*     */     
/*     */ 
/*  70 */     deleteFile(dir + slotName("STSPlayer", slot));
/*  71 */     deleteFile(dir + slotName("STSTips", slot));
/*     */     
/*     */ 
/*  74 */     dir = "runs" + File.separator;
/*  75 */     deleteFolder(dir + slotName("IRONCLAD", slot));
/*  76 */     deleteFolder(dir + slotName("THE_SILENT", slot));
/*  77 */     deleteFolder(dir + slotName("DEFECT", slot));
/*  78 */     deleteFolder(dir + slotName("DAILY", slot));
/*     */     
/*     */ 
/*  81 */     dir = "saves" + File.separator;
/*  82 */     if (!Settings.isBeta) {
/*  83 */       deleteFile(dir + slotName("IRONCLAD.autosave", slot));
/*  84 */       deleteFile(dir + slotName("DEFECT.autosave", slot));
/*  85 */       deleteFile(dir + slotName("THE_SILENT.autosave", slot));
/*     */     } else {
/*  87 */       deleteFile(dir + slotName("IRONCLAD.autosaveBETA", slot));
/*  88 */       deleteFile(dir + slotName("DEFECT.autosaveBETA", slot));
/*  89 */       deleteFile(dir + slotName("THE_SILENT.autosaveBETA", slot));
/*     */     }
/*     */     
/*     */ 
/*  93 */     CardCrawlGame.saveSlotPref.putString(slotName("PROFILE_NAME", slot), "");
/*  94 */     CardCrawlGame.saveSlotPref.putFloat(slotName("COMPLETION", slot), 0.0F);
/*  95 */     CardCrawlGame.saveSlotPref.putLong(slotName("PLAYTIME", slot), 0L);
/*  96 */     CardCrawlGame.saveSlotPref.flush();
/*     */     
/*     */ 
/*  99 */     if ((slot == CardCrawlGame.saveSlot) || (CardCrawlGame.saveSlot == -1)) {
/* 100 */       String name = "";
/* 101 */       boolean newDefaultSet = false;
/* 102 */       for (int i = 0; i < 3; i++) {
/* 103 */         name = CardCrawlGame.saveSlotPref.getString(slotName("PROFILE_NAME", slot), "");
/* 104 */         if (!name.equals("")) {
/* 105 */           logger.info("Current slots deleted, DEFAULT_SLOT is now " + i);
/* 106 */           CardCrawlGame.saveSlotPref.putInteger("DEFAULT_SLOT", i);
/* 107 */           newDefaultSet = true;
/* 108 */           break;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 113 */       if (!newDefaultSet) {
/* 114 */         logger.info("All slots deleted, DEFAULT_SLOT is now -1");
/* 115 */         CardCrawlGame.saveSlotPref.putInteger("DEFAULT_SLOT", -1);
/*     */       }
/*     */       
/* 118 */       CardCrawlGame.saveSlotPref.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   private static void deleteFile(String fileName) {
/* 123 */     logger.info("Deleting " + fileName);
/* 124 */     if (Gdx.files.local(fileName).delete()) {
/* 125 */       logger.info(fileName + " deleted.");
/*     */     }
/*     */     
/* 128 */     if (Gdx.files.local(fileName + ".backUp").delete()) {
/* 129 */       logger.info(fileName + ".backUp deleted.");
/*     */     }
/*     */   }
/*     */   
/*     */   private static void deleteFolder(String dirName) {
/* 134 */     logger.info("Deleting " + dirName);
/* 135 */     if (Gdx.files.local(dirName).deleteDirectory()) {
/* 136 */       logger.info(dirName + " deleted.");
/*     */     }
/*     */   }
/*     */   
/*     */   public static String slotName(String name, int slot) {
/* 141 */     switch (slot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */     case 1: 
/*     */     case 2: 
/*     */     default: 
/* 149 */       name = slot + "_" + name;
/*     */     }
/*     */     
/* 152 */     return name;
/*     */   }
/*     */   
/*     */   public static Prefs getPrefs(String name) {
/* 156 */     switch (CardCrawlGame.saveSlot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */     case 1: 
/*     */     case 2: 
/*     */     default: 
/* 164 */       name = CardCrawlGame.saveSlot + "_" + name;
/*     */     }
/*     */     
/*     */     
/* 168 */     Gson gson = new Gson();
/* 169 */     Prefs retVal = new Prefs();
/*     */     
/*     */ 
/* 172 */     Type type = new com.google.gson.reflect.TypeToken() {}.getType();
/* 174 */     String filepath = DIR + File.separator + name;
/*     */     
/* 176 */     if (Settings.isBeta) {
/* 177 */       filepath = DIR_BETA + File.separator + name;
/*     */     }
/*     */     
/* 180 */     String jsonStr = null;
/*     */     try {
/* 182 */       jsonStr = loadJson(filepath);
/* 183 */       if (jsonStr.isEmpty()) {
/* 184 */         logger.error("Empty Pref file: name=" + name + ", filepath=" + filepath);
/* 185 */         handleCorruption(jsonStr, filepath, name);
/* 186 */         retVal = getPrefs(name);
/*     */       } else {
/* 188 */         retVal.data = ((Map)gson.fromJson(jsonStr, type));
/*     */       }
/*     */     } catch (JsonSyntaxException e) {
/* 191 */       logger.error("Corrupt Pref file", e);
/* 192 */       handleCorruption(jsonStr, filepath, name);
/* 193 */       retVal = getPrefs(name);
/*     */     }
/* 195 */     retVal.filepath = filepath;
/* 196 */     return retVal;
/*     */   }
/*     */   
/*     */   private static void handleCorruption(String jsonStr, String filepath, String name) {
/* 200 */     STSSentry.attachToEvent("corruptPref", jsonStr);
/* 201 */     preserveCorruptFile(filepath);
/* 202 */     FileHandle backup = Gdx.files.local(filepath + ".backUp");
/* 203 */     if (backup.exists()) {
/* 204 */       backup.moveTo(Gdx.files.local(filepath));
/* 205 */       logger.info("Original corrupted, backup loaded for " + filepath);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void preserveCorruptFile(String filePath)
/*     */   {
/* 213 */     FileHandle file = Gdx.files.local(filePath);
/* 214 */     FileHandle corruptFile = Gdx.files.local("sendToDevs" + File.separator + filePath + ".corrupt");
/* 215 */     file.moveTo(corruptFile);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String loadJson(String filepath)
/*     */   {
/* 225 */     if (Gdx.files.local(filepath).exists()) {
/* 226 */       return Gdx.files.local(filepath).readString(String.valueOf(StandardCharsets.UTF_8));
/*     */     }
/* 228 */     Map<String, String> map = new HashMap();
/* 229 */     Gson gson = new Gson();
/* 230 */     AsyncSaver.save(filepath, gson.toJson(map));
/* 231 */     return "{}";
/*     */   }
/*     */   
/*     */   public static boolean saveExists()
/*     */   {
/* 236 */     StringBuilder retVal = new StringBuilder();
/*     */     
/* 238 */     if (Settings.isBeta) {
/* 239 */       retVal.append(DIR_BETA + File.separator);
/*     */     } else {
/* 241 */       retVal.append(DIR + File.separator);
/*     */     }
/*     */     
/* 244 */     switch (CardCrawlGame.saveSlot) {
/*     */     case 0: 
/* 246 */       retVal.append("STSPlayer");
/* 247 */       break;
/*     */     
/*     */ 
/*     */     case 1: 
/*     */     case 2: 
/*     */     case 3: 
/* 253 */       retVal.append(CardCrawlGame.saveSlot + "_STSPlayer");
/* 254 */       break;
/*     */     default: 
/* 256 */       retVal.append("STSPlayer");
/*     */     }
/*     */     
/*     */     
/* 260 */     return Gdx.files.local(retVal.toString()).exists();
/*     */   }
/*     */   
/*     */   public static void saveIfAppropriate(SaveFile.SaveType saveType) {
/* 264 */     if (!shouldSave()) {
/* 265 */       return;
/*     */     }
/* 267 */     SaveFile saveFile = new SaveFile(saveType);
/* 268 */     SaveAndContinue.save(saveFile);
/* 269 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new GameSavedEffect());
/*     */   }
/*     */   
/*     */   public static boolean shouldSave() {
/* 273 */     return !Settings.isDemo;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\SaveHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */