package com.megacrit.cardcrawl.helpers;

public abstract interface HitboxListener
{
  public abstract void hoverStarted(Hitbox paramHitbox);
  
  public abstract void startClicking(Hitbox paramHitbox);
  
  public abstract void clicked(Hitbox paramHitbox);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\HitboxListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */