/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.beyond.Falling;
/*     */ import com.megacrit.cardcrawl.events.beyond.MindBloom;
/*     */ import com.megacrit.cardcrawl.events.beyond.MoaiHead;
/*     */ import com.megacrit.cardcrawl.events.beyond.MysteriousSphere;
/*     */ import com.megacrit.cardcrawl.events.beyond.SecretPortal;
/*     */ import com.megacrit.cardcrawl.events.beyond.SensoryStone;
/*     */ import com.megacrit.cardcrawl.events.beyond.TombRedMask;
/*     */ import com.megacrit.cardcrawl.events.beyond.WindingHalls;
/*     */ import com.megacrit.cardcrawl.events.city.Addict;
/*     */ import com.megacrit.cardcrawl.events.city.BackToBasics;
/*     */ import com.megacrit.cardcrawl.events.city.Beggar;
/*     */ import com.megacrit.cardcrawl.events.city.Colosseum;
/*     */ import com.megacrit.cardcrawl.events.city.CursedTome;
/*     */ import com.megacrit.cardcrawl.events.city.DrugDealer;
/*     */ import com.megacrit.cardcrawl.events.city.ForgottenAltar;
/*     */ import com.megacrit.cardcrawl.events.city.Ghosts;
/*     */ import com.megacrit.cardcrawl.events.city.KnowingSkull;
/*     */ import com.megacrit.cardcrawl.events.city.MaskedBandits;
/*     */ import com.megacrit.cardcrawl.events.city.Nest;
/*     */ import com.megacrit.cardcrawl.events.city.TheJoust;
/*     */ import com.megacrit.cardcrawl.events.city.TheLibrary;
/*     */ import com.megacrit.cardcrawl.events.city.TheMausoleum;
/*     */ import com.megacrit.cardcrawl.events.city.Vampires;
/*     */ import com.megacrit.cardcrawl.events.exordium.BigFish;
/*     */ import com.megacrit.cardcrawl.events.exordium.Cleric;
/*     */ import com.megacrit.cardcrawl.events.exordium.DeadAdventurer;
/*     */ import com.megacrit.cardcrawl.events.exordium.GoldenIdolEvent;
/*     */ import com.megacrit.cardcrawl.events.exordium.GoldenWing;
/*     */ import com.megacrit.cardcrawl.events.exordium.GoopPuddle;
/*     */ import com.megacrit.cardcrawl.events.exordium.LivingWall;
/*     */ import com.megacrit.cardcrawl.events.exordium.Mushrooms;
/*     */ import com.megacrit.cardcrawl.events.exordium.Nloth;
/*     */ import com.megacrit.cardcrawl.events.exordium.ScrapOoze;
/*     */ import com.megacrit.cardcrawl.events.exordium.ShiningLight;
/*     */ import com.megacrit.cardcrawl.events.exordium.Sssserpent;
/*     */ import com.megacrit.cardcrawl.events.shrines.AccursedBlacksmith;
/*     */ import com.megacrit.cardcrawl.events.shrines.Bonfire;
/*     */ import com.megacrit.cardcrawl.events.shrines.Designer;
/*     */ import com.megacrit.cardcrawl.events.shrines.Duplicator;
/*     */ import com.megacrit.cardcrawl.events.shrines.FaceTrader;
/*     */ import com.megacrit.cardcrawl.events.shrines.FountainOfCurseRemoval;
/*     */ import com.megacrit.cardcrawl.events.shrines.GoldShrine;
/*     */ import com.megacrit.cardcrawl.events.shrines.GremlinMatchGame;
/*     */ import com.megacrit.cardcrawl.events.shrines.GremlinWheelGame;
/*     */ import com.megacrit.cardcrawl.events.shrines.Lab;
/*     */ import com.megacrit.cardcrawl.events.shrines.NoteForYourself;
/*     */ import com.megacrit.cardcrawl.events.shrines.PurificationShrine;
/*     */ import com.megacrit.cardcrawl.events.shrines.Transmogrifier;
/*     */ import com.megacrit.cardcrawl.events.shrines.UpgradeShrine;
/*     */ import com.megacrit.cardcrawl.events.shrines.WeMeetAgain;
/*     */ import com.megacrit.cardcrawl.events.shrines.WomanInBlue;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.ShopRoom;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class EventHelper
/*     */ {
/*  69 */   private static final Logger logger = LogManager.getLogger(EventHelper.class.getName());
/*     */   
/*     */   private static final float BASE_ELITE_CHANCE = 0.02F;
/*     */   
/*     */   private static final float BASE_MONSTER_CHANCE = 0.1F;
/*     */   
/*     */   private static final float BASE_SHOP_CHANCE = 0.03F;
/*     */   
/*     */   private static final float BASE_TREASURE_CHANCE = 0.02F;
/*     */   
/*     */   private static final float RAMP_ELITE_CHANCE = 0.02F;
/*     */   
/*     */   private static final float RAMP_MONSTER_CHANCE = 0.1F;
/*     */   
/*     */   private static final float RAMP_SHOP_CHANCE = 0.03F;
/*     */   
/*     */   private static final float RAMP_TREASURE_CHANCE = 0.02F;
/*     */   private static final float RESET_ELITE_CHANCE = 0.0F;
/*     */   private static final float RESET_MONSTER_CHANCE = 0.1F;
/*     */   private static final float RESET_SHOP_CHANCE = 0.03F;
/*     */   private static final float RESET_TREASURE_CHANCE = 0.02F;
/*  90 */   private static float ELITE_CHANCE = 0.02F;
/*  91 */   private static float MONSTER_CHANCE = 0.1F;
/*  92 */   private static float SHOP_CHANCE = 0.03F;
/*  93 */   public static float TREASURE_CHANCE = 0.02F;
/*     */   
/*     */   public static enum RoomResult {
/*  96 */     EVENT,  ELITE,  TREASURE,  SHOP,  MONSTER;
/*     */     
/*     */     private RoomResult() {} }
/*     */   
/* 100 */   public static RoomResult roll() { float roll = AbstractDungeon.eventRng.random();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 109 */     logger.info("ROLL: " + roll);
/* 110 */     logger.info("ELIT: " + ELITE_CHANCE);
/* 111 */     logger.info("MNST: " + MONSTER_CHANCE);
/* 112 */     logger.info("SHOP: " + SHOP_CHANCE);
/* 113 */     logger.info("TRSR: " + TREASURE_CHANCE);
/*     */     
/* 115 */     RoomResult[] possibleResults = new RoomResult[100];
/* 116 */     Arrays.fill(possibleResults, RoomResult.EVENT);
/* 117 */     int eliteSize = (int)(ELITE_CHANCE * 100.0F);
/* 118 */     if (AbstractDungeon.getCurrMapNode().y < 6) {
/* 119 */       eliteSize = 0;
/*     */     }
/* 121 */     int monsterSize = (int)(MONSTER_CHANCE * 100.0F);
/* 122 */     int shopSize = (int)(SHOP_CHANCE * 100.0F);
/* 123 */     if ((AbstractDungeon.getCurrRoom() instanceof ShopRoom)) {
/* 124 */       shopSize = 0;
/*     */     }
/* 126 */     int treasureSize = (int)(TREASURE_CHANCE * 100.0F);
/*     */     
/*     */ 
/* 129 */     int fillIndex = 0;
/* 130 */     Arrays.fill(possibleResults, Math.min(99, fillIndex), Math.min(100, fillIndex + eliteSize), RoomResult.MONSTER);
/* 131 */     fillIndex += eliteSize;
/* 132 */     Arrays.fill(possibleResults, 
/*     */     
/* 134 */       Math.min(99, fillIndex), 
/* 135 */       Math.min(100, fillIndex + monsterSize), RoomResult.MONSTER);
/*     */     
/* 137 */     fillIndex += monsterSize;
/* 138 */     Arrays.fill(possibleResults, Math.min(99, fillIndex), Math.min(100, fillIndex + shopSize), RoomResult.SHOP);
/* 139 */     fillIndex += shopSize;
/* 140 */     Arrays.fill(possibleResults, 
/*     */     
/* 142 */       Math.min(99, fillIndex), 
/* 143 */       Math.min(100, fillIndex + treasureSize), RoomResult.TREASURE);
/*     */     
/*     */ 
/* 146 */     RoomResult choice = possibleResults[((int)(roll * 100.0F))];
/*     */     
/*     */ 
/* 149 */     if (choice == RoomResult.ELITE) {
/* 150 */       if (AbstractDungeon.player.hasRelic("Juzu Bracelet")) {
/* 151 */         AbstractDungeon.player.getRelic("Juzu Bracelet").flash();
/* 152 */         choice = RoomResult.EVENT;
/*     */       }
/* 154 */       ELITE_CHANCE = 0.0F;
/*     */     } else {
/* 156 */       ELITE_CHANCE += 0.02F;
/*     */     }
/*     */     
/* 159 */     if (choice == RoomResult.MONSTER) {
/* 160 */       if (AbstractDungeon.player.hasRelic("Juzu Bracelet")) {
/* 161 */         AbstractDungeon.player.getRelic("Juzu Bracelet").flash();
/* 162 */         choice = RoomResult.EVENT;
/*     */       }
/* 164 */       MONSTER_CHANCE = 0.1F;
/*     */     } else {
/* 166 */       MONSTER_CHANCE += 0.1F;
/*     */     }
/*     */     
/* 169 */     if (choice == RoomResult.SHOP) {
/* 170 */       SHOP_CHANCE = 0.03F;
/*     */     } else {
/* 172 */       SHOP_CHANCE += 0.03F;
/*     */     }
/*     */     
/* 175 */     if ((Settings.isEndless) && (AbstractDungeon.player.hasBlight("MimicInfestation"))) {
/* 176 */       if (choice == RoomResult.TREASURE) {
/* 177 */         if (AbstractDungeon.player.hasRelic("Juzu Bracelet")) {
/* 178 */           AbstractDungeon.player.getRelic("Juzu Bracelet").flash();
/* 179 */           choice = RoomResult.EVENT;
/*     */         } else {
/* 181 */           choice = RoomResult.ELITE;
/*     */         }
/* 183 */         TREASURE_CHANCE = 0.02F;
/*     */       }
/*     */     }
/* 186 */     else if (choice == RoomResult.TREASURE) {
/* 187 */       if (AbstractDungeon.player.hasRelic("Tiny Chest")) {
/* 188 */         AbstractDungeon.player.getRelic("Tiny Chest").flash();
/* 189 */         TREASURE_CHANCE = 0.120000005F;
/*     */       } else {
/* 191 */         TREASURE_CHANCE = 0.02F;
/*     */       }
/*     */     } else {
/* 194 */       TREASURE_CHANCE += 0.02F;
/*     */     }
/*     */     
/* 197 */     return choice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void resetProbabilities()
/*     */   {
/* 204 */     ELITE_CHANCE = 0.0F;
/* 205 */     MONSTER_CHANCE = 0.1F;
/* 206 */     SHOP_CHANCE = 0.03F;
/* 207 */     if (AbstractDungeon.player.hasRelic("Tiny Chest")) {
/* 208 */       TREASURE_CHANCE = 0.120000005F;
/*     */     } else {
/* 210 */       TREASURE_CHANCE = 0.02F;
/*     */     }
/*     */   }
/*     */   
/*     */   public static void setChances(ArrayList<Float> chances) {
/* 215 */     ELITE_CHANCE = ((Float)chances.get(0)).floatValue();
/* 216 */     MONSTER_CHANCE = ((Float)chances.get(1)).floatValue();
/* 217 */     SHOP_CHANCE = ((Float)chances.get(2)).floatValue();
/* 218 */     TREASURE_CHANCE = ((Float)chances.get(3)).floatValue();
/*     */   }
/*     */   
/*     */   public static ArrayList<Float> getChances() {
/* 222 */     ArrayList<Float> chances = new ArrayList();
/* 223 */     chances.add(Float.valueOf(ELITE_CHANCE));
/* 224 */     chances.add(Float.valueOf(MONSTER_CHANCE));
/* 225 */     chances.add(Float.valueOf(SHOP_CHANCE));
/* 226 */     chances.add(Float.valueOf(TREASURE_CHANCE));
/* 227 */     return chances;
/*     */   }
/*     */   
/*     */   public static AbstractEvent getEvent(String key)
/*     */   {
/* 232 */     if (Settings.isDev) {}
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 237 */     switch (key)
/*     */     {
/*     */     case "Accursed Blacksmith": 
/* 240 */       return new AccursedBlacksmith();
/*     */     case "Bonfire Elementals": 
/* 242 */       return new Bonfire();
/*     */     case "Fountain of Cleansing": 
/* 244 */       return new FountainOfCurseRemoval();
/*     */     case "Designer": 
/* 246 */       return new Designer();
/*     */     case "Duplicator": 
/* 248 */       return new Duplicator();
/*     */     case "Lab": 
/* 250 */       return new Lab();
/*     */     case "Match and Keep!": 
/* 252 */       return new GremlinMatchGame();
/*     */     case "Golden Shrine": 
/* 254 */       return new GoldShrine();
/*     */     case "Purifier": 
/* 256 */       return new PurificationShrine();
/*     */     case "Transmorgrifier": 
/* 258 */       return new Transmogrifier();
/*     */     case "Wheel of Change": 
/* 260 */       return new GremlinWheelGame();
/*     */     case "Upgrade Shrine": 
/* 262 */       return new UpgradeShrine();
/*     */     case "FaceTrader": 
/* 264 */       return new FaceTrader();
/*     */     case "NoteForYourself": 
/* 266 */       return new NoteForYourself();
/*     */     case "WeMeetAgain": 
/* 268 */       return new WeMeetAgain();
/*     */     case "The Woman in Blue": 
/* 270 */       return new WomanInBlue();
/*     */     
/*     */ 
/*     */     case "Big Fish": 
/* 274 */       return new BigFish();
/*     */     case "The Cleric": 
/* 276 */       return new Cleric();
/*     */     case "Dead Adventurer": 
/* 278 */       return new DeadAdventurer();
/*     */     case "Golden Wing": 
/* 280 */       return new GoldenWing();
/*     */     case "Golden Idol": 
/* 282 */       return new GoldenIdolEvent();
/*     */     case "World of Goop": 
/* 284 */       return new GoopPuddle();
/*     */     case "Forgotten Altar": 
/* 286 */       return new ForgottenAltar();
/*     */     case "Scrap Ooze": 
/* 288 */       return new ScrapOoze();
/*     */     case "Liars Game": 
/* 290 */       return new Sssserpent();
/*     */     case "Living Wall": 
/* 292 */       return new LivingWall();
/*     */     case "Mushrooms": 
/* 294 */       return new Mushrooms();
/*     */     case "N'loth": 
/* 296 */       return new Nloth();
/*     */     case "Shining Light": 
/* 298 */       return new ShiningLight();
/*     */     
/*     */ 
/*     */     case "Vampires": 
/* 302 */       return new Vampires();
/*     */     case "Ghosts": 
/* 304 */       return new Ghosts();
/*     */     case "Addict": 
/* 306 */       return new Addict();
/*     */     case "Back to Basics": 
/* 308 */       return new BackToBasics();
/*     */     case "Beggar": 
/* 310 */       return new Beggar();
/*     */     case "Cursed Tome": 
/* 312 */       return new CursedTome();
/*     */     case "Drug Dealer": 
/* 314 */       return new DrugDealer();
/*     */     case "Knowing Skull": 
/* 316 */       return new KnowingSkull();
/*     */     case "Masked Bandits": 
/* 318 */       return new MaskedBandits();
/*     */     case "Nest": 
/* 320 */       return new Nest();
/*     */     case "The Library": 
/* 322 */       return new TheLibrary();
/*     */     case "The Mausoleum": 
/* 324 */       return new TheMausoleum();
/*     */     case "The Joust": 
/* 326 */       return new TheJoust();
/*     */     case "Colosseum": 
/* 328 */       return new Colosseum();
/*     */     
/*     */ 
/*     */     case "Mysterious Sphere": 
/* 332 */       return new MysteriousSphere();
/*     */     case "SecretPortal": 
/* 334 */       return new SecretPortal();
/*     */     case "Tomb of Lord Red Mask": 
/* 336 */       return new TombRedMask();
/*     */     case "Falling": 
/* 338 */       return new Falling();
/*     */     case "Winding Halls": 
/* 340 */       return new WindingHalls();
/*     */     case "The Moai Head": 
/* 342 */       return new MoaiHead();
/*     */     case "SensoryStone": 
/* 344 */       return new SensoryStone();
/*     */     case "MindBloom": 
/* 346 */       return new MindBloom();
/*     */     }
/* 348 */     logger.info("---------------------------\nERROR: Unspecified key: " + key + " in EventHelper.\n---------------------------");
/*     */     
/*     */ 
/* 351 */     return null;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\EventHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */