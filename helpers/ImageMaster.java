/*      */ package com.megacrit.cardcrawl.helpers;
/*      */ 
/*      */ import com.badlogic.gdx.Files;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.Texture.TextureFilter;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper.ControllerModel;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class ImageMaster
/*      */ {
/*   14 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ImageMaster.class.getName());
/*      */   
/*      */   public static TextureAtlas vfxAtlas;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_1;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_2;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_3;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_4;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_5;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DECK_GLOW_6;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_1;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_2;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_3;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_4;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_5;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DUST_6;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_1;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_2;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_3;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_4;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_5;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion DEATH_VFX_6;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion SCENE_TRANSITION_FADER;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion SMOKE_1;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion SMOKE_2;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion SMOKE_3;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion CONE_1;
/*      */   
/*      */   public static TextureAtlas.AtlasRegion CONE_2;
/*      */   public static TextureAtlas.AtlasRegion CONE_3;
/*      */   public static TextureAtlas.AtlasRegion CONE_4;
/*      */   public static TextureAtlas.AtlasRegion CONE_5;
/*      */   public static TextureAtlas.AtlasRegion EXHAUST_L;
/*      */   public static TextureAtlas.AtlasRegion EXHAUST_S;
/*      */   public static TextureAtlas.AtlasRegion ATK_BLUNT_HEAVY;
/*      */   public static TextureAtlas.AtlasRegion ATK_BLUNT_LIGHT;
/*      */   public static TextureAtlas.AtlasRegion ATK_FIRE;
/*      */   public static TextureAtlas.AtlasRegion ATK_POISON;
/*      */   public static TextureAtlas.AtlasRegion ATK_SHIELD;
/*      */   public static TextureAtlas.AtlasRegion ATK_SLASH_HEAVY;
/*      */   public static TextureAtlas.AtlasRegion ATK_SLASH_H;
/*      */   public static TextureAtlas.AtlasRegion ATK_SLASH_V;
/*      */   public static TextureAtlas.AtlasRegion ATK_SLASH_D;
/*      */   public static TextureAtlas.AtlasRegion ATK_SLASH_RED;
/*      */   public static TextureAtlas.AtlasRegion ROOM_SHINE_1;
/*      */   public static TextureAtlas.AtlasRegion ROOM_SHINE_2;
/*      */   public static TextureAtlas.AtlasRegion COPPER_COIN_1;
/*      */   public static TextureAtlas.AtlasRegion COPPER_COIN_2;
/*      */   public static TextureAtlas.AtlasRegion GRAB_COIN;
/*      */   public static TextureAtlas.AtlasRegion FLAME_1;
/*      */   public static TextureAtlas.AtlasRegion FLAME_2;
/*      */   public static TextureAtlas.AtlasRegion FLAME_3;
/*      */   public static TextureAtlas.AtlasRegion STRIKE_LINE;
/*      */   public static TextureAtlas.AtlasRegion STRIKE_LINE_2;
/*      */   public static TextureAtlas.AtlasRegion STRIKE_BLUR;
/*      */   public static TextureAtlas.AtlasRegion GLOW_SPARK;
/*      */   public static TextureAtlas.AtlasRegion GLOW_SPARK_2;
/*      */   public static TextureAtlas.AtlasRegion THICK_3D_LINE;
/*      */   public static TextureAtlas.AtlasRegion THICK_3D_LINE_2;
/*      */   public static TextureAtlas.AtlasRegion MOVE_NAME_BG;
/*      */   public static TextureAtlas.AtlasRegion VERTICAL_AURA;
/*      */   public static TextureAtlas.AtlasRegion POWER_UP_1;
/*      */   public static TextureAtlas.AtlasRegion POWER_UP_2;
/*      */   public static TextureAtlas.AtlasRegion WOBBLY_LINE;
/*      */   public static TextureAtlas.AtlasRegion BLUR_WAVE;
/*      */   public static TextureAtlas.AtlasRegion DAGGER_STREAK;
/*      */   public static TextureAtlas.AtlasRegion WHITE_RING;
/*      */   public static TextureAtlas.AtlasRegion VERTICAL_IMPACT;
/*      */   public static TextureAtlas.AtlasRegion BORDER_GLOW;
/*      */   public static TextureAtlas.AtlasRegion BORDER_GLOW_2;
/*      */   public static TextureAtlas.AtlasRegion UPGRADE_HAMMER_IMPACT;
/*      */   public static TextureAtlas.AtlasRegion UPGRADE_HAMMER_LINE;
/*      */   public static TextureAtlas.AtlasRegion CRYSTAL_IMPACT;
/*      */   public static TextureAtlas.AtlasRegion TORCH_FIRE_1;
/*      */   public static TextureAtlas.AtlasRegion TORCH_FIRE_2;
/*      */   public static TextureAtlas.AtlasRegion TORCH_FIRE_3;
/*      */   public static TextureAtlas.AtlasRegion TINY_STAR;
/*      */   public static TextureAtlas cardOrbAtlas;
/*      */   public static TextureAtlas.AtlasRegion RED_ORB;
/*      */   public static TextureAtlas.AtlasRegion GREEN_ORB;
/*      */   public static TextureAtlas.AtlasRegion BLUE_ORB;
/*      */   public static Texture PROFILE_A;
/*      */   public static Texture PROFILE_B;
/*      */   public static Texture PROFILE_C;
/*      */   public static Texture PROFILE_D;
/*      */   public static Texture PROFILE_GLOW;
/*      */   public static Texture PROFILE_SLOT;
/*      */   public static Texture PROFILE_DELETE;
/*      */   public static Texture PROFILE_RENAME;
/*      */   public static Texture CARD_ATTACK_BG_RED;
/*      */   public static Texture CARD_ATTACK_BG_GREEN;
/*      */   public static Texture CARD_ATTACK_BG_BLUE;
/*      */   public static Texture CARD_ATTACK_BG_GRAY;
/*      */   public static Texture CARD_ATTACK_BG_SILHOUETTE;
/*      */   public static Texture CARD_SKILL_BG_RED;
/*      */   public static Texture CARD_SKILL_BG_GREEN;
/*      */   public static Texture CARD_SKILL_BG_BLUE;
/*      */   public static Texture CARD_SKILL_BG_BLACK;
/*      */   public static Texture CARD_SKILL_BG_GRAY;
/*      */   public static Texture CARD_SKILL_BG_SILHOUETTE;
/*      */   public static Texture CARD_POWER_BG_RED;
/*      */   public static Texture CARD_POWER_BG_GREEN;
/*      */   public static Texture CARD_POWER_BG_BLUE;
/*      */   public static Texture CARD_POWER_BG_GRAY;
/*      */   public static Texture CARD_POWER_BG_SILHOUETTE;
/*      */   public static Texture CARD_FRAME_ATTACK_COMMON;
/*      */   public static Texture CARD_FRAME_ATTACK_UNCOMMON;
/*      */   public static Texture CARD_FRAME_ATTACK_RARE;
/*      */   public static Texture CARD_FRAME_SKILL_COMMON;
/*      */   public static Texture CARD_FRAME_SKILL_UNCOMMON;
/*      */   public static Texture CARD_FRAME_SKILL_RARE;
/*      */   public static Texture CARD_FRAME_POWER_COMMON;
/*      */   public static Texture CARD_FRAME_POWER_UNCOMMON;
/*      */   public static Texture CARD_FRAME_POWER_RARE;
/*      */   public static Texture CARD_BANNER_COMMON;
/*      */   public static Texture CARD_BANNER_UNCOMMON;
/*      */   public static Texture CARD_BANNER_RARE;
/*      */   public static Texture CARD_SUPER_SHADOW;
/*      */   public static Texture CARD_BACK;
/*      */   public static Texture CARD_BG;
/*      */   public static Texture CARD_RED_ORB;
/*      */   public static Texture CARD_GREEN_ORB;
/*      */   public static Texture CARD_BLUE_ORB;
/*      */   public static Texture CARD_COLORLESS_ORB;
/*      */   public static Texture CARD_LOCKED_ATTACK;
/*      */   public static Texture CARD_LOCKED_SKILL;
/*      */   public static Texture CARD_LOCKED_POWER;
/*      */   public static Texture CARD_FLASH_VFX;
/*      */   public static Texture CARD_ATTACK_BG_RED_L;
/*      */   public static Texture CARD_ATTACK_BG_GREEN_L;
/*      */   public static Texture CARD_ATTACK_BG_BLUE_L;
/*      */   public static Texture CARD_ATTACK_BG_GRAY_L;
/*      */   public static Texture CARD_SKILL_BG_RED_L;
/*      */   public static Texture CARD_SKILL_BG_GREEN_L;
/*      */   public static Texture CARD_SKILL_BG_BLUE_L;
/*      */   public static Texture CARD_SKILL_BG_GRAY_L;
/*      */   public static Texture CARD_SKILL_BG_BLACK_L;
/*      */   public static Texture CARD_POWER_BG_RED_L;
/*      */   public static Texture CARD_POWER_BG_GREEN_L;
/*      */   public static Texture CARD_POWER_BG_BLUE_L;
/*      */   public static Texture CARD_POWER_BG_GRAY_L;
/*      */   public static Texture CARD_FRAME_ATTACK_COMMON_L;
/*      */   public static Texture CARD_FRAME_ATTACK_UNCOMMON_L;
/*      */   public static Texture CARD_FRAME_ATTACK_RARE_L;
/*      */   public static Texture CARD_FRAME_SKILL_COMMON_L;
/*      */   public static Texture CARD_FRAME_SKILL_UNCOMMON_L;
/*      */   public static Texture CARD_FRAME_SKILL_RARE_L;
/*      */   public static Texture CARD_FRAME_POWER_COMMON_L;
/*      */   public static Texture CARD_FRAME_POWER_UNCOMMON_L;
/*      */   public static Texture CARD_FRAME_POWER_RARE_L;
/*      */   public static Texture CARD_BANNER_RARE_L;
/*      */   public static Texture CARD_BANNER_UNCOMMON_L;
/*      */   public static Texture CARD_BANNER_COMMON_L;
/*      */   public static Texture CARD_GREEN_ORB_L;
/*      */   public static Texture CARD_RED_ORB_L;
/*      */   public static Texture CARD_BLUE_ORB_L;
/*      */   public static Texture CARD_GRAY_ORB_L;
/*      */   public static Texture CARD_LOCKED_ATTACK_L;
/*      */   public static Texture CARD_LOCKED_SKILL_L;
/*      */   public static Texture CARD_LOCKED_POWER_L;
/*      */   public static Texture CONTROLLER_A;
/*      */   public static Texture CONTROLLER_B;
/*      */   public static Texture CONTROLLER_X;
/*      */   public static Texture CONTROLLER_Y;
/*      */   public static Texture CONTROLLER_BACK;
/*      */   public static Texture CONTROLLER_START;
/*      */   public static Texture CONTROLLER_LB;
/*      */   public static Texture CONTROLLER_RB;
/*      */   public static Texture CONTROLLER_LS_UP;
/*      */   public static Texture CONTROLLER_LS_DOWN;
/*      */   public static Texture CONTROLLER_LS_LEFT;
/*      */   public static Texture CONTROLLER_LS_RIGHT;
/*      */   public static Texture CONTROLLER_D_UP;
/*      */   public static Texture CONTROLLER_D_DOWN;
/*      */   public static Texture CONTROLLER_D_LEFT;
/*      */   public static Texture CONTROLLER_D_RIGHT;
/*      */   public static Texture CONTROLLER_LT;
/*      */   public static Texture CONTROLLER_RT;
/*      */   public static Texture CONTROLLER_HB_HIGHLIGHT;
/*      */   public static Texture TIMER_ICON;
/*      */   public static Texture TOP_PANEL_BAR;
/*      */   public static Texture DECK_COUNT_CIRCLE;
/*      */   public static Texture HEALTH_BAR_B;
/*      */   public static Texture HEALTH_BAR_L;
/*      */   public static Texture HEALTH_BAR_R;
/*      */   public static Texture BLOCK_BAR_B;
/*      */   public static Texture BLOCK_BAR_R;
/*      */   public static Texture BLOCK_BAR_L;
/*      */   public static Texture BLOCK_ICON;
/*      */   public static Texture BLOCK_ICON_L;
/*      */   public static Texture BLOCK_ICON_R;
/*      */   public static Texture HB_SHADOW_L;
/*      */   public static Texture HB_SHADOW_B;
/*      */   public static Texture HB_SHADOW_R;
/*      */   public static Texture DRAW_PILE_BANNER;
/*      */   public static Texture DISCARD_PILE_BANNER;
/*      */   public static Texture DECK_BTN_BASE;
/*      */   public static Texture DISCARD_BTN_BASE;
/*      */   public static Texture CAMPFIRE_FIRE;
/*      */   public static Texture CAMPFIRE_REST_BUTTON;
/*      */   public static Texture CAMPFIRE_MEDITATE_BUTTON;
/*      */   public static Texture CAMPFIRE_SMITH_BUTTON;
/*      */   public static Texture CAMPFIRE_SMITH_DISABLE_BUTTON;
/*      */   public static Texture CAMPFIRE_RITUAL_BUTTON;
/*      */   public static Texture CAMPFIRE_TOKE_BUTTON;
/*      */   public static Texture CAMPFIRE_TOKE_DISABLE_BUTTON;
/*      */   public static Texture CAMPFIRE_TRAIN_BUTTON;
/*      */   public static Texture CAMPFIRE_DIG_BUTTON;
/*      */   public static Texture CAMPFIRE_BREW_BUTTON;
/*      */   public static Texture CAMPFIRE_HOVER_BUTTON;
/*      */   public static Texture CAMPFIRE_BUTTON_SHADOW;
/*      */   public static Texture CF_IC_BODY;
/*      */   public static Texture CF_IC_BODY_G;
/*      */   public static Texture CF_IC_SHADOW;
/*      */   public static Texture CF_S_BODY;
/*      */   public static Texture CF_S_BODY_G;
/*      */   public static Texture CF_S_SHADOW;
/*      */   public static Texture CF_LEFT_ARROW;
/*      */   public static Texture CF_RIGHT_ARROW;
/*      */   public static Texture UPGRADE_ARROW;
/*      */   public static Texture INTENT_ATK_1;
/*      */   public static Texture INTENT_ATK_2;
/*      */   public static Texture INTENT_ATK_3;
/*      */   public static Texture INTENT_ATK_4;
/*      */   public static Texture INTENT_ATK_5;
/*      */   public static Texture INTENT_ATK_6;
/*      */   public static Texture INTENT_ATK_7;
/*      */   public static Texture INTENT_ATK_TIP_1;
/*      */   public static Texture INTENT_ATK_TIP_2;
/*      */   public static Texture INTENT_ATK_TIP_3;
/*      */   public static Texture INTENT_ATK_TIP_4;
/*      */   public static Texture INTENT_ATK_TIP_5;
/*      */   public static Texture INTENT_ATK_TIP_6;
/*      */   public static Texture INTENT_ATK_TIP_7;
/*      */   public static Texture INTENT_BUFF;
/*      */   public static Texture INTENT_BUFF_L;
/*      */   public static Texture INTENT_DEBUFF;
/*      */   public static Texture INTENT_DEBUFF_L;
/*      */   public static Texture INTENT_DEBUFF2;
/*      */   public static Texture INTENT_DEBUFF2_L;
/*      */   public static Texture INTENT_DEFEND;
/*      */   public static Texture INTENT_DEFEND_L;
/*      */   public static Texture INTENT_DEFEND_BUFF;
/*      */   public static Texture INTENT_DEFEND_BUFF_L;
/*      */   public static Texture INTENT_ESCAPE;
/*      */   public static Texture INTENT_ESCAPE_L;
/*      */   public static Texture INTENT_MAGIC;
/*      */   public static Texture INTENT_MAGIC_L;
/*      */   public static Texture INTENT_SLEEP;
/*      */   public static Texture INTENT_SLEEP_L;
/*      */   public static Texture INTENT_STUN;
/*      */   public static Texture INTENT_STUN_L;
/*      */   public static Texture INTENT_UNKNOWN;
/*      */   public static Texture INTENT_UNKNOWN_L;
/*      */   public static Texture INTENT_ATTACK_BUFF;
/*      */   public static Texture INTENT_ATTACK_DEBUFF;
/*      */   public static Texture INTENT_ATTACK_DEFEND;
/*      */   public static Texture CHECKBOX;
/*      */   public static Texture TICK;
/*      */   public static Texture ORB_DARK;
/*      */   public static Texture ORB_LIGHTNING;
/*      */   public static Texture ORB_PAPER;
/*      */   public static Texture ORB_PLASMA;
/*      */   public static Texture COLOR_TAB_RED;
/*      */   public static Texture COLOR_TAB_GREEN;
/*      */   public static Texture COLOR_TAB_BLUE;
/*      */   public static Texture COLOR_TAB_COLORLESS;
/*      */   public static Texture COLOR_TAB_CURSE;
/*      */   public static Texture COLOR_TAB_BAR;
/*      */   public static Texture COLOR_TAB_LOCK;
/*      */   public static Texture COLOR_TAB_BOX_UNTICKED;
/*      */   public static Texture COLOR_TAB_BOX_TICKED;
/*      */   public static Texture FILTER_IRONCLAD;
/*      */   public static Texture FILTER_SILENT;
/*      */   public static Texture FILTER_DEFECT;
/*      */   public static Texture P_STANDARD;
/*      */   public static Texture P_DAILY;
/*      */   public static Texture P_LOOP;
/*      */   public static Texture P_INFO_CARD;
/*      */   
/*      */   public static void initialize()
/*      */   {
/*  318 */     long startTime = System.currentTimeMillis();
/*      */     
/*      */ 
/*  321 */     vfxAtlas = new TextureAtlas(Gdx.files.internal("vfx/vfx.atlas"));
/*  322 */     DECK_GLOW_1 = vfxAtlas.findRegion("ui/p1");
/*  323 */     DECK_GLOW_2 = vfxAtlas.findRegion("ui/p2");
/*  324 */     DECK_GLOW_3 = vfxAtlas.findRegion("ui/p3");
/*  325 */     DECK_GLOW_4 = vfxAtlas.findRegion("ui/p4");
/*  326 */     DECK_GLOW_5 = vfxAtlas.findRegion("ui/p5");
/*  327 */     DECK_GLOW_6 = vfxAtlas.findRegion("ui/p6");
/*  328 */     SCENE_TRANSITION_FADER = vfxAtlas.findRegion("ui/topDownFader");
/*  329 */     DEATH_VFX_1 = vfxAtlas.findRegion("env/death1");
/*  330 */     DEATH_VFX_2 = vfxAtlas.findRegion("env/death2");
/*  331 */     DEATH_VFX_3 = vfxAtlas.findRegion("env/death3");
/*  332 */     DEATH_VFX_4 = vfxAtlas.findRegion("env/death4");
/*  333 */     DEATH_VFX_5 = vfxAtlas.findRegion("env/death5");
/*  334 */     DEATH_VFX_6 = vfxAtlas.findRegion("env/death6");
/*  335 */     DUST_1 = vfxAtlas.findRegion("env/dust1");
/*  336 */     DUST_2 = vfxAtlas.findRegion("env/dust2");
/*  337 */     DUST_3 = vfxAtlas.findRegion("env/dust3");
/*  338 */     DUST_4 = vfxAtlas.findRegion("env/dust4");
/*  339 */     DUST_5 = vfxAtlas.findRegion("env/dust5");
/*  340 */     DUST_6 = vfxAtlas.findRegion("env/dust6");
/*  341 */     SMOKE_1 = vfxAtlas.findRegion("env/smoke1");
/*  342 */     SMOKE_2 = vfxAtlas.findRegion("env/smoke2");
/*  343 */     SMOKE_3 = vfxAtlas.findRegion("env/smoke3");
/*  344 */     CONE_1 = vfxAtlas.findRegion("cone7");
/*  345 */     CONE_2 = vfxAtlas.findRegion("cone8");
/*  346 */     CONE_3 = vfxAtlas.findRegion("cone9");
/*  347 */     CONE_4 = vfxAtlas.findRegion("cone5");
/*  348 */     CONE_5 = vfxAtlas.findRegion("cone6");
/*  349 */     EXHAUST_L = vfxAtlas.findRegion("exhaust/bigBlur");
/*  350 */     EXHAUST_S = vfxAtlas.findRegion("exhaust/smallBlur");
/*  351 */     ATK_BLUNT_HEAVY = vfxAtlas.findRegion("attack/blunt_heavy");
/*  352 */     ATK_BLUNT_LIGHT = vfxAtlas.findRegion("attack/blunt_light");
/*  353 */     ATK_FIRE = vfxAtlas.findRegion("attack/fire");
/*  354 */     ATK_POISON = vfxAtlas.findRegion("attack/poison");
/*  355 */     ATK_SHIELD = vfxAtlas.findRegion("attack/shield");
/*  356 */     ATK_SLASH_HEAVY = vfxAtlas.findRegion("attack/slash_heavy");
/*  357 */     ATK_SLASH_H = vfxAtlas.findRegion("attack/slash_horizontal");
/*  358 */     ATK_SLASH_V = vfxAtlas.findRegion("attack/slash_vertical");
/*  359 */     ATK_SLASH_D = vfxAtlas.findRegion("attack/slash_light");
/*  360 */     ATK_SLASH_RED = vfxAtlas.findRegion("attack/slash1");
/*  361 */     ROOM_SHINE_1 = vfxAtlas.findRegion("shine1");
/*  362 */     ROOM_SHINE_2 = vfxAtlas.findRegion("shine2");
/*  363 */     COPPER_COIN_1 = vfxAtlas.findRegion("copperCoin");
/*  364 */     COPPER_COIN_2 = vfxAtlas.findRegion("copperCoin2");
/*  365 */     GRAB_COIN = vfxAtlas.findRegion("shineLines");
/*  366 */     FLAME_1 = vfxAtlas.findRegion("combat/flame4");
/*  367 */     FLAME_2 = vfxAtlas.findRegion("combat/flame5");
/*  368 */     FLAME_3 = vfxAtlas.findRegion("combat/flame6");
/*  369 */     STRIKE_LINE = vfxAtlas.findRegion("combat/strikeLine2");
/*  370 */     STRIKE_LINE_2 = vfxAtlas.findRegion("combat/strikeLine3");
/*  371 */     STRIKE_BLUR = vfxAtlas.findRegion("combat/blurDot");
/*  372 */     GLOW_SPARK = vfxAtlas.findRegion("glowSpark");
/*  373 */     GLOW_SPARK_2 = vfxAtlas.findRegion("glowSpark2");
/*  374 */     THICK_3D_LINE = vfxAtlas.findRegion("combat/spike");
/*  375 */     THICK_3D_LINE_2 = vfxAtlas.findRegion("combat/spike2");
/*  376 */     MOVE_NAME_BG = vfxAtlas.findRegion("combat/moveNameBg");
/*  377 */     VERTICAL_AURA = vfxAtlas.findRegion("combat/verticalAura");
/*  378 */     POWER_UP_1 = vfxAtlas.findRegion("combat/empowerCircle1");
/*  379 */     POWER_UP_2 = vfxAtlas.findRegion("combat/empowerCircle2");
/*  380 */     WOBBLY_LINE = vfxAtlas.findRegion("combat/wobblyLine");
/*  381 */     BLUR_WAVE = vfxAtlas.findRegion("combat/blurWave");
/*  382 */     DAGGER_STREAK = vfxAtlas.findRegion("combat/streak");
/*  383 */     WHITE_RING = vfxAtlas.findRegion("whiteRing");
/*  384 */     VERTICAL_IMPACT = vfxAtlas.findRegion("combat/verticalImpact");
/*  385 */     BORDER_GLOW = vfxAtlas.findRegion("borderGlow");
/*  386 */     BORDER_GLOW_2 = vfxAtlas.findRegion("borderGlow2");
/*  387 */     UPGRADE_HAMMER_IMPACT = vfxAtlas.findRegion("ui/hammerImprint");
/*  388 */     UPGRADE_HAMMER_LINE = vfxAtlas.findRegion("ui/impactLineThick");
/*  389 */     CRYSTAL_IMPACT = vfxAtlas.findRegion("blurRing");
/*  390 */     TORCH_FIRE_1 = vfxAtlas.findRegion("env/fire1");
/*  391 */     TORCH_FIRE_2 = vfxAtlas.findRegion("env/fire2");
/*  392 */     TORCH_FIRE_3 = vfxAtlas.findRegion("env/fire3");
/*  393 */     TINY_STAR = vfxAtlas.findRegion("combat/tinyStar2");
/*      */     
/*      */ 
/*  396 */     cardOrbAtlas = new TextureAtlas(Gdx.files.internal("orbs/orb.atlas"));
/*  397 */     RED_ORB = cardOrbAtlas.findRegion("red");
/*  398 */     GREEN_ORB = cardOrbAtlas.findRegion("green");
/*  399 */     BLUE_ORB = cardOrbAtlas.findRegion("blue");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  404 */     PROFILE_A = loadImage("images/ui/profile/1.png");
/*  405 */     PROFILE_B = loadImage("images/ui/profile/2.png");
/*  406 */     PROFILE_C = loadImage("images/ui/profile/3.png");
/*  407 */     PROFILE_SLOT = loadImage("images/ui/profile/save_panel.png");
/*  408 */     PROFILE_DELETE = loadImage("images/ui/profile/delete_button.png");
/*  409 */     PROFILE_RENAME = loadImage("images/ui/profile/rename_button.png");
/*  410 */     CARD_BACK = loadImage("images/cardui/512/card_back.png");
/*  411 */     CARD_BG = loadImage("images/cardui/512/card_bg.png");
/*  412 */     CARD_ATTACK_BG_RED = loadImage("images/cardui/512/bg_attack_red.png");
/*  413 */     CARD_ATTACK_BG_GREEN = loadImage("images/cardui/512/bg_attack_green.png");
/*  414 */     CARD_ATTACK_BG_BLUE = loadImage("images/cardui/512/bg_attack_blue.png");
/*  415 */     CARD_ATTACK_BG_GRAY = loadImage("images/cardui/512/bg_attack_gray.png");
/*  416 */     CARD_ATTACK_BG_SILHOUETTE = loadImage("images/cardui/512/bg_attack_silhouette.png");
/*  417 */     CARD_SKILL_BG_RED = loadImage("images/cardui/512/bg_skill_red.png");
/*  418 */     CARD_SKILL_BG_GREEN = loadImage("images/cardui/512/bg_skill_green.png");
/*  419 */     CARD_SKILL_BG_BLUE = loadImage("images/cardui/512/bg_skill_blue.png");
/*  420 */     CARD_SKILL_BG_BLACK = loadImage("images/cardui/512/bg_skill_black.png");
/*  421 */     CARD_SKILL_BG_GRAY = loadImage("images/cardui/512/bg_skill_gray.png");
/*  422 */     CARD_SKILL_BG_SILHOUETTE = loadImage("images/cardui/512/bg_skill_silhouette.png");
/*  423 */     CARD_POWER_BG_RED = loadImage("images/cardui/512/bg_power_red.png");
/*  424 */     CARD_POWER_BG_GREEN = loadImage("images/cardui/512/bg_power_green.png");
/*  425 */     CARD_POWER_BG_BLUE = loadImage("images/cardui/512/bg_power_blue.png");
/*  426 */     CARD_POWER_BG_GRAY = loadImage("images/cardui/512/bg_power_gray.png");
/*  427 */     CARD_POWER_BG_SILHOUETTE = loadImage("images/cardui/512/bg_power_silhouette.png");
/*  428 */     CARD_FRAME_ATTACK_COMMON = loadImage("images/cardui/512/frame_attack_common.png");
/*  429 */     CARD_FRAME_ATTACK_UNCOMMON = loadImage("images/cardui/512/frame_attack_uncommon.png");
/*  430 */     CARD_FRAME_ATTACK_RARE = loadImage("images/cardui/512/frame_attack_rare.png");
/*  431 */     CARD_FRAME_SKILL_COMMON = loadImage("images/cardui/512/frame_skill_common.png");
/*  432 */     CARD_FRAME_SKILL_UNCOMMON = loadImage("images/cardui/512/frame_skill_uncommon.png");
/*  433 */     CARD_FRAME_SKILL_RARE = loadImage("images/cardui/512/frame_skill_rare.png");
/*  434 */     CARD_FRAME_POWER_COMMON = loadImage("images/cardui/512/frame_power_common.png");
/*  435 */     CARD_FRAME_POWER_UNCOMMON = loadImage("images/cardui/512/frame_power_uncommon.png");
/*  436 */     CARD_FRAME_POWER_RARE = loadImage("images/cardui/512/frame_power_rare.png");
/*  437 */     CARD_BANNER_COMMON = loadImage("images/cardui/512/banner_common.png");
/*  438 */     CARD_BANNER_UNCOMMON = loadImage("images/cardui/512/banner_uncommon.png");
/*  439 */     CARD_BANNER_RARE = loadImage("images/cardui/512/banner_rare.png");
/*  440 */     CARD_RED_ORB = loadImage("images/cardui/512/card_red_orb.png");
/*  441 */     CARD_GREEN_ORB = loadImage("images/cardui/512/card_green_orb.png");
/*  442 */     CARD_BLUE_ORB = loadImage("images/cardui/512/card_blue_orb.png");
/*  443 */     CARD_COLORLESS_ORB = loadImage("images/cardui/512/card_colorless_orb.png");
/*  444 */     CARD_SUPER_SHADOW = loadImage("images/cardui/512/card_super_shadow.png");
/*  445 */     CARD_LOCKED_ATTACK = loadImage("images/cards/locked_attack.png");
/*  446 */     CARD_LOCKED_SKILL = loadImage("images/cards/locked_skill.png");
/*  447 */     CARD_LOCKED_POWER = loadImage("images/cards/locked_power.png");
/*  448 */     CARD_FLASH_VFX = loadImage("images/cardui/512/card_flash_vfx.png");
/*      */     
/*      */ 
/*  451 */     CARD_ATTACK_BG_RED_L = loadImage("images/cardui/1024/bg_attack_red.png");
/*  452 */     CARD_ATTACK_BG_GREEN_L = loadImage("images/cardui/1024/bg_attack_green.png");
/*  453 */     CARD_ATTACK_BG_BLUE_L = loadImage("images/cardui/1024/bg_attack_blue.png");
/*  454 */     CARD_ATTACK_BG_GRAY_L = loadImage("images/cardui/1024/bg_attack_colorless.png");
/*  455 */     CARD_SKILL_BG_RED_L = loadImage("images/cardui/1024/bg_skill_red.png");
/*  456 */     CARD_SKILL_BG_GREEN_L = loadImage("images/cardui/1024/bg_skill_green.png");
/*  457 */     CARD_SKILL_BG_BLUE_L = loadImage("images/cardui/1024/bg_skill_blue.png");
/*  458 */     CARD_SKILL_BG_GRAY_L = loadImage("images/cardui/1024/bg_skill_colorless.png");
/*  459 */     CARD_SKILL_BG_BLACK_L = loadImage("images/cardui/1024/bg_skill_black.png");
/*  460 */     CARD_POWER_BG_RED_L = loadImage("images/cardui/1024/bg_power_red.png");
/*  461 */     CARD_POWER_BG_GREEN_L = loadImage("images/cardui/1024/bg_power_green.png");
/*  462 */     CARD_POWER_BG_BLUE_L = loadImage("images/cardui/1024/bg_power_blue.png");
/*  463 */     CARD_POWER_BG_GRAY_L = loadImage("images/cardui/1024/bg_power_colorless.png");
/*  464 */     CARD_FRAME_ATTACK_COMMON_L = loadImage("images/cardui/1024/frame_attack_common.png");
/*  465 */     CARD_FRAME_ATTACK_UNCOMMON_L = loadImage("images/cardui/1024/frame_attack_uncommon.png");
/*  466 */     CARD_FRAME_ATTACK_RARE_L = loadImage("images/cardui/1024/frame_attack_rare.png");
/*  467 */     CARD_FRAME_SKILL_COMMON_L = loadImage("images/cardui/1024/frame_skill_common.png");
/*  468 */     CARD_FRAME_SKILL_UNCOMMON_L = loadImage("images/cardui/1024/frame_skill_uncommon.png");
/*  469 */     CARD_FRAME_SKILL_RARE_L = loadImage("images/cardui/1024/frame_skill_rare.png");
/*  470 */     CARD_FRAME_POWER_COMMON_L = loadImage("images/cardui/1024/frame_power_common.png");
/*  471 */     CARD_FRAME_POWER_UNCOMMON_L = loadImage("images/cardui/1024/frame_power_uncommon.png");
/*  472 */     CARD_FRAME_POWER_RARE_L = loadImage("images/cardui/1024/frame_power_rare.png");
/*  473 */     CARD_BANNER_COMMON_L = loadImage("images/cardui/1024/banner_common.png");
/*  474 */     CARD_BANNER_UNCOMMON_L = loadImage("images/cardui/1024/banner_uncommon.png");
/*  475 */     CARD_BANNER_RARE_L = loadImage("images/cardui/1024/banner_rare.png");
/*  476 */     CARD_RED_ORB_L = loadImage("images/cardui/1024/card_red_orb.png");
/*  477 */     CARD_GREEN_ORB_L = loadImage("images/cardui/1024/card_green_orb.png");
/*  478 */     CARD_BLUE_ORB_L = loadImage("images/cardui/1024/card_blue_orb.png");
/*  479 */     CARD_GRAY_ORB_L = loadImage("images/cardui/1024/card_colorless_orb.png");
/*  480 */     CARD_LOCKED_ATTACK_L = loadImage("images/cards/locked_attack_l.png");
/*  481 */     CARD_LOCKED_SKILL_L = loadImage("images/cards/locked_skill_l.png");
/*  482 */     CARD_LOCKED_POWER_L = loadImage("images/cards/locked_power_l.png");
/*      */     
/*  484 */     MERCHANT_RUG_IMG = loadImage("images/npcs/merchantObjects.png");
/*  485 */     MERCHANT_GLOW_IMG = loadImage("images/npcs/merchantOutline.png");
/*      */     
/*      */ 
/*  488 */     POTION_UI_SHADOW = loadImage("images/ui/potionPopUp/shadow.png");
/*  489 */     POTION_UI_BG = loadImage("images/ui/potionPopUp/bg.png");
/*  490 */     POTION_UI_OVERLAY = loadImage("images/ui/potionPopUp/overlay.png");
/*  491 */     POTION_UI_TOP = loadImage("images/ui/potionPopUp/top.png");
/*  492 */     POTION_UI_MID = loadImage("images/ui/potionPopUp/mid.png");
/*  493 */     POTION_UI_BOT = loadImage("images/ui/potionPopUp/bot.png");
/*      */     
/*      */ 
/*  496 */     POTION_T_CONTAINER = loadImage("images/potion/potion_t_glass.png");
/*  497 */     POTION_T_LIQUID = loadImage("images/potion/potion_t_liquid.png");
/*  498 */     POTION_T_HYBRID = loadImage("images/potion/potion_t_hybrid.png");
/*  499 */     POTION_T_SPOTS = loadImage("images/potion/potion_t_spots.png");
/*  500 */     POTION_T_OUTLINE = loadImage("images/potion/potion_t_outline.png");
/*  501 */     POTION_S_CONTAINER = loadImage("images/potion/potion_s_glass.png");
/*  502 */     POTION_S_LIQUID = loadImage("images/potion/potion_s_liquid.png");
/*  503 */     POTION_S_HYBRID = loadImage("images/potion/potion_s_hybrid.png");
/*  504 */     POTION_S_SPOTS = loadImage("images/potion/potion_s_spots.png");
/*  505 */     POTION_S_OUTLINE = loadImage("images/potion/potion_s_outline.png");
/*  506 */     POTION_M_CONTAINER = loadImage("images/potion/potion_m_glass.png");
/*  507 */     POTION_M_LIQUID = loadImage("images/potion/potion_m_liquid.png");
/*  508 */     POTION_M_HYBRID = loadImage("images/potion/potion_m_hybrid.png");
/*  509 */     POTION_M_SPOTS = loadImage("images/potion/potion_m_spots.png");
/*  510 */     POTION_M_OUTLINE = loadImage("images/potion/potion_m_outline.png");
/*      */     
/*      */ 
/*  513 */     POTION_SPHERE_CONTAINER = loadImage("images/potion/sphere/body.png");
/*  514 */     POTION_SPHERE_LIQUID = loadImage("images/potion/sphere/liquid.png");
/*  515 */     POTION_SPHERE_HYBRID = loadImage("images/potion/sphere/hybrid.png");
/*  516 */     POTION_SPHERE_SPOTS = loadImage("images/potion/sphere/spots.png");
/*  517 */     POTION_SPHERE_OUTLINE = loadImage("images/potion/sphere/outline.png");
/*      */     
/*      */ 
/*  520 */     POTION_H_CONTAINER = loadImage("images/potion/potion_h_glass.png");
/*  521 */     POTION_H_LIQUID = loadImage("images/potion/potion_h_liquid.png");
/*  522 */     POTION_H_HYBRID = loadImage("images/potion/potion_h_hybrid.png");
/*  523 */     POTION_H_SPOTS = loadImage("images/potion/potion_h_spots.png");
/*  524 */     POTION_H_OUTLINE = loadImage("images/potion/potion_h_outline.png");
/*      */     
/*      */ 
/*  527 */     POTION_BOTTLE_CONTAINER = loadImage("images/potion/bottle/body.png");
/*  528 */     POTION_BOTTLE_LIQUID = loadImage("images/potion/bottle/liquid.png");
/*  529 */     POTION_BOTTLE_HYBRID = loadImage("images/potion/bottle/hybrid.png");
/*  530 */     POTION_BOTTLE_SPOTS = loadImage("images/potion/bottle/spots.png");
/*  531 */     POTION_BOTTLE_OUTLINE = loadImage("images/potion/bottle/outline.png");
/*      */     
/*      */ 
/*  534 */     POTION_HEART_CONTAINER = loadImage("images/potion/heart/body.png");
/*  535 */     POTION_HEART_LIQUID = loadImage("images/potion/heart/liquid.png");
/*  536 */     POTION_HEART_HYBRID = loadImage("images/potion/heart/hybrid.png");
/*  537 */     POTION_HEART_SPOTS = loadImage("images/potion/heart/spots.png");
/*  538 */     POTION_HEART_OUTLINE = loadImage("images/potion/heart/outline.png");
/*      */     
/*      */ 
/*  541 */     POTION_SNECKO_CONTAINER = loadImage("images/potion/snecko/body.png");
/*  542 */     POTION_SNECKO_LIQUID = loadImage("images/potion/snecko/liquid.png");
/*  543 */     POTION_SNECKO_HYBRID = loadImage("images/potion/snecko/hybrid.png");
/*  544 */     POTION_SNECKO_SPOTS = loadImage("images/potion/snecko/spots.png");
/*  545 */     POTION_SNECKO_OUTLINE = loadImage("images/potion/snecko/outline.png");
/*      */     
/*      */ 
/*  548 */     POTION_FAIRY_CONTAINER = loadImage("images/potion/fairy/body.png");
/*  549 */     POTION_FAIRY_LIQUID = loadImage("images/potion/fairy/liquid.png");
/*  550 */     POTION_FAIRY_HYBRID = loadImage("images/potion/fairy/hybrid.png");
/*  551 */     POTION_FAIRY_SPOTS = loadImage("images/potion/fairy/spots.png");
/*  552 */     POTION_FAIRY_OUTLINE = loadImage("images/potion/fairy/outline.png");
/*      */     
/*      */ 
/*  555 */     POTION_GHOST_CONTAINER = loadImage("images/potion/ghost/body.png");
/*  556 */     POTION_GHOST_LIQUID = loadImage("images/potion/ghost/liquid.png");
/*  557 */     POTION_GHOST_HYBRID = loadImage("images/potion/ghost/hybrid.png");
/*  558 */     POTION_GHOST_SPOTS = loadImage("images/potion/ghost/spots.png");
/*  559 */     POTION_GHOST_OUTLINE = loadImage("images/potion/ghost/outline.png");
/*      */     
/*      */ 
/*  562 */     POTION_JAR_CONTAINER = loadImage("images/potion/jar/body.png");
/*  563 */     POTION_JAR_LIQUID = loadImage("images/potion/jar/liquid.png");
/*  564 */     POTION_JAR_HYBRID = loadImage("images/potion/jar/hybrid.png");
/*  565 */     POTION_JAR_SPOTS = loadImage("images/potion/jar/spots.png");
/*  566 */     POTION_JAR_OUTLINE = loadImage("images/potion/jar/outline.png");
/*      */     
/*      */ 
/*  569 */     POTION_BOLT_CONTAINER = loadImage("images/potion/bolt/body.png");
/*  570 */     POTION_BOLT_LIQUID = loadImage("images/potion/bolt/liquid.png");
/*  571 */     POTION_BOLT_HYBRID = loadImage("images/potion/bolt/hybrid.png");
/*  572 */     POTION_BOLT_SPOTS = loadImage("images/potion/bolt/spots.png");
/*  573 */     POTION_BOLT_OUTLINE = loadImage("images/potion/bolt/outline.png");
/*      */     
/*      */ 
/*  576 */     POTION_CARD_CONTAINER = loadImage("images/potion/card/body.png");
/*  577 */     POTION_CARD_LIQUID = loadImage("images/potion/card/liquid.png");
/*  578 */     POTION_CARD_HYBRID = loadImage("images/potion/card/hybrid.png");
/*  579 */     POTION_CARD_SPOTS = loadImage("images/potion/card/spots.png");
/*  580 */     POTION_CARD_OUTLINE = loadImage("images/potion/card/outline.png");
/*      */     
/*  582 */     POTION_PLACEHOLDER = loadImage("images/potion/potion_placeholder.png");
/*  583 */     POTION_SHADOW = loadImage("images/potion/potion_shadow.png");
/*      */     
/*      */ 
/*  586 */     CHECKBOX = loadImage("images/ui/checkbox.png");
/*  587 */     TICK = loadImage("images/ui/tick.png");
/*      */     
/*      */ 
/*  590 */     ORB_DARK = loadImage("images/orbs/dark.png");
/*  591 */     ORB_LIGHTNING = loadImage("images/orbs/lightning.png");
/*  592 */     ORB_PAPER = loadImage("images/orbs/paper.png");
/*  593 */     ORB_PLASMA = loadImage("images/orbs/plasma.png");
/*      */     
/*      */ 
/*  596 */     COLOR_TAB_RED = loadImage("images/ui/cardlibrary/redTab.png");
/*  597 */     COLOR_TAB_GREEN = loadImage("images/ui/cardlibrary/greenTab.png");
/*  598 */     COLOR_TAB_BLUE = loadImage("images/ui/cardlibrary/blueTab.png");
/*  599 */     COLOR_TAB_COLORLESS = loadImage("images/ui/cardlibrary/colorlessTab.png");
/*  600 */     COLOR_TAB_CURSE = loadImage("images/ui/cardlibrary/curseTab.png");
/*  601 */     COLOR_TAB_BAR = loadImage("images/ui/cardlibrary/colorTabBar.png");
/*  602 */     COLOR_TAB_LOCK = loadImage("images/ui/cardlibrary/colorTabLock.png");
/*  603 */     COLOR_TAB_BOX_UNTICKED = loadImage("images/ui/cardlibrary/tickbox_unticked.png");
/*  604 */     COLOR_TAB_BOX_TICKED = loadImage("images/ui/cardlibrary/tickbox_ticked.png");
/*      */     
/*  606 */     FILTER_IRONCLAD = loadImage("images/ui/leaderboards/ironclad.png");
/*  607 */     FILTER_SILENT = loadImage("images/ui/leaderboards/silent.png");
/*  608 */     FILTER_DEFECT = loadImage("images/ui/leaderboards/defect.png");
/*  609 */     P_STANDARD = loadImage("images/ui/mainMenu/portrait/standard.jpg");
/*  610 */     P_DAILY = loadImage("images/ui/mainMenu/portrait/daily.jpg");
/*  611 */     P_LOOP = loadImage("images/ui/mainMenu/portrait/loop.jpg");
/*  612 */     P_INFO_CARD = loadImage("images/ui/mainMenu/portrait/card.jpg");
/*  613 */     P_INFO_RELIC = loadImage("images/ui/mainMenu/portrait/relics.jpg");
/*  614 */     P_INFO_ENEMY = loadImage("images/ui/mainMenu/portrait/bestiary.jpg");
/*  615 */     P_STAT_CHAR = loadImage("images/ui/mainMenu/portrait/charstat.jpg");
/*  616 */     P_STAT_LEADERBOARD = loadImage("images/ui/mainMenu/portrait/leaderboards.jpg");
/*  617 */     P_STAT_HISTORY = loadImage("images/ui/mainMenu/portrait/history.jpg");
/*  618 */     P_SETTING_GAME = loadImage("images/ui/mainMenu/portrait/gamesettings.jpg");
/*  619 */     P_SETTING_INPUT = loadImage("images/ui/mainMenu/portrait/standard.jpg");
/*  620 */     P_SETTING_CREDITS = loadImage("images/ui/mainMenu/portrait/credits.jpg");
/*  621 */     P_LOCK = loadImage("images/ui/mainMenu/portrait/lock.png");
/*  622 */     MENU_PANEL_PORTRAIT = loadImage("images/ui/mainMenu/menuPanelPortrait.png");
/*  623 */     MENU_PANEL_BG_BLUE = loadImage("images/ui/mainMenu/menuPanel.png");
/*  624 */     MENU_PANEL_BG_GRAY = loadImage("images/ui/mainMenu/menuPanel4.png");
/*  625 */     MENU_PANEL_BG_RED = loadImage("images/ui/mainMenu/menuPanel3.png");
/*  626 */     MENU_PANEL_BG_BEIGE = loadImage("images/ui/mainMenu/menuPanel2.png");
/*  627 */     MENU_PANEL_FRAME = loadImage("images/ui/mainMenu/menuPanelFrame.png");
/*  628 */     FILTER_GLOW_BG = loadImage("images/ui/leaderboards/glow.png");
/*  629 */     CHAR_OPT_HIGHLIGHT = loadImage("images/ui/charSelect/highlightButton2.png");
/*  630 */     POPUP_ARROW = loadImage("images/ui/popupArrow.png");
/*  631 */     RELIC_POPUP = loadImage("images/ui/relicPopup.png");
/*  632 */     NEOW_BODY = loadImage("images/npcs/neow.png");
/*  633 */     RELIC_LOCK = loadImage("images/relics/lock.png");
/*  634 */     RELIC_LOCK_OUTLINE = loadImage("images/relics/outline/lock.png");
/*  635 */     FILTER_ARROW = loadImage("images/ui/filterArrow.png");
/*  636 */     MAIN_MENU_OPT_BTN = loadImage("images/ui/mainMenu/optButton.png");
/*  637 */     MAIN_MENU_OPT_BTN2 = loadImage("images/ui/mainMenu/optButton2.png");
/*  638 */     MAIN_MENU_OPT_BTN3 = loadImage("images/ui/mainMenu/optButton3.png");
/*  639 */     LOADOUT_ACTIVE = loadImage("images/ui/mainMenu/loadoutActive.png");
/*  640 */     LOADOUT_INACTIVE = loadImage("images/ui/mainMenu/loadoutInactive.png");
/*  641 */     LOADOUT_LOCKED = loadImage("images/ui/mainMenu/loadoutLocked.png");
/*  642 */     SCROLL_GRADIENT = loadImage("images/ui/scrollGradient.png");
/*  643 */     MAP_ICON = loadImage("images/ui/topPanel/map.png");
/*  644 */     DECK_ICON = loadImage("images/ui/topPanel/deck.png");
/*  645 */     SETTINGS_ICON = loadImage("images/ui/topPanel/settings.png");
/*  646 */     UNLOCK_ICON = loadImage("images/ui/topPanel/unlock.png");
/*  647 */     ENDLESS_ICON = loadImage("images/ui/topPanel/endless.png");
/*  648 */     RED_BANNER = loadImage("images/characterSelect/banner.png");
/*  649 */     GREEN_BANNER = loadImage("images/characterSelect/banner2.png");
/*  650 */     BLUE_BANNER = loadImage("images/characterSelect/banner3.png");
/*  651 */     RETICLE_CORNER = loadImage("images/ui/combat/reticleCorner.png");
/*  652 */     TARGET_UI_ARROW = loadImage("images/ui/combat/reticleArrow.png");
/*  653 */     TARGET_UI_CIRCLE = loadImage("images/ui/combat/reticleBlock.png");
/*  654 */     EDGE_FADE = loadImage("images/ui/edgeFade.png");
/*  655 */     LARGE_CLOUD = loadImage("images/ui/dialog/largeCircle2.png");
/*  656 */     SMALL_CLOUD = loadImage("images/ui/dialog/smallCircle.png");
/*  657 */     CAMPFIRE_LIT = loadImage("images/npcs/fireLit.png");
/*  658 */     CAMPFIRE_UNLIT = loadImage("images/npcs/fireUnlit.png");
/*  659 */     GHOST_ORB_1 = loadImage("images/monsters/theBottom/boss/fire1.png");
/*  660 */     GHOST_ORB_2 = loadImage("images/monsters/theBottom/boss/fire2.png");
/*      */     
/*  662 */     CONTROLLER_HB_HIGHLIGHT = loadImage("images/ui/selectionBox.png");
/*  663 */     DEBUG_HITBOX_IMG = new Texture("images/debugHitbox.png");
/*  664 */     DEBUG_HITBOX_IMG.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  665 */     DIALOG_BOX_IMG = loadImage("images/ui/dialog/dialogPanel.png");
/*  666 */     SPEECH_BUBBLE_IMG = loadImage("images/ui/dialog/speechBubble2.png");
/*  667 */     SHOP_SPEECH_BUBBLE_IMG = loadImage("images/ui/dialog/speechBubble3.png");
/*  668 */     SPEECH_BUBBLE_L_IMG = loadImage("images/ui/dialog/speechBubbleLarge.png");
/*  669 */     DIALOG_DOT_IMG = loadImage("images/ui/dialog/dialogDot.png");
/*  670 */     DOTTED_SQUARE = loadImage("images/dottedSquare.png");
/*  671 */     KEYWORD_TOP = loadImage("images/ui/tip/tipTop.png");
/*  672 */     KEYWORD_BODY = loadImage("images/ui/tip/tipMid.png");
/*  673 */     KEYWORD_BOT = loadImage("images/ui/tip/tipBot.png");
/*  674 */     FTUE = loadImage("images/ui/tip/ftue.png");
/*  675 */     FTUE_BTN = loadImage("images/ui/tip/ftueButton.png");
/*  676 */     DYNAMIC_BTN_IMG2 = loadImage("images/ui/topPanel/buttonL.png");
/*  677 */     DYNAMIC_BTN_IMG3 = loadImage("images/ui/topPanel/buttonLRed.png");
/*  678 */     DYNAMIC_BTN_IMG = loadImage("images/ui/topPanel/button.png");
/*  679 */     CANT_USE_RELIC = loadImage("images/relics/cantUseRelic.png");
/*  680 */     DIALOG_OPTION_BUTTON = loadImage("images/dialogOptionButton.png");
/*  681 */     WHITE_SQUARE_IMG = new Texture("images/whiteSquare32.png");
/*  682 */     WHITE_SQUARE_IMG.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  683 */     DECK_COUNT_CIRCLE = loadImage("images/ui/topPanel/countCircle.png");
/*  684 */     TOP_PANEL_BAR = loadImage("images/ui/topPanel/bar.png");
/*  685 */     TIMER_ICON = loadImage("images/ui/timerIcon.png");
/*      */     
/*      */ 
/*  688 */     VICTORY_BANNER = loadImage("images/ui/selectBanner.png");
/*  689 */     REWARD_SCREEN_SHEET = loadImage("images/ui/reward/rewardScreenSheet.png");
/*  690 */     REWARD_SCREEN_TAKE_BUTTON = loadImage("images/ui/reward/takeAll.png");
/*  691 */     REWARD_SCREEN_TAKE_USED_BUTTON = loadImage("images/ui/reward/takeAllUsed.png");
/*  692 */     REWARD_SCREEN_ITEM = loadImage("images/ui/reward/rewardListItemPanel.png");
/*  693 */     REWARD_CARD_NORMAL = loadImage("images/ui/reward/normalCardReward.png");
/*  694 */     REWARD_CARD_BOSS = loadImage("images/ui/reward/bossCardReward.png");
/*      */     
/*      */ 
/*  697 */     UI_GOLD = loadImage("images/ui/topPanel/gold.png");
/*  698 */     TP_HP = loadImage("images/ui/topPanel/panelHeart.png");
/*  699 */     TP_GOLD = loadImage("images/ui/topPanel/panelGoldBag.png");
/*  700 */     TP_CRYSTAL = loadImage("images/ui/topPanel/bossCrystal.png");
/*  701 */     HEALTH_BAR_B = loadImage("images/ui/combat/body7.png");
/*  702 */     HEALTH_BAR_L = loadImage("images/ui/combat/left7.png");
/*  703 */     HEALTH_BAR_R = loadImage("images/ui/combat/right7.png");
/*  704 */     HB_SHADOW_L = loadImage("images/ui/combat/leftBg.png");
/*  705 */     HB_SHADOW_R = loadImage("images/ui/combat/rightBg.png");
/*  706 */     HB_SHADOW_B = loadImage("images/ui/combat/bodyBg.png");
/*  707 */     BLOCK_ICON = loadImage("images/ui/combat/block.png");
/*  708 */     BLOCK_ICON_L = loadImage("images/ui/combat/blockL.png");
/*  709 */     BLOCK_ICON_R = loadImage("images/ui/combat/blockR.png");
/*  710 */     BLOCK_BAR_B = loadImage("images/ui/combat/blockBody3.png");
/*  711 */     BLOCK_BAR_R = loadImage("images/ui/combat/blockRight3.png");
/*  712 */     BLOCK_BAR_L = loadImage("images/ui/combat/blockLeft3.png");
/*  713 */     DRAW_PILE_BANNER = loadImage("images/ui/combat/combatDeckBanner.png");
/*  714 */     DISCARD_PILE_BANNER = loadImage("images/ui/combat/discardPileBanner.png");
/*      */     
/*  716 */     SETTINGS_BACKGROUND = loadImage("images/ui/option/settingsBackground.png");
/*  717 */     OPTION_ABANDON = loadImage("images/ui/option/abandon.png");
/*  718 */     OPTION_TOGGLE = loadImage("images/ui/option/toggleButtonBase.png");
/*  719 */     OPTION_TOGGLE_ON = loadImage("images/ui/option/toggleButtonOverlay2.png");
/*  720 */     OPTION_SLIDER_BG = loadImage("images/ui/option/sliderBg.png");
/*  721 */     OPTION_SLIDER = loadImage("images/ui/option/slider.png");
/*  722 */     OPTION_ABANDON = loadImage("images/ui/option/abandon.png");
/*  723 */     OPTION_EXIT = loadImage("images/ui/option/quitButton.png");
/*  724 */     OPTION_CONFIRM = loadImage("images/ui/option/confirm.png");
/*  725 */     RENAME_BOX = loadImage("images/ui/option/nameBox.png");
/*  726 */     OPTION_YES = loadImage("images/ui/option/yes.png");
/*  727 */     OPTION_NO = loadImage("images/ui/option/no.png");
/*  728 */     OPTION_VERTICAL_SLIDER = loadImage("images/ui/option/verticalSlider.png");
/*  729 */     SCROLL_BAR_TOP = loadImage("images/ui/mainMenu/scrollBar/scrollBarTrackTop.png");
/*  730 */     SCROLL_BAR_MIDDLE = loadImage("images/ui/mainMenu/scrollBar/scrollBarTrackMid.png");
/*  731 */     SCROLL_BAR_BOTTOM = loadImage("images/ui/mainMenu/scrollBar/scrollBarTrackBottom.png");
/*  732 */     SCROLL_BAR_TRAIN = loadImage("images/ui/mainMenu/scrollBar/scrollBarTrain.png");
/*  733 */     INPUT_SETTINGS_BG = loadImage("images/ui/option/inputPaneBg2.png");
/*  734 */     INPUT_SETTINGS_EDGES = loadImage("images/ui/option/inputPaneFg.png");
/*  735 */     INPUT_SETTINGS_ROW = loadImage("images/ui/option/inputCell2.png");
/*      */     
/*      */ 
/*  738 */     DECK_BTN_BASE = loadImage("images/ui/deckButton/base.png");
/*  739 */     DISCARD_BTN_BASE = loadImage("images/ui/discardButton/base.png");
/*      */     
/*      */ 
/*  742 */     CAMPFIRE_FIRE = loadImage("images/ui/campfire/fire.png");
/*  743 */     CAMPFIRE_REST_BUTTON = loadImage("images/ui/campfire/sleep.png");
/*  744 */     CAMPFIRE_MEDITATE_BUTTON = loadImage("images/ui/campfire/meditate.png");
/*  745 */     CAMPFIRE_SMITH_BUTTON = loadImage("images/ui/campfire/smith.png");
/*  746 */     CAMPFIRE_SMITH_DISABLE_BUTTON = loadImage("images/ui/campfire/smithDisabled.png");
/*  747 */     CAMPFIRE_RITUAL_BUTTON = loadImage("images/ui/campfire/ritual.png");
/*  748 */     CAMPFIRE_TOKE_BUTTON = loadImage("images/ui/campfire/toke.png");
/*  749 */     CAMPFIRE_TOKE_DISABLE_BUTTON = loadImage("images/ui/campfire/tokeDisabled.png");
/*  750 */     CAMPFIRE_TRAIN_BUTTON = loadImage("images/ui/campfire/train.png");
/*  751 */     CAMPFIRE_DIG_BUTTON = loadImage("images/ui/campfire/dig.png");
/*  752 */     CAMPFIRE_BREW_BUTTON = loadImage("images/ui/campfire/brew.png");
/*  753 */     CAMPFIRE_HOVER_BUTTON = loadImage("images/ui/campfire/outline.png");
/*  754 */     CAMPFIRE_BUTTON_SHADOW = loadImage("images/ui/campfire/buttonShadow.png");
/*  755 */     UPGRADE_ARROW = loadImage("images/ui/campfire/upgradeArrow.png");
/*  756 */     CF_IC_BODY = loadImage("images/characterSelect/ironclad/body.png");
/*  757 */     CF_IC_BODY_G = loadImage("images/characterSelect/ironclad/glow.png");
/*  758 */     CF_IC_SHADOW = loadImage("images/characterSelect/ironclad/shadow.png");
/*  759 */     CF_S_BODY = loadImage("images/characterSelect/silent/body.png");
/*  760 */     CF_S_BODY_G = loadImage("images/characterSelect/silent/glow.png");
/*  761 */     CF_S_SHADOW = loadImage("images/characterSelect/silent/shadow.png");
/*  762 */     CF_LEFT_ARROW = loadImage("images/ui/charSelect/tinyLeftArrow.png");
/*  763 */     CF_RIGHT_ARROW = loadImage("images/ui/charSelect/tinyRightArrow.png");
/*      */     
/*      */ 
/*  766 */     INTENT_ATK_1 = loadImage("images/ui/intent/attack/attack_intent_1.png");
/*  767 */     INTENT_ATK_2 = loadImage("images/ui/intent/attack/attack_intent_2.png");
/*  768 */     INTENT_ATK_3 = loadImage("images/ui/intent/attack/attack_intent_3.png");
/*  769 */     INTENT_ATK_4 = loadImage("images/ui/intent/attack/attack_intent_4.png");
/*  770 */     INTENT_ATK_5 = loadImage("images/ui/intent/attack/attack_intent_5.png");
/*  771 */     INTENT_ATK_6 = loadImage("images/ui/intent/attack/attack_intent_6.png");
/*  772 */     INTENT_ATK_7 = loadImage("images/ui/intent/attack/attack_intent_7.png");
/*  773 */     INTENT_ATK_TIP_1 = loadImage("images/ui/intent/tip/1.png");
/*  774 */     INTENT_ATK_TIP_2 = loadImage("images/ui/intent/tip/2.png");
/*  775 */     INTENT_ATK_TIP_3 = loadImage("images/ui/intent/tip/3.png");
/*  776 */     INTENT_ATK_TIP_4 = loadImage("images/ui/intent/tip/4.png");
/*  777 */     INTENT_ATK_TIP_5 = loadImage("images/ui/intent/tip/5.png");
/*  778 */     INTENT_ATK_TIP_6 = loadImage("images/ui/intent/tip/6.png");
/*  779 */     INTENT_ATK_TIP_7 = loadImage("images/ui/intent/tip/7.png");
/*  780 */     INTENT_BUFF = loadImage("images/ui/intent/buff1.png");
/*  781 */     INTENT_BUFF_L = loadImage("images/ui/intent/buff1L.png");
/*  782 */     INTENT_DEBUFF = loadImage("images/ui/intent/debuff1.png");
/*  783 */     INTENT_DEBUFF_L = loadImage("images/ui/intent/debuff1L.png");
/*  784 */     INTENT_DEBUFF2 = loadImage("images/ui/intent/debuff2.png");
/*  785 */     INTENT_DEBUFF2_L = loadImage("images/ui/intent/debuff2L.png");
/*  786 */     INTENT_DEFEND = loadImage("images/ui/intent/defend.png");
/*  787 */     INTENT_DEFEND_L = loadImage("images/ui/intent/defendL.png");
/*  788 */     INTENT_DEFEND_BUFF = loadImage("images/ui/intent/defendBuff.png");
/*  789 */     INTENT_DEFEND_BUFF_L = loadImage("images/ui/intent/defendBuffL.png");
/*  790 */     INTENT_ESCAPE = loadImage("images/ui/intent/escape.png");
/*  791 */     INTENT_ESCAPE_L = loadImage("images/ui/intent/escapeL.png");
/*  792 */     INTENT_MAGIC = loadImage("images/ui/intent/magic.png");
/*  793 */     INTENT_MAGIC_L = loadImage("images/ui/intent/magicL.png");
/*  794 */     INTENT_SLEEP = loadImage("images/ui/intent/sleep.png");
/*  795 */     INTENT_SLEEP_L = loadImage("images/ui/intent/sleepL.png");
/*  796 */     INTENT_STUN = loadImage("images/ui/intent/stun.png");
/*  797 */     INTENT_STUN_L = loadImage("images/ui/intent/stunL.png");
/*  798 */     INTENT_UNKNOWN = loadImage("images/ui/intent/unknown.png");
/*  799 */     INTENT_UNKNOWN_L = loadImage("images/ui/intent/unknownL.png");
/*  800 */     INTENT_ATTACK_BUFF = loadImage("images/ui/intent/attackBuff.png");
/*  801 */     INTENT_ATTACK_DEBUFF = loadImage("images/ui/intent/attackDebuff.png");
/*  802 */     INTENT_ATTACK_DEFEND = loadImage("images/ui/intent/attackDefend.png");
/*      */     
/*  804 */     ENERGY_RED_LAYER6 = loadImage("images/ui/topPanel/red/layer6.png");
/*  805 */     ENERGY_RED_LAYER5 = loadImage("images/ui/topPanel/red/layer5.png");
/*  806 */     ENERGY_RED_LAYER4 = loadImage("images/ui/topPanel/red/layer4.png");
/*  807 */     ENERGY_RED_LAYER3 = loadImage("images/ui/topPanel/red/layer3.png");
/*  808 */     ENERGY_RED_LAYER2 = loadImage("images/ui/topPanel/red/layer2.png");
/*  809 */     ENERGY_RED_LAYER1 = loadImage("images/ui/topPanel/red/layer1.png");
/*  810 */     ENERGY_RED_LAYER5D = loadImage("images/ui/topPanel/red/layer5d.png");
/*  811 */     ENERGY_RED_LAYER4D = loadImage("images/ui/topPanel/red/layer4d.png");
/*  812 */     ENERGY_RED_LAYER3D = loadImage("images/ui/topPanel/red/layer3d.png");
/*  813 */     ENERGY_RED_LAYER2D = loadImage("images/ui/topPanel/red/layer2d.png");
/*  814 */     ENERGY_RED_LAYER1D = loadImage("images/ui/topPanel/red/layer1d.png");
/*      */     
/*  816 */     ENERGY_GREEN_LAYER6 = loadImage("images/ui/topPanel/green/layer6.png");
/*  817 */     ENERGY_GREEN_LAYER5 = loadImage("images/ui/topPanel/green/layer5.png");
/*  818 */     ENERGY_GREEN_LAYER4 = loadImage("images/ui/topPanel/green/layer4.png");
/*  819 */     ENERGY_GREEN_LAYER3 = loadImage("images/ui/topPanel/green/layer3.png");
/*  820 */     ENERGY_GREEN_LAYER2 = loadImage("images/ui/topPanel/green/layer2.png");
/*  821 */     ENERGY_GREEN_LAYER1 = loadImage("images/ui/topPanel/green/layer1.png");
/*  822 */     ENERGY_GREEN_LAYER5D = loadImage("images/ui/topPanel/green/layer5d.png");
/*  823 */     ENERGY_GREEN_LAYER4D = loadImage("images/ui/topPanel/green/layer4d.png");
/*  824 */     ENERGY_GREEN_LAYER3D = loadImage("images/ui/topPanel/green/layer3d.png");
/*  825 */     ENERGY_GREEN_LAYER2D = loadImage("images/ui/topPanel/green/layer2d.png");
/*  826 */     ENERGY_GREEN_LAYER1D = loadImage("images/ui/topPanel/green/layer1d.png");
/*      */     
/*  828 */     ENERGY_BLUE_LAYER6 = loadImage("images/ui/topPanel/blue/border.png");
/*  829 */     ENERGY_BLUE_LAYER5 = loadImage("images/ui/topPanel/blue/5.png");
/*  830 */     ENERGY_BLUE_LAYER4 = loadImage("images/ui/topPanel/blue/4.png");
/*  831 */     ENERGY_BLUE_LAYER3 = loadImage("images/ui/topPanel/blue/3.png");
/*  832 */     ENERGY_BLUE_LAYER2 = loadImage("images/ui/topPanel/blue/2.png");
/*  833 */     ENERGY_BLUE_LAYER1 = loadImage("images/ui/topPanel/blue/1.png");
/*  834 */     ENERGY_BLUE_LAYER5D = loadImage("images/ui/topPanel/blue/5d.png");
/*  835 */     ENERGY_BLUE_LAYER4D = loadImage("images/ui/topPanel/blue/4d.png");
/*  836 */     ENERGY_BLUE_LAYER3D = loadImage("images/ui/topPanel/blue/3d.png");
/*  837 */     ENERGY_BLUE_LAYER2D = loadImage("images/ui/topPanel/blue/2d.png");
/*  838 */     ENERGY_BLUE_LAYER1D = loadImage("images/ui/topPanel/blue/1d.png");
/*      */     
/*  840 */     RED_ORB_FLASH_VFX = loadImage("images/ui/topPanel/energyRedVFX.png");
/*  841 */     GREEN_ORB_FLASH_VFX = loadImage("images/ui/topPanel/energyGreenVFX.png");
/*  842 */     BLUE_ORB_FLASH_VFX = loadImage("images/ui/topPanel/energyBlueVFX.png");
/*      */     
/*      */ 
/*  845 */     MAP_NODE_PURGE = loadImage("images/ui/map/purgeWellIcon.png");
/*  846 */     MAP_NODE_ELITE = loadImage("images/ui/map/elite.png");
/*  847 */     MAP_NODE_ELITE_OUTLINE = loadImage("images/ui/map/eliteOutline.png");
/*  848 */     MAP_NODE_ENEMY = loadImage("images/ui/map/monster.png");
/*  849 */     MAP_NODE_ENEMY_OUTLINE = loadImage("images/ui/map/monsterOutline.png");
/*  850 */     MAP_NODE_SHRINE = loadImage("images/ui/map/mapShrineIcon.png");
/*  851 */     MAP_NODE_REST = loadImage("images/ui/map/rest.png");
/*  852 */     MAP_NODE_REST_OUTLINE = loadImage("images/ui/map/restOutline.png");
/*  853 */     MAP_NODE_MERCHANT = loadImage("images/ui/map/shop.png");
/*  854 */     MAP_NODE_MERCHANT_OUTLINE = loadImage("images/ui/map/shopOutline.png");
/*  855 */     MAP_LEGEND = loadImage("images/ui/map/legend2.png");
/*  856 */     MAP_NODE_TREASURE = loadImage("images/ui/map/chest.png");
/*  857 */     MAP_NODE_TREASURE_OUTLINE = loadImage("images/ui/map/chestOutline.png");
/*  858 */     MAP_NODE_COMPLETE = loadImage("images/ui/map/mapCompleteIcon.png");
/*  859 */     MAP_CIRCLE_1 = loadImage("images/ui/map/circle1.png");
/*  860 */     MAP_CIRCLE_2 = loadImage("images/ui/map/circle2.png");
/*  861 */     MAP_CIRCLE_3 = loadImage("images/ui/map/circle3.png");
/*  862 */     MAP_CIRCLE_4 = loadImage("images/ui/map/circle4.png");
/*  863 */     MAP_CIRCLE_5 = loadImage("images/ui/map/circle5.png");
/*  864 */     MAP_NODE_EVENT = loadImage("images/ui/map/event.png");
/*  865 */     MAP_NODE_EVENT_OUTLINE = loadImage("images/ui/map/eventOutline.png");
/*  866 */     MAP_DOT_1 = loadImage("images/ui/map/dot1.png");
/*  867 */     MAP_TOP = loadImage("images/ui/map/mapTop.png");
/*  868 */     MAP_MID = loadImage("images/ui/map/mapMid.png");
/*  869 */     MAP_BOT = loadImage("images/ui/map/mapBot.png");
/*  870 */     MAP_BLEND = loadImage("images/ui/map/mapBlend.png");
/*      */     
/*      */ 
/*  873 */     CANCEL_BUTTON = loadImage("images/ui/topPanel/cancelButton.png");
/*  874 */     CANCEL_BUTTON_SHADOW = loadImage("images/ui/topPanel/cancelButtonShadow.png");
/*  875 */     CANCEL_BUTTON_OUTLINE = loadImage("images/ui/topPanel/cancelButtonOutline.png");
/*  876 */     CONFIRM_BUTTON = loadImage("images/ui/topPanel/confirmButton.png");
/*  877 */     CONFIRM_BUTTON_SHADOW = loadImage("images/ui/topPanel/confirmButtonShadow.png");
/*  878 */     CONFIRM_BUTTON_OUTLINE = loadImage("images/ui/topPanel/confirmButtonOutline.png");
/*  879 */     PROCEED_BUTTON = loadImage("images/ui/topPanel/proceedButton.png");
/*  880 */     PROCEED_BUTTON_SHADOW = loadImage("images/ui/topPanel/proceedButtonShadow.png");
/*  881 */     PROCEED_BUTTON_OUTLINE = loadImage("images/ui/topPanel/proceedButtonOutline.png");
/*      */     
/*  883 */     UNLOCK_TEXT_BG = loadImage("images/ui/unlockBlur.png");
/*  884 */     EVENT_IMG_FRAME = loadImage("images/ui/event/imgFrame.png");
/*  885 */     EVENT_BUTTON_ENABLED = loadImage("images/ui/event/enabledButton.png");
/*  886 */     EVENT_BUTTON_DISABLED = loadImage("images/ui/event/disabledButton.png");
/*  887 */     EVENT_ROOM_PANEL = loadImage("images/ui/event/roomTextPanel.png");
/*  888 */     END_TURN_BUTTON = loadImage("images/ui/topPanel/endTurnButton.png");
/*  889 */     END_TURN_BUTTON_GLOW = loadImage("images/ui/topPanel/endTurnButtonGlow.png");
/*  890 */     END_TURN_HOVER = loadImage("images/ui/topPanel/endTurnHover.png");
/*      */     
/*      */ 
/*  893 */     TINY_CARD_ATTACK = loadImage("images/tinyCards/attackPortrait.png");
/*  894 */     TINY_CARD_ATTACK.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  895 */     TINY_CARD_SKILL = loadImage("images/tinyCards/skillPortrait.png");
/*  896 */     TINY_CARD_SKILL.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  897 */     TINY_CARD_POWER = loadImage("images/tinyCards/powerPortrait.png");
/*  898 */     TINY_CARD_POWER.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  899 */     TINY_CARD_PORTRAIT_SHADOW = loadImage("images/tinyCards/portraitShadow.png");
/*  900 */     TINY_CARD_PORTRAIT_SHADOW.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  901 */     TINY_CARD_BACKGROUND = loadImage("images/tinyCards/cardBack.png");
/*  902 */     TINY_CARD_BACKGROUND.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  903 */     TINY_CARD_BANNER = loadImage("images/tinyCards/banner.png");
/*  904 */     TINY_CARD_BANNER.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  905 */     TINY_CARD_BANNER_SHADOW = loadImage("images/tinyCards/bannerShadow.png");
/*  906 */     TINY_CARD_BANNER_SHADOW.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*  907 */     TINY_CARD_DESCRIPTION = loadImage("images/tinyCards/descBox.png");
/*  908 */     TINY_CARD_DESCRIPTION.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
/*      */     
/*      */ 
/*  911 */     RUN_HISTORY_MAP_ICON_BOSS = loadImage("images/ui/runHistory/mapIcons/boss.png");
/*  912 */     RUN_HISTORY_MAP_ICON_BOSS_CHEST = loadImage("images/ui/runHistory/mapIcons/bosschest.png");
/*  913 */     RUN_HISTORY_MAP_ICON_CHEST = loadImage("images/ui/runHistory/mapIcons/chest.png");
/*  914 */     RUN_HISTORY_MAP_ICON_ELITE = loadImage("images/ui/runHistory/mapIcons/elite.png");
/*  915 */     RUN_HISTORY_MAP_ICON_EVENT = loadImage("images/ui/runHistory/mapIcons/unknown.png");
/*  916 */     RUN_HISTORY_MAP_ICON_MONSTER = loadImage("images/ui/runHistory/mapIcons/monster.png");
/*  917 */     RUN_HISTORY_MAP_ICON_REST = loadImage("images/ui/runHistory/mapIcons/rest.png");
/*  918 */     RUN_HISTORY_MAP_ICON_SHOP = loadImage("images/ui/runHistory/mapIcons/shop.png");
/*  919 */     RUN_HISTORY_MAP_ICON_UNKNOWN_CHEST = loadImage("images/ui/runHistory/mapIcons/unknownChest.png");
/*  920 */     RUN_HISTORY_MAP_ICON_UNKNOWN_MONSTER = loadImage("images/ui/runHistory/mapIcons/unknownMonster.png");
/*  921 */     RUN_HISTORY_MAP_ICON_UNKNOWN_SHOP = loadImage("images/ui/runHistory/mapIcons/unknownShop.png");
/*      */     
/*  923 */     logger.info("Texture load time: " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */   public static void loadControllerImages(CInputHelper.ControllerModel model)
/*      */   {
/*  928 */     if (CONTROLLER_A != null) {
/*  929 */       CONTROLLER_A.dispose();
/*  930 */       CONTROLLER_B.dispose();
/*  931 */       CONTROLLER_X.dispose();
/*  932 */       CONTROLLER_Y.dispose();
/*  933 */       CONTROLLER_LB.dispose();
/*  934 */       CONTROLLER_RB.dispose();
/*  935 */       CONTROLLER_BACK.dispose();
/*  936 */       CONTROLLER_START.dispose();
/*  937 */       CONTROLLER_LS_UP.dispose();
/*  938 */       CONTROLLER_LS_DOWN.dispose();
/*  939 */       CONTROLLER_LS_LEFT.dispose();
/*  940 */       CONTROLLER_LS_RIGHT.dispose();
/*  941 */       CONTROLLER_D_UP.dispose();
/*  942 */       CONTROLLER_D_DOWN.dispose();
/*  943 */       CONTROLLER_D_LEFT.dispose();
/*  944 */       CONTROLLER_D_RIGHT.dispose();
/*  945 */       CONTROLLER_LT.dispose();
/*  946 */       CONTROLLER_RT.dispose();
/*      */     }
/*      */     
/*  949 */     String dirName = "xbox360";
/*      */     
/*  951 */     switch (model)
/*      */     {
/*      */     case XBOX_360: 
/*      */       break;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     case XBOX_ONE: 
/*  963 */       dirName = "xboxOne";
/*  964 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  969 */     CONTROLLER_A = loadImage("images/ui/controller/" + dirName + "/a.png");
/*  970 */     CONTROLLER_B = loadImage("images/ui/controller/" + dirName + "/b.png");
/*  971 */     CONTROLLER_X = loadImage("images/ui/controller/" + dirName + "/x.png");
/*  972 */     CONTROLLER_Y = loadImage("images/ui/controller/" + dirName + "/y.png");
/*  973 */     CONTROLLER_LB = loadImage("images/ui/controller/" + dirName + "/lb.png");
/*  974 */     CONTROLLER_RB = loadImage("images/ui/controller/" + dirName + "/rb.png");
/*  975 */     CONTROLLER_BACK = loadImage("images/ui/controller/" + dirName + "/back.png");
/*  976 */     CONTROLLER_START = loadImage("images/ui/controller/" + dirName + "/start.png");
/*  977 */     CONTROLLER_LS_UP = loadImage("images/ui/controller/" + dirName + "/ls_up.png");
/*  978 */     CONTROLLER_LS_DOWN = loadImage("images/ui/controller/" + dirName + "/ls_down.png");
/*  979 */     CONTROLLER_LS_LEFT = loadImage("images/ui/controller/" + dirName + "/ls_left.png");
/*  980 */     CONTROLLER_LS_RIGHT = loadImage("images/ui/controller/" + dirName + "/ls_right.png");
/*  981 */     CONTROLLER_D_UP = loadImage("images/ui/controller/" + dirName + "/up.png");
/*  982 */     CONTROLLER_D_DOWN = loadImage("images/ui/controller/" + dirName + "/down.png");
/*  983 */     CONTROLLER_D_LEFT = loadImage("images/ui/controller/" + dirName + "/left.png");
/*  984 */     CONTROLLER_D_RIGHT = loadImage("images/ui/controller/" + dirName + "/right.png");
/*  985 */     CONTROLLER_LT = loadImage("images/ui/controller/" + dirName + "/lt.png");
/*  986 */     CONTROLLER_RT = loadImage("images/ui/controller/" + dirName + "/rt.png");
/*      */   }
/*      */   
/*      */   public static Texture P_INFO_RELIC;
/*      */   public static Texture P_INFO_ENEMY;
/*      */   
/*  992 */   public static Texture loadImage(String imgUrl) { assert (imgUrl != null) : "DO NOT CALL LOAD IMAGE WITH NULL";
/*      */     try
/*      */     {
/*  995 */       Texture retVal = new Texture(imgUrl);
/*  996 */       retVal.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
/*  997 */       return retVal;
/*      */     }
/*      */     catch (Exception e) {}
/* 1000 */     return null;
/*      */   }
/*      */   
/*      */   public static Texture P_STAT_CHAR;
/*      */   public static Texture P_STAT_LEADERBOARD;
/*      */   public static Texture P_STAT_HISTORY;
/*      */   public static Texture P_SETTING_GAME;
/*      */   public static Texture P_SETTING_INPUT;
/*      */   public static Texture P_SETTING_CREDITS;
/*      */   public static Texture P_LOCK;
/*      */   public static Texture MENU_PANEL_BG_GRAY;
/*      */   public static Texture MENU_PANEL_BG_RED;
/*      */   public static Texture MENU_PANEL_BG_BEIGE;
/*      */   public static Texture MENU_PANEL_BG_BLUE;
/*      */   public static Texture MENU_PANEL_FRAME;
/*      */   public static Texture MENU_PANEL_PORTRAIT;
/*      */   public static Texture FILTER_GLOW_BG;
/*      */   public static Texture CHAR_OPT_HIGHLIGHT;
/*      */   public static Texture RELIC_POPUP;
/*      */   public static Texture POPUP_ARROW;
/*      */   public static Texture NEOW_BODY;
/*      */   public static Texture RELIC_LOCK;
/*      */   public static Texture RELIC_LOCK_OUTLINE;
/*      */   public static Texture FILTER_ARROW;
/*      */   public static Texture MAIN_MENU_OPT_BTN;
/*      */   public static Texture MAIN_MENU_OPT_BTN2;
/*      */   public static Texture MAIN_MENU_OPT_BTN3;
/*      */   public static Texture LOADOUT_ACTIVE;
/*      */   public static Texture LOADOUT_INACTIVE;
/*      */   public static Texture LOADOUT_LOCKED;
/*      */   public static Texture SCROLL_GRADIENT;
/*      */   public static Texture MAP_ICON;
/*      */   public static Texture DECK_ICON;
/*      */   public static Texture SETTINGS_ICON;
/*      */   public static Texture UNLOCK_ICON;
/*      */   public static Texture ENDLESS_ICON;
/*      */   public static Texture RED_BANNER;
/*      */   public static Texture GREEN_BANNER;
/*      */   public static Texture BLUE_BANNER;
/*      */   public static Texture RETICLE_CORNER;
/*      */   public static Texture TARGET_UI_ARROW;
/*      */   public static Texture TARGET_UI_CIRCLE;
/*      */   public static Texture EDGE_FADE;
/*      */   public static Texture LARGE_CLOUD;
/*      */   public static Texture SMALL_CLOUD;
/*      */   public static Texture CAMPFIRE_LIT;
/*      */   public static Texture CAMPFIRE_UNLIT;
/*      */   public static Texture DIALOG_BOX_IMG;
/*      */   public static Texture SPEECH_BUBBLE_IMG;
/*      */   public static Texture SPEECH_BUBBLE_L_IMG;
/*      */   public static Texture DIALOG_DOT_IMG;
/*      */   public static Texture SHOP_SPEECH_BUBBLE_IMG;
/*      */   public static Texture WHITE_SQUARE_IMG;
/*      */   public static Texture MERCHANT_RUG_IMG;
/*      */   public static Texture MERCHANT_GLOW_IMG;
/*      */   public static Texture TP_HP;
/*      */   public static Texture TP_GOLD;
/*      */   public static Texture TP_CRYSTAL;
/*      */   public static Texture UI_GOLD;
/*      */   public static Texture SETTINGS_BACKGROUND;
/*      */   public static Texture OPTION_ABANDON;
/*      */   public static Texture OPTION_EXIT;
/*      */   public static Texture OPTION_BANNER;
/*      */   public static Texture OPTION_TOGGLE;
/*      */   public static Texture OPTION_TOGGLE_ON;
/*      */   public static Texture OPTION_SLIDER_BG;
/*      */   public static Texture OPTION_SLIDER;
/*      */   public static Texture OPTION_CONFIRM;
/*      */   public static Texture RENAME_BOX;
/*      */   public static Texture OPTION_YES;
/*      */   public static Texture OPTION_NO;
/*      */   public static Texture OPTION_VERTICAL_SLIDER;
/*      */   public static Texture SCROLL_BAR_TOP;
/*      */   public static Texture SCROLL_BAR_MIDDLE;
/*      */   public static Texture SCROLL_BAR_BOTTOM;
/*      */   public static Texture SCROLL_BAR_TRAIN;
/*      */   public static Texture INPUT_SETTINGS_BG;
/*      */   public static Texture INPUT_SETTINGS_EDGES;
/*      */   public static Texture INPUT_SETTINGS_ROW;
/*      */   public static Texture ENERGY_RED_LAYER1;
/*      */   public static Texture ENERGY_RED_LAYER2;
/*      */   public static Texture ENERGY_RED_LAYER3;
/*      */   public static Texture ENERGY_RED_LAYER4;
/*      */   public static Texture ENERGY_RED_LAYER5;
/*      */   public static Texture ENERGY_RED_LAYER6;
/*      */   public static Texture ENERGY_RED_LAYER1D;
/*      */   public static Texture ENERGY_RED_LAYER2D;
/*      */   public static Texture ENERGY_RED_LAYER3D;
/*      */   public static Texture ENERGY_RED_LAYER4D;
/*      */   public static Texture ENERGY_RED_LAYER5D;
/*      */   public static Texture ENERGY_GREEN_LAYER1;
/*      */   public static Texture ENERGY_GREEN_LAYER2;
/*      */   public static Texture ENERGY_GREEN_LAYER3;
/*      */   public static Texture ENERGY_GREEN_LAYER4;
/*      */   public static Texture ENERGY_GREEN_LAYER5;
/*      */   public static Texture ENERGY_GREEN_LAYER6;
/*      */   public static Texture ENERGY_GREEN_LAYER1D;
/*      */   public static Texture ENERGY_GREEN_LAYER2D;
/*      */   public static Texture ENERGY_GREEN_LAYER3D;
/*      */   public static Texture ENERGY_GREEN_LAYER4D;
/*      */   public static Texture ENERGY_GREEN_LAYER5D;
/*      */   public static Texture ENERGY_BLUE_LAYER1;
/*      */   public static Texture ENERGY_BLUE_LAYER2;
/*      */   public static Texture ENERGY_BLUE_LAYER3;
/*      */   public static Texture ENERGY_BLUE_LAYER4;
/*      */   public static Texture ENERGY_BLUE_LAYER5;
/*      */   public static Texture ENERGY_BLUE_LAYER6;
/*      */   public static Texture ENERGY_BLUE_LAYER1D;
/*      */   public static Texture ENERGY_BLUE_LAYER2D;
/*      */   public static Texture ENERGY_BLUE_LAYER3D;
/*      */   public static Texture ENERGY_BLUE_LAYER4D;
/*      */   public static Texture ENERGY_BLUE_LAYER5D;
/*      */   public static Texture RED_ORB_FLASH_VFX;
/*      */   public static Texture GREEN_ORB_FLASH_VFX;
/*      */   public static Texture BLUE_ORB_FLASH_VFX;
/*      */   public static Texture DIALOG_OPTION_BUTTON;
/*      */   public static Texture DOTTED_SQUARE;
/*      */   public static Texture KEYWORD_TOP;
/*      */   public static Texture KEYWORD_BODY;
/*      */   public static Texture KEYWORD_BOT;
/*      */   public static Texture FTUE;
/*      */   public static Texture FTUE_BTN;
/*      */   public static Texture CANT_USE_RELIC;
/*      */   public static Texture DYNAMIC_BTN_IMG;
/*      */   public static Texture DYNAMIC_BTN_IMG2;
/*      */   public static Texture DYNAMIC_BTN_IMG3;
/*      */   public static Texture VICTORY_BANNER;
/*      */   public static Texture REWARD_SCREEN_SHEET;
/*      */   public static Texture REWARD_SCREEN_TAKE_BUTTON;
/*      */   public static Texture REWARD_SCREEN_TAKE_USED_BUTTON;
/*      */   public static Texture REWARD_SCREEN_ITEM;
/*      */   public static Texture REWARD_CARD_NORMAL;
/*      */   public static Texture REWARD_CARD_BOSS;
/*      */   public static Texture POTION_UI_SHADOW;
/*      */   public static Texture POTION_UI_BG;
/*      */   public static Texture POTION_UI_OVERLAY;
/*      */   public static Texture POTION_UI_TOP;
/*      */   public static Texture POTION_UI_MID;
/*      */   public static Texture POTION_UI_BOT;
/*      */   public static Texture POTION_T_CONTAINER;
/*      */   public static Texture POTION_T_LIQUID;
/*      */   public static Texture POTION_T_HYBRID;
/*      */   public static Texture POTION_T_SPOTS;
/*      */   public static Texture POTION_T_OUTLINE;
/*      */   public static Texture POTION_S_CONTAINER;
/*      */   public static Texture POTION_S_LIQUID;
/*      */   public static Texture POTION_S_HYBRID;
/*      */   public static Texture POTION_S_SPOTS;
/*      */   public static Texture POTION_S_OUTLINE;
/*      */   public static Texture POTION_M_CONTAINER;
/*      */   public static Texture POTION_M_LIQUID;
/*      */   public static Texture POTION_M_HYBRID;
/*      */   public static Texture POTION_M_SPOTS;
/*      */   public static Texture POTION_M_OUTLINE;
/*      */   public static Texture POTION_SPHERE_CONTAINER;
/*      */   public static Texture POTION_SPHERE_LIQUID;
/*      */   public static Texture POTION_SPHERE_HYBRID;
/*      */   public static Texture POTION_SPHERE_SPOTS;
/*      */   public static Texture POTION_SPHERE_OUTLINE;
/*      */   public static Texture POTION_H_CONTAINER;
/*      */   public static Texture POTION_H_LIQUID;
/*      */   public static Texture POTION_H_HYBRID;
/*      */   public static Texture POTION_H_SPOTS;
/*      */   public static Texture POTION_H_OUTLINE;
/*      */   public static Texture POTION_BOTTLE_CONTAINER;
/*      */   public static Texture POTION_BOTTLE_LIQUID;
/*      */   public static Texture POTION_BOTTLE_HYBRID;
/*      */   public static Texture POTION_BOTTLE_SPOTS;
/*      */   public static Texture POTION_BOTTLE_OUTLINE;
/*      */   public static Texture POTION_HEART_CONTAINER;
/*      */   public static Texture POTION_HEART_LIQUID;
/*      */   public static Texture POTION_HEART_HYBRID;
/*      */   public static Texture POTION_HEART_SPOTS;
/*      */   public static Texture POTION_HEART_OUTLINE;
/*      */   public static Texture POTION_SNECKO_CONTAINER;
/*      */   public static Texture POTION_SNECKO_LIQUID;
/*      */   public static Texture POTION_SNECKO_HYBRID;
/*      */   public static Texture POTION_SNECKO_SPOTS;
/*      */   public static Texture POTION_SNECKO_OUTLINE;
/*      */   public static Texture POTION_FAIRY_CONTAINER;
/*      */   public static Texture POTION_FAIRY_LIQUID;
/*      */   public static Texture POTION_FAIRY_HYBRID;
/*      */   public static Texture POTION_FAIRY_SPOTS;
/*      */   public static Texture POTION_FAIRY_OUTLINE;
/*      */   public static Texture POTION_GHOST_CONTAINER;
/*      */   public static Texture POTION_GHOST_LIQUID;
/*      */   public static Texture POTION_GHOST_HYBRID;
/*      */   public static Texture POTION_GHOST_SPOTS;
/*      */   public static Texture POTION_GHOST_OUTLINE;
/*      */   public static Texture POTION_JAR_CONTAINER;
/*      */   public static Texture POTION_JAR_LIQUID;
/*      */   public static Texture POTION_JAR_HYBRID;
/*      */   public static Texture POTION_JAR_SPOTS;
/*      */   public static Texture POTION_JAR_OUTLINE;
/*      */   public static Texture POTION_BOLT_CONTAINER;
/*      */   public static Texture POTION_BOLT_LIQUID;
/*      */   public static Texture POTION_BOLT_HYBRID;
/*      */   public static Texture POTION_BOLT_SPOTS;
/*      */   public static Texture POTION_BOLT_OUTLINE;
/*      */   public static Texture POTION_CARD_CONTAINER;
/*      */   public static Texture POTION_CARD_LIQUID;
/*      */   public static Texture POTION_CARD_HYBRID;
/*      */   public static Texture POTION_CARD_SPOTS;
/*      */   public static Texture POTION_CARD_OUTLINE;
/*      */   public static Texture POTION_PLACEHOLDER;
/*      */   public static Texture POTION_SHADOW;
/*      */   private static final String BUTTON_DIR = "images/ui/topPanel/";
/*      */   public static Texture EVENT_BUTTON_ENABLED;
/*      */   public static Texture EVENT_BUTTON_DISABLED;
/*      */   public static Texture UNLOCK_TEXT_BG;
/*      */   public static Texture EVENT_IMG_FRAME;
/*      */   public static Texture EVENT_ROOM_PANEL;
/*      */   public static Texture CANCEL_BUTTON;
/*      */   public static Texture CANCEL_BUTTON_SHADOW;
/*      */   public static Texture CANCEL_BUTTON_OUTLINE;
/*      */   public static Texture CONFIRM_BUTTON;
/*      */   public static Texture CONFIRM_BUTTON_SHADOW;
/*      */   public static Texture CONFIRM_BUTTON_OUTLINE;
/*      */   public static Texture PROCEED_BUTTON;
/*      */   public static Texture PROCEED_BUTTON_SHADOW;
/*      */   public static Texture PROCEED_BUTTON_OUTLINE;
/*      */   public static Texture END_TURN_BUTTON;
/*      */   public static Texture END_TURN_BUTTON_GLOW;
/*      */   public static Texture END_TURN_HOVER;
/*      */   public static Texture SETTINGS_BUTTON_IMG;
/*      */   public static Texture SETTINGS_BUTTON_SHADOW_IMG;
/*      */   public static Texture EXIT_BUTTON_IMG;
/*      */   public static Texture GHOST_ORB_1;
/*      */   public static Texture GHOST_ORB_2;
/*      */   public static Texture DEBUG_HITBOX_IMG;
/*      */   public static Texture SOUL_HEAD_IMG;
/*      */   public static Texture MAP_TOP;
/*      */   public static Texture MAP_MID;
/*      */   public static Texture MAP_BOT;
/*      */   public static Texture MAP_BLEND;
/*      */   public static Texture MAP_NODE_PURGE;
/*      */   public static Texture MAP_NODE_ELITE;
/*      */   public static Texture MAP_NODE_ELITE_OUTLINE;
/*      */   public static Texture MAP_NODE_ENEMY;
/*      */   public static Texture MAP_NODE_ENEMY_OUTLINE;
/*      */   public static Texture MAP_NODE_SHRINE;
/*      */   public static Texture MAP_NODE_REST;
/*      */   public static Texture MAP_NODE_REST_OUTLINE;
/*      */   public static Texture MAP_NODE_TREASURE;
/*      */   public static Texture MAP_NODE_TREASURE_OUTLINE;
/*      */   public static Texture MAP_NODE_MERCHANT;
/*      */   public static Texture MAP_NODE_MERCHANT_OUTLINE;
/*      */   public static Texture MAP_LEGEND;
/*      */   public static Texture MAP_NODE_COMPLETE;
/*      */   public static Texture MAP_CIRCLE_1;
/*      */   public static Texture MAP_CIRCLE_2;
/*      */   public static Texture MAP_CIRCLE_3;
/*      */   public static Texture MAP_CIRCLE_4;
/*      */   public static Texture MAP_CIRCLE_5;
/*      */   public static Texture MAP_NODE_EVENT;
/*      */   public static Texture MAP_NODE_EVENT_OUTLINE;
/*      */   public static Texture MAP_DOT_1;
/*      */   public static Texture TINY_CARD_ATTACK;
/*      */   public static Texture TINY_CARD_SKILL;
/*      */   public static Texture TINY_CARD_POWER;
/*      */   public static Texture TINY_CARD_PORTRAIT_SHADOW;
/*      */   public static Texture TINY_CARD_BACKGROUND;
/*      */   public static Texture TINY_CARD_BANNER;
/*      */   public static Texture TINY_CARD_BANNER_SHADOW;
/*      */   public static Texture TINY_CARD_DESCRIPTION;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_BOSS;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_CHEST;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_BOSS_CHEST;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_ELITE;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_EVENT;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_MONSTER;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_REST;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_SHOP;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_UNKNOWN_CHEST;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_UNKNOWN_MONSTER;
/*      */   public static Texture RUN_HISTORY_MAP_ICON_UNKNOWN_SHOP;
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\ImageMaster.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */