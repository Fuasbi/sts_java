/*     */ package com.megacrit.cardcrawl.helpers.input;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class InputHelper
/*     */ {
/*  16 */   private static final Logger logger = LogManager.getLogger(InputHelper.class.getName());
/*     */   public static int mX;
/*  18 */   public static int mY; public static boolean isMouseDown = false; public static boolean isMouseDown_R = false;
/*  19 */   private static boolean isPrevMouseDown = false; private static boolean isPrevMouseDown_R = false;
/*  20 */   public static boolean justClickedLeft = false;
/*  21 */   public static boolean justClickedRight = false;
/*  22 */   public static boolean justReleasedClickLeft = false; public static boolean justReleasedClickRight = false;
/*  23 */   public static boolean scrolledUp = false; public static boolean scrolledDown = false;
/*  24 */   public static boolean pressedEscape = false;
/*     */   private static ScrollInputProcessor processor;
/*  26 */   public static int scrollY = 0;
/*     */   
/*     */ 
/*  29 */   private static boolean ignoreOneCycle = false;
/*     */   
/*     */   public static void initialize() {
/*  32 */     processor = new ScrollInputProcessor();
/*  33 */     Gdx.input.setInputProcessor(processor);
/*  34 */     logger.info("Setting input processor to Scroller");
/*  35 */     InputActionSet.load();
/*     */   }
/*     */   
/*     */   public static void regainInputFocus() {
/*  39 */     Gdx.input.setInputProcessor(processor);
/*     */     
/*     */ 
/*  42 */     ignoreOneCycle = true;
/*     */   }
/*     */   
/*     */   public static void updateFirst() {
/*  46 */     if (ignoreOneCycle) {
/*  47 */       ignoreOneCycle = false;
/*  48 */       return;
/*     */     }
/*     */     
/*  51 */     mX = Gdx.input.getX();
/*  52 */     if (mX > Settings.WIDTH) {
/*  53 */       mX = Settings.WIDTH;
/*  54 */     } else if (mX < 0) {
/*  55 */       mX = 0;
/*     */     }
/*  57 */     mY = Settings.HEIGHT - Gdx.input.getY();
/*  58 */     if (mY > Settings.HEIGHT) {
/*  59 */       mY = Settings.HEIGHT;
/*  60 */     } else if (mY < 0) {
/*  61 */       mY = 0;
/*     */     }
/*     */     
/*  64 */     isMouseDown = Gdx.input.isButtonPressed(0);
/*  65 */     isMouseDown_R = Gdx.input.isButtonPressed(1);
/*     */     
/*     */ 
/*  68 */     if ((!isPrevMouseDown) && (isMouseDown)) {
/*  69 */       justClickedLeft = true;
/*     */       
/*  71 */       if (Settings.isControllerMode) {
/*  72 */         leaveControllerMode();
/*     */       }
/*     */       
/*  75 */       if (Settings.isDebug) {
/*  76 */         logger.info("Clicked: (" + mX + "," + mY + ")");
/*     */       }
/*  78 */     } else if ((isPrevMouseDown) && (!isMouseDown)) {
/*  79 */       justReleasedClickLeft = true;
/*     */     }
/*     */     
/*     */ 
/*  83 */     if ((!isPrevMouseDown_R) && (isMouseDown_R)) {
/*  84 */       justClickedRight = true;
/*     */       
/*  86 */       if (Settings.isControllerMode) {
/*  87 */         leaveControllerMode();
/*     */       }
/*  89 */     } else if ((isPrevMouseDown_R) && (!isMouseDown_R)) {
/*  90 */       justReleasedClickRight = true;
/*     */     }
/*     */     
/*     */ 
/*  94 */     if (InputActionSet.cancel.isJustPressed()) {
/*  95 */       pressedEscape = true;
/*     */     } else {
/*  97 */       pressedEscape = false;
/*     */     }
/*     */     
/* 100 */     isPrevMouseDown_R = isMouseDown_R;
/* 101 */     isPrevMouseDown = isMouseDown;
/*     */   }
/*     */   
/*     */   private static void leaveControllerMode() {
/* 105 */     logger.info("LEAVING CONTROLLER MODE");
/* 106 */     Settings.isControllerMode = false;
/* 107 */     com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*     */     
/* 109 */     if (AbstractDungeon.player != null) {
/* 110 */       AbstractDungeon.player.viewingRelics = false;
/* 111 */       AbstractDungeon.topPanel.selectPotionMode = false;
/* 112 */       AbstractDungeon.player.releaseCard();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void updateLast() {
/* 117 */     justClickedLeft = false;
/* 118 */     justClickedRight = false;
/* 119 */     justReleasedClickLeft = false;
/* 120 */     scrolledUp = false;
/* 121 */     scrolledDown = false;
/*     */   }
/*     */   
/*     */   public static AbstractCard getCardSelectedByHotkey(CardGroup cards) {
/* 125 */     for (int i = 0; (i < InputActionSet.selectCardActions.length) && (i < cards.size()); i++) {
/* 126 */       if (InputActionSet.selectCardActions[i].isJustPressed()) {
/* 127 */         return (AbstractCard)cards.group.get(i);
/*     */       }
/*     */     }
/* 130 */     return null;
/*     */   }
/*     */   
/* 133 */   public static final int[] SHORTCUT_MODIFIER_KEYS = { 129, 130, 63 };
/*     */   
/*     */   public static boolean isShortcutModifierKeyPressed() {
/* 136 */     for (int keycode : SHORTCUT_MODIFIER_KEYS) {
/* 137 */       if (Gdx.input.isKeyPressed(keycode)) {
/* 138 */         return true;
/*     */       }
/*     */     }
/* 141 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isPasteJustPressed() {
/* 145 */     return (isShortcutModifierKeyPressed()) && (Gdx.input.isKeyJustPressed(50));
/*     */   }
/*     */   
/*     */   public static boolean didMoveMouse() {
/* 149 */     return (Gdx.input.getDeltaX() != 0) || (Gdx.input.getDeltaY() != 0);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\input\InputHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */