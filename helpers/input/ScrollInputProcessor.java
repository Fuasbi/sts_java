/*    */ package com.megacrit.cardcrawl.helpers.input;
/*    */ 
/*    */ import com.badlogic.gdx.InputProcessor;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class ScrollInputProcessor implements InputProcessor
/*    */ {
/* 10 */   private static final Logger logger = LogManager.getLogger(ScrollInputProcessor.class.getName());
/* 11 */   public static String lastPressed = "";
/* 12 */   public static String lastPressed2 = "";
/* 13 */   public static boolean lastPressedSwitch = true;
/*    */   
/*    */   public static void logLastPressed(String msg) {
/* 16 */     lastPressedSwitch = !lastPressedSwitch;
/* 17 */     if (lastPressedSwitch) {
/* 18 */       lastPressed = msg;
/*    */     } else {
/* 20 */       lastPressed2 = msg;
/*    */     }
/*    */     
/* 23 */     if (Settings.isInfo) {
/* 24 */       logger.info(msg);
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean keyDown(int keycode)
/*    */   {
/* 30 */     return false;
/*    */   }
/*    */   
/*    */   public boolean keyUp(int keycode)
/*    */   {
/* 35 */     return false;
/*    */   }
/*    */   
/*    */   public boolean keyTyped(char character)
/*    */   {
/* 40 */     return false;
/*    */   }
/*    */   
/*    */   public boolean touchDown(int screenX, int screenY, int pointer, int button)
/*    */   {
/* 45 */     return false;
/*    */   }
/*    */   
/*    */   public boolean touchUp(int screenX, int screenY, int pointer, int button)
/*    */   {
/* 50 */     return false;
/*    */   }
/*    */   
/*    */   public boolean touchDragged(int screenX, int screenY, int pointer)
/*    */   {
/* 55 */     return false;
/*    */   }
/*    */   
/*    */   public boolean mouseMoved(int screenX, int screenY)
/*    */   {
/* 60 */     return false;
/*    */   }
/*    */   
/*    */   public boolean scrolled(int amount)
/*    */   {
/* 65 */     if (amount == -1) {
/* 66 */       InputHelper.scrolledUp = true;
/* 67 */     } else if (amount == 1) {
/* 68 */       InputHelper.scrolledDown = true;
/*    */     }
/* 70 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\input\ScrollInputProcessor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */