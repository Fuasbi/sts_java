/*      */ package com.megacrit.cardcrawl.helpers;
/*      */ 
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*      */ import com.megacrit.cardcrawl.cards.blue.Aggregate;
/*      */ import com.megacrit.cardcrawl.cards.blue.AllForOne;
/*      */ import com.megacrit.cardcrawl.cards.blue.Amplify;
/*      */ import com.megacrit.cardcrawl.cards.blue.AutoShields;
/*      */ import com.megacrit.cardcrawl.cards.blue.BallLightning;
/*      */ import com.megacrit.cardcrawl.cards.blue.Barrage;
/*      */ import com.megacrit.cardcrawl.cards.blue.BeamCell;
/*      */ import com.megacrit.cardcrawl.cards.blue.BiasedCognition;
/*      */ import com.megacrit.cardcrawl.cards.blue.Blizzard;
/*      */ import com.megacrit.cardcrawl.cards.blue.BootSequence;
/*      */ import com.megacrit.cardcrawl.cards.blue.Buffer;
/*      */ import com.megacrit.cardcrawl.cards.blue.Capacitor;
/*      */ import com.megacrit.cardcrawl.cards.blue.Chaos;
/*      */ import com.megacrit.cardcrawl.cards.blue.Chill;
/*      */ import com.megacrit.cardcrawl.cards.blue.Claw;
/*      */ import com.megacrit.cardcrawl.cards.blue.ColdSnap;
/*      */ import com.megacrit.cardcrawl.cards.blue.CompileDriver;
/*      */ import com.megacrit.cardcrawl.cards.blue.ConserveBattery;
/*      */ import com.megacrit.cardcrawl.cards.blue.Consume;
/*      */ import com.megacrit.cardcrawl.cards.blue.Coolheaded;
/*      */ import com.megacrit.cardcrawl.cards.blue.CoreSurge;
/*      */ import com.megacrit.cardcrawl.cards.blue.CreativeAI;
/*      */ import com.megacrit.cardcrawl.cards.blue.Darkness;
/*      */ import com.megacrit.cardcrawl.cards.blue.Defend_Blue;
/*      */ import com.megacrit.cardcrawl.cards.blue.Defragment;
/*      */ import com.megacrit.cardcrawl.cards.blue.DoomAndGloom;
/*      */ import com.megacrit.cardcrawl.cards.blue.DoubleEnergy;
/*      */ import com.megacrit.cardcrawl.cards.blue.Dualcast;
/*      */ import com.megacrit.cardcrawl.cards.blue.EchoForm;
/*      */ import com.megacrit.cardcrawl.cards.blue.Electrodynamics;
/*      */ import com.megacrit.cardcrawl.cards.blue.FTL;
/*      */ import com.megacrit.cardcrawl.cards.blue.Fission;
/*      */ import com.megacrit.cardcrawl.cards.blue.ForceField;
/*      */ import com.megacrit.cardcrawl.cards.blue.Fusion;
/*      */ import com.megacrit.cardcrawl.cards.blue.GeneticAlgorithm;
/*      */ import com.megacrit.cardcrawl.cards.blue.Glacier;
/*      */ import com.megacrit.cardcrawl.cards.blue.GoForTheEyes;
/*      */ import com.megacrit.cardcrawl.cards.blue.Heatsinks;
/*      */ import com.megacrit.cardcrawl.cards.blue.HelloWorld;
/*      */ import com.megacrit.cardcrawl.cards.blue.Hologram;
/*      */ import com.megacrit.cardcrawl.cards.blue.Hyperbeam;
/*      */ import com.megacrit.cardcrawl.cards.blue.Leap;
/*      */ import com.megacrit.cardcrawl.cards.blue.LockOn;
/*      */ import com.megacrit.cardcrawl.cards.blue.Loop;
/*      */ import com.megacrit.cardcrawl.cards.blue.MachineLearning;
/*      */ import com.megacrit.cardcrawl.cards.blue.Melter;
/*      */ import com.megacrit.cardcrawl.cards.blue.MeteorStrike;
/*      */ import com.megacrit.cardcrawl.cards.blue.MultiCast;
/*      */ import com.megacrit.cardcrawl.cards.blue.Overclock;
/*      */ import com.megacrit.cardcrawl.cards.blue.Rainbow;
/*      */ import com.megacrit.cardcrawl.cards.blue.Reboot;
/*      */ import com.megacrit.cardcrawl.cards.blue.Rebound;
/*      */ import com.megacrit.cardcrawl.cards.blue.Recursion;
/*      */ import com.megacrit.cardcrawl.cards.blue.Recycle;
/*      */ import com.megacrit.cardcrawl.cards.blue.ReinforcedBody;
/*      */ import com.megacrit.cardcrawl.cards.blue.Reprogram;
/*      */ import com.megacrit.cardcrawl.cards.blue.RipAndTear;
/*      */ import com.megacrit.cardcrawl.cards.blue.Scrape;
/*      */ import com.megacrit.cardcrawl.cards.blue.Seek;
/*      */ import com.megacrit.cardcrawl.cards.blue.SelfRepair;
/*      */ import com.megacrit.cardcrawl.cards.blue.Skim;
/*      */ import com.megacrit.cardcrawl.cards.blue.Stack;
/*      */ import com.megacrit.cardcrawl.cards.blue.StaticDischarge;
/*      */ import com.megacrit.cardcrawl.cards.blue.Steam;
/*      */ import com.megacrit.cardcrawl.cards.blue.Storm;
/*      */ import com.megacrit.cardcrawl.cards.blue.Streamline;
/*      */ import com.megacrit.cardcrawl.cards.blue.Strike_Blue;
/*      */ import com.megacrit.cardcrawl.cards.blue.Sunder;
/*      */ import com.megacrit.cardcrawl.cards.blue.SweepingBeam;
/*      */ import com.megacrit.cardcrawl.cards.blue.Tempest;
/*      */ import com.megacrit.cardcrawl.cards.blue.ThunderStrike;
/*      */ import com.megacrit.cardcrawl.cards.blue.Turbo;
/*      */ import com.megacrit.cardcrawl.cards.blue.Undo;
/*      */ import com.megacrit.cardcrawl.cards.blue.WhiteNoise;
/*      */ import com.megacrit.cardcrawl.cards.blue.Zap;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Apotheosis;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Apparition;
/*      */ import com.megacrit.cardcrawl.cards.colorless.BandageUp;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Bite;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Blind;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Chrysalis;
/*      */ import com.megacrit.cardcrawl.cards.colorless.DarkShackles;
/*      */ import com.megacrit.cardcrawl.cards.colorless.DeepBreath;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Discovery;
/*      */ import com.megacrit.cardcrawl.cards.colorless.DramaticEntrance;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Enlightenment;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Finesse;
/*      */ import com.megacrit.cardcrawl.cards.colorless.FlashOfSteel;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Forethought;
/*      */ import com.megacrit.cardcrawl.cards.colorless.GoodInstincts;
/*      */ import com.megacrit.cardcrawl.cards.colorless.HandOfGreed;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Impatience;
/*      */ import com.megacrit.cardcrawl.cards.colorless.JAX;
/*      */ import com.megacrit.cardcrawl.cards.colorless.JackOfAllTrades;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Madness;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Magnetism;
/*      */ import com.megacrit.cardcrawl.cards.colorless.MasterOfStrategy;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Mayhem;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Metamorphosis;
/*      */ import com.megacrit.cardcrawl.cards.colorless.MindBlast;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Panacea;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Panache;
/*      */ import com.megacrit.cardcrawl.cards.colorless.PanicButton;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Purity;
/*      */ import com.megacrit.cardcrawl.cards.colorless.RitualDagger;
/*      */ import com.megacrit.cardcrawl.cards.colorless.SadisticNature;
/*      */ import com.megacrit.cardcrawl.cards.colorless.SecretTechnique;
/*      */ import com.megacrit.cardcrawl.cards.colorless.SecretWeapon;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Shiv;
/*      */ import com.megacrit.cardcrawl.cards.colorless.TheBomb;
/*      */ import com.megacrit.cardcrawl.cards.colorless.ThinkingAhead;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Transmutation;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Trip;
/*      */ import com.megacrit.cardcrawl.cards.colorless.Violence;
/*      */ import com.megacrit.cardcrawl.cards.curses.AscendersBane;
/*      */ import com.megacrit.cardcrawl.cards.curses.Clumsy;
/*      */ import com.megacrit.cardcrawl.cards.curses.Doubt;
/*      */ import com.megacrit.cardcrawl.cards.curses.Injury;
/*      */ import com.megacrit.cardcrawl.cards.curses.Necronomicurse;
/*      */ import com.megacrit.cardcrawl.cards.curses.Normality;
/*      */ import com.megacrit.cardcrawl.cards.curses.Pain;
/*      */ import com.megacrit.cardcrawl.cards.curses.Parasite;
/*      */ import com.megacrit.cardcrawl.cards.curses.Pride;
/*      */ import com.megacrit.cardcrawl.cards.curses.Regret;
/*      */ import com.megacrit.cardcrawl.cards.curses.Shame;
/*      */ import com.megacrit.cardcrawl.cards.curses.Writhe;
/*      */ import com.megacrit.cardcrawl.cards.green.AThousandCuts;
/*      */ import com.megacrit.cardcrawl.cards.green.Accuracy;
/*      */ import com.megacrit.cardcrawl.cards.green.Acrobatics;
/*      */ import com.megacrit.cardcrawl.cards.green.Adrenaline;
/*      */ import com.megacrit.cardcrawl.cards.green.AfterImage;
/*      */ import com.megacrit.cardcrawl.cards.green.Alchemize;
/*      */ import com.megacrit.cardcrawl.cards.green.AllOutAttack;
/*      */ import com.megacrit.cardcrawl.cards.green.Backflip;
/*      */ import com.megacrit.cardcrawl.cards.green.Backstab;
/*      */ import com.megacrit.cardcrawl.cards.green.Bane;
/*      */ import com.megacrit.cardcrawl.cards.green.BladeDance;
/*      */ import com.megacrit.cardcrawl.cards.green.Blur;
/*      */ import com.megacrit.cardcrawl.cards.green.BouncingFlask;
/*      */ import com.megacrit.cardcrawl.cards.green.BulletTime;
/*      */ import com.megacrit.cardcrawl.cards.green.Burst;
/*      */ import com.megacrit.cardcrawl.cards.green.CalculatedGamble;
/*      */ import com.megacrit.cardcrawl.cards.green.Caltrops;
/*      */ import com.megacrit.cardcrawl.cards.green.Catalyst;
/*      */ import com.megacrit.cardcrawl.cards.green.Choke;
/*      */ import com.megacrit.cardcrawl.cards.green.CloakAndDagger;
/*      */ import com.megacrit.cardcrawl.cards.green.Concentrate;
/*      */ import com.megacrit.cardcrawl.cards.green.CorpseExplosion;
/*      */ import com.megacrit.cardcrawl.cards.green.CripplingPoison;
/*      */ import com.megacrit.cardcrawl.cards.green.DaggerSpray;
/*      */ import com.megacrit.cardcrawl.cards.green.DaggerThrow;
/*      */ import com.megacrit.cardcrawl.cards.green.Dash;
/*      */ import com.megacrit.cardcrawl.cards.green.DeadlyPoison;
/*      */ import com.megacrit.cardcrawl.cards.green.Defend_Green;
/*      */ import com.megacrit.cardcrawl.cards.green.Deflect;
/*      */ import com.megacrit.cardcrawl.cards.green.DieDieDie;
/*      */ import com.megacrit.cardcrawl.cards.green.Distraction;
/*      */ import com.megacrit.cardcrawl.cards.green.DodgeAndRoll;
/*      */ import com.megacrit.cardcrawl.cards.green.Doppelganger;
/*      */ import com.megacrit.cardcrawl.cards.green.EndlessAgony;
/*      */ import com.megacrit.cardcrawl.cards.green.Envenom;
/*      */ import com.megacrit.cardcrawl.cards.green.EscapePlan;
/*      */ import com.megacrit.cardcrawl.cards.green.Eviscerate;
/*      */ import com.megacrit.cardcrawl.cards.green.Expertise;
/*      */ import com.megacrit.cardcrawl.cards.green.Finisher;
/*      */ import com.megacrit.cardcrawl.cards.green.Flechettes;
/*      */ import com.megacrit.cardcrawl.cards.green.FlyingKnee;
/*      */ import com.megacrit.cardcrawl.cards.green.Footwork;
/*      */ import com.megacrit.cardcrawl.cards.green.GlassKnife;
/*      */ import com.megacrit.cardcrawl.cards.green.GrandFinale;
/*      */ import com.megacrit.cardcrawl.cards.green.HeelHook;
/*      */ import com.megacrit.cardcrawl.cards.green.InfiniteBlades;
/*      */ import com.megacrit.cardcrawl.cards.green.LegSweep;
/*      */ import com.megacrit.cardcrawl.cards.green.Malaise;
/*      */ import com.megacrit.cardcrawl.cards.green.MasterfulStab;
/*      */ import com.megacrit.cardcrawl.cards.green.Neutralize;
/*      */ import com.megacrit.cardcrawl.cards.green.Nightmare;
/*      */ import com.megacrit.cardcrawl.cards.green.NoxiousFumes;
/*      */ import com.megacrit.cardcrawl.cards.green.Outmaneuver;
/*      */ import com.megacrit.cardcrawl.cards.green.PhantasmalKiller;
/*      */ import com.megacrit.cardcrawl.cards.green.PiercingWail;
/*      */ import com.megacrit.cardcrawl.cards.green.PoisonedStab;
/*      */ import com.megacrit.cardcrawl.cards.green.Predator;
/*      */ import com.megacrit.cardcrawl.cards.green.Prepared;
/*      */ import com.megacrit.cardcrawl.cards.green.QuickSlash;
/*      */ import com.megacrit.cardcrawl.cards.green.Reflex;
/*      */ import com.megacrit.cardcrawl.cards.green.RiddleWithHoles;
/*      */ import com.megacrit.cardcrawl.cards.green.Setup;
/*      */ import com.megacrit.cardcrawl.cards.green.Skewer;
/*      */ import com.megacrit.cardcrawl.cards.green.Slice;
/*      */ import com.megacrit.cardcrawl.cards.green.StormOfSteel;
/*      */ import com.megacrit.cardcrawl.cards.green.Strike_Green;
/*      */ import com.megacrit.cardcrawl.cards.green.SuckerPunch;
/*      */ import com.megacrit.cardcrawl.cards.green.Survivor;
/*      */ import com.megacrit.cardcrawl.cards.green.Tactician;
/*      */ import com.megacrit.cardcrawl.cards.green.Terror;
/*      */ import com.megacrit.cardcrawl.cards.green.ToolsOfTheTrade;
/*      */ import com.megacrit.cardcrawl.cards.green.UnderhandedStrike;
/*      */ import com.megacrit.cardcrawl.cards.green.Unload;
/*      */ import com.megacrit.cardcrawl.cards.green.WellLaidPlans;
/*      */ import com.megacrit.cardcrawl.cards.green.WraithForm;
/*      */ import com.megacrit.cardcrawl.cards.red.Anger;
/*      */ import com.megacrit.cardcrawl.cards.red.Armaments;
/*      */ import com.megacrit.cardcrawl.cards.red.Barricade;
/*      */ import com.megacrit.cardcrawl.cards.red.Bash;
/*      */ import com.megacrit.cardcrawl.cards.red.BattleTrance;
/*      */ import com.megacrit.cardcrawl.cards.red.Berserk;
/*      */ import com.megacrit.cardcrawl.cards.red.BloodForBlood;
/*      */ import com.megacrit.cardcrawl.cards.red.Bloodletting;
/*      */ import com.megacrit.cardcrawl.cards.red.Bludgeon;
/*      */ import com.megacrit.cardcrawl.cards.red.BodySlam;
/*      */ import com.megacrit.cardcrawl.cards.red.Brutality;
/*      */ import com.megacrit.cardcrawl.cards.red.BurningPact;
/*      */ import com.megacrit.cardcrawl.cards.red.Carnage;
/*      */ import com.megacrit.cardcrawl.cards.red.Clash;
/*      */ import com.megacrit.cardcrawl.cards.red.Cleave;
/*      */ import com.megacrit.cardcrawl.cards.red.Clothesline;
/*      */ import com.megacrit.cardcrawl.cards.red.Combust;
/*      */ import com.megacrit.cardcrawl.cards.red.Corruption;
/*      */ import com.megacrit.cardcrawl.cards.red.DarkEmbrace;
/*      */ import com.megacrit.cardcrawl.cards.red.Defend_Red;
/*      */ import com.megacrit.cardcrawl.cards.red.DemonForm;
/*      */ import com.megacrit.cardcrawl.cards.red.Disarm;
/*      */ import com.megacrit.cardcrawl.cards.red.DoubleTap;
/*      */ import com.megacrit.cardcrawl.cards.red.Dropkick;
/*      */ import com.megacrit.cardcrawl.cards.red.DualWield;
/*      */ import com.megacrit.cardcrawl.cards.red.Entrench;
/*      */ import com.megacrit.cardcrawl.cards.red.Evolve;
/*      */ import com.megacrit.cardcrawl.cards.red.Exhume;
/*      */ import com.megacrit.cardcrawl.cards.red.Feed;
/*      */ import com.megacrit.cardcrawl.cards.red.FeelNoPain;
/*      */ import com.megacrit.cardcrawl.cards.red.FiendFire;
/*      */ import com.megacrit.cardcrawl.cards.red.FireBreathing;
/*      */ import com.megacrit.cardcrawl.cards.red.FlameBarrier;
/*      */ import com.megacrit.cardcrawl.cards.red.Flex;
/*      */ import com.megacrit.cardcrawl.cards.red.GhostlyArmor;
/*      */ import com.megacrit.cardcrawl.cards.red.Havoc;
/*      */ import com.megacrit.cardcrawl.cards.red.Headbutt;
/*      */ import com.megacrit.cardcrawl.cards.red.HeavyBlade;
/*      */ import com.megacrit.cardcrawl.cards.red.Hemokinesis;
/*      */ import com.megacrit.cardcrawl.cards.red.Immolate;
/*      */ import com.megacrit.cardcrawl.cards.red.Impervious;
/*      */ import com.megacrit.cardcrawl.cards.red.InfernalBlade;
/*      */ import com.megacrit.cardcrawl.cards.red.Inflame;
/*      */ import com.megacrit.cardcrawl.cards.red.Intimidate;
/*      */ import com.megacrit.cardcrawl.cards.red.IronWave;
/*      */ import com.megacrit.cardcrawl.cards.red.Juggernaut;
/*      */ import com.megacrit.cardcrawl.cards.red.LimitBreak;
/*      */ import com.megacrit.cardcrawl.cards.red.Metallicize;
/*      */ import com.megacrit.cardcrawl.cards.red.Offering;
/*      */ import com.megacrit.cardcrawl.cards.red.PerfectedStrike;
/*      */ import com.megacrit.cardcrawl.cards.red.PommelStrike;
/*      */ import com.megacrit.cardcrawl.cards.red.PowerThrough;
/*      */ import com.megacrit.cardcrawl.cards.red.Pummel;
/*      */ import com.megacrit.cardcrawl.cards.red.Rage;
/*      */ import com.megacrit.cardcrawl.cards.red.Rampage;
/*      */ import com.megacrit.cardcrawl.cards.red.Reaper;
/*      */ import com.megacrit.cardcrawl.cards.red.RecklessCharge;
/*      */ import com.megacrit.cardcrawl.cards.red.Rupture;
/*      */ import com.megacrit.cardcrawl.cards.red.SearingBlow;
/*      */ import com.megacrit.cardcrawl.cards.red.SecondWind;
/*      */ import com.megacrit.cardcrawl.cards.red.SeeingRed;
/*      */ import com.megacrit.cardcrawl.cards.red.Sentinel;
/*      */ import com.megacrit.cardcrawl.cards.red.SeverSoul;
/*      */ import com.megacrit.cardcrawl.cards.red.Shockwave;
/*      */ import com.megacrit.cardcrawl.cards.red.ShrugItOff;
/*      */ import com.megacrit.cardcrawl.cards.red.SpotWeakness;
/*      */ import com.megacrit.cardcrawl.cards.red.Strike_Red;
/*      */ import com.megacrit.cardcrawl.cards.red.SwordBoomerang;
/*      */ import com.megacrit.cardcrawl.cards.red.ThunderClap;
/*      */ import com.megacrit.cardcrawl.cards.red.TrueGrit;
/*      */ import com.megacrit.cardcrawl.cards.red.TwinStrike;
/*      */ import com.megacrit.cardcrawl.cards.red.Uppercut;
/*      */ import com.megacrit.cardcrawl.cards.red.Warcry;
/*      */ import com.megacrit.cardcrawl.cards.red.Whirlwind;
/*      */ import com.megacrit.cardcrawl.cards.red.WildStrike;
/*      */ import com.megacrit.cardcrawl.cards.status.Burn;
/*      */ import com.megacrit.cardcrawl.cards.status.Dazed;
/*      */ import com.megacrit.cardcrawl.cards.status.Slimed;
/*      */ import com.megacrit.cardcrawl.cards.status.Void;
/*      */ import com.megacrit.cardcrawl.cards.status.Wound;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.metrics.BotDataUploader;
/*      */ import com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType;
/*      */ import com.megacrit.cardcrawl.random.Random;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import java.io.PrintStream;
/*      */ import java.util.ArrayList;
/*      */ import java.util.HashMap;
/*      */ import java.util.Map.Entry;
/*      */ import java.util.Objects;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class CardLibrary
/*      */ {
/*  307 */   private static final Logger logger = LogManager.getLogger(CardLibrary.class.getName());
/*  308 */   public static int totalCardCount = 0;
/*  309 */   public static HashMap<String, AbstractCard> cards = new HashMap();
/*  310 */   private static HashMap<String, AbstractCard> curses = new HashMap();
/*  311 */   public static int redCards = 0; public static int greenCards = 0; public static int blueCards = 0; public static int colorlessCards = 0; public static int curseCards = 0;
/*  312 */   public static int seenRedCards = 0; public static int seenGreenCards = 0; public static int seenBlueCards = 0; public static int seenColorlessCards = 0;
/*  313 */   public static int seenCurseCards = 0;
/*      */   
/*      */   public static enum LibraryType {
/*  316 */     RED,  GREEN,  BLUE,  CURSE,  COLORLESS;
/*      */     
/*      */     private LibraryType() {}
/*      */   }
/*      */   
/*      */   public static void initialize()
/*      */   {
/*  323 */     long startTime = System.currentTimeMillis();
/*      */     
/*  325 */     addRedCards();
/*  326 */     addGreenCards();
/*  327 */     addBlueCards();
/*  328 */     addColorlessCards();
/*  329 */     addCurseCards();
/*      */     
/*  331 */     if (Settings.isDev) {}
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  337 */     logger.info("Card load time: " + (
/*  338 */       System.currentTimeMillis() - startTime) + "ms with " + cards.size() + " cards");
/*      */     
/*  340 */     if (Settings.isDev) {
/*  341 */       logger.info("[INFO] Red Cards: \t" + redCards);
/*  342 */       logger.info("[INFO] Green Cards: \t" + greenCards);
/*  343 */       logger.info("[INFO] Blue Cards: \t" + blueCards);
/*  344 */       logger.info("[INFO] Colorless Cards: \t" + colorlessCards);
/*  345 */       logger.info("[INFO] Curse Cards: \t" + curseCards);
/*  346 */       logger.info("[INFO] Total Cards: \t" + (redCards + greenCards + blueCards + colorlessCards + curseCards));
/*      */     }
/*      */   }
/*      */   
/*      */   public static void resetForReload() {
/*  351 */     cards = new HashMap();
/*  352 */     curses = new HashMap();
/*  353 */     totalCardCount = 0;
/*  354 */     redCards = 0;
/*  355 */     greenCards = 0;
/*  356 */     blueCards = 0;
/*  357 */     colorlessCards = 0;
/*  358 */     curseCards = 0;
/*  359 */     seenRedCards = 0;
/*  360 */     seenGreenCards = 0;
/*  361 */     seenBlueCards = 0;
/*  362 */     seenColorlessCards = 0;
/*  363 */     seenCurseCards = 0;
/*      */   }
/*      */   
/*      */   private static void addRedCards() {
/*  367 */     add(new Anger());
/*  368 */     add(new Armaments());
/*  369 */     add(new Barricade());
/*  370 */     add(new Bash());
/*  371 */     add(new BattleTrance());
/*  372 */     add(new Berserk());
/*  373 */     add(new BloodForBlood());
/*  374 */     add(new Bloodletting());
/*  375 */     add(new Bludgeon());
/*  376 */     add(new BodySlam());
/*  377 */     add(new Brutality());
/*  378 */     add(new BurningPact());
/*  379 */     add(new Carnage());
/*  380 */     add(new Clash());
/*  381 */     add(new Cleave());
/*  382 */     add(new Clothesline());
/*  383 */     add(new Combust());
/*  384 */     add(new Corruption());
/*  385 */     add(new DarkEmbrace());
/*  386 */     add(new Defend_Red());
/*  387 */     add(new DemonForm());
/*  388 */     add(new Disarm());
/*  389 */     add(new DoubleTap());
/*  390 */     add(new Dropkick());
/*  391 */     add(new DualWield());
/*  392 */     add(new Entrench());
/*  393 */     add(new Evolve());
/*  394 */     add(new Exhume());
/*  395 */     add(new Feed());
/*  396 */     add(new FeelNoPain());
/*  397 */     add(new FiendFire());
/*  398 */     add(new FireBreathing());
/*  399 */     add(new FlameBarrier());
/*  400 */     add(new Flex());
/*  401 */     add(new GhostlyArmor());
/*  402 */     add(new Havoc());
/*  403 */     add(new Headbutt());
/*  404 */     add(new HeavyBlade());
/*  405 */     add(new Hemokinesis());
/*  406 */     add(new Immolate());
/*  407 */     add(new Impervious());
/*  408 */     add(new InfernalBlade());
/*  409 */     add(new Inflame());
/*  410 */     add(new Intimidate());
/*  411 */     add(new IronWave());
/*  412 */     add(new Juggernaut());
/*  413 */     add(new LimitBreak());
/*  414 */     add(new Metallicize());
/*  415 */     add(new Offering());
/*  416 */     add(new PerfectedStrike());
/*  417 */     add(new PommelStrike());
/*  418 */     add(new PowerThrough());
/*  419 */     add(new Pummel());
/*  420 */     add(new Rage());
/*  421 */     add(new Rampage());
/*  422 */     add(new Reaper());
/*  423 */     add(new RecklessCharge());
/*  424 */     add(new Rupture());
/*  425 */     add(new SearingBlow());
/*  426 */     add(new SecondWind());
/*  427 */     add(new SeeingRed());
/*  428 */     add(new Sentinel());
/*  429 */     add(new SeverSoul());
/*  430 */     add(new Shockwave());
/*  431 */     add(new ShrugItOff());
/*  432 */     add(new SpotWeakness());
/*  433 */     add(new Strike_Red());
/*  434 */     add(new SwordBoomerang());
/*  435 */     add(new ThunderClap());
/*  436 */     add(new TrueGrit());
/*  437 */     add(new TwinStrike());
/*  438 */     add(new Uppercut());
/*  439 */     add(new Warcry());
/*  440 */     add(new Whirlwind());
/*  441 */     add(new WildStrike());
/*      */   }
/*      */   
/*      */   private static void addGreenCards() {
/*  445 */     add(new Accuracy());
/*  446 */     add(new Acrobatics());
/*  447 */     add(new Adrenaline());
/*  448 */     add(new AfterImage());
/*  449 */     add(new Alchemize());
/*  450 */     add(new AllOutAttack());
/*  451 */     add(new AThousandCuts());
/*  452 */     add(new Backflip());
/*  453 */     add(new Backstab());
/*  454 */     add(new Bane());
/*  455 */     add(new BladeDance());
/*  456 */     add(new Blur());
/*  457 */     add(new BouncingFlask());
/*  458 */     add(new BulletTime());
/*  459 */     add(new Burst());
/*  460 */     add(new CalculatedGamble());
/*  461 */     add(new Caltrops());
/*  462 */     add(new Catalyst());
/*  463 */     add(new Choke());
/*  464 */     add(new CloakAndDagger());
/*  465 */     add(new Concentrate());
/*  466 */     add(new CorpseExplosion());
/*  467 */     add(new CripplingPoison());
/*  468 */     add(new DaggerSpray());
/*  469 */     add(new DaggerThrow());
/*  470 */     add(new Dash());
/*  471 */     add(new DeadlyPoison());
/*  472 */     add(new Defend_Green());
/*  473 */     add(new Deflect());
/*  474 */     add(new DieDieDie());
/*  475 */     add(new Distraction());
/*  476 */     add(new DodgeAndRoll());
/*  477 */     add(new Doppelganger());
/*  478 */     add(new EndlessAgony());
/*  479 */     add(new Envenom());
/*  480 */     add(new EscapePlan());
/*  481 */     add(new Eviscerate());
/*  482 */     add(new Expertise());
/*  483 */     add(new Finisher());
/*  484 */     add(new Flechettes());
/*  485 */     add(new FlyingKnee());
/*  486 */     add(new Footwork());
/*  487 */     add(new GlassKnife());
/*  488 */     add(new GrandFinale());
/*  489 */     add(new HeelHook());
/*  490 */     add(new InfiniteBlades());
/*  491 */     add(new LegSweep());
/*  492 */     add(new Malaise());
/*  493 */     add(new MasterfulStab());
/*  494 */     add(new Neutralize());
/*  495 */     add(new Nightmare());
/*  496 */     add(new NoxiousFumes());
/*  497 */     add(new Outmaneuver());
/*  498 */     add(new PhantasmalKiller());
/*  499 */     add(new PiercingWail());
/*  500 */     add(new PoisonedStab());
/*  501 */     add(new Predator());
/*  502 */     add(new Prepared());
/*  503 */     add(new QuickSlash());
/*  504 */     add(new Reflex());
/*  505 */     add(new RiddleWithHoles());
/*  506 */     add(new Setup());
/*  507 */     add(new Skewer());
/*  508 */     add(new Slice());
/*  509 */     add(new StormOfSteel());
/*  510 */     add(new Strike_Green());
/*  511 */     add(new SuckerPunch());
/*  512 */     add(new Survivor());
/*  513 */     add(new Tactician());
/*  514 */     add(new Terror());
/*  515 */     add(new ToolsOfTheTrade());
/*  516 */     add(new UnderhandedStrike());
/*  517 */     add(new Unload());
/*  518 */     add(new WellLaidPlans());
/*  519 */     add(new WraithForm());
/*      */   }
/*      */   
/*      */   private static void addBlueCards() {
/*  523 */     add(new Aggregate());
/*  524 */     add(new AllForOne());
/*  525 */     add(new Amplify());
/*  526 */     add(new AutoShields());
/*  527 */     add(new BallLightning());
/*  528 */     add(new Barrage());
/*  529 */     add(new BeamCell());
/*  530 */     add(new BiasedCognition());
/*  531 */     add(new Blizzard());
/*  532 */     add(new BootSequence());
/*  533 */     add(new Buffer());
/*  534 */     add(new Capacitor());
/*  535 */     add(new Chaos());
/*  536 */     add(new Chill());
/*  537 */     add(new Claw());
/*  538 */     add(new ColdSnap());
/*  539 */     add(new CompileDriver());
/*  540 */     add(new ConserveBattery());
/*  541 */     add(new Consume());
/*  542 */     add(new Coolheaded());
/*  543 */     add(new CoreSurge());
/*  544 */     add(new CreativeAI());
/*  545 */     add(new Darkness());
/*  546 */     add(new Defend_Blue());
/*  547 */     add(new Defragment());
/*  548 */     add(new DoomAndGloom());
/*  549 */     add(new DoubleEnergy());
/*  550 */     add(new Dualcast());
/*  551 */     add(new EchoForm());
/*  552 */     add(new Electrodynamics());
/*  553 */     add(new Fission());
/*  554 */     add(new ForceField());
/*  555 */     add(new FTL());
/*  556 */     add(new Fusion());
/*  557 */     add(new GeneticAlgorithm());
/*  558 */     add(new Glacier());
/*  559 */     add(new GoForTheEyes());
/*  560 */     add(new Heatsinks());
/*  561 */     add(new HelloWorld());
/*  562 */     add(new Hologram());
/*  563 */     add(new Hyperbeam());
/*  564 */     add(new Leap());
/*  565 */     add(new LockOn());
/*  566 */     add(new Loop());
/*  567 */     add(new MachineLearning());
/*  568 */     add(new Melter());
/*  569 */     add(new MeteorStrike());
/*  570 */     add(new MultiCast());
/*  571 */     add(new Overclock());
/*  572 */     add(new Rainbow());
/*  573 */     add(new Reboot());
/*  574 */     add(new Rebound());
/*  575 */     add(new Recursion());
/*  576 */     add(new Recycle());
/*  577 */     add(new ReinforcedBody());
/*  578 */     add(new Reprogram());
/*  579 */     add(new RipAndTear());
/*  580 */     add(new Scrape());
/*  581 */     add(new Seek());
/*  582 */     add(new SelfRepair());
/*  583 */     add(new Skim());
/*  584 */     add(new Stack());
/*  585 */     add(new StaticDischarge());
/*  586 */     add(new Steam());
/*  587 */     add(new Storm());
/*  588 */     add(new Streamline());
/*  589 */     add(new Strike_Blue());
/*  590 */     add(new Sunder());
/*  591 */     add(new SweepingBeam());
/*  592 */     add(new Tempest());
/*  593 */     add(new ThunderStrike());
/*  594 */     add(new Turbo());
/*  595 */     add(new Undo());
/*  596 */     add(new WhiteNoise());
/*  597 */     add(new Zap());
/*      */   }
/*      */   
/*      */   private static void printBlueCards(AbstractCard.CardColor color)
/*      */   {
/*  602 */     for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  603 */       if (((AbstractCard)c.getValue()).color == color) {
/*  604 */         AbstractCard card = (AbstractCard)c.getValue();
/*  605 */         System.out.println(card.originalName + "; " + card.type
/*  606 */           .toString() + "; " + card.rarity.toString() + "; " + card.cost + "; " + card.rawDescription);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private static void addColorlessCards()
/*      */   {
/*  613 */     add(new Apotheosis());
/*  614 */     add(new Apparition());
/*  615 */     add(new BandageUp());
/*  616 */     add(new Bite());
/*  617 */     add(new Blind());
/*  618 */     add(new Chrysalis());
/*  619 */     add(new DarkShackles());
/*  620 */     add(new DeepBreath());
/*  621 */     add(new Discovery());
/*  622 */     add(new DramaticEntrance());
/*  623 */     add(new Enlightenment());
/*  624 */     add(new Finesse());
/*  625 */     add(new FlashOfSteel());
/*  626 */     add(new Forethought());
/*  627 */     add(new GoodInstincts());
/*  628 */     add(new HandOfGreed());
/*  629 */     add(new Impatience());
/*  630 */     add(new JackOfAllTrades());
/*  631 */     add(new JAX());
/*  632 */     add(new Madness());
/*  633 */     add(new Magnetism());
/*  634 */     add(new MasterOfStrategy());
/*  635 */     add(new Mayhem());
/*  636 */     add(new Metamorphosis());
/*  637 */     add(new MindBlast());
/*  638 */     add(new Panacea());
/*  639 */     add(new Panache());
/*  640 */     add(new PanicButton());
/*  641 */     add(new Purity());
/*  642 */     add(new RitualDagger());
/*  643 */     add(new SadisticNature());
/*  644 */     add(new SecretTechnique());
/*  645 */     add(new SecretWeapon());
/*  646 */     add(new Shiv());
/*  647 */     add(new com.megacrit.cardcrawl.cards.colorless.SwiftStrike());
/*  648 */     add(new TheBomb());
/*  649 */     add(new ThinkingAhead());
/*  650 */     add(new Transmutation());
/*  651 */     add(new Trip());
/*  652 */     add(new Violence());
/*      */     
/*      */ 
/*  655 */     add(new Burn());
/*  656 */     add(new Dazed());
/*  657 */     add(new Slimed());
/*  658 */     add(new Void());
/*  659 */     add(new Wound());
/*      */   }
/*      */   
/*      */   private static void addCurseCards() {
/*  663 */     add(new AscendersBane());
/*  664 */     add(new Clumsy());
/*  665 */     add(new com.megacrit.cardcrawl.cards.curses.Decay());
/*  666 */     add(new Doubt());
/*  667 */     add(new Injury());
/*  668 */     add(new Necronomicurse());
/*  669 */     add(new Normality());
/*  670 */     add(new Pain());
/*  671 */     add(new Parasite());
/*  672 */     add(new Pride());
/*  673 */     add(new Regret());
/*  674 */     add(new Shame());
/*  675 */     add(new Writhe());
/*      */   }
/*      */   
/*      */   private static void removeNonFinalizedCards()
/*      */   {
/*  680 */     ArrayList<String> toRemove = new ArrayList();
/*  681 */     for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  682 */       if (((AbstractCard)c.getValue()).assetURL == null) {
/*  683 */         toRemove.add(c.getKey());
/*      */       }
/*      */     }
/*      */     
/*  687 */     for (String s : toRemove) {
/*  688 */       logger.info("Removing Card " + s + " for trailer build.");
/*  689 */       cards.remove(s);
/*      */     }
/*  691 */     toRemove.clear();
/*      */     
/*  693 */     for (Map.Entry<String, AbstractCard> c : curses.entrySet()) {
/*  694 */       if (((AbstractCard)c.getValue()).assetURL == null) {
/*  695 */         toRemove.add(c.getKey());
/*      */       }
/*      */     }
/*      */     
/*  699 */     for (String s : toRemove) {
/*  700 */       logger.info("Removing Curse " + s + " for trailer build.");
/*  701 */       curses.remove(s);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void unlockAndSeeAllCards()
/*      */   {
/*  710 */     for (String s : UnlockTracker.lockedCards) {
/*  711 */       UnlockTracker.hardUnlockOverride(s);
/*      */     }
/*      */     
/*      */ 
/*  715 */     for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  716 */       if ((((AbstractCard)c.getValue()).rarity != AbstractCard.CardRarity.BASIC) && 
/*  717 */         (!UnlockTracker.isCardSeen((String)c.getKey()))) {
/*  718 */         UnlockTracker.markCardAsSeen((String)c.getKey());
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  723 */     for (Map.Entry<String, AbstractCard> c : curses.entrySet()) {
/*  724 */       if (!UnlockTracker.isCardSeen((String)c.getKey())) {
/*  725 */         UnlockTracker.markCardAsSeen((String)c.getKey());
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void add(AbstractCard card)
/*      */   {
/*  736 */     switch (card.color) {
/*      */     case RED: 
/*  738 */       redCards += 1;
/*  739 */       if (UnlockTracker.isCardSeen(card.cardID)) {
/*  740 */         seenRedCards += 1;
/*      */       }
/*      */       break;
/*      */     case GREEN: 
/*  744 */       greenCards += 1;
/*  745 */       if (UnlockTracker.isCardSeen(card.cardID)) {
/*  746 */         seenGreenCards += 1;
/*      */       }
/*      */       break;
/*      */     case BLUE: 
/*  750 */       blueCards += 1;
/*  751 */       if (UnlockTracker.isCardSeen(card.cardID)) {
/*  752 */         seenBlueCards += 1;
/*      */       }
/*      */       break;
/*      */     case COLORLESS: 
/*  756 */       colorlessCards += 1;
/*  757 */       if (UnlockTracker.isCardSeen(card.cardID)) {
/*  758 */         seenColorlessCards += 1;
/*      */       }
/*      */       break;
/*      */     case CURSE: 
/*  762 */       curseCards += 1;
/*  763 */       if (UnlockTracker.isCardSeen(card.cardID)) {
/*  764 */         seenCurseCards += 1;
/*      */       }
/*  766 */       curses.put(card.cardID, card);
/*  767 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  772 */     if (!UnlockTracker.isCardSeen(card.cardID)) {
/*  773 */       card.isSeen = false;
/*      */     }
/*  775 */     cards.put(card.cardID, card);
/*  776 */     card.initializeDescription();
/*  777 */     totalCardCount += 1;
/*      */   }
/*      */   
/*      */   public static AbstractCard getCopy(String key, int upgradeTime, int misc) {
/*  781 */     AbstractCard source = getCard(key);
/*  782 */     AbstractCard retVal = null;
/*      */     
/*  784 */     if (source == null) {
/*  785 */       retVal = getCard("Madness").makeCopy();
/*      */     } else {
/*  787 */       retVal = getCard(key).makeCopy();
/*      */     }
/*      */     
/*  790 */     for (int i = 0; i < upgradeTime; i++) {
/*  791 */       retVal.upgrade();
/*      */     }
/*      */     
/*  794 */     retVal.misc = misc;
/*  795 */     if (misc != 0) {
/*  796 */       if (retVal.cardID.equals("Genetic Algorithm")) {
/*  797 */         retVal.block = misc;
/*  798 */         retVal.baseBlock = misc;
/*  799 */         retVal.initializeDescription();
/*      */       }
/*  801 */       if (retVal.cardID.equals("RitualDagger")) {
/*  802 */         retVal.damage = misc;
/*  803 */         retVal.baseDamage = misc;
/*  804 */         retVal.initializeDescription();
/*      */       }
/*      */     }
/*  807 */     return retVal;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard getCopy(String key)
/*      */   {
/*  817 */     return getCard(key).makeCopy();
/*      */   }
/*      */   
/*      */   public static AbstractCard getCard(AbstractPlayer.PlayerClass plyrClass, String key) {
/*  821 */     return (AbstractCard)cards.get(key);
/*      */   }
/*      */   
/*      */   public static AbstractCard getCard(String key) {
/*  825 */     return (AbstractCard)cards.get(key);
/*      */   }
/*      */   
/*      */   public static String getCardNameFromKey(String key)
/*      */   {
/*  830 */     AbstractCard card = (AbstractCard)cards.getOrDefault(key, null);
/*  831 */     if (card != null) {
/*  832 */       return card.originalName;
/*      */     }
/*  834 */     return null;
/*      */   }
/*      */   
/*      */   public static String getCardNameFromMetricID(String metricID) {
/*  838 */     String[] components = metricID.split("\\+");
/*      */     
/*  840 */     String baseId = components[0];
/*  841 */     AbstractCard card = (AbstractCard)cards.getOrDefault(baseId, null);
/*  842 */     if (card == null) {
/*  843 */       return metricID;
/*      */     }
/*      */     try {
/*  846 */       if (components.length > 1) {
/*  847 */         card = card.makeCopy();
/*  848 */         int upgrades = Integer.parseInt(components[1]);
/*  849 */         for (int i = 0; i < upgrades; i++) {
/*  850 */           card.upgrade();
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (IndexOutOfBoundsException|NumberFormatException localIndexOutOfBoundsException) {}
/*      */     
/*  856 */     return card.name;
/*      */   }
/*      */   
/*      */   public static boolean isACard(String metricID) {
/*  860 */     String[] components = metricID.split("\\+");
/*  861 */     String baseId = components[0];
/*  862 */     AbstractCard card = (AbstractCard)cards.getOrDefault(baseId, null);
/*  863 */     return card != null;
/*      */   }
/*      */   
/*      */   public static AbstractCard getCurse() {
/*  867 */     ArrayList<String> tmp = new ArrayList();
/*  868 */     for (Map.Entry<String, AbstractCard> c : curses.entrySet()) {
/*  869 */       if ((!((AbstractCard)c.getValue()).cardID.equals("AscendersBane")) && (!((AbstractCard)c.getValue()).cardID.equals("Necronomicurse")) && 
/*  870 */         (!((AbstractCard)c.getValue()).cardID.equals("Pride"))) {
/*  871 */         tmp.add(c.getKey());
/*      */       }
/*      */     }
/*      */     
/*  875 */     return (AbstractCard)cards.get(tmp.get(AbstractDungeon.cardRng.random(0, tmp.size() - 1)));
/*      */   }
/*      */   
/*      */   public static AbstractCard getColorSpecificCard(AbstractCard.CardColor color, Random rand) {
/*  879 */     ArrayList<String> tmp = new ArrayList();
/*  880 */     switch (color) {
/*      */     case RED: 
/*  882 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  883 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.RED) {
/*  884 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*  887 */       break;
/*      */     case GREEN: 
/*  889 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  890 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.GREEN) {
/*  891 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*  894 */       break;
/*      */     }
/*      */     
/*      */     
/*  898 */     return (AbstractCard)cards.get(tmp.get(rand.random(0, tmp.size() - 1)));
/*      */   }
/*      */   
/*      */   public static AbstractCard getColorSpecificCard(AbstractPlayer.PlayerClass chosenclass, Random rand) {
/*  902 */     ArrayList<String> tmp = new ArrayList();
/*  903 */     switch (chosenclass) {
/*      */     case IRONCLAD: 
/*  905 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  906 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.RED) {
/*  907 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*  910 */       break;
/*      */     case THE_SILENT: 
/*  912 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  913 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.GREEN) {
/*  914 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*  917 */       break;
/*      */     case DEFECT: 
/*  919 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  920 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.BLUE) {
/*  921 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*  924 */       break;
/*      */     }
/*      */     
/*      */     
/*  928 */     return (AbstractCard)cards.get(tmp.get(rand.random(0, tmp.size() - 1)));
/*      */   }
/*      */   
/*      */   public static AbstractCard getCurse(AbstractCard prohibitedCard, Random rng) {
/*  932 */     ArrayList<String> tmp = new ArrayList();
/*  933 */     for (Map.Entry<String, AbstractCard> c : curses.entrySet()) {
/*  934 */       if ((!Objects.equals(((AbstractCard)c.getValue()).cardID, prohibitedCard.cardID)) && (!Objects.equals(
/*  935 */         ((AbstractCard)c.getValue()).cardID, "Necronomicurse"))) {
/*  936 */         if ((!Objects.equals(((AbstractCard)c.getValue()).cardID, "AscendersBane")) && (!Objects.equals(
/*  937 */           ((AbstractCard)c.getValue()).cardID, "Pride")))
/*      */         {
/*      */ 
/*  940 */           tmp.add(c.getKey());
/*      */         }
/*      */       }
/*      */     }
/*  944 */     return (AbstractCard)cards.get(tmp.get(rng.random(0, tmp.size() - 1)));
/*      */   }
/*      */   
/*      */   public static AbstractCard getCurse(AbstractCard prohibitedCard) {
/*  948 */     return getCurse(prohibitedCard, new Random());
/*      */   }
/*      */   
/*      */   public static void uploadCardData() {
/*  952 */     ArrayList<String> data = new ArrayList();
/*      */     
/*  954 */     for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  955 */       data.add(((AbstractCard)c.getValue()).gameDataUploadData());
/*  956 */       AbstractCard c2 = ((AbstractCard)c.getValue()).makeCopy();
/*  957 */       if (c2.canUpgrade()) {
/*  958 */         c2.upgrade();
/*  959 */         data.add(c2.gameDataUploadData());
/*      */       }
/*      */     }
/*  962 */     BotDataUploader.uploadDataAsync(BotDataUploader.GameDataType.CARD_DATA, AbstractCard.gameDataUploadHeader(), data);
/*      */   }
/*      */   
/*      */   public static ArrayList<AbstractCard> getAllCards() {
/*  966 */     ArrayList<AbstractCard> retVal = new ArrayList();
/*  967 */     for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  968 */       retVal.add(c.getValue());
/*      */     }
/*  970 */     return retVal;
/*      */   }
/*      */   
/*      */   public static CardGroup getEachRare(AbstractPlayer.PlayerClass chosenClass) {
/*  974 */     CardGroup everyRareCard = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*      */     
/*  976 */     switch (chosenClass) {
/*      */     case IRONCLAD: 
/*  978 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  979 */         if ((((AbstractCard)c.getValue()).color == AbstractCard.CardColor.RED) && (((AbstractCard)c.getValue()).rarity == AbstractCard.CardRarity.RARE)) {
/*  980 */           everyRareCard.addToBottom(((AbstractCard)c.getValue()).makeCopy());
/*      */         }
/*      */       }
/*  983 */       break;
/*      */     case THE_SILENT: 
/*  985 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  986 */         if ((((AbstractCard)c.getValue()).color == AbstractCard.CardColor.GREEN) && (((AbstractCard)c.getValue()).rarity == AbstractCard.CardRarity.RARE)) {
/*  987 */           everyRareCard.addToBottom(((AbstractCard)c.getValue()).makeCopy());
/*      */         }
/*      */       }
/*  990 */       break;
/*      */     case DEFECT: 
/*  992 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/*  993 */         if ((((AbstractCard)c.getValue()).color == AbstractCard.CardColor.BLUE) && (((AbstractCard)c.getValue()).rarity == AbstractCard.CardRarity.RARE)) {
/*  994 */           everyRareCard.addToBottom(((AbstractCard)c.getValue()).makeCopy());
/*      */         }
/*      */       }
/*  997 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 1002 */     return everyRareCard;
/*      */   }
/*      */   
/*      */   public static ArrayList<AbstractCard> getCardList(LibraryType type) {
/* 1006 */     ArrayList<AbstractCard> retVal = new ArrayList();
/* 1007 */     switch (type) {
/*      */     case COLORLESS: 
/* 1009 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/* 1010 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.COLORLESS) {
/* 1011 */           retVal.add(c.getValue());
/*      */         }
/*      */       }
/* 1014 */       break;
/*      */     case CURSE: 
/* 1016 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/* 1017 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.CURSE) {
/* 1018 */           retVal.add(c.getValue());
/*      */         }
/*      */       }
/* 1021 */       break;
/*      */     case RED: 
/* 1023 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/* 1024 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.RED) {
/* 1025 */           retVal.add(c.getValue());
/*      */         }
/*      */       }
/* 1028 */       break;
/*      */     case GREEN: 
/* 1030 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/* 1031 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.GREEN) {
/* 1032 */           retVal.add(c.getValue());
/*      */         }
/*      */       }
/* 1035 */       break;
/*      */     case BLUE: 
/* 1037 */       for (Map.Entry<String, AbstractCard> c : cards.entrySet()) {
/* 1038 */         if (((AbstractCard)c.getValue()).color == AbstractCard.CardColor.BLUE) {
/* 1039 */           retVal.add(c.getValue());
/*      */         }
/*      */       }
/* 1042 */       break;
/*      */     }
/*      */     
/*      */     
/* 1046 */     return retVal;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\CardLibrary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */