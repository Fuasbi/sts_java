/*      */ package com.megacrit.cardcrawl.helpers;
/*      */ 
/*      */ import com.badlogic.gdx.Files;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.files.FileHandle;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture.TextureFilter;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
/*      */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*      */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
/*      */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeBitmapFontData;
/*      */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
/*      */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.Hinting;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.badlogic.gdx.math.Matrix4;
/*      */ import com.badlogic.gdx.math.Vector2;
/*      */ import com.badlogic.gdx.math.Vector3;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import java.util.ArrayList;
/*      */ import java.util.HashMap;
/*      */ import java.util.Scanner;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class FontHelper
/*      */ {
/*   32 */   private static final Logger logger = LogManager.getLogger(FontHelper.class.getName());
/*   33 */   private static FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();
/*      */   private static FreeTypeFontGenerator generator;
/*   35 */   private static FreeTypeFontGenerator.FreeTypeBitmapFontData data = new FreeTypeFontGenerator.FreeTypeBitmapFontData();
/*   36 */   private static ArrayList<FreeTypeFontGenerator> generators = new ArrayList();
/*   37 */   private static FileHandle fontFile = null;
/*      */   
/*      */   private static final String TINY_NUMBERS_FONT = "font/04b03.ttf";
/*      */   
/*      */   private static final String ENG_DEFAULT_FONT = "font/Kreon-Regular.ttf";
/*      */   
/*      */   private static final String ENG_BOLD_FONT = "font/Kreon-Bold.ttf";
/*      */   
/*      */   private static final String ENG_ITALIC_FONT = "font/ZillaSlab-RegularItalic.otf";
/*      */   
/*      */   private static final String ENG_DRAMATIC_FONT = "font/FeDPrm27C.otf";
/*      */   private static final String ZHS_DEFAULT_FONT = "font/zhs/NotoSansMonoCJKsc-Regular.otf";
/*      */   private static final String ZHS_BOLD_FONT = "font/zhs/SourceHanSerifSC-Bold.otf";
/*      */   private static final String ZHS_ITALIC_FONT = "font/zhs/SourceHanSerifSC-Medium.otf";
/*      */   private static final String ZHT_DEFAULT_FONT = "font/zht/NotoSansCJKtc-Regular.otf";
/*      */   private static final String ZHT_BOLD_FONT = "font/zht/NotoSansCJKtc-Bold.otf";
/*      */   private static final String ZHT_ITALIC_FONT = "font/zht/NotoSansCJKtc-Medium.otf";
/*      */   private static final String EPO_DEFAULT_FONT = "font/epo/Andada-Regular.otf";
/*      */   private static final String EPO_BOLD_FONT = "font/epo/Andada-Bold.otf";
/*      */   private static final String EPO_ITALIC_FONT = "font/epo/Andada-Italic.otf";
/*      */   private static final String JPN_DEFAULT_FONT = "font/jpn/NotoSansCJKjp-Regular.otf";
/*      */   private static final String JPN_BOLD_FONT = "font/jpn/NotoSansCJKjp-Bold.otf";
/*      */   private static final String JPN_ITALIC_FONT = "font/jpn/NotoSansCJKjp-Medium.otf";
/*      */   private static final String KOR_DEFAULT_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
/*      */   private static final String KOR_BOLD_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
/*      */   private static final String KOR_ITALIC_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
/*      */   private static final String RUS_DEFAULT_FONT = "font/rus/FiraSansExtraCondensed-Regular.ttf";
/*      */   private static final String RUS_BOLD_FONT = "font/rus/FiraSansExtraCondensed-Bold.ttf";
/*      */   private static final String RUS_ITALIC_FONT = "font/rus/FiraSansExtraCondensed-Italic.ttf";
/*      */   private static final String SRB_DEFAULT_FONT = "font/srb/InfluBG.otf";
/*      */   private static final String SRB_BOLD_FONT = "font/srb/InfluBG-Bold.otf";
/*      */   private static final String SRB_ITALIC_FONT = "font/srb/InfluBG-Italic.otf";
/*      */   private static final String THA_DEFAULT_FONT = "font/tha/Itim-Regular.otf";
/*      */   private static final String THA_BOLD_FONT = "font/tha/Itim-Regular.otf";
/*      */   private static final String THA_ITALIC_FONT = "font/tha/Itim-Regular.otf";
/*      */   private static final String TUR_DEFAULT_FONT = "font/tur/Roboto-Regular.ttf";
/*      */   private static final String TUR_BOLD_FONT = "font/tur/Roboto-Bold.ttf";
/*      */   private static final String TUR_ITALIC_FONT = "font/tur/Roboto-Italic.ttf";
/*      */   private static final float CHAR_DESC_SIZE = 28.0F;
/*      */   private static final float CHAR_TITLE_SIZE = 44.0F;
/*      */   public static BitmapFont charDescFont;
/*      */   public static BitmapFont charTitleFont;
/*      */   private static final float CARD_ENERGY_SIZE = 38.0F;
/*      */   private static final float CARD_TITLE_SIZE = 27.0F;
/*      */   private static final float CARD_TITLE_SMALL_SIZE = 23.0F;
/*      */   private static final float CARD_TYPE_SIZE = 17.0F;
/*      */   private static final float CARD_DESC_SIZE = 24.0F;
/*      */   private static final float DECK_COUNT_SIZE = 30.0F;
/*      */   public static BitmapFont cardTitleFont_L;
/*      */   public static BitmapFont cardTitleFont_small_L;
/*      */   public static BitmapFont cardTitleFont_N;
/*      */   public static BitmapFont cardTitleFont_small_N;
/*      */   public static BitmapFont cardTypeFont_N;
/*      */   public static BitmapFont cardTypeFont_L;
/*      */   public static BitmapFont cardEnergyFont_L;
/*      */   public static BitmapFont cardDescFont_N;
/*      */   public static BitmapFont cardDescFont_L;
/*      */   public static BitmapFont deckCountFont;
/*      */   public static BitmapFont SCP_cardDescFont;
/*      */   public static BitmapFont SCP_cardEnergyFont;
/*      */   public static BitmapFont SCP_cardTitleFont;
/*      */   public static BitmapFont SCP_cardTitleFont_small;
/*      */   public static BitmapFont SCP_cardTypeFont;
/*      */   public static BitmapFont SRV_quoteFont;
/*      */   private static final float APPLY_POWER_SIZE = 44.0F;
/*      */   public static BitmapFont applyPowerFont;
/*      */   private static final float LOSE_POWER_SIZE = 36.0F;
/*      */   public static BitmapFont losePowerFont;
/*      */   private static final float ENERGY_NUM_SIZE = 36.0F;
/*      */   public static BitmapFont energyNumFontRed;
/*      */   public static BitmapFont energyNumFontGreen;
/*      */   public static BitmapFont energyNumFontBlue;
/*      */   private static final float MAP_LEGEND_BODY = 26.0F;
/*      */   public static BitmapFont mapLegendBodyFont;
/*      */   public static BitmapFont levelTransitionFont;
/*      */   private static final float TURN_NUM = 32.0F;
/*      */   public static BitmapFont turnNumFont;
/*      */   private static final float DAMAGE_NUM = 48.0F;
/*      */   public static BitmapFont damageNumberFont;
/*      */   private static final float TEXT_ABOVE_ENEMY_EFFECT = 32.0F;
/*      */   public static BitmapFont textAboveEnemyFont;
/*      */   private static final float BANNER_FONT_SIZE = 40.0F;
/*      */   public static BitmapFont bannerFont;
/*      */   private static final float BUTTON_LABEL_SIZE = 32.0F;
/*      */   public static BitmapFont buttonLabelFont;
/*  122 */   private static final float CARD_ENERGY_IMG_WIDTH = 24.0F * Settings.scale;
/*      */   
/*      */   private static final float TRANSITION_TITLE_SIZE = 144.0F;
/*      */   
/*      */   public static BitmapFont dungeonTitleFont;
/*      */   
/*      */   private static final float CHAR_SELECT_NAME_SIZE = 72.0F;
/*      */   
/*      */   public static BitmapFont bannerNameFont;
/*      */   
/*      */   private static final float TOP_PANEL_AMT_SIZE = 24.0F;
/*      */   
/*      */   public static BitmapFont topPanelAmountFont;
/*      */   
/*      */   private static final float POWER_AMT_SIZE2 = 16.0F;
/*      */   
/*      */   public static BitmapFont powerAmountFont;
/*      */   
/*      */   public static BitmapFont rewardTipFont;
/*      */   
/*      */   private static final float PANEL_NAME_SIZE = 34.0F;
/*      */   
/*      */   private static final float HEALTH_INFO_SIZE = 22.0F;
/*      */   
/*      */   private static final float BLOCK_INFO_SIZE = 24.0F;
/*      */   
/*      */   private static final float NAME_TITLE_SCALE = 0.66F;
/*      */   
/*      */   private static final float KEY_GOLD_SIZE = 26.0F;
/*      */   public static BitmapFont panelNameFont;
/*      */   public static BitmapFont healthInfoFont;
/*      */   public static BitmapFont blockInfoFont;
/*      */   public static BitmapFont panelNameTitleFont;
/*      */   public static BitmapFont topPanelInfoFont;
/*      */   private static final float TIP_HEADER_SIZE = 23.0F;
/*      */   private static final float TIP_BODY_SIZE = 22.0F;
/*      */   public static BitmapFont tipHeaderFont;
/*      */   public static BitmapFont tipBodyFont;
/*      */   private static final float END_TURN_SIZE = 26.0F;
/*      */   public static BitmapFont panelEndTurnFont;
/*      */   private static final float SPEECH_FONT_SIZE = 30.0F;
/*      */   public static BitmapFont speech_font;
/*      */   private static final float EVENT_BODY_SIZE = 30.0F;
/*      */   public static BitmapFont eventBodyText;
/*      */   public static BitmapFont largeDialogOptionFont;
/*      */   public static BitmapFont smallDialogOptionFont;
/*      */   private static final float TEXT_ONLY_EVENT_TITLE = 32.0F;
/*      */   public static BitmapFont textOnlyEventTitle;
/*      */   private static final float TEXT_ONLY_EVENT_SIZE = 26.0F;
/*      */   public static BitmapFont textOnlyEventBody;
/*      */   private static final float DECK_BANNER_NAME_SIZE = 34.0F;
/*      */   public static BitmapFont deckBannerFont;
/*      */   private static final float CARD_SELECT_SCREEN_FONT_SIZE = 48.0F;
/*      */   public static BitmapFont cardSelectMsgFont;
/*      */   public static final float QUESTION_MARK_POPUP_SIZE = 120.0F;
/*      */   public static BitmapFont menuBannerFont;
/*      */   public static BitmapFont largeCardFont;
/*      */   private static final float LEADERBOARD_FONT_SIZE = 30.0F;
/*      */   public static BitmapFont leaderboardFont;
/*  181 */   public static GlyphLayout layout = new GlyphLayout();
/*  182 */   public static Scanner s = null;
/*      */   
/*      */   private static final int TIP_OFFSET_X = 50;
/*      */   private static final float TIP_PADDING = 8.0F;
/*      */   
/*      */   public static void initialize()
/*      */   {
/*  189 */     generators.clear();
/*  190 */     HashMap<Character, Integer> paramCreator = new HashMap();
/*  191 */     long startTime = System.currentTimeMillis();
/*      */     
/*  193 */     switch (Settings.language) {
/*      */     case ZHS: 
/*  195 */       fontFile = Gdx.files.internal("font/zhs/NotoSansMonoCJKsc-Regular.otf");
/*  196 */       break;
/*      */     case ZHT: 
/*  198 */       fontFile = Gdx.files.internal("font/zht/NotoSansCJKtc-Regular.otf");
/*  199 */       break;
/*      */     case EPO: 
/*  201 */       fontFile = Gdx.files.internal("font/epo/Andada-Regular.otf");
/*  202 */       break;
/*      */     case JPN: 
/*  204 */       fontFile = Gdx.files.internal("font/jpn/NotoSansCJKjp-Regular.otf");
/*  205 */       break;
/*      */     case KOR: 
/*  207 */       fontFile = Gdx.files.internal("font/kor/GyeonggiCheonnyeonBatangBold.ttf");
/*  208 */       break;
/*      */     case POL: 
/*      */     case RUS: 
/*      */     case UKR: 
/*  212 */       fontFile = Gdx.files.internal("font/rus/FiraSansExtraCondensed-Regular.ttf");
/*  213 */       break;
/*      */     case SRP: 
/*      */     case SRB: 
/*  216 */       fontFile = Gdx.files.internal("font/srb/InfluBG.otf");
/*  217 */       break;
/*      */     case THA: 
/*  219 */       fontFile = Gdx.files.internal("font/tha/Itim-Regular.otf");
/*  220 */       break;
/*      */     case GRE: 
/*      */     case TUR: 
/*  223 */       fontFile = Gdx.files.internal("font/tur/Roboto-Regular.ttf");
/*  224 */       break;
/*      */     default: 
/*  226 */       fontFile = Gdx.files.internal("font/Kreon-Regular.ttf");
/*      */     }
/*      */     
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  233 */     data.xChars = new char[] { '动' };
/*  234 */     data.capChars = new char[] { '动' };
/*  235 */     generator = new FreeTypeFontGenerator(fontFile);
/*  236 */     param.minFilter = Texture.TextureFilter.Linear;
/*  237 */     param.magFilter = Texture.TextureFilter.Linear;
/*  238 */     param.hinting = FreeTypeFontGenerator.Hinting.Slight;
/*  239 */     param.spaceX = 0;
/*  240 */     param.kerning = true;
/*  241 */     mapLegendBodyFont = prepFont(26.0F, true);
/*      */     
/*  243 */     logger.info("Font load time (Map Fonts): " + (System.currentTimeMillis() - startTime) + "ms");
/*  244 */     startTime = System.currentTimeMillis();
/*      */     
/*      */ 
/*  247 */     paramCreator.clear();
/*  248 */     param.spaceX = ((int)(-4.0F * Settings.scale));
/*  249 */     param.borderWidth = (4.0F * Settings.scale);
/*  250 */     param.borderColor = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*  251 */     levelTransitionFont = prepFont(48.0F, true);
/*  252 */     param.spaceX = 0;
/*  253 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  256 */     param.gamma = 0.9F;
/*  257 */     param.borderGamma = 0.9F;
/*  258 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  259 */     param.shadowOffsetX = ((int)(4.0F * Settings.scale));
/*  260 */     charDescFont = prepFont(28.0F, false);
/*  261 */     param.gamma = 1.8F;
/*  262 */     param.borderGamma = 1.8F;
/*  263 */     param.shadowOffsetX = ((int)(6.0F * Settings.scale));
/*  264 */     charTitleFont = prepFont(44.0F, false);
/*      */     
/*      */ 
/*  267 */     param.minFilter = Texture.TextureFilter.Nearest;
/*  268 */     param.magFilter = Texture.TextureFilter.MipMapNearestNearest;
/*  269 */     param.gamma = 0.9F;
/*  270 */     param.borderGamma = 0.9F;
/*  271 */     param.shadowOffsetX = Math.round(3.0F * Settings.scale);
/*  272 */     param.shadowOffsetY = Math.round(3.0F * Settings.scale);
/*  273 */     param.borderStraight = false;
/*  274 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  275 */     param.borderColor = new Color(0.35F, 0.35F, 0.35F, 1.0F);
/*  276 */     param.borderWidth = (2.0F * Settings.scale);
/*  277 */     cardTitleFont_N = prepFont(27.0F, true);
/*  278 */     cardTitleFont_L = prepFont(27.0F, true);
/*  279 */     param.borderWidth = (2.25F * Settings.scale);
/*  280 */     cardTitleFont_small_N = prepFont(23.0F, true);
/*  281 */     cardTitleFont_small_L = prepFont(23.0F, true);
/*      */     
/*      */ 
/*  284 */     param.borderWidth = 0.0F;
/*  285 */     param.shadowOffsetX = 1;
/*  286 */     param.shadowOffsetY = 1;
/*  287 */     param.spaceX = 0;
/*  288 */     cardDescFont_N = prepFont(24.0F, false);
/*  289 */     cardDescFont_L = prepFont(24.0F, true);
/*      */     
/*      */ 
/*  292 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  293 */     param.shadowOffsetX = Math.round(4.0F * Settings.scale);
/*  294 */     param.shadowOffsetY = Math.round(3.0F * Settings.scale);
/*  295 */     SCP_cardDescFont = prepFont(48.0F, true);
/*  296 */     param.shadowOffsetX = Math.round(6.0F * Settings.scale);
/*  297 */     param.shadowOffsetY = Math.round(6.0F * Settings.scale);
/*  298 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  299 */     param.borderColor = new Color(0.35F, 0.35F, 0.35F, 1.0F);
/*  300 */     param.borderWidth = (4.0F * Settings.scale);
/*  301 */     SCP_cardTitleFont = prepFont(54.0F, true);
/*  302 */     SCP_cardTitleFont_small = prepFont(46.0F, true);
/*      */     
/*      */ 
/*  305 */     param.borderWidth = 0.0F;
/*  306 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  307 */     param.shadowOffsetX = Math.round(3.0F * Settings.scale);
/*  308 */     param.shadowOffsetY = Math.round(3.0F * Settings.scale);
/*  309 */     panelNameFont = prepFont(34.0F, true);
/*  310 */     panelNameTitleFont = prepFont(22.44F, false);
/*      */     
/*      */ 
/*  313 */     param.shadowOffsetX = ((int)(3.0F * Settings.scale));
/*  314 */     param.shadowOffsetY = ((int)(3.0F * Settings.scale));
/*  315 */     param.borderColor = new Color(0.67F, 0.06F, 0.22F, 1.0F);
/*  316 */     param.gamma = 0.9F;
/*  317 */     param.borderGamma = 0.9F;
/*  318 */     param.borderColor = new Color(0.4F, 0.1F, 0.1F, 1.0F);
/*      */     
/*      */ 
/*  321 */     param.minFilter = Texture.TextureFilter.Nearest;
/*  322 */     param.magFilter = Texture.TextureFilter.MipMapLinearNearest;
/*  323 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  326 */     tipBodyFont = prepFont(22.0F, false);
/*      */     
/*      */ 
/*  329 */     textAboveEnemyFont = prepFont(32.0F, false);
/*      */     
/*      */ 
/*  332 */     param.borderColor = new Color(0.1F, 0.1F, 0.1F, 0.5F);
/*  333 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  336 */     param.borderWidth = (2.0F * Settings.scale);
/*  337 */     topPanelAmountFont = prepFont(24.0F, false);
/*  338 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  341 */     param.borderColor = Color.valueOf("42514dff");
/*  342 */     param.shadowOffsetX = 0;
/*  343 */     param.shadowOffsetY = 0;
/*      */     
/*      */ 
/*  346 */     param.borderWidth = 0.0F;
/*  347 */     rewardTipFont = prepFont(24.0F, false);
/*  348 */     param.shadowOffsetX = ((int)(3.0F * Settings.scale));
/*  349 */     param.shadowOffsetY = ((int)(3.0F * Settings.scale));
/*      */     
/*      */ 
/*  352 */     param.shadowOffsetX = ((int)(4.0F * Settings.scale));
/*  353 */     param.shadowOffsetY = ((int)(4.0F * Settings.scale));
/*  354 */     param.spaceX = ((int)(-2.1F * Settings.scale));
/*  355 */     param.borderWidth = (3.0F * Settings.scale);
/*  356 */     panelEndTurnFont = prepFont(26.0F, false);
/*  357 */     param.spaceX = 0;
/*  358 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  361 */     param.shadowOffsetX = 0;
/*  362 */     param.shadowOffsetY = 0;
/*  363 */     speech_font = prepFont(30.0F, true);
/*      */     
/*      */ 
/*  366 */     param.shadowOffsetX = ((int)(3.0F * Settings.scale));
/*  367 */     param.shadowOffsetY = ((int)(3.0F * Settings.scale));
/*  368 */     eventBodyText = prepFont(30.0F, false);
/*  369 */     largeDialogOptionFont = prepFont(30.0F, false);
/*  370 */     largeDialogOptionFont.getData().markupEnabled = false;
/*  371 */     smallDialogOptionFont = prepFont(26.0F, false);
/*  372 */     smallDialogOptionFont.getData().markupEnabled = false;
/*  373 */     textOnlyEventBody = prepFont(26.0F, false);
/*      */     
/*      */ 
/*  376 */     param.shadowOffsetX = 0;
/*  377 */     param.shadowOffsetY = 0;
/*  378 */     cardSelectMsgFont = prepFont(48.0F, true);
/*      */     
/*      */ 
/*  381 */     turnNumFont = prepFont(32.0F, true);
/*      */     
/*      */ 
/*  384 */     param.borderWidth = (4.0F * Settings.scale);
/*  385 */     param.borderColor = new Color(0.3F, 0.3F, 0.3F, 1.0F);
/*  386 */     applyPowerFont = prepFont(44.0F, true);
/*  387 */     losePowerFont = prepFont(36.0F, true);
/*      */     
/*      */ 
/*  390 */     param.borderWidth = (3.0F * Settings.scale);
/*  391 */     param.borderColor = Color.DARK_GRAY.cpy();
/*  392 */     param.minFilter = Texture.TextureFilter.Linear;
/*  393 */     param.magFilter = Texture.TextureFilter.Linear;
/*  394 */     damageNumberFont = prepFont(48.0F, true);
/*  395 */     param.borderWidth = 0.0F;
/*      */     
/*      */ 
/*  398 */     param.shadowOffsetX = ((int)(5.0F * Settings.scale));
/*  399 */     param.shadowOffsetY = ((int)(5.0F * Settings.scale));
/*  400 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*  401 */     deckBannerFont = prepFont(34.0F, false);
/*      */     
/*      */ 
/*  404 */     param.borderWidth = (2.7F * Settings.scale);
/*  405 */     param.shadowOffsetX = ((int)(3.0F * Settings.scale));
/*  406 */     param.shadowOffsetY = ((int)(3.0F * Settings.scale));
/*  407 */     param.borderColor = new Color(0.45F, 0.1F, 0.12F, 1.0F);
/*  408 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  409 */     healthInfoFont = prepFont(22.0F, false);
/*  410 */     param.borderColor = new Color(0.408F, 0.361F, 0.321F, 1.0F);
/*  411 */     textOnlyEventTitle = prepFont(32.0F, false);
/*      */     
/*      */ 
/*  414 */     param.spaceX = ((int)(-2.1F * Settings.scale));
/*  415 */     param.borderWidth = (4.0F * Settings.scale);
/*  416 */     param.spaceX = ((int)(-2.5F * Settings.scale));
/*  417 */     param.borderColor = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  418 */     buttonLabelFont = prepFont(32.0F, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  423 */     param.spaceX = 0;
/*  424 */     generator.dispose();
/*      */     
/*  426 */     logger.info("Font load time (Kreon Standard): " + (System.currentTimeMillis() - startTime) + "ms");
/*  427 */     startTime = System.currentTimeMillis();
/*      */     
/*      */ 
/*  430 */     fontFile = Gdx.files.internal("font/Kreon-Bold.ttf");
/*  431 */     generator = new FreeTypeFontGenerator(fontFile);
/*      */     
/*      */ 
/*  434 */     param.borderStraight = true;
/*  435 */     param.borderWidth = (4.0F * Settings.scale);
/*  436 */     param.borderColor = new Color(0.4F, 0.15F, 0.15F, 1.0F);
/*  437 */     energyNumFontRed = prepFont(36.0F, true);
/*  438 */     param.borderColor = new Color(0.15F, 0.4F, 0.15F, 1.0F);
/*  439 */     energyNumFontGreen = prepFont(36.0F, true);
/*  440 */     param.borderColor = new Color(0.1F, 0.2F, 0.4F, 1.0F);
/*  441 */     energyNumFontBlue = prepFont(36.0F, true);
/*  442 */     param.borderWidth = (4.0F * Settings.scale);
/*  443 */     param.borderColor = new Color(0.3F, 0.3F, 0.3F, 1.0F);
/*  444 */     cardEnergyFont_L = prepFont(38.0F, true);
/*  445 */     param.borderWidth = (8.0F * Settings.scale);
/*  446 */     SCP_cardEnergyFont = prepFont(76.0F, true);
/*      */     
/*      */ 
/*  449 */     param.shadowOffsetX = ((int)(2.0F * Settings.scale));
/*  450 */     param.shadowOffsetY = ((int)(2.0F * Settings.scale));
/*  451 */     param.borderColor = new Color(0.0F, 0.33F, 0.2F, 0.8F);
/*  452 */     param.borderWidth = (2.7F * Settings.scale);
/*  453 */     param.spaceX = ((int)(-0.9F * Settings.scale));
/*  454 */     blockInfoFont = prepFont(24.0F, false);
/*      */     
/*      */ 
/*  457 */     param.shadowOffsetX = 0;
/*  458 */     param.shadowOffsetY = 0;
/*  459 */     param.borderWidth = 0.0F;
/*  460 */     deckCountFont = prepFont(30.0F, false);
/*  461 */     generator.dispose();
/*      */     
/*      */ 
/*  464 */     switch (Settings.language) {
/*      */     case ZHS: 
/*  466 */       fontFile = Gdx.files.internal("font/zhs/SourceHanSerifSC-Bold.otf");
/*  467 */       break;
/*      */     case ZHT: 
/*  469 */       fontFile = Gdx.files.internal("font/zht/NotoSansCJKtc-Bold.otf");
/*  470 */       break;
/*      */     case EPO: 
/*  472 */       fontFile = Gdx.files.internal("font/epo/Andada-Bold.otf");
/*  473 */       break;
/*      */     case JPN: 
/*  475 */       fontFile = Gdx.files.internal("font/jpn/NotoSansCJKjp-Bold.otf");
/*  476 */       break;
/*      */     case KOR: 
/*  478 */       fontFile = Gdx.files.internal("font/kor/GyeonggiCheonnyeonBatangBold.ttf");
/*  479 */       break;
/*      */     case POL: 
/*      */     case RUS: 
/*      */     case UKR: 
/*  483 */       fontFile = Gdx.files.internal("font/rus/FiraSansExtraCondensed-Bold.ttf");
/*  484 */       break;
/*      */     case SRP: 
/*      */     case SRB: 
/*  487 */       fontFile = Gdx.files.internal("font/srb/InfluBG-Bold.otf");
/*  488 */       break;
/*      */     case THA: 
/*  490 */       fontFile = Gdx.files.internal("font/tha/Itim-Regular.otf");
/*  491 */       break;
/*      */     case GRE: 
/*      */     case TUR: 
/*  494 */       fontFile = Gdx.files.internal("font/tur/Roboto-Bold.ttf");
/*  495 */       break;
/*      */     default: 
/*  497 */       fontFile = Gdx.files.internal("font/Kreon-Bold.ttf");
/*      */     }
/*      */     
/*      */     
/*  501 */     generator = new FreeTypeFontGenerator(fontFile);
/*      */     
/*      */ 
/*  504 */     param.gamma = 1.2F;
/*  505 */     param.borderWidth = 0.0F;
/*  506 */     param.shadowOffsetX = 0;
/*  507 */     param.shadowOffsetY = 0;
/*  508 */     if (Settings.WIDTH >= 1600) {
/*  509 */       param.spaceX = -1;
/*      */     }
/*  511 */     cardTypeFont_N = prepFont(17.0F, false);
/*  512 */     cardTypeFont_L = prepFont(17.0F, true);
/*  513 */     SCP_cardTypeFont = prepFont(34.0F, true);
/*      */     
/*      */ 
/*  516 */     param.gamma = 1.2F;
/*  517 */     param.borderGamma = 1.2F;
/*  518 */     param.borderWidth = (5.0F * Settings.scale);
/*  519 */     param.borderColor = new Color(0.23F, 0.26F, 0.317F, 1.0F);
/*  520 */     param.shadowOffsetX = Math.round(3.0F * Settings.scale);
/*  521 */     param.shadowOffsetY = Math.round(3.0F * Settings.scale);
/*  522 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*  523 */     bannerFont = prepFont(40.0F, true);
/*  524 */     param.borderWidth = 0.0F;
/*  525 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.12F);
/*  526 */     param.shadowOffsetX = ((int)(5.0F * Settings.scale));
/*  527 */     param.shadowOffsetY = ((int)(4.0F * Settings.scale));
/*  528 */     menuBannerFont = prepFont(38.0F, true);
/*  529 */     param.characters = "?";
/*  530 */     param.shadowOffsetX = ((int)(15.0F * Settings.scale));
/*  531 */     param.shadowOffsetY = ((int)(12.0F * Settings.scale));
/*  532 */     largeCardFont = prepFont(120.0F, true);
/*      */     
/*      */ 
/*  535 */     param.minFilter = Texture.TextureFilter.Nearest;
/*  536 */     param.magFilter = Texture.TextureFilter.MipMapLinearNearest;
/*  537 */     param.shadowOffsetX = 2;
/*  538 */     param.shadowOffsetY = 2;
/*  539 */     param.shadowColor = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*  540 */     param.gamma = 2.0F;
/*  541 */     param.borderGamma = 2.0F;
/*  542 */     param.borderStraight = true;
/*  543 */     param.borderColor = Color.DARK_GRAY.cpy();
/*  544 */     param.borderWidth = (2.0F * Settings.scale);
/*      */     
/*  546 */     param.shadowOffsetX = 1;
/*  547 */     param.shadowOffsetY = 1;
/*  548 */     tipHeaderFont = prepFont(23.0F, false);
/*  549 */     param.shadowOffsetX = 2;
/*  550 */     param.shadowOffsetY = 2;
/*  551 */     topPanelInfoFont = prepFont(26.0F, false);
/*  552 */     param.spaceX = 0;
/*  553 */     param.gamma = 0.9F;
/*  554 */     param.borderGamma = 0.9F;
/*  555 */     param.borderWidth = 0.0F;
/*  556 */     generator.dispose();
/*      */     
/*      */ 
/*  559 */     fontFile = Gdx.files.internal("font/04b03.ttf");
/*  560 */     param.borderWidth = (2.0F * Settings.scale);
/*  561 */     param.minFilter = Texture.TextureFilter.Nearest;
/*  562 */     param.magFilter = Texture.TextureFilter.MipMapLinearNearest;
/*  563 */     generator = new FreeTypeFontGenerator(fontFile);
/*  564 */     powerAmountFont = prepFont(16.0F, false);
/*  565 */     generator.dispose();
/*      */     
/*      */ 
/*  568 */     switch (Settings.language) {
/*      */     case ZHS: 
/*  570 */       fontFile = Gdx.files.internal("font/zhs/SourceHanSerifSC-Bold.otf");
/*  571 */       break;
/*      */     case ZHT: 
/*  573 */       fontFile = Gdx.files.internal("font/zht/NotoSansCJKtc-Bold.otf");
/*  574 */       break;
/*      */     case EPO: 
/*  576 */       fontFile = Gdx.files.internal("font/epo/Andada-Bold.otf");
/*  577 */       break;
/*      */     case JPN: 
/*  579 */       fontFile = Gdx.files.internal("font/jpn/NotoSansCJKjp-Bold.otf");
/*  580 */       break;
/*      */     case KOR: 
/*  582 */       fontFile = Gdx.files.internal("font/kor/GyeonggiCheonnyeonBatangBold.ttf");
/*  583 */       break;
/*      */     case POL: 
/*      */     case RUS: 
/*      */     case UKR: 
/*  587 */       fontFile = Gdx.files.internal("font/rus/FiraSansExtraCondensed-Bold.ttf");
/*  588 */       break;
/*      */     case SRP: 
/*      */     case SRB: 
/*  591 */       fontFile = Gdx.files.internal("font/srb/InfluBG-Bold.otf");
/*  592 */       break;
/*      */     case THA: 
/*  594 */       fontFile = Gdx.files.internal("font/tha/Itim-Regular.otf");
/*  595 */       break;
/*      */     case GRE: 
/*      */     case TUR: 
/*  598 */       fontFile = Gdx.files.internal("font/tur/Roboto-Bold.ttf");
/*  599 */       break;
/*      */     default: 
/*  601 */       fontFile = Gdx.files.internal("font/FeDPrm27C.otf");
/*      */     }
/*      */     
/*      */     
/*  605 */     generator = new FreeTypeFontGenerator(fontFile);
/*  606 */     param.gamma = 0.5F;
/*  607 */     param.borderGamma = 0.5F;
/*  608 */     param.shadowOffsetX = 0;
/*  609 */     param.shadowOffsetY = 0;
/*  610 */     param.borderWidth = (6.0F * Settings.scale);
/*  611 */     param.borderColor = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*  612 */     param.spaceX = ((int)(-5.0F * Settings.scale));
/*  613 */     dungeonTitleFont = prepFont(144.0F, true);
/*  614 */     param.borderWidth = (4.0F * Settings.scale);
/*  615 */     param.borderColor = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*  616 */     param.spaceX = ((int)(-2.0F * Settings.scale));
/*  617 */     bannerNameFont = prepFont(72.0F, true);
/*  618 */     generator.dispose();
/*      */     
/*      */ 
/*  621 */     switch (Settings.language) {
/*      */     case ZHS: 
/*  623 */       fontFile = Gdx.files.internal("font/zhs/SourceHanSerifSC-Medium.otf");
/*  624 */       break;
/*      */     case ZHT: 
/*  626 */       fontFile = Gdx.files.internal("font/zht/NotoSansCJKtc-Medium.otf");
/*  627 */       break;
/*      */     case EPO: 
/*  629 */       fontFile = Gdx.files.internal("font/epo/Andada-Italic.otf");
/*  630 */       break;
/*      */     case JPN: 
/*  632 */       fontFile = Gdx.files.internal("font/jpn/NotoSansCJKjp-Medium.otf");
/*  633 */       break;
/*      */     case KOR: 
/*  635 */       fontFile = Gdx.files.internal("font/kor/GyeonggiCheonnyeonBatangBold.ttf");
/*  636 */       break;
/*      */     case POL: 
/*      */     case RUS: 
/*      */     case UKR: 
/*  640 */       fontFile = Gdx.files.internal("font/rus/FiraSansExtraCondensed-Italic.ttf");
/*  641 */       break;
/*      */     case SRP: 
/*      */     case SRB: 
/*  644 */       fontFile = Gdx.files.internal("font/srb/InfluBG-Italic.otf");
/*  645 */       break;
/*      */     case THA: 
/*  647 */       fontFile = Gdx.files.internal("font/tha/Itim-Regular.otf");
/*  648 */       break;
/*      */     case GRE: 
/*      */     case TUR: 
/*  651 */       fontFile = Gdx.files.internal("font/tur/Roboto-Italic.ttf");
/*  652 */       break;
/*      */     default: 
/*  654 */       fontFile = Gdx.files.internal("font/ZillaSlab-RegularItalic.otf");
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  659 */     param.shadowOffsetX = 0;
/*  660 */     param.shadowOffsetY = 0;
/*  661 */     param.borderWidth = 0.0F;
/*  662 */     param.shadowOffsetX = Math.round(2.0F * Settings.scale);
/*  663 */     param.shadowOffsetY = Math.round(2.0F * Settings.scale);
/*  664 */     param.spaceX = 0;
/*      */     
/*  666 */     generator = new FreeTypeFontGenerator(fontFile);
/*  667 */     SRV_quoteFont = prepFont(28.0F, false);
/*  668 */     generator.dispose();
/*      */     
/*      */ 
/*  671 */     fontFile = Gdx.files.internal("font/zhs/NotoSansMonoCJKsc-Regular.otf");
/*  672 */     generator = new FreeTypeFontGenerator(fontFile);
/*  673 */     leaderboardFont = prepFont(30.0F, false);
/*  674 */     generator.dispose();
/*      */     
/*  676 */     logger.info("Font load time (The rest of it): " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static BitmapFont prepFont(float size, boolean isLinearFiltering)
/*      */   {
/*  687 */     FreeTypeFontGenerator g = new FreeTypeFontGenerator(fontFile);
/*  688 */     FreeTypeFontGenerator.FreeTypeFontParameter p = new FreeTypeFontGenerator.FreeTypeFontParameter();
/*      */     
/*      */ 
/*  691 */     p.characters = "";
/*  692 */     p.incremental = true;
/*  693 */     p.size = Math.round(size * Settings.scale);
/*  694 */     p.gamma = param.gamma;
/*  695 */     p.spaceX = param.spaceX;
/*  696 */     p.spaceY = param.spaceY;
/*      */     
/*      */ 
/*  699 */     p.borderColor = param.borderColor;
/*  700 */     p.borderStraight = param.borderStraight;
/*  701 */     p.borderWidth = param.borderWidth;
/*  702 */     p.borderGamma = param.borderGamma;
/*      */     
/*      */ 
/*  705 */     p.shadowColor = param.shadowColor;
/*  706 */     p.shadowOffsetX = param.shadowOffsetX;
/*  707 */     p.shadowOffsetY = param.shadowOffsetY;
/*      */     
/*      */ 
/*  710 */     if (isLinearFiltering) {
/*  711 */       p.minFilter = Texture.TextureFilter.Linear;
/*  712 */       p.magFilter = Texture.TextureFilter.Linear;
/*      */     } else {
/*  714 */       p.minFilter = Texture.TextureFilter.Nearest;
/*  715 */       p.magFilter = Texture.TextureFilter.MipMapLinearNearest;
/*      */     }
/*  717 */     g.scaleForPixelHeight(p.size);
/*      */     
/*  719 */     BitmapFont font = g.generateFont(p);
/*  720 */     font.setUseIntegerPositions(!isLinearFiltering);
/*  721 */     font.getData().markupEnabled = true;
/*  722 */     generators.add(g);
/*  723 */     return font;
/*      */   }
/*      */   
/*      */   public static void renderTipLeft(SpriteBatch sb, String msg) {
/*  727 */     layout.setText(cardDescFont_N, msg);
/*  728 */     sb.setColor(Color.BLACK);
/*  729 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, InputHelper.mX - layout.width - 16.0F - 12.5F, InputHelper.mY - layout.height, layout.width + 16.0F, layout.height + 16.0F);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  735 */     renderFont(sb, cardDescFont_N, msg, InputHelper.mX - layout.width - 8.0F - 12.0F, InputHelper.mY + 8.0F, Color.WHITE);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFont(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/*  755 */     font.setColor(c);
/*  756 */     font.draw(sb, msg, x, y);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderRotatedText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float offsetX, float offsetY, float angle, boolean roundY, Color c)
/*      */   {
/*  783 */     if (roundY) {
/*  784 */       y = Math.round(y) + 0.25F;
/*      */     }
/*      */     
/*  787 */     if (font.getData().scaleX == 1.0F) {
/*  788 */       x = MathUtils.round(x);
/*  789 */       y = MathUtils.round(y);
/*  790 */       offsetX = MathUtils.round(offsetX);
/*  791 */       offsetY = MathUtils.round(offsetY);
/*      */     }
/*      */     
/*  794 */     Matrix4 mx4 = new Matrix4();
/*  795 */     mx4.setToRotation(new Vector3(0.0F, 0.0F, 1.0F), angle);
/*  796 */     Vector2 test = new Vector2(offsetX, offsetY);
/*  797 */     test.rotate(angle);
/*  798 */     mx4.trn(x + test.x, y + test.y, 0.0F);
/*  799 */     sb.end();
/*  800 */     sb.setTransformMatrix(mx4);
/*  801 */     sb.begin();
/*      */     
/*  803 */     font.setColor(c);
/*  804 */     layout.setText(font, msg);
/*  805 */     font.draw(sb, msg, -layout.width / 2.0F, layout.height / 2.0F);
/*      */     
/*  807 */     sb.end();
/*  808 */     sb.setTransformMatrix(new Matrix4());
/*  809 */     sb.begin();
/*      */   }
/*      */   
/*      */   public static void renderWrappedText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float width) {
/*  813 */     renderWrappedText(sb, font, msg, x, y, width, Color.WHITE, 1.0F);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderWrappedText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float width, float scale)
/*      */   {
/*  824 */     renderWrappedText(sb, font, msg, x, y, width, Color.WHITE, scale);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderWrappedText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float width, Color c, float scale)
/*      */   {
/*  836 */     font.getData().setScale(scale);
/*  837 */     font.setColor(c);
/*  838 */     layout.setText(font, msg, Color.WHITE, width, 1, true);
/*  839 */     font.draw(sb, msg, x - width / 2.0F, y + layout.height / 2.0F * scale, width, 1, true);
/*  840 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontLeftDownAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/*  850 */     layout.setText(font, msg);
/*  851 */     renderFont(sb, font, msg, x, y + layout.height, c);
/*      */   }
/*      */   
/*      */   public static void renderFontRightToLeft(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c) {
/*  855 */     layout.setText(font, msg, c, 1.0F, 18, false);
/*  856 */     font.setColor(c);
/*  857 */     font.draw(sb, msg, x - layout.width, y);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontRightTopAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/*  867 */     layout.setText(font, msg);
/*  868 */     renderFont(sb, font, msg, x - layout.width, y, c);
/*      */   }
/*      */   
/*      */   public static void renderFontRightAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c) {
/*  872 */     layout.setText(font, msg);
/*  873 */     renderFont(sb, font, msg, x - layout.width, y + layout.height / 2.0F, c);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontRightTopAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float scale, Color c)
/*      */   {
/*  884 */     font.getData().setScale(1.0F);
/*  885 */     layout.setText(font, msg);
/*  886 */     float offsetX = layout.width / 2.0F;
/*  887 */     float offsetY = layout.height;
/*  888 */     font.getData().setScale(scale);
/*  889 */     layout.setText(font, msg);
/*  890 */     renderFont(sb, font, msg, x - layout.width / 2.0F - offsetX, y + layout.height / 2.0F + offsetY, c);
/*      */   }
/*      */   
/*      */   public static void renderSmartText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color baseColor) {
/*  894 */     renderSmartText(sb, font, msg, x, y, Float.MAX_VALUE, font.getLineHeight(), baseColor);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderSmartText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float lineWidth, float lineSpacing, Color baseColor)
/*      */   {
/*  918 */     if (msg == null) {
/*  919 */       return;
/*      */     }
/*      */     
/*  922 */     if ((Settings.lineBreakViaCharacter) && (font.getData().markupEnabled)) {
/*  923 */       exampleNonWordWrappedText(sb, font, msg, x, y, baseColor, lineWidth);
/*  924 */       return;
/*      */     }
/*      */     
/*  927 */     s = new Scanner(msg);
/*  928 */     Color color = null;
/*  929 */     float curWidth = 0.0F;float curHeight = 0.0F;
/*  930 */     layout.setText(font, " ");
/*  931 */     float spaceWidth = layout.width;
/*      */     
/*  933 */     while (s.hasNext()) {
/*  934 */       String word = s.next();
/*  935 */       if (word.equals("NL")) {
/*  936 */         curWidth = 0.0F;
/*  937 */         curHeight -= lineSpacing;
/*      */       }
/*  939 */       else if (word.equals("TAB")) {
/*  940 */         curWidth += spaceWidth * 5.0F;
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*  945 */         TextureAtlas.AtlasRegion orb = identifyOrb(word);
/*      */         
/*  947 */         if (orb == null)
/*      */         {
/*      */ 
/*  950 */           color = identifyColor(word);
/*  951 */           if (!color.equals(Color.WHITE)) {
/*  952 */             word = word.substring(2, word.length());
/*  953 */             color.a = baseColor.a;
/*  954 */             font.setColor(color);
/*      */           } else {
/*  956 */             font.setColor(baseColor);
/*      */           }
/*      */           
/*  959 */           layout.setText(font, word);
/*      */           
/*      */ 
/*  962 */           if (curWidth + layout.width > lineWidth) {
/*  963 */             curHeight -= lineSpacing;
/*  964 */             font.draw(sb, word, x, y + curHeight);
/*  965 */             curWidth = layout.width + spaceWidth;
/*      */           } else {
/*  967 */             font.draw(sb, word, x + curWidth, y + curHeight);
/*  968 */             curWidth += layout.width + spaceWidth;
/*      */           }
/*      */         } else {
/*  971 */           sb.setColor(new Color(1.0F, 1.0F, 1.0F, baseColor.a));
/*      */           
/*      */ 
/*  974 */           if (curWidth + CARD_ENERGY_IMG_WIDTH > lineWidth) {
/*  975 */             curHeight -= lineSpacing;
/*  976 */             sb.draw(orb, x, y + curHeight - CARD_ENERGY_IMG_WIDTH + 2.0F * Settings.scale, CARD_ENERGY_IMG_WIDTH, CARD_ENERGY_IMG_WIDTH);
/*      */             
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  982 */             curWidth = CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */           } else {
/*  984 */             sb.draw(orb, x + curWidth, y + curHeight - CARD_ENERGY_IMG_WIDTH + 2.0F * Settings.scale, CARD_ENERGY_IMG_WIDTH, CARD_ENERGY_IMG_WIDTH);
/*      */             
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  990 */             curWidth += CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*  995 */     layout.setText(font, msg);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderSmartTextCentered(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float lineWidth, float lineSpacing, Color baseColor)
/*      */   {
/* 1019 */     s = new Scanner(msg);
/* 1020 */     Color color = null;
/* 1021 */     float curWidth = 0.0F;float curHeight = 0.0F;
/* 1022 */     layout.setText(font, " ");
/* 1023 */     float spaceWidth = layout.width;
/*      */     
/* 1025 */     while (s.hasNext()) {
/* 1026 */       String word = s.next();
/* 1027 */       if (word.equals("NL")) {
/* 1028 */         curWidth = 0.0F;
/* 1029 */         curHeight -= lineSpacing;
/*      */       }
/* 1031 */       else if (word.equals("TAB")) {
/* 1032 */         curWidth += spaceWidth * 5.0F;
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/* 1042 */         color = identifyColor(word);
/* 1043 */         if (!color.equals(Color.WHITE)) {
/* 1044 */           word = word.substring(2, word.length());
/* 1045 */           color.a = baseColor.a;
/* 1046 */           font.setColor(color);
/*      */         } else {
/* 1048 */           font.setColor(baseColor);
/*      */         }
/*      */         
/* 1051 */         layout.setText(font, word);
/*      */         
/*      */ 
/* 1054 */         if (curWidth + layout.width > lineWidth) {
/* 1055 */           curHeight -= lineSpacing;
/* 1056 */           font.draw(sb, word, x - lineWidth / 2.0F, y + curHeight);
/* 1057 */           curWidth = layout.width + spaceWidth;
/*      */         } else {
/* 1059 */           font.draw(sb, word, x + curWidth - lineWidth / 2.0F, y + curHeight);
/* 1060 */           curWidth += layout.width + spaceWidth;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1088 */     layout.setText(font, msg);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static float getSmartHeight(BitmapFont font, String msg, float lineWidth, float lineSpacing)
/*      */   {
/* 1102 */     if (msg == null) {
/* 1103 */       return 0.0F;
/*      */     }
/*      */     
/* 1106 */     if (Settings.lineBreakViaCharacter) {
/* 1107 */       return -getHeightForCharLineBreak(font, msg, lineWidth, lineSpacing);
/*      */     }
/*      */     
/* 1110 */     s = new Scanner(msg);
/* 1111 */     float curWidth = 0.0F;float curHeight = 0.0F;
/* 1112 */     layout.setText(font, " ");
/* 1113 */     float spaceWidth = layout.width;
/*      */     
/* 1115 */     while (s.hasNext()) {
/* 1116 */       String word = s.next();
/* 1117 */       if (word.equals("NL")) {
/* 1118 */         curWidth = 0.0F;
/* 1119 */         curHeight -= lineSpacing;
/*      */       }
/* 1121 */       else if (word.equals("TAB")) {
/* 1122 */         curWidth += spaceWidth * 5.0F;
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/* 1127 */         TextureAtlas.AtlasRegion orb = identifyOrb(word);
/* 1128 */         if (orb == null) {
/* 1129 */           if (!identifyColor(word).equals(Color.WHITE)) {
/* 1130 */             word = word.substring(2, word.length());
/*      */           }
/*      */           
/* 1133 */           layout.setText(font, word);
/*      */           
/*      */ 
/* 1136 */           if (curWidth + layout.width > lineWidth) {
/* 1137 */             curHeight -= lineSpacing;
/* 1138 */             curWidth = layout.width + spaceWidth;
/*      */           } else {
/* 1140 */             curWidth += layout.width + spaceWidth;
/*      */           }
/*      */           
/*      */         }
/* 1144 */         else if (curWidth + CARD_ENERGY_IMG_WIDTH > lineWidth) {
/* 1145 */           curHeight -= lineSpacing;
/* 1146 */           curWidth = CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */         } else {
/* 1148 */           curWidth += CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1154 */     return curHeight;
/*      */   }
/*      */   
/*      */   private static float getHeightForCharLineBreak(BitmapFont font, String msg, float lineWidth, float lineSpacing)
/*      */   {
/* 1159 */     StringBuilder newMsg = new StringBuilder();
/* 1160 */     s = new Scanner(msg);
/* 1161 */     while (s.hasNext()) {
/* 1162 */       String derp = s.next();
/* 1163 */       if (derp.equals("NL")) {
/* 1164 */         newMsg.append("\n");
/*      */ 
/*      */       }
/* 1167 */       else if (derp.charAt(0) == '#') {
/* 1168 */         switch (derp.charAt(1)) {
/*      */         case 'r': 
/* 1170 */           derp = "[#ff6563]" + derp.substring(2) + "[]";
/* 1171 */           break;
/*      */         case 'g': 
/* 1173 */           derp = "[#7fff00]" + derp.substring(2) + "[]";
/* 1174 */           break;
/*      */         case 'b': 
/* 1176 */           derp = "[#87ceeb]" + derp.substring(2) + "[]";
/* 1177 */           break;
/*      */         case 'y': 
/* 1179 */           derp = "[#efc851]" + derp.substring(2) + "[]";
/* 1180 */           break;
/*      */         }
/*      */         
/*      */         
/* 1184 */         newMsg.append(derp);
/*      */       } else {
/* 1186 */         newMsg.append(derp);
/*      */       }
/*      */     }
/*      */     
/* 1190 */     msg = newMsg.toString();
/*      */     
/* 1192 */     if ((msg != null) && (msg.length() > 0)) {
/* 1193 */       layout.setText(font, msg, new Color(), lineWidth, -1, true);
/*      */     }
/* 1195 */     return layout.height - 16.0F * Settings.scale;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static float getSmartWidth(BitmapFont font, String msg, float lineWidth, float lineSpacing)
/*      */   {
/* 1209 */     s = new Scanner(msg);
/* 1210 */     float curWidth = 0.0F;
/* 1211 */     layout.setText(font, " ");
/* 1212 */     float spaceWidth = layout.width;
/*      */     
/* 1214 */     while (s.hasNext()) {
/* 1215 */       String word = s.next();
/* 1216 */       if (word.equals("NL")) {
/* 1217 */         curWidth = 0.0F;
/*      */       }
/* 1219 */       else if (word.equals("TAB")) {
/* 1220 */         curWidth += spaceWidth * 5.0F;
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/* 1225 */         TextureAtlas.AtlasRegion orb = identifyOrb(word);
/* 1226 */         if (orb == null) {
/* 1227 */           if (!identifyColor(word).equals(Color.WHITE)) {
/* 1228 */             word = word.substring(2, word.length());
/*      */           }
/*      */           
/* 1231 */           layout.setText(font, word);
/*      */           
/*      */ 
/* 1234 */           if (curWidth + layout.width > lineWidth) {
/* 1235 */             curWidth = layout.width + spaceWidth;
/*      */           } else {
/* 1237 */             curWidth += layout.width + spaceWidth;
/*      */           }
/*      */           
/*      */         }
/* 1241 */         else if (curWidth + CARD_ENERGY_IMG_WIDTH > lineWidth) {
/* 1242 */           curWidth = CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */         } else {
/* 1244 */           curWidth += CARD_ENERGY_IMG_WIDTH + spaceWidth;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1249 */     return curWidth;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TextureAtlas.AtlasRegion identifyOrb(String word)
/*      */   {
/* 1259 */     switch (word) {
/*      */     case "[R]": 
/* 1261 */       return AbstractCard.orb_red;
/*      */     case "[G]": 
/* 1263 */       return AbstractCard.orb_green;
/*      */     case "[B]": 
/* 1265 */       return AbstractCard.orb_blue;
/*      */     }
/* 1267 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static Color identifyColor(String word)
/*      */   {
/* 1278 */     if (word.charAt(0) == '#') {
/* 1279 */       switch (word.charAt(1)) {
/*      */       case 'r': 
/* 1281 */         return Settings.RED_TEXT_COLOR.cpy();
/*      */       case 'g': 
/* 1283 */         return Settings.GREEN_TEXT_COLOR.cpy();
/*      */       case 'b': 
/* 1285 */         return Settings.BLUE_TEXT_COLOR.cpy();
/*      */       case 'y': 
/* 1287 */         return Settings.GOLD_COLOR.cpy();
/*      */       case 'p': 
/* 1289 */         return Settings.PURPLE_COLOR.cpy();
/*      */       }
/* 1291 */       return Color.WHITE.cpy();
/*      */     }
/*      */     
/* 1294 */     return Color.WHITE.cpy();
/*      */   }
/*      */   
/*      */   public static void renderDeckViewTip(SpriteBatch sb, String msg, float y, Color color)
/*      */   {
/* 1299 */     layout.setText(rewardTipFont, msg);
/* 1300 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.66F));
/* 1301 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH / 2.0F - layout.width / 2.0F - 12.0F * Settings.scale, y - 24.0F * Settings.scale, layout.width + 24.0F * Settings.scale, 48.0F * Settings.scale);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1308 */     renderFontCentered(sb, rewardTipFont, msg, Settings.WIDTH / 2.0F, y, color);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontLeftTopAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/* 1318 */     layout.setText(font, msg);
/* 1319 */     renderFont(sb, font, msg, x, y, c);
/*      */   }
/*      */   
/*      */   public static void renderFontCentered(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c) {
/* 1323 */     layout.setText(font, msg);
/* 1324 */     renderFont(sb, font, msg, x - layout.width / 2.0F, y + layout.height / 2.0F, c);
/*      */   }
/*      */   
/*      */   public static void renderFontLeft(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c) {
/* 1328 */     layout.setText(font, msg);
/* 1329 */     renderFont(sb, font, msg, x, y + layout.height / 2.0F, c);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void exampleNonWordWrappedText(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c, float widthMax)
/*      */   {
/* 1341 */     String newMsg = "";
/* 1342 */     s = new Scanner(msg);
/*      */     
/* 1344 */     while (s.hasNext()) {
/* 1345 */       String word = s.next();
/* 1346 */       layout.setText(font, word);
/* 1347 */       float lh = 0.0F;
/*      */       
/* 1349 */       if (word.equals("NL")) {
/* 1350 */         newMsg = newMsg + "\n";
/* 1351 */       } else if (word.charAt(0) == '[') {
/* 1352 */         newMsg = newMsg + "   ";
/* 1353 */         layout.setText(font, newMsg, 0, newMsg.length(), c, widthMax, -1, true, null);
/* 1354 */         lh = layout.height;
/* 1355 */         TextureAtlas.AtlasRegion orb = identifyOrb(word);
/* 1356 */         if (orb != null) {
/* 1357 */           sb.setColor(new Color(1.0F, 1.0F, 1.0F, c.a));
/* 1358 */           if (layout.width + CARD_ENERGY_IMG_WIDTH <= widthMax * 2.0F)
/*      */           {
/* 1360 */             if (layout.width + CARD_ENERGY_IMG_WIDTH > widthMax) {
/* 1361 */               layout.setText(font, newMsg, 0, newMsg.length(), c, 9999999.0F, -1, true, null);
/* 1362 */               sb.draw(orb, x + layout.width - widthMax - 16.0F * Settings.scale, y - lh + layout.height - CARD_ENERGY_IMG_WIDTH + 2.0F * Settings.scale, CARD_ENERGY_IMG_WIDTH, CARD_ENERGY_IMG_WIDTH);
/*      */ 
/*      */ 
/*      */             }
/*      */             else
/*      */             {
/*      */ 
/* 1369 */               sb.draw(orb, x + layout.width - 28.0F * Settings.scale, y - layout.height + lh - CARD_ENERGY_IMG_WIDTH + 2.0F * Settings.scale, CARD_ENERGY_IMG_WIDTH, CARD_ENERGY_IMG_WIDTH);
/*      */             }
/*      */             
/*      */           }
/*      */           
/*      */         }
/*      */         
/*      */ 
/*      */       }
/* 1378 */       else if (word.charAt(0) == '#') {
/* 1379 */         switch (word.charAt(1)) {
/*      */         case 'r': 
/* 1381 */           word = "[#ff6563]" + word.substring(2) + "[]";
/* 1382 */           break;
/*      */         case 'g': 
/* 1384 */           word = "[#7fff00]" + word.substring(2) + "[]";
/* 1385 */           break;
/*      */         case 'b': 
/* 1387 */           word = "[#87ceeb]" + word.substring(2) + "[]";
/* 1388 */           break;
/*      */         case 'y': 
/* 1390 */           word = "[#efc851]" + word.substring(2) + "[]";
/* 1391 */           break;
/*      */         }
/*      */         
/*      */         
/* 1395 */         newMsg = newMsg + word;
/*      */       } else {
/* 1397 */         newMsg = newMsg + word;
/*      */       }
/*      */     }
/*      */     
/* 1401 */     msg = newMsg;
/*      */     
/* 1403 */     font.setColor(c);
/* 1404 */     if ((msg != null) && (msg.length() > 0)) {
/* 1405 */       font.draw(sb, msg, x, y, 0, msg.length(), widthMax, -1, true, null);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontCenteredTopAligned(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/* 1416 */     layout.setText(font, "lL");
/* 1417 */     font.setColor(c);
/* 1418 */     font.draw(sb, msg, x, y + layout.height / 2.0F, 0.0F, 1, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontCentered(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c, float scale)
/*      */   {
/* 1429 */     font.getData().setScale(scale);
/* 1430 */     layout.setText(font, msg);
/* 1431 */     renderFont(sb, font, msg, x - layout.width / 2.0F, y + layout.height / 2.0F, c);
/* 1432 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */   public static void renderFontCentered(SpriteBatch sb, BitmapFont font, String msg, float x, float y) {
/* 1436 */     layout.setText(font, msg);
/* 1437 */     renderFont(sb, font, msg, x - layout.width / 2.0F, y + layout.height / 2.0F, Color.WHITE);
/*      */   }
/*      */   
/*      */   public static void renderFontCenteredWidth(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c) {
/* 1441 */     layout.setText(font, msg);
/* 1442 */     renderFont(sb, font, msg, x - layout.width / 2.0F, y, c);
/*      */   }
/*      */   
/*      */   public static void renderFontCenteredWidth(SpriteBatch sb, BitmapFont font, String msg, float x, float y) {
/* 1446 */     layout.setText(font, msg);
/* 1447 */     renderFont(sb, font, msg, x - layout.width / 2.0F, y, Color.WHITE);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontCenteredHeight(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float lineWidth, Color c)
/*      */   {
/* 1469 */     layout.setText(font, msg, c, lineWidth, 1, true);
/* 1470 */     font.setColor(c);
/* 1471 */     font.draw(sb, msg, x, y + layout.height / 2.0F, lineWidth, 1, true);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontCenteredHeight(SpriteBatch sb, BitmapFont font, String msg, float x, float y, float lineWidth, Color c, float scale)
/*      */   {
/* 1483 */     font.getData().setScale(scale);
/* 1484 */     layout.setText(font, msg, c, lineWidth, 1, true);
/* 1485 */     font.setColor(c);
/* 1486 */     font.draw(sb, msg, x, y + layout.height / 2.0F, lineWidth, 1, true);
/* 1487 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void renderFontCenteredHeight(SpriteBatch sb, BitmapFont font, String msg, float x, float y, Color c)
/*      */   {
/* 1497 */     layout.setText(font, msg);
/* 1498 */     renderFont(sb, font, msg, x, y + layout.height / 2.0F, c);
/*      */   }
/*      */   
/*      */   public static void renderFontCenteredHeight(SpriteBatch sb, BitmapFont font, String msg, float x, float y) {
/* 1502 */     layout.setText(font, msg);
/* 1503 */     renderFont(sb, font, msg, x, y + layout.height / 2.0F, Color.WHITE);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String colorString(String input, String colorValue)
/*      */   {
/* 1515 */     StringBuilder retVal = new StringBuilder();
/* 1516 */     Scanner s = new Scanner(input);
/* 1517 */     while (s.hasNext()) {
/* 1518 */       retVal.append("#").append(colorValue).append(s.next());
/* 1519 */       retVal.append(' ');
/*      */     }
/* 1521 */     s.close();
/* 1522 */     return retVal.toString().trim();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static float getWidth(BitmapFont font, String text, float scale)
/*      */   {
/* 1534 */     layout.setText(font, text);
/* 1535 */     return layout.width * scale;
/*      */   }
/*      */   
/*      */   public static float getHeight(BitmapFont font, String text, float scale) {
/* 1539 */     layout.setText(font, text);
/* 1540 */     return layout.height * scale;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\FontHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */