/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.io.PrintStream;
/*     */ import java.math.BigInteger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SeedHelper
/*     */ {
/*     */   public static final String CHARACTERS = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
/*  18 */   public static final int SEED_DEFAULT_LENGTH = getString(Long.MIN_VALUE).length();
/*     */   
/*     */   public static void setSeed(String seedStr)
/*     */   {
/*  22 */     if (seedStr.isEmpty()) {
/*  23 */       Settings.seedSet = false;
/*  24 */       Settings.seed = null;
/*  25 */       Settings.specialSeed = null;
/*     */     } else {
/*  27 */       long seed = getLong(seedStr);
/*  28 */       Settings.seedSet = true;
/*  29 */       Settings.seed = Long.valueOf(seed);
/*  30 */       Settings.specialSeed = null;
/*  31 */       Settings.isDailyRun = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public static String getUserFacingSeedString() {
/*  36 */     if (Settings.seed != null) {
/*  37 */       return getString(Settings.seed.longValue());
/*     */     }
/*  39 */     return "";
/*     */   }
/*     */   
/*     */   public static String getValidCharacter(String character, String textSoFar)
/*     */   {
/*  44 */     character = character.toUpperCase();
/*  45 */     if (character.equals("O")) {
/*  46 */       character = "0";
/*     */     }
/*  48 */     boolean isValid = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ".contains(character);
/*  49 */     if (isValid) {
/*  50 */       return character;
/*     */     }
/*  52 */     return null;
/*     */   }
/*     */   
/*     */   public static String sterilizeString(String raw) {
/*  56 */     raw = raw.trim().toUpperCase();
/*     */     
/*  58 */     String pattern = "([A-Z]*[0-9]*)*";
/*  59 */     if (raw.matches("([A-Z]*[0-9]*)*")) {
/*  60 */       return raw.replace("O", "0");
/*     */     }
/*  62 */     return "";
/*     */   }
/*     */   
/*     */   public static String getString(long seed) {
/*  66 */     StringBuilder bldr = new StringBuilder();
/*     */     
/*     */ 
/*  69 */     BigInteger leftover = new BigInteger(Long.toUnsignedString(seed));
/*  70 */     BigInteger charCount = BigInteger.valueOf("0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ".length());
/*     */     
/*  72 */     while (!leftover.equals(BigInteger.ZERO)) {
/*  73 */       BigInteger remainder = leftover.remainder(charCount);
/*  74 */       leftover = leftover.divide(charCount);
/*     */       
/*  76 */       int charIndex = remainder.intValue();
/*  77 */       char c = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ".charAt(charIndex);
/*  78 */       bldr.insert(0, c);
/*     */     }
/*  80 */     return bldr.toString();
/*     */   }
/*     */   
/*     */   public static long getLong(String seedStr) {
/*  84 */     long total = 0L;
/*  85 */     seedStr = seedStr.toUpperCase().replaceAll("O", "0");
/*  86 */     for (int i = 0; i < seedStr.length(); i++) {
/*  87 */       char toFind = seedStr.charAt(i);
/*  88 */       int remainder = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ".indexOf(toFind);
/*  89 */       if (remainder == -1) {
/*  90 */         System.out.println("Character in seed is invalid: " + toFind);
/*     */       }
/*     */       
/*  93 */       total *= "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ".length();
/*  94 */       total += remainder;
/*     */     }
/*  96 */     return total;
/*     */   }
/*     */   
/*     */   public static long generateUnoffensiveSeed(Random rng) {
/* 100 */     String safeString = "fuck";
/*     */     
/* 102 */     while ((BadWordChecker.containsBadWord(safeString)) || (TrialHelper.isTrialSeed(safeString))) {
/* 103 */       long possible = rng.randomLong();
/* 104 */       safeString = getString(possible);
/*     */     }
/* 106 */     return getLong(safeString);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\SeedHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */