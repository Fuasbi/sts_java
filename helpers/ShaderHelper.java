/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.glutils.ShaderProgram;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class ShaderHelper
/*     */ {
/*  15 */   private static final Logger logger = LogManager.getLogger(ShaderHelper.class.getName());
/*     */   private static ShaderProgram gsShader;
/*     */   private static ShaderProgram rsShader;
/*     */   private static ShaderProgram wsShader;
/*     */   private static ShaderProgram blurShader;
/*     */   private static ShaderProgram waterShader;
/*     */   private static ShaderProgram outlineShader;
/*     */   
/*     */   public static void initializeShaders() {
/*  24 */     ShaderProgram.pedantic = false;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  29 */     gsShader = new ShaderProgram(Gdx.files.internal("shaders/grayscale/vertexShader.vs").readString(), Gdx.files.internal("shaders/grayscale/fragShader.fs").readString());
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  34 */     wsShader = new ShaderProgram(Gdx.files.internal("shaders/whiteSilhouette/vertexShader.vs").readString(), Gdx.files.internal("shaders/whiteSilhouette/fragShader.fs").readString());
/*     */     
/*     */ 
/*     */ 
/*  38 */     rsShader = new ShaderProgram(Gdx.files.internal("shaders/redSilhouette/vertexShader.vs").readString(), Gdx.files.internal("shaders/redSilhouette/fragShader.fs").readString());
/*     */     
/*     */ 
/*     */ 
/*  42 */     blurShader = new ShaderProgram(Gdx.files.internal("shaders/blur/vertexShader.vs").readString(), Gdx.files.internal("shaders/blur/fragShader.fs").readString());
/*     */     
/*  44 */     if (blurShader.getLog().length() != 0) {
/*  45 */       logger.info(blurShader.getLog());
/*     */     }
/*  47 */     blurShader.setUniformf("dir", 0.0F, 1.0F);
/*  48 */     blurShader.setUniformf("resolution", 1024.0F);
/*  49 */     blurShader.setUniformf("radius", 5.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*  53 */     waterShader = new ShaderProgram(Gdx.files.internal("shaders/water/vertex.vs").readString(), Gdx.files.internal("shaders/water/fragment.fs").readString());
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  58 */     outlineShader = new ShaderProgram(Gdx.files.internal("shaders/outline/vertex.vs").readString(), Gdx.files.internal("shaders/outline/fragment.fs").readString());
/*  59 */     outlineShader.begin();
/*  60 */     outlineShader.setUniformf("u_viewportInverse", new com.badlogic.gdx.math.Vector2(0.0078125F, 0.0078125F));
/*  61 */     outlineShader.setUniformf("u_offset", 7.0F * Settings.scale);
/*  62 */     outlineShader.setUniformf("u_step", Math.min(1.0F, 128.0F));
/*  63 */     outlineShader.setUniformf("u_color", new com.badlogic.gdx.math.Vector3(0.0F, 0.12F, 0.12F));
/*  64 */     outlineShader.end();
/*     */   }
/*     */   
/*     */   public static void setShader(SpriteBatch sb, Shader shader) {
/*  68 */     switch (shader) {
/*     */     case BLUR: 
/*  70 */       sb.end();
/*  71 */       sb.setShader(blurShader);
/*  72 */       sb.begin();
/*  73 */       break;
/*     */     case DEFAULT: 
/*  75 */       sb.end();
/*  76 */       sb.setShader(null);
/*  77 */       sb.begin();
/*  78 */       break;
/*     */     case GRAYSCALE: 
/*  80 */       sb.end();
/*  81 */       sb.setShader(gsShader);
/*  82 */       sb.begin();
/*  83 */       break;
/*     */     case OUTLINE: 
/*  85 */       sb.end();
/*  86 */       sb.setShader(outlineShader);
/*  87 */       sb.begin();
/*  88 */       break;
/*     */     case RED_SILHOUETTE: 
/*  90 */       sb.end();
/*  91 */       sb.setShader(rsShader);
/*  92 */       sb.begin();
/*  93 */       break;
/*     */     case WATER: 
/*  95 */       sb.end();
/*  96 */       sb.setShader(waterShader);
/*  97 */       sb.begin();
/*  98 */       break;
/*     */     case WHITE_SILHOUETTE: 
/* 100 */       sb.end();
/* 101 */       sb.setShader(wsShader);
/* 102 */       sb.begin();
/* 103 */       break;
/*     */     default: 
/* 105 */       sb.end();
/* 106 */       sb.setShader(null);
/* 107 */       sb.begin();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void setShader(PolygonSpriteBatch sb, Shader shader)
/*     */   {
/* 113 */     switch (shader) {
/*     */     case BLUR: 
/* 115 */       sb.setShader(blurShader);
/* 116 */       break;
/*     */     case DEFAULT: 
/* 118 */       sb.setShader(null);
/* 119 */       break;
/*     */     case GRAYSCALE: 
/* 121 */       sb.setShader(gsShader);
/* 122 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     default: 
/* 132 */       sb.setShader(null);
/*     */     }
/*     */   }
/*     */   
/*     */   public static enum Shader
/*     */   {
/* 138 */     BLUR,  DEFAULT,  GRAYSCALE,  RED_SILHOUETTE,  WHITE_SILHOUETTE,  OUTLINE,  WATER;
/*     */     
/*     */     private Shader() {}
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\ShaderHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */