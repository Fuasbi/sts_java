/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.relics.CoffeeDripper;
/*     */ import com.megacrit.cardcrawl.relics.Lantern;
/*     */ import com.megacrit.cardcrawl.relics.RunicPyramid;
/*     */ import com.megacrit.cardcrawl.relics.TwistedFunnel;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map.Entry;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class RelicLibrary
/*     */ {
/*  21 */   private static final Logger logger = LogManager.getLogger(RelicLibrary.class.getName());
/*  22 */   public static int totalRelicCount = 0;
/*  23 */   public static int seenRelics = 0;
/*     */   
/*  25 */   private static HashMap<String, AbstractRelic> sharedRelics = new HashMap();
/*  26 */   private static HashMap<String, AbstractRelic> redRelics = new HashMap();
/*  27 */   private static HashMap<String, AbstractRelic> greenRelics = new HashMap();
/*  28 */   private static HashMap<String, AbstractRelic> blueRelics = new HashMap();
/*     */   
/*     */ 
/*  31 */   public static ArrayList<AbstractRelic> starterList = new ArrayList();
/*  32 */   public static ArrayList<AbstractRelic> commonList = new ArrayList();
/*  33 */   public static ArrayList<AbstractRelic> uncommonList = new ArrayList();
/*  34 */   public static ArrayList<AbstractRelic> rareList = new ArrayList();
/*  35 */   public static ArrayList<AbstractRelic> bossList = new ArrayList();
/*  36 */   public static ArrayList<AbstractRelic> specialList = new ArrayList();
/*  37 */   public static ArrayList<AbstractRelic> shopList = new ArrayList();
/*  38 */   public static ArrayList<AbstractRelic> redList = new ArrayList();
/*  39 */   public static ArrayList<AbstractRelic> greenList = new ArrayList();
/*  40 */   public static ArrayList<AbstractRelic> blueList = new ArrayList();
/*     */   
/*     */ 
/*     */ 
/*     */   public static void initialize()
/*     */   {
/*  46 */     long startTime = System.currentTimeMillis();
/*     */     
/*  48 */     add(new com.megacrit.cardcrawl.relics.Anchor());
/*  49 */     add(new com.megacrit.cardcrawl.relics.AncientTeaSet());
/*  50 */     add(new com.megacrit.cardcrawl.relics.ArtOfWar());
/*  51 */     add(new com.megacrit.cardcrawl.relics.Astrolabe());
/*  52 */     add(new com.megacrit.cardcrawl.relics.BagOfMarbles());
/*  53 */     add(new com.megacrit.cardcrawl.relics.BagOfPreparation());
/*  54 */     add(new com.megacrit.cardcrawl.relics.BirdFacedUrn());
/*  55 */     add(new com.megacrit.cardcrawl.relics.BlackStar());
/*  56 */     add(new com.megacrit.cardcrawl.relics.BloodVial());
/*  57 */     add(new com.megacrit.cardcrawl.relics.BloodyIdol());
/*  58 */     add(new com.megacrit.cardcrawl.relics.BlueCandle());
/*  59 */     add(new com.megacrit.cardcrawl.relics.Boot());
/*  60 */     add(new com.megacrit.cardcrawl.relics.BottledFlame());
/*  61 */     add(new com.megacrit.cardcrawl.relics.BottledLightning());
/*  62 */     add(new com.megacrit.cardcrawl.relics.BottledTornado());
/*  63 */     add(new com.megacrit.cardcrawl.relics.BronzeScales());
/*  64 */     add(new com.megacrit.cardcrawl.relics.BustedCrown());
/*  65 */     add(new com.megacrit.cardcrawl.relics.Calipers());
/*  66 */     add(new com.megacrit.cardcrawl.relics.CallingBell());
/*  67 */     add(new com.megacrit.cardcrawl.relics.Cauldron());
/*  68 */     add(new com.megacrit.cardcrawl.relics.CentennialPuzzle());
/*  69 */     add(new com.megacrit.cardcrawl.relics.ChemicalX());
/*  70 */     add(new com.megacrit.cardcrawl.relics.ClockworkSouvenir());
/*  71 */     add(new CoffeeDripper());
/*  72 */     add(new com.megacrit.cardcrawl.relics.Courier());
/*  73 */     add(new com.megacrit.cardcrawl.relics.CultistMask());
/*  74 */     add(new com.megacrit.cardcrawl.relics.CursedKey());
/*  75 */     add(new com.megacrit.cardcrawl.relics.DarkstonePeriapt());
/*  76 */     add(new com.megacrit.cardcrawl.relics.DeadBranch());
/*  77 */     add(new com.megacrit.cardcrawl.relics.Dodecahedron());
/*  78 */     add(new com.megacrit.cardcrawl.relics.DollysMirror());
/*  79 */     add(new com.megacrit.cardcrawl.relics.DreamCatcher());
/*  80 */     add(new com.megacrit.cardcrawl.relics.DuVuDoll());
/*  81 */     add(new com.megacrit.cardcrawl.relics.Ectoplasm());
/*  82 */     add(new com.megacrit.cardcrawl.relics.EmptyCage());
/*  83 */     add(new com.megacrit.cardcrawl.relics.Enchiridion());
/*  84 */     add(new com.megacrit.cardcrawl.relics.EternalFeather());
/*  85 */     add(new com.megacrit.cardcrawl.relics.FaceOfCleric());
/*  86 */     add(new com.megacrit.cardcrawl.relics.FrozenEgg2());
/*  87 */     add(new com.megacrit.cardcrawl.relics.FrozenEye());
/*  88 */     add(new com.megacrit.cardcrawl.relics.FusionHammer());
/*  89 */     add(new com.megacrit.cardcrawl.relics.GamblingChip());
/*  90 */     add(new com.megacrit.cardcrawl.relics.Ginger());
/*  91 */     add(new com.megacrit.cardcrawl.relics.Girya());
/*  92 */     add(new com.megacrit.cardcrawl.relics.GoldenIdol());
/*  93 */     add(new com.megacrit.cardcrawl.relics.GremlinHorn());
/*  94 */     add(new com.megacrit.cardcrawl.relics.GremlinMask());
/*  95 */     add(new com.megacrit.cardcrawl.relics.HandDrill());
/*  96 */     add(new com.megacrit.cardcrawl.relics.HappyFlower());
/*  97 */     add(new com.megacrit.cardcrawl.relics.IceCream());
/*  98 */     add(new com.megacrit.cardcrawl.relics.IncenseBurner());
/*  99 */     add(new com.megacrit.cardcrawl.relics.JuzuBracelet());
/* 100 */     add(new com.megacrit.cardcrawl.relics.Kunai());
/* 101 */     add(new Lantern());
/* 102 */     add(new com.megacrit.cardcrawl.relics.LetterOpener());
/* 103 */     add(new com.megacrit.cardcrawl.relics.LizardTail());
/* 104 */     add(new com.megacrit.cardcrawl.relics.Mango());
/* 105 */     add(new com.megacrit.cardcrawl.relics.MarkOfTheBloom());
/* 106 */     add(new com.megacrit.cardcrawl.relics.Matryoshka());
/* 107 */     add(new com.megacrit.cardcrawl.relics.MawBank());
/* 108 */     add(new com.megacrit.cardcrawl.relics.MealTicket());
/* 109 */     add(new com.megacrit.cardcrawl.relics.MeatOnTheBone());
/* 110 */     add(new com.megacrit.cardcrawl.relics.MedicalKit());
/* 111 */     add(new com.megacrit.cardcrawl.relics.MembershipCard());
/* 112 */     add(new com.megacrit.cardcrawl.relics.MercuryHourglass());
/* 113 */     add(new com.megacrit.cardcrawl.relics.MoltenEgg2());
/* 114 */     add(new com.megacrit.cardcrawl.relics.MummifiedHand());
/* 115 */     add(new com.megacrit.cardcrawl.relics.Necronomicon());
/* 116 */     add(new com.megacrit.cardcrawl.relics.NeowsLament());
/* 117 */     add(new com.megacrit.cardcrawl.relics.NilrysCodex());
/* 118 */     add(new com.megacrit.cardcrawl.relics.NlothsGift());
/* 119 */     add(new com.megacrit.cardcrawl.relics.NlothsMask());
/* 120 */     add(new com.megacrit.cardcrawl.relics.Nunchaku());
/* 121 */     add(new com.megacrit.cardcrawl.relics.OddlySmoothStone());
/* 122 */     add(new com.megacrit.cardcrawl.relics.OddMushroom());
/* 123 */     add(new com.megacrit.cardcrawl.relics.OldCoin());
/* 124 */     add(new com.megacrit.cardcrawl.relics.Omamori());
/* 125 */     add(new com.megacrit.cardcrawl.relics.OrangePellets());
/* 126 */     add(new com.megacrit.cardcrawl.relics.Orichalcum());
/* 127 */     add(new com.megacrit.cardcrawl.relics.OrnamentalFan());
/* 128 */     add(new com.megacrit.cardcrawl.relics.Orrery());
/* 129 */     add(new com.megacrit.cardcrawl.relics.PandorasBox());
/* 130 */     add(new com.megacrit.cardcrawl.relics.Pantograph());
/* 131 */     add(new com.megacrit.cardcrawl.relics.PeacePipe());
/* 132 */     add(new com.megacrit.cardcrawl.relics.Pear());
/* 133 */     add(new com.megacrit.cardcrawl.relics.PenNib());
/* 134 */     add(new com.megacrit.cardcrawl.relics.PhilosopherStone());
/* 135 */     add(new com.megacrit.cardcrawl.relics.PotionBelt());
/* 136 */     add(new com.megacrit.cardcrawl.relics.PrayerWheel());
/* 137 */     add(new com.megacrit.cardcrawl.relics.QuestionCard());
/* 138 */     add(new com.megacrit.cardcrawl.relics.RedMask());
/* 139 */     add(new com.megacrit.cardcrawl.relics.RegalPillow());
/* 140 */     add(new com.megacrit.cardcrawl.relics.RunicDome());
/* 141 */     add(new RunicPyramid());
/* 142 */     add(new com.megacrit.cardcrawl.relics.Shovel());
/* 143 */     add(new com.megacrit.cardcrawl.relics.Shuriken());
/* 144 */     add(new com.megacrit.cardcrawl.relics.SingingBowl());
/* 145 */     add(new com.megacrit.cardcrawl.relics.Sling());
/* 146 */     add(new com.megacrit.cardcrawl.relics.SmilingMask());
/* 147 */     add(new com.megacrit.cardcrawl.relics.SneckoEye());
/* 148 */     add(new com.megacrit.cardcrawl.relics.Sozu());
/* 149 */     add(new com.megacrit.cardcrawl.relics.SpiritPoop());
/* 150 */     add(new com.megacrit.cardcrawl.relics.SsserpentHead());
/* 151 */     add(new com.megacrit.cardcrawl.relics.StoneCalendar());
/* 152 */     add(new com.megacrit.cardcrawl.relics.StrangeSpoon());
/* 153 */     add(new com.megacrit.cardcrawl.relics.Strawberry());
/* 154 */     add(new com.megacrit.cardcrawl.relics.Sundial());
/* 155 */     add(new com.megacrit.cardcrawl.relics.TheAbacus());
/* 156 */     add(new com.megacrit.cardcrawl.relics.ThreadAndNeedle());
/* 157 */     add(new com.megacrit.cardcrawl.relics.TinyChest());
/* 158 */     add(new com.megacrit.cardcrawl.relics.TinyHouse());
/* 159 */     add(new com.megacrit.cardcrawl.relics.Toolbox());
/* 160 */     add(new com.megacrit.cardcrawl.relics.Torii());
/* 161 */     add(new com.megacrit.cardcrawl.relics.ToxicEgg2());
/* 162 */     add(new com.megacrit.cardcrawl.relics.ToyOrnithopter());
/* 163 */     add(new com.megacrit.cardcrawl.relics.Turnip());
/* 164 */     add(new com.megacrit.cardcrawl.relics.UnceasingTop());
/* 165 */     add(new com.megacrit.cardcrawl.relics.Vajra());
/* 166 */     add(new com.megacrit.cardcrawl.relics.VelvetChoker());
/* 167 */     add(new com.megacrit.cardcrawl.relics.Waffle());
/* 168 */     add(new com.megacrit.cardcrawl.relics.WarPaint());
/* 169 */     add(new com.megacrit.cardcrawl.relics.Whetstone());
/* 170 */     add(new com.megacrit.cardcrawl.relics.WhiteBeast());
/* 171 */     add(new com.megacrit.cardcrawl.relics.MutagenicStrength());
/*     */     
/*     */ 
/* 174 */     addGreen(new com.megacrit.cardcrawl.relics.HoveringKite());
/* 175 */     addGreen(new com.megacrit.cardcrawl.relics.NinjaScroll());
/* 176 */     addGreen(new com.megacrit.cardcrawl.relics.PaperCrane());
/* 177 */     addGreen(new com.megacrit.cardcrawl.relics.RingOfTheSerpent());
/* 178 */     addGreen(new com.megacrit.cardcrawl.relics.SnakeRing());
/* 179 */     addGreen(new com.megacrit.cardcrawl.relics.SneckoSkull());
/* 180 */     addGreen(new com.megacrit.cardcrawl.relics.TheSpecimen());
/* 181 */     addGreen(new com.megacrit.cardcrawl.relics.Tingsha());
/* 182 */     addGreen(new com.megacrit.cardcrawl.relics.ToughBandages());
/* 183 */     addGreen(new TwistedFunnel());
/* 184 */     addGreen(new com.megacrit.cardcrawl.relics.WristBlade());
/*     */     
/*     */ 
/* 187 */     addRed(new com.megacrit.cardcrawl.relics.BlackBlood());
/* 188 */     addRed(new com.megacrit.cardcrawl.relics.BurningBlood());
/* 189 */     addRed(new com.megacrit.cardcrawl.relics.ChampionsBelt());
/* 190 */     addRed(new com.megacrit.cardcrawl.relics.CharonsAshes());
/* 191 */     addRed(new com.megacrit.cardcrawl.relics.MagicFlower());
/* 192 */     addRed(new com.megacrit.cardcrawl.relics.MarkOfPain());
/* 193 */     addRed(new com.megacrit.cardcrawl.relics.PaperFrog());
/* 194 */     addRed(new com.megacrit.cardcrawl.relics.RedSkull());
/* 195 */     addRed(new com.megacrit.cardcrawl.relics.RunicCube());
/* 196 */     addRed(new com.megacrit.cardcrawl.relics.SelfFormingClay());
/*     */     
/*     */ 
/* 199 */     addBlue(new com.megacrit.cardcrawl.relics.CrackedCore());
/* 200 */     addBlue(new com.megacrit.cardcrawl.relics.DataDisk());
/* 201 */     addBlue(new com.megacrit.cardcrawl.relics.EmotionChip());
/* 202 */     addBlue(new com.megacrit.cardcrawl.relics.FrozenCore());
/* 203 */     addBlue(new com.megacrit.cardcrawl.relics.GoldPlatedCables());
/* 204 */     addBlue(new com.megacrit.cardcrawl.relics.Inserter());
/* 205 */     addBlue(new com.megacrit.cardcrawl.relics.NuclearBattery());
/* 206 */     addBlue(new com.megacrit.cardcrawl.relics.RunicCapacitor());
/* 207 */     addBlue(new com.megacrit.cardcrawl.relics.SymbioticVirus());
/*     */     
/*     */ 
/*     */ 
/* 211 */     if (Settings.isBeta) {}
/*     */     
/*     */ 
/*     */ 
/* 215 */     logger.info("Relic load time: " + (System.currentTimeMillis() - startTime) + "ms");
/* 216 */     sortLists();
/*     */   }
/*     */   
/*     */   public static void resetForReload()
/*     */   {
/* 221 */     totalRelicCount = 0;
/* 222 */     seenRelics = 0;
/* 223 */     sharedRelics.clear();
/* 224 */     redRelics.clear();
/* 225 */     greenRelics.clear();
/* 226 */     blueRelics.clear();
/* 227 */     starterList.clear();
/* 228 */     commonList.clear();
/* 229 */     uncommonList.clear();
/* 230 */     rareList.clear();
/* 231 */     bossList.clear();
/* 232 */     specialList.clear();
/* 233 */     shopList.clear();
/* 234 */     redList.clear();
/* 235 */     greenList.clear();
/* 236 */     blueList.clear();
/*     */   }
/*     */   
/*     */   private static void sortLists() {
/* 240 */     Collections.sort(starterList);
/* 241 */     Collections.sort(commonList);
/* 242 */     Collections.sort(uncommonList);
/* 243 */     Collections.sort(rareList);
/* 244 */     Collections.sort(bossList);
/* 245 */     Collections.sort(specialList);
/* 246 */     Collections.sort(shopList);
/*     */     
/* 248 */     if (Settings.isDev) {
/* 249 */       logger.info(starterList);
/* 250 */       logger.info(commonList);
/* 251 */       logger.info(uncommonList);
/* 252 */       logger.info(rareList);
/* 253 */       logger.info(bossList);
/*     */     }
/*     */   }
/*     */   
/*     */   private static void printRelicCount()
/*     */   {
/* 259 */     int common = 0;int uncommon = 0;int rare = 0;int boss = 0;int shop = 0;int other = 0;
/*     */     
/* 261 */     for (Map.Entry<String, AbstractRelic> r : sharedRelics.entrySet()) {
/* 262 */       switch (((AbstractRelic)r.getValue()).tier) {
/*     */       case COMMON: 
/* 264 */         common++;
/* 265 */         break;
/*     */       case UNCOMMON: 
/* 267 */         uncommon++;
/* 268 */         break;
/*     */       case RARE: 
/* 270 */         rare++;
/* 271 */         break;
/*     */       case BOSS: 
/* 273 */         boss++;
/* 274 */         break;
/*     */       case SHOP: 
/* 276 */         shop++;
/* 277 */         break;
/*     */       default: 
/* 279 */         other++;
/*     */       }
/*     */       
/*     */     }
/* 283 */     if (Settings.isDev) {
/* 284 */       logger.info("RELIC COUNTS");
/* 285 */       logger.info("Common: " + common);
/* 286 */       logger.info("Uncommon: " + uncommon);
/* 287 */       logger.info("Rare: " + rare);
/* 288 */       logger.info("Boss: " + boss);
/* 289 */       logger.info("Shop: " + shop);
/* 290 */       logger.info("Other: " + other);
/* 291 */       logger.info("Red: " + redRelics.size());
/* 292 */       logger.info("Green: " + greenRelics.size());
/* 293 */       logger.info("Blue: " + blueRelics.size());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void add(AbstractRelic relic)
/*     */   {
/* 301 */     if (UnlockTracker.isRelicSeen(relic.relicId)) {
/* 302 */       seenRelics += 1;
/*     */     }
/* 304 */     relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/* 305 */     sharedRelics.put(relic.relicId, relic);
/* 306 */     addToTierList(relic);
/* 307 */     totalRelicCount += 1;
/*     */   }
/*     */   
/*     */   public static void addRed(AbstractRelic relic) {
/* 311 */     if (UnlockTracker.isRelicSeen(relic.relicId)) {
/* 312 */       seenRelics += 1;
/*     */     }
/* 314 */     relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/* 315 */     redRelics.put(relic.relicId, relic);
/* 316 */     addToTierList(relic);
/* 317 */     redList.add(relic);
/* 318 */     totalRelicCount += 1;
/*     */   }
/*     */   
/*     */   public static void addGreen(AbstractRelic relic) {
/* 322 */     if (UnlockTracker.isRelicSeen(relic.relicId)) {
/* 323 */       seenRelics += 1;
/*     */     }
/* 325 */     relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/* 326 */     greenRelics.put(relic.relicId, relic);
/* 327 */     addToTierList(relic);
/* 328 */     greenList.add(relic);
/* 329 */     totalRelicCount += 1;
/*     */   }
/*     */   
/*     */   public static void addBlue(AbstractRelic relic) {
/* 333 */     if (UnlockTracker.isRelicSeen(relic.relicId)) {
/* 334 */       seenRelics += 1;
/*     */     }
/* 336 */     relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/* 337 */     blueRelics.put(relic.relicId, relic);
/* 338 */     addToTierList(relic);
/* 339 */     blueList.add(relic);
/* 340 */     totalRelicCount += 1;
/*     */   }
/*     */   
/*     */   public static void addToTierList(AbstractRelic relic) {
/* 344 */     switch (relic.tier) {
/*     */     case STARTER: 
/* 346 */       starterList.add(relic);
/* 347 */       break;
/*     */     case COMMON: 
/* 349 */       commonList.add(relic);
/* 350 */       break;
/*     */     case UNCOMMON: 
/* 352 */       uncommonList.add(relic);
/* 353 */       break;
/*     */     case RARE: 
/* 355 */       rareList.add(relic);
/* 356 */       break;
/*     */     case SHOP: 
/* 358 */       shopList.add(relic);
/* 359 */       break;
/*     */     case SPECIAL: 
/* 361 */       specialList.add(relic);
/* 362 */       break;
/*     */     case BOSS: 
/* 364 */       bossList.add(relic);
/* 365 */       break;
/*     */     case DEPRECATED: 
/* 367 */       logger.info(relic.relicId + " is deprecated.");
/* 368 */       break;
/*     */     default: 
/* 370 */       logger.info(relic.relicId + " is undefined tier.");
/*     */     }
/*     */   }
/*     */   
/*     */   public static AbstractRelic getRelic(String key)
/*     */   {
/* 376 */     if (sharedRelics.containsKey(key))
/* 377 */       return (AbstractRelic)sharedRelics.get(key);
/* 378 */     if (redRelics.containsKey(key))
/* 379 */       return (AbstractRelic)redRelics.get(key);
/* 380 */     if (greenRelics.containsKey(key))
/* 381 */       return (AbstractRelic)greenRelics.get(key);
/* 382 */     if (blueRelics.containsKey(key)) {
/* 383 */       return (AbstractRelic)blueRelics.get(key);
/*     */     }
/*     */     
/* 386 */     return new com.megacrit.cardcrawl.relics.Circlet();
/*     */   }
/*     */   
/*     */   public static boolean isARelic(String key) {
/* 390 */     return (sharedRelics.containsKey(key)) || (redRelics.containsKey(key)) || (greenRelics.containsKey(key)) || 
/* 391 */       (blueRelics.containsKey(key));
/*     */   }
/*     */   
/*     */   public static void populateRelicPool(ArrayList<String> pool, AbstractRelic.RelicTier tier, AbstractPlayer.PlayerClass c) {
/* 395 */     for (Map.Entry<String, AbstractRelic> r : sharedRelics.entrySet()) {
/* 396 */       if ((((AbstractRelic)r.getValue()).tier == tier) && (
/* 397 */         (!UnlockTracker.isRelicLocked((String)r.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 398 */         pool.add(r.getKey());
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 403 */     switch (c) {
/*     */     case IRONCLAD: 
/* 405 */       for (Map.Entry<String, AbstractRelic> r : redRelics.entrySet()) {
/* 406 */         if ((((AbstractRelic)r.getValue()).tier == tier) && (
/* 407 */           (!UnlockTracker.isRelicLocked((String)r.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 408 */           pool.add(r.getKey());
/*     */         }
/*     */       }
/*     */       
/* 412 */       break;
/*     */     case THE_SILENT: 
/* 414 */       for (Map.Entry<String, AbstractRelic> r : greenRelics.entrySet()) {
/* 415 */         if ((((AbstractRelic)r.getValue()).tier == tier) && (
/* 416 */           (!UnlockTracker.isRelicLocked((String)r.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 417 */           pool.add(r.getKey());
/*     */         }
/*     */       }
/*     */       
/* 421 */       break;
/*     */     case DEFECT: 
/* 423 */       for (Map.Entry<String, AbstractRelic> r : blueRelics.entrySet()) {
/* 424 */         if ((((AbstractRelic)r.getValue()).tier == tier) && (
/* 425 */           (!UnlockTracker.isRelicLocked((String)r.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 426 */           pool.add(r.getKey());
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public static void addSharedRelics(ArrayList<AbstractRelic> relicPool)
/*     */   {
/* 435 */     if (Settings.isDev) {
/* 436 */       logger.info("[RELIC] Adding " + sharedRelics.size() + " shared relics...");
/*     */     }
/* 438 */     for (Map.Entry<String, AbstractRelic> r : sharedRelics.entrySet()) {
/* 439 */       relicPool.add(r.getValue());
/*     */     }
/*     */   }
/*     */   
/*     */   public static void addClassSpecificRelics(ArrayList<AbstractRelic> relicPool) {
/* 444 */     switch (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 446 */       if (Settings.isDev) {
/* 447 */         logger.info("[RELIC] Adding " + redRelics.size() + " red relics...");
/*     */       }
/* 449 */       for (Map.Entry<String, AbstractRelic> r : redRelics.entrySet()) {
/* 450 */         relicPool.add(r.getValue());
/*     */       }
/* 452 */       break;
/*     */     case THE_SILENT: 
/* 454 */       if (Settings.isDev) {
/* 455 */         logger.info("[RELIC] Adding " + greenRelics.size() + " green relics...");
/*     */       }
/* 457 */       for (Map.Entry<String, AbstractRelic> r : greenRelics.entrySet()) {
/* 458 */         relicPool.add(r.getValue());
/*     */       }
/* 460 */       break;
/*     */     case DEFECT: 
/* 462 */       if (Settings.isDev) {
/* 463 */         logger.info("[RELIC] Adding " + blueRelics.size() + " blue relics...");
/*     */       }
/* 465 */       for (Map.Entry<String, AbstractRelic> r : blueRelics.entrySet()) {
/* 466 */         relicPool.add(r.getValue());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public static void uploadRelicData()
/*     */   {
/* 473 */     ArrayList<String> data = new ArrayList();
/* 474 */     for (Map.Entry<String, AbstractRelic> r : sharedRelics.entrySet()) {
/* 475 */       data.add(((AbstractRelic)r.getValue()).gameDataUploadData("All"));
/*     */     }
/* 477 */     for (Map.Entry<String, AbstractRelic> r : redRelics.entrySet()) {
/* 478 */       data.add(((AbstractRelic)r.getValue()).gameDataUploadData("Red"));
/*     */     }
/* 480 */     for (Map.Entry<String, AbstractRelic> r : greenRelics.entrySet()) {
/* 481 */       data.add(((AbstractRelic)r.getValue()).gameDataUploadData("Green"));
/*     */     }
/* 483 */     for (Map.Entry<String, AbstractRelic> r : blueRelics.entrySet()) {
/* 484 */       data.add(((AbstractRelic)r.getValue()).gameDataUploadData("Blue"));
/*     */     }
/*     */     
/* 487 */     com.megacrit.cardcrawl.metrics.BotDataUploader.uploadDataAsync(com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType.RELIC_DATA, AbstractRelic.gameDataUploadHeader(), data);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ArrayList<AbstractRelic> sortByName(ArrayList<AbstractRelic> group, boolean ascending)
/*     */   {
/* 498 */     ArrayList<AbstractRelic> tmp = new ArrayList();
/*     */     
/*     */ 
/*     */ 
/* 502 */     for (AbstractRelic r : group) {
/* 503 */       int addIndex = 0;
/* 504 */       for (AbstractRelic r2 : tmp) {
/* 505 */         if (!ascending ? 
/* 506 */           r.name.compareTo(r2.name) < 0 : 
/*     */           
/*     */ 
/*     */ 
/* 510 */           r.name.compareTo(r2.name) > 0) {
/*     */           break;
/*     */         }
/*     */         
/* 514 */         addIndex++;
/*     */       }
/* 516 */       tmp.add(addIndex, r);
/*     */     }
/* 518 */     return tmp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ArrayList<AbstractRelic> sortByStatus(ArrayList<AbstractRelic> group, boolean ascending)
/*     */   {
/* 529 */     ArrayList<AbstractRelic> tmp = new ArrayList();
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 534 */     for (AbstractRelic r : group) {
/* 535 */       int addIndex = 0;
/* 536 */       for (AbstractRelic r2 : tmp) {
/* 537 */         if (!ascending) { String a;
/* 538 */           String a; if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 539 */             a = "LOCKED"; } else { String a;
/* 540 */             if (UnlockTracker.isRelicSeen(r.relicId)) {
/* 541 */               a = "UNSEEN";
/*     */             } else
/* 543 */               a = "SEEN"; }
/*     */           String b;
/*     */           String b;
/* 546 */           if (UnlockTracker.isRelicLocked(r2.relicId)) {
/* 547 */             b = "LOCKED"; } else { String b;
/* 548 */             if (UnlockTracker.isRelicSeen(r2.relicId)) {
/* 549 */               b = "UNSEEN";
/*     */             } else {
/* 551 */               b = "SEEN";
/*     */             }
/*     */           }
/* 554 */           if (a.compareTo(b) > 0)
/*     */             break;
/*     */         } else { String a;
/*     */           String a;
/* 558 */           if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 559 */             a = "LOCKED"; } else { String a;
/* 560 */             if (UnlockTracker.isRelicSeen(r.relicId)) {
/* 561 */               a = "UNSEEN";
/*     */             } else
/* 563 */               a = "SEEN"; }
/*     */           String b;
/*     */           String b;
/* 566 */           if (UnlockTracker.isRelicLocked(r2.relicId)) {
/* 567 */             b = "LOCKED"; } else { String b;
/* 568 */             if (UnlockTracker.isRelicSeen(r2.relicId)) {
/* 569 */               b = "UNSEEN";
/*     */             } else {
/* 571 */               b = "SEEN";
/*     */             }
/*     */           }
/* 574 */           if (a.compareTo(b) < 0) {
/*     */             break;
/*     */           }
/*     */         }
/* 578 */         addIndex++;
/*     */       }
/* 580 */       tmp.add(addIndex, r);
/*     */     }
/* 582 */     return tmp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void unlockAndSeeAllRelics()
/*     */   {
/* 590 */     for (String s : UnlockTracker.lockedRelics) {
/* 591 */       UnlockTracker.hardUnlockOverride(s);
/*     */     }
/*     */     
/*     */ 
/* 595 */     for (Map.Entry<String, AbstractRelic> r : sharedRelics.entrySet()) {
/* 596 */       UnlockTracker.markRelicAsSeen((String)r.getKey());
/*     */     }
/* 598 */     for (Map.Entry<String, AbstractRelic> r : redRelics.entrySet()) {
/* 599 */       UnlockTracker.markRelicAsSeen((String)r.getKey());
/*     */     }
/* 601 */     for (Map.Entry<String, AbstractRelic> r : greenRelics.entrySet()) {
/* 602 */       UnlockTracker.markRelicAsSeen((String)r.getKey());
/*     */     }
/* 604 */     for (Map.Entry<String, AbstractRelic> r : blueRelics.entrySet()) {
/* 605 */       UnlockTracker.markRelicAsSeen((String)r.getKey());
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\RelicLibrary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */