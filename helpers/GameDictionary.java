/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.megacrit.cardcrawl.localization.Keyword;
/*     */ import com.megacrit.cardcrawl.localization.KeywordStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import java.util.TreeMap;
/*     */ 
/*     */ public class GameDictionary
/*     */ {
/*  10 */   private static final KeywordStrings keywordStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getKeywordString("Game Dictionary");
/*  11 */   public static final String[] TEXT = keywordStrings.TEXT;
/*  12 */   public static final Keyword ARTIFACT = keywordStrings.ARTIFACT;
/*  13 */   public static final Keyword BLOCK = keywordStrings.BLOCK;
/*  14 */   public static final Keyword EVOKE = keywordStrings.EVOKE;
/*  15 */   public static final Keyword CONFUSED = keywordStrings.CONFUSED;
/*  16 */   public static final Keyword CHANNEL = keywordStrings.CHANNEL;
/*  17 */   public static final Keyword CURSE = keywordStrings.CURSE;
/*  18 */   public static final Keyword DARK = keywordStrings.DARK;
/*  19 */   public static final Keyword DEXTERITY = keywordStrings.DEXTERITY;
/*  20 */   public static final Keyword ETHEREAL = keywordStrings.ETHEREAL;
/*  21 */   public static final Keyword EXHAUST = keywordStrings.EXHAUST;
/*  22 */   public static final Keyword FRAIL = keywordStrings.FRAIL;
/*  23 */   public static final Keyword FROST = keywordStrings.FROST;
/*  24 */   public static final Keyword INNATE = keywordStrings.INNATE;
/*  25 */   public static final Keyword INTANGIBLE = keywordStrings.INTANGIBLE;
/*  26 */   public static final Keyword FOCUS = keywordStrings.FOCUS;
/*  27 */   public static final Keyword LIGHTNING = keywordStrings.LIGHTNING;
/*  28 */   public static final Keyword LOCKED = keywordStrings.LOCKED;
/*  29 */   public static final Keyword LOCK_ON = keywordStrings.LOCK_ON;
/*  30 */   public static final Keyword OPENER = keywordStrings.OPENER;
/*  31 */   public static final Keyword PLASMA = keywordStrings.PLASMA;
/*  32 */   public static final Keyword POISON = keywordStrings.POISON;
/*  33 */   public static final Keyword RETAIN = keywordStrings.RETAIN;
/*  34 */   public static final Keyword SHIV = keywordStrings.SHIV;
/*  35 */   public static final Keyword STATUS = keywordStrings.STATUS;
/*  36 */   public static final Keyword STRENGTH = keywordStrings.STRENGTH;
/*  37 */   public static final Keyword STRIKE = keywordStrings.STRIKE;
/*  38 */   public static final Keyword TRANSFORM = keywordStrings.TRANSFORM;
/*  39 */   public static final Keyword UNKNOWN = keywordStrings.UNKNOWN;
/*  40 */   public static final Keyword UNPLAYABLE = keywordStrings.UNPLAYABLE;
/*  41 */   public static final Keyword UPGRADE = keywordStrings.UPGRADE;
/*  42 */   public static final Keyword VOID = keywordStrings.VOID;
/*  43 */   public static final Keyword VULNERABLE = keywordStrings.VULNERABLE;
/*  44 */   public static final Keyword WEAK = keywordStrings.WEAK;
/*  45 */   public static final Keyword WOUND = keywordStrings.WOUND;
/*  46 */   public static final Keyword DAZED = keywordStrings.DAZED;
/*  47 */   public static final Keyword BURN = keywordStrings.BURN;
/*  48 */   public static final Keyword THORNS = keywordStrings.THORNS;
/*     */   
/*  50 */   public static final TreeMap<String, String> keywords = new TreeMap();
/*  51 */   public static final TreeMap<String, String> parentWord = new TreeMap();
/*     */   
/*     */   public static void initialize() {
/*  54 */     keywords.put("[R]", TEXT[0]);
/*  55 */     keywords.put("[G]", TEXT[0]);
/*  56 */     keywords.put("[B]", TEXT[0]);
/*     */     
/*  58 */     createDictionaryEntry(ARTIFACT.NAMES, ARTIFACT.DESCRIPTION);
/*  59 */     createDictionaryEntry(BLOCK.NAMES, BLOCK.DESCRIPTION);
/*  60 */     createDictionaryEntry(EVOKE.NAMES, EVOKE.DESCRIPTION);
/*  61 */     createDictionaryEntry(CONFUSED.NAMES, CONFUSED.DESCRIPTION);
/*  62 */     createDictionaryEntry(CHANNEL.NAMES, CHANNEL.DESCRIPTION);
/*  63 */     createDictionaryEntry(CURSE.NAMES, CURSE.DESCRIPTION);
/*  64 */     createDictionaryEntry(DARK.NAMES, DARK.DESCRIPTION);
/*  65 */     createDictionaryEntry(DEXTERITY.NAMES, DEXTERITY.DESCRIPTION);
/*  66 */     createDictionaryEntry(ETHEREAL.NAMES, ETHEREAL.DESCRIPTION);
/*  67 */     createDictionaryEntry(EXHAUST.NAMES, EXHAUST.DESCRIPTION);
/*  68 */     createDictionaryEntry(FRAIL.NAMES, FRAIL.DESCRIPTION);
/*  69 */     createDictionaryEntry(FROST.NAMES, FROST.DESCRIPTION);
/*  70 */     createDictionaryEntry(INNATE.NAMES, INNATE.DESCRIPTION);
/*  71 */     createDictionaryEntry(FOCUS.NAMES, FOCUS.DESCRIPTION);
/*  72 */     createDictionaryEntry(INTANGIBLE.NAMES, INTANGIBLE.DESCRIPTION);
/*  73 */     createDictionaryEntry(LIGHTNING.NAMES, LIGHTNING.DESCRIPTION);
/*  74 */     createDictionaryEntry(LOCKED.NAMES, LOCKED.DESCRIPTION);
/*  75 */     createDictionaryEntry(LOCK_ON.NAMES, LOCK_ON.DESCRIPTION);
/*  76 */     createDictionaryEntry(OPENER.NAMES, OPENER.DESCRIPTION);
/*  77 */     createDictionaryEntry(PLASMA.NAMES, PLASMA.DESCRIPTION);
/*  78 */     createDictionaryEntry(POISON.NAMES, POISON.DESCRIPTION);
/*  79 */     createDictionaryEntry(RETAIN.NAMES, RETAIN.DESCRIPTION);
/*  80 */     createDictionaryEntry(SHIV.NAMES, SHIV.DESCRIPTION);
/*  81 */     createDictionaryEntry(STATUS.NAMES, STATUS.DESCRIPTION);
/*  82 */     createDictionaryEntry(STRENGTH.NAMES, STRENGTH.DESCRIPTION);
/*  83 */     createDictionaryEntry(STRIKE.NAMES, STRIKE.DESCRIPTION);
/*  84 */     createDictionaryEntry(TRANSFORM.NAMES, TRANSFORM.DESCRIPTION);
/*  85 */     createDictionaryEntry(UNKNOWN.NAMES, UNKNOWN.DESCRIPTION);
/*  86 */     createDictionaryEntry(UNPLAYABLE.NAMES, UNPLAYABLE.DESCRIPTION);
/*  87 */     createDictionaryEntry(UPGRADE.NAMES, UPGRADE.DESCRIPTION);
/*  88 */     createDictionaryEntry(VOID.NAMES, VOID.DESCRIPTION);
/*  89 */     createDictionaryEntry(VULNERABLE.NAMES, VULNERABLE.DESCRIPTION);
/*  90 */     createDictionaryEntry(WEAK.NAMES, WEAK.DESCRIPTION);
/*  91 */     createDictionaryEntry(WOUND.NAMES, WOUND.DESCRIPTION);
/*  92 */     createDictionaryEntry(DAZED.NAMES, DAZED.DESCRIPTION);
/*  93 */     createDictionaryEntry(BURN.NAMES, BURN.DESCRIPTION);
/*  94 */     createDictionaryEntry(THORNS.NAMES, THORNS.DESCRIPTION);
/*     */   }
/*     */   
/*     */   private static void createDictionaryEntry(String[] names, String desc) {
/*  98 */     String parent = names[0];
/*     */     
/* 100 */     for (String n : names) {
/* 101 */       keywords.put(n, desc);
/* 102 */       parentWord.put(n, parent);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\GameDictionary.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */