/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class CardHelper
/*     */ {
/*  19 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(CardHelper.class.getName());
/*     */   
/*     */   public static final int COMMON_CARD_LIMIT = 3;
/*     */   public static final int UNCOMMON_CARD_LIMIT = 2;
/*  23 */   public static HashMap<String, Integer> obtainedCards = new HashMap();
/*  24 */   public static ArrayList<CardInfo> removedCards = new ArrayList();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void obtain(String key, AbstractCard.CardRarity rarity, AbstractCard.CardColor color)
/*     */   {
/*  31 */     if ((rarity == AbstractCard.CardRarity.SPECIAL) || (rarity == AbstractCard.CardRarity.BASIC) || (rarity == AbstractCard.CardRarity.CURSE)) {
/*  32 */       logger.info("No need to track rarity type: " + rarity.name());
/*  33 */       return;
/*     */     }
/*     */     
/*  36 */     if (obtainedCards.containsKey(key)) {
/*  37 */       int tmp = ((Integer)obtainedCards.get(key)).intValue() + 1;
/*  38 */       obtainedCards.put(key, Integer.valueOf(tmp));
/*  39 */       logger.info("Obtained " + key + " (" + rarity.name() + "). You have " + tmp + " now");
/*     */     } else {
/*  41 */       obtainedCards.put(key, Integer.valueOf(1));
/*  42 */       logger.info("Obtained " + key + " (" + rarity.name() + "). Creating new map entry.");
/*     */     }
/*     */     
/*  45 */     UnlockTracker.markCardAsSeen(key);
/*     */   }
/*     */   
/*     */   public static void clear() {
/*  49 */     logger.info("Clearing CardHelper (obtained cards)");
/*  50 */     removedCards.clear();
/*  51 */     obtainedCards.clear();
/*     */   }
/*     */   
/*     */   public static Color getColor(float r, float g, float b) {
/*  55 */     return new Color(r / 255.0F, g / 255.0F, b / 255.0F, 1.0F);
/*     */   }
/*     */   
/*     */   public static class CardInfo {
/*     */     String id;
/*     */     String name;
/*     */     AbstractCard.CardRarity rarity;
/*     */     AbstractCard.CardColor color;
/*     */     
/*     */     public CardInfo(String id, String name, AbstractCard.CardRarity rarity, AbstractCard.CardColor color) {
/*  65 */       this.id = id;
/*  66 */       this.name = name;
/*  67 */       this.rarity = rarity;
/*  68 */       this.color = color;
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean hasCardWithXDamage(int damage) {
/*  73 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  74 */       if ((c.type == AbstractCard.CardType.ATTACK) && (c.baseDamage >= 10)) {
/*  75 */         logger.info(c.name + " is 10 Attack!");
/*  76 */         return true;
/*     */       }
/*     */     }
/*  79 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean hasCardWithID(String targetID) {
/*  83 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  84 */       if (c.cardID.equals(targetID)) {
/*  85 */         return true;
/*     */       }
/*     */     }
/*  88 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean hasCardType(AbstractCard.CardType hasType) {
/*  92 */     for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/*  93 */       if (c.type == hasType) {
/*  94 */         return true;
/*     */       }
/*     */     }
/*  97 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean hasCardWithType(AbstractCard.CardType type) {
/* 101 */     for (AbstractCard c : CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).group) {
/* 102 */       if (c.type == type) {
/* 103 */         return true;
/*     */       }
/*     */     }
/* 106 */     return false;
/*     */   }
/*     */   
/*     */   public static AbstractCard returnCardOfType(AbstractCard.CardType type) {
/* 110 */     ArrayList<AbstractCard> cards = new ArrayList();
/*     */     
/* 112 */     for (AbstractCard c : CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).group) {
/* 113 */       if (c.type == type) {
/* 114 */         cards.add(c);
/*     */       }
/*     */     }
/*     */     
/* 118 */     return (AbstractCard)cards.remove(AbstractDungeon.eventRng.random(cards.size() - 1));
/*     */   }
/*     */   
/*     */   public static boolean hasUpgradedCard() {
/* 122 */     for (AbstractCard c : CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).group) {
/* 123 */       if (c.upgraded == true) {
/* 124 */         return true;
/*     */       }
/*     */     }
/* 127 */     return false;
/*     */   }
/*     */   
/*     */   public static AbstractCard returnUpgradedCard() {
/* 131 */     ArrayList<AbstractCard> cards = new ArrayList();
/*     */     
/* 133 */     for (AbstractCard c : CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck).group) {
/* 134 */       if (c.upgraded == true) {
/* 135 */         cards.add(c);
/*     */       }
/*     */     }
/*     */     
/* 139 */     return (AbstractCard)cards.remove(AbstractDungeon.eventRng.random(cards.size() - 1));
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\CardHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */