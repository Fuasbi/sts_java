/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.megacrit.cardcrawl.daily.mods.AbstractDailyMod;
/*     */ import com.megacrit.cardcrawl.daily.mods.Allstar;
/*     */ import com.megacrit.cardcrawl.daily.mods.BigGameHunter;
/*     */ import com.megacrit.cardcrawl.daily.mods.Binary;
/*     */ import com.megacrit.cardcrawl.daily.mods.Brewmaster;
/*     */ import com.megacrit.cardcrawl.daily.mods.CertainFuture;
/*     */ import com.megacrit.cardcrawl.daily.mods.CursedRun;
/*     */ import com.megacrit.cardcrawl.daily.mods.Diverse;
/*     */ import com.megacrit.cardcrawl.daily.mods.Draft;
/*     */ import com.megacrit.cardcrawl.daily.mods.Heirloom;
/*     */ import com.megacrit.cardcrawl.daily.mods.Hoarder;
/*     */ import com.megacrit.cardcrawl.daily.mods.Insanity;
/*     */ import com.megacrit.cardcrawl.daily.mods.Lethality;
/*     */ import com.megacrit.cardcrawl.daily.mods.Midas;
/*     */ import com.megacrit.cardcrawl.daily.mods.NightTerrors;
/*     */ import com.megacrit.cardcrawl.daily.mods.Shiny;
/*     */ import com.megacrit.cardcrawl.daily.mods.Specialized;
/*     */ import com.megacrit.cardcrawl.daily.mods.Terminal;
/*     */ import com.megacrit.cardcrawl.daily.mods.TimeDilation;
/*     */ import com.megacrit.cardcrawl.daily.mods.Vintage;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.HashSet;
/*     */ import java.util.List;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Random;
/*     */ import java.util.Set;
/*     */ 
/*     */ public class ModHelper
/*     */ {
/*  36 */   private static HashMap<String, AbstractDailyMod> negativeMods = new HashMap();
/*  37 */   private static HashMap<String, AbstractDailyMod> cardMods = new HashMap();
/*     */   
/*     */ 
/*     */   public static void initialize()
/*     */   {
/*  42 */     addCardMod(new Shiny());
/*  43 */     addCardMod(new Allstar());
/*  44 */     addCardMod(new Diverse());
/*  45 */     addCardMod(new Draft());
/*  46 */     addCardMod(new Insanity());
/*  47 */     addCardMod(new Heirloom());
/*  48 */     addCardMod(new Specialized());
/*  49 */     addCardMod(new Brewmaster());
/*     */     
/*     */ 
/*  52 */     addNegative(new TimeDilation());
/*  53 */     addNegative(new CursedRun());
/*  54 */     addNegative(new BigGameHunter());
/*  55 */     addNegative(new Lethality());
/*  56 */     addNegative(new Vintage());
/*  57 */     addNegative(new NightTerrors());
/*  58 */     addNegative(new CertainFuture());
/*  59 */     addNegative(new Binary());
/*  60 */     addNegative(new Midas());
/*  61 */     addNegative(new Terminal());
/*  62 */     addNegative(new Hoarder());
/*     */   }
/*     */   
/*     */   private static void addNegative(AbstractDailyMod mod) {
/*  66 */     negativeMods.put(mod.modID, mod);
/*     */   }
/*     */   
/*     */   private static void addCardMod(AbstractDailyMod mod) {
/*  70 */     cardMods.put(mod.modID, mod);
/*     */   }
/*     */   
/*     */   public static AbstractDailyMod getMod(String key) {
/*  74 */     AbstractDailyMod mod = getNegativeMod(key);
/*  75 */     if (mod == null) {
/*  76 */       mod = getCardMod(key);
/*     */     }
/*  78 */     return mod;
/*     */   }
/*     */   
/*     */   public static AbstractDailyMod getNegativeMod(String key) {
/*  82 */     return (AbstractDailyMod)negativeMods.get(key);
/*     */   }
/*     */   
/*     */   public static AbstractDailyMod getCardMod(String key) {
/*  86 */     return (AbstractDailyMod)cardMods.get(key);
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> getCardMods(long daysSince1970) {
/*  90 */     ArrayList<AbstractDailyMod> shuffledList = new ArrayList();
/*     */     
/*  92 */     HashMap<String, Boolean> retVal = new HashMap();
/*  93 */     for (Map.Entry<String, AbstractDailyMod> m : cardMods.entrySet()) {
/*  94 */       shuffledList.add(m.getValue());
/*  95 */       retVal.put(m.getKey(), Boolean.valueOf(false));
/*     */     }
/*     */     
/*  98 */     int rotationConstant = cardMods.size() / 2;
/*     */     
/*     */ 
/*     */ 
/* 102 */     int index = (int)(daysSince1970 % rotationConstant);
/* 103 */     if (index < 0) {
/* 104 */       index += rotationConstant;
/*     */     }
/*     */     
/*     */ 
/* 108 */     int seedForRotation = (int)(daysSince1970 / rotationConstant);
/* 109 */     Collections.shuffle(shuffledList, new Random(seedForRotation));
/*     */     
/*     */ 
/* 112 */     retVal.put(((AbstractDailyMod)shuffledList.get(index)).modID, Boolean.valueOf(true));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 117 */     return retVal;
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> getNegativeMods(long daysSince1970)
/*     */   {
/* 122 */     ArrayList<AbstractDailyMod> shuffledList = new ArrayList();
/*     */     
/* 124 */     HashMap<String, Boolean> retVal = new HashMap();
/* 125 */     for (Map.Entry<String, AbstractDailyMod> m : negativeMods.entrySet()) {
/* 126 */       shuffledList.add(m.getValue());
/* 127 */       retVal.put(m.getKey(), Boolean.valueOf(false));
/*     */     }
/*     */     
/* 130 */     int rotationConstant = negativeMods.size() / 2;
/*     */     
/*     */ 
/*     */ 
/* 134 */     int index = (int)(daysSince1970 % rotationConstant);
/* 135 */     if (index < 0) {
/* 136 */       index += rotationConstant;
/*     */     }
/*     */     
/*     */ 
/* 140 */     int seedForRotation = (int)(daysSince1970 / rotationConstant);
/* 141 */     Collections.shuffle(shuffledList, new Random(seedForRotation));
/*     */     
/*     */ 
/* 144 */     retVal.put(((AbstractDailyMod)shuffledList.get(index)).modID, Boolean.valueOf(true));
/*     */     
/*     */ 
/* 147 */     int altIndex = index + rotationConstant;
/* 148 */     retVal.put(((AbstractDailyMod)shuffledList.get(altIndex)).modID, Boolean.valueOf(true));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 153 */     return retVal;
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> noNegativeMods() {
/* 157 */     HashMap<String, Boolean> retVal = new HashMap();
/* 158 */     for (Map.Entry<String, AbstractDailyMod> m : negativeMods.entrySet()) {
/* 159 */       retVal.put(m.getKey(), Boolean.valueOf(false));
/*     */     }
/* 161 */     return retVal;
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> noPositiveMods() {
/* 165 */     HashMap<String, Boolean> retVal = new HashMap();
/* 166 */     for (Map.Entry<String, AbstractDailyMod> m : cardMods.entrySet()) {
/* 167 */       retVal.put(m.getKey(), Boolean.valueOf(false));
/*     */     }
/* 169 */     return retVal;
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> getCardMods(List<String> enabledModIDs) {
/* 173 */     return getModsFromSet(cardMods, new HashSet(enabledModIDs));
/*     */   }
/*     */   
/*     */   public static HashMap<String, Boolean> getNegativeMods(List<String> modIDs) {
/* 177 */     return getModsFromSet(negativeMods, new HashSet(modIDs));
/*     */   }
/*     */   
/*     */   private static HashMap<String, Boolean> getModsFromSet(HashMap<String, AbstractDailyMod> set, Set<String> modIDs) {
/* 181 */     HashMap<String, Boolean> retVal = new HashMap();
/* 182 */     for (Map.Entry<String, AbstractDailyMod> m : set.entrySet()) {
/* 183 */       retVal.put(m.getKey(), Boolean.valueOf(modIDs.contains(m.getKey())));
/*     */     }
/* 185 */     return retVal;
/*     */   }
/*     */   
/*     */   public static void uploadModData() {
/* 189 */     ArrayList<String> data = new ArrayList();
/*     */     
/* 191 */     for (Map.Entry<String, AbstractDailyMod> m : cardMods.entrySet()) {
/* 192 */       data.add(((AbstractDailyMod)m.getValue()).gameDataUploadData());
/*     */     }
/*     */     
/* 195 */     for (Map.Entry<String, AbstractDailyMod> m : negativeMods.entrySet()) {
/* 196 */       data.add(((AbstractDailyMod)m.getValue()).gameDataUploadData());
/*     */     }
/*     */     
/* 199 */     BotDataUploader.uploadDataAsync(BotDataUploader.GameDataType.DAILY_MOD_DATA, AbstractDailyMod.gameDataUploadHeader(), data);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\ModHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */