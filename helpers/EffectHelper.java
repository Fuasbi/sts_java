/*    */ package com.megacrit.cardcrawl.helpers;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class EffectHelper
/*    */ {
/*    */   public static void gainGold(AbstractCreature target, float srcX, float srcY, int amount)
/*    */   {
/*  9 */     for (int i = 0; i < amount; i++) {
/* 10 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.GainPennyEffect(target, srcX, srcY, target.hb.cX, target.hb.cY, true));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\EffectHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */