/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import java.util.ArrayList;
/*     */ import java.util.TreeMap;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class TipHelper
/*     */ {
/*  19 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("TipHelper");
/*  20 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  22 */   private static final Logger logger = LogManager.getLogger(TipHelper.class.getName());
/*  23 */   private static boolean renderedTipThisFrame = false; private static boolean isCard = false;
/*     */   private static float drawX;
/*  25 */   private static float drawY; private static ArrayList<String> KEYWORDS = new ArrayList();
/*  26 */   private static ArrayList<PowerTip> POWER_TIPS = new ArrayList();
/*  27 */   private static String HEADER = null; private static String BODY = null;
/*     */   private static AbstractCard card;
/*  29 */   private static final Color BASE_COLOR = new Color(1.0F, 0.9725F, 0.8745F, 1.0F);
/*     */   
/*     */ 
/*  32 */   private static final float CARD_TIP_PAD = 12.0F * Settings.scale;
/*  33 */   private static final float SHADOW_DIST_Y = 14.0F * Settings.scale;
/*  34 */   private static final float SHADOW_DIST_X = 9.0F * Settings.scale;
/*  35 */   private static final float BOX_EDGE_H = 32.0F * Settings.scale;
/*  36 */   private static final float BOX_BODY_H = 64.0F * Settings.scale;
/*  37 */   private static final float BOX_W = 320.0F * Settings.scale;
/*     */   
/*     */ 
/*  40 */   private static GlyphLayout gl = new GlyphLayout();
/*     */   private static float textHeight;
/*  42 */   private static final float TEXT_OFFSET_X = 22.0F * Settings.scale;
/*  43 */   private static final float HEADER_OFFSET_Y = 12.0F * Settings.scale;
/*  44 */   private static final float ORB_OFFSET_Y = -8.0F * Settings.scale;
/*  45 */   private static final float BODY_OFFSET_Y = -20.0F * Settings.scale;
/*  46 */   private static final float BODY_TEXT_WIDTH = 280.0F * Settings.scale;
/*  47 */   private static final float TIP_DESC_LINE_SPACING = 26.0F * Settings.scale;
/*     */   
/*     */ 
/*  50 */   private static final float POWER_ICON_OFFSET_X = 40.0F * Settings.scale;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void render(SpriteBatch sb)
/*     */   {
/*  58 */     if ((!Settings.hidePopupDetails) && 
/*  59 */       (renderedTipThisFrame))
/*     */     {
/*     */ 
/*  62 */       if ((AbstractDungeon.player != null) && ((AbstractDungeon.player.inSingleTargetMode) || (AbstractDungeon.player.isDraggingCard)))
/*     */       {
/*  64 */         KEYWORDS.clear();
/*  65 */         HEADER = null;
/*  66 */         BODY = null;
/*  67 */         card = null;
/*  68 */         renderedTipThisFrame = false;
/*  69 */         return;
/*     */       }
/*     */       
/*  72 */       if ((isCard) && (card == null)) {
/*  73 */         logger.info("how did this happen? CARD IS NULL ON TIP HELPER");
/*     */       }
/*     */       
/*  76 */       if ((isCard) && (card != null)) {
/*  77 */         if (card.current_x > Settings.WIDTH * 0.75F) {
/*  78 */           renderKeywords(card.current_x - AbstractCard.IMG_WIDTH / 2.0F - CARD_TIP_PAD - BOX_W, card.current_y + AbstractCard.IMG_HEIGHT / 2.0F - BOX_EDGE_H, sb, KEYWORDS);
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/*  84 */           renderKeywords(card.current_x + AbstractCard.IMG_WIDTH / 2.0F + CARD_TIP_PAD, card.current_y + AbstractCard.IMG_HEIGHT / 2.0F - BOX_EDGE_H, sb, KEYWORDS);
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*  90 */         card = null;
/*  91 */         isCard = false;
/*     */ 
/*     */       }
/*  94 */       else if (HEADER != null) {
/*  95 */         textHeight = -FontHelper.getSmartHeight(FontHelper.tipBodyFont, BODY, BODY_TEXT_WIDTH, TIP_DESC_LINE_SPACING) - 7.0F * Settings.scale;
/*     */         
/*     */ 
/*     */ 
/*     */ 
/* 100 */         renderTipBox(drawX, drawY, sb, HEADER, BODY);
/* 101 */         HEADER = null;
/*     */       }
/*     */       else
/*     */       {
/* 105 */         renderPowerTips(drawX, drawY, sb, POWER_TIPS);
/*     */       }
/*     */       
/* 108 */       renderedTipThisFrame = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public static void renderGenericTip(float x, float y, String header, String body)
/*     */   {
/* 114 */     if (!Settings.hidePopupDetails) {
/* 115 */       if (!renderedTipThisFrame)
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/* 120 */         renderedTipThisFrame = true;
/* 121 */         HEADER = header;
/* 122 */         BODY = body;
/* 123 */         drawX = x;
/* 124 */         drawY = y;
/*     */       }
/* 126 */       else if ((HEADER == null) && (!KEYWORDS.isEmpty())) {
/* 127 */         logger.info("! " + (String)KEYWORDS.get(0));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void queuePowerTips(float x, float y, ArrayList<PowerTip> powerTips)
/*     */   {
/* 137 */     if (!renderedTipThisFrame) {
/* 138 */       renderedTipThisFrame = true;
/* 139 */       drawX = x;
/* 140 */       drawY = y;
/* 141 */       drawY -= powerTips.size() * 20.0F * Settings.scale;
/* 142 */       POWER_TIPS = powerTips;
/*     */     }
/* 144 */     else if ((HEADER == null) && (!KEYWORDS.isEmpty())) {
/* 145 */       logger.info("! " + (String)KEYWORDS.get(0));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void renderTipForCard(AbstractCard c, SpriteBatch sb, ArrayList<String> keywords)
/*     */   {
/* 158 */     if (!renderedTipThisFrame) {
/* 159 */       isCard = true;
/* 160 */       card = c;
/* 161 */       convertToReadable(keywords);
/* 162 */       KEYWORDS = keywords;
/* 163 */       renderedTipThisFrame = true;
/*     */     }
/*     */   }
/*     */   
/*     */   private static void convertToReadable(ArrayList<String> keywords) {
/* 168 */     ArrayList<String> add = new ArrayList();
/* 169 */     keywords.addAll(add);
/*     */   }
/*     */   
/*     */   private static void renderPowerTips(float x, float y, SpriteBatch sb, ArrayList<PowerTip> powerTips) {
/* 173 */     float originalY = y;
/* 174 */     boolean offsetLeft = false;
/* 175 */     if (x > Settings.WIDTH / 2.0F) {
/* 176 */       offsetLeft = true;
/*     */     }
/* 178 */     boolean shift = false;
/* 179 */     float offset = 0.0F;
/* 180 */     for (PowerTip tip : powerTips) {
/* 181 */       textHeight = -FontHelper.getSmartHeight(FontHelper.tipBodyFont, tip.body, BODY_TEXT_WIDTH, TIP_DESC_LINE_SPACING) - 7.0F * Settings.scale;
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 187 */       renderTipBox(x, y, sb, tip.header, tip.body);
/*     */       
/*     */ 
/* 190 */       gl.setText(FontHelper.tipHeaderFont, tip.header, Color.WHITE, 0.0F, -1, false);
/* 191 */       if (tip.img != null) {
/* 192 */         sb.setColor(Color.WHITE);
/* 193 */         sb.draw(tip.img, x + TEXT_OFFSET_X + gl.width + 5.0F * Settings.scale, y - 10.0F * Settings.scale, 32.0F * Settings.scale, 32.0F * Settings.scale);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/* 199 */       else if (tip.imgRegion != null) {
/* 200 */         sb.setColor(Color.WHITE);
/* 201 */         sb.draw(tip.imgRegion, x + gl.width + POWER_ICON_OFFSET_X - tip.imgRegion.packedWidth / 2.0F, y + 5.0F * Settings.scale - tip.imgRegion.packedHeight / 2.0F, tip.imgRegion.packedWidth / 2.0F, tip.imgRegion.packedHeight / 2.0F, tip.imgRegion.packedWidth, tip.imgRegion.packedHeight, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 214 */       y -= textHeight + BOX_EDGE_H * 3.15F;
/* 215 */       offset += textHeight + BOX_EDGE_H;
/*     */       
/* 217 */       if ((offset > 220.0F * Settings.scale) && 
/* 218 */         (!shift)) {
/* 219 */         shift = true;
/* 220 */         y = originalY;
/* 221 */         if (offsetLeft) {
/* 222 */           x -= 324.0F * Settings.scale;
/*     */         } else {
/* 224 */           x += 324.0F * Settings.scale;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private static void renderKeywords(float x, float y, SpriteBatch sb, ArrayList<String> keywords)
/*     */   {
/* 232 */     if (keywords.size() >= 4) {
/* 233 */       y += (keywords.size() - 1) * 62.0F * Settings.scale;
/*     */     }
/*     */     
/* 236 */     for (String s : keywords) {
/* 237 */       if (!GameDictionary.keywords.containsKey(s)) {
/* 238 */         logger.info("MISSING: " + s + " in Dictionary!");
/*     */       } else {
/* 240 */         textHeight = -FontHelper.getSmartHeight(FontHelper.tipBodyFont, 
/*     */         
/* 242 */           (String)GameDictionary.keywords.get(s), BODY_TEXT_WIDTH, TIP_DESC_LINE_SPACING) - 7.0F * Settings.scale;
/*     */         
/*     */ 
/* 245 */         renderBox(sb, s, x, y);
/* 246 */         y -= textHeight + BOX_EDGE_H * 3.15F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void renderTipBox(float x, float y, SpriteBatch sb, String title, String description)
/*     */   {
/* 261 */     float h = textHeight;
/*     */     
/*     */ 
/* 264 */     sb.setColor(Settings.TOP_PANEL_SHADOW_COLOR);
/* 265 */     sb.draw(ImageMaster.KEYWORD_TOP, x + SHADOW_DIST_X, y - SHADOW_DIST_Y, BOX_W, BOX_EDGE_H);
/* 266 */     sb.draw(ImageMaster.KEYWORD_BODY, x + SHADOW_DIST_X, y - h - BOX_EDGE_H - SHADOW_DIST_Y, BOX_W, h + BOX_EDGE_H);
/* 267 */     sb.draw(ImageMaster.KEYWORD_BOT, x + SHADOW_DIST_X, y - h - BOX_BODY_H - SHADOW_DIST_Y, BOX_W, BOX_EDGE_H);
/*     */     
/*     */ 
/* 270 */     sb.setColor(Color.WHITE);
/* 271 */     sb.draw(ImageMaster.KEYWORD_TOP, x, y, BOX_W, BOX_EDGE_H);
/* 272 */     sb.draw(ImageMaster.KEYWORD_BODY, x, y - h - BOX_EDGE_H, BOX_W, h + BOX_EDGE_H);
/* 273 */     sb.draw(ImageMaster.KEYWORD_BOT, x, y - h - BOX_BODY_H, BOX_W, BOX_EDGE_H);
/*     */     
/*     */ 
/* 276 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, title, x + TEXT_OFFSET_X, y + HEADER_OFFSET_Y, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 285 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, description, x + TEXT_OFFSET_X, y + BODY_OFFSET_Y, BODY_TEXT_WIDTH, TIP_DESC_LINE_SPACING, BASE_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void renderTipEnergy(SpriteBatch sb, TextureAtlas.AtlasRegion region, float x, float y)
/*     */   {
/* 298 */     sb.setColor(Color.WHITE.cpy());
/* 299 */     sb.draw(region
/* 300 */       .getTexture(), x + region.offsetX * Settings.scale, y + region.offsetY * Settings.scale, 0.0F, 0.0F, region.packedWidth, region.packedHeight, Settings.scale, Settings.scale, 0.0F, region
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 310 */       .getRegionX(), region
/* 311 */       .getRegionY(), region
/* 312 */       .getRegionWidth(), region
/* 313 */       .getRegionHeight(), false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void renderBox(SpriteBatch sb, String word, float x, float y)
/*     */   {
/* 327 */     float h = textHeight;
/*     */     
/*     */ 
/* 330 */     sb.setColor(Settings.TOP_PANEL_SHADOW_COLOR);
/* 331 */     sb.draw(ImageMaster.KEYWORD_TOP, x + SHADOW_DIST_X, y - SHADOW_DIST_Y, BOX_W, BOX_EDGE_H);
/* 332 */     sb.draw(ImageMaster.KEYWORD_BODY, x + SHADOW_DIST_X, y - h - BOX_EDGE_H - SHADOW_DIST_Y, BOX_W, h + BOX_EDGE_H);
/* 333 */     sb.draw(ImageMaster.KEYWORD_BOT, x + SHADOW_DIST_X, y - h - BOX_BODY_H - SHADOW_DIST_Y, BOX_W, BOX_EDGE_H);
/*     */     
/*     */ 
/* 336 */     sb.setColor(Color.WHITE);
/* 337 */     sb.draw(ImageMaster.KEYWORD_TOP, x, y, BOX_W, BOX_EDGE_H);
/* 338 */     sb.draw(ImageMaster.KEYWORD_BODY, x, y - h - BOX_EDGE_H, BOX_W, h + BOX_EDGE_H);
/* 339 */     sb.draw(ImageMaster.KEYWORD_BOT, x, y - h - BOX_BODY_H, BOX_W, BOX_EDGE_H);
/*     */     
/*     */ 
/* 342 */     if ((word.equals("[R]")) || (word.equals("[G]")) || (word.equals("[B]"))) {
/* 343 */       if (word.equals("[R]")) {
/* 344 */         renderTipEnergy(sb, AbstractCard.orb_red, x + TEXT_OFFSET_X, y + ORB_OFFSET_Y);
/* 345 */       } else if (word.equals("[G]")) {
/* 346 */         renderTipEnergy(sb, AbstractCard.orb_green, x + TEXT_OFFSET_X, y + ORB_OFFSET_Y);
/* 347 */       } else if (word.equals("[B]")) {
/* 348 */         renderTipEnergy(sb, AbstractCard.orb_blue, x + TEXT_OFFSET_X, y + ORB_OFFSET_Y);
/*     */       }
/*     */       
/* 351 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*     */       
/*     */ 
/* 354 */         capitalize(TEXT[0]), x + TEXT_OFFSET_X * 2.5F, y + HEADER_OFFSET_Y, Settings.GOLD_COLOR);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 359 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*     */       
/*     */ 
/* 362 */         capitalize(word), x + TEXT_OFFSET_X, y + HEADER_OFFSET_Y, Settings.GOLD_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 369 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, 
/*     */     
/*     */ 
/* 372 */       (String)GameDictionary.keywords.get(word), x + TEXT_OFFSET_X, y + BODY_OFFSET_Y, BODY_TEXT_WIDTH, TIP_DESC_LINE_SPACING, BASE_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String capitalize(String input)
/*     */   {
/* 388 */     StringBuilder sb = new StringBuilder();
/* 389 */     for (int i = 0; i < input.length(); i++) {
/* 390 */       char tmp = input.charAt(i);
/* 391 */       if (i == 0) {
/* 392 */         tmp = Character.toUpperCase(tmp);
/*     */       } else {
/* 394 */         tmp = Character.toLowerCase(tmp);
/*     */       }
/* 396 */       sb.append(tmp);
/*     */     }
/* 398 */     return sb.toString();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\TipHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */