/*     */ package com.megacrit.cardcrawl.helpers;
/*     */ 
/*     */ import com.badlogic.gdx.math.RandomXS128;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.blights.Accursed;
/*     */ import com.megacrit.cardcrawl.blights.AncientAugmentation;
/*     */ import com.megacrit.cardcrawl.blights.Durian;
/*     */ import com.megacrit.cardcrawl.blights.GrotesqueTrophy;
/*     */ import com.megacrit.cardcrawl.blights.Hauntings;
/*     */ import com.megacrit.cardcrawl.blights.MimicInfestation;
/*     */ import com.megacrit.cardcrawl.blights.Muzzle;
/*     */ import com.megacrit.cardcrawl.blights.Scatterbrain;
/*     */ import com.megacrit.cardcrawl.blights.Shield;
/*     */ import com.megacrit.cardcrawl.blights.Spear;
/*     */ import com.megacrit.cardcrawl.blights.TimeMaze;
/*     */ import com.megacrit.cardcrawl.blights.TwistingMind;
/*     */ import com.megacrit.cardcrawl.blights.VoidEssence;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class BlightHelper
/*     */ {
/*  28 */   private static final Logger logger = LogManager.getLogger(BlightHelper.class.getName());
/*  29 */   public static ArrayList<String> blights = new ArrayList();
/*  30 */   public static ArrayList<String> chestBlights = new ArrayList();
/*     */   
/*     */   public static void initialize() {
/*  33 */     blights.clear();
/*  34 */     blights.add("DeadlyEnemies");
/*  35 */     blights.add("ToughEnemies");
/*  36 */     blights.add("TimeMaze");
/*  37 */     blights.add("MimicInfestation");
/*  38 */     blights.add("FullBelly");
/*  39 */     blights.add("GrotesqueTrophy");
/*     */     
/*     */ 
/*  42 */     blights.add("Accursed");
/*  43 */     blights.add("Scatterbrain");
/*  44 */     blights.add("TwistingMind");
/*  45 */     blights.add("BlightedDurian");
/*  46 */     blights.add("VoidEssence");
/*  47 */     blights.add("GraspOfShadows");
/*  48 */     blights.add("MetallicRebirth");
/*     */     
/*  50 */     chestBlights.add("Accursed");
/*  51 */     chestBlights.add("Scatterbrain");
/*  52 */     chestBlights.add("TwistingMind");
/*  53 */     chestBlights.add("BlightedDurian");
/*  54 */     chestBlights.add("VoidEssence");
/*  55 */     chestBlights.add("GraspOfShadows");
/*  56 */     chestBlights.add("MetallicRebirth");
/*     */   }
/*     */   
/*     */   public static AbstractBlight getBlight(String id) {
/*  60 */     switch (id)
/*     */     {
/*     */     case "DeadlyEnemies": 
/*  63 */       return new Spear();
/*     */     case "ToughEnemies": 
/*  65 */       return new Shield();
/*     */     case "TimeMaze": 
/*  67 */       return new TimeMaze();
/*     */     case "MimicInfestation": 
/*  69 */       return new MimicInfestation();
/*     */     case "FullBelly": 
/*  71 */       return new Muzzle();
/*     */     case "GrotesqueTrophy": 
/*  73 */       return new GrotesqueTrophy();
/*     */     
/*     */ 
/*     */     case "TwistingMind": 
/*  77 */       return new TwistingMind();
/*     */     case "Scatterbrain": 
/*  79 */       return new Scatterbrain();
/*     */     case "Accursed": 
/*  81 */       return new Accursed();
/*     */     case "BlightedDurian": 
/*  83 */       return new Durian();
/*     */     case "VoidEssence": 
/*  85 */       return new VoidEssence();
/*     */     case "GraspOfShadows": 
/*  87 */       return new Hauntings();
/*     */     case "MetallicRebirth": 
/*  89 */       return new AncientAugmentation();
/*     */     }
/*  91 */     logger.info("MISSING KEY: " + id);
/*  92 */     return null;
/*     */   }
/*     */   
/*     */   public static AbstractBlight getRandomChestBlight(ArrayList<String> exclusion)
/*     */   {
/*  97 */     ArrayList<String> blightTmp = new ArrayList();
/*     */     
/*     */ 
/* 100 */     for (String s : chestBlights) {
/* 101 */       boolean exclude = false;
/* 102 */       for (String s2 : exclusion) {
/* 103 */         if (s.equals(s2)) {
/* 104 */           logger.info(s + " EXCLUDED");
/* 105 */           exclude = true;
/*     */         }
/*     */       }
/*     */       
/* 109 */       if (!exclude) {
/* 110 */         blightTmp.add(s);
/*     */       }
/*     */     }
/*     */     
/* 114 */     String randomKey = (String)blightTmp.get(AbstractDungeon.relicRng.random.nextInt(blightTmp.size()));
/* 115 */     return getBlight(randomKey);
/*     */   }
/*     */   
/*     */   public static AbstractBlight getRandomBlight(Random rng) {
/* 119 */     String randomKey = (String)blights.get(rng.random.nextInt(blights.size()));
/* 120 */     return getBlight(randomKey);
/*     */   }
/*     */   
/*     */   public static AbstractBlight getRandomBlight() {
/* 124 */     String randomKey = (String)chestBlights.get(AbstractDungeon.relicRng.random.nextInt(chestBlights.size()));
/* 125 */     if (AbstractDungeon.player.maxHealth <= 20) {
/* 126 */       while (randomKey.equals("BlightedDurian")) {
/* 127 */         randomKey = (String)chestBlights.get(AbstractDungeon.relicRng.random.nextInt(chestBlights.size()));
/*     */       }
/*     */     }
/* 130 */     return getBlight(randomKey);
/*     */   }
/*     */   
/*     */   public static void uploadBlightData() {
/* 134 */     ArrayList<String> data = new ArrayList();
/*     */     
/* 136 */     if (blights.isEmpty()) {
/* 137 */       initialize();
/*     */     }
/*     */     
/* 140 */     ArrayList<String> allBlights = new ArrayList(blights);
/* 141 */     allBlights.addAll(chestBlights);
/*     */     
/* 143 */     for (String b : allBlights) {
/* 144 */       AbstractBlight blight = getBlight(b);
/* 145 */       if (blight != null) {
/* 146 */         data.add(blight.gameDataUploadData());
/*     */       }
/*     */     }
/* 149 */     BotDataUploader.uploadDataAsync(BotDataUploader.GameDataType.BLIGHT_DATA, AbstractBlight.gameDataUploadHeader(), data);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\helpers\BlightHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */