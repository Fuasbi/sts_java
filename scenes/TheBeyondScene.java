/*     */ package com.megacrit.cardcrawl.scenes;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ 
/*     */ public class TheBeyondScene extends AbstractScene { private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion bg1;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion bg2;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion floor;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion ceiling;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion fg;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion mg1;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion mg2;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion mg3;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion mg4;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion c1;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion c2;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion c3;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion c4;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion f1;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion f2;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion f3;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion f4;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion f5;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion i1;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion i2;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion i3;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion i4;
/*     */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion i5;
/*  28 */   private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion s1; private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion s2; private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion s3; private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion s4; private final com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion s5; private boolean renderAltBg; private boolean renderM1; private boolean renderM2; private boolean renderM3; private boolean renderM4; private boolean renderFloater; private boolean renderF1; private boolean renderF2; private boolean renderF3; private boolean renderF4; private boolean renderF5; private boolean renderIce; private boolean renderI1; private boolean renderI2; private boolean renderI3; private boolean renderI4; private boolean renderI5; private boolean renderStalactites; private boolean renderS1; private boolean renderS2; private boolean renderS3; private boolean renderS4; private boolean renderS5; private ColumnConfig columnConfig = ColumnConfig.OPEN;
/*     */   
/*  30 */   private com.badlogic.gdx.graphics.Color overlayColor = new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, 0.2F);
/*     */   
/*     */   public TheBeyondScene() {
/*  33 */     super("beyondScene/scene.atlas");
/*  34 */     this.bg1 = this.atlas.findRegion("mod/bg1");
/*  35 */     this.bg2 = this.atlas.findRegion("mod/bg2");
/*  36 */     this.floor = this.atlas.findRegion("mod/floor");
/*  37 */     this.ceiling = this.atlas.findRegion("mod/ceiling");
/*  38 */     this.fg = this.atlas.findRegion("mod/fg");
/*     */     
/*  40 */     this.mg1 = this.atlas.findRegion("mod/mod1");
/*  41 */     this.mg2 = this.atlas.findRegion("mod/mod2");
/*  42 */     this.mg3 = this.atlas.findRegion("mod/mod3");
/*  43 */     this.mg4 = this.atlas.findRegion("mod/mod4");
/*     */     
/*  45 */     this.c1 = this.atlas.findRegion("mod/c1");
/*  46 */     this.c2 = this.atlas.findRegion("mod/c2");
/*  47 */     this.c3 = this.atlas.findRegion("mod/c3");
/*  48 */     this.c4 = this.atlas.findRegion("mod/c4");
/*     */     
/*  50 */     this.f1 = this.atlas.findRegion("mod/f1");
/*  51 */     this.f2 = this.atlas.findRegion("mod/f2");
/*  52 */     this.f3 = this.atlas.findRegion("mod/f3");
/*  53 */     this.f4 = this.atlas.findRegion("mod/f4");
/*  54 */     this.f5 = this.atlas.findRegion("mod/f5");
/*     */     
/*  56 */     this.i1 = this.atlas.findRegion("mod/i1");
/*  57 */     this.i2 = this.atlas.findRegion("mod/i2");
/*  58 */     this.i3 = this.atlas.findRegion("mod/i3");
/*  59 */     this.i4 = this.atlas.findRegion("mod/i4");
/*  60 */     this.i5 = this.atlas.findRegion("mod/i5");
/*     */     
/*  62 */     this.s1 = this.atlas.findRegion("mod/s1");
/*  63 */     this.s2 = this.atlas.findRegion("mod/s2");
/*  64 */     this.s3 = this.atlas.findRegion("mod/s3");
/*  65 */     this.s4 = this.atlas.findRegion("mod/s4");
/*  66 */     this.s5 = this.atlas.findRegion("mod/s5");
/*     */     
/*  68 */     this.ambianceName = "AMBIANCE_BEYOND";
/*  69 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static enum ColumnConfig
/*     */   {
/*  76 */     OPEN,  SMALL_ONLY,  SMALL_PLUS_LEFT,  SMALL_PLUS_RIGHT;
/*     */     
/*     */     private ColumnConfig() {}
/*     */   }
/*     */   
/*  81 */   public void randomizeScene() { this.overlayColor.r = MathUtils.random(0.7F, 0.9F);
/*  82 */     this.overlayColor.g = MathUtils.random(0.7F, 0.9F);
/*  83 */     this.overlayColor.b = MathUtils.random(0.7F, 1.0F);
/*  84 */     this.overlayColor.a = MathUtils.random(0.0F, 0.2F);
/*     */     
/*     */ 
/*  87 */     this.renderAltBg = MathUtils.randomBoolean(0.2F);
/*  88 */     this.renderM1 = false;
/*  89 */     this.renderM2 = false;
/*  90 */     this.renderM3 = false;
/*  91 */     this.renderM4 = false;
/*  92 */     if ((!this.renderAltBg) && 
/*  93 */       (MathUtils.randomBoolean(0.8F))) {
/*  94 */       this.renderM1 = MathUtils.randomBoolean();
/*  95 */       this.renderM2 = MathUtils.randomBoolean();
/*  96 */       this.renderM3 = MathUtils.randomBoolean();
/*  97 */       if (!this.renderM3) {
/*  98 */         this.renderM4 = MathUtils.randomBoolean();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 104 */     if (MathUtils.randomBoolean(0.6F)) {
/* 105 */       this.columnConfig = ColumnConfig.OPEN;
/*     */     }
/* 107 */     else if (MathUtils.randomBoolean()) {
/* 108 */       this.columnConfig = ColumnConfig.SMALL_ONLY;
/*     */     }
/* 110 */     else if (MathUtils.randomBoolean()) {
/* 111 */       this.columnConfig = ColumnConfig.SMALL_PLUS_LEFT;
/*     */     } else {
/* 113 */       this.columnConfig = ColumnConfig.SMALL_PLUS_RIGHT;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 119 */     this.renderFloater = MathUtils.randomBoolean();
/* 120 */     if (this.renderFloater) {
/* 121 */       this.renderFloater = true;
/* 122 */       this.renderF1 = MathUtils.randomBoolean();
/* 123 */       this.renderF2 = MathUtils.randomBoolean();
/* 124 */       this.renderF3 = MathUtils.randomBoolean();
/* 125 */       this.renderF4 = MathUtils.randomBoolean();
/* 126 */       this.renderF5 = MathUtils.randomBoolean();
/*     */     } else {
/* 128 */       this.renderF1 = false;
/* 129 */       this.renderF2 = false;
/* 130 */       this.renderF3 = false;
/* 131 */       this.renderF4 = false;
/* 132 */       this.renderF5 = false;
/*     */     }
/*     */     
/*     */ 
/* 136 */     this.renderIce = MathUtils.randomBoolean();
/* 137 */     if (this.renderIce) {
/* 138 */       this.renderIce = true;
/* 139 */       this.renderI1 = MathUtils.randomBoolean();
/* 140 */       this.renderI2 = MathUtils.randomBoolean();
/* 141 */       this.renderI3 = MathUtils.randomBoolean();
/* 142 */       this.renderI4 = MathUtils.randomBoolean();
/* 143 */       this.renderI5 = MathUtils.randomBoolean();
/*     */     } else {
/* 145 */       this.renderI1 = false;
/* 146 */       this.renderI2 = false;
/* 147 */       this.renderI3 = false;
/* 148 */       this.renderI4 = false;
/* 149 */       this.renderI5 = false;
/*     */     }
/*     */     
/*     */ 
/* 153 */     this.renderStalactites = MathUtils.randomBoolean();
/* 154 */     if (this.renderStalactites) {
/* 155 */       this.renderStalactites = true;
/* 156 */       this.renderS1 = MathUtils.randomBoolean();
/* 157 */       this.renderS2 = MathUtils.randomBoolean();
/* 158 */       this.renderS3 = MathUtils.randomBoolean();
/* 159 */       this.renderS4 = MathUtils.randomBoolean();
/* 160 */       this.renderS5 = MathUtils.randomBoolean();
/*     */     } else {
/* 162 */       this.renderS1 = false;
/* 163 */       this.renderS2 = false;
/* 164 */       this.renderS3 = false;
/* 165 */       this.renderS4 = false;
/* 166 */       this.renderS5 = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextRoom(com.megacrit.cardcrawl.rooms.AbstractRoom room)
/*     */   {
/* 176 */     super.nextRoom(room);
/* 177 */     randomizeScene();
/* 178 */     if ((room instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 179 */       com.megacrit.cardcrawl.core.CardCrawlGame.music.silenceBGM();
/*     */     }
/* 181 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */   public void renderCombatRoomBg(com.badlogic.gdx.graphics.g2d.SpriteBatch sb)
/*     */   {
/* 186 */     sb.setColor(new com.badlogic.gdx.graphics.Color(this.overlayColor.r, this.overlayColor.g, this.overlayColor.b, 1.0F));
/*     */     
/*     */ 
/* 189 */     renderAtlasRegionIf(sb, this.floor, true);
/* 190 */     renderAtlasRegionIf(sb, this.ceiling, true);
/*     */     
/*     */ 
/* 193 */     renderAtlasRegionIf(sb, this.bg1, true);
/* 194 */     renderAtlasRegionIf(sb, this.bg2, this.renderAltBg);
/* 195 */     renderAtlasRegionIf(sb, this.mg2, this.renderM2);
/* 196 */     renderAtlasRegionIf(sb, this.mg1, this.renderM1);
/* 197 */     renderAtlasRegionIf(sb, this.mg3, this.renderM3);
/* 198 */     renderAtlasRegionIf(sb, this.mg4, this.renderM4);
/*     */     
/*     */ 
/* 201 */     switch (this.columnConfig) {
/*     */     case OPEN: 
/*     */       break;
/*     */     case SMALL_ONLY: 
/* 205 */       renderAtlasRegionIf(sb, this.c1, true);
/* 206 */       renderAtlasRegionIf(sb, this.c4, true);
/* 207 */       break;
/*     */     case SMALL_PLUS_LEFT: 
/* 209 */       renderAtlasRegionIf(sb, this.c1, true);
/* 210 */       renderAtlasRegionIf(sb, this.c2, true);
/* 211 */       renderAtlasRegionIf(sb, this.c4, true);
/* 212 */       break;
/*     */     case SMALL_PLUS_RIGHT: 
/* 214 */       renderAtlasRegionIf(sb, this.c1, true);
/* 215 */       renderAtlasRegionIf(sb, this.c3, true);
/* 216 */       renderAtlasRegionIf(sb, this.c4, true);
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 221 */     renderAtlasRegionIf(sb, this.s1, this.renderS1);
/* 222 */     renderAtlasRegionIf(sb, this.s2, this.renderS2);
/* 223 */     renderAtlasRegionIf(sb, this.s3, this.renderS3);
/* 224 */     renderAtlasRegionIf(sb, this.s4, this.renderS4);
/* 225 */     renderAtlasRegionIf(sb, this.s5, this.renderS5);
/*     */     
/* 227 */     sb.setColor(this.overlayColor);
/* 228 */     sb.setBlendFunction(770, 1);
/*     */     
/*     */ 
/* 231 */     renderAtlasRegionIf(sb, this.bg1, true);
/* 232 */     renderAtlasRegionIf(sb, this.bg2, this.renderAltBg);
/* 233 */     renderAtlasRegionIf(sb, this.mg2, this.renderM2);
/* 234 */     renderAtlasRegionIf(sb, this.mg1, this.renderM1);
/* 235 */     renderAtlasRegionIf(sb, this.mg3, this.renderM3);
/* 236 */     renderAtlasRegionIf(sb, this.mg4, this.renderM4);
/*     */     
/*     */ 
/* 239 */     switch (this.columnConfig) {
/*     */     case OPEN: 
/*     */       break;
/*     */     case SMALL_ONLY: 
/* 243 */       renderAtlasRegionIf(sb, this.c1, true);
/* 244 */       renderAtlasRegionIf(sb, this.c4, true);
/* 245 */       break;
/*     */     case SMALL_PLUS_LEFT: 
/* 247 */       renderAtlasRegionIf(sb, this.c1, true);
/* 248 */       renderAtlasRegionIf(sb, this.c2, true);
/* 249 */       renderAtlasRegionIf(sb, this.c4, true);
/* 250 */       break;
/*     */     case SMALL_PLUS_RIGHT: 
/* 252 */       renderAtlasRegionIf(sb, this.c1, true);
/* 253 */       renderAtlasRegionIf(sb, this.c3, true);
/* 254 */       renderAtlasRegionIf(sb, this.c4, true);
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 259 */     renderAtlasRegionIf(sb, this.f1, this.renderF1);
/* 260 */     renderAtlasRegionIf(sb, this.f2, this.renderF2);
/* 261 */     renderAtlasRegionIf(sb, this.f3, this.renderF3);
/* 262 */     renderAtlasRegionIf(sb, this.f4, this.renderF4);
/* 263 */     renderAtlasRegionIf(sb, this.f5, this.renderF5);
/*     */     
/*     */ 
/* 266 */     renderAtlasRegionIf(sb, this.s1, this.renderS1);
/* 267 */     renderAtlasRegionIf(sb, this.s2, this.renderS2);
/* 268 */     renderAtlasRegionIf(sb, this.s3, this.renderS3);
/* 269 */     renderAtlasRegionIf(sb, this.s4, this.renderS4);
/* 270 */     renderAtlasRegionIf(sb, this.s5, this.renderS5);
/* 271 */     sb.setBlendFunction(770, 771);
/*     */     
/* 273 */     sb.setColor(new com.badlogic.gdx.graphics.Color(this.overlayColor.r, this.overlayColor.g, this.overlayColor.b, 1.0F));
/*     */     
/*     */ 
/* 276 */     renderAtlasRegionIf(sb, this.i1, this.renderI1);
/* 277 */     renderAtlasRegionIf(sb, this.i2, this.renderI2);
/* 278 */     renderAtlasRegionIf(sb, this.i3, this.renderI3);
/* 279 */     renderAtlasRegionIf(sb, this.i4, this.renderI4);
/* 280 */     renderAtlasRegionIf(sb, this.i5, this.renderI5);
/*     */     
/* 282 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/*     */     
/* 284 */     renderAtlasRegionIf(sb, this.f1, this.renderF1);
/* 285 */     renderAtlasRegionIf(sb, this.f2, this.renderF2);
/* 286 */     renderAtlasRegionIf(sb, this.f3, this.renderF3);
/* 287 */     renderAtlasRegionIf(sb, this.f4, this.renderF4);
/* 288 */     renderAtlasRegionIf(sb, this.f5, this.renderF5);
/*     */   }
/*     */   
/*     */   public void renderCombatRoomFg(com.badlogic.gdx.graphics.g2d.SpriteBatch sb)
/*     */   {
/* 293 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 294 */     renderAtlasRegionIf(sb, this.fg, true);
/*     */   }
/*     */   
/*     */   public void renderCampfireRoom(com.badlogic.gdx.graphics.g2d.SpriteBatch sb)
/*     */   {
/* 299 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 300 */     renderAtlasRegionIf(sb, this.campfireBg, true);
/* 301 */     sb.setBlendFunction(770, 1);
/* 302 */     sb.setColor(new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, MathUtils.cosDeg((float)(System.currentTimeMillis() / 3L % 360L)) / 10.0F + 0.8F));
/* 303 */     renderQuadrupleSize(sb, this.campfireGlow, !com.megacrit.cardcrawl.rooms.CampfireUI.hidden);
/* 304 */     sb.setBlendFunction(770, 771);
/* 305 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 306 */     renderAtlasRegionIf(sb, this.campfireKindling, true);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\TheBeyondScene.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */