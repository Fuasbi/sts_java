/*     */ package com.megacrit.cardcrawl.scenes;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public abstract class AbstractScene
/*     */ {
/*  20 */   private static final Logger logger = LogManager.getLogger(AbstractScene.class.getName());
/*  21 */   private Color bgOverlayColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  22 */   private float bgOverlayTarget = 0.0F;
/*     */   protected com.badlogic.gdx.graphics.Texture img;
/*     */   protected final TextureAtlas atlas;
/*     */   protected final TextureAtlas.AtlasRegion bg;
/*  26 */   protected final TextureAtlas.AtlasRegion campfireBg; protected final TextureAtlas.AtlasRegion campfireGlow; protected final TextureAtlas.AtlasRegion campfireKindling; protected final TextureAtlas.AtlasRegion event; protected boolean isCamp = false;
/*  27 */   private float vertY = 0.0F;
/*     */   
/*     */ 
/*  30 */   protected long ambianceSoundId = 0L;
/*  31 */   protected String ambianceSoundKey = null;
/*     */   protected String ambianceName;
/*     */   
/*     */   public AbstractScene(String atlasUrl) {
/*  35 */     this.atlas = new TextureAtlas(Gdx.files.internal(atlasUrl));
/*  36 */     this.bg = this.atlas.findRegion("bg");
/*  37 */     this.campfireBg = this.atlas.findRegion("campfire");
/*  38 */     this.campfireGlow = this.atlas.findRegion("mod/campfireGlow");
/*  39 */     this.campfireKindling = this.atlas.findRegion("mod/campfireKindling");
/*  40 */     this.event = this.atlas.findRegion("event");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/*  48 */     updateBgOverlay();
/*  49 */     if (this.vertY != 0.0F) {
/*  50 */       this.vertY = com.megacrit.cardcrawl.helpers.MathHelper.uiLerpSnap(this.vertY, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   protected void updateBgOverlay() {
/*  55 */     if (this.bgOverlayColor.a != this.bgOverlayTarget) {
/*  56 */       this.bgOverlayColor.a = MathUtils.lerp(this.bgOverlayColor.a, this.bgOverlayTarget, Gdx.graphics.getDeltaTime() * 2.0F);
/*  57 */       if (Math.abs(this.bgOverlayColor.a - this.bgOverlayTarget) < 0.01F) {
/*  58 */         this.bgOverlayColor.a = this.bgOverlayTarget;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextRoom(AbstractRoom room)
/*     */   {
/*  68 */     this.bgOverlayColor = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*  69 */     this.bgOverlayTarget = 0.5F;
/*     */   }
/*     */   
/*     */   public void changeOverlay(float target) {
/*  73 */     this.bgOverlayTarget = target;
/*     */   }
/*     */   
/*     */   public abstract void renderCombatRoomBg(SpriteBatch paramSpriteBatch);
/*     */   
/*     */   public abstract void renderCombatRoomFg(SpriteBatch paramSpriteBatch);
/*     */   
/*     */   public abstract void renderCampfireRoom(SpriteBatch paramSpriteBatch);
/*     */   
/*     */   public abstract void randomizeScene();
/*     */   
/*     */   public void renderEventRoom(SpriteBatch sb) {
/*  85 */     sb.setColor(Color.WHITE);
/*  86 */     sb.draw(this.event
/*  87 */       .getTexture(), this.event.offsetX * Settings.scale, this.event.offsetY * Settings.scale, 0.0F, 0.0F, this.event.packedWidth, this.event.packedHeight, Settings.scale, Settings.scale, 0.0F, this.event
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  97 */       .getRegionX(), this.event
/*  98 */       .getRegionY(), this.event
/*  99 */       .getRegionWidth(), this.event
/* 100 */       .getRegionHeight(), false, false);
/*     */   }
/*     */   
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 106 */     this.atlas.dispose();
/*     */   }
/*     */   
/*     */   public void fadeOutAmbiance() {
/* 110 */     if (this.ambianceSoundKey != null) {
/* 111 */       logger.info("Fading out ambiance: " + this.ambianceSoundKey);
/* 112 */       CardCrawlGame.sound.fadeOut(this.ambianceSoundKey, this.ambianceSoundId);
/* 113 */       this.ambianceSoundKey = null;
/*     */     }
/*     */   }
/*     */   
/*     */   public void fadeInAmbiance() {
/* 118 */     if (this.ambianceSoundKey == null) {
/* 119 */       logger.info("Fading in ambiance: " + this.ambianceName);
/* 120 */       this.ambianceSoundKey = this.ambianceName;
/* 121 */       this.ambianceSoundId = CardCrawlGame.sound.playAndLoop(this.ambianceName);
/* 122 */       updateAmbienceVolume();
/*     */     }
/*     */   }
/*     */   
/*     */   public void muteAmbienceVolume() {
/* 127 */     if (Settings.AMBIANCE_ON) {
/* 128 */       CardCrawlGame.sound.adjustVolume(this.ambianceName, this.ambianceSoundId, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateAmbienceVolume() {
/* 133 */     if (this.ambianceSoundId != 0L) {
/* 134 */       if (Settings.AMBIANCE_ON) {
/* 135 */         CardCrawlGame.sound.adjustVolume(this.ambianceName, this.ambianceSoundId);
/*     */       } else {
/* 137 */         CardCrawlGame.sound.adjustVolume(this.ambianceName, this.ambianceSoundId, 0.0F);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected void renderAtlasRegionIf(SpriteBatch sb, TextureAtlas.AtlasRegion region, boolean condition) {
/* 143 */     if (condition) {
/* 144 */       sb.draw(region
/* 145 */         .getTexture(), region.offsetX * Settings.scale, region.offsetY * Settings.scale + AbstractDungeon.sceneOffsetY, 0.0F, 0.0F, region.packedWidth, region.packedHeight, Settings.scale, Settings.scale, 0.0F, region
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 155 */         .getRegionX(), region
/* 156 */         .getRegionY(), region
/* 157 */         .getRegionWidth(), region
/* 158 */         .getRegionHeight(), false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void renderQuadrupleSize(SpriteBatch sb, TextureAtlas.AtlasRegion region, boolean condition)
/*     */   {
/* 165 */     if (condition) {
/* 166 */       sb.draw(region
/* 167 */         .getTexture(), region.offsetX * Settings.scale * 2.0F, region.offsetY * Settings.scale + AbstractDungeon.sceneOffsetY, 0.0F, 0.0F, region.packedWidth * 2, region.packedHeight * 2, Settings.scale, Settings.scale, 0.0F, region
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 177 */         .getRegionX(), region
/* 178 */         .getRegionY(), region
/* 179 */         .getRegionWidth(), region
/* 180 */         .getRegionHeight(), false, false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\AbstractScene.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */