/*     */ package com.megacrit.cardcrawl.scenes;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.vfx.scene.LogoFlameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.scene.TitleDustEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class TitleBackground
/*     */ {
/*     */   protected final TextureAtlas atlas;
/*     */   protected final TextureAtlas.AtlasRegion mg3Bot;
/*     */   protected final TextureAtlas.AtlasRegion mg3Top;
/*     */   protected final TextureAtlas.AtlasRegion topGlow;
/*     */   protected final TextureAtlas.AtlasRegion topGlow2;
/*     */   protected final TextureAtlas.AtlasRegion botGlow;
/*     */   protected final TextureAtlas.AtlasRegion sky;
/*  28 */   private static Texture titleLogoImg = null;
/*  29 */   private static int W = 0; private static int H = 0;
/*     */   
/*  31 */   protected ArrayList<TitleCloud> topClouds = new ArrayList();
/*  32 */   protected ArrayList<TitleCloud> midClouds = new ArrayList();
/*  33 */   public float slider = 1.0F;
/*  34 */   private float timer = 1.0F;
/*  35 */   public boolean activated = false;
/*     */   
/*     */ 
/*  38 */   private ArrayList<TitleDustEffect> dust = new ArrayList();
/*  39 */   private ArrayList<TitleDustEffect> dust2 = new ArrayList();
/*  40 */   private ArrayList<LogoFlameEffect> flame = new ArrayList();
/*  41 */   private float dustTimer = 2.0F; private float flameTimer = 0.2F;
/*     */   private static final float FLAME_INTERVAL = 0.05F;
/*  43 */   private float logoAlpha = 1.0F;
/*     */   
/*  45 */   private Color promptTextColor = Settings.CREAM_COLOR.cpy();
/*     */   
/*     */   public TitleBackground() {
/*  48 */     this.promptTextColor.a = 0.0F;
/*  49 */     this.atlas = new TextureAtlas(Gdx.files.internal("title/title.atlas"));
/*  50 */     this.sky = this.atlas.findRegion("jpg/sky");
/*  51 */     this.mg3Bot = this.atlas.findRegion("mg3Bot");
/*  52 */     this.mg3Top = this.atlas.findRegion("mg3Top");
/*  53 */     this.topGlow = this.atlas.findRegion("mg3TopGlow1");
/*  54 */     this.topGlow2 = this.atlas.findRegion("mg3TopGlow2");
/*  55 */     this.botGlow = this.atlas.findRegion("mg3BotGlow");
/*     */     
/*  57 */     for (int i = 1; i < 7; i++) {
/*  58 */       this.topClouds.add(new TitleCloud(this.atlas
/*     */       
/*  60 */         .findRegion("topCloud" + Integer.toString(i)), 
/*  61 */         MathUtils.random(10.0F, 50.0F) * Settings.scale, 
/*  62 */         MathUtils.random(-1920.0F, 1920.0F) * Settings.scale));
/*     */     }
/*     */     
/*  65 */     for (int i = 1; i < 13; i++) {
/*  66 */       this.midClouds.add(new TitleCloud(this.atlas
/*     */       
/*  68 */         .findRegion("midCloud" + Integer.toString(i)), 
/*  69 */         MathUtils.random(-50.0F, -10.0F) * Settings.scale, 
/*  70 */         MathUtils.random(-1920.0F, 1920.0F) * Settings.scale));
/*     */     }
/*     */     
/*  73 */     if (titleLogoImg == null) {
/*  74 */       switch (Settings.language)
/*     */       {
/*     */       }
/*     */       
/*     */       
/*  79 */       titleLogoImg = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/ui/title_logo/eng.png");
/*     */       
/*     */ 
/*  82 */       W = titleLogoImg.getWidth();
/*  83 */       H = titleLogoImg.getHeight();
/*     */     }
/*     */   }
/*     */   
/*     */   public void slideDownInstantly() {
/*  88 */     this.activated = true;
/*  89 */     this.timer = 0.0F;
/*  90 */     this.slider = 0.0F;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  95 */     if (com.megacrit.cardcrawl.core.CardCrawlGame.mainMenuScreen.darken) {
/*  96 */       this.logoAlpha = MathHelper.slowColorLerpSnap(this.logoAlpha, 0.25F);
/*     */     } else {
/*  98 */       this.logoAlpha = MathHelper.slowColorLerpSnap(this.logoAlpha, 1.0F);
/*     */     }
/*     */     
/* 101 */     if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && 
/* 102 */       (!this.activated)) {
/* 103 */       this.activated = true;
/* 104 */       this.timer = 1.0F;
/*     */     }
/*     */     
/*     */ 
/* 108 */     if ((this.activated) && 
/* 109 */       (this.timer != 0.0F)) {
/* 110 */       this.timer -= Gdx.graphics.getDeltaTime();
/* 111 */       if (this.timer < 0.0F) {
/* 112 */         this.timer = 0.0F;
/*     */       }
/* 114 */       if (this.timer < 1.0F) {
/* 115 */         this.slider = com.badlogic.gdx.math.Interpolation.pow4In.apply(0.0F, 1.0F, this.timer);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 120 */     for (TitleCloud c : this.topClouds) {
/* 121 */       c.update();
/*     */     }
/* 123 */     for (TitleCloud c : this.midClouds) {
/* 124 */       c.update();
/*     */     }
/*     */     
/* 127 */     updateDust();
/*     */     
/* 129 */     if (!com.megacrit.cardcrawl.core.CardCrawlGame.mainMenuScreen.isFadingOut) {
/* 130 */       updateFlame();
/*     */     } else {
/* 132 */       this.flame.clear();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFlame() {
/* 137 */     this.flameTimer -= Gdx.graphics.getDeltaTime();
/* 138 */     if (this.flameTimer < 0.0F) {
/* 139 */       this.flameTimer = 0.05F;
/* 140 */       this.flame.add(new LogoFlameEffect());
/*     */     }
/*     */     
/* 143 */     for (Iterator<LogoFlameEffect> e = this.flame.iterator(); e.hasNext();) {
/* 144 */       LogoFlameEffect effect = (LogoFlameEffect)e.next();
/* 145 */       effect.update();
/* 146 */       if (effect.isDone) {
/* 147 */         e.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateDust() {
/* 153 */     this.dustTimer -= Gdx.graphics.getDeltaTime();
/* 154 */     if (this.dustTimer < 0.0F) {
/* 155 */       this.dustTimer = 0.05F;
/* 156 */       this.dust.add(new TitleDustEffect());
/*     */     }
/*     */     
/* 159 */     for (Iterator<TitleDustEffect> e = this.dust.iterator(); e.hasNext();) {
/* 160 */       TitleDustEffect effect = (TitleDustEffect)e.next();
/* 161 */       effect.update();
/* 162 */       if (effect.isDone) {
/* 163 */         e.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 170 */     renderRegion(sb, this.sky, 0.0F, -100.0F * Settings.scale * this.slider);
/*     */     
/*     */ 
/* 173 */     renderRegion(sb, this.mg3Bot, 0.0F, 
/*     */     
/*     */ 
/*     */ 
/* 177 */       MathUtils.round(-45.0F * Settings.scale * this.slider + Settings.HEIGHT - 2219.0F * Settings.scale));
/*     */     
/*     */ 
/* 180 */     renderRegion(sb, this.mg3Top, 0.0F, 
/*     */     
/*     */ 
/*     */ 
/* 184 */       MathUtils.round(-45.0F * Settings.scale * this.slider + Settings.HEIGHT - 1080.0F * Settings.scale));
/*     */     
/*     */ 
/* 187 */     sb.setBlendFunction(770, 1);
/* 188 */     sb.setColor(new Color(1.0F, 0.2F, 0.1F, 0.1F + 
/* 189 */       (MathUtils.cosDeg((float)(System.currentTimeMillis() / 16L % 360L)) + 1.25F) / 5.0F));
/* 190 */     renderRegion(sb, this.botGlow, 0.0F, 
/*     */     
/*     */ 
/*     */ 
/* 194 */       MathUtils.round(-45.0F * Settings.scale * this.slider + Settings.HEIGHT - 2220.0F * Settings.scale));
/* 195 */     renderRegion(sb, this.topGlow, 0.0F, -45.0F * Settings.scale * this.slider + Settings.HEIGHT - 1080.0F * Settings.scale);
/* 196 */     renderRegion(sb, this.topGlow2, 0.0F, -45.0F * Settings.scale * this.slider + Settings.HEIGHT - 1080.0F * Settings.scale);
/* 197 */     sb.setColor(Color.WHITE);
/* 198 */     sb.setBlendFunction(770, 771);
/*     */     
/* 200 */     for (TitleDustEffect e : this.dust2) {
/* 201 */       e.render(sb, 0.0F, -50.0F * Settings.scale * this.slider + Settings.HEIGHT - 1300.0F * Settings.scale);
/*     */     }
/* 203 */     for (TitleDustEffect e : this.dust) {
/* 204 */       e.render(sb, 0.0F, -50.0F * Settings.scale * this.slider + Settings.HEIGHT - 1300.0F * Settings.scale);
/*     */     }
/* 206 */     sb.setColor(Color.WHITE);
/*     */     
/* 208 */     for (TitleCloud c : this.midClouds) {
/* 209 */       c.render(sb, this.slider);
/*     */     }
/* 211 */     for (TitleCloud c : this.topClouds) {
/* 212 */       c.render(sb, this.slider);
/*     */     }
/*     */     
/* 215 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.logoAlpha));
/* 216 */     sb.draw(titleLogoImg, 930.0F * Settings.scale - W / 2.0F, -70.0F * Settings.scale * this.slider + Settings.HEIGHT / 2.0F - H / 2.0F + 14.0F * Settings.scale, W / 2.0F, H / 2.0F, W, H, Settings.scale, Settings.scale, 0.0F, 0, 0, W, H, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 233 */     sb.setBlendFunction(770, 1);
/*     */     
/* 235 */     for (LogoFlameEffect e : this.flame) {
/* 236 */       switch (Settings.language)
/*     */       {
/*     */       }
/*     */       
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 244 */       e.render(sb, Settings.WIDTH / 2.0F, -70.0F * Settings.scale * this.slider + Settings.HEIGHT / 2.0F - 260.0F * Settings.scale);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 251 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */   
/*     */   private void renderRegion(SpriteBatch sb, TextureAtlas.AtlasRegion region, float x, float y) {
/* 255 */     sb.draw(region
/* 256 */       .getTexture(), region.offsetX * Settings.scale + x, region.offsetY * Settings.scale + y, 0.0F, 0.0F, region.packedWidth, region.packedHeight, Settings.scale, Settings.scale, 0.0F, region
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 266 */       .getRegionX(), region
/* 267 */       .getRegionY(), region
/* 268 */       .getRegionWidth(), region
/* 269 */       .getRegionHeight(), false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\TitleBackground.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */