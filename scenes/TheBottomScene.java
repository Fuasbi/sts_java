/*     */ package com.megacrit.cardcrawl.scenes;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class TheBottomScene extends AbstractScene
/*     */ {
/*     */   private boolean renderLeftWall;
/*     */   private boolean renderSolidMid;
/*     */   private boolean renderHollowMid;
/*     */   private TextureAtlas.AtlasRegion fg;
/*     */   private TextureAtlas.AtlasRegion mg;
/*     */   private TextureAtlas.AtlasRegion leftWall;
/*     */   private TextureAtlas.AtlasRegion hollowWall;
/*     */   private TextureAtlas.AtlasRegion solidWall;
/*     */   private boolean renderCeilingMod1;
/*     */   private boolean renderCeilingMod2;
/*     */   private boolean renderCeilingMod3;
/*     */   private boolean renderCeilingMod4;
/*     */   private boolean renderCeilingMod5;
/*     */   private boolean renderCeilingMod6;
/*     */   private TextureAtlas.AtlasRegion ceiling;
/*     */   private TextureAtlas.AtlasRegion ceilingMod1;
/*     */   private TextureAtlas.AtlasRegion ceilingMod2;
/*     */   private TextureAtlas.AtlasRegion ceilingMod3;
/*     */   private TextureAtlas.AtlasRegion ceilingMod4;
/*     */   private TextureAtlas.AtlasRegion ceilingMod5;
/*     */   private TextureAtlas.AtlasRegion ceilingMod6;
/*  36 */   private Color overlayColor = Color.WHITE.cpy();
/*     */   
/*     */ 
/*  39 */   private ArrayList<com.megacrit.cardcrawl.vfx.scene.DustEffect> dust = new ArrayList();
/*  40 */   private ArrayList<com.megacrit.cardcrawl.vfx.scene.BottomFogEffect> fog = new ArrayList();
/*  41 */   private ArrayList<InteractableTorchEffect> torches = new ArrayList();
/*     */   private static final int DUST_AMT = 24;
/*     */   
/*     */   public TheBottomScene() {
/*  45 */     super("bottomScene/scene.atlas");
/*  46 */     this.fg = this.atlas.findRegion("mod/fg");
/*  47 */     this.mg = this.atlas.findRegion("mod/mg");
/*  48 */     this.leftWall = this.atlas.findRegion("mod/mod1");
/*  49 */     this.hollowWall = this.atlas.findRegion("mod/mod2");
/*  50 */     this.solidWall = this.atlas.findRegion("mod/midWall");
/*     */     
/*     */ 
/*  53 */     this.ceiling = this.atlas.findRegion("mod/ceiling");
/*  54 */     this.ceilingMod1 = this.atlas.findRegion("mod/ceilingMod1");
/*  55 */     this.ceilingMod2 = this.atlas.findRegion("mod/ceilingMod2");
/*  56 */     this.ceilingMod3 = this.atlas.findRegion("mod/ceilingMod3");
/*  57 */     this.ceilingMod4 = this.atlas.findRegion("mod/ceilingMod4");
/*  58 */     this.ceilingMod5 = this.atlas.findRegion("mod/ceilingMod5");
/*  59 */     this.ceilingMod6 = this.atlas.findRegion("mod/ceilingMod6");
/*     */     
/*  61 */     this.ambianceName = "AMBIANCE_BOTTOM";
/*  62 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  67 */     super.update();
/*  68 */     updateDust();
/*  69 */     updateFog();
/*  70 */     updateTorches();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateDust()
/*     */   {
/*  77 */     for (Iterator<com.megacrit.cardcrawl.vfx.scene.DustEffect> e = this.dust.iterator(); e.hasNext();) {
/*  78 */       com.megacrit.cardcrawl.vfx.scene.DustEffect effect = (com.megacrit.cardcrawl.vfx.scene.DustEffect)e.next();
/*  79 */       effect.update();
/*  80 */       if (effect.isDone) {
/*  81 */         e.remove();
/*     */       }
/*     */     }
/*     */     
/*  85 */     if (this.dust.size() < 96) {
/*  86 */       this.dust.add(new com.megacrit.cardcrawl.vfx.scene.DustEffect());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFog()
/*     */   {
/*  94 */     if (this.fog.size() < 50) {
/*  95 */       this.fog.add(new com.megacrit.cardcrawl.vfx.scene.BottomFogEffect(true));
/*     */     }
/*     */     
/*  98 */     for (Iterator<com.megacrit.cardcrawl.vfx.scene.BottomFogEffect> e = this.fog.iterator(); e.hasNext();) {
/*  99 */       com.megacrit.cardcrawl.vfx.scene.BottomFogEffect effect = (com.megacrit.cardcrawl.vfx.scene.BottomFogEffect)e.next();
/* 100 */       effect.update();
/* 101 */       if (effect.isDone) {
/* 102 */         e.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateTorches() {
/* 108 */     for (Iterator<InteractableTorchEffect> e = this.torches.iterator(); e.hasNext();) {
/* 109 */       InteractableTorchEffect effect = (InteractableTorchEffect)e.next();
/* 110 */       effect.update();
/* 111 */       if (effect.isDone) {
/* 112 */         e.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextRoom(com.megacrit.cardcrawl.rooms.AbstractRoom room)
/*     */   {
/* 123 */     super.nextRoom(room);
/* 124 */     randomizeScene();
/*     */     
/* 126 */     if ((room instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 127 */       com.megacrit.cardcrawl.core.CardCrawlGame.music.silenceBGM();
/*     */     }
/*     */     
/* 130 */     if (((room instanceof com.megacrit.cardcrawl.rooms.EventRoom)) || ((room instanceof com.megacrit.cardcrawl.rooms.RestRoom))) {
/* 131 */       this.torches.clear();
/*     */     }
/*     */     
/* 134 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */   public void randomizeScene()
/*     */   {
/* 139 */     if (MathUtils.randomBoolean()) {
/* 140 */       this.renderSolidMid = false;
/* 141 */       this.renderLeftWall = false;
/* 142 */       this.renderHollowMid = true;
/* 143 */       if (MathUtils.randomBoolean()) {
/* 144 */         this.renderSolidMid = true;
/* 145 */         if (MathUtils.randomBoolean()) {
/* 146 */           this.renderLeftWall = true;
/*     */         }
/*     */       }
/*     */     } else {
/* 150 */       this.renderLeftWall = false;
/* 151 */       this.renderHollowMid = false;
/* 152 */       this.renderSolidMid = true;
/* 153 */       if (MathUtils.randomBoolean()) {
/* 154 */         this.renderLeftWall = true;
/*     */       }
/*     */     }
/*     */     
/* 158 */     this.renderCeilingMod1 = MathUtils.randomBoolean();
/* 159 */     this.renderCeilingMod2 = MathUtils.randomBoolean();
/* 160 */     this.renderCeilingMod3 = MathUtils.randomBoolean();
/* 161 */     this.renderCeilingMod4 = MathUtils.randomBoolean();
/* 162 */     this.renderCeilingMod5 = MathUtils.randomBoolean();
/* 163 */     this.renderCeilingMod6 = MathUtils.randomBoolean();
/*     */     
/* 165 */     randomizeTorch();
/* 166 */     this.overlayColor.r = MathUtils.random(0.0F, 0.05F);
/* 167 */     this.overlayColor.g = MathUtils.random(0.0F, 0.2F);
/* 168 */     this.overlayColor.b = MathUtils.random(0.0F, 0.2F);
/*     */   }
/*     */   
/*     */ 
/*     */   public void renderCombatRoomBg(SpriteBatch sb)
/*     */   {
/* 174 */     sb.setColor(Color.WHITE);
/* 175 */     renderAtlasRegionIf(sb, this.bg, true);
/*     */     
/* 177 */     if (!this.isCamp) {
/* 178 */       for (com.megacrit.cardcrawl.vfx.scene.BottomFogEffect e : this.fog) {
/* 179 */         e.render(sb);
/*     */       }
/*     */     }
/*     */     
/* 183 */     sb.setColor(Color.WHITE);
/* 184 */     renderAtlasRegionIf(sb, this.mg, true);
/* 185 */     if ((this.renderHollowMid) && ((this.renderSolidMid) || (this.renderLeftWall))) {
/* 186 */       sb.setColor(Color.GRAY);
/*     */     }
/* 188 */     renderAtlasRegionIf(sb, this.solidWall, this.renderSolidMid);
/* 189 */     sb.setColor(Color.WHITE);
/* 190 */     renderAtlasRegionIf(sb, this.hollowWall, this.renderHollowMid);
/* 191 */     renderAtlasRegionIf(sb, this.leftWall, this.renderLeftWall);
/*     */     
/*     */ 
/* 194 */     renderAtlasRegionIf(sb, this.ceiling, true);
/* 195 */     renderAtlasRegionIf(sb, this.ceilingMod1, this.renderCeilingMod1);
/* 196 */     renderAtlasRegionIf(sb, this.ceilingMod2, this.renderCeilingMod2);
/* 197 */     renderAtlasRegionIf(sb, this.ceilingMod3, this.renderCeilingMod3);
/* 198 */     renderAtlasRegionIf(sb, this.ceilingMod4, this.renderCeilingMod4);
/* 199 */     renderAtlasRegionIf(sb, this.ceilingMod5, this.renderCeilingMod5);
/* 200 */     renderAtlasRegionIf(sb, this.ceilingMod6, this.renderCeilingMod6);
/*     */     
/* 202 */     for (InteractableTorchEffect e : this.torches) {
/* 203 */       e.render(sb);
/*     */     }
/*     */     
/* 206 */     sb.setBlendFunction(768, 1);
/* 207 */     sb.setColor(this.overlayColor);
/* 208 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 209 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */   
/*     */   private void randomizeTorch()
/*     */   {
/* 214 */     this.torches.clear();
/*     */     
/* 216 */     if (MathUtils.randomBoolean(0.1F)) {
/* 217 */       this.torches.add(new InteractableTorchEffect(1790.0F * Settings.scale, 850.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.S));
/*     */     }
/*     */     
/* 220 */     if ((this.renderHollowMid) && (!this.renderSolidMid)) {
/* 221 */       int roll = MathUtils.random(2);
/* 222 */       if (roll == 0) {
/* 223 */         this.torches.add(new InteractableTorchEffect(800.0F * Settings.scale, 768.0F * Settings.scale));
/* 224 */         this.torches.add(new InteractableTorchEffect(1206.0F * Settings.scale, 768.0F * Settings.scale));
/* 225 */       } else if (roll == 1) {
/* 226 */         this.torches.add(new InteractableTorchEffect(328.0F * Settings.scale, 865.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.S));
/*     */       }
/* 228 */     } else if ((!this.renderLeftWall) && (!this.renderHollowMid)) {
/* 229 */       if (MathUtils.randomBoolean(0.75F)) {
/* 230 */         this.torches.add(new InteractableTorchEffect(613.0F * Settings.scale, 860.0F * Settings.scale));
/* 231 */         this.torches.add(new InteractableTorchEffect(613.0F * Settings.scale, 672.0F * Settings.scale));
/* 232 */         if (MathUtils.randomBoolean(0.3F)) {
/* 233 */           this.torches.add(new InteractableTorchEffect(1482.0F * Settings.scale, 860.0F * Settings.scale));
/* 234 */           this.torches.add(new InteractableTorchEffect(1482.0F * Settings.scale, 672.0F * Settings.scale));
/*     */         }
/*     */       }
/* 237 */     } else if ((this.renderSolidMid) && (this.renderHollowMid)) {
/* 238 */       if (!this.renderLeftWall) {
/* 239 */         int roll = MathUtils.random(3);
/* 240 */         if (roll == 0) {
/* 241 */           this.torches.add(new InteractableTorchEffect(912.0F * Settings.scale, 790.0F * Settings.scale));
/* 242 */           this.torches.add(new InteractableTorchEffect(912.0F * Settings.scale, 526.0F * Settings.scale));
/* 243 */           this.torches.add(new InteractableTorchEffect(844.0F * Settings.scale, 658.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.S));
/* 244 */           this.torches.add(new InteractableTorchEffect(980.0F * Settings.scale, 658.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.S));
/* 245 */         } else if ((roll == 1) || (roll == 2)) {
/* 246 */           this.torches.add(new InteractableTorchEffect(1828.0F * Settings.scale, 720.0F * Settings.scale));
/*     */         }
/*     */       }
/* 249 */       else if (MathUtils.randomBoolean(0.75F)) {
/* 250 */         this.torches.add(new InteractableTorchEffect(970.0F * Settings.scale, 874.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.L));
/*     */       }
/*     */     }
/* 253 */     else if ((this.renderLeftWall) && (!this.renderHollowMid) && 
/* 254 */       (MathUtils.randomBoolean(0.75F))) {
/* 255 */       this.torches.add(new InteractableTorchEffect(970.0F * Settings.scale, 873.0F * Settings.scale, com.megacrit.cardcrawl.vfx.scene.InteractableTorchEffect.TorchSize.L));
/* 256 */       this.torches.add(new InteractableTorchEffect(616.0F * Settings.scale, 813.0F * Settings.scale));
/* 257 */       this.torches.add(new InteractableTorchEffect(1266.0F * Settings.scale, 708.0F * Settings.scale));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 262 */     com.megacrit.cardcrawl.vfx.scene.LightFlareMEffect.renderGreen = MathUtils.randomBoolean();
/* 263 */     com.megacrit.cardcrawl.vfx.scene.TorchParticleMEffect.renderGreen = com.megacrit.cardcrawl.vfx.scene.LightFlareMEffect.renderGreen;
/*     */   }
/*     */   
/*     */   public void renderCombatRoomFg(SpriteBatch sb)
/*     */   {
/* 268 */     if (!this.isCamp) {
/* 269 */       for (com.megacrit.cardcrawl.vfx.scene.DustEffect e : this.dust) {
/* 270 */         e.render(sb);
/*     */       }
/*     */     }
/* 273 */     sb.setColor(Color.WHITE);
/* 274 */     renderAtlasRegionIf(sb, this.fg, true);
/*     */   }
/*     */   
/*     */   public void renderCampfireRoom(SpriteBatch sb)
/*     */   {
/* 279 */     sb.setColor(Color.WHITE);
/* 280 */     renderAtlasRegionIf(sb, this.campfireBg, true);
/* 281 */     sb.setBlendFunction(770, 1);
/* 282 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, MathUtils.cosDeg((float)(System.currentTimeMillis() / 3L % 360L)) / 10.0F + 0.8F));
/* 283 */     renderQuadrupleSize(sb, this.campfireGlow, !com.megacrit.cardcrawl.rooms.CampfireUI.hidden);
/* 284 */     sb.setBlendFunction(770, 771);
/* 285 */     sb.setColor(Color.WHITE);
/* 286 */     renderAtlasRegionIf(sb, this.campfireKindling, true);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\TheBottomScene.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */