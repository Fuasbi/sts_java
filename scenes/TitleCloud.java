/*    */ package com.megacrit.cardcrawl.scenes;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class TitleCloud {
/*    */   private com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion region;
/*    */   private float x;
/*    */   private float y;
/*    */   private float vX;
/*    */   private float vY;
/*    */   private float sliderJiggle;
/*    */   
/*    */   public TitleCloud(com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion region, float vX, float x) {
/* 14 */     this.region = region;
/* 15 */     this.vX = vX;
/* 16 */     this.x = x;
/* 17 */     this.y = (Settings.HEIGHT - 1100.0F * Settings.scale + com.badlogic.gdx.math.MathUtils.random(-50.0F, 50.0F) * Settings.scale);
/* 18 */     this.vY = (com.badlogic.gdx.math.MathUtils.random(-vX / 10.0F, vX / 10.0F) * Settings.scale);
/* 19 */     this.sliderJiggle = com.badlogic.gdx.math.MathUtils.random(-4.0F, 4.0F);
/*    */   }
/*    */   
/*    */   public void update() {
/* 23 */     this.x += this.vX * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 24 */     this.y += this.vY * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     
/* 26 */     if ((this.vX > 0.0F) && (this.x > 1920.0F * Settings.scale)) {
/* 27 */       this.x = (-1920.0F * Settings.scale);
/* 28 */       this.vX = (com.badlogic.gdx.math.MathUtils.random(10.0F, 50.0F) * Settings.scale);
/* 29 */       this.y = (Settings.HEIGHT - 1100.0F * Settings.scale + com.badlogic.gdx.math.MathUtils.random(-50.0F, 50.0F) * Settings.scale);
/* 30 */       this.vY = (com.badlogic.gdx.math.MathUtils.random(-this.vX / 5.0F, this.vX / 5.0F) * Settings.scale);
/* 31 */     } else if (this.x < -1920.0F * Settings.scale) {
/* 32 */       this.x = (1920.0F * Settings.scale);
/* 33 */       this.vX = (com.badlogic.gdx.math.MathUtils.random(-50.0F, -10.0F) * Settings.scale);
/* 34 */       this.y = (Settings.HEIGHT - 1100.0F * Settings.scale + com.badlogic.gdx.math.MathUtils.random(-50.0F, 50.0F) * Settings.scale);
/* 35 */       this.vY = (com.badlogic.gdx.math.MathUtils.random(-this.vX / 5.0F, this.vX / 5.0F) * Settings.scale);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb, float slider) {
/* 40 */     renderRegion(sb, this.region, this.x, (-55.0F + this.sliderJiggle) * Settings.scale * slider + this.y);
/*    */   }
/*    */   
/*    */   private void renderRegion(com.badlogic.gdx.graphics.g2d.SpriteBatch sb, com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion region, float x, float y) {
/* 44 */     sb.draw(region
/* 45 */       .getTexture(), region.offsetX * Settings.scale + x, region.offsetY * Settings.scale + y, 0.0F, 0.0F, region.packedWidth, region.packedHeight, Settings.scale, Settings.scale, 0.0F, region
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 55 */       .getRegionX(), region
/* 56 */       .getRegionY(), region
/* 57 */       .getRegionWidth(), region
/* 58 */       .getRegionHeight(), false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\TitleCloud.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */