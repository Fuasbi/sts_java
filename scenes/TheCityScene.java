/*     */ package com.megacrit.cardcrawl.scenes;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ 
/*     */ public class TheCityScene extends AbstractScene
/*     */ {
/*     */   private final TextureAtlas.AtlasRegion bg;
/*     */   private final TextureAtlas.AtlasRegion bgGlow;
/*     */   private final TextureAtlas.AtlasRegion bgGlow2;
/*     */   private final TextureAtlas.AtlasRegion bg2;
/*     */   private final TextureAtlas.AtlasRegion bg2Glow;
/*     */   private final TextureAtlas.AtlasRegion floor;
/*     */   private final TextureAtlas.AtlasRegion ceiling;
/*     */   private final TextureAtlas.AtlasRegion wall;
/*     */   private final TextureAtlas.AtlasRegion chains;
/*     */   private final TextureAtlas.AtlasRegion chainsGlow;
/*     */   private final TextureAtlas.AtlasRegion pillar1;
/*     */   private final TextureAtlas.AtlasRegion pillar2;
/*     */   private final TextureAtlas.AtlasRegion pillar3;
/*     */   private final TextureAtlas.AtlasRegion pillar4;
/*     */   private final TextureAtlas.AtlasRegion pillar5;
/*     */   private final TextureAtlas.AtlasRegion throne;
/*     */   private final TextureAtlas.AtlasRegion throneGlow;
/*     */   private final TextureAtlas.AtlasRegion mg;
/*     */   private final TextureAtlas.AtlasRegion mgGlow;
/*     */   private final TextureAtlas.AtlasRegion mg2;
/*     */   private final TextureAtlas.AtlasRegion fg;
/*     */   private final TextureAtlas.AtlasRegion fgGlow;
/*     */   private final TextureAtlas.AtlasRegion fg2;
/*  33 */   private com.badlogic.gdx.graphics.Color overlayColor = com.badlogic.gdx.graphics.Color.WHITE.cpy();
/*     */   private boolean renderAltBg;
/*     */   private boolean renderMg;
/*     */   private boolean renderMgGlow;
/*  37 */   private boolean renderMgAlt; private boolean renderWall; private boolean renderChains; private boolean renderThrone; private boolean renderFg2; private boolean darkDay; private PillarConfig pillarConfig = PillarConfig.OPEN;
/*     */   
/*     */ 
/*  40 */   private float ceilingDustTimer = 1.0F;
/*  41 */   private java.util.ArrayList<com.megacrit.cardcrawl.vfx.scene.FireFlyEffect> fireFlies = new java.util.ArrayList();
/*     */   private static final int FF_AMT = 9;
/*     */   private boolean hasFlies;
/*     */   private boolean blueFlies;
/*     */   
/*  46 */   public TheCityScene() { super("cityScene/scene.atlas");
/*  47 */     this.bg = this.atlas.findRegion("mod/bg1");
/*  48 */     this.bgGlow = this.atlas.findRegion("mod/bgGlowv2");
/*  49 */     this.bgGlow2 = this.atlas.findRegion("mod/bgGlowBlur");
/*  50 */     this.bg2 = this.atlas.findRegion("mod/bg2");
/*  51 */     this.bg2Glow = this.atlas.findRegion("mod/bg2Glow");
/*  52 */     this.floor = this.atlas.findRegion("mod/floor");
/*  53 */     this.ceiling = this.atlas.findRegion("mod/ceiling");
/*  54 */     this.wall = this.atlas.findRegion("mod/wall");
/*  55 */     this.chains = this.atlas.findRegion("mod/chains");
/*  56 */     this.chainsGlow = this.atlas.findRegion("mod/chainsGlow");
/*  57 */     this.pillar1 = this.atlas.findRegion("mod/p1");
/*  58 */     this.pillar2 = this.atlas.findRegion("mod/p2");
/*  59 */     this.pillar3 = this.atlas.findRegion("mod/p3");
/*  60 */     this.pillar4 = this.atlas.findRegion("mod/p4");
/*  61 */     this.pillar5 = this.atlas.findRegion("mod/p5");
/*  62 */     this.throne = this.atlas.findRegion("mod/throne");
/*  63 */     this.throneGlow = this.atlas.findRegion("mod/throneGlow");
/*  64 */     this.mg = this.atlas.findRegion("mod/mg1");
/*  65 */     this.mgGlow = this.atlas.findRegion("mod/mg1Glow");
/*  66 */     this.mg2 = this.atlas.findRegion("mod/mg2");
/*  67 */     this.fg = this.atlas.findRegion("mod/fg");
/*  68 */     this.fgGlow = this.atlas.findRegion("mod/fgGlow");
/*  69 */     this.fg2 = this.atlas.findRegion("mod/fgHideWindow");
/*     */     
/*  71 */     this.ambianceName = "AMBIANCE_CITY";
/*  72 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */   private static enum PillarConfig {
/*  76 */     OPEN,  SIDES_ONLY,  FULL,  LEFT_1,  LEFT_2;
/*     */     
/*     */     private PillarConfig() {}
/*     */   }
/*     */   
/*  81 */   public void update() { super.update();
/*  82 */     updateFireFlies();
/*  83 */     if ((!(com.megacrit.cardcrawl.dungeons.AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.RestRoom)) && 
/*  84 */       (!(com.megacrit.cardcrawl.dungeons.AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.EventRoom))) {
/*  85 */       updateParticles();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFireFlies()
/*     */   {
/*  93 */     for (java.util.Iterator<com.megacrit.cardcrawl.vfx.scene.FireFlyEffect> e = this.fireFlies.iterator(); e.hasNext();) {
/*  94 */       com.megacrit.cardcrawl.vfx.scene.FireFlyEffect effect = (com.megacrit.cardcrawl.vfx.scene.FireFlyEffect)e.next();
/*  95 */       effect.update();
/*  96 */       if (effect.isDone) {
/*  97 */         e.remove();
/*     */       }
/*     */     }
/*     */     
/* 101 */     if ((this.fireFlies.size() < 9) && 
/* 102 */       (MathUtils.randomBoolean(0.1F))) {
/* 103 */       if (this.blueFlies) {
/* 104 */         this.fireFlies.add(new com.megacrit.cardcrawl.vfx.scene.FireFlyEffect(new com.badlogic.gdx.graphics.Color(
/*     */         
/*     */ 
/* 107 */           MathUtils.random(0.1F, 0.2F), 
/* 108 */           MathUtils.random(0.6F, 0.8F), 
/* 109 */           MathUtils.random(0.8F, 1.0F), 1.0F)));
/*     */       }
/*     */       else {
/* 112 */         this.fireFlies.add(new com.megacrit.cardcrawl.vfx.scene.FireFlyEffect(new com.badlogic.gdx.graphics.Color(
/*     */         
/*     */ 
/* 115 */           MathUtils.random(0.8F, 1.0F), 
/* 116 */           MathUtils.random(0.5F, 0.8F), 
/* 117 */           MathUtils.random(0.3F, 0.5F), 1.0F)));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateParticles()
/*     */   {
/* 125 */     this.ceilingDustTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 126 */     if (this.ceilingDustTimer < 0.0F) {
/* 127 */       int roll = MathUtils.random(4);
/* 128 */       if (roll == 0) {
/* 129 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 130 */         playDustSfx(false);
/*     */       }
/* 132 */       else if (roll == 1) {
/* 133 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 134 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 135 */         playDustSfx(false);
/*     */       }
/*     */       else {
/* 138 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 139 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 140 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.scene.CeilingDustEffect());
/* 141 */         playDustSfx(true);
/*     */       }
/*     */       
/* 144 */       this.ceilingDustTimer = MathUtils.random(0.5F, 60.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void playDustSfx(boolean boom) {
/* 149 */     if (boom) {
/* 150 */       int roll = MathUtils.random(2);
/* 151 */       if (roll == 0) {
/* 152 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_BOOM_1", 0.2F);
/* 153 */       } else if (roll == 1) {
/* 154 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_BOOM_2", 0.2F);
/*     */       } else {
/* 156 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_BOOM_3", 0.2F);
/*     */       }
/*     */     } else {
/* 159 */       int roll = MathUtils.random(2);
/* 160 */       if (roll == 0) {
/* 161 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_DUST_1", 0.2F);
/* 162 */       } else if (roll == 1) {
/* 163 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_DUST_2", 0.2F);
/*     */       } else {
/* 165 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CEILING_DUST_3", 0.2F);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void randomizeScene()
/*     */   {
/* 172 */     this.hasFlies = MathUtils.randomBoolean();
/* 173 */     this.blueFlies = MathUtils.randomBoolean();
/*     */     
/*     */ 
/* 176 */     this.overlayColor.r = MathUtils.random(0.8F, 0.9F);
/* 177 */     this.overlayColor.g = MathUtils.random(0.8F, 0.9F);
/* 178 */     this.overlayColor.b = MathUtils.random(0.95F, 1.0F);
/*     */     
/* 180 */     this.darkDay = MathUtils.randomBoolean(0.33F);
/*     */     
/* 182 */     if (this.darkDay) {
/* 183 */       this.overlayColor.r = 0.6F;
/* 184 */       this.overlayColor.g = MathUtils.random(0.7F, 0.8F);
/* 185 */       this.overlayColor.b = MathUtils.random(0.8F, 0.95F);
/*     */     }
/*     */     
/* 188 */     this.renderAltBg = MathUtils.randomBoolean();
/*     */     
/* 190 */     this.renderMg = true;
/* 191 */     if (this.renderMg) {
/* 192 */       this.renderMgAlt = MathUtils.randomBoolean();
/* 193 */       if (!this.renderMgAlt) {
/* 194 */         this.renderMgGlow = MathUtils.randomBoolean();
/*     */       }
/*     */     }
/*     */     
/* 198 */     if ((com.megacrit.cardcrawl.dungeons.AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 199 */       this.renderWall = false;
/*     */     } else {
/* 201 */       this.renderWall = (MathUtils.random(4) == 4);
/*     */     }
/* 203 */     if (this.renderWall) {
/* 204 */       this.renderChains = MathUtils.randomBoolean();
/*     */     } else {
/* 206 */       this.renderChains = false;
/*     */     }
/*     */     
/* 209 */     this.renderFg2 = MathUtils.randomBoolean();
/*     */     
/* 211 */     if (this.renderWall) {
/* 212 */       int roll = MathUtils.random(2);
/* 213 */       if (roll == 0) {
/* 214 */         this.pillarConfig = PillarConfig.OPEN;
/* 215 */       } else if (roll == 1) {
/* 216 */         this.pillarConfig = PillarConfig.LEFT_1;
/*     */       } else {
/* 218 */         this.pillarConfig = PillarConfig.LEFT_2;
/*     */       }
/*     */     } else {
/* 221 */       int roll = MathUtils.random(2);
/* 222 */       if (roll == 0) {
/* 223 */         this.pillarConfig = PillarConfig.OPEN;
/* 224 */       } else if (roll == 1) {
/* 225 */         this.pillarConfig = PillarConfig.SIDES_ONLY;
/*     */       } else {
/* 227 */         this.pillarConfig = PillarConfig.FULL;
/*     */       }
/*     */     }
/*     */     
/* 231 */     if (((com.megacrit.cardcrawl.dungeons.AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) && 
/* 232 */       (com.megacrit.cardcrawl.dungeons.AbstractDungeon.getCurrRoom().monsters.getMonster("TheCollector") != null)) {
/* 233 */       this.renderThrone = true;
/*     */     } else {
/* 235 */       this.renderThrone = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextRoom(com.megacrit.cardcrawl.rooms.AbstractRoom room)
/*     */   {
/* 245 */     super.nextRoom(room);
/* 246 */     this.fireFlies.clear();
/* 247 */     randomizeScene();
/* 248 */     if ((room instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 249 */       com.megacrit.cardcrawl.core.CardCrawlGame.music.silenceBGM();
/*     */     }
/* 251 */     fadeInAmbiance();
/*     */   }
/*     */   
/*     */   public void renderCombatRoomBg(SpriteBatch sb)
/*     */   {
/* 256 */     sb.setColor(this.overlayColor);
/*     */     
/*     */ 
/* 259 */     renderAtlasRegionIf(sb, this.bg, true);
/* 260 */     sb.setBlendFunction(770, 1);
/* 261 */     renderAtlasRegionIf(sb, this.bgGlow, true);
/* 262 */     if (this.darkDay) {
/* 263 */       sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 264 */       renderAtlasRegionIf(sb, this.bgGlow2, true);
/* 265 */       renderAtlasRegionIf(sb, this.bgGlow2, true);
/*     */     }
/* 267 */     sb.setBlendFunction(770, 771);
/*     */     
/*     */ 
/* 270 */     renderAtlasRegionIf(sb, this.bg2, this.renderAltBg);
/* 271 */     sb.setBlendFunction(770, 1);
/* 272 */     renderAtlasRegionIf(sb, this.bg2Glow, this.renderAltBg);
/* 273 */     sb.setBlendFunction(770, 771);
/*     */     
/*     */ 
/* 276 */     sb.setColor(this.overlayColor);
/* 277 */     renderAtlasRegionIf(sb, this.floor, true);
/* 278 */     renderAtlasRegionIf(sb, this.ceiling, true);
/*     */     
/*     */ 
/* 281 */     renderAtlasRegionIf(sb, this.wall, this.renderWall);
/* 282 */     renderAtlasRegionIf(sb, this.chains, this.renderChains);
/* 283 */     if (this.renderChains) {
/* 284 */       sb.setBlendFunction(770, 1);
/* 285 */       sb.setColor(new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, MathUtils.cosDeg((float)(System.currentTimeMillis() / 1L % 360L)) / 10.0F + 0.9F));
/* 286 */       renderAtlasRegionIf(sb, this.chainsGlow, true);
/* 287 */       renderAtlasRegionIf(sb, this.chainsGlow, true);
/* 288 */       sb.setBlendFunction(770, 771);
/* 289 */       sb.setColor(this.overlayColor);
/*     */     }
/*     */     
/*     */ 
/* 293 */     renderAtlasRegionIf(sb, this.mg, this.renderMg);
/* 294 */     sb.setBlendFunction(770, 1);
/* 295 */     if (this.renderMgGlow) {
/* 296 */       sb.setColor(new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, MathUtils.cosDeg((float)(System.currentTimeMillis() / 10L % 360L)) / 2.0F + 0.5F));
/* 297 */       renderAtlasRegionIf(sb, this.mgGlow, this.renderMg);
/* 298 */       renderAtlasRegionIf(sb, this.mgGlow, this.renderMg);
/* 299 */       sb.setColor(this.overlayColor);
/* 300 */       sb.setColor(new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 0.9F, 1.0F));
/*     */     } else {
/* 302 */       renderAtlasRegionIf(sb, this.mgGlow, this.renderMg);
/*     */     }
/* 304 */     sb.setBlendFunction(770, 771);
/* 305 */     renderAtlasRegionIf(sb, this.mg2, this.renderMgAlt);
/*     */     
/*     */ 
/* 308 */     switch (this.pillarConfig) {
/*     */     case OPEN: 
/*     */       break;
/*     */     case SIDES_ONLY: 
/* 312 */       renderAtlasRegionIf(sb, this.pillar1, true);
/* 313 */       renderAtlasRegionIf(sb, this.pillar5, true);
/* 314 */       break;
/*     */     case FULL: 
/* 316 */       renderAtlasRegionIf(sb, this.pillar1, true);
/* 317 */       renderAtlasRegionIf(sb, this.pillar2, true);
/* 318 */       renderAtlasRegionIf(sb, this.pillar3, true);
/* 319 */       renderAtlasRegionIf(sb, this.pillar4, true);
/* 320 */       renderAtlasRegionIf(sb, this.pillar5, true);
/* 321 */       break;
/*     */     case LEFT_1: 
/* 323 */       renderAtlasRegionIf(sb, this.pillar1, true);
/* 324 */       break;
/*     */     case LEFT_2: 
/* 326 */       renderAtlasRegionIf(sb, this.pillar1, true);
/* 327 */       renderAtlasRegionIf(sb, this.pillar2, true);
/*     */     }
/*     */     
/*     */     
/* 331 */     renderAtlasRegionIf(sb, this.throne, this.renderThrone);
/* 332 */     sb.setBlendFunction(770, 1);
/* 333 */     renderAtlasRegionIf(sb, this.throneGlow, this.renderThrone);
/* 334 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */   
/*     */   public void renderCombatRoomFg(SpriteBatch sb)
/*     */   {
/* 339 */     if ((!this.isCamp) && (this.hasFlies)) {
/* 340 */       for (com.megacrit.cardcrawl.vfx.scene.FireFlyEffect e : this.fireFlies) {
/* 341 */         e.render(sb);
/*     */       }
/*     */     }
/* 344 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 345 */     renderAtlasRegionIf(sb, this.fg, true);
/* 346 */     sb.setBlendFunction(770, 1);
/* 347 */     renderAtlasRegionIf(sb, this.fgGlow, true);
/* 348 */     sb.setBlendFunction(770, 771);
/* 349 */     renderAtlasRegionIf(sb, this.fg2, this.renderFg2);
/*     */   }
/*     */   
/*     */   public void renderCampfireRoom(SpriteBatch sb)
/*     */   {
/* 354 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 355 */     renderAtlasRegionIf(sb, this.campfireBg, true);
/* 356 */     sb.setBlendFunction(770, 1);
/* 357 */     sb.setColor(new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, MathUtils.cosDeg((float)(System.currentTimeMillis() / 3L % 360L)) / 10.0F + 0.8F));
/* 358 */     renderQuadrupleSize(sb, this.campfireGlow, !com.megacrit.cardcrawl.rooms.CampfireUI.hidden);
/* 359 */     sb.setBlendFunction(770, 771);
/* 360 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 361 */     renderAtlasRegionIf(sb, this.campfireKindling, true);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\scenes\TheCityScene.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */