/*     */ package com.megacrit.cardcrawl.credits;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.SwingIn;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.CreditStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class CreditsScreen
/*     */ {
/*  26 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CreditsScreen");
/*     */   
/*  28 */   private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  29 */   private float fadeInTimer = 2.0F;
/*     */   private float targetY;
/*  31 */   private float currentY; private static final float scrollSpeed = 50.0F * Settings.scale;
/*     */   private static final int W = 720;
/*  33 */   private ArrayList<CreditLine> lines = new ArrayList();
/*     */   private static final float LINE_SPACING = 45.0F;
/*  35 */   private static final float SECTION_SPACING = 150.0F; private static final float SCROLL_START_Y = 400.0F * Settings.scale;
/*     */   private static final float THANK_YOU_TIME = 3.0F;
/*  37 */   private float thankYouTimer = 3.0F;
/*  38 */   private Color thankYouColor = Settings.CREAM_COLOR.cpy();
/*  39 */   private static final float END_OF_CREDITS_Y = 10000.0F * Settings.scale;
/*  40 */   private String THANKS_MSG = CardCrawlGame.languagePack.getCreditString("THANKS_MSG").HEADER;
/*  41 */   private static com.badlogic.gdx.graphics.Texture logoImg = null;
/*     */   
/*     */   private float skipTimer;
/*     */   private static final float SKIP_MENU_UP_DUR = 2.0F;
/*     */   private static final float SKIP_APPEAR_TIME = 0.5F;
/*     */   private boolean isSkippable;
/*     */   private boolean closingSkipMenu;
/*  48 */   private static final float SKIP_START_X = -300.0F * Settings.scale; private static final float SKIP_END_X = 50.0F * Settings.scale;
/*     */   private float skipX;
/*  50 */   private float tmpY = -400.0F;
/*     */   
/*     */   public CreditsScreen() {
/*  53 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.CREDITS;
/*  54 */     this.currentY = SCROLL_START_Y;
/*  55 */     this.targetY = this.currentY;
/*  56 */     creditLineHelper("DEV");
/*  57 */     creditLineHelper("OPS");
/*  58 */     creditLineHelper("SOUND");
/*  59 */     creditLineHelper("VOICE");
/*  60 */     creditLineHelper("PORTRAITS");
/*  61 */     creditLineHelper("ILLUSTRATION");
/*  62 */     creditLineHelper("ANIMATION");
/*  63 */     creditLineHelper("LOC_ZHS");
/*  64 */     creditLineHelper("LOC_ZHT");
/*  65 */     creditLineHelper("LOC_DEU");
/*  66 */     creditLineHelper("LOC_EPO");
/*  67 */     creditLineHelper("LOC_FRA");
/*  68 */     creditLineHelper("LOC_GRE");
/*  69 */     creditLineHelper("LOC_IND");
/*  70 */     creditLineHelper("LOC_ITA");
/*  71 */     creditLineHelper("LOC_JPN");
/*  72 */     creditLineHelper("LOC_KOR");
/*  73 */     creditLineHelper("LOC_NOR");
/*  74 */     creditLineHelper("LOC_POL");
/*  75 */     creditLineHelper("LOC_PTB");
/*  76 */     creditLineHelper("LOC_RUS");
/*  77 */     creditLineHelper("LOC_SPA");
/*  78 */     creditLineHelper("LOC_SRB");
/*  79 */     creditLineHelper("LOC_TUR");
/*  80 */     creditLineHelper("LOC_UKR");
/*  81 */     creditLineHelper("LOC_ADDITIONAL");
/*  82 */     creditLineHelper("TEST");
/*  83 */     creditLineHelper("SPECIAL");
/*     */     
/*  85 */     if (logoImg == null) {
/*  86 */       switch (Settings.language)
/*     */       {
/*     */       }
/*     */       
/*     */       
/*  91 */       logoImg = ImageMaster.loadImage("images/ui/credits_logo/eng.png");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void creditLineHelper(String id)
/*     */   {
/*  98 */     CreditStrings str = CardCrawlGame.languagePack.getCreditString(id);
/*     */     
/* 100 */     this.lines.add(new CreditLine(str.HEADER, this.tmpY -= 150.0F, true));
/* 101 */     for (int i = 0; i < str.NAMES.length; i++) {
/* 102 */       this.lines.add(new CreditLine(str.NAMES[i], this.tmpY -= 45.0F, false));
/*     */     }
/*     */   }
/*     */   
/*     */   public void open() {
/* 107 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.CREDITS;
/* 108 */     this.skipTimer = 0.0F;
/* 109 */     this.isSkippable = false;
/* 110 */     this.closingSkipMenu = false;
/* 111 */     this.skipX = SKIP_START_X;
/* 112 */     com.megacrit.cardcrawl.core.GameCursor.hidden = true;
/* 113 */     this.thankYouColor.a = 0.0F;
/* 114 */     this.thankYouTimer = 3.0F;
/* 115 */     CardCrawlGame.mainMenuScreen.darken();
/* 116 */     this.fadeInTimer = 2.0F;
/* 117 */     this.currentY = SCROLL_START_Y;
/* 118 */     this.targetY = this.currentY;
/*     */   }
/*     */   
/*     */   public void update() {
/* 122 */     if (InputHelper.pressedEscape) {
/* 123 */       InputHelper.pressedEscape = false;
/* 124 */       close();
/*     */     }
/*     */     
/* 127 */     if (InputHelper.isMouseDown_R) {
/* 128 */       this.targetY -= Gdx.graphics.getDeltaTime() * scrollSpeed * 4.0F;
/*     */     } else {
/* 130 */       this.targetY += Gdx.graphics.getDeltaTime() * scrollSpeed;
/* 131 */       if (this.currentY > END_OF_CREDITS_Y)
/*     */       {
/* 133 */         this.thankYouTimer -= Gdx.graphics.getDeltaTime();
/* 134 */         if (this.thankYouTimer < 0.0F) {
/* 135 */           this.thankYouTimer = 0.0F;
/*     */         }
/* 137 */         this.thankYouColor.a = Interpolation.fade.apply(1.0F, 0.0F, this.thankYouTimer / 3.0F);
/*     */       }
/*     */     }
/*     */     
/* 141 */     if (Gdx.input.isKeyJustPressed(62)) {
/* 142 */       this.targetY = SCROLL_START_Y;
/*     */     }
/*     */     
/* 145 */     if (InputHelper.scrolledUp) {
/* 146 */       this.targetY -= 100.0F * Settings.scale;
/* 147 */     } else if (InputHelper.scrolledDown) {
/* 148 */       this.targetY += 100.0F * Settings.scale;
/*     */     }
/*     */     
/* 151 */     this.currentY = MathHelper.scrollSnapLerpSpeed(this.currentY, this.targetY);
/*     */     
/* 153 */     updateFade();
/* 154 */     skipLogic();
/*     */   }
/*     */   
/*     */   private void skipLogic() {
/* 158 */     if ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) {
/* 159 */       if (this.isSkippable) {
/* 160 */         close();
/* 161 */       } else if ((!this.isSkippable) && (this.skipTimer == 0.0F)) {
/* 162 */         this.skipTimer = 0.5F;
/* 163 */         this.skipX = SKIP_END_X;
/*     */       }
/*     */     }
/*     */     
/* 167 */     if (this.skipTimer != 0.0F) {
/* 168 */       this.skipTimer -= Gdx.graphics.getDeltaTime();
/*     */       
/* 170 */       if ((!this.isSkippable) && (!this.closingSkipMenu)) {
/* 171 */         this.skipX = Interpolation.swingIn.apply(SKIP_END_X, SKIP_START_X, this.skipTimer * 2.0F);
/* 172 */       } else if (this.closingSkipMenu) {
/* 173 */         this.skipX = Interpolation.fade.apply(SKIP_START_X, SKIP_END_X, this.skipTimer * 2.0F);
/*     */       } else {
/* 175 */         this.skipX = SKIP_END_X;
/*     */       }
/*     */       
/* 178 */       if (this.skipTimer < 0.0F) {
/* 179 */         if ((!this.isSkippable) && (!this.closingSkipMenu)) {
/* 180 */           this.isSkippable = true;
/* 181 */           this.skipTimer = 2.0F;
/* 182 */         } else if (!this.closingSkipMenu) {
/* 183 */           this.closingSkipMenu = true;
/* 184 */           this.isSkippable = false;
/* 185 */           this.skipTimer = 0.5F;
/*     */         } else {
/* 187 */           this.isSkippable = false;
/* 188 */           this.closingSkipMenu = false;
/* 189 */           this.skipTimer = 0.0F;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void close() {
/* 196 */     if (CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.CREDITS) {
/* 197 */       CardCrawlGame.mainMenuScreen.lighten();
/* 198 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.MAIN_MENU;
/* 199 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFade() {
/* 204 */     this.fadeInTimer -= Gdx.graphics.getDeltaTime();
/* 205 */     if (this.fadeInTimer < 0.0F) {
/* 206 */       this.fadeInTimer = 0.0F;
/*     */     }
/* 208 */     this.screenColor.a = Interpolation.fade.apply(1.0F, 0.0F, this.fadeInTimer / 2.0F);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 212 */     sb.setColor(this.screenColor);
/* 213 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 215 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.screenColor.a));
/* 216 */     sb.draw(logoImg, Settings.WIDTH / 2.0F - 360.0F, this.currentY - 360.0F, 360.0F, 360.0F, 720.0F, 720.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 720, 720, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 234 */     for (CreditLine c : this.lines) {
/* 235 */       c.render(sb, this.currentY);
/*     */     }
/*     */     
/* 238 */     FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.THANKS_MSG, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, this.thankYouColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 247 */     if (!Settings.isControllerMode) {
/* 248 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipBodyFont, uiStrings.TEXT[0], this.skipX, 50.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 256 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipBodyFont, uiStrings.TEXT[1], this.skipX + 46.0F * Settings.scale, 50.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 263 */       sb.setColor(Color.WHITE);
/* 264 */       sb.draw(CInputActionSet.select
/* 265 */         .getKeyImg(), this.skipX - 32.0F + 10.0F * Settings.scale, -32.0F + 44.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.9F, Settings.scale * 0.9F, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\credits\CreditsScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */