/*    */ package com.megacrit.cardcrawl.screens;
/*    */ 
/*    */ public class DisplayOption implements Comparable<DisplayOption> {
/*    */   public int width;
/*    */   public int height;
/*    */   
/*  7 */   public DisplayOption(int width, int height) { this.width = width;
/*  8 */     this.height = height;
/*    */   }
/*    */   
/*    */   public int compareTo(DisplayOption other)
/*    */   {
/* 13 */     if (this.width == other.width) {
/* 14 */       if (this.height == other.height)
/* 15 */         return 0;
/* 16 */       if (this.height < other.height) {
/* 17 */         return -1;
/*    */       }
/* 19 */       return 1;
/*    */     }
/* 21 */     if (this.width < other.width) {
/* 22 */       return -1;
/*    */     }
/* 24 */     return 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean equals(Object other)
/*    */   {
/* 30 */     if ((((DisplayOption)other).width == this.width) && 
/* 31 */       (((DisplayOption)other).height == this.height)) {
/* 32 */       return true;
/*    */     }
/*    */     
/* 35 */     return false;
/*    */   }
/*    */   
/*    */   public String toString()
/*    */   {
/* 40 */     return "(" + this.width + "," + this.height + ")";
/*    */   }
/*    */   
/*    */   public String uiString() {
/* 44 */     return this.width + " x " + this.height;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DisplayOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */