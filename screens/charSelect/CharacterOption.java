/*     */ package com.megacrit.cardcrawl.screens.charSelect;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.characters.Ironclad;
/*     */ import com.megacrit.cardcrawl.characters.TheSilent;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*     */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ConfirmButton;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class CharacterOption
/*     */ {
/*  33 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CharacterOption");
/*  34 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private com.badlogic.gdx.graphics.Texture buttonImg;
/*     */   private String portraitUrl;
/*     */   public AbstractPlayer.PlayerClass c;
/*  39 */   public boolean selected = false; public boolean locked = false;
/*     */   public Hitbox hb;
/*  41 */   private static final float HB_W = 150.0F * Settings.scale;
/*     */   private static final int BUTTON_W = 220;
/*     */   private static final String ASSETS_DIR = "images/ui/charSelect/";
/*  44 */   private static final Color BLACK_OUTLINE_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.5F);
/*     */   
/*     */   private static final int ICON_W = 64;
/*     */   
/*  48 */   private static final float DEST_INFO_X = 200.0F * Settings.scale;
/*  49 */   private static final float START_INFO_X = -800.0F * Settings.scale; private static final float START_INFO_Y = Settings.HEIGHT / 2.0F;
/*  50 */   private float infoX = START_INFO_X; private float infoY = START_INFO_Y;
/*  51 */   public String name = "";
/*  52 */   private static final float NAME_OFFSET_Y = 200.0F * Settings.scale;
/*     */   private String hp;
/*     */   private int gold;
/*     */   private String flavorText;
/*     */   private CharSelectInfo charInfo;
/*     */   private int unlocksRemaining;
/*  58 */   private int maxAscensionLevel = 1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CharacterOption(String optionName, AbstractPlayer.PlayerClass c, String buttonUrl, String portraitImg)
/*     */   {
/*  69 */     this.name = optionName;
/*  70 */     this.hb = new Hitbox(HB_W, HB_W);
/*  71 */     this.buttonImg = ImageMaster.loadImage("images/ui/charSelect/" + buttonUrl);
/*  72 */     this.portraitUrl = portraitImg;
/*  73 */     this.c = c;
/*  74 */     this.charInfo = null;
/*  75 */     switch (c) {
/*     */     case IRONCLAD: 
/*  77 */       this.charInfo = Ironclad.getLoadout();
/*  78 */       break;
/*     */     case THE_SILENT: 
/*  80 */       this.charInfo = TheSilent.getLoadout();
/*  81 */       break;
/*     */     case DEFECT: 
/*  83 */       this.charInfo = com.megacrit.cardcrawl.characters.Defect.getLoadout();
/*  84 */       break;
/*     */     }
/*     */     
/*     */     
/*  88 */     this.hp = this.charInfo.hp;
/*  89 */     this.gold = this.charInfo.gold;
/*  90 */     this.flavorText = this.charInfo.flavorText;
/*  91 */     this.unlocksRemaining = (5 - UnlockTracker.getUnlockLevel(c));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public CharacterOption(AbstractPlayer.PlayerClass c)
/*     */   {
/*  98 */     this.hb = new Hitbox(HB_W, HB_W);
/*  99 */     this.buttonImg = ImageMaster.loadImage("images/ui/charSelect/lockedButton.png");
/* 100 */     this.locked = true;
/* 101 */     this.c = c;
/*     */   }
/*     */   
/*     */   public void incrementAscensionLevel(int level) {
/* 105 */     if (level > this.maxAscensionLevel) {
/* 106 */       return;
/*     */     }
/*     */     
/* 109 */     CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel = level;
/* 110 */     CardCrawlGame.mainMenuScreen.charSelectScreen.ascLevelInfoString = CharacterSelectScreen.A_TEXT[(level - 1)];
/*     */   }
/*     */   
/*     */   public void decrementAscensionLevel(int level) {
/* 114 */     if (level == 0) {
/* 115 */       return;
/*     */     }
/*     */     
/* 118 */     CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel = level;
/* 119 */     CardCrawlGame.mainMenuScreen.charSelectScreen.ascLevelInfoString = CharacterSelectScreen.A_TEXT[(level - 1)];
/*     */   }
/*     */   
/*     */   public void move(float x, float y) {
/* 123 */     this.hb.move(x, y);
/*     */   }
/*     */   
/*     */   public void update() {
/* 127 */     updateHitbox();
/* 128 */     updateInfoPosition();
/*     */   }
/*     */   
/*     */   private void updateHitbox() {
/* 132 */     this.hb.update();
/* 133 */     if (this.hb.justHovered) {
/* 134 */       CardCrawlGame.sound.playA("UI_HOVER", -0.3F);
/*     */     }
/*     */     
/* 137 */     if ((this.hb.hovered) && (this.locked)) {
/* 138 */       if (this.c == AbstractPlayer.PlayerClass.THE_SILENT) {
/* 139 */         TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY - 10.0F * Settings.scale, TEXT[0], TEXT[1]);
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/* 144 */       else if (this.c == AbstractPlayer.PlayerClass.DEFECT) {
/* 145 */         TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY - 10.0F * Settings.scale, TEXT[0], TEXT[3]);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 153 */     if ((InputHelper.justClickedLeft) && (!this.locked) && 
/* 154 */       (this.hb.hovered)) {
/* 155 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.4F);
/* 156 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/*     */ 
/* 160 */     if (this.hb.clicked) {
/* 161 */       this.hb.clicked = false;
/*     */       
/* 163 */       if (!this.selected) {
/* 164 */         CardCrawlGame.mainMenuScreen.charSelectScreen.deselectOtherOptions(this);
/* 165 */         this.selected = true;
/* 166 */         CardCrawlGame.mainMenuScreen.charSelectScreen.justSelected();
/* 167 */         CardCrawlGame.chosenCharacter = this.c;
/* 168 */         CardCrawlGame.mainMenuScreen.charSelectScreen.confirmButton.isDisabled = false;
/* 169 */         CardCrawlGame.mainMenuScreen.charSelectScreen.confirmButton.show();
/* 170 */         CardCrawlGame.mainMenuScreen.charSelectScreen.bgCharImg = ImageMaster.loadImage("images/ui/charSelect/" + this.portraitUrl);
/*     */         
/*     */ 
/* 173 */         Prefs pref = null;
/*     */         
/*     */ 
/* 176 */         switch (this.c) {
/*     */         case IRONCLAD: 
/* 178 */           pref = CardCrawlGame.ironcladPrefs;
/* 179 */           CardCrawlGame.sound.playA("ATTACK_HEAVY", MathUtils.random(-0.2F, 0.2F));
/* 180 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, true);
/* 181 */           break;
/*     */         case THE_SILENT: 
/* 183 */           pref = CardCrawlGame.silentPrefs;
/* 184 */           CardCrawlGame.sound.playA("ATTACK_DAGGER_2", MathUtils.random(-0.2F, 0.2F));
/* 185 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/* 186 */           break;
/*     */         case DEFECT: 
/* 188 */           pref = CardCrawlGame.defectPrefs;
/* 189 */           CardCrawlGame.sound.playA("ATTACK_MAGIC_BEAM_SHORT", MathUtils.random(-0.2F, 0.2F));
/* 190 */           CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/* 191 */           break;
/*     */         }
/*     */         
/*     */         
/*     */ 
/* 196 */         if (pref != null) {
/* 197 */           CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel = pref.getInteger("ASCENSION_LEVEL", 1);
/*     */           
/*     */ 
/* 200 */           if (20 < CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel) {
/* 201 */             CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel = 20;
/*     */           }
/* 203 */           this.maxAscensionLevel = pref.getInteger("ASCENSION_LEVEL", 1);
/* 204 */           if (20 < this.maxAscensionLevel) {
/* 205 */             this.maxAscensionLevel = 20;
/*     */           }
/* 207 */           int ascensionLevel = CardCrawlGame.mainMenuScreen.charSelectScreen.ascensionLevel;
/* 208 */           if (ascensionLevel > this.maxAscensionLevel) {
/* 209 */             ascensionLevel = this.maxAscensionLevel;
/*     */           }
/*     */           
/* 212 */           if (ascensionLevel > 0) {
/* 213 */             CardCrawlGame.mainMenuScreen.charSelectScreen.ascLevelInfoString = CharacterSelectScreen.A_TEXT[(ascensionLevel - 1)];
/*     */           }
/*     */           else {
/* 216 */             CardCrawlGame.mainMenuScreen.charSelectScreen.ascLevelInfoString = "";
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateInfoPosition() {
/* 224 */     if (this.selected) {
/* 225 */       this.infoX = MathHelper.uiLerpSnap(this.infoX, DEST_INFO_X);
/*     */     } else {
/* 227 */       this.infoX = MathHelper.uiLerpSnap(this.infoX, START_INFO_X);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 232 */     renderOptionButton(sb);
/* 233 */     renderInfo(sb);
/* 234 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderOptionButton(SpriteBatch sb) {
/* 238 */     if (this.selected) {
/* 239 */       sb.setColor(new Color(1.0F, 0.8F, 0.2F, 0.25F + 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 244 */         (MathUtils.cosDeg((float)(System.currentTimeMillis() / 4L % 360L)) + 1.25F) / 3.5F));
/*     */     } else {
/* 246 */       sb.setColor(BLACK_OUTLINE_COLOR);
/*     */     }
/* 248 */     sb.draw(ImageMaster.CHAR_OPT_HIGHLIGHT, this.hb.cX - 110.0F, this.hb.cY - 110.0F, 110.0F, 110.0F, 220.0F, 220.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 220, 220, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 266 */     if ((this.selected) || (this.hb.hovered)) {
/* 267 */       sb.setColor(Color.WHITE);
/*     */     } else {
/* 269 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     }
/* 271 */     sb.draw(this.buttonImg, this.hb.cX - 110.0F, this.hb.cY - 110.0F, 110.0F, 110.0F, 220.0F, 220.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 220, 220, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderInfo(SpriteBatch sb)
/*     */   {
/* 291 */     if (!this.name.equals("")) {
/* 292 */       FontHelper.renderSmartText(sb, FontHelper.bannerNameFont, this.name, this.infoX - 35.0F * Settings.scale, this.infoY + NAME_OFFSET_Y, 99999.0F, 38.0F * Settings.scale, Settings.GOLD_COLOR
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 300 */         .cpy());
/*     */       
/*     */ 
/* 303 */       sb.draw(ImageMaster.TP_HP, this.infoX - 10.0F * Settings.scale - 32.0F, this.infoY + 95.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 321 */       FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, TEXT[4] + this.hp, this.infoX + 18.0F * Settings.scale, this.infoY + 102.0F * Settings.scale, 10000.0F, 10000.0F, Settings.RED_TEXT_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 332 */       sb.draw(ImageMaster.TP_GOLD, this.infoX + 190.0F * Settings.scale - 32.0F, this.infoY + 95.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 350 */       FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, TEXT[5] + 
/*     */       
/*     */ 
/* 353 */         Integer.toString(this.gold), this.infoX + 220.0F * Settings.scale, this.infoY + 102.0F * Settings.scale, 10000.0F, 10000.0F, Settings.GOLD_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 360 */       FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, this.flavorText, this.infoX - 26.0F * Settings.scale, this.infoY + 40.0F * Settings.scale, 10000.0F, 30.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 370 */       if (this.unlocksRemaining > 0) {
/* 371 */         FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, 
/*     */         
/*     */ 
/* 374 */           Integer.toString(this.unlocksRemaining) + TEXT[6], this.infoX - 26.0F * Settings.scale, this.infoY - 112.0F * Settings.scale, 10000.0F, 10000.0F, Settings.CREAM_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 381 */         int unlockProgress = UnlockTracker.getCurrentProgress(this.c);
/* 382 */         int unlockCost = UnlockTracker.getCurrentScoreCost(this.c);
/*     */         
/* 384 */         FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, 
/*     */         
/*     */ 
/* 387 */           Integer.toString(unlockProgress) + "/" + unlockCost + TEXT[9], this.infoX - 26.0F * Settings.scale, this.infoY - 140.0F * Settings.scale, 10000.0F, 10000.0F, Settings.CREAM_COLOR);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 395 */       renderRelics(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderRelics(SpriteBatch sb) {
/* 400 */     if (this.charInfo.relics.size() == 1)
/*     */     {
/* 402 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/* 403 */       sb.draw(
/* 404 */         RelicLibrary.getRelic((String)this.charInfo.relics.get(0)).outlineImg, this.infoX - 64.0F, this.infoY - 60.0F * Settings.scale - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 421 */       sb.setColor(Color.WHITE);
/* 422 */       sb.draw(
/* 423 */         RelicLibrary.getRelic((String)this.charInfo.relics.get(0)).img, this.infoX - 64.0F, this.infoY - 60.0F * Settings.scale - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 440 */       FontHelper.renderSmartText(sb, FontHelper.tipHeaderFont, 
/*     */       
/*     */ 
/* 443 */         RelicLibrary.getRelic((String)this.charInfo.relics.get(0)).name, this.infoX + 44.0F * Settings.scale, this.infoY - 40.0F * Settings.scale, 10000.0F, 10000.0F, Settings.GOLD_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 450 */       String relicString = RelicLibrary.getRelic((String)this.charInfo.relics.get(0)).description;
/* 451 */       if (this.charInfo.name.equals(TEXT[7])) {
/* 452 */         relicString = TEXT[8];
/*     */       }
/*     */       
/* 455 */       FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, relicString, this.infoX + 44.0F * Settings.scale, this.infoY - 66.0F * Settings.scale, 10000.0F, 10000.0F, Settings.CREAM_COLOR);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 465 */       for (int i = 0; i < this.charInfo.relics.size(); i++) {
/* 466 */         AbstractRelic r = RelicLibrary.getRelic((String)this.charInfo.relics.get(i));
/* 467 */         r.updateDescription(this.charInfo.color);
/*     */         
/*     */ 
/*     */ 
/* 471 */         Hitbox relicHitbox = new Hitbox(80.0F * Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics.size())), 80.0F * Settings.scale);
/*     */         
/* 473 */         relicHitbox.move(this.infoX + i * 72.0F * Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/* 474 */           .size())), this.infoY - 60.0F * Settings.scale);
/*     */         
/* 476 */         relicHitbox.render(sb);
/* 477 */         relicHitbox.update();
/* 478 */         if (relicHitbox.hovered) {
/* 479 */           if (InputHelper.mX < 1400.0F * Settings.scale) {
/* 480 */             TipHelper.queuePowerTips(InputHelper.mX + 60.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, r.tips);
/*     */ 
/*     */           }
/*     */           else
/*     */           {
/* 485 */             TipHelper.queuePowerTips(InputHelper.mX - 350.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, r.tips);
/*     */           }
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 493 */         sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/* 494 */         sb.draw(r.outlineImg, this.infoX - 64.0F + i * 72.0F * Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/*     */         
/* 496 */           .size())), this.infoY - 60.0F * Settings.scale - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 502 */           .size())), Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/* 503 */           .size())), 0.0F, 0, 0, 128, 128, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 512 */         sb.setColor(Color.WHITE);
/* 513 */         sb.draw(r.img, this.infoX - 64.0F + i * 72.0F * Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/*     */         
/* 515 */           .size())), this.infoY - 60.0F * Settings.scale - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 521 */           .size())), Settings.scale * (0.01F + (1.0F - 0.019F * this.charInfo.relics
/* 522 */           .size())), 0.0F, 0, 0, 128, 128, false, false);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\charSelect\CharacterOption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */