/*     */ package com.megacrit.cardcrawl.screens.charSelect;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.TrialHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader;
/*     */ import com.megacrit.cardcrawl.metrics.BotDataUploader.GameDataType;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ConfirmButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.SeedPanel;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class CharacterSelectScreen
/*     */ {
/*  35 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CharacterSelectScreen");
/*  36 */   public static final String[] TEXT = uiStrings.TEXT;
/*  37 */   private static final UIStrings uiStrings2 = CardCrawlGame.languagePack.getUIString("AscensionModeDescriptions");
/*  38 */   public static final String[] A_TEXT = uiStrings2.TEXT;
/*     */   
/*     */   private static float ASC_LEFT_W;
/*     */   private static float ASC_RIGHT_W;
/*  42 */   private SeedPanel seedPanel = new SeedPanel();
/*  43 */   private static final float SEED_X = 30.0F * Settings.scale;
/*  44 */   private static final float SEED_Y = 90.0F * Settings.scale;
/*     */   
/*  46 */   private static final String CHOOSE_CHAR_MSG = TEXT[0];
/*  47 */   public ConfirmButton confirmButton = new ConfirmButton(TEXT[1]);
/*  48 */   public MenuCancelButton cancelButton = new MenuCancelButton();
/*  49 */   public ArrayList<CharacterOption> options = new ArrayList();
/*  50 */   private boolean anySelected = false;
/*     */   
/*     */ 
/*  53 */   public com.badlogic.gdx.graphics.Texture bgCharImg = null;
/*  54 */   private Color bgCharColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  55 */   private static final float BG_Y_OFFSET_TARGET = 100.0F * Settings.scale;
/*  56 */   private float bg_y_offset = BG_Y_OFFSET_TARGET / 2.0F;
/*     */   
/*     */ 
/*  59 */   public boolean isAscensionMode = false;
/*  60 */   private boolean isAscensionModeUnlocked = false;
/*     */   private Hitbox ascensionModeHb;
/*  62 */   private Hitbox ascLeftHb; private Hitbox ascRightHb; private Hitbox seedHb; public int ascensionLevel = 0;
/*  63 */   public String ascLevelInfoString = "";
/*     */   
/*     */   public void initialize()
/*     */   {
/*  67 */     this.options.add(new CharacterOption(TEXT[2], AbstractPlayer.PlayerClass.IRONCLAD, "ironcladButton.png", "ironcladPortrait.jpg"));
/*     */     
/*     */ 
/*  70 */     if (!UnlockTracker.isCharacterLocked("The Silent")) {
/*  71 */       this.options.add(new CharacterOption(TEXT[3], AbstractPlayer.PlayerClass.THE_SILENT, "silentButton.png", "silentPortrait.jpg"));
/*     */     } else {
/*  73 */       this.options.add(new CharacterOption(AbstractPlayer.PlayerClass.THE_SILENT));
/*     */     }
/*     */     
/*     */ 
/*  77 */     if (!UnlockTracker.isCharacterLocked("Defect")) {
/*  78 */       this.options.add(new CharacterOption(TEXT[4], AbstractPlayer.PlayerClass.DEFECT, "defectButton.png", "defectPortrait.jpg"));
/*     */     } else {
/*  80 */       this.options.add(new CharacterOption(AbstractPlayer.PlayerClass.DEFECT));
/*     */     }
/*     */     
/*  83 */     positionButtons();
/*     */     
/*  85 */     this.isAscensionMode = Settings.gamePref.getBoolean("Ascension Mode Default", false);
/*  86 */     ASC_LEFT_W = FontHelper.getSmartWidth(FontHelper.cardTitleFont_N, TEXT[6], 9999.0F, 0.0F);
/*  87 */     ASC_RIGHT_W = FontHelper.getSmartWidth(FontHelper.cardTitleFont_N, TEXT[7] + "22", 9999.0F, 0.0F);
/*  88 */     this.ascensionModeHb = new Hitbox(ASC_LEFT_W + 100.0F * Settings.scale, 50.0F * Settings.scale);
/*  89 */     this.ascensionModeHb.move(Settings.WIDTH / 2.0F - ASC_LEFT_W / 2.0F - 50.0F * Settings.scale, 70.0F * Settings.scale);
/*  90 */     this.ascLeftHb = new Hitbox(70.0F * Settings.scale, 70.0F * Settings.scale);
/*  91 */     this.ascRightHb = new Hitbox(70.0F * Settings.scale, 70.0F * Settings.scale);
/*  92 */     this.ascLeftHb.move(Settings.WIDTH / 2.0F + 200.0F * Settings.scale - ASC_RIGHT_W * 0.5F, 70.0F * Settings.scale);
/*  93 */     this.ascRightHb.move(Settings.WIDTH / 2.0F + 200.0F * Settings.scale + ASC_RIGHT_W * 1.5F, 70.0F * Settings.scale);
/*     */     
/*  95 */     this.seedHb = new Hitbox(700.0F * Settings.scale, 60.0F * Settings.scale);
/*  96 */     this.seedHb.move(60.0F * Settings.scale, 70.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   private void positionButtons() {
/* 100 */     int count = this.options.size();
/* 101 */     float offsetX = Settings.WIDTH / 2.0F - count / 2 * 220.0F * Settings.scale;
/* 102 */     for (int i = 0; i < count; i++) {
/* 103 */       ((CharacterOption)this.options.get(i)).move(offsetX + i * 220.0F * Settings.scale, 190.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */   public void open(boolean isEndless) {
/* 108 */     Settings.isEndless = isEndless;
/* 109 */     Settings.seedSet = false;
/* 110 */     Settings.seed = null;
/* 111 */     Settings.specialSeed = null;
/* 112 */     Settings.isTrial = false;
/* 113 */     CardCrawlGame.trial = null;
/*     */     
/* 115 */     this.cancelButton.show(TEXT[5]);
/* 116 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.CHAR_SELECT;
/*     */   }
/*     */   
/*     */   private void setRandomSeed() {
/* 120 */     long sourceTime = System.nanoTime();
/* 121 */     Random rng = new Random(Long.valueOf(sourceTime));
/* 122 */     Settings.seedSourceTimestamp = sourceTime;
/*     */     
/* 124 */     Settings.seed = Long.valueOf(SeedHelper.generateUnoffensiveSeed(rng));
/*     */   }
/*     */   
/*     */   public void update() {
/* 128 */     if (this.ascLeftHb != null) {
/* 129 */       this.ascLeftHb.move(Settings.WIDTH / 2.0F + 200.0F * Settings.scale - ASC_RIGHT_W * 0.25F, 70.0F * Settings.scale);
/* 130 */       this.ascRightHb.move(Settings.WIDTH / 2.0F + 200.0F * Settings.scale + ASC_RIGHT_W * 1.25F, 70.0F * Settings.scale);
/*     */     }
/*     */     
/* 133 */     this.anySelected = false;
/*     */     
/* 135 */     if (!this.seedPanel.shown) {
/* 136 */       for (CharacterOption o : this.options) {
/* 137 */         o.update();
/*     */         
/* 139 */         if (o.selected) {
/* 140 */           this.anySelected = true;
/* 141 */           this.isAscensionModeUnlocked = UnlockTracker.isAscensionUnlocked(o.c);
/* 142 */           if (!this.isAscensionModeUnlocked) {
/* 143 */             this.isAscensionMode = false;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 148 */       updateButtons();
/*     */       
/* 150 */       if ((InputHelper.justReleasedClickLeft) && (!this.anySelected)) {
/* 151 */         this.confirmButton.isDisabled = true;
/* 152 */         this.confirmButton.hide();
/*     */       }
/*     */       
/* 155 */       if (this.anySelected) {
/* 156 */         this.bgCharColor.a = MathHelper.fadeLerpSnap(this.bgCharColor.a, 1.0F);
/* 157 */         this.bg_y_offset = MathHelper.fadeLerpSnap(this.bg_y_offset, -BG_Y_OFFSET_TARGET / 2.0F);
/*     */       } else {
/* 159 */         this.bgCharColor.a = MathHelper.fadeLerpSnap(this.bgCharColor.a, 0.0F);
/*     */       }
/*     */       
/* 162 */       updateAscensionToggle();
/* 163 */       this.seedHb.update();
/*     */     }
/* 165 */     this.seedPanel.update();
/*     */     
/* 167 */     if (((this.seedHb.hovered) && (InputHelper.justClickedLeft)) || (CInputActionSet.drawPile.isJustPressed())) {
/* 168 */       InputHelper.justClickedLeft = false;
/* 169 */       this.seedHb.hovered = false;
/* 170 */       this.seedPanel.show();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateAscensionToggle() {
/* 175 */     if (this.isAscensionModeUnlocked) {
/* 176 */       if (this.anySelected) {
/* 177 */         this.ascensionModeHb.update();
/* 178 */         this.ascRightHb.update();
/* 179 */         this.ascLeftHb.update();
/*     */       }
/*     */       
/* 182 */       if (InputHelper.justClickedLeft) {
/* 183 */         if (this.ascensionModeHb.hovered) {
/* 184 */           this.ascensionModeHb.clickStarted = true;
/* 185 */         } else if (this.ascRightHb.hovered) {
/* 186 */           this.ascRightHb.clickStarted = true;
/* 187 */         } else if (this.ascLeftHb.hovered) {
/* 188 */           this.ascLeftHb.clickStarted = true;
/*     */         }
/*     */       }
/*     */       
/* 192 */       if ((this.ascensionModeHb.clicked) || (CInputActionSet.proceed.isJustPressed())) {
/* 193 */         this.ascensionModeHb.clicked = false;
/* 194 */         this.isAscensionMode = (!this.isAscensionMode);
/* 195 */         Settings.gamePref.putBoolean("Ascension Mode Default", this.isAscensionMode);
/* 196 */         Settings.gamePref.flush();
/*     */       }
/*     */       
/* 199 */       if ((this.ascLeftHb.clicked) || (CInputActionSet.pageLeftViewDeck.isJustPressed())) {
/* 200 */         this.ascLeftHb.clicked = false;
/* 201 */         for (CharacterOption o : this.options) {
/* 202 */           if (o.selected) {
/* 203 */             o.decrementAscensionLevel(this.ascensionLevel - 1);
/* 204 */             break;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 209 */       if ((this.ascRightHb.clicked) || (CInputActionSet.pageRightViewExhaust.isJustPressed())) {
/* 210 */         this.ascRightHb.clicked = false;
/* 211 */         for (CharacterOption o : this.options) {
/* 212 */           if (o.selected) {
/* 213 */             o.incrementAscensionLevel(this.ascensionLevel + 1);
/* 214 */             break;
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void justSelected() {
/* 222 */     this.bg_y_offset = (BG_Y_OFFSET_TARGET / 2.0F);
/*     */   }
/*     */   
/*     */   public void updateButtons() {
/* 226 */     this.cancelButton.update();
/* 227 */     this.confirmButton.update();
/*     */     
/* 229 */     if ((this.cancelButton.hb.clicked) || (InputHelper.pressedEscape)) {
/* 230 */       InputHelper.pressedEscape = false;
/* 231 */       this.cancelButton.hb.clicked = false;
/* 232 */       this.cancelButton.hide();
/* 233 */       CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/* 234 */       for (CharacterOption o : this.options) {
/* 235 */         o.selected = false;
/*     */       }
/* 237 */       this.bgCharColor.a = 0.0F;
/* 238 */       this.anySelected = false;
/*     */     }
/*     */     
/*     */ 
/* 242 */     if (this.confirmButton.hb.clicked) {
/* 243 */       this.confirmButton.hb.clicked = false;
/* 244 */       this.confirmButton.isDisabled = true;
/* 245 */       this.confirmButton.hide();
/*     */       
/* 247 */       if (Settings.seed == null) {
/* 248 */         setRandomSeed();
/*     */       }
/*     */       
/* 251 */       CardCrawlGame.mainMenuScreen.isFadingOut = true;
/* 252 */       CardCrawlGame.mainMenuScreen.fadeOutMusic();
/*     */       
/* 254 */       Settings.isDailyRun = false;
/* 255 */       Settings.isTrial = TrialHelper.isTrialSeed(SeedHelper.getString(Settings.seed.longValue()));
/* 256 */       if (Settings.isTrial) {
/* 257 */         Settings.specialSeed = Settings.seed;
/* 258 */         long sourceTime = System.nanoTime();
/* 259 */         Random rng = new Random(Long.valueOf(sourceTime));
/* 260 */         Settings.seed = Long.valueOf(SeedHelper.generateUnoffensiveSeed(rng));
/*     */       }
/* 262 */       com.megacrit.cardcrawl.daily.DailyMods.setModsFalse();
/* 263 */       AbstractDungeon.generateSeeds();
/*     */       
/* 265 */       AbstractDungeon.isAscensionMode = this.isAscensionMode;
/* 266 */       if (this.isAscensionMode) {
/* 267 */         AbstractDungeon.ascensionLevel = this.ascensionLevel;
/*     */       } else {
/* 269 */         AbstractDungeon.ascensionLevel = 0;
/*     */       }
/*     */       
/* 272 */       this.confirmButton.hb.clicked = false;
/* 273 */       this.confirmButton.hide();
/*     */       
/* 275 */       if ((Settings.isDemo) || (Settings.isPublisherBuild)) {
/* 276 */         BotDataUploader poster = new BotDataUploader();
/* 277 */         poster.setValues(BotDataUploader.GameDataType.DEMO_EMBARK, null, null);
/* 278 */         Thread t = new Thread(poster);
/* 279 */         t.setName("LeaderboardPoster");
/* 280 */         t.run();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 286 */     sb.setColor(this.bgCharColor);
/* 287 */     if (this.bgCharImg != null)
/*     */     {
/* 289 */       if (Settings.isSixteenByTen) {
/* 290 */         sb.draw(this.bgCharImg, Settings.WIDTH / 2.0F - 960.0F, Settings.HEIGHT / 2.0F - 600.0F, 960.0F, 600.0F, 1920.0F, 1200.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1200, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 308 */         sb.draw(this.bgCharImg, Settings.WIDTH / 2.0F - 960.0F, Settings.HEIGHT / 2.0F - 600.0F + this.bg_y_offset, 960.0F, 600.0F, 1920.0F, 1200.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1200, false, false);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 328 */     this.cancelButton.render(sb);
/* 329 */     this.confirmButton.render(sb);
/* 330 */     renderAscensionMode(sb);
/* 331 */     renderSeedSettings(sb);
/* 332 */     this.seedPanel.render(sb);
/*     */     
/* 334 */     boolean anythingSelected = false;
/*     */     
/* 336 */     if (!this.seedPanel.shown) {
/* 337 */       for (CharacterOption o : this.options) {
/* 338 */         if (o.selected) {
/* 339 */           anythingSelected = true;
/*     */         }
/* 341 */         o.render(sb);
/*     */       }
/*     */     }
/*     */     
/* 345 */     if ((!this.seedPanel.shown) && 
/* 346 */       (!anythingSelected)) {
/* 347 */       FontHelper.renderFontCentered(sb, FontHelper.bannerFont, CHOOSE_CHAR_MSG, Settings.WIDTH / 2.0F, 340.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderSeedSettings(SpriteBatch sb)
/*     */   {
/* 359 */     if (!this.anySelected) {
/* 360 */       return;
/*     */     }
/*     */     
/* 363 */     Color textColor = Settings.GOLD_COLOR;
/*     */     
/* 365 */     if (this.seedHb.hovered) {
/* 366 */       textColor = Settings.GREEN_TEXT_COLOR;
/* 367 */       TipHelper.renderGenericTip(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY + 100.0F * Settings.scale, TEXT[11], TEXT[12]);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 373 */     if (!Settings.isControllerMode) {
/* 374 */       if (Settings.seedSet) {
/* 375 */         FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, TEXT[10], SEED_X, SEED_Y, 9999.0F, 0.0F, textColor);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 384 */         FontHelper.renderFontLeftTopAligned(sb, FontHelper.cardTitleFont_N, 
/*     */         
/*     */ 
/* 387 */           SeedHelper.getUserFacingSeedString(), 
/* 388 */           FontHelper.getSmartWidth(FontHelper.cardTitleFont_N, TEXT[13], 9999.0F, 0.0F), 90.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */       }
/*     */       else
/*     */       {
/* 392 */         FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, TEXT[13], SEED_X, SEED_Y, 9999.0F, 0.0F, textColor);
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 403 */       if (Settings.seedSet) {
/* 404 */         FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, TEXT[10], SEED_X + 100.0F * Settings.scale, SEED_Y, 9999.0F, 0.0F, textColor);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 413 */         FontHelper.renderFontLeftTopAligned(sb, FontHelper.cardTitleFont_N, 
/*     */         
/*     */ 
/* 416 */           SeedHelper.getUserFacingSeedString(), 
/* 417 */           FontHelper.getSmartWidth(FontHelper.cardTitleFont_N, TEXT[13], 9999.0F, 0.0F) + 100.0F * Settings.scale, 90.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */       }
/*     */       else
/*     */       {
/* 421 */         FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, TEXT[13], SEED_X + 100.0F * Settings.scale, SEED_Y, 9999.0F, 0.0F, textColor);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 432 */       sb.draw(ImageMaster.CONTROLLER_LT, 80.0F * Settings.scale - 32.0F, 80.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderAscensionMode(SpriteBatch sb)
/*     */   {
/* 453 */     if (!this.anySelected) {
/* 454 */       return;
/*     */     }
/*     */     
/* 457 */     if (this.isAscensionModeUnlocked) {
/* 458 */       sb.setColor(Color.WHITE);
/* 459 */       sb.draw(ImageMaster.OPTION_TOGGLE, Settings.WIDTH / 2.0F - ASC_LEFT_W - 16.0F - 30.0F * Settings.scale, 70.0F * Settings.scale - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 32, 32, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 477 */       if (this.ascensionModeHb.hovered) {
/* 478 */         FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[6], Settings.WIDTH / 2.0F - ASC_LEFT_W / 2.0F, 70.0F * Settings.scale, Settings.GREEN_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 486 */         TipHelper.renderGenericTip(InputHelper.mX - 140.0F * Settings.scale, InputHelper.mY + 340.0F * Settings.scale, TEXT[8], TEXT[9]);
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/* 492 */         FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[6], Settings.WIDTH / 2.0F - ASC_LEFT_W / 2.0F, 70.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 501 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[7] + this.ascensionLevel, Settings.WIDTH / 2.0F + ASC_RIGHT_W / 2.0F + 200.0F * Settings.scale, 70.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 509 */       if (this.isAscensionMode) {
/* 510 */         sb.setColor(Color.WHITE);
/* 511 */         sb.draw(ImageMaster.OPTION_TOGGLE_ON, Settings.WIDTH / 2.0F - ASC_LEFT_W - 16.0F - 30.0F * Settings.scale, 70.0F * Settings.scale - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 32, 32, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 529 */         FontHelper.renderFontCentered(sb, FontHelper.cardDescFont_N, this.ascLevelInfoString, Settings.WIDTH / 2.0F, 35.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 539 */       if ((this.ascLeftHb.hovered) || (Settings.isControllerMode)) {
/* 540 */         sb.setColor(Color.WHITE);
/*     */       } else {
/* 542 */         sb.setColor(Color.LIGHT_GRAY);
/*     */       }
/* 544 */       sb.draw(ImageMaster.CF_LEFT_ARROW, this.ascLeftHb.cX - 24.0F, this.ascLeftHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 562 */       if ((this.ascRightHb.hovered) || (Settings.isControllerMode)) {
/* 563 */         sb.setColor(Color.WHITE);
/*     */       } else {
/* 565 */         sb.setColor(Color.LIGHT_GRAY);
/*     */       }
/* 567 */       sb.draw(ImageMaster.CF_RIGHT_ARROW, this.ascRightHb.cX - 24.0F, this.ascRightHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 585 */       if (Settings.isControllerMode) {
/* 586 */         sb.draw(CInputActionSet.proceed
/* 587 */           .getKeyImg(), this.ascensionModeHb.cX - 100.0F * Settings.scale - 32.0F, this.ascensionModeHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 603 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 604 */           .getKeyImg(), this.ascLeftHb.cX - 60.0F * Settings.scale - 32.0F, this.ascLeftHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 620 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 621 */           .getKeyImg(), this.ascRightHb.cX + 60.0F * Settings.scale - 32.0F, this.ascRightHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 639 */       this.ascensionModeHb.render(sb);
/* 640 */       this.ascLeftHb.render(sb);
/* 641 */       this.ascRightHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void deselectOtherOptions(CharacterOption characterOption) {
/* 646 */     for (CharacterOption o : this.options) {
/* 647 */       if (o != characterOption) {
/* 648 */         o.selected = false;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\charSelect\CharacterSelectScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */