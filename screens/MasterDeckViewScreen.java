/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class MasterDeckViewScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  28 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("MasterDeckViewScreen");
/*  29 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   private static float drawStartX;
/*  31 */   private static float drawStartY = Settings.HEIGHT * 0.66F;
/*  32 */   private static float padX; private static float padY; private static final float SCROLL_BAR_THRESHOLD = 500.0F * Settings.scale;
/*     */   private static final int CARDS_PER_LINE = 5;
/*  34 */   private boolean grabbedScreen = false;
/*  35 */   private float grabStartY = 0.0F; private float currentDiffY = 0.0F;
/*  36 */   public CardGroup rewardGroup = null;
/*  37 */   public boolean openedDuringReward = false;
/*  38 */   private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  39 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  40 */   private static final String HEADER_INFO = TEXT[0];
/*  41 */   private AbstractCard hoveredCard = null; private AbstractCard clickStartedCard = null;
/*  42 */   private int prevDeckSize = 0;
/*     */   
/*     */   private ScrollBar scrollBar;
/*  45 */   private AbstractCard controllerCard = null;
/*     */   
/*     */   public MasterDeckViewScreen() {
/*  48 */     drawStartX = Settings.WIDTH;
/*  49 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  50 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  51 */     drawStartX /= 2.0F;
/*  52 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*     */     
/*  54 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  55 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*     */     
/*  57 */     this.scrollBar = new ScrollBar(this);
/*  58 */     this.scrollBar.move(0.0F, -30.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  62 */     updateControllerInput();
/*  63 */     if ((Settings.isControllerMode) && (this.controllerCard != null) && (!CardCrawlGame.isPopupOpen) && (!AbstractDungeon.topPanel.selectPotionMode))
/*     */     {
/*  65 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.7F) {
/*  66 */         this.currentDiffY += Settings.SCROLL_SPEED;
/*  67 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.3F) {
/*  68 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/*  72 */     boolean isDraggingScrollBar = false;
/*  73 */     if (shouldShowScrollBar()) {
/*  74 */       isDraggingScrollBar = this.scrollBar.update();
/*     */     }
/*  76 */     if (!isDraggingScrollBar) {
/*  77 */       updateScrolling();
/*     */     }
/*  79 */     updatePositions();
/*  80 */     updateClicking();
/*     */     
/*  82 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/*  83 */       Gdx.input.setCursorPosition((int)this.controllerCard.hb.cX, (int)(Settings.HEIGHT - this.controllerCard.hb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/*  88 */     if ((!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode)) {
/*  89 */       return;
/*     */     }
/*     */     
/*  92 */     CardGroup deck = AbstractDungeon.player.masterDeck;
/*  93 */     boolean anyHovered = false;
/*  94 */     int index = 0;
/*     */     
/*  96 */     for (AbstractCard c : deck.group) {
/*  97 */       if (c.hb.hovered) {
/*  98 */         anyHovered = true;
/*  99 */         break;
/*     */       }
/* 101 */       index++;
/*     */     }
/*     */     
/* 104 */     if (!anyHovered) {
/* 105 */       Gdx.input.setCursorPosition((int)((AbstractCard)deck.group.get(0)).hb.cX, (int)((AbstractCard)deck.group.get(0)).hb.cY);
/* 106 */       this.controllerCard = ((AbstractCard)deck.group.get(0));
/*     */     }
/* 108 */     else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 109 */       (deck.size() > 5))
/*     */     {
/* 111 */       if (index < 5) {
/* 112 */         index = deck.size() + 2 - (4 - index);
/* 113 */         if (index > deck.size() - 1) {
/* 114 */           index -= 5;
/*     */         }
/* 116 */         if ((index > deck.size() - 1) || (index < 0)) {
/* 117 */           System.out.println("oops");
/* 118 */           index = 0;
/*     */         }
/*     */       }
/*     */       else {
/* 122 */         index -= 5;
/*     */       }
/* 124 */       Gdx.input.setCursorPosition(
/* 125 */         (int)((AbstractCard)deck.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 126 */         (int)((AbstractCard)deck.group.get(index)).hb.cY);
/* 127 */       this.controllerCard = ((AbstractCard)deck.group.get(index));
/* 128 */     } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 129 */       (deck.size() > 5)) {
/* 130 */       if (index < deck.size() - 5) {
/* 131 */         index += 5;
/*     */       } else {
/* 133 */         index %= 5;
/*     */       }
/* 135 */       Gdx.input.setCursorPosition(
/* 136 */         (int)((AbstractCard)deck.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 137 */         (int)((AbstractCard)deck.group.get(index)).hb.cY);
/* 138 */       this.controllerCard = ((AbstractCard)deck.group.get(index));
/* 139 */     } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 140 */       if (index % 5 > 0) {
/* 141 */         index--;
/*     */       } else {
/* 143 */         index += 4;
/* 144 */         if (index > deck.size() - 1) {
/* 145 */           index = deck.size() - 1;
/*     */         }
/*     */       }
/* 148 */       Gdx.input.setCursorPosition(
/* 149 */         (int)((AbstractCard)deck.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 150 */         (int)((AbstractCard)deck.group.get(index)).hb.cY);
/* 151 */       this.controllerCard = ((AbstractCard)deck.group.get(index));
/* 152 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 153 */       if (index % 5 < 4) {
/* 154 */         index++;
/* 155 */         if (index > deck.size() - 1) {
/* 156 */           index -= deck.size() % 5;
/*     */         }
/*     */       } else {
/* 159 */         index -= 4;
/* 160 */         if (index < 0) {
/* 161 */           index = 0;
/*     */         }
/*     */       }
/* 164 */       Gdx.input.setCursorPosition(
/* 165 */         (int)((AbstractCard)deck.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 166 */         (int)((AbstractCard)deck.group.get(index)).hb.cY);
/* 167 */       this.controllerCard = ((AbstractCard)deck.group.get(index));
/*     */     }
/*     */   }
/*     */   
/*     */   public void open()
/*     */   {
/* 173 */     if (Settings.isControllerMode) {
/* 174 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 175 */       this.controllerCard = null;
/*     */     }
/* 177 */     AbstractDungeon.player.releaseCard();
/* 178 */     CardCrawlGame.sound.play("DECK_OPEN");
/* 179 */     this.currentDiffY = this.scrollLowerBound;
/* 180 */     this.grabStartY = this.scrollLowerBound;
/* 181 */     this.grabbedScreen = false;
/* 182 */     hideCards();
/* 183 */     AbstractDungeon.isScreenUp = true;
/* 184 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.MASTER_DECK_VIEW;
/* 185 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 186 */     AbstractDungeon.overlayMenu.hideCombatPanels();
/* 187 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 188 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/* 189 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updatePositions()
/*     */   {
/* 196 */     this.hoveredCard = null;
/* 197 */     int lineNum = 0;
/* 198 */     ArrayList<AbstractCard> cards = AbstractDungeon.player.masterDeck.group;
/* 199 */     for (int i = 0; i < cards.size(); i++) {
/* 200 */       int mod = i % 5;
/* 201 */       if ((mod == 0) && (i != 0)) {
/* 202 */         lineNum++;
/*     */       }
/* 204 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 205 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 206 */       ((AbstractCard)cards.get(i)).update();
/* 207 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/* 208 */       if (((AbstractCard)cards.get(i)).hb.hovered) {
/* 209 */         this.hoveredCard = ((AbstractCard)cards.get(i));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling() {
/* 215 */     int y = InputHelper.mY;
/*     */     
/* 217 */     if (!this.grabbedScreen) {
/* 218 */       if (InputHelper.scrolledDown) {
/* 219 */         this.currentDiffY += Settings.SCROLL_SPEED;
/* 220 */       } else if (InputHelper.scrolledUp) {
/* 221 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 224 */       if (InputHelper.justClickedLeft) {
/* 225 */         this.grabbedScreen = true;
/* 226 */         this.grabStartY = (y - this.currentDiffY);
/*     */       }
/*     */     }
/* 229 */     else if (InputHelper.isMouseDown) {
/* 230 */       this.currentDiffY = (y - this.grabStartY);
/*     */     } else {
/* 232 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 236 */     if (this.prevDeckSize != AbstractDungeon.player.masterDeck.size()) {
/* 237 */       calculateScrollBounds();
/*     */     }
/* 239 */     resetScrolling();
/* 240 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateClicking() {
/* 244 */     if (this.hoveredCard != null) {
/* 245 */       CardCrawlGame.cursor.changeType(com.megacrit.cardcrawl.core.GameCursor.CursorType.INSPECT);
/* 246 */       if (InputHelper.justClickedLeft) {
/* 247 */         this.clickStartedCard = this.hoveredCard;
/*     */       }
/* 249 */       if (((InputHelper.justReleasedClickLeft) && (this.hoveredCard == this.clickStartedCard)) || 
/* 250 */         (CInputActionSet.select.isJustPressed())) {
/* 251 */         InputHelper.justReleasedClickLeft = false;
/* 252 */         CardCrawlGame.cardPopup.open(this.hoveredCard, AbstractDungeon.player.masterDeck);
/* 253 */         this.clickStartedCard = null;
/*     */       }
/*     */     } else {
/* 256 */       this.clickStartedCard = null;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/* 264 */     int scrollTmp = 0;
/* 265 */     if (AbstractDungeon.player.masterDeck.size() > 10) {
/* 266 */       scrollTmp = AbstractDungeon.player.masterDeck.size() / 5 - 2;
/* 267 */       if (AbstractDungeon.player.masterDeck.size() % 5 != 0) {
/* 268 */         scrollTmp++;
/*     */       }
/* 270 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/* 272 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/* 274 */     this.prevDeckSize = AbstractDungeon.player.masterDeck.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 281 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 282 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 283 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 284 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   private void hideCards() {
/* 289 */     int lineNum = 0;
/* 290 */     ArrayList<AbstractCard> cards = AbstractDungeon.player.masterDeck.group;
/* 291 */     for (int i = 0; i < cards.size(); i++) {
/* 292 */       int mod = i % 5;
/* 293 */       if ((mod == 0) && (i != 0)) {
/* 294 */         lineNum++;
/*     */       }
/* 296 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 297 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY - com.badlogic.gdx.math.MathUtils.random(100.0F * Settings.scale, 200.0F * Settings.scale));
/*     */       
/*     */ 
/* 300 */       ((AbstractCard)cards.get(i)).targetDrawScale = 0.75F;
/* 301 */       ((AbstractCard)cards.get(i)).drawScale = 0.75F;
/* 302 */       ((AbstractCard)cards.get(i)).setAngle(0.0F, true);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 307 */     if (this.hoveredCard == null) {
/* 308 */       AbstractDungeon.player.masterDeck.renderMasterDeck(sb);
/*     */     } else {
/* 310 */       AbstractDungeon.player.masterDeck.renderMasterDeckExceptOneCard(sb, this.hoveredCard);
/* 311 */       this.hoveredCard.renderHoverShadow(sb);
/* 312 */       this.hoveredCard.render(sb);
/* 313 */       if (this.hoveredCard.inBottleFlame) {
/* 314 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledFlame();
/* 315 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 316 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 317 */         r.render(sb);
/* 318 */       } else if (this.hoveredCard.inBottleLightning) {
/* 319 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledLightning();
/* 320 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 321 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 322 */         r.render(sb);
/* 323 */       } else if (this.hoveredCard.inBottleTornado) {
/* 324 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledTornado();
/* 325 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 326 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 327 */         r.render(sb);
/*     */       }
/*     */     }
/* 330 */     AbstractDungeon.player.masterDeck.renderTip(sb);
/* 331 */     FontHelper.renderDeckViewTip(sb, HEADER_INFO, 96.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/* 333 */     if (shouldShowScrollBar()) {
/* 334 */       this.scrollBar.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 340 */     this.currentDiffY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 341 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 345 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.currentDiffY);
/* 346 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */   
/*     */   private boolean shouldShowScrollBar() {
/* 350 */     return this.scrollUpperBound > SCROLL_BAR_THRESHOLD;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\MasterDeckViewScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */