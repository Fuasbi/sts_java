/*      */ package com.megacrit.cardcrawl.screens;
/*      */ 
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.Interpolation;
/*      */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.dungeons.TheBeyond;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.Prefs;
/*      */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.ScoreBonusStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.metrics.Metrics;
/*      */ import com.megacrit.cardcrawl.metrics.Metrics.MetricRequestType;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*      */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*      */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*      */ import com.megacrit.cardcrawl.ui.buttons.DynamicBanner;
/*      */ import com.megacrit.cardcrawl.ui.buttons.ReturnToMenuButton;
/*      */ import com.megacrit.cardcrawl.unlock.AbstractUnlock;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockCharacterScreen;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.unlock.misc.DefectUnlock;
/*      */ import com.megacrit.cardcrawl.unlock.misc.TheSilentUnlock;
/*      */ import com.megacrit.cardcrawl.vfx.DeathScreenFloatyEffect;
/*      */ import com.megacrit.cardcrawl.vfx.UnlockTextEffect;
/*      */ import java.io.PrintStream;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Iterator;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class DeathScreen
/*      */ {
/*   53 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DeathScreen.class.getName());
/*   54 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DeathScreen");
/*   55 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   
/*      */ 
/*   58 */   private static final ScoreBonusStrings EXORDIUM_ELITE = CardCrawlGame.languagePack.getScoreString("Exordium Elites Killed");
/*      */   
/*   60 */   private static final ScoreBonusStrings CITY_ELITE = CardCrawlGame.languagePack.getScoreString("City Elites Killed");
/*   61 */   private static final ScoreBonusStrings BEYOND_ELITE = CardCrawlGame.languagePack.getScoreString("Beyond Elites Killed");
/*      */   
/*   63 */   private static final ScoreBonusStrings BOSSES_SLAIN = CardCrawlGame.languagePack.getScoreString("Bosses Slain");
/*   64 */   private static final ScoreBonusStrings ASCENSION = CardCrawlGame.languagePack.getScoreString("Ascension");
/*   65 */   private static final ScoreBonusStrings CHAMPION = CardCrawlGame.languagePack.getScoreString("Champion");
/*   66 */   private static final ScoreBonusStrings PERFECT = CardCrawlGame.languagePack.getScoreString("Perfect");
/*   67 */   private static final ScoreBonusStrings BEYOND_PERFECT = CardCrawlGame.languagePack.getScoreString("Beyond Perfect");
/*   68 */   private static final ScoreBonusStrings OVERKILL = CardCrawlGame.languagePack.getScoreString("Overkill");
/*   69 */   private static final ScoreBonusStrings COMBO = CardCrawlGame.languagePack.getScoreString("Combo");
/*   70 */   private static final ScoreBonusStrings POOPY = CardCrawlGame.languagePack.getScoreString("Poopy");
/*   71 */   private static final ScoreBonusStrings SPEEDSTER = CardCrawlGame.languagePack.getScoreString("Speedster");
/*   72 */   private static final ScoreBonusStrings LIGHT_SPEED = CardCrawlGame.languagePack.getScoreString("Light Speed");
/*   73 */   private static final ScoreBonusStrings MONEY_MONEY = CardCrawlGame.languagePack.getScoreString("Money Money");
/*   74 */   private static final ScoreBonusStrings RAINING_MONEY = CardCrawlGame.languagePack.getScoreString("Raining Money");
/*   75 */   private static final ScoreBonusStrings I_LIKE_GOLD = CardCrawlGame.languagePack.getScoreString("I Like Gold");
/*   76 */   private static final ScoreBonusStrings HIGHLANDER = CardCrawlGame.languagePack.getScoreString("Highlander");
/*   77 */   private static final ScoreBonusStrings SHINY = CardCrawlGame.languagePack.getScoreString("Shiny");
/*   78 */   private static final ScoreBonusStrings COLLECTOR = CardCrawlGame.languagePack.getScoreString("Collector");
/*   79 */   private static final ScoreBonusStrings PAUPER = CardCrawlGame.languagePack.getScoreString("Pauper");
/*   80 */   private static final ScoreBonusStrings LIBRARIAN = CardCrawlGame.languagePack.getScoreString("Librarian");
/*   81 */   private static final ScoreBonusStrings ENCYCLOPEDIAN = CardCrawlGame.languagePack.getScoreString("Encyclopedian");
/*   82 */   private static final ScoreBonusStrings WELL_FED = CardCrawlGame.languagePack.getScoreString("Well Fed");
/*   83 */   private static final ScoreBonusStrings STUFFED = CardCrawlGame.languagePack.getScoreString("Stuffed");
/*   84 */   private static final ScoreBonusStrings CURSES = CardCrawlGame.languagePack.getScoreString("Curses");
/*      */   
/*      */ 
/*   87 */   private static final ScoreBonusStrings MINIMALIST = CardCrawlGame.languagePack.getScoreString("Minimalist");
/*      */   
/*   89 */   private static final ScoreBonusStrings ALCHEMIST = CardCrawlGame.languagePack.getScoreString("Alchemist");
/*      */   
/*   91 */   private static final ScoreBonusStrings MYSTERY_MACHINE = CardCrawlGame.languagePack.getScoreString("Mystery Machine");
/*      */   
/*   93 */   private static final ScoreBonusStrings ON_MY_OWN_TERMS = CardCrawlGame.languagePack.getScoreString("On My Own Terms");
/*      */   
/*      */   private MonsterGroup monsters;
/*      */   
/*      */   private String deathText;
/*   98 */   private ArrayList<DeathScreenFloatyEffect> particles = new ArrayList();
/*      */   private static final float NUM_PARTICLES = 50.0F;
/*  100 */   private float deathAnimWaitTimer = 1.0F;
/*      */   private static final float DEATH_TEXT_TIME = 5.0F;
/*  102 */   private float deathTextTimer = 5.0F;
/*  103 */   private Color defeatTextColor = Color.WHITE.cpy();
/*  104 */   private Color deathTextColor = Settings.BLUE_TEXT_COLOR.cpy();
/*  105 */   private static final float DEATH_TEXT_Y = Settings.HEIGHT - 360.0F * Settings.scale;
/*      */   public ReturnToMenuButton returnButton;
/*  107 */   public ArrayList<GameOverStat> stats = new ArrayList();
/*  108 */   public ArrayList<AbstractUnlock> unlockBundle = null;
/*      */   
/*      */   private boolean showingStats;
/*      */   
/*      */   private static final float STATS_TRANSITION_TIME = 0.5F;
/*      */   private static final float STAT_ANIM_INTERVAL = 0.1F;
/*      */   private static final float PROGRESS_BAR_ANIM_TIME = 2.0F;
/*  115 */   private float statsTimer = 0.0F; private float statAnimateTimer = 0.0F; private float progressBarTimer = 2.0F; private float progressBarAlpha = 0.0F;
/*      */   private long playtime;
/*  117 */   private static final float STAT_OFFSET_Y = 36.0F * Settings.scale;
/*  118 */   private static final float STAT_START_Y = Settings.HEIGHT / 2.0F - 20.0F * Settings.scale;
/*  119 */   private int score = 0; private int unlockCost; private int nextUnlockCost; private int unlockLevel = 0;
/*  120 */   private boolean maxLevel = false;
/*      */   private float progressPercent;
/*      */   private float unlockTargetProgress;
/*      */   private float unlockTargetStart;
/*      */   private float unlockProgress;
/*      */   private static int floorPoints;
/*      */   private static int monsterPoints;
/*      */   private static int elite1Points;
/*      */   private static int elite2Points;
/*      */   private static int elite3Points;
/*      */   private static int bossPoints;
/*      */   private static int ascensionPoints;
/*      */   private static final int FLOOR_MULTIPLIER = 5;
/*      */   private static final int ENEMY_MULTIPLIER = 2;
/*      */   private static final int ELITE_MULTIPLIER_1 = 10;
/*  135 */   private static final int ELITE_MULTIPLIER_2 = 20; private static final int ELITE_MULTIPLIER_3 = 30; private static final int BOSS_MULTIPLIER = 50; private static final float ASCENSION_MULTIPLIER = 0.05F; private static boolean IS_POOPY = false;
/*  136 */   private static boolean IS_SPEEDSTER = false;
/*  137 */   private static boolean IS_LIGHT_SPEED = false;
/*  138 */   private static boolean IS_HIGHLANDER = false;
/*  139 */   private static int IS_FULL_SET = 0;
/*  140 */   private static boolean IS_SHINY = false;
/*  141 */   private static boolean IS_PAUPER = false;
/*  142 */   private static boolean IS_LIBRARY = false;
/*  143 */   private static boolean IS_ENCYCLOPEDIA = false;
/*  144 */   private static boolean IS_WELL_FED = false;
/*  145 */   private static boolean IS_STUFFED = false;
/*  146 */   private static boolean IS_CURSES = false;
/*  147 */   private static boolean IS_ON_MY_OWN = false;
/*  148 */   private static boolean IS_MONEY_MONEY = false;
/*  149 */   private static boolean IS_RAINING_MONEY = false;
/*  150 */   private static boolean IS_I_LIKE_GOLD = false;
/*  151 */   private static boolean IS_MYSTERY_MACHINE = false;
/*      */   
/*      */   private static final int POOPY_SCORE = -1;
/*      */   
/*      */   private static final int SPEEDER_SCORE = 25;
/*      */   
/*      */   private static final int LIGHT_SPEED_SCORE = 50;
/*      */   private static final int HIGHLANDER_SCORE = 100;
/*      */   private static final int FULL_SET_SCORE = 25;
/*      */   private static final int SHINY_SCORE = 50;
/*      */   private static final int PAUPER_SCORE = 50;
/*      */   private static final int LIBRARY_SCORE = 25;
/*      */   private static final int ENCYCLOPEDIA_SCORE = 50;
/*      */   private static final int WELL_FED_SCORE = 25;
/*      */   private static final int STUFFED_SCORE = 50;
/*      */   private static final int CURSES_SCORE = 100;
/*      */   private static final int ON_MY_OWN_SCORE = 50;
/*      */   private static final int MONEY_MONEY_SCORE = 25;
/*      */   private static final int RAINING_MONEY_SCORE = 50;
/*      */   private static final int I_LIKE_GOLD_SCORE = 75;
/*      */   private static final int CHAMPION_SCORE = 25;
/*      */   private static final int PERFECT_SCORE = 50;
/*      */   private static final int BEYOND_PERFECT_SCORE = 200;
/*      */   private static final int OVERKILL_SCORE = 25;
/*      */   private static final int COMBO_SCORE = 25;
/*      */   private static final int MYSTERY_MACHINE_SCORE = 25;
/*  177 */   public boolean isVictory = false;
/*  178 */   private boolean playedWhir = false;
/*      */   private long whirId;
/*      */   
/*      */   public DeathScreen(MonsterGroup m) {
/*  182 */     this.playtime = (CardCrawlGame.playtime);
/*      */     
/*  184 */     if (this.playtime < 0L) {
/*  185 */       this.playtime = 0L;
/*      */     }
/*      */     
/*  188 */     AbstractDungeon.getCurrRoom().clearEvent();
/*  189 */     resetScoreChecks();
/*      */     
/*  191 */     AbstractDungeon.dungeonMapScreen.closeInstantly();
/*  192 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.DEATH;
/*  193 */     AbstractDungeon.overlayMenu.showBlackScreen(1.0F);
/*  194 */     AbstractDungeon.previousScreen = null;
/*  195 */     AbstractDungeon.overlayMenu.cancelButton.hideInstantly();
/*  196 */     AbstractDungeon.isScreenUp = true;
/*  197 */     this.deathText = getDeathText();
/*  198 */     this.monsters = m;
/*  199 */     logger.info("PLAYTIME: " + this.playtime);
/*      */     
/*  201 */     if ((AbstractDungeon.getCurrRoom() instanceof RestRoom)) {
/*  202 */       ((RestRoom)AbstractDungeon.getCurrRoom()).cutFireSound();
/*      */     }
/*      */     
/*  205 */     this.isVictory = (AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.VictoryRoom);
/*      */     
/*  207 */     if (!this.isVictory) {
/*  208 */       switch (AbstractDungeon.player.chosenClass) {
/*      */       case IRONCLAD: 
/*  210 */         if (Settings.isBeta) {
/*  211 */           SteamSaveSync.setStat("win_streak_ironclad_BETA", 0);
/*  212 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad_BETA"));
/*      */         } else {
/*  214 */           SteamSaveSync.setStat("win_streak_ironclad", 0);
/*  215 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad"));
/*      */         }
/*  217 */         break;
/*      */       case THE_SILENT: 
/*  219 */         if (Settings.isBeta) {
/*  220 */           SteamSaveSync.setStat("win_streak_silent_BETA", 0);
/*  221 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent_BETA"));
/*      */         } else {
/*  223 */           SteamSaveSync.setStat("win_streak_silent", 0);
/*  224 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent"));
/*      */         }
/*  226 */         break;
/*      */       case DEFECT: 
/*  228 */         if (Settings.isBeta) {
/*  229 */           SteamSaveSync.setStat("win_streak_defect_BETA", 0);
/*  230 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent_BETA"));
/*      */         } else {
/*  232 */           SteamSaveSync.setStat("win_streak_defect", 0);
/*  233 */           System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent"));
/*      */         }
/*  235 */         break;
/*      */       }
/*      */       
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  243 */     this.showingStats = false;
/*  244 */     this.returnButton = new ReturnToMenuButton();
/*  245 */     this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[0]);
/*      */     
/*  247 */     if ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.VictoryRoom)) {
/*  248 */       AbstractDungeon.dynamicBanner.appear(TEXT[1]);
/*      */     } else {
/*  250 */       AbstractDungeon.dynamicBanner.appear(getDeathBannerText());
/*      */     }
/*      */     
/*  253 */     if (Settings.isStandardRun()) {
/*  254 */       if ((AbstractDungeon.floorNum >= 16) || (Settings.isDev)) {
/*  255 */         logger.info("Neow available");
/*  256 */         CardCrawlGame.playerPref.putInteger(AbstractDungeon.player.chosenClass.name() + "_SPIRITS", 1);
/*      */       } else {
/*  258 */         logger.info("No Neow for you");
/*  259 */         CardCrawlGame.playerPref.putInteger(AbstractDungeon.player.chosenClass.name() + "_SPIRITS", 0);
/*  260 */         AbstractDungeon.bossCount = 0;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  265 */     CardCrawlGame.music.dispose();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  271 */     CardCrawlGame.sound.play("DEATH_STINGER", true);
/*      */     
/*      */ 
/*  274 */     String bgmKey = null;
/*  275 */     switch (MathUtils.random(0, 3)) {
/*      */     case 0: 
/*  277 */       bgmKey = "STS_DeathStinger_1_v3_MUSIC.ogg";
/*  278 */       break;
/*      */     case 1: 
/*  280 */       bgmKey = "STS_DeathStinger_2_v3_MUSIC.ogg";
/*  281 */       break;
/*      */     case 2: 
/*  283 */       bgmKey = "STS_DeathStinger_3_v3_MUSIC.ogg";
/*  284 */       break;
/*      */     case 3: 
/*  286 */       bgmKey = "STS_DeathStinger_4_v3_MUSIC.ogg";
/*  287 */       break;
/*      */     }
/*      */     
/*      */     
/*  291 */     CardCrawlGame.music.playTempBgmInstantly(bgmKey, false);
/*      */     
/*  293 */     if (this.isVictory) {
/*  294 */       switch (AbstractDungeon.player.chosenClass) {
/*      */       case IRONCLAD: 
/*  296 */         UnlockTracker.unlockAchievement("RUBY");
/*  297 */         break;
/*      */       case THE_SILENT: 
/*  299 */         UnlockTracker.unlockAchievement("EMERALD");
/*  300 */         break;
/*      */       case DEFECT: 
/*  302 */         UnlockTracker.unlockAchievement("SAPPHIRE");
/*  303 */         break;
/*      */       }
/*      */       
/*      */       
/*  307 */       submitVictoryMetrics();
/*      */       
/*  309 */       if (this.playtime != 0L) {
/*  310 */         StatsScreen.updateVictoryTime(this.playtime);
/*      */       }
/*      */       
/*  313 */       StatsScreen.incrementVictory(AbstractDungeon.player.chosenClass);
/*      */       
/*  315 */       if ((AbstractDungeon.ascensionLevel == 10) && (!Settings.isTrial)) {
/*  316 */         UnlockTracker.unlockAchievement("ASCEND_10");
/*  317 */       } else if ((AbstractDungeon.ascensionLevel == 20) && (!Settings.isTrial)) {
/*  318 */         UnlockTracker.unlockAchievement("ASCEND_20");
/*      */       }
/*      */     } else {
/*  321 */       submitDefeatMetrics(m);
/*  322 */       StatsScreen.incrementDeath(AbstractDungeon.player.chosenClass);
/*      */     }
/*      */     
/*  325 */     this.defeatTextColor.a = 0.0F;
/*  326 */     this.deathTextColor.a = 0.0F;
/*      */     
/*  328 */     if (this.playtime != 0L) {
/*  329 */       StatsScreen.incrementPlayTime(this.playtime);
/*      */     }
/*      */     
/*  332 */     if (Settings.isStandardRun()) {
/*  333 */       StatsScreen.updateFurthestAscent(AbstractDungeon.floorNum);
/*  334 */     } else if ((Settings.isDailyRun) && 
/*  335 */       (!com.megacrit.cardcrawl.daily.TimeHelper.isOfflineMode())) {
/*  336 */       StatsScreen.updateHighestDailyScore(AbstractDungeon.floorNum);
/*      */     }
/*      */     
/*      */ 
/*  340 */     if (SaveHelper.shouldSave()) {
/*  341 */       SaveAndContinue.deleteSave(AbstractDungeon.player.chosenClass);
/*      */     }
/*      */     
/*  344 */     calculateUnlockProgress();
/*  345 */     if (!Settings.isEndless) {
/*  346 */       uploadToSteamLeaderboards();
/*      */     }
/*  348 */     createGameOverStats();
/*  349 */     CardCrawlGame.playerPref.flush();
/*      */   }
/*      */   
/*      */   public static void resetScoreChecks() {
/*  353 */     IS_POOPY = false;
/*  354 */     IS_SPEEDSTER = false;
/*  355 */     IS_LIGHT_SPEED = false;
/*  356 */     IS_HIGHLANDER = false;
/*  357 */     IS_FULL_SET = 0;
/*  358 */     IS_SHINY = false;
/*  359 */     IS_PAUPER = false;
/*  360 */     IS_LIBRARY = false;
/*  361 */     IS_ENCYCLOPEDIA = false;
/*  362 */     IS_WELL_FED = false;
/*  363 */     IS_STUFFED = false;
/*  364 */     IS_CURSES = false;
/*  365 */     IS_ON_MY_OWN = false;
/*  366 */     IS_MONEY_MONEY = false;
/*  367 */     IS_RAINING_MONEY = false;
/*  368 */     IS_I_LIKE_GOLD = false;
/*  369 */     IS_MYSTERY_MACHINE = false;
/*      */   }
/*      */   
/*      */   private void createGameOverStats() {
/*  373 */     this.stats.clear();
/*      */     
/*      */ 
/*  376 */     this.stats.add(new GameOverStat(TEXT[2] + " (" + AbstractDungeon.floorNum + ")", null, 
/*  377 */       Integer.toString(floorPoints)));
/*      */     
/*      */ 
/*  380 */     this.stats.add(new GameOverStat(TEXT[43] + " (" + CardCrawlGame.monstersSlain + ")", null, 
/*      */     
/*      */ 
/*      */ 
/*  384 */       Integer.toString(monsterPoints)));
/*      */     
/*      */ 
/*  387 */     this.stats.add(new GameOverStat(EXORDIUM_ELITE.NAME + " (" + CardCrawlGame.elites1Slain + ")", null, 
/*      */     
/*      */ 
/*      */ 
/*  391 */       Integer.toString(elite1Points)));
/*  392 */     if (((CardCrawlGame.dungeon instanceof com.megacrit.cardcrawl.dungeons.TheCity)) || ((CardCrawlGame.dungeon instanceof TheBeyond))) {
/*  393 */       this.stats.add(new GameOverStat(CITY_ELITE.NAME + " (" + CardCrawlGame.elites2Slain + ")", null, 
/*      */       
/*      */ 
/*      */ 
/*  397 */         Integer.toString(elite2Points)));
/*      */     }
/*  399 */     if ((CardCrawlGame.dungeon instanceof TheBeyond)) {
/*  400 */       this.stats.add(new GameOverStat(BEYOND_ELITE.NAME + " (" + CardCrawlGame.elites3Slain + ")", null, 
/*      */       
/*      */ 
/*      */ 
/*  404 */         Integer.toString(elite3Points)));
/*      */     }
/*      */     
/*      */ 
/*  408 */     this.stats.add(new GameOverStat(BOSSES_SLAIN.NAME + " (" + AbstractDungeon.bossCount + ")", null, 
/*      */     
/*      */ 
/*      */ 
/*  412 */       Integer.toString(bossPoints)));
/*      */     
/*      */ 
/*  415 */     if (IS_POOPY) {
/*  416 */       this.stats.add(new GameOverStat(POOPY.NAME, POOPY.DESCRIPTIONS[0], Integer.toString(-1)));
/*      */     }
/*  418 */     if (IS_SPEEDSTER) {
/*  419 */       this.stats.add(new GameOverStat(SPEEDSTER.NAME, SPEEDSTER.DESCRIPTIONS[0], Integer.toString(25)));
/*      */     }
/*  421 */     if (IS_LIGHT_SPEED) {
/*  422 */       this.stats.add(new GameOverStat(LIGHT_SPEED.NAME, LIGHT_SPEED.DESCRIPTIONS[0], 
/*  423 */         Integer.toString(50)));
/*      */     }
/*  425 */     if (IS_HIGHLANDER) {
/*  426 */       this.stats.add(new GameOverStat(HIGHLANDER.NAME, HIGHLANDER.DESCRIPTIONS[0], 
/*  427 */         Integer.toString(100)));
/*      */     }
/*  429 */     if (IS_SHINY) {
/*  430 */       this.stats.add(new GameOverStat(SHINY.NAME, SHINY.DESCRIPTIONS[0], Integer.toString(50)));
/*      */     }
/*      */     
/*  433 */     if (IS_I_LIKE_GOLD) {
/*  434 */       this.stats.add(new GameOverStat(I_LIKE_GOLD.NAME + " (" + CardCrawlGame.goldGained + ")", I_LIKE_GOLD.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  438 */         Integer.toString(75)));
/*  439 */     } else if (IS_RAINING_MONEY) {
/*  440 */       this.stats.add(new GameOverStat(RAINING_MONEY.NAME + " (" + CardCrawlGame.goldGained + ")", RAINING_MONEY.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  444 */         Integer.toString(50)));
/*  445 */     } else if (IS_MONEY_MONEY) {
/*  446 */       this.stats.add(new GameOverStat(MONEY_MONEY.NAME + " (" + CardCrawlGame.goldGained + ")", MONEY_MONEY.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  450 */         Integer.toString(25)));
/*      */     }
/*      */     
/*  453 */     if (IS_MYSTERY_MACHINE) {
/*  454 */       this.stats.add(new GameOverStat(MYSTERY_MACHINE.NAME + " (" + CardCrawlGame.mysteryMachine + ")", MYSTERY_MACHINE.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  458 */         Integer.toString(25)));
/*      */     }
/*      */     
/*  461 */     if (IS_FULL_SET > 0) {
/*  462 */       this.stats.add(new GameOverStat(COLLECTOR.NAME + " (" + IS_FULL_SET + ")", COLLECTOR.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  466 */         Integer.toString(25 * IS_FULL_SET)));
/*      */     }
/*      */     
/*  469 */     if (IS_PAUPER) {
/*  470 */       this.stats.add(new GameOverStat(PAUPER.NAME, PAUPER.DESCRIPTIONS[0], Integer.toString(50)));
/*      */     }
/*  472 */     if (IS_LIBRARY) {
/*  473 */       this.stats.add(new GameOverStat(LIBRARIAN.NAME, LIBRARIAN.DESCRIPTIONS[0], Integer.toString(25)));
/*      */     }
/*  475 */     if (IS_ENCYCLOPEDIA) {
/*  476 */       this.stats.add(new GameOverStat(ENCYCLOPEDIAN.NAME, ENCYCLOPEDIAN.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  480 */         Integer.toString(50)));
/*      */     }
/*      */     
/*  483 */     if (IS_STUFFED) {
/*  484 */       this.stats.add(new GameOverStat(STUFFED.NAME, STUFFED.DESCRIPTIONS[0], Integer.toString(50)));
/*  485 */     } else if (IS_WELL_FED) {
/*  486 */       this.stats.add(new GameOverStat(WELL_FED.NAME, WELL_FED.DESCRIPTIONS[0], Integer.toString(25)));
/*      */     }
/*      */     
/*  489 */     if (IS_CURSES) {
/*  490 */       this.stats.add(new GameOverStat(CURSES.NAME, CURSES.DESCRIPTIONS[0], Integer.toString(100)));
/*      */     }
/*      */     
/*  493 */     if (IS_ON_MY_OWN) {
/*  494 */       this.stats.add(new GameOverStat(ON_MY_OWN_TERMS.NAME, ON_MY_OWN_TERMS.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  498 */         Integer.toString(50)));
/*      */     }
/*      */     
/*  501 */     if (CardCrawlGame.champion > 0) {
/*  502 */       this.stats.add(new GameOverStat(CHAMPION.NAME + " (" + CardCrawlGame.champion + ")", CHAMPION.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  506 */         Integer.toString(25 * CardCrawlGame.champion)));
/*      */     }
/*      */     
/*  509 */     if (CardCrawlGame.perfect >= 3) {
/*  510 */       this.stats.add(new GameOverStat(BEYOND_PERFECT.NAME, BEYOND_PERFECT.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  514 */         Integer.toString(200)));
/*  515 */     } else if (CardCrawlGame.perfect > 0) {
/*  516 */       this.stats.add(new GameOverStat(PERFECT.NAME + " (" + CardCrawlGame.perfect + ")", PERFECT.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  520 */         Integer.toString(50 * CardCrawlGame.perfect)));
/*      */     }
/*      */     
/*  523 */     if (CardCrawlGame.overkill) {
/*  524 */       this.stats.add(new GameOverStat(OVERKILL.NAME, OVERKILL.DESCRIPTIONS[0], Integer.toString(25)));
/*      */     }
/*      */     
/*  527 */     if (CardCrawlGame.combo) {
/*  528 */       this.stats.add(new GameOverStat(COMBO.NAME, COMBO.DESCRIPTIONS[0], Integer.toString(25)));
/*      */     }
/*      */     
/*  531 */     if (AbstractDungeon.isAscensionMode)
/*      */     {
/*  533 */       this.stats.add(new GameOverStat(ASCENSION.NAME + " (" + AbstractDungeon.ascensionLevel + ")", ASCENSION.DESCRIPTIONS[0], 
/*      */       
/*      */ 
/*      */ 
/*  537 */         Integer.toString(ascensionPoints)));
/*      */     }
/*      */     
/*      */ 
/*  541 */     this.stats.add(new GameOverStat());
/*      */     
/*      */ 
/*  544 */     this.stats.add(new GameOverStat(TEXT[6], null, Integer.toString(this.score)));
/*      */   }
/*      */   
/*      */   private void calculateUnlockProgress() {
/*  548 */     this.score = calcScore(this.isVictory);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  554 */     if (((this.score > 10000) || (CardCrawlGame.cheater)) && (!Settings.isModded)) {
/*  555 */       CardCrawlGame.playerPref.putInteger("fun_count3", CardCrawlGame.playerPref.getInteger("fun_count3", 0) + 1);
/*      */     }
/*      */     
/*  558 */     this.unlockLevel = UnlockTracker.getUnlockLevel(AbstractDungeon.player.chosenClass);
/*  559 */     if (this.unlockLevel >= 5) {
/*  560 */       this.maxLevel = true;
/*  561 */       return;
/*      */     }
/*      */     
/*  564 */     if (this.score == 0) {
/*  565 */       this.playedWhir = true;
/*      */     }
/*      */     
/*  568 */     this.unlockProgress = UnlockTracker.getCurrentProgress(AbstractDungeon.player.chosenClass);
/*  569 */     this.unlockTargetStart = this.unlockProgress;
/*  570 */     this.unlockCost = UnlockTracker.getCurrentScoreCost(AbstractDungeon.player.chosenClass);
/*  571 */     this.unlockTargetProgress = (this.unlockProgress + this.score);
/*      */     
/*  573 */     this.nextUnlockCost = UnlockTracker.incrementUnlockRamp(this.unlockCost);
/*      */     
/*  575 */     if (this.unlockTargetProgress >= this.unlockCost) {
/*  576 */       this.unlockBundle = UnlockTracker.getUnlockBundle(AbstractDungeon.player.chosenClass, this.unlockLevel);
/*      */       
/*      */ 
/*  579 */       if (this.unlockLevel == 4) {
/*  580 */         this.unlockTargetProgress = this.unlockCost;
/*  581 */       } else if (this.unlockTargetProgress > this.unlockCost - this.unlockProgress + this.nextUnlockCost - 1.0F)
/*      */       {
/*  583 */         this.unlockTargetProgress = (this.unlockCost - this.unlockProgress + this.nextUnlockCost - 1.0F);
/*      */       }
/*      */     }
/*      */     
/*  587 */     logger.info("SCOR: " + this.score);
/*  588 */     logger.info("PROG: " + this.unlockProgress);
/*  589 */     logger.info("STRT: " + this.unlockTargetStart);
/*  590 */     logger.info("TRGT: " + this.unlockTargetProgress);
/*  591 */     logger.info("COST: " + this.unlockCost);
/*      */     
/*      */ 
/*  594 */     UnlockTracker.addScore(AbstractDungeon.player.chosenClass, this.score);
/*      */     
/*  596 */     this.progressPercent = (this.unlockTargetStart / this.unlockCost);
/*      */   }
/*      */   
/*      */   private boolean canUploadLeaderboards()
/*      */   {
/*  601 */     if ((Settings.isModded) || (Settings.isTrial) || (Settings.seedSet)) {
/*  602 */       return false;
/*      */     }
/*      */     
/*  605 */     return true;
/*      */   }
/*      */   
/*      */   private void uploadToSteamLeaderboards() {
/*  609 */     if (!canUploadLeaderboards()) {
/*  610 */       return;
/*      */     }
/*      */     
/*      */ 
/*  614 */     switch (AbstractDungeon.player.chosenClass) {
/*      */     case IRONCLAD: 
/*  616 */       uploadScoreHelper("IRONCLAD");
/*  617 */       break;
/*      */     case THE_SILENT: 
/*  619 */       uploadScoreHelper("SILENT");
/*  620 */       break;
/*      */     case DEFECT: 
/*  622 */       uploadScoreHelper("DEFECT");
/*  623 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  628 */     StatsScreen.updateHighestScore(this.score);
/*      */   }
/*      */   
/*      */   private void uploadScoreHelper(String characterString) {
/*  632 */     StringBuilder highScoreString = new StringBuilder();
/*  633 */     StringBuilder fastestWinString = new StringBuilder(characterString);
/*      */     
/*  635 */     if (Settings.isDailyRun) {
/*  636 */       highScoreString.append("DAILY_" + Long.toString(Settings.dailyDate));
/*  637 */       long lastDaily = Settings.dailyPref.getLong("LAST_DAILY", 0L);
/*      */       
/*      */ 
/*  640 */       Settings.hasDoneDailyToday = lastDaily == Settings.dailyDate;
/*  641 */       if (Settings.hasDoneDailyToday) {
/*  642 */         logger.info("Player has already done the daily for: " + Settings.dailyDate);
/*      */       }
/*      */     } else {
/*  645 */       highScoreString.append(characterString);
/*  646 */       highScoreString.append("_HIGH_SCORE");
/*      */     }
/*  648 */     fastestWinString.append("_FASTEST_WIN");
/*      */     
/*  650 */     if (Settings.isBeta) {
/*  651 */       highScoreString.append("_BETA");
/*  652 */       fastestWinString.append("_BETA");
/*      */     }
/*      */     
/*  655 */     if ((Settings.isDailyRun) && (!Settings.hasDoneDailyToday)) {
/*  656 */       logger.info("Uploading score for day: " + Settings.dailyDate + "\nScore is: " + this.score);
/*  657 */       Settings.dailyPref.putLong("LAST_DAILY", Settings.dailyDate);
/*  658 */       Settings.dailyPref.flush();
/*  659 */       SteamSaveSync.uploadDailyLeaderboardScore(highScoreString.toString(), this.score);
/*  660 */     } else if (!Settings.isDailyRun) {
/*  661 */       SteamSaveSync.uploadLeaderboardScore(highScoreString.toString(), this.score);
/*      */     }
/*      */     
/*  664 */     if ((this.isVictory) && (this.playtime < 18000L) && (this.playtime > 280L) && (!Settings.isDailyRun)) {
/*  665 */       SteamSaveSync.uploadLeaderboardScore(fastestWinString.toString(), Math.toIntExact(this.playtime));
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public static int calcScore(boolean victory)
/*      */   {
/*  672 */     floorPoints = 0;
/*  673 */     monsterPoints = 0;
/*  674 */     elite1Points = 0;
/*  675 */     elite2Points = 0;
/*  676 */     elite3Points = 0;
/*  677 */     bossPoints = 0;
/*  678 */     ascensionPoints = 0;
/*      */     
/*  680 */     int tmp = AbstractDungeon.floorNum * 5;
/*  681 */     floorPoints = AbstractDungeon.floorNum * 5;
/*  682 */     monsterPoints = CardCrawlGame.monstersSlain * 2;
/*  683 */     elite1Points = CardCrawlGame.elites1Slain * 10;
/*  684 */     elite2Points = CardCrawlGame.elites2Slain * 20;
/*  685 */     elite3Points = CardCrawlGame.elites3Slain * 30;
/*      */     
/*      */ 
/*  688 */     bossPoints = 0;
/*  689 */     int bossMultiplier = 50;
/*  690 */     for (int i = 0; i < AbstractDungeon.bossCount; i++) {
/*  691 */       bossPoints += bossMultiplier;
/*  692 */       bossMultiplier += 50;
/*      */     }
/*      */     
/*  695 */     tmp += monsterPoints;
/*  696 */     tmp += elite1Points;
/*  697 */     tmp += elite2Points;
/*  698 */     tmp += elite3Points;
/*  699 */     tmp += bossPoints;
/*      */     
/*  701 */     tmp += CardCrawlGame.champion * 25;
/*  702 */     if (CardCrawlGame.perfect >= 3) {
/*  703 */       tmp += 200;
/*      */     } else {
/*  705 */       tmp += CardCrawlGame.perfect * 50;
/*      */     }
/*      */     
/*  708 */     if (CardCrawlGame.overkill) {
/*  709 */       tmp += 25;
/*      */     }
/*      */     
/*  712 */     if (CardCrawlGame.combo) {
/*  713 */       tmp += 25;
/*      */     }
/*      */     
/*  716 */     if (AbstractDungeon.isAscensionMode) {
/*  717 */       ascensionPoints = MathUtils.round(tmp * (0.05F * AbstractDungeon.ascensionLevel));
/*  718 */       tmp += ascensionPoints;
/*      */     }
/*      */     
/*  721 */     tmp += checkScoreBonus(victory);
/*      */     
/*  723 */     return tmp;
/*      */   }
/*      */   
/*      */   private static int checkScoreBonus(boolean victory) {
/*  727 */     int points = 0;
/*      */     
/*      */ 
/*  730 */     if (AbstractDungeon.player.hasRelic("Spirit Poop")) {
/*  731 */       IS_POOPY = true;
/*  732 */       points--;
/*      */     }
/*      */     
/*      */ 
/*  736 */     IS_FULL_SET = AbstractDungeon.player.masterDeck.fullSetCheck();
/*  737 */     if (IS_FULL_SET > 0) {
/*  738 */       points += 25 * IS_FULL_SET;
/*      */     }
/*      */     
/*      */ 
/*  742 */     if (AbstractDungeon.player.relics.size() >= 25) {
/*  743 */       IS_SHINY = true;
/*  744 */       points += 50;
/*      */     }
/*      */     
/*      */ 
/*  748 */     if (AbstractDungeon.player.masterDeck.size() >= 50) {
/*  749 */       IS_ENCYCLOPEDIA = true;
/*  750 */       points += 50;
/*  751 */     } else if (AbstractDungeon.player.masterDeck.size() >= 35) {
/*  752 */       IS_LIBRARY = true;
/*  753 */       points += 25;
/*      */     }
/*      */     
/*      */ 
/*  757 */     switch (AbstractDungeon.player.chosenClass) {
/*      */     case IRONCLAD: 
/*  759 */       if (AbstractDungeon.player.maxHealth >= 110) {
/*  760 */         IS_STUFFED = true;
/*  761 */         points += 50;
/*  762 */       } else if (AbstractDungeon.player.maxHealth >= 95) {
/*  763 */         IS_WELL_FED = true;
/*  764 */         points += 25;
/*      */       }
/*      */       break;
/*      */     case THE_SILENT: 
/*  768 */       if (AbstractDungeon.player.maxHealth >= 100) {
/*  769 */         IS_STUFFED = true;
/*  770 */         points += 50;
/*  771 */       } else if (AbstractDungeon.player.maxHealth >= 85) {
/*  772 */         IS_WELL_FED = true;
/*  773 */         points += 25;
/*      */       }
/*      */       break;
/*      */     case DEFECT: 
/*  777 */       if (AbstractDungeon.player.maxHealth >= 105) {
/*  778 */         IS_STUFFED = true;
/*  779 */         points += 50;
/*  780 */       } else if (AbstractDungeon.player.maxHealth >= 90) {
/*  781 */         IS_WELL_FED = true;
/*  782 */         points += 25;
/*      */       }
/*      */       
/*      */ 
/*      */       break;
/*      */     }
/*      */     
/*      */     
/*  790 */     if (AbstractDungeon.player.masterDeck.cursedCheck()) {
/*  791 */       IS_CURSES = true;
/*  792 */       points += 100;
/*      */     }
/*      */     
/*      */ 
/*  796 */     if (CardCrawlGame.goldGained >= 3000) {
/*  797 */       IS_I_LIKE_GOLD = true;
/*  798 */       points += 75;
/*  799 */     } else if (CardCrawlGame.goldGained >= 2000) {
/*  800 */       IS_RAINING_MONEY = true;
/*  801 */       points += 50;
/*  802 */     } else if (CardCrawlGame.goldGained >= 1000) {
/*  803 */       IS_MONEY_MONEY = true;
/*  804 */       points += 25;
/*      */     }
/*      */     
/*  807 */     if (CardCrawlGame.mysteryMachine >= 15) {
/*  808 */       IS_MYSTERY_MACHINE = true;
/*  809 */       points += 25;
/*      */     }
/*      */     
/*  812 */     if (victory)
/*      */     {
/*      */ 
/*      */ 
/*  816 */       System.out.println(CardCrawlGame.playtime);
/*  817 */       if (CardCrawlGame.playtime <= 2700L) {
/*  818 */         IS_LIGHT_SPEED = true;
/*  819 */         points += 50;
/*  820 */       } else if (CardCrawlGame.playtime <= 3600L) {
/*  821 */         IS_SPEEDSTER = true;
/*  822 */         points += 25;
/*      */       }
/*      */       
/*      */ 
/*  826 */       if (AbstractDungeon.player.masterDeck.highlanderCheck()) {
/*  827 */         IS_HIGHLANDER = true;
/*  828 */         points += 100;
/*      */       }
/*      */       
/*      */ 
/*  832 */       if (AbstractDungeon.player.masterDeck.pauperCheck()) {
/*  833 */         IS_PAUPER = true;
/*  834 */         points += 50;
/*      */       }
/*      */     }
/*      */     
/*  838 */     return points;
/*      */   }
/*      */   
/*      */   private void submitDefeatMetrics(MonsterGroup m) {
/*  842 */     if ((m != null) && (!m.areMonstersDead()) && (!m.areMonstersBasicallyDead())) {
/*  843 */       CardCrawlGame.metricData.addEncounterData();
/*      */     }
/*      */     
/*  846 */     Metrics metrics = new Metrics();
/*      */     
/*      */ 
/*  849 */     metrics.gatherAllDataAndSave(true, m);
/*      */     
/*  851 */     if (shouldUploadMetricData()) {
/*  852 */       metrics.setValues(true, m, Metrics.MetricRequestType.UPLOAD_METRICS);
/*  853 */       Thread t = new Thread(metrics);
/*  854 */       t.setName("Metrics");
/*  855 */       t.start();
/*      */     }
/*      */   }
/*      */   
/*      */   public static boolean shouldUploadMetricData() {
/*  860 */     return (Settings.UPLOAD_DATA) && (SteamSaveSync.steamUser != null) && (SteamSaveSync.steamStats != null) && 
/*  861 */       (Settings.isStandardRun());
/*      */   }
/*      */   
/*      */   private void submitVictoryMetrics() {
/*  865 */     Metrics metrics = new Metrics();
/*      */     
/*      */ 
/*  868 */     metrics.gatherAllDataAndSave(false, null);
/*      */     
/*  870 */     if (shouldUploadMetricData()) {
/*  871 */       metrics.setValues(false, null, Metrics.MetricRequestType.UPLOAD_METRICS);
/*  872 */       Thread t = new Thread(metrics);
/*  873 */       t.start();
/*      */     }
/*      */     
/*  876 */     if (Settings.isStandardRun()) {
/*  877 */       StatsScreen.updateFurthestAscent(AbstractDungeon.floorNum);
/*      */     }
/*      */     
/*  880 */     if (SaveHelper.shouldSave()) {
/*  881 */       SaveAndContinue.deleteSave(AbstractDungeon.player.chosenClass);
/*      */     }
/*      */   }
/*      */   
/*      */   private String getDeathBannerText() {
/*  886 */     ArrayList<String> list = new ArrayList();
/*  887 */     list.add(TEXT[7]);
/*  888 */     list.add(TEXT[8]);
/*  889 */     list.add(TEXT[9]);
/*  890 */     list.add(TEXT[10]);
/*  891 */     list.add(TEXT[11]);
/*  892 */     list.add(TEXT[12]);
/*  893 */     list.add(TEXT[13]);
/*  894 */     list.add(TEXT[14]);
/*      */     
/*  896 */     return (String)list.get(MathUtils.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   private String getDeathText() {
/*  900 */     ArrayList<String> list = new ArrayList();
/*  901 */     list.add(TEXT[15]);
/*  902 */     list.add(TEXT[16]);
/*  903 */     list.add(TEXT[17]);
/*  904 */     list.add(TEXT[18]);
/*  905 */     list.add(TEXT[19]);
/*  906 */     list.add(TEXT[20]);
/*  907 */     list.add(TEXT[21]);
/*  908 */     list.add(TEXT[22]);
/*  909 */     list.add(TEXT[23]);
/*  910 */     list.add(TEXT[24]);
/*  911 */     list.add(TEXT[25]);
/*  912 */     list.add(TEXT[26]);
/*  913 */     list.add(TEXT[27]);
/*  914 */     list.add(TEXT[28]);
/*  915 */     list.add(TEXT[29]);
/*      */     
/*  917 */     if (AbstractDungeon.player.chosenClass == AbstractPlayer.PlayerClass.THE_SILENT) {
/*  918 */       list.add("...");
/*      */     }
/*      */     
/*  921 */     return (String)list.get(MathUtils.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public void hide() {
/*  925 */     this.returnButton.hide();
/*  926 */     AbstractDungeon.dynamicBanner.hide();
/*      */   }
/*      */   
/*      */   public void reopen() {
/*  930 */     reopen(false);
/*      */   }
/*      */   
/*      */   public void reopen(boolean fromVictoryUnlock)
/*      */   {
/*  935 */     AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.DEATH;
/*  936 */     this.statsTimer = 0.5F;
/*      */     
/*  938 */     if (this.isVictory) {
/*  939 */       AbstractDungeon.dynamicBanner.appearInstantly(TEXT[1]);
/*      */     } else {
/*  941 */       AbstractDungeon.dynamicBanner.appearInstantly(TEXT[30]);
/*      */     }
/*  943 */     AbstractDungeon.overlayMenu.showBlackScreen(1.0F);
/*      */     
/*  945 */     if (fromVictoryUnlock) {
/*  946 */       this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[31]);
/*      */     }
/*  948 */     else if (!this.showingStats) {
/*  949 */       this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[32]);
/*      */     }
/*  951 */     else if (this.unlockBundle == null) {
/*  952 */       if (!this.isVictory) {
/*  953 */         if ((UnlockTracker.isCharacterLocked("The Silent")) || ((UnlockTracker.isCharacterLocked("Defect")) && (AbstractDungeon.player.chosenClass == AbstractPlayer.PlayerClass.DEFECT)))
/*      */         {
/*  955 */           this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[40]);
/*      */         } else {
/*  957 */           this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[34]);
/*      */         }
/*      */       } else {
/*  960 */         this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[35]);
/*      */       }
/*      */     } else {
/*  963 */       this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[36]);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void update()
/*      */   {
/*  970 */     if ((Settings.isDebug) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedRight)) {
/*  971 */       UnlockTracker.resetUnlockProgress(AbstractDungeon.player.chosenClass);
/*      */     }
/*      */     
/*  974 */     this.returnButton.update();
/*  975 */     if ((this.returnButton.hb.clicked) || ((this.returnButton.show) && (CInputActionSet.select.isJustPressed()))) {
/*  976 */       CInputActionSet.topPanel.unpress();
/*  977 */       if (Settings.isControllerMode) {
/*  978 */         Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/*      */       }
/*  980 */       this.returnButton.hb.clicked = false;
/*  981 */       if (!this.showingStats) {
/*  982 */         this.showingStats = true;
/*  983 */         this.statsTimer = 0.5F;
/*  984 */         logger.info("Clicked");
/*      */         
/*      */ 
/*  987 */         this.returnButton = new ReturnToMenuButton();
/*      */         
/*  989 */         if ((this.isVictory) && (AbstractDungeon.isAscensionMode) && (!Settings.isTrial) && (AbstractDungeon.ascensionLevel < 20))
/*      */         {
/*  991 */           if (StatsScreen.isPlayingHighestAscension(AbstractDungeon.player.chosenClass)) {
/*  992 */             AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.AscensionLevelUpTextEffect());
/*  993 */             break label227; } } if ((!AbstractDungeon.ascensionCheck) && (UnlockTracker.isAscensionUnlocked(AbstractDungeon.player.chosenClass)))
/*      */         {
/*  995 */           AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.AscensionUnlockedTextEffect());
/*      */         }
/*      */         label227:
/*  998 */         if (this.unlockBundle == null) {
/*  999 */           if (!this.isVictory) {
/* 1000 */             if ((UnlockTracker.isCharacterLocked("The Silent")) || (UnlockTracker.isCharacterLocked("Defect")))
/*      */             {
/* 1002 */               this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[40]);
/*      */             } else {
/* 1004 */               this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[37]);
/*      */             }
/*      */           } else {
/* 1007 */             this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[39]);
/*      */           }
/*      */         } else {
/* 1010 */           this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[40]);
/*      */         }
/* 1012 */       } else if (this.unlockBundle != null) {
/* 1013 */         AbstractDungeon.gUnlockScreen.open(this.unlockBundle);
/* 1014 */         AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.DEATH;
/* 1015 */         AbstractDungeon.screen = AbstractDungeon.CurrentScreen.NEOW_UNLOCK;
/* 1016 */         this.unlockBundle = null;
/* 1017 */         if (UnlockTracker.isCharacterLocked("The Silent")) {
/* 1018 */           this.returnButton.appear(Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.15F, TEXT[40]);
/*      */         } else {
/* 1020 */           this.returnButton.label = TEXT[37];
/*      */         }
/*      */       }
/* 1023 */       else if (this.isVictory) {
/* 1024 */         this.returnButton.hide();
/*      */         
/*      */ 
/* 1027 */         if ((AbstractDungeon.unlocks.isEmpty()) || (Settings.isDemo)) {
/* 1028 */           if ((Settings.isDemo) || (Settings.isDailyRun)) {
/* 1029 */             CardCrawlGame.startOver();
/*      */           }
/* 1031 */           else if (UnlockTracker.isCharacterLocked("The Silent")) {
/* 1032 */             AbstractDungeon.unlocks.add(new TheSilentUnlock());
/* 1033 */             AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/* 1034 */           } else if ((UnlockTracker.isCharacterLocked("Defect")) && (AbstractDungeon.player.chosenClass == AbstractPlayer.PlayerClass.THE_SILENT))
/*      */           {
/* 1036 */             AbstractDungeon.unlocks.add(new DefectUnlock());
/* 1037 */             AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/*      */           } else {
/* 1039 */             CardCrawlGame.startOverButShowCredits();
/*      */           }
/*      */         }
/*      */         else {
/* 1043 */           AbstractDungeon.unlocks.clear();
/* 1044 */           Settings.isTrial = false;
/* 1045 */           Settings.isDailyRun = false;
/* 1046 */           Settings.isEndless = false;
/* 1047 */           CardCrawlGame.trial = null;
/* 1048 */           if (Settings.isDailyRun) {
/* 1049 */             CardCrawlGame.startOver();
/*      */           } else {
/* 1051 */             CardCrawlGame.startOverButShowCredits();
/*      */           }
/*      */         }
/*      */       } else {
/* 1055 */         this.returnButton.hide();
/*      */         
/*      */ 
/* 1058 */         if ((AbstractDungeon.unlocks.isEmpty()) || (Settings.isDemo) || (Settings.isDailyRun) || (Settings.isTrial))
/*      */         {
/* 1060 */           if (UnlockTracker.isCharacterLocked("The Silent")) {
/* 1061 */             AbstractDungeon.unlocks.add(new TheSilentUnlock());
/* 1062 */             AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/* 1063 */           } else if ((UnlockTracker.isCharacterLocked("Defect")) && (AbstractDungeon.player.chosenClass == AbstractPlayer.PlayerClass.THE_SILENT))
/*      */           {
/* 1065 */             AbstractDungeon.unlocks.add(new DefectUnlock());
/* 1066 */             AbstractDungeon.unlockScreen.open((AbstractUnlock)AbstractDungeon.unlocks.remove(0));
/*      */           } else {
/* 1068 */             Settings.isTrial = false;
/* 1069 */             Settings.isDailyRun = false;
/* 1070 */             Settings.isEndless = false;
/* 1071 */             CardCrawlGame.trial = null;
/* 1072 */             CardCrawlGame.startOver();
/*      */           }
/*      */         } else {
/* 1075 */           AbstractDungeon.unlocks.clear();
/* 1076 */           Settings.isTrial = false;
/* 1077 */           Settings.isDailyRun = false;
/* 1078 */           Settings.isEndless = false;
/* 1079 */           CardCrawlGame.trial = null;
/* 1080 */           CardCrawlGame.startOverButShowCredits();
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1086 */     updateStatsScreen();
/*      */     
/* 1088 */     if (this.deathAnimWaitTimer != 0.0F) {
/* 1089 */       this.deathAnimWaitTimer -= Gdx.graphics.getDeltaTime();
/* 1090 */       if (this.deathAnimWaitTimer < 0.0F) {
/* 1091 */         this.deathAnimWaitTimer = 0.0F;
/* 1092 */         AbstractDungeon.player.playDeathAnimation();
/*      */       }
/*      */     } else {
/* 1095 */       this.deathTextTimer -= Gdx.graphics.getDeltaTime();
/* 1096 */       if (this.deathTextTimer < 0.0F) {
/* 1097 */         this.deathTextTimer = 0.0F;
/*      */       }
/*      */       
/* 1100 */       this.deathTextColor.a = Interpolation.fade.apply(0.0F, 1.0F, 1.0F - this.deathTextTimer / 5.0F);
/* 1101 */       this.defeatTextColor.a = Interpolation.fade.apply(0.0F, 1.0F, 1.0F - this.deathTextTimer / 5.0F);
/*      */     }
/*      */     
/* 1104 */     if (this.monsters != null) {
/* 1105 */       this.monsters.update();
/* 1106 */       this.monsters.updateAnimations();
/*      */     }
/*      */     
/* 1109 */     if (this.particles.size() < 50.0F) {
/* 1110 */       this.particles.add(new DeathScreenFloatyEffect());
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateStatsScreen() {
/* 1115 */     if (this.showingStats) {
/* 1116 */       this.progressBarAlpha = com.megacrit.cardcrawl.helpers.MathHelper.slowColorLerpSnap(this.progressBarAlpha, 1.0F);
/*      */       
/* 1118 */       this.statsTimer -= Gdx.graphics.getDeltaTime();
/* 1119 */       if (this.statsTimer < 0.0F) {
/* 1120 */         this.statsTimer = 0.0F;
/*      */       }
/*      */       
/* 1123 */       this.returnButton.y = Interpolation.pow3In.apply(Settings.HEIGHT * 0.1F, Settings.HEIGHT * 0.15F, this.statsTimer * 1.0F / 0.5F);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1135 */       AbstractDungeon.dynamicBanner.y = Interpolation.pow3In.apply(Settings.HEIGHT - 220.0F * Settings.scale, Settings.HEIGHT - 280.0F * Settings.scale, this.statsTimer * 1.0F / 0.5F);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1140 */       for (Iterator localIterator = this.stats.iterator(); localIterator.hasNext();) { i = (GameOverStat)localIterator.next();
/* 1141 */         i.update();
/*      */       }
/*      */       GameOverStat i;
/* 1144 */       if (this.statAnimateTimer < 0.0F) {
/* 1145 */         boolean allStatsShown = true;
/*      */         
/* 1147 */         for (GameOverStat i : this.stats) {
/* 1148 */           if (i.hidden) {
/* 1149 */             i.hidden = false;
/* 1150 */             this.statAnimateTimer = 0.1F;
/* 1151 */             allStatsShown = false;
/* 1152 */             break;
/*      */           }
/*      */         }
/*      */         
/* 1156 */         if (allStatsShown) {
/* 1157 */           animateProgressBar();
/*      */         }
/*      */       } else {
/* 1160 */         this.statAnimateTimer -= Gdx.graphics.getDeltaTime();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void animateProgressBar() {
/* 1166 */     if (this.maxLevel) {
/* 1167 */       return;
/*      */     }
/*      */     
/* 1170 */     this.progressBarTimer -= Gdx.graphics.getDeltaTime();
/* 1171 */     if (this.progressBarTimer < 0.0F) {
/* 1172 */       this.progressBarTimer = 0.0F;
/*      */     }
/*      */     
/* 1175 */     if (this.progressBarTimer > 2.0F) {
/* 1176 */       return;
/*      */     }
/*      */     
/* 1179 */     if (!this.playedWhir) {
/* 1180 */       this.playedWhir = true;
/* 1181 */       this.whirId = CardCrawlGame.sound.play("UNLOCK_WHIR");
/*      */     }
/*      */     
/* 1184 */     this.unlockProgress = Interpolation.pow2In.apply(this.unlockTargetProgress, this.unlockTargetStart, this.progressBarTimer / 2.0F);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1189 */     if ((this.unlockProgress >= this.unlockCost) && (this.unlockLevel != 5)) {
/* 1190 */       if (this.unlockLevel == 4) {
/* 1191 */         this.unlockProgress = this.unlockCost;
/* 1192 */         this.unlockLevel += 1;
/* 1193 */         AbstractDungeon.topLevelEffects.add(new UnlockTextEffect());
/*      */       } else {
/* 1195 */         this.unlockTargetProgress = (this.score - (this.unlockCost - this.unlockTargetStart));
/* 1196 */         this.progressBarTimer = 3.0F;
/* 1197 */         AbstractDungeon.topLevelEffects.add(new UnlockTextEffect());
/* 1198 */         CardCrawlGame.sound.stop("UNLOCK_WHIR", this.whirId);
/* 1199 */         this.playedWhir = false;
/* 1200 */         this.unlockProgress = 0.0F;
/* 1201 */         this.unlockTargetStart = 0.0F;
/*      */         
/*      */ 
/* 1204 */         if (this.unlockTargetProgress > this.nextUnlockCost - 1) {
/* 1205 */           this.unlockTargetProgress = (this.nextUnlockCost - 1);
/*      */         }
/*      */         
/* 1208 */         this.unlockCost = this.nextUnlockCost;
/* 1209 */         this.unlockLevel += 1;
/*      */       }
/*      */     }
/*      */     
/* 1213 */     this.progressPercent = (this.unlockProgress / this.unlockCost);
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb) {
/* 1217 */     for (Iterator<DeathScreenFloatyEffect> i = this.particles.iterator(); i.hasNext();) {
/* 1218 */       DeathScreenFloatyEffect e = (DeathScreenFloatyEffect)i.next();
/* 1219 */       if (e.renderBehind) {
/* 1220 */         e.render(sb);
/*      */       }
/* 1222 */       e.update();
/* 1223 */       if (e.isDone) {
/* 1224 */         i.remove();
/*      */       }
/*      */     }
/*      */     
/* 1228 */     AbstractDungeon.player.render(sb);
/* 1229 */     if (this.monsters != null) {
/* 1230 */       this.monsters.render(sb);
/*      */     }
/*      */     
/* 1233 */     sb.setBlendFunction(770, 1);
/* 1234 */     for (DeathScreenFloatyEffect e : this.particles) {
/* 1235 */       if (!e.renderBehind) {
/* 1236 */         e.render(sb);
/*      */       }
/*      */     }
/* 1239 */     sb.setBlendFunction(770, 771);
/*      */     
/* 1241 */     renderStatsScreen(sb);
/*      */     
/* 1243 */     if ((!this.showingStats) && (!this.isVictory)) {
/* 1244 */       FontHelper.renderFontCentered(sb, FontHelper.topPanelInfoFont, this.deathText, Settings.WIDTH / 2.0F, DEATH_TEXT_Y, this.deathTextColor);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1252 */     this.returnButton.render(sb);
/*      */   }
/*      */   
/*      */   private void renderStatsScreen(SpriteBatch sb) {
/* 1256 */     if (this.showingStats) {
/* 1257 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, (1.0F - this.statsTimer) * 0.6F));
/* 1258 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*      */       
/*      */ 
/* 1261 */       float y = STAT_START_Y + this.stats.size() * STAT_OFFSET_Y / 2.0F;
/* 1262 */       if (this.stats.size() >= 10) {
/* 1263 */         y = STAT_START_Y + this.stats.size() / 2 * STAT_OFFSET_Y / 2.0F;
/*      */       }
/*      */       
/*      */ 
/* 1267 */       for (int i = 0; i < this.stats.size(); i++)
/*      */       {
/*      */ 
/* 1270 */         if (this.stats.size() <= 10) {
/* 1271 */           if (i == this.stats.size() - 2) {
/* 1272 */             ((GameOverStat)this.stats.get(i)).renderLine(sb, false, y);
/*      */           } else {
/* 1274 */             ((GameOverStat)this.stats.get(i)).render(sb, 760.0F * Settings.scale, y);
/*      */           }
/*      */           
/*      */         }
/* 1278 */         else if (i != this.stats.size() - 1) {
/* 1279 */           if (i < (this.stats.size() - 1) / 2) {
/* 1280 */             ((GameOverStat)this.stats.get(i)).render(sb, 460.0F * Settings.scale, y);
/*      */           } else {
/* 1282 */             ((GameOverStat)this.stats.get(i)).render(sb, 1060.0F * Settings.scale, y + STAT_OFFSET_Y * ((this.stats.size() - 1) / 2));
/*      */           }
/*      */         }
/*      */         else {
/* 1286 */           ((GameOverStat)this.stats.get(i)).renderLine(sb, true, y + STAT_OFFSET_Y * (this.stats.size() / 2));
/*      */           
/*      */ 
/* 1289 */           ((GameOverStat)this.stats.get(i)).render(sb, 760.0F * Settings.scale, y + STAT_OFFSET_Y * (this.stats.size() / 2 - 1));
/*      */         }
/* 1291 */         y -= STAT_OFFSET_Y;
/*      */       }
/*      */       
/* 1294 */       renderProgressBar(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderProgressBar(SpriteBatch sb) {
/* 1299 */     if (this.maxLevel) {
/* 1300 */       return;
/*      */     }
/*      */     
/* 1303 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.progressBarAlpha * 0.3F));
/* 1304 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 576.0F * Settings.scale, Settings.HEIGHT * 0.2F, 768.0F * Settings.scale, 14.0F * Settings.scale);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1311 */     sb.setColor(new Color(1.0F, 0.8F, 0.3F, this.progressBarAlpha * 0.9F));
/* 1312 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 576.0F * Settings.scale, Settings.HEIGHT * 0.2F, 768.0F * Settings.scale * this.progressPercent, 14.0F * Settings.scale);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1319 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.progressBarAlpha * 0.25F));
/* 1320 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 576.0F * Settings.scale, Settings.HEIGHT * 0.2F, 768.0F * Settings.scale * this.progressPercent, 4.0F * Settings.scale);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1327 */     String derp = "[" + (int)this.unlockProgress + "/" + this.unlockCost + "]";
/* 1328 */     Color tColor = Settings.CREAM_COLOR.cpy();
/* 1329 */     tColor.a = (this.progressBarAlpha * 0.9F);
/* 1330 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont, derp, 576.0F * Settings.scale, Settings.HEIGHT * 0.2F - 12.0F * Settings.scale, tColor);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1338 */     if (5 - this.unlockLevel == 1) {
/* 1339 */       derp = TEXT[42] + (5 - this.unlockLevel);
/*      */     } else {
/* 1341 */       derp = TEXT[41] + (5 - this.unlockLevel);
/*      */     }
/* 1343 */     FontHelper.renderFontRightTopAligned(sb, FontHelper.topPanelInfoFont, derp, 1344.0F * Settings.scale, Settings.HEIGHT * 0.2F - 12.0F * Settings.scale, tColor);
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DeathScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */