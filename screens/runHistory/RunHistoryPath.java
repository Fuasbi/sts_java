/*     */ package com.megacrit.cardcrawl.screens.runHistory;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.GameCursor.CursorType;
/*     */ import com.megacrit.cardcrawl.screens.stats.BattleStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.BossRelicChoiceStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.CampfireChoice;
/*     */ import com.megacrit.cardcrawl.screens.stats.CardChoiceStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.EventStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.ObtainStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.RunData;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.LinkedList;
/*     */ import java.util.List;
/*     */ 
/*     */ public class RunHistoryPath
/*     */ {
/*     */   public static final String BOSS_TREASURE_LABEL = "T!";
/*     */   private RunData runData;
/*     */   public List<RunPathElement> pathElements;
/*     */   private static final int ICONS_PER_ROW = 25;
/*     */   
/*     */   public RunHistoryPath()
/*     */   {
/*  27 */     this.pathElements = new ArrayList();
/*     */   }
/*     */   
/*     */   public void setRunData(RunData newData) {
/*  31 */     this.runData = newData;
/*     */     
/*  33 */     List<String> labels = new ArrayList();
/*  34 */     int choiceIndex = 0;int outcomeIndex = 0;
/*  35 */     List<String> choices = this.runData.path_taken;
/*  36 */     List<String> outcomes = this.runData.path_per_floor;
/*  37 */     if (choices == null) {
/*  38 */       choices = new LinkedList();
/*     */     }
/*  40 */     if (outcomes == null) {
/*  41 */       outcomes = new LinkedList();
/*     */     }
/*     */     
/*  44 */     while ((choiceIndex < choices.size()) || (outcomeIndex < outcomes.size())) {
/*  45 */       String choice = choiceIndex < choices.size() ? (String)choices.get(choiceIndex) : "_";
/*  46 */       outcome = outcomeIndex < outcomes.size() ? (String)outcomes.get(outcomeIndex) : "_";
/*  47 */       if (outcome == null) {
/*  48 */         outcomeIndex++;
/*  49 */         labels.add("T!");
/*     */       } else {
/*  51 */         if ((choice.equals("?")) && (!outcome.equals("?"))) {
/*  52 */           labels.add("?(" + outcome + ")");
/*     */         } else {
/*  54 */           labels.add(choice);
/*     */         }
/*  56 */         choiceIndex++;
/*  57 */         outcomeIndex++;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  62 */     HashMap<Integer, BattleStats> battlesByFloor = new HashMap();
/*  63 */     for (String outcome = this.runData.damage_taken.iterator(); outcome.hasNext();) { battle = (BattleStats)outcome.next();
/*  64 */       battlesByFloor.put(Integer.valueOf(battle.floor), battle);
/*     */     }
/*     */     
/*     */ 
/*  68 */     HashMap<Integer, EventStats> eventsByFloor = new HashMap();
/*  69 */     for (BattleStats battle = this.runData.event_choices.iterator(); battle.hasNext();) { event = (EventStats)battle.next();
/*  70 */       eventsByFloor.put(Integer.valueOf(event.floor), event);
/*     */     }
/*     */     
/*     */ 
/*  74 */     HashMap<Integer, CardChoiceStats> cardsByFloor = new HashMap();
/*  75 */     for (EventStats event = this.runData.card_choices.iterator(); event.hasNext();) { choice = (CardChoiceStats)event.next();
/*     */       
/*  77 */       cardsByFloor.put(Integer.valueOf(choice.floor), choice);
/*     */     }
/*     */     
/*     */     CardChoiceStats choice;
/*  81 */     HashMap<Integer, List<String>> relicsByFloor = new HashMap();
/*  82 */     if (this.runData.relics_obtained != null) {
/*  83 */       for (choice = this.runData.relics_obtained.iterator(); choice.hasNext();) { relicData = (ObtainStats)choice.next();
/*  84 */         if (!relicsByFloor.containsKey(Integer.valueOf(relicData.floor))) {
/*  85 */           relicsByFloor.put(Integer.valueOf(relicData.floor), new ArrayList());
/*     */         }
/*  87 */         ((List)relicsByFloor.get(Integer.valueOf(relicData.floor))).add(relicData.key);
/*     */       }
/*     */     }
/*     */     
/*     */     ObtainStats relicData;
/*  92 */     HashMap<Integer, List<String>> potionsByFloor = new HashMap();
/*  93 */     if (this.runData.potions_obtained != null) {
/*  94 */       for (relicData = this.runData.potions_obtained.iterator(); relicData.hasNext();) { potionData = (ObtainStats)relicData.next();
/*  95 */         if (!potionsByFloor.containsKey(Integer.valueOf(potionData.floor))) {
/*  96 */           potionsByFloor.put(Integer.valueOf(potionData.floor), new ArrayList());
/*     */         }
/*  98 */         ((List)potionsByFloor.get(Integer.valueOf(potionData.floor))).add(potionData.key);
/*     */       }
/*     */     }
/*     */     
/*     */     ObtainStats potionData;
/* 103 */     HashMap<Integer, CampfireChoice> campfireChoicesByFloor = new HashMap();
/* 104 */     if (this.runData.campfire_choices != null) {
/* 105 */       for (CampfireChoice choice : this.runData.campfire_choices) {
/* 106 */         campfireChoicesByFloor.put(Integer.valueOf(choice.floor), choice);
/*     */       }
/*     */     }
/*     */     
/* 110 */     HashMap<Integer, ArrayList<String>> purchasesByFloor = new HashMap();
/* 111 */     if ((this.runData.items_purchased != null) && (this.runData.item_purchase_floors != null) && 
/* 112 */       (this.runData.items_purchased.size() == this.runData.item_purchase_floors.size())) {
/* 113 */       for (int i = 0; i < this.runData.items_purchased.size(); i++) {
/* 114 */         int floor = ((Integer)this.runData.item_purchase_floors.get(i)).intValue();
/* 115 */         String key = (String)this.runData.items_purchased.get(i);
/* 116 */         if (!purchasesByFloor.containsKey(Integer.valueOf(floor))) {
/* 117 */           purchasesByFloor.put(Integer.valueOf(floor), new ArrayList());
/*     */         }
/* 119 */         ((ArrayList)purchasesByFloor.get(Integer.valueOf(floor))).add(key);
/*     */       }
/*     */     }
/*     */     
/* 123 */     HashMap<Integer, ArrayList<String>> purgesByFloor = new HashMap();
/* 124 */     if ((this.runData.items_purged != null) && (this.runData.items_purged_floors != null) && 
/* 125 */       (this.runData.items_purged.size() == this.runData.items_purged_floors.size())) {
/* 126 */       for (int i = 0; i < this.runData.items_purged.size(); i++) {
/* 127 */         int floor = ((Integer)this.runData.items_purged_floors.get(i)).intValue();
/* 128 */         String key = (String)this.runData.items_purged.get(i);
/* 129 */         if (!purgesByFloor.containsKey(Integer.valueOf(floor))) {
/* 130 */           purgesByFloor.put(Integer.valueOf(floor), new ArrayList());
/*     */         }
/* 132 */         ((ArrayList)purgesByFloor.get(Integer.valueOf(floor))).add(key);
/*     */       }
/*     */     }
/*     */     
/* 136 */     this.pathElements.clear();
/*     */     
/* 138 */     int bossRelicChoiceIndex = 0;
/* 139 */     for (int i = 0; i < labels.size(); i++) {
/* 140 */       String label = (String)labels.get(i);
/* 141 */       int floor = i + 1;
/* 142 */       RunPathElement element = new RunPathElement(label, floor);
/*     */       
/* 144 */       if ((this.runData.current_hp_per_floor != null) && (this.runData.max_hp_per_floor != null) && 
/* 145 */         (i < this.runData.current_hp_per_floor.size()) && (i < this.runData.max_hp_per_floor.size())) {
/* 146 */         element.addHpData((Integer)this.runData.current_hp_per_floor.get(i), (Integer)this.runData.max_hp_per_floor.get(i));
/*     */       }
/* 148 */       if ((this.runData.gold_per_floor != null) && (i < this.runData.gold_per_floor.size())) {
/* 149 */         element.addGoldData((Integer)this.runData.gold_per_floor.get(i));
/*     */       }
/*     */       
/* 152 */       if (battlesByFloor.containsKey(Integer.valueOf(floor))) {
/* 153 */         element.addBattleData((BattleStats)battlesByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 155 */       if (eventsByFloor.containsKey(Integer.valueOf(floor))) {
/* 156 */         element.addEventData((EventStats)eventsByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 158 */       if (cardsByFloor.containsKey(Integer.valueOf(floor))) {
/* 159 */         element.addCardChoiceData((CardChoiceStats)cardsByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 161 */       if (relicsByFloor.containsKey(Integer.valueOf(floor))) {
/* 162 */         element.addRelicObtainStats((List)relicsByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 164 */       if (potionsByFloor.containsKey(Integer.valueOf(floor))) {
/* 165 */         element.addPotionObtainStats((List)potionsByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 167 */       if (campfireChoicesByFloor.containsKey(Integer.valueOf(floor))) {
/* 168 */         element.addCampfireChoiceData((CampfireChoice)campfireChoicesByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 170 */       if (purchasesByFloor.containsKey(Integer.valueOf(floor))) {
/* 171 */         element.addShopPurchaseData((ArrayList)purchasesByFloor.get(Integer.valueOf(floor)));
/*     */       }
/* 173 */       if (purgesByFloor.containsKey(Integer.valueOf(floor))) {
/* 174 */         element.addPurgeData((ArrayList)purgesByFloor.get(Integer.valueOf(floor)));
/*     */       }
/*     */       
/* 177 */       if ((label.equals("T!")) && (this.runData.boss_relics != null) && 
/* 178 */         (bossRelicChoiceIndex < this.runData.boss_relics.size())) {
/* 179 */         element.addRelicObtainStats(((BossRelicChoiceStats)this.runData.boss_relics.get(bossRelicChoiceIndex)).picked);
/* 180 */         bossRelicChoiceIndex++;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 186 */       this.pathElements.add(element);
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/* 191 */     boolean isHovered = false;
/* 192 */     for (RunPathElement element : this.pathElements) {
/* 193 */       element.update();
/* 194 */       if (element.isHovered()) {
/* 195 */         isHovered = true;
/*     */       }
/*     */     }
/* 198 */     if (isHovered) {
/* 199 */       com.megacrit.cardcrawl.core.CardCrawlGame.cursor.changeType(GameCursor.CursorType.INSPECT);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float x, float y) {
/* 204 */     float offsetX = x;
/* 205 */     float offsetY = y;
/*     */     
/* 207 */     int count = 0;
/* 208 */     for (RunPathElement element : this.pathElements) {
/* 209 */       element.position(offsetX, offsetY);
/* 210 */       count++;
/* 211 */       if (count % 25 == 0) {
/* 212 */         offsetY -= RunPathElement.getApproximateHeight();
/* 213 */         offsetX = x;
/*     */       } else {
/* 215 */         offsetX += RunPathElement.getApproximateWidth();
/*     */       }
/*     */     }
/*     */     
/* 219 */     for (RunPathElement element : this.pathElements) {
/* 220 */       element.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public float approximateHeight() {
/* 225 */     return (float)Math.ceil(this.pathElements.size() / 25.0F) * RunPathElement.getApproximateHeight();
/*     */   }
/*     */   
/*     */   public float approximateMaxWidth() {
/* 229 */     return 24.5F * RunPathElement.getApproximateWidth();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\runHistory\RunHistoryPath.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */