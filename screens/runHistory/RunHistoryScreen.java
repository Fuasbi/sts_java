/*      */ package com.megacrit.cardcrawl.screens.runHistory;
/*      */ 
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Input;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.google.gson.Gson;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*      */ import com.megacrit.cardcrawl.screens.SingleCardViewPopup;
/*      */ import com.megacrit.cardcrawl.screens.SingleRelicViewPopup;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*      */ import com.megacrit.cardcrawl.screens.options.DropdownMenu;
/*      */ import com.megacrit.cardcrawl.screens.stats.CharStat;
/*      */ import com.megacrit.cardcrawl.screens.stats.RunData;
/*      */ import java.io.PrintStream;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Arrays;
/*      */ import java.util.Hashtable;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class RunHistoryScreen implements com.megacrit.cardcrawl.screens.options.DropdownMenuListener
/*      */ {
/*   51 */   private static final Logger logger = LogManager.getLogger(RunHistoryScreen.class.getName());
/*   52 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("RunHistoryScreen");
/*   53 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   
/*      */ 
/*   56 */   private static final AbstractCard.CardRarity[] orderedRarity = { AbstractCard.CardRarity.SPECIAL, AbstractCard.CardRarity.RARE, AbstractCard.CardRarity.UNCOMMON, AbstractCard.CardRarity.COMMON, AbstractCard.CardRarity.BASIC, AbstractCard.CardRarity.CURSE };
/*      */   
/*      */ 
/*   59 */   private static final AbstractRelic.RelicTier[] orderedRelicRarity = { AbstractRelic.RelicTier.BOSS, AbstractRelic.RelicTier.SPECIAL, AbstractRelic.RelicTier.RARE, AbstractRelic.RelicTier.SHOP, AbstractRelic.RelicTier.UNCOMMON, AbstractRelic.RelicTier.COMMON, AbstractRelic.RelicTier.STARTER, AbstractRelic.RelicTier.DEPRECATED };
/*      */   
/*      */ 
/*      */ 
/*      */   private static final boolean SHOULD_SHOW_PATH = true;
/*      */   
/*      */ 
/*   66 */   private static final String IRONCLAD_NAME = TEXT[0];
/*   67 */   private static final String SILENT_NAME = TEXT[1];
/*   68 */   private static final String DEFECT_NAME = TEXT[2];
/*   69 */   private static final String ALL_CHARACTERS_TEXT = TEXT[23];
/*   70 */   private static final String WINS_AND_LOSSES_TEXT = TEXT[24];
/*   71 */   private static final String WINS_TEXT = TEXT[25];
/*   72 */   private static final String LOSSES_TEXT = TEXT[26];
/*      */   
/*   74 */   private static final String RUN_TYPE_ALL = TEXT[28];
/*   75 */   private static final String RUN_TYPE_NORMAL = TEXT[29];
/*   76 */   private static final String RUN_TYPE_ASCENSION = TEXT[30];
/*   77 */   private static final String RUN_TYPE_DAILY = TEXT[31];
/*      */   
/*   79 */   private static final String RARITY_LABEL_STARTER = TEXT[11];
/*   80 */   private static final String RARITY_LABEL_COMMON = TEXT[12];
/*   81 */   private static final String RARITY_LABEL_UNCOMMON = TEXT[13];
/*   82 */   private static final String RARITY_LABEL_RARE = TEXT[14];
/*   83 */   private static final String RARITY_LABEL_SPECIAL = TEXT[15];
/*   84 */   private static final String RARITY_LABEL_CURSE = TEXT[16];
/*   85 */   private static final String RARITY_LABEL_BOSS = TEXT[17];
/*   86 */   private static final String RARITY_LABEL_SHOP = TEXT[18];
/*   87 */   private static final String RARITY_LABEL_UNKNOWN = TEXT[19];
/*   88 */   private static final String COUNT_WITH_LABEL = TEXT[20];
/*   89 */   private static final String LABEL_WITH_COUNT_IN_PARENS = TEXT[21];
/*   90 */   private static final String SEED_LABEL = TEXT[32];
/*   91 */   private static final String CUSTOM_SEED_LABEL = TEXT[33];
/*      */   
/*   93 */   public MenuCancelButton button = new MenuCancelButton();
/*   94 */   private static Gson gson = new Gson();
/*   95 */   private ArrayList<RunData> unfilteredRuns = new ArrayList();
/*   96 */   private ArrayList<RunData> filteredRuns = new ArrayList();
/*   97 */   private int runIndex = 0;
/*   98 */   private RunData viewedRun = null;
/*      */   
/*      */ 
/*  101 */   public boolean screenUp = false;
/*  102 */   public AbstractPlayer.PlayerClass currentChar = null;
/*  103 */   private static final float SHOW_X = 300.0F * Settings.scale; private static final float HIDE_X = -800.0F * Settings.scale;
/*  104 */   private float screenX = HIDE_X; private float targetX = HIDE_X;
/*      */   
/*      */   private RunHistoryPath runPath;
/*      */   
/*      */   private ModIcons modIcons;
/*      */   
/*      */   private CopyableTextElement seedElement;
/*      */   private CopyableTextElement secondSeedElement;
/*  112 */   private boolean grabbedScreen = false;
/*  113 */   private float grabStartY = 0.0F; private float scrollTargetY = 0.0F; private float scrollY = 0.0F;
/*  114 */   private float scrollLowerBound = 0.0F;
/*  115 */   private float scrollUpperBound = 0.0F;
/*      */   
/*      */   private DropdownMenu characterFilter;
/*      */   
/*      */   private DropdownMenu winLossFilter;
/*      */   
/*      */   private DropdownMenu runTypeFilter;
/*      */   
/*      */   private Hitbox prevHb;
/*      */   
/*      */   private Hitbox nextHb;
/*  126 */   private ArrayList<AbstractRelic> relics = new ArrayList();
/*  127 */   private ArrayList<TinyCard> cards = new ArrayList();
/*  128 */   private String cardCountByRarityString = "";
/*  129 */   private String relicCountByRarityString = "";
/*  130 */   private int circletCount = 0;
/*      */   
/*      */   private DropdownMenu runsDropdown;
/*      */   
/*      */   private static final float ARROW_SIDE_PADDING = 180.0F;
/*      */   
/*      */   private Hitbox currentHb;
/*      */   
/*      */   public RunHistoryScreen()
/*      */   {
/*  140 */     this.runPath = new RunHistoryPath();
/*  141 */     this.modIcons = new ModIcons();
/*  142 */     this.seedElement = new CopyableTextElement(FontHelper.cardDescFont_N);
/*  143 */     this.secondSeedElement = new CopyableTextElement(FontHelper.cardDescFont_N);
/*  144 */     this.prevHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  145 */     this.prevHb.move(180.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*  146 */     this.nextHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  147 */     this.nextHb.move(Settings.WIDTH - 180.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*      */   }
/*      */   
/*      */   /* Error */
/*      */   public void refreshData()
/*      */   {
/*      */     // Byte code:
/*      */     //   0: getstatic 52	com/badlogic/gdx/Gdx:files	Lcom/badlogic/gdx/Files;
/*      */     //   3: new 53	java/lang/StringBuilder
/*      */     //   6: dup
/*      */     //   7: invokespecial 54	java/lang/StringBuilder:<init>	()V
/*      */     //   10: ldc 55
/*      */     //   12: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   15: getstatic 57	java/io/File:separator	Ljava/lang/String;
/*      */     //   18: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   21: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*      */     //   24: invokeinterface 59 2 0
/*      */     //   29: invokevirtual 60	com/badlogic/gdx/files/FileHandle:list	()[Lcom/badlogic/gdx/files/FileHandle;
/*      */     //   32: astore_1
/*      */     //   33: aload_0
/*      */     //   34: getfield 8	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:unfilteredRuns	Ljava/util/ArrayList;
/*      */     //   37: invokevirtual 61	java/util/ArrayList:clear	()V
/*      */     //   40: aload_1
/*      */     //   41: astore_2
/*      */     //   42: aload_2
/*      */     //   43: arraylength
/*      */     //   44: istore_3
/*      */     //   45: iconst_0
/*      */     //   46: istore 4
/*      */     //   48: iload 4
/*      */     //   50: iload_3
/*      */     //   51: if_icmpge +402 -> 453
/*      */     //   54: aload_2
/*      */     //   55: iload 4
/*      */     //   57: aaload
/*      */     //   58: astore 5
/*      */     //   60: getstatic 62	com/megacrit/cardcrawl/core/CardCrawlGame:saveSlot	I
/*      */     //   63: lookupswitch	default:+59->122, 0:+17->80
/*      */     //   80: aload 5
/*      */     //   82: invokevirtual 63	com/badlogic/gdx/files/FileHandle:name	()Ljava/lang/String;
/*      */     //   85: ldc 64
/*      */     //   87: invokevirtual 65	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
/*      */     //   90: ifne +357 -> 447
/*      */     //   93: aload 5
/*      */     //   95: invokevirtual 63	com/badlogic/gdx/files/FileHandle:name	()Ljava/lang/String;
/*      */     //   98: ldc 66
/*      */     //   100: invokevirtual 65	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
/*      */     //   103: ifne +344 -> 447
/*      */     //   106: aload 5
/*      */     //   108: invokevirtual 63	com/badlogic/gdx/files/FileHandle:name	()Ljava/lang/String;
/*      */     //   111: ldc 67
/*      */     //   113: invokevirtual 65	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
/*      */     //   116: ifeq +41 -> 157
/*      */     //   119: goto +328 -> 447
/*      */     //   122: aload 5
/*      */     //   124: invokevirtual 63	com/badlogic/gdx/files/FileHandle:name	()Ljava/lang/String;
/*      */     //   127: new 53	java/lang/StringBuilder
/*      */     //   130: dup
/*      */     //   131: invokespecial 54	java/lang/StringBuilder:<init>	()V
/*      */     //   134: getstatic 62	com/megacrit/cardcrawl/core/CardCrawlGame:saveSlot	I
/*      */     //   137: invokevirtual 68	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
/*      */     //   140: ldc 69
/*      */     //   142: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   145: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*      */     //   148: invokevirtual 65	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
/*      */     //   151: ifne +6 -> 157
/*      */     //   154: goto +293 -> 447
/*      */     //   157: aload 5
/*      */     //   159: invokevirtual 60	com/badlogic/gdx/files/FileHandle:list	()[Lcom/badlogic/gdx/files/FileHandle;
/*      */     //   162: astore 6
/*      */     //   164: aload 6
/*      */     //   166: arraylength
/*      */     //   167: istore 7
/*      */     //   169: iconst_0
/*      */     //   170: istore 8
/*      */     //   172: iload 8
/*      */     //   174: iload 7
/*      */     //   176: if_icmpge +271 -> 447
/*      */     //   179: aload 6
/*      */     //   181: iload 8
/*      */     //   183: aaload
/*      */     //   184: astore 9
/*      */     //   186: getstatic 70	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:gson	Lcom/google/gson/Gson;
/*      */     //   189: aload 9
/*      */     //   191: invokevirtual 71	com/badlogic/gdx/files/FileHandle:readString	()Ljava/lang/String;
/*      */     //   194: ldc 72
/*      */     //   196: invokevirtual 73	com/google/gson/Gson:fromJson	(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/*      */     //   199: checkcast 72	com/megacrit/cardcrawl/screens/stats/RunData
/*      */     //   202: astore 10
/*      */     //   204: aload 10
/*      */     //   206: ifnull +126 -> 332
/*      */     //   209: aload 10
/*      */     //   211: getfield 74	com/megacrit/cardcrawl/screens/stats/RunData:timestamp	Ljava/lang/String;
/*      */     //   214: ifnonnull +118 -> 332
/*      */     //   217: aload 10
/*      */     //   219: aload 9
/*      */     //   221: invokevirtual 75	com/badlogic/gdx/files/FileHandle:nameWithoutExtension	()Ljava/lang/String;
/*      */     //   224: putfield 74	com/megacrit/cardcrawl/screens/stats/RunData:timestamp	Ljava/lang/String;
/*      */     //   227: ldc 76
/*      */     //   229: astore 11
/*      */     //   231: aload 10
/*      */     //   233: getfield 74	com/megacrit/cardcrawl/screens/stats/RunData:timestamp	Ljava/lang/String;
/*      */     //   236: invokevirtual 77	java/lang/String:length	()I
/*      */     //   239: aload 11
/*      */     //   241: invokevirtual 77	java/lang/String:length	()I
/*      */     //   244: if_icmpne +7 -> 251
/*      */     //   247: iconst_1
/*      */     //   248: goto +4 -> 252
/*      */     //   251: iconst_0
/*      */     //   252: istore 12
/*      */     //   254: iload 12
/*      */     //   256: ifeq +76 -> 332
/*      */     //   259: ldc2_w 78
/*      */     //   262: lstore 13
/*      */     //   264: aload 10
/*      */     //   266: getfield 74	com/megacrit/cardcrawl/screens/stats/RunData:timestamp	Ljava/lang/String;
/*      */     //   269: invokestatic 80	java/lang/Long:parseLong	(Ljava/lang/String;)J
/*      */     //   272: lstore 15
/*      */     //   274: aload 10
/*      */     //   276: lload 15
/*      */     //   278: ldc2_w 78
/*      */     //   281: lmul
/*      */     //   282: invokestatic 81	java/lang/Long:toString	(J)Ljava/lang/String;
/*      */     //   285: putfield 74	com/megacrit/cardcrawl/screens/stats/RunData:timestamp	Ljava/lang/String;
/*      */     //   288: goto +44 -> 332
/*      */     //   291: astore 13
/*      */     //   293: getstatic 83	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:logger	Lorg/apache/logging/log4j/Logger;
/*      */     //   296: new 53	java/lang/StringBuilder
/*      */     //   299: dup
/*      */     //   300: invokespecial 54	java/lang/StringBuilder:<init>	()V
/*      */     //   303: ldc 84
/*      */     //   305: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   308: aload 9
/*      */     //   310: invokevirtual 85	com/badlogic/gdx/files/FileHandle:path	()Ljava/lang/String;
/*      */     //   313: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   316: ldc 86
/*      */     //   318: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   321: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*      */     //   324: invokeinterface 87 2 0
/*      */     //   329: aconst_null
/*      */     //   330: astore 10
/*      */     //   332: aload 10
/*      */     //   334: ifnull +71 -> 405
/*      */     //   337: aload 10
/*      */     //   339: getfield 88	com/megacrit/cardcrawl/screens/stats/RunData:character_chosen	Ljava/lang/String;
/*      */     //   342: invokestatic 89	com/megacrit/cardcrawl/characters/AbstractPlayer$PlayerClass:valueOf	(Ljava/lang/String;)Lcom/megacrit/cardcrawl/characters/AbstractPlayer$PlayerClass;
/*      */     //   345: pop
/*      */     //   346: aload_0
/*      */     //   347: getfield 8	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:unfilteredRuns	Ljava/util/ArrayList;
/*      */     //   350: aload 10
/*      */     //   352: invokevirtual 90	java/util/ArrayList:add	(Ljava/lang/Object;)Z
/*      */     //   355: pop
/*      */     //   356: goto +49 -> 405
/*      */     //   359: astore 11
/*      */     //   361: getstatic 83	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:logger	Lorg/apache/logging/log4j/Logger;
/*      */     //   364: new 53	java/lang/StringBuilder
/*      */     //   367: dup
/*      */     //   368: invokespecial 54	java/lang/StringBuilder:<init>	()V
/*      */     //   371: ldc 84
/*      */     //   373: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   376: aload 9
/*      */     //   378: invokevirtual 85	com/badlogic/gdx/files/FileHandle:path	()Ljava/lang/String;
/*      */     //   381: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   384: ldc 93
/*      */     //   386: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   389: aload 10
/*      */     //   391: getfield 88	com/megacrit/cardcrawl/screens/stats/RunData:character_chosen	Ljava/lang/String;
/*      */     //   394: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   397: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*      */     //   400: invokeinterface 87 2 0
/*      */     //   405: goto +36 -> 441
/*      */     //   408: astore 10
/*      */     //   410: getstatic 83	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:logger	Lorg/apache/logging/log4j/Logger;
/*      */     //   413: new 53	java/lang/StringBuilder
/*      */     //   416: dup
/*      */     //   417: invokespecial 54	java/lang/StringBuilder:<init>	()V
/*      */     //   420: ldc 95
/*      */     //   422: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   425: aload 9
/*      */     //   427: invokevirtual 85	com/badlogic/gdx/files/FileHandle:path	()Ljava/lang/String;
/*      */     //   430: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*      */     //   433: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*      */     //   436: invokeinterface 87 2 0
/*      */     //   441: iinc 8 1
/*      */     //   444: goto -272 -> 172
/*      */     //   447: iinc 4 1
/*      */     //   450: goto -402 -> 48
/*      */     //   453: aload_0
/*      */     //   454: getfield 8	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:unfilteredRuns	Ljava/util/ArrayList;
/*      */     //   457: invokevirtual 96	java/util/ArrayList:size	()I
/*      */     //   460: ifle +28 -> 488
/*      */     //   463: aload_0
/*      */     //   464: getfield 8	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:unfilteredRuns	Ljava/util/ArrayList;
/*      */     //   467: getstatic 97	com/megacrit/cardcrawl/screens/stats/RunData:orderByTimestampDesc	Ljava/util/Comparator;
/*      */     //   470: invokevirtual 98	java/util/ArrayList:sort	(Ljava/util/Comparator;)V
/*      */     //   473: aload_0
/*      */     //   474: aload_0
/*      */     //   475: getfield 8	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:unfilteredRuns	Ljava/util/ArrayList;
/*      */     //   478: iconst_0
/*      */     //   479: invokevirtual 99	java/util/ArrayList:get	(I)Ljava/lang/Object;
/*      */     //   482: checkcast 72	com/megacrit/cardcrawl/screens/stats/RunData
/*      */     //   485: putfield 11	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:viewedRun	Lcom/megacrit/cardcrawl/screens/stats/RunData;
/*      */     //   488: iconst_4
/*      */     //   489: anewarray 100	java/lang/String
/*      */     //   492: dup
/*      */     //   493: iconst_0
/*      */     //   494: getstatic 101	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:ALL_CHARACTERS_TEXT	Ljava/lang/String;
/*      */     //   497: aastore
/*      */     //   498: dup
/*      */     //   499: iconst_1
/*      */     //   500: getstatic 102	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:IRONCLAD_NAME	Ljava/lang/String;
/*      */     //   503: aastore
/*      */     //   504: dup
/*      */     //   505: iconst_2
/*      */     //   506: getstatic 103	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:SILENT_NAME	Ljava/lang/String;
/*      */     //   509: aastore
/*      */     //   510: dup
/*      */     //   511: iconst_3
/*      */     //   512: getstatic 104	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:DEFECT_NAME	Ljava/lang/String;
/*      */     //   515: aastore
/*      */     //   516: astore_2
/*      */     //   517: aload_0
/*      */     //   518: new 105	com/megacrit/cardcrawl/screens/options/DropdownMenu
/*      */     //   521: dup
/*      */     //   522: aload_0
/*      */     //   523: aload_2
/*      */     //   524: getstatic 38	com/megacrit/cardcrawl/helpers/FontHelper:cardDescFont_N	Lcom/badlogic/gdx/graphics/g2d/BitmapFont;
/*      */     //   527: getstatic 106	com/megacrit/cardcrawl/core/Settings:CREAM_COLOR	Lcom/badlogic/gdx/graphics/Color;
/*      */     //   530: invokespecial 107	com/megacrit/cardcrawl/screens/options/DropdownMenu:<init>	(Lcom/megacrit/cardcrawl/screens/options/DropdownMenuListener;[Ljava/lang/String;Lcom/badlogic/gdx/graphics/g2d/BitmapFont;Lcom/badlogic/gdx/graphics/Color;)V
/*      */     //   533: putfield 108	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:characterFilter	Lcom/megacrit/cardcrawl/screens/options/DropdownMenu;
/*      */     //   536: iconst_3
/*      */     //   537: anewarray 100	java/lang/String
/*      */     //   540: dup
/*      */     //   541: iconst_0
/*      */     //   542: getstatic 109	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:WINS_AND_LOSSES_TEXT	Ljava/lang/String;
/*      */     //   545: aastore
/*      */     //   546: dup
/*      */     //   547: iconst_1
/*      */     //   548: getstatic 110	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:WINS_TEXT	Ljava/lang/String;
/*      */     //   551: aastore
/*      */     //   552: dup
/*      */     //   553: iconst_2
/*      */     //   554: getstatic 111	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:LOSSES_TEXT	Ljava/lang/String;
/*      */     //   557: aastore
/*      */     //   558: astore_3
/*      */     //   559: aload_0
/*      */     //   560: new 105	com/megacrit/cardcrawl/screens/options/DropdownMenu
/*      */     //   563: dup
/*      */     //   564: aload_0
/*      */     //   565: aload_3
/*      */     //   566: getstatic 38	com/megacrit/cardcrawl/helpers/FontHelper:cardDescFont_N	Lcom/badlogic/gdx/graphics/g2d/BitmapFont;
/*      */     //   569: getstatic 106	com/megacrit/cardcrawl/core/Settings:CREAM_COLOR	Lcom/badlogic/gdx/graphics/Color;
/*      */     //   572: invokespecial 107	com/megacrit/cardcrawl/screens/options/DropdownMenu:<init>	(Lcom/megacrit/cardcrawl/screens/options/DropdownMenuListener;[Ljava/lang/String;Lcom/badlogic/gdx/graphics/g2d/BitmapFont;Lcom/badlogic/gdx/graphics/Color;)V
/*      */     //   575: putfield 112	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:winLossFilter	Lcom/megacrit/cardcrawl/screens/options/DropdownMenu;
/*      */     //   578: iconst_4
/*      */     //   579: anewarray 100	java/lang/String
/*      */     //   582: dup
/*      */     //   583: iconst_0
/*      */     //   584: getstatic 113	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:RUN_TYPE_ALL	Ljava/lang/String;
/*      */     //   587: aastore
/*      */     //   588: dup
/*      */     //   589: iconst_1
/*      */     //   590: getstatic 114	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:RUN_TYPE_NORMAL	Ljava/lang/String;
/*      */     //   593: aastore
/*      */     //   594: dup
/*      */     //   595: iconst_2
/*      */     //   596: getstatic 115	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:RUN_TYPE_ASCENSION	Ljava/lang/String;
/*      */     //   599: aastore
/*      */     //   600: dup
/*      */     //   601: iconst_3
/*      */     //   602: getstatic 116	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:RUN_TYPE_DAILY	Ljava/lang/String;
/*      */     //   605: aastore
/*      */     //   606: astore 4
/*      */     //   608: aload_0
/*      */     //   609: new 105	com/megacrit/cardcrawl/screens/options/DropdownMenu
/*      */     //   612: dup
/*      */     //   613: aload_0
/*      */     //   614: aload 4
/*      */     //   616: getstatic 38	com/megacrit/cardcrawl/helpers/FontHelper:cardDescFont_N	Lcom/badlogic/gdx/graphics/g2d/BitmapFont;
/*      */     //   619: getstatic 106	com/megacrit/cardcrawl/core/Settings:CREAM_COLOR	Lcom/badlogic/gdx/graphics/Color;
/*      */     //   622: invokespecial 107	com/megacrit/cardcrawl/screens/options/DropdownMenu:<init>	(Lcom/megacrit/cardcrawl/screens/options/DropdownMenuListener;[Ljava/lang/String;Lcom/badlogic/gdx/graphics/g2d/BitmapFont;Lcom/badlogic/gdx/graphics/Color;)V
/*      */     //   625: putfield 117	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:runTypeFilter	Lcom/megacrit/cardcrawl/screens/options/DropdownMenu;
/*      */     //   628: aload_0
/*      */     //   629: invokespecial 118	com/megacrit/cardcrawl/screens/runHistory/RunHistoryScreen:resetRunsDropdown	()V
/*      */     //   632: return
/*      */     // Line number table:
/*      */     //   Java source line #151	-> byte code offset #0
/*      */     //   Java source line #152	-> byte code offset #33
/*      */     //   Java source line #154	-> byte code offset #40
/*      */     //   Java source line #155	-> byte code offset #60
/*      */     //   Java source line #157	-> byte code offset #80
/*      */     //   Java source line #159	-> byte code offset #119
/*      */     //   Java source line #163	-> byte code offset #122
/*      */     //   Java source line #164	-> byte code offset #154
/*      */     //   Java source line #169	-> byte code offset #157
/*      */     //   Java source line #171	-> byte code offset #186
/*      */     //   Java source line #172	-> byte code offset #204
/*      */     //   Java source line #175	-> byte code offset #217
/*      */     //   Java source line #178	-> byte code offset #227
/*      */     //   Java source line #179	-> byte code offset #231
/*      */     //   Java source line #180	-> byte code offset #254
/*      */     //   Java source line #182	-> byte code offset #259
/*      */     //   Java source line #183	-> byte code offset #264
/*      */     //   Java source line #184	-> byte code offset #274
/*      */     //   Java source line #189	-> byte code offset #288
/*      */     //   Java source line #185	-> byte code offset #291
/*      */     //   Java source line #186	-> byte code offset #293
/*      */     //   Java source line #187	-> byte code offset #310
/*      */     //   Java source line #186	-> byte code offset #324
/*      */     //   Java source line #188	-> byte code offset #329
/*      */     //   Java source line #193	-> byte code offset #332
/*      */     //   Java source line #195	-> byte code offset #337
/*      */     //   Java source line #196	-> byte code offset #346
/*      */     //   Java source line #200	-> byte code offset #356
/*      */     //   Java source line #197	-> byte code offset #359
/*      */     //   Java source line #198	-> byte code offset #361
/*      */     //   Java source line #199	-> byte code offset #378
/*      */     //   Java source line #198	-> byte code offset #400
/*      */     //   Java source line #204	-> byte code offset #405
/*      */     //   Java source line #202	-> byte code offset #408
/*      */     //   Java source line #203	-> byte code offset #410
/*      */     //   Java source line #169	-> byte code offset #441
/*      */     //   Java source line #154	-> byte code offset #447
/*      */     //   Java source line #208	-> byte code offset #453
/*      */     //   Java source line #209	-> byte code offset #463
/*      */     //   Java source line #210	-> byte code offset #473
/*      */     //   Java source line #213	-> byte code offset #488
/*      */     //   Java source line #214	-> byte code offset #517
/*      */     //   Java source line #216	-> byte code offset #536
/*      */     //   Java source line #217	-> byte code offset #559
/*      */     //   Java source line #219	-> byte code offset #578
/*      */     //   Java source line #221	-> byte code offset #608
/*      */     //   Java source line #223	-> byte code offset #628
/*      */     //   Java source line #224	-> byte code offset #632
/*      */     // Local variable table:
/*      */     //   start	length	slot	name	signature
/*      */     //   0	633	0	this	RunHistoryScreen
/*      */     //   32	9	1	subfolders	com.badlogic.gdx.files.FileHandle[]
/*      */     //   41	14	2	arrayOfFileHandle1	com.badlogic.gdx.files.FileHandle[]
/*      */     //   516	8	2	charFilterOptions	String[]
/*      */     //   44	8	3	i	int
/*      */     //   558	8	3	winLossFilterOptions	String[]
/*      */     //   46	402	4	j	int
/*      */     //   606	9	4	runTypeFilterOptions	String[]
/*      */     //   58	100	5	subFolder	com.badlogic.gdx.files.FileHandle
/*      */     //   162	18	6	arrayOfFileHandle2	com.badlogic.gdx.files.FileHandle[]
/*      */     //   167	10	7	k	int
/*      */     //   170	272	8	m	int
/*      */     //   184	242	9	file	com.badlogic.gdx.files.FileHandle
/*      */     //   202	188	10	data	RunData
/*      */     //   408	3	10	ex	com.google.gson.JsonSyntaxException
/*      */     //   229	11	11	exampleDaysSinceUnixStr	String
/*      */     //   359	3	11	ex	RuntimeException
/*      */     //   252	3	12	assumeDaysSinceUnix	boolean
/*      */     //   262	3	13	secondsInDay	long
/*      */     //   291	3	13	ex	NumberFormatException
/*      */     //   272	5	15	days	long
/*      */     // Exception table:
/*      */     //   from	to	target	type
/*      */     //   259	288	291	java/lang/NumberFormatException
/*      */     //   337	356	359	java/lang/IllegalArgumentException
/*      */     //   337	356	359	java/lang/NullPointerException
/*      */     //   186	405	408	com/google/gson/JsonSyntaxException
/*      */   }
/*      */   
/*      */   private void resetRunsDropdown()
/*      */   {
/*  227 */     this.filteredRuns.clear();
/*      */     
/*  229 */     boolean only_wins = this.winLossFilter.getSelectedIndex() == 1;
/*  230 */     boolean only_losses = this.winLossFilter.getSelectedIndex() == 2;
/*      */     
/*  232 */     boolean only_ironclad = this.characterFilter.getSelectedIndex() == 1;
/*  233 */     boolean only_silent = this.characterFilter.getSelectedIndex() == 2;
/*  234 */     boolean only_defect = this.characterFilter.getSelectedIndex() == 3;
/*      */     
/*  236 */     boolean only_normal = this.runTypeFilter.getSelectedIndex() == 1;
/*  237 */     boolean only_ascension = this.runTypeFilter.getSelectedIndex() == 2;
/*  238 */     boolean only_daily = this.runTypeFilter.getSelectedIndex() == 3;
/*      */     
/*  240 */     for (RunData data : this.unfilteredRuns) {
/*  241 */       includeMe = true;
/*      */       
/*      */ 
/*  244 */       if (only_wins) {
/*  245 */         includeMe = (includeMe) && (data.victory);
/*  246 */       } else if (only_losses) {
/*  247 */         includeMe = (includeMe) && (!data.victory);
/*      */       }
/*      */       
/*      */ 
/*  251 */       String runCharacter = data.character_chosen;
/*  252 */       if (only_ironclad) {
/*  253 */         includeMe = (includeMe) && (runCharacter.equals(AbstractPlayer.PlayerClass.IRONCLAD.name()));
/*  254 */       } else if (only_silent) {
/*  255 */         includeMe = (includeMe) && (runCharacter.equals(AbstractPlayer.PlayerClass.THE_SILENT.name()));
/*  256 */       } else if (only_defect) {
/*  257 */         includeMe = (includeMe) && (runCharacter.equals(AbstractPlayer.PlayerClass.DEFECT.name()));
/*      */       }
/*      */       
/*      */ 
/*  261 */       if (only_normal) {
/*  262 */         includeMe = (includeMe) && (!data.is_ascension_mode) && (!data.is_daily);
/*  263 */       } else if (only_ascension) {
/*  264 */         includeMe = (includeMe) && (data.is_ascension_mode);
/*  265 */       } else if (only_daily) {
/*  266 */         includeMe = (includeMe) && (data.is_daily);
/*      */       }
/*      */       
/*  269 */       if (includeMe) {
/*  270 */         this.filteredRuns.add(data);
/*      */       }
/*      */     }
/*      */     
/*      */     boolean includeMe;
/*  275 */     Object options = new ArrayList();
/*  276 */     SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy, h:mm a");
/*  277 */     for (RunData run : this.filteredRuns) {
/*      */       try {
/*      */         String dateTimeStr;
/*  280 */         if (run.local_time != null) {
/*  281 */           dateTimeStr = dateFormat.format(com.megacrit.cardcrawl.metrics.Metrics.timestampFormatter.parse(run.local_time));
/*      */         } else {
/*  283 */           dateTimeStr = dateFormat.format(Long.valueOf(Long.valueOf(run.timestamp).longValue() * 1000L));
/*      */         }
/*  285 */         String dateTimeStr = dateTimeStr + "  - " + run.score;
/*  286 */         ((ArrayList)options).add(dateTimeStr);
/*      */       } catch (Exception e) {
/*  288 */         logger.info(e.getMessage());
/*      */       }
/*      */     }
/*  291 */     this.runsDropdown = new DropdownMenu(this, (ArrayList)options, FontHelper.eventBodyText, Settings.CREAM_COLOR);
/*  292 */     this.runIndex = 0;
/*      */     
/*  294 */     if (this.filteredRuns.size() > 0) {
/*  295 */       reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */     } else {
/*  297 */       this.viewedRun = null;
/*  298 */       reloadWithRunData(null);
/*      */     }
/*      */   }
/*      */   
/*      */   public String baseCardSuffixForCharacter(String character) {
/*  303 */     switch (AbstractPlayer.PlayerClass.valueOf(character)) {
/*      */     case IRONCLAD: 
/*  305 */       return "_R";
/*      */     case THE_SILENT: 
/*  307 */       return "_G";
/*      */     case DEFECT: 
/*  309 */       return "_B";
/*      */     }
/*  311 */     return "";
/*      */   }
/*      */   
/*      */   public void reloadWithRunData(RunData runData) {
/*  315 */     if (runData == null) {
/*  316 */       logger.info("Attempted to load Run History with 0 runs.");
/*  317 */       return;
/*      */     }
/*  319 */     this.scrollUpperBound = 0.0F;
/*  320 */     this.viewedRun = runData;
/*      */     
/*  322 */     reloadRelics(runData);
/*  323 */     reloadCards(runData);
/*  324 */     this.runPath.setRunData(runData);
/*      */     
/*  326 */     this.modIcons.setRunData(runData);
/*      */     try
/*      */     {
/*  329 */       if ((this.viewedRun.special_seed == null) || (this.viewedRun.special_seed.longValue() == 0L) || (this.viewedRun.is_daily)) {
/*  330 */         String seedFormat = this.viewedRun.chose_seed ? CUSTOM_SEED_LABEL : SEED_LABEL;
/*  331 */         String seedText = SeedHelper.getString(Long.parseLong(runData.seed_played));
/*  332 */         this.seedElement.setText(String.format(seedFormat, new Object[] { seedText }), seedText);
/*  333 */         this.secondSeedElement.setText("", "");
/*      */       } else {
/*  335 */         String seedText = SeedHelper.getString(runData.special_seed.longValue());
/*  336 */         this.seedElement.setText(String.format(CUSTOM_SEED_LABEL, new Object[] { seedText }), seedText);
/*  337 */         String secondSeedText = SeedHelper.getString(Long.parseLong(runData.seed_played));
/*  338 */         this.secondSeedElement.setText(String.format(SEED_LABEL, new Object[] { secondSeedText }), secondSeedText);
/*      */       }
/*      */     } catch (NumberFormatException ex) {
/*  341 */       this.seedElement.setText("", "");
/*  342 */       this.secondSeedElement.setText("", "");
/*      */     }
/*  344 */     this.scrollTargetY = 0.0F;
/*  345 */     resetScrolling();
/*  346 */     if (this.runsDropdown != null) {
/*  347 */       this.runsDropdown.setSelectedIndex(this.filteredRuns.indexOf(runData));
/*      */     }
/*      */   }
/*      */   
/*      */   private void reloadRelics(RunData runData) {
/*  352 */     this.relics.clear();
/*  353 */     this.circletCount = runData.circlet_count;
/*  354 */     boolean circletCountSet = this.circletCount > 0;
/*      */     
/*  356 */     Hashtable<AbstractRelic.RelicTier, Integer> relicRarityCounts = new Hashtable();
/*      */     
/*  358 */     AbstractRelic circlet = null;
/*  359 */     for (Iterator localIterator = runData.relics.iterator(); localIterator.hasNext();) { relicName = (String)localIterator.next();
/*      */       try {
/*  361 */         AbstractRelic relic = RelicLibrary.getRelic(relicName).makeCopy();
/*  362 */         relic.isSeen = true;
/*  363 */         if ((relic instanceof com.megacrit.cardcrawl.relics.Circlet)) {
/*  364 */           if (relicName.equals("Circlet")) {
/*  365 */             if (!circletCountSet) {
/*  366 */               this.circletCount += 1;
/*      */             }
/*  368 */             if (circlet == null) {
/*  369 */               circlet = relic;
/*  370 */               this.relics.add(relic);
/*      */             }
/*      */           } else {
/*  373 */             logger.info("Could not find relic for: " + relicName);
/*      */           }
/*      */         } else {
/*  376 */           this.relics.add(relic);
/*      */         }
/*  378 */         newCount = relicRarityCounts.containsKey(relic.tier) ? ((Integer)relicRarityCounts.get(relic.tier)).intValue() + 1 : 1;
/*  379 */         relicRarityCounts.put(relic.tier, Integer.valueOf(newCount));
/*      */       } catch (NullPointerException ex) {
/*  381 */         logger.info("NPE while loading: " + relicName);
/*      */       } }
/*      */     String relicName;
/*      */     int newCount;
/*  385 */     if ((circlet != null) && (this.circletCount > 1)) {
/*  386 */       circlet.setCounter(this.circletCount);
/*      */     }
/*      */     
/*  389 */     StringBuilder bldr = new StringBuilder();
/*  390 */     for (AbstractRelic.RelicTier rarity : orderedRelicRarity) {
/*  391 */       if (relicRarityCounts.containsKey(rarity)) {
/*  392 */         if (bldr.length() > 0) {
/*  393 */           bldr.append(", ");
/*      */         }
/*  395 */         bldr.append(String.format(COUNT_WITH_LABEL, new Object[] { relicRarityCounts.get(rarity), rarityLabel(rarity) }));
/*      */       }
/*      */     }
/*  398 */     this.relicCountByRarityString = bldr.toString();
/*      */   }
/*      */   
/*      */   private void reloadCards(RunData runData)
/*      */   {
/*  403 */     Hashtable<String, AbstractCard> rawNameToCards = new Hashtable();
/*  404 */     Hashtable<AbstractCard, Integer> cardCounts = new Hashtable();
/*  405 */     Hashtable<AbstractCard.CardRarity, Integer> cardRarityCounts = new Hashtable();
/*  406 */     CardGroup sortedMasterDeck = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*      */     
/*      */ 
/*  409 */     for (String cardID : runData.master_deck) {
/*      */       AbstractCard card;
/*  411 */       if (rawNameToCards.containsKey(cardID)) {
/*  412 */         card = (AbstractCard)rawNameToCards.get(cardID);
/*      */       } else {
/*  414 */         card = cardForName(runData, cardID);
/*      */       }
/*  416 */       if (card != null) {
/*  417 */         value = cardCounts.containsKey(card) ? ((Integer)cardCounts.get(card)).intValue() + 1 : 1;
/*  418 */         cardCounts.put(card, Integer.valueOf(value));
/*  419 */         rawNameToCards.put(cardID, card);
/*      */         
/*  421 */         int rarityCount = cardRarityCounts.containsKey(card.rarity) ? ((Integer)cardRarityCounts.get(card.rarity)).intValue() + 1 : 1;
/*  422 */         cardRarityCounts.put(card.rarity, Integer.valueOf(rarityCount));
/*      */       }
/*      */     }
/*      */     AbstractCard card;
/*      */     int value;
/*  427 */     sortedMasterDeck.clear();
/*  428 */     for (AbstractCard card : rawNameToCards.values()) {
/*  429 */       sortedMasterDeck.addToTop(card);
/*      */     }
/*      */     
/*  432 */     sortedMasterDeck.sortAlphabetically(true);
/*  433 */     sortedMasterDeck.sortByRarityPlusStatusCardType(false);
/*  434 */     sortedMasterDeck = sortedMasterDeck.getGroupedByColor();
/*      */     
/*  436 */     this.cards.clear();
/*      */     
/*  438 */     for (??? = sortedMasterDeck.group.iterator(); ???.hasNext();) { card = (AbstractCard)???.next();
/*  439 */       this.cards.add(new TinyCard(card, ((Integer)cardCounts.get(card)).intValue()));
/*      */     }
/*      */     AbstractCard card;
/*  442 */     StringBuilder bldr = new StringBuilder();
/*  443 */     for (AbstractCard.CardRarity rarity : orderedRarity) {
/*  444 */       if (cardRarityCounts.containsKey(rarity)) {
/*  445 */         if (bldr.length() > 0) {
/*  446 */           bldr.append(", ");
/*      */         }
/*      */         
/*  449 */         bldr.append(String.format(COUNT_WITH_LABEL, new Object[] { cardRarityCounts.get(rarity), rarityLabel(rarity) }));
/*      */       }
/*      */     }
/*  452 */     this.cardCountByRarityString = bldr.toString();
/*      */   }
/*      */   
/*      */   private String rarityLabel(AbstractCard.CardRarity rarity) {
/*  456 */     switch (rarity) {
/*      */     case BASIC: 
/*  458 */       return RARITY_LABEL_STARTER;
/*      */     case SPECIAL: 
/*  460 */       return RARITY_LABEL_SPECIAL;
/*      */     case COMMON: 
/*  462 */       return RARITY_LABEL_COMMON;
/*      */     case UNCOMMON: 
/*  464 */       return RARITY_LABEL_UNCOMMON;
/*      */     case RARE: 
/*  466 */       return RARITY_LABEL_RARE;
/*      */     case CURSE: 
/*  468 */       return RARITY_LABEL_CURSE;
/*      */     }
/*      */     
/*  471 */     return RARITY_LABEL_UNKNOWN;
/*      */   }
/*      */   
/*      */   private String rarityLabel(AbstractRelic.RelicTier rarity)
/*      */   {
/*  476 */     switch (rarity) {
/*      */     case STARTER: 
/*  478 */       return RARITY_LABEL_STARTER;
/*      */     case COMMON: 
/*  480 */       return RARITY_LABEL_COMMON;
/*      */     case UNCOMMON: 
/*  482 */       return RARITY_LABEL_UNCOMMON;
/*      */     case RARE: 
/*  484 */       return RARITY_LABEL_RARE;
/*      */     case SPECIAL: 
/*  486 */       return RARITY_LABEL_SPECIAL;
/*      */     case BOSS: 
/*  488 */       return RARITY_LABEL_BOSS;
/*      */     case SHOP: 
/*  490 */       return RARITY_LABEL_SHOP;
/*      */     }
/*      */     
/*  493 */     return RARITY_LABEL_UNKNOWN;
/*      */   }
/*      */   
/*      */   private void layoutTinyCards(ArrayList<TinyCard> cards, float x, float y)
/*      */   {
/*  498 */     float originX = x + screenPos(60.0F);
/*  499 */     float originY = y - screenPos(100.0F);
/*  500 */     float rowHeight = screenPos(48.0F);
/*  501 */     float columnWidth = screenPos(340.0F);
/*  502 */     int row = 0;int column = 0;
/*      */     
/*  504 */     int desiredColumns = cards.size() <= 36 ? 3 : 4;
/*  505 */     int cardsPerColumn = cards.size() / desiredColumns;
/*  506 */     int remainderCards = cards.size() - cardsPerColumn * desiredColumns;
/*      */     
/*  508 */     int[] columnSizes = new int[desiredColumns];
/*  509 */     Arrays.fill(columnSizes, cardsPerColumn);
/*  510 */     for (int i = 0; i < remainderCards; i++) {
/*  511 */       columnSizes[(i % desiredColumns)] += 1;
/*      */     }
/*      */     
/*  514 */     for (TinyCard card : cards) {
/*  515 */       if (row >= columnSizes[column]) {
/*  516 */         row = 0;
/*  517 */         column++;
/*      */       }
/*  519 */       float cardY = originY - row * rowHeight;
/*  520 */       card.move(originX + column * columnWidth, cardY);
/*  521 */       row++;
/*      */       
/*  523 */       this.scrollUpperBound = Math.max(this.scrollUpperBound, this.scrollY - cardY + screenPos(50.0F));
/*      */     }
/*      */   }
/*      */   
/*      */   private AbstractCard cardForName(RunData runData, String cardID) {
/*  528 */     String libraryLookupName = cardID;
/*  529 */     if (cardID.endsWith("+")) {
/*  530 */       libraryLookupName = cardID.substring(0, cardID.length() - 1);
/*      */     }
/*  532 */     if ((libraryLookupName.equals("Defend")) || (libraryLookupName.equals("Strike"))) {
/*  533 */       libraryLookupName = libraryLookupName + baseCardSuffixForCharacter(runData.character_chosen);
/*      */     }
/*      */     
/*  536 */     AbstractCard card = CardLibrary.getCard(libraryLookupName);
/*  537 */     int upgrades = 0;
/*  538 */     if (card != null) {
/*  539 */       if (cardID.endsWith("+")) {
/*  540 */         upgrades = 1;
/*      */       }
/*  542 */     } else if (libraryLookupName.contains("+"))
/*      */     {
/*  544 */       String[] split = libraryLookupName.split("\\+", -1);
/*  545 */       libraryLookupName = split[0];
/*  546 */       upgrades = Integer.parseInt(split[1]);
/*  547 */       card = CardLibrary.getCard(libraryLookupName);
/*      */     }
/*  549 */     if (card != null) {
/*  550 */       card = card.makeCopy();
/*  551 */       for (int i = 0; i < upgrades; i++) {
/*  552 */         card.upgrade();
/*      */       }
/*  554 */       return card;
/*      */     }
/*  556 */     logger.info("Could not find card named: " + cardID);
/*  557 */     return null;
/*      */   }
/*      */   
/*      */   public void update() {
/*  561 */     updateControllerInput();
/*      */     
/*      */ 
/*  564 */     if (this.runsDropdown.isOpen) {
/*  565 */       this.runsDropdown.update();
/*  566 */       return; }
/*  567 */     if (this.winLossFilter.isOpen) {
/*  568 */       this.winLossFilter.update();
/*  569 */       return; }
/*  570 */     if (this.characterFilter.isOpen) {
/*  571 */       this.characterFilter.update();
/*  572 */       return; }
/*  573 */     if (this.runTypeFilter.isOpen) {
/*  574 */       this.runTypeFilter.update();
/*  575 */       return;
/*      */     }
/*  577 */     this.runsDropdown.update();
/*  578 */     this.winLossFilter.update();
/*  579 */     this.characterFilter.update();
/*  580 */     this.runTypeFilter.update();
/*      */     
/*      */ 
/*  583 */     this.button.update();
/*  584 */     updateScrolling();
/*  585 */     updateArrows();
/*  586 */     this.modIcons.update();
/*  587 */     this.runPath.update();
/*  588 */     if (!this.seedElement.getText().isEmpty()) {
/*  589 */       this.seedElement.update();
/*      */     }
/*  591 */     if (!this.secondSeedElement.getText().isEmpty()) {
/*  592 */       this.secondSeedElement.update();
/*      */     }
/*      */     
/*  595 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/*  596 */       InputHelper.pressedEscape = false;
/*  597 */       hide();
/*      */     }
/*      */     
/*  600 */     this.screenX = MathHelper.uiLerpSnap(this.screenX, this.targetX);
/*      */     
/*  602 */     if (this.filteredRuns.size() == 0) {
/*  603 */       return;
/*      */     }
/*      */     
/*  606 */     boolean isAPopupOpen = (CardCrawlGame.cardPopup.isOpen) || (CardCrawlGame.relicPopup.isOpen);
/*  607 */     if (!isAPopupOpen) {
/*  608 */       if (InputActionSet.left.isJustPressed()) {
/*  609 */         this.runIndex = Math.max(0, this.runIndex - 1);
/*  610 */         reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */       }
/*  612 */       if (InputActionSet.right.isJustPressed()) {
/*  613 */         this.runIndex = Math.min(this.runIndex + 1, this.filteredRuns.size() - 1);
/*  614 */         reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */       }
/*      */     }
/*      */     
/*  618 */     handleRelicInteraction();
/*  619 */     for (TinyCard card : this.cards) {
/*  620 */       boolean didClick = card.updateDidClick();
/*  621 */       if (didClick) {
/*  622 */         CardGroup cardGroup = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  623 */         for (TinyCard addMe : this.cards) {
/*  624 */           cardGroup.addToTop(addMe.card);
/*      */         }
/*  626 */         CardCrawlGame.cardPopup.open(card.card, cardGroup);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private static enum InputSection {
/*  632 */     DROPDOWN,  ROOM,  RELIC,  CARD;
/*      */     
/*      */     private InputSection() {} }
/*      */   
/*  636 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/*  637 */       return;
/*      */     }
/*      */     
/*  640 */     InputSection section = null;
/*  641 */     boolean anyHovered = false;
/*  642 */     int index = 0;
/*      */     
/*      */ 
/*  645 */     ArrayList<Hitbox> hbs = new ArrayList();
/*  646 */     if (!this.runsDropdown.rows.isEmpty()) {
/*  647 */       hbs.add(this.runsDropdown.getHitbox());
/*      */     }
/*  649 */     hbs.add(this.winLossFilter.getHitbox());
/*  650 */     hbs.add(this.characterFilter.getHitbox());
/*  651 */     hbs.add(this.runTypeFilter.getHitbox());
/*  652 */     for (Hitbox hb : hbs) {
/*  653 */       if (hb.hovered) {
/*  654 */         section = InputSection.DROPDOWN;
/*  655 */         this.currentHb = hb;
/*  656 */         anyHovered = true;
/*  657 */         break;
/*      */       }
/*  659 */       index++;
/*      */     }
/*      */     
/*      */ 
/*  663 */     if (!anyHovered) {
/*  664 */       index = 0;
/*  665 */       for (RunPathElement e : this.runPath.pathElements) {
/*  666 */         if (e.hb.hovered) {
/*  667 */           section = InputSection.ROOM;
/*  668 */           this.currentHb = e.hb;
/*  669 */           anyHovered = true;
/*  670 */           break;
/*      */         }
/*  672 */         index++;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  677 */     if (!anyHovered) {
/*  678 */       index = 0;
/*  679 */       for (AbstractRelic r : this.relics) {
/*  680 */         if (r.hb.hovered) {
/*  681 */           section = InputSection.RELIC;
/*  682 */           this.currentHb = r.hb;
/*  683 */           anyHovered = true;
/*  684 */           break;
/*      */         }
/*  686 */         index++;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  691 */     if (!anyHovered) {
/*  692 */       index = 0;
/*  693 */       for (??? = this.cards.iterator(); ???.hasNext();) { card = (TinyCard)???.next();
/*  694 */         if (card.hb.hovered) {
/*  695 */           section = InputSection.CARD;
/*  696 */           this.currentHb = card.hb;
/*  697 */           anyHovered = true;
/*  698 */           break;
/*      */         }
/*  700 */         index++;
/*      */       }
/*      */     }
/*      */     TinyCard card;
/*  704 */     if (!anyHovered) {
/*  705 */       Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(0)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(0)).cY);
/*      */     } else {
/*  707 */       switch (section) {
/*      */       case DROPDOWN: 
/*  709 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  710 */           index--;
/*  711 */           if (index != -1) {
/*  712 */             Gdx.input.setCursorPosition(
/*  713 */               (int)((Hitbox)hbs.get(index)).cX, Settings.HEIGHT - 
/*  714 */               (int)((Hitbox)hbs.get(index)).cY);
/*      */           }
/*  716 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  717 */           index++;
/*  718 */           if (hbs.size() == 4) {
/*  719 */             if ((index > hbs.size() - 1) || (index == 1))
/*      */             {
/*  721 */               if (!this.runPath.pathElements.isEmpty()) {
/*  722 */                 Gdx.input.setCursorPosition(
/*  723 */                   (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cX, Settings.HEIGHT - 
/*  724 */                   (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cY);
/*      */               }
/*      */               else
/*      */               {
/*  728 */                 Gdx.input.setCursorPosition(
/*  729 */                   (int)((AbstractRelic)this.relics.get(0)).hb.cX, Settings.HEIGHT - 
/*  730 */                   (int)((AbstractRelic)this.relics.get(0)).hb.cY);
/*      */               }
/*      */             } else {
/*  733 */               Gdx.input.setCursorPosition(
/*  734 */                 (int)((Hitbox)hbs.get(index)).cX, Settings.HEIGHT - 
/*  735 */                 (int)((Hitbox)hbs.get(index)).cY);
/*      */             }
/*      */           } else {
/*  738 */             if (index > hbs.size() - 1) {
/*  739 */               index = 0;
/*      */             }
/*  741 */             Gdx.input.setCursorPosition(
/*  742 */               (int)((Hitbox)hbs.get(index)).cX, Settings.HEIGHT - 
/*  743 */               (int)((Hitbox)hbs.get(index)).cY);
/*      */           }
/*  745 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  746 */           if (index == 0) {
/*  747 */             Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(1)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(1)).cY);
/*      */           }
/*  749 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  750 */           if (index == 1) {
/*  751 */             Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(0)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(0)).cY);
/*  752 */           } else if (index > 1)
/*      */           {
/*  754 */             if (!this.runPath.pathElements.isEmpty()) {
/*  755 */               Gdx.input.setCursorPosition(
/*  756 */                 (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cX, Settings.HEIGHT - 
/*  757 */                 (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cY);
/*      */             }
/*      */             else
/*      */             {
/*  761 */               Gdx.input.setCursorPosition(
/*  762 */                 (int)((AbstractRelic)this.relics.get(0)).hb.cX, Settings.HEIGHT - 
/*  763 */                 (int)((AbstractRelic)this.relics.get(0)).hb.cY);
/*      */             }
/*      */           }
/*      */         }
/*      */         break;
/*      */       case ROOM: 
/*  769 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  770 */           index -= 25;
/*  771 */           if (index < 0) {
/*  772 */             Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(0)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(0)).cY);
/*      */           } else {
/*  774 */             Gdx.input.setCursorPosition(
/*  775 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cX, Settings.HEIGHT - 
/*  776 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cY);
/*      */           }
/*  778 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  779 */           index += 25;
/*  780 */           if (index > this.runPath.pathElements.size() - 1) {
/*  781 */             Gdx.input.setCursorPosition(
/*  782 */               (int)((AbstractRelic)this.relics.get(0)).hb.cX, Settings.HEIGHT - 
/*  783 */               (int)((AbstractRelic)this.relics.get(0)).hb.cY);
/*      */           } else {
/*  785 */             Gdx.input.setCursorPosition(
/*  786 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cX, Settings.HEIGHT - 
/*  787 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cY);
/*      */           }
/*  789 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  790 */           index--;
/*  791 */           if (index < 0) {
/*  792 */             if (hbs.size() > 3) {
/*  793 */               Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(3)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(3)).cY);
/*      */             }
/*      */           } else {
/*  796 */             Gdx.input.setCursorPosition(
/*  797 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cX, Settings.HEIGHT - 
/*  798 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cY);
/*      */           }
/*  800 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  801 */           index++;
/*  802 */           if (index > this.runPath.pathElements.size() - 1) {
/*  803 */             Gdx.input.setCursorPosition(
/*  804 */               (int)((AbstractRelic)this.relics.get(0)).hb.cX, Settings.HEIGHT - 
/*  805 */               (int)((AbstractRelic)this.relics.get(0)).hb.cY);
/*      */           } else {
/*  807 */             Gdx.input.setCursorPosition(
/*  808 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cX, Settings.HEIGHT - 
/*  809 */               (int)((RunPathElement)this.runPath.pathElements.get(index)).hb.cY);
/*      */           }
/*      */         }
/*      */         break;
/*      */       case RELIC: 
/*  814 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  815 */           index -= 15;
/*  816 */           if (index < 0) {
/*  817 */             if (!this.runPath.pathElements.isEmpty()) {
/*  818 */               Gdx.input.setCursorPosition(
/*  819 */                 (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cX, Settings.HEIGHT - 
/*  820 */                 (int)((RunPathElement)this.runPath.pathElements.get(0)).hb.cY);
/*      */             } else {
/*  822 */               Gdx.input.setCursorPosition((int)((Hitbox)hbs.get(0)).cX, Settings.HEIGHT - (int)((Hitbox)hbs.get(0)).cY);
/*      */             }
/*      */           } else {
/*  825 */             Gdx.input.setCursorPosition(
/*  826 */               (int)((AbstractRelic)this.relics.get(index)).hb.cX, Settings.HEIGHT - 
/*  827 */               (int)((AbstractRelic)this.relics.get(index)).hb.cY);
/*      */           }
/*  829 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  830 */           index += 15;
/*  831 */           if (index > this.relics.size() - 1) {
/*  832 */             if (!this.cards.isEmpty()) {
/*  833 */               Gdx.input.setCursorPosition(
/*  834 */                 (int)(((TinyCard)this.cards.get(0)).hb.x + 10.0F), Settings.HEIGHT - 
/*  835 */                 (int)(((TinyCard)this.cards.get(0)).hb.y + 10.0F));
/*  836 */               System.out.println(((TinyCard)this.cards.get(0)).hb.x);
/*  837 */               System.out.println(((TinyCard)this.cards.get(0)).hb.y);
/*      */             }
/*      */           } else {
/*  840 */             Gdx.input.setCursorPosition(
/*  841 */               (int)((AbstractRelic)this.relics.get(index)).hb.cX, Settings.HEIGHT - 
/*  842 */               (int)((AbstractRelic)this.relics.get(index)).hb.cY);
/*      */           }
/*  844 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  845 */           index--;
/*  846 */           if (index < 0) {
/*  847 */             if (!this.cards.isEmpty()) {
/*  848 */               Gdx.input.setCursorPosition(
/*  849 */                 (int)(((TinyCard)this.cards.get(0)).hb.x + 10.0F), Settings.HEIGHT - 
/*  850 */                 (int)(((TinyCard)this.cards.get(0)).hb.y + 10.0F));
/*      */             }
/*  852 */           } else if (!this.relics.isEmpty()) {
/*  853 */             Gdx.input.setCursorPosition(
/*  854 */               (int)((AbstractRelic)this.relics.get(index)).hb.cX, Settings.HEIGHT - 
/*  855 */               (int)((AbstractRelic)this.relics.get(index)).hb.cY);
/*      */           }
/*  857 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  858 */           index++;
/*  859 */           if (index > this.relics.size() - 1) {
/*  860 */             if (!this.cards.isEmpty()) {
/*  861 */               Gdx.input.setCursorPosition(
/*  862 */                 (int)(((TinyCard)this.cards.get(0)).hb.x + 10.0F), Settings.HEIGHT - 
/*  863 */                 (int)(((TinyCard)this.cards.get(0)).hb.y + 10.0F));
/*      */             }
/*  865 */           } else if (!this.relics.isEmpty()) {
/*  866 */             Gdx.input.setCursorPosition(
/*  867 */               (int)((AbstractRelic)this.relics.get(index)).hb.cX, Settings.HEIGHT - 
/*  868 */               (int)((AbstractRelic)this.relics.get(index)).hb.cY);
/*      */           }
/*  870 */         } else if (CInputActionSet.select.isJustPressed()) {
/*  871 */           CardCrawlGame.relicPopup.open((AbstractRelic)this.relics.get(index), this.relics);
/*      */         }
/*      */         break;
/*      */       case CARD: 
/*  875 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  876 */           index -= 3;
/*  877 */           if (index < 0) {
/*  878 */             Gdx.input.setCursorPosition(
/*  879 */               (int)((AbstractRelic)this.relics.get(0)).hb.cX, Settings.HEIGHT - 
/*  880 */               (int)((AbstractRelic)this.relics.get(0)).hb.cY);
/*      */           } else {
/*  882 */             Gdx.input.setCursorPosition(
/*  883 */               (int)(((TinyCard)this.cards.get(index)).hb.x + 10.0F), Settings.HEIGHT - 
/*  884 */               (int)(((TinyCard)this.cards.get(index)).hb.y + 10.0F));
/*      */           }
/*  886 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  887 */           index += 3;
/*  888 */           if (index <= this.cards.size() - 1) {
/*  889 */             Gdx.input.setCursorPosition(
/*  890 */               (int)(((TinyCard)this.cards.get(index)).hb.x + 10.0F), Settings.HEIGHT - 
/*  891 */               (int)(((TinyCard)this.cards.get(index)).hb.y + 10.0F));
/*      */           }
/*  893 */         } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  894 */           index--;
/*  895 */           if (index < 0) {
/*  896 */             Gdx.input.setCursorPosition(
/*  897 */               (int)((AbstractRelic)this.relics.get(this.relics.size() - 1)).hb.cX, Settings.HEIGHT - 
/*  898 */               (int)((AbstractRelic)this.relics.get(this.relics.size() - 1)).hb.cY);
/*      */           } else {
/*  900 */             Gdx.input.setCursorPosition(
/*  901 */               (int)(((TinyCard)this.cards.get(index)).hb.x + 10.0F), Settings.HEIGHT - 
/*  902 */               (int)(((TinyCard)this.cards.get(index)).hb.y + 10.0F));
/*      */           }
/*  904 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  905 */           index++;
/*  906 */           if (index <= this.cards.size() - 1) {
/*  907 */             Gdx.input.setCursorPosition(
/*  908 */               (int)(((TinyCard)this.cards.get(index)).hb.x + 10.0F), Settings.HEIGHT - 
/*  909 */               (int)(((TinyCard)this.cards.get(index)).hb.y + 10.0F));
/*      */           }
/*  911 */         } else if (CInputActionSet.select.isJustPressed()) {
/*  912 */           CardGroup cardGroup = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  913 */           for (TinyCard addMe : this.cards) {
/*  914 */             cardGroup.addToTop(addMe.card);
/*      */           }
/*  916 */           CardCrawlGame.cardPopup.open(((TinyCard)this.cards.get(index)).card, cardGroup); }
/*  917 */         break;
/*      */       }
/*      */       
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void open()
/*      */   {
/*  928 */     this.currentHb = null;
/*  929 */     CardCrawlGame.mainMenuScreen.screen = com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen.RUN_HISTORY;
/*  930 */     SingleCardViewPopup.enableUpgradeToggle = false;
/*      */     
/*  932 */     refreshData();
/*      */     
/*  934 */     this.targetX = SHOW_X;
/*  935 */     this.button.show(TEXT[3]);
/*  936 */     this.screenUp = true;
/*      */     
/*  938 */     this.scrollY = this.scrollLowerBound;
/*  939 */     this.scrollTargetY = this.scrollLowerBound;
/*      */   }
/*      */   
/*      */   public void hide() {
/*  943 */     this.targetX = HIDE_X;
/*  944 */     this.button.hide();
/*  945 */     this.screenUp = false;
/*  946 */     this.currentChar = null;
/*  947 */     CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*  948 */     SingleCardViewPopup.enableUpgradeToggle = true;
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb) {
/*  952 */     renderRunHistoryScreen(sb);
/*  953 */     renderArrows(sb);
/*  954 */     renderFilters(sb);
/*  955 */     this.button.render(sb);
/*      */     
/*  957 */     if (Settings.isControllerMode) {
/*  958 */       renderControllerUi(sb, this.currentHb);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderControllerUi(SpriteBatch sb, Hitbox hb) {
/*  963 */     if (hb != null) {
/*  964 */       sb.setBlendFunction(770, 1);
/*  965 */       sb.setColor(new Color(0.7F, 0.9F, 1.0F, 0.25F));
/*  966 */       sb.draw(ImageMaster.CONTROLLER_HB_HIGHLIGHT, hb.cX - hb.width / 2.0F, hb.cY - hb.height / 2.0F, hb.width, hb.height);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  972 */       sb.setBlendFunction(770, 771);
/*      */     }
/*      */   }
/*      */   
/*      */   private String characterText(String chosenCharacter) {
/*  977 */     if (chosenCharacter.equals(AbstractPlayer.PlayerClass.IRONCLAD.name()))
/*  978 */       return IRONCLAD_NAME;
/*  979 */     if (chosenCharacter.equals(AbstractPlayer.PlayerClass.THE_SILENT.name()))
/*  980 */       return SILENT_NAME;
/*  981 */     if (chosenCharacter.equals(AbstractPlayer.PlayerClass.DEFECT.name())) {
/*  982 */       return DEFECT_NAME;
/*      */     }
/*  984 */     return chosenCharacter;
/*      */   }
/*      */   
/*      */   private void renderArrows(SpriteBatch sb) {
/*  988 */     if (this.runIndex < this.filteredRuns.size() - 1) {
/*  989 */       sb.setColor(Color.WHITE);
/*  990 */       sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1008 */       if (this.nextHb.hovered) {
/* 1009 */         sb.setBlendFunction(770, 1);
/* 1010 */         sb.setColor(Color.WHITE);
/* 1011 */         sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1028 */         sb.setBlendFunction(770, 771);
/*      */       }
/*      */       
/* 1031 */       if (Settings.isControllerMode) {
/* 1032 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 1033 */           .getKeyImg(), this.nextHb.cX - 32.0F + 0.0F * Settings.scale, this.nextHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1051 */       this.nextHb.render(sb);
/*      */     }
/*      */     
/* 1054 */     if (this.runIndex > 0) {
/* 1055 */       sb.setColor(Color.WHITE);
/* 1056 */       sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1074 */       if (this.prevHb.hovered) {
/* 1075 */         sb.setBlendFunction(770, 1);
/* 1076 */         sb.setColor(Color.WHITE);
/* 1077 */         sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1094 */         sb.setBlendFunction(770, 771);
/*      */       }
/*      */       
/* 1097 */       if (Settings.isControllerMode) {
/* 1098 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 1099 */           .getKeyImg(), this.prevHb.cX - 32.0F + 0.0F * Settings.scale, this.prevHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1116 */       this.prevHb.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderRunHistoryScreen(SpriteBatch sb) {
/* 1121 */     float TOP_POSITION = 1020.0F;
/* 1122 */     if (this.viewedRun == null) {
/* 1123 */       FontHelper.renderSmartText(sb, FontHelper.charTitleFont, TEXT[4], 
/*      */       
/*      */ 
/*      */ 
/* 1127 */         screenPos(500.0F), Settings.HEIGHT / 2, 9999.0F, 36.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1132 */       return;
/*      */     }
/*      */     
/* 1135 */     float header1x = this.screenX + screenPos(50.0F);
/* 1136 */     float header2x = this.screenX + screenPos(80.0F);
/* 1137 */     float yOffset = this.scrollY + screenPos(1020.0F);
/*      */     
/* 1139 */     String characterName = characterText(this.viewedRun.character_chosen);
/* 1140 */     renderHeader1(sb, characterName, header1x, yOffset);
/* 1141 */     float approxCharNameWidth = approximateHeader1Width(characterName);
/*      */     
/*      */ 
/* 1144 */     if (!this.seedElement.getText().isEmpty()) {
/* 1145 */       float maxSeedWidth = Math.max(this.seedElement.approximateWidth(), this.secondSeedElement.approximateWidth());
/* 1146 */       float seedX = header2x + this.runPath.approximateMaxWidth() - maxSeedWidth;
/* 1147 */       this.seedElement.render(sb, seedX, yOffset);
/* 1148 */       if (!this.secondSeedElement.getText().isEmpty()) {
/* 1149 */         this.secondSeedElement.render(sb, seedX, yOffset - screenPos(36.0F));
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1154 */     yOffset -= screenPos(50.0F);
/* 1155 */     String specialModeText = "";
/* 1156 */     if (this.viewedRun.is_daily) {
/* 1157 */       specialModeText = " (" + TEXT[27] + ")";
/* 1158 */     } else if (this.viewedRun.is_ascension_mode)
/* 1159 */       specialModeText = " (" + TEXT[5] + this.viewedRun.ascension_level + ")";
/*      */     Color resultTextColor;
/*      */     Color resultTextColor;
/*      */     String resultText;
/* 1163 */     if (this.viewedRun.victory) {
/* 1164 */       String resultText = TEXT[8] + specialModeText;
/* 1165 */       resultTextColor = Settings.GREEN_TEXT_COLOR;
/*      */     } else {
/* 1167 */       resultTextColor = Settings.RED_TEXT_COLOR;
/* 1168 */       String resultText; if (this.viewedRun.killed_by == null)
/*      */       {
/* 1170 */         resultText = String.format(TEXT[7], new Object[] { Integer.valueOf(this.viewedRun.floor_reached) }) + specialModeText;
/*      */       } else {
/* 1172 */         resultText = String.format(TEXT[6], new Object[] { Integer.valueOf(this.viewedRun.floor_reached), this.viewedRun.killed_by }) + specialModeText;
/*      */       }
/*      */     }
/* 1175 */     renderSubHeading(sb, resultText, header1x, yOffset, resultTextColor);
/*      */     
/* 1177 */     if (this.viewedRun.victory) {
/* 1178 */       sb.setColor(Color.WHITE);
/* 1179 */       sb.draw(ImageMaster.TIMER_ICON, header1x + 
/*      */       
/* 1181 */         approximateSubHeaderWidth(resultText) - 32.0F + 54.0F * Settings.scale, yOffset - 32.0F - 10.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1197 */       renderSubHeading(sb, 
/*      */       
/* 1199 */         CharStat.formatHMSM(this.viewedRun.playtime), header1x + 
/* 1200 */         approximateSubHeaderWidth(resultText) + 80.0F * Settings.scale, yOffset, Settings.CREAM_COLOR);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1205 */     float scoreLineXOffset = header1x;
/*      */     
/* 1207 */     yOffset -= screenPos(40.0F);
/* 1208 */     String scoreText = String.format(TEXT[22], new Object[] { Integer.valueOf(this.viewedRun.score) });
/* 1209 */     renderSubHeading(sb, scoreText, header1x, yOffset, Settings.GOLD_COLOR);
/* 1210 */     scoreLineXOffset += approximateSubHeaderWidth(scoreText);
/*      */     
/* 1212 */     if (this.modIcons.hasMods()) {
/* 1213 */       this.modIcons.renderDailyMods(sb, scoreLineXOffset, yOffset);
/* 1214 */       scoreLineXOffset += this.modIcons.approximateWidth();
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1219 */     yOffset -= screenPos(18.0F);
/* 1220 */     this.runPath.render(sb, header2x, yOffset);
/* 1221 */     yOffset -= this.runPath.approximateHeight();
/*      */     
/*      */ 
/*      */ 
/* 1225 */     yOffset -= screenPos(35.0F);
/* 1226 */     float relicBottom = renderRelics(sb, header2x, yOffset);
/* 1227 */     yOffset = relicBottom - screenPos(70.0F);
/* 1228 */     renderDeck(sb, header2x, yOffset);
/*      */     
/*      */ 
/* 1231 */     this.runsDropdown.render(sb, header1x + approxCharNameWidth + screenPos(30.0F), this.scrollY + screenPos(1026.0F));
/*      */   }
/*      */   
/*      */   private void renderHeader1(SpriteBatch sb, String text, float x, float y) {
/* 1235 */     FontHelper.renderSmartText(sb, FontHelper.charTitleFont, text, x, y, 9999.0F, 36.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private float approximateHeader1Width(String text)
/*      */   {
/* 1247 */     return FontHelper.getSmartWidth(FontHelper.charTitleFont, text, 9999.0F, 36.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   private float approximateSubHeaderWidth(String text) {
/* 1251 */     return FontHelper.getSmartWidth(FontHelper.buttonLabelFont, text, 9999.0F, 36.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   private void renderSubHeading(SpriteBatch sb, String text, float x, float y, Color color) {
/* 1255 */     FontHelper.renderSmartText(sb, FontHelper.buttonLabelFont, text, x, y, 9999.0F, 36.0F * Settings.scale, color);
/*      */   }
/*      */   
/*      */   private void renderSubHeadingWithMessage(SpriteBatch sb, String main, String description, float x, float y) {
/* 1259 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.buttonLabelFont, main, x, y, Settings.GOLD_COLOR);
/* 1260 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.cardDescFont_N, description, x + 
/*      */     
/*      */ 
/*      */ 
/* 1264 */       FontHelper.getSmartWidth(FontHelper.buttonLabelFont, main, 99999.0F, 0.0F), y - 4.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */   }
/*      */   
/*      */ 
/*      */   private void renderDeck(SpriteBatch sb, float x, float y)
/*      */   {
/* 1270 */     layoutTinyCards(this.cards, this.screenX + screenPos(50.0F), y);
/* 1271 */     int cardCount = 0;
/*      */     
/* 1273 */     for (TinyCard card : this.cards) {
/* 1274 */       card.render(sb);
/* 1275 */       cardCount += card.count;
/*      */     }
/* 1277 */     String mainText = String.format(LABEL_WITH_COUNT_IN_PARENS, new Object[] { TEXT[9], Integer.valueOf(cardCount) });
/* 1278 */     renderSubHeadingWithMessage(sb, mainText, this.cardCountByRarityString, x, y);
/*      */   }
/*      */   
/* 1281 */   private static final Color RED_OUTLINE_COLOR = new Color(-10132568);
/* 1282 */   private static final Color GREEN_OUTLINE_COLOR = new Color(2147418280);
/* 1283 */   private static final Color BLUE_OUTLINE_COLOR = new Color(-2016482392);
/* 1284 */   private static final Color BLACK_OUTLINE_COLOR = new Color(168);
/*      */   
/* 1286 */   private static final float RELIC_SPACE = 64.0F * Settings.scale;
/*      */   
/*      */   private float renderRelics(SpriteBatch sb, float x, float y) {
/* 1289 */     String mainText = String.format(LABEL_WITH_COUNT_IN_PARENS, new Object[] { TEXT[10], Integer.valueOf(this.relics.size()) });
/* 1290 */     renderSubHeadingWithMessage(sb, mainText, this.relicCountByRarityString, x, y);
/*      */     
/* 1292 */     int col = 0;
/* 1293 */     int row = 0;
/*      */     
/* 1295 */     float relicStartX = x + screenPos(30.0F) + RELIC_SPACE / 2.0F;
/* 1296 */     float relicStartY = y - RELIC_SPACE - screenPos(10.0F);
/*      */     
/* 1298 */     for (AbstractRelic relic : this.relics)
/*      */     {
/* 1300 */       if (col == 15) {
/* 1301 */         col = 0;
/* 1302 */         row++;
/*      */       }
/* 1304 */       relic.currentX = (relicStartX + RELIC_SPACE * col);
/* 1305 */       relic.currentY = (relicStartY - RELIC_SPACE * row);
/* 1306 */       relic.hb.move(relic.currentX, relic.currentY);
/* 1307 */       if (RelicLibrary.redList.contains(relic)) {
/* 1308 */         relic.render(sb, false, RED_OUTLINE_COLOR);
/* 1309 */       } else if (RelicLibrary.greenList.contains(relic)) {
/* 1310 */         relic.render(sb, false, GREEN_OUTLINE_COLOR);
/* 1311 */       } else if (RelicLibrary.blueList.contains(relic)) {
/* 1312 */         relic.render(sb, false, BLUE_OUTLINE_COLOR);
/*      */       } else {
/* 1314 */         relic.render(sb, false, BLACK_OUTLINE_COLOR);
/*      */       }
/* 1316 */       col++;
/*      */     }
/* 1318 */     return relicStartY - RELIC_SPACE * row;
/*      */   }
/*      */   
/* 1321 */   AbstractRelic hoveredRelic = null;
/* 1322 */   AbstractRelic clickStartedRelic = null;
/*      */   
/*      */   private void handleRelicInteraction() {
/* 1325 */     for (AbstractRelic r : this.relics)
/*      */     {
/*      */ 
/* 1328 */       boolean wasScreenUp = AbstractDungeon.isScreenUp;
/* 1329 */       AbstractDungeon.isScreenUp = true;
/* 1330 */       r.update();
/* 1331 */       AbstractDungeon.isScreenUp = wasScreenUp;
/*      */       
/* 1333 */       if (r.hb.hovered) {
/* 1334 */         this.hoveredRelic = r;
/*      */       }
/*      */     }
/*      */     
/* 1338 */     if (this.hoveredRelic != null) {
/* 1339 */       CardCrawlGame.cursor.changeType(com.megacrit.cardcrawl.core.GameCursor.CursorType.INSPECT);
/* 1340 */       if (InputHelper.justClickedLeft) {
/* 1341 */         this.clickStartedRelic = this.hoveredRelic;
/*      */       }
/* 1343 */       if ((InputHelper.justReleasedClickLeft) && 
/* 1344 */         (this.hoveredRelic == this.clickStartedRelic)) {
/* 1345 */         CardCrawlGame.relicPopup.open(this.hoveredRelic, this.relics);
/* 1346 */         this.clickStartedRelic = null;
/*      */       }
/*      */     }
/*      */     else {
/* 1350 */       this.clickStartedRelic = null;
/*      */     }
/* 1352 */     this.hoveredRelic = null;
/*      */   }
/*      */   
/*      */   private float screenPos(float val) {
/* 1356 */     return val * Settings.scale;
/*      */   }
/*      */   
/*      */   private void updateArrows() {
/* 1360 */     if (this.runIndex < this.filteredRuns.size() - 1) {
/* 1361 */       this.nextHb.update();
/* 1362 */       if (this.nextHb.justHovered) {
/* 1363 */         CardCrawlGame.sound.play("UI_HOVER");
/* 1364 */       } else if ((this.nextHb.hovered) && (InputHelper.justClickedLeft)) {
/* 1365 */         this.nextHb.clickStarted = true;
/* 1366 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 1367 */       } else if ((this.nextHb.clicked) || ((CInputActionSet.pageRightViewExhaust.isJustPressed()) && (!CardCrawlGame.isPopupOpen)))
/*      */       {
/* 1369 */         this.nextHb.clicked = false;
/* 1370 */         this.runIndex = Math.min(this.runIndex + 1, this.filteredRuns.size() - 1);
/* 1371 */         reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */       }
/*      */     }
/*      */     
/* 1375 */     if (this.runIndex > 0) {
/* 1376 */       this.prevHb.update();
/* 1377 */       if (this.prevHb.justHovered) {
/* 1378 */         CardCrawlGame.sound.play("UI_HOVER");
/* 1379 */       } else if ((this.prevHb.hovered) && (InputHelper.justClickedLeft)) {
/* 1380 */         this.prevHb.clickStarted = true;
/* 1381 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 1382 */       } else if ((this.prevHb.clicked) || ((CInputActionSet.pageLeftViewDeck.isJustPressed()) && (!CardCrawlGame.isPopupOpen)))
/*      */       {
/* 1384 */         this.prevHb.clicked = false;
/* 1385 */         this.runIndex = Math.max(0, this.runIndex - 1);
/* 1386 */         reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderFilters(SpriteBatch sb)
/*      */   {
/* 1393 */     float charFilterWidth = this.characterFilter.approximateOverallWidth();
/* 1394 */     float winLossFilterWidth = this.winLossFilter.approximateOverallWidth();
/* 1395 */     float runTypeFilterWidth = this.runTypeFilter.approximateOverallWidth();
/*      */     
/* 1397 */     float maxDropdownWidth = Math.max(charFilterWidth, Math.max(winLossFilterWidth, runTypeFilterWidth));
/* 1398 */     float widthOffset = maxDropdownWidth + screenPos(35.0F);
/* 1399 */     float filterX = this.screenX + screenPos(50.0F) - widthOffset;
/* 1400 */     float winLossY = this.scrollY + screenPos(1000.0F);
/* 1401 */     float charY = winLossY - screenPos(54.0F);
/* 1402 */     float runTypeY = charY - screenPos(54.0F);
/*      */     
/*      */ 
/* 1405 */     this.runTypeFilter.render(sb, filterX, runTypeY);
/* 1406 */     this.characterFilter.render(sb, filterX, charY);
/* 1407 */     this.winLossFilter.render(sb, filterX, winLossY);
/*      */   }
/*      */   
/*      */   private void updateScrolling()
/*      */   {
/* 1412 */     int y = InputHelper.mY;
/*      */     
/* 1414 */     if (this.scrollUpperBound > 0.0F) {
/* 1415 */       if (!this.grabbedScreen) {
/* 1416 */         if (InputHelper.scrolledDown) {
/* 1417 */           this.scrollTargetY += Settings.SCROLL_SPEED;
/* 1418 */         } else if (InputHelper.scrolledUp) {
/* 1419 */           this.scrollTargetY -= Settings.SCROLL_SPEED;
/*      */         }
/*      */         
/* 1422 */         if (InputHelper.justClickedLeft) {
/* 1423 */           this.grabbedScreen = true;
/* 1424 */           this.grabStartY = (y - this.scrollTargetY);
/*      */         }
/*      */       }
/* 1427 */       else if (InputHelper.isMouseDown) {
/* 1428 */         this.scrollTargetY = (y - this.grabStartY);
/*      */       } else {
/* 1430 */         this.grabbedScreen = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1435 */     this.scrollY = MathHelper.scrollSnapLerpSpeed(this.scrollY, this.scrollTargetY);
/* 1436 */     resetScrolling();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void resetScrolling()
/*      */   {
/* 1443 */     if (this.scrollTargetY < this.scrollLowerBound) {
/* 1444 */       this.scrollTargetY = MathHelper.scrollSnapLerpSpeed(this.scrollTargetY, this.scrollLowerBound);
/* 1445 */     } else if (this.scrollTargetY > this.scrollUpperBound) {
/* 1446 */       this.scrollTargetY = MathHelper.scrollSnapLerpSpeed(this.scrollTargetY, this.scrollUpperBound);
/*      */     }
/*      */   }
/*      */   
/*      */   public void changedSelectionTo(DropdownMenu dropdownMenu, int index, String optionText)
/*      */   {
/* 1452 */     if (dropdownMenu == this.runsDropdown) {
/* 1453 */       runDropdownChangedTo(index);
/* 1454 */     } else if (isFilterDropdown(dropdownMenu)) {
/* 1455 */       resetRunsDropdown();
/*      */     }
/*      */   }
/*      */   
/*      */   private boolean isFilterDropdown(DropdownMenu dropdownMenu) {
/* 1460 */     return (dropdownMenu == this.winLossFilter) || (dropdownMenu == this.characterFilter) || (dropdownMenu == this.runTypeFilter);
/*      */   }
/*      */   
/*      */   private void runDropdownChangedTo(int index) {
/* 1464 */     if (this.runIndex == index)
/*      */     {
/* 1466 */       return;
/*      */     }
/* 1468 */     this.runIndex = index;
/* 1469 */     reloadWithRunData((RunData)this.filteredRuns.get(this.runIndex));
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\runHistory\RunHistoryScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */