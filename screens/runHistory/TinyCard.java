/*     */ package com.megacrit.cardcrawl.screens.runHistory;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.GameCursor.CursorType;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class TinyCard
/*     */ {
/*     */   private static final int MAX_CARD_TEXT_LENGTH = 18;
/*  19 */   private static final BitmapFont TINY_CARD_FONT = FontHelper.eventBodyText;
/*     */   
/*  21 */   private static final Color RED_BACKGROUND_COLOR = new Color(-719117313);
/*  22 */   private static final Color RED_DESCRIPTION_COLOR = new Color(1613902591);
/*     */   
/*  24 */   private static final Color GREEN_BACKGROUND_COLOR = new Color(1792302079);
/*  25 */   private static final Color GREEN_DESCRIPTION_COLOR = new Color(894908927);
/*     */   
/*  27 */   private static final Color BLUE_BACKGROUND_COLOR = new Color(1774256127);
/*  28 */   private static final Color BLUE_DESCRIPTION_COLOR = new Color(1417522687);
/*     */   
/*  30 */   private static final Color COLORLESS_BACKGROUND_COLOR = new Color(2054847231);
/*  31 */   private static final Color COLORLESS_DESCRIPTION_COLOR = new Color(1077952767);
/*     */   
/*  33 */   private static final Color CURSE_BACKGROUND_COLOR = new Color(993541375);
/*  34 */   private static final Color CURSE_DESCRIPTION_COLOR = new Color(454761471);
/*     */   
/*  36 */   private static final Color COMMON_BANNER_COLOR = new Color(-1364283649);
/*  37 */   private static final Color UNCOMMON_BANNER_COLOR = new Color(-1930365185);
/*  38 */   private static final Color RARE_BANNER_COLOR = new Color(-103454721);
/*     */   
/*  40 */   public static final float LINE_SPACING = 36.0F * Settings.scale;
/*     */   public static final float LINE_WIDTH = 9999.0F;
/*  42 */   public static final BitmapFont FONT = FontHelper.eventBodyText;
/*  43 */   public static final float TEXT_LEADING_SPACE = scaled(60.0F);
/*     */   public AbstractCard card;
/*     */   public int count;
/*     */   public Hitbox hb;
/*     */   
/*     */   public TinyCard(AbstractCard card, int count)
/*     */   {
/*  50 */     this.card = card;
/*  51 */     this.count = count;
/*  52 */     this.hb = new Hitbox(approximateWidth(), ImageMaster.TINY_CARD_ATTACK.getHeight());
/*     */   }
/*     */   
/*     */   public void move(float x, float y) {
/*  56 */     this.hb.x = x;
/*  57 */     this.hb.y = y;
/*     */   }
/*     */   
/*     */   public float approximateWidth() {
/*  61 */     String text = getText();
/*  62 */     return TEXT_LEADING_SPACE + FontHelper.getSmartWidth(TINY_CARD_FONT, text, 9999.0F, LINE_SPACING);
/*     */   }
/*     */   
/*     */   private String getText() {
/*  66 */     String text = this.count + " x " + this.card.name;
/*  67 */     if (text.length() > 18) {
/*  68 */       text = text.substring(0, 15) + "...";
/*     */     }
/*  70 */     return text;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  74 */     float x = this.hb.x;
/*  75 */     float y = this.hb.y;
/*  76 */     float width = scaled(46.0F);
/*  77 */     float height = scaled(46.0F);
/*     */     
/*  79 */     renderTinyCardIcon(sb, this.card, x, y, width, height);
/*     */     
/*  81 */     String text = getText();
/*     */     
/*  83 */     float textOffset = -(height / 2.0F) + scaled(7.0F);
/*     */     
/*  85 */     Color basicColor = this.card.upgraded ? Settings.GREEN_TEXT_COLOR : Settings.CREAM_COLOR;
/*  86 */     Color textColor = this.hb.hovered ? Settings.GOLD_COLOR : basicColor;
/*  87 */     FontHelper.renderSmartText(sb, FONT, text, x + TEXT_LEADING_SPACE, y + height + textOffset, 9999.0F, LINE_SPACING, textColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  96 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public boolean updateDidClick() {
/* 100 */     this.hb.update();
/* 101 */     if (this.hb.justHovered) {
/* 102 */       CardCrawlGame.sound.playV("UI_HOVER", 0.75F);
/*     */     }
/*     */     
/* 105 */     if (this.hb.hovered) {
/* 106 */       CardCrawlGame.cursor.changeType(GameCursor.CursorType.INSPECT);
/* 107 */       if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/* 108 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 109 */         this.hb.clickStarted = true;
/*     */       }
/*     */     }
/*     */     
/* 113 */     if (this.hb.clicked) {
/* 114 */       this.hb.clicked = false;
/* 115 */       return true;
/*     */     }
/*     */     
/* 118 */     return false;
/*     */   }
/*     */   
/*     */   private Color getIconBackgroundColor(AbstractCard card) {
/* 122 */     switch (card.color) {
/*     */     case RED: 
/* 124 */       return RED_BACKGROUND_COLOR;
/*     */     case GREEN: 
/* 126 */       return GREEN_BACKGROUND_COLOR;
/*     */     case BLUE: 
/* 128 */       return BLUE_BACKGROUND_COLOR;
/*     */     case COLORLESS: 
/* 130 */       return COLORLESS_BACKGROUND_COLOR;
/*     */     case CURSE: 
/* 132 */       return CURSE_BACKGROUND_COLOR;
/*     */     }
/* 134 */     return new Color(-9849601);
/*     */   }
/*     */   
/*     */   private Color getIconDescriptionColor(AbstractCard card)
/*     */   {
/* 139 */     switch (card.color) {
/*     */     case RED: 
/* 141 */       return RED_DESCRIPTION_COLOR;
/*     */     case GREEN: 
/* 143 */       return GREEN_DESCRIPTION_COLOR;
/*     */     case BLUE: 
/* 145 */       return BLUE_DESCRIPTION_COLOR;
/*     */     case COLORLESS: 
/* 147 */       return COLORLESS_DESCRIPTION_COLOR;
/*     */     case CURSE: 
/* 149 */       return CURSE_DESCRIPTION_COLOR;
/*     */     }
/* 151 */     return new Color(-1303806465);
/*     */   }
/*     */   
/*     */   private Color getIconBannerColor(AbstractCard card)
/*     */   {
/* 156 */     switch (card.rarity) {
/*     */     case BASIC: 
/*     */     case SPECIAL: 
/*     */     case COMMON: 
/*     */     case CURSE: 
/* 161 */       return COMMON_BANNER_COLOR;
/*     */     case UNCOMMON: 
/* 163 */       return UNCOMMON_BANNER_COLOR;
/*     */     case RARE: 
/* 165 */       return RARE_BANNER_COLOR;
/*     */     }
/* 167 */     return COMMON_BANNER_COLOR;
/*     */   }
/*     */   
/*     */   private Texture getIconPortrait(AbstractCard card) {
/* 171 */     switch (card.type) {
/*     */     case ATTACK: 
/* 173 */       return ImageMaster.TINY_CARD_ATTACK;
/*     */     case SKILL: 
/*     */     case STATUS: 
/*     */     case CURSE: 
/* 177 */       return ImageMaster.TINY_CARD_SKILL;
/*     */     case POWER: 
/* 179 */       return ImageMaster.TINY_CARD_POWER;
/*     */     }
/* 181 */     return ImageMaster.TINY_CARD_SKILL;
/*     */   }
/*     */   
/*     */   private void renderTinyCardIcon(SpriteBatch sb, AbstractCard card, float x, float y, float width, float height) {
/* 185 */     sb.setColor(getIconBackgroundColor(card));
/* 186 */     sb.draw(ImageMaster.TINY_CARD_BACKGROUND, x, y, width, height);
/*     */     
/* 188 */     sb.setColor(getIconDescriptionColor(card));
/* 189 */     sb.draw(ImageMaster.TINY_CARD_DESCRIPTION, x, y, width, height);
/*     */     
/* 191 */     sb.setColor(Color.WHITE);
/* 192 */     sb.draw(ImageMaster.TINY_CARD_PORTRAIT_SHADOW, x, y, width, height);
/* 193 */     sb.draw(getIconPortrait(card), x, y, width, height);
/* 194 */     sb.draw(ImageMaster.TINY_CARD_BANNER_SHADOW, x, y, width, height);
/*     */     
/* 196 */     sb.setColor(getIconBannerColor(card));
/* 197 */     sb.draw(ImageMaster.TINY_CARD_BANNER, x, y, width, height);
/*     */   }
/*     */   
/*     */   private static float scaled(float val) {
/* 201 */     return Settings.scale * val;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\runHistory\TinyCard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */