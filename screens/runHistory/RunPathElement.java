/*     */ package com.megacrit.cardcrawl.screens.runHistory;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.PotionHelper;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.stats.BattleStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.CampfireChoice;
/*     */ import com.megacrit.cardcrawl.screens.stats.CardChoiceStats;
/*     */ import com.megacrit.cardcrawl.screens.stats.EventStats;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RunPathElement
/*     */ {
/*     */   private static final boolean SHOW_ROOM_TYPE = true;
/*     */   private static final boolean SHOW_HP = true;
/*     */   private static final boolean SHOW_GOLD = true;
/*     */   private static final boolean SHOW_FIGHT_DETAILS = true;
/*     */   private static final boolean SHOW_EVENT_DETAILS = true;
/*     */   private static final boolean SHOW_EVENT_PLAYER_CHOICE = false;
/*     */   private static final boolean SHOW_CARD_PICK_DETAILS = true;
/*     */   private static final boolean SHOW_CARD_SKIP_INFO = true;
/*     */   private static final boolean SHOW_RELIC_OBTAIN_DETAILS = true;
/*     */   private static final boolean SHOW_POTION_OBTAIN_DETAILS = true;
/*     */   private static final boolean SHOW_CAMPFIRE_CHOICE_DETAILS = true;
/*     */   private static final boolean SHOW_PURCHASE_DETAILS = true;
/*     */   private static final boolean SHOW_ADVANCED_TIP = true;
/*  45 */   private static final float ICON_SIZE = 48.0F * Settings.scale;
/*     */   
/*     */   private static final float ICON_OVERLAP = 0.0F;
/*     */   private static final float ICON_HOVER_SCALE = 1.5F;
/*  49 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("RunHistoryPathNodes");
/*  50 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   private static final String NEW_LINE = " NL ";
/*     */   private static final String TAB = " TAB ";
/*  53 */   private static final String TEXT_ERROR = TEXT[0];
/*  54 */   private static final String TEXT_MONSTER = TEXT[1];
/*  55 */   private static final String TEXT_ELITE = TEXT[2];
/*  56 */   private static final String TEXT_EVENT = TEXT[3];
/*  57 */   private static final String TEXT_BOSS = TEXT[4];
/*  58 */   private static final String TEXT_TREASURE = TEXT[5];
/*  59 */   private static final String TEXT_BOSS_TREASURE = TEXT[6];
/*  60 */   private static final String TEXT_CAMPFIRE = TEXT[7];
/*  61 */   private static final String TEXT_SHOP = TEXT[8];
/*  62 */   private static final String TEXT_UNKNOWN_MONSTER = TEXT[9];
/*  63 */   private static final String TEXT_UNKN0WN_SHOP = TEXT[10];
/*  64 */   private static final String TEXT_UNKNOWN_TREASURE = TEXT[11];
/*  65 */   private static final String TEXT_FLOOR_FORMAT = TEXT[12];
/*  66 */   private static final String TEXT_SIMPLE_FLOOR_FORMAT = TEXT[13];
/*     */   
/*  68 */   private static final String TEXT_DAMAGE_FORMAT = TEXT[14];
/*  69 */   private static final String TEXT_TURNS_FORMAT = TEXT[15];
/*  70 */   private static final String TEXT_HP_FORMAT = TEXT[16];
/*  71 */   private static final String TEXT_GOLD_FORMAT = TEXT[17];
/*     */   
/*  73 */   private static final String TEXT_OBTAIN_HEADER = TEXT[18];
/*  74 */   private static final String TEXT_SKIP_HEADER = TEXT[19];
/*  75 */   private static final String TEXT_MISSING_INFO = TEXT[20];
/*  76 */   private static final String TEXT_SINGING_BOWL_CHOICE = TEXT[21];
/*     */   
/*  78 */   private static final String TEXT_OBTAIN_TYPE_CARD = TEXT[22];
/*  79 */   private static final String TEXT_OBTAIN_TYPE_RELIC = TEXT[23];
/*  80 */   private static final String TEXT_OBTAIN_TYPE_POTION = TEXT[24];
/*  81 */   private static final String TEXT_OBTAIN_TYPE_SPECIAL = TEXT[25];
/*     */   
/*  83 */   private static final String TEXT_REST_OPTION = TEXT[26];
/*  84 */   private static final String TEXT_SMITH_OPTION = TEXT[27];
/*  85 */   private static final String TEXT_TOKE_OPTION = TEXT[28];
/*  86 */   private static final String TEXT_DIG_OPTION = TEXT[29];
/*  87 */   private static final String TEXT_LIFT_OPTION = TEXT[30];
/*  88 */   private static final String TEXT_PURCHASED = TEXT[31];
/*  89 */   private static final String TEXT_REMOVE_OPTION = TEXT_TOKE_OPTION;
/*     */   public Hitbox hb;
/*     */   private PathNodeType nodeType;
/*     */   private int floor;
/*     */   
/*     */   private static enum PathNodeType {
/*  95 */     ERROR,  MONSTER,  ELITE,  EVENT,  BOSS,  TREASURE,  BOSS_TREASURE,  CAMPFIRE,  SHOP,  UNKNOWN_MONSTER,  UNKNOWN_SHOP,  UNKNOWN_TREASURE;
/*     */     
/*     */ 
/*     */     private PathNodeType() {}
/*     */   }
/*     */   
/*     */ 
/*     */   private Integer currentHP;
/*     */   
/*     */   private Integer maxHP;
/*     */   private Integer gold;
/*     */   private BattleStats battleStats;
/*     */   private EventStats eventStats;
/*     */   private CardChoiceStats cardChoiceStats;
/*     */   private List<String> relicsObtained;
/*     */   private List<String> potionsObtained;
/*     */   private CampfireChoice campfireChoice;
/*     */   private List<String> shopPurchases;
/*     */   private List<String> shopPurges;
/* 114 */   private String cachedTooltip = null;
/*     */   
/*     */   public RunPathElement(String roomKey, int floorNum) {
/* 117 */     this.hb = new Hitbox(ICON_SIZE, ICON_SIZE);
/* 118 */     this.nodeType = pathNodeTypeForRoomKey(roomKey);
/* 119 */     this.floor = floorNum;
/*     */   }
/*     */   
/*     */   public void addHpData(Integer current, Integer max) {
/* 123 */     this.currentHP = current;
/* 124 */     this.maxHP = max;
/*     */   }
/*     */   
/*     */   public void addGoldData(Integer gold) {
/* 128 */     this.gold = gold;
/*     */   }
/*     */   
/*     */   public void addBattleData(BattleStats battle) {
/* 132 */     this.battleStats = battle;
/*     */   }
/*     */   
/*     */   public void addEventData(EventStats event) {
/* 136 */     this.eventStats = event;
/*     */   }
/*     */   
/*     */   public void addCardChoiceData(CardChoiceStats choice) {
/* 140 */     this.cardChoiceStats = choice;
/*     */   }
/*     */   
/*     */   public void addRelicObtainStats(List<String> relicKeys) {
/* 144 */     this.relicsObtained = relicKeys;
/*     */   }
/*     */   
/*     */   public void addRelicObtainStats(String relicKey) {
/* 148 */     addRelicObtainStats(Arrays.asList(new String[] { relicKey }));
/*     */   }
/*     */   
/*     */   public void addPotionObtainStats(List<String> potionKey) {
/* 152 */     this.potionsObtained = potionKey;
/*     */   }
/*     */   
/*     */   public void addCampfireChoiceData(CampfireChoice choice) {
/* 156 */     this.campfireChoice = choice;
/*     */   }
/*     */   
/*     */   public void addShopPurchaseData(ArrayList<String> keys) {
/* 160 */     this.shopPurchases = keys;
/*     */   }
/*     */   
/*     */   public void addPurgeData(ArrayList<String> keys) {
/* 164 */     this.shopPurges = keys;
/*     */   }
/*     */   
/*     */   public void update() {
/* 168 */     this.hb.update();
/* 169 */     if (this.hb.hovered) {
/* 170 */       float tipX = this.hb.x + 64.0F * Settings.scale;
/* 171 */       float tipY = this.hb.y + ICON_SIZE / 2.0F;
/*     */       
/* 173 */       String header = String.format(TEXT_SIMPLE_FLOOR_FORMAT, new Object[] { Integer.valueOf(this.floor) });
/* 174 */       TipHelper.renderGenericTip(tipX, tipY, header, getTipDescriptionText());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String getTipHeaderWithRoomTypeText()
/*     */   {
/* 184 */     return String.format(TEXT_FLOOR_FORMAT, new Object[] { Integer.toString(this.floor), stringForType() });
/*     */   }
/*     */   
/*     */   private String getTipDescriptionText() {
/* 188 */     if (this.cachedTooltip != null) {
/* 189 */       return this.cachedTooltip;
/*     */     }
/*     */     
/* 192 */     StringBuilder sb = new StringBuilder();
/*     */     
/*     */ 
/*     */ 
/* 196 */     sb.append(stringForType());
/*     */     
/*     */ 
/*     */ 
/* 200 */     boolean displayHP = (this.currentHP != null) && (this.maxHP != null);
/* 201 */     if (displayHP) {
/* 202 */       if (sb.length() > 0) {
/* 203 */         sb.append(" NL ");
/*     */       }
/* 205 */       sb.append(String.format(TEXT_HP_FORMAT, new Object[] { this.currentHP, this.maxHP }));
/* 206 */       sb.append(" TAB ");
/*     */     }
/*     */     
/*     */ 
/* 210 */     boolean displayGold = this.gold != null;
/* 211 */     if (displayGold) {
/* 212 */       sb.append(String.format(TEXT_GOLD_FORMAT, new Object[] { this.gold }));
/*     */     }
/*     */     
/*     */ 
/* 216 */     if (this.eventStats != null) {
/* 217 */       if (sb.length() > 0) {
/* 218 */         sb.append(" NL ");
/*     */       }
/* 220 */       sb.append(localizedEventNameForKey(this.eventStats.event_name));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 228 */     if (this.battleStats != null) {
/* 229 */       if (sb.length() > 0) {
/* 230 */         sb.append(" NL ");
/*     */       }
/* 232 */       sb.append(localizedEnemyNameForKey(this.battleStats.enemies));
/* 233 */       sb.append(" NL ");
/* 234 */       sb.append(" TAB ").append(String.format(TEXT_DAMAGE_FORMAT, new Object[] { Integer.valueOf(this.battleStats.damage) }));
/* 235 */       sb.append(" NL ");
/* 236 */       sb.append(" TAB ").append(String.format(TEXT_TURNS_FORMAT, new Object[] { Integer.valueOf(this.battleStats.turns) }));
/*     */     }
/*     */     
/* 239 */     if (this.campfireChoice != null) {
/* 240 */       if (sb.length() > 0) {
/* 241 */         sb.append(" NL ");
/*     */       }
/* 243 */       switch (this.campfireChoice.key) {
/*     */       case "REST": 
/* 245 */         sb.append(TEXT_REST_OPTION);
/* 246 */         break;
/*     */       case "SMITH": 
/* 248 */         sb.append(
/* 249 */           String.format(TEXT_SMITH_OPTION, new Object[] {CardLibrary.getCardNameFromMetricID(this.campfireChoice.data) }));
/* 250 */         break;
/*     */       case "PURGE": 
/* 252 */         sb.append(
/* 253 */           String.format(TEXT_TOKE_OPTION, new Object[] {CardLibrary.getCardNameFromMetricID(this.campfireChoice.data) }));
/* 254 */         break;
/*     */       case "DIG": 
/* 256 */         sb.append(TEXT_DIG_OPTION);
/* 257 */         break;
/*     */       case "LIFT": 
/* 259 */         sb.append(String.format(TEXT_LIFT_OPTION, new Object[] { this.campfireChoice.data, Integer.valueOf(3) }));
/* 260 */         break;
/*     */       default: 
/* 262 */         sb.append(TEXT_MISSING_INFO);
/*     */       }
/*     */       
/*     */     }
/*     */     
/* 267 */     boolean showRelic = this.relicsObtained != null;
/* 268 */     boolean showPotion = this.potionsObtained != null;
/* 269 */     boolean showCards = (this.cardChoiceStats != null) && (!this.cardChoiceStats.picked.equals("SKIP"));
/*     */     
/* 271 */     if ((showRelic) || (showPotion) || (showCards)) {
/* 272 */       sb.append(" NL ").append(TEXT_OBTAIN_HEADER);
/*     */     }
/*     */     
/*     */ 
/* 276 */     if (showRelic) {
/* 277 */       if (sb.length() > 0) {
/* 278 */         sb.append(" NL ");
/*     */       }
/* 280 */       for (int i = 0; i < this.relicsObtained.size(); i++) {
/* 281 */         String name = RelicLibrary.getRelic((String)this.relicsObtained.get(i)).name;
/* 282 */         if (i > 0) {
/* 283 */           sb.append(" NL ");
/*     */         }
/* 285 */         sb.append(" TAB ").append(TEXT_OBTAIN_TYPE_RELIC).append(name);
/*     */       }
/*     */     }
/*     */     
/*     */     String text;
/* 290 */     if ((showCards) && 
/* 291 */       (!this.cardChoiceStats.picked.isEmpty()) && (!this.cardChoiceStats.picked.equals("SKIP"))) {
/*     */       String text;
/* 293 */       if (this.cardChoiceStats.picked.equals("Singing Bowl")) {
/* 294 */         text = TEXT_OBTAIN_TYPE_SPECIAL + TEXT_SINGING_BOWL_CHOICE;
/*     */       } else {
/* 296 */         text = TEXT_OBTAIN_TYPE_CARD + CardLibrary.getCardNameFromMetricID(this.cardChoiceStats.picked);
/*     */       }
/* 298 */       if (sb.length() > 0) {
/* 299 */         sb.append(" NL ");
/*     */       }
/* 301 */       sb.append(" TAB ").append(text);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 306 */     if (showPotion) {
/* 307 */       if (sb.length() > 0) {
/* 308 */         sb.append(" NL ");
/*     */       }
/* 310 */       for (String key : this.potionsObtained) {
/* 311 */         sb.append(" TAB ").append(TEXT_OBTAIN_TYPE_POTION).append(PotionHelper.getPotion(key).name);
/*     */       }
/*     */     }
/*     */     
/*     */     int i;
/* 316 */     if (this.cardChoiceStats != null) {
/* 317 */       sb.append(" NL ");
/* 318 */       sb.append(TEXT_SKIP_HEADER);
/* 319 */       sb.append(" NL ");
/*     */       
/* 321 */       for (i = 0; i < this.cardChoiceStats.not_picked.size(); i++) {
/* 322 */         String cardID = (String)this.cardChoiceStats.not_picked.get(i);
/* 323 */         String cardName = CardLibrary.getCardNameFromMetricID(cardID);
/* 324 */         sb.append(" TAB ").append(TEXT_OBTAIN_TYPE_CARD).append(cardName);
/* 325 */         if (i < this.cardChoiceStats.not_picked.size() - 1) {
/* 326 */           sb.append(" NL ");
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 332 */     if (this.shopPurchases != null) {
/* 333 */       sb.append(" NL ").append(TEXT_PURCHASED);
/* 334 */       for (String key : this.shopPurchases)
/*     */       {
/* 336 */         String text = null;
/* 337 */         if (CardLibrary.isACard(key)) {
/* 338 */           text = TEXT_OBTAIN_TYPE_CARD + CardLibrary.getCardNameFromMetricID(key);
/* 339 */         } else if (RelicLibrary.isARelic(key)) {
/* 340 */           text = TEXT_OBTAIN_TYPE_RELIC + RelicLibrary.getRelic(key).name;
/* 341 */         } else if (PotionHelper.isAPotion(key)) {
/* 342 */           text = TEXT_OBTAIN_TYPE_POTION + PotionHelper.getPotion(key).name;
/*     */         }
/*     */         
/* 345 */         if (text != null) {
/* 346 */           sb.append(" NL ").append(" TAB ").append(text);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 351 */     if (this.shopPurges != null) {
/* 352 */       for (String key : this.shopPurges) {
/* 353 */         sb.append(" NL ").append(String.format(TEXT_REMOVE_OPTION, new Object[] { CardLibrary.getCardNameFromMetricID(key) }));
/*     */       }
/*     */     }
/*     */     
/* 357 */     if (sb.length() > 0) {
/* 358 */       this.cachedTooltip = sb.toString();
/* 359 */       return this.cachedTooltip;
/*     */     }
/* 361 */     return TEXT_MISSING_INFO;
/*     */   }
/*     */   
/*     */   public boolean isHovered() {
/* 365 */     return this.hb.hovered;
/*     */   }
/*     */   
/*     */   public void position(float x, float y) {
/* 369 */     this.hb.move(x, y - ICON_SIZE);
/*     */   }
/*     */   
/*     */   public static float getApproximateWidth() {
/* 373 */     return ICON_SIZE - 0.0F;
/*     */   }
/*     */   
/*     */   public static float getApproximateHeight() {
/* 377 */     return ICON_SIZE;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 381 */     Texture image = imageForType(this.nodeType);
/* 382 */     if (isHovered()) {
/* 383 */       float hoverSize = ICON_SIZE * 1.5F;
/* 384 */       float offset = (hoverSize - ICON_SIZE) / 2.0F;
/* 385 */       sb.draw(image, this.hb.x - offset, this.hb.y - offset, hoverSize, hoverSize);
/*     */     } else {
/* 387 */       sb.draw(image, this.hb.x, this.hb.y, ICON_SIZE, ICON_SIZE);
/*     */     }
/* 389 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public String localizedEnemyNameForKey(String enemyId)
/*     */   {
/* 394 */     return enemyId;
/*     */   }
/*     */   
/*     */   public String localizedEventNameForKey(String eventId)
/*     */   {
/* 399 */     return eventId;
/*     */   }
/*     */   
/*     */   public PathNodeType pathNodeTypeForRoomKey(String roomKey) {
/* 403 */     switch (roomKey) {
/*     */     case "M": 
/* 405 */       return PathNodeType.MONSTER;
/*     */     case "E": 
/* 407 */       return PathNodeType.ELITE;
/*     */     case "?": 
/* 409 */       return PathNodeType.EVENT;
/*     */     case "B": 
/*     */     case "BOSS": 
/* 412 */       return PathNodeType.BOSS;
/*     */     case "T": 
/* 414 */       return PathNodeType.TREASURE;
/*     */     case "T!": 
/* 416 */       return PathNodeType.BOSS_TREASURE;
/*     */     case "R": 
/* 418 */       return PathNodeType.CAMPFIRE;
/*     */     case "$": 
/* 420 */       return PathNodeType.SHOP;
/*     */     case "?(M)": 
/* 422 */       return PathNodeType.UNKNOWN_MONSTER;
/*     */     case "?($)": 
/* 424 */       return PathNodeType.UNKNOWN_SHOP;
/*     */     case "?(T)": 
/* 426 */       return PathNodeType.UNKNOWN_TREASURE;
/*     */     
/*     */     case "?(_)": 
/* 429 */       return PathNodeType.EVENT;
/*     */     }
/* 431 */     return PathNodeType.ERROR;
/*     */   }
/*     */   
/*     */   public Texture imageForType(PathNodeType nodeType)
/*     */   {
/* 436 */     switch (nodeType) {
/*     */     case MONSTER: 
/* 438 */       return ImageMaster.RUN_HISTORY_MAP_ICON_MONSTER;
/*     */     case ELITE: 
/* 440 */       return ImageMaster.RUN_HISTORY_MAP_ICON_ELITE;
/*     */     case EVENT: 
/* 442 */       return ImageMaster.RUN_HISTORY_MAP_ICON_EVENT;
/*     */     case BOSS: 
/* 444 */       return ImageMaster.RUN_HISTORY_MAP_ICON_BOSS;
/*     */     case TREASURE: 
/* 446 */       return ImageMaster.RUN_HISTORY_MAP_ICON_CHEST;
/*     */     case BOSS_TREASURE: 
/* 448 */       return ImageMaster.RUN_HISTORY_MAP_ICON_BOSS_CHEST;
/*     */     case CAMPFIRE: 
/* 450 */       return ImageMaster.RUN_HISTORY_MAP_ICON_REST;
/*     */     case SHOP: 
/* 452 */       return ImageMaster.RUN_HISTORY_MAP_ICON_SHOP;
/*     */     case UNKNOWN_MONSTER: 
/* 454 */       return ImageMaster.RUN_HISTORY_MAP_ICON_UNKNOWN_MONSTER;
/*     */     case UNKNOWN_SHOP: 
/* 456 */       return ImageMaster.RUN_HISTORY_MAP_ICON_UNKNOWN_SHOP;
/*     */     case UNKNOWN_TREASURE: 
/* 458 */       return ImageMaster.RUN_HISTORY_MAP_ICON_UNKNOWN_CHEST;
/*     */     }
/* 460 */     return ImageMaster.RUN_HISTORY_MAP_ICON_EVENT;
/*     */   }
/*     */   
/*     */   private String stringForType()
/*     */   {
/* 465 */     return stringForType(this.nodeType);
/*     */   }
/*     */   
/*     */   private String stringForType(PathNodeType type) {
/* 469 */     switch (type) {
/*     */     case MONSTER: 
/* 471 */       return TEXT_MONSTER;
/*     */     case ELITE: 
/* 473 */       return TEXT_ELITE;
/*     */     case EVENT: 
/* 475 */       return TEXT_EVENT;
/*     */     case BOSS: 
/* 477 */       return TEXT_BOSS;
/*     */     case TREASURE: 
/* 479 */       return TEXT_TREASURE;
/*     */     case BOSS_TREASURE: 
/* 481 */       return TEXT_BOSS_TREASURE;
/*     */     case CAMPFIRE: 
/* 483 */       return TEXT_CAMPFIRE;
/*     */     case SHOP: 
/* 485 */       return TEXT_SHOP;
/*     */     case UNKNOWN_MONSTER: 
/* 487 */       return TEXT_UNKNOWN_MONSTER;
/*     */     case UNKNOWN_SHOP: 
/* 489 */       return TEXT_UNKN0WN_SHOP;
/*     */     case UNKNOWN_TREASURE: 
/* 491 */       return TEXT_UNKNOWN_TREASURE;
/*     */     }
/* 493 */     return TEXT_ERROR;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\runHistory\RunPathElement.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */