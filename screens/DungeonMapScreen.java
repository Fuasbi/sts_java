/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.map.DungeonMap;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DungeonMapScreen
/*     */ {
/*  30 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DungeonMapScreen");
/*  31 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  33 */   public DungeonMap map = new DungeonMap();
/*     */   
/*     */ 
/*  36 */   private ArrayList<MapRoomNode> visibleMapNodes = new ArrayList();
/*  37 */   public boolean dismissable = false;
/*  38 */   private static final float MAP_SCROLL_UPPER = -2290.0F * Settings.scale;
/*  39 */   private static final float MAP_SCROLL_LOWER = 190.0F * Settings.scale;
/*  40 */   public static final float ICON_SPACING_Y = 120.0F * Settings.scale;
/*  41 */   public static float offsetY = -100.0F * Settings.scale;
/*  42 */   private float targetOffsetY = offsetY;
/*  43 */   private float grabStartY = 0.0F;
/*  44 */   private boolean grabbedScreen = false;
/*  45 */   public boolean clicked = false;
/*  46 */   public float clickTimer = 0.0F;
/*     */   
/*     */   private float clickStartX;
/*     */   private float clickStartY;
/*  50 */   private float scrollWaitTimer = 0.0F;
/*     */   private static final float SCROLL_WAIT_TIME = 1.0F;
/*     */   private static final float SPECIAL_ANIMATE_TIME = 3.0F;
/*     */   private float oscillatingTimer;
/*  54 */   private float oscillatingFader; private Color oscillatingColor = Settings.GOLD_COLOR.cpy();
/*     */   
/*     */ 
/*  57 */   private float scrollBackTimer = 0.0F;
/*  58 */   public Hitbox mapNodeHb = null;
/*  59 */   private static final float RETICLE_DIST = 20.0F * Settings.scale;
/*     */   private static final int RETICLE_W = 36;
/*     */   
/*     */   public DungeonMapScreen() {
/*  63 */     this.oscillatingFader = 0.0F;
/*  64 */     this.oscillatingTimer = 0.0F;
/*  65 */     this.oscillatingColor.a = 0.0F;
/*     */   }
/*     */   
/*     */   public void update() {
/*  69 */     if ((this.scrollWaitTimer < 0.0F) && (Settings.isControllerMode) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && (!this.map.legend.isLegendHighlighted))
/*     */     {
/*  71 */       if (this.scrollBackTimer > 0.0F) {
/*  72 */         this.scrollBackTimer -= Gdx.graphics.getDeltaTime();
/*  73 */         if (Gdx.input.getY() > Settings.HEIGHT * 0.85F) {
/*  74 */           this.targetOffsetY += Settings.SCROLL_SPEED * 2.0F;
/*  75 */         } else if (Gdx.input.getY() < Settings.HEIGHT * 0.15F) {
/*  76 */           this.targetOffsetY -= Settings.SCROLL_SPEED * 2.0F;
/*     */         }
/*     */         
/*  79 */         if (this.targetOffsetY > MAP_SCROLL_LOWER) {
/*  80 */           this.targetOffsetY = MAP_SCROLL_LOWER;
/*  81 */         } else if (this.targetOffsetY < MAP_SCROLL_UPPER) {
/*  82 */           this.targetOffsetY = MAP_SCROLL_UPPER;
/*     */         }
/*  84 */         offsetY = MathUtils.lerp(offsetY, this.targetOffsetY, Gdx.graphics
/*     */         
/*     */ 
/*  87 */           .getDeltaTime() * 12.0F);
/*     */       }
/*     */     }
/*     */     
/*  91 */     this.map.update();
/*     */     
/*  93 */     if (AbstractDungeon.isScreenUp) {
/*  94 */       for (MapRoomNode n : this.visibleMapNodes) {
/*  95 */         n.update();
/*     */       }
/*     */     }
/*     */     
/*  99 */     if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && (!this.dismissable) && (this.scrollWaitTimer < 0.0F)) {
/* 100 */       oscillateColor();
/*     */     }
/*     */     
/* 103 */     for (com.megacrit.cardcrawl.vfx.AbstractGameEffect e : AbstractDungeon.topLevelEffects) {
/* 104 */       if ((e instanceof com.megacrit.cardcrawl.vfx.MapCircleEffect)) {
/* 105 */         return;
/*     */       }
/*     */     }
/* 108 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) {
/* 109 */       updateYOffset();
/*     */     }
/*     */     
/* 112 */     updateMouse();
/* 113 */     updateControllerInput();
/*     */     
/* 115 */     if ((Settings.isControllerMode) && (this.mapNodeHb != null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP)) {
/* 116 */       Gdx.input.setCursorPosition((int)this.mapNodeHb.cX, (int)(Settings.HEIGHT - this.mapNodeHb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMouse() {
/* 121 */     if (this.clicked) {
/* 122 */       this.clicked = false;
/*     */     }
/*     */     
/* 125 */     if ((InputHelper.justReleasedClickLeft) && (this.clickTimer < 0.4F) && (com.badlogic.gdx.math.Vector2.dst(this.clickStartX, this.clickStartY, InputHelper.mX, InputHelper.mY) < Settings.CLICK_DIST_THRESHOLD))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 130 */       this.clicked = true;
/*     */     }
/*     */     
/* 133 */     if ((InputHelper.justClickedLeft) || ((CInputActionSet.select.isJustPressed()) && (AbstractDungeon.topPanel.potionUi.isHidden) && (!AbstractDungeon.topPanel.selectPotionMode)))
/*     */     {
/* 135 */       this.clickTimer = 0.0F;
/* 136 */       this.clickStartX = InputHelper.mX;
/* 137 */       this.clickStartY = InputHelper.mY;
/* 138 */     } else if (InputHelper.isMouseDown) {
/* 139 */       this.clickTimer += Gdx.graphics.getDeltaTime();
/*     */     }
/*     */     
/* 142 */     if ((CInputActionSet.select.isJustPressed()) && (this.clickTimer < 0.4F) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP))
/*     */     {
/* 144 */       this.clicked = true;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 149 */     if ((this.scrollWaitTimer > 0.0F) || (!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode) || (!AbstractDungeon.topPanel.potionUi.isHidden) || (this.map.legend.isLegendHighlighted) || (AbstractDungeon.player.viewingRelics))
/*     */     {
/*     */ 
/* 152 */       this.mapNodeHb = null;
/* 153 */       return;
/*     */     }
/*     */     
/* 156 */     if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 157 */       this.targetOffsetY += Settings.SCROLL_SPEED * 4.0F;
/* 158 */       return; }
/* 159 */     if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 160 */       this.targetOffsetY -= Settings.SCROLL_SPEED * 4.0F;
/* 161 */       return;
/*     */     }
/*     */     
/* 164 */     if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/* 165 */       (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 166 */       this.scrollBackTimer = 0.1F;
/*     */     }
/*     */     
/* 169 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) {
/* 170 */       ArrayList<MapRoomNode> nodes = new ArrayList();
/* 171 */       if (!AbstractDungeon.firstRoomChosen) {
/* 172 */         for (MapRoomNode n : this.visibleMapNodes) {
/* 173 */           if (n.y == 0) {
/* 174 */             nodes.add(n);
/*     */           }
/*     */         }
/*     */       } else {
/* 178 */         for (MapRoomNode n : this.visibleMapNodes) {
/* 179 */           if (AbstractDungeon.currMapNode.isConnectedTo(n)) {
/* 180 */             nodes.add(n);
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 185 */       boolean anyHovered = false;
/* 186 */       int index = 0;
/* 187 */       for (MapRoomNode n : nodes) {
/* 188 */         if (n.hb.hovered) {
/* 189 */           anyHovered = true;
/* 190 */           break;
/*     */         }
/* 192 */         index++;
/*     */       }
/*     */       
/* 195 */       if (!anyHovered) {
/* 196 */         if (!nodes.isEmpty()) {
/* 197 */           Gdx.input.setCursorPosition(
/* 198 */             (int)((MapRoomNode)nodes.get(nodes.size() / 2)).hb.cX, Settings.HEIGHT - 
/* 199 */             (int)((MapRoomNode)nodes.get(nodes.size() / 2)).hb.cY);
/* 200 */           this.mapNodeHb = ((MapRoomNode)nodes.get(nodes.size() / 2)).hb;
/*     */         }
/*     */         else {
/* 203 */           Gdx.input.setCursorPosition((int)AbstractDungeon.dungeonMapScreen.map.bossHb.cX, Settings.HEIGHT - (int)AbstractDungeon.dungeonMapScreen.map.bossHb.cY);
/*     */           
/*     */ 
/* 206 */           this.mapNodeHb = null;
/*     */         }
/*     */       }
/* 209 */       else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 210 */         index--;
/* 211 */         if (index < 0) {
/* 212 */           index = nodes.size() - 1;
/*     */         }
/* 214 */         Gdx.input.setCursorPosition(
/* 215 */           (int)((MapRoomNode)nodes.get(index)).hb.cX, Settings.HEIGHT - 
/* 216 */           (int)((MapRoomNode)nodes.get(index)).hb.cY);
/* 217 */         this.mapNodeHb = ((MapRoomNode)nodes.get(index)).hb;
/* 218 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 219 */         index++;
/* 220 */         if (index > nodes.size() - 1) {
/* 221 */           index = 0;
/*     */         }
/* 223 */         Gdx.input.setCursorPosition(
/* 224 */           (int)((MapRoomNode)nodes.get(index)).hb.cX, Settings.HEIGHT - 
/* 225 */           (int)((MapRoomNode)nodes.get(index)).hb.cY);
/* 226 */         this.mapNodeHb = ((MapRoomNode)nodes.get(index)).hb;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateYOffset()
/*     */   {
/* 234 */     if (this.grabbedScreen) {
/* 235 */       if (InputHelper.isMouseDown) {
/* 236 */         this.targetOffsetY = (InputHelper.mY - this.grabStartY);
/*     */       } else {
/* 238 */         this.grabbedScreen = false;
/*     */       }
/* 240 */     } else if (this.scrollWaitTimer < 0.0F) {
/* 241 */       if (InputHelper.scrolledDown) {
/* 242 */         this.targetOffsetY += Settings.MAP_SCROLL_SPEED;
/* 243 */       } else if (InputHelper.scrolledUp) {
/* 244 */         this.targetOffsetY -= Settings.MAP_SCROLL_SPEED;
/*     */       }
/* 246 */       if ((InputHelper.justClickedLeft) && (this.scrollWaitTimer < 0.0F)) {
/* 247 */         this.grabbedScreen = true;
/* 248 */         this.grabStartY = (InputHelper.mY - this.targetOffsetY);
/*     */       }
/*     */     }
/*     */     
/* 252 */     resetScrolling();
/* 253 */     updateAnimation();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 260 */     if (this.targetOffsetY < MAP_SCROLL_UPPER) {
/* 261 */       this.targetOffsetY = MathHelper.scrollSnapLerpSpeed(this.targetOffsetY, MAP_SCROLL_UPPER);
/* 262 */     } else if (this.targetOffsetY > MAP_SCROLL_LOWER) {
/* 263 */       this.targetOffsetY = MathHelper.scrollSnapLerpSpeed(this.targetOffsetY, MAP_SCROLL_LOWER);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateAnimation() {
/* 268 */     this.scrollWaitTimer -= Gdx.graphics.getDeltaTime();
/* 269 */     if (this.scrollWaitTimer < 0.0F) {
/* 270 */       offsetY = MathUtils.lerp(offsetY, this.targetOffsetY, Gdx.graphics.getDeltaTime() * 12.0F);
/* 271 */     } else if (this.scrollWaitTimer < 3.0F) {
/* 272 */       offsetY = com.badlogic.gdx.math.Interpolation.exp10.apply(MAP_SCROLL_LOWER, MAP_SCROLL_UPPER, this.scrollWaitTimer / 3.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void updateImage()
/*     */   {
/* 283 */     this.visibleMapNodes.clear();
/*     */     
/*     */ 
/* 286 */     for (ArrayList<MapRoomNode> rows : CardCrawlGame.dungeon.getMap()) {
/* 287 */       for (MapRoomNode node : rows)
/*     */       {
/* 289 */         if (node.hasEdges()) {
/* 290 */           this.visibleMapNodes.add(node);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 297 */     this.map.render(sb);
/*     */     
/* 299 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP)
/*     */     {
/* 301 */       for (MapRoomNode n : this.visibleMapNodes) {
/* 302 */         n.render(sb);
/*     */       }
/*     */     }
/*     */     
/* 306 */     this.map.renderBossIcon(sb);
/*     */     
/* 308 */     if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && (!this.dismissable) && (this.scrollWaitTimer < 0.0F)) {
/* 309 */       com.megacrit.cardcrawl.helpers.FontHelper.renderDeckViewTip(sb, TEXT[0], 80.0F * Settings.scale, this.oscillatingColor);
/*     */     }
/*     */     
/* 312 */     renderControllerUi(sb);
/*     */   }
/*     */   
/*     */   private void renderControllerUi(SpriteBatch sb) {
/* 316 */     if (!Settings.isControllerMode) {
/* 317 */       return;
/*     */     }
/*     */     
/* 320 */     if (this.mapNodeHb != null) {
/* 321 */       renderReticle(sb, this.mapNodeHb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void oscillateColor() {
/* 326 */     this.oscillatingFader += Gdx.graphics.getDeltaTime();
/* 327 */     if (this.oscillatingFader > 1.0F) {
/* 328 */       this.oscillatingFader = 1.0F;
/* 329 */       this.oscillatingTimer += Gdx.graphics.getDeltaTime() * 5.0F;
/*     */     }
/* 331 */     this.oscillatingColor.a = ((0.33F + (MathUtils.cos(this.oscillatingTimer) + 1.0F) / 3.0F) * this.oscillatingFader);
/*     */   }
/*     */   
/*     */   public void open(boolean doScrollingAnimation) {
/* 335 */     AbstractDungeon.player.releaseCard();
/* 336 */     this.map.legend.isLegendHighlighted = false;
/* 337 */     if (Settings.isDebug) {
/* 338 */       doScrollingAnimation = false;
/*     */     }
/*     */     
/* 341 */     InputHelper.justClickedLeft = false;
/* 342 */     this.clicked = false;
/* 343 */     this.clickTimer = 999.0F;
/* 344 */     this.grabbedScreen = false;
/*     */     
/* 346 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 347 */     this.map.show();
/* 348 */     this.dismissable = (!doScrollingAnimation);
/* 349 */     if (MathUtils.randomBoolean()) {
/* 350 */       CardCrawlGame.sound.play("MAP_OPEN", 0.1F);
/*     */     } else {
/* 352 */       CardCrawlGame.sound.play("MAP_OPEN_2", 0.1F);
/*     */     }
/*     */     
/*     */ 
/* 356 */     if (doScrollingAnimation) {
/* 357 */       this.mapNodeHb = null;
/* 358 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.scene.LevelTransitionTextOverlayEffect(AbstractDungeon.name, AbstractDungeon.levelNum));
/*     */       
/* 360 */       this.scrollWaitTimer = 4.0F;
/* 361 */       offsetY = MAP_SCROLL_UPPER;
/* 362 */       this.targetOffsetY = MAP_SCROLL_LOWER;
/*     */     } else {
/* 364 */       this.scrollWaitTimer = 0.0F;
/* 365 */       AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/* 366 */       if (AbstractDungeon.getCurrMapNode() == null) {
/* 367 */         offsetY = MAP_SCROLL_UPPER;
/*     */       } else {
/* 369 */         offsetY = -50.0F * Settings.scale + AbstractDungeon.getCurrMapNode().y * -ICON_SPACING_Y;
/*     */       }
/* 371 */       this.targetOffsetY = offsetY;
/*     */     }
/*     */     
/* 374 */     AbstractDungeon.dynamicBanner.hide();
/* 375 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.MAP;
/* 376 */     AbstractDungeon.isScreenUp = true;
/* 377 */     this.grabStartY = 0.0F;
/*     */     
/* 379 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 380 */     AbstractDungeon.overlayMenu.hideCombatPanels();
/* 381 */     AbstractDungeon.overlayMenu.endTurnButton.hide();
/* 382 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 383 */     updateImage();
/*     */   }
/*     */   
/*     */   public void close() {
/* 387 */     this.map.hide();
/* 388 */     AbstractDungeon.overlayMenu.cancelButton.hide();
/* 389 */     this.clicked = false;
/*     */   }
/*     */   
/*     */   public void closeInstantly() {
/* 393 */     this.map.hideInstantly();
/* 394 */     AbstractDungeon.overlayMenu.cancelButton.hideInstantly();
/* 395 */     this.clicked = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderReticle(SpriteBatch sb, Hitbox hb)
/*     */   {
/* 405 */     renderReticleCorner(sb, -hb.width / 2.0F - RETICLE_DIST, hb.height / 2.0F + RETICLE_DIST, hb, false, false);
/* 406 */     renderReticleCorner(sb, hb.width / 2.0F + RETICLE_DIST, hb.height / 2.0F + RETICLE_DIST, hb, true, false);
/* 407 */     renderReticleCorner(sb, -hb.width / 2.0F - RETICLE_DIST, -hb.height / 2.0F - RETICLE_DIST, hb, false, true);
/* 408 */     renderReticleCorner(sb, hb.width / 2.0F + RETICLE_DIST, -hb.height / 2.0F - RETICLE_DIST, hb, true, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderReticleCorner(SpriteBatch sb, float x, float y, Hitbox hb, boolean flipX, boolean flipY)
/*     */   {
/* 422 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.map.targetAlpha / 4.0F));
/* 423 */     sb.draw(ImageMaster.RETICLE_CORNER, hb.cX + x - 18.0F + 4.0F * Settings.scale, hb.cY + y - 18.0F - 4.0F * Settings.scale, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 442 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.map.targetAlpha));
/* 443 */     sb.draw(ImageMaster.RETICLE_CORNER, hb.cX + x - 18.0F, hb.cY + y - 18.0F, 18.0F, 18.0F, 36.0F, 36.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 36, 36, flipX, flipY);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DungeonMapScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */