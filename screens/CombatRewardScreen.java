/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.GameTips;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem.RewardType;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.TreasureRoom;
/*     */ import com.megacrit.cardcrawl.ui.buttons.DynamicBanner;
/*     */ import com.megacrit.cardcrawl.ui.buttons.DynamicButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class CombatRewardScreen
/*     */ {
/*  33 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CombatRewardScreen");
/*  34 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  36 */   public ArrayList<RewardItem> rewards = new ArrayList();
/*  37 */   public ArrayList<AbstractGameEffect> effects = new ArrayList();
/*  38 */   public boolean hasTakenAll = false;
/*  39 */   private String labelOverride = null;
/*  40 */   private boolean mug = false; private boolean smoke = false;
/*     */   
/*     */   private static final float REWARD_ANIM_TIME = 0.2F;
/*     */   
/*  44 */   private float rewardAnimTimer = 0.2F;
/*  45 */   private Color uiColor = Color.BLACK.cpy();
/*     */   private static final int SHEET_W = 612;
/*  47 */   private static final int SHEET_H = 716; private static final Color TIP_COLOR = Color.valueOf("a7b0b8ff");
/*     */   private String tip;
/*     */   
/*     */   public void update() {
/*  51 */     if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (Settings.isDebug)) {
/*  52 */       this.tip = CardCrawlGame.tips.getTip();
/*     */     }
/*     */     
/*  55 */     rewardViewUpdate();
/*  56 */     updateEffects();
/*     */   }
/*     */   
/*     */   private void updateEffects()
/*     */   {
/*  61 */     for (Iterator<AbstractGameEffect> i = this.effects.iterator(); i.hasNext();) {
/*  62 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/*  63 */       e.update();
/*  64 */       if (e.isDone) {
/*  65 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void setupItemReward() {
/*  71 */     this.rewardAnimTimer = 0.2F;
/*  72 */     com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*  73 */     this.rewards = new ArrayList(AbstractDungeon.getCurrRoom().rewards);
/*     */     
/*  75 */     if (((AbstractDungeon.getCurrRoom().event == null) || ((AbstractDungeon.getCurrRoom().event != null) && 
/*  76 */       (!AbstractDungeon.getCurrRoom().event.noCardsInRewards))) && 
/*  77 */       (!(AbstractDungeon.getCurrRoom() instanceof TreasureRoom)) && 
/*  78 */       (!(AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.RestRoom))) {
/*  79 */       if ((((Boolean)com.megacrit.cardcrawl.daily.DailyMods.negativeMods.get("Vintage")).booleanValue()) && ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoom))) {
/*  80 */         if (((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomElite)) || 
/*  81 */           ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss))) {
/*  82 */           RewardItem cardReward = new RewardItem();
/*  83 */           if (cardReward.cards.size() > 0) {
/*  84 */             this.rewards.add(cardReward);
/*     */           }
/*     */         }
/*     */       } else {
/*  88 */         RewardItem cardReward = new RewardItem();
/*  89 */         if (cardReward.cards.size() > 0) {
/*  90 */           this.rewards.add(cardReward);
/*     */         }
/*  92 */         if (((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoom)) && (AbstractDungeon.player.hasRelic("Prayer Wheel")) && 
/*  93 */           (!(AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomElite)) && 
/*  94 */           (!(AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss))) {
/*  95 */           cardReward = new RewardItem();
/*  96 */           if (cardReward.cards.size() > 0) {
/*  97 */             this.rewards.add(cardReward);
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 104 */     AbstractDungeon.dynamicButton.hide();
/* 105 */     AbstractDungeon.overlayMenu.proceedButton.show();
/* 106 */     this.hasTakenAll = false;
/* 107 */     positionRewards();
/*     */   }
/*     */   
/*     */   public void positionRewards() {
/* 111 */     for (int i = 0; i < this.rewards.size(); i++) {
/* 112 */       ((RewardItem)this.rewards.get(i)).move(Settings.HEIGHT - 410.0F * Settings.scale - i * 100.0F * Settings.scale);
/*     */     }
/* 114 */     if (this.rewards.isEmpty()) {
/* 115 */       this.hasTakenAll = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void rewardViewUpdate()
/*     */   {
/* 122 */     if (this.rewardAnimTimer != 0.0F) {
/* 123 */       this.rewardAnimTimer -= Gdx.graphics.getDeltaTime();
/* 124 */       if (this.rewardAnimTimer < 0.0F) {
/* 125 */         this.rewardAnimTimer = 0.0F;
/*     */       }
/* 127 */       this.uiColor.r = (1.0F - this.rewardAnimTimer / 0.2F);
/* 128 */       this.uiColor.g = (1.0F - this.rewardAnimTimer / 0.2F);
/* 129 */       this.uiColor.b = (1.0F - this.rewardAnimTimer / 0.2F);
/*     */     }
/*     */     
/* 132 */     updateControllerInput();
/*     */     
/* 134 */     boolean removedSomething = false;
/* 135 */     for (Iterator<RewardItem> i = this.rewards.iterator(); i.hasNext();) {
/* 136 */       RewardItem r = (RewardItem)i.next();
/* 137 */       r.update();
/*     */       
/* 139 */       if (r.isDone) {
/* 140 */         if (r.claimReward()) {
/* 141 */           i.remove();
/* 142 */           removedSomething = true;
/* 143 */         } else if (r.type == RewardItem.RewardType.POTION) {
/* 144 */           r.isDone = false;
/* 145 */           AbstractDungeon.topPanel.flashRed();
/* 146 */           this.tip = CardCrawlGame.tips.getPotionTip();
/* 147 */         } else if (r.type == RewardItem.RewardType.CARD) {
/* 148 */           r.isDone = false;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 153 */     if (removedSomething) {
/* 154 */       positionRewards();
/* 155 */       setLabel();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 160 */     if ((!Settings.isControllerMode) || (this.rewards.isEmpty()) || (AbstractDungeon.topPanel.selectPotionMode) || (!AbstractDungeon.topPanel.potionUi.isHidden) || (AbstractDungeon.player.viewingRelics))
/*     */     {
/* 162 */       return;
/*     */     }
/*     */     
/* 165 */     int index = 0;
/* 166 */     boolean anyHovered = false;
/* 167 */     for (RewardItem r : this.rewards) {
/* 168 */       if (r.hb.hovered) {
/* 169 */         anyHovered = true;
/* 170 */         break;
/*     */       }
/* 172 */       index++;
/*     */     }
/*     */     
/* 175 */     if (!anyHovered) {
/* 176 */       index = 0;
/* 177 */       Gdx.input.setCursorPosition(
/* 178 */         (int)((RewardItem)this.rewards.get(index)).hb.cX, Settings.HEIGHT - 
/* 179 */         (int)((RewardItem)this.rewards.get(index)).hb.cY);
/*     */     }
/* 181 */     else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 182 */       index--;
/* 183 */       if (index < 0) {
/* 184 */         index = this.rewards.size() - 1;
/*     */       }
/*     */       
/* 187 */       Gdx.input.setCursorPosition(
/* 188 */         (int)((RewardItem)this.rewards.get(index)).hb.cX, Settings.HEIGHT - 
/* 189 */         (int)((RewardItem)this.rewards.get(index)).hb.cY);
/* 190 */     } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 191 */       index++;
/* 192 */       if (index > this.rewards.size() - 1) {
/* 193 */         index = 0;
/*     */       }
/*     */       
/* 196 */       Gdx.input.setCursorPosition(
/* 197 */         (int)((RewardItem)this.rewards.get(index)).hb.cX, Settings.HEIGHT - 
/* 198 */         (int)((RewardItem)this.rewards.get(index)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   private void setLabel()
/*     */   {
/* 204 */     if (this.rewards.size() == 0) {
/* 205 */       AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[0]);
/* 206 */     } else if (this.rewards.size() == 1) {
/* 207 */       switch (((RewardItem)this.rewards.get(0)).type) {
/*     */       case CARD: 
/* 209 */         AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[1]);
/* 210 */         break;
/*     */       case GOLD: 
/* 212 */         AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[2]);
/* 213 */         break;
/*     */       case POTION: 
/* 215 */         AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[3]);
/* 216 */         break;
/*     */       case RELIC: 
/* 218 */         AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[4]);
/* 219 */         break;
/*     */       
/*     */       }
/*     */       
/*     */     } else {
/* 224 */       AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[5]);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 229 */     renderItemReward(sb);
/*     */     
/* 231 */     FontHelper.renderFontCentered(sb, FontHelper.rewardTipFont, TEXT[12] + this.tip, Settings.WIDTH / 2.0F, Settings.HEIGHT - 1010.0F * Settings.scale, TIP_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 239 */     for (AbstractGameEffect e : this.effects) {
/* 240 */       e.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderItemReward(SpriteBatch sb) {
/* 245 */     AbstractDungeon.overlayMenu.proceedButton.render(sb);
/* 246 */     sb.setColor(this.uiColor);
/* 247 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.REWARD_SCREEN_SHEET, Settings.WIDTH / 2.0F - 306.0F, Settings.HEIGHT - 584.0F * Settings.scale - 358.0F, 306.0F, 358.0F, 612.0F, 716.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 612, 716, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 265 */     for (RewardItem i : this.rewards) {
/* 266 */       i.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen()
/*     */   {
/* 272 */     this.rewardAnimTimer = 0.2F;
/* 273 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/* 274 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 275 */     AbstractDungeon.isScreenUp = true;
/* 276 */     if ((this.labelOverride == null) || (this.mug) || (this.smoke)) {
/* 277 */       if ((this.mug) || (this.smoke)) {
/* 278 */         AbstractDungeon.dynamicBanner.appear(this.labelOverride);
/*     */       } else {
/* 280 */         AbstractDungeon.dynamicBanner.appear(getRandomBannerLabel());
/*     */       }
/* 282 */       AbstractDungeon.overlayMenu.proceedButton.show();
/*     */     } else {
/* 284 */       AbstractDungeon.dynamicBanner.appear(this.labelOverride);
/* 285 */       AbstractDungeon.overlayMenu.cancelButton.show(TEXT[6]);
/*     */     }
/*     */     
/* 288 */     setLabel();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void open(String setLabel)
/*     */   {
/* 297 */     this.labelOverride = setLabel;
/* 298 */     this.mug = false;
/* 299 */     this.smoke = false;
/* 300 */     this.tip = CardCrawlGame.tips.getTip();
/*     */     
/* 302 */     this.rewardAnimTimer = 0.5F;
/* 303 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 304 */     AbstractDungeon.isScreenUp = true;
/* 305 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/* 306 */     AbstractDungeon.dynamicBanner.appear(setLabel);
/* 307 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 308 */     setupItemReward();
/* 309 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/*     */     
/*     */ 
/* 312 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[6]);
/*     */     
/*     */ 
/* 315 */     for (RewardItem r : this.rewards) {
/* 316 */       if (r.type == RewardItem.RewardType.RELIC) {
/* 317 */         UnlockTracker.markRelicAsSeen(r.relic.relicId);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void openCombat(String setLabel) {
/* 323 */     openCombat(setLabel, false);
/*     */   }
/*     */   
/*     */   public void openCombat(String setLabel, boolean smoked) {
/* 327 */     this.labelOverride = setLabel;
/* 328 */     this.mug = (!smoked);
/* 329 */     this.smoke = smoked;
/* 330 */     this.tip = CardCrawlGame.tips.getTip();
/*     */     
/* 332 */     this.rewardAnimTimer = 0.5F;
/* 333 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 334 */     AbstractDungeon.isScreenUp = true;
/* 335 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/* 336 */     AbstractDungeon.dynamicBanner.appear(setLabel);
/* 337 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 338 */     if (!this.smoke) {
/* 339 */       setupItemReward();
/*     */       
/* 341 */       for (RewardItem r : this.rewards) {
/* 342 */         if (r.type == RewardItem.RewardType.RELIC) {
/* 343 */           UnlockTracker.markRelicAsSeen(r.relic.relicId);
/*     */         }
/*     */       }
/*     */     } else {
/* 347 */       AbstractDungeon.dynamicButton.hide();
/* 348 */       AbstractDungeon.overlayMenu.proceedButton.show();
/*     */     }
/* 350 */     setLabel();
/*     */   }
/*     */   
/*     */   public void open()
/*     */   {
/* 355 */     this.tip = CardCrawlGame.tips.getTip();
/* 356 */     this.mug = false;
/* 357 */     this.smoke = false;
/*     */     
/* 359 */     this.rewardAnimTimer = 0.5F;
/* 360 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 361 */     AbstractDungeon.isScreenUp = true;
/* 362 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/* 363 */     AbstractDungeon.dynamicBanner.appear(getRandomBannerLabel());
/* 364 */     this.labelOverride = null;
/* 365 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 366 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 367 */     setupItemReward();
/* 368 */     setLabel();
/*     */     
/*     */ 
/* 371 */     for (RewardItem r : this.rewards) {
/* 372 */       if (r.type == RewardItem.RewardType.RELIC) {
/* 373 */         UnlockTracker.markRelicAsSeen(r.relic.relicId);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private String getRandomBannerLabel() {
/* 379 */     ArrayList<String> list = new ArrayList();
/* 380 */     if ((AbstractDungeon.getCurrRoom() instanceof TreasureRoom)) {
/* 381 */       list.add(TEXT[7]);
/* 382 */       list.add(TEXT[8]);
/*     */     } else {
/* 384 */       list.add(TEXT[9]);
/* 385 */       list.add(TEXT[10]);
/* 386 */       list.add(TEXT[11]);
/*     */     }
/* 388 */     return (String)list.get(com.badlogic.gdx.math.MathUtils.random(list.size() - 1));
/*     */   }
/*     */   
/*     */   public void clear() {
/* 392 */     this.rewards.clear();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\CombatRewardScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */