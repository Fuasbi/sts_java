/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DrawPileViewScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  27 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DrawPileViewScreen");
/*  28 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  30 */   private CardGroup drawPileCopy = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/*  31 */   public boolean isHovered = false;
/*     */   private static final int CARDS_PER_LINE = 5;
/*  33 */   private static final float SCROLL_BAR_THRESHOLD = 500.0F * Settings.scale;
/*  34 */   private boolean grabbedScreen = false;
/*     */   private static float drawStartX;
/*  36 */   private static float drawStartY; private static float padX; private static float padY; private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  37 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  38 */   private float grabStartY = this.scrollLowerBound; private float currentDiffY = this.scrollLowerBound;
/*  39 */   private static final String HEADER_INFO = TEXT[0];
/*  40 */   private static final String BODY_INFO = TEXT[1];
/*  41 */   private AbstractCard hoveredCard = null;
/*  42 */   private int prevDeckSize = 0;
/*     */   private ScrollBar scrollBar;
/*  44 */   private AbstractCard controllerCard = null;
/*     */   
/*     */   public DrawPileViewScreen() {
/*  47 */     drawStartX = Settings.WIDTH;
/*  48 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  49 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  50 */     drawStartX /= 2.0F;
/*  51 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*     */     
/*  53 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  54 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*  55 */     this.scrollBar = new ScrollBar(this);
/*  56 */     this.scrollBar.move(0.0F, -30.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  60 */     boolean isDraggingScrollBar = false;
/*  61 */     if (shouldShowScrollBar()) {
/*  62 */       isDraggingScrollBar = this.scrollBar.update();
/*     */     }
/*  64 */     if (!isDraggingScrollBar) {
/*  65 */       updateScrolling();
/*     */     }
/*  67 */     updateControllerInput();
/*  68 */     if ((Settings.isControllerMode) && (this.controllerCard != null) && (!CardCrawlGame.isPopupOpen) && (!AbstractDungeon.topPanel.selectPotionMode))
/*     */     {
/*  70 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.7F) {
/*  71 */         this.currentDiffY += Settings.SCROLL_SPEED;
/*  72 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.3F) {
/*  73 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/*  77 */     updatePositions();
/*     */     
/*  79 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/*  80 */       Gdx.input.setCursorPosition((int)this.controllerCard.hb.cX, (int)(Settings.HEIGHT - this.controllerCard.hb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/*  85 */     if ((!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode)) {
/*  86 */       return;
/*     */     }
/*     */     
/*  89 */     boolean anyHovered = false;
/*  90 */     int index = 0;
/*     */     
/*  92 */     for (AbstractCard c : this.drawPileCopy.group) {
/*  93 */       if (c.hb.hovered) {
/*  94 */         anyHovered = true;
/*  95 */         break;
/*     */       }
/*  97 */       index++;
/*     */     }
/*     */     
/* 100 */     if (!anyHovered) {
/* 101 */       Gdx.input.setCursorPosition(
/* 102 */         (int)((AbstractCard)this.drawPileCopy.group.get(0)).hb.cX, Settings.HEIGHT - 
/* 103 */         (int)((AbstractCard)this.drawPileCopy.group.get(0)).hb.cY);
/* 104 */       this.controllerCard = ((AbstractCard)this.drawPileCopy.group.get(0));
/*     */     }
/* 106 */     else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 107 */       (this.drawPileCopy.size() > 5))
/*     */     {
/* 109 */       if (index < 5) {
/* 110 */         index = this.drawPileCopy.size() + 2 - (4 - index);
/* 111 */         if (index > this.drawPileCopy.size() - 1) {
/* 112 */           index -= 5;
/*     */         }
/* 114 */         if ((index > this.drawPileCopy.size() - 1) || (index < 0)) {
/* 115 */           System.out.println("oops");
/* 116 */           index = 0;
/*     */         }
/*     */       }
/*     */       else {
/* 120 */         index -= 5;
/*     */       }
/* 122 */       Gdx.input.setCursorPosition(
/* 123 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 124 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cY);
/* 125 */       this.controllerCard = ((AbstractCard)this.drawPileCopy.group.get(index));
/* 126 */     } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 127 */       (this.drawPileCopy.size() > 5)) {
/* 128 */       if (index < this.drawPileCopy.size() - 5) {
/* 129 */         index += 5;
/*     */       } else {
/* 131 */         index %= 5;
/*     */       }
/* 133 */       Gdx.input.setCursorPosition(
/* 134 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 135 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cY);
/* 136 */       this.controllerCard = ((AbstractCard)this.drawPileCopy.group.get(index));
/* 137 */     } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 138 */       if (index % 5 > 0) {
/* 139 */         index--;
/*     */       } else {
/* 141 */         index += 4;
/* 142 */         if (index > this.drawPileCopy.size() - 1) {
/* 143 */           index = this.drawPileCopy.size() - 1;
/*     */         }
/*     */       }
/* 146 */       Gdx.input.setCursorPosition(
/* 147 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 148 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cY);
/* 149 */       this.controllerCard = ((AbstractCard)this.drawPileCopy.group.get(index));
/* 150 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 151 */       if (index % 5 < 4) {
/* 152 */         index++;
/* 153 */         if (index > this.drawPileCopy.size() - 1) {
/* 154 */           index -= this.drawPileCopy.size() % 5;
/*     */         }
/*     */       } else {
/* 157 */         index -= 4;
/* 158 */         if (index < 0) {
/* 159 */           index = 0;
/*     */         }
/*     */       }
/* 162 */       Gdx.input.setCursorPosition(
/* 163 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 164 */         (int)((AbstractCard)this.drawPileCopy.group.get(index)).hb.cY);
/* 165 */       this.controllerCard = ((AbstractCard)this.drawPileCopy.group.get(index));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling()
/*     */   {
/* 171 */     int y = InputHelper.mY;
/*     */     
/* 173 */     if (!this.grabbedScreen) {
/* 174 */       if (InputHelper.scrolledDown) {
/* 175 */         this.currentDiffY += Settings.SCROLL_SPEED;
/* 176 */       } else if (InputHelper.scrolledUp) {
/* 177 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 180 */       if (InputHelper.justClickedLeft) {
/* 181 */         this.grabbedScreen = true;
/* 182 */         this.grabStartY = (y - this.currentDiffY);
/*     */       }
/*     */     }
/* 185 */     else if (InputHelper.isMouseDown) {
/* 186 */       this.currentDiffY = (y - this.grabStartY);
/*     */     } else {
/* 188 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 192 */     if (this.prevDeckSize != this.drawPileCopy.size()) {
/* 193 */       calculateScrollBounds();
/*     */     }
/* 195 */     resetScrolling();
/* 196 */     updateBarPosition();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/* 203 */     int scrollTmp = 0;
/* 204 */     if (this.drawPileCopy.size() > 10) {
/* 205 */       scrollTmp = this.drawPileCopy.size() / 5 - 2;
/* 206 */       if (this.drawPileCopy.size() % 5 != 0) {
/* 207 */         scrollTmp++;
/*     */       }
/* 209 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/* 211 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/*     */     
/* 214 */     this.prevDeckSize = this.drawPileCopy.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 221 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 222 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 223 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 224 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updatePositions()
/*     */   {
/* 232 */     this.hoveredCard = null;
/* 233 */     int lineNum = 0;
/* 234 */     ArrayList<AbstractCard> cards = this.drawPileCopy.group;
/* 235 */     for (int i = 0; i < cards.size(); i++) {
/* 236 */       int mod = i % 5;
/* 237 */       if ((mod == 0) && (i != 0)) {
/* 238 */         lineNum++;
/*     */       }
/* 240 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 241 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 242 */       ((AbstractCard)cards.get(i)).update();
/* 243 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/* 244 */       if (((AbstractCard)cards.get(i)).hb.hovered) {
/* 245 */         this.hoveredCard = ((AbstractCard)cards.get(i));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen() {
/* 251 */     if (Settings.isControllerMode) {
/* 252 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 253 */       this.controllerCard = null;
/*     */     }
/* 255 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[2]);
/*     */   }
/*     */   
/*     */   public void open() {
/* 259 */     if (Settings.isControllerMode) {
/* 260 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 261 */       this.controllerCard = null;
/*     */     }
/* 263 */     CardCrawlGame.sound.play("DECK_OPEN");
/* 264 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 265 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[2]);
/* 266 */     this.currentDiffY = this.scrollLowerBound;
/* 267 */     this.grabStartY = this.scrollLowerBound;
/* 268 */     this.grabbedScreen = false;
/* 269 */     AbstractDungeon.isScreenUp = true;
/* 270 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.GAME_DECK_VIEW;
/* 271 */     this.drawPileCopy.clear();
/*     */     
/* 273 */     for (AbstractCard c : AbstractDungeon.player.drawPile.group) {
/* 274 */       c.setAngle(0.0F, true);
/* 275 */       c.targetDrawScale = 0.75F;
/* 276 */       c.targetDrawScale = 0.75F;
/* 277 */       c.drawScale = 0.75F;
/*     */       
/* 279 */       c.lighten(true);
/* 280 */       this.drawPileCopy.addToBottom(c);
/*     */     }
/*     */     
/* 283 */     if (!AbstractDungeon.player.hasRelic("Frozen Eye")) {
/* 284 */       this.drawPileCopy.sortAlphabetically(true);
/* 285 */       this.drawPileCopy.sortByRarityPlusStatusCardType(true);
/*     */     }
/* 287 */     hideCards();
/*     */     
/* 289 */     if (this.drawPileCopy.group.size() <= 5) {
/* 290 */       drawStartY = Settings.HEIGHT * 0.5F;
/*     */     } else {
/* 292 */       drawStartY = Settings.HEIGHT * 0.66F;
/*     */     }
/*     */     
/* 295 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */   private void hideCards() {
/* 299 */     int lineNum = 0;
/* 300 */     ArrayList<AbstractCard> cards = this.drawPileCopy.group;
/* 301 */     for (int i = 0; i < cards.size(); i++) {
/* 302 */       int mod = i % 5;
/* 303 */       if ((mod == 0) && (i != 0)) {
/* 304 */         lineNum++;
/*     */       }
/* 306 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 307 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY - com.badlogic.gdx.math.MathUtils.random(100.0F * Settings.scale, 200.0F * Settings.scale));
/*     */       
/*     */ 
/* 310 */       ((AbstractCard)cards.get(i)).targetDrawScale = 0.75F;
/* 311 */       ((AbstractCard)cards.get(i)).drawScale = 0.75F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 316 */     if (this.hoveredCard == null) {
/* 317 */       this.drawPileCopy.render(sb);
/*     */     } else {
/* 319 */       this.drawPileCopy.renderExceptOneCard(sb, this.hoveredCard);
/* 320 */       this.hoveredCard.renderHoverShadow(sb);
/* 321 */       this.hoveredCard.render(sb);
/* 322 */       this.hoveredCard.renderCardTip(sb);
/*     */     }
/*     */     
/* 325 */     sb.setColor(Color.WHITE);
/* 326 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.DRAW_PILE_BANNER, 0.0F, 0.0F, 630.0F * Settings.scale, 128.0F * Settings.scale);
/* 327 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.deckBannerFont, TEXT[3], 166.0F * Settings.scale, 82.0F * Settings.scale, new Color(1.0F, 1.0F, 0.75F, 1.0F));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 335 */     if (!AbstractDungeon.player.hasRelic("Frozen Eye")) {
/* 336 */       FontHelper.renderDeckViewTip(sb, BODY_INFO, 48.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     }
/* 338 */     FontHelper.renderDeckViewTip(sb, HEADER_INFO, 96.0F * Settings.scale, Settings.CREAM_COLOR);
/* 339 */     AbstractDungeon.overlayMenu.combatDeckPanel.render(sb);
/*     */     
/* 341 */     if (shouldShowScrollBar()) {
/* 342 */       this.scrollBar.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 348 */     this.currentDiffY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 349 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 353 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.currentDiffY);
/* 354 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */   
/*     */   private boolean shouldShowScrollBar() {
/* 358 */     return this.scrollUpperBound > SCROLL_BAR_THRESHOLD;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DrawPileViewScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */