/*     */ package com.megacrit.cardcrawl.screens.custom;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ 
/*     */ public class CustomModeCharacterButton
/*     */ {
/*     */   public float y;
/*     */   public float x;
/*     */   private static final int W = 128;
/*     */   public Hitbox hb;
/*  23 */   public boolean locked = false; public boolean selected = false;
/*     */   public AbstractPlayer.PlayerClass c;
/*     */   private com.badlogic.gdx.graphics.Texture buttonImg;
/*     */   private com.megacrit.cardcrawl.localization.CharacterStrings charStrings;
/*     */   
/*     */   public CustomModeCharacterButton(AbstractPlayer.PlayerClass c, boolean locked) {
/*  29 */     switch (c) {
/*     */     case IRONCLAD: 
/*  31 */       this.buttonImg = ImageMaster.FILTER_IRONCLAD;
/*  32 */       this.charStrings = CardCrawlGame.languagePack.getCharacterString("Ironclad");
/*  33 */       break;
/*     */     case THE_SILENT: 
/*  35 */       this.buttonImg = ImageMaster.FILTER_SILENT;
/*  36 */       this.charStrings = CardCrawlGame.languagePack.getCharacterString("Silent");
/*  37 */       break;
/*     */     case DEFECT: 
/*  39 */       this.buttonImg = ImageMaster.FILTER_DEFECT;
/*  40 */       this.charStrings = CardCrawlGame.languagePack.getCharacterString("Defect");
/*  41 */       break;
/*     */     default: 
/*  43 */       this.buttonImg = ImageMaster.FILTER_IRONCLAD;
/*  44 */       this.charStrings = CardCrawlGame.languagePack.getCharacterString("Ironclad");
/*     */     }
/*     */     
/*     */     
/*  48 */     this.hb = new Hitbox(80.0F * Settings.scale, 80.0F * Settings.scale);
/*  49 */     this.locked = locked;
/*  50 */     this.c = c;
/*     */   }
/*     */   
/*     */   public void move(float x, float y) {
/*  54 */     this.x = x;
/*  55 */     this.y = y;
/*  56 */     this.hb.move(x, y);
/*     */   }
/*     */   
/*     */   public void update(float x, float y) {
/*  60 */     this.x = x;
/*  61 */     this.y = y;
/*  62 */     this.hb.move(x, y);
/*  63 */     updateHitbox();
/*     */   }
/*     */   
/*     */   private void updateHitbox() {
/*  67 */     this.hb.update();
/*  68 */     if (this.hb.justHovered) {
/*  69 */       CardCrawlGame.sound.playA("UI_HOVER", -0.3F);
/*     */     }
/*     */     
/*  72 */     if ((InputHelper.justClickedLeft) && (!this.locked) && (this.hb.hovered)) {
/*  73 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.4F);
/*  74 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/*  77 */     if (this.hb.clicked) {
/*  78 */       this.hb.clicked = false;
/*     */       
/*  80 */       if (!this.selected) {
/*  81 */         CardCrawlGame.mainMenuScreen.customModeScreen.deselectOtherOptions(this);
/*  82 */         this.selected = true;
/*  83 */         CardCrawlGame.chosenCharacter = this.c;
/*  84 */         CardCrawlGame.mainMenuScreen.customModeScreen.confirmButton.isDisabled = false;
/*  85 */         CardCrawlGame.mainMenuScreen.customModeScreen.confirmButton.show();
/*     */         
/*     */ 
/*  88 */         switch (this.c) {
/*     */         case IRONCLAD: 
/*  90 */           CardCrawlGame.sound.playA("ATTACK_HEAVY", MathUtils.random(-0.2F, 0.2F));
/*  91 */           break;
/*     */         case THE_SILENT: 
/*  93 */           CardCrawlGame.sound.playA("ATTACK_DAGGER_2", MathUtils.random(-0.2F, 0.2F));
/*  94 */           break;
/*     */         case DEFECT: 
/*  96 */           CardCrawlGame.sound.playA("ATTACK_MAGIC_BEAM_SHORT", MathUtils.random(-0.2F, 0.2F));
/*  97 */           break;
/*     */         }
/*     */         
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 106 */     renderOptionButton(sb);
/* 107 */     if (this.hb.hovered) {
/* 108 */       TipHelper.renderGenericTip(InputHelper.mX + 180.0F * Settings.scale, this.hb.cY + 40.0F * Settings.scale, this.charStrings.NAMES[0], this.charStrings.TEXT[0]);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 114 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderOptionButton(SpriteBatch sb) {
/* 118 */     if (this.selected) {
/* 119 */       sb.setColor(new Color(1.0F, 0.8F, 0.2F, 0.25F + 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 124 */         (MathUtils.cosDeg((float)(System.currentTimeMillis() / 4L % 360L)) + 1.25F) / 3.5F));
/* 125 */       sb.draw(ImageMaster.FILTER_GLOW_BG, this.hb.cX - 64.0F, this.hb.cY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 144 */     if ((this.selected) || (this.hb.hovered)) {
/* 145 */       sb.setColor(Color.WHITE);
/*     */     } else {
/* 147 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     }
/* 149 */     sb.draw(this.buttonImg, this.hb.cX - 64.0F, this.y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\custom\CustomModeCharacterButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */