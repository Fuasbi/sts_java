/*     */ package com.megacrit.cardcrawl.screens.custom;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuPanelScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener;
/*     */ import com.megacrit.cardcrawl.trials.AbstractTrial;
/*     */ import com.megacrit.cardcrawl.trials.CustomTrial;
/*     */ import com.megacrit.cardcrawl.ui.buttons.GridSelectConfirmButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.SeedPanel;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CustomModeScreen
/*     */   implements ScrollBarListener
/*     */ {
/*  68 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CustomModeScreen");
/*  69 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  72 */   private MenuCancelButton cancelButton = new MenuCancelButton();
/*  73 */   public GridSelectConfirmButton confirmButton = new GridSelectConfirmButton(CharacterSelectScreen.TEXT[1]);
/*     */   
/*     */ 
/*     */   private Hitbox controllerHb;
/*     */   
/*     */ 
/*  79 */   public ArrayList<CustomModeCharacterButton> options = new ArrayList();
/*     */   
/*     */   private static float ASC_RIGHT_W;
/*     */   
/*  83 */   public boolean isAscensionMode = false;
/*     */   private Hitbox ascensionModeHb;
/*  85 */   private Hitbox ascLeftHb; private Hitbox ascRightHb; public int ascensionLevel = 0;
/*     */   
/*     */ 
/*  88 */   private Hitbox seedHb = new Hitbox(400.0F * Settings.scale, 90.0F * Settings.scale);
/*     */   
/*     */   private String currentSeed;
/*     */   
/*     */   private SeedPanel seedPanel;
/*     */   
/*     */   private ArrayList<CustomMod> modList;
/*     */   
/*     */   private static final String MOD_BLIGHT_CHESTS = "Blight Chests";
/*     */   
/*     */   private static final String MOD_ONE_HIT_WONDER = "One Hit Wonder";
/*     */   
/*     */   private static final String MOD_PRAISE_SNECKO = "Praise Snecko";
/*     */   
/*     */   private static final String MOD_INCEPTION = "Inception";
/*     */   
/*     */   private static final String MOD_MY_TRUE_FORM = "My True Form";
/*     */   
/*     */   private static final String MOD_STARTER_DECK = "Starter Deck";
/*     */   private static final String NEUTRAL_COLOR = "b";
/*     */   private static final String POSITIVE_COLOR = "g";
/*     */   private static final String NEGATIVE_COLOR = "r";
/* 110 */   public boolean screenUp = false;
/* 111 */   private static final float SHOW_X = 300.0F * Settings.scale;
/* 112 */   private float screenX = SHOW_X;
/* 113 */   private float ASCENSION_TEXT_Y = 480.0F;
/*     */   
/*     */ 
/* 116 */   private boolean grabbedScreen = false;
/* 117 */   private float grabStartY = 0.0F; private float targetY = 0.0F; private float scrollY = 0.0F;
/*     */   private float scrollLowerBound;
/*     */   private float scrollUpperBound;
/*     */   private ScrollBar scrollBar;
/*     */   
/* 122 */   public CustomModeScreen() { initializeMods();
/* 123 */     initializeCharacters();
/* 124 */     calculateScrollBounds();
/* 125 */     this.scrollBar = new ScrollBar(this, Settings.WIDTH - 340.0F * Settings.scale - ScrollBar.TRACK_W / 2.0F, Settings.HEIGHT / 2.0F, Settings.HEIGHT - 256.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 130 */     this.seedPanel = new SeedPanel();
/*     */   }
/*     */   
/*     */   private void initializeMods() {
/* 134 */     this.modList = new ArrayList();
/*     */     
/*     */ 
/* 137 */     addDailyMod("Draft", "b");
/* 138 */     addDailyMod("Endless", "b");
/* 139 */     addMod("Blight Chests", "b", false);
/* 140 */     addDailyMod("Hoarder", "b");
/* 141 */     CustomMod insanityMod = addDailyMod("Insanity", "b");
/* 142 */     addMod("Praise Snecko", "b", false);
/* 143 */     CustomMod shinyMod = addDailyMod("Shiny", "b");
/* 144 */     addDailyMod("Specialized", "b");
/* 145 */     addDailyMod("Vintage", "b");
/* 146 */     addMod("Inception", "b", false);
/*     */     
/*     */ 
/* 149 */     addDailyMod("Allstar", "g");
/* 150 */     addDailyMod("Brewmaster", "g");
/* 151 */     addDailyMod("Diverse", "g");
/* 152 */     addMod("Red Cards", "g", false);
/* 153 */     addMod("Green Cards", "g", false);
/* 154 */     addMod("Blue Cards", "g", false);
/* 155 */     addMod("Colorless Cards", "g", false);
/* 156 */     addDailyMod("Heirloom", "g");
/* 157 */     addDailyMod("Time Dilation", "g");
/* 158 */     addMod("My True Form", "g", false);
/*     */     
/*     */ 
/* 161 */     addDailyMod("Binary", "r");
/* 162 */     addMod("One Hit Wonder", "r", false);
/* 163 */     addDailyMod("Cursed Run", "r");
/* 164 */     addDailyMod("Elite Swarm", "r");
/* 165 */     addDailyMod("Lethality", "r");
/* 166 */     addDailyMod("Midas", "r");
/* 167 */     addDailyMod("Night Terrors", "r");
/* 168 */     addDailyMod("Terminal", "r");
/* 169 */     addDailyMod("Uncertain Future", "r");
/* 170 */     addMod("Starter Deck", "r", false);
/*     */     
/*     */ 
/* 173 */     insanityMod.setMutualExclusionPair(shinyMod);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private CustomMod addMod(String id, String color, boolean isDailyMod)
/*     */   {
/* 181 */     RunModStrings modString = CardCrawlGame.languagePack.getRunModString(id);
/* 182 */     if (modString != null) {
/* 183 */       CustomMod mod = new CustomMod(id, color, isDailyMod);
/* 184 */       this.modList.add(mod);
/* 185 */       return mod;
/*     */     }
/* 187 */     return null;
/*     */   }
/*     */   
/*     */   private CustomMod addDailyMod(String id, String color) {
/* 191 */     return addMod(id, color, true);
/*     */   }
/*     */   
/*     */   public void open() {
/* 195 */     this.confirmButton.show();
/* 196 */     this.controllerHb = null;
/* 197 */     this.targetY = 0.0F;
/* 198 */     this.screenUp = true;
/* 199 */     Settings.seed = null;
/* 200 */     Settings.specialSeed = null;
/*     */     
/* 202 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.CUSTOM;
/* 203 */     CardCrawlGame.mainMenuScreen.darken();
/* 204 */     this.cancelButton.show(CharacterSelectScreen.TEXT[5]);
/* 205 */     this.confirmButton.isDisabled = false;
/* 206 */     ASC_RIGHT_W = FontHelper.getSmartWidth(FontHelper.charDescFont, TEXT[4] + "22", 9999.0F, 0.0F);
/* 207 */     this.ascensionModeHb = new Hitbox(80.0F * Settings.scale, 80.0F * Settings.scale);
/* 208 */     this.ascensionModeHb.move(this.screenX, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/* 209 */     this.ascLeftHb = new Hitbox(70.0F * Settings.scale, 70.0F * Settings.scale);
/* 210 */     this.ascRightHb = new Hitbox(70.0F * Settings.scale, 70.0F * Settings.scale);
/* 211 */     this.ascLeftHb.move(this.screenX - ASC_RIGHT_W * 0.5F, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/* 212 */     this.ascRightHb.move(this.screenX + ASC_RIGHT_W * 1.5F, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/*     */   }
/*     */   
/*     */   public void initializeCharacters() {
/* 216 */     this.options.clear();
/* 217 */     this.options.add(new CustomModeCharacterButton(AbstractPlayer.PlayerClass.IRONCLAD, false));
/* 218 */     this.options.add(new CustomModeCharacterButton(AbstractPlayer.PlayerClass.THE_SILENT, 
/*     */     
/*     */ 
/* 221 */       UnlockTracker.isCharacterLocked("The Silent")));
/* 222 */     this.options.add(new CustomModeCharacterButton(AbstractPlayer.PlayerClass.DEFECT, 
/*     */     
/*     */ 
/* 225 */       UnlockTracker.isCharacterLocked("Defect")));
/*     */     
/* 227 */     int count = this.options.size();
/*     */     
/* 229 */     for (int i = 0; i < count; i++) {
/* 230 */       ((CustomModeCharacterButton)this.options.get(i)).move(this.screenX + i * 100.0F * Settings.scale - 200.0F * Settings.scale, this.scrollY - 80.0F * Settings.scale);
/*     */     }
/*     */     
/*     */ 
/* 234 */     ((CustomModeCharacterButton)this.options.get(0)).hb.clicked = true;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 239 */     updateControllerInput();
/* 240 */     if ((Settings.isControllerMode) && (this.controllerHb != null)) {
/* 241 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.75F) {
/* 242 */         this.targetY += Settings.SCROLL_SPEED;
/* 243 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.25F) {
/* 244 */         this.targetY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 249 */     this.seedPanel.update();
/* 250 */     if (!this.seedPanel.shown)
/*     */     {
/* 252 */       boolean isDraggingScrollBar = this.scrollBar.update();
/* 253 */       if (!isDraggingScrollBar) {
/* 254 */         updateScrolling();
/*     */       }
/* 256 */       updateCharacterButtons();
/* 257 */       updateAscension();
/* 258 */       updateSeed();
/* 259 */       updateMods();
/* 260 */       updateEmbarkButton();
/* 261 */       updateCancelButton();
/*     */     }
/* 263 */     this.currentSeed = SeedHelper.getUserFacingSeedString();
/*     */     
/* 265 */     if ((Settings.isControllerMode) && (this.controllerHb != null)) {
/* 266 */       CInputHelper.setCursor(this.controllerHb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateCancelButton() {
/* 271 */     this.cancelButton.update();
/* 272 */     if ((this.cancelButton.hb.clicked) || (InputHelper.pressedEscape)) {
/* 273 */       InputHelper.pressedEscape = false;
/* 274 */       this.cancelButton.hb.clicked = false;
/* 275 */       this.cancelButton.hide();
/* 276 */       CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateEmbarkButton() {
/* 281 */     this.confirmButton.update();
/* 282 */     if ((this.confirmButton.hb.clicked) || (CInputActionSet.proceed.isJustPressed())) {
/* 283 */       this.confirmButton.hb.clicked = false;
/*     */       
/* 285 */       for (CustomModeCharacterButton b : this.options) {
/* 286 */         if (b.selected) {
/* 287 */           CardCrawlGame.chosenCharacter = b.c;
/* 288 */           break;
/*     */         }
/*     */       }
/*     */       
/* 292 */       CardCrawlGame.mainMenuScreen.isFadingOut = true;
/* 293 */       CardCrawlGame.mainMenuScreen.fadeOutMusic();
/* 294 */       Settings.isTrial = true;
/* 295 */       Settings.isDailyRun = false;
/* 296 */       Settings.isEndless = false;
/*     */       
/* 298 */       AbstractDungeon.isAscensionMode = this.isAscensionMode;
/* 299 */       AbstractDungeon.ascensionLevel = this.ascensionLevel;
/*     */       
/* 301 */       if (this.currentSeed.isEmpty()) {
/* 302 */         long sourceTime = System.nanoTime();
/* 303 */         Random rng = new Random(Long.valueOf(sourceTime));
/* 304 */         Settings.seed = Long.valueOf(SeedHelper.generateUnoffensiveSeed(rng));
/*     */       }
/* 306 */       AbstractDungeon.generateSeeds();
/*     */       
/* 308 */       CustomTrial trial = new CustomTrial();
/* 309 */       trial.addDailyMods(getActiveDailyModIds());
/* 310 */       addNonDailyMods(trial, getActiveNonDailyMods());
/*     */       
/*     */ 
/* 313 */       Settings.isEndless = trial.dailyModIDs().contains("Endless");
/* 314 */       CardCrawlGame.trial = trial;
/* 315 */       com.megacrit.cardcrawl.characters.AbstractPlayer.customMods = CardCrawlGame.trial.dailyModIDs();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateCharacterButtons() {
/* 320 */     for (int i = 0; i < this.options.size(); i++) {
/* 321 */       ((CustomModeCharacterButton)this.options.get(i)).update(this.screenX + i * 100.0F * Settings.scale + 130.0F * Settings.scale, this.scrollY + 640.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateSeed()
/*     */   {
/* 328 */     this.seedHb.move(this.screenX + 280.0F * Settings.scale, this.scrollY + 320.0F * Settings.scale);
/* 329 */     this.seedHb.update();
/*     */     
/* 331 */     if (this.seedHb.justHovered) {
/* 332 */       playHoverSound();
/*     */     }
/*     */     
/* 335 */     if ((this.seedHb.hovered) && (InputHelper.justClickedLeft)) {
/* 336 */       this.seedHb.clickStarted = true;
/*     */     }
/*     */     
/* 339 */     if ((this.seedHb.clicked) || ((CInputActionSet.select.isJustPressed()) && (this.seedHb.hovered))) {
/* 340 */       this.seedHb.clicked = false;
/* 341 */       if (Settings.seed == null) {
/* 342 */         Settings.seed = Long.valueOf(0L);
/*     */       }
/* 344 */       this.seedPanel.show(MainMenuScreen.CurScreen.CUSTOM);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateAscension()
/*     */   {
/* 351 */     this.ascLeftHb.move(this.screenX - ASC_RIGHT_W * 0.5F + 405.0F * Settings.scale, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/*     */     
/*     */ 
/* 354 */     this.ascRightHb.move(this.screenX + ASC_RIGHT_W * 1.5F + 250.0F * Settings.scale, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/*     */     
/*     */ 
/* 357 */     this.ascensionModeHb.move(this.screenX + 130.0F * Settings.scale, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale);
/*     */     
/*     */ 
/* 360 */     this.ascensionModeHb.update();
/* 361 */     this.ascLeftHb.update();
/* 362 */     this.ascRightHb.update();
/*     */     
/* 364 */     if ((this.ascensionModeHb.justHovered) || (this.ascRightHb.justHovered) || (this.ascLeftHb.justHovered)) {
/* 365 */       playHoverSound();
/*     */     }
/*     */     
/* 368 */     if ((this.ascensionModeHb.hovered) && (InputHelper.justClickedLeft)) {
/* 369 */       playClickStartSound();
/* 370 */       this.ascensionModeHb.clickStarted = true;
/* 371 */     } else if ((this.ascLeftHb.hovered) && (InputHelper.justClickedLeft)) {
/* 372 */       playClickStartSound();
/* 373 */       this.ascLeftHb.clickStarted = true;
/* 374 */     } else if ((this.ascRightHb.hovered) && (InputHelper.justClickedLeft)) {
/* 375 */       playClickStartSound();
/* 376 */       this.ascRightHb.clickStarted = true;
/*     */     }
/*     */     
/*     */ 
/* 380 */     if ((this.ascensionModeHb.clicked) || (CInputActionSet.topPanel.isJustPressed())) {
/* 381 */       CInputActionSet.topPanel.unpress();
/* 382 */       playClickFinishSound();
/* 383 */       this.ascensionModeHb.clicked = false;
/* 384 */       this.isAscensionMode = (!this.isAscensionMode);
/* 385 */       if ((this.isAscensionMode) && (this.ascensionLevel == 0)) {
/* 386 */         this.ascensionLevel = 1;
/*     */       }
/*     */     }
/* 389 */     else if ((this.ascLeftHb.clicked) || (CInputActionSet.pageLeftViewDeck.isJustPressed())) {
/* 390 */       playClickFinishSound();
/* 391 */       this.ascLeftHb.clicked = false;
/* 392 */       this.ascensionLevel -= 1;
/* 393 */       if (this.ascensionLevel < 0) {
/* 394 */         this.ascensionLevel = 0;
/*     */       }
/*     */       
/* 397 */       if (this.ascensionLevel == 0) {
/* 398 */         this.isAscensionMode = false;
/*     */       } else {
/* 400 */         this.isAscensionMode = true;
/*     */       }
/*     */       
/*     */     }
/* 404 */     else if ((this.ascRightHb.clicked) || (CInputActionSet.pageRightViewExhaust.isJustPressed())) {
/* 405 */       playClickFinishSound();
/* 406 */       this.ascRightHb.clicked = false;
/* 407 */       this.ascensionLevel += 1;
/* 408 */       if (this.ascensionLevel > 20) {
/* 409 */         this.ascensionLevel = 20;
/*     */       }
/* 411 */       this.isAscensionMode = true;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMods() {
/* 416 */     float offset = 0.0F;
/* 417 */     for (int i = 0; i < this.modList.size(); i++) {
/* 418 */       ((CustomMod)this.modList.get(i)).update(this.scrollY + offset);
/* 419 */       offset -= ((CustomMod)this.modList.get(i)).height;
/*     */     }
/*     */   }
/*     */   
/*     */   private ArrayList<String> getActiveDailyModIds() {
/* 424 */     ArrayList<String> active = new ArrayList();
/* 425 */     for (CustomMod mod : this.modList) {
/* 426 */       if ((mod.selected) && (mod.isDailyMod)) {
/* 427 */         active.add(mod.ID);
/*     */       }
/*     */     }
/* 430 */     return active;
/*     */   }
/*     */   
/*     */   private ArrayList<String> getActiveNonDailyMods() {
/* 434 */     ArrayList<String> active = new ArrayList();
/* 435 */     for (CustomMod mod : this.modList) {
/* 436 */       if ((mod.selected) && (!mod.isDailyMod)) {
/* 437 */         active.add(mod.ID);
/*     */       }
/*     */     }
/* 440 */     return active;
/*     */   }
/*     */   
/*     */   private void addNonDailyMods(CustomTrial trial, ArrayList<String> modIds) {
/* 444 */     for (String modId : modIds) {
/* 445 */       switch (modId)
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */       case "One Hit Wonder": 
/* 451 */         trial.setMaxHpOverride(1);
/* 452 */         break;
/*     */       case "Praise Snecko": 
/* 454 */         trial.addStarterRelic("Snecko Eye");
/* 455 */         trial.setShouldKeepStarterRelic(false);
/* 456 */         break;
/*     */       case "Inception": 
/* 458 */         trial.addStarterRelic("Unceasing Top");
/* 459 */         trial.setShouldKeepStarterRelic(false);
/* 460 */         break;
/*     */       case "My True Form": 
/* 462 */         trial.addStarterCards(Arrays.asList(new String[] { "Demon Form", "Wraith Form v2", "Echo Form" }));
/* 463 */         break;
/*     */       case "Starter Deck": 
/* 465 */         trial.addStarterRelic("Busted Crown");
/* 466 */         trial.addDailyMod("Binary");
/* 467 */         break;
/*     */       case "Blight Chests": 
/* 469 */         trial.addDailyMod("Blight Chests");
/* 470 */         break;
/*     */       case "Red Cards": 
/* 472 */         trial.addDailyMod("Red Cards");
/* 473 */         break;
/*     */       case "Green Cards": 
/* 475 */         trial.addDailyMod("Green Cards");
/* 476 */         break;
/*     */       case "Blue Cards": 
/* 478 */         trial.addDailyMod("Blue Cards");
/* 479 */         break;
/*     */       case "Colorless Cards": 
/* 481 */         trial.addDailyMod("Colorless Cards");
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void playClickStartSound()
/*     */   {
/* 490 */     CardCrawlGame.sound.playA("UI_CLICK_1", -0.1F);
/*     */   }
/*     */   
/*     */   private void playClickFinishSound()
/*     */   {
/* 495 */     CardCrawlGame.sound.playA("UI_CLICK_1", -0.1F);
/*     */   }
/*     */   
/*     */   private void playHoverSound() {
/* 499 */     CardCrawlGame.sound.playV("UI_HOVER", 0.75F);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 503 */     renderScreen(sb);
/* 504 */     this.scrollBar.render(sb);
/* 505 */     this.cancelButton.render(sb);
/* 506 */     this.confirmButton.render(sb);
/*     */     
/*     */ 
/* 509 */     for (CustomModeCharacterButton o : this.options) {
/* 510 */       o.render(sb);
/*     */     }
/*     */     
/*     */ 
/* 514 */     renderAscension(sb);
/*     */     
/*     */ 
/* 517 */     renderSeed(sb);
/*     */     
/*     */ 
/* 520 */     sb.setColor(Color.WHITE);
/* 521 */     for (CustomMod m : this.modList) {
/* 522 */       m.render(sb);
/*     */     }
/* 524 */     this.seedPanel.render(sb);
/*     */   }
/*     */   
/*     */   public void renderScreen(SpriteBatch sb)
/*     */   {
/* 529 */     renderTitle(sb, TEXT[0], this.scrollY - 50.0F * Settings.scale);
/* 530 */     renderHeader(sb, TEXT[2], this.scrollY - 120.0F * Settings.scale);
/* 531 */     renderHeader(sb, TEXT[3], this.scrollY - 290.0F * Settings.scale);
/* 532 */     renderHeader(sb, TEXT[7], this.scrollY - 460.0F * Settings.scale);
/* 533 */     renderHeader(sb, TEXT[6], this.scrollY - 630.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   private void renderAscension(SpriteBatch sb) {
/* 537 */     sb.setColor(Color.WHITE);
/* 538 */     if (this.ascensionModeHb.hovered) {
/* 539 */       sb.draw(ImageMaster.CHECKBOX, this.ascensionModeHb.cX - 32.0F, this.ascensionModeHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 1.1F, Settings.scale * 1.1F, 0.0F, 0, 0, 64, 64, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 557 */       sb.draw(ImageMaster.CHECKBOX, this.ascensionModeHb.cX - 32.0F, this.ascensionModeHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 576 */     FontHelper.renderFontCentered(sb, FontHelper.charDescFont, TEXT[4] + this.ascensionLevel, this.screenX + 240.0F * Settings.scale, this.scrollY + this.ASCENSION_TEXT_Y * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 584 */     if (this.isAscensionMode) {
/* 585 */       sb.draw(ImageMaster.TICK, this.ascensionModeHb.cX - 32.0F, this.ascensionModeHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 604 */     if (this.ascensionLevel != 0) {
/* 605 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, CardCrawlGame.mainMenuScreen.charSelectScreen.ascLevelInfoString = CharacterSelectScreen.A_TEXT[(this.ascensionLevel - 1)], this.screenX + 475.0F * Settings.scale, this.ascensionModeHb.cY + 10.0F * Settings.scale, 9999.0F, 32.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 618 */     if ((this.ascLeftHb.hovered) || (Settings.isControllerMode)) {
/* 619 */       sb.setColor(Color.WHITE);
/*     */     } else {
/* 621 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     }
/* 623 */     sb.draw(ImageMaster.CF_LEFT_ARROW, this.ascLeftHb.cX - 24.0F, this.ascLeftHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 641 */     if ((this.ascRightHb.hovered) || (Settings.isControllerMode)) {
/* 642 */       sb.setColor(Color.WHITE);
/*     */     } else {
/* 644 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     }
/* 646 */     sb.draw(ImageMaster.CF_RIGHT_ARROW, this.ascRightHb.cX - 24.0F, this.ascRightHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 664 */     if (Settings.isControllerMode) {
/* 665 */       sb.draw(CInputActionSet.topPanel
/* 666 */         .getKeyImg(), this.ascensionModeHb.cX - 64.0F * Settings.scale - 32.0F, this.ascensionModeHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 682 */       sb.draw(CInputActionSet.pageLeftViewDeck
/* 683 */         .getKeyImg(), this.ascLeftHb.cX - 12.0F * Settings.scale - 32.0F, this.ascLeftHb.cY + 40.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 699 */       sb.draw(CInputActionSet.pageRightViewExhaust
/* 700 */         .getKeyImg(), this.ascRightHb.cX + 12.0F * Settings.scale - 32.0F, this.ascRightHb.cY + 40.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 718 */     this.ascensionModeHb.render(sb);
/* 719 */     this.ascLeftHb.render(sb);
/* 720 */     this.ascRightHb.render(sb);
/*     */   }
/*     */   
/*     */ 
/*     */   private void renderSeed(SpriteBatch sb)
/*     */   {
/* 726 */     if (this.seedHb.hovered) {
/* 727 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, 
/*     */       
/*     */ 
/* 730 */         FontHelper.colorString(TEXT[8], "g") + ": " + this.currentSeed, this.screenX + 96.0F * Settings.scale, this.seedHb.cY, 9999.0F, 32.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 737 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, 
/*     */       
/*     */ 
/* 740 */         FontHelper.colorString(TEXT[8], "b") + ": " + this.currentSeed, this.screenX + 96.0F * Settings.scale, this.seedHb.cY, 9999.0F, 32.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 747 */     this.seedHb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderHeader(SpriteBatch sb, String text, float y) {
/* 751 */     FontHelper.renderSmartText(sb, FontHelper.deckBannerFont, text, this.screenX + 50.0F * Settings.scale, y + 850.0F * Settings.scale, 9999.0F, 32.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderTitle(SpriteBatch sb, String text, float y)
/*     */   {
/* 763 */     FontHelper.renderSmartText(sb, FontHelper.charTitleFont, text, this.screenX, y + 900.0F * Settings.scale, 9999.0F, 32.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 773 */     FontHelper.renderSmartText(sb, FontHelper.panelNameTitleFont, TEXT[1], this.screenX + 
/*     */     
/*     */ 
/*     */ 
/* 777 */       FontHelper.getSmartWidth(FontHelper.charTitleFont, text, 9999.0F, 9999.0F) + 18.0F * Settings.scale, y + 888.0F * Settings.scale, 9999.0F, 32.0F * Settings.scale, Settings.RED_TEXT_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static enum CSelectionType
/*     */   {
/* 785 */     CHARACTER,  ASCENSION,  SEED,  MODIFIERS;
/*     */     
/*     */     private CSelectionType() {} }
/*     */   
/* 789 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/* 790 */       return;
/*     */     }
/*     */     
/* 793 */     CSelectionType type = CSelectionType.CHARACTER;
/* 794 */     boolean anyHovered = false;
/* 795 */     int index = 0;
/* 796 */     for (CustomModeCharacterButton b : this.options) {
/* 797 */       if (b.hb.hovered) {
/* 798 */         anyHovered = true;
/* 799 */         break;
/*     */       }
/* 801 */       index++;
/*     */     }
/*     */     
/* 804 */     if ((!anyHovered) && (this.ascensionModeHb.hovered)) {
/* 805 */       anyHovered = true;
/* 806 */       type = CSelectionType.ASCENSION;
/*     */     }
/* 808 */     if ((!anyHovered) && (this.seedHb.hovered)) {
/* 809 */       anyHovered = true;
/* 810 */       type = CSelectionType.SEED;
/*     */     }
/* 812 */     if (!anyHovered) {
/* 813 */       index = 0;
/* 814 */       for (CustomMod m : this.modList) {
/* 815 */         if (m.hb.hovered) {
/* 816 */           anyHovered = true;
/* 817 */           type = CSelectionType.MODIFIERS;
/* 818 */           break;
/*     */         }
/* 820 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 824 */     if ((!anyHovered) && (this.controllerHb == null)) {
/* 825 */       System.out.println("NONE HOVERED");
/* 826 */       CInputHelper.setCursor(((CustomModeCharacterButton)this.options.get(0)).hb);
/* 827 */       this.controllerHb = ((CustomModeCharacterButton)this.options.get(0)).hb;
/*     */     } else {
/* 829 */       switch (type) {
/*     */       case CHARACTER: 
/* 831 */         if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 832 */           index++;
/* 833 */           if (index > this.options.size() - 1) {
/* 834 */             index = this.options.size() - 1;
/*     */           }
/* 836 */           CInputHelper.setCursor(((CustomModeCharacterButton)this.options.get(index)).hb);
/* 837 */           this.controllerHb = ((CustomModeCharacterButton)this.options.get(index)).hb;
/* 838 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 839 */           index--;
/* 840 */           if (index < 0) {
/* 841 */             index = 0;
/*     */           }
/* 843 */           CInputHelper.setCursor(((CustomModeCharacterButton)this.options.get(index)).hb);
/* 844 */           this.controllerHb = ((CustomModeCharacterButton)this.options.get(index)).hb;
/* 845 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 846 */           CInputHelper.setCursor(this.ascensionModeHb);
/* 847 */           this.controllerHb = this.ascensionModeHb;
/* 848 */         } else if (CInputActionSet.select.isJustPressed()) {
/* 849 */           CInputActionSet.select.unpress();
/* 850 */           ((CustomModeCharacterButton)this.options.get(index)).hb.clicked = true;
/*     */         }
/*     */         break;
/*     */       case ASCENSION: 
/* 854 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 855 */           CInputHelper.setCursor(((CustomModeCharacterButton)this.options.get(0)).hb);
/* 856 */           this.controllerHb = ((CustomModeCharacterButton)this.options.get(0)).hb;
/* 857 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 858 */           CInputHelper.setCursor(this.seedHb);
/* 859 */           this.controllerHb = this.seedHb;
/*     */         }
/*     */         break;
/*     */       case SEED: 
/* 863 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 864 */           CInputHelper.setCursor(this.ascensionModeHb);
/* 865 */           this.controllerHb = this.ascensionModeHb;
/* 866 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 867 */           CInputHelper.setCursor(((CustomMod)this.modList.get(0)).hb);
/* 868 */           this.controllerHb = ((CustomMod)this.modList.get(0)).hb;
/*     */         }
/*     */         break;
/*     */       case MODIFIERS: 
/* 872 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 873 */           index--;
/* 874 */           if (index < 0) {
/* 875 */             CInputHelper.setCursor(this.seedHb);
/* 876 */             this.controllerHb = this.seedHb;
/*     */           } else {
/* 878 */             CInputHelper.setCursor(((CustomMod)this.modList.get(index)).hb);
/* 879 */             this.controllerHb = ((CustomMod)this.modList.get(index)).hb;
/*     */           }
/* 881 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 882 */           index++;
/* 883 */           if (index > this.modList.size() - 1) {
/* 884 */             index = this.modList.size() - 1;
/*     */           }
/* 886 */           CInputHelper.setCursor(((CustomMod)this.modList.get(index)).hb);
/* 887 */           this.controllerHb = ((CustomMod)this.modList.get(index)).hb;
/* 888 */         } else if (CInputActionSet.select.isJustPressed()) {
/* 889 */           CInputActionSet.select.unpress();
/* 890 */           ((CustomMod)this.modList.get(index)).hb.clicked = true;
/*     */         }
/*     */         break;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling()
/*     */   {
/* 899 */     int y = InputHelper.mY;
/*     */     
/* 901 */     if (this.scrollUpperBound > 0.0F) {
/* 902 */       if (!this.grabbedScreen) {
/* 903 */         if (InputHelper.scrolledDown) {
/* 904 */           this.targetY += Settings.SCROLL_SPEED;
/* 905 */         } else if (InputHelper.scrolledUp) {
/* 906 */           this.targetY -= Settings.SCROLL_SPEED;
/*     */         }
/*     */         
/* 909 */         if (InputHelper.justClickedLeft) {
/* 910 */           this.grabbedScreen = true;
/* 911 */           this.grabStartY = (y - this.targetY);
/*     */         }
/*     */       }
/* 914 */       else if (InputHelper.isMouseDown) {
/* 915 */         this.targetY = (y - this.grabStartY);
/*     */       } else {
/* 917 */         this.grabbedScreen = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 922 */     this.scrollY = MathHelper.scrollSnapLerpSpeed(this.scrollY, this.targetY);
/*     */     
/*     */ 
/* 925 */     if (this.targetY < this.scrollLowerBound) {
/* 926 */       this.targetY = MathHelper.scrollSnapLerpSpeed(this.targetY, this.scrollLowerBound);
/* 927 */     } else if (this.targetY > this.scrollUpperBound) {
/* 928 */       this.targetY = MathHelper.scrollSnapLerpSpeed(this.targetY, this.scrollUpperBound);
/*     */     }
/*     */     
/* 931 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void calculateScrollBounds() {
/* 935 */     this.scrollUpperBound = (this.modList.size() * 90.0F * Settings.scale + 270.0F * Settings.scale);
/* 936 */     this.scrollLowerBound = (100.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 941 */     float newPosition = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 942 */     this.scrollY = newPosition;
/* 943 */     this.targetY = newPosition;
/* 944 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 948 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.scrollY);
/* 949 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */   
/*     */   public void deselectOtherOptions(CustomModeCharacterButton characterOption) {
/* 953 */     for (CustomModeCharacterButton o : this.options) {
/* 954 */       if (o != characterOption) {
/* 955 */         o.selected = false;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\custom\CustomModeScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */