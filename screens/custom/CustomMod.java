/*     */ package com.megacrit.cardcrawl.screens.custom;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import java.util.HashSet;
/*     */ 
/*     */ public class CustomMod
/*     */ {
/*     */   public String ID;
/*     */   public String name;
/*     */   public String description;
/*     */   public String color;
/*     */   private String label;
/*     */   public boolean isDailyMod;
/*  18 */   public boolean selected = false;
/*     */   public Hitbox hb;
/*  20 */   private static final float X = 420.0F * Settings.scale;
/*  21 */   private static final float OFFSET_Y = 130.0F * Settings.scale;
/*  22 */   float height = 0.0F;
/*     */   private HashSet<CustomMod> mutuallyExclusive;
/*     */   
/*     */   public CustomMod(String setID, String color, boolean isDailyMod) {
/*  26 */     this.color = color;
/*  27 */     this.ID = setID;
/*  28 */     com.megacrit.cardcrawl.localization.RunModStrings modStrings = CardCrawlGame.languagePack.getRunModString(setID);
/*  29 */     this.name = modStrings.NAME;
/*  30 */     this.description = modStrings.DESCRIPTION;
/*  31 */     this.hb = new Hitbox(1050.0F * Settings.scale, 75.0F * Settings.scale);
/*  32 */     this.isDailyMod = isDailyMod;
/*  33 */     this.label = (FontHelper.colorString(new StringBuilder().append("[").append(this.name).append("]").toString(), color) + " " + this.description);
/*     */     
/*  35 */     this.height = (-FontHelper.getSmartHeight(FontHelper.charDescFont, this.label, 1050.0F * Settings.scale, 32.0F * Settings.scale) + 70.0F * Settings.scale);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update(float y)
/*     */   {
/*  43 */     this.hb.update();
/*  44 */     this.hb.move(X + 450.0F * Settings.scale, y + OFFSET_Y);
/*     */     
/*  46 */     if (this.hb.justHovered) {
/*  47 */       playHoverSound();
/*     */     }
/*     */     
/*  50 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  51 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/*  54 */     if (this.hb.clicked) {
/*  55 */       this.hb.clicked = false;
/*  56 */       this.selected = (!this.selected);
/*  57 */       playClickSound();
/*  58 */       if (this.selected) {
/*  59 */         disableMutuallyExclusiveMods();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  65 */     float scale = Settings.scale;
/*  66 */     if (this.hb.hovered) {
/*  67 */       scale = 1.15F * Settings.scale;
/*     */     }
/*     */     
/*  70 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.CHECKBOX, X - 32.0F, this.hb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, scale, scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  88 */     if (this.selected) {
/*  89 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.TICK, X - 32.0F, this.hb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, scale, scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 108 */     FontHelper.renderSmartText(sb, FontHelper.charDescFont, this.label, X + 40.0F * Settings.scale, this.hb.cY + 12.0F * Settings.scale, 1050.0F * Settings.scale, 32.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 117 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   private void playClickSound() {
/* 121 */     CardCrawlGame.sound.playA("UI_CLICK_1", -0.1F);
/*     */   }
/*     */   
/*     */   private void playHoverSound() {
/* 125 */     CardCrawlGame.sound.playV("UI_HOVER", 0.75F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMutualExclusionPair(CustomMod otherMod)
/*     */   {
/* 134 */     setMutualExclusion(otherMod);
/* 135 */     otherMod.setMutualExclusion(this);
/*     */   }
/*     */   
/*     */   private void setMutualExclusion(CustomMod otherMod) {
/* 139 */     if (this.mutuallyExclusive == null) {
/* 140 */       this.mutuallyExclusive = new HashSet();
/*     */     }
/* 142 */     this.mutuallyExclusive.add(otherMod);
/*     */   }
/*     */   
/*     */   private void disableMutuallyExclusiveMods() {
/* 146 */     if (this.mutuallyExclusive != null) {
/* 147 */       for (CustomMod mods : this.mutuallyExclusive) {
/* 148 */         mods.selected = false;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\custom\CustomMod.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */