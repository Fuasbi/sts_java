/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.characters.Defect;
/*     */ import com.megacrit.cardcrawl.characters.Ironclad;
/*     */ import com.megacrit.cardcrawl.characters.TheSilent;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map.Entry;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CharSelectInfo
/*     */ {
/*     */   public String name;
/*     */   public String flavorText;
/*     */   public String hp;
/*     */   public int gold;
/*     */   public int currentHp;
/*     */   public int maxHp;
/*     */   public int maxOrbs;
/*     */   public int cardDraw;
/*     */   public int floorNum;
/*     */   public String levelName;
/*     */   public long saveDate;
/*     */   public AbstractPlayer.PlayerClass color;
/*     */   public String deckString;
/*     */   public ArrayList<String> relics;
/*     */   public ArrayList<String> deck;
/*     */   public boolean resumeGame;
/*     */   public boolean isHardMode;
/*     */   
/*     */   public CharSelectInfo(String name, String flavorText, int currentHp, int maxHp, int maxOrbs, int gold, int cardDraw, AbstractPlayer.PlayerClass color, ArrayList<String> relics, ArrayList<String> deck, boolean resumeGame)
/*     */   {
/*  55 */     this.name = name;
/*  56 */     this.flavorText = flavorText;
/*  57 */     this.currentHp = currentHp;
/*  58 */     this.maxHp = maxHp;
/*  59 */     this.maxOrbs = maxOrbs;
/*  60 */     this.hp = (Integer.toString(currentHp) + "/" + Integer.toString(maxHp));
/*  61 */     this.gold = gold;
/*  62 */     this.cardDraw = cardDraw;
/*  63 */     this.relics = relics;
/*  64 */     this.deck = deck;
/*  65 */     this.color = color;
/*  66 */     this.resumeGame = resumeGame;
/*     */     
/*  68 */     if (!resumeGame) {
/*  69 */       setDeck();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CharSelectInfo(String name, String fText, int currentHp, int maxHp, int maxOrbs, int gold, int cardDraw, AbstractPlayer.PlayerClass color, ArrayList<String> relics, ArrayList<String> deck, long saveDate, int floorNum, String levelName, boolean isHardMode)
/*     */   {
/* 102 */     this(name, fText, currentHp, maxHp, maxOrbs, gold, cardDraw, color, relics, deck, true);
/* 103 */     this.isHardMode = isHardMode;
/* 104 */     this.saveDate = saveDate;
/* 105 */     this.floorNum = floorNum;
/* 106 */     this.levelName = levelName;
/*     */   }
/*     */   
/*     */   private void setDeck() {
/* 110 */     switch (this.color) {
/*     */     case IRONCLAD: 
/* 112 */       this.deckString = createDeckInfoString(Ironclad.getStartingDeck());
/* 113 */       break;
/*     */     case THE_SILENT: 
/* 115 */       this.deckString = createDeckInfoString(TheSilent.getStartingDeck());
/* 116 */       break;
/*     */     case DEFECT: 
/* 118 */       this.deckString = createDeckInfoString(Defect.getStartingDeck());
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String createDeckInfoString(ArrayList<String> deck)
/*     */   {
/* 131 */     HashMap<String, Integer> cards = new HashMap();
/*     */     
/* 133 */     for (Iterator localIterator = deck.iterator(); localIterator.hasNext();) { s = (String)localIterator.next();
/* 134 */       if (!cards.containsKey(s)) {
/* 135 */         cards.put(s, Integer.valueOf(1));
/*     */       } else {
/* 137 */         cards.put(s, Integer.valueOf(((Integer)cards.get(s)).intValue() + 1));
/*     */       }
/*     */     }
/*     */     String s;
/* 141 */     StringBuilder sb = new StringBuilder();
/* 142 */     for (Map.Entry<String, Integer> c : cards.entrySet()) {
/* 143 */       sb.append("#b").append(c.getValue()).append(" ").append((String)c.getKey());
/* 144 */       if (((Integer)c.getValue()).intValue() > 1) {
/* 145 */         sb.append("s");
/*     */       }
/* 147 */       sb.append(", ");
/*     */     }
/* 149 */     String retVal = sb.toString();
/*     */     
/* 151 */     if (retVal.length() > 80) {
/* 152 */       return "Click the deck icon to view starting cards.";
/*     */     }
/* 154 */     return retVal.substring(0, retVal.length() - 2);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\CharSelectInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */