/*      */ package com.megacrit.cardcrawl.screens;
/*      */ 
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
/*      */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.DescriptionLine;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Scanner;
/*      */ import java.util.TreeMap;
/*      */ 
/*      */ public class SingleCardViewPopup
/*      */ {
/*   33 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SingleCardViewPopup");
/*   34 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   
/*   36 */   public boolean isOpen = false;
/*      */   private CardGroup group;
/*      */   private AbstractCard card;
/*   39 */   private AbstractCard prevCard; private AbstractCard nextCard; private Texture portraitImg = null;
/*      */   private Hitbox nextHb;
/*   41 */   private Hitbox prevHb; private Hitbox cardHb; private float fadeTimer = 0.0F;
/*   42 */   private Color fadeColor = Color.BLACK.cpy();
/*      */   private static final float LINE_SPACING = 1.53F;
/*      */   private float current_x;
/*      */   private float current_y;
/*      */   private Scanner scanner;
/*      */   private float drawScale;
/*   48 */   private float card_energy_w; private static final float DESC_OFFSET_Y2 = -12.0F; private static final Color CARD_TYPE_COLOR = new Color(0.35F, 0.35F, 0.35F, 1.0F);
/*   49 */   private static final GlyphLayout gl = new GlyphLayout();
/*      */   
/*      */ 
/*   52 */   public static boolean isViewingUpgrade = false;
/*   53 */   public static boolean enableUpgradeToggle = true;
/*   54 */   private Hitbox upgradeHb = new Hitbox(250.0F * Settings.scale, 80.0F * Settings.scale);
/*      */   
/*      */   public SingleCardViewPopup() {
/*   57 */     this.prevHb = new Hitbox(200.0F * Settings.scale, 70.0F * Settings.scale);
/*   58 */     this.nextHb = new Hitbox(200.0F * Settings.scale, 70.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   public void open(AbstractCard card, CardGroup group) {
/*   62 */     CardCrawlGame.isPopupOpen = true;
/*      */     
/*   64 */     this.prevCard = null;
/*   65 */     this.nextCard = null;
/*   66 */     this.prevHb = null;
/*   67 */     this.nextHb = null;
/*      */     
/*   69 */     for (int i = 0; i < group.size(); i++) {
/*   70 */       if (group.group.get(i) == card) {
/*   71 */         if (i != 0) {
/*   72 */           this.prevCard = ((AbstractCard)group.group.get(i - 1));
/*      */         }
/*   74 */         if (i == group.size() - 1) break;
/*   75 */         this.nextCard = ((AbstractCard)group.group.get(i + 1)); break;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*   81 */     this.prevHb = new Hitbox(160.0F * Settings.scale, 160.0F * Settings.scale);
/*   82 */     this.nextHb = new Hitbox(160.0F * Settings.scale, 160.0F * Settings.scale);
/*   83 */     this.prevHb.move(Settings.WIDTH / 2.0F - 400.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*   84 */     this.nextHb.move(Settings.WIDTH / 2.0F + 400.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*      */     
/*   86 */     this.card_energy_w = (24.0F * Settings.scale);
/*   87 */     this.drawScale = 2.0F;
/*   88 */     this.cardHb = new Hitbox(550.0F * Settings.scale, 770.0F * Settings.scale);
/*   89 */     this.cardHb.move(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F);
/*   90 */     this.card = card.makeStatEquivalentCopy();
/*   91 */     this.portraitImg = ImageMaster.loadImage("images/1024Portraits/" + card.assetURL + ".png");
/*   92 */     this.group = group;
/*   93 */     this.isOpen = true;
/*   94 */     this.fadeTimer = 0.25F;
/*   95 */     this.fadeColor.a = 0.0F;
/*   96 */     this.current_x = (Settings.WIDTH / 2.0F - 10.0F * Settings.scale);
/*   97 */     this.current_y = (Settings.HEIGHT / 2.0F - 300.0F * Settings.scale);
/*   98 */     this.upgradeHb.move(Settings.WIDTH / 2.0F, 70.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   public void open(AbstractCard card) {
/*  102 */     CardCrawlGame.isPopupOpen = true;
/*      */     
/*  104 */     this.prevCard = null;
/*  105 */     this.nextCard = null;
/*  106 */     this.prevHb = null;
/*  107 */     this.nextHb = null;
/*      */     
/*  109 */     this.card_energy_w = (24.0F * Settings.scale);
/*  110 */     this.drawScale = 2.0F;
/*  111 */     this.cardHb = new Hitbox(550.0F * Settings.scale, 770.0F * Settings.scale);
/*  112 */     this.cardHb.move(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F);
/*  113 */     this.card = card.makeStatEquivalentCopy();
/*  114 */     this.portraitImg = ImageMaster.loadImage("images/1024Portraits/" + card.assetURL + ".png");
/*  115 */     this.group = null;
/*  116 */     this.isOpen = true;
/*  117 */     this.fadeTimer = 0.25F;
/*  118 */     this.fadeColor.a = 0.0F;
/*  119 */     this.current_x = (Settings.WIDTH / 2.0F - 10.0F * Settings.scale);
/*  120 */     this.current_y = (Settings.HEIGHT / 2.0F - 300.0F * Settings.scale);
/*  121 */     this.upgradeHb.move(Settings.WIDTH / 2.0F, 70.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   public void close() {
/*  125 */     isViewingUpgrade = false;
/*  126 */     InputHelper.justReleasedClickLeft = false;
/*  127 */     CardCrawlGame.isPopupOpen = false;
/*  128 */     this.isOpen = false;
/*  129 */     if (this.portraitImg != null) {
/*  130 */       this.portraitImg.dispose();
/*  131 */       this.portraitImg = null;
/*      */     }
/*      */   }
/*      */   
/*      */   public void update() {
/*  136 */     this.cardHb.update();
/*  137 */     updateArrows();
/*  138 */     updateInput();
/*  139 */     updateFade();
/*  140 */     if (allowUpgradePreview()) {
/*  141 */       updateUpgradePreview();
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateUpgradePreview() {
/*  146 */     this.upgradeHb.update();
/*      */     
/*  148 */     if ((this.upgradeHb.hovered) && (InputHelper.justClickedLeft)) {
/*  149 */       this.upgradeHb.clickStarted = true;
/*      */     }
/*      */     
/*  152 */     if ((this.upgradeHb.clicked) || (CInputActionSet.proceed.isJustPressed())) {
/*  153 */       CInputActionSet.topPanel.unpress();
/*  154 */       this.upgradeHb.clicked = false;
/*  155 */       isViewingUpgrade = !isViewingUpgrade;
/*      */     }
/*      */   }
/*      */   
/*      */   private boolean allowUpgradePreview() {
/*  160 */     return (enableUpgradeToggle) && (this.card.color != com.megacrit.cardcrawl.cards.AbstractCard.CardColor.CURSE) && (this.card.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.STATUS);
/*      */   }
/*      */   
/*      */   private void updateArrows()
/*      */   {
/*  165 */     if (this.prevCard != null) {
/*  166 */       this.prevHb.update();
/*  167 */       if (this.prevHb.justHovered) {
/*  168 */         CardCrawlGame.sound.play("UI_HOVER");
/*      */       }
/*  170 */       if ((this.prevHb.clicked) || ((this.prevCard != null) && (CInputActionSet.pageLeftViewDeck.isJustPressed()))) {
/*  171 */         CInputActionSet.pageLeftViewDeck.unpress();
/*  172 */         openPrev();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  177 */     if (this.nextCard != null) {
/*  178 */       this.nextHb.update();
/*  179 */       if (this.nextHb.justHovered) {
/*  180 */         CardCrawlGame.sound.play("UI_HOVER");
/*      */       }
/*      */       
/*  183 */       if ((this.nextHb.clicked) || ((this.nextCard != null) && (CInputActionSet.pageRightViewExhaust.isJustPressed()))) {
/*  184 */         CInputActionSet.pageRightViewExhaust.unpress();
/*  185 */         openNext();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateInput() {
/*  191 */     if (InputHelper.justClickedLeft) {
/*  192 */       if ((this.prevCard != null) && (this.prevHb.hovered)) {
/*  193 */         this.prevHb.clickStarted = true;
/*  194 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*  195 */         return; }
/*  196 */       if ((this.nextCard != null) && (this.nextHb.hovered)) {
/*  197 */         this.nextHb.clickStarted = true;
/*  198 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*  199 */         return;
/*      */       }
/*      */     }
/*      */     
/*  203 */     if (InputHelper.justReleasedClickLeft) {
/*  204 */       if ((!this.cardHb.hovered) && (!this.upgradeHb.hovered)) {
/*  205 */         close();
/*      */       }
/*  207 */     } else if ((InputHelper.pressedEscape) || (CInputActionSet.cancel.isJustPressed())) {
/*  208 */       CInputActionSet.cancel.unpress();
/*  209 */       InputHelper.pressedEscape = false;
/*  210 */       close();
/*      */     }
/*      */     
/*      */ 
/*  214 */     if ((this.prevCard != null) && (InputActionSet.left.isJustPressed())) {
/*  215 */       openPrev();
/*  216 */     } else if ((this.nextCard != null) && (InputActionSet.right.isJustPressed())) {
/*  217 */       openNext();
/*      */     }
/*      */   }
/*      */   
/*      */   private void openPrev() {
/*  222 */     boolean tmp = isViewingUpgrade;
/*  223 */     close();
/*  224 */     open(this.prevCard, this.group);
/*  225 */     isViewingUpgrade = tmp;
/*  226 */     this.fadeTimer = 0.0F;
/*  227 */     this.fadeColor.a = 0.9F;
/*      */   }
/*      */   
/*      */   private void openNext() {
/*  231 */     boolean tmp = isViewingUpgrade;
/*  232 */     close();
/*  233 */     open(this.nextCard, this.group);
/*  234 */     isViewingUpgrade = tmp;
/*  235 */     this.fadeTimer = 0.0F;
/*  236 */     this.fadeColor.a = 0.9F;
/*      */   }
/*      */   
/*      */   private void updateFade() {
/*  240 */     this.fadeTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  241 */     if (this.fadeTimer < 0.0F) {
/*  242 */       this.fadeTimer = 0.0F;
/*      */     }
/*  244 */     this.fadeColor.a = com.badlogic.gdx.math.Interpolation.pow2In.apply(0.9F, 0.0F, this.fadeTimer * 4.0F);
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb)
/*      */   {
/*  249 */     AbstractCard copy = null;
/*  250 */     if (isViewingUpgrade) {
/*  251 */       copy = this.card.makeStatEquivalentCopy();
/*  252 */       this.card.upgrade();
/*  253 */       this.card.displayUpgrades();
/*      */     }
/*      */     
/*  256 */     sb.setColor(this.fadeColor);
/*  257 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*  258 */     sb.setColor(Color.WHITE);
/*  259 */     renderCardBack(sb);
/*  260 */     renderPortrait(sb);
/*  261 */     renderFrame(sb);
/*  262 */     renderCardBanner(sb);
/*  263 */     renderCardTypeText(sb);
/*  264 */     if (Settings.lineBreakViaCharacter) {
/*  265 */       renderDescriptionCN(sb);
/*      */     } else {
/*  267 */       renderDescription(sb);
/*      */     }
/*  269 */     renderTitle(sb);
/*  270 */     renderCost(sb);
/*  271 */     renderArrows(sb);
/*  272 */     renderTips(sb);
/*      */     
/*  274 */     this.cardHb.render(sb);
/*  275 */     if (this.nextHb != null) {
/*  276 */       this.nextHb.render(sb);
/*      */     }
/*  278 */     if (this.prevHb != null) {
/*  279 */       this.prevHb.render(sb);
/*      */     }
/*  281 */     if (allowUpgradePreview()) {
/*  282 */       renderUpgradeViewToggle(sb);
/*  283 */       if (Settings.isControllerMode) {
/*  284 */         sb.draw(CInputActionSet.proceed
/*  285 */           .getKeyImg(), Settings.WIDTH / 2.0F - 32.0F - 155.0F * Settings.scale, -32.0F + 67.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  304 */     if (copy != null) {
/*  305 */       this.card = copy;
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderCardBack(SpriteBatch sb) {
/*  310 */     Texture tmpImg = null;
/*      */     
/*  312 */     switch (this.card.type) {
/*      */     case ATTACK: 
/*  314 */       switch (this.card.color) {
/*      */       case RED: 
/*  316 */         tmpImg = ImageMaster.CARD_ATTACK_BG_RED_L;
/*  317 */         break;
/*      */       case GREEN: 
/*  319 */         tmpImg = ImageMaster.CARD_ATTACK_BG_GREEN_L;
/*  320 */         break;
/*      */       case BLUE: 
/*  322 */         tmpImg = ImageMaster.CARD_ATTACK_BG_BLUE_L;
/*  323 */         break;
/*      */       case COLORLESS: 
/*  325 */         tmpImg = ImageMaster.CARD_ATTACK_BG_GRAY_L;
/*      */       }
/*      */       
/*  328 */       break;
/*      */     
/*      */ 
/*      */     case POWER: 
/*  332 */       switch (this.card.color) {
/*      */       case RED: 
/*  334 */         tmpImg = ImageMaster.CARD_POWER_BG_RED_L;
/*  335 */         break;
/*      */       case GREEN: 
/*  337 */         tmpImg = ImageMaster.CARD_POWER_BG_GREEN_L;
/*  338 */         break;
/*      */       case BLUE: 
/*  340 */         tmpImg = ImageMaster.CARD_POWER_BG_BLUE_L;
/*  341 */         break;
/*      */       case COLORLESS: 
/*  343 */         tmpImg = ImageMaster.CARD_POWER_BG_GRAY_L;
/*      */       }
/*      */       
/*  346 */       break;
/*      */     
/*      */ 
/*      */     default: 
/*  350 */       switch (this.card.color) {
/*      */       case RED: 
/*  352 */         tmpImg = ImageMaster.CARD_SKILL_BG_RED_L;
/*  353 */         break;
/*      */       case GREEN: 
/*  355 */         tmpImg = ImageMaster.CARD_SKILL_BG_GREEN_L;
/*  356 */         break;
/*      */       case BLUE: 
/*  358 */         tmpImg = ImageMaster.CARD_SKILL_BG_BLUE_L;
/*  359 */         break;
/*      */       case COLORLESS: 
/*  361 */         tmpImg = ImageMaster.CARD_SKILL_BG_GRAY_L;
/*  362 */         break;
/*      */       case CURSE: 
/*  364 */         tmpImg = ImageMaster.CARD_SKILL_BG_BLACK_L; }
/*  365 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  372 */     if (tmpImg == null) {
/*  373 */       return;
/*      */     }
/*  375 */     sb.draw(tmpImg, Settings.WIDTH / 2.0F - 512.0F, Settings.HEIGHT / 2.0F - 512.0F, 512.0F, 512.0F, 1024.0F, 1024.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1024, 1024, false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderPortrait(SpriteBatch sb)
/*      */   {
/*  395 */     if (this.card.isLocked) {
/*  396 */       switch (this.card.type) {
/*      */       case ATTACK: 
/*  398 */         sb.draw(ImageMaster.CARD_LOCKED_ATTACK_L, Settings.WIDTH / 2.0F - 250.0F, Settings.HEIGHT / 2.0F - 190.0F + 136.0F * Settings.scale, 250.0F, 190.0F, 500.0F, 380.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 500, 380, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  415 */         break;
/*      */       case SKILL: 
/*  417 */         sb.draw(ImageMaster.CARD_LOCKED_SKILL_L, Settings.WIDTH / 2.0F - 250.0F, Settings.HEIGHT / 2.0F - 190.0F + 136.0F * Settings.scale, 250.0F, 190.0F, 500.0F, 380.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 500, 380, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  434 */         break;
/*      */       case POWER: 
/*  436 */         sb.draw(ImageMaster.CARD_LOCKED_POWER_L, Settings.WIDTH / 2.0F - 250.0F, Settings.HEIGHT / 2.0F - 190.0F + 136.0F * Settings.scale, 250.0F, 190.0F, 500.0F, 380.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 500, 380, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  453 */         break;
/*      */       default: 
/*  455 */         sb.draw(ImageMaster.CARD_LOCKED_SKILL_L, Settings.WIDTH / 2.0F - 250.0F, Settings.HEIGHT / 2.0F - 190.0F + 136.0F * Settings.scale, 250.0F, 190.0F, 500.0F, 380.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 500, 380, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  472 */         break;
/*      */       }
/*      */       
/*  475 */     } else if (this.portraitImg != null) {
/*  476 */       sb.draw(this.portraitImg, Settings.WIDTH / 2.0F - 250.0F, Settings.HEIGHT / 2.0F - 190.0F + 136.0F * Settings.scale, 250.0F, 190.0F, 500.0F, 380.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 500, 380, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderFrame(SpriteBatch sb)
/*      */   {
/*  498 */     Texture tmpImg = null;
/*  499 */     switch (this.card.type) {
/*      */     case ATTACK: 
/*  501 */       switch (this.card.rarity) {
/*      */       case COMMON: 
/*  503 */         tmpImg = ImageMaster.CARD_FRAME_ATTACK_COMMON_L;
/*  504 */         break;
/*      */       case UNCOMMON: 
/*  506 */         tmpImg = ImageMaster.CARD_FRAME_ATTACK_UNCOMMON_L;
/*  507 */         break;
/*      */       case RARE: 
/*  509 */         tmpImg = ImageMaster.CARD_FRAME_ATTACK_RARE_L;
/*  510 */         break;
/*      */       default: 
/*  512 */         tmpImg = ImageMaster.CARD_FRAME_ATTACK_COMMON_L; }
/*  513 */       break;
/*      */     
/*      */ 
/*      */     case POWER: 
/*  517 */       switch (this.card.rarity) {
/*      */       case COMMON: 
/*  519 */         tmpImg = ImageMaster.CARD_FRAME_POWER_COMMON_L;
/*  520 */         break;
/*      */       case UNCOMMON: 
/*  522 */         tmpImg = ImageMaster.CARD_FRAME_POWER_UNCOMMON_L;
/*  523 */         break;
/*      */       case RARE: 
/*  525 */         tmpImg = ImageMaster.CARD_FRAME_POWER_RARE_L;
/*  526 */         break;
/*      */       default: 
/*  528 */         tmpImg = ImageMaster.CARD_FRAME_POWER_COMMON_L; }
/*  529 */       break;
/*      */     
/*      */ 
/*      */     default: 
/*  533 */       switch (this.card.rarity) {
/*      */       case COMMON: 
/*  535 */         tmpImg = ImageMaster.CARD_FRAME_SKILL_COMMON_L;
/*  536 */         break;
/*      */       case UNCOMMON: 
/*  538 */         tmpImg = ImageMaster.CARD_FRAME_SKILL_UNCOMMON_L;
/*  539 */         break;
/*      */       case RARE: 
/*  541 */         tmpImg = ImageMaster.CARD_FRAME_SKILL_RARE_L;
/*  542 */         break;
/*      */       default: 
/*  544 */         tmpImg = ImageMaster.CARD_FRAME_SKILL_COMMON_L;
/*      */       }
/*      */       
/*      */       break;
/*      */     }
/*  549 */     sb.draw(tmpImg, Settings.WIDTH / 2.0F - 512.0F, Settings.HEIGHT / 2.0F - 512.0F, 512.0F, 512.0F, 1024.0F, 1024.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1024, 1024, false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderCardBanner(SpriteBatch sb)
/*      */   {
/*  569 */     Texture tmpImg = null;
/*  570 */     switch (this.card.rarity) {
/*      */     case COMMON: 
/*  572 */       tmpImg = ImageMaster.CARD_BANNER_COMMON_L;
/*  573 */       break;
/*      */     case UNCOMMON: 
/*  575 */       tmpImg = ImageMaster.CARD_BANNER_UNCOMMON_L;
/*  576 */       break;
/*      */     case RARE: 
/*  578 */       tmpImg = ImageMaster.CARD_BANNER_RARE_L;
/*  579 */       break;
/*      */     default: 
/*  581 */       tmpImg = ImageMaster.CARD_BANNER_COMMON_L;
/*      */     }
/*      */     
/*  584 */     sb.draw(tmpImg, Settings.WIDTH / 2.0F - 512.0F, Settings.HEIGHT / 2.0F - 512.0F, 512.0F, 512.0F, 1024.0F, 1024.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1024, 1024, false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private String getDynamicValue(char key)
/*      */   {
/*  605 */     switch (key) {
/*      */     case 'B': 
/*  607 */       if (this.card.isBlockModified) {
/*  608 */         if (this.card.block >= this.card.baseBlock) {
/*  609 */           return "[#7fff00]" + Integer.toString(this.card.block) + "[]";
/*      */         }
/*  611 */         return "[#ff6563]" + Integer.toString(this.card.block) + "[]";
/*      */       }
/*      */       
/*  614 */       return Integer.toString(this.card.baseBlock);
/*      */     
/*      */     case 'D': 
/*  617 */       if (this.card.isDamageModified) {
/*  618 */         if (this.card.damage >= this.card.baseDamage) {
/*  619 */           return "[#7fff00]" + Integer.toString(this.card.damage) + "[]";
/*      */         }
/*  621 */         return "[#ff6563]" + Integer.toString(this.card.damage) + "[]";
/*      */       }
/*      */       
/*  624 */       return Integer.toString(this.card.baseDamage);
/*      */     
/*      */     case 'M': 
/*  627 */       if (this.card.isMagicNumberModified) {
/*  628 */         if (this.card.magicNumber >= this.card.baseMagicNumber) {
/*  629 */           return "[#7fff00]" + Integer.toString(this.card.magicNumber) + "[]";
/*      */         }
/*  631 */         return "[#ff6563]" + Integer.toString(this.card.magicNumber) + "[]";
/*      */       }
/*      */       
/*  634 */       return Integer.toString(this.card.baseMagicNumber);
/*      */     }
/*      */     
/*  637 */     System.out.println("KEY: " + key);
/*  638 */     return Integer.toString(-99);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderDescriptionCN(SpriteBatch sb)
/*      */   {
/*  646 */     if ((this.card.isLocked) || (!this.card.isSeen)) {
/*  647 */       FontHelper.renderFontCentered(sb, FontHelper.largeCardFont, "? ? ?", Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 195.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  654 */       return;
/*      */     }
/*      */     
/*  657 */     BitmapFont font = FontHelper.SCP_cardDescFont;
/*  658 */     float draw_y = this.current_y + 100.0F * Settings.scale;
/*  659 */     draw_y += this.card.description.size() * font.getCapHeight() * 0.775F - font.getCapHeight() * 0.375F;
/*  660 */     float spacing = 1.53F * -font.getCapHeight() / Settings.scale / this.drawScale;
/*  661 */     GlyphLayout gl = new GlyphLayout();
/*      */     
/*  663 */     for (int i = 0; i < this.card.description.size(); i++) {
/*  664 */       float start_x = this.current_x - ((DescriptionLine)this.card.description.get(i)).width * this.drawScale / 2.0F - 20.0F * Settings.scale;
/*  665 */       this.scanner = new Scanner(((DescriptionLine)this.card.description.get(i)).text);
/*      */       
/*  667 */       while (this.scanner.hasNext()) {
/*  668 */         String tmp = this.scanner.next();
/*      */         
/*  670 */         tmp = tmp.replace("!", "");
/*  671 */         String updateTmp = null;
/*  672 */         for (int j = 0; j < tmp.length(); j++) {
/*  673 */           if ((tmp.charAt(j) == 'D') || ((tmp.charAt(j) == 'B') && (!tmp.contains("[B]"))) || (tmp.charAt(j) == 'M'))
/*      */           {
/*  675 */             updateTmp = tmp.substring(0, j);
/*  676 */             updateTmp = updateTmp + getDynamicValue(tmp.charAt(j));
/*  677 */             updateTmp = updateTmp + tmp.substring(j + 1);
/*  678 */             break;
/*      */           }
/*      */         }
/*      */         
/*  682 */         if (updateTmp != null) {
/*  683 */           tmp = updateTmp;
/*      */         }
/*      */         
/*      */ 
/*  687 */         for (int j = 0; j < tmp.length(); j++) {
/*  688 */           if ((tmp.charAt(j) == 'D') || ((tmp.charAt(j) == 'B') && (!tmp.contains("[B]"))) || (tmp.charAt(j) == 'M'))
/*      */           {
/*  690 */             updateTmp = tmp.substring(0, j);
/*  691 */             updateTmp = updateTmp + getDynamicValue(tmp.charAt(j));
/*  692 */             updateTmp = updateTmp + tmp.substring(j + 1);
/*  693 */             break;
/*      */           }
/*      */         }
/*      */         
/*  697 */         if (updateTmp != null) {
/*  698 */           tmp = updateTmp;
/*      */         }
/*      */         
/*      */ 
/*  702 */         if (tmp.charAt(0) == '*') {
/*  703 */           tmp = tmp.substring(1);
/*  704 */           String punctuation = "";
/*  705 */           if ((tmp.length() > 1) && (!Character.isLetter(tmp.charAt(tmp.length() - 2)))) {
/*  706 */             punctuation = punctuation + tmp.charAt(tmp.length() - 2);
/*  707 */             tmp = tmp.substring(0, tmp.length() - 2);
/*  708 */             punctuation = punctuation + ' ';
/*      */           }
/*      */           
/*  711 */           gl.setText(font, tmp);
/*  712 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  719 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.GOLD_COLOR
/*      */             
/*      */ 
/*  722 */             .cpy());
/*      */           
/*  724 */           start_x = Math.round(start_x + gl.width);
/*  725 */           gl.setText(font, punctuation);
/*  726 */           FontHelper.renderRotatedText(sb, font, punctuation, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  733 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR
/*      */             
/*      */ 
/*  736 */             .cpy());
/*      */           
/*  738 */           gl.setText(font, punctuation);
/*  739 */           start_x += gl.width;
/*      */ 
/*      */         }
/*  742 */         else if (tmp.equals("[R]")) {
/*  743 */           gl.width = (this.card_energy_w * this.drawScale);
/*  744 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  745 */           renderSmallEnergy(sb, ImageMaster.RED_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  751 */           start_x += gl.width;
/*  752 */         } else if (tmp.equals("[G]")) {
/*  753 */           gl.width = (this.card_energy_w * this.drawScale);
/*  754 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  755 */           renderSmallEnergy(sb, ImageMaster.GREEN_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  761 */           start_x += gl.width;
/*  762 */         } else if (tmp.equals("[B]")) {
/*  763 */           gl.width = (this.card_energy_w * this.drawScale);
/*  764 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  765 */           renderSmallEnergy(sb, ImageMaster.BLUE_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  771 */           start_x += gl.width;
/*      */         }
/*      */         else
/*      */         {
/*  775 */           gl.setText(font, tmp);
/*  776 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  783 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */           
/*      */ 
/*      */ 
/*  787 */           start_x += gl.width;
/*      */         }
/*      */       }
/*      */     }
/*  791 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void renderDescription(SpriteBatch sb)
/*      */   {
/*  798 */     if ((this.card.isLocked) || (!this.card.isSeen)) {
/*  799 */       FontHelper.renderFontCentered(sb, FontHelper.largeCardFont, "? ? ?", Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 195.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  806 */       return;
/*      */     }
/*      */     
/*  809 */     BitmapFont font = FontHelper.SCP_cardDescFont;
/*  810 */     float draw_y = this.current_y + 100.0F * Settings.scale;
/*  811 */     draw_y += this.card.description.size() * font.getCapHeight() * 0.775F - font.getCapHeight() * 0.375F;
/*  812 */     float spacing = 1.53F * -font.getCapHeight() / Settings.scale / this.drawScale;
/*  813 */     GlyphLayout gl = new GlyphLayout();
/*      */     
/*  815 */     for (int i = 0; i < this.card.description.size(); i++) {
/*  816 */       float start_x = this.current_x - ((DescriptionLine)this.card.description.get(i)).width * this.drawScale / 2.0F;
/*  817 */       this.scanner = new Scanner(((DescriptionLine)this.card.description.get(i)).text);
/*      */       
/*  819 */       while (this.scanner.hasNext()) {
/*  820 */         String tmp = this.scanner.next() + ' ';
/*      */         
/*      */ 
/*  823 */         if (tmp.charAt(0) == '*') {
/*  824 */           tmp = tmp.substring(1);
/*  825 */           String punctuation = "";
/*  826 */           if ((tmp.length() > 1) && (!Character.isLetter(tmp.charAt(tmp.length() - 2)))) {
/*  827 */             punctuation = punctuation + tmp.charAt(tmp.length() - 2);
/*  828 */             tmp = tmp.substring(0, tmp.length() - 2);
/*  829 */             punctuation = punctuation + ' ';
/*      */           }
/*      */           
/*  832 */           gl.setText(font, tmp);
/*  833 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  840 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.GOLD_COLOR
/*      */             
/*      */ 
/*  843 */             .cpy());
/*      */           
/*  845 */           start_x = Math.round(start_x + gl.width);
/*  846 */           gl.setText(font, punctuation);
/*  847 */           FontHelper.renderRotatedText(sb, font, punctuation, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  854 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR
/*      */             
/*      */ 
/*  857 */             .cpy());
/*      */           
/*  859 */           gl.setText(font, punctuation);
/*  860 */           start_x += gl.width;
/*      */ 
/*      */         }
/*  863 */         else if (tmp.charAt(0) == '!') {
/*  864 */           if (tmp.length() == 4) {
/*  865 */             start_x += renderDynamicVariable(tmp.charAt(1), start_x, draw_y, i, font, sb, null);
/*  866 */           } else if (tmp.length() == 5) {
/*  867 */             start_x += renderDynamicVariable(tmp.charAt(1), start_x, draw_y, i, font, sb, Character.valueOf(tmp.charAt(3)));
/*      */           }
/*      */           
/*      */         }
/*  871 */         else if (tmp.equals("[R] ")) {
/*  872 */           gl.width = (this.card_energy_w * this.drawScale);
/*  873 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  874 */           renderSmallEnergy(sb, ImageMaster.RED_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  880 */           start_x += gl.width;
/*      */ 
/*      */         }
/*  883 */         else if (tmp.equals("[R]. ")) {
/*  884 */           gl.width = (this.card_energy_w * this.drawScale / Settings.scale);
/*  885 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  886 */           renderSmallEnergy(sb, ImageMaster.RED_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  892 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + this.card_energy_w * this.drawScale, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  899 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*  904 */           start_x += gl.width;
/*  905 */           gl.setText(font, ".");
/*  906 */           start_x += gl.width;
/*      */         }
/*  908 */         else if (tmp.equals("[G] ")) {
/*  909 */           gl.width = (this.card_energy_w * this.drawScale);
/*  910 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  911 */           renderSmallEnergy(sb, ImageMaster.GREEN_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  917 */           start_x += gl.width;
/*      */         }
/*  919 */         else if (tmp.equals("[G]. ")) {
/*  920 */           gl.width = (this.card_energy_w * this.drawScale);
/*  921 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  922 */           renderSmallEnergy(sb, ImageMaster.GREEN_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  928 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + this.card_energy_w * this.drawScale, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  935 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */           
/*      */ 
/*      */ 
/*  939 */           start_x += gl.width;
/*      */ 
/*      */         }
/*  942 */         else if (tmp.equals("[B] ")) {
/*  943 */           gl.width = (this.card_energy_w * this.drawScale);
/*  944 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  945 */           renderSmallEnergy(sb, ImageMaster.BLUE_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  951 */           start_x += gl.width;
/*      */         }
/*  953 */         else if (tmp.equals("[B]. ")) {
/*  954 */           gl.width = (this.card_energy_w * this.drawScale);
/*  955 */           float tmp2 = (this.card.description.size() - 4) * spacing;
/*  956 */           renderSmallEnergy(sb, ImageMaster.BLUE_ORB, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + this.card_energy_w * this.drawScale / Settings.scale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  962 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + this.card_energy_w * this.drawScale, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  969 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */           
/*      */ 
/*      */ 
/*  973 */           start_x += gl.width;
/*      */         }
/*      */         else
/*      */         {
/*  977 */           gl.setText(font, tmp);
/*  978 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  985 */             -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */           
/*      */ 
/*      */ 
/*  989 */           start_x += gl.width;
/*      */         }
/*      */       }
/*      */     }
/*  993 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */   private void renderSmallEnergy(SpriteBatch sb, TextureAtlas.AtlasRegion region, float x, float y) {
/*  997 */     sb.setColor(Color.WHITE);
/*  998 */     sb.draw(region
/*  999 */       .getTexture(), this.current_x + x * Settings.scale * this.drawScale + region.offsetX * Settings.scale - 4.0F * Settings.scale, this.current_y + y * Settings.scale * this.drawScale / 2.0F + 280.0F * Settings.scale, 0.0F, 0.0F, region.packedWidth, region.packedHeight, this.drawScale * Settings.scale, this.drawScale * Settings.scale, 0.0F, region
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1009 */       .getRegionX(), region
/* 1010 */       .getRegionY(), region
/* 1011 */       .getRegionWidth(), region
/* 1012 */       .getRegionHeight(), false, false);
/*      */   }
/*      */   
/*      */ 
/*      */   private void renderCardTypeText(SpriteBatch sb)
/*      */   {
/* 1018 */     String label = "";
/* 1019 */     switch (this.card.type) {
/*      */     case ATTACK: 
/* 1021 */       label = TEXT[0];
/* 1022 */       break;
/*      */     case SKILL: 
/* 1024 */       label = TEXT[1];
/* 1025 */       break;
/*      */     case POWER: 
/* 1027 */       label = TEXT[2];
/* 1028 */       break;
/*      */     case CURSE: 
/* 1030 */       label = TEXT[3];
/* 1031 */       break;
/*      */     case STATUS: 
/* 1033 */       label = TEXT[7];
/* 1034 */       break;
/*      */     }
/*      */     
/*      */     
/* 1038 */     FontHelper.renderFontCentered(sb, FontHelper.SCP_cardTypeFont, label, Settings.WIDTH / 2.0F + 3.0F * Settings.scale, Settings.HEIGHT / 2.0F - 40.0F * Settings.scale, CARD_TYPE_COLOR);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private float renderDynamicVariable(char key, float start_x, float draw_y, int i, BitmapFont font, SpriteBatch sb, Character end)
/*      */   {
/* 1056 */     StringBuilder stringBuilder = new StringBuilder();
/* 1057 */     Color c = null;
/* 1058 */     int num = 0;
/*      */     
/* 1060 */     switch (key) {
/*      */     case 'D': 
/* 1062 */       num = this.card.baseDamage;
/* 1063 */       if (this.card.upgradedDamage) {
/* 1064 */         c = Settings.GREEN_TEXT_COLOR;
/*      */       } else {
/* 1066 */         c = Settings.CREAM_COLOR;
/*      */       }
/* 1068 */       break;
/*      */     case 'B': 
/* 1070 */       num = this.card.baseBlock;
/* 1071 */       if (this.card.upgradedBlock) {
/* 1072 */         c = Settings.GREEN_TEXT_COLOR;
/*      */       } else {
/* 1074 */         c = Settings.CREAM_COLOR;
/*      */       }
/* 1076 */       break;
/*      */     case 'M': 
/* 1078 */       num = this.card.baseMagicNumber;
/* 1079 */       if (this.card.upgradedMagicNumber) {
/* 1080 */         c = Settings.GREEN_TEXT_COLOR;
/*      */       } else {
/* 1082 */         c = Settings.CREAM_COLOR;
/*      */       }
/* 1084 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 1089 */     stringBuilder.append(Integer.toString(num));
/* 1090 */     gl.setText(font, stringBuilder.toString());
/* 1091 */     FontHelper.renderRotatedText(sb, font, stringBuilder
/*      */     
/*      */ 
/* 1094 */       .toString(), this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.53F * 
/*      */       
/*      */ 
/*      */ 
/* 1098 */       -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, c);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1103 */     if (end != null) {
/* 1104 */       FontHelper.renderRotatedText(sb, font, 
/*      */       
/*      */ 
/* 1107 */         Character.toString(end.charValue()), this.current_x, this.current_y, start_x - this.current_x + gl.width + 10.0F * Settings.scale, i * 1.53F * 
/*      */         
/*      */ 
/*      */ 
/* 1111 */         -font.getCapHeight() + draw_y - this.current_y + -12.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1117 */     stringBuilder.append(' ');
/* 1118 */     gl.setText(font, stringBuilder.toString());
/* 1119 */     return gl.width;
/*      */   }
/*      */   
/*      */   private void renderTitle(SpriteBatch sb) {
/* 1123 */     if (this.card.isLocked) {
/* 1124 */       FontHelper.renderFontCentered(sb, FontHelper.SCP_cardTitleFont_small, TEXT[4], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 338.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/* 1131 */     else if (this.card.isSeen) {
/* 1132 */       if ((!isViewingUpgrade) || (allowUpgradePreview())) {
/* 1133 */         FontHelper.renderFontCentered(sb, FontHelper.SCP_cardTitleFont_small, this.card.name, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 338.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/* 1141 */         FontHelper.renderFontCentered(sb, FontHelper.SCP_cardTitleFont_small, this.card.name, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 338.0F * Settings.scale, Settings.GREEN_TEXT_COLOR);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1151 */       FontHelper.renderFontCentered(sb, FontHelper.SCP_cardTitleFont_small, TEXT[5], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 338.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderCost(SpriteBatch sb)
/*      */   {
/* 1162 */     if ((this.card.isLocked) || (!this.card.isSeen)) {
/* 1163 */       return;
/*      */     }
/* 1165 */     if (this.card.cost > -2) {
/* 1166 */       switch (this.card.color) {
/*      */       case RED: 
/* 1168 */         sb.draw(ImageMaster.CARD_RED_ORB_L, Settings.WIDTH / 2.0F - 82.0F - 270.0F * Settings.scale, Settings.HEIGHT / 2.0F - 82.0F + 380.0F * Settings.scale, 82.0F, 82.0F, 164.0F, 164.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 164, 164, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1185 */         break;
/*      */       case GREEN: 
/* 1187 */         sb.draw(ImageMaster.CARD_GREEN_ORB_L, Settings.WIDTH / 2.0F - 82.0F - 270.0F * Settings.scale, Settings.HEIGHT / 2.0F - 82.0F + 380.0F * Settings.scale, 82.0F, 82.0F, 164.0F, 164.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 164, 164, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1204 */         break;
/*      */       case BLUE: 
/* 1206 */         sb.draw(ImageMaster.CARD_BLUE_ORB_L, Settings.WIDTH / 2.0F - 82.0F - 270.0F * Settings.scale, Settings.HEIGHT / 2.0F - 82.0F + 380.0F * Settings.scale, 82.0F, 82.0F, 164.0F, 164.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 164, 164, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1223 */         break;
/*      */       case COLORLESS: 
/* 1225 */         sb.draw(ImageMaster.CARD_GRAY_ORB_L, Settings.WIDTH / 2.0F - 82.0F - 270.0F * Settings.scale, Settings.HEIGHT / 2.0F - 82.0F + 380.0F * Settings.scale, 82.0F, 82.0F, 164.0F, 164.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 164, 164, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1242 */         break;
/*      */       }
/*      */       
/*      */     }
/*      */     
/*      */ 
/* 1248 */     Color c = null;
/* 1249 */     if (this.card.isCostModified) {
/* 1250 */       c = Settings.GREEN_TEXT_COLOR;
/*      */     } else {
/* 1252 */       c = Settings.CREAM_COLOR;
/*      */     }
/* 1254 */     switch (this.card.cost) {
/*      */     case -2: 
/*      */       break;
/*      */     case -1: 
/* 1258 */       FontHelper.renderFont(sb, FontHelper.SCP_cardEnergyFont, "X", 666.0F * Settings.scale, Settings.HEIGHT / 2.0F + 404.0F * Settings.scale, c);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1265 */       break;
/*      */     case 1: 
/* 1267 */       FontHelper.renderFont(sb, FontHelper.SCP_cardEnergyFont, 
/*      */       
/*      */ 
/* 1270 */         Integer.toString(this.card.cost), 674.0F * Settings.scale, Settings.HEIGHT / 2.0F + 404.0F * Settings.scale, c);
/*      */       
/*      */ 
/*      */ 
/* 1274 */       break;
/*      */     case 0: default: 
/* 1276 */       FontHelper.renderFont(sb, FontHelper.SCP_cardEnergyFont, 
/*      */       
/*      */ 
/* 1279 */         Integer.toString(this.card.cost), 668.0F * Settings.scale, Settings.HEIGHT / 2.0F + 404.0F * Settings.scale, c);
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderArrows(SpriteBatch sb)
/*      */   {
/* 1293 */     if (this.prevCard != null) {
/* 1294 */       sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1312 */       if (Settings.isControllerMode) {
/* 1313 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 1314 */           .getKeyImg(), this.prevHb.cX - 32.0F + 0.0F * Settings.scale, this.prevHb.cY - 32.0F + 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1332 */       if (this.prevHb.hovered) {
/* 1333 */         sb.setBlendFunction(770, 1);
/* 1334 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 1335 */         sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1352 */         sb.setColor(Color.WHITE);
/* 1353 */         sb.setBlendFunction(770, 771);
/*      */       }
/*      */     }
/*      */     
/* 1357 */     if (this.nextCard != null) {
/* 1358 */       sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, true, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1376 */       if (Settings.isControllerMode) {
/* 1377 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 1378 */           .getKeyImg(), this.nextHb.cX - 32.0F + 0.0F * Settings.scale, this.nextHb.cY - 32.0F + 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1396 */       if (this.nextHb.hovered) {
/* 1397 */         sb.setBlendFunction(770, 1);
/* 1398 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 1399 */         sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, true, false);
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1416 */         sb.setColor(Color.WHITE);
/* 1417 */         sb.setBlendFunction(770, 771);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderUpgradeViewToggle(SpriteBatch sb) {
/* 1423 */     sb.setColor(Color.WHITE);
/* 1424 */     sb.draw(ImageMaster.OPTION_TOGGLE, 870.0F * Settings.scale - 16.0F, 70.0F * Settings.scale - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 32, 32, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1442 */     if ((this.upgradeHb.hovered) && (!isViewingUpgrade)) {
/* 1443 */       FontHelper.renderFont(sb, FontHelper.cardTitleFont_N, TEXT[6], 900.0F * Settings.scale, 80.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*      */ 
/* 1451 */       FontHelper.renderFont(sb, FontHelper.cardTitleFont_N, TEXT[6], 900.0F * Settings.scale, 80.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1460 */     if (isViewingUpgrade) {
/* 1461 */       sb.setColor(Color.WHITE);
/* 1462 */       sb.draw(ImageMaster.OPTION_TOGGLE_ON, 870.0F * Settings.scale - 16.0F, 70.0F * Settings.scale - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 32, 32, false, false);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1480 */     this.upgradeHb.render(sb);
/*      */   }
/*      */   
/*      */   private void renderTips(SpriteBatch sb) {
/* 1484 */     ArrayList<PowerTip> t = new ArrayList();
/* 1485 */     if (this.card.isLocked) {
/* 1486 */       t.add(new PowerTip(TEXT[4], (String)GameDictionary.keywords.get(TEXT[4].toLowerCase())));
/* 1487 */     } else if (!this.card.isSeen) {
/* 1488 */       t.add(new PowerTip(TEXT[5], (String)GameDictionary.keywords.get(TEXT[5].toLowerCase())));
/*      */     } else {
/* 1490 */       for (String s : this.card.keywords) {
/* 1491 */         if ((!s.equals("[R]")) && (!s.equals("[G]")) && (!s.equals("[B]"))) {
/* 1492 */           t.add(new PowerTip(com.megacrit.cardcrawl.helpers.TipHelper.capitalize(s), (String)GameDictionary.keywords.get(s)));
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1497 */     if (!t.isEmpty()) {
/* 1498 */       com.megacrit.cardcrawl.helpers.TipHelper.queuePowerTips(1300.0F * Settings.scale, 440.0F * Settings.scale, t);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\SingleCardViewPopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */