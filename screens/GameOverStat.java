/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class GameOverStat
/*     */ {
/*     */   public final String label;
/*     */   public final String description;
/*     */   public final String value;
/*  15 */   private static final float VALUE_X = 390.0F * Settings.scale;
/*  16 */   private static final float LINE_W = 1000.0F * Settings.scale;
/*  17 */   public boolean hidden = true;
/*  18 */   private Color color = Settings.CREAM_COLOR.cpy();
/*  19 */   private float offsetX = -50.0F * Settings.scale;
/*  20 */   private boolean line = false;
/*  21 */   private Hitbox hb = null;
/*     */   
/*     */   public GameOverStat() {
/*  24 */     this.label = null;
/*  25 */     this.description = null;
/*  26 */     this.value = null;
/*  27 */     this.color.a = 0.0F;
/*  28 */     this.line = true;
/*     */   }
/*     */   
/*     */   public GameOverStat(String label, String description, String value) {
/*  32 */     this.label = label;
/*     */     
/*  34 */     if ((description != null) && (description.equals(""))) {
/*  35 */       this.description = null;
/*     */     } else {
/*  37 */       this.description = description;
/*     */     }
/*     */     
/*  40 */     if (description != null) {
/*  41 */       this.hb = new Hitbox(250.0F * Settings.scale, 32.0F * Settings.scale);
/*     */     }
/*     */     
/*  44 */     this.value = value;
/*  45 */     this.color.a = 0.0F;
/*     */   }
/*     */   
/*     */   public void update() {
/*  49 */     if (!this.hidden) {
/*  50 */       this.color.a = com.megacrit.cardcrawl.helpers.MathHelper.slowColorLerpSnap(this.color.a, 1.0F);
/*  51 */       this.offsetX = com.megacrit.cardcrawl.helpers.MathHelper.uiLerpSnap(this.offsetX, 0.0F);
/*  52 */       if (this.hb != null) {
/*  53 */         this.hb.update();
/*  54 */         if (this.hb.hovered) {
/*  55 */           com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(com.megacrit.cardcrawl.helpers.input.InputHelper.mX + 80.0F * Settings.scale, com.megacrit.cardcrawl.helpers.input.InputHelper.mY, this.label, this.description);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderLine(SpriteBatch sb, boolean isWide, float y)
/*     */   {
/*  66 */     if (isWide) {
/*  67 */       sb.setColor(this.color);
/*  68 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH / 2.0F - LINE_W / 2.0F, y - 10.0F * Settings.scale, LINE_W, 4.0F * Settings.scale);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  74 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/*  75 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH / 2.0F - LINE_W / 2.0F, y - 10.0F * Settings.scale, LINE_W, 1.0F * Settings.scale);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*  82 */       sb.setColor(this.color);
/*  83 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH / 2.0F - LINE_W / 5.0F, y - 10.0F * Settings.scale, LINE_W / 2.5F, 4.0F * Settings.scale);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  89 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/*  90 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, Settings.WIDTH / 2.0F - LINE_W / 5.0F, y - 10.0F * Settings.scale, LINE_W / 2.5F, 1.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb, float x, float y)
/*     */   {
/* 100 */     if (!this.line)
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 116 */       Color prevColor = null;
/* 117 */       if ((this.hb != null) && (this.hb.hovered)) {
/* 118 */         prevColor = this.color;
/* 119 */         this.color = new Color(0.0F, 0.9F, 0.0F, this.color.a);
/*     */       }
/*     */       
/*     */ 
/* 123 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont, this.label, x + this.offsetX, y, this.color);
/*     */       
/*     */ 
/* 126 */       FontHelper.renderFontRightTopAligned(sb, FontHelper.topPanelInfoFont, this.value, x + VALUE_X + this.offsetX, y, this.color);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 134 */       if (this.hb != null) {
/* 135 */         this.hb.move(x + 120.0F * Settings.scale, y - 8.0F * Settings.scale);
/* 136 */         this.hb.render(sb);
/*     */         
/* 138 */         if (this.hb.hovered) {
/* 139 */           this.color = prevColor;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\GameOverStat.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */