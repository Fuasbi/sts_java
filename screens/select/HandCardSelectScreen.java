/*     */ package com.megacrit.cardcrawl.screens.select;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CardSelectConfirmButton;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class HandCardSelectScreen
/*     */ {
/*  26 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("HandCardSelectScreen");
/*  27 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public int numCardsToSelect;
/*  30 */   public CardGroup selectedCards = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/*  31 */   public AbstractCard hoveredCard; public AbstractCard upgradePreviewCard = null;
/*     */   
/*     */   public String selectionReason;
/*     */   
/*  35 */   public boolean wereCardsRetrieved = false; public boolean canPickZero = false; public boolean upTo = false;
/*  36 */   private String message = "";
/*  37 */   public CardSelectConfirmButton button = new CardSelectConfirmButton();
/*  38 */   private boolean anyNumber = false; private boolean forTransform = false;
/*  39 */   private boolean forUpgrade; public int numSelected = 0;
/*  40 */   public static final float MIN_HOVER_DIST = 64.0F * Settings.scale;
/*     */   
/*  42 */   private boolean waitThenClose = false;
/*  43 */   private float waitToCloseTimer = 0.0F;
/*     */   
/*     */   private CardGroup hand;
/*     */   
/*  47 */   public static final float HOVER_CARD_Y_POSITION = 210.0F * Settings.scale;
/*     */   
/*     */   private static final int ARROW_W = 64;
/*     */   
/*  51 */   private float arrowScale1 = 0.75F; private float arrowScale2 = 0.75F; private float arrowScale3 = 0.75F; private float arrowTimer = 0.0F;
/*     */   
/*     */   public void update() {
/*  54 */     updateControllerInput();
/*  55 */     updateHand();
/*  56 */     updateSelectedCards();
/*     */     
/*  58 */     if (this.waitThenClose) {
/*  59 */       this.waitToCloseTimer -= Gdx.graphics.getDeltaTime();
/*  60 */       if (this.waitToCloseTimer < 0.0F) {
/*  61 */         this.waitThenClose = false;
/*  62 */         AbstractDungeon.closeCurrentScreen();
/*     */         
/*  64 */         if ((this.forTransform) && (this.selectedCards.size() == 1)) {
/*  65 */           if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/*  66 */             AbstractDungeon.srcTransformCard(this.selectedCards.getBottomCard());
/*     */           } else {
/*  68 */             AbstractDungeon.transformCard(this.selectedCards.getBottomCard());
/*     */           }
/*  70 */           this.selectedCards.group.clear();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  75 */     if ((Settings.FAST_HAND_CONF) && (this.numCardsToSelect == 1) && (this.selectedCards.size() == 1) && (!this.canPickZero) && 
/*  76 */       (!this.waitThenClose)) {
/*  77 */       InputHelper.justClickedLeft = false;
/*  78 */       this.waitToCloseTimer = 0.25F;
/*  79 */       this.waitThenClose = true;
/*  80 */       return;
/*     */     }
/*     */     
/*     */ 
/*  84 */     this.button.update();
/*  85 */     if ((this.button.hb.clicked) || (CInputActionSet.proceed.isJustPressed())) {
/*  86 */       CInputActionSet.proceed.unpress();
/*  87 */       this.button.hb.clicked = false;
/*  88 */       if ((this.canPickZero) && (this.selectedCards.size() == 0)) {
/*  89 */         InputHelper.justClickedLeft = false;
/*  90 */         AbstractDungeon.closeCurrentScreen();
/*  91 */         return;
/*     */       }
/*     */       
/*  94 */       if ((this.anyNumber) || (this.upTo)) {
/*  95 */         InputHelper.justClickedLeft = false;
/*  96 */         AbstractDungeon.closeCurrentScreen();
/*  97 */         return;
/*     */       }
/*     */       
/* 100 */       if (this.selectedCards.size() == this.numCardsToSelect) {
/* 101 */         InputHelper.justClickedLeft = false;
/* 102 */         AbstractDungeon.closeCurrentScreen();
/*     */         
/* 104 */         if ((this.forTransform) && (this.selectedCards.size() == 1)) {
/* 105 */           if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 106 */             AbstractDungeon.srcTransformCard(this.selectedCards.getBottomCard());
/*     */           } else {
/* 108 */             AbstractDungeon.transformCard(this.selectedCards.getBottomCard());
/*     */           }
/* 110 */           this.selectedCards.group.clear();
/*     */         }
/* 112 */         return;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 118 */     if (!Settings.isControllerMode) {
/* 119 */       return;
/*     */     }
/*     */     
/* 122 */     boolean inHand = true;
/* 123 */     boolean anyHovered = false;
/* 124 */     int index = 0;
/* 125 */     int y = 0;
/*     */     
/* 127 */     for (AbstractCard c : this.selectedCards.group) {
/* 128 */       if (c.hb.hovered) {
/* 129 */         anyHovered = true;
/* 130 */         inHand = false;
/* 131 */         break;
/*     */       }
/* 133 */       index++;
/*     */     }
/*     */     
/* 136 */     if (inHand) {
/* 137 */       index = 0;
/* 138 */       for (AbstractCard c : this.hand.group) {
/* 139 */         if (c == this.hoveredCard) {
/* 140 */           anyHovered = true;
/* 141 */           break;
/*     */         }
/* 143 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 147 */     if (!anyHovered) {
/* 148 */       if (!this.hand.group.isEmpty()) {
/* 149 */         setHoveredCard((AbstractCard)this.hand.group.get(0));
/* 150 */         Gdx.input.setCursorPosition((int)this.hoveredCard.hb.cX, Settings.HEIGHT - (int)this.hoveredCard.hb.cY);
/* 151 */       } else if (!this.selectedCards.isEmpty()) {
/* 152 */         Gdx.input.setCursorPosition(
/* 153 */           (int)((AbstractCard)this.selectedCards.group.get(0)).hb.cX, Settings.HEIGHT - 
/* 154 */           (int)((AbstractCard)this.selectedCards.group.get(0)).hb.cY);
/*     */       }
/*     */     }
/* 157 */     else if (!inHand) {
/* 158 */       if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 159 */         (!this.hand.group.isEmpty())) {
/* 160 */         index = 0;
/* 161 */         if (((AbstractCard)this.hand.group.get(index)).hb.cY < 0.0F) {
/* 162 */           y = 1;
/*     */         } else {
/* 164 */           y = (int)((AbstractCard)this.hand.group.get(index)).hb.cY;
/*     */         }
/* 166 */         Gdx.input.setCursorPosition((int)((AbstractCard)this.hand.group.get(index)).hb.cX, Settings.HEIGHT - y);
/* 167 */         setHoveredCard((AbstractCard)this.hand.group.get(index));
/* 168 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 169 */         index++;
/* 170 */         if (index > this.selectedCards.size() - 1) {
/* 171 */           index = 0;
/*     */         }
/* 173 */         Gdx.input.setCursorPosition(
/* 174 */           (int)((AbstractCard)this.selectedCards.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 175 */           (int)((AbstractCard)this.selectedCards.group.get(index)).hb.cY);
/* 176 */         unhoverCard(this.hoveredCard);
/* 177 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 178 */         index--;
/* 179 */         if (index < 0) {
/* 180 */           index = this.selectedCards.size() - 1;
/*     */         }
/* 182 */         Gdx.input.setCursorPosition(
/* 183 */           (int)((AbstractCard)this.selectedCards.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 184 */           (int)((AbstractCard)this.selectedCards.group.get(index)).hb.cY);
/* 185 */         unhoverCard(this.hoveredCard);
/* 186 */       } else if (CInputActionSet.select.isJustPressed()) {
/* 187 */         CInputActionSet.select.unpress();
/* 188 */         if (((AbstractCard)this.selectedCards.group.get(index)).hb.hovered) {
/* 189 */           AbstractCard tmp = (AbstractCard)this.selectedCards.group.get(index);
/* 190 */           AbstractDungeon.player.hand.addToTop(tmp);
/* 191 */           this.selectedCards.group.remove(tmp);
/* 192 */           refreshSelectedCards();
/* 193 */           updateMessage();
/* 194 */           this.hand.refreshHandLayout();
/*     */         }
/*     */         
/*     */       }
/*     */       
/*     */     }
/* 200 */     else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 201 */       (!this.selectedCards.isEmpty())) {
/* 202 */       index = 0;
/* 203 */       if (((AbstractCard)this.selectedCards.group.get(index)).hb.cY < 0.0F) {
/* 204 */         y = 1;
/*     */       } else {
/* 206 */         y = (int)((AbstractCard)this.selectedCards.group.get(index)).hb.cY;
/*     */       }
/*     */       
/* 209 */       unhoverCard(this.hoveredCard);
/* 210 */       Gdx.input.setCursorPosition((int)((AbstractCard)this.selectedCards.group.get(index)).hb.cX, Settings.HEIGHT - y);
/* 211 */     } else if ((this.hand.size() > 1) && ((CInputActionSet.left.isJustPressed()) || 
/* 212 */       (CInputActionSet.altLeft.isJustPressed()))) {
/* 213 */       index--;
/* 214 */       if (index < 0) {
/* 215 */         index = this.hand.size() - 1;
/*     */       }
/*     */       
/* 218 */       unhoverCard(this.hoveredCard);
/* 219 */       setHoveredCard((AbstractCard)this.hand.group.get(index));
/* 220 */       Gdx.input.setCursorPosition((int)this.hoveredCard.hb.cX, Settings.HEIGHT - (int)this.hoveredCard.hb.cY);
/* 221 */     } else if ((this.hand.size() > 1) && ((CInputActionSet.right.isJustPressed()) || 
/* 222 */       (CInputActionSet.altRight.isJustPressed()))) {
/* 223 */       index++;
/* 224 */       if (index > this.hand.size() - 1) {
/* 225 */         index = 0;
/*     */       }
/*     */       
/* 228 */       unhoverCard(this.hoveredCard);
/* 229 */       setHoveredCard((AbstractCard)this.hand.group.get(index));
/* 230 */       Gdx.input.setCursorPosition((int)this.hoveredCard.hb.cX, Settings.HEIGHT - (int)this.hoveredCard.hb.cY);
/* 231 */     } else if ((this.hand.size() == 1) && (this.hoveredCard == null)) {
/* 232 */       setHoveredCard((AbstractCard)this.hand.group.get(index));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void unhoverCard(AbstractCard card)
/*     */   {
/* 239 */     if (card == null) {
/* 240 */       return;
/*     */     }
/*     */     
/* 243 */     card.targetDrawScale = 0.7F;
/* 244 */     card.hoverTimer = 0.25F;
/* 245 */     card.unhover();
/* 246 */     card = null;
/* 247 */     this.hand.refreshHandLayout();
/*     */   }
/*     */   
/*     */   private void setHoveredCard(AbstractCard card) {
/* 251 */     this.hoveredCard = card;
/* 252 */     this.hoveredCard.current_y = HOVER_CARD_Y_POSITION;
/* 253 */     this.hoveredCard.target_y = HOVER_CARD_Y_POSITION;
/* 254 */     this.hoveredCard.drawScale = 1.0F;
/* 255 */     this.hoveredCard.targetDrawScale = 1.0F;
/* 256 */     this.hoveredCard.setAngle(0.0F, true);
/* 257 */     this.hand.hoverCardPush(this.hoveredCard);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void updateHand()
/*     */   {
/* 265 */     if (!Settings.isControllerMode) {
/* 266 */       hoverCheck();
/* 267 */       unhoverCheck();
/*     */     }
/* 269 */     startDraggingCardCheck();
/* 270 */     hotkeyCheck();
/*     */   }
/*     */   
/*     */   private void refreshSelectedCards() {
/* 274 */     for (AbstractCard c : this.selectedCards.group) {
/* 275 */       c.target_y = (Settings.HEIGHT / 2.0F + 160.0F * Settings.scale);
/*     */     }
/*     */     
/* 278 */     switch (this.selectedCards.size()) {
/*     */     case 1: 
/* 280 */       if (this.forUpgrade) {
/* 281 */         ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH * 0.37F);
/*     */       } else {
/* 283 */         ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F);
/*     */       }
/* 285 */       break;
/*     */     case 2: 
/* 287 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 120.0F * Settings.scale);
/* 288 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F + 120.0F * Settings.scale);
/* 289 */       break;
/*     */     case 3: 
/* 291 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 240.0F * Settings.scale);
/* 292 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F);
/* 293 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F + 240.0F * Settings.scale);
/* 294 */       break;
/*     */     case 4: 
/* 296 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 360.0F * Settings.scale);
/* 297 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 120.0F * Settings.scale);
/* 298 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F + 120.0F * Settings.scale);
/* 299 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F + 360.0F * Settings.scale);
/* 300 */       break;
/*     */     case 5: 
/* 302 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 360.0F * Settings.scale);
/* 303 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 180.0F * Settings.scale);
/* 304 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F);
/* 305 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F + 180.0F * Settings.scale);
/* 306 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F + 360.0F * Settings.scale);
/* 307 */       break;
/*     */     case 6: 
/* 309 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 450.0F * Settings.scale);
/* 310 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 270.0F * Settings.scale);
/* 311 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F - 90.0F * Settings.scale);
/* 312 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F + 90.0F * Settings.scale);
/* 313 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F + 270.0F * Settings.scale);
/* 314 */       ((AbstractCard)this.selectedCards.group.get(5)).target_x = (Settings.WIDTH / 2.0F + 450.0F * Settings.scale);
/* 315 */       break;
/*     */     case 7: 
/* 317 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 540.0F * Settings.scale);
/* 318 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 360.0F * Settings.scale);
/* 319 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F - 180.0F * Settings.scale);
/* 320 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F);
/* 321 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F + 180.0F * Settings.scale);
/* 322 */       ((AbstractCard)this.selectedCards.group.get(5)).target_x = (Settings.WIDTH / 2.0F + 360.0F * Settings.scale);
/* 323 */       ((AbstractCard)this.selectedCards.group.get(6)).target_x = (Settings.WIDTH / 2.0F + 540.0F * Settings.scale);
/* 324 */       break;
/*     */     case 8: 
/* 326 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 630.0F * Settings.scale);
/* 327 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 450.0F * Settings.scale);
/* 328 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F - 270.0F * Settings.scale);
/* 329 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F - 90.0F * Settings.scale);
/* 330 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F + 90.0F * Settings.scale);
/* 331 */       ((AbstractCard)this.selectedCards.group.get(5)).target_x = (Settings.WIDTH / 2.0F + 270.0F * Settings.scale);
/* 332 */       ((AbstractCard)this.selectedCards.group.get(6)).target_x = (Settings.WIDTH / 2.0F + 450.0F * Settings.scale);
/* 333 */       ((AbstractCard)this.selectedCards.group.get(7)).target_x = (Settings.WIDTH / 2.0F + 630.0F * Settings.scale);
/* 334 */       break;
/*     */     case 9: 
/* 336 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 720.0F * Settings.scale);
/* 337 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 540.0F * Settings.scale);
/* 338 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F - 360.0F * Settings.scale);
/* 339 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F - 180.0F * Settings.scale);
/* 340 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F);
/* 341 */       ((AbstractCard)this.selectedCards.group.get(5)).target_x = (Settings.WIDTH / 2.0F + 180.0F * Settings.scale);
/* 342 */       ((AbstractCard)this.selectedCards.group.get(6)).target_x = (Settings.WIDTH / 2.0F + 360.0F * Settings.scale);
/* 343 */       ((AbstractCard)this.selectedCards.group.get(7)).target_x = (Settings.WIDTH / 2.0F + 540.0F * Settings.scale);
/* 344 */       ((AbstractCard)this.selectedCards.group.get(8)).target_x = (Settings.WIDTH / 2.0F + 720.0F * Settings.scale);
/* 345 */       break;
/*     */     case 10: 
/* 347 */       ((AbstractCard)this.selectedCards.group.get(0)).target_x = (Settings.WIDTH / 2.0F - 810.0F * Settings.scale);
/* 348 */       ((AbstractCard)this.selectedCards.group.get(1)).target_x = (Settings.WIDTH / 2.0F - 630.0F * Settings.scale);
/* 349 */       ((AbstractCard)this.selectedCards.group.get(2)).target_x = (Settings.WIDTH / 2.0F - 450.0F * Settings.scale);
/* 350 */       ((AbstractCard)this.selectedCards.group.get(3)).target_x = (Settings.WIDTH / 2.0F - 270.0F * Settings.scale);
/* 351 */       ((AbstractCard)this.selectedCards.group.get(4)).target_x = (Settings.WIDTH / 2.0F - 90.0F * Settings.scale);
/* 352 */       ((AbstractCard)this.selectedCards.group.get(5)).target_x = (Settings.WIDTH / 2.0F + 90.0F * Settings.scale);
/* 353 */       ((AbstractCard)this.selectedCards.group.get(6)).target_x = (Settings.WIDTH / 2.0F + 270.0F * Settings.scale);
/* 354 */       ((AbstractCard)this.selectedCards.group.get(7)).target_x = (Settings.WIDTH / 2.0F + 450.0F * Settings.scale);
/* 355 */       ((AbstractCard)this.selectedCards.group.get(8)).target_x = (Settings.WIDTH / 2.0F + 630.0F * Settings.scale);
/* 356 */       ((AbstractCard)this.selectedCards.group.get(9)).target_x = (Settings.WIDTH / 2.0F + 810.0F * Settings.scale);
/* 357 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 362 */     if (this.upTo) {
/* 363 */       this.button.enable();
/* 364 */     } else if (this.selectedCards.size() == this.numCardsToSelect) {
/* 365 */       this.button.enable();
/* 366 */     } else if ((this.selectedCards.size() > 1) && (this.anyNumber) && (!this.canPickZero)) {
/* 367 */       this.button.enable();
/* 368 */     } else if ((this.selectedCards.size() != this.numCardsToSelect) && (!this.anyNumber)) {
/* 369 */       this.button.disable();
/* 370 */     } else if ((this.anyNumber) && (this.canPickZero)) {
/* 371 */       this.button.enable();
/*     */     }
/*     */   }
/*     */   
/*     */   private void hoverCheck() {
/* 376 */     if (this.hoveredCard == null) {
/* 377 */       this.hoveredCard = this.hand.getHoveredCard();
/* 378 */       if (this.hoveredCard != null) {
/* 379 */         this.hoveredCard.current_y = HOVER_CARD_Y_POSITION;
/* 380 */         this.hoveredCard.target_y = HOVER_CARD_Y_POSITION;
/* 381 */         this.hoveredCard.drawScale = 1.0F;
/* 382 */         this.hoveredCard.targetDrawScale = 1.0F;
/* 383 */         this.hoveredCard.setAngle(0.0F, true);
/* 384 */         this.hand.hoverCardPush(this.hoveredCard);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void unhoverCheck() {
/* 390 */     if ((this.hoveredCard != null) && (!this.hoveredCard.isHoveredInHand(1.0F))) {
/* 391 */       this.hoveredCard.targetDrawScale = 0.7F;
/* 392 */       this.hoveredCard.hoverTimer = 0.25F;
/* 393 */       this.hoveredCard.unhover();
/* 394 */       this.hoveredCard = null;
/* 395 */       this.hand.refreshHandLayout();
/*     */     }
/*     */   }
/*     */   
/*     */   private void startDraggingCardCheck() {
/* 400 */     if ((this.hoveredCard != null) && ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed()))) {
/* 401 */       selectHoveredCard();
/*     */     }
/*     */   }
/*     */   
/*     */   private void selectHoveredCard() {
/* 406 */     if (this.numCardsToSelect > this.selectedCards.group.size()) {
/* 407 */       InputHelper.justClickedLeft = false;
/* 408 */       CardCrawlGame.sound.play("CARD_OBTAIN");
/* 409 */       this.hand.removeCard(this.hoveredCard);
/* 410 */       this.hand.refreshHandLayout();
/* 411 */       this.hoveredCard.setAngle(0.0F, false);
/* 412 */       this.selectedCards.addToTop(this.hoveredCard);
/* 413 */       refreshSelectedCards();
/* 414 */       this.hoveredCard = null;
/* 415 */       updateMessage();
/* 416 */     } else if ((this.numCardsToSelect == 1) && (this.selectedCards.group.size() == 1)) {
/* 417 */       InputHelper.justClickedLeft = false;
/* 418 */       CardCrawlGame.sound.play("CARD_OBTAIN");
/* 419 */       this.hand.removeCard(this.hoveredCard);
/* 420 */       this.hoveredCard.setAngle(0.0F, false);
/* 421 */       this.selectedCards.addToBottom(this.hoveredCard);
/* 422 */       refreshSelectedCards();
/* 423 */       this.hoveredCard = null;
/* 424 */       AbstractDungeon.player.hand.addToTop(this.selectedCards.getTopCard());
/* 425 */       this.selectedCards.removeTopCard();
/* 426 */       refreshSelectedCards();
/* 427 */       this.hand.refreshHandLayout();
/* 428 */       if ((this.forUpgrade) && (this.selectedCards.size() == 1)) {
/* 429 */         this.upgradePreviewCard = ((AbstractCard)this.selectedCards.group.get(0)).makeStatEquivalentCopy();
/* 430 */         this.upgradePreviewCard.upgrade();
/* 431 */         this.upgradePreviewCard.displayUpgrades();
/* 432 */         this.upgradePreviewCard.drawScale = 0.75F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void hotkeyCheck() {
/* 438 */     AbstractCard hotkeyCard = InputHelper.getCardSelectedByHotkey(this.hand);
/* 439 */     if (hotkeyCard != null) {
/* 440 */       this.hoveredCard = hotkeyCard;
/* 441 */       System.out.println("HOVERED CARD: 394");
/* 442 */       this.hoveredCard.setAngle(0.0F, false);
/* 443 */       selectHoveredCard();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateSelectedCards()
/*     */   {
/* 451 */     this.selectedCards.update();
/*     */     
/* 453 */     for (Iterator<AbstractCard> i = this.selectedCards.group.iterator(); i.hasNext(); 
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 486 */         goto 293)
/*     */     {
/* 454 */       AbstractCard e = (AbstractCard)i.next();
/* 455 */       e.current_x = com.megacrit.cardcrawl.helpers.MathHelper.cardLerpSnap(e.current_x, e.target_x);
/* 456 */       e.current_y = com.megacrit.cardcrawl.helpers.MathHelper.cardLerpSnap(e.current_y, e.target_y);
/* 457 */       e.hb.update();
/* 458 */       if (this.selectedCards.group.size() >= 5) {
/* 459 */         e.targetDrawScale = 0.5F;
/* 460 */         if ((Math.abs(e.current_x - e.target_x) < MIN_HOVER_DIST) && (e.hb.hovered)) {
/* 461 */           e.targetDrawScale = 0.66F;
/*     */         }
/*     */       } else {
/* 464 */         e.targetDrawScale = 0.66F;
/* 465 */         if (this.forUpgrade) {
/* 466 */           e.targetDrawScale = 0.75F;
/*     */         }
/* 468 */         if ((Math.abs(e.current_x - e.target_x) < MIN_HOVER_DIST) && (e.hb.hovered)) {
/* 469 */           if (this.forUpgrade) {
/* 470 */             e.targetDrawScale = 0.85F;
/*     */           } else {
/* 472 */             e.targetDrawScale = 0.75F;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 477 */       if ((Math.abs(e.current_x - e.target_x) < MIN_HOVER_DIST) && (e.hb.hovered) && ((InputHelper.justClickedLeft) || 
/* 478 */         (CInputActionSet.select.isJustPressed()))) {
/* 479 */         InputHelper.justClickedLeft = false;
/* 480 */         AbstractDungeon.player.hand.addToTop(e);
/* 481 */         i.remove();
/* 482 */         refreshSelectedCards();
/* 483 */         updateMessage();
/*     */         
/* 485 */         if (!Settings.isControllerMode) break;
/* 486 */         this.hand.refreshHandLayout();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 492 */     if ((this.selectedCards.isEmpty()) && (!this.canPickZero)) {
/* 493 */       this.button.disable();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMessage() {
/* 498 */     if (this.selectedCards.group.size() == 0) {
/* 499 */       this.upgradePreviewCard = null;
/* 500 */       if (this.selectedCards.group.size() == this.numCardsToSelect) {
/* 501 */         if (this.numCardsToSelect == 1) {
/* 502 */           this.message = (TEXT[0] + this.selectionReason);
/*     */         } else {
/* 504 */           this.message = (TEXT[1] + this.selectionReason);
/*     */         }
/* 506 */       } else if (this.numCardsToSelect != 1) {
/* 507 */         if (!this.anyNumber) {
/* 508 */           this.message = (TEXT[2] + this.numCardsToSelect + TEXT[3] + this.selectionReason);
/*     */         } else {
/* 510 */           this.message = (TEXT[4] + this.selectionReason);
/*     */         }
/*     */       } else {
/* 513 */         this.message = (TEXT[5] + this.selectionReason);
/*     */       }
/* 515 */     } else if (this.selectedCards.group.size() != 0) {
/* 516 */       int numLeft = this.numCardsToSelect - this.selectedCards.group.size();
/* 517 */       if (this.selectedCards.group.size() == this.numCardsToSelect) {
/* 518 */         if (this.numCardsToSelect == 1) {
/* 519 */           this.message = (TEXT[0] + this.selectionReason);
/*     */         } else {
/* 521 */           this.message = (TEXT[1] + this.selectionReason);
/*     */         }
/* 523 */         if ((this.forUpgrade) && (this.selectedCards.size() == 1)) {
/* 524 */           if (this.upgradePreviewCard == null) {
/* 525 */             this.upgradePreviewCard = ((AbstractCard)this.selectedCards.group.get(0)).makeStatEquivalentCopy();
/*     */           }
/* 527 */           this.upgradePreviewCard.upgrade();
/* 528 */           this.upgradePreviewCard.displayUpgrades();
/* 529 */           this.upgradePreviewCard.drawScale = 0.75F;
/* 530 */           this.upgradePreviewCard.targetDrawScale = 0.75F;
/*     */         } else {
/* 532 */           this.upgradePreviewCard = null;
/*     */         }
/* 534 */       } else if (numLeft != 1) {
/* 535 */         this.upgradePreviewCard = null;
/* 536 */         if (!this.anyNumber) {
/* 537 */           this.message = (TEXT[2] + numLeft + TEXT[3] + this.selectionReason);
/*     */         } else {
/* 539 */           this.message = (TEXT[4] + this.selectionReason);
/*     */         }
/*     */       } else {
/* 542 */         this.upgradePreviewCard = null;
/* 543 */         this.message = (TEXT[5] + this.selectionReason);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen() {
/* 549 */     AbstractDungeon.overlayMenu.showBlackScreen(0.5F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void open(String msg, int amount, boolean anyNumber, boolean canPickZero, boolean forTransform, boolean forUpgrade, boolean upTo)
/*     */   {
/* 560 */     prep();
/* 561 */     this.numCardsToSelect = amount;
/* 562 */     this.canPickZero = canPickZero;
/* 563 */     this.anyNumber = anyNumber;
/* 564 */     this.selectionReason = msg;
/* 565 */     this.upTo = upTo;
/*     */     
/* 567 */     if (canPickZero) {
/* 568 */       this.button.isDisabled = true;
/* 569 */       this.button.enable();
/*     */     } else {
/* 571 */       this.button.isDisabled = false;
/* 572 */       this.button.disable();
/*     */     }
/*     */     
/* 575 */     this.forTransform = forTransform;
/* 576 */     this.forUpgrade = forUpgrade;
/*     */     
/* 578 */     if (!forUpgrade) {
/* 579 */       this.upgradePreviewCard = null;
/*     */     }
/*     */     
/* 582 */     this.button.hideInstantly();
/* 583 */     this.button.show();
/*     */     
/* 585 */     updateMessage();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void open(String msg, int amount, boolean anyNumber, boolean canPickZero, boolean forTransform, boolean forUpgrade)
/*     */   {
/* 595 */     open(msg, amount, anyNumber, canPickZero, forTransform, forUpgrade, false);
/*     */   }
/*     */   
/*     */   public void open(String msg, int amount, boolean anyNumber, boolean canPickZero, boolean forTransform) {
/* 599 */     open(msg, amount, anyNumber, canPickZero, forTransform, false);
/*     */   }
/*     */   
/*     */   public void open(String msg, int amount, boolean anyNumber, boolean canPickZero) {
/* 603 */     prep();
/*     */     
/* 605 */     this.numCardsToSelect = amount;
/* 606 */     this.canPickZero = canPickZero;
/* 607 */     this.anyNumber = anyNumber;
/* 608 */     this.selectionReason = msg;
/*     */     
/* 610 */     if (canPickZero) {
/* 611 */       this.button.isDisabled = true;
/* 612 */       this.button.enable();
/*     */     } else {
/* 614 */       this.button.isDisabled = false;
/* 615 */       this.button.disable();
/*     */     }
/* 617 */     this.button.hideInstantly();
/* 618 */     this.button.show();
/*     */     
/* 620 */     updateMessage();
/*     */   }
/*     */   
/*     */   public void open(String msg, int amount, boolean anyNumber) {
/* 624 */     open(msg, amount, anyNumber, false);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 629 */     com.megacrit.cardcrawl.helpers.FontHelper.renderFontCentered(sb, com.megacrit.cardcrawl.helpers.FontHelper.buttonLabelFont, this.message, Settings.WIDTH / 2, Settings.HEIGHT - 180.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 638 */     if ((!Settings.FAST_HAND_CONF) || (this.numCardsToSelect != 1) || (this.canPickZero))
/*     */     {
/*     */ 
/* 641 */       this.button.render(sb);
/*     */     }
/*     */     
/*     */ 
/* 645 */     this.selectedCards.render(sb);
/* 646 */     AbstractDungeon.player.hand.render(sb);
/* 647 */     AbstractDungeon.overlayMenu.energyPanel.render(sb);
/*     */     
/*     */ 
/* 650 */     if ((this.forUpgrade) && (this.upgradePreviewCard != null)) {
/* 651 */       renderArrows(sb);
/* 652 */       this.upgradePreviewCard.current_x = (Settings.WIDTH * 0.63F);
/* 653 */       this.upgradePreviewCard.current_y = (Settings.HEIGHT / 2.0F + 160.0F * Settings.scale);
/* 654 */       this.upgradePreviewCard.target_x = (Settings.WIDTH * 0.63F);
/* 655 */       this.upgradePreviewCard.target_y = (Settings.HEIGHT / 2.0F + 160.0F * Settings.scale);
/*     */       
/* 657 */       this.upgradePreviewCard.displayUpgrades();
/* 658 */       boolean t1 = this.upgradePreviewCard.isDamageModified;
/* 659 */       boolean t2 = this.upgradePreviewCard.isBlockModified;
/* 660 */       boolean t3 = this.upgradePreviewCard.isMagicNumberModified;
/* 661 */       boolean t4 = this.upgradePreviewCard.isCostModified;
/* 662 */       this.upgradePreviewCard.applyPowers();
/*     */       
/* 664 */       if ((!this.upgradePreviewCard.isDamageModified) && (t1)) {
/* 665 */         this.upgradePreviewCard.isDamageModified = true;
/*     */       }
/* 667 */       if ((!this.upgradePreviewCard.isBlockModified) && (t2)) {
/* 668 */         this.upgradePreviewCard.isBlockModified = true;
/*     */       }
/* 670 */       if ((!this.upgradePreviewCard.isMagicNumberModified) && (t3)) {
/* 671 */         this.upgradePreviewCard.isMagicNumberModified = true;
/*     */       }
/* 673 */       if ((!this.upgradePreviewCard.isCostModified) && (t4)) {
/* 674 */         this.upgradePreviewCard.isCostModified = true;
/*     */       }
/*     */       
/* 677 */       this.upgradePreviewCard.render(sb);
/* 678 */       this.upgradePreviewCard.updateHoverLogic();
/* 679 */       this.upgradePreviewCard.renderCardTip(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderArrows(SpriteBatch sb)
/*     */   {
/* 685 */     float x = Settings.WIDTH / 2.0F - 96.0F * Settings.scale - 10.0F * Settings.scale;
/* 686 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 687 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F + 120.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale1 * Settings.scale, this.arrowScale1 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 705 */     x += 64.0F * Settings.scale;
/* 706 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 707 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F + 120.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale2 * Settings.scale, this.arrowScale2 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 725 */     x += 64.0F * Settings.scale;
/*     */     
/* 727 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F + 120.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale3 * Settings.scale, this.arrowScale3 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 745 */     this.arrowTimer += Gdx.graphics.getDeltaTime() * 2.0F;
/* 746 */     this.arrowScale1 = (0.8F + (MathUtils.cos(this.arrowTimer) + 1.0F) / 8.0F);
/* 747 */     this.arrowScale2 = (0.8F + (MathUtils.cos(this.arrowTimer - 0.8F) + 1.0F) / 8.0F);
/* 748 */     this.arrowScale3 = (0.8F + (MathUtils.cos(this.arrowTimer - 1.6F) + 1.0F) / 8.0F);
/*     */   }
/*     */   
/*     */   private void prep() {
/* 752 */     this.upTo = false;
/* 753 */     this.forTransform = false;
/* 754 */     this.forUpgrade = false;
/* 755 */     this.canPickZero = false;
/* 756 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 757 */     AbstractDungeon.actionManager.cleanCardQueue();
/* 758 */     this.hand = AbstractDungeon.player.hand;
/* 759 */     AbstractDungeon.player.releaseCard();
/* 760 */     AbstractDungeon.getMonsters().hoveredMonster = null;
/* 761 */     this.waitThenClose = false;
/* 762 */     this.waitToCloseTimer = 0.0F;
/* 763 */     this.selectedCards.clear();
/* 764 */     this.hoveredCard = null;
/* 765 */     this.wereCardsRetrieved = false;
/* 766 */     AbstractDungeon.isScreenUp = true;
/* 767 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.HAND_SELECT;
/* 768 */     AbstractDungeon.player.hand.stopGlowing();
/* 769 */     AbstractDungeon.player.hand.refreshHandLayout();
/* 770 */     AbstractDungeon.overlayMenu.showBlackScreen(0.5F);
/* 771 */     this.numSelected = 0;
/*     */     
/* 773 */     if (Settings.isControllerMode) {
/* 774 */       Gdx.input.setCursorPosition((int)((AbstractCard)this.hand.group.get(0)).hb.cX, (int)((AbstractCard)this.hand.group.get(0)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   public void returnAllCards() {}
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\select\HandCardSelectScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */