/*     */ package com.megacrit.cardcrawl.screens.select;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.GridSelectConfirmButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GridCardSelectScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  32 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("GridCardSelectScreen");
/*  33 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   private static float drawStartX;
/*     */   private static float drawStartY;
/*     */   private static float padX;
/*  37 */   private static float padY; private static final int CARDS_PER_LINE = 5; private static final float SCROLL_BAR_THRESHOLD = 500.0F * Settings.scale;
/*  38 */   private float grabStartY = 0.0F; private float currentDiffY = 0.0F;
/*  39 */   public ArrayList<AbstractCard> selectedCards = new ArrayList();
/*     */   private CardGroup targetGroup;
/*  41 */   private AbstractCard hoveredCard = null;
/*  42 */   public AbstractCard upgradePreviewCard = null;
/*  43 */   private int numCards = 0; private int cardSelectAmount = 0;
/*  44 */   private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  45 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  46 */   private boolean grabbedScreen = false; private boolean canCancel = true;
/*  47 */   public boolean forUpgrade = false; public boolean forTransform = false; public boolean forPurge = false; public boolean confirmScreenUp = false; public boolean isJustForConfirming = false;
/*     */   
/*  49 */   public GridSelectConfirmButton confirmButton = new GridSelectConfirmButton(TEXT[0]);
/*  50 */   private String tipMsg = "";
/*  51 */   private String lastTip = "";
/*  52 */   private float ritualAnimTimer = 0.0F;
/*     */   private static final float RITUAL_ANIM_INTERVAL = 0.1F;
/*  54 */   private int prevDeckSize = 0;
/*  55 */   public boolean cancelWasOn = false; public boolean anyNumber = false;
/*     */   public String cancelText;
/*     */   private ScrollBar scrollBar;
/*  58 */   private AbstractCard controllerCard = null;
/*     */   
/*     */   private static final int ARROW_W = 64;
/*     */   
/*  62 */   private float arrowScale1 = 1.0F; private float arrowScale2 = 1.0F; private float arrowScale3 = 1.0F; private float arrowTimer = 0.0F;
/*     */   
/*     */   public GridCardSelectScreen() {
/*  65 */     drawStartX = Settings.WIDTH;
/*  66 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  67 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  68 */     drawStartX /= 2.0F;
/*  69 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*     */     
/*  71 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  72 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*  73 */     this.scrollBar = new ScrollBar(this);
/*  74 */     this.scrollBar.move(0.0F, -30.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  78 */     updateControllerInput();
/*  79 */     if ((Settings.isControllerMode) && (this.controllerCard != null) && (!CardCrawlGame.isPopupOpen) && (this.upgradePreviewCard == null))
/*     */     {
/*  81 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.75F) {
/*  82 */         this.currentDiffY += Settings.SCROLL_SPEED;
/*  83 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.25F) {
/*  84 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/*  88 */     boolean isDraggingScrollBar = false;
/*  89 */     if (shouldShowScrollBar()) {
/*  90 */       isDraggingScrollBar = this.scrollBar.update();
/*     */     }
/*  92 */     if (!isDraggingScrollBar) {
/*  93 */       updateScrolling();
/*     */     }
/*  95 */     this.confirmButton.update();
/*     */     
/*     */ 
/*  98 */     if (this.isJustForConfirming) {
/*  99 */       updateCardPositionsAndHoverLogic();
/* 100 */       if ((this.confirmButton.hb.clicked) || (CInputActionSet.topPanel.isJustPressed())) {
/* 101 */         CInputActionSet.select.unpress();
/* 102 */         this.confirmButton.hb.clicked = false;
/* 103 */         AbstractDungeon.overlayMenu.cancelButton.hide();
/* 104 */         AbstractDungeon.dynamicBanner.hide();
/* 105 */         this.confirmScreenUp = false;
/* 106 */         for (AbstractCard c : this.targetGroup.group) {
/* 107 */           AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.FastCardObtainEffect(c, c.current_x, c.current_y));
/*     */         }
/* 109 */         AbstractDungeon.closeCurrentScreen();
/*     */       }
/* 111 */       return; }
/* 112 */     if ((this.anyNumber) && 
/* 113 */       (this.confirmButton.hb.clicked)) {
/* 114 */       this.confirmButton.hb.clicked = false;
/* 115 */       AbstractDungeon.closeCurrentScreen();
/* 116 */       return;
/*     */     }
/*     */     
/*     */ 
/* 120 */     if (!this.confirmScreenUp) {
/* 121 */       updateCardPositionsAndHoverLogic();
/*     */       
/* 123 */       if ((this.hoveredCard != null) && (InputHelper.justClickedLeft)) {
/* 124 */         this.hoveredCard.hb.clickStarted = true;
/*     */       }
/*     */       
/* 127 */       if ((this.hoveredCard != null) && ((this.hoveredCard.hb.clicked) || (CInputActionSet.select.isJustPressed()))) {
/* 128 */         this.hoveredCard.hb.clicked = false;
/* 129 */         if (!this.selectedCards.contains(this.hoveredCard)) {
/* 130 */           this.selectedCards.add(this.hoveredCard);
/* 131 */           this.hoveredCard.beginGlowing();
/* 132 */           this.hoveredCard.targetDrawScale = 0.75F;
/* 133 */           this.hoveredCard.drawScale = 0.875F;
/*     */           
/* 135 */           this.cardSelectAmount += 1;
/* 136 */           CardCrawlGame.sound.play("CARD_SELECT");
/*     */           
/* 138 */           if (this.numCards == this.cardSelectAmount) {
/* 139 */             if (this.forUpgrade)
/*     */             {
/* 141 */               this.hoveredCard.untip();
/* 142 */               this.confirmScreenUp = true;
/* 143 */               this.upgradePreviewCard = this.hoveredCard.makeStatEquivalentCopy();
/* 144 */               this.upgradePreviewCard.upgrade();
/* 145 */               this.upgradePreviewCard.displayUpgrades();
/* 146 */               this.upgradePreviewCard.drawScale = 0.875F;
/* 147 */               this.hoveredCard.stopGlowing();
/* 148 */               this.selectedCards.clear();
/* 149 */               AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/* 150 */               this.confirmButton.show();
/* 151 */               this.confirmButton.isDisabled = false;
/* 152 */               this.lastTip = this.tipMsg;
/* 153 */               this.tipMsg = TEXT[2];
/* 154 */               return; }
/* 155 */             if (this.forTransform)
/*     */             {
/* 157 */               this.hoveredCard.untip();
/* 158 */               this.confirmScreenUp = true;
/* 159 */               this.upgradePreviewCard = this.hoveredCard.makeStatEquivalentCopy();
/* 160 */               this.upgradePreviewCard.drawScale = 0.875F;
/* 161 */               this.hoveredCard.stopGlowing();
/* 162 */               this.selectedCards.clear();
/* 163 */               AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/* 164 */               this.confirmButton.show();
/* 165 */               this.confirmButton.isDisabled = false;
/* 166 */               this.lastTip = this.tipMsg;
/* 167 */               this.tipMsg = TEXT[2];
/* 168 */               return; }
/* 169 */             if (this.forPurge) {
/* 170 */               if (this.numCards == 1) {
/* 171 */                 this.hoveredCard.untip();
/* 172 */                 this.hoveredCard.stopGlowing();
/* 173 */                 this.confirmScreenUp = true;
/* 174 */                 this.hoveredCard.current_x = (Settings.WIDTH / 2.0F);
/* 175 */                 this.hoveredCard.target_x = (Settings.WIDTH / 2.0F);
/* 176 */                 this.hoveredCard.current_y = (Settings.HEIGHT / 2.0F);
/* 177 */                 this.hoveredCard.target_y = (Settings.HEIGHT / 2.0F);
/* 178 */                 this.hoveredCard.update();
/* 179 */                 this.hoveredCard.targetDrawScale = 1.0F;
/* 180 */                 this.hoveredCard.drawScale = 1.0F;
/* 181 */                 this.selectedCards.clear();
/* 182 */                 this.confirmButton.show();
/* 183 */                 this.confirmButton.isDisabled = false;
/* 184 */                 this.lastTip = this.tipMsg;
/* 185 */                 this.tipMsg = TEXT[2];
/* 186 */                 AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */               } else {
/* 188 */                 AbstractDungeon.closeCurrentScreen();
/*     */               }
/* 190 */               for (AbstractCard c : this.selectedCards) {
/* 191 */                 c.stopGlowing();
/*     */               }
/* 193 */               return;
/*     */             }
/*     */             
/* 196 */             AbstractDungeon.closeCurrentScreen();
/* 197 */             if (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.SHOP) {
/* 198 */               AbstractDungeon.overlayMenu.cancelButton.hide();
/*     */             } else {
/* 200 */               AbstractDungeon.overlayMenu.cancelButton.show(TEXT[3]);
/*     */             }
/*     */             
/* 203 */             for (AbstractCard c : this.selectedCards) {
/* 204 */               c.stopGlowing();
/*     */             }
/*     */             
/* 207 */             if (this.targetGroup.type == com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.DISCARD_PILE) {
/* 208 */               for (AbstractCard c : this.targetGroup.group) {
/* 209 */                 c.drawScale = 0.12F;
/* 210 */                 c.targetDrawScale = 0.12F;
/* 211 */                 c.teleportToDiscardPile();
/* 212 */                 c.lighten(true);
/*     */               }
/*     */               
/*     */             }
/*     */           }
/*     */         }
/* 218 */         else if (this.selectedCards.contains(this.hoveredCard))
/*     */         {
/* 220 */           this.hoveredCard.stopGlowing();
/* 221 */           this.selectedCards.remove(this.hoveredCard);
/* 222 */           this.cardSelectAmount -= 1;
/*     */         }
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 228 */       if (this.forTransform) {
/* 229 */         this.ritualAnimTimer -= Gdx.graphics.getDeltaTime();
/* 230 */         if (this.ritualAnimTimer < 0.0F)
/*     */         {
/*     */ 
/* 233 */           this.upgradePreviewCard = AbstractDungeon.returnTrulyRandomCardFromAvailable(this.upgradePreviewCard).makeCopy();
/* 234 */           this.ritualAnimTimer = 0.1F;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 240 */       if (this.forUpgrade) {
/* 241 */         this.upgradePreviewCard.update();
/*     */       }
/* 243 */       if (!this.forPurge) {
/* 244 */         this.upgradePreviewCard.drawScale = 1.0F;
/* 245 */         this.hoveredCard.update();
/* 246 */         this.hoveredCard.drawScale = 1.0F;
/*     */       }
/*     */       
/* 249 */       if ((this.confirmButton.hb.clicked) || (CInputActionSet.topPanel.isJustPressed())) {
/* 250 */         CInputActionSet.select.unpress();
/* 251 */         this.confirmButton.hb.clicked = false;
/* 252 */         AbstractDungeon.overlayMenu.cancelButton.hide();
/* 253 */         this.confirmScreenUp = false;
/* 254 */         this.selectedCards.add(this.hoveredCard);
/* 255 */         AbstractDungeon.closeCurrentScreen();
/*     */       }
/*     */     }
/*     */     
/* 259 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/* 260 */       Gdx.input.setCursorPosition((int)this.controllerCard.hb.cX, (int)(Settings.HEIGHT - this.controllerCard.hb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 265 */     if ((!Settings.isControllerMode) || (this.upgradePreviewCard != null)) {
/* 266 */       return;
/*     */     }
/*     */     
/* 269 */     boolean anyHovered = false;
/* 270 */     int index = 0;
/*     */     
/* 272 */     for (AbstractCard c : this.targetGroup.group) {
/* 273 */       if (c.hb.hovered) {
/* 274 */         anyHovered = true;
/* 275 */         break;
/*     */       }
/* 277 */       index++;
/*     */     }
/*     */     
/* 280 */     if (!anyHovered) {
/* 281 */       Gdx.input.setCursorPosition((int)((AbstractCard)this.targetGroup.group.get(0)).hb.cX, (int)((AbstractCard)this.targetGroup.group.get(0)).hb.cY);
/* 282 */       this.controllerCard = ((AbstractCard)this.targetGroup.group.get(0));
/*     */     }
/* 284 */     else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 285 */       (this.targetGroup.size() > 5))
/*     */     {
/* 287 */       if (index < 5) {
/* 288 */         index = this.targetGroup.size() + 2 - (4 - index);
/* 289 */         if (index > this.targetGroup.size() - 1) {
/* 290 */           index -= 5;
/*     */         }
/* 292 */         if ((index > this.targetGroup.size() - 1) || (index < 0)) {
/* 293 */           index = 0;
/*     */         }
/*     */       }
/*     */       else {
/* 297 */         index -= 5;
/*     */       }
/* 299 */       Gdx.input.setCursorPosition(
/* 300 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 301 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cY);
/* 302 */       this.controllerCard = ((AbstractCard)this.targetGroup.group.get(index));
/* 303 */     } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 304 */       (this.targetGroup.size() > 5)) {
/* 305 */       if (index < this.targetGroup.size() - 5) {
/* 306 */         index += 5;
/*     */       } else {
/* 308 */         index %= 5;
/*     */       }
/* 310 */       Gdx.input.setCursorPosition(
/* 311 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 312 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cY);
/* 313 */       this.controllerCard = ((AbstractCard)this.targetGroup.group.get(index));
/* 314 */     } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 315 */       if (index % 5 > 0) {
/* 316 */         index--;
/*     */       } else {
/* 318 */         index += 4;
/* 319 */         if (index > this.targetGroup.size() - 1) {
/* 320 */           index = this.targetGroup.size() - 1;
/*     */         }
/*     */       }
/* 323 */       Gdx.input.setCursorPosition(
/* 324 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 325 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cY);
/* 326 */       this.controllerCard = ((AbstractCard)this.targetGroup.group.get(index));
/* 327 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 328 */       if (index % 5 < 4) {
/* 329 */         index++;
/* 330 */         if (index > this.targetGroup.size() - 1) {
/* 331 */           index -= this.targetGroup.size() % 5;
/*     */         }
/*     */       } else {
/* 334 */         index -= 4;
/* 335 */         if (index < 0) {
/* 336 */           index = 0;
/*     */         }
/*     */       }
/* 339 */       Gdx.input.setCursorPosition(
/* 340 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 341 */         (int)((AbstractCard)this.targetGroup.group.get(index)).hb.cY);
/* 342 */       this.controllerCard = ((AbstractCard)this.targetGroup.group.get(index));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateCardPositionsAndHoverLogic()
/*     */   {
/* 349 */     if ((this.isJustForConfirming) && (this.targetGroup.size() <= 4)) {
/* 350 */       switch (this.targetGroup.size()) {
/*     */       case 1: 
/* 352 */         this.targetGroup.getBottomCard().current_x = (Settings.WIDTH / 2.0F);
/* 353 */         this.targetGroup.getBottomCard().target_x = (Settings.WIDTH / 2.0F);
/* 354 */         break;
/*     */       case 2: 
/* 356 */         ((AbstractCard)this.targetGroup.group.get(0)).current_x = (Settings.WIDTH / 2.0F - padX / 2.0F);
/* 357 */         ((AbstractCard)this.targetGroup.group.get(0)).target_x = (Settings.WIDTH / 2.0F - padX / 2.0F);
/* 358 */         ((AbstractCard)this.targetGroup.group.get(1)).current_x = (Settings.WIDTH / 2.0F + padX / 2.0F);
/* 359 */         ((AbstractCard)this.targetGroup.group.get(1)).target_x = (Settings.WIDTH / 2.0F + padX / 2.0F);
/* 360 */         break;
/*     */       case 3: 
/* 362 */         ((AbstractCard)this.targetGroup.group.get(0)).current_x = (drawStartX + padX);
/* 363 */         ((AbstractCard)this.targetGroup.group.get(1)).current_x = (drawStartX + padX * 2.0F);
/* 364 */         ((AbstractCard)this.targetGroup.group.get(2)).current_x = (drawStartX + padX * 3.0F);
/* 365 */         ((AbstractCard)this.targetGroup.group.get(0)).target_x = (drawStartX + padX);
/* 366 */         ((AbstractCard)this.targetGroup.group.get(1)).target_x = (drawStartX + padX * 2.0F);
/* 367 */         ((AbstractCard)this.targetGroup.group.get(2)).target_x = (drawStartX + padX * 3.0F);
/* 368 */         break;
/*     */       case 4: 
/* 370 */         ((AbstractCard)this.targetGroup.group.get(0)).current_x = (Settings.WIDTH / 2.0F - padX / 2.0F - padX);
/* 371 */         ((AbstractCard)this.targetGroup.group.get(0)).target_x = (Settings.WIDTH / 2.0F - padX / 2.0F - padX);
/* 372 */         ((AbstractCard)this.targetGroup.group.get(1)).current_x = (Settings.WIDTH / 2.0F - padX / 2.0F);
/* 373 */         ((AbstractCard)this.targetGroup.group.get(1)).target_x = (Settings.WIDTH / 2.0F - padX / 2.0F);
/* 374 */         ((AbstractCard)this.targetGroup.group.get(2)).current_x = (Settings.WIDTH / 2.0F + padX / 2.0F);
/* 375 */         ((AbstractCard)this.targetGroup.group.get(2)).target_x = (Settings.WIDTH / 2.0F + padX / 2.0F);
/* 376 */         ((AbstractCard)this.targetGroup.group.get(3)).current_x = (Settings.WIDTH / 2.0F + padX / 2.0F + padX);
/* 377 */         ((AbstractCard)this.targetGroup.group.get(3)).target_x = (Settings.WIDTH / 2.0F + padX / 2.0F + padX);
/*     */       }
/*     */       
/*     */       
/* 381 */       ArrayList<AbstractCard> c2 = this.targetGroup.group;
/*     */       
/* 383 */       for (int i = 0; i < c2.size(); i++) {
/* 384 */         ((AbstractCard)c2.get(i)).target_y = (drawStartY + this.currentDiffY);
/* 385 */         ((AbstractCard)c2.get(i)).fadingOut = false;
/* 386 */         ((AbstractCard)c2.get(i)).update();
/* 387 */         ((AbstractCard)c2.get(i)).updateHoverLogic();
/*     */         
/* 389 */         this.hoveredCard = null;
/* 390 */         for (AbstractCard c : c2) {
/* 391 */           if (c.hb.hovered) {
/* 392 */             this.hoveredCard = c;
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 397 */       return;
/*     */     }
/*     */     
/*     */ 
/* 401 */     int lineNum = 0;
/* 402 */     ArrayList<AbstractCard> cards = this.targetGroup.group;
/* 403 */     for (int i = 0; i < cards.size(); i++) {
/* 404 */       int mod = i % 5;
/* 405 */       if ((mod == 0) && (i != 0)) {
/* 406 */         lineNum++;
/*     */       }
/* 408 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 409 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 410 */       ((AbstractCard)cards.get(i)).fadingOut = false;
/* 411 */       ((AbstractCard)cards.get(i)).update();
/* 412 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/*     */       
/* 414 */       this.hoveredCard = null;
/* 415 */       for (AbstractCard c : cards) {
/* 416 */         if (c.hb.hovered) {
/* 417 */           this.hoveredCard = c;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void open(CardGroup group, int numCards, boolean anyNumber, String msg)
/*     */   {
/* 432 */     open(group, numCards, msg, false, false, true, false);
/* 433 */     this.anyNumber = true;
/* 434 */     this.confirmButton.show();
/* 435 */     this.confirmButton.updateText(TEXT[0]);
/* 436 */     this.confirmButton.isDisabled = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void open(CardGroup group, int numCards, String tipMsg, boolean forUpgrade, boolean forTransform, boolean canCancel, boolean forPurge)
/*     */   {
/* 448 */     this.targetGroup = group;
/* 449 */     callOnOpen();
/*     */     
/*     */ 
/* 452 */     this.forUpgrade = forUpgrade;
/* 453 */     this.forTransform = forTransform;
/* 454 */     this.canCancel = canCancel;
/* 455 */     this.forPurge = forPurge;
/* 456 */     this.tipMsg = tipMsg;
/* 457 */     this.numCards = numCards;
/*     */     
/* 459 */     if (((forUpgrade) || (forTransform) || (forPurge) || (AbstractDungeon.previousScreen == AbstractDungeon.CurrentScreen.SHOP)) && (canCancel))
/*     */     {
/* 461 */       AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */     }
/*     */     
/* 464 */     if (!canCancel) {
/* 465 */       AbstractDungeon.overlayMenu.cancelButton.hide();
/*     */     }
/*     */     
/* 468 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */   public void open(CardGroup group, int numCards, String tipMsg, boolean forUpgrade, boolean forRitual) {
/* 472 */     open(group, numCards, tipMsg, forUpgrade, forRitual, true, false);
/*     */   }
/*     */   
/*     */   public void open(CardGroup group, int numCards, String tipMsg, boolean forUpgrade) {
/* 476 */     open(group, numCards, tipMsg, forUpgrade, false);
/*     */   }
/*     */   
/*     */   public void openConfirmationGrid(CardGroup group, String tipMsg) {
/* 480 */     this.targetGroup = group;
/* 481 */     callOnOpen();
/*     */     
/*     */ 
/* 484 */     this.isJustForConfirming = true;
/* 485 */     this.tipMsg = tipMsg;
/*     */     
/*     */ 
/* 488 */     AbstractDungeon.overlayMenu.cancelButton.hideInstantly();
/*     */     
/*     */ 
/* 491 */     this.confirmButton.show();
/* 492 */     this.confirmButton.updateText(TEXT[0]);
/* 493 */     this.confirmButton.isDisabled = false;
/*     */     
/* 495 */     this.canCancel = false;
/*     */     
/* 497 */     if (group.size() <= 5)
/*     */     {
/*     */ 
/* 500 */       AbstractDungeon.dynamicBanner.appear(tipMsg);
/*     */     }
/*     */   }
/*     */   
/*     */   private void callOnOpen() {
/* 505 */     if (Settings.isControllerMode) {
/* 506 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 507 */       this.controllerCard = null;
/*     */     }
/*     */     
/* 510 */     this.anyNumber = false;
/* 511 */     this.canCancel = false;
/* 512 */     this.forUpgrade = false;
/* 513 */     this.forTransform = false;
/* 514 */     this.forPurge = false;
/* 515 */     this.confirmScreenUp = false;
/* 516 */     this.isJustForConfirming = false;
/* 517 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 518 */     this.controllerCard = null;
/* 519 */     this.hoveredCard = null;
/* 520 */     this.selectedCards.clear();
/* 521 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 522 */     this.cardSelectAmount = 0;
/* 523 */     this.currentDiffY = 0.0F;
/* 524 */     this.grabStartY = 0.0F;
/* 525 */     this.grabbedScreen = false;
/* 526 */     hideCards();
/* 527 */     AbstractDungeon.isScreenUp = true;
/* 528 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.GRID;
/* 529 */     AbstractDungeon.overlayMenu.showBlackScreen(0.5F);
/* 530 */     this.confirmButton.hideInstantly();
/* 531 */     if (this.targetGroup.group.size() <= 5) {
/* 532 */       drawStartY = Settings.HEIGHT * 0.5F;
/*     */     } else {
/* 534 */       drawStartY = Settings.HEIGHT * 0.66F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen() {
/* 539 */     AbstractDungeon.overlayMenu.showBlackScreen(0.5F);
/* 540 */     AbstractDungeon.isScreenUp = true;
/* 541 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.GRID;
/* 542 */     AbstractDungeon.topPanel.unhoverHitboxes();
/* 543 */     if ((this.cancelWasOn) && (!this.isJustForConfirming)) {
/* 544 */       AbstractDungeon.overlayMenu.cancelButton.show(this.cancelText);
/*     */     }
/* 546 */     for (AbstractCard c : this.targetGroup.group) {
/* 547 */       c.targetDrawScale = 0.75F;
/* 548 */       c.drawScale = 0.75F;
/* 549 */       c.lighten(false);
/*     */     }
/* 551 */     this.scrollBar.reset();
/*     */   }
/*     */   
/*     */   public void hide() {
/* 555 */     if (!AbstractDungeon.overlayMenu.cancelButton.isHidden) {
/* 556 */       this.cancelWasOn = true;
/* 557 */       this.cancelText = AbstractDungeon.overlayMenu.cancelButton.buttonText;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling()
/*     */   {
/* 563 */     if ((this.isJustForConfirming) && (this.targetGroup.size() <= 5)) {
/* 564 */       this.currentDiffY = (-64.0F * Settings.scale);
/* 565 */       return;
/*     */     }
/*     */     
/* 568 */     int y = InputHelper.mY;
/* 569 */     boolean isDraggingScrollBar = this.scrollBar.update();
/*     */     
/* 571 */     if (!isDraggingScrollBar) {
/* 572 */       if (!this.grabbedScreen) {
/* 573 */         if (InputHelper.scrolledDown) {
/* 574 */           this.currentDiffY += Settings.SCROLL_SPEED;
/* 575 */         } else if (InputHelper.scrolledUp) {
/* 576 */           this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */         }
/*     */         
/* 579 */         if (InputHelper.justClickedLeft) {
/* 580 */           this.grabbedScreen = true;
/* 581 */           this.grabStartY = (y - this.currentDiffY);
/*     */         }
/*     */       }
/* 584 */       else if (InputHelper.isMouseDown) {
/* 585 */         this.currentDiffY = (y - this.grabStartY);
/*     */       } else {
/* 587 */         this.grabbedScreen = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 592 */     if (this.prevDeckSize != this.targetGroup.size()) {
/* 593 */       calculateScrollBounds();
/*     */     }
/* 595 */     resetScrolling();
/* 596 */     updateBarPosition();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/* 603 */     int scrollTmp = 0;
/* 604 */     if (this.targetGroup.size() > 10) {
/* 605 */       scrollTmp = this.targetGroup.size() / 5 - 2;
/* 606 */       if (this.targetGroup.size() % 5 != 0) {
/* 607 */         scrollTmp++;
/*     */       }
/* 609 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/* 611 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/*     */     
/* 614 */     this.prevDeckSize = this.targetGroup.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 621 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 622 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 623 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 624 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   private void hideCards() {
/* 629 */     int lineNum = 0;
/* 630 */     ArrayList<AbstractCard> cards = this.targetGroup.group;
/* 631 */     for (int i = 0; i < cards.size(); i++) {
/* 632 */       ((AbstractCard)cards.get(i)).setAngle(0.0F, true);
/* 633 */       int mod = i % 5;
/* 634 */       if ((mod == 0) && (i != 0)) {
/* 635 */         lineNum++;
/*     */       }
/*     */       
/* 638 */       ((AbstractCard)cards.get(i)).lighten(true);
/* 639 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 640 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY - MathUtils.random(100.0F * Settings.scale, 200.0F * Settings.scale));
/*     */       
/*     */ 
/* 643 */       ((AbstractCard)cards.get(i)).targetDrawScale = 0.75F;
/* 644 */       ((AbstractCard)cards.get(i)).drawScale = 0.75F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void cancelUpgrade() {
/* 649 */     this.cardSelectAmount = 0;
/* 650 */     this.confirmScreenUp = false;
/* 651 */     this.confirmButton.hide();
/* 652 */     this.confirmButton.isDisabled = true;
/* 653 */     this.hoveredCard = null;
/* 654 */     this.upgradePreviewCard = null;
/*     */     
/* 656 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/* 657 */       System.out.println("DERP");
/* 658 */       this.hoveredCard = this.controllerCard;
/*     */     }
/*     */     
/* 661 */     if (((this.forUpgrade) || (this.forTransform) || (this.forPurge) || (AbstractDungeon.previousScreen == AbstractDungeon.CurrentScreen.SHOP)) && (this.canCancel))
/*     */     {
/* 663 */       AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */     }
/*     */     
/*     */ 
/* 667 */     int lineNum = 0;
/* 668 */     ArrayList<AbstractCard> cards = this.targetGroup.group;
/* 669 */     for (int i = 0; i < cards.size(); i++) {
/* 670 */       int mod = i % 5;
/* 671 */       if ((mod == 0) && (i != 0)) {
/* 672 */         lineNum++;
/*     */       }
/* 674 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 675 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY);
/*     */     }
/*     */     
/* 678 */     this.tipMsg = this.lastTip;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 682 */     if (shouldShowScrollBar()) {
/* 683 */       this.scrollBar.render(sb);
/*     */     }
/*     */     
/* 686 */     if (this.hoveredCard != null) {
/* 687 */       this.targetGroup.renderExceptOneCard(sb, this.hoveredCard);
/* 688 */       this.hoveredCard.renderHoverShadow(sb);
/* 689 */       this.hoveredCard.render(sb);
/* 690 */       if (this.hoveredCard.inBottleFlame) {
/* 691 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledFlame();
/* 692 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 693 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 694 */         r.render(sb);
/* 695 */       } else if (this.hoveredCard.inBottleLightning) {
/* 696 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledLightning();
/* 697 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 698 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 699 */         r.render(sb);
/* 700 */       } else if (this.hoveredCard.inBottleTornado) {
/* 701 */         AbstractRelic r = new com.megacrit.cardcrawl.relics.BottledTornado();
/* 702 */         r.currentX = (this.hoveredCard.current_x + 130.0F * Settings.scale);
/* 703 */         r.currentY = (this.hoveredCard.current_y + 182.0F * Settings.scale);
/* 704 */         r.render(sb);
/*     */       }
/* 706 */       this.hoveredCard.renderCardTip(sb);
/*     */     } else {
/* 708 */       this.targetGroup.render(sb);
/*     */     }
/*     */     
/* 711 */     if (this.confirmScreenUp) {
/* 712 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.8F));
/* 713 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT - 64.0F * Settings.scale);
/*     */       
/* 715 */       if ((this.forTransform) || (this.forUpgrade)) {
/* 716 */         renderArrows(sb);
/*     */         
/* 718 */         this.hoveredCard.current_x = (Settings.WIDTH * 0.36F);
/* 719 */         this.hoveredCard.current_y = (Settings.HEIGHT / 2.0F);
/* 720 */         this.hoveredCard.target_x = (Settings.WIDTH * 0.36F);
/* 721 */         this.hoveredCard.target_y = (Settings.HEIGHT / 2.0F);
/* 722 */         this.hoveredCard.render(sb);
/* 723 */         this.hoveredCard.updateHoverLogic();
/*     */         
/*     */ 
/* 726 */         this.upgradePreviewCard.current_x = (Settings.WIDTH * 0.63F);
/* 727 */         this.upgradePreviewCard.current_y = (Settings.HEIGHT / 2.0F);
/* 728 */         this.upgradePreviewCard.target_x = (Settings.WIDTH * 0.63F);
/* 729 */         this.upgradePreviewCard.target_y = (Settings.HEIGHT / 2.0F);
/* 730 */         this.upgradePreviewCard.render(sb);
/* 731 */         this.upgradePreviewCard.updateHoverLogic();
/* 732 */         this.upgradePreviewCard.renderCardTip(sb);
/*     */       }
/*     */       else
/*     */       {
/* 736 */         this.hoveredCard.current_x = (Settings.WIDTH / 2.0F);
/* 737 */         this.hoveredCard.current_y = (Settings.HEIGHT / 2.0F);
/* 738 */         this.hoveredCard.render(sb);
/* 739 */         this.hoveredCard.updateHoverLogic();
/*     */       }
/*     */     }
/*     */     
/* 743 */     if ((this.forUpgrade) || (this.forTransform) || (this.forPurge) || (this.isJustForConfirming) || (this.anyNumber)) {
/* 744 */       this.confirmButton.render(sb);
/*     */     }
/*     */     
/* 747 */     if ((!this.isJustForConfirming) || (this.targetGroup.size() > 5)) {
/* 748 */       com.megacrit.cardcrawl.helpers.FontHelper.renderDeckViewTip(sb, this.tipMsg, 96.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderArrows(SpriteBatch sb)
/*     */   {
/* 754 */     float x = Settings.WIDTH / 2.0F - 73.0F * Settings.scale - 32.0F;
/* 755 */     sb.setColor(Color.WHITE);
/* 756 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale1 * Settings.scale, this.arrowScale1 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 774 */     x += 64.0F * Settings.scale;
/* 775 */     sb.setColor(Color.WHITE);
/* 776 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale2 * Settings.scale, this.arrowScale2 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 794 */     x += 64.0F * Settings.scale;
/*     */     
/* 796 */     sb.draw(ImageMaster.UPGRADE_ARROW, x, Settings.HEIGHT / 2.0F - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.arrowScale3 * Settings.scale, this.arrowScale3 * Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 814 */     this.arrowTimer += Gdx.graphics.getDeltaTime() * 2.0F;
/* 815 */     this.arrowScale1 = (0.8F + (MathUtils.cos(this.arrowTimer) + 1.0F) / 8.0F);
/* 816 */     this.arrowScale2 = (0.8F + (MathUtils.cos(this.arrowTimer - 0.8F) + 1.0F) / 8.0F);
/* 817 */     this.arrowScale3 = (0.8F + (MathUtils.cos(this.arrowTimer - 1.6F) + 1.0F) / 8.0F);
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 822 */     this.currentDiffY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 823 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 827 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.currentDiffY);
/* 828 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */   
/*     */   private boolean shouldShowScrollBar() {
/* 832 */     return (!this.confirmScreenUp) && (this.scrollUpperBound > SCROLL_BAR_THRESHOLD);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\select\GridCardSelectScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */