/*     */ package com.megacrit.cardcrawl.screens.compendium;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary.LibraryType;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.SingleCardViewPopup;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ColorTabBar.CurrentTab;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.SortHeaderButton;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class CardLibraryScreen implements com.megacrit.cardcrawl.screens.mainMenu.TabBarListener, com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  37 */   private static final Logger logger = LogManager.getLogger(CardLibraryScreen.class.getName());
/*  38 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CardLibraryScreen");
/*  39 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   private static float drawStartX;
/*  41 */   private static float drawStartY = Settings.HEIGHT * 0.66F;
/*     */   private static float padX;
/*  43 */   private static float padY; private static final int CARDS_PER_LINE = 5; private boolean grabbedScreen = false;
/*  44 */   private float grabStartY = 0.0F; private float currentDiffY = 0.0F;
/*  45 */   private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  46 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  47 */   private AbstractCard hoveredCard = null; private AbstractCard clickStartedCard = null;
/*     */   
/*     */   private ColorTabBar colorBar;
/*  50 */   public MenuCancelButton button = new MenuCancelButton();
/*  51 */   private CardGroup redCards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  52 */   private CardGroup greenCards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  53 */   private CardGroup blueCards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  54 */   private CardGroup colorlessCards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*  55 */   private CardGroup curseCards = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*     */   
/*     */   private CardLibSortHeader sortHeader;
/*     */   
/*     */   private CardGroup visibleCards;
/*     */   private ScrollBar scrollBar;
/*  61 */   private CardLibSelectionType type = CardLibSelectionType.NONE;
/*  62 */   private com.badlogic.gdx.graphics.Texture filterSelectionImg = null;
/*  63 */   private int selectionIndex = 0;
/*  64 */   private AbstractCard controllerCard = null;
/*     */   
/*     */   public CardLibraryScreen() {
/*  67 */     drawStartX = Settings.WIDTH;
/*  68 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  69 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  70 */     drawStartX /= 2.0F;
/*  71 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*  72 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  73 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*  74 */     this.colorBar = new ColorTabBar(this);
/*  75 */     this.sortHeader = new CardLibSortHeader(null);
/*  76 */     this.scrollBar = new ScrollBar(this);
/*     */   }
/*     */   
/*     */   public void initialize() {
/*  80 */     logger.info("Initializing card library screen.");
/*     */     
/*  82 */     this.redCards.group = CardLibrary.getCardList(CardLibrary.LibraryType.RED);
/*  83 */     this.greenCards.group = CardLibrary.getCardList(CardLibrary.LibraryType.GREEN);
/*  84 */     this.blueCards.group = CardLibrary.getCardList(CardLibrary.LibraryType.BLUE);
/*  85 */     this.colorlessCards.group = CardLibrary.getCardList(CardLibrary.LibraryType.COLORLESS);
/*  86 */     this.curseCards.group = CardLibrary.getCardList(CardLibrary.LibraryType.CURSE);
/*     */     
/*  88 */     this.visibleCards = this.redCards;
/*  89 */     this.sortHeader.setGroup(this.visibleCards);
/*  90 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */   private void setLockStatus() {
/*  94 */     lockStatusHelper(this.redCards);
/*  95 */     lockStatusHelper(this.greenCards);
/*  96 */     lockStatusHelper(this.blueCards);
/*  97 */     lockStatusHelper(this.colorlessCards);
/*  98 */     lockStatusHelper(this.curseCards);
/*     */   }
/*     */   
/*     */   private void lockStatusHelper(CardGroup group) {
/* 102 */     ArrayList<AbstractCard> toAdd = new ArrayList();
/* 103 */     for (Iterator<AbstractCard> i = group.group.iterator(); i.hasNext();) {
/* 104 */       AbstractCard c = (AbstractCard)i.next();
/* 105 */       if (com.megacrit.cardcrawl.unlock.UnlockTracker.isCardLocked(c.cardID)) {
/* 106 */         AbstractCard tmp = CardLibrary.getCopy(c.cardID);
/* 107 */         tmp.setLocked();
/* 108 */         toAdd.add(tmp);
/* 109 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 113 */     group.group.addAll(toAdd);
/*     */   }
/*     */   
/*     */   public void open() {
/* 117 */     this.controllerCard = null;
/* 118 */     if (Settings.isInfo) {
/* 119 */       CardLibrary.unlockAndSeeAllCards();
/*     */     }
/*     */     
/* 122 */     if (this.filterSelectionImg == null) {
/* 123 */       this.filterSelectionImg = ImageMaster.loadImage("images/ui/cardlibrary/selectBox.png");
/*     */     }
/*     */     
/* 126 */     setLockStatus();
/* 127 */     sortOnOpen();
/* 128 */     this.button.show(TEXT[0]);
/* 129 */     this.currentDiffY = -200.0F;
/*     */     
/* 131 */     for (AbstractCard c : this.redCards.group) {
/* 132 */       c.drawScale = MathUtils.random(0.2F, 0.4F);
/* 133 */       c.targetDrawScale = 0.75F;
/*     */     }
/* 135 */     for (AbstractCard c : this.greenCards.group) {
/* 136 */       c.drawScale = MathUtils.random(0.2F, 0.4F);
/* 137 */       c.targetDrawScale = 0.75F;
/*     */     }
/* 139 */     for (AbstractCard c : this.blueCards.group) {
/* 140 */       c.drawScale = MathUtils.random(0.2F, 0.4F);
/* 141 */       c.targetDrawScale = 0.75F;
/*     */     }
/* 143 */     SingleCardViewPopup.isViewingUpgrade = false;
/* 144 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.CARD_LIBRARY;
/*     */   }
/*     */   
/*     */   private void sortOnOpen() {
/* 148 */     this.sortHeader.justSorted = true;
/* 149 */     this.visibleCards.sortAlphabetically(true);
/* 150 */     this.visibleCards.sortByRarity(true);
/* 151 */     this.visibleCards.sortByStatus(true);
/*     */   }
/*     */   
/*     */   public void update() {
/* 155 */     updateControllerInput();
/* 156 */     if ((Settings.isControllerMode) && (this.controllerCard != null) && (!CardCrawlGame.isPopupOpen)) {
/* 157 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.75F) {
/* 158 */         this.currentDiffY += Settings.SCROLL_SPEED;
/* 159 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.25F) {
/* 160 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/* 164 */     this.colorBar.update(this.visibleCards.getBottomCard().current_y + 230.0F * Settings.scale);
/* 165 */     this.sortHeader.update();
/*     */     
/* 167 */     if (this.hoveredCard != null) {
/* 168 */       CardCrawlGame.cursor.changeType(com.megacrit.cardcrawl.core.GameCursor.CursorType.INSPECT);
/* 169 */       if (InputHelper.justClickedLeft) {
/* 170 */         this.clickStartedCard = this.hoveredCard;
/*     */       }
/* 172 */       if (((InputHelper.justReleasedClickLeft) && (this.clickStartedCard != null) && (this.hoveredCard != null)) || ((this.hoveredCard != null) && 
/* 173 */         (CInputActionSet.select.isJustPressed())))
/*     */       {
/* 175 */         if (Settings.isControllerMode) {
/* 176 */           this.clickStartedCard = this.hoveredCard;
/*     */         }
/*     */         
/* 179 */         InputHelper.justReleasedClickLeft = false;
/* 180 */         CardCrawlGame.cardPopup.open(this.clickStartedCard, this.visibleCards);
/* 181 */         this.clickStartedCard = null;
/*     */       }
/*     */     } else {
/* 184 */       this.clickStartedCard = null;
/*     */     }
/*     */     
/* 187 */     boolean isScrollBarScrolling = this.scrollBar.update();
/* 188 */     if ((!CardCrawlGame.cardPopup.isOpen) && (!isScrollBarScrolling)) {
/* 189 */       updateScrolling();
/*     */     }
/* 191 */     updateCards();
/*     */     
/* 193 */     this.button.update();
/* 194 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/* 195 */       InputHelper.pressedEscape = false;
/* 196 */       this.button.hb.clicked = false;
/* 197 */       this.button.hide();
/* 198 */       CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */     }
/*     */     
/* 201 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/* 202 */       Gdx.input.setCursorPosition((int)this.controllerCard.hb.cX, (int)(Settings.HEIGHT - this.controllerCard.hb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private static enum CardLibSelectionType {
/* 207 */     NONE,  FILTERS,  CARDS;
/*     */     
/*     */     private CardLibSelectionType() {} }
/*     */   
/* 211 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/* 212 */       return;
/*     */     }
/*     */     
/* 215 */     this.selectionIndex = 0;
/* 216 */     boolean anyHovered = false;
/* 217 */     this.type = CardLibSelectionType.NONE;
/*     */     
/* 219 */     if (this.colorBar.viewUpgradeHb.hovered) {
/* 220 */       anyHovered = true;
/* 221 */       this.type = CardLibSelectionType.FILTERS;
/* 222 */       this.selectionIndex = 4;
/* 223 */       this.controllerCard = null;
/* 224 */     } else if (this.sortHeader.updateControllerInput() != null) {
/* 225 */       anyHovered = true;
/* 226 */       this.controllerCard = null;
/* 227 */       this.type = CardLibSelectionType.FILTERS;
/* 228 */       this.selectionIndex = this.sortHeader.getHoveredIndex();
/*     */     } else {
/* 230 */       for (AbstractCard c : this.visibleCards.group) {
/* 231 */         if (c.hb.hovered) {
/* 232 */           anyHovered = true;
/* 233 */           this.type = CardLibSelectionType.CARDS;
/* 234 */           break;
/*     */         }
/* 236 */         this.selectionIndex += 1;
/*     */       }
/*     */     }
/*     */     
/* 240 */     if (!anyHovered) {
/* 241 */       Gdx.input.setCursorPosition(
/* 242 */         (int)((AbstractCard)this.visibleCards.group.get(0)).hb.cX, Settings.HEIGHT - 
/* 243 */         (int)((AbstractCard)this.visibleCards.group.get(0)).hb.cY);
/* 244 */       return;
/*     */     }
/*     */     
/* 247 */     switch (this.type) {
/*     */     case CARDS: 
/* 249 */       if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 250 */         (this.visibleCards.size() > 5))
/*     */       {
/* 252 */         if (this.selectionIndex < 5) {
/* 253 */           Gdx.input.setCursorPosition((int)this.sortHeader.buttons[0].hb.cX, Settings.HEIGHT - (int)this.sortHeader.buttons[0].hb.cY);
/*     */           
/*     */ 
/* 256 */           this.controllerCard = null;
/* 257 */           return;
/*     */         }
/*     */         
/* 260 */         this.selectionIndex -= 5;
/*     */         
/* 262 */         Gdx.input.setCursorPosition(
/* 263 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cX, Settings.HEIGHT - 
/* 264 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cY);
/* 265 */         this.controllerCard = ((AbstractCard)this.visibleCards.group.get(this.selectionIndex));
/* 266 */       } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 267 */         (this.visibleCards.size() > 5)) {
/* 268 */         if (this.selectionIndex < this.visibleCards.size() - 5) {
/* 269 */           this.selectionIndex += 5;
/*     */         } else {
/* 271 */           this.selectionIndex %= 5;
/*     */         }
/*     */         
/* 274 */         Gdx.input.setCursorPosition(
/* 275 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cX, Settings.HEIGHT - 
/* 276 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cY);
/* 277 */         this.controllerCard = ((AbstractCard)this.visibleCards.group.get(this.selectionIndex));
/* 278 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 279 */         if (this.selectionIndex % 5 > 0) {
/* 280 */           this.selectionIndex -= 1;
/*     */         } else {
/* 282 */           this.selectionIndex += 4;
/* 283 */           if (this.selectionIndex > this.visibleCards.size() - 1) {
/* 284 */             this.selectionIndex = (this.visibleCards.size() - 1);
/*     */           }
/*     */         }
/* 287 */         Gdx.input.setCursorPosition(
/* 288 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cX, Settings.HEIGHT - 
/* 289 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cY);
/* 290 */         this.controllerCard = ((AbstractCard)this.visibleCards.group.get(this.selectionIndex));
/* 291 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 292 */         if (this.selectionIndex % 5 < 4) {
/* 293 */           this.selectionIndex += 1;
/* 294 */           if (this.selectionIndex > this.visibleCards.size() - 1) {
/* 295 */             this.selectionIndex -= this.visibleCards.size() % 5;
/*     */           }
/*     */         } else {
/* 298 */           this.selectionIndex -= 4;
/* 299 */           if (this.selectionIndex < 0) {
/* 300 */             this.selectionIndex = 0;
/*     */           }
/*     */         }
/* 303 */         Gdx.input.setCursorPosition(
/* 304 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cX, Settings.HEIGHT - 
/* 305 */           (int)((AbstractCard)this.visibleCards.group.get(this.selectionIndex)).hb.cY);
/* 306 */         this.controllerCard = ((AbstractCard)this.visibleCards.group.get(this.selectionIndex));
/*     */       }
/*     */       break;
/*     */     case FILTERS: 
/* 310 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 311 */         Gdx.input.setCursorPosition(
/* 312 */           (int)((AbstractCard)this.visibleCards.group.get(0)).hb.cX, Settings.HEIGHT - 
/* 313 */           (int)((AbstractCard)this.visibleCards.group.get(0)).hb.cY);
/* 314 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 315 */         this.selectionIndex += 1;
/* 316 */         if (this.selectionIndex == 4) {
/* 317 */           Gdx.input.setCursorPosition((int)this.colorBar.viewUpgradeHb.cX, Settings.HEIGHT - (int)this.colorBar.viewUpgradeHb.cY);
/*     */         }
/*     */         else
/*     */         {
/* 321 */           if (this.selectionIndex == 5) {
/* 322 */             this.selectionIndex = 0;
/*     */           }
/* 324 */           Gdx.input.setCursorPosition((int)this.sortHeader.buttons[this.selectionIndex].hb.cX, Settings.HEIGHT - (int)this.sortHeader.buttons[this.selectionIndex].hb.cY);
/*     */         }
/*     */         
/*     */       }
/* 328 */       else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 329 */         this.selectionIndex -= 1;
/* 330 */         if (this.selectionIndex == -1) {
/* 331 */           Gdx.input.setCursorPosition((int)this.colorBar.viewUpgradeHb.cX, Settings.HEIGHT - (int)this.colorBar.viewUpgradeHb.cY);
/*     */         }
/*     */         else
/*     */         {
/* 335 */           Gdx.input.setCursorPosition((int)this.sortHeader.buttons[this.selectionIndex].hb.cX, Settings.HEIGHT - (int)this.sortHeader.buttons[this.selectionIndex].hb.cY);
/*     */         }
/*     */       }
/*     */       
/*     */       break;
/*     */     case NONE: 
/*     */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateCards()
/*     */   {
/* 349 */     this.hoveredCard = null;
/* 350 */     int lineNum = 0;
/*     */     
/* 352 */     ArrayList<AbstractCard> cards = this.visibleCards.group;
/*     */     
/* 354 */     for (int i = 0; i < cards.size(); i++) {
/* 355 */       int mod = i % 5;
/* 356 */       if ((mod == 0) && (i != 0)) {
/* 357 */         lineNum++;
/*     */       }
/* 359 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 360 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 361 */       ((AbstractCard)cards.get(i)).update();
/* 362 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/* 363 */       if (((AbstractCard)cards.get(i)).hb.hovered) {
/* 364 */         this.hoveredCard = ((AbstractCard)cards.get(i));
/*     */       }
/*     */     }
/* 367 */     if (this.sortHeader.justSorted) {
/* 368 */       for (AbstractCard c : cards) {
/* 369 */         c.current_x = c.target_x;
/* 370 */         c.current_y = c.target_y;
/*     */       }
/* 372 */       this.sortHeader.justSorted = false;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling() {
/* 377 */     int y = InputHelper.mY;
/*     */     
/* 379 */     if (!this.grabbedScreen) {
/* 380 */       if (InputHelper.scrolledDown) {
/* 381 */         this.currentDiffY += Settings.SCROLL_SPEED;
/* 382 */       } else if (InputHelper.scrolledUp) {
/* 383 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 386 */       if (InputHelper.justClickedLeft) {
/* 387 */         this.grabbedScreen = true;
/* 388 */         this.grabStartY = (y - this.currentDiffY);
/*     */       }
/*     */     }
/* 391 */     else if (InputHelper.isMouseDown) {
/* 392 */       this.currentDiffY = (y - this.grabStartY);
/*     */     } else {
/* 394 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 398 */     resetScrolling();
/* 399 */     updateBarPosition();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/* 406 */     int size = this.visibleCards.size();
/*     */     
/* 408 */     int scrollTmp = 0;
/* 409 */     if (size > 10) {
/* 410 */       scrollTmp = size / 5 - 2;
/* 411 */       if (size % 5 != 0) {
/* 412 */         scrollTmp++;
/*     */       }
/* 414 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/* 416 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 424 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 425 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 426 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 427 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 432 */     this.colorBar.render(sb, this.visibleCards.getBottomCard().current_y + 230.0F * Settings.scale);
/* 433 */     this.sortHeader.render(sb);
/* 434 */     renderGroup(sb, this.visibleCards);
/*     */     
/* 436 */     if (this.hoveredCard != null) {
/* 437 */       this.hoveredCard.renderHoverShadow(sb);
/* 438 */       this.hoveredCard.renderInLibrary(sb);
/*     */     }
/*     */     
/* 441 */     this.button.render(sb);
/* 442 */     this.scrollBar.render(sb);
/* 443 */     if (Settings.isControllerMode) {
/* 444 */       renderControllerUi(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderControllerUi(SpriteBatch sb)
/*     */   {
/* 450 */     sb.draw(CInputActionSet.pageLeftViewDeck
/* 451 */       .getKeyImg(), 280.0F * Settings.scale - 32.0F, 
/*     */       
/* 453 */       this.sortHeader.group.getBottomCard().current_y + 280.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 467 */     sb.draw(CInputActionSet.pageRightViewExhaust
/* 468 */       .getKeyImg(), 1640.0F * Settings.scale - 32.0F, 
/*     */       
/* 470 */       this.sortHeader.group.getBottomCard().current_y + 280.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 486 */     if (this.type == CardLibSelectionType.FILTERS) {
/* 487 */       if ((this.selectionIndex != 4) && (this.selectionIndex != -1)) {
/* 488 */         sb.setColor(new Color(1.0F, 0.95F, 0.5F, 0.7F + 
/* 489 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 490 */         float doop = 1.0F + (1.0F + MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L))) / 50.0F;
/* 491 */         sb.draw(this.filterSelectionImg, this.sortHeader.buttons[this.selectionIndex].hb.cX - 100.0F, this.sortHeader.buttons[this.selectionIndex].hb.cY - 43.0F, 100.0F, 43.0F, 200.0F, 86.0F, Settings.scale * doop, Settings.scale * doop, 0.0F, 0, 0, 200, 86, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 509 */         sb.setColor(new Color(1.0F, 0.95F, 0.5F, 0.7F + 
/* 510 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 511 */         float doop = 1.0F + (1.0F + MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L))) / 50.0F;
/* 512 */         sb.draw(this.filterSelectionImg, this.colorBar.viewUpgradeHb.cX - 100.0F + 25.0F * Settings.scale, this.colorBar.viewUpgradeHb.cY - 43.0F, 100.0F, 43.0F, 200.0F, 86.0F, Settings.scale * doop * 1.6F, Settings.scale * doop, 0.0F, 0, 0, 200, 86, false, false);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderGroup(SpriteBatch sb, CardGroup group)
/*     */   {
/* 534 */     group.renderInLibrary(sb);
/* 535 */     group.renderTip(sb);
/*     */   }
/*     */   
/*     */   public void didChangeTab(ColorTabBar tabBar, ColorTabBar.CurrentTab newSelection)
/*     */   {
/* 540 */     CardGroup oldSelection = this.visibleCards;
/* 541 */     switch (newSelection) {
/*     */     case RED: 
/* 543 */       this.visibleCards = this.redCards;
/* 544 */       break;
/*     */     case GREEN: 
/* 546 */       this.visibleCards = this.greenCards;
/* 547 */       break;
/*     */     case BLUE: 
/* 549 */       this.visibleCards = this.blueCards;
/* 550 */       break;
/*     */     case COLORLESS: 
/* 552 */       this.visibleCards = this.colorlessCards;
/* 553 */       break;
/*     */     case CURSE: 
/* 555 */       this.visibleCards = this.curseCards;
/*     */     }
/*     */     
/* 558 */     if (oldSelection != this.visibleCards) {
/* 559 */       this.sortHeader.setGroup(this.visibleCards);
/* 560 */       calculateScrollBounds();
/*     */     }
/* 562 */     this.sortHeader.justSorted = true;
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 567 */     this.currentDiffY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 568 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 572 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.currentDiffY);
/* 573 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\compendium\CardLibraryScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */