/*     */ package com.megacrit.cardcrawl.screens.compendium;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.SortHeaderButton;
/*     */ 
/*     */ public class CardLibSortHeader implements com.megacrit.cardcrawl.screens.mainMenu.SortHeaderButtonListener
/*     */ {
/*  13 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CardLibSortHeader");
/*  14 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public CardGroup group;
/*  17 */   public boolean justSorted = false;
/*  18 */   private static final float START_X = 430.0F * Settings.scale;
/*  19 */   private static final float SPACE_X = 226.0F * Settings.scale;
/*     */   private SortHeaderButton rarityButton;
/*     */   private SortHeaderButton typeButton;
/*     */   private SortHeaderButton nameButton;
/*     */   private SortHeaderButton costButton;
/*     */   public SortHeaderButton[] buttons;
/*     */   
/*     */   public CardLibSortHeader(CardGroup group)
/*     */   {
/*  28 */     this.group = group;
/*     */     
/*  30 */     float xPosition = START_X;
/*  31 */     this.rarityButton = new SortHeaderButton(TEXT[0], xPosition, 0.0F, this);
/*  32 */     xPosition += SPACE_X;
/*  33 */     this.typeButton = new SortHeaderButton(TEXT[1], xPosition, 0.0F, this);
/*  34 */     xPosition += SPACE_X;
/*  35 */     this.nameButton = new SortHeaderButton(TEXT[2], xPosition, 0.0F, this);
/*  36 */     xPosition += SPACE_X;
/*  37 */     this.costButton = new SortHeaderButton(TEXT[3], xPosition, 0.0F, this);
/*     */     
/*  39 */     this.buttons = new SortHeaderButton[] { this.rarityButton, this.typeButton, this.nameButton, this.costButton };
/*     */   }
/*     */   
/*     */   public void setGroup(CardGroup group) {
/*  43 */     this.group = group;
/*  44 */     group.sortAlphabetically(true);
/*  45 */     group.sortByRarity(true);
/*  46 */     group.sortByStatus(true);
/*     */     
/*  48 */     for (SortHeaderButton button : this.buttons) {
/*  49 */       button.reset();
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/*  54 */     for (SortHeaderButton button : this.buttons) {
/*  55 */       button.update();
/*     */     }
/*     */   }
/*     */   
/*     */   public Hitbox updateControllerInput() {
/*  60 */     for (SortHeaderButton button : this.buttons) {
/*  61 */       if (button.hb.hovered) {
/*  62 */         return button.hb;
/*     */       }
/*     */     }
/*  65 */     return null;
/*     */   }
/*     */   
/*     */   public int getHoveredIndex() {
/*  69 */     int retVal = 0;
/*  70 */     for (SortHeaderButton button : this.buttons) {
/*  71 */       if (button.hb.hovered) {
/*  72 */         return retVal;
/*     */       }
/*  74 */       retVal++;
/*     */     }
/*  76 */     return 0;
/*     */   }
/*     */   
/*     */   public void didChangeOrder(SortHeaderButton button, boolean isAscending)
/*     */   {
/*  81 */     if (button == this.rarityButton) {
/*  82 */       this.group.sortByRarity(isAscending);
/*  83 */     } else if (button == this.typeButton) {
/*  84 */       this.group.sortByType(isAscending);
/*  85 */     } else if (button == this.nameButton) {
/*  86 */       this.group.sortAlphabetically(isAscending);
/*  87 */     } else if (button == this.costButton) {
/*  88 */       this.group.sortByCost(isAscending);
/*     */     } else {
/*  90 */       return;
/*     */     }
/*  92 */     this.group.sortByStatus(false);
/*  93 */     this.justSorted = true;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  97 */     float scrolledY = this.group.getBottomCard().current_y + 230.0F * Settings.scale;
/*     */     
/*  99 */     for (SortHeaderButton button : this.buttons) {
/* 100 */       button.updateScrollPosition(scrolledY);
/* 101 */       button.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\compendium\CardLibSortHeader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */