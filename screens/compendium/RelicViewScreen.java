/*     */ package com.megacrit.cardcrawl.screens.compendium;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class RelicViewScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  26 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("RelicViewScreen");
/*  27 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  29 */   private static final float SPACE = 80.0F * Settings.scale;
/*  30 */   private static final float START_X = 600.0F * Settings.scale;
/*  31 */   private static final float START_Y = Settings.HEIGHT - 300.0F * Settings.scale;
/*  32 */   private float scrollY = START_Y; private float targetY = this.scrollY;
/*  33 */   private float scrollLowerBound = Settings.HEIGHT - 100.0F * Settings.scale;
/*  34 */   private float scrollUpperBound = 2600.0F * Settings.scale;
/*  35 */   private int row = 0; private int col = 0;
/*  36 */   public MenuCancelButton button = new MenuCancelButton();
/*  37 */   private static final Color RED_OUTLINE_COLOR = new Color(-10132568);
/*  38 */   private static final Color GREEN_OUTLINE_COLOR = new Color(2147418280);
/*  39 */   private static final Color BLUE_OUTLINE_COLOR = new Color(-2016482392);
/*  40 */   private static final Color BLACK_OUTLINE_COLOR = new Color(168);
/*     */   
/*     */ 
/*  43 */   private AbstractRelic hoveredRelic = null; private AbstractRelic clickStartedRelic = null;
/*  44 */   private ArrayList<AbstractRelic> relicGroup = null;
/*     */   
/*     */ 
/*  47 */   private boolean grabbedScreen = false;
/*  48 */   private float grabStartY = 0.0F;
/*     */   private ScrollBar scrollBar;
/*  50 */   private Hitbox controllerRelicHb = null;
/*     */   
/*     */   public RelicViewScreen() {
/*  53 */     this.scrollBar = new ScrollBar(this);
/*     */   }
/*     */   
/*     */   public void open() {
/*  57 */     this.controllerRelicHb = null;
/*  58 */     if (Settings.isInfo) {
/*  59 */       RelicLibrary.unlockAndSeeAllRelics();
/*     */     }
/*     */     
/*  62 */     sortOnOpen();
/*  63 */     this.button.show(TEXT[0]);
/*  64 */     this.targetY = this.scrollLowerBound;
/*  65 */     this.scrollY = (Settings.HEIGHT - 400.0F * Settings.scale);
/*  66 */     CardCrawlGame.mainMenuScreen.screen = com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen.RELIC_VIEW;
/*     */   }
/*     */   
/*     */   private void sortOnOpen() {
/*  70 */     RelicLibrary.starterList = RelicLibrary.sortByStatus(RelicLibrary.starterList, false);
/*  71 */     RelicLibrary.commonList = RelicLibrary.sortByStatus(RelicLibrary.commonList, false);
/*  72 */     RelicLibrary.uncommonList = RelicLibrary.sortByStatus(RelicLibrary.uncommonList, false);
/*  73 */     RelicLibrary.rareList = RelicLibrary.sortByStatus(RelicLibrary.rareList, false);
/*  74 */     RelicLibrary.bossList = RelicLibrary.sortByStatus(RelicLibrary.bossList, false);
/*  75 */     RelicLibrary.specialList = RelicLibrary.sortByStatus(RelicLibrary.specialList, false);
/*  76 */     RelicLibrary.shopList = RelicLibrary.sortByStatus(RelicLibrary.shopList, false);
/*     */   }
/*     */   
/*     */   public void update() {
/*  80 */     updateControllerInput();
/*  81 */     if ((Settings.isControllerMode) && (this.controllerRelicHb != null)) {
/*  82 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.7F) {
/*  83 */         this.targetY += Settings.SCROLL_SPEED;
/*  84 */         if (this.targetY > this.scrollUpperBound) {
/*  85 */           this.targetY = this.scrollUpperBound;
/*     */         }
/*  87 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.3F) {
/*  88 */         this.targetY -= Settings.SCROLL_SPEED;
/*  89 */         if (this.targetY < this.scrollLowerBound) {
/*  90 */           this.targetY = this.scrollLowerBound;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  95 */     if (this.hoveredRelic != null) {
/*  96 */       CardCrawlGame.cursor.changeType(com.megacrit.cardcrawl.core.GameCursor.CursorType.INSPECT);
/*  97 */       if ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) {
/*  98 */         this.clickStartedRelic = this.hoveredRelic;
/*     */       }
/* 100 */       if ((InputHelper.justReleasedClickLeft) || (CInputActionSet.select.isJustPressed())) {
/* 101 */         CInputActionSet.select.unpress();
/* 102 */         if (this.hoveredRelic == this.clickStartedRelic) {
/* 103 */           CardCrawlGame.relicPopup.open(this.hoveredRelic, this.relicGroup);
/* 104 */           this.clickStartedRelic = null;
/*     */         }
/*     */       }
/*     */     } else {
/* 108 */       this.clickStartedRelic = null;
/*     */     }
/*     */     
/* 111 */     this.button.update();
/* 112 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/* 113 */       InputHelper.pressedEscape = false;
/* 114 */       this.button.hide();
/* 115 */       CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */     }
/*     */     
/* 118 */     boolean isScrollingScrollBar = this.scrollBar.update();
/* 119 */     if (!isScrollingScrollBar) {
/* 120 */       updateScrolling();
/*     */     }
/* 122 */     InputHelper.justClickedLeft = false;
/*     */     
/* 124 */     this.hoveredRelic = null;
/* 125 */     this.relicGroup = null;
/* 126 */     updateList(RelicLibrary.starterList);
/* 127 */     updateList(RelicLibrary.commonList);
/* 128 */     updateList(RelicLibrary.uncommonList);
/* 129 */     updateList(RelicLibrary.rareList);
/* 130 */     updateList(RelicLibrary.bossList);
/* 131 */     updateList(RelicLibrary.specialList);
/* 132 */     updateList(RelicLibrary.shopList);
/*     */     
/* 134 */     if ((Settings.isControllerMode) && (this.controllerRelicHb != null)) {
/* 135 */       Gdx.input.setCursorPosition((int)this.controllerRelicHb.cX, (int)(Settings.HEIGHT - this.controllerRelicHb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private static enum RelicCategory {
/* 140 */     STARTER,  COMMON,  UNCOMMON,  RARE,  BOSS,  EVENT,  SHOP,  NONE;
/*     */     
/*     */     private RelicCategory() {} }
/*     */   
/* 144 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/* 145 */       return;
/*     */     }
/*     */     
/* 148 */     RelicCategory category = RelicCategory.NONE;
/* 149 */     int index = 0;
/* 150 */     boolean anyHovered = false;
/* 151 */     for (AbstractRelic r : RelicLibrary.starterList) {
/* 152 */       if (r.hb.hovered) {
/* 153 */         anyHovered = true;
/* 154 */         category = RelicCategory.STARTER;
/* 155 */         break;
/*     */       }
/* 157 */       index++;
/*     */     }
/*     */     
/* 160 */     if (!anyHovered) {
/* 161 */       index = 0;
/* 162 */       for (AbstractRelic r : RelicLibrary.commonList) {
/* 163 */         if (r.hb.hovered) {
/* 164 */           anyHovered = true;
/* 165 */           category = RelicCategory.COMMON;
/* 166 */           break;
/*     */         }
/* 168 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 172 */     if (!anyHovered) {
/* 173 */       index = 0;
/* 174 */       for (AbstractRelic r : RelicLibrary.uncommonList) {
/* 175 */         if (r.hb.hovered) {
/* 176 */           anyHovered = true;
/* 177 */           category = RelicCategory.UNCOMMON;
/* 178 */           break;
/*     */         }
/* 180 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 184 */     if (!anyHovered) {
/* 185 */       index = 0;
/* 186 */       for (AbstractRelic r : RelicLibrary.rareList) {
/* 187 */         if (r.hb.hovered) {
/* 188 */           anyHovered = true;
/* 189 */           category = RelicCategory.RARE;
/* 190 */           break;
/*     */         }
/* 192 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 196 */     if (!anyHovered) {
/* 197 */       index = 0;
/* 198 */       for (AbstractRelic r : RelicLibrary.bossList) {
/* 199 */         if (r.hb.hovered) {
/* 200 */           anyHovered = true;
/* 201 */           category = RelicCategory.BOSS;
/* 202 */           break;
/*     */         }
/* 204 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 208 */     if (!anyHovered) {
/* 209 */       index = 0;
/* 210 */       for (AbstractRelic r : RelicLibrary.specialList) {
/* 211 */         if (r.hb.hovered) {
/* 212 */           anyHovered = true;
/* 213 */           category = RelicCategory.EVENT;
/* 214 */           break;
/*     */         }
/* 216 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 220 */     if (!anyHovered) {
/* 221 */       index = 0;
/* 222 */       for (AbstractRelic r : RelicLibrary.shopList) {
/* 223 */         if (r.hb.hovered) {
/* 224 */           anyHovered = true;
/* 225 */           category = RelicCategory.SHOP;
/* 226 */           break;
/*     */         }
/* 228 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 232 */     if (!anyHovered) {
/* 233 */       Gdx.input.setCursorPosition(
/* 234 */         (int)((AbstractRelic)RelicLibrary.starterList.get(0)).hb.cX, Settings.HEIGHT - 
/* 235 */         (int)((AbstractRelic)RelicLibrary.starterList.get(0)).hb.cY);
/*     */     } else {
/* 237 */       switch (category) {
/*     */       case STARTER: 
/* 239 */         if ((!CInputActionSet.up.isJustPressed()) && (!CInputActionSet.altUp.isJustPressed()))
/*     */         {
/* 241 */           if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 242 */             index += 10;
/* 243 */             if (index > RelicLibrary.starterList.size() - 1) {
/* 244 */               index %= 10;
/* 245 */               if (index > RelicLibrary.commonList.size() - 1) {
/* 246 */                 index = RelicLibrary.commonList.size() - 1;
/*     */               }
/* 248 */               Gdx.input.setCursorPosition(
/* 249 */                 (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 250 */                 (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 251 */               this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/*     */             }
/* 253 */           } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 254 */             index--;
/* 255 */             if (index < 0) {
/* 256 */               index = RelicLibrary.starterList.size() - 1;
/*     */             }
/* 258 */             Gdx.input.setCursorPosition(
/* 259 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cX, Settings.HEIGHT - 
/* 260 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cY);
/* 261 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.starterList.get(index)).hb;
/* 262 */           } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 263 */             index++;
/* 264 */             if (index > RelicLibrary.starterList.size() - 1) {
/* 265 */               index = 0;
/*     */             }
/* 267 */             Gdx.input.setCursorPosition(
/* 268 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cX, Settings.HEIGHT - 
/* 269 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cY);
/* 270 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.starterList.get(index)).hb;
/*     */           } }
/*     */         break;
/*     */       case COMMON: 
/* 274 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 275 */           index -= 10;
/* 276 */           if (index < 0) {
/* 277 */             index = RelicLibrary.starterList.size() - 1;
/* 278 */             Gdx.input.setCursorPosition(
/* 279 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cX, Settings.HEIGHT - 
/* 280 */               (int)((AbstractRelic)RelicLibrary.starterList.get(index)).hb.cY);
/* 281 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.starterList.get(index)).hb;
/*     */           } else {
/* 283 */             Gdx.input.setCursorPosition(
/* 284 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 285 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 286 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/*     */           }
/* 288 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 289 */           index += 10;
/* 290 */           if (index > RelicLibrary.commonList.size() - 1) {
/* 291 */             index %= 10;
/* 292 */             if (index > RelicLibrary.uncommonList.size() - 1) {
/* 293 */               index = RelicLibrary.uncommonList.size() - 1;
/*     */             }
/* 295 */             Gdx.input.setCursorPosition(
/* 296 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 297 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 298 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/*     */           } else {
/* 300 */             Gdx.input.setCursorPosition(
/* 301 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 302 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 303 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/*     */           }
/* 305 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 306 */           index--;
/* 307 */           if (index < 0) {
/* 308 */             index = RelicLibrary.commonList.size() - 1;
/*     */           }
/* 310 */           Gdx.input.setCursorPosition(
/* 311 */             (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 312 */             (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 313 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/* 314 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 315 */           index++;
/* 316 */           if (index > RelicLibrary.commonList.size() - 1) {
/* 317 */             index = 0;
/*     */           }
/* 319 */           Gdx.input.setCursorPosition(
/* 320 */             (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 321 */             (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 322 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/*     */         }
/*     */         break;
/*     */       case UNCOMMON: 
/* 326 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 327 */           index -= 10;
/* 328 */           if (index < 0) {
/* 329 */             index = RelicLibrary.commonList.size() - 1;
/* 330 */             Gdx.input.setCursorPosition(
/* 331 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 332 */               (int)((AbstractRelic)RelicLibrary.commonList.get(index)).hb.cY);
/* 333 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.commonList.get(index)).hb;
/*     */           } else {
/* 335 */             Gdx.input.setCursorPosition(
/* 336 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 337 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 338 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/*     */           }
/* 340 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 341 */           index += 10;
/* 342 */           if (index > RelicLibrary.uncommonList.size() - 1) {
/* 343 */             index %= 10;
/* 344 */             if (index > RelicLibrary.rareList.size() - 1) {
/* 345 */               index = RelicLibrary.rareList.size() - 1;
/*     */             }
/* 347 */             Gdx.input.setCursorPosition(
/* 348 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 349 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 350 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/*     */           } else {
/* 352 */             Gdx.input.setCursorPosition(
/* 353 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 354 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 355 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/*     */           }
/* 357 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 358 */           index--;
/* 359 */           if (index < 0) {
/* 360 */             index = RelicLibrary.uncommonList.size() - 1;
/*     */           }
/* 362 */           Gdx.input.setCursorPosition(
/* 363 */             (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 364 */             (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 365 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/* 366 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 367 */           index++;
/* 368 */           if (index > RelicLibrary.uncommonList.size() - 1) {
/* 369 */             index = 0;
/*     */           }
/* 371 */           Gdx.input.setCursorPosition(
/* 372 */             (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 373 */             (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 374 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/*     */         }
/*     */         break;
/*     */       case RARE: 
/* 378 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 379 */           index -= 10;
/* 380 */           if (index < 0) {
/* 381 */             index = RelicLibrary.uncommonList.size() - 1;
/* 382 */             Gdx.input.setCursorPosition(
/* 383 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cX, Settings.HEIGHT - 
/* 384 */               (int)((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb.cY);
/* 385 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.uncommonList.get(index)).hb;
/*     */           } else {
/* 387 */             Gdx.input.setCursorPosition(
/* 388 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 389 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 390 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/*     */           }
/* 392 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 393 */           index += 10;
/* 394 */           if (index > RelicLibrary.rareList.size() - 1) {
/* 395 */             index %= 10;
/* 396 */             if (index > RelicLibrary.bossList.size() - 1) {
/* 397 */               index = RelicLibrary.bossList.size() - 1;
/*     */             }
/* 399 */             Gdx.input.setCursorPosition(
/* 400 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 401 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 402 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/*     */           } else {
/* 404 */             Gdx.input.setCursorPosition(
/* 405 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 406 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 407 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/*     */           }
/* 409 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 410 */           index--;
/* 411 */           if (index < 0) {
/* 412 */             index = RelicLibrary.rareList.size() - 1;
/*     */           }
/* 414 */           Gdx.input.setCursorPosition(
/* 415 */             (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 416 */             (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 417 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/* 418 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 419 */           index++;
/* 420 */           if (index > RelicLibrary.rareList.size() - 1) {
/* 421 */             index = 0;
/*     */           }
/* 423 */           Gdx.input.setCursorPosition(
/* 424 */             (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 425 */             (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 426 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/*     */         }
/*     */         break;
/*     */       case BOSS: 
/* 430 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 431 */           index -= 10;
/* 432 */           if (index < 0) {
/* 433 */             index = RelicLibrary.rareList.size() - 1;
/* 434 */             Gdx.input.setCursorPosition(
/* 435 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cX, Settings.HEIGHT - 
/* 436 */               (int)((AbstractRelic)RelicLibrary.rareList.get(index)).hb.cY);
/* 437 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.rareList.get(index)).hb;
/*     */           } else {
/* 439 */             Gdx.input.setCursorPosition(
/* 440 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 441 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 442 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/*     */           }
/* 444 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 445 */           index += 10;
/* 446 */           if (index > RelicLibrary.bossList.size() - 1) {
/* 447 */             index %= 10;
/* 448 */             if (index > RelicLibrary.specialList.size() - 1) {
/* 449 */               index = RelicLibrary.specialList.size() - 1;
/*     */             }
/* 451 */             Gdx.input.setCursorPosition(
/* 452 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 453 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 454 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/*     */           } else {
/* 456 */             Gdx.input.setCursorPosition(
/* 457 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 458 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 459 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/*     */           }
/* 461 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 462 */           index--;
/* 463 */           if (index < 0) {
/* 464 */             index = RelicLibrary.bossList.size() - 1;
/*     */           }
/* 466 */           Gdx.input.setCursorPosition(
/* 467 */             (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 468 */             (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 469 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/* 470 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 471 */           index++;
/* 472 */           if (index > RelicLibrary.bossList.size() - 1) {
/* 473 */             index = 0;
/*     */           }
/* 475 */           Gdx.input.setCursorPosition(
/* 476 */             (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 477 */             (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 478 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/*     */         }
/*     */         break;
/*     */       case EVENT: 
/* 482 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 483 */           index -= 10;
/* 484 */           if (index < 0) {
/* 485 */             index = RelicLibrary.bossList.size() - 1;
/* 486 */             Gdx.input.setCursorPosition(
/* 487 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cX, Settings.HEIGHT - 
/* 488 */               (int)((AbstractRelic)RelicLibrary.bossList.get(index)).hb.cY);
/* 489 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.bossList.get(index)).hb;
/*     */           } else {
/* 491 */             if (index < RelicLibrary.specialList.size() - 1) {
/* 492 */               index = RelicLibrary.specialList.size() - 1;
/*     */             }
/* 494 */             Gdx.input.setCursorPosition(
/* 495 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 496 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 497 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/*     */           }
/* 499 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 500 */           index += 10;
/* 501 */           if (index > RelicLibrary.specialList.size() - 1) {
/* 502 */             index %= 10;
/* 503 */             if (index > RelicLibrary.shopList.size() - 1) {
/* 504 */               index = RelicLibrary.shopList.size() - 1;
/*     */             }
/* 506 */             Gdx.input.setCursorPosition(
/* 507 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cX, Settings.HEIGHT - 
/* 508 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cY);
/* 509 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.shopList.get(index)).hb;
/*     */           } else {
/* 511 */             Gdx.input.setCursorPosition(
/* 512 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 513 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 514 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/*     */           }
/* 516 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 517 */           index--;
/* 518 */           if (index < 0) {
/* 519 */             index = RelicLibrary.specialList.size() - 1;
/*     */           }
/* 521 */           Gdx.input.setCursorPosition(
/* 522 */             (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 523 */             (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 524 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/* 525 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 526 */           index++;
/* 527 */           if (index > RelicLibrary.specialList.size() - 1) {
/* 528 */             index = 0;
/*     */           }
/* 530 */           Gdx.input.setCursorPosition(
/* 531 */             (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 532 */             (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 533 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/*     */         }
/*     */         break;
/*     */       case SHOP: 
/* 537 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 538 */           index -= 10;
/* 539 */           if (index < 0) {
/* 540 */             index = RelicLibrary.specialList.size() - 1;
/* 541 */             Gdx.input.setCursorPosition(
/* 542 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cX, Settings.HEIGHT - 
/* 543 */               (int)((AbstractRelic)RelicLibrary.specialList.get(index)).hb.cY);
/* 544 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.specialList.get(index)).hb;
/*     */           } else {
/* 546 */             if (index > RelicLibrary.shopList.size() - 1) {
/* 547 */               index = RelicLibrary.shopList.size() - 1;
/*     */             }
/* 549 */             Gdx.input.setCursorPosition(
/* 550 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cX, Settings.HEIGHT - 
/* 551 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cY);
/* 552 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.shopList.get(index)).hb;
/*     */           }
/* 554 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 555 */           index += 10;
/* 556 */           if (index <= RelicLibrary.shopList.size() - 1)
/*     */           {
/*     */ 
/* 559 */             Gdx.input.setCursorPosition(
/* 560 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cX, Settings.HEIGHT - 
/* 561 */               (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cY);
/* 562 */             this.controllerRelicHb = ((AbstractRelic)RelicLibrary.shopList.get(index)).hb;
/*     */           }
/* 564 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 565 */           index--;
/* 566 */           if (index < 0) {
/* 567 */             index = RelicLibrary.shopList.size() - 1;
/*     */           }
/* 569 */           Gdx.input.setCursorPosition(
/* 570 */             (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cX, Settings.HEIGHT - 
/* 571 */             (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cY);
/* 572 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.shopList.get(index)).hb;
/* 573 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 574 */           index++;
/* 575 */           if (index > RelicLibrary.shopList.size() - 1) {
/* 576 */             index = 0;
/*     */           }
/* 578 */           Gdx.input.setCursorPosition(
/* 579 */             (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cX, Settings.HEIGHT - 
/* 580 */             (int)((AbstractRelic)RelicLibrary.shopList.get(index)).hb.cY);
/* 581 */           this.controllerRelicHb = ((AbstractRelic)RelicLibrary.shopList.get(index)).hb;
/*     */         }
/*     */         
/*     */         break;
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling()
/*     */   {
/* 592 */     int y = InputHelper.mY;
/*     */     
/* 594 */     if (!this.grabbedScreen) {
/* 595 */       if (InputHelper.scrolledDown) {
/* 596 */         this.targetY += Settings.SCROLL_SPEED;
/* 597 */       } else if (InputHelper.scrolledUp) {
/* 598 */         this.targetY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 601 */       if (InputHelper.justClickedLeft) {
/* 602 */         this.grabbedScreen = true;
/* 603 */         this.grabStartY = (y - this.targetY);
/*     */       }
/*     */     }
/* 606 */     else if (InputHelper.isMouseDown) {
/* 607 */       this.targetY = (y - this.grabStartY);
/*     */     } else {
/* 609 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 613 */     this.scrollY = MathHelper.scrollSnapLerpSpeed(this.scrollY, this.targetY);
/* 614 */     resetScrolling();
/* 615 */     updateBarPosition();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 622 */     if (this.targetY < this.scrollLowerBound) {
/* 623 */       this.targetY = MathHelper.scrollSnapLerpSpeed(this.targetY, this.scrollLowerBound);
/* 624 */     } else if (this.targetY > this.scrollUpperBound) {
/* 625 */       this.targetY = MathHelper.scrollSnapLerpSpeed(this.targetY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateList(ArrayList<AbstractRelic> list) {
/* 630 */     for (AbstractRelic r : list) {
/* 631 */       r.hb.move(r.currentX, r.currentY);
/* 632 */       r.update();
/*     */       
/* 634 */       if (r.hb.hovered)
/*     */       {
/* 636 */         this.hoveredRelic = r;
/* 637 */         this.relicGroup = list;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 643 */     this.row = -1;
/* 644 */     this.col = 0;
/* 645 */     renderList(sb, TEXT[1], TEXT[2], RelicLibrary.starterList);
/* 646 */     renderList(sb, TEXT[3], TEXT[4], RelicLibrary.commonList);
/* 647 */     renderList(sb, TEXT[5], TEXT[6], RelicLibrary.uncommonList);
/* 648 */     renderList(sb, TEXT[7], TEXT[8], RelicLibrary.rareList);
/* 649 */     renderList(sb, TEXT[9], TEXT[10], RelicLibrary.bossList);
/* 650 */     renderList(sb, TEXT[11], TEXT[12], RelicLibrary.specialList);
/* 651 */     renderList(sb, TEXT[13], TEXT[14], RelicLibrary.shopList);
/*     */     
/* 653 */     this.button.render(sb);
/* 654 */     this.scrollBar.render(sb);
/*     */   }
/*     */   
/*     */   private void renderList(SpriteBatch sb, String msg, String desc, ArrayList<AbstractRelic> list) {
/* 658 */     this.row += 2;
/* 659 */     FontHelper.renderSmartText(sb, FontHelper.buttonLabelFont, msg, START_X - 50.0F * Settings.scale, this.scrollY + 4.0F * Settings.scale - SPACE * this.row, 99999.0F, 0.0F, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 668 */     FontHelper.renderSmartText(sb, FontHelper.cardDescFont_N, desc, START_X - 50.0F * Settings.scale + 
/*     */     
/*     */ 
/*     */ 
/* 672 */       FontHelper.getSmartWidth(FontHelper.buttonLabelFont, msg, 99999.0F, 0.0F), this.scrollY - 0.0F * Settings.scale - SPACE * this.row, 99999.0F, 0.0F, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 678 */     this.row += 1;
/* 679 */     this.col = 0;
/* 680 */     for (AbstractRelic r : list) {
/* 681 */       if (this.col == 10) {
/* 682 */         this.col = 0;
/* 683 */         this.row += 1;
/*     */       }
/* 685 */       r.currentX = (START_X + SPACE * this.col);
/* 686 */       r.currentY = (this.scrollY - SPACE * this.row);
/* 687 */       if (RelicLibrary.redList.contains(r)) {
/* 688 */         if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 689 */           r.renderLock(sb, RED_OUTLINE_COLOR);
/*     */         } else {
/* 691 */           r.render(sb, false, RED_OUTLINE_COLOR);
/*     */         }
/* 693 */       } else if (RelicLibrary.greenList.contains(r)) {
/* 694 */         if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 695 */           r.renderLock(sb, GREEN_OUTLINE_COLOR);
/*     */         } else {
/* 697 */           r.render(sb, false, GREEN_OUTLINE_COLOR);
/*     */         }
/* 699 */       } else if (RelicLibrary.blueList.contains(r)) {
/* 700 */         if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 701 */           r.renderLock(sb, BLUE_OUTLINE_COLOR);
/*     */         } else {
/* 703 */           r.render(sb, false, BLUE_OUTLINE_COLOR);
/*     */         }
/*     */       }
/* 706 */       else if (UnlockTracker.isRelicLocked(r.relicId)) {
/* 707 */         r.renderLock(sb, BLACK_OUTLINE_COLOR);
/*     */       } else {
/* 709 */         r.render(sb, false, BLACK_OUTLINE_COLOR);
/*     */       }
/*     */       
/* 712 */       this.col += 1;
/*     */     }
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 718 */     float newPosition = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 719 */     this.scrollY = newPosition;
/* 720 */     this.targetY = newPosition;
/* 721 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 725 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.scrollY);
/* 726 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\compendium\RelicViewScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */