/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.options.ConfirmPopup;
/*     */ import com.megacrit.cardcrawl.screens.options.ConfirmPopup.ConfirmType;
/*     */ import java.util.HashMap;
/*     */ 
/*     */ public class DungeonTransitionScreen
/*     */ {
/*  21 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DungeonTransitionScreen");
/*  22 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  24 */   public boolean isComplete = false; public boolean msgCreated = false; public boolean isFading = false;
/*     */   public float timer;
/*     */   public String name;
/*     */   public String levelNum;
/*     */   public String levelName;
/*  29 */   private String source; private boolean playSFX = false;
/*  30 */   private ConfirmPopup popup = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  37 */   private Color color = Settings.CREAM_COLOR.cpy(); private Color lvlColor = Settings.BLUE_TEXT_COLOR.cpy();
/*  38 */   private float oscillateTimer; private float continueFader; private float animTimer = 0.0F;
/*  39 */   private Color continueColor = Settings.GOLD_COLOR.cpy();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DungeonTransitionScreen(String key)
/*     */   {
/*  50 */     if (!((Boolean)TipTracker.tips.get("NO_FTUE")).booleanValue()) {
/*  51 */       this.popup = new ConfirmPopup(TEXT[0], TEXT[1], ConfirmPopup.ConfirmType.SKIP_FTUE);
/*  52 */       this.popup.show();
/*     */     }
/*     */     
/*  55 */     this.source = "";
/*  56 */     this.name = "";
/*     */     
/*     */ 
/*  59 */     this.timer = 2.0F;
/*  60 */     this.continueFader = 0.0F;
/*  61 */     this.oscillateTimer = 0.0F;
/*  62 */     this.continueColor.a = 0.0F;
/*  63 */     this.lvlColor.a = 0.0F;
/*  64 */     this.color.a = 0.0F;
/*  65 */     setAreaName(key);
/*  66 */     this.isComplete = true;
/*     */   }
/*     */   
/*     */   private void setAreaName(String key) {
/*  70 */     switch (key) {
/*     */     case "Exordium": 
/*  72 */       this.levelNum = TEXT[2];
/*  73 */       this.levelName = TEXT[3];
/*  74 */       break;
/*     */     case "TheCity": 
/*  76 */       this.levelNum = TEXT[4];
/*  77 */       this.levelName = TEXT[5];
/*  78 */       break;
/*     */     case "TheBeyond": 
/*  80 */       this.levelNum = TEXT[6];
/*  81 */       this.levelName = TEXT[7];
/*  82 */       break;
/*     */     default: 
/*  84 */       this.levelNum = TEXT[8];
/*  85 */       this.levelName = TEXT[9];
/*     */     }
/*     */     
/*  88 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.name = this.levelName;
/*  89 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.levelNum = this.levelNum;
/*     */   }
/*     */   
/*     */   private void oscillateColor() {
/*  93 */     this.oscillateTimer += Gdx.graphics.getDeltaTime() * 5.0F;
/*  94 */     this.continueColor.a = (0.33F + (MathUtils.cos(this.oscillateTimer) + 1.0F) / 3.0F);
/*     */     
/*  96 */     if (!this.isFading) {
/*  97 */       if (this.continueFader != 1.0F) {
/*  98 */         this.continueFader += Gdx.graphics.getDeltaTime() / 2.0F;
/*  99 */         if (this.continueFader > 1.0F) {
/* 100 */           this.continueFader = 1.0F;
/*     */         }
/*     */       }
/*     */     }
/* 104 */     else if (this.continueFader != 0.0F) {
/* 105 */       this.continueFader -= Gdx.graphics.getDeltaTime();
/* 106 */       if (this.continueFader < 0.0F) {
/* 107 */         this.continueFader = 0.0F;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 112 */     this.continueColor.a *= this.continueFader;
/*     */   }
/*     */   
/*     */   public void update() {
/* 116 */     if ((this.popup != null) && 
/* 117 */       (this.popup.shown)) {
/* 118 */       this.popup.update();
/* 119 */       return;
/*     */     }
/*     */     
/*     */ 
/* 123 */     if (this.msgCreated) {
/* 124 */       oscillateColor();
/*     */     }
/*     */     
/* 127 */     if ((Settings.isDebug) || (InputHelper.justClickedLeft)) {
/* 128 */       InputHelper.justClickedLeft = false;
/* 129 */       this.isComplete = true;
/*     */     }
/*     */     
/* 132 */     if (this.isFading) {
/* 133 */       this.timer -= Gdx.graphics.getDeltaTime();
/* 134 */       if (this.timer < 0.0F) {
/* 135 */         this.isComplete = true;
/*     */       } else {
/* 137 */         this.color.a = this.timer;
/* 138 */         return;
/*     */       }
/*     */     }
/*     */     
/* 142 */     if ((this.animTimer > 0.5F) && (!this.playSFX)) {
/* 143 */       this.playSFX = true;
/* 144 */       CardCrawlGame.sound.play("DUNGEON_TRANSITION");
/*     */     }
/*     */     
/* 147 */     if (!this.msgCreated) {
/* 148 */       this.animTimer += Gdx.graphics.getDeltaTime();
/* 149 */       if (this.animTimer > 4.0F) {
/* 150 */         this.msgCreated = true;
/* 151 */         this.animTimer = 4.0F;
/*     */       }
/*     */       
/* 154 */       if (this.animTimer > 2.0F) {
/* 155 */         this.color.a = 1.0F;
/*     */       } else {
/* 157 */         this.color.a = (this.animTimer / 2.0F);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 163 */     this.lvlColor.a = this.color.a;
/* 164 */     FontHelper.renderFontCentered(sb, FontHelper.panelNameTitleFont, this.levelNum, Settings.WIDTH / 2.0F - 44.0F * Settings.scale, Settings.HEIGHT * 0.54F, this.lvlColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 172 */     FontHelper.renderFontCentered(sb, FontHelper.dungeonTitleFont, this.levelName, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, this.color, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 181 */     FontHelper.renderFontCentered(sb, FontHelper.tipBodyFont, "\"" + this.source + "\"", Settings.WIDTH / 2.0F, Settings.HEIGHT * 0.44F, this.color);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 189 */     FontHelper.renderFontCenteredWidth(sb, FontHelper.tipBodyFont, TEXT[10], Settings.WIDTH / 2.0F, 100.0F * Settings.scale, this.continueColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 197 */     if ((this.popup != null) && (this.popup.shown)) {
/* 198 */       this.popup.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DungeonTransitionScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */