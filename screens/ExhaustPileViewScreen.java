/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ExhaustPileViewScreen
/*     */ {
/*  21 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ExhaustViewScreen");
/*  22 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  24 */   private CardGroup exhaustPileCopy = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/*  25 */   public boolean isHovered = false;
/*     */   private static final int CARDS_PER_LINE = 5;
/*  27 */   private boolean grabbedScreen = false;
/*  28 */   private float grabStartY = 0.0F; private float currentDiffY = 0.0F;
/*     */   private static float drawStartX;
/*  30 */   private static float drawStartY; private static float padX; private static float padY; private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  31 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  32 */   private static final String DESC = TEXT[0];
/*  33 */   private AbstractCard hoveredCard = null;
/*  34 */   private int prevDeckSize = 0;
/*     */   
/*     */   public ExhaustPileViewScreen() {
/*  37 */     drawStartX = Settings.WIDTH;
/*  38 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  39 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  40 */     drawStartX /= 2.0F;
/*  41 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*     */     
/*  43 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  44 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*     */   }
/*     */   
/*     */   public void update() {
/*  48 */     updateScrolling();
/*  49 */     updatePositions();
/*     */   }
/*     */   
/*     */   private void updateScrolling() {
/*  53 */     int y = InputHelper.mY;
/*     */     
/*  55 */     if (!this.grabbedScreen) {
/*  56 */       if (InputHelper.scrolledDown) {
/*  57 */         this.currentDiffY += Settings.SCROLL_SPEED;
/*  58 */       } else if (InputHelper.scrolledUp) {
/*  59 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/*  62 */       if (InputHelper.justClickedLeft) {
/*  63 */         this.grabbedScreen = true;
/*  64 */         this.grabStartY = (y - this.currentDiffY);
/*     */       }
/*     */     }
/*  67 */     else if (InputHelper.isMouseDown) {
/*  68 */       this.currentDiffY = (y - this.grabStartY);
/*     */     } else {
/*  70 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/*  74 */     if (this.prevDeckSize != this.exhaustPileCopy.size()) {
/*  75 */       calculateScrollBounds();
/*     */     }
/*  77 */     resetScrolling();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/*  84 */     int scrollTmp = 0;
/*  85 */     if (this.exhaustPileCopy.size() > 10) {
/*  86 */       scrollTmp = this.exhaustPileCopy.size() / 5 - 2;
/*  87 */       if (this.exhaustPileCopy.size() % 5 != 0) {
/*  88 */         scrollTmp++;
/*     */       }
/*  90 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/*  92 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/*     */     
/*  95 */     this.prevDeckSize = this.exhaustPileCopy.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 102 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 103 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 104 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 105 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updatePositions()
/*     */   {
/* 113 */     this.hoveredCard = null;
/* 114 */     int lineNum = 0;
/* 115 */     ArrayList<AbstractCard> cards = this.exhaustPileCopy.group;
/* 116 */     for (int i = 0; i < cards.size(); i++) {
/* 117 */       int mod = i % 5;
/* 118 */       if ((mod == 0) && (i != 0)) {
/* 119 */         lineNum++;
/*     */       }
/* 121 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 122 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 123 */       ((AbstractCard)cards.get(i)).update();
/* 124 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/* 125 */       if (((AbstractCard)cards.get(i)).hb.hovered) {
/* 126 */         this.hoveredCard = ((AbstractCard)cards.get(i));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen() {
/* 132 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */   }
/*     */   
/*     */   public void open() {
/* 136 */     CardCrawlGame.sound.play("DECK_OPEN");
/* 137 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 138 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/* 139 */     this.currentDiffY = 0.0F;
/* 140 */     this.grabStartY = 0.0F;
/* 141 */     this.grabbedScreen = false;
/* 142 */     AbstractDungeon.isScreenUp = true;
/* 143 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.EXHAUST_VIEW;
/* 144 */     this.exhaustPileCopy.clear();
/*     */     
/* 146 */     for (AbstractCard c : AbstractDungeon.player.exhaustPile.group) {
/* 147 */       AbstractCard toAdd = c.makeStatEquivalentCopy();
/* 148 */       toAdd.setAngle(0.0F, true);
/* 149 */       toAdd.targetDrawScale = 0.75F;
/* 150 */       toAdd.targetDrawScale = 0.75F;
/* 151 */       toAdd.drawScale = 0.75F;
/* 152 */       toAdd.lighten(true);
/* 153 */       this.exhaustPileCopy.addToBottom(toAdd);
/*     */     }
/*     */     
/* 156 */     if (!AbstractDungeon.player.hasRelic("Frozen Eye")) {
/* 157 */       this.exhaustPileCopy.sortAlphabetically(true);
/* 158 */       this.exhaustPileCopy.sortByRarityPlusStatusCardType(true);
/*     */     }
/* 160 */     hideCards();
/*     */     
/* 162 */     if (this.exhaustPileCopy.group.size() <= 5) {
/* 163 */       drawStartY = Settings.HEIGHT * 0.5F;
/*     */     } else {
/* 165 */       drawStartY = Settings.HEIGHT * 0.66F;
/*     */     }
/*     */     
/* 168 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */   private void hideCards() {
/* 172 */     int lineNum = 0;
/* 173 */     ArrayList<AbstractCard> cards = this.exhaustPileCopy.group;
/* 174 */     for (int i = 0; i < cards.size(); i++) {
/* 175 */       int mod = i % 5;
/* 176 */       if ((mod == 0) && (i != 0)) {
/* 177 */         lineNum++;
/*     */       }
/* 179 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 180 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY - MathUtils.random(100.0F * Settings.scale, 200.0F * Settings.scale));
/*     */       
/*     */ 
/* 183 */       ((AbstractCard)cards.get(i)).targetDrawScale = 0.75F;
/* 184 */       ((AbstractCard)cards.get(i)).drawScale = 0.75F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 189 */     if (this.hoveredCard == null) {
/* 190 */       this.exhaustPileCopy.render(sb);
/*     */     } else {
/* 192 */       this.exhaustPileCopy.renderExceptOneCard(sb, this.hoveredCard);
/* 193 */       this.hoveredCard.renderHoverShadow(sb);
/* 194 */       this.hoveredCard.render(sb);
/* 195 */       this.hoveredCard.renderCardTip(sb);
/*     */     }
/*     */     
/* 198 */     FontHelper.renderDeckViewTip(sb, DESC, 96.0F * Settings.scale, Settings.CREAM_COLOR);
/* 199 */     AbstractDungeon.overlayMenu.exhaustPanel.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\ExhaustPileViewScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */