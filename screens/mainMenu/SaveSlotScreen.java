/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.ui.panels.DeleteSaveConfirmPopup;
/*     */ import com.megacrit.cardcrawl.ui.panels.RenamePopup;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SaveSlotScreen
/*     */ {
/*  26 */   private static final Logger logger = LogManager.getLogger(SaveSlotScreen.class.getName());
/*  27 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SaveSlotScreen");
/*  28 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  30 */   private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  31 */   public Color uiColor = new Color(1.0F, 0.965F, 0.886F, 0.0F);
/*  32 */   public boolean shown = false;
/*  33 */   private ArrayList<SaveSlot> slots = new ArrayList();
/*  34 */   private MenuCancelButton cancelButton = new MenuCancelButton();
/*     */   
/*     */ 
/*  37 */   private RenamePopup renamePopup = new RenamePopup();
/*  38 */   private DeleteSaveConfirmPopup deletePopup = new DeleteSaveConfirmPopup();
/*  39 */   public CurrentPopup curPop = CurrentPopup.NONE;
/*     */   
/*     */   public static enum CurrentPopup {
/*  42 */     DELETE,  RENAME,  NONE;
/*     */     
/*     */     private CurrentPopup() {} }
/*     */   
/*  46 */   public void update() { this.deletePopup.update();
/*  47 */     this.renamePopup.update();
/*     */     
/*  49 */     updateColors();
/*     */     
/*  51 */     switch (this.curPop) {
/*     */     case DELETE: 
/*     */       break;
/*     */     case NONE: 
/*  55 */       if (this.shown) {
/*  56 */         updateSaveSlots();
/*  57 */         updateControllerInput();
/*     */       }
/*     */       
/*  60 */       updateCancelButton();
/*  61 */       break;
/*     */     case RENAME: 
/*     */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void updateCancelButton()
/*     */   {
/*  70 */     this.cancelButton.update();
/*  71 */     if ((this.cancelButton.hb.clicked) || ((!this.cancelButton.isHidden) && (InputActionSet.cancel.isJustPressed()))) {
/*  72 */       this.cancelButton.hb.clicked = false;
/*  73 */       if (!((SaveSlot)this.slots.get(CardCrawlGame.saveSlot)).emptySlot) {
/*  74 */         confirm(CardCrawlGame.saveSlot);
/*     */       } else {
/*  76 */         for (int i = 0; i < 3; i++) {
/*  77 */           if (!((SaveSlot)this.slots.get(i)).emptySlot) {
/*  78 */             confirm(i);
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/*  86 */     if ((!Settings.isControllerMode) || (this.slots.isEmpty())) {
/*  87 */       return;
/*     */     }
/*     */     
/*  90 */     boolean anyHovered = false;
/*     */     
/*  92 */     int index = 0;
/*  93 */     for (SaveSlot slot : this.slots) {
/*  94 */       if (slot.slotHb.hovered) {
/*  95 */         anyHovered = true;
/*  96 */         break;
/*     */       }
/*  98 */       index++;
/*     */     }
/*     */     
/* 101 */     if (!anyHovered) {
/* 102 */       CInputHelper.setCursor(((SaveSlot)this.slots.get(0)).slotHb);
/*     */     } else {
/* 104 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 105 */         index--;
/* 106 */         if (index < 0) {
/* 107 */           index = 2;
/*     */         }
/* 109 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 110 */         index++;
/* 111 */         if (index > 2) {
/* 112 */           index = 0;
/*     */         }
/*     */       }
/*     */       
/* 116 */       CInputHelper.setCursor(((SaveSlot)this.slots.get(index)).slotHb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateColors() {
/* 121 */     if (this.shown) {
/* 122 */       this.screenColor.a = MathHelper.fadeLerpSnap(this.screenColor.a, 0.75F);
/* 123 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 1.0F);
/*     */     } else {
/* 125 */       this.screenColor.a = MathHelper.fadeLerpSnap(this.screenColor.a, 0.0F);
/* 126 */       this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateSaveSlots() {
/* 131 */     for (SaveSlot slot : this.slots) {
/* 132 */       slot.update();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void open(String curName)
/*     */   {
/* 139 */     if (this.slots.isEmpty()) {
/* 140 */       this.slots.add(new SaveSlot(0));
/* 141 */       this.slots.add(new SaveSlot(1));
/* 142 */       this.slots.add(new SaveSlot(2));
/* 143 */       SaveSlot.uiColor = this.uiColor;
/*     */     }
/*     */     
/* 146 */     this.shown = true;
/* 147 */     for (SaveSlot s : this.slots) {
/* 148 */       if (!s.emptySlot) {
/* 149 */         this.cancelButton.show(com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen.TEXT[5]);
/* 150 */         break;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void rename(int slot, String name) {
/* 156 */     ((SaveSlot)this.slots.get(slot)).setName(name);
/*     */   }
/*     */   
/*     */   public void confirm(int slot)
/*     */   {
/* 161 */     this.shown = false;
/* 162 */     CardCrawlGame.saveSlot = slot;
/* 163 */     CardCrawlGame.playerName = ((SaveSlot)this.slots.get(slot)).getName();
/* 164 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.MAIN_MENU;
/* 165 */     this.cancelButton.hide();
/*     */     
/*     */ 
/* 168 */     if (CardCrawlGame.saveSlotPref.getInteger("DEFAULT_SLOT", -1) != slot) {
/* 169 */       logger.info("Default slot updated: " + slot);
/* 170 */       CardCrawlGame.saveSlotPref.putInteger("DEFAULT_SLOT", slot);
/* 171 */       CardCrawlGame.reloadPrefs();
/* 172 */       CardCrawlGame.saveSlotPref.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 177 */     sb.setColor(this.screenColor);
/* 178 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 179 */     if (this.shown) {
/* 180 */       renderSaveSlots(sb);
/* 181 */       this.deletePopup.render(sb);
/* 182 */       this.renamePopup.render(sb);
/* 183 */       if (this.curPop == CurrentPopup.NONE) {
/* 184 */         this.cancelButton.render(sb);
/* 185 */         renderSelectSlotMessage(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderSelectSlotMessage(SpriteBatch sb)
/*     */   {
/* 192 */     boolean showingTip = false;
/* 193 */     for (SaveSlot s : this.slots) {
/* 194 */       if (s.renameHb.hovered) {
/* 195 */         FontHelper.renderFontCentered(sb, FontHelper.topPanelAmountFont, TEXT[1], Settings.WIDTH / 2.0F, 80.0F * Settings.scale, Settings.BLUE_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 202 */         showingTip = true;
/* 203 */         break;
/*     */       }
/*     */       
/* 206 */       if (s.deleteHb.hovered) {
/* 207 */         FontHelper.renderFontCentered(sb, FontHelper.topPanelAmountFont, TEXT[2], Settings.WIDTH / 2.0F, 80.0F * Settings.scale, Settings.RED_TEXT_COLOR);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 214 */         showingTip = true;
/* 215 */         break;
/*     */       }
/*     */     }
/*     */     
/* 219 */     if (!showingTip) {
/* 220 */       FontHelper.renderFontCentered(sb, FontHelper.topPanelAmountFont, TEXT[0], Settings.WIDTH / 2.0F, 80.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 229 */     if (Settings.isControllerMode) {
/* 230 */       sb.setColor(Color.WHITE);
/* 231 */       sb.draw(CInputActionSet.select
/* 232 */         .getKeyImg(), Settings.WIDTH / 2.0F - 
/* 233 */         FontHelper.getSmartWidth(FontHelper.topPanelAmountFont, TEXT[0], 99999.0F, 0.0F) / 2.0F - 32.0F - 48.0F * Settings.scale, 80.0F * Settings.scale - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderSaveSlots(SpriteBatch sb)
/*     */   {
/* 253 */     sb.setColor(this.uiColor);
/* 254 */     for (SaveSlot slot : this.slots) {
/* 255 */       slot.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void openDeletePopup(int index) {
/* 260 */     this.deletePopup.open(index);
/* 261 */     this.curPop = CurrentPopup.DELETE;
/*     */   }
/*     */   
/*     */   public void deleteSlot(int index) {
/* 265 */     CardCrawlGame.saveSlotPref.putString(com.megacrit.cardcrawl.helpers.SaveHelper.slotName("PROFILE_NAME", index), "");
/* 266 */     ((SaveSlot)this.slots.get(index)).clear();
/*     */   }
/*     */   
/*     */   public void openRenamePopup(int index, boolean newSave) {
/* 270 */     this.renamePopup.open(index, newSave);
/* 271 */     this.curPop = CurrentPopup.RENAME;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\SaveSlotScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */