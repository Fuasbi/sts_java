/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ 
/*     */ public class ScrollBar
/*     */ {
/*     */   public ScrollBarListener sliderListener;
/*  14 */   public boolean isBackgroundVisible = true;
/*     */   
/*     */   private Hitbox sliderHb;
/*     */   
/*     */   private float currentScrollPercent;
/*     */   private boolean isDragging;
/*  20 */   public static final float TRACK_W = 54.0F * Settings.scale;
/*  21 */   private final float TRACK_CAP_HEIGHT = TRACK_W;
/*  22 */   private final float CURSOR_W = 38.0F * Settings.scale;
/*  23 */   private final float CURSOR_H = 60.0F * Settings.scale;
/*  24 */   private final float DRAW_BORDER = this.CURSOR_H / 4.0F;
/*     */   
/*  26 */   private float cursorDrawPosition = 0.0F;
/*     */   
/*     */   public ScrollBar(ScrollBarListener listener) {
/*  29 */     this(listener, Settings.WIDTH - 150.0F * Settings.scale - TRACK_W / 2.0F, Settings.HEIGHT / 2.0F, Settings.HEIGHT - 256.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  34 */     this.cursorDrawPosition = getYPositionForPercent(0.0F);
/*     */   }
/*     */   
/*     */   public ScrollBar(ScrollBarListener listener, float x, float y, float height) {
/*  38 */     this.sliderListener = listener;
/*  39 */     this.currentScrollPercent = 0.0F;
/*  40 */     this.isDragging = false;
/*     */     
/*  42 */     this.sliderHb = new Hitbox(TRACK_W, height);
/*  43 */     this.sliderHb.move(x, y);
/*  44 */     reset();
/*     */   }
/*     */   
/*     */   public void setCenter(float x, float y) {
/*  48 */     this.sliderHb.move(x, y);
/*  49 */     reset();
/*     */   }
/*     */   
/*     */   public void move(float xOffset, float yOffset) {
/*  53 */     this.sliderHb.move(this.sliderHb.cX + xOffset, this.sliderHb.cY + yOffset);
/*  54 */     reset();
/*     */   }
/*     */   
/*     */   public void changeHeight(float newHeight) {
/*  58 */     this.sliderHb.height = newHeight;
/*  59 */     this.sliderHb.move(this.sliderHb.cX, this.sliderHb.cY);
/*  60 */     reset();
/*     */   }
/*     */   
/*     */   public float width() {
/*  64 */     return this.sliderHb.width;
/*     */   }
/*     */   
/*     */   public void reset() {
/*  68 */     this.cursorDrawPosition = getYPositionForPercent(0.0F);
/*     */   }
/*     */   
/*     */   public boolean update() {
/*  72 */     this.sliderHb.update();
/*  73 */     if ((this.sliderHb.hovered) && (InputHelper.isMouseDown)) {
/*  74 */       this.isDragging = true;
/*     */     }
/*  76 */     if ((this.isDragging) && (InputHelper.justReleasedClickLeft)) {
/*  77 */       this.isDragging = false;
/*  78 */       return true;
/*     */     }
/*  80 */     if (this.isDragging) {
/*  81 */       float newPercent = getPercentFromY(InputHelper.mY);
/*  82 */       this.sliderListener.scrolledUsingBar(newPercent);
/*  83 */       return true;
/*     */     }
/*     */     
/*  86 */     return false;
/*     */   }
/*     */   
/*     */   public void parentScrolledToPercent(float percent) {
/*  90 */     this.currentScrollPercent = boundedPercent(percent);
/*     */   }
/*     */   
/*     */   private float getPercentFromY(float y) {
/*  94 */     float minY = this.sliderHb.y + this.sliderHb.height - this.DRAW_BORDER;
/*  95 */     float maxY = this.sliderHb.y + this.DRAW_BORDER;
/*  96 */     return boundedPercent(MathHelper.percentFromValueBetween(minY, maxY, y));
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 100 */     Color previousColor = sb.getColor();
/* 101 */     sb.setColor(Color.WHITE);
/*     */     
/* 103 */     if (this.isBackgroundVisible) {
/* 104 */       renderTrack(sb);
/*     */     }
/* 106 */     renderCursor(sb);
/* 107 */     this.sliderHb.render(sb);
/* 108 */     sb.setColor(previousColor);
/*     */   }
/*     */   
/*     */   private float getYPositionForPercent(float percent) {
/* 112 */     float topY = this.sliderHb.y + this.sliderHb.height - this.CURSOR_H + this.DRAW_BORDER;
/* 113 */     float bottomY = this.sliderHb.y - this.DRAW_BORDER;
/* 114 */     return MathHelper.valueFromPercentBetween(topY, bottomY, boundedPercent(percent));
/*     */   }
/*     */   
/*     */   private void renderCursor(SpriteBatch sb) {
/* 118 */     float x = this.sliderHb.cX - this.CURSOR_W / 2.0F;
/* 119 */     float yForPercent = getYPositionForPercent(this.currentScrollPercent);
/*     */     
/* 121 */     this.cursorDrawPosition = MathHelper.scrollSnapLerpSpeed(this.cursorDrawPosition, yForPercent);
/* 122 */     sb.draw(ImageMaster.SCROLL_BAR_TRAIN, x, this.cursorDrawPosition, this.CURSOR_W, this.CURSOR_H);
/*     */   }
/*     */   
/*     */   private void renderTrack(SpriteBatch sb) {
/* 126 */     sb.draw(ImageMaster.SCROLL_BAR_MIDDLE, this.sliderHb.x, this.sliderHb.y, this.sliderHb.width, this.sliderHb.height);
/* 127 */     sb.draw(ImageMaster.SCROLL_BAR_TOP, this.sliderHb.x, this.sliderHb.y + this.sliderHb.height, this.sliderHb.width, this.TRACK_CAP_HEIGHT);
/* 128 */     sb.draw(ImageMaster.SCROLL_BAR_BOTTOM, this.sliderHb.x, this.sliderHb.y - this.TRACK_CAP_HEIGHT, this.sliderHb.width, this.TRACK_CAP_HEIGHT);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private float boundedPercent(float percent)
/*     */   {
/* 137 */     return Math.max(0.0F, Math.min(percent, 1.0F));
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\ScrollBar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */