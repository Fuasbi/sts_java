package com.megacrit.cardcrawl.screens.mainMenu;

public abstract interface SortHeaderButtonListener
{
  public abstract void didChangeOrder(SortHeaderButton paramSortHeaderButton, boolean paramBoolean);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\SortHeaderButtonListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */