/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.credits.CreditsScreen;
/*     */ import com.megacrit.cardcrawl.daily.DailyScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.scenes.TitleBackground;
/*     */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*     */ import com.megacrit.cardcrawl.screens.charSelect.CharacterOption;
/*     */ import com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen;
/*     */ import com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen;
/*     */ import com.megacrit.cardcrawl.screens.compendium.RelicViewScreen;
/*     */ import com.megacrit.cardcrawl.screens.custom.CustomModeScreen;
/*     */ import com.megacrit.cardcrawl.screens.leaderboards.LeaderboardScreen;
/*     */ import com.megacrit.cardcrawl.screens.options.AbandonConfirmPopup;
/*     */ import com.megacrit.cardcrawl.screens.options.InputSettingsScreen;
/*     */ import com.megacrit.cardcrawl.screens.options.OptionsPanel;
/*     */ import com.megacrit.cardcrawl.screens.runHistory.RunHistoryScreen;
/*     */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ConfirmButton;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class MainMenuScreen
/*     */ {
/*  48 */   private static final Logger logger = LogManager.getLogger(MainMenuScreen.class.getName());
/*  49 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("MainMenuScreen");
/*  50 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  53 */   private static final String VERSION_INFO = TEXT[0] + CardCrawlGame.VERSION_NUM;
/*  54 */   private Hitbox nameEditHb = new Hitbox(400.0F * Settings.scale, 100.0F * Settings.scale);
/*     */   public String newName;
/*  56 */   public boolean darken = false;
/*  57 */   public com.badlogic.gdx.graphics.Texture saveSlotImg = null;
/*     */   
/*     */ 
/*  60 */   public Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*     */   public static final float OVERLAY_ALPHA = 0.8F;
/*  62 */   private Color overlayColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  63 */   public boolean fadedOut = false; public boolean isFadingOut = false;
/*  64 */   public long windId = 0L;
/*     */   
/*     */ 
/*  67 */   private CharSelectInfo charInfo = null;
/*     */   
/*     */ 
/*  70 */   public TitleBackground bg = new TitleBackground();
/*     */   
/*     */ 
/*  73 */   private EarlyAccessPopup eaPopup = new EarlyAccessPopup();
/*  74 */   public CurScreen screen = CurScreen.MAIN_MENU;
/*  75 */   public SaveSlotScreen saveSlotScreen = new SaveSlotScreen();
/*  76 */   public MenuPanelScreen panelScreen = new MenuPanelScreen();
/*  77 */   public StatsScreen statsScreen = new StatsScreen();
/*  78 */   public DailyScreen dailyScreen = new DailyScreen();
/*  79 */   public CardLibraryScreen cardLibraryScreen = new CardLibraryScreen();
/*  80 */   public LeaderboardScreen leaderboardsScreen = new LeaderboardScreen();
/*  81 */   public RelicViewScreen relicScreen = new RelicViewScreen();
/*  82 */   public CreditsScreen creditsScreen = new CreditsScreen();
/*  83 */   public PatchNotesScreen patchNotesScreen = new PatchNotesScreen();
/*     */   public RunHistoryScreen runHistoryScreen;
/*  85 */   public CharacterSelectScreen charSelectScreen = new CharacterSelectScreen();
/*  86 */   public CustomModeScreen customModeScreen = new CustomModeScreen();
/*  87 */   public AbandonConfirmPopup abandonPopup = new AbandonConfirmPopup();
/*  88 */   public InputSettingsScreen inputSettingsScreen = new InputSettingsScreen();
/*  89 */   public OptionsPanel optionPanel = new OptionsPanel();
/*  90 */   public boolean isSettingsUp = false;
/*     */   
/*     */ 
/*  93 */   public ConfirmButton confirmButton = new ConfirmButton(TEXT[1]);
/*  94 */   public MenuCancelButton cancelButton = new MenuCancelButton();
/*     */   
/*     */ 
/*  97 */   private Hitbox deckHb = new Hitbox(-1000.0F, 0.0F, 400.0F * Settings.scale, 80.0F * Settings.scale);
/*     */   
/*  99 */   public ArrayList<MenuButton> buttons = new ArrayList();
/* 100 */   public boolean abandonedRun = false;
/*     */   
/*     */   public static enum CurScreen {
/* 103 */     CHAR_SELECT,  RELIC_VIEW,  BANNER_DECK_VIEW,  DAILY,  TRIALS,  SETTINGS,  MAIN_MENU,  SAVE_SLOT,  STATS,  RUN_HISTORY,  CARD_LIBRARY,  CREDITS,  PATCH_NOTES,  NONE,  LEADERBOARD,  ABANDON_CONFIRM,  PANEL_MENU,  INPUT_SETTINGS,  CUSTOM;
/*     */     
/*     */     private CurScreen() {} }
/*     */   
/* 107 */   public MainMenuScreen() { this(true); }
/*     */   
/*     */   public MainMenuScreen(boolean playBgm)
/*     */   {
/* 111 */     if ((Settings.isDemo) && (Settings.isShowBuild)) {
/* 112 */       TipTracker.reset();
/*     */     }
/*     */     
/* 115 */     if (playBgm) {
/* 116 */       CardCrawlGame.music.changeBGM("MENU");
/* 117 */       if (Settings.AMBIANCE_ON) {
/* 118 */         this.windId = CardCrawlGame.sound.playAndLoop("WIND");
/*     */       } else {
/* 120 */         this.windId = CardCrawlGame.sound.playAndLoop("WIND", 0.0F);
/*     */       }
/*     */     }
/*     */     
/* 124 */     com.megacrit.cardcrawl.unlock.UnlockTracker.refresh();
/* 125 */     this.cardLibraryScreen.initialize();
/* 126 */     this.charSelectScreen.initialize();
/* 127 */     this.confirmButton.hide();
/* 128 */     updateAmbienceVolume();
/* 129 */     SaveAndContinue.checkForSaves();
/* 130 */     this.nameEditHb.move(200.0F * Settings.scale, Settings.HEIGHT - 50.0F * Settings.scale);
/*     */     
/* 132 */     setMainMenuButtons();
/* 133 */     this.runHistoryScreen = new RunHistoryScreen();
/*     */   }
/*     */   
/*     */   private void setMainMenuButtons() {
/* 137 */     this.buttons.clear();
/* 138 */     int index = 0;
/*     */     
/* 140 */     this.buttons.add(new MenuButton(MenuButton.ClickResult.QUIT, index++));
/* 141 */     this.buttons.add(new MenuButton(MenuButton.ClickResult.PATCH_NOTES, index++));
/* 142 */     this.buttons.add(new MenuButton(MenuButton.ClickResult.SETTINGS, index++));
/*     */     
/* 144 */     if ((!Settings.isShowBuild) && 
/* 145 */       (this.statsScreen.statScreenUnlocked())) {
/* 146 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.STAT, index++));
/* 147 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.INFO, index++));
/*     */     }
/*     */     
/*     */ 
/* 151 */     if ((SaveAndContinue.ironcladSaveExists) || (SaveAndContinue.silentSaveExists) || (SaveAndContinue.crowbotSaveExists) || (SaveAndContinue.defectSaveExists))
/*     */     {
/* 153 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.ABANDON_RUN, index++));
/* 154 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.RESUME_GAME, index++));
/*     */     } else {
/* 156 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.PLAY, index++));
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 162 */     if (this.isFadingOut) {
/* 163 */       InputHelper.justClickedLeft = false;
/* 164 */       InputHelper.justReleasedClickLeft = false;
/* 165 */       InputHelper.justClickedRight = false;
/* 166 */       InputHelper.justReleasedClickRight = false;
/*     */     }
/*     */     
/* 169 */     this.abandonPopup.update();
/*     */     
/* 171 */     if (this.abandonedRun) {
/* 172 */       this.abandonedRun = false;
/* 173 */       this.buttons.remove(this.buttons.size() - 1);
/* 174 */       this.buttons.remove(this.buttons.size() - 1);
/* 175 */       this.buttons.add(new MenuButton(MenuButton.ClickResult.PLAY, this.buttons.size()));
/*     */     }
/*     */     
/* 178 */     if ((this.eaPopup != null) && (EarlyAccessPopup.isUp)) {
/* 179 */       this.eaPopup.update();
/* 180 */       InputHelper.justClickedLeft = false;
/*     */     } else {
/* 182 */       this.eaPopup = null;
/*     */     }
/*     */     
/* 185 */     if ((Settings.isInfo) && (com.megacrit.cardcrawl.helpers.input.DevInputActionSet.deleteSteamCloud.isJustPressed())) {
/* 186 */       SteamSaveSync.deleteAllCloudFiles(SteamSaveSync.getAllCloudFiles());
/* 187 */       logger.info("[STEAM] Deleted all Cloud Files");
/*     */     }
/*     */     
/* 190 */     this.cancelButton.update();
/* 191 */     updateSettings();
/* 192 */     if (this.screen != CurScreen.SAVE_SLOT) {
/* 193 */       for (MenuButton b : this.buttons) {
/* 194 */         b.update();
/*     */       }
/*     */     }
/*     */     
/* 198 */     switch (this.screen) {
/*     */     case CHAR_SELECT: 
/* 200 */       updateCharSelectController();
/* 201 */       this.charSelectScreen.update();
/* 202 */       break;
/*     */     case CARD_LIBRARY: 
/* 204 */       this.cardLibraryScreen.update();
/* 205 */       break;
/*     */     case CUSTOM: 
/* 207 */       this.customModeScreen.update();
/* 208 */       break;
/*     */     case PANEL_MENU: 
/* 210 */       updateMenuPanelController();
/* 211 */       this.panelScreen.update();
/* 212 */       break;
/*     */     case DAILY: 
/* 214 */       this.dailyScreen.update();
/* 215 */       break;
/*     */     case BANNER_DECK_VIEW: 
/*     */       break;
/*     */     case MAIN_MENU: 
/* 219 */       updateMenuButtonController();
/* 220 */       break;
/*     */     case LEADERBOARD: 
/* 222 */       this.leaderboardsScreen.update();
/* 223 */       break;
/*     */     case RELIC_VIEW: 
/* 225 */       this.relicScreen.update();
/* 226 */       break;
/*     */     case SAVE_SLOT: 
/*     */       break;
/*     */     case SETTINGS: 
/*     */       break;
/*     */     case TRIALS: 
/*     */       break;
/*     */     case STATS: 
/* 234 */       this.statsScreen.update();
/* 235 */       break;
/*     */     case CREDITS: 
/* 237 */       this.creditsScreen.update();
/* 238 */       break;
/*     */     case PATCH_NOTES: 
/* 240 */       this.patchNotesScreen.update();
/* 241 */       break;
/*     */     case RUN_HISTORY: 
/* 243 */       this.runHistoryScreen.update();
/* 244 */       break;
/*     */     case INPUT_SETTINGS: 
/* 246 */       this.inputSettingsScreen.update();
/* 247 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 252 */     this.saveSlotScreen.update();
/* 253 */     this.bg.update();
/*     */     
/* 255 */     if (this.darken) {
/* 256 */       this.screenColor.a = MathHelper.popLerpSnap(this.screenColor.a, 0.8F);
/*     */     } else {
/* 258 */       this.screenColor.a = MathHelper.popLerpSnap(this.screenColor.a, 0.0F);
/*     */     }
/*     */     
/* 261 */     if (!this.statsScreen.screenUp) {
/* 262 */       updateRenameArea();
/*     */     }
/*     */     
/* 265 */     if ((this.charInfo != null) && (this.charInfo.resumeGame)) {
/* 266 */       this.deckHb.update();
/* 267 */       if (this.deckHb.justHovered) {
/* 268 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*     */     }
/* 271 */     if (!this.isFadingOut) {
/* 272 */       handleInput();
/*     */     }
/* 274 */     fadeOut();
/*     */   }
/*     */   
/*     */   private void updateMenuButtonController() {
/* 278 */     if ((!Settings.isControllerMode) || (EarlyAccessPopup.isUp)) {
/* 279 */       return;
/*     */     }
/*     */     
/* 282 */     boolean anyHovered = false;
/* 283 */     int index = 0;
/* 284 */     for (MenuButton b : this.buttons) {
/* 285 */       if (b.hb.hovered) {
/* 286 */         anyHovered = true;
/* 287 */         break;
/*     */       }
/* 289 */       index++;
/*     */     }
/*     */     
/*     */ 
/* 293 */     if (anyHovered) {
/* 294 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 295 */         index--;
/* 296 */         if (index < 0) {
/* 297 */           index = this.buttons.size() - 1;
/*     */         }
/* 299 */         CInputHelper.setCursor(((MenuButton)this.buttons.get(index)).hb);
/* 300 */       } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 301 */         index++;
/* 302 */         if (index > this.buttons.size() - 1) {
/* 303 */           index = 0;
/*     */         }
/* 305 */         CInputHelper.setCursor(((MenuButton)this.buttons.get(index)).hb);
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 310 */       index = this.buttons.size() - 1;
/* 311 */       CInputHelper.setCursor(((MenuButton)this.buttons.get(index)).hb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateCharSelectController() {
/* 316 */     if ((!Settings.isControllerMode) || (this.isFadingOut)) {
/* 317 */       return;
/*     */     }
/*     */     
/* 320 */     boolean anyHovered = false;
/* 321 */     int index = 0;
/* 322 */     for (CharacterOption b : this.charSelectScreen.options) {
/* 323 */       if (b.hb.hovered) {
/* 324 */         anyHovered = true;
/* 325 */         break;
/*     */       }
/* 327 */       index++;
/*     */     }
/*     */     
/* 330 */     if (!anyHovered) {
/* 331 */       index = 0;
/* 332 */       Gdx.input.setCursorPosition(
/* 333 */         (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cX, Settings.HEIGHT - 
/* 334 */         (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cY);
/* 335 */       ((CharacterOption)this.charSelectScreen.options.get(index)).hb.clicked = true;
/*     */     }
/*     */     else {
/* 338 */       if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 339 */         index--;
/* 340 */         if (index < 0) {
/* 341 */           index = this.charSelectScreen.options.size() - 1;
/*     */         }
/*     */         
/* 344 */         Gdx.input.setCursorPosition(
/* 345 */           (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cX, Settings.HEIGHT - 
/* 346 */           (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cY);
/* 347 */         ((CharacterOption)this.charSelectScreen.options.get(index)).hb.clicked = true;
/* 348 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 349 */         index++;
/* 350 */         if (index > this.charSelectScreen.options.size() - 1) {
/* 351 */           index = 0;
/*     */         }
/*     */         
/* 354 */         Gdx.input.setCursorPosition(
/* 355 */           (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cX, Settings.HEIGHT - 
/* 356 */           (int)((CharacterOption)this.charSelectScreen.options.get(index)).hb.cY);
/* 357 */         ((CharacterOption)this.charSelectScreen.options.get(index)).hb.clicked = true;
/*     */       }
/*     */       
/* 360 */       if (((CharacterOption)this.charSelectScreen.options.get(index)).locked) {
/* 361 */         this.charSelectScreen.confirmButton.hide();
/*     */       } else {
/* 363 */         this.charSelectScreen.confirmButton.show();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMenuPanelController() {
/* 369 */     if (!Settings.isControllerMode) {
/* 370 */       return;
/*     */     }
/*     */     
/* 373 */     boolean anyHovered = false;
/* 374 */     int index = 0;
/* 375 */     for (MainMenuPanelButton b : this.panelScreen.panels) {
/* 376 */       if (b.hb.hovered) {
/* 377 */         anyHovered = true;
/* 378 */         break;
/*     */       }
/* 380 */       index++;
/*     */     }
/*     */     
/*     */ 
/* 384 */     if (anyHovered) {
/* 385 */       if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 386 */         index--;
/* 387 */         if (index < 0) {
/* 388 */           index = this.panelScreen.panels.size() - 1;
/*     */         }
/*     */         
/* 391 */         if (((MainMenuPanelButton)this.panelScreen.panels.get(index)).pColor == MainMenuPanelButton.PanelColor.GRAY) {
/* 392 */           index--;
/*     */         }
/*     */         
/* 395 */         Gdx.input.setCursorPosition(
/* 396 */           (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cX, Settings.HEIGHT - 
/* 397 */           (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cY);
/* 398 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 399 */         index++;
/* 400 */         if (index > this.panelScreen.panels.size() - 1) {
/* 401 */           index = 0;
/*     */         }
/* 403 */         if (((MainMenuPanelButton)this.panelScreen.panels.get(index)).pColor == MainMenuPanelButton.PanelColor.GRAY) {
/* 404 */           index = 0;
/*     */         }
/*     */         
/* 407 */         Gdx.input.setCursorPosition(
/* 408 */           (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cX, Settings.HEIGHT - 
/* 409 */           (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cY);
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 414 */       index = 0;
/* 415 */       Gdx.input.setCursorPosition(
/* 416 */         (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cX, Settings.HEIGHT - 
/* 417 */         (int)((MainMenuPanelButton)this.panelScreen.panels.get(index)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateSettings() {
/* 422 */     if (this.saveSlotScreen.shown) {
/* 423 */       return;
/*     */     }
/*     */     
/* 426 */     if ((!EarlyAccessPopup.isUp) && (InputHelper.pressedEscape) && (this.screen == CurScreen.MAIN_MENU) && (!this.isFadingOut)) {
/* 427 */       if (!this.isSettingsUp) {
/* 428 */         com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/* 429 */         CardCrawlGame.sound.play("END_TURN");
/* 430 */         this.isSettingsUp = true;
/* 431 */         darken();
/* 432 */         InputHelper.pressedEscape = false;
/* 433 */         this.statsScreen.hide();
/* 434 */         this.dailyScreen.hide();
/* 435 */         this.cancelButton.hide();
/* 436 */         CardCrawlGame.cancelButton.show(TEXT[2]);
/* 437 */         this.screen = CurScreen.SETTINGS;
/* 438 */         this.panelScreen.panels.clear();
/* 439 */         hideMenuButtons();
/*     */       }
/* 441 */       else if (!EarlyAccessPopup.isUp) {
/* 442 */         this.isSettingsUp = false;
/* 443 */         CardCrawlGame.cancelButton.hide();
/* 444 */         this.screen = CurScreen.MAIN_MENU;
/* 445 */         if (this.screen == CurScreen.MAIN_MENU) {
/* 446 */           this.cancelButton.hide();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 452 */     if (this.isSettingsUp) {
/* 453 */       this.optionPanel.update();
/*     */     }
/*     */     
/* 456 */     CardCrawlGame.cancelButton.update();
/*     */   }
/*     */   
/*     */   private void updateRenameArea() {
/* 460 */     if (this.screen == CurScreen.MAIN_MENU) {
/* 461 */       this.nameEditHb.update();
/*     */     }
/*     */     
/* 464 */     if (((this.nameEditHb.hovered) && (InputHelper.justClickedLeft)) || (CInputActionSet.map.isJustPressed())) {
/* 465 */       InputHelper.justClickedLeft = false;
/* 466 */       this.nameEditHb.hovered = false;
/* 467 */       this.saveSlotScreen.open(CardCrawlGame.playerName);
/* 468 */       this.screen = CurScreen.SAVE_SLOT;
/*     */     }
/*     */     
/* 471 */     if ((this.bg.slider <= 0.1F) && (CardCrawlGame.saveSlotPref.getInteger("DEFAULT_SLOT", -1) == -1) && (this.screen == CurScreen.MAIN_MENU))
/*     */     {
/* 473 */       if (!setDefaultSlot()) {
/* 474 */         logger.info("No saves detected, opening Save Slot screen automatically.");
/* 475 */         CardCrawlGame.playerPref.putBoolean("ftuePopupShown", true);
/* 476 */         this.saveSlotScreen.open(CardCrawlGame.playerName);
/* 477 */         this.screen = CurScreen.SAVE_SLOT;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private boolean setDefaultSlot() {
/* 483 */     if (!CardCrawlGame.playerPref.getString("name", "").equals("")) {
/* 484 */       logger.info("Migration to Save Slot schema detected, setting DEFAULT_SLOT to 0.");
/* 485 */       CardCrawlGame.saveSlot = 0;
/* 486 */       CardCrawlGame.saveSlotPref.putInteger("DEFAULT_SLOT", 0);
/* 487 */       CardCrawlGame.saveSlotPref.flush();
/* 488 */       return true;
/*     */     }
/* 490 */     return false;
/*     */   }
/*     */   
/*     */   private void handleInput() {
/* 494 */     this.confirmButton.update();
/*     */   }
/*     */   
/*     */   public void fadeOutMusic() {
/* 498 */     CardCrawlGame.music.fadeOutBGM();
/* 499 */     if (Settings.AMBIANCE_ON) {
/* 500 */       CardCrawlGame.sound.fadeOut("WIND", this.windId);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 505 */     this.bg.render(sb);
/* 506 */     this.cancelButton.render(sb);
/* 507 */     renderNameEdit(sb);
/*     */     
/* 509 */     for (MenuButton b : this.buttons) {
/* 510 */       b.render(sb);
/*     */     }
/*     */     
/* 513 */     this.abandonPopup.render(sb);
/*     */     
/* 515 */     sb.setColor(this.screenColor);
/* 516 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 518 */     if (this.isFadingOut) {
/* 519 */       this.confirmButton.update();
/*     */     }
/*     */     
/* 522 */     if (this.screen == CurScreen.CHAR_SELECT) {
/* 523 */       this.charSelectScreen.render(sb);
/*     */     }
/*     */     
/* 526 */     sb.setColor(this.overlayColor);
/* 527 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 528 */     renderSettings(sb);
/*     */     
/* 530 */     this.confirmButton.render(sb);
/*     */     
/* 532 */     if (CardCrawlGame.displayVersion) {
/* 533 */       FontHelper.renderSmartText(sb, FontHelper.cardDescFont_N, VERSION_INFO, 20.0F * Settings.scale - 700.0F * this.bg.slider, 30.0F * Settings.scale, 10000.0F, 32.0F * Settings.scale, new Color(1.0F, 1.0F, 1.0F, 0.3F));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 544 */     switch (this.screen) {
/*     */     case CARD_LIBRARY: 
/* 546 */       this.cardLibraryScreen.render(sb);
/* 547 */       break;
/*     */     case CUSTOM: 
/* 549 */       this.customModeScreen.render(sb);
/* 550 */       break;
/*     */     case PANEL_MENU: 
/* 552 */       this.panelScreen.render(sb);
/* 553 */       break;
/*     */     case DAILY: 
/* 555 */       this.dailyScreen.render(sb);
/* 556 */       sb.setColor(this.overlayColor);
/* 557 */       sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 558 */       break;
/*     */     case BANNER_DECK_VIEW: 
/*     */       break;
/*     */     case MAIN_MENU: 
/*     */       break;
/*     */     case RELIC_VIEW: 
/* 564 */       this.relicScreen.render(sb);
/* 565 */       break;
/*     */     case SAVE_SLOT: 
/*     */       break;
/*     */     case SETTINGS: 
/*     */       break;
/*     */     case TRIALS: 
/*     */       break;
/*     */     case LEADERBOARD: 
/* 573 */       this.leaderboardsScreen.render(sb);
/* 574 */       break;
/*     */     case STATS: 
/* 576 */       this.statsScreen.render(sb);
/* 577 */       break;
/*     */     case RUN_HISTORY: 
/* 579 */       this.runHistoryScreen.render(sb);
/* 580 */       break;
/*     */     case INPUT_SETTINGS: 
/* 582 */       this.inputSettingsScreen.render(sb);
/* 583 */       break;
/*     */     case CREDITS: 
/* 585 */       this.creditsScreen.render(sb);
/* 586 */       break;
/*     */     case PATCH_NOTES: 
/* 588 */       this.patchNotesScreen.render(sb);
/* 589 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/* 595 */     this.saveSlotScreen.render(sb);
/* 596 */     if (this.eaPopup != null) {
/* 597 */       this.eaPopup.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderSettings(SpriteBatch sb) {
/* 602 */     if ((this.isSettingsUp) && (this.screen == CurScreen.SETTINGS)) {
/* 603 */       this.optionPanel.render(sb);
/*     */     }
/* 605 */     CardCrawlGame.cancelButton.render(sb);
/*     */   }
/*     */   
/*     */   private void renderNameEdit(SpriteBatch sb) {
/* 609 */     if (!this.nameEditHb.hovered) {
/* 610 */       FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, CardCrawlGame.playerName, 100.0F * Settings.scale - 500.0F * this.bg.slider, Settings.HEIGHT - 24.0F * Settings.scale, 1000.0F, 30.0F * Settings.scale, Color.GOLD);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 620 */       FontHelper.renderSmartText(sb, FontHelper.cardTitleFont_N, CardCrawlGame.playerName, 100.0F * Settings.scale - 500.0F * this.bg.slider, Settings.HEIGHT - 24.0F * Settings.scale, 1000.0F, 30.0F * Settings.scale, Settings.GREEN_TEXT_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 631 */     if (!Settings.isControllerMode) {
/* 632 */       FontHelper.renderSmartText(sb, FontHelper.cardDescFont_N, TEXT[3], 100.0F * Settings.scale - 500.0F * this.bg.slider, Settings.HEIGHT - 60.0F * Settings.scale, 1000.0F, 30.0F * Settings.scale, Color.SKY);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 642 */       sb.draw(CInputActionSet.map
/* 643 */         .getKeyImg(), -32.0F + 120.0F * Settings.scale - 500.0F * this.bg.slider, -32.0F + Settings.HEIGHT - 78.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.8F, Settings.scale * 0.8F, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 659 */       FontHelper.renderSmartText(sb, FontHelper.cardDescFont_N, TEXT[4], 150.0F * Settings.scale - 500.0F * this.bg.slider, Settings.HEIGHT - 70.0F * Settings.scale, 1000.0F, 30.0F * Settings.scale, Color.SKY);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 670 */     sb.draw(
/* 671 */       CardCrawlGame.getSaveSlotImg(), 50.0F * Settings.scale - 50.0F - 500.0F * this.bg.slider, Settings.HEIGHT - 50.0F * Settings.scale - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 100, 100, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 688 */     this.nameEditHb.render(sb);
/*     */   }
/*     */   
/*     */   private void fadeOut() {
/* 692 */     if ((this.isFadingOut) && (!this.fadedOut)) {
/* 693 */       this.overlayColor.a += Gdx.graphics.getDeltaTime();
/* 694 */       if (this.overlayColor.a > 1.0F) {
/* 695 */         this.overlayColor.a = 1.0F;
/* 696 */         this.fadedOut = true;
/*     */       }
/*     */     }
/* 699 */     else if (this.overlayColor.a != 0.0F) {
/* 700 */       this.overlayColor.a -= Gdx.graphics.getDeltaTime() * 2.0F;
/* 701 */       if (this.overlayColor.a < 0.0F) {
/* 702 */         this.overlayColor.a = 0.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateAmbienceVolume()
/*     */   {
/* 709 */     if (Settings.AMBIANCE_ON) {
/* 710 */       CardCrawlGame.sound.adjustVolume("WIND", this.windId);
/*     */     } else {
/* 712 */       CardCrawlGame.sound.adjustVolume("WIND", this.windId, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void muteAmbienceVolume() {
/* 717 */     if (Settings.AMBIANCE_ON) {
/* 718 */       CardCrawlGame.sound.adjustVolume("WIND", this.windId, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void unmuteAmbienceVolume() {
/* 723 */     CardCrawlGame.sound.adjustVolume("WIND", this.windId);
/*     */   }
/*     */   
/*     */   public void darken() {
/* 727 */     this.darken = true;
/*     */   }
/*     */   
/*     */   public void lighten() {
/* 731 */     this.darken = false;
/*     */   }
/*     */   
/*     */   public void hideMenuButtons() {
/* 735 */     for (MenuButton b : this.buttons) {
/* 736 */       b.hide();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\MainMenuScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */