package com.megacrit.cardcrawl.screens.mainMenu;

public abstract interface TabBarListener
{
  public abstract void didChangeTab(ColorTabBar paramColorTabBar, ColorTabBar.CurrentTab paramCurrentTab);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\TabBarListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */