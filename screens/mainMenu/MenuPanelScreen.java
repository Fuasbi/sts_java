/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.stats.CharStat;
/*     */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MenuPanelScreen
/*     */ {
/*  18 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("MenuPanels");
/*  19 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  21 */   public ArrayList<MainMenuPanelButton> panels = new ArrayList();
/*     */   private PanelScreen screen;
/*  23 */   private static final float PANEL_Y = Settings.HEIGHT / 2.0F;
/*  24 */   public MenuCancelButton button = new MenuCancelButton();
/*     */   
/*     */   public static enum PanelScreen {
/*  27 */     PLAY,  COMPENDIUM,  STATS,  SETTINGS;
/*     */     
/*     */     private PanelScreen() {} }
/*     */   
/*  31 */   public void open(PanelScreen screenType) { CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.PANEL_MENU;
/*  32 */     this.screen = screenType;
/*  33 */     initializePanels();
/*  34 */     this.button.show(com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen.TEXT[5]);
/*  35 */     CardCrawlGame.mainMenuScreen.darken();
/*     */   }
/*     */   
/*     */ 
/*     */   public void hide() {}
/*     */   
/*     */   private void initializePanels()
/*     */   {
/*  43 */     this.panels.clear();
/*  44 */     switch (this.screen) {
/*     */     case PLAY: 
/*  46 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.PLAY_NORMAL, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F - 450.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  53 */       if (CardCrawlGame.mainMenuScreen.statsScreen.statScreenUnlocked()) {
/*  54 */         this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.PLAY_DAILY, MainMenuPanelButton.PanelColor.BEIGE, Settings.WIDTH / 2.0F, PANEL_Y));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*  61 */         this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.PLAY_DAILY, MainMenuPanelButton.PanelColor.GRAY, Settings.WIDTH / 2.0F, PANEL_Y));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  69 */       if (StatsScreen.all.highestDaily > 0) {
/*  70 */         this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.PLAY_CUSTOM, MainMenuPanelButton.PanelColor.RED, Settings.WIDTH / 2.0F + 450.0F * Settings.scale, PANEL_Y));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*  77 */         this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.PLAY_CUSTOM, MainMenuPanelButton.PanelColor.GRAY, Settings.WIDTH / 2.0F + 450.0F * Settings.scale, PANEL_Y));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  84 */       break;
/*     */     case COMPENDIUM: 
/*  86 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.INFO_CARD, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F - 225.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  92 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.INFO_RELIC, MainMenuPanelButton.PanelColor.RED, Settings.WIDTH / 2.0F + 225.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 104 */       break;
/*     */     case STATS: 
/* 106 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.STAT_CHAR, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F - 450.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 112 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.STAT_LEADERBOARDS, MainMenuPanelButton.PanelColor.BEIGE, Settings.WIDTH / 2.0F, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 118 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.STAT_HISTORY, MainMenuPanelButton.PanelColor.RED, Settings.WIDTH / 2.0F + 450.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 124 */       break;
/*     */     case SETTINGS: 
/* 126 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.SETTINGS_GAME, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F - 450.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 132 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.SETTINGS_INPUT, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */       this.panels.add(new MainMenuPanelButton(MainMenuPanelButton.PanelClickResult.SETTINGS_CREDITS, MainMenuPanelButton.PanelColor.BLUE, Settings.WIDTH / 2.0F + 450.0F * Settings.scale, PANEL_Y));
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 144 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 151 */     this.button.update();
/*     */     
/* 153 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/* 154 */       InputHelper.pressedEscape = false;
/* 155 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.MAIN_MENU;
/* 156 */       this.button.hide();
/* 157 */       CardCrawlGame.mainMenuScreen.lighten();
/*     */     }
/*     */     
/* 160 */     for (MainMenuPanelButton panel : this.panels) {
/* 161 */       panel.update();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 166 */     for (MainMenuPanelButton panel : this.panels) {
/* 167 */       panel.render(sb);
/*     */     }
/*     */     
/* 170 */     this.button.render(sb);
/*     */   }
/*     */   
/*     */   public void refresh() {
/* 174 */     this.button.hideInstantly();
/* 175 */     this.button.show(com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen.TEXT[5]);
/* 176 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.PANEL_MENU;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\MenuPanelScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */