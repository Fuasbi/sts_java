/*    */ package com.megacrit.cardcrawl.screens.mainMenu;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*    */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*    */ 
/*    */ public class SortHeaderButton
/*    */ {
/*    */   public Hitbox hb;
/* 16 */   private boolean isAscending = false;
/*    */   private String text;
/*    */   public SortHeaderButtonListener delegate;
/* 19 */   private final float ARROW_SIZE = 32.0F;
/*    */   
/*    */   public SortHeaderButton(String text, float cx, float cy) {
/* 22 */     this.hb = new Hitbox(210.0F * Settings.scale, 48.0F * Settings.scale);
/* 23 */     this.hb.move(cx, cy);
/* 24 */     this.text = text;
/*    */   }
/*    */   
/*    */   public SortHeaderButton(String text, float cx, float cy, SortHeaderButtonListener delegate) {
/* 28 */     this(text, cx, cy);
/* 29 */     this.delegate = delegate;
/*    */   }
/*    */   
/*    */   public void update() {
/* 33 */     this.hb.update();
/* 34 */     if (this.hb.justHovered) {
/* 35 */       CardCrawlGame.sound.playA("UI_HOVER", -0.3F);
/*    */     }
/*    */     
/* 38 */     if ((this.hb.hovered) && (InputHelper.justClickedLeft)) {
/* 39 */       this.hb.clickStarted = true;
/*    */     }
/*    */     
/* 42 */     if ((this.hb.clicked) || ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/* 43 */       this.hb.clicked = false;
/* 44 */       this.isAscending = (!this.isAscending);
/* 45 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.2F);
/* 46 */       this.delegate.didChangeOrder(this, this.isAscending);
/*    */     }
/*    */   }
/*    */   
/*    */   public void updateScrollPosition(float newY) {
/* 51 */     this.hb.move(this.hb.cX, newY);
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {
/* 55 */     com.badlogic.gdx.graphics.Color color = this.hb.hovered ? Settings.GOLD_COLOR : Settings.CREAM_COLOR;
/* 56 */     FontHelper.renderFontCentered(sb, FontHelper.topPanelInfoFont, this.text, this.hb.cX, this.hb.cY, color);
/* 57 */     sb.setColor(color);
/* 58 */     float arrowCenterOffset = 16.0F;
/*    */     
/* 60 */     float textWidth = FontHelper.getSmartWidth(FontHelper.topPanelInfoFont, this.text, Float.MAX_VALUE, 0.0F);
/* 61 */     float arrowOffset = textWidth / 2.0F + 16.0F * Settings.scale;
/*    */     
/* 63 */     sb.draw(ImageMaster.FILTER_ARROW, this.hb.cX - 16.0F + arrowOffset, this.hb.cY - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 32, 32, false, !this.isAscending);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 81 */     this.hb.render(sb);
/*    */   }
/*    */   
/*    */   public void reset() {
/* 85 */     this.isAscending = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\SortHeaderButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */