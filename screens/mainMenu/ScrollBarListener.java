package com.megacrit.cardcrawl.screens.mainMenu;

public abstract interface ScrollBarListener
{
  public abstract void scrolledUsingBar(float paramFloat);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\ScrollBarListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */