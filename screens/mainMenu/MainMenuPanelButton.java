/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.screens.charSelect.CharacterSelectScreen;
/*     */ import com.megacrit.cardcrawl.screens.options.InputSettingsScreen;
/*     */ import com.megacrit.cardcrawl.screens.runHistory.RunHistoryScreen;
/*     */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*     */ 
/*     */ public class MainMenuPanelButton
/*     */ {
/*  21 */   public Hitbox hb = new Hitbox(400.0F * Settings.scale, 700.0F * Settings.scale);
/*     */   private PanelClickResult result;
/*     */   public PanelColor pColor;
/*  24 */   private Color gColor = Settings.GOLD_COLOR.cpy();
/*  25 */   private Color cColor = Settings.CREAM_COLOR.cpy();
/*  26 */   private Color wColor = Color.WHITE.cpy();
/*  27 */   private Color grColor = Color.GRAY.cpy();
/*  28 */   private Texture portraitImg = null;
/*  29 */   private Texture panelImg = ImageMaster.MENU_PANEL_BG_BLUE;
/*     */   private static final int W = 512;
/*  31 */   private static final int H = 800; private static final int P_W = 317; private static final int P_H = 206; private String header = null; private String description = null;
/*     */   private float yMod;
/*  33 */   private float animTimer; private float animTime; private static final float START_Y = -100.0F * Settings.scale;
/*  34 */   private float uiScale = 1.0F;
/*     */   
/*     */   public static enum PanelClickResult {
/*  37 */     PLAY_NORMAL,  PLAY_DAILY,  PLAY_CUSTOM,  INFO_CARD,  INFO_RELIC,  INFO_ENEMY,  STAT_CHAR,  STAT_LEADERBOARDS,  STAT_HISTORY,  SETTINGS_GAME,  SETTINGS_INPUT,  SETTINGS_CREDITS;
/*     */     
/*     */     private PanelClickResult() {} }
/*     */   
/*  41 */   public static enum PanelColor { RED,  BLUE,  BEIGE,  GRAY;
/*     */     
/*     */     private PanelColor() {} }
/*     */   
/*  45 */   public MainMenuPanelButton(PanelClickResult setResult, PanelColor setColor, float x, float y) { this.result = setResult;
/*  46 */     this.pColor = setColor;
/*  47 */     this.hb.move(x, y);
/*  48 */     setLabel();
/*  49 */     this.animTime = com.badlogic.gdx.math.MathUtils.random(0.2F, 0.35F);
/*  50 */     this.animTimer = this.animTime;
/*     */   }
/*     */   
/*     */   private void setLabel() {
/*  54 */     this.panelImg = ImageMaster.MENU_PANEL_BG_BEIGE;
/*  55 */     switch (this.result) {
/*     */     case PLAY_CUSTOM: 
/*  57 */       this.header = MenuPanelScreen.TEXT[39];
/*  58 */       if (this.pColor == PanelColor.GRAY) {
/*  59 */         this.description = MenuPanelScreen.TEXT[37];
/*  60 */         this.panelImg = ImageMaster.MENU_PANEL_BG_GRAY;
/*     */       } else {
/*  62 */         this.description = MenuPanelScreen.TEXT[40];
/*  63 */         this.panelImg = ImageMaster.MENU_PANEL_BG_RED;
/*     */       }
/*  65 */       this.portraitImg = ImageMaster.P_LOOP;
/*  66 */       break;
/*     */     case PLAY_DAILY: 
/*  68 */       this.header = MenuPanelScreen.TEXT[3];
/*  69 */       this.description = MenuPanelScreen.TEXT[5];
/*  70 */       this.portraitImg = ImageMaster.P_DAILY;
/*  71 */       if (this.pColor == PanelColor.GRAY) {
/*  72 */         this.panelImg = ImageMaster.MENU_PANEL_BG_GRAY;
/*     */       } else {
/*  74 */         this.panelImg = ImageMaster.MENU_PANEL_BG_BLUE;
/*     */       }
/*  76 */       break;
/*     */     case PLAY_NORMAL: 
/*  78 */       this.header = MenuPanelScreen.TEXT[0];
/*  79 */       this.description = MenuPanelScreen.TEXT[2];
/*  80 */       this.portraitImg = ImageMaster.P_STANDARD;
/*  81 */       break;
/*     */     case INFO_CARD: 
/*  83 */       this.header = MenuPanelScreen.TEXT[9];
/*  84 */       this.description = MenuPanelScreen.TEXT[11];
/*  85 */       this.portraitImg = ImageMaster.P_INFO_CARD;
/*  86 */       break;
/*     */     case INFO_ENEMY: 
/*  88 */       this.header = MenuPanelScreen.TEXT[15];
/*  89 */       this.description = MenuPanelScreen.TEXT[17];
/*  90 */       this.portraitImg = ImageMaster.P_INFO_ENEMY;
/*  91 */       this.panelImg = ImageMaster.MENU_PANEL_BG_GRAY;
/*  92 */       break;
/*     */     case INFO_RELIC: 
/*  94 */       this.header = MenuPanelScreen.TEXT[12];
/*  95 */       this.description = MenuPanelScreen.TEXT[14];
/*  96 */       this.portraitImg = ImageMaster.P_INFO_RELIC;
/*  97 */       this.panelImg = ImageMaster.MENU_PANEL_BG_BLUE;
/*  98 */       break;
/*     */     case STAT_CHAR: 
/* 100 */       this.header = MenuPanelScreen.TEXT[18];
/* 101 */       this.description = MenuPanelScreen.TEXT[20];
/* 102 */       this.portraitImg = ImageMaster.P_STAT_CHAR;
/* 103 */       break;
/*     */     case STAT_HISTORY: 
/* 105 */       this.header = MenuPanelScreen.TEXT[24];
/* 106 */       this.description = MenuPanelScreen.TEXT[26];
/* 107 */       this.portraitImg = ImageMaster.P_STAT_HISTORY;
/* 108 */       this.panelImg = ImageMaster.MENU_PANEL_BG_RED;
/* 109 */       break;
/*     */     case STAT_LEADERBOARDS: 
/* 111 */       this.header = MenuPanelScreen.TEXT[21];
/* 112 */       this.description = MenuPanelScreen.TEXT[23];
/* 113 */       this.portraitImg = ImageMaster.P_STAT_LEADERBOARD;
/* 114 */       this.panelImg = ImageMaster.MENU_PANEL_BG_BLUE;
/* 115 */       break;
/*     */     case SETTINGS_CREDITS: 
/* 117 */       this.header = MenuPanelScreen.TEXT[33];
/* 118 */       this.description = MenuPanelScreen.TEXT[35];
/* 119 */       this.portraitImg = ImageMaster.P_SETTING_CREDITS;
/* 120 */       this.panelImg = ImageMaster.MENU_PANEL_BG_RED;
/* 121 */       break;
/*     */     case SETTINGS_GAME: 
/* 123 */       this.header = MenuPanelScreen.TEXT[27];
/* 124 */       this.description = MenuPanelScreen.TEXT[29];
/* 125 */       this.portraitImg = ImageMaster.P_SETTING_GAME;
/* 126 */       break;
/*     */     case SETTINGS_INPUT: 
/* 128 */       this.header = MenuPanelScreen.TEXT[30];
/* 129 */       this.description = MenuPanelScreen.TEXT[32];
/* 130 */       this.portraitImg = ImageMaster.P_SETTING_INPUT;
/* 131 */       this.panelImg = ImageMaster.MENU_PANEL_BG_BLUE;
/* 132 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 139 */     if (this.pColor != PanelColor.GRAY) {
/* 140 */       this.hb.update();
/*     */     }
/*     */     
/* 143 */     if (this.hb.justHovered) {
/* 144 */       CardCrawlGame.sound.playV("UI_HOVER", 0.5F);
/*     */     }
/*     */     
/* 147 */     if (this.hb.hovered) {
/* 148 */       this.uiScale = MathHelper.fadeLerpSnap(this.uiScale, 1.025F);
/* 149 */       if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/* 150 */         this.hb.clickStarted = true;
/*     */       }
/*     */     } else {
/* 153 */       this.uiScale = MathHelper.cardScaleLerpSnap(this.uiScale, 1.0F);
/*     */     }
/*     */     
/* 156 */     if ((this.hb.hovered) && (CInputActionSet.select.isJustPressed())) {
/* 157 */       this.hb.clicked = true;
/*     */     }
/*     */     
/* 160 */     if (this.hb.clicked) {
/* 161 */       this.hb.clicked = false;
/* 162 */       CardCrawlGame.sound.play("DECK_OPEN");
/* 163 */       CardCrawlGame.mainMenuScreen.panelScreen.hide();
/* 164 */       buttonEffect();
/*     */     }
/*     */     
/* 167 */     animatePanelIn();
/*     */   }
/*     */   
/*     */   private void animatePanelIn() {
/* 171 */     this.animTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 172 */     if (this.animTimer < 0.0F) {
/* 173 */       this.animTimer = 0.0F;
/*     */     }
/* 175 */     this.yMod = com.badlogic.gdx.math.Interpolation.swingIn.apply(0.0F, START_Y, this.animTimer / this.animTime);
/* 176 */     this.wColor.a = (1.0F - this.animTimer / this.animTime);
/* 177 */     this.cColor.a = this.wColor.a;
/* 178 */     this.gColor.a = this.wColor.a;
/* 179 */     this.grColor.a = this.wColor.a;
/*     */   }
/*     */   
/*     */   private void buttonEffect() {
/* 183 */     switch (this.result) {
/*     */     case INFO_CARD: 
/* 185 */       CardCrawlGame.mainMenuScreen.cardLibraryScreen.open();
/* 186 */       break;
/*     */     case INFO_ENEMY: 
/*     */       break;
/*     */     
/*     */     case INFO_RELIC: 
/* 191 */       CardCrawlGame.mainMenuScreen.relicScreen.open();
/* 192 */       break;
/*     */     case PLAY_CUSTOM: 
/* 194 */       CardCrawlGame.mainMenuScreen.customModeScreen.open();
/* 195 */       break;
/*     */     case PLAY_DAILY: 
/* 197 */       CardCrawlGame.mainMenuScreen.dailyScreen.open();
/* 198 */       break;
/*     */     case PLAY_NORMAL: 
/* 200 */       CardCrawlGame.mainMenuScreen.charSelectScreen.open(false);
/* 201 */       break;
/*     */     case SETTINGS_CREDITS: 
/* 203 */       CardCrawlGame.mainMenuScreen.creditsScreen.open();
/* 204 */       break;
/*     */     case SETTINGS_GAME: 
/* 206 */       CardCrawlGame.sound.play("END_TURN");
/* 207 */       CardCrawlGame.mainMenuScreen.isSettingsUp = true;
/* 208 */       com.megacrit.cardcrawl.helpers.input.InputHelper.pressedEscape = false;
/* 209 */       CardCrawlGame.mainMenuScreen.statsScreen.hide();
/* 210 */       CardCrawlGame.mainMenuScreen.cancelButton.hide();
/* 211 */       CardCrawlGame.cancelButton.show(MainMenuScreen.TEXT[2]);
/* 212 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.SETTINGS;
/* 213 */       break;
/*     */     case SETTINGS_INPUT: 
/* 215 */       CardCrawlGame.mainMenuScreen.inputSettingsScreen.open();
/* 216 */       break;
/*     */     case STAT_CHAR: 
/* 218 */       CardCrawlGame.mainMenuScreen.statsScreen.open();
/* 219 */       break;
/*     */     case STAT_HISTORY: 
/* 221 */       CardCrawlGame.mainMenuScreen.runHistoryScreen.open();
/* 222 */       break;
/*     */     case STAT_LEADERBOARDS: 
/* 224 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.open();
/* 225 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 232 */     sb.setColor(this.wColor);
/* 233 */     sb.draw(this.panelImg, this.hb.cX - 256.0F, this.hb.cY + this.yMod - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, this.uiScale * Settings.scale, this.uiScale * Settings.scale, 0.0F, 0, 0, 512, 800, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 251 */     if (this.hb.hovered) {
/* 252 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, (this.uiScale - 1.0F) * 16.0F));
/* 253 */       sb.setBlendFunction(770, 1);
/* 254 */       sb.draw(ImageMaster.MENU_PANEL_BG_BLUE, this.hb.cX - 256.0F, this.hb.cY + this.yMod - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, this.uiScale * Settings.scale, this.uiScale * Settings.scale, 0.0F, 0, 0, 512, 800, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 271 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 274 */     if (this.pColor == PanelColor.GRAY) {
/* 275 */       sb.setColor(this.grColor);
/*     */     } else {
/* 277 */       sb.setColor(this.wColor);
/*     */     }
/* 279 */     sb.draw(this.portraitImg, this.hb.cX - 158.5F, this.hb.cY + this.yMod - 103.0F + 140.0F * Settings.scale, 158.5F, 103.0F, 317.0F, 206.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 317, 206, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 296 */     if (this.pColor == PanelColor.GRAY) {
/* 297 */       sb.setColor(this.wColor);
/* 298 */       sb.draw(ImageMaster.P_LOCK, this.hb.cX - 158.5F, this.hb.cY + this.yMod - 103.0F + 140.0F * Settings.scale, 158.5F, 103.0F, 317.0F, 206.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 317, 206, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 317 */     sb.draw(ImageMaster.MENU_PANEL_FRAME, this.hb.cX - 256.0F, this.hb.cY + this.yMod - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 800, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 336 */     if (FontHelper.getWidth(FontHelper.damageNumberFont, this.header, 0.8F) > 310.0F * Settings.scale) {
/* 337 */       FontHelper.renderFontCenteredHeight(sb, FontHelper.damageNumberFont, this.header, this.hb.cX - 138.0F * Settings.scale, this.hb.cY + this.yMod + 294.0F * Settings.scale, 280.0F * Settings.scale, this.gColor, 0.7F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 347 */       FontHelper.renderFontCenteredHeight(sb, FontHelper.damageNumberFont, this.header, this.hb.cX - 153.0F * Settings.scale, this.hb.cY + this.yMod + 294.0F * Settings.scale, 310.0F * Settings.scale, this.gColor, 0.8F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 359 */     FontHelper.renderFontCenteredHeight(sb, FontHelper.eventBodyText, this.description, this.hb.cX - 153.0F * Settings.scale, this.hb.cY + this.yMod - 130.0F * Settings.scale, 310.0F * Settings.scale, this.cColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 368 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\MainMenuPanelButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */