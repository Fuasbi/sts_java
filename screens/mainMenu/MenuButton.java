/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.scenes.TitleBackground;
/*     */ import com.megacrit.cardcrawl.screens.options.AbandonConfirmPopup;
/*     */ 
/*     */ public class MenuButton
/*     */ {
/*  25 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("MenuButton");
/*  26 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public ClickResult result;
/*     */   private String label;
/*     */   public Hitbox hb;
/*  31 */   private Color tint = Color.WHITE.cpy();
/*  32 */   private Color highlightColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*     */   private int index;
/*  34 */   private boolean isDisabled = false;
/*  35 */   private float x = 0.0F; private float targetX = 0.0F;
/*  36 */   public static final float FONT_X = 120.0F * Settings.scale;
/*  37 */   public static final float START_Y = 120.0F * Settings.scale;
/*  38 */   public static final float SPACE_Y = 50.0F * Settings.scale;
/*  39 */   public static final float FONT_OFFSET_Y = 10.0F * Settings.scale;
/*  40 */   private boolean confirmation = false;
/*  41 */   private com.badlogic.gdx.graphics.Texture highlightImg = null;
/*     */   
/*     */   public static enum ClickResult {
/*  44 */     PLAY,  RESUME_GAME,  ABANDON_RUN,  INFO,  STAT,  SETTINGS,  PATCH_NOTES,  QUIT;
/*     */     
/*     */     private ClickResult() {} }
/*     */   
/*  48 */   public MenuButton(ClickResult r, int index) { if (this.highlightImg == null) {
/*  49 */       this.highlightImg = ImageMaster.loadImage("images/ui/mainMenu/menu_option_highlight.png");
/*     */     }
/*  51 */     this.result = r;
/*  52 */     this.index = index;
/*  53 */     setLabel();
/*     */     
/*     */ 
/*  56 */     this.hb = new Hitbox(FontHelper.getSmartWidth(FontHelper.buttonLabelFont, this.label, 9999.0F, 1.0F) + 100.0F * Settings.scale, SPACE_Y);
/*     */     
/*     */ 
/*  59 */     this.hb.move(this.hb.width / 2.0F + 75.0F * Settings.scale, START_Y + index * SPACE_Y);
/*     */   }
/*     */   
/*     */   private void setLabel() {
/*  63 */     switch (this.result) {
/*     */     case PLAY: 
/*  65 */       this.label = TEXT[1];
/*  66 */       break;
/*     */     case RESUME_GAME: 
/*  68 */       this.label = TEXT[4];
/*  69 */       break;
/*     */     case ABANDON_RUN: 
/*  71 */       this.label = TEXT[10];
/*  72 */       break;
/*     */     case INFO: 
/*  74 */       this.label = TEXT[14];
/*  75 */       break;
/*     */     case STAT: 
/*  77 */       this.label = TEXT[6];
/*  78 */       break;
/*     */     case SETTINGS: 
/*  80 */       this.label = TEXT[12];
/*  81 */       break;
/*     */     case QUIT: 
/*  83 */       this.label = TEXT[8];
/*  84 */       break;
/*     */     case PATCH_NOTES: 
/*  86 */       this.label = TEXT[9];
/*  87 */       break;
/*     */     default: 
/*  89 */       this.label = "ERROR";
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  95 */     if ((CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.MAIN_MENU) && (CardCrawlGame.mainMenuScreen.bg.slider < 0.5F))
/*     */     {
/*  97 */       this.hb.update();
/*     */     }
/*  99 */     this.x = MathHelper.uiLerpSnap(this.x, this.targetX);
/* 100 */     if (this.hb.justHovered) {
/* 101 */       CardCrawlGame.sound.playV("UI_HOVER", 0.75F);
/*     */     }
/* 103 */     if (this.hb.hovered) {
/* 104 */       this.highlightColor.a = 0.9F;
/* 105 */       this.targetX = (25.0F * Settings.scale);
/* 106 */       if (InputHelper.justClickedLeft) {
/* 107 */         CardCrawlGame.sound.playA("UI_CLICK_1", -0.1F);
/* 108 */         this.hb.clickStarted = true;
/*     */       }
/* 110 */       this.tint = Color.WHITE.cpy();
/* 111 */     } else if (CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.MAIN_MENU) {
/* 112 */       this.highlightColor.a = MathHelper.fadeLerpSnap(this.highlightColor.a, 0.0F);
/* 113 */       this.targetX = 0.0F;
/* 114 */       this.tint.r = MathHelper.fadeLerpSnap(this.tint.r, 0.3F);
/* 115 */       this.tint.g = this.tint.r;
/* 116 */       this.tint.b = this.tint.r;
/*     */     }
/*     */     
/* 119 */     if ((this.hb.hovered) && (CInputActionSet.select.isJustPressed())) {
/* 120 */       this.hb.clicked = true;
/* 121 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.1F);
/*     */     }
/*     */     
/* 124 */     if (this.hb.clicked) {
/* 125 */       this.hb.clicked = false;
/* 126 */       buttonEffect();
/* 127 */       CardCrawlGame.mainMenuScreen.hideMenuButtons();
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/* 132 */     this.hb.hovered = false;
/* 133 */     this.targetX = (-1000.0F * Settings.scale + 30.0F * Settings.scale * this.index);
/*     */   }
/*     */   
/*     */   public void buttonEffect() {
/* 137 */     switch (this.result) {
/*     */     case PLAY: 
/* 139 */       CardCrawlGame.mainMenuScreen.panelScreen.open(MenuPanelScreen.PanelScreen.PLAY);
/* 140 */       break;
/*     */     case RESUME_GAME: 
/* 142 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.NONE;
/* 143 */       CardCrawlGame.mainMenuScreen.hideMenuButtons();
/* 144 */       CardCrawlGame.mainMenuScreen.darken();
/* 145 */       resumeGame();
/* 146 */       break;
/*     */     case ABANDON_RUN: 
/* 148 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.ABANDON_CONFIRM;
/* 149 */       CardCrawlGame.mainMenuScreen.abandonPopup.show();
/* 150 */       break;
/*     */     case INFO: 
/* 152 */       CardCrawlGame.mainMenuScreen.panelScreen.open(MenuPanelScreen.PanelScreen.COMPENDIUM);
/* 153 */       break;
/*     */     case STAT: 
/* 155 */       CardCrawlGame.mainMenuScreen.panelScreen.open(MenuPanelScreen.PanelScreen.STATS);
/* 156 */       break;
/*     */     case SETTINGS: 
/* 158 */       CardCrawlGame.mainMenuScreen.panelScreen.open(MenuPanelScreen.PanelScreen.SETTINGS);
/* 159 */       break;
/*     */     case PATCH_NOTES: 
/* 161 */       CardCrawlGame.mainMenuScreen.patchNotesScreen.open();
/* 162 */       break;
/*     */     case QUIT: 
/* 164 */       Gdx.app.exit();
/* 165 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void resumeGame()
/*     */   {
/* 172 */     CardCrawlGame.loadingSave = true;
/*     */     
/* 174 */     if (SaveAndContinue.ironcladSaveExists) {
/* 175 */       CardCrawlGame.chosenCharacter = AbstractPlayer.PlayerClass.IRONCLAD;
/* 176 */     } else if (SaveAndContinue.silentSaveExists) {
/* 177 */       CardCrawlGame.chosenCharacter = AbstractPlayer.PlayerClass.THE_SILENT;
/* 178 */     } else if (SaveAndContinue.defectSaveExists) {
/* 179 */       CardCrawlGame.chosenCharacter = AbstractPlayer.PlayerClass.DEFECT;
/*     */     }
/*     */     
/* 182 */     CardCrawlGame.mainMenuScreen.isFadingOut = true;
/* 183 */     CardCrawlGame.mainMenuScreen.fadeOutMusic();
/* 184 */     Settings.isDailyRun = false;
/* 185 */     Settings.isTrial = false;
/* 186 */     com.megacrit.cardcrawl.daily.DailyMods.setModsFalse();
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 190 */     float lerper = Interpolation.circleIn.apply(CardCrawlGame.mainMenuScreen.bg.slider);
/* 191 */     float sliderX = -1000.0F * Settings.scale * lerper;
/* 192 */     sliderX -= this.index * 250.0F * Settings.scale * lerper;
/* 193 */     if (this.result == ClickResult.ABANDON_RUN) {
/* 194 */       if (this.confirmation) {
/* 195 */         this.label = TEXT[11];
/*     */       } else {
/* 197 */         this.label = TEXT[10];
/*     */       }
/*     */     }
/*     */     
/* 201 */     sb.setBlendFunction(770, 1);
/* 202 */     sb.setColor(this.highlightColor);
/* 203 */     sb.draw(this.highlightImg, this.x + FONT_X + sliderX - 179.0F + 120.0F * Settings.scale, this.hb.cY - 52.0F, 179.0F, 52.0F, 358.0F, 104.0F, Settings.scale, Settings.scale * 0.8F, 0.0F, 0, 0, 358, 104, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 220 */     sb.setBlendFunction(770, 771);
/*     */     
/* 222 */     if (this.isDisabled) {
/* 223 */       FontHelper.renderSmartText(sb, FontHelper.buttonLabelFont, this.label, this.x + FONT_X + sliderX, this.hb.cY + FONT_OFFSET_Y, 9999.0F, 1.0F, Settings.RED_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 233 */       FontHelper.renderSmartText(sb, FontHelper.buttonLabelFont, this.label, this.x + FONT_X + sliderX, this.hb.cY + FONT_OFFSET_Y, 9999.0F, 1.0F, Settings.CREAM_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 243 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\MenuButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */