/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SaveSlot
/*     */ {
/*  23 */   private static final Logger logger = LogManager.getLogger(SaveSlot.class.getName());
/*  24 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SaveSlot");
/*  25 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  28 */   private String name = "";
/*  29 */   private long playtime = 0L;
/*  30 */   private float completionPercentage = 0.0F;
/*  31 */   public boolean emptySlot = false;
/*     */   
/*     */ 
/*  34 */   private int index = 0;
/*  35 */   private com.badlogic.gdx.graphics.Texture iconImg = null;
/*     */   public Hitbox slotHb;
/*  37 */   public Hitbox deleteHb; public Hitbox renameHb; public static Color uiColor = null;
/*     */   
/*     */   public SaveSlot(int slot)
/*     */   {
/*  41 */     if ((slot == CardCrawlGame.saveSlot) && (!CardCrawlGame.playerName.equals(CardCrawlGame.saveSlotPref
/*  42 */       .getString(SaveHelper.slotName("PROFILE_NAME", slot), "")))) {
/*  43 */       logger.info("Migrating from legacy save.");
/*  44 */       migration(slot);
/*     */     }
/*     */     
/*  47 */     this.name = CardCrawlGame.saveSlotPref.getString(SaveHelper.slotName("PROFILE_NAME", slot), "");
/*     */     
/*  49 */     if (this.name.equals("")) {
/*  50 */       this.emptySlot = true;
/*     */     }
/*     */     
/*  53 */     if (!this.emptySlot) {
/*  54 */       loadInfo(slot);
/*     */     }
/*     */     
/*     */ 
/*  58 */     this.index = slot;
/*  59 */     this.slotHb = new Hitbox(800.0F * Settings.scale, 260.0F * Settings.scale);
/*  60 */     this.slotHb.move(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 280.0F * Settings.scale - this.index * 280.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*  64 */     this.renameHb = new Hitbox(90.0F * Settings.scale, 90.0F * Settings.scale);
/*  65 */     this.renameHb.move(1400.0F * Settings.scale, this.slotHb.cY + 44.0F * Settings.scale);
/*     */     
/*  67 */     this.deleteHb = new Hitbox(90.0F * Settings.scale, 90.0F * Settings.scale);
/*  68 */     this.deleteHb.move(1400.0F * Settings.scale, this.renameHb.cY - 91.0F * Settings.scale);
/*     */     
/*     */ 
/*  71 */     switch (this.index) {
/*     */     case 0: 
/*  73 */       this.iconImg = ImageMaster.PROFILE_A;
/*  74 */       break;
/*     */     case 1: 
/*  76 */       this.iconImg = ImageMaster.PROFILE_B;
/*  77 */       break;
/*     */     case 2: 
/*  79 */       this.iconImg = ImageMaster.PROFILE_C;
/*  80 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void loadInfo(int slot)
/*     */   {
/*  87 */     this.completionPercentage = CardCrawlGame.saveSlotPref.getFloat(SaveHelper.slotName("COMPLETION", slot), 0.0F);
/*  88 */     this.playtime = CardCrawlGame.saveSlotPref.getLong(SaveHelper.slotName("PLAYTIME", slot), 0L);
/*     */   }
/*     */   
/*     */   private void migration(int slot) {
/*  92 */     CardCrawlGame.saveSlotPref.putString(SaveHelper.slotName("PROFILE_NAME", slot), CardCrawlGame.playerName);
/*     */     
/*     */ 
/*  95 */     CardCrawlGame.saveSlotPref.putFloat(
/*  96 */       SaveHelper.slotName("COMPLETION", slot), 
/*  97 */       UnlockTracker.getCompletionPercentage());
/*  98 */     this.completionPercentage = CardCrawlGame.saveSlotPref.getFloat(SaveHelper.slotName("COMPLETION", slot), 0.0F);
/*     */     
/*     */ 
/* 101 */     CardCrawlGame.saveSlotPref.putLong(SaveHelper.slotName("PLAYTIME", slot), UnlockTracker.getTotalPlaytime());
/* 102 */     this.playtime = CardCrawlGame.saveSlotPref.getLong(SaveHelper.slotName("PLAYTIME", slot), 0L);
/*     */     
/* 104 */     CardCrawlGame.saveSlotPref.flush();
/*     */   }
/*     */   
/*     */   public void update() {
/* 108 */     if (!this.emptySlot) {
/* 109 */       this.deleteHb.update();
/* 110 */       this.renameHb.update();
/*     */       
/*     */ 
/* 113 */       if ((this.slotHb.hovered) && (CInputActionSet.topPanel.isJustPressed())) {
/* 114 */         CInputActionSet.topPanel.unpress();
/* 115 */         this.deleteHb.clicked = true;
/* 116 */         this.deleteHb.hovered = true;
/*     */       }
/*     */       
/*     */ 
/* 120 */       if ((this.slotHb.hovered) && (CInputActionSet.proceed.isJustPressed())) {
/* 121 */         CInputActionSet.proceed.unpress();
/* 122 */         this.renameHb.clicked = true;
/* 123 */         this.renameHb.hovered = true;
/*     */       }
/*     */     }
/*     */     
/* 127 */     if ((!this.deleteHb.hovered) && (!this.renameHb.hovered)) {
/* 128 */       this.slotHb.update();
/*     */       
/* 130 */       if ((this.slotHb.hovered) && (CInputActionSet.select.isJustPressed())) {
/* 131 */         this.slotHb.clicked = true;
/* 132 */       } else if (this.slotHb.justHovered) {
/* 133 */         CardCrawlGame.sound.play("UI_HOVER");
/* 134 */       } else if ((this.slotHb.hovered) && (InputHelper.justClickedLeft)) {
/* 135 */         this.slotHb.clickStarted = true;
/* 136 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 137 */       } else if (this.slotHb.clicked) {
/* 138 */         this.slotHb.clicked = false;
/* 139 */         CardCrawlGame.saveSlot = this.index;
/* 140 */         if (this.name.equals("")) {
/* 141 */           CardCrawlGame.mainMenuScreen.saveSlotScreen.openRenamePopup(this.index, true);
/*     */         } else {
/* 143 */           CardCrawlGame.mainMenuScreen.saveSlotScreen.confirm(this.index);
/*     */         }
/*     */       }
/*     */     } else {
/* 147 */       this.slotHb.unhover();
/*     */       
/* 149 */       if (this.deleteHb.justHovered) {
/* 150 */         CardCrawlGame.sound.play("UI_HOVER");
/* 151 */       } else if ((this.deleteHb.hovered) && (InputHelper.justClickedLeft)) {
/* 152 */         this.deleteHb.clickStarted = true;
/* 153 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 154 */       } else if (this.deleteHb.clicked) {
/* 155 */         this.deleteHb.clicked = false;
/* 156 */         CardCrawlGame.mainMenuScreen.saveSlotScreen.openDeletePopup(this.index);
/*     */       }
/*     */       
/* 159 */       if (this.renameHb.justHovered) {
/* 160 */         CardCrawlGame.sound.play("UI_HOVER");
/* 161 */       } else if ((this.renameHb.hovered) && (InputHelper.justClickedLeft)) {
/* 162 */         this.renameHb.clickStarted = true;
/* 163 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 164 */       } else if (this.renameHb.clicked) {
/* 165 */         this.renameHb.clicked = false;
/* 166 */         CardCrawlGame.mainMenuScreen.saveSlotScreen.openRenamePopup(this.index, false);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 173 */     renderSlotImage(sb);
/* 174 */     renderText(sb);
/*     */     
/* 176 */     if (!this.emptySlot) {
/* 177 */       renderDeleteIcon(sb);
/* 178 */       renderRenameIcon(sb);
/*     */     }
/*     */     
/* 181 */     renderHbs(sb);
/*     */   }
/*     */   
/*     */   private void renderText(SpriteBatch sb) {
/* 185 */     Color c = Settings.GOLD_COLOR.cpy();
/* 186 */     c.a = uiColor.a;
/*     */     
/* 188 */     if (!this.emptySlot) {
/* 189 */       FontHelper.renderFontLeft(sb, FontHelper.buttonLabelFont, this.name, 740.0F * Settings.scale, this.slotHb.cY + 26.0F * Settings.scale, c);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 198 */       FontHelper.renderFontLeft(sb, FontHelper.charDescFont, TEXT[0] + 
/*     */       
/*     */ 
/* 201 */         com.megacrit.cardcrawl.screens.stats.CharStat.formatHMSM((int)this.playtime), 740.0F * Settings.scale, this.slotHb.cY - 24.0F * Settings.scale, uiColor);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 207 */       FontHelper.renderFontCentered(sb, FontHelper.charTitleFont, 
/*     */       
/*     */ 
/* 210 */         String.format("%.1f", new Object[] {Float.valueOf(this.completionPercentage) }) + "%", 1200.0F * Settings.scale, this.slotHb.cY + 0.0F * Settings.scale, c);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 215 */       FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, TEXT[1], Settings.WIDTH / 2.0F, this.slotHb.cY, c);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderSlotImage(SpriteBatch sb) {
/* 220 */     float scale = Settings.scale;
/*     */     
/* 222 */     if (this.slotHb.hovered) {
/* 223 */       scale = Settings.scale * 1.02F;
/*     */     }
/*     */     
/* 226 */     sb.draw(ImageMaster.PROFILE_SLOT, this.slotHb.cX - 400.0F, this.slotHb.cY - 130.0F, 400.0F, 130.0F, 800.0F, 260.0F, scale, scale, 0.0F, 0, 0, 800, 260, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 244 */     sb.draw(this.iconImg, 670.0F * Settings.scale - 50.0F, this.slotHb.cY - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 100, 100, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 262 */     if (this.slotHb.hovered) {
/* 263 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.4F));
/* 264 */       sb.setBlendFunction(770, 1);
/* 265 */       sb.draw(ImageMaster.PROFILE_SLOT, this.slotHb.cX - 400.0F, this.slotHb.cY - 130.0F, 400.0F, 130.0F, 800.0F, 260.0F, scale, scale, 0.0F, 0, 0, 800, 260, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 282 */       sb.setBlendFunction(770, 771);
/* 283 */       sb.setColor(uiColor);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderHbs(SpriteBatch sb) {
/* 288 */     if (CardCrawlGame.mainMenuScreen.saveSlotScreen.shown) {
/* 289 */       this.slotHb.render(sb);
/* 290 */       this.deleteHb.render(sb);
/* 291 */       this.renameHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderDeleteIcon(SpriteBatch sb) {
/* 296 */     float scale = Settings.scale;
/*     */     
/* 298 */     if (this.deleteHb.hovered) {
/* 299 */       scale = Settings.scale * 1.04F;
/*     */     }
/*     */     
/*     */ 
/* 303 */     sb.draw(ImageMaster.PROFILE_DELETE, this.deleteHb.cX - 50.0F, this.deleteHb.cY - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, scale, scale, 0.0F, 0, 0, 100, 100, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 321 */     if (this.deleteHb.hovered) {
/* 322 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.4F));
/* 323 */       sb.setBlendFunction(770, 1);
/* 324 */       sb.draw(ImageMaster.PROFILE_DELETE, this.deleteHb.cX - 50.0F, this.deleteHb.cY - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, scale, scale, 0.0F, 0, 0, 100, 100, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 341 */       sb.setBlendFunction(770, 771);
/* 342 */       sb.setColor(uiColor);
/*     */     }
/*     */     
/* 345 */     if ((this.slotHb.hovered) && (Settings.isControllerMode)) {
/* 346 */       sb.draw(CInputActionSet.topPanel
/* 347 */         .getKeyImg(), this.deleteHb.cX - 32.0F + 82.0F * Settings.scale, this.deleteHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderRenameIcon(SpriteBatch sb)
/*     */   {
/* 367 */     float scale = Settings.scale;
/*     */     
/* 369 */     if (this.renameHb.hovered) {
/* 370 */       scale = Settings.scale * 1.04F;
/*     */     }
/*     */     
/*     */ 
/* 374 */     sb.draw(ImageMaster.PROFILE_RENAME, this.renameHb.cX - 50.0F, this.renameHb.cY - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, scale, scale, 0.0F, 0, 0, 100, 100, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 392 */     if (this.renameHb.hovered) {
/* 393 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.4F));
/* 394 */       sb.setBlendFunction(770, 1);
/* 395 */       sb.draw(ImageMaster.PROFILE_RENAME, this.renameHb.cX - 50.0F, this.renameHb.cY - 50.0F, 50.0F, 50.0F, 100.0F, 100.0F, scale, scale, 0.0F, 0, 0, 100, 100, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 412 */       sb.setBlendFunction(770, 771);
/* 413 */       sb.setColor(uiColor);
/*     */     }
/*     */     
/* 416 */     if ((this.slotHb.hovered) && (Settings.isControllerMode)) {
/* 417 */       sb.draw(CInputActionSet.proceed
/* 418 */         .getKeyImg(), this.renameHb.cX - 32.0F + 82.0F * Settings.scale, this.renameHb.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void clear()
/*     */   {
/* 438 */     this.name = "";
/* 439 */     this.emptySlot = true;
/* 440 */     this.completionPercentage = 0.0F;
/* 441 */     this.playtime = 0L;
/* 442 */     this.deleteHb.unhover();
/*     */   }
/*     */   
/*     */   public String getName() {
/* 446 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String setName) {
/* 450 */     this.name = setName;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\SaveSlot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */