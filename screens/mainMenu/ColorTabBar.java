/*     */ package com.megacrit.cardcrawl.screens.mainMenu;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ 
/*     */ public class ColorTabBar
/*     */ {
/*  17 */   private static final float TAB_SPACING = 245.0F * Settings.scale;
/*     */   private static final int BAR_W = 1334;
/*     */   private static final int BAR_H = 102;
/*     */   private static final int TAB_W = 310;
/*     */   private static final int TAB_H = 68;
/*     */   private static final int LOCK_W = 40;
/*  23 */   private static final int TICKBOX_W = 48; public Hitbox redHb; public Hitbox greenHb; public Hitbox blueHb; public Hitbox colorlessHb; public Hitbox curseHb; public Hitbox viewUpgradeHb; public CurrentTab curTab = CurrentTab.RED;
/*     */   private TabBarListener delegate;
/*     */   
/*     */   public static enum CurrentTab {
/*  27 */     RED,  GREEN,  BLUE,  COLORLESS,  CURSE;
/*     */     
/*     */     private CurrentTab() {} }
/*     */   
/*  31 */   public ColorTabBar(TabBarListener delegate) { float w = 200.0F * Settings.scale;
/*  32 */     float h = 50.0F * Settings.scale;
/*     */     
/*  34 */     this.redHb = new Hitbox(w, h);
/*  35 */     this.greenHb = new Hitbox(w, h);
/*  36 */     this.blueHb = new Hitbox(w, h);
/*  37 */     this.colorlessHb = new Hitbox(w, h);
/*  38 */     this.curseHb = new Hitbox(w, h);
/*  39 */     this.delegate = delegate;
/*  40 */     this.viewUpgradeHb = new Hitbox(360.0F * Settings.scale, 48.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update(float y) {
/*  44 */     float x = 470.0F * Settings.scale;
/*  45 */     this.redHb.move(x, y + 50.0F * Settings.scale);
/*  46 */     this.greenHb.move(x += TAB_SPACING, y + 50.0F * Settings.scale);
/*  47 */     this.blueHb.move(x += TAB_SPACING, y + 50.0F * Settings.scale);
/*  48 */     this.colorlessHb.move(x += TAB_SPACING, y + 50.0F * Settings.scale);
/*  49 */     this.curseHb.move(x += TAB_SPACING, y + 50.0F * Settings.scale);
/*  50 */     this.viewUpgradeHb.move(1410.0F * Settings.scale, y);
/*     */     
/*  52 */     this.redHb.update();
/*  53 */     this.greenHb.update();
/*  54 */     this.blueHb.update();
/*     */     
/*  56 */     this.colorlessHb.update();
/*  57 */     this.curseHb.update();
/*  58 */     this.viewUpgradeHb.update();
/*     */     
/*  60 */     if ((this.redHb.justHovered) || (this.greenHb.justHovered) || (this.blueHb.justHovered) || (this.colorlessHb.justHovered) || (this.curseHb.justHovered))
/*     */     {
/*  62 */       CardCrawlGame.sound.playA("UI_HOVER", -0.4F);
/*     */     }
/*     */     
/*  65 */     if (Settings.isControllerMode) {
/*  66 */       if (CInputActionSet.pageRightViewExhaust.isJustPressed()) {
/*  67 */         if (this.curTab == CurrentTab.RED) {
/*  68 */           this.curTab = CurrentTab.GREEN;
/*  69 */         } else if (this.curTab == CurrentTab.GREEN) {
/*  70 */           this.curTab = CurrentTab.BLUE;
/*  71 */         } else if (this.curTab == CurrentTab.BLUE) {
/*  72 */           this.curTab = CurrentTab.COLORLESS;
/*  73 */         } else if (this.curTab == CurrentTab.COLORLESS) {
/*  74 */           this.curTab = CurrentTab.CURSE;
/*  75 */         } else if (this.curTab == CurrentTab.CURSE) {
/*  76 */           this.curTab = CurrentTab.RED;
/*     */         }
/*  78 */         this.delegate.didChangeTab(this, this.curTab);
/*  79 */       } else if (CInputActionSet.pageLeftViewDeck.isJustPressed()) {
/*  80 */         if (this.curTab == CurrentTab.RED) {
/*  81 */           this.curTab = CurrentTab.CURSE;
/*  82 */         } else if (this.curTab == CurrentTab.GREEN) {
/*  83 */           this.curTab = CurrentTab.RED;
/*  84 */         } else if (this.curTab == CurrentTab.BLUE) {
/*  85 */           this.curTab = CurrentTab.GREEN;
/*  86 */         } else if (this.curTab == CurrentTab.COLORLESS) {
/*  87 */           this.curTab = CurrentTab.BLUE;
/*  88 */         } else if (this.curTab == CurrentTab.CURSE) {
/*  89 */           this.curTab = CurrentTab.COLORLESS;
/*     */         }
/*  91 */         this.delegate.didChangeTab(this, this.curTab);
/*     */       }
/*     */     }
/*     */     
/*  95 */     if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/*  96 */       CurrentTab oldTab = this.curTab;
/*  97 */       if (this.redHb.hovered) {
/*  98 */         this.curTab = CurrentTab.RED;
/*  99 */       } else if (this.greenHb.hovered) {
/* 100 */         this.curTab = CurrentTab.GREEN;
/* 101 */       } else if (this.blueHb.hovered) {
/* 102 */         this.curTab = CurrentTab.BLUE;
/* 103 */       } else if (this.colorlessHb.hovered) {
/* 104 */         this.curTab = CurrentTab.COLORLESS;
/* 105 */       } else if (this.curseHb.hovered) {
/* 106 */         this.curTab = CurrentTab.CURSE;
/*     */       }
/* 108 */       if (oldTab != this.curTab) {
/* 109 */         this.delegate.didChangeTab(this, this.curTab);
/*     */       }
/*     */     }
/*     */     
/* 113 */     if (this.viewUpgradeHb.justHovered) {
/* 114 */       CardCrawlGame.sound.playA("UI_HOVER", -0.3F);
/*     */     }
/*     */     
/* 117 */     if ((this.viewUpgradeHb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/* 118 */       this.viewUpgradeHb.clickStarted = true;
/*     */     }
/*     */     
/* 121 */     if ((this.viewUpgradeHb.clicked) || ((this.viewUpgradeHb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/* 122 */       this.viewUpgradeHb.clicked = false;
/* 123 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.2F);
/* 124 */       com.megacrit.cardcrawl.screens.SingleCardViewPopup.isViewingUpgrade = !com.megacrit.cardcrawl.screens.SingleCardViewPopup.isViewingUpgrade;
/*     */     }
/*     */   }
/*     */   
/*     */   public Hitbox hoveredHb() {
/* 129 */     if (this.redHb.hovered)
/* 130 */       return this.redHb;
/* 131 */     if (this.greenHb.hovered)
/* 132 */       return this.greenHb;
/* 133 */     if (this.blueHb.hovered)
/* 134 */       return this.blueHb;
/* 135 */     if (this.colorlessHb.hovered)
/* 136 */       return this.colorlessHb;
/* 137 */     if (this.curseHb.hovered)
/* 138 */       return this.curseHb;
/* 139 */     if (this.viewUpgradeHb.hovered) {
/* 140 */       return this.viewUpgradeHb;
/*     */     }
/*     */     
/* 143 */     return null;
/*     */   }
/*     */   
/*     */   public Color getBarColor() {
/* 147 */     switch (this.curTab) {
/*     */     case RED: 
/* 149 */       return new Color(0.5F, 0.1F, 0.1F, 1.0F);
/*     */     case GREEN: 
/* 151 */       return new Color(0.25F, 0.55F, 0.0F, 1.0F);
/*     */     case BLUE: 
/* 153 */       return new Color(0.01F, 0.34F, 0.52F, 1.0F);
/*     */     case COLORLESS: 
/* 155 */       return new Color(0.4F, 0.4F, 0.4F, 1.0F);
/*     */     case CURSE: 
/* 157 */       return new Color(0.18F, 0.18F, 0.16F, 1.0F);
/*     */     }
/*     */     
/*     */     
/* 161 */     return Color.WHITE;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float y) {
/* 165 */     sb.setColor(Color.GRAY);
/*     */     
/* 167 */     if (this.curTab != CurrentTab.CURSE) {
/* 168 */       renderTab(sb, ImageMaster.COLOR_TAB_CURSE, 1450.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[5], true);
/*     */     }
/* 170 */     if (this.curTab != CurrentTab.COLORLESS) {
/* 171 */       renderTab(sb, ImageMaster.COLOR_TAB_COLORLESS, 1205.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[4], true);
/*     */     }
/* 173 */     if (this.curTab != CurrentTab.BLUE) {
/* 174 */       renderTab(sb, ImageMaster.COLOR_TAB_BLUE, 960.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[3], true);
/*     */     }
/* 176 */     if (this.curTab != CurrentTab.GREEN) {
/* 177 */       renderTab(sb, ImageMaster.COLOR_TAB_GREEN, 715.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[2], true);
/*     */     }
/* 179 */     if (this.curTab != CurrentTab.RED) {
/* 180 */       renderTab(sb, ImageMaster.COLOR_TAB_RED, 470.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[1], true);
/*     */     }
/*     */     
/* 183 */     sb.setColor(getBarColor());
/* 184 */     sb.draw(ImageMaster.COLOR_TAB_BAR, Settings.WIDTH / 2.0F - 667.0F, y - 51.0F, 667.0F, 51.0F, 1334.0F, 102.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1334, 102, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 202 */     sb.setColor(Color.WHITE);
/*     */     
/* 204 */     switch (this.curTab) {
/*     */     case RED: 
/* 206 */       renderTab(sb, ImageMaster.COLOR_TAB_RED, 470.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[1], false);
/* 207 */       break;
/*     */     case GREEN: 
/* 209 */       renderTab(sb, ImageMaster.COLOR_TAB_GREEN, 715.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[2], false);
/* 210 */       break;
/*     */     case BLUE: 
/* 212 */       renderTab(sb, ImageMaster.COLOR_TAB_BLUE, 960.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[3], false);
/* 213 */       break;
/*     */     case COLORLESS: 
/* 215 */       renderTab(sb, ImageMaster.COLOR_TAB_COLORLESS, 1205.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[4], false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 222 */       break;
/*     */     case CURSE: 
/* 224 */       renderTab(sb, ImageMaster.COLOR_TAB_CURSE, 1450.0F * Settings.scale, y, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[5], false);
/* 225 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 230 */     renderViewUpgrade(sb, y);
/*     */     
/* 232 */     this.redHb.render(sb);
/* 233 */     this.greenHb.render(sb);
/* 234 */     this.blueHb.render(sb);
/* 235 */     this.colorlessHb.render(sb);
/* 236 */     this.curseHb.render(sb);
/* 237 */     this.viewUpgradeHb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderTab(SpriteBatch sb, Texture img, float x, float y, String label, boolean selected) {
/* 241 */     sb.draw(img, x - 155.0F, y - 34.0F + 53.0F * Settings.scale, 155.0F, 34.0F, 310.0F, 68.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 310, 68, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 259 */     Color c = Settings.GOLD_COLOR;
/* 260 */     if (selected) {
/* 261 */       c = Color.GRAY;
/*     */     }
/*     */     
/* 264 */     FontHelper.renderFontCentered(sb, FontHelper.bannerFont, label, x, y + 50.0F * Settings.scale, c, 0.6F);
/*     */   }
/*     */   
/*     */   private void renderLockIcon(SpriteBatch sb, float y)
/*     */   {
/* 269 */     sb.setColor(new Color(0.7F, 0.7F, 0.7F, 0.7F));
/* 270 */     sb.draw(ImageMaster.COLOR_TAB_LOCK, 942.0F * Settings.scale - 
/*     */     
/* 272 */       FontHelper.getSmartWidth(FontHelper.topPanelInfoFont, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[3], 9999.0F, 0.0F) / 2.0F - 20.0F, y - 20.0F + 52.0F * Settings.scale, 20.0F, 20.0F, 40.0F, 40.0F, Settings.scale * 0.8F, Settings.scale * 0.8F, 0.0F, 0, 0, 40, 40, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 291 */     sb.setColor(Color.LIGHT_GRAY);
/*     */   }
/*     */   
/*     */   private void renderViewUpgrade(SpriteBatch sb, float y) {
/* 295 */     Color c = Settings.CREAM_COLOR;
/* 296 */     if (this.viewUpgradeHb.hovered) {
/* 297 */       c = Settings.GOLD_COLOR;
/*     */     }
/*     */     
/* 300 */     FontHelper.renderFontRightAligned(sb, FontHelper.topPanelInfoFont, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[7], 1546.0F * Settings.scale, y, c);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 308 */     Texture img = ImageMaster.COLOR_TAB_BOX_UNTICKED;
/* 309 */     if (com.megacrit.cardcrawl.screens.SingleCardViewPopup.isViewingUpgrade) {
/* 310 */       img = ImageMaster.COLOR_TAB_BOX_TICKED;
/*     */     }
/*     */     
/* 313 */     sb.setColor(c);
/* 314 */     sb.draw(img, 1532.0F * Settings.scale - 
/*     */     
/* 316 */       FontHelper.getSmartWidth(FontHelper.topPanelInfoFont, com.megacrit.cardcrawl.screens.compendium.CardLibraryScreen.TEXT[7], 9999.0F, 0.0F) - 24.0F, y - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\mainMenu\ColorTabBar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */