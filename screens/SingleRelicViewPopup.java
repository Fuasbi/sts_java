/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class SingleRelicViewPopup
/*     */ {
/*  29 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SingleViewRelicPopup");
/*  30 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  32 */   public boolean isOpen = false;
/*     */   private ArrayList<AbstractRelic> group;
/*     */   private AbstractRelic relic;
/*     */   private AbstractRelic prevRelic;
/*     */   private AbstractRelic nextRelic;
/*  37 */   private static final int W = 128; private Texture relicFrameImg; private Texture largeImg; private float fadeTimer = 0.0F;
/*  38 */   private Color fadeColor = Color.BLACK.cpy();
/*     */   private Hitbox popupHb;
/*  40 */   private Hitbox prevHb; private Hitbox nextHb; private String rarityLabel = "";
/*     */   private static final String LARGE_IMG_DIR = "images/largeRelics/";
/*  42 */   private static final float DESC_LINE_SPACING = 30.0F * Settings.scale;
/*  43 */   private static final float DESC_LINE_WIDTH = 418.0F * Settings.scale;
/*     */   
/*     */   private final float RELIC_OFFSET_Y;
/*     */   
/*     */   public SingleRelicViewPopup()
/*     */   {
/*  49 */     this.RELIC_OFFSET_Y = (76.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void open(AbstractRelic relic, ArrayList<AbstractRelic> group) {
/*  53 */     CardCrawlGame.isPopupOpen = true;
/*  54 */     relic.playLandingSFX();
/*     */     
/*  56 */     this.prevRelic = null;
/*  57 */     this.nextRelic = null;
/*  58 */     this.prevHb = null;
/*  59 */     this.nextHb = null;
/*     */     
/*  61 */     for (int i = 0; i < group.size(); i++) {
/*  62 */       if (group.get(i) == relic) {
/*  63 */         if (i != 0) {
/*  64 */           this.prevRelic = ((AbstractRelic)group.get(i - 1));
/*     */         }
/*  66 */         if (i == group.size() - 1) break;
/*  67 */         this.nextRelic = ((AbstractRelic)group.get(i + 1)); break;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  73 */     this.prevHb = new Hitbox(160.0F * Settings.scale, 160.0F * Settings.scale);
/*  74 */     this.nextHb = new Hitbox(160.0F * Settings.scale, 160.0F * Settings.scale);
/*  75 */     this.prevHb.move(Settings.WIDTH / 2.0F - 400.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*  76 */     this.nextHb.move(Settings.WIDTH / 2.0F + 400.0F * Settings.scale, Settings.HEIGHT / 2.0F);
/*     */     
/*  78 */     this.popupHb = new Hitbox(550.0F * Settings.scale, 680.0F * Settings.scale);
/*  79 */     this.popupHb.move(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F);
/*  80 */     this.isOpen = true;
/*  81 */     this.group = group;
/*  82 */     this.relic = relic;
/*  83 */     this.fadeTimer = 0.25F;
/*  84 */     this.fadeColor.a = 0.0F;
/*  85 */     generateRarityLabel();
/*  86 */     generateFrameImg();
/*  87 */     this.relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/*  88 */     initializeLargeImg();
/*     */   }
/*     */   
/*     */   public void open(AbstractRelic relic) {
/*  92 */     CardCrawlGame.isPopupOpen = true;
/*  93 */     relic.playLandingSFX();
/*     */     
/*  95 */     this.prevRelic = null;
/*  96 */     this.nextRelic = null;
/*  97 */     this.prevHb = null;
/*  98 */     this.nextHb = null;
/*     */     
/* 100 */     this.popupHb = new Hitbox(550.0F * Settings.scale, 680.0F * Settings.scale);
/* 101 */     this.popupHb.move(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F);
/* 102 */     this.isOpen = true;
/* 103 */     this.group = null;
/* 104 */     this.relic = relic;
/* 105 */     this.fadeTimer = 0.25F;
/* 106 */     this.fadeColor.a = 0.0F;
/* 107 */     generateRarityLabel();
/* 108 */     generateFrameImg();
/* 109 */     this.relic.isSeen = UnlockTracker.isRelicSeen(relic.relicId);
/* 110 */     initializeLargeImg();
/*     */   }
/*     */   
/*     */   private void initializeLargeImg() {
/* 114 */     this.largeImg = ImageMaster.loadImage("images/largeRelics/" + this.relic.imgUrl);
/*     */   }
/*     */   
/*     */   public void close() {
/* 118 */     CardCrawlGame.isPopupOpen = false;
/* 119 */     this.isOpen = false;
/* 120 */     InputHelper.justReleasedClickLeft = false;
/*     */   }
/*     */   
/*     */   public void update() {
/* 124 */     this.popupHb.update();
/* 125 */     updateArrows();
/* 126 */     updateInput();
/* 127 */     updateFade();
/*     */   }
/*     */   
/*     */   private void updateArrows()
/*     */   {
/* 132 */     if (this.prevRelic != null) {
/* 133 */       this.prevHb.update();
/* 134 */       if (this.prevHb.justHovered) {
/* 135 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/* 137 */       if ((this.prevHb.clicked) || ((this.prevRelic != null) && (CInputActionSet.pageLeftViewDeck.isJustPressed()))) {
/* 138 */         openPrev();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 143 */     if (this.nextRelic != null) {
/* 144 */       this.nextHb.update();
/* 145 */       if (this.nextHb.justHovered) {
/* 146 */         CardCrawlGame.sound.play("UI_HOVER");
/*     */       }
/*     */       
/* 149 */       if ((this.nextHb.clicked) || ((this.nextRelic != null) && (CInputActionSet.pageRightViewExhaust.isJustPressed()))) {
/* 150 */         openNext();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateInput() {
/* 156 */     if (InputHelper.justClickedLeft) {
/* 157 */       if ((this.prevRelic != null) && (this.prevHb.hovered)) {
/* 158 */         this.prevHb.clickStarted = true;
/* 159 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 160 */         return; }
/* 161 */       if ((this.nextRelic != null) && (this.nextHb.hovered)) {
/* 162 */         this.nextHb.clickStarted = true;
/* 163 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 164 */         return;
/*     */       }
/*     */     }
/*     */     
/* 168 */     if (InputHelper.justReleasedClickLeft) {
/* 169 */       if (!this.popupHb.hovered) {
/* 170 */         close();
/*     */       }
/* 172 */     } else if ((InputHelper.pressedEscape) || (CInputActionSet.cancel.isJustPressed())) {
/* 173 */       CInputActionSet.cancel.unpress();
/* 174 */       InputHelper.pressedEscape = false;
/* 175 */       if ((AbstractDungeon.screen == null) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NONE)) {
/* 176 */         AbstractDungeon.isScreenUp = false;
/*     */       }
/* 178 */       close();
/*     */     }
/*     */     
/*     */ 
/* 182 */     if ((this.prevRelic != null) && (InputActionSet.left.isJustPressed())) {
/* 183 */       openPrev();
/* 184 */     } else if ((this.nextRelic != null) && (InputActionSet.right.isJustPressed())) {
/* 185 */       openNext();
/*     */     }
/*     */   }
/*     */   
/*     */   private void openPrev() {
/* 190 */     close();
/* 191 */     open(this.prevRelic, this.group);
/* 192 */     this.fadeTimer = 0.0F;
/* 193 */     this.fadeColor.a = 0.9F;
/*     */   }
/*     */   
/*     */   private void openNext() {
/* 197 */     close();
/* 198 */     open(this.nextRelic, this.group);
/* 199 */     this.fadeTimer = 0.0F;
/* 200 */     this.fadeColor.a = 0.9F;
/*     */   }
/*     */   
/*     */   private void updateFade() {
/* 204 */     this.fadeTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 205 */     if (this.fadeTimer < 0.0F) {
/* 206 */       this.fadeTimer = 0.0F;
/*     */     }
/* 208 */     this.fadeColor.a = com.badlogic.gdx.math.Interpolation.pow2In.apply(0.9F, 0.0F, this.fadeTimer * 4.0F);
/*     */   }
/*     */   
/*     */   private void generateRarityLabel() {
/* 212 */     switch (this.relic.tier) {
/*     */     case BOSS: 
/* 214 */       this.rarityLabel = TEXT[0];
/* 215 */       break;
/*     */     case COMMON: 
/* 217 */       this.rarityLabel = TEXT[1];
/* 218 */       break;
/*     */     case DEPRECATED: 
/* 220 */       this.rarityLabel = TEXT[2];
/* 221 */       break;
/*     */     case RARE: 
/* 223 */       this.rarityLabel = TEXT[3];
/* 224 */       break;
/*     */     case SHOP: 
/* 226 */       this.rarityLabel = TEXT[4];
/* 227 */       break;
/*     */     case SPECIAL: 
/* 229 */       this.rarityLabel = TEXT[5];
/* 230 */       break;
/*     */     case STARTER: 
/* 232 */       this.rarityLabel = TEXT[6];
/* 233 */       break;
/*     */     case UNCOMMON: 
/* 235 */       this.rarityLabel = TEXT[7];
/* 236 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   private void generateFrameImg()
/*     */   {
/* 244 */     if (!this.relic.isSeen) {
/* 245 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameCommon.png");
/* 246 */       return;
/*     */     }
/*     */     
/* 249 */     switch (this.relic.tier) {
/*     */     case BOSS: 
/* 251 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameBoss.png");
/* 252 */       break;
/*     */     case COMMON: 
/* 254 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameCommon.png");
/* 255 */       break;
/*     */     case DEPRECATED: 
/* 257 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameCommon.png");
/* 258 */       break;
/*     */     case RARE: 
/* 260 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameRare.png");
/* 261 */       break;
/*     */     case SHOP: 
/* 263 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameRare.png");
/* 264 */       break;
/*     */     case SPECIAL: 
/* 266 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameRare.png");
/* 267 */       break;
/*     */     case STARTER: 
/* 269 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameCommon.png");
/* 270 */       break;
/*     */     case UNCOMMON: 
/* 272 */       this.relicFrameImg = ImageMaster.loadImage("images/ui/relicFrameUncommon.png");
/* 273 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 281 */     sb.setColor(this.fadeColor);
/* 282 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 283 */     renderPopupBg(sb);
/* 284 */     renderFrame(sb);
/* 285 */     renderArrows(sb);
/* 286 */     renderRelicImage(sb);
/* 287 */     renderName(sb);
/* 288 */     renderRarity(sb);
/* 289 */     renderDescription(sb);
/* 290 */     renderQuote(sb);
/* 291 */     renderTips(sb);
/* 292 */     this.popupHb.render(sb);
/* 293 */     if (this.prevHb != null) {
/* 294 */       this.prevHb.render(sb);
/*     */     }
/* 296 */     if (this.nextHb != null) {
/* 297 */       this.nextHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderPopupBg(SpriteBatch sb) {
/* 302 */     sb.setColor(Color.WHITE);
/*     */     
/* 304 */     sb.draw(ImageMaster.RELIC_POPUP, Settings.WIDTH / 2.0F - 960.0F, Settings.HEIGHT / 2.0F - 540.0F, 960.0F, 540.0F, 1920.0F, 1080.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1080, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderFrame(SpriteBatch sb)
/*     */   {
/* 324 */     sb.draw(this.relicFrameImg, Settings.WIDTH / 2.0F - 960.0F, Settings.HEIGHT / 2.0F - 540.0F, 960.0F, 540.0F, 1920.0F, 1080.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1080, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderArrows(SpriteBatch sb)
/*     */   {
/* 349 */     if (this.prevRelic != null) {
/* 350 */       sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 368 */       if (Settings.isControllerMode) {
/* 369 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 370 */           .getKeyImg(), this.prevHb.cX - 32.0F + 0.0F * Settings.scale, this.prevHb.cY - 32.0F + 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 388 */       if (this.prevHb.hovered) {
/* 389 */         sb.setBlendFunction(770, 1);
/* 390 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 391 */         sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 408 */         sb.setColor(Color.WHITE);
/* 409 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */     }
/*     */     
/* 413 */     if (this.nextRelic != null) {
/* 414 */       sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, true, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 432 */       if (Settings.isControllerMode) {
/* 433 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 434 */           .getKeyImg(), this.nextHb.cX - 32.0F + 0.0F * Settings.scale, this.nextHb.cY - 32.0F + 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 452 */       if (this.nextHb.hovered) {
/* 453 */         sb.setBlendFunction(770, 1);
/* 454 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.5F));
/* 455 */         sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, true, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 472 */         sb.setColor(Color.WHITE);
/* 473 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderRelicImage(SpriteBatch sb) {
/* 479 */     if (UnlockTracker.isRelicLocked(this.relic.relicId)) {
/* 480 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
/* 481 */       sb.draw(ImageMaster.RELIC_LOCK_OUTLINE, Settings.WIDTH / 2.0F - 64.0F, Settings.HEIGHT / 2.0F - 64.0F + this.RELIC_OFFSET_Y, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 498 */       sb.setColor(Color.WHITE);
/* 499 */       sb.draw(ImageMaster.RELIC_LOCK, Settings.WIDTH / 2.0F - 64.0F, Settings.HEIGHT / 2.0F - 64.0F + this.RELIC_OFFSET_Y, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 516 */       return;
/*     */     }
/*     */     
/* 519 */     if (!this.relic.isSeen) {
/* 520 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.75F));
/*     */     } else {
/* 522 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
/*     */     }
/*     */     
/* 525 */     sb.draw(this.relic.outlineImg, Settings.WIDTH / 2.0F - 64.0F, Settings.HEIGHT / 2.0F - 64.0F + this.RELIC_OFFSET_Y, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 543 */     if (!this.relic.isSeen) {
/* 544 */       sb.setColor(Color.BLACK);
/*     */     } else {
/* 546 */       sb.setColor(Color.WHITE);
/*     */     }
/*     */     
/* 549 */     if (this.largeImg == null) {
/* 550 */       sb.draw(this.relic.img, Settings.WIDTH / 2.0F - 64.0F, Settings.HEIGHT / 2.0F - 64.0F + this.RELIC_OFFSET_Y, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 568 */       sb.draw(this.largeImg, Settings.WIDTH / 2.0F - 128.0F, Settings.HEIGHT / 2.0F - 128.0F + this.RELIC_OFFSET_Y, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 256, 256, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderName(SpriteBatch sb)
/*     */   {
/* 589 */     if (UnlockTracker.isRelicLocked(this.relic.relicId)) {
/* 590 */       FontHelper.renderWrappedText(sb, FontHelper.SCP_cardDescFont, TEXT[8], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 265.0F * Settings.scale, 9999.0F, Settings.CREAM_COLOR, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 599 */     else if (this.relic.isSeen) {
/* 600 */       FontHelper.renderWrappedText(sb, FontHelper.SCP_cardDescFont, this.relic.name, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 280.0F * Settings.scale, 9999.0F, Settings.CREAM_COLOR, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 610 */       FontHelper.renderWrappedText(sb, FontHelper.SCP_cardDescFont, TEXT[9], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 265.0F * Settings.scale, 9999.0F, Settings.CREAM_COLOR, 1.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void renderRarity(SpriteBatch sb)
/*     */   {
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/*     */     Color tmpColor;
/* 625 */     switch (this.relic.tier) {
/*     */     case BOSS: 
/* 627 */       tmpColor = Settings.RED_TEXT_COLOR;
/* 628 */       break;
/*     */     case RARE: 
/* 630 */       tmpColor = Settings.GOLD_COLOR;
/* 631 */       break;
/*     */     case UNCOMMON: 
/* 633 */       tmpColor = Settings.BLUE_TEXT_COLOR;
/* 634 */       break;
/*     */     case COMMON: 
/* 636 */       tmpColor = Settings.CREAM_COLOR;
/* 637 */       break;
/*     */     case STARTER: 
/* 639 */       tmpColor = Settings.CREAM_COLOR;
/* 640 */       break;
/*     */     case SPECIAL: 
/* 642 */       tmpColor = Settings.GOLD_COLOR;
/* 643 */       break;
/*     */     case SHOP: 
/* 645 */       tmpColor = Settings.GOLD_COLOR;
/* 646 */       break;
/*     */     case DEPRECATED: default: 
/* 648 */       tmpColor = Settings.CREAM_COLOR;
/*     */     }
/*     */     
/* 651 */     if (this.relic.isSeen) {
/* 652 */       FontHelper.renderWrappedText(sb, FontHelper.cardDescFont_N, this.rarityLabel + TEXT[10], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F + 240.0F * Settings.scale, 9999.0F, tmpColor, 1.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderDescription(SpriteBatch sb)
/*     */   {
/* 665 */     if (UnlockTracker.isRelicLocked(this.relic.relicId)) {
/* 666 */       FontHelper.renderFontCentered(sb, FontHelper.cardDescFont_N, TEXT[11], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 140.0F * Settings.scale, Settings.CREAM_COLOR, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 674 */     else if (this.relic.isSeen) {
/* 675 */       FontHelper.renderSmartText(sb, FontHelper.cardDescFont_N, this.relic.description, Settings.WIDTH / 2.0F - 200.0F * Settings.scale, Settings.HEIGHT / 2.0F - 140.0F * Settings.scale - 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 680 */         FontHelper.getSmartHeight(FontHelper.cardDescFont_N, this.relic.description, DESC_LINE_WIDTH, DESC_LINE_SPACING) / 2.0F, DESC_LINE_WIDTH, DESC_LINE_SPACING, Settings.CREAM_COLOR);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 689 */       FontHelper.renderFontCentered(sb, FontHelper.cardDescFont_N, TEXT[12], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 140.0F * Settings.scale, Settings.CREAM_COLOR, 1.0F);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderQuote(SpriteBatch sb)
/*     */   {
/* 701 */     if (this.relic.isSeen) {
/* 702 */       if (this.relic.flavorText != null) {
/* 703 */         FontHelper.renderWrappedText(sb, FontHelper.SRV_quoteFont, this.relic.flavorText, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 310.0F * Settings.scale, DESC_LINE_WIDTH, Settings.CREAM_COLOR, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/* 713 */         FontHelper.renderWrappedText(sb, FontHelper.SRV_quoteFont, "\"Missing quote...\"", Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F - 300.0F * Settings.scale, DESC_LINE_WIDTH, Settings.CREAM_COLOR, 1.0F);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderTips(SpriteBatch sb)
/*     */   {
/* 727 */     if (this.relic.isSeen) {
/* 728 */       ArrayList<PowerTip> t = new ArrayList();
/* 729 */       if (this.relic.tips.size() > 1) {
/* 730 */         for (int i = 1; i < this.relic.tips.size(); i++) {
/* 731 */           t.add(this.relic.tips.get(i));
/*     */         }
/*     */       }
/*     */       
/* 735 */       if (!t.isEmpty()) {
/* 736 */         com.megacrit.cardcrawl.helpers.TipHelper.queuePowerTips(1300.0F * Settings.scale, 440.0F * Settings.scale, t);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\SingleRelicViewPopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */