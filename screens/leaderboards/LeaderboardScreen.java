/*     */ package com.megacrit.cardcrawl.screens.leaderboards;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class LeaderboardScreen
/*     */ {
/*  29 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("LeaderboardsScreen");
/*  30 */   public static final String[] TEXT = uiStrings.TEXT;
/*  31 */   public MenuCancelButton button = new MenuCancelButton();
/*  32 */   public boolean screenUp = false; public boolean waiting = true; public boolean refresh = true;
/*  33 */   public ArrayList<LeaderboardEntry> entries = new ArrayList();
/*  34 */   public ArrayList<FilterButton> charButtons = new ArrayList();
/*  35 */   public ArrayList<FilterButton> regionButtons = new ArrayList();
/*  36 */   public ArrayList<FilterButton> typeButtons = new ArrayList();
/*  37 */   public static final float RANK_X = 1000.0F * Settings.scale;
/*  38 */   public static final float NAME_X = 1160.0F * Settings.scale;
/*  39 */   public static final float SCORE_X = 1500.0F * Settings.scale;
/*  40 */   public String charLabel = TEXT[2]; public String regionLabel = TEXT[3]; public String typeLabel = TEXT[4];
/*     */   public int currentStartIndex;
/*  42 */   public int currentEndIndex; private static final float FILTER_SPACING_X = 100.0F * Settings.scale;
/*     */   private Hitbox prevHb;
/*  44 */   private Hitbox nextHb; private Hitbox viewMyScoreHb = new Hitbox(250.0F * Settings.scale, 80.0F * Settings.scale);
/*  45 */   public boolean viewMyScore = false;
/*     */   
/*  47 */   private float lineFadeInTimer = 0.0F;
/*  48 */   private static final float LINE_THICKNESS = 4.0F * Settings.scale;
/*     */   
/*     */   public LeaderboardScreen() {
/*  51 */     this.prevHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  52 */     this.prevHb.move(880.0F * Settings.scale, 530.0F * Settings.scale);
/*  53 */     this.nextHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  54 */     this.nextHb.move(1740.0F * Settings.scale, 530.0F * Settings.scale);
/*  55 */     this.viewMyScoreHb.move(1300.0F * Settings.scale, 80.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  59 */     updateControllerInput();
/*     */     
/*  61 */     for (FilterButton b : this.charButtons) {
/*  62 */       b.update();
/*     */     }
/*     */     
/*  65 */     for (FilterButton b : this.regionButtons) {
/*  66 */       b.update();
/*     */     }
/*     */     
/*  69 */     for (FilterButton b : this.typeButtons) {
/*  70 */       b.update();
/*     */     }
/*     */     
/*  73 */     this.button.update();
/*     */     
/*  75 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/*  76 */       InputHelper.pressedEscape = false;
/*  77 */       hide();
/*     */     }
/*     */     
/*  80 */     if ((!this.entries.isEmpty()) && (!this.waiting)) {
/*  81 */       this.lineFadeInTimer = MathHelper.slowColorLerpSnap(this.lineFadeInTimer, 1.0F);
/*  82 */       updateEntries();
/*     */     }
/*     */     
/*  85 */     if (this.refresh) {
/*  86 */       this.refresh = false;
/*  87 */       this.waiting = true;
/*  88 */       this.lineFadeInTimer = 0.0F;
/*  89 */       this.currentStartIndex = 1;
/*  90 */       this.currentEndIndex = 20;
/*  91 */       getData(this.currentStartIndex, this.currentEndIndex);
/*     */     }
/*     */     
/*  94 */     updateViewMyScore();
/*  95 */     updateArrows();
/*     */   }
/*     */   
/*     */   private static enum LeaderboardSelectionType {
/*  99 */     NONE,  CHAR,  REGION,  TYPE;
/*     */     
/*     */     private LeaderboardSelectionType() {} }
/*     */   
/* 103 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/* 104 */       return;
/*     */     }
/*     */     
/* 107 */     LeaderboardSelectionType type = LeaderboardSelectionType.NONE;
/* 108 */     boolean anyHovered = false;
/* 109 */     int index = 0;
/*     */     
/* 111 */     for (FilterButton b : this.charButtons) {
/* 112 */       if (b.hb.hovered) {
/* 113 */         anyHovered = true;
/* 114 */         type = LeaderboardSelectionType.CHAR;
/* 115 */         break;
/*     */       }
/* 117 */       index++;
/*     */     }
/*     */     
/* 120 */     if (!anyHovered) {
/* 121 */       index = 0;
/* 122 */       for (FilterButton b : this.regionButtons) {
/* 123 */         if (b.hb.hovered) {
/* 124 */           anyHovered = true;
/* 125 */           type = LeaderboardSelectionType.REGION;
/* 126 */           break;
/*     */         }
/* 128 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 132 */     if (!anyHovered) {
/* 133 */       index = 0;
/* 134 */       for (FilterButton b : this.typeButtons) {
/* 135 */         if (b.hb.hovered) {
/* 136 */           anyHovered = true;
/* 137 */           type = LeaderboardSelectionType.TYPE;
/* 138 */           break;
/*     */         }
/* 140 */         index++;
/*     */       }
/*     */     }
/*     */     
/* 144 */     if (!anyHovered) {
/* 145 */       Gdx.input.setCursorPosition(
/* 146 */         (int)((FilterButton)this.charButtons.get(0)).hb.cX, Settings.HEIGHT - 
/* 147 */         (int)((FilterButton)this.charButtons.get(0)).hb.cY);
/* 148 */       System.out.println("NOT HOVERED");
/*     */     } else {
/* 150 */       switch (type) {
/*     */       case CHAR: 
/* 152 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 153 */           index--;
/* 154 */           if (index < 0) {
/* 155 */             return;
/*     */           }
/* 157 */           Gdx.input.setCursorPosition(
/* 158 */             (int)((FilterButton)this.charButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 159 */             (int)((FilterButton)this.charButtons.get(index)).hb.cY);
/* 160 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 161 */           index++;
/* 162 */           if (index > this.charButtons.size() - 1) {
/* 163 */             return;
/*     */           }
/* 165 */           Gdx.input.setCursorPosition(
/* 166 */             (int)((FilterButton)this.charButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 167 */             (int)((FilterButton)this.charButtons.get(index)).hb.cY);
/* 168 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 169 */           if (index > this.regionButtons.size() - 1) {
/* 170 */             index = this.regionButtons.size() - 1;
/*     */           }
/* 172 */           Gdx.input.setCursorPosition(
/* 173 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 174 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cY);
/*     */         }
/*     */         break;
/*     */       case REGION: 
/* 178 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 179 */           index--;
/* 180 */           if (index < 0) {
/* 181 */             return;
/*     */           }
/* 183 */           Gdx.input.setCursorPosition(
/* 184 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 185 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cY);
/* 186 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 187 */           index++;
/* 188 */           if (index > this.regionButtons.size() - 1) {
/* 189 */             return;
/*     */           }
/* 191 */           Gdx.input.setCursorPosition(
/* 192 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 193 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cY);
/* 194 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 195 */           if (index > this.typeButtons.size() - 1) {
/* 196 */             index = this.typeButtons.size() - 1;
/*     */           }
/* 198 */           Gdx.input.setCursorPosition(
/* 199 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 200 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cY);
/* 201 */         } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 202 */           if (index > this.charButtons.size() - 1) {
/* 203 */             index = this.charButtons.size() - 1;
/*     */           }
/* 205 */           Gdx.input.setCursorPosition(
/* 206 */             (int)((FilterButton)this.charButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 207 */             (int)((FilterButton)this.charButtons.get(index)).hb.cY);
/*     */         }
/*     */         break;
/*     */       case TYPE: 
/* 211 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 212 */           index--;
/* 213 */           if (index < 0) {
/* 214 */             return;
/*     */           }
/* 216 */           Gdx.input.setCursorPosition(
/* 217 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 218 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cY);
/* 219 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 220 */           index++;
/* 221 */           if (index > this.typeButtons.size() - 1) {
/* 222 */             return;
/*     */           }
/* 224 */           Gdx.input.setCursorPosition(
/* 225 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 226 */             (int)((FilterButton)this.typeButtons.get(index)).hb.cY);
/* 227 */         } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 228 */           if (index > this.regionButtons.size() - 1) {
/* 229 */             index = this.regionButtons.size() - 1;
/*     */           }
/* 231 */           Gdx.input.setCursorPosition(
/* 232 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cX, Settings.HEIGHT - 
/* 233 */             (int)((FilterButton)this.regionButtons.get(index)).hb.cY);
/*     */         }
/*     */         
/*     */         break;
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateEntries()
/*     */   {
/* 244 */     for (LeaderboardEntry e : this.entries) {
/* 245 */       e.update();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateViewMyScore() {
/* 250 */     if (((FilterButton)this.regionButtons.get(0)).active) {
/* 251 */       return;
/*     */     }
/*     */     
/* 254 */     this.viewMyScoreHb.update();
/*     */     
/* 256 */     if (this.viewMyScoreHb.justHovered) {
/* 257 */       CardCrawlGame.sound.play("UI_HOVER");
/* 258 */     } else if ((this.viewMyScoreHb.hovered) && (InputHelper.justClickedLeft)) {
/* 259 */       this.viewMyScoreHb.clickStarted = true;
/* 260 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 261 */     } else if ((this.viewMyScoreHb.clicked) || (CInputActionSet.topPanel.isJustPressed())) {
/* 262 */       this.viewMyScoreHb.clicked = false;
/* 263 */       this.viewMyScore = true;
/* 264 */       this.waiting = true;
/* 265 */       getData(this.currentStartIndex, this.currentEndIndex);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateArrows() {
/* 270 */     if (this.waiting) {
/* 271 */       return;
/*     */     }
/*     */     
/* 274 */     if (this.entries.size() == 20) {
/* 275 */       this.nextHb.update();
/* 276 */       if (this.nextHb.justHovered) {
/* 277 */         CardCrawlGame.sound.play("UI_HOVER");
/* 278 */       } else if ((this.nextHb.hovered) && (InputHelper.justClickedLeft)) {
/* 279 */         this.nextHb.clickStarted = true;
/* 280 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 281 */       } else if ((this.nextHb.clicked) || (CInputActionSet.pageRightViewExhaust.isJustPressed())) {
/* 282 */         this.nextHb.clicked = false;
/* 283 */         this.currentStartIndex = (this.currentEndIndex + 1);
/* 284 */         this.currentEndIndex = (this.currentStartIndex + 19);
/* 285 */         this.waiting = true;
/* 286 */         getData(this.currentStartIndex, this.currentEndIndex);
/*     */       }
/*     */     }
/*     */     
/* 290 */     if (this.currentStartIndex != 1) {
/* 291 */       this.prevHb.update();
/* 292 */       if (this.prevHb.justHovered) {
/* 293 */         CardCrawlGame.sound.play("UI_HOVER");
/* 294 */       } else if ((this.prevHb.hovered) && (InputHelper.justClickedLeft)) {
/* 295 */         this.prevHb.clickStarted = true;
/* 296 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 297 */       } else if ((this.prevHb.clicked) || (CInputActionSet.pageLeftViewDeck.isJustPressed())) {
/* 298 */         this.prevHb.clicked = false;
/* 299 */         this.currentStartIndex -= 20;
/* 300 */         if (this.currentStartIndex < 1) {
/* 301 */           this.currentStartIndex = 1;
/*     */         }
/* 303 */         this.currentEndIndex = (this.currentStartIndex + 19);
/* 304 */         this.waiting = true;
/* 305 */         getData(this.currentStartIndex, this.currentEndIndex);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void getData(int startIndex, int endIndex) {
/* 311 */     AbstractPlayer.PlayerClass tmpClass = null;
/* 312 */     FilterButton.RegionSetting rSetting = null;
/* 313 */     FilterButton.LeaderboardType lType = null;
/*     */     
/* 315 */     for (FilterButton b : this.charButtons) {
/* 316 */       if (b.active) {
/* 317 */         tmpClass = b.pClass;
/* 318 */         break;
/*     */       }
/*     */     }
/*     */     
/* 322 */     for (FilterButton b : this.regionButtons) {
/* 323 */       if (b.active) {
/* 324 */         rSetting = b.rType;
/* 325 */         break;
/*     */       }
/*     */     }
/*     */     
/* 329 */     for (FilterButton b : this.typeButtons) {
/* 330 */       if (b.active) {
/* 331 */         lType = b.lType;
/* 332 */         break;
/*     */       }
/*     */     }
/*     */     
/* 336 */     if ((tmpClass != null) && (rSetting != null) && (lType != null)) {
/* 337 */       SteamSaveSync.getLeaderboardEntries(tmpClass, rSetting, lType, startIndex, endIndex);
/*     */     }
/*     */   }
/*     */   
/*     */   public void open() {
/* 342 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.LEADERBOARD;
/* 343 */     if (this.charButtons.isEmpty()) {
/* 344 */       this.charButtons.add(new FilterButton("ironclad.png", true, AbstractPlayer.PlayerClass.IRONCLAD));
/* 345 */       this.charButtons.add(new FilterButton("silent.png", false, AbstractPlayer.PlayerClass.THE_SILENT));
/* 346 */       if (!UnlockTracker.isCharacterLocked("Defect")) {
/* 347 */         this.charButtons.add(new FilterButton("defect.png", false, AbstractPlayer.PlayerClass.DEFECT));
/*     */       }
/*     */       
/* 350 */       this.regionButtons.add(new FilterButton("friends.png", true, FilterButton.RegionSetting.FRIEND));
/* 351 */       this.regionButtons.add(new FilterButton("global.png", false, FilterButton.RegionSetting.GLOBAL));
/*     */       
/* 353 */       this.typeButtons.add(new FilterButton("score.png", true, FilterButton.LeaderboardType.HIGH_SCORE));
/* 354 */       this.typeButtons.add(new FilterButton("chain.png", false, FilterButton.LeaderboardType.CONSECUTIVE_WINS));
/* 355 */       this.typeButtons.add(new FilterButton("time.png", false, FilterButton.LeaderboardType.FASTEST_WIN));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 361 */     this.button.show(TEXT[0]);
/* 362 */     this.screenUp = true;
/*     */   }
/*     */   
/*     */   public void hide() {
/* 366 */     CardCrawlGame.sound.play("DECK_CLOSE", 0.1F);
/* 367 */     this.button.hide();
/* 368 */     this.screenUp = false;
/* 369 */     CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 373 */     renderScoreHeaders(sb);
/* 374 */     renderScores(sb);
/* 375 */     renderViewMyScoreBox(sb);
/* 376 */     renderFilters(sb);
/* 377 */     renderArrows(sb);
/* 378 */     this.button.render(sb);
/*     */   }
/*     */   
/*     */   private void renderFilters(SpriteBatch sb) {
/* 382 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.charTitleFont, TEXT[1], 240.0F * Settings.scale, 920.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 390 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, this.charLabel, 280.0F * Settings.scale, 860.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 398 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, this.regionLabel, 280.0F * Settings.scale, 680.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 406 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, this.typeLabel, 280.0F * Settings.scale, 500.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 414 */     for (int i = 0; i < this.charButtons.size(); i++) {
/* 415 */       ((FilterButton)this.charButtons.get(i)).render(sb, 340.0F * Settings.scale + i * FILTER_SPACING_X, 780.0F * Settings.scale);
/*     */     }
/*     */     
/* 418 */     for (int i = 0; i < this.regionButtons.size(); i++) {
/* 419 */       ((FilterButton)this.regionButtons.get(i)).render(sb, 340.0F * Settings.scale + i * FILTER_SPACING_X, 600.0F * Settings.scale);
/*     */     }
/*     */     
/* 422 */     for (int i = 0; i < this.typeButtons.size(); i++) {
/* 423 */       ((FilterButton)this.typeButtons.get(i)).render(sb, 340.0F * Settings.scale + i * FILTER_SPACING_X, 420.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderArrows(SpriteBatch sb)
/*     */   {
/* 429 */     boolean renderLeftArrow = true;
/*     */     
/*     */ 
/* 432 */     for (FilterButton b : this.regionButtons) {
/* 433 */       if ((b.rType == FilterButton.RegionSetting.FRIEND) && (this.entries.size() < 20)) {
/* 434 */         renderLeftArrow = false;
/*     */       }
/*     */     }
/*     */     
/* 438 */     if ((this.currentStartIndex != 1) && (renderLeftArrow)) {
/* 439 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer));
/* 440 */       sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 458 */       if (this.prevHb.hovered) {
/* 459 */         sb.setBlendFunction(770, 1);
/* 460 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer / 2.0F));
/* 461 */         sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 478 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */       
/* 481 */       if (Settings.isControllerMode) {
/* 482 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 483 */           .getKeyImg(), this.prevHb.cX - 32.0F + 0.0F * Settings.scale, this.prevHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 501 */       this.prevHb.render(sb);
/*     */     }
/*     */     
/*     */ 
/* 505 */     if (this.entries.size() == 20) {
/* 506 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer));
/* 507 */       sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 525 */       if (this.nextHb.hovered) {
/* 526 */         sb.setBlendFunction(770, 1);
/* 527 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer / 2.0F));
/* 528 */         sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 545 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */       
/* 548 */       if (Settings.isControllerMode) {
/* 549 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 550 */           .getKeyImg(), this.nextHb.cX - 32.0F + 0.0F * Settings.scale, this.nextHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 568 */       this.nextHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderScoreHeaders(SpriteBatch sb) {
/* 573 */     Color creamColor = new Color(1.0F, 0.965F, 0.886F, this.lineFadeInTimer);
/* 574 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.charTitleFont, TEXT[10], 960.0F * Settings.scale, 920.0F * Settings.scale, new Color(0.937F, 0.784F, 0.317F, this.lineFadeInTimer));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 582 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT[6], RANK_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 590 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT[7], NAME_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 598 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT[8], SCORE_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 606 */     sb.setColor(creamColor);
/* 607 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 1138.0F * Settings.scale, 168.0F * Settings.scale, LINE_THICKNESS, 692.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 613 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 1480.0F * Settings.scale, 168.0F * Settings.scale, LINE_THICKNESS, 692.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 619 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.lineFadeInTimer * 0.75F));
/* 620 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 982.0F * Settings.scale, 814.0F * Settings.scale, 630.0F * Settings.scale, 16.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 626 */     sb.setColor(creamColor);
/* 627 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 982.0F * Settings.scale, 820.0F * Settings.scale, 630.0F * Settings.scale, LINE_THICKNESS);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderViewMyScoreBox(SpriteBatch sb)
/*     */   {
/* 636 */     if ((((FilterButton)this.regionButtons.get(0)).active) || (this.waiting)) {
/* 637 */       return;
/*     */     }
/*     */     
/* 640 */     if (this.viewMyScoreHb.hovered) {
/* 641 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[5], 1310.0F * Settings.scale, 80.0F * Settings.scale, Settings.GREEN_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 649 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[5], 1310.0F * Settings.scale, 80.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 658 */     if (Settings.isControllerMode) {
/* 659 */       sb.draw(CInputActionSet.topPanel
/* 660 */         .getKeyImg(), Settings.WIDTH / 2.0F - 32.0F + 210.0F * Settings.scale, -32.0F + 80.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 678 */     this.viewMyScoreHb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderScores(SpriteBatch sb) {
/* 682 */     if (!this.waiting) {
/* 683 */       if (this.entries.isEmpty()) {
/* 684 */         FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT[12], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/* 692 */         for (int i = 0; i < this.entries.size(); i++) {
/* 693 */           ((LeaderboardEntry)this.entries.get(i)).render(sb, i);
/*     */         }
/*     */       }
/*     */     }
/* 697 */     else if (SteamSaveSync.steamUser == null) {
/* 698 */       FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT[11], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.RED_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 706 */       FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT[9], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\leaderboards\LeaderboardScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */