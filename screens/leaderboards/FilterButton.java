/*     */ package com.megacrit.cardcrawl.screens.leaderboards;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ 
/*     */ public class FilterButton
/*     */ {
/*  18 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("LeaderboardFilters");
/*  19 */   public static final String[] TEXT = uiStrings.TEXT;
/*  20 */   public LeaderboardType lType = null;
/*  21 */   public RegionSetting rType = null;
/*  22 */   public boolean active = false;
/*  23 */   public AbstractPlayer.PlayerClass pClass = null;
/*     */   private com.badlogic.gdx.graphics.Texture img;
/*     */   private static final int W = 128;
/*     */   private static final String IMG_DIR = "images/ui/leaderboards/";
/*  27 */   public Hitbox hb = new Hitbox(100.0F * Settings.scale, 100.0F * Settings.scale);
/*     */   public String label;
/*     */   
/*     */   public static enum LeaderboardType {
/*  31 */     HIGH_SCORE,  FASTEST_WIN,  CONSECUTIVE_WINS,  AVG_FLOOR,  AVG_SCORE,  SPIRE_LEVEL;
/*     */     
/*     */     private LeaderboardType() {} }
/*     */   
/*  35 */   public static enum RegionSetting { GLOBAL,  FRIEND;
/*     */     
/*     */     private RegionSetting() {} }
/*     */   
/*  39 */   public FilterButton(String imgUrl, boolean active, AbstractPlayer.PlayerClass pClass, LeaderboardType lType, RegionSetting rType) { this.img = ImageMaster.loadImage("images/ui/leaderboards/" + imgUrl);
/*  40 */     this.lType = lType;
/*  41 */     this.rType = rType;
/*  42 */     this.active = active;
/*  43 */     this.pClass = pClass;
/*     */   }
/*     */   
/*     */   public FilterButton(String imgUrl, boolean active, AbstractPlayer.PlayerClass pClass) {
/*  47 */     this(imgUrl, active, pClass, null, null);
/*     */     
/*  49 */     switch (pClass) {
/*     */     case IRONCLAD: 
/*  51 */       this.label = TEXT[0];
/*  52 */       break;
/*     */     case THE_SILENT: 
/*  54 */       this.label = TEXT[1];
/*  55 */       break;
/*     */     case DEFECT: 
/*  57 */       this.label = TEXT[2];
/*  58 */       break;
/*     */     default: 
/*  60 */       this.label = TEXT[0];
/*     */     }
/*     */   }
/*     */   
/*     */   public FilterButton(String imgUrl, boolean active, LeaderboardType lType)
/*     */   {
/*  66 */     this(imgUrl, active, null, lType, null);
/*     */     
/*  68 */     switch (lType) {
/*     */     case AVG_FLOOR: 
/*  70 */       this.label = TEXT[3];
/*  71 */       break;
/*     */     case AVG_SCORE: 
/*  73 */       this.label = TEXT[4];
/*  74 */       break;
/*     */     case CONSECUTIVE_WINS: 
/*  76 */       this.label = TEXT[5];
/*  77 */       break;
/*     */     case FASTEST_WIN: 
/*  79 */       this.label = TEXT[6];
/*  80 */       break;
/*     */     case HIGH_SCORE: 
/*  82 */       this.label = TEXT[7];
/*  83 */       break;
/*     */     case SPIRE_LEVEL: 
/*  85 */       this.label = TEXT[8];
/*  86 */       break;
/*     */     default: 
/*  88 */       this.label = TEXT[7];
/*     */     }
/*     */   }
/*     */   
/*     */   public FilterButton(String imgUrl, boolean active, RegionSetting rType)
/*     */   {
/*  94 */     this(imgUrl, active, null, null, rType);
/*     */     
/*  96 */     switch (rType) {
/*     */     case FRIEND: 
/*  98 */       this.label = TEXT[9];
/*  99 */       break;
/*     */     case GLOBAL: 
/* 101 */       this.label = TEXT[10];
/* 102 */       break;
/*     */     default: 
/* 104 */       this.label = TEXT[9];
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 110 */     this.hb.update();
/* 111 */     if ((this.hb.justHovered) && (!this.active)) {
/* 112 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/* 115 */     if (Settings.isControllerMode) {
/* 116 */       if ((!this.active) && (this.hb.hovered) && (CInputActionSet.select.isJustPressed())) {
/* 117 */         CInputActionSet.select.unpress();
/* 118 */         this.hb.clicked = true;
/*     */       }
/*     */     }
/* 121 */     else if ((!this.active) && (this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (!CardCrawlGame.mainMenuScreen.leaderboardsScreen.waiting))
/*     */     {
/* 123 */       CardCrawlGame.sound.playA("UI_CLICK_1", -0.4F);
/* 124 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/*     */ 
/* 128 */     if (this.hb.clicked) {
/* 129 */       this.hb.clicked = false;
/* 130 */       if (!this.active) {
/* 131 */         toggle();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void toggle()
/*     */   {
/* 140 */     this.active = true;
/* 141 */     CardCrawlGame.mainMenuScreen.leaderboardsScreen.refresh = true;
/*     */     
/* 143 */     if (this.pClass != null) {
/* 144 */       for (FilterButton b : CardCrawlGame.mainMenuScreen.leaderboardsScreen.charButtons) {
/* 145 */         if (b != this) {
/* 146 */           b.active = false;
/*     */         }
/*     */       }
/* 149 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.charLabel = (LeaderboardScreen.TEXT[2] + ":  " + this.label);
/* 150 */       return;
/*     */     }
/*     */     
/* 153 */     if (this.rType != null) {
/* 154 */       for (FilterButton b : CardCrawlGame.mainMenuScreen.leaderboardsScreen.regionButtons) {
/* 155 */         if (b != this) {
/* 156 */           b.active = false;
/*     */         }
/*     */       }
/* 159 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.regionLabel = (LeaderboardScreen.TEXT[3] + ":  " + this.label);
/* 160 */       return;
/*     */     }
/*     */     
/* 163 */     if (this.lType != null) {
/* 164 */       for (FilterButton b : CardCrawlGame.mainMenuScreen.leaderboardsScreen.typeButtons) {
/* 165 */         if (b != this) {
/* 166 */           b.active = false;
/*     */         }
/*     */       }
/* 169 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.typeLabel = (LeaderboardScreen.TEXT[4] + ":  " + this.label);
/* 170 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float x, float y) {
/* 175 */     if (this.active) {
/* 176 */       sb.setColor(new Color(1.0F, 0.8F, 0.2F, 0.5F + 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 181 */         (com.badlogic.gdx.math.MathUtils.cosDeg((float)(System.currentTimeMillis() / 4L % 360L)) + 1.25F) / 5.0F));
/* 182 */       sb.draw(ImageMaster.FILTER_GLOW_BG, x - 64.0F, y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 201 */     if ((this.hb.hovered) || (this.active)) {
/* 202 */       sb.setColor(Color.WHITE);
/* 203 */       sb.draw(this.img, x - 64.0F, y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 221 */       sb.setColor(Color.GRAY);
/* 222 */       sb.draw(this.img, x - 64.0F, y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 241 */     if (this.hb.hovered) {
/* 242 */       sb.setBlendFunction(770, 1);
/* 243 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.25F));
/* 244 */       sb.draw(this.img, x - 64.0F, y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 261 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 264 */     this.hb.move(x, y);
/* 265 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\leaderboards\FilterButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */