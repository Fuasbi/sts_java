/*     */ package com.megacrit.cardcrawl.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DiscardPileViewScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  24 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("DiscardPileViewScreen");
/*  25 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  27 */   public boolean isHovered = false;
/*     */   private static final int CARDS_PER_LINE = 5;
/*  29 */   private boolean grabbedScreen = false;
/*     */   private static float drawStartX;
/*  31 */   private static float drawStartY; private static float padX; private static float padY; private static final float SCROLL_BAR_THRESHOLD = 500.0F * Settings.scale;
/*  32 */   private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  33 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*  34 */   private float grabStartY = this.scrollLowerBound; private float currentDiffY = this.scrollLowerBound;
/*  35 */   private static final String HEADER_INFO = TEXT[0];
/*  36 */   private AbstractCard hoveredCard = null;
/*  37 */   private int prevDeckSize = 0;
/*     */   private ScrollBar scrollBar;
/*  39 */   private AbstractCard controllerCard = null;
/*     */   
/*     */   public DiscardPileViewScreen() {
/*  42 */     drawStartX = Settings.WIDTH;
/*  43 */     drawStartX -= 5.0F * AbstractCard.IMG_WIDTH * 0.75F;
/*  44 */     drawStartX -= 4.0F * Settings.CARD_VIEW_PAD_X;
/*  45 */     drawStartX /= 2.0F;
/*  46 */     drawStartX += AbstractCard.IMG_WIDTH * 0.75F / 2.0F;
/*     */     
/*  48 */     padX = AbstractCard.IMG_WIDTH * 0.75F + Settings.CARD_VIEW_PAD_X;
/*  49 */     padY = AbstractCard.IMG_HEIGHT * 0.75F + Settings.CARD_VIEW_PAD_Y;
/*  50 */     this.scrollBar = new ScrollBar(this);
/*  51 */     this.scrollBar.changeHeight(Settings.HEIGHT - 384.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  55 */     updateControllerInput();
/*  56 */     if ((Settings.isControllerMode) && (this.controllerCard != null) && (!CardCrawlGame.isPopupOpen) && (!AbstractDungeon.topPanel.selectPotionMode))
/*     */     {
/*  58 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.7F) {
/*  59 */         this.currentDiffY += Settings.SCROLL_SPEED;
/*  60 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.3F) {
/*  61 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*     */     
/*  65 */     boolean isDraggingScrollBar = false;
/*  66 */     if (shouldShowScrollBar()) {
/*  67 */       isDraggingScrollBar = this.scrollBar.update();
/*     */     }
/*  69 */     if (!isDraggingScrollBar) {
/*  70 */       updateScrolling();
/*     */     }
/*  72 */     updatePositions();
/*  73 */     if ((Settings.isControllerMode) && (this.controllerCard != null)) {
/*  74 */       Gdx.input.setCursorPosition((int)this.controllerCard.hb.cX, (int)(Settings.HEIGHT - this.controllerCard.hb.cY));
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/*  79 */     if ((!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode)) {
/*  80 */       return;
/*     */     }
/*     */     
/*  83 */     boolean anyHovered = false;
/*  84 */     int index = 0;
/*     */     
/*  86 */     for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/*  87 */       if (c.hb.hovered) {
/*  88 */         anyHovered = true;
/*  89 */         break;
/*     */       }
/*  91 */       index++;
/*     */     }
/*     */     
/*  94 */     if (!anyHovered) {
/*  95 */       Gdx.input.setCursorPosition(
/*  96 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(0)).hb.cX, Settings.HEIGHT - 
/*  97 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(0)).hb.cY);
/*  98 */       this.controllerCard = ((AbstractCard)AbstractDungeon.player.discardPile.group.get(0));
/*     */     }
/* 100 */     else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 101 */       (AbstractDungeon.player.discardPile.size() > 5))
/*     */     {
/* 103 */       if (index < 5) {
/* 104 */         index = AbstractDungeon.player.discardPile.size() + 2 - (4 - index);
/* 105 */         if (index > AbstractDungeon.player.discardPile.size() - 1) {
/* 106 */           index -= 5;
/*     */         }
/* 108 */         if ((index > AbstractDungeon.player.discardPile.size() - 1) || (index < 0)) {
/* 109 */           System.out.println("oops");
/* 110 */           index = 0;
/*     */         }
/*     */       }
/*     */       else {
/* 114 */         index -= 5;
/*     */       }
/* 116 */       Gdx.input.setCursorPosition(
/* 117 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 118 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cY);
/* 119 */       this.controllerCard = ((AbstractCard)AbstractDungeon.player.discardPile.group.get(index));
/* 120 */     } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 121 */       (AbstractDungeon.player.discardPile.size() > 5)) {
/* 122 */       if (index < AbstractDungeon.player.discardPile.size() - 5) {
/* 123 */         index += 5;
/*     */       } else {
/* 125 */         index %= 5;
/*     */       }
/* 127 */       Gdx.input.setCursorPosition(
/* 128 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 129 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cY);
/* 130 */       this.controllerCard = ((AbstractCard)AbstractDungeon.player.discardPile.group.get(index));
/* 131 */     } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 132 */       if (index % 5 > 0) {
/* 133 */         index--;
/*     */       } else {
/* 135 */         index += 4;
/* 136 */         if (index > AbstractDungeon.player.discardPile.size() - 1) {
/* 137 */           index = AbstractDungeon.player.discardPile.size() - 1;
/*     */         }
/*     */       }
/* 140 */       Gdx.input.setCursorPosition(
/* 141 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 142 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cY);
/* 143 */       this.controllerCard = ((AbstractCard)AbstractDungeon.player.discardPile.group.get(index));
/* 144 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 145 */       if (index % 5 < 4) {
/* 146 */         index++;
/* 147 */         if (index > AbstractDungeon.player.discardPile.size() - 1) {
/* 148 */           index -= AbstractDungeon.player.discardPile.size() % 5;
/*     */         }
/*     */       } else {
/* 151 */         index -= 4;
/* 152 */         if (index < 0) {
/* 153 */           index = 0;
/*     */         }
/*     */       }
/* 156 */       Gdx.input.setCursorPosition(
/* 157 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cX, Settings.HEIGHT - 
/* 158 */         (int)((AbstractCard)AbstractDungeon.player.discardPile.group.get(index)).hb.cY);
/* 159 */       this.controllerCard = ((AbstractCard)AbstractDungeon.player.discardPile.group.get(index));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void updatePositions()
/*     */   {
/* 168 */     this.hoveredCard = null;
/* 169 */     int lineNum = 0;
/* 170 */     ArrayList<AbstractCard> cards = AbstractDungeon.player.discardPile.group;
/* 171 */     for (int i = 0; i < cards.size(); i++) {
/* 172 */       int mod = i % 5;
/* 173 */       if ((mod == 0) && (i != 0)) {
/* 174 */         lineNum++;
/*     */       }
/* 176 */       ((AbstractCard)cards.get(i)).target_x = (drawStartX + mod * padX);
/* 177 */       ((AbstractCard)cards.get(i)).target_y = (drawStartY + this.currentDiffY - lineNum * padY);
/* 178 */       ((AbstractCard)cards.get(i)).update();
/* 179 */       ((AbstractCard)cards.get(i)).updateHoverLogic();
/* 180 */       if (((AbstractCard)cards.get(i)).hb.hovered) {
/* 181 */         this.hoveredCard = ((AbstractCard)cards.get(i));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling() {
/* 187 */     int y = InputHelper.mY;
/*     */     
/* 189 */     if (!this.grabbedScreen) {
/* 190 */       if (InputHelper.scrolledDown) {
/* 191 */         this.currentDiffY += Settings.SCROLL_SPEED;
/* 192 */       } else if (InputHelper.scrolledUp) {
/* 193 */         this.currentDiffY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 196 */       if (InputHelper.justClickedLeft) {
/* 197 */         this.grabbedScreen = true;
/* 198 */         this.grabStartY = (y - this.currentDiffY);
/*     */       }
/*     */     }
/* 201 */     else if (InputHelper.isMouseDown) {
/* 202 */       this.currentDiffY = (y - this.grabStartY);
/*     */     } else {
/* 204 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 208 */     if (this.prevDeckSize != AbstractDungeon.player.discardPile.size()) {
/* 209 */       calculateScrollBounds();
/*     */     }
/* 211 */     resetScrolling();
/* 212 */     updateBarPosition();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void calculateScrollBounds()
/*     */   {
/* 219 */     int scrollTmp = 0;
/* 220 */     if (AbstractDungeon.player.discardPile.size() > 10) {
/* 221 */       scrollTmp = AbstractDungeon.player.discardPile.size() / 5 - 2;
/* 222 */       if (AbstractDungeon.player.discardPile.size() % 5 != 0) {
/* 223 */         scrollTmp++;
/*     */       }
/* 225 */       this.scrollUpperBound = (Settings.DEFAULT_SCROLL_LIMIT + scrollTmp * padY);
/*     */     } else {
/* 227 */       this.scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */     }
/*     */     
/* 230 */     this.prevDeckSize = AbstractDungeon.player.discardPile.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 237 */     if (this.currentDiffY < this.scrollLowerBound) {
/* 238 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollLowerBound);
/* 239 */     } else if (this.currentDiffY > this.scrollUpperBound) {
/* 240 */       this.currentDiffY = MathHelper.scrollSnapLerpSpeed(this.currentDiffY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   public void reopen() {
/* 245 */     if (Settings.isControllerMode) {
/* 246 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 247 */       this.controllerCard = null;
/*     */     }
/* 249 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */   }
/*     */   
/*     */   public void open() {
/* 253 */     if (Settings.isControllerMode) {
/* 254 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/* 255 */       this.controllerCard = null;
/*     */     }
/* 257 */     CardCrawlGame.sound.play("DECK_OPEN");
/* 258 */     AbstractDungeon.overlayMenu.showBlackScreen();
/* 259 */     this.currentDiffY = this.scrollLowerBound;
/* 260 */     this.grabStartY = this.scrollLowerBound;
/* 261 */     this.grabbedScreen = false;
/* 262 */     AbstractDungeon.isScreenUp = true;
/* 263 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.DISCARD_VIEW;
/* 264 */     for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/* 265 */       c.setAngle(0.0F, true);
/* 266 */       c.targetDrawScale = 0.75F;
/* 267 */       c.drawScale = 0.75F;
/* 268 */       c.lighten(true);
/*     */     }
/* 270 */     hideCards();
/* 271 */     AbstractDungeon.overlayMenu.cancelButton.show(TEXT[1]);
/*     */     
/* 273 */     if (AbstractDungeon.player.discardPile.group.size() <= 5) {
/* 274 */       drawStartY = Settings.HEIGHT * 0.5F;
/*     */     } else {
/* 276 */       drawStartY = Settings.HEIGHT * 0.66F;
/*     */     }
/*     */     
/* 279 */     calculateScrollBounds();
/*     */   }
/*     */   
/*     */   private void hideCards() {
/* 283 */     int lineNum = 0;
/* 284 */     ArrayList<AbstractCard> cards = AbstractDungeon.player.discardPile.group;
/* 285 */     for (int i = 0; i < cards.size(); i++) {
/* 286 */       int mod = i % 5;
/* 287 */       if ((mod == 0) && (i != 0)) {
/* 288 */         lineNum++;
/*     */       }
/* 290 */       ((AbstractCard)cards.get(i)).current_x = (drawStartX + mod * padX);
/* 291 */       ((AbstractCard)cards.get(i)).current_y = (drawStartY + this.currentDiffY - lineNum * padY - com.badlogic.gdx.math.MathUtils.random(100.0F * Settings.scale, 200.0F * Settings.scale));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 298 */     if (this.hoveredCard == null) {
/* 299 */       AbstractDungeon.player.discardPile.render(sb);
/*     */     } else {
/* 301 */       AbstractDungeon.player.discardPile.renderExceptOneCard(sb, this.hoveredCard);
/* 302 */       this.hoveredCard.renderHoverShadow(sb);
/* 303 */       this.hoveredCard.render(sb);
/* 304 */       this.hoveredCard.renderCardTip(sb);
/*     */     }
/*     */     
/* 307 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 308 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.DISCARD_PILE_BANNER, 1290.0F * Settings.scale, 0.0F, 630.0F * Settings.scale, 128.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 315 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.deckBannerFont, TEXT[2], 1558.0F * Settings.scale, 82.0F * Settings.scale, new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 0.75F, 1.0F));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 323 */     FontHelper.renderDeckViewTip(sb, HEADER_INFO, 96.0F * Settings.scale, Settings.CREAM_COLOR);
/* 324 */     AbstractDungeon.overlayMenu.discardPilePanel.render(sb);
/*     */     
/* 326 */     if (shouldShowScrollBar()) {
/* 327 */       this.scrollBar.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 333 */     this.currentDiffY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 334 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 338 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.currentDiffY);
/* 339 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */   
/*     */   private boolean shouldShowScrollBar() {
/* 343 */     return this.scrollUpperBound > SCROLL_BAR_THRESHOLD;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\DiscardPileViewScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */