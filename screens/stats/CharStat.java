/*     */ package com.megacrit.cardcrawl.screens.stats;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.google.gson.Gson;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.localization.AchievementStrings;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class CharStat
/*     */ {
/*  22 */   private static final Logger logger = LogManager.getLogger(CharStat.class.getName());
/*  23 */   private static final AchievementStrings achievementStrings = CardCrawlGame.languagePack.getAchievementString("CharStat");
/*     */   
/*  25 */   public static final String[] NAMES = achievementStrings.NAMES;
/*  26 */   public static final String[] TEXT = achievementStrings.TEXT;
/*     */   public Prefs pref;
/*     */   public AbstractPlayer.PlayerClass c;
/*     */   public String info;
/*  30 */   public String info2 = null;
/*  31 */   private static Gson gson = new Gson();
/*     */   private int cardsUnlocked;
/*     */   private int relicsUnlocked;
/*     */   private int cardsDiscovered;
/*     */   private int cardsToDiscover;
/*     */   public int furthestAscent;
/*     */   public int highestScore;
/*     */   public int highestDaily;
/*     */   private int totalFloorsClimbed;
/*     */   public int numVictory;
/*     */   public int numDeath;
/*     */   public int winStreak;
/*  43 */   public int bestWinStreak; public int enemyKilled; public int bossKilled; public long playTime; public long fastestTime; private ArrayList<RunData> runs = new ArrayList();
/*     */   
/*     */   public static final String CARD_UNLOCK = "CARD_UNLOCK";
/*     */   public static final String RELIC_UNLOCK = "RELIC_UNLOCK";
/*     */   public static final String HIGHEST_FLOOR = "HIGHEST_FLOOR";
/*     */   public static final String HIGHEST_SCORE = "HIGHEST_SCORE";
/*     */   public static final String HIGHEST_DAILY = "HIGHEST_DAILY";
/*     */   public static final String TOTAL_FLOORS = "TOTAL_FLOORS";
/*     */   public static final String TOTAL_CRYSTALS_FED = "TOTAL_CRYSTALS_FED";
/*     */   public static final String WIN_COUNT = "WIN_COUNT";
/*     */   public static final String LOSE_COUNT = "LOSE_COUNT";
/*     */   public static final String WIN_STREAK = "WIN_STREAK";
/*     */   public static final String BEST_WIN_STREAK = "BEST_WIN_STREAK";
/*     */   public static final String ASCENSION_LEVEL = "ASCENSION_LEVEL";
/*     */   public static final String ENEMY_KILL = "ENEMY_KILL";
/*     */   public static final String BOSS_KILL = "BOSS_KILL";
/*     */   public static final String PLAYTIME = "PLAYTIME";
/*     */   public static final String FASTEST_VICTORY = "FAST_VICTORY";
/*     */   
/*     */   public CharStat(ArrayList<CharStat> allChars)
/*     */   {
/*  64 */     this.cardsUnlocked = 0;
/*  65 */     this.relicsUnlocked = 0;
/*  66 */     this.furthestAscent = 0;
/*  67 */     this.highestScore = 0;
/*  68 */     this.totalFloorsClimbed = 0;
/*  69 */     this.numVictory = 0;
/*  70 */     this.numDeath = 0;
/*  71 */     this.enemyKilled = 0;
/*  72 */     this.bossKilled = 0;
/*  73 */     this.playTime = 0L;
/*  74 */     this.fastestTime = 999999999999L;
/*     */     
/*  76 */     int highestFloorTmp = 0;
/*  77 */     int highestDailyTmp = 0;
/*     */     
/*  79 */     for (CharStat stat : allChars) {
/*  80 */       this.cardsUnlocked += stat.cardsUnlocked;
/*  81 */       this.relicsUnlocked += stat.relicsUnlocked;
/*     */       
/*  83 */       if (stat.furthestAscent > highestFloorTmp) {
/*  84 */         this.furthestAscent = stat.furthestAscent;
/*  85 */         highestFloorTmp = this.furthestAscent;
/*     */       }
/*     */       
/*  88 */       if (stat.highestDaily > highestDailyTmp) {
/*  89 */         this.highestDaily = stat.highestDaily;
/*  90 */         highestDailyTmp = this.highestDaily;
/*     */       }
/*     */       
/*  93 */       if ((stat.fastestTime < this.fastestTime) && (stat.fastestTime != 0L)) {
/*  94 */         this.fastestTime = stat.fastestTime;
/*     */       }
/*     */       
/*  97 */       this.totalFloorsClimbed += stat.totalFloorsClimbed;
/*  98 */       this.numVictory += stat.numVictory;
/*  99 */       this.numDeath += stat.numDeath;
/* 100 */       this.enemyKilled += stat.enemyKilled;
/* 101 */       this.bossKilled += stat.bossKilled;
/* 102 */       this.playTime += stat.playTime;
/*     */     }
/*     */     
/* 105 */     this.info = (TEXT[0] + formatHMSM(this.playTime) + " NL ");
/* 106 */     this.info = (this.info + TEXT[1] + this.numVictory + " NL ");
/* 107 */     this.info = (this.info + TEXT[2] + this.numDeath + " NL ");
/* 108 */     this.info = (this.info + TEXT[3] + this.totalFloorsClimbed + " NL ");
/* 109 */     this.info = (this.info + TEXT[4] + this.bossKilled + " NL ");
/* 110 */     this.info = (this.info + TEXT[5] + this.enemyKilled + " NL ");
/*     */     
/* 112 */     this.info2 = (TEXT[7] + UnlockTracker.getCardsSeenString() + " NL ");
/*     */     
/* 114 */     int unlockedCardCount = UnlockTracker.unlockedRedCardCount + UnlockTracker.unlockedGreenCardCount + UnlockTracker.unlockedBlueCardCount;
/*     */     
/* 116 */     int lockedCardCount = UnlockTracker.lockedRedCardCount + UnlockTracker.lockedGreenCardCount + UnlockTracker.lockedBlueCardCount;
/*     */     
/*     */ 
/* 119 */     this.info2 = (this.info2 + TEXT[8] + unlockedCardCount + "/" + lockedCardCount + " NL ");
/* 120 */     this.info2 = (this.info2 + TEXT[9] + UnlockTracker.getRelicsSeenString() + " NL ");
/* 121 */     this.info2 = (this.info2 + TEXT[10] + UnlockTracker.unlockedRelicCount + "/" + UnlockTracker.lockedRelicCount + " NL ");
/*     */     
/* 123 */     if (this.fastestTime != 999999999999L) {
/* 124 */       this.info2 = (this.info2 + TEXT[13] + formatHMSM(this.fastestTime) + " NL ");
/*     */     }
/*     */   }
/*     */   
/*     */   public CharStat(AbstractPlayer.PlayerClass c) {
/* 129 */     this.c = c;
/* 130 */     switch (c) {
/*     */     case IRONCLAD: 
/* 132 */       this.pref = CardCrawlGame.ironcladPrefs;
/* 133 */       break;
/*     */     case THE_SILENT: 
/* 135 */       this.pref = CardCrawlGame.silentPrefs;
/* 136 */       break;
/*     */     case DEFECT: 
/* 138 */       this.pref = CardCrawlGame.defectPrefs;
/*     */     }
/*     */     
/*     */     
/* 142 */     this.cardsUnlocked = calculateCardsUnlocked(c);
/* 143 */     this.cardsDiscovered = getSeenCardCount(c);
/* 144 */     this.cardsToDiscover = getCardCountForChar(c);
/* 145 */     this.relicsUnlocked = this.pref.getInteger("RELIC_UNLOCK", 0);
/* 146 */     this.furthestAscent = this.pref.getInteger("HIGHEST_FLOOR", 0);
/* 147 */     this.highestDaily = this.pref.getInteger("HIGHEST_DAILY", 0);
/* 148 */     this.totalFloorsClimbed = this.pref.getInteger("TOTAL_FLOORS", 0);
/* 149 */     this.numVictory = this.pref.getInteger("WIN_COUNT", 0);
/* 150 */     this.numDeath = this.pref.getInteger("LOSE_COUNT", 0);
/* 151 */     this.winStreak = this.pref.getInteger("WIN_STREAK", 0);
/* 152 */     this.bestWinStreak = this.pref.getInteger("BEST_WIN_STREAK", 0);
/* 153 */     this.enemyKilled = this.pref.getInteger("ENEMY_KILL", 0);
/* 154 */     this.bossKilled = this.pref.getInteger("BOSS_KILL", 0);
/* 155 */     this.playTime = this.pref.getLong("PLAYTIME", 0L);
/* 156 */     this.fastestTime = this.pref.getLong("FAST_VICTORY", 0L);
/* 157 */     this.highestScore = this.pref.getInteger("HIGHEST_SCORE", 0);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 171 */     this.info = (TEXT[0] + formatHMSM(this.playTime) + " NL ");
/*     */     
/* 173 */     this.info = (this.info + TEXT[7] + this.cardsDiscovered + "/" + this.cardsToDiscover + " NL ");
/* 174 */     this.info = (this.info + TEXT[8] + this.cardsUnlocked + "/" + UnlockTracker.lockedRedCardCount + " NL ");
/*     */     
/*     */ 
/* 177 */     if (this.fastestTime != 0L) {
/* 178 */       this.info = (this.info + TEXT[13] + formatHMSM(this.fastestTime) + " NL ");
/*     */     }
/* 180 */     this.info = (this.info + TEXT[23] + this.highestScore + " NL ");
/* 181 */     if (this.bestWinStreak > 0) {
/* 182 */       this.info = (this.info + TEXT[22] + this.bestWinStreak + " NL ");
/*     */     }
/*     */     
/* 185 */     this.info2 = (TEXT[17] + this.numVictory + " NL ");
/* 186 */     this.info2 = (this.info2 + TEXT[18] + this.numDeath + " NL ");
/* 187 */     this.info2 = (this.info2 + TEXT[19] + this.totalFloorsClimbed + " NL ");
/* 188 */     this.info2 = (this.info2 + TEXT[20] + this.bossKilled + " NL ");
/* 189 */     this.info2 = (this.info2 + TEXT[21] + this.enemyKilled + " NL ");
/*     */     
/* 191 */     StringBuilder sb = new StringBuilder();
/* 192 */     sb.append("runs" + File.separator);
/*     */     
/* 194 */     switch (CardCrawlGame.saveSlot)
/*     */     {
/*     */     case 0: 
/*     */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/*     */     case 2: 
/*     */     case 3: 
/*     */     default: 
/* 205 */       sb.append(CardCrawlGame.saveSlot + "_");
/*     */     }
/*     */     
/*     */     
/* 209 */     sb.append(c.name() + File.separator);
/*     */     
/* 211 */     FileHandle[] files = Gdx.files.local(sb.toString()).list();
/* 212 */     for (int i = 0; i < files.length; i++) {
/*     */       try {
/* 214 */         this.runs.add(gson.fromJson(files[i].readString(), RunData.class));
/*     */       } catch (Exception e) {
/* 216 */         files[i].delete();
/* 217 */         logger.warn("Deleted corrupt .run file, preventing crash!", e);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private int calculateCardsUnlocked(AbstractPlayer.PlayerClass c2) {
/* 223 */     switch (c2) {
/*     */     case IRONCLAD: 
/* 225 */       return UnlockTracker.unlockedRedCardCount;
/*     */     case THE_SILENT: 
/* 227 */       return UnlockTracker.unlockedGreenCardCount;
/*     */     case DEFECT: 
/* 229 */       return UnlockTracker.unlockedBlueCardCount;
/*     */     }
/* 231 */     return 0;
/*     */   }
/*     */   
/*     */   private int getSeenCardCount(AbstractPlayer.PlayerClass c)
/*     */   {
/* 236 */     switch (c) {
/*     */     case IRONCLAD: 
/* 238 */       return CardLibrary.seenRedCards;
/*     */     case THE_SILENT: 
/* 240 */       return CardLibrary.seenGreenCards;
/*     */     case DEFECT: 
/* 242 */       return CardLibrary.seenBlueCards;
/*     */     }
/* 244 */     return 0;
/*     */   }
/*     */   
/*     */   private int getCardCountForChar(AbstractPlayer.PlayerClass c)
/*     */   {
/* 249 */     switch (c) {
/*     */     case IRONCLAD: 
/* 251 */       return CardLibrary.redCards;
/*     */     case THE_SILENT: 
/* 253 */       return CardLibrary.greenCards;
/*     */     case DEFECT: 
/* 255 */       return CardLibrary.blueCards;
/*     */     }
/* 257 */     return 0;
/*     */   }
/*     */   
/*     */   public void highestScore(int score)
/*     */   {
/* 262 */     if (score > this.highestScore) {
/* 263 */       this.highestScore = score;
/* 264 */       this.pref.putInteger("HIGHEST_SCORE", this.highestScore);
/* 265 */       this.pref.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   public void furthestAscent(int floor) {
/* 270 */     if (floor > this.furthestAscent) {
/* 271 */       this.furthestAscent = floor;
/* 272 */       this.pref.putInteger("HIGHEST_FLOOR", this.furthestAscent);
/* 273 */       this.pref.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   public void highestDaily(int score) {
/* 278 */     if (score > this.highestDaily) {
/* 279 */       this.highestDaily = score;
/* 280 */       this.pref.putInteger("HIGHEST_DAILY", this.highestDaily);
/* 281 */       this.pref.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   public void incrementFloorClimbed() {
/* 286 */     this.totalFloorsClimbed += 1;
/* 287 */     this.pref.putInteger("TOTAL_FLOORS", this.totalFloorsClimbed);
/* 288 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public void incrementDeath() {
/* 292 */     this.numDeath += 1;
/*     */     
/* 294 */     if (!AbstractDungeon.isAscensionMode) {
/* 295 */       this.winStreak = 0;
/* 296 */       this.pref.putInteger("WIN_STREAK", this.winStreak);
/*     */     }
/*     */     
/* 299 */     this.pref.putInteger("LOSE_COUNT", this.numDeath);
/* 300 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public int getVictoryCount() {
/* 304 */     return this.numVictory;
/*     */   }
/*     */   
/*     */   public void unlockAscension() {
/* 308 */     this.pref.putInteger("ASCENSION_LEVEL", 1);
/*     */   }
/*     */   
/*     */   public void incrementVictory() {
/* 312 */     this.numVictory += 1;
/*     */     
/* 314 */     if (!AbstractDungeon.isAscensionMode) {
/* 315 */       this.winStreak += 1;
/* 316 */       this.pref.putInteger("WIN_STREAK", this.winStreak);
/*     */       
/* 318 */       if (this.winStreak > this.pref.getInteger("BEST_WIN_STREAK", 0)) {
/* 319 */         this.pref.putInteger("BEST_WIN_STREAK", this.winStreak);
/*     */       }
/*     */     }
/* 322 */     else if (!Settings.isTrial) {
/* 323 */       int derp = this.pref.getInteger("ASCENSION_LEVEL", 1);
/* 324 */       if (derp == AbstractDungeon.ascensionLevel) {
/* 325 */         derp++;
/*     */         
/* 327 */         if (derp <= 20) {
/* 328 */           this.pref.putInteger("ASCENSION_LEVEL", derp);
/* 329 */           logger.info("ASCENSION LEVEL IS NOW: " + derp);
/*     */         }
/*     */         else {
/* 332 */           this.pref.putInteger("ASCENSION_LEVEL", 20);
/* 333 */           logger.info("MAX ASCENSION");
/*     */         }
/*     */       } else {
/* 336 */         logger.info("Played Ascension that wasn't Max");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 341 */     this.pref.putInteger("WIN_COUNT", this.numVictory);
/* 342 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public void incrementBossSlain() {
/* 346 */     this.bossKilled += 1;
/* 347 */     this.pref.putInteger("BOSS_KILL", this.bossKilled);
/* 348 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public void incrementEnemySlain() {
/* 352 */     this.enemyKilled += 1;
/* 353 */     this.pref.putInteger("ENEMY_KILL", this.enemyKilled);
/* 354 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public void incrementPlayTime(long time) {
/* 358 */     this.playTime += time;
/* 359 */     this.pref.putLong("PLAYTIME", this.playTime);
/* 360 */     this.pref.flush();
/*     */   }
/*     */   
/*     */   public static String formatHMSM(float t) {
/* 364 */     String res = "";
/* 365 */     long duration = t;
/* 366 */     int seconds = (int)(duration % 60L);
/* 367 */     duration /= 60L;
/* 368 */     int minutes = (int)(duration % 60L);
/* 369 */     int hours = (int)t / 3600;
/*     */     
/* 371 */     if (hours > 0) {
/* 372 */       res = String.format("%02d:%02d:%02d", new Object[] { Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds) });
/*     */     } else {
/* 374 */       res = String.format("%02d:%02d", new Object[] { Integer.valueOf(minutes), Integer.valueOf(seconds) });
/*     */     }
/* 376 */     return res;
/*     */   }
/*     */   
/*     */   public static String formatHMSM(long t) {
/* 380 */     String res = "";
/* 381 */     long duration = t;
/* 382 */     int seconds = (int)(duration % 60L);
/* 383 */     duration /= 60L;
/* 384 */     int minutes = (int)(duration % 60L);
/* 385 */     int hours = (int)t / 3600;
/*     */     
/* 387 */     if (hours > 0) {
/* 388 */       res = String.format("#y%02dh #y%02dm #y%02ds", new Object[] { Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds) });
/*     */     } else {
/* 390 */       res = String.format("#y%02dm #y%02ds", new Object[] { Integer.valueOf(minutes), Integer.valueOf(seconds) });
/*     */     }
/* 392 */     return res;
/*     */   }
/*     */   
/*     */   public static String formatHMSM(int t) {
/* 396 */     String res = "";
/* 397 */     long duration = t;
/* 398 */     int seconds = (int)(duration % 60L);
/* 399 */     duration /= 60L;
/* 400 */     int minutes = (int)(duration % 60L);
/* 401 */     int hours = t / 3600;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 408 */     res = String.format("%01d:%02d:%02d", new Object[] { Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds) });
/* 409 */     return res;
/*     */   }
/*     */   
/*     */   public void updateFastestVictory(long newTime) {
/* 413 */     if ((newTime < this.fastestTime) || (this.fastestTime == 0L)) {
/* 414 */       this.fastestTime = newTime;
/* 415 */       this.pref.putLong("FAST_VICTORY", this.fastestTime);
/* 416 */       this.pref.flush();
/* 417 */       logger.info("Fastest victory time updated to: " + this.fastestTime);
/*     */     } else {
/* 419 */       logger.info("Did not save fastest victory.");
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\stats\CharStat.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */