/*     */ package com.megacrit.cardcrawl.screens.stats;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.localization.AchievementStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class AchievementGrid
/*     */ {
/*  11 */   private static final AchievementStrings achievementStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getAchievementString("AchievementGrid");
/*     */   
/*  13 */   public static final String[] NAMES = achievementStrings.NAMES;
/*  14 */   public static final String[] TEXT = achievementStrings.TEXT;
/*     */   
/*  16 */   public ArrayList<AchievementItem> items = new ArrayList();
/*  17 */   private static final float SPACING = 200.0F * Settings.scale;
/*     */   
/*     */   private static final int ITEMS_PER_ROW = 5;
/*     */   
/*     */   public static final String SHRUG_KEY = "SHRUG_IT_OFF";
/*     */   
/*     */   public static final String PURITY_KEY = "PURITY";
/*     */   
/*     */   public static final String COME_AT_ME_KEY = "COME_AT_ME";
/*     */   
/*     */   public static final String THE_PACT_KEY = "THE_PACT";
/*     */   public static final String ADRENALINE_KEY = "ADRENALINE";
/*     */   public static final String POWERFUL_KEY = "POWERFUL";
/*     */   public static final String JAXXED_KEY = "JAXXED";
/*     */   public static final String IMPERVIOUS_KEY = "IMPERVIOUS";
/*     */   public static final String BARRICADED_KEY = "BARRICADED";
/*     */   public static final String CATALYST_KEY = "CATALYST";
/*     */   public static final String PLAGUE_KEY = "PLAGUE";
/*     */   public static final String NINJA_KEY = "NINJA";
/*     */   public static final String INFINITY_KEY = "INFINITY";
/*     */   public static final String YOU_ARE_NOTHING_KEY = "YOU_ARE_NOTHING";
/*     */   public static final String PERFECT_KEY = "PERFECT";
/*     */   public static final String ONE_RELIC_KEY = "ONE_RELIC";
/*     */   public static final String SPEED_CLIMBER_KEY = "SPEED_CLIMBER";
/*     */   public static final String ASCEND_0_KEY = "ASCEND_0";
/*     */   public static final String ASCEND_10_KEY = "ASCEND_10";
/*     */   public static final String ASCEND_20_KEY = "ASCEND_20";
/*     */   public static final String MINMALIST_KEY = "MINIMALIST";
/*     */   public static final String DONUT_KEY = "DONUT";
/*     */   public static final String COMMON_SENSE_KEY = "COMMON_SENSE";
/*     */   public static final String FOCUSED_KEY = "FOCUSED";
/*     */   public static final String LUCKY_DAY_KEY = "LUCKY_DAY";
/*     */   public static final String NEON_KEY = "NEON";
/*     */   public static final String TRANSIENT_KEY = "TRANSIENT";
/*     */   public static final String GUARDIAN_KEY = "GUARDIAN";
/*     */   public static final String GHOST_GUARDIAN_KEY = "GHOST_GUARDIAN";
/*     */   public static final String SLIME_BOSS_KEY = "SLIME_BOSS";
/*     */   public static final String AUTOMATON_KEY = "AUTOMATON";
/*     */   public static final String COLLECTOR_KEY = "COLLECTOR";
/*     */   public static final String CHAMP_KEY = "CHAMP";
/*     */   public static final String CROW_KEY = "CROW";
/*     */   public static final String SHAPES_KEY = "SHAPES";
/*     */   public static final String TIME_EATER_KEY = "TIME_EATER";
/*     */   public static final String RUBY_KEY = "RUBY";
/*     */   public static final String EMERALD_KEY = "EMERALD";
/*     */   public static final String SAPPHIRE_KEY = "SAPPHIRE";
/*     */   
/*     */   public AchievementGrid()
/*     */   {
/*  66 */     this.items.add(new AchievementItem(NAMES[0], TEXT[0], "shrugItOff.jpg", "SHRUG_IT_OFF"));
/*  67 */     this.items.add(new AchievementItem(NAMES[1], TEXT[1], "purity.jpg", "PURITY"));
/*  68 */     this.items.add(new AchievementItem(NAMES[2], TEXT[2], "comeAtMe.jpg", "COME_AT_ME"));
/*  69 */     this.items.add(new AchievementItem(NAMES[3], TEXT[3], "thePact.jpg", "THE_PACT"));
/*  70 */     this.items.add(new AchievementItem(NAMES[4], TEXT[4], "adrenaline.jpg", "ADRENALINE"));
/*  71 */     this.items.add(new AchievementItem(NAMES[5], TEXT[5], "powerful.jpg", "POWERFUL"));
/*  72 */     this.items.add(new AchievementItem(NAMES[6], TEXT[6], "jaxxed.jpg", "JAXXED"));
/*  73 */     this.items.add(new AchievementItem(NAMES[7], TEXT[7], "impervious.jpg", "IMPERVIOUS"));
/*  74 */     this.items.add(new AchievementItem(NAMES[8], TEXT[8], "barricaded.jpg", "BARRICADED"));
/*  75 */     this.items.add(new AchievementItem(NAMES[9], TEXT[9], "catalyst.jpg", "CATALYST"));
/*  76 */     this.items.add(new AchievementItem(NAMES[10], TEXT[10], "plague.jpg", "PLAGUE"));
/*  77 */     this.items.add(new AchievementItem(NAMES[11], TEXT[11], "ninja.jpg", "NINJA"));
/*  78 */     this.items.add(new AchievementItem(NAMES[12], TEXT[12], "infinity.jpg", "INFINITY"));
/*  79 */     this.items.add(new AchievementItem(NAMES[35], TEXT[35], "focused.jpg", "FOCUSED"));
/*  80 */     this.items.add(new AchievementItem(NAMES[36], TEXT[36], "neon.jpg", "NEON"));
/*  81 */     this.items.add(new AchievementItem(NAMES[13], TEXT[13], "youAreNothing.jpg", "YOU_ARE_NOTHING"));
/*  82 */     this.items.add(new AchievementItem(NAMES[31], TEXT[31], "minimalist.jpg", "MINIMALIST"));
/*  83 */     this.items.add(new AchievementItem(NAMES[32], TEXT[32], "donut.jpg", "DONUT"));
/*  84 */     this.items.add(new AchievementItem(NAMES[14], TEXT[14], "perfect.jpg", "PERFECT"));
/*  85 */     this.items.add(new AchievementItem(NAMES[27], TEXT[27], "onerelic.jpg", "ONE_RELIC"));
/*  86 */     this.items.add(new AchievementItem(NAMES[28], TEXT[28], "speed.jpg", "SPEED_CLIMBER"));
/*  87 */     this.items.add(new AchievementItem(NAMES[34], TEXT[34], "commonSense.jpg", "COMMON_SENSE"));
/*  88 */     this.items.add(new AchievementItem(NAMES[38], TEXT[38], "luckyDay.jpg", "LUCKY_DAY"));
/*  89 */     this.items.add(new AchievementItem(NAMES[29], TEXT[29], "0.jpg", "ASCEND_0"));
/*  90 */     this.items.add(new AchievementItem(NAMES[30], TEXT[30], "10.jpg", "ASCEND_10"));
/*  91 */     this.items.add(new AchievementItem(NAMES[40], TEXT[40], "20.jpg", "ASCEND_20"));
/*  92 */     this.items.add(new AchievementItem(NAMES[39], TEXT[39], "transient.jpg", "TRANSIENT"));
/*  93 */     this.items.add(new AchievementItem(NAMES[15], TEXT[15], "guardian.jpg", "GUARDIAN", true));
/*  94 */     this.items.add(new AchievementItem(NAMES[16], TEXT[16], "ghostGuardian.jpg", "GHOST_GUARDIAN", true));
/*  95 */     this.items.add(new AchievementItem(NAMES[17], TEXT[17], "slimeBoss.jpg", "SLIME_BOSS", true));
/*  96 */     this.items.add(new AchievementItem(NAMES[18], TEXT[18], "automaton.jpg", "AUTOMATON", true));
/*  97 */     this.items.add(new AchievementItem(NAMES[19], TEXT[19], "collector.jpg", "COLLECTOR", true));
/*  98 */     this.items.add(new AchievementItem(NAMES[20], TEXT[20], "champ.jpg", "CHAMP", true));
/*  99 */     this.items.add(new AchievementItem(NAMES[21], TEXT[21], "awakenedOne.jpg", "CROW", true));
/* 100 */     this.items.add(new AchievementItem(NAMES[22], TEXT[22], "shapes.jpg", "SHAPES", true));
/* 101 */     this.items.add(new AchievementItem(NAMES[23], TEXT[23], "timeEater.jpg", "TIME_EATER", true));
/* 102 */     this.items.add(new AchievementItem(NAMES[24], TEXT[24], "ironclad.jpg", "RUBY"));
/* 103 */     this.items.add(new AchievementItem(NAMES[25], TEXT[25], "silent.jpg", "EMERALD"));
/* 104 */     this.items.add(new AchievementItem(NAMES[33], TEXT[33], "sapphire.jpg", "SAPPHIRE"));
/*     */   }
/*     */   
/*     */   public void update() {
/* 108 */     for (AchievementItem i : this.items) {
/* 109 */       i.update();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float renderY)
/*     */   {
/* 115 */     for (int i = 0; i < this.items.size(); i++) {
/* 116 */       ((AchievementItem)this.items.get(i)).render(sb, 560.0F * Settings.scale + i % 5 * SPACING, renderY - i / 5 * SPACING + 680.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\stats\AchievementGrid.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */