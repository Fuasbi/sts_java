/*     */ package com.megacrit.cardcrawl.screens.stats;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.AchievementStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class StatsScreen implements com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  29 */   private static final Logger logger = LogManager.getLogger(StatsScreen.class.getName());
/*  30 */   private static final AchievementStrings achievementStrings = CardCrawlGame.languagePack.getAchievementString("StatsScreen");
/*     */   
/*  32 */   public static final String[] NAMES = achievementStrings.NAMES;
/*  33 */   public static final String[] TEXT = achievementStrings.TEXT;
/*  34 */   public MenuCancelButton button = new MenuCancelButton();
/*     */   
/*     */ 
/*  37 */   private Hitbox allCharsHb = new Hitbox(150.0F * Settings.scale, 150.0F * Settings.scale); private Hitbox ironcladHb = new Hitbox(150.0F * Settings.scale, 150.0F * Settings.scale);
/*     */   
/*     */   private Hitbox silentHb;
/*     */   private Hitbox defectHb;
/*     */   private Hitbox controllerHb;
/*  42 */   public AbstractPlayer.PlayerClass currentChar = null;
/*  43 */   public boolean screenUp = false;
/*  44 */   private static final float SHOW_X = 300.0F * Settings.scale; private static final float HIDE_X = -800.0F * Settings.scale;
/*  45 */   private float screenX = HIDE_X; private float targetX = HIDE_X;
/*     */   
/*     */ 
/*  48 */   private boolean grabbedScreen = false;
/*  49 */   private float grabStartY = 0.0F; private float scrollTargetY = 0.0F; private float scrollY = 0.0F;
/*  50 */   private float renderY; private float scrollLowerBound = -Settings.DEFAULT_SCROLL_LIMIT;
/*  51 */   private float scrollUpperBound = Settings.DEFAULT_SCROLL_LIMIT;
/*     */   private ScrollBar scrollBar;
/*     */   public static CharStat all;
/*     */   public static CharStat ironclad;
/*     */   public static CharStat silent;
/*     */   public static CharStat defect;
/*     */   public static AchievementGrid achievements;
/*     */   
/*  59 */   public StatsScreen() { refreshData();
/*  60 */     calculateScrollBounds();
/*  61 */     this.scrollBar = new ScrollBar(this);
/*     */   }
/*     */   
/*     */   public void refreshData() {
/*  65 */     ironclad = new CharStat(AbstractPlayer.PlayerClass.IRONCLAD);
/*  66 */     silent = new CharStat(AbstractPlayer.PlayerClass.THE_SILENT);
/*  67 */     defect = new CharStat(AbstractPlayer.PlayerClass.DEFECT);
/*  68 */     ArrayList<CharStat> allChars = new ArrayList();
/*  69 */     allChars.add(ironclad);
/*  70 */     allChars.add(silent);
/*  71 */     allChars.add(defect);
/*  72 */     all = new CharStat(allChars);
/*  73 */     achievements = new AchievementGrid();
/*  74 */     Settings.totalPlayTime = all.playTime;
/*     */   }
/*     */   
/*     */   public void update() {
/*  78 */     updateControllerInput();
/*  79 */     if ((Settings.isControllerMode) && (this.controllerHb != null)) {
/*  80 */       if (Gdx.input.getY() > Settings.HEIGHT * 0.75F) {
/*  81 */         this.scrollTargetY += Settings.SCROLL_SPEED;
/*  82 */       } else if (Gdx.input.getY() < Settings.HEIGHT * 0.25F) {
/*  83 */         this.scrollTargetY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */     }
/*  86 */     if ((Settings.isControllerMode) && (this.controllerHb != null)) {
/*  87 */       Gdx.input.setCursorPosition((int)this.controllerHb.cX, (int)(Settings.HEIGHT - this.controllerHb.cY));
/*     */     }
/*     */     
/*  90 */     this.button.update();
/*     */     
/*  92 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape)) {
/*  93 */       InputHelper.pressedEscape = false;
/*  94 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.MAIN_MENU;
/*  95 */       hide();
/*     */     }
/*     */     
/*  98 */     this.screenX = MathHelper.uiLerpSnap(this.screenX, this.targetX);
/*  99 */     boolean isDraggingScrollBar = this.scrollBar.update();
/* 100 */     if (!isDraggingScrollBar) {
/* 101 */       updateScrolling();
/*     */     }
/* 103 */     achievements.update();
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 107 */     if (!Settings.isControllerMode) {
/* 108 */       return;
/*     */     }
/*     */     
/* 111 */     boolean anyHovered = false;
/* 112 */     int index = 0;
/*     */     
/* 114 */     this.allCharsHb.update();
/* 115 */     this.ironcladHb.update();
/* 116 */     if (this.silentHb != null) {
/* 117 */       this.silentHb.update();
/*     */     }
/* 119 */     if (this.defectHb != null) {
/* 120 */       this.defectHb.update();
/*     */     }
/*     */     
/*     */ 
/* 124 */     if ((this.allCharsHb != null) && 
/* 125 */       (this.allCharsHb.hovered)) {
/* 126 */       anyHovered = true;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 131 */     index = 0;
/* 132 */     if (!anyHovered) {
/* 133 */       for (AchievementItem a : achievements.items) {
/* 134 */         a.hb.update();
/* 135 */         if (a.hb.hovered) {
/* 136 */           anyHovered = true;
/* 137 */           break;
/*     */         }
/* 139 */         index++;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 144 */     if ((!anyHovered) && (this.ironcladHb != null) && (this.ironcladHb.hovered)) {
/* 145 */       anyHovered = true;
/*     */     }
/*     */     
/*     */ 
/* 149 */     if ((!anyHovered) && (this.silentHb != null) && (this.silentHb.hovered)) {
/* 150 */       anyHovered = true;
/*     */     }
/*     */     
/*     */ 
/* 154 */     if ((!anyHovered) && (this.defectHb != null) && (this.defectHb.hovered)) {
/* 155 */       anyHovered = true;
/*     */     }
/*     */     
/* 158 */     if (!anyHovered) {
/* 159 */       Gdx.input.setCursorPosition((int)this.allCharsHb.cX, Settings.HEIGHT - (int)this.allCharsHb.cY);
/* 160 */       this.controllerHb = this.allCharsHb;
/*     */     }
/* 162 */     else if (this.allCharsHb.hovered) {
/* 163 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 164 */         Gdx.input.setCursorPosition(
/* 165 */           (int)((AchievementItem)achievements.items.get(0)).hb.cX, Settings.HEIGHT - 
/* 166 */           (int)((AchievementItem)achievements.items.get(0)).hb.cY);
/* 167 */         this.controllerHb = ((AchievementItem)achievements.items.get(0)).hb;
/*     */       }
/* 169 */     } else if (this.ironcladHb.hovered) {
/* 170 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 171 */         index = achievements.items.size() - achievements.items.size() % 5;
/* 172 */         Gdx.input.setCursorPosition(
/* 173 */           (int)((AchievementItem)achievements.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 174 */           (int)((AchievementItem)achievements.items.get(index)).hb.cY);
/* 175 */         this.controllerHb = ((AchievementItem)achievements.items.get(index)).hb;
/* 176 */       } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && (this.silentHb != null))
/*     */       {
/* 178 */         Gdx.input.setCursorPosition((int)this.silentHb.cX, Settings.HEIGHT - (int)this.silentHb.cY);
/* 179 */         this.controllerHb = this.silentHb;
/*     */       }
/* 181 */     } else if ((this.silentHb != null) && (this.silentHb.hovered)) {
/* 182 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 183 */         Gdx.input.setCursorPosition((int)this.ironcladHb.cX, Settings.HEIGHT - (int)this.ironcladHb.cY);
/* 184 */         this.controllerHb = this.ironcladHb;
/* 185 */       } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && (this.defectHb != null))
/*     */       {
/* 187 */         Gdx.input.setCursorPosition((int)this.defectHb.cX, Settings.HEIGHT - (int)this.defectHb.cY);
/* 188 */         this.controllerHb = this.defectHb;
/*     */       }
/* 190 */     } else if ((this.defectHb != null) && (this.defectHb.hovered)) {
/* 191 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 192 */         Gdx.input.setCursorPosition((int)this.silentHb.cX, Settings.HEIGHT - (int)this.silentHb.cY);
/* 193 */         this.controllerHb = this.silentHb;
/*     */       }
/*     */     }
/* 196 */     else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 197 */       index -= 5;
/* 198 */       if (index < 0) {
/* 199 */         Gdx.input.setCursorPosition((int)this.allCharsHb.cX, Settings.HEIGHT - (int)this.allCharsHb.cY);
/* 200 */         this.controllerHb = this.allCharsHb;
/*     */       } else {
/* 202 */         Gdx.input.setCursorPosition(
/* 203 */           (int)((AchievementItem)achievements.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 204 */           (int)((AchievementItem)achievements.items.get(index)).hb.cY);
/* 205 */         this.controllerHb = ((AchievementItem)achievements.items.get(index)).hb;
/*     */       }
/* 207 */     } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 208 */       index += 5;
/* 209 */       if (index > achievements.items.size() - 1) {
/* 210 */         Gdx.input.setCursorPosition((int)this.ironcladHb.cX, Settings.HEIGHT - (int)this.ironcladHb.cY);
/* 211 */         this.controllerHb = this.ironcladHb;
/*     */       } else {
/* 213 */         Gdx.input.setCursorPosition(
/* 214 */           (int)((AchievementItem)achievements.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 215 */           (int)((AchievementItem)achievements.items.get(index)).hb.cY);
/* 216 */         this.controllerHb = ((AchievementItem)achievements.items.get(index)).hb;
/*     */       }
/* 218 */     } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 219 */       index--;
/* 220 */       if ((index % 5 == 4) || (index == -1)) {
/* 221 */         index += 5;
/* 222 */         if (index > achievements.items.size() - 1) {
/* 223 */           index = achievements.items.size() - 1;
/*     */         }
/*     */       }
/* 226 */       Gdx.input.setCursorPosition(
/* 227 */         (int)((AchievementItem)achievements.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 228 */         (int)((AchievementItem)achievements.items.get(index)).hb.cY);
/* 229 */       this.controllerHb = ((AchievementItem)achievements.items.get(index)).hb;
/* 230 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 231 */       index++;
/* 232 */       if (index % 5 == 0) {
/* 233 */         index -= 5;
/*     */       }
/* 235 */       if (index > achievements.items.size() - 1) {
/* 236 */         index = achievements.items.size() - achievements.items.size() % 5;
/*     */       }
/* 238 */       Gdx.input.setCursorPosition(
/* 239 */         (int)((AchievementItem)achievements.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 240 */         (int)((AchievementItem)achievements.items.get(index)).hb.cY);
/* 241 */       this.controllerHb = ((AchievementItem)achievements.items.get(index)).hb;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void updateScrolling()
/*     */   {
/* 248 */     int y = InputHelper.mY;
/*     */     
/* 250 */     if (!this.grabbedScreen) {
/* 251 */       if (InputHelper.scrolledDown) {
/* 252 */         this.scrollTargetY += Settings.SCROLL_SPEED;
/* 253 */       } else if (InputHelper.scrolledUp) {
/* 254 */         this.scrollTargetY -= Settings.SCROLL_SPEED;
/*     */       }
/*     */       
/* 257 */       if (InputHelper.justClickedLeft) {
/* 258 */         this.grabbedScreen = true;
/* 259 */         this.grabStartY = (y - this.scrollTargetY);
/*     */       }
/*     */     }
/* 262 */     else if (InputHelper.isMouseDown) {
/* 263 */       this.scrollTargetY = (y - this.grabStartY);
/*     */     } else {
/* 265 */       this.grabbedScreen = false;
/*     */     }
/*     */     
/*     */ 
/* 269 */     this.scrollY = MathHelper.scrollSnapLerpSpeed(this.scrollY, this.scrollTargetY);
/* 270 */     resetScrolling();
/* 271 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void calculateScrollBounds() {
/* 275 */     if (!UnlockTracker.isCharacterLocked("Defect")) {
/* 276 */       this.scrollUpperBound = (2600.0F * Settings.scale);
/* 277 */     } else if (!UnlockTracker.isCharacterLocked("The Silent")) {
/* 278 */       this.scrollUpperBound = (2200.0F * Settings.scale);
/*     */     } else {
/* 280 */       this.scrollUpperBound = (1800.0F * Settings.scale);
/*     */     }
/* 282 */     this.scrollLowerBound = (100.0F * Settings.scale);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void resetScrolling()
/*     */   {
/* 289 */     if (this.scrollTargetY < this.scrollLowerBound) {
/* 290 */       this.scrollTargetY = MathHelper.scrollSnapLerpSpeed(this.scrollTargetY, this.scrollLowerBound);
/* 291 */     } else if (this.scrollTargetY > this.scrollUpperBound) {
/* 292 */       this.scrollTargetY = MathHelper.scrollSnapLerpSpeed(this.scrollTargetY, this.scrollUpperBound);
/*     */     }
/*     */   }
/*     */   
/*     */   public void open() {
/* 297 */     this.controllerHb = null;
/* 298 */     this.scrollTargetY = 0.0F;
/* 299 */     debugCharacterUnlock();
/*     */     
/* 301 */     this.targetX = SHOW_X;
/* 302 */     this.button.show(TEXT[0]);
/* 303 */     this.screenUp = true;
/* 304 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.STATS;
/*     */     
/* 306 */     debugAchievementUnlock();
/*     */   }
/*     */   
/*     */   private void debugAchievementUnlock() {
/* 310 */     if (Settings.isInfo) {
/* 311 */       for (AchievementItem i : achievements.items) {
/* 312 */         UnlockTracker.unlockAchievement(i.key);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void debugCharacterUnlock() {
/* 318 */     if (Settings.isInfo) {
/* 319 */       for (String s : UnlockTracker.lockedCharacters) {
/* 320 */         UnlockTracker.hardUnlockOverride(s);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/* 326 */     CardCrawlGame.sound.play("DECK_CLOSE", 0.1F);
/* 327 */     this.targetX = HIDE_X;
/* 328 */     this.button.hide();
/* 329 */     this.screenUp = false;
/* 330 */     this.currentChar = null;
/* 331 */     CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */   }
/*     */   
/*     */   public static void updateFurthestAscent(int floor) {
/* 335 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 337 */       ironclad.furthestAscent(floor);
/* 338 */       break;
/*     */     case THE_SILENT: 
/* 340 */       silent.furthestAscent(floor);
/* 341 */       break;
/*     */     case DEFECT: 
/* 343 */       defect.furthestAscent(floor);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void updateHighestScore(int score)
/*     */   {
/* 349 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 351 */       ironclad.highestScore(score);
/* 352 */       break;
/*     */     case THE_SILENT: 
/* 354 */       silent.highestScore(score);
/* 355 */       break;
/*     */     case DEFECT: 
/* 357 */       defect.highestScore(score);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void updateHighestDailyScore(int score)
/*     */   {
/* 363 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 365 */       ironclad.highestDaily(score);
/* 366 */       break;
/*     */     case THE_SILENT: 
/* 368 */       silent.highestDaily(score);
/* 369 */       break;
/*     */     case DEFECT: 
/* 371 */       defect.highestDaily(score);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void updateVictoryTime(long time)
/*     */   {
/* 377 */     logger.info("Saving fastest victory...");
/* 378 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 380 */       ironclad.updateFastestVictory(time);
/* 381 */       break;
/*     */     case THE_SILENT: 
/* 383 */       silent.updateFastestVictory(time);
/* 384 */       break;
/*     */     case DEFECT: 
/* 386 */       defect.updateFastestVictory(time);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementFloorClimbed()
/*     */   {
/* 392 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 394 */       ironclad.incrementFloorClimbed();
/* 395 */       break;
/*     */     case THE_SILENT: 
/* 397 */       silent.incrementFloorClimbed();
/* 398 */       break;
/*     */     case DEFECT: 
/* 400 */       defect.incrementFloorClimbed();
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean isPlayingHighestAscension(AbstractPlayer.PlayerClass c)
/*     */   {
/* 406 */     switch (c) {
/*     */     case IRONCLAD: 
/* 408 */       return AbstractDungeon.ascensionLevel == ironclad.pref.getInteger("ASCENSION_LEVEL", 1) - 1;
/*     */     case THE_SILENT: 
/* 410 */       return AbstractDungeon.ascensionLevel == silent.pref.getInteger("ASCENSION_LEVEL", 1) - 1;
/*     */     case DEFECT: 
/* 412 */       return AbstractDungeon.ascensionLevel == defect.pref.getInteger("ASCENSION_LEVEL", 1) - 1;
/*     */     }
/* 414 */     logger.info("[ERROR] Missing character: " + c.name() + " for isPlayingHighestAscension()");
/* 415 */     return false;
/*     */   }
/*     */   
/*     */   public static void retroactiveAscend10Unlock()
/*     */   {
/* 420 */     if (ironclad.pref.getInteger("ASCENSION_LEVEL", 0) >= 11) {
/* 421 */       UnlockTracker.unlockAchievement("ASCEND_10");
/* 422 */     } else if (silent.pref.getInteger("ASCENSION_LEVEL", 0) >= 11) {
/* 423 */       UnlockTracker.unlockAchievement("ASCEND_10");
/* 424 */     } else if (defect.pref.getInteger("ASCENSION_LEVEL", 0) >= 11) {
/* 425 */       UnlockTracker.unlockAchievement("ASCEND_10");
/*     */     }
/*     */   }
/*     */   
/*     */   public static void retroactiveAscend20Unlock() {
/* 430 */     if (ironclad.pref.getInteger("ASCENSION_LEVEL", 0) >= 21) {
/* 431 */       UnlockTracker.unlockAchievement("ASCEND_20");
/* 432 */     } else if (silent.pref.getInteger("ASCENSION_LEVEL", 0) >= 21) {
/* 433 */       UnlockTracker.unlockAchievement("ASCEND_20");
/* 434 */     } else if (defect.pref.getInteger("ASCENSION_LEVEL", 0) >= 21) {
/* 435 */       UnlockTracker.unlockAchievement("ASCEND_20");
/*     */     }
/*     */   }
/*     */   
/*     */   public static int getVictory(AbstractPlayer.PlayerClass c) {
/* 440 */     switch (c) {
/*     */     case IRONCLAD: 
/* 442 */       return ironclad.getVictoryCount();
/*     */     case THE_SILENT: 
/* 444 */       return silent.getVictoryCount();
/*     */     case DEFECT: 
/* 446 */       return defect.getVictoryCount();
/*     */     }
/*     */     
/* 449 */     return 0;
/*     */   }
/*     */   
/*     */   public static void unlockAscension(AbstractPlayer.PlayerClass c) {
/* 453 */     switch (c) {
/*     */     case IRONCLAD: 
/* 455 */       ironclad.unlockAscension();
/* 456 */       break;
/*     */     case THE_SILENT: 
/* 458 */       silent.unlockAscension();
/* 459 */       break;
/*     */     case DEFECT: 
/* 461 */       defect.unlockAscension();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementVictory(AbstractPlayer.PlayerClass c)
/*     */   {
/* 467 */     switch (c) {
/*     */     case IRONCLAD: 
/* 469 */       ironclad.incrementVictory();
/* 470 */       break;
/*     */     case THE_SILENT: 
/* 472 */       silent.incrementVictory();
/* 473 */       break;
/*     */     case DEFECT: 
/* 475 */       defect.incrementVictory();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementDeath(AbstractPlayer.PlayerClass c)
/*     */   {
/* 481 */     switch (c) {
/*     */     case IRONCLAD: 
/* 483 */       ironclad.incrementDeath();
/* 484 */       break;
/*     */     case THE_SILENT: 
/* 486 */       silent.incrementDeath();
/* 487 */       break;
/*     */     case DEFECT: 
/* 489 */       defect.incrementDeath();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementEnemySlain()
/*     */   {
/* 495 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 497 */       ironclad.incrementEnemySlain();
/* 498 */       break;
/*     */     case THE_SILENT: 
/* 500 */       silent.incrementEnemySlain();
/* 501 */       break;
/*     */     case DEFECT: 
/* 503 */       defect.incrementEnemySlain();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementBossSlain()
/*     */   {
/* 509 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 511 */       ironclad.incrementBossSlain();
/* 512 */       break;
/*     */     case THE_SILENT: 
/* 514 */       silent.incrementBossSlain();
/* 515 */       break;
/*     */     case DEFECT: 
/* 517 */       defect.incrementBossSlain();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void incrementPlayTime(long time)
/*     */   {
/* 523 */     switch (AbstractDungeon.player.chosenClass) {
/*     */     case IRONCLAD: 
/* 525 */       ironclad.incrementPlayTime(time);
/* 526 */       break;
/*     */     case THE_SILENT: 
/* 528 */       silent.incrementPlayTime(time);
/* 529 */       break;
/*     */     case DEFECT: 
/* 531 */       defect.incrementPlayTime(time);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 537 */     renderStatScreen(sb);
/* 538 */     this.button.render(sb);
/* 539 */     this.scrollBar.render(sb);
/*     */   }
/*     */   
/*     */   private void renderStatScreen(SpriteBatch sb) {
/* 543 */     this.renderY = this.scrollY;
/* 544 */     renderHeader(sb, NAMES[0]);
/* 545 */     renderCharacterStats(sb, all);
/* 546 */     this.renderY -= 400.0F * Settings.scale;
/*     */     
/*     */ 
/* 549 */     renderHeader(sb, NAMES[1]);
/* 550 */     achievements.render(sb, this.renderY);
/* 551 */     this.renderY -= 1800.0F * Settings.scale;
/*     */     
/*     */ 
/* 554 */     renderHeader(sb, NAMES[2]);
/* 555 */     renderCharacterStats(sb, ironclad);
/*     */     
/*     */ 
/* 558 */     if (!UnlockTracker.isCharacterLocked("The Silent")) {
/* 559 */       if (this.silentHb == null) {
/* 560 */         this.silentHb = new Hitbox(150.0F * Settings.scale, 150.0F * Settings.scale);
/*     */       }
/* 562 */       this.renderY -= 400.0F * Settings.scale;
/* 563 */       renderHeader(sb, NAMES[3]);
/* 564 */       renderCharacterStats(sb, silent);
/*     */     }
/*     */     
/*     */ 
/* 568 */     if (!UnlockTracker.isCharacterLocked("Defect")) {
/* 569 */       if (this.defectHb == null) {
/* 570 */         this.defectHb = new Hitbox(150.0F * Settings.scale, 150.0F * Settings.scale);
/*     */       }
/* 572 */       this.renderY -= 400.0F * Settings.scale;
/* 573 */       renderHeader(sb, NAMES[4]);
/* 574 */       renderCharacterStats(sb, defect);
/*     */     }
/*     */     
/* 577 */     if (Settings.isControllerMode) {
/* 578 */       this.allCharsHb.move(300.0F * Settings.scale, this.scrollY + 600.0F * Settings.scale);
/* 579 */       this.ironcladHb.move(300.0F * Settings.scale, this.scrollY - 1300.0F * Settings.scale);
/*     */       
/* 581 */       if (this.silentHb != null) {
/* 582 */         this.silentHb.move(300.0F * Settings.scale, this.scrollY - 1700.0F * Settings.scale);
/*     */       }
/* 584 */       if (this.defectHb != null) {
/* 585 */         this.defectHb.move(300.0F * Settings.scale, this.scrollY - 2100.0F * Settings.scale);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderHeader(SpriteBatch sb, String text) {
/* 591 */     FontHelper.renderSmartText(sb, FontHelper.charTitleFont, text, this.screenX + 50.0F * Settings.scale, this.renderY + 850.0F * Settings.scale, 9999.0F, 32.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderCharacterStats(SpriteBatch sb, CharStat stat)
/*     */   {
/* 603 */     FontHelper.renderSmartText(sb, FontHelper.eventBodyText, stat.info, this.screenX + 75.0F * Settings.scale, this.renderY + 766.0F * Settings.scale, 9999.0F, 38.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 613 */     if (stat.info2 != null) {
/* 614 */       FontHelper.renderSmartText(sb, FontHelper.eventBodyText, stat.info2, this.screenX + 675.0F * Settings.scale, this.renderY + 766.0F * Settings.scale, 9999.0F, 38.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean statScreenUnlocked()
/*     */   {
/* 628 */     boolean temp = false;
/* 629 */     if ((ironclad.bossKilled > 0) || (ironclad.numDeath > 0)) {
/* 630 */       temp = true;
/*     */     }
/* 632 */     return temp;
/*     */   }
/*     */   
/*     */   public boolean dailiesUnlocked() {
/* 636 */     boolean temp = false;
/* 637 */     if ((ironclad.furthestAscent > 17) || (silent.furthestAscent > 17)) {
/* 638 */       temp = true;
/*     */     }
/* 640 */     if (Settings.isDemo) {
/* 641 */       return false;
/*     */     }
/* 643 */     return temp;
/*     */   }
/*     */   
/*     */   public boolean trialsUnlocked() {
/* 647 */     boolean temp = false;
/* 648 */     if ((ironclad.numVictory > 0) || (silent.numVictory > 0)) {
/* 649 */       temp = true;
/*     */     }
/* 651 */     return temp;
/*     */   }
/*     */   
/*     */   public static int getTotalVictories() {
/* 655 */     return ironclad.numVictory + silent.numVictory + defect.numVictory;
/*     */   }
/*     */   
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 660 */     this.scrollY = MathHelper.valueFromPercentBetween(this.scrollLowerBound, this.scrollUpperBound, newPercent);
/* 661 */     this.scrollTargetY = this.scrollY;
/* 662 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 666 */     float percent = MathHelper.percentFromValueBetween(this.scrollLowerBound, this.scrollUpperBound, this.scrollY);
/* 667 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\stats\StatsScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */