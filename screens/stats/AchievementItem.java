/*     */ package com.megacrit.cardcrawl.screens.stats;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ 
/*     */ public class AchievementItem
/*     */ {
/*     */   private com.badlogic.gdx.graphics.Texture img;
/*     */   private static final String IMG_DIR = "images/achievements/";
/*     */   private static final String LOCKED_DIR = "locked/";
/*     */   private static final String UNLOCKED_DIR = "unlocked/";
/*     */   private static final int W = 160;
/*  20 */   public Hitbox hb = new Hitbox(160.0F * Settings.scale, 160.0F * Settings.scale);
/*     */   private String title;
/*     */   private String desc;
/*     */   public String key;
/*  24 */   public boolean isUnlocked; private static final Color LOCKED_COLOR = new Color(1.0F, 1.0F, 1.0F, 0.8F);
/*     */   
/*     */   public AchievementItem(String title, String desc, String imgUrl, String key, boolean hidden) {
/*  27 */     this.isUnlocked = UnlockTracker.achievementPref.getBoolean(key, false);
/*  28 */     this.key = key;
/*  29 */     if (this.isUnlocked) {
/*  30 */       com.megacrit.cardcrawl.steam.SteamSaveSync.unlockAchievement(key);
/*  31 */       this.title = title;
/*  32 */       this.desc = desc;
/*  33 */       this.img = ImageMaster.loadImage("images/achievements/unlocked/" + imgUrl);
/*  34 */     } else if (hidden) {
/*  35 */       this.title = AchievementGrid.NAMES[26];
/*  36 */       this.desc = AchievementGrid.TEXT[26];
/*  37 */       this.img = ImageMaster.loadImage("images/achievements/locked/" + imgUrl);
/*     */     } else {
/*  39 */       this.title = title;
/*  40 */       this.desc = desc;
/*  41 */       this.img = ImageMaster.loadImage("images/achievements/locked/" + imgUrl);
/*     */     }
/*     */   }
/*     */   
/*     */   public AchievementItem(String title, String desc, String imgUrl, String key) {
/*  46 */     this(title, desc, imgUrl, key, false);
/*     */   }
/*     */   
/*     */   public void update() {
/*  50 */     if (this.hb != null) {
/*  51 */       this.hb.update();
/*  52 */       if (this.hb.hovered) {
/*  53 */         TipHelper.renderGenericTip(InputHelper.mX + 100.0F * Settings.scale, InputHelper.mY, this.title, this.desc);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float x, float y) {
/*  59 */     if (!this.isUnlocked) {
/*  60 */       sb.setColor(LOCKED_COLOR);
/*     */     } else {
/*  62 */       sb.setColor(Color.WHITE);
/*     */     }
/*     */     
/*  65 */     if (this.hb.hovered) {
/*  66 */       sb.draw(this.img, x - 80.0F, y - 80.0F, 80.0F, 80.0F, 160.0F, 160.0F, Settings.scale * 1.1F, Settings.scale * 1.1F, 0.0F, 0, 0, 160, 160, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  84 */       sb.draw(this.img, x - 80.0F, y - 80.0F, 80.0F, 80.0F, 160.0F, 160.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 160, 160, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 102 */     this.hb.move(x, y);
/* 103 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\stats\AchievementItem.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */