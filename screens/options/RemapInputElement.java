/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.HitboxListener;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputListener;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ public class RemapInputElement implements HitboxListener, com.badlogic.gdx.InputProcessor
/*     */ {
/*     */   public Hitbox hb;
/*     */   public InputAction action;
/*     */   public CInputAction cAction;
/*  27 */   public boolean hasInputFocus = false;
/*  28 */   public boolean isHidden = false;
/*  29 */   public boolean isHeader = false;
/*     */   
/*     */   private String labelText;
/*     */   
/*     */   public RemapInputElementListener listener;
/*  34 */   public static final float ROW_HEIGHT = 58.0F * Settings.scale;
/*  35 */   public static final float ROW_WIDTH = 1048.0F * Settings.scale;
/*     */   
/*  37 */   public static final float ROW_RENDER_HEIGHT = 64.0F * Settings.scale;
/*  38 */   private static final float ROW_TEXT_Y_OFFSET = 12.0F * Settings.scale;
/*  39 */   private static final float ROW_TEXT_LEADING_OFFSET = 40.0F * Settings.scale;
/*     */   
/*  41 */   public static final float KEYBOARD_COLUMN_X_OFFSET = Settings.scale * 400.0F;
/*  42 */   public static final float CONTROLLER_COLUMN_X_OFFSET = Settings.scale * 250.0F;
/*     */   
/*  44 */   private static final Color ROW_BG_COLOR = new Color(588124159);
/*  45 */   private static final Color ROW_HOVER_COLOR = new Color(65343);
/*  46 */   private static final Color ROW_SELECT_COLOR = new Color(-1924910337);
/*     */   
/*  48 */   private static final Color TEXT_DEFAULT_COLOR = Settings.CREAM_COLOR;
/*  49 */   private static final Color TEXT_FOCUSED_COLOR = Settings.GREEN_TEXT_COLOR;
/*  50 */   private static final Color TEXT_HOVERED_COLOR = Settings.GOLD_COLOR;
/*     */   
/*     */   public RemapInputElement(RemapInputElementListener listener, String label, InputAction action) {
/*  53 */     this(listener, label, action, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public RemapInputElement(RemapInputElementListener listener, String label, InputAction action, CInputAction controllerAction)
/*     */   {
/*  61 */     this.hb = new Hitbox(ROW_WIDTH, ROW_HEIGHT);
/*  62 */     this.labelText = label;
/*  63 */     this.action = action;
/*  64 */     this.cAction = controllerAction;
/*  65 */     this.listener = listener;
/*     */   }
/*     */   
/*     */   public void move(float x, float y) {
/*  69 */     this.hb.move(x, y);
/*     */   }
/*     */   
/*     */   public void update() {
/*  73 */     if (this.isHidden) {
/*  74 */       return;
/*     */     }
/*  76 */     this.hb.encapsulatedUpdate(this);
/*     */     
/*  78 */     if ((this.hasInputFocus) && ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed()))) {
/*  79 */       CInputActionSet.select.unpress();
/*  80 */       System.out.println("Lose focus");
/*  81 */       this.hasInputFocus = false;
/*  82 */       InputHelper.regainInputFocus();
/*  83 */       CInputHelper.regainInputFocus();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  88 */     if (this.isHidden) {
/*  89 */       return;
/*     */     }
/*     */     
/*     */ 
/*  93 */     sb.setBlendFunction(770, 1);
/*  94 */     sb.setColor(getRowBgColor());
/*  95 */     sb.draw(ImageMaster.INPUT_SETTINGS_ROW, this.hb.x, this.hb.y, ROW_WIDTH, ROW_RENDER_HEIGHT);
/*  96 */     sb.setBlendFunction(770, 771);
/*  97 */     sb.setColor(Color.WHITE);
/*     */     
/*     */ 
/* 100 */     Color textColor = getTextColor();
/* 101 */     float textY = this.hb.cY + ROW_TEXT_Y_OFFSET;
/* 102 */     float textX = this.hb.x + ROW_TEXT_LEADING_OFFSET;
/* 103 */     FontHelper.renderFont(sb, FontHelper.topPanelInfoFont, this.labelText, textX, textY, textColor);
/* 104 */     textX += KEYBOARD_COLUMN_X_OFFSET;
/* 105 */     FontHelper.renderFont(sb, FontHelper.topPanelInfoFont, getKeyColumnText(), textX, textY, textColor);
/* 106 */     textX += CONTROLLER_COLUMN_X_OFFSET;
/* 107 */     if (!this.isHeader) {
/* 108 */       Texture img = getControllerColumnImg();
/* 109 */       if (img != null) {
/* 110 */         sb.draw(img, textX - 32.0F, textY - 32.0F - 10.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 64, 64, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */       FontHelper.renderFont(sb, FontHelper.topPanelInfoFont, getControllerColumnText(), textX, textY, textColor);
/*     */     }
/* 131 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   protected Color getRowBgColor() {
/* 135 */     Color bgColor = ROW_BG_COLOR;
/* 136 */     if (this.hasInputFocus) {
/* 137 */       bgColor = ROW_SELECT_COLOR;
/* 138 */     } else if (this.hb.hovered) {
/* 139 */       bgColor = ROW_HOVER_COLOR;
/*     */     }
/* 141 */     return bgColor;
/*     */   }
/*     */   
/*     */   protected Color getTextColor()
/*     */   {
/* 146 */     Color color = TEXT_DEFAULT_COLOR;
/* 147 */     if (this.hasInputFocus) {
/* 148 */       color = TEXT_FOCUSED_COLOR;
/* 149 */     } else if (this.hb.hovered) {
/* 150 */       color = TEXT_HOVERED_COLOR;
/*     */     }
/* 152 */     return color;
/*     */   }
/*     */   
/*     */   protected String getKeyColumnText() {
/* 156 */     return this.action != null ? this.action.getKeyString() : "";
/*     */   }
/*     */   
/*     */   protected String getControllerColumnText() {
/* 160 */     return this.cAction != null ? this.cAction.getKeyString() : "";
/*     */   }
/*     */   
/*     */   protected Texture getControllerColumnImg() {
/* 164 */     Texture retVal = null;
/* 165 */     if (this.cAction != null) {
/* 166 */       retVal = this.cAction.getKeyImg();
/*     */     }
/* 168 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void hoverStarted(Hitbox hitbox)
/*     */   {
/* 175 */     CardCrawlGame.sound.play("UI_HOVER");
/*     */   }
/*     */   
/*     */   public void startClicking(Hitbox hitbox)
/*     */   {
/* 180 */     CardCrawlGame.sound.play("UI_CLICK_1");
/*     */   }
/*     */   
/*     */   public void clicked(Hitbox hitbox)
/*     */   {
/* 185 */     System.out.println("BEGIN REMAPPING...");
/* 186 */     this.listener.didStartRemapping(this);
/* 187 */     Gdx.input.setInputProcessor(this);
/* 188 */     CInputListener.listenForRemap(this);
/* 189 */     this.hasInputFocus = true;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean keyDown(int keycode)
/*     */   {
/* 195 */     if (this.listener.willRemap(this, this.action.getKey(), keycode)) {
/* 196 */       this.action.remap(keycode);
/*     */     }
/* 198 */     this.hasInputFocus = false;
/* 199 */     InputHelper.regainInputFocus();
/* 200 */     CInputHelper.regainInputFocus();
/* 201 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public boolean keyUp(int keycode)
/*     */   {
/* 208 */     return false;
/*     */   }
/*     */   
/*     */   public boolean keyTyped(char character)
/*     */   {
/* 213 */     return false;
/*     */   }
/*     */   
/*     */   public boolean touchDown(int screenX, int screenY, int pointer, int button)
/*     */   {
/* 218 */     return false;
/*     */   }
/*     */   
/*     */   public boolean touchUp(int screenX, int screenY, int pointer, int button)
/*     */   {
/* 223 */     return false;
/*     */   }
/*     */   
/*     */   public boolean touchDragged(int screenX, int screenY, int pointer)
/*     */   {
/* 228 */     return false;
/*     */   }
/*     */   
/*     */   public boolean mouseMoved(int screenX, int screenY)
/*     */   {
/* 233 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean scrolled(int amount)
/*     */   {
/* 239 */     if (amount == -1) {
/* 240 */       InputHelper.scrolledUp = true;
/* 241 */     } else if (amount == 1) {
/* 242 */       InputHelper.scrolledDown = true;
/*     */     }
/* 244 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\RemapInputElement.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */