/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Graphics.DisplayMode;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame.GameMode;
/*     */ import com.megacrit.cardcrawl.core.DisplayConfig;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class ToggleButton
/*     */ {
/*  21 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ToggleButton.class.getName());
/*     */   private static final int W = 32;
/*     */   private float x;
/*     */   private float y;
/*  25 */   public Hitbox hb; public boolean enabled = true;
/*     */   private ToggleBtnType type;
/*     */   
/*     */   public static enum ToggleBtnType {
/*  29 */     FULL_SCREEN,  W_FULL_SCREEN,  SCREEN_SHAKE,  AMBIENCE_ON,  MUTE_IF_BG,  SUM_DMG,  BLOCK_DMG,  HAND_CONF,  FAST_MODE,  UPLOAD_DATA,  V_SYNC,  PLAYTESTER_ART,  SHOW_CARD_HOTKEYS;
/*     */     
/*     */     private ToggleBtnType() {} }
/*     */   
/*  33 */   public ToggleButton(float x, float y, float middleY, ToggleBtnType type) { this.x = x;
/*  34 */     this.y = (middleY + y * Settings.scale);
/*  35 */     this.type = type;
/*  36 */     this.hb = new Hitbox(200.0F * Settings.scale, 32.0F * Settings.scale);
/*  37 */     this.hb.move(x + 74.0F * Settings.scale, this.y);
/*  38 */     this.enabled = getPref();
/*     */   }
/*     */   
/*     */   public ToggleButton(float x, float y, float middleY, ToggleBtnType type, boolean isLarge) {
/*  42 */     this.x = x;
/*  43 */     this.y = (middleY + y * Settings.scale);
/*  44 */     this.type = type;
/*  45 */     if (isLarge) {
/*  46 */       this.hb = new Hitbox(480.0F * Settings.scale, 32.0F * Settings.scale);
/*  47 */       this.hb.move(x + 214.0F * Settings.scale, this.y);
/*     */     } else {
/*  49 */       this.hb = new Hitbox(240.0F * Settings.scale, 32.0F * Settings.scale);
/*  50 */       this.hb.move(x + 74.0F * Settings.scale, this.y);
/*     */     }
/*  52 */     this.enabled = getPref();
/*     */   }
/*     */   
/*     */   private boolean getPref() {
/*  56 */     switch (this.type) {
/*     */     case AMBIENCE_ON: 
/*  58 */       return Settings.soundPref.getBoolean("Ambience On");
/*     */     case MUTE_IF_BG: 
/*  60 */       return Settings.soundPref.getBoolean("Mute in Bg");
/*     */     case BLOCK_DMG: 
/*  62 */       return Settings.gamePref.getBoolean("Blocked Damage");
/*     */     case FAST_MODE: 
/*  64 */       return Settings.gamePref.getBoolean("Fast Mode");
/*     */     case SHOW_CARD_HOTKEYS: 
/*  66 */       return Settings.gamePref.getBoolean("Show Card keys");
/*     */     case FULL_SCREEN: 
/*  68 */       return Settings.IS_FULLSCREEN;
/*     */     case W_FULL_SCREEN: 
/*  70 */       return Settings.IS_W_FULLSCREEN;
/*     */     case V_SYNC: 
/*  72 */       return Settings.IS_V_SYNC;
/*     */     case HAND_CONF: 
/*  74 */       return Settings.gamePref.getBoolean("Hand Confirmation");
/*     */     case SCREEN_SHAKE: 
/*  76 */       return Settings.gamePref.getBoolean("Screen Shake");
/*     */     case SUM_DMG: 
/*  78 */       return Settings.gamePref.getBoolean("Summed Damage");
/*     */     case UPLOAD_DATA: 
/*  80 */       return Settings.gamePref.getBoolean("Upload Data");
/*     */     case PLAYTESTER_ART: 
/*  82 */       return Settings.gamePref.getBoolean("Playtester Art");
/*     */     }
/*  84 */     return true;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  89 */     this.hb.update();
/*  90 */     if ((this.hb.hovered) && ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) || (com.megacrit.cardcrawl.helpers.controller.CInputActionSet.select.isJustPressed()))) {
/*  91 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*  92 */       toggle();
/*     */     }
/*     */   }
/*     */   
/*     */   public void toggle() {
/*  97 */     this.enabled = (!this.enabled);
/*  98 */     switch (this.type) {
/*     */     case AMBIENCE_ON: 
/* 100 */       Settings.soundPref.putBoolean("Ambience On", this.enabled);
/* 101 */       Settings.soundPref.flush();
/* 102 */       Settings.AMBIANCE_ON = this.enabled;
/* 103 */       if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 104 */         CardCrawlGame.mainMenuScreen.updateAmbienceVolume();
/*     */       } else {
/* 106 */         AbstractDungeon.scene.updateAmbienceVolume();
/*     */       }
/* 108 */       logger.info("Ambience On: " + this.enabled);
/* 109 */       break;
/*     */     case MUTE_IF_BG: 
/* 111 */       Settings.soundPref.putBoolean("Mute in Bg", this.enabled);
/* 112 */       Settings.soundPref.flush();
/* 113 */       CardCrawlGame.MUTE_IF_BG = this.enabled;
/* 114 */       logger.info("Mute while in Background: " + this.enabled);
/* 115 */       break;
/*     */     case BLOCK_DMG: 
/* 117 */       Settings.gamePref.putBoolean("Blocked Damage", this.enabled);
/* 118 */       Settings.gamePref.flush();
/* 119 */       Settings.SHOW_DMG_BLOCK = this.enabled;
/* 120 */       logger.info("Show Blocked Damage: " + this.enabled);
/* 121 */       break;
/*     */     case FAST_MODE: 
/* 123 */       Settings.gamePref.putBoolean("Fast Mode", this.enabled);
/* 124 */       Settings.gamePref.flush();
/* 125 */       Settings.FAST_MODE = this.enabled;
/* 126 */       logger.info("Fast Mode: " + this.enabled);
/* 127 */       break;
/*     */     case SHOW_CARD_HOTKEYS: 
/* 129 */       Settings.gamePref.putBoolean("Show Card keys", this.enabled);
/* 130 */       Settings.gamePref.flush();
/* 131 */       Settings.SHOW_CARD_HOTKEYS = this.enabled;
/* 132 */       logger.info("Show Card Hotkeys: " + this.enabled);
/* 133 */       break;
/*     */     case FULL_SCREEN: 
/* 135 */       Settings.IS_FULLSCREEN = !Settings.IS_FULLSCREEN;
/*     */       
/* 137 */       if (Settings.IS_FULLSCREEN)
/*     */       {
/* 139 */         if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 140 */           CardCrawlGame.mainMenuScreen.optionPanel.setFullscreen(false);
/*     */         } else {
/* 142 */           AbstractDungeon.settingsScreen.panel.setFullscreen(false);
/*     */         }
/*     */         
/*     */ 
/* 146 */         if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 147 */           if (CardCrawlGame.mainMenuScreen.optionPanel.wfsToggle.enabled) {
/* 148 */             CardCrawlGame.mainMenuScreen.optionPanel.wfsToggle.enabled = false;
/*     */           }
/*     */         }
/* 151 */         else if (AbstractDungeon.settingsScreen.panel.wfsToggle.enabled) {
/* 152 */           AbstractDungeon.settingsScreen.panel.wfsToggle.enabled = false;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 157 */       Settings.IS_W_FULLSCREEN = false;
/* 158 */       DisplayConfig.writeDisplayConfigFile(
/* 159 */         Gdx.graphics.getDisplayMode().width, 
/* 160 */         Gdx.graphics.getDisplayMode().height, Settings.MAX_FPS, Settings.IS_FULLSCREEN, Settings.IS_W_FULLSCREEN, Settings.IS_V_SYNC);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 166 */       logger.info("Fullscreen: " + Settings.IS_FULLSCREEN);
/* 167 */       break;
/*     */     case W_FULL_SCREEN: 
/* 169 */       Settings.IS_W_FULLSCREEN = !Settings.IS_W_FULLSCREEN;
/*     */       
/* 171 */       if (Settings.IS_W_FULLSCREEN)
/*     */       {
/* 173 */         if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 174 */           CardCrawlGame.mainMenuScreen.optionPanel.setFullscreen(true);
/*     */         } else {
/* 176 */           AbstractDungeon.settingsScreen.panel.setFullscreen(true);
/*     */         }
/*     */         
/*     */ 
/* 180 */         if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 181 */           if (CardCrawlGame.mainMenuScreen.optionPanel.fsToggle.enabled) {
/* 182 */             CardCrawlGame.mainMenuScreen.optionPanel.fsToggle.enabled = false;
/*     */           }
/*     */         }
/* 185 */         else if (AbstractDungeon.settingsScreen.panel.fsToggle.enabled) {
/* 186 */           AbstractDungeon.settingsScreen.panel.fsToggle.enabled = false;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 191 */       Settings.IS_FULLSCREEN = false;
/* 192 */       DisplayConfig.writeDisplayConfigFile(
/* 193 */         Gdx.graphics.getDisplayMode().width, 
/* 194 */         Gdx.graphics.getDisplayMode().height, Settings.MAX_FPS, Settings.IS_FULLSCREEN, Settings.IS_W_FULLSCREEN, Settings.IS_V_SYNC);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 200 */       logger.info("Borderless Fullscreen: " + Settings.IS_W_FULLSCREEN);
/* 201 */       break;
/*     */     case V_SYNC: 
/* 203 */       Settings.IS_V_SYNC = !Settings.IS_V_SYNC;
/*     */       
/*     */ 
/* 206 */       if (CardCrawlGame.mode == CardCrawlGame.GameMode.CHAR_SELECT) {
/* 207 */         CardCrawlGame.mainMenuScreen.optionPanel.vSyncToggle.enabled = Settings.IS_V_SYNC;
/* 208 */         CardCrawlGame.mainMenuScreen.optionPanel.effects.clear();
/* 209 */         CardCrawlGame.mainMenuScreen.optionPanel.effects.add(new com.megacrit.cardcrawl.vfx.RestartForChangesEffect());
/*     */       } else {
/* 211 */         AbstractDungeon.settingsScreen.panel.vSyncToggle.enabled = Settings.IS_V_SYNC;
/* 212 */         AbstractDungeon.settingsScreen.panel.effects.clear();
/* 213 */         AbstractDungeon.settingsScreen.panel.effects.add(new com.megacrit.cardcrawl.vfx.RestartForChangesEffect());
/*     */       }
/*     */       
/* 216 */       DisplayConfig.writeDisplayConfigFile(Settings.SAVED_WIDTH, Settings.SAVED_HEIGHT, Settings.MAX_FPS, Settings.IS_FULLSCREEN, Settings.IS_W_FULLSCREEN, Settings.IS_V_SYNC);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 224 */       logger.info("V Sync: " + Settings.IS_V_SYNC);
/* 225 */       break;
/*     */     case HAND_CONF: 
/* 227 */       Settings.gamePref.putBoolean("Hand Confirmation", this.enabled);
/* 228 */       Settings.gamePref.flush();
/* 229 */       Settings.FAST_HAND_CONF = this.enabled;
/* 230 */       logger.info("Hand Confirmation: " + this.enabled);
/* 231 */       break;
/*     */     case SCREEN_SHAKE: 
/* 233 */       Settings.gamePref.putBoolean("Screen Shake", this.enabled);
/* 234 */       Settings.gamePref.flush();
/* 235 */       Settings.SCREEN_SHAKE = this.enabled;
/* 236 */       logger.info("Screen Shake: " + this.enabled);
/* 237 */       break;
/*     */     case SUM_DMG: 
/* 239 */       Settings.gamePref.putBoolean("Summed Damage", this.enabled);
/* 240 */       Settings.gamePref.flush();
/* 241 */       Settings.SHOW_DMG_SUM = this.enabled;
/* 242 */       logger.info("Display Summed Damage: " + this.enabled);
/* 243 */       break;
/*     */     case UPLOAD_DATA: 
/* 245 */       Settings.gamePref.putBoolean("Upload Data", this.enabled);
/* 246 */       Settings.gamePref.flush();
/* 247 */       Settings.UPLOAD_DATA = this.enabled;
/* 248 */       logger.info("Upload Data: " + this.enabled);
/* 249 */       break;
/*     */     case PLAYTESTER_ART: 
/* 251 */       Settings.gamePref.putBoolean("Playtester Art", this.enabled);
/* 252 */       Settings.gamePref.flush();
/* 253 */       Settings.PLAYTESTER_ART_MODE = this.enabled;
/* 254 */       logger.info("Playtester Art: " + this.enabled);
/* 255 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 262 */     if (this.enabled) {
/* 263 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     } else {
/* 265 */       sb.setColor(Color.WHITE);
/*     */     }
/*     */     
/* 268 */     float scale = Settings.scale;
/* 269 */     if (this.hb.hovered) {
/* 270 */       sb.setColor(Color.SKY);
/* 271 */       scale = Settings.scale * 1.25F;
/*     */     }
/*     */     
/* 274 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.OPTION_TOGGLE, this.x - 16.0F, this.y - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, scale, scale, 0.0F, 0, 0, 32, 32, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 292 */     if (this.enabled) {
/* 293 */       sb.setColor(Color.WHITE);
/* 294 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.OPTION_TOGGLE_ON, this.x - 16.0F, this.y - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, scale, scale, 0.0F, 0, 0, 32, 32, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 313 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\ToggleButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */