/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class VerticalSlider
/*     */ {
/*     */   public SliderListener sliderListener;
/*     */   private Hitbox sliderHitbox;
/*     */   private int optionCount;
/*     */   private int visibleCount;
/*     */   private int currentIndexOffset;
/*     */   private boolean isDragging;
/*  20 */   private final float CURSOR_W = 16.0F * Settings.scale;
/*  21 */   private final float CURSOR_H = 32.0F * Settings.scale;
/*  22 */   private final float DRAW_BORDER = this.CURSOR_H / 2.0F;
/*     */   
/*     */   public VerticalSlider(SliderListener listener, int optionCount, int visibleCount, int currentIndexOffset) {
/*  25 */     this.sliderListener = listener;
/*     */     
/*  27 */     this.optionCount = optionCount;
/*  28 */     this.visibleCount = visibleCount;
/*  29 */     this.currentIndexOffset = currentIndexOffset;
/*  30 */     this.isDragging = false;
/*     */     
/*  32 */     this.sliderHitbox = new Hitbox(24.0F * Settings.scale, 100.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public float width() {
/*  36 */     return this.sliderHitbox.width;
/*     */   }
/*     */   
/*     */   public void position(float x, float y, float height) {
/*  40 */     this.sliderHitbox.move(x + width() / 2.0F, y - height / 2.0F);
/*     */     
/*  42 */     this.sliderHitbox.height = height;
/*     */   }
/*     */   
/*     */   public void positionCenter(float x, float y) {
/*  46 */     this.sliderHitbox.cX = x;
/*  47 */     this.sliderHitbox.cY = y;
/*     */   }
/*     */   
/*     */   public boolean update() {
/*  51 */     this.sliderHitbox.update();
/*  52 */     if ((this.sliderHitbox.hovered) && (InputHelper.isMouseDown)) {
/*  53 */       this.isDragging = true;
/*     */     }
/*  55 */     if ((this.isDragging) && (InputHelper.justReleasedClickLeft)) {
/*  56 */       this.isDragging = false;
/*  57 */       return true;
/*     */     }
/*  59 */     if (this.isDragging) {
/*  60 */       int newIndex = getIndexFromY(InputHelper.mY);
/*  61 */       if (newIndex != this.currentIndexOffset) {
/*  62 */         this.currentIndexOffset = newIndex;
/*  63 */         this.sliderListener.sliderValueChanged(this, newIndex);
/*     */       }
/*  65 */       return true;
/*     */     }
/*     */     
/*  68 */     return false;
/*     */   }
/*     */   
/*     */   public void setCurrentIndexOffset(int newIndex) {
/*  72 */     this.currentIndexOffset = boundedIndex(newIndex);
/*     */   }
/*     */   
/*     */   private int getIndexFromY(float y) {
/*  76 */     float minY = this.sliderHitbox.y + this.sliderHitbox.height - this.DRAW_BORDER;
/*  77 */     float maxY = this.sliderHitbox.y + this.DRAW_BORDER;
/*  78 */     float percent = MathHelper.percentFromValueBetween(minY, maxY, y);
/*     */     
/*  80 */     int rawIndex = (int)(percent * (this.optionCount - this.visibleCount));
/*  81 */     return boundedIndex(rawIndex);
/*     */   }
/*     */   
/*     */   private int boundedIndex(int rawIndex) {
/*  85 */     return Math.max(0, Math.min(rawIndex, this.optionCount - this.visibleCount));
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  89 */     this.sliderHitbox.render(sb);
/*     */     
/*  91 */     renderCursor(sb);
/*     */   }
/*     */   
/*     */   private void renderCursor(SpriteBatch sb) {
/*  95 */     float x = this.sliderHitbox.cX - this.CURSOR_W / 2.0F;
/*     */     
/*  97 */     float percent = this.currentIndexOffset / (this.optionCount - this.visibleCount);
/*  98 */     float minY = this.sliderHitbox.y + this.sliderHitbox.height - this.DRAW_BORDER;
/*  99 */     float maxY = this.sliderHitbox.y + this.DRAW_BORDER;
/* 100 */     float yForPercent = MathHelper.valueFromPercentBetween(minY, maxY, percent);
/*     */     
/* 102 */     float y = yForPercent - this.DRAW_BORDER;
/*     */     
/* 104 */     sb.draw(ImageMaster.OPTION_VERTICAL_SLIDER, x, y, this.CURSOR_W, this.CURSOR_H);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\VerticalSlider.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */