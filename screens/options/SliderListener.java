package com.megacrit.cardcrawl.screens.options;

public abstract interface SliderListener
{
  public abstract void sliderValueChanged(VerticalSlider paramVerticalSlider, int paramInt);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\SliderListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */