/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import java.text.DecimalFormat;
/*     */ 
/*     */ public class Slider
/*     */ {
/*     */   private static final int BG_W = 250;
/*     */   private static final int BG_H = 24;
/*     */   private static final int S_W = 44;
/*  21 */   private static final float SLIDE_W = 230.0F * Settings.scale;
/*  22 */   private static final float BG_X = 1350.0F * Settings.scale;
/*  23 */   private static final float L_X = 1235.0F * Settings.scale;
/*     */   private float x;
/*     */   private float y;
/*  26 */   private float volume; public Hitbox hb; public Hitbox bgHb; private boolean sliderGrabbed = false;
/*     */   private SliderType type;
/*  28 */   private static DecimalFormat df = new DecimalFormat("#");
/*     */   
/*     */   public static enum SliderType {
/*  31 */     MASTER,  BGM,  SFX;
/*     */     
/*     */     private SliderType() {} }
/*     */   
/*  35 */   public Slider(float y, float volume, SliderType type) { this.type = type;
/*  36 */     this.y = y;
/*  37 */     this.volume = volume;
/*  38 */     this.hb = new Hitbox(42.0F * Settings.scale, 38.0F * Settings.scale);
/*  39 */     this.bgHb = new Hitbox(300.0F * Settings.scale, 38.0F * Settings.scale);
/*  40 */     this.bgHb.move(BG_X, y);
/*  41 */     this.x = (L_X + SLIDE_W * volume);
/*     */   }
/*     */   
/*     */   public void update() {
/*  45 */     this.hb.update();
/*  46 */     this.bgHb.update();
/*  47 */     this.hb.move(this.x, this.y);
/*     */     
/*     */ 
/*  50 */     if (this.sliderGrabbed) {
/*  51 */       if (InputHelper.isMouseDown) {
/*  52 */         this.x = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.x, InputHelper.mX);
/*  53 */         if (this.x < L_X) {
/*  54 */           this.x = L_X;
/*  55 */         } else if (this.x > L_X + SLIDE_W) {
/*  56 */           this.x = (L_X + SLIDE_W);
/*     */         }
/*  58 */         this.volume = ((this.x - L_X) / SLIDE_W);
/*  59 */         modifyVolume();
/*     */       } else {
/*  61 */         if (this.type == SliderType.SFX) {
/*  62 */           int roll = com.badlogic.gdx.math.MathUtils.random(2);
/*  63 */           if (roll == 0) {
/*  64 */             CardCrawlGame.sound.play("ATTACK_DAGGER_1");
/*  65 */           } else if (roll == 1) {
/*  66 */             CardCrawlGame.sound.play("ATTACK_DAGGER_2");
/*  67 */           } else if (roll == 2) {
/*  68 */             CardCrawlGame.sound.play("ATTACK_DAGGER_3");
/*     */           }
/*     */         }
/*  71 */         this.sliderGrabbed = false;
/*  72 */         Settings.soundPref.flush();
/*     */       }
/*  74 */     } else if (InputHelper.justClickedLeft) {
/*  75 */       if (this.hb.hovered) {
/*  76 */         this.sliderGrabbed = true;
/*  77 */       } else if (this.bgHb.hovered) {
/*  78 */         this.sliderGrabbed = true;
/*     */       }
/*     */     }
/*     */     
/*  82 */     if ((Settings.isControllerMode) && 
/*  83 */       (this.bgHb.hovered)) {
/*  84 */       if (CInputActionSet.inspectLeft.isJustPressed()) {
/*  85 */         this.x -= 10.0F * Settings.scale;
/*  86 */         if (this.x < L_X) {
/*  87 */           this.x = L_X;
/*     */         }
/*  89 */         this.volume = ((this.x - L_X) / SLIDE_W);
/*  90 */         modifyVolume();
/*  91 */       } else if (CInputActionSet.inspectRight.isJustPressed()) {
/*  92 */         this.x += 10.0F * Settings.scale;
/*  93 */         if (this.x > L_X + SLIDE_W) {
/*  94 */           this.x = (L_X + SLIDE_W);
/*     */         }
/*  96 */         this.volume = ((this.x - L_X) / SLIDE_W);
/*  97 */         modifyVolume();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void modifyVolume()
/*     */   {
/* 104 */     switch (this.type) {
/*     */     case MASTER: 
/* 106 */       Settings.MASTER_VOLUME = this.volume;
/* 107 */       Settings.soundPref.putFloat("Master Volume", this.volume);
/*     */       
/* 109 */       CardCrawlGame.music.updateVolume();
/* 110 */       if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/* 111 */         CardCrawlGame.mainMenuScreen.updateAmbienceVolume();
/* 112 */       } else if (AbstractDungeon.scene != null) {
/* 113 */         AbstractDungeon.scene.updateAmbienceVolume();
/*     */       }
/*     */       break;
/*     */     case BGM: 
/* 117 */       Settings.MUSIC_VOLUME = this.volume;
/* 118 */       CardCrawlGame.music.updateVolume();
/* 119 */       Settings.soundPref.putFloat("Music Volume", this.volume);
/* 120 */       break;
/*     */     case SFX: 
/* 122 */       Settings.SOUND_VOLUME = this.volume;
/* 123 */       if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/* 124 */         CardCrawlGame.mainMenuScreen.updateAmbienceVolume();
/* 125 */       } else if (AbstractDungeon.scene != null) {
/* 126 */         AbstractDungeon.scene.updateAmbienceVolume();
/*     */       }
/* 128 */       Settings.soundPref.putFloat("Sound Volume", this.volume);
/* 129 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 136 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/* 137 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.OPTION_SLIDER_BG, BG_X - 125.0F, this.y - 12.0F, 125.0F, 12.0F, 250.0F, 24.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 250, 24, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 155 */     if (this.sliderGrabbed) {
/* 156 */       FontHelper.renderFontCentered(sb, FontHelper.tipBodyFont, df
/*     */       
/*     */ 
/* 159 */         .format(this.volume * 100.0F) + '%', BG_X + 170.0F * Settings.scale, this.y, Settings.GREEN_TEXT_COLOR);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 164 */       FontHelper.renderFontCentered(sb, FontHelper.tipBodyFont, df
/*     */       
/*     */ 
/* 167 */         .format(this.volume * 100.0F) + '%', BG_X + 170.0F * Settings.scale, this.y, Settings.BLUE_TEXT_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 173 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.OPTION_SLIDER, this.x - 22.0F, this.y - 22.0F, 22.0F, 22.0F, 44.0F, 44.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 44, 44, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 191 */     this.hb.render(sb);
/* 192 */     this.bgHb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\Slider.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */