/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import java.io.PrintStream;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class AbandonConfirmPopup
/*     */ {
/*  28 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(AbandonConfirmPopup.class.getName());
/*  29 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ConfirmPopup");
/*  30 */   private static final UIStrings uiStrings2 = CardCrawlGame.languagePack.getUIString("SettingsScreen");
/*  31 */   public static final String[] TEXT_BUTTONS = uiStrings.TEXT;
/*  32 */   public static final String[] TEXT_DESC = uiStrings2.TEXT;
/*     */   public Hitbox yesHb;
/*     */   public Hitbox noHb;
/*     */   private static final int CONFIRM_W = 360;
/*     */   private static final int CONFIRM_H = 414;
/*  37 */   private static final int YES_W = 173; private static final int NO_W = 161; private static final int BUTTON_H = 74; private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  38 */   private Color uiColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  39 */   private float targetAlpha = 0.0F; private float targetAlpha2 = 0.0F;
/*  40 */   public boolean shown = false;
/*     */   private static final float SCREEN_DARKNESS = 0.75F;
/*     */   
/*     */   public AbandonConfirmPopup() {
/*  44 */     this.yesHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*  45 */     this.noHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*  46 */     this.yesHb.move(860.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/*  47 */     this.noHb.move(1062.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void show() {
/*  51 */     if (!this.shown) {
/*  52 */       this.shown = true;
/*  53 */       this.targetAlpha = 0.75F;
/*  54 */       this.targetAlpha2 = 1.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/*  59 */     if (this.shown) {
/*  60 */       CardCrawlGame.mainMenuScreen.screen = com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen.MAIN_MENU;
/*  61 */       this.shown = false;
/*  62 */       this.targetAlpha = 0.0F;
/*  63 */       this.targetAlpha2 = 0.0F;
/*  64 */       if (AbstractDungeon.overlayMenu != null) {
/*  65 */         AbstractDungeon.overlayMenu.cancelButton.show(TEXT_DESC[0]);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/*  71 */     if (this.shown) {
/*  72 */       updateYes();
/*  73 */       updateNo();
/*     */     }
/*  75 */     this.screenColor.a = MathHelper.fadeLerpSnap(this.screenColor.a, this.targetAlpha);
/*  76 */     this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, this.targetAlpha2);
/*     */   }
/*     */   
/*     */   private void updateYes() {
/*  80 */     this.yesHb.update();
/*  81 */     if (this.yesHb.justHovered) {
/*  82 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/*  85 */     if ((InputHelper.justClickedLeft) && (this.yesHb.hovered)) {
/*  86 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  87 */       this.yesHb.clickStarted = true;
/*     */     }
/*     */     
/*  90 */     if (CInputActionSet.proceed.isJustPressed()) {
/*  91 */       this.yesHb.clicked = true;
/*     */     }
/*     */     
/*  94 */     if (this.yesHb.clicked) {
/*  95 */       this.yesHb.clicked = false;
/*  96 */       effect();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateNo() {
/* 101 */     this.noHb.update();
/* 102 */     if (this.noHb.justHovered) {
/* 103 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/* 106 */     if ((InputHelper.justClickedLeft) && (this.noHb.hovered)) {
/* 107 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 108 */       this.noHb.clickStarted = true;
/*     */     }
/*     */     
/* 111 */     if (CInputActionSet.cancel.isJustPressed()) {
/* 112 */       this.noHb.clicked = true;
/*     */     }
/*     */     
/* 115 */     if (this.noHb.clicked) {
/* 116 */       this.noHb.clicked = false;
/* 117 */       hide();
/*     */     }
/*     */   }
/*     */   
/*     */   private void effect() {
/* 122 */     if (SaveAndContinue.ironcladSaveExists) {
/* 123 */       if (Settings.isBeta) {
/* 124 */         SteamSaveSync.setStat("win_streak_ironclad_BETA", 0);
/* 125 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad_BETA"));
/*     */       } else {
/* 127 */         SteamSaveSync.setStat("win_streak_ironclad", 0);
/* 128 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_ironclad"));
/*     */       }
/* 130 */       SaveAndContinue.ironcladSaveExists = false;
/* 131 */       abandonRun(AbstractPlayer.PlayerClass.IRONCLAD);
/* 132 */     } else if (SaveAndContinue.silentSaveExists) {
/* 133 */       if (Settings.isBeta) {
/* 134 */         SteamSaveSync.setStat("win_streak_silent_BETA", 0);
/* 135 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent_BETA"));
/*     */       } else {
/* 137 */         SteamSaveSync.setStat("win_streak_silent", 0);
/* 138 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_silent"));
/*     */       }
/* 140 */       SaveAndContinue.silentSaveExists = false;
/* 141 */       abandonRun(AbstractPlayer.PlayerClass.THE_SILENT);
/* 142 */     } else if (SaveAndContinue.defectSaveExists) {
/* 143 */       if (Settings.isBeta) {
/* 144 */         SteamSaveSync.setStat("win_streak_defect_BETA", 0);
/* 145 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_defect_BETA"));
/*     */       } else {
/* 147 */         SteamSaveSync.setStat("win_streak_defect", 0);
/* 148 */         System.out.println("WIN STREAK  " + SteamSaveSync.getStat("win_streak_defect"));
/*     */       }
/* 150 */       SaveAndContinue.defectSaveExists = false;
/* 151 */       abandonRun(AbstractPlayer.PlayerClass.DEFECT);
/*     */     }
/* 153 */     CardCrawlGame.mainMenuScreen.abandonedRun = true;
/* 154 */     hide();
/*     */   }
/*     */   
/*     */   private void abandonRun(AbstractPlayer.PlayerClass pClass) {
/* 158 */     logger.info("Abandoning run with " + pClass.name());
/* 159 */     SaveFile file = SaveAndContinue.loadSaveFile(pClass);
/* 160 */     if (Settings.isStandardRun()) {
/* 161 */       if (file.floor_num >= 16) {
/* 162 */         CardCrawlGame.playerPref.putInteger(pClass.name() + "_SPIRITS", 1);
/* 163 */         CardCrawlGame.playerPref.flush();
/*     */       } else {
/* 165 */         CardCrawlGame.playerPref.putInteger(pClass.name() + "_SPIRITS", 0);
/* 166 */         CardCrawlGame.playerPref.flush();
/*     */       }
/*     */     }
/* 169 */     SaveAndContinue.deleteSave(pClass);
/*     */     
/* 171 */     if (!file.is_ascension_mode) {
/* 172 */       com.megacrit.cardcrawl.screens.stats.StatsScreen.incrementDeath(pClass);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 177 */     sb.setColor(this.screenColor);
/* 178 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 180 */     sb.setColor(this.uiColor);
/* 181 */     sb.draw(ImageMaster.OPTION_CONFIRM, Settings.WIDTH / 2.0F - 180.0F, Settings.OPTION_Y - 207.0F, 180.0F, 207.0F, 360.0F, 414.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 360, 414, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 199 */     Color c = Settings.GOLD_COLOR.cpy();
/* 200 */     c.a = this.uiColor.a;
/* 201 */     FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, TEXT_DESC[0], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 126.0F * Settings.scale, c);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 209 */     sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 227 */     if (this.yesHb.hovered) {
/* 228 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 229 */       sb.setBlendFunction(770, 1);
/* 230 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 247 */       sb.setBlendFunction(770, 771);
/* 248 */       sb.setColor(this.uiColor);
/*     */       
/* 250 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_BUTTONS[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, this.uiColor, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 259 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_BUTTONS[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 269 */     sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 287 */     if (this.noHb.hovered) {
/* 288 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 289 */       sb.setBlendFunction(770, 1);
/* 290 */       sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 307 */       sb.setBlendFunction(770, 771);
/* 308 */       sb.setColor(this.uiColor);
/* 309 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_BUTTONS[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, this.uiColor, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 318 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_BUTTONS[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 328 */     c = Settings.CREAM_COLOR.cpy();
/* 329 */     c.a = this.uiColor.a;
/*     */     
/* 331 */     FontHelper.renderWrappedText(sb, FontHelper.tipBodyFont, TEXT_DESC[2], Settings.WIDTH / 2.0F, Settings.OPTION_Y + 20.0F * Settings.scale, 240.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 341 */     if (Settings.isControllerMode) {
/* 342 */       sb.draw(CInputActionSet.proceed
/* 343 */         .getKeyImg(), 770.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 359 */       sb.draw(CInputActionSet.cancel
/* 360 */         .getKeyImg(), 1150.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 378 */     if (this.shown) {
/* 379 */       this.yesHb.render(sb);
/* 380 */       this.noHb.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\AbandonConfirmPopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */