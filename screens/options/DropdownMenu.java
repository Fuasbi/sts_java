/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ 
/*     */ public class DropdownMenu implements SliderListener
/*     */ {
/*     */   private DropdownMenuListener listener;
/*     */   public ArrayList<DropdownRow> rows;
/*  24 */   public boolean isOpen = false;
/*     */   
/*  26 */   private int rowIndexOffset = 0;
/*     */   
/*     */   private int maxRows;
/*     */   
/*     */   private Color textColor;
/*     */   
/*     */   private BitmapFont textFont;
/*     */   
/*     */   private DropdownRow selectionBox;
/*     */   
/*     */   private VerticalSlider slider;
/*     */   private static final int DEFAULT_MAX_ROWS = 15;
/*  38 */   private static final float BOX_EDGE_H = 32.0F * Settings.scale;
/*  39 */   private static final float BOX_BODY_H = 64.0F * Settings.scale;
/*     */   
/*     */   public DropdownMenu(DropdownMenuListener listener, String[] options, BitmapFont font, Color textColor) {
/*  42 */     this(listener, new ArrayList(Arrays.asList(options)), font, textColor);
/*     */   }
/*     */   
/*     */   public DropdownMenu(DropdownMenuListener listener, ArrayList<String> options, BitmapFont font, Color textColor) {
/*  46 */     this(listener, options, font, textColor, 15);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DropdownMenu(DropdownMenuListener listener, String[] options, BitmapFont font, Color textColor, int maxRows)
/*     */   {
/*  55 */     this(listener, new ArrayList(Arrays.asList(options)), font, textColor, maxRows);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DropdownMenu(DropdownMenuListener listener, ArrayList<String> options, BitmapFont font, Color textColor, int maxRows)
/*     */   {
/*  64 */     this.listener = listener;
/*  65 */     this.textFont = font;
/*  66 */     this.textColor = textColor;
/*  67 */     this.maxRows = maxRows;
/*  68 */     this.rows = new ArrayList(options.size());
/*     */     
/*  70 */     this.slider = new VerticalSlider(this, options.size(), maxRows, 0);
/*  71 */     float width = approximateOverallWidth(options) - this.slider.width();
/*     */     
/*  73 */     if (options.size() > 0) {
/*  74 */       this.selectionBox = new DropdownRow((String)options.get(0), this.cachedMaxWidth, approximateRowHeight(), 0);
/*  75 */       this.selectionBox.isSelected = true;
/*  76 */       for (String option : options) {
/*  77 */         this.rows.add(new DropdownRow(option, width, approximateRowHeight(), this.rows.size()));
/*     */       }
/*  79 */       ((DropdownRow)this.rows.get(0)).isSelected = true;
/*     */     }
/*     */   }
/*     */   
/*     */   boolean shouldShowSlider() {
/*  84 */     return this.rows.size() > this.maxRows;
/*     */   }
/*     */   
/*     */   public Hitbox getHitbox() {
/*  88 */     return this.selectionBox.hb;
/*     */   }
/*     */   
/*     */   private int visibleRowCount() {
/*  92 */     return Math.min(this.rows.size(), this.maxRows);
/*     */   }
/*     */   
/*     */   void layoutRowsBelow(float originX, float originY) {
/*  96 */     this.selectionBox.position(originX, yPositionForRowBelow(originY, 0, 0.0F));
/*  97 */     for (int i = 0; i < visibleRowCount(); i++) {
/*  98 */       ((DropdownRow)this.rows.get(this.rowIndexOffset + i)).position(originX, yPositionForRowBelow(originY, i + 1, 0.0F));
/*     */     }
/* 100 */     if (shouldShowSlider()) {
/* 101 */       float top = yPositionForRowBelow(originY, 1, 0.0F);
/* 102 */       float bottom = yPositionForRowBelow(originY, visibleRowCount() + 1, 0.0F);
/* 103 */       this.slider.position(originX + this.cachedMaxWidth - this.slider.width(), top, top - bottom);
/*     */     }
/*     */   }
/*     */   
/*     */   void layoutRowsAbove(float originX, float originY) {
/* 108 */     this.selectionBox.position(originX, yPositionForRowBelow(originY, 0, 0.0F));
/* 109 */     for (int i = 0; i < visibleRowCount(); i++) {
/* 110 */       int reversed = visibleRowCount() - 1 - i;
/* 111 */       ((DropdownRow)this.rows.get(this.rowIndexOffset + reversed)).position(originX, yPositionForRowAbove(originY, i + 1, 0.0F));
/*     */     }
/* 113 */     if (shouldShowSlider()) {
/* 114 */       float bottom = yPositionForRowAbove(originY, 0, 0.0F);
/* 115 */       float top = yPositionForRowAbove(originY, visibleRowCount(), 0.0F);
/* 116 */       this.slider.position(originX + this.cachedMaxWidth - this.slider.width(), top, top - bottom);
/*     */     }
/*     */   }
/*     */   
/*     */   public float approximateRowHeight() {
/* 121 */     float extraSpace = Math.min(Math.max(this.textFont.getCapHeight(), 15.0F) * Settings.scale, 15.0F);
/* 122 */     return this.textFont.getCapHeight() + extraSpace;
/*     */   }
/*     */   
/*     */   private float approximateWidthForText(String option) {
/* 126 */     return FontHelper.getSmartWidth(this.textFont, option, Float.MAX_VALUE, Float.MAX_VALUE);
/*     */   }
/*     */   
/* 129 */   private float cachedMaxWidth = -1.0F;
/* 130 */   private static final float ICON_WIDTH = 60.0F * Settings.scale;
/*     */   
/*     */   public float approximateOverallWidth() {
/* 133 */     if (this.cachedMaxWidth > 0.0F) {
/* 134 */       return this.cachedMaxWidth;
/*     */     }
/*     */     
/* 137 */     ArrayList<String> options = new ArrayList();
/* 138 */     for (DropdownRow row : this.rows) {
/* 139 */       options.add(row.text);
/*     */     }
/* 141 */     return approximateOverallWidth(options);
/*     */   }
/*     */   
/*     */   private float approximateOverallWidth(ArrayList<String> options) {
/* 145 */     if (this.cachedMaxWidth > 0.0F) {
/* 146 */       return this.cachedMaxWidth;
/*     */     }
/* 148 */     for (String option : options) {
/* 149 */       this.cachedMaxWidth = Math.max(this.cachedMaxWidth, approximateWidthForText(option) + ICON_WIDTH);
/*     */     }
/* 151 */     return this.cachedMaxWidth;
/*     */   }
/*     */   
/*     */   private float yPositionForRowBelow(float originY, int rowIndex, float scrollAmount)
/*     */   {
/* 156 */     float rowHeight = approximateRowHeight();
/* 157 */     float extraHeight = rowIndex > 0 ? Settings.scale * 8.0F : 0.0F;
/* 158 */     return originY - rowHeight * rowIndex - extraHeight;
/*     */   }
/*     */   
/*     */   private float yPositionForRowAbove(float originY, int rowIndex, float scrollAmount)
/*     */   {
/* 163 */     float rowHeight = approximateRowHeight();
/* 164 */     float extraHeight = rowIndex > 0 ? Settings.scale * 8.0F : 0.0F;
/* 165 */     return originY + rowHeight * rowIndex + extraHeight;
/*     */   }
/*     */   
/*     */   public void setSelectedIndex(int i) {
/* 169 */     if (this.selectionBox.index == i)
/*     */     {
/* 171 */       return;
/*     */     }
/* 173 */     if (i < this.rows.size()) {
/* 174 */       changeSelectionToRow((DropdownRow)this.rows.get(i));
/*     */     }
/* 176 */     this.rowIndexOffset = Math.min(i, this.rows.size() - visibleRowCount());
/* 177 */     this.slider.setCurrentIndexOffset(this.rowIndexOffset);
/*     */   }
/*     */   
/*     */   public int getSelectedIndex() {
/* 181 */     return this.selectionBox.index;
/*     */   }
/*     */   
/*     */   private void changeSelectionToRow(DropdownRow newSelection) {
/* 185 */     this.selectionBox.text = newSelection.text;
/* 186 */     this.selectionBox.index = newSelection.index;
/* 187 */     for (DropdownRow row : this.rows) {
/* 188 */       row.isSelected = (row == newSelection);
/*     */     }
/* 190 */     if (this.listener != null) {
/* 191 */       this.listener.changedSelectionTo(this, newSelection.index, newSelection.text);
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/* 196 */     if (this.rows.size() == 0) {
/* 197 */       return;
/*     */     }
/*     */     
/* 200 */     if (this.isOpen) {
/* 201 */       updateControllerInput();
/* 202 */       for (int i = 0; i < visibleRowCount(); i++) {
/* 203 */         DropdownRow row = (DropdownRow)this.rows.get(i + this.rowIndexOffset);
/* 204 */         if (row.update()) {
/* 205 */           changeSelectionToRow(row);
/* 206 */           this.isOpen = false;
/* 207 */           CardCrawlGame.sound.play("UI_CLICK_2");
/*     */         }
/*     */       }
/* 210 */       boolean sliderClicked = false;
/* 211 */       if (shouldShowSlider()) {
/* 212 */         sliderClicked = this.slider.update();
/*     */       }
/*     */       
/* 215 */       boolean shouldCloseMenu = ((InputHelper.justReleasedClickLeft) && (!sliderClicked)) || (CInputActionSet.cancel.isJustPressed());
/* 216 */       if (shouldCloseMenu) {
/* 217 */         if (Settings.isControllerMode) {
/* 218 */           CInputActionSet.cancel.unpress();
/* 219 */           com.megacrit.cardcrawl.helpers.controller.CInputHelper.setCursor(getHitbox());
/*     */         }
/* 221 */         this.isOpen = false;
/*     */       }
/*     */     }
/* 224 */     else if (this.selectionBox.update()) {
/* 225 */       this.isOpen = true;
/* 226 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput()
/*     */   {
/* 232 */     if (!Settings.isControllerMode) {
/* 233 */       return;
/*     */     }
/*     */     
/* 236 */     boolean anyHovered = false;
/* 237 */     int index = 0;
/* 238 */     for (DropdownRow r : this.rows) {
/* 239 */       if (r.hb.hovered) {
/* 240 */         anyHovered = true;
/* 241 */         break;
/*     */       }
/* 243 */       index++;
/*     */     }
/*     */     
/* 246 */     if (!anyHovered) {
/* 247 */       index = 0;
/* 248 */       for (DropdownRow r : this.rows) {
/* 249 */         if (r.isSelected) {
/*     */           break;
/*     */         }
/* 252 */         index++;
/*     */       }
/* 254 */       Gdx.input.setCursorPosition((int)((DropdownRow)this.rows.get(index)).hb.cX, Settings.HEIGHT - (int)((DropdownRow)this.rows.get(index)).hb.cY);
/*     */     }
/* 256 */     else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 257 */       index--;
/* 258 */       if (index < 0) {
/* 259 */         index = this.rows.size() - 1;
/*     */       }
/* 261 */       Gdx.input.setCursorPosition((int)((DropdownRow)this.rows.get(index)).hb.cX, Settings.HEIGHT - (int)((DropdownRow)this.rows.get(index)).hb.cY);
/* 262 */     } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 263 */       index++;
/* 264 */       if (index > this.rows.size() - 1) {
/* 265 */         index = 0;
/*     */       }
/* 267 */       Gdx.input.setCursorPosition((int)((DropdownRow)this.rows.get(index)).hb.cX, Settings.HEIGHT - (int)((DropdownRow)this.rows.get(index)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, float x, float y)
/*     */   {
/* 273 */     if (this.rows.size() == 0) {
/* 274 */       return;
/*     */     }
/*     */     
/* 277 */     int rowCount = this.isOpen ? visibleRowCount() : 0;
/* 278 */     float heightOfSelector = approximateRowHeight() * 1.0F - BOX_EDGE_H * 2.5F;
/*     */     
/*     */ 
/* 281 */     float bottomSpace = yPositionForRowBelow(y, visibleRowCount(), 0.0F) - approximateRowHeight();
/* 282 */     float topSpace = Settings.HEIGHT - yPositionForRowAbove(y, visibleRowCount(), 0.0F);
/* 283 */     float REQUIRED_VERTICAL_PADDING = Settings.scale * 30.0F;
/*     */     
/*     */     boolean isBelow;
/*     */     boolean isBelow;
/* 287 */     if (bottomSpace > REQUIRED_VERTICAL_PADDING) {
/* 288 */       isBelow = true; } else { boolean isBelow;
/* 289 */       if (topSpace > REQUIRED_VERTICAL_PADDING) {
/* 290 */         isBelow = false;
/*     */       } else {
/* 292 */         isBelow = bottomSpace >= topSpace;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 297 */     if (isBelow) {
/* 298 */       layoutRowsBelow(x, y);
/* 299 */       float topY = yPositionForRowBelow(y, 0, 0.0F);
/* 300 */       float bottomY = yPositionForRowBelow(y, rowCount + 1, 0.0F);
/* 301 */       if (this.isOpen) {
/* 302 */         renderBorder(sb, x, bottomY, this.cachedMaxWidth, topY - bottomY);
/*     */       }
/* 304 */       renderBorderFromTop(sb, x, y, this.cachedMaxWidth, heightOfSelector);
/*     */     } else {
/* 306 */       layoutRowsAbove(x, y);
/* 307 */       float bottomY = yPositionForRowAbove(y, 0, 0.0F);
/* 308 */       float topY = yPositionForRowAbove(y, rowCount + 1, 0.0F);
/* 309 */       if (this.isOpen) {
/* 310 */         renderBorder(sb, x, bottomY, this.cachedMaxWidth, Settings.scale * 8.0F + topY - bottomY);
/*     */       }
/*     */       
/*     */ 
/* 314 */       renderBorderFromTop(sb, x, y, this.cachedMaxWidth, heightOfSelector);
/*     */     }
/*     */     
/* 317 */     this.selectionBox.renderRow(sb);
/* 318 */     if (this.isOpen)
/*     */     {
/*     */ 
/*     */ 
/* 322 */       for (int i = 0; i < visibleRowCount(); i++) {
/* 323 */         ((DropdownRow)this.rows.get(i + this.rowIndexOffset)).renderRow(sb);
/*     */       }
/* 325 */       if (shouldShowSlider()) {
/* 326 */         this.slider.render(sb);
/*     */       }
/*     */     }
/*     */     
/* 330 */     float ARROW_ICON_W = 30.0F * Settings.scale;
/* 331 */     float ARROW_ICON_H = 30.0F * Settings.scale;
/* 332 */     float arrowIconX = x + this.cachedMaxWidth - ARROW_ICON_W - Settings.scale * 10.0F;
/* 333 */     float arrowIconY = y - ARROW_ICON_H;
/* 334 */     com.badlogic.gdx.graphics.Texture dropdownArrowIcon = this.isOpen ? ImageMaster.OPTION_TOGGLE_ON : ImageMaster.FILTER_ARROW;
/* 335 */     sb.draw(dropdownArrowIcon, arrowIconX, arrowIconY, ARROW_ICON_W, ARROW_ICON_H);
/*     */   }
/*     */   
/*     */   private void renderBorder(SpriteBatch sb, float x, float bottom, float width, float height) {
/* 339 */     float border = Settings.scale * 10.0F;
/*     */     
/* 341 */     float BOX_W = width + 2.0F * border;
/* 342 */     float FRAME_X = x - border;
/*     */     
/* 344 */     float rowHeight = approximateRowHeight();
/*     */     
/* 346 */     sb.setColor(Color.WHITE);
/* 347 */     float bottomY = bottom - border;
/* 348 */     sb.draw(ImageMaster.KEYWORD_BOT, FRAME_X, bottomY, BOX_W, rowHeight);
/* 349 */     float middleHeight = height - 2.0F * rowHeight - border;
/* 350 */     sb.draw(ImageMaster.KEYWORD_BODY, FRAME_X, bottomY + rowHeight, BOX_W, middleHeight);
/* 351 */     sb.draw(ImageMaster.KEYWORD_TOP, FRAME_X, bottom + middleHeight + border, BOX_W, rowHeight);
/*     */   }
/*     */   
/*     */   private void renderBorderFromTop(SpriteBatch sb, float x, float top, float width, float height) {
/* 355 */     float border = Settings.scale * 10.0F;
/*     */     
/* 357 */     float BORDER_TOP_Y = top - BOX_EDGE_H + border;
/* 358 */     float BOX_W = width + 2.0F * border;
/* 359 */     float FRAME_X = x - border;
/*     */     
/*     */ 
/* 362 */     sb.setColor(Color.WHITE);
/* 363 */     sb.draw(ImageMaster.KEYWORD_TOP, FRAME_X, BORDER_TOP_Y, BOX_W, BOX_EDGE_H);
/* 364 */     sb.draw(ImageMaster.KEYWORD_BODY, FRAME_X, BORDER_TOP_Y - height - BOX_EDGE_H, BOX_W, height + BOX_EDGE_H);
/* 365 */     sb.draw(ImageMaster.KEYWORD_BOT, FRAME_X, BORDER_TOP_Y - height - BOX_BODY_H, BOX_W, BOX_EDGE_H);
/*     */   }
/*     */   
/*     */ 
/*     */   public void sliderValueChanged(VerticalSlider slider, int newValue)
/*     */   {
/* 371 */     this.rowIndexOffset = newValue;
/*     */   }
/*     */   
/*     */   private class DropdownRow {
/*     */     String text;
/*     */     Hitbox hb;
/* 377 */     boolean isSelected = false;
/*     */     int index;
/*     */     
/*     */     DropdownRow(String text, float width, float height, int index) {
/* 381 */       this.text = text;
/* 382 */       this.hb = new Hitbox(width, height);
/* 383 */       this.index = index;
/*     */     }
/*     */     
/*     */     private void position(float x, float y) {
/* 387 */       this.hb.x = x;
/* 388 */       this.hb.y = (y - this.hb.height);
/* 389 */       this.hb.cX = (this.hb.x + this.hb.width / 2.0F);
/* 390 */       this.hb.cY = (this.hb.y + this.hb.height / 2.0F);
/*     */     }
/*     */     
/*     */     private boolean update()
/*     */     {
/* 395 */       if ((this.hb.hovered) && 
/* 396 */         (InputHelper.justClickedLeft)) {
/* 397 */         this.hb.clickStarted = true;
/*     */       }
/*     */       
/* 400 */       this.hb.update();
/* 401 */       if ((this.hb.clicked) || ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/* 402 */         this.hb.clicked = false;
/* 403 */         return true;
/*     */       }
/* 405 */       return false;
/*     */     }
/*     */     
/*     */     private void renderRow(SpriteBatch sb) {
/* 409 */       this.hb.render(sb);
/*     */       
/* 411 */       Color renderTextColor = DropdownMenu.this.textColor;
/* 412 */       if (this.hb.hovered) {
/* 413 */         renderTextColor = Settings.GREEN_TEXT_COLOR;
/* 414 */       } else if (this.isSelected) {
/* 415 */         renderTextColor = Settings.GOLD_COLOR;
/*     */       }
/*     */       
/* 418 */       float textX = this.hb.x + Settings.scale * 10.0F;
/* 419 */       float a = DropdownMenu.this.approximateRowHeight();
/* 420 */       float b = DropdownMenu.this.textFont.getCapHeight();
/* 421 */       float yOffset = (a - b) / 2.0F;
/* 422 */       float textY = this.hb.y - yOffset;
/*     */       
/* 424 */       FontHelper.renderSmartText(sb, 
/*     */       
/* 426 */         DropdownMenu.this.textFont, this.text, textX, textY + this.hb.height, Float.MAX_VALUE, Float.MAX_VALUE, renderTextColor);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\DropdownMenu.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */