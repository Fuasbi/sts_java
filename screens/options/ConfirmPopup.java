/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ 
/*     */ public class ConfirmPopup
/*     */ {
/*  20 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ConfirmPopup");
/*  21 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   public String title;
/*     */   public String desc;
/*     */   public ConfirmType type;
/*     */   public Hitbox yesHb;
/*     */   public Hitbox noHb;
/*     */   private static final int CONFIRM_W = 360;
/*  28 */   private static final int CONFIRM_H = 414; private static final int YES_W = 173; private static final int NO_W = 161; private static final int BUTTON_H = 74; private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  29 */   private Color uiColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  30 */   private float targetAlpha = 0.0F; private float targetAlpha2 = 0.0F;
/*  31 */   public boolean shown = false;
/*     */   private static final float SCREEN_DARKNESS = 0.75F;
/*     */   
/*     */   public static enum ConfirmType {
/*  35 */     EXIT,  ABANDON,  DELETE_SAVE,  SKIP_FTUE;
/*     */     
/*     */     private ConfirmType() {} }
/*     */   
/*  39 */   public ConfirmPopup(String title, String desc, ConfirmType type) { this.type = type;
/*  40 */     this.title = title;
/*  41 */     this.desc = desc;
/*  42 */     this.yesHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*  43 */     this.noHb = new Hitbox(160.0F * Settings.scale, 70.0F * Settings.scale);
/*  44 */     this.yesHb.move(860.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/*  45 */     this.noHb.move(1062.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void show() {
/*  49 */     if (!this.shown) {
/*  50 */       this.shown = true;
/*  51 */       this.targetAlpha = 0.75F;
/*  52 */       this.targetAlpha2 = 1.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/*  57 */     if (this.shown) {
/*  58 */       this.shown = false;
/*  59 */       this.targetAlpha = 0.0F;
/*  60 */       this.targetAlpha2 = 0.0F;
/*  61 */       if (AbstractDungeon.overlayMenu != null) {
/*  62 */         AbstractDungeon.overlayMenu.cancelButton.show(TEXT[0]);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/*  68 */     if (this.shown) {
/*  69 */       updateYes();
/*  70 */       updateNo();
/*     */     }
/*  72 */     this.screenColor.a = MathHelper.fadeLerpSnap(this.screenColor.a, this.targetAlpha);
/*  73 */     this.uiColor.a = MathHelper.fadeLerpSnap(this.uiColor.a, this.targetAlpha2);
/*     */   }
/*     */   
/*     */   private void updateYes() {
/*  77 */     this.yesHb.update();
/*  78 */     if (this.yesHb.justHovered) {
/*  79 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/*  82 */     if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.yesHb.hovered)) {
/*  83 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  84 */       this.yesHb.clickStarted = true;
/*     */     }
/*     */     
/*  87 */     if (CInputActionSet.proceed.isJustPressed()) {
/*  88 */       this.yesHb.clicked = true;
/*     */     }
/*     */     
/*  91 */     if (this.yesHb.clicked) {
/*  92 */       this.yesHb.clicked = false;
/*  93 */       effect();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateNo() {
/*  98 */     this.noHb.update();
/*  99 */     if (this.noHb.justHovered) {
/* 100 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/* 103 */     if ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) && (this.noHb.hovered)) {
/* 104 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 105 */       this.noHb.clickStarted = true;
/*     */     }
/*     */     
/* 108 */     if (CInputActionSet.cancel.isJustPressed()) {
/* 109 */       this.noHb.clicked = true;
/*     */     }
/*     */     
/* 112 */     if (this.noHb.clicked) {
/* 113 */       this.noHb.clicked = false;
/* 114 */       if (this.type == ConfirmType.DELETE_SAVE) {
/* 115 */         CardCrawlGame.cancelButton.show(TEXT[1]);
/* 116 */       } else if (this.type == ConfirmType.SKIP_FTUE) {
/* 117 */         com.megacrit.cardcrawl.helpers.TipTracker.disableAllFtues();
/*     */       }
/* 119 */       hide();
/*     */     }
/*     */   }
/*     */   
/*     */   private void effect() {
/* 124 */     switch (this.type)
/*     */     {
/*     */     case EXIT: 
/* 127 */       CardCrawlGame.music.fadeAll();
/*     */       
/* 129 */       hide();
/* 130 */       AbstractDungeon.getCurrRoom().clearEvent();
/* 131 */       AbstractDungeon.closeCurrentScreen();
/* 132 */       CardCrawlGame.startOver();
/* 133 */       break;
/*     */     case ABANDON: 
/* 135 */       hide();
/* 136 */       AbstractDungeon.closeCurrentScreen();
/* 137 */       AbstractDungeon.player.isDead = true;
/* 138 */       AbstractDungeon.deathScreen = new com.megacrit.cardcrawl.screens.DeathScreen(AbstractDungeon.getMonsters());
/* 139 */       break;
/*     */     case DELETE_SAVE: 
/* 141 */       hide();
/* 142 */       CardCrawlGame.sound.play("ATTACK_HEAVY", 0.2F);
/* 143 */       CardCrawlGame.sound.play("SLIME_ATTACK_2");
/* 144 */       break;
/*     */     case SKIP_FTUE: 
/* 146 */       com.megacrit.cardcrawl.helpers.TipTracker.neverShowAgain("NO_FTUE");
/* 147 */       hide();
/* 148 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 155 */     sb.setColor(this.screenColor);
/* 156 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 158 */     sb.setColor(this.uiColor);
/* 159 */     sb.draw(ImageMaster.OPTION_CONFIRM, Settings.WIDTH / 2.0F - 180.0F, Settings.OPTION_Y - 207.0F, 180.0F, 207.0F, 360.0F, 414.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 360, 414, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 177 */     Color c = Settings.GOLD_COLOR.cpy();
/* 178 */     c.a = this.uiColor.a;
/* 179 */     FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, this.title, Settings.WIDTH / 2.0F, Settings.OPTION_Y + 126.0F * Settings.scale, c);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 187 */     sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */     if (this.yesHb.hovered) {
/* 206 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 207 */       sb.setBlendFunction(770, 1);
/* 208 */       sb.draw(ImageMaster.OPTION_YES, Settings.WIDTH / 2.0F - 86.5F - 100.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 86.5F, 37.0F, 173.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 173, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 225 */       sb.setBlendFunction(770, 771);
/* 226 */       sb.setColor(this.uiColor);
/*     */       
/* 228 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, this.uiColor, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 237 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[2], Settings.WIDTH / 2.0F - 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 247 */     sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 265 */     if (this.noHb.hovered) {
/* 266 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.uiColor.a * 0.25F));
/* 267 */       sb.setBlendFunction(770, 1);
/* 268 */       sb.draw(ImageMaster.OPTION_NO, Settings.WIDTH / 2.0F - 80.5F + 106.0F * Settings.scale, Settings.OPTION_Y - 37.0F - 120.0F * Settings.scale, 80.5F, 37.0F, 161.0F, 74.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 161, 74, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 285 */       sb.setBlendFunction(770, 771);
/* 286 */       sb.setColor(this.uiColor);
/* 287 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, this.uiColor, 1.0F);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 296 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT[3], Settings.WIDTH / 2.0F + 110.0F * Settings.scale, Settings.OPTION_Y - 118.0F * Settings.scale, c, 1.0F);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 306 */     c = Settings.CREAM_COLOR.cpy();
/* 307 */     c.a = this.uiColor.a;
/*     */     
/* 309 */     FontHelper.renderWrappedText(sb, FontHelper.tipBodyFont, this.desc, Settings.WIDTH / 2.0F, Settings.OPTION_Y + 20.0F * Settings.scale, 240.0F * Settings.scale, c, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 319 */     if (Settings.isControllerMode) {
/* 320 */       sb.draw(CInputActionSet.proceed
/* 321 */         .getKeyImg(), 770.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 337 */       sb.draw(CInputActionSet.cancel
/* 338 */         .getKeyImg(), 1150.0F * Settings.scale - 32.0F, Settings.OPTION_Y - 32.0F - 140.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 356 */     this.yesHb.render(sb);
/* 357 */     this.noHb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\ConfirmPopup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */