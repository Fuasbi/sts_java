package com.megacrit.cardcrawl.screens.options;

public abstract interface RemapInputElementListener
{
  public abstract void didStartRemapping(RemapInputElement paramRemapInputElement);
  
  public abstract boolean willRemap(RemapInputElement paramRemapInputElement, int paramInt1, int paramInt2);
  
  public abstract boolean willRemapController(RemapInputElement paramRemapInputElement, int paramInt1, int paramInt2);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\RemapInputElementListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */