/*      */ package com.megacrit.cardcrawl.screens.options;
/*      */ 
/*      */ import com.badlogic.gdx.Files;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.Graphics.DisplayMode;
/*      */ import com.badlogic.gdx.Input;
/*      */ import com.badlogic.gdx.files.FileHandle;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.DisplayConfig;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.core.Settings.GameLanguage;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*      */ import com.megacrit.cardcrawl.screens.DisplayOption;
/*      */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*      */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Iterator;
/*      */ 
/*      */ public class OptionsPanel implements DropdownMenuListener, com.megacrit.cardcrawl.helpers.HitboxListener
/*      */ {
/*   36 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Options Tip");
/*   37 */   public static final String[] MSG = tutorialStrings.TEXT;
/*   38 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*   39 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("OptionsPanel");
/*   40 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   private static final int RAW_W = 1920;
/*      */   private static final int RAW_H = 1080;
/*   43 */   private static final float SCREEN_CENTER_Y = Settings.HEIGHT / 2.0F - 64.0F * Settings.scale;
/*      */   
/*   45 */   private static final float GAME_SETTINGS_TAB_CX = 522.0F * Settings.scale;
/*   46 */   private static final float GAME_SETTINGS_TAB_CY = (Settings.isSixteenByTen ? 924.0F : 864.0F) * Settings.scale;
/*   47 */   private static final float INPUT_SETTINGS_TAB_CX = 918.0F * Settings.scale;
/*      */   
/*      */   private AbandonRunButton abandonBtn;
/*      */   
/*      */   private ExitGameButton exitBtn;
/*      */   
/*      */   private Hitbox inputSettingsButtonHitbox;
/*      */   
/*      */   public DropdownMenu fpsDropdown;
/*      */   
/*      */   public DropdownMenu resoDropdown;
/*      */   
/*      */   private Slider masterSlider;
/*      */   private Slider bgmSlider;
/*      */   private Slider sfxSlider;
/*   62 */   private static final float TOGGLE_X_LEFT = 430.0F * Settings.scale;
/*   63 */   private static final float TOGGLE_X_LEFT_2 = 660.0F * Settings.scale;
/*   64 */   private static final float TOGGLE_X_RIGHT = 1030.0F * Settings.scale;
/*      */   public ToggleButton fsToggle;
/*      */   public ToggleButton wfsToggle;
/*      */   public ToggleButton vSyncToggle;
/*      */   private ToggleButton ssToggle;
/*      */   private ToggleButton ambienceToggle;
/*      */   private ToggleButton muteBgToggle;
/*      */   private ToggleButton sumToggle;
/*      */   private ToggleButton blockToggle;
/*      */   private ToggleButton confirmToggle;
/*      */   private ToggleButton fastToggle;
/*      */   private ToggleButton cardKeyOverlayToggle;
/*      */   private ToggleButton uploadToggle;
/*   77 */   private ToggleButton playtesterToggle; private DropdownMenu languageDropdown; private String[] languageLabels; public ArrayList<AbstractGameEffect> effects = new ArrayList();
/*   78 */   private Hitbox currentHb = null;
/*      */   
/*      */ 
/*   81 */   public static final String[] LOCALIZED_LANGUAGE_LABELS = CardCrawlGame.languagePack.getUIString("LanguageDropdown").TEXT;
/*      */   
/*      */ 
/*   84 */   private String[] FRAMERATE_LABELS = { "24", "30", "60", "120", "240" };
/*   85 */   private int[] FRAMERATE_OPTIONS = { 24, 30, 60, 120, 240 };
/*      */   
/*      */ 
/*   88 */   private static final float LEFT_TOGGLE_X = 670.0F * Settings.scale;
/*   89 */   private static final float LEFT_TEXT_X = 410.0F * Settings.scale;
/*   90 */   private static final float LEFT_TOGGLE_TEXT_X = 456.0F * Settings.scale;
/*      */   
/*      */ 
/*      */ 
/*   94 */   private static final String HEADER_TEXT = TEXT[1];
/*   95 */   private static final String GRAPHICS_PANEL_HEADER_TEXT = TEXT[2];
/*   96 */   private static final String RESOLUTION_TEXTS = TEXT[3];
/*   97 */   private static final String FULLSCREEN_TEXTS = TEXT[4];
/*   98 */   private static final String SOUND_PANEL_HEADER_TEXT = TEXT[5];
/*   99 */   private static final String VOLUME_TEXTS = TEXT[6];
/*  100 */   private static final String OTHER_SOUND_TEXTS = TEXT[7];
/*  101 */   private static final String PREF_PANEL_HEADER_TEXT = TEXT[8];
/*  102 */   private static final String PREF_TEXTS = TEXT[9];
/*  103 */   private static final String FAST_MODE_TEXT = TEXT[10];
/*      */   
/*  105 */   private static final String MISC_PANEL_HEADER_TEXT = TEXT[12];
/*  106 */   private static final String LANGUAGE_TEXT = TEXT[13];
/*  107 */   private static final String UPLAOD_TEXT = TEXT[14];
/*  108 */   private static final String EXIT_TEXT = TEXT[15];
/*  109 */   private static final String SAVE_TEXT = TEXT[16];
/*  110 */   private static final String VSYNC_TEXT = TEXT[17];
/*  111 */   private static final String PLAYTESTER_ART_TEXT = TEXT[18];
/*  112 */   private static final String SHOW_CARD_QUICK_SELECT_TEXT = TEXT[19];
/*      */   
/*      */ 
/*      */ 
/*      */   public OptionsPanel()
/*      */   {
/*  118 */     this.fsToggle = new ToggleButton(TOGGLE_X_LEFT, 98.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.FULL_SCREEN, true);
/*  119 */     this.wfsToggle = new ToggleButton(TOGGLE_X_LEFT, 64.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.W_FULL_SCREEN, true);
/*  120 */     this.ssToggle = new ToggleButton(TOGGLE_X_LEFT, 30.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SCREEN_SHAKE);
/*  121 */     this.vSyncToggle = new ToggleButton(TOGGLE_X_LEFT_2, 30.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.V_SYNC);
/*      */     
/*  123 */     this.resoDropdown = new DropdownMenu(this, getResolutionLabels(), FontHelper.tipBodyFont, Settings.CREAM_COLOR);
/*  124 */     resetResolutionDropdownSelection();
/*      */     
/*  126 */     this.fpsDropdown = new DropdownMenu(this, this.FRAMERATE_LABELS, FontHelper.tipBodyFont, Settings.CREAM_COLOR);
/*  127 */     resetFpsDropdownSelection();
/*      */     
/*      */ 
/*  130 */     this.sumToggle = new ToggleButton(TOGGLE_X_LEFT, -122.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SUM_DMG, true);
/*  131 */     this.blockToggle = new ToggleButton(TOGGLE_X_LEFT, -156.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.BLOCK_DMG, true);
/*  132 */     this.confirmToggle = new ToggleButton(TOGGLE_X_LEFT, -190.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.HAND_CONF, true);
/*  133 */     this.fastToggle = new ToggleButton(TOGGLE_X_LEFT, -254.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.FAST_MODE, true);
/*  134 */     this.cardKeyOverlayToggle = new ToggleButton(TOGGLE_X_LEFT, -288.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SHOW_CARD_HOTKEYS, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  142 */     this.ambienceToggle = new ToggleButton(TOGGLE_X_RIGHT, 58.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.AMBIENCE_ON, true);
/*  143 */     this.muteBgToggle = new ToggleButton(TOGGLE_X_RIGHT, 24.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.MUTE_IF_BG, true);
/*      */     
/*  145 */     this.masterSlider = new Slider(SCREEN_CENTER_Y + 186.0F * Settings.scale, Settings.MASTER_VOLUME, Slider.SliderType.MASTER);
/*  146 */     this.bgmSlider = new Slider(SCREEN_CENTER_Y + 142.0F * Settings.scale, Settings.MUSIC_VOLUME, Slider.SliderType.BGM);
/*  147 */     this.sfxSlider = new Slider(SCREEN_CENTER_Y + 98.0F * Settings.scale, Settings.SOUND_VOLUME, Slider.SliderType.SFX);
/*      */     
/*      */ 
/*  150 */     if (Settings.isBeta) {
/*  151 */       this.playtesterToggle = new ToggleButton(TOGGLE_X_RIGHT, -220.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.PLAYTESTER_ART, true);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  158 */     this.uploadToggle = new ToggleButton(TOGGLE_X_RIGHT, -254.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.UPLOAD_DATA, true);
/*      */     
/*  160 */     this.languageLabels = languageLabels();
/*  161 */     this.languageDropdown = new DropdownMenu(this, this.languageLabels, FontHelper.tipBodyFont, Settings.CREAM_COLOR, 20);
/*  162 */     resetLanguageDropdownSelection();
/*      */     
/*      */ 
/*  165 */     this.exitBtn = new ExitGameButton();
/*  166 */     this.abandonBtn = new AbandonRunButton();
/*  167 */     this.inputSettingsButtonHitbox = new Hitbox(360.0F * Settings.scale, 50.0F * Settings.scale);
/*  168 */     this.inputSettingsButtonHitbox.move(INPUT_SETTINGS_TAB_CX, GAME_SETTINGS_TAB_CY);
/*      */   }
/*      */   
/*      */   public void update() {
/*  172 */     updateControllerInput();
/*      */     
/*  174 */     if (CardCrawlGame.isInARun()) {
/*  175 */       this.abandonBtn.update();
/*      */     }
/*  177 */     this.exitBtn.update();
/*      */     
/*  179 */     if ((Settings.isControllerMode) && 
/*  180 */       (CInputActionSet.pageRightViewExhaust.isJustPressed())) {
/*  181 */       clicked(this.inputSettingsButtonHitbox);
/*      */     }
/*      */     
/*      */ 
/*  185 */     this.inputSettingsButtonHitbox.encapsulatedUpdate(this);
/*      */     
/*      */ 
/*  188 */     if (this.fpsDropdown.isOpen) {
/*  189 */       this.fpsDropdown.update();
/*  190 */     } else if (this.resoDropdown.isOpen) {
/*  191 */       this.resoDropdown.update();
/*  192 */     } else if (this.languageDropdown.isOpen) {
/*  193 */       this.languageDropdown.update();
/*      */     } else {
/*  195 */       updateEffects();
/*  196 */       updateGraphics();
/*  197 */       updateSound();
/*  198 */       updatePreferences();
/*  199 */       updateMiscellaneous();
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateControllerInput() {
/*  204 */     if ((!Settings.isControllerMode) || (this.resoDropdown.isOpen) || (this.fpsDropdown.isOpen) || (this.languageDropdown.isOpen)) {
/*  205 */       return;
/*      */     }
/*      */     
/*  208 */     if ((AbstractDungeon.player != null) && 
/*  209 */       (AbstractDungeon.player.viewingRelics)) {
/*  210 */       return;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  215 */     if (this.resoDropdown.getHitbox().hovered) {
/*  216 */       if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  217 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  218 */         CInputHelper.setCursor(this.masterSlider.bgHb);
/*  219 */         this.currentHb = this.masterSlider.bgHb;
/*  220 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  221 */         CInputHelper.setCursor(this.fpsDropdown.getHitbox());
/*  222 */         this.currentHb = this.fpsDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  226 */     else if (this.fpsDropdown.getHitbox().hovered) {
/*  227 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  228 */         CInputHelper.setCursor(this.resoDropdown.getHitbox());
/*  229 */         this.currentHb = this.resoDropdown.getHitbox();
/*  230 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  231 */         CInputHelper.setCursor(this.fsToggle.hb);
/*  232 */         this.currentHb = this.fsToggle.hb;
/*  233 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  234 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  235 */         CInputHelper.setCursor(this.bgmSlider.bgHb);
/*  236 */         this.currentHb = this.bgmSlider.bgHb;
/*      */       }
/*      */       
/*      */     }
/*  240 */     else if (this.fsToggle.hb.hovered) {
/*  241 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  242 */         CInputHelper.setCursor(this.resoDropdown.getHitbox());
/*  243 */         this.currentHb = this.resoDropdown.getHitbox();
/*  244 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  245 */         CInputHelper.setCursor(this.wfsToggle.hb);
/*  246 */         this.currentHb = this.wfsToggle.hb;
/*  247 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  248 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  249 */         CInputHelper.setCursor(this.sfxSlider.bgHb);
/*  250 */         this.currentHb = this.sfxSlider.bgHb;
/*      */       }
/*      */       
/*      */     }
/*  254 */     else if (this.wfsToggle.hb.hovered) {
/*  255 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  256 */         CInputHelper.setCursor(this.fsToggle.hb);
/*  257 */         this.currentHb = this.fsToggle.hb;
/*  258 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  259 */         CInputHelper.setCursor(this.ssToggle.hb);
/*  260 */         this.currentHb = this.ssToggle.hb;
/*  261 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  262 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  263 */         CInputHelper.setCursor(this.ambienceToggle.hb);
/*  264 */         this.currentHb = this.ambienceToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  268 */     else if (this.ssToggle.hb.hovered) {
/*  269 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  270 */         CInputHelper.setCursor(this.wfsToggle.hb);
/*  271 */         this.currentHb = this.wfsToggle.hb;
/*  272 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  273 */         CInputHelper.setCursor(this.sumToggle.hb);
/*  274 */         this.currentHb = this.sumToggle.hb;
/*  275 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  276 */         CInputHelper.setCursor(this.vSyncToggle.hb);
/*  277 */         this.currentHb = this.vSyncToggle.hb;
/*  278 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  279 */         CInputHelper.setCursor(this.muteBgToggle.hb);
/*  280 */         this.currentHb = this.muteBgToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  284 */     else if (this.vSyncToggle.hb.hovered) {
/*  285 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  286 */         CInputHelper.setCursor(this.wfsToggle.hb);
/*  287 */         this.currentHb = this.wfsToggle.hb;
/*  288 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  289 */         CInputHelper.setCursor(this.sumToggle.hb);
/*  290 */         this.currentHb = this.sumToggle.hb;
/*  291 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  292 */         CInputHelper.setCursor(this.ssToggle.hb);
/*  293 */         this.currentHb = this.ssToggle.hb;
/*  294 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  295 */         CInputHelper.setCursor(this.muteBgToggle.hb);
/*  296 */         this.currentHb = this.muteBgToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  300 */     else if (this.sumToggle.hb.hovered) {
/*  301 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  302 */         CInputHelper.setCursor(this.ssToggle.hb);
/*  303 */         this.currentHb = this.ssToggle.hb;
/*  304 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  305 */         CInputHelper.setCursor(this.blockToggle.hb);
/*  306 */         this.currentHb = this.blockToggle.hb;
/*  307 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  308 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  309 */         CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  310 */         this.currentHb = this.languageDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  314 */     else if (this.blockToggle.hb.hovered) {
/*  315 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  316 */         CInputHelper.setCursor(this.sumToggle.hb);
/*  317 */         this.currentHb = this.sumToggle.hb;
/*  318 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  319 */         CInputHelper.setCursor(this.confirmToggle.hb);
/*  320 */         this.currentHb = this.confirmToggle.hb;
/*  321 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  322 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  323 */         CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  324 */         this.currentHb = this.languageDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  328 */     else if (this.confirmToggle.hb.hovered) {
/*  329 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  330 */         CInputHelper.setCursor(this.blockToggle.hb);
/*  331 */         this.currentHb = this.blockToggle.hb;
/*  332 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  333 */         CInputHelper.setCursor(this.fastToggle.hb);
/*  334 */         this.currentHb = this.fastToggle.hb;
/*  335 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  336 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  337 */         CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  338 */         this.currentHb = this.languageDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  342 */     else if (this.fastToggle.hb.hovered) {
/*  343 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  344 */         CInputHelper.setCursor(this.confirmToggle.hb);
/*  345 */         this.currentHb = this.confirmToggle.hb;
/*  346 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  347 */         CInputHelper.setCursor(this.cardKeyOverlayToggle.hb);
/*  348 */         this.currentHb = this.cardKeyOverlayToggle.hb;
/*  349 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  350 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  351 */         CInputHelper.setCursor(this.uploadToggle.hb);
/*  352 */         this.currentHb = this.uploadToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  356 */     else if (this.cardKeyOverlayToggle.hb.hovered) {
/*  357 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  358 */         CInputHelper.setCursor(this.fastToggle.hb);
/*  359 */         this.currentHb = this.fastToggle.hb;
/*  360 */       } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()) || 
/*  361 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  362 */         CInputHelper.setCursor(this.uploadToggle.hb);
/*  363 */         this.currentHb = this.uploadToggle.hb;
/*  364 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  365 */         CInputHelper.setCursor(this.masterSlider.bgHb);
/*  366 */         this.currentHb = this.masterSlider.bgHb;
/*      */       }
/*      */       
/*      */     }
/*  370 */     else if (this.masterSlider.bgHb.hovered) {
/*  371 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  372 */         CInputHelper.setCursor(this.bgmSlider.bgHb);
/*  373 */         this.currentHb = this.bgmSlider.bgHb;
/*  374 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  375 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  376 */         CInputHelper.setCursor(this.resoDropdown.getHitbox());
/*  377 */         this.currentHb = this.resoDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  381 */     else if (this.bgmSlider.bgHb.hovered) {
/*  382 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  383 */         CInputHelper.setCursor(this.sfxSlider.bgHb);
/*  384 */         this.currentHb = this.sfxSlider.bgHb;
/*  385 */       } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  386 */         CInputHelper.setCursor(this.masterSlider.bgHb);
/*  387 */         this.currentHb = this.masterSlider.bgHb;
/*  388 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  389 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  390 */         CInputHelper.setCursor(this.fpsDropdown.getHitbox());
/*  391 */         this.currentHb = this.fpsDropdown.getHitbox();
/*      */       }
/*      */       
/*      */     }
/*  395 */     else if (this.sfxSlider.bgHb.hovered) {
/*  396 */       if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  397 */         CInputHelper.setCursor(this.ambienceToggle.hb);
/*  398 */         this.currentHb = this.ambienceToggle.hb;
/*  399 */       } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  400 */         CInputHelper.setCursor(this.bgmSlider.bgHb);
/*  401 */         this.currentHb = this.bgmSlider.bgHb;
/*  402 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  403 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  404 */         CInputHelper.setCursor(this.fsToggle.hb);
/*  405 */         this.currentHb = this.fsToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  409 */     else if (this.ambienceToggle.hb.hovered) {
/*  410 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  411 */         CInputHelper.setCursor(this.sfxSlider.bgHb);
/*  412 */         this.currentHb = this.sfxSlider.bgHb;
/*  413 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  414 */         CInputHelper.setCursor(this.muteBgToggle.hb);
/*  415 */         this.currentHb = this.muteBgToggle.hb;
/*  416 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  417 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  418 */         CInputHelper.setCursor(this.wfsToggle.hb);
/*  419 */         this.currentHb = this.wfsToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  423 */     else if (this.muteBgToggle.hb.hovered) {
/*  424 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  425 */         CInputHelper.setCursor(this.ambienceToggle.hb);
/*  426 */         this.currentHb = this.ambienceToggle.hb;
/*  427 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  428 */         CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  429 */         this.currentHb = this.languageDropdown.getHitbox();
/*  430 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  431 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  432 */         CInputHelper.setCursor(this.vSyncToggle.hb);
/*  433 */         this.currentHb = this.vSyncToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  437 */     else if (this.languageDropdown.getHitbox().hovered) {
/*  438 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  439 */         CInputHelper.setCursor(this.muteBgToggle.hb);
/*  440 */         this.currentHb = this.muteBgToggle.hb;
/*  441 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  442 */         if (Settings.isBeta) {
/*  443 */           CInputHelper.setCursor(this.playtesterToggle.hb);
/*  444 */           this.currentHb = this.playtesterToggle.hb;
/*      */         } else {
/*  446 */           CInputHelper.setCursor(this.uploadToggle.hb);
/*  447 */           this.currentHb = this.uploadToggle.hb;
/*      */         }
/*  449 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  450 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  451 */         CInputHelper.setCursor(this.sumToggle.hb);
/*  452 */         this.currentHb = this.sumToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  456 */     else if ((Settings.isBeta) && (this.playtesterToggle.hb.hovered)) {
/*  457 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  458 */         CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  459 */         this.currentHb = this.languageDropdown.getHitbox();
/*  460 */       } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  461 */         CInputHelper.setCursor(this.uploadToggle.hb);
/*  462 */         this.currentHb = this.uploadToggle.hb;
/*  463 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  464 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  465 */         CInputHelper.setCursor(this.fastToggle.hb);
/*  466 */         this.currentHb = this.fastToggle.hb;
/*      */       }
/*      */       
/*      */     }
/*  470 */     else if (this.uploadToggle.hb.hovered) {
/*  471 */       if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  472 */         if (Settings.isBeta) {
/*  473 */           CInputHelper.setCursor(this.playtesterToggle.hb);
/*  474 */           this.currentHb = this.playtesterToggle.hb;
/*      */         } else {
/*  476 */           CInputHelper.setCursor(this.languageDropdown.getHitbox());
/*  477 */           this.currentHb = this.languageDropdown.getHitbox();
/*      */         }
/*  479 */       } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/*  480 */         (CInputActionSet.altRight.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  481 */         CInputHelper.setCursor(this.fastToggle.hb);
/*  482 */         this.currentHb = this.fastToggle.hb;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  487 */       CInputHelper.setCursor(this.resoDropdown.getHitbox());
/*  488 */       this.currentHb = this.resoDropdown.getHitbox();
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateEffects() {
/*  493 */     for (Iterator<AbstractGameEffect> c = this.effects.iterator(); c.hasNext();) {
/*  494 */       AbstractGameEffect e = (AbstractGameEffect)c.next();
/*  495 */       e.update();
/*  496 */       if (e.isDone) {
/*  497 */         c.remove();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateGraphics() {
/*  503 */     this.fsToggle.update();
/*  504 */     this.wfsToggle.update();
/*  505 */     this.ssToggle.update();
/*  506 */     this.vSyncToggle.update();
/*  507 */     this.resoDropdown.update();
/*  508 */     this.fpsDropdown.update();
/*      */     
/*  510 */     if (this.fsToggle.hb.hovered) {
/*  511 */       TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, LABEL[1], MSG[1]);
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  516 */     else if (this.wfsToggle.hb.hovered) {
/*  517 */       TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, LABEL[2], MSG[2]);
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  522 */     else if (this.fpsDropdown.getHitbox().hovered) {
/*  523 */       TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, LABEL[3], MSG[3]);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void updateSound()
/*      */   {
/*  532 */     this.ambienceToggle.update();
/*  533 */     this.muteBgToggle.update();
/*  534 */     this.masterSlider.update();
/*  535 */     this.bgmSlider.update();
/*  536 */     this.sfxSlider.update();
/*      */   }
/*      */   
/*      */   private void updatePreferences() {
/*  540 */     this.sumToggle.update();
/*  541 */     this.blockToggle.update();
/*  542 */     this.confirmToggle.update();
/*  543 */     this.fastToggle.update();
/*  544 */     this.cardKeyOverlayToggle.update();
/*      */   }
/*      */   
/*      */   private void updateMiscellaneous() {
/*  548 */     this.uploadToggle.update();
/*  549 */     if (Settings.isBeta) {
/*  550 */       this.playtesterToggle.update();
/*      */     }
/*  552 */     if (this.uploadToggle.hb.hovered) {
/*  553 */       TipHelper.renderGenericTip(InputHelper.mX + 70.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, LABEL[0], MSG[0]);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  559 */     this.languageDropdown.update();
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb) {
/*  563 */     renderBg(sb);
/*  564 */     renderBanner(sb);
/*  565 */     renderGraphics(sb);
/*  566 */     renderSound(sb);
/*  567 */     renderPreferences(sb);
/*  568 */     renderMiscellaneous(sb);
/*      */     
/*  570 */     if (CardCrawlGame.isInARun()) {
/*  571 */       this.abandonBtn.render(sb);
/*      */     }
/*  573 */     this.exitBtn.render(sb);
/*      */     
/*  575 */     this.languageDropdown.render(sb, 1150.0F * Settings.scale, SCREEN_CENTER_Y - 145.0F * Settings.scale);
/*      */     
/*      */ 
/*  578 */     if (this.resoDropdown.isOpen) {
/*  579 */       this.fpsDropdown.render(sb, LEFT_TOGGLE_X, SCREEN_CENTER_Y + 160.0F * Settings.scale);
/*  580 */       this.resoDropdown.render(sb, LEFT_TOGGLE_X, SCREEN_CENTER_Y + 206.0F * Settings.scale);
/*      */     } else {
/*  582 */       this.resoDropdown.render(sb, LEFT_TOGGLE_X, SCREEN_CENTER_Y + 206.0F * Settings.scale);
/*  583 */       this.fpsDropdown.render(sb, LEFT_TOGGLE_X, SCREEN_CENTER_Y + 160.0F * Settings.scale);
/*      */     }
/*      */     
/*  586 */     for (AbstractGameEffect e : this.effects) {
/*  587 */       e.render(sb);
/*      */     }
/*      */     
/*  590 */     renderControllerUi(sb, this.currentHb);
/*      */   }
/*      */   
/*      */   private void renderControllerUi(SpriteBatch sb, Hitbox hb) {
/*  594 */     if (!Settings.isControllerMode) {
/*  595 */       return;
/*      */     }
/*  597 */     if (hb != null) {
/*  598 */       sb.setBlendFunction(770, 1);
/*  599 */       sb.setColor(new Color(0.7F, 0.9F, 1.0F, 0.25F));
/*  600 */       sb.draw(ImageMaster.CONTROLLER_HB_HIGHLIGHT, hb.cX - hb.width / 2.0F, hb.cY - hb.height / 2.0F, hb.width, hb.height);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  606 */       sb.setBlendFunction(770, 771);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderBg(SpriteBatch sb) {
/*  611 */     sb.setColor(Color.WHITE);
/*      */     
/*  613 */     sb.draw(ImageMaster.INPUT_SETTINGS_EDGES, Settings.WIDTH / 2.0F - 960.0F, Settings.OPTION_Y - 540.0F, 960.0F, 540.0F, 1920.0F, 1080.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1080, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  631 */     sb.draw(ImageMaster.SETTINGS_BACKGROUND, Settings.WIDTH / 2.0F - 960.0F, Settings.OPTION_Y - 540.0F, 960.0F, 540.0F, 1920.0F, 1080.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1080, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  649 */     if (Settings.isControllerMode) {
/*  650 */       sb.draw(CInputActionSet.pageRightViewExhaust
/*  651 */         .getKeyImg(), INPUT_SETTINGS_TAB_CX - 32.0F + 
/*  652 */         FontHelper.getSmartWidth(FontHelper.deckBannerFont, TEXT[20], 99999.0F, 0.0F) / 2.0F + 42.0F * Settings.scale, Settings.OPTION_Y - 32.0F + 354.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBanner(SpriteBatch sb)
/*      */   {
/*  673 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, HEADER_TEXT, GAME_SETTINGS_TAB_CX, GAME_SETTINGS_TAB_CY, Settings.GOLD_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  682 */     Color textColor = this.inputSettingsButtonHitbox.hovered ? Settings.GOLD_COLOR : Color.LIGHT_GRAY;
/*  683 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, TEXT[20], INPUT_SETTINGS_TAB_CX, GAME_SETTINGS_TAB_CY, textColor);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  690 */     this.inputSettingsButtonHitbox.render(sb);
/*      */   }
/*      */   
/*      */   private void renderGraphics(SpriteBatch sb)
/*      */   {
/*  695 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, GRAPHICS_PANEL_HEADER_TEXT, 636.0F * Settings.scale, SCREEN_CENTER_Y + 250.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  704 */     FontHelper.renderSmartText(sb, FontHelper.rewardTipFont, RESOLUTION_TEXTS, LEFT_TEXT_X, SCREEN_CENTER_Y + 196.0F * Settings.scale, 10000.0F, 36.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  714 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, FULLSCREEN_TEXTS, LEFT_TOGGLE_TEXT_X, SCREEN_CENTER_Y + 106.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  725 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, VSYNC_TEXT, 686.0F * Settings.scale, SCREEN_CENTER_Y + 106.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  735 */     this.fsToggle.render(sb);
/*  736 */     this.wfsToggle.render(sb);
/*  737 */     this.ssToggle.render(sb);
/*  738 */     this.vSyncToggle.render(sb);
/*      */   }
/*      */   
/*      */   private void renderSound(SpriteBatch sb) {
/*  742 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, SOUND_PANEL_HEADER_TEXT, 1264.0F * Settings.scale, SCREEN_CENTER_Y + 250.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  750 */     FontHelper.renderSmartText(sb, FontHelper.rewardTipFont, VOLUME_TEXTS, 1020.0F * Settings.scale, SCREEN_CENTER_Y + 196.0F * Settings.scale, 10000.0F, 44.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  760 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, OTHER_SOUND_TEXTS, 1056.0F * Settings.scale, SCREEN_CENTER_Y + 66.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  770 */     this.masterSlider.render(sb);
/*  771 */     this.bgmSlider.render(sb);
/*  772 */     this.sfxSlider.render(sb);
/*  773 */     this.ambienceToggle.render(sb);
/*  774 */     this.muteBgToggle.render(sb);
/*      */   }
/*      */   
/*      */   private void renderPreferences(SpriteBatch sb) {
/*  778 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, PREF_PANEL_HEADER_TEXT, 636.0F * Settings.scale, SCREEN_CENTER_Y - 66.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  786 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, PREF_TEXTS, 456.0F * Settings.scale, SCREEN_CENTER_Y + -112.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  796 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, FAST_MODE_TEXT, 456.0F * Settings.scale, SCREEN_CENTER_Y + -246.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  806 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, SHOW_CARD_QUICK_SELECT_TEXT, 456.0F * Settings.scale, SCREEN_CENTER_Y + -280.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  816 */     this.sumToggle.render(sb);
/*  817 */     this.blockToggle.render(sb);
/*  818 */     this.confirmToggle.render(sb);
/*  819 */     this.fastToggle.render(sb);
/*  820 */     this.cardKeyOverlayToggle.render(sb);
/*      */   }
/*      */   
/*      */   private void renderMiscellaneous(SpriteBatch sb)
/*      */   {
/*  825 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, MISC_PANEL_HEADER_TEXT, 1264.0F * Settings.scale, SCREEN_CENTER_Y - 66.0F * Settings.scale, Settings.GOLD_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  833 */     FontHelper.renderSmartText(sb, FontHelper.rewardTipFont, LANGUAGE_TEXT, 1020.0F * Settings.scale, SCREEN_CENTER_Y + -150.0F * Settings.scale, 10000.0F, 44.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  843 */     if (Settings.isBeta) {
/*  844 */       FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, PLAYTESTER_ART_TEXT, 1056.0F * Settings.scale, SCREEN_CENTER_Y + -212.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  855 */     FontHelper.renderSmartText(sb, FontHelper.tipBodyFont, UPLAOD_TEXT, 1056.0F * Settings.scale, SCREEN_CENTER_Y + -248.0F * Settings.scale, 10000.0F, 34.0F * Settings.scale, Settings.CREAM_COLOR);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  865 */     this.uploadToggle.render(sb);
/*  866 */     if (Settings.isBeta) {
/*  867 */       this.playtesterToggle.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void refresh() {
/*  872 */     this.currentHb = null;
/*  873 */     this.fsToggle = new ToggleButton(TOGGLE_X_LEFT, 98.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.FULL_SCREEN);
/*  874 */     this.wfsToggle = new ToggleButton(TOGGLE_X_LEFT, 64.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.W_FULL_SCREEN);
/*  875 */     this.ssToggle = new ToggleButton(TOGGLE_X_LEFT, 30.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SCREEN_SHAKE);
/*  876 */     this.vSyncToggle = new ToggleButton(TOGGLE_X_LEFT_2, 30.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.V_SYNC);
/*  877 */     this.sumToggle = new ToggleButton(TOGGLE_X_LEFT, -122.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SUM_DMG);
/*  878 */     this.blockToggle = new ToggleButton(TOGGLE_X_LEFT, -156.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.BLOCK_DMG);
/*  879 */     this.confirmToggle = new ToggleButton(TOGGLE_X_LEFT, -190.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.HAND_CONF);
/*  880 */     this.fastToggle = new ToggleButton(TOGGLE_X_LEFT, -254.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.FAST_MODE);
/*  881 */     this.cardKeyOverlayToggle = new ToggleButton(TOGGLE_X_LEFT, -288.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.SHOW_CARD_HOTKEYS);
/*  882 */     this.ambienceToggle = new ToggleButton(TOGGLE_X_RIGHT, 58.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.AMBIENCE_ON);
/*  883 */     this.muteBgToggle = new ToggleButton(TOGGLE_X_RIGHT, 24.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.MUTE_IF_BG);
/*  884 */     if (Settings.isBeta) {
/*  885 */       this.playtesterToggle = new ToggleButton(TOGGLE_X_RIGHT, -220.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.PLAYTESTER_ART);
/*      */     }
/*  887 */     this.uploadToggle = new ToggleButton(TOGGLE_X_RIGHT, -254.0F, SCREEN_CENTER_Y, ToggleButton.ToggleBtnType.UPLOAD_DATA);
/*      */     
/*  889 */     this.masterSlider = new Slider(SCREEN_CENTER_Y + 186.0F * Settings.scale, Settings.MASTER_VOLUME, Slider.SliderType.MASTER);
/*  890 */     this.bgmSlider = new Slider(SCREEN_CENTER_Y + 142.0F * Settings.scale, Settings.MUSIC_VOLUME, Slider.SliderType.BGM);
/*  891 */     this.sfxSlider = new Slider(SCREEN_CENTER_Y + 98.0F * Settings.scale, Settings.SOUND_VOLUME, Slider.SliderType.SFX);
/*      */     
/*  893 */     this.resoDropdown = new DropdownMenu(this, getResolutionLabels(), FontHelper.tipBodyFont, Settings.CREAM_COLOR);
/*  894 */     resetResolutionDropdownSelection();
/*      */     
/*  896 */     this.fpsDropdown = new DropdownMenu(this, this.FRAMERATE_LABELS, FontHelper.tipBodyFont, Settings.CREAM_COLOR);
/*  897 */     resetFpsDropdownSelection();
/*      */     
/*  899 */     this.languageDropdown = new DropdownMenu(this, this.languageLabels, FontHelper.tipBodyFont, Settings.CREAM_COLOR);
/*  900 */     resetLanguageDropdownSelection();
/*      */     
/*  902 */     this.exitBtn.updateLabel(SAVE_TEXT);
/*      */     
/*  904 */     switch (AbstractDungeon.player.chosenClass) {
/*      */     case IRONCLAD: 
/*  906 */       if (!Gdx.files.local(SaveAndContinue.IRONCLAD_FILE).exists()) {
/*  907 */         this.exitBtn.updateLabel(EXIT_TEXT);
/*      */       }
/*      */       break;
/*      */     case THE_SILENT: 
/*  911 */       if (!Gdx.files.local(SaveAndContinue.THE_SILENT_FILE).exists()) {
/*  912 */         this.exitBtn.updateLabel(EXIT_TEXT);
/*      */       }
/*      */       break;
/*      */     case DEFECT: 
/*  916 */       if (!Gdx.files.local(SaveAndContinue.DEFECT_FILE).exists()) {
/*  917 */         this.exitBtn.updateLabel(TEXT[15]);
/*      */       }
/*      */       break;
/*      */     default: 
/*  921 */       System.out.print(AbstractDungeon.player.chosenClass.toString() + " NOT FOUND IN CONFIRMPOPUP()");
/*      */     }
/*      */   }
/*      */   
/*      */   private void displayRestartRequiredText()
/*      */   {
/*  927 */     if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/*  928 */       if (CardCrawlGame.mainMenuScreen != null) {
/*  929 */         CardCrawlGame.mainMenuScreen.optionPanel.effects.clear();
/*  930 */         CardCrawlGame.mainMenuScreen.optionPanel.effects.add(new com.megacrit.cardcrawl.vfx.RestartForChangesEffect());
/*      */       }
/*      */     } else {
/*  933 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.RestartForChangesEffect());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void changedSelectionTo(DropdownMenu dropdownMenu, int index, String optionText)
/*      */   {
/*  941 */     if (dropdownMenu == this.languageDropdown) {
/*  942 */       changeLanguageToIndex(index);
/*  943 */       if (Settings.isControllerMode) {
/*  944 */         Gdx.input.setCursorPosition(
/*  945 */           (int)this.languageDropdown.getHitbox().cX, Settings.HEIGHT - 
/*  946 */           (int)this.languageDropdown.getHitbox().cY);
/*  947 */         this.currentHb = this.languageDropdown.getHitbox();
/*      */       }
/*  949 */     } else if (dropdownMenu == this.resoDropdown) {
/*  950 */       changeResolutionToIndex(index);
/*  951 */       if (Settings.isControllerMode) {
/*  952 */         Gdx.input.setCursorPosition(
/*  953 */           (int)this.resoDropdown.getHitbox().cX, Settings.HEIGHT - 
/*  954 */           (int)this.resoDropdown.getHitbox().cY);
/*  955 */         this.currentHb = this.resoDropdown.getHitbox();
/*      */       }
/*  957 */     } else if (dropdownMenu == this.fpsDropdown) {
/*  958 */       changeFrameRateToIndex(index);
/*  959 */       if (Settings.isControllerMode) {
/*  960 */         Gdx.input.setCursorPosition(
/*  961 */           (int)this.fpsDropdown.getHitbox().cX, Settings.HEIGHT - 
/*  962 */           (int)this.fpsDropdown.getHitbox().cY);
/*  963 */         this.currentHb = this.fpsDropdown.getHitbox();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private void resetLanguageDropdownSelection()
/*      */   {
/*  971 */     Settings.GameLanguage[] languageOptions = LanguageOptions();
/*  972 */     for (int i = 0; i < languageOptions.length; i++) {
/*  973 */       if (Settings.language == languageOptions[i]) {
/*  974 */         this.languageDropdown.setSelectedIndex(i);
/*  975 */         return;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void changeLanguageToIndex(int index) {
/*  981 */     if (index >= LOCALIZED_LANGUAGE_LABELS.length)
/*      */     {
/*  983 */       return;
/*      */     }
/*      */     
/*  986 */     Settings.GameLanguage[] languageOptions = LanguageOptions();
/*  987 */     for (int i = 0; i < languageOptions.length; i++) {
/*  988 */       if ((Settings.language == languageOptions[i]) && 
/*  989 */         (i == index)) {
/*  990 */         return;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  995 */     Settings.setLanguage(LanguageOptions()[index], false);
/*  996 */     displayRestartRequiredText();
/*      */   }
/*      */   
/*      */   public String[] languageLabels() {
/* 1000 */     return new String[] { LOCALIZED_LANGUAGE_LABELS[3], LOCALIZED_LANGUAGE_LABELS[0], LOCALIZED_LANGUAGE_LABELS[1], LOCALIZED_LANGUAGE_LABELS[2], LOCALIZED_LANGUAGE_LABELS[25], LOCALIZED_LANGUAGE_LABELS[4], LOCALIZED_LANGUAGE_LABELS[5], LOCALIZED_LANGUAGE_LABELS[23], LOCALIZED_LANGUAGE_LABELS[24], LOCALIZED_LANGUAGE_LABELS[6], LOCALIZED_LANGUAGE_LABELS[7], LOCALIZED_LANGUAGE_LABELS[8], LOCALIZED_LANGUAGE_LABELS[15], LOCALIZED_LANGUAGE_LABELS[9], LOCALIZED_LANGUAGE_LABELS[10], LOCALIZED_LANGUAGE_LABELS[17], LOCALIZED_LANGUAGE_LABELS[21], LOCALIZED_LANGUAGE_LABELS[11], LOCALIZED_LANGUAGE_LABELS[16], LOCALIZED_LANGUAGE_LABELS[13], LOCALIZED_LANGUAGE_LABELS[18] };
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Settings.GameLanguage[] LanguageOptions()
/*      */   {
/* 1025 */     return new Settings.GameLanguage[] { Settings.GameLanguage.ENG, Settings.GameLanguage.PTB, Settings.GameLanguage.ZHS, Settings.GameLanguage.ZHT, Settings.GameLanguage.EPO, Settings.GameLanguage.FRA, Settings.GameLanguage.DEU, Settings.GameLanguage.GRE, Settings.GameLanguage.IND, Settings.GameLanguage.ITA, Settings.GameLanguage.JPN, Settings.GameLanguage.KOR, Settings.GameLanguage.NOR, Settings.GameLanguage.POL, Settings.GameLanguage.RUS, Settings.GameLanguage.SRP, Settings.GameLanguage.SRB, Settings.GameLanguage.SPA, Settings.GameLanguage.THA, Settings.GameLanguage.TUR, Settings.GameLanguage.UKR };
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void changeFrameRateToIndex(int index)
/*      */   {
/* 1037 */     if (Settings.MAX_FPS != this.FRAMERATE_OPTIONS[index]) {
/* 1038 */       this.fpsDropdown.setSelectedIndex(index);
/* 1039 */       Settings.MAX_FPS = this.FRAMERATE_OPTIONS[index];
/* 1040 */       DisplayConfig.writeDisplayConfigFile(Settings.SAVED_WIDTH, Settings.SAVED_HEIGHT, Settings.MAX_FPS, Settings.IS_FULLSCREEN, Settings.IS_W_FULLSCREEN, Settings.IS_V_SYNC);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1047 */       displayRestartRequiredText();
/*      */     }
/*      */   }
/*      */   
/*      */   private void resetFpsDropdownSelection() {
/* 1052 */     boolean found = false;
/* 1053 */     for (int i = 0; i < this.FRAMERATE_OPTIONS.length; i++) {
/* 1054 */       if (Settings.MAX_FPS == this.FRAMERATE_OPTIONS[i]) {
/* 1055 */         found = true;
/* 1056 */         changeFrameRateToIndex(i);
/* 1057 */         this.fpsDropdown.setSelectedIndex(i);
/*      */       }
/*      */     }
/* 1060 */     if (!found) {
/* 1061 */       Settings.MAX_FPS = 60;
/* 1062 */       changeFrameRateToIndex(2);
/* 1063 */       this.fpsDropdown.setSelectedIndex(2);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private void changeResolutionToIndex(int index)
/*      */   {
/* 1070 */     if (Settings.displayIndex == index) {
/* 1071 */       return;
/*      */     }
/* 1073 */     this.resoDropdown.setSelectedIndex(index);
/* 1074 */     Settings.displayIndex = index;
/* 1075 */     displayRestartRequiredText();
/*      */     
/* 1077 */     if (this.resoDropdown.getSelectedIndex() != Settings.displayOptions.size() - 1) {
/* 1078 */       if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/* 1079 */         if (CardCrawlGame.mainMenuScreen.optionPanel.fsToggle.enabled) {
/* 1080 */           CardCrawlGame.mainMenuScreen.optionPanel.fsToggle.toggle();
/*      */         }
/* 1082 */         if (CardCrawlGame.mainMenuScreen.optionPanel.wfsToggle.enabled) {
/* 1083 */           CardCrawlGame.mainMenuScreen.optionPanel.wfsToggle.toggle();
/*      */         }
/*      */       } else {
/* 1086 */         if (AbstractDungeon.settingsScreen.panel.fsToggle.enabled) {
/* 1087 */           AbstractDungeon.settingsScreen.panel.fsToggle.toggle();
/*      */         }
/* 1089 */         if (AbstractDungeon.settingsScreen.panel.wfsToggle.enabled) {
/* 1090 */           AbstractDungeon.settingsScreen.panel.wfsToggle.toggle();
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1095 */     if (index > Settings.displayOptions.size() - 1) {
/* 1096 */       index = 0;
/*      */     }
/* 1098 */     int TMP_WIDTH = ((DisplayOption)Settings.displayOptions.get(index)).width;
/* 1099 */     int TMP_HEIGHT = ((DisplayOption)Settings.displayOptions.get(index)).height;
/*      */     
/* 1101 */     DisplayConfig.writeDisplayConfigFile(TMP_WIDTH, TMP_HEIGHT, Settings.MAX_FPS, Settings.IS_FULLSCREEN, Settings.IS_W_FULLSCREEN, Settings.IS_V_SYNC);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1109 */     Settings.SAVED_WIDTH = TMP_WIDTH;
/* 1110 */     Settings.SAVED_HEIGHT = TMP_HEIGHT;
/*      */   }
/*      */   
/*      */   private void resetResolutionDropdownSelection() {
/* 1114 */     for (int i = 0; i < Settings.displayOptions.size(); i++) {
/* 1115 */       if ((Settings.WIDTH == ((DisplayOption)Settings.displayOptions.get(i)).width) && 
/* 1116 */         (Settings.HEIGHT == ((DisplayOption)Settings.displayOptions.get(i)).height)) {
/* 1117 */         Settings.displayIndex = i;
/* 1118 */         this.resoDropdown.setSelectedIndex(i);
/* 1119 */         return;
/*      */       }
/*      */     }
/* 1122 */     this.resoDropdown.setSelectedIndex(Settings.displayIndex);
/*      */   }
/*      */   
/*      */   private ArrayList<String> getResolutionLabels() {
/* 1126 */     initalizeDisplayOptionsIfNull();
/* 1127 */     ArrayList<String> labels = new ArrayList();
/* 1128 */     for (DisplayOption option : Settings.displayOptions) {
/* 1129 */       labels.add(option.uiString());
/*      */     }
/* 1131 */     return labels;
/*      */   }
/*      */   
/*      */   private void initalizeDisplayOptionsIfNull() {
/* 1135 */     if (Settings.displayOptions == null)
/*      */     {
/* 1137 */       ArrayList<DisplayOption> availableResos = new ArrayList();
/*      */       
/*      */ 
/* 1140 */       availableResos.add(new DisplayOption(1024, 576));
/* 1141 */       availableResos.add(new DisplayOption(1280, 720));
/* 1142 */       availableResos.add(new DisplayOption(1366, 768));
/* 1143 */       availableResos.add(new DisplayOption(1536, 864));
/* 1144 */       availableResos.add(new DisplayOption(1600, 900));
/* 1145 */       availableResos.add(new DisplayOption(1920, 1080));
/* 1146 */       availableResos.add(new DisplayOption(2560, 1440));
/* 1147 */       availableResos.add(new DisplayOption(3840, 2160));
/*      */       
/*      */ 
/* 1150 */       availableResos.add(new DisplayOption(1024, 640));
/* 1151 */       availableResos.add(new DisplayOption(1280, 800));
/* 1152 */       availableResos.add(new DisplayOption(1680, 1050));
/* 1153 */       availableResos.add(new DisplayOption(1920, 1200));
/* 1154 */       availableResos.add(new DisplayOption(2560, 1600));
/*      */       
/*      */ 
/*      */ 
/* 1158 */       DisplayOption screenRes = new DisplayOption(Gdx.graphics.getDisplayMode().width, Gdx.graphics.getDisplayMode().height);
/* 1159 */       if (!availableResos.contains(screenRes)) {
/* 1160 */         availableResos.add(screenRes);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1166 */       Graphics.DisplayMode[] modes = Gdx.graphics.getDisplayModes(Gdx.graphics.getPrimaryMonitor());
/* 1167 */       ArrayList<DisplayOption> monitorResos = new ArrayList();
/*      */       
/* 1169 */       for (Graphics.DisplayMode m : modes) {
/* 1170 */         DisplayOption o = new DisplayOption(m.width, m.height);
/* 1171 */         if (!monitorResos.contains(o)) {
/* 1172 */           monitorResos.add(o);
/*      */         }
/*      */       }
/* 1175 */       Settings.displayOptions = new ArrayList();
/*      */       
/* 1177 */       for (??? = availableResos.iterator(); ((Iterator)???).hasNext();) { DisplayOption o = (DisplayOption)((Iterator)???).next();
/* 1178 */         if ((o.width <= Gdx.graphics.getDisplayMode().width) && 
/* 1179 */           (o.height <= Gdx.graphics.getDisplayMode().height)) {
/* 1180 */           Settings.displayOptions.add(o);
/*      */         }
/*      */       }
/*      */       
/* 1184 */       java.util.Collections.sort(Settings.displayOptions);
/*      */     }
/*      */   }
/*      */   
/*      */   public void setFullscreen(boolean borderless) {
/* 1189 */     int TMP_WIDTH = Gdx.graphics.getDisplayMode().width;
/* 1190 */     int TMP_HEIGHT = Gdx.graphics.getDisplayMode().height;
/*      */     
/* 1192 */     DisplayConfig.writeDisplayConfigFile(TMP_WIDTH, TMP_HEIGHT, Settings.MAX_FPS, !borderless, borderless, Settings.IS_V_SYNC);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1200 */     Settings.SAVED_WIDTH = TMP_WIDTH;
/* 1201 */     Settings.SAVED_HEIGHT = TMP_HEIGHT;
/*      */     
/* 1203 */     for (int i = 0; i < Settings.displayOptions.size(); i++) {
/* 1204 */       if (((DisplayOption)Settings.displayOptions.get(i)).equals(new DisplayOption(TMP_WIDTH, TMP_HEIGHT))) {
/* 1205 */         changeResolutionToIndex(i);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void hoverStarted(Hitbox hitbox)
/*      */   {
/* 1218 */     CardCrawlGame.sound.play("UI_HOVER");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void startClicking(Hitbox hitbox)
/*      */   {
/* 1226 */     CardCrawlGame.sound.play("UI_CLICK_1");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void clicked(Hitbox hitbox)
/*      */   {
/* 1234 */     if (hitbox == this.inputSettingsButtonHitbox) {
/* 1235 */       if (CardCrawlGame.isInARun()) {
/* 1236 */         AbstractDungeon.inputSettingsScreen.open(false);
/*      */       }
/*      */       else {
/* 1239 */         CardCrawlGame.cancelButton.hideInstantly();
/* 1240 */         CardCrawlGame.mainMenuScreen.inputSettingsScreen.open(false);
/* 1241 */         CardCrawlGame.mainMenuScreen.screen = com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen.INPUT_SETTINGS;
/* 1242 */         CardCrawlGame.mainMenuScreen.isSettingsUp = false;
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\OptionsPanel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */