/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class GiantToggleButton
/*     */ {
/*  17 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(GiantToggleButton.class.getName());
/*  18 */   public boolean ticked = false;
/*     */   public ToggleType type;
/*     */   private String label;
/*  21 */   Hitbox hb = new Hitbox(320.0F * Settings.scale, 64.0F * Settings.scale);
/*  22 */   private float x; private float y; private float scale = Settings.scale;
/*     */   
/*     */   public static enum ToggleType {
/*  25 */     CONTROLLER_ENABLED;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     private ToggleType() {}
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public GiantToggleButton(ToggleType type, float x, float y, String label)
/*     */   {
/*  37 */     this.type = type;
/*  38 */     this.x = x;
/*  39 */     this.y = y;
/*  40 */     this.label = label;
/*  41 */     this.hb.move(x + 110.0F * Settings.scale, y);
/*  42 */     initialize();
/*     */   }
/*     */   
/*     */   private void initialize() {
/*  46 */     switch (this.type) {
/*     */     case CONTROLLER_ENABLED: 
/*  48 */       this.ticked = Settings.gamePref.getBoolean("Controller Enabled", true);
/*  49 */       break;
/*     */     default: 
/*  51 */       logger.info(this.type.name() + " not found (initialize())");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  57 */     this.hb.update();
/*  58 */     if (this.hb.justHovered) {
/*  59 */       CardCrawlGame.sound.play("UI_HOVER");
/*  60 */     } else if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  61 */       this.hb.clickStarted = true;
/*  62 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  63 */     } else if ((this.hb.clicked) || ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/*  64 */       CInputActionSet.select.unpress();
/*  65 */       this.hb.clicked = false;
/*  66 */       this.ticked = (!this.ticked);
/*  67 */       useEffect();
/*     */     }
/*     */     
/*  70 */     if (this.hb.hovered) {
/*  71 */       this.scale = (Settings.scale * 1.125F);
/*     */     } else {
/*  73 */       this.scale = Settings.scale;
/*     */     }
/*     */   }
/*     */   
/*     */   private void useEffect() {
/*  78 */     switch (this.type) {
/*     */     case CONTROLLER_ENABLED: 
/*  80 */       Settings.gamePref.putBoolean("Controller Enabled", this.ticked);
/*  81 */       Settings.gamePref.flush();
/*  82 */       Settings.CONTROLLER_ENABLED = this.ticked;
/*  83 */       break;
/*     */     default: 
/*  85 */       logger.info(this.type.name() + " not found (useEffect())");
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  91 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/*  92 */     sb.draw(ImageMaster.CHECKBOX, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, 0.0F, 0, 0, 64, 64, false, false);
/*  93 */     if (this.ticked) {
/*  94 */       sb.draw(ImageMaster.TICK, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*  97 */     FontHelper.renderFontLeft(sb, FontHelper.panelEndTurnFont, this.label, this.x + 40.0F * Settings.scale, this.y, Settings.CREAM_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 106 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\GiantToggleButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */