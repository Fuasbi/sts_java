/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ 
/*     */ public class ExitGameButton
/*     */ {
/*  20 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ExitGameButton");
/*  21 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  23 */   private int W = 635; private int H = 488;
/*     */   private Hitbox hb;
/*     */   private float x;
/*  26 */   private float y; private String label = TEXT[0];
/*     */   
/*     */   public ExitGameButton() {
/*  29 */     this.hb = new Hitbox(280.0F * Settings.scale, 80.0F * Settings.scale);
/*  30 */     this.x = (1490.0F * Settings.scale);
/*  31 */     this.y = (Settings.OPTION_Y + -184.0F * Settings.scale);
/*  32 */     this.hb.move(this.x + 50.0F * Settings.scale, this.y - 173.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  36 */     this.hb.update();
/*  37 */     if (this.hb.justHovered) {
/*  38 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/*  41 */     if ((this.hb.clicked) || (CInputActionSet.discardPile.isJustPressed())) {
/*  42 */       this.hb.clicked = false;
/*  43 */       AbstractDungeon.settingsScreen.popup(ConfirmPopup.ConfirmType.EXIT);
/*     */     }
/*     */     
/*  46 */     if (((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) || (CInputActionSet.discardPile.isJustPressed())) {
/*  47 */       if (CardCrawlGame.mode == com.megacrit.cardcrawl.core.CardCrawlGame.GameMode.CHAR_SELECT) {
/*  48 */         Gdx.app.exit();
/*     */       }
/*  50 */       CardCrawlGame.sound.play("UI_CLICK_1");
/*  51 */       this.hb.clickStarted = true;
/*  52 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateLabel(String newLabel) {
/*  57 */     this.label = newLabel;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  61 */     sb.setColor(Color.WHITE);
/*  62 */     sb.draw(ImageMaster.OPTION_EXIT, this.x - this.W / 2.0F, this.y - this.H / 2.0F, this.W / 2.0F, this.H / 2.0F, this.W, this.H, Settings.scale, Settings.scale, 0.0F, 0, 0, this.W, this.H, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  80 */     FontHelper.renderFontCentered(sb, FontHelper.losePowerFont, this.label, this.x + 50.0F * Settings.scale, this.y - 170.0F * Settings.scale, Settings.GOLD_COLOR, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  89 */     if (this.hb.hovered) {
/*  90 */       sb.setBlendFunction(770, 1);
/*  91 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.2F));
/*  92 */       sb.draw(ImageMaster.OPTION_EXIT, this.x - this.W / 2.0F, this.y - this.H / 2.0F, this.W / 2.0F, this.H / 2.0F, this.W, this.H, Settings.scale, Settings.scale, 0.0F, 0, 0, this.W, this.H, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 109 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 112 */     if (Settings.isControllerMode) {
/* 113 */       sb.draw(ImageMaster.CONTROLLER_RT, this.x - 32.0F - 32.0F * Settings.scale - 
/*     */       
/* 115 */         FontHelper.getSmartWidth(FontHelper.losePowerFont, this.label, 99999.0F, 0.0F) / 2.0F, this.y - 32.0F - 170.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 133 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\ExitGameButton.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */