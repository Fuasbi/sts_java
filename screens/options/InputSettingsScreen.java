/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.ScrollBar;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class InputSettingsScreen implements RemapInputElementListener, com.megacrit.cardcrawl.helpers.HitboxListener, com.megacrit.cardcrawl.screens.mainMenu.ScrollBarListener
/*     */ {
/*  30 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("InputSettingsScreen");
/*  31 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  34 */   private static final String TAB_HEADER = TEXT[0];
/*  35 */   private static final String GAME_SETTINNGS_TAB_HEADER = TEXT[1];
/*  36 */   private static final String RETURN_BUTTON_TEXT = TEXT[2];
/*     */   
/*     */   private static final boolean ALLOW_OVERSCROLL = false;
/*     */   private static final int BG_RAW_WIDTH = 1920;
/*     */   private static final int BG_RAW_HEIGHT = 1080;
/*     */   private static final float SHOW_X = 0.0F;
/*  42 */   private static final float HIDE_X = -1100.0F * Settings.scale;
/*     */   
/*  44 */   private static final float GAME_SETTINGS_BUTTON_CX = 522.0F * Settings.scale;
/*  45 */   private static final float GAME_SETTINGS_BUTTON_CY = (Settings.isSixteenByTen ? 920.0F : 860.0F) * Settings.scale;
/*  46 */   private static final float GAME_SETTINGS_BUTTON_WIDTH = 360.0F * Settings.scale;
/*     */   
/*  48 */   private static final float INPUT_TITLE_TEXT_CX = 918.0F * Settings.scale;
/*     */   
/*  50 */   private static final float ROW_X_POSITION = Settings.WIDTH / 2 - 37.0F * Settings.scale;
/*  51 */   private static final float ROW_TABLE_VERTICAL_EXTRA_PADDING = 10.0F * Settings.scale;
/*  52 */   private static final float ROW_PADDING = 0.0F * Settings.scale;
/*  53 */   private static final float ROW_Y_DIFF = RemapInputElement.ROW_HEIGHT + ROW_PADDING;
/*     */   
/*  55 */   private static final float SCROLL_CONTAINER_VISIBLE_HEIGHT = Settings.scale * 651.0F;
/*  56 */   private static final float SCROLL_CONTAINER_BOTTOM = (Settings.isSixteenByTen ? 213.0F : 150.0F) * Settings.scale;
/*  57 */   private static final float SCROLL_CONTAINER_TOP = SCROLL_CONTAINER_BOTTOM + SCROLL_CONTAINER_VISIBLE_HEIGHT;
/*     */   
/*  59 */   private static final float SCROLLBAR_X = Settings.WIDTH - 384.0F * Settings.scale;
/*  60 */   private static final float SCROLLBAR_Y = Settings.HEIGHT / 2 - 61.0F * Settings.scale;
/*  61 */   private static final float SCROLLBAR_HEIGHT = 584.0F * Settings.scale;
/*     */   
/*  63 */   private static final float RESET_BUTTON_CY = (Settings.isSixteenByTen ? 100.0F : 70.0F) * Settings.scale;
/*     */   
/*     */ 
/*  66 */   public MenuCancelButton button = new MenuCancelButton();
/*  67 */   private ScrollBar scrollBar = new ScrollBar(this, SCROLLBAR_X, SCROLLBAR_Y, SCROLLBAR_HEIGHT);
/*  68 */   private Hitbox resetButtonHb = new Hitbox(Settings.scale * 300.0F, Settings.scale * 50.0F);
/*  69 */   private ArrayList<RemapInputElement> elements = new ArrayList();
/*  70 */   private Hitbox gameSettingButton = new Hitbox(GAME_SETTINGS_BUTTON_WIDTH, Settings.scale * 50.0F);
/*  71 */   private GiantToggleButton controllerEnabledToggleButton = new GiantToggleButton(GiantToggleButton.ToggleType.CONTROLLER_ENABLED, Settings.WIDTH * 0.6F, RESET_BUTTON_CY, TEXT[48]);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  78 */   private float screenX = HIDE_X; private float targetX = HIDE_X;
/*     */   private float maxScrollAmount;
/*  80 */   private float targetScrollOffset = 0.0F;
/*  81 */   private float visibleScrollOffset = 0.0F;
/*  82 */   public boolean screenUp = false;
/*     */   
/*     */ 
/*  85 */   private Hitbox elementHb = null;
/*     */   
/*     */   public void open() {
/*  88 */     open(true);
/*     */   }
/*     */   
/*     */   public void open(boolean animated) {
/*  92 */     this.targetX = 0.0F;
/*  93 */     this.targetScrollOffset = 0.0F;
/*  94 */     this.visibleScrollOffset = 0.0F;
/*  95 */     if (animated) {
/*  96 */       this.button.show(RETURN_BUTTON_TEXT);
/*     */     } else {
/*  98 */       this.button.showInstantly(RETURN_BUTTON_TEXT);
/*     */     }
/* 100 */     this.screenUp = true;
/* 101 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.INPUT_SETTINGS;
/* 102 */     refreshData();
/* 103 */     this.resetButtonHb.move(Settings.WIDTH * 0.35F, RESET_BUTTON_CY);
/* 104 */     this.gameSettingButton.move(GAME_SETTINGS_BUTTON_CX, GAME_SETTINGS_BUTTON_CY);
/* 105 */     this.scrollBar.isBackgroundVisible = false;
/* 106 */     this.scrollBar.setCenter(SCROLLBAR_X, SCROLLBAR_Y);
/* 107 */     if (CardCrawlGame.isInARun()) {
/* 108 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.INPUT_SETTINGS;
/*     */     }
/*     */   }
/*     */   
/*     */   private void refreshData() {
/* 113 */     this.elementHb = null;
/* 114 */     this.elements.clear();
/* 115 */     this.elements.add(new RemapInputElementHeader(TEXT[3], TEXT[4], TEXT[5]));
/* 116 */     if (Settings.isControllerMode) {
/* 117 */       this.elements.add(new RemapInputElement(this, TEXT[28], null, CInputActionSet.select));
/* 118 */       this.elements.add(new RemapInputElement(this, TEXT[29], null, CInputActionSet.cancel));
/* 119 */       this.elements.add(new RemapInputElement(this, TEXT[30], null, CInputActionSet.topPanel));
/* 120 */       this.elements.add(new RemapInputElement(this, TEXT[31], null, CInputActionSet.proceed));
/* 121 */       this.elements.add(new RemapInputElement(this, TEXT[32], null, CInputActionSet.pageLeftViewDeck));
/* 122 */       this.elements.add(new RemapInputElement(this, TEXT[33], null, CInputActionSet.pageRightViewExhaust));
/* 123 */       this.elements.add(new RemapInputElement(this, TEXT[34], null, CInputActionSet.map));
/* 124 */       this.elements.add(new RemapInputElement(this, TEXT[35], null, CInputActionSet.settings));
/* 125 */       this.elements.add(new RemapInputElement(this, TEXT[36], null, CInputActionSet.drawPile));
/* 126 */       this.elements.add(new RemapInputElement(this, TEXT[37], null, CInputActionSet.discardPile));
/* 127 */       this.elements.add(new RemapInputElement(this, TEXT[38], null, CInputActionSet.up));
/* 128 */       this.elements.add(new RemapInputElement(this, TEXT[39], null, CInputActionSet.down));
/* 129 */       this.elements.add(new RemapInputElement(this, TEXT[40], null, CInputActionSet.left));
/* 130 */       this.elements.add(new RemapInputElement(this, TEXT[41], null, CInputActionSet.right));
/* 131 */       this.elements.add(new RemapInputElement(this, TEXT[42], null, CInputActionSet.altUp));
/* 132 */       this.elements.add(new RemapInputElement(this, TEXT[43], null, CInputActionSet.altDown));
/* 133 */       this.elements.add(new RemapInputElement(this, TEXT[44], null, CInputActionSet.altLeft));
/* 134 */       this.elements.add(new RemapInputElement(this, TEXT[45], null, CInputActionSet.altRight));
/*     */     } else {
/* 136 */       this.elements.add(new RemapInputElement(this, TEXT[7], InputActionSet.confirm));
/* 137 */       this.elements.add(new RemapInputElement(this, TEXT[8], InputActionSet.cancel));
/* 138 */       this.elements.add(new RemapInputElement(this, TEXT[9], InputActionSet.map));
/* 139 */       this.elements.add(new RemapInputElement(this, TEXT[10], InputActionSet.masterDeck));
/* 140 */       this.elements.add(new RemapInputElement(this, TEXT[11], InputActionSet.drawPile));
/* 141 */       this.elements.add(new RemapInputElement(this, TEXT[12], InputActionSet.discardPile));
/* 142 */       this.elements.add(new RemapInputElement(this, TEXT[13], InputActionSet.exhaustPile));
/* 143 */       this.elements.add(new RemapInputElement(this, TEXT[14], InputActionSet.endTurn));
/* 144 */       this.elements.add(new RemapInputElement(this, TEXT[15], InputActionSet.left));
/* 145 */       this.elements.add(new RemapInputElement(this, TEXT[16], InputActionSet.right));
/* 146 */       this.elements.add(new RemapInputElement(this, TEXT[17], InputActionSet.selectCard_1));
/* 147 */       this.elements.add(new RemapInputElement(this, TEXT[18], InputActionSet.selectCard_2));
/* 148 */       this.elements.add(new RemapInputElement(this, TEXT[19], InputActionSet.selectCard_3));
/* 149 */       this.elements.add(new RemapInputElement(this, TEXT[20], InputActionSet.selectCard_4));
/* 150 */       this.elements.add(new RemapInputElement(this, TEXT[21], InputActionSet.selectCard_5));
/* 151 */       this.elements.add(new RemapInputElement(this, TEXT[22], InputActionSet.selectCard_6));
/* 152 */       this.elements.add(new RemapInputElement(this, TEXT[23], InputActionSet.selectCard_7));
/* 153 */       this.elements.add(new RemapInputElement(this, TEXT[24], InputActionSet.selectCard_8));
/* 154 */       this.elements.add(new RemapInputElement(this, TEXT[25], InputActionSet.selectCard_9));
/* 155 */       this.elements.add(new RemapInputElement(this, TEXT[26], InputActionSet.selectCard_10));
/* 156 */       this.elements.add(new RemapInputElement(this, TEXT[27], InputActionSet.releaseCard));
/*     */     }
/*     */     
/* 159 */     this.maxScrollAmount = (ROW_Y_DIFF * this.elements.size() + 2.0F * ROW_TABLE_VERTICAL_EXTRA_PADDING - SCROLL_CONTAINER_VISIBLE_HEIGHT);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 164 */     updateControllerInput();
/* 165 */     this.button.update();
/* 166 */     this.controllerEnabledToggleButton.update();
/* 167 */     updateScrolling();
/*     */     
/* 169 */     if ((this.button.hb.clicked) || (InputHelper.pressedEscape) || (CInputActionSet.cancel.isJustPressed())) {
/* 170 */       InputHelper.pressedEscape = false;
/* 171 */       CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.MAIN_MENU;
/* 172 */       hide();
/*     */     }
/*     */     
/* 175 */     updateKeyPositions();
/* 176 */     for (RemapInputElement element : this.elements) {
/* 177 */       element.update();
/*     */     }
/*     */     
/* 180 */     if ((Settings.isControllerMode) && 
/* 181 */       (CInputActionSet.pageLeftViewDeck.isJustPressed())) {
/* 182 */       clicked(this.gameSettingButton);
/*     */     }
/*     */     
/*     */ 
/* 186 */     this.resetButtonHb.encapsulatedUpdate(this);
/* 187 */     this.gameSettingButton.encapsulatedUpdate(this);
/* 188 */     this.screenX = MathHelper.uiLerpSnap(this.screenX, this.targetX);
/* 189 */     if ((this.elementHb != null) && (Settings.isControllerMode)) {
/* 190 */       CInputHelper.setCursor(this.elementHb);
/*     */     }
/* 192 */     this.scrollBar.update();
/*     */   }
/*     */   
/*     */   private static enum HighlightType {
/* 196 */     INPUT,  RESET,  CONTROLLER_ON_TOGGLE;
/*     */     
/*     */     private HighlightType() {} }
/*     */   
/* 200 */   private void updateControllerInput() { if (!Settings.isControllerMode) {
/* 201 */       return;
/*     */     }
/*     */     
/* 204 */     HighlightType type = HighlightType.INPUT;
/* 205 */     boolean anyHovered = false;
/* 206 */     int index = 0;
/* 207 */     for (RemapInputElement e : this.elements) {
/* 208 */       e.hb.update();
/* 209 */       if (e.hb.hovered) {
/* 210 */         anyHovered = true;
/* 211 */         break;
/*     */       }
/* 213 */       index++;
/*     */     }
/*     */     
/* 216 */     if (this.resetButtonHb.hovered) {
/* 217 */       type = HighlightType.RESET;
/* 218 */       anyHovered = true;
/* 219 */     } else if (this.controllerEnabledToggleButton.hb.hovered) {
/* 220 */       type = HighlightType.CONTROLLER_ON_TOGGLE;
/* 221 */       anyHovered = true;
/*     */     }
/*     */     
/* 224 */     if (!anyHovered) {
/* 225 */       index = 1;
/* 226 */       CInputHelper.setCursor(((RemapInputElement)this.elements.get(1)).hb);
/*     */     } else {
/* 228 */       switch (type) {
/*     */       case INPUT: 
/* 230 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 231 */           index--;
/* 232 */           if (index < 1) {
/* 233 */             index = 1;
/*     */           }
/* 235 */           CInputHelper.setCursor(((RemapInputElement)this.elements.get(index)).hb);
/* 236 */           this.elementHb = ((RemapInputElement)this.elements.get(index)).hb;
/* 237 */         } else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 238 */           index++;
/* 239 */           if (index > this.elements.size() - 1) {
/* 240 */             CInputHelper.setCursor(this.resetButtonHb);
/* 241 */             this.elementHb = this.resetButtonHb;
/*     */           } else {
/* 243 */             CInputHelper.setCursor(((RemapInputElement)this.elements.get(index)).hb);
/* 244 */             this.elementHb = ((RemapInputElement)this.elements.get(index)).hb;
/*     */           }
/*     */         }
/*     */         break;
/*     */       case RESET: 
/* 249 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 250 */           CInputHelper.setCursor(((RemapInputElement)this.elements.get(this.elements.size() - 1)).hb);
/* 251 */           this.elementHb = ((RemapInputElement)this.elements.get(this.elements.size() - 1)).hb;
/* 252 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 253 */           CInputHelper.setCursor(this.controllerEnabledToggleButton.hb);
/* 254 */           this.elementHb = this.controllerEnabledToggleButton.hb;
/*     */         }
/*     */         break;
/*     */       case CONTROLLER_ON_TOGGLE: 
/* 258 */         if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 259 */           CInputHelper.setCursor(((RemapInputElement)this.elements.get(this.elements.size() - 1)).hb);
/* 260 */           this.elementHb = ((RemapInputElement)this.elements.get(this.elements.size() - 1)).hb;
/* 261 */         } else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 262 */           CInputHelper.setCursor(this.resetButtonHb);
/* 263 */           this.elementHb = this.resetButtonHb;
/*     */         }
/*     */         
/*     */         break;
/*     */       }
/*     */       
/*     */     }
/*     */     
/* 271 */     updateControllerScrolling();
/*     */   }
/*     */   
/*     */   private void updateControllerScrolling() {
/* 275 */     if (Gdx.input.getY() > Settings.HEIGHT * 0.65F) {
/* 276 */       this.targetScrollOffset += Settings.SCROLL_SPEED / 3.0F;
/* 277 */       if (this.targetScrollOffset > this.maxScrollAmount) {
/* 278 */         this.targetScrollOffset = this.maxScrollAmount;
/*     */       }
/* 280 */     } else if (Gdx.input.getY() < Settings.HEIGHT * 0.35F) {
/* 281 */       this.targetScrollOffset -= Settings.SCROLL_SPEED / 3.0F;
/* 282 */       if (this.targetScrollOffset < 0.0F) {
/* 283 */         this.targetScrollOffset = 0.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateScrolling()
/*     */   {
/* 290 */     float targetScrollOffset = this.targetScrollOffset;
/* 291 */     if (InputHelper.scrolledDown) {
/* 292 */       targetScrollOffset += Settings.SCROLL_SPEED;
/* 293 */     } else if (InputHelper.scrolledUp) {
/* 294 */       targetScrollOffset -= Settings.SCROLL_SPEED;
/*     */     }
/*     */     
/* 297 */     if (targetScrollOffset != this.targetScrollOffset) {
/* 298 */       this.targetScrollOffset = MathHelper.scrollSnapLerpSpeed(this.targetScrollOffset, targetScrollOffset);
/*     */       
/* 300 */       this.targetScrollOffset = com.badlogic.gdx.math.MathUtils.clamp(this.targetScrollOffset, 0.0F, this.maxScrollAmount);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 311 */     updateBarPosition();
/*     */   }
/*     */   
/*     */   public void hide() {
/* 315 */     CardCrawlGame.sound.play("DECK_CLOSE", 0.1F);
/* 316 */     this.targetX = HIDE_X;
/* 317 */     this.button.hide();
/* 318 */     this.screenUp = false;
/* 319 */     InputActionSet.save();
/* 320 */     CInputActionSet.save();
/* 321 */     CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 325 */     sb.setColor(Color.WHITE);
/* 326 */     renderFullscreenBackground(sb, ImageMaster.INPUT_SETTINGS_BG);
/*     */     
/* 328 */     for (RemapInputElement element : this.elements) {
/* 329 */       element.render(sb);
/*     */     }
/*     */     
/* 332 */     renderResetDefaultButton(sb);
/* 333 */     renderFullscreenBackground(sb, ImageMaster.INPUT_SETTINGS_EDGES);
/*     */     
/* 335 */     this.scrollBar.render(sb);
/* 336 */     renderHeader(sb);
/*     */     
/* 338 */     if (Settings.isControllerMode) {
/* 339 */       sb.draw(CInputActionSet.pageLeftViewDeck
/* 340 */         .getKeyImg(), this.gameSettingButton.cX - 32.0F - 
/* 341 */         FontHelper.getSmartWidth(FontHelper.deckBannerFont, GAME_SETTINNGS_TAB_HEADER, 99999.0F, 2.0F) / 2.0F - 42.0F * Settings.scale, Settings.OPTION_Y - 32.0F + 354.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 362 */     this.controllerEnabledToggleButton.render(sb);
/* 363 */     this.button.render(sb);
/*     */   }
/*     */   
/*     */   private void updateKeyPositions() {
/* 367 */     float x = ROW_X_POSITION;
/* 368 */     float y = SCROLL_CONTAINER_TOP - ROW_TABLE_VERTICAL_EXTRA_PADDING - ROW_Y_DIFF / 2.0F;
/* 369 */     if (Settings.isSixteenByTen) {
/* 370 */       y -= Settings.scale * 4.0F;
/*     */     }
/*     */     
/* 373 */     this.visibleScrollOffset = MathHelper.scrollSnapLerpSpeed(this.visibleScrollOffset, this.targetScrollOffset);
/* 374 */     y += this.visibleScrollOffset;
/*     */     
/* 376 */     float maxVisibleY = SCROLL_CONTAINER_TOP - Settings.scale * 10.0F;
/* 377 */     float minVisibleY = SCROLL_CONTAINER_BOTTOM - (RemapInputElement.ROW_HEIGHT - Settings.scale * 10.0F);
/*     */     
/* 379 */     for (RemapInputElement element : this.elements) {
/* 380 */       element.move(x, y);
/* 381 */       boolean isInView = (minVisibleY < element.hb.y) && (element.hb.y < maxVisibleY);
/* 382 */       element.isHidden = (!isInView);
/* 383 */       y -= ROW_Y_DIFF;
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderHeader(SpriteBatch sb) {
/* 388 */     Color textColor = this.gameSettingButton.hovered ? Settings.GOLD_COLOR : Color.LIGHT_GRAY;
/* 389 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, GAME_SETTINNGS_TAB_HEADER, this.gameSettingButton.cX, this.gameSettingButton.cY + 4.0F * Settings.scale, textColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 396 */     this.gameSettingButton.render(sb);
/*     */     
/* 398 */     FontHelper.renderFontCentered(sb, FontHelper.deckBannerFont, TAB_HEADER, INPUT_TITLE_TEXT_CX, this.gameSettingButton.cY + 4.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderFullscreenBackground(SpriteBatch sb, Texture image)
/*     */   {
/* 408 */     sb.draw(image, Settings.WIDTH / 2.0F - 960.0F, Settings.OPTION_Y - 540.0F, 960.0F, 540.0F, 1920.0F, 1080.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1920, 1080, false, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderResetDefaultButton(SpriteBatch sb)
/*     */   {
/* 430 */     Color color = this.resetButtonHb.hovered ? Settings.GOLD_COLOR : Settings.CREAM_COLOR;
/* 431 */     FontHelper.renderFontCentered(sb, FontHelper.panelEndTurnFont, TEXT[6], this.resetButtonHb.cX, this.resetButtonHb.cY, color);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 438 */     this.resetButtonHb.render(sb);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void didStartRemapping(RemapInputElement element)
/*     */   {
/* 449 */     for (RemapInputElement e : this.elements) {
/* 450 */       e.hasInputFocus = (e == element);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean willRemap(RemapInputElement element, int oldKey, int newKey)
/*     */   {
/* 467 */     if (oldKey == newKey)
/*     */     {
/* 469 */       return true;
/*     */     }
/* 471 */     for (RemapInputElement e : this.elements)
/*     */     {
/* 473 */       if ((e.action != null) && (e.action.getKey() == newKey)) {
/* 474 */         e.action.remap(oldKey);
/*     */       }
/*     */     }
/* 477 */     return true;
/*     */   }
/*     */   
/*     */   public boolean willRemapController(RemapInputElement element, int oldCode, int newCode)
/*     */   {
/* 482 */     if (oldCode == newCode) {
/* 483 */       return true;
/*     */     }
/* 485 */     for (RemapInputElement e : this.elements) {
/* 486 */       if ((e.cAction != null) && (e.cAction.getKey() == newCode)) {
/* 487 */         e.cAction.remap(oldCode);
/*     */       }
/*     */     }
/* 490 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public void hoverStarted(Hitbox hitbox)
/*     */   {
/* 496 */     CardCrawlGame.sound.play("UI_HOVER");
/*     */   }
/*     */   
/*     */   public void startClicking(Hitbox hitbox)
/*     */   {
/* 501 */     CardCrawlGame.sound.play("UI_CLICK_1");
/*     */   }
/*     */   
/*     */   public void clicked(Hitbox hb)
/*     */   {
/* 506 */     if (hb == this.resetButtonHb) {
/* 507 */       CardCrawlGame.sound.play("END_TURN");
/* 508 */       InputActionSet.resetToDefaults();
/* 509 */       refreshData();
/* 510 */       updateKeyPositions();
/* 511 */     } else if (hb == this.gameSettingButton) {
/* 512 */       if (CardCrawlGame.isInARun()) {
/* 513 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.settingsScreen.open(false);
/*     */       }
/*     */       else {
/* 516 */         CardCrawlGame.sound.play("END_TURN");
/* 517 */         CardCrawlGame.mainMenuScreen.isSettingsUp = true;
/* 518 */         InputHelper.pressedEscape = false;
/* 519 */         CardCrawlGame.mainMenuScreen.statsScreen.hide();
/* 520 */         CardCrawlGame.mainMenuScreen.cancelButton.hide();
/* 521 */         CardCrawlGame.cancelButton.showInstantly(MainMenuScreen.TEXT[2]);
/* 522 */         CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.SETTINGS;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void scrolledUsingBar(float newPercent)
/*     */   {
/* 531 */     this.targetScrollOffset = MathHelper.valueFromPercentBetween(0.0F, this.maxScrollAmount, newPercent);
/* 532 */     this.scrollBar.parentScrolledToPercent(newPercent);
/*     */   }
/*     */   
/*     */   private void updateBarPosition() {
/* 536 */     float percent = MathHelper.percentFromValueBetween(0.0F, this.maxScrollAmount, this.targetScrollOffset);
/* 537 */     this.scrollBar.parentScrolledToPercent(percent);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\InputSettingsScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */