/*     */ package com.megacrit.cardcrawl.screens.options;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.files.FileHandle;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SettingsScreen
/*     */ {
/*  16 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SettingsScreen.class.getName());
/*  17 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("SettingsScreen");
/*  18 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  20 */   public OptionsPanel panel = new OptionsPanel();
/*  21 */   public ConfirmPopup exitPopup = new ConfirmPopup(TEXT[0], TEXT[1], ConfirmPopup.ConfirmType.EXIT);
/*     */   
/*  23 */   public ConfirmPopup abandonPopup = new ConfirmPopup(TEXT[0], TEXT[2], ConfirmPopup.ConfirmType.ABANDON);
/*     */   
/*  25 */   private static final String NOT_SAVED_MSG = TEXT[3];
/*     */   
/*     */   public void update() {
/*  28 */     if ((!this.exitPopup.shown) && (!this.abandonPopup.shown)) {
/*  29 */       this.panel.update();
/*     */     }
/*  31 */     this.exitPopup.update();
/*  32 */     this.abandonPopup.update();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void open()
/*     */   {
/*  39 */     open(true);
/*     */   }
/*     */   
/*     */   public void open(boolean animated) {
/*  43 */     AbstractDungeon.player.releaseCard();
/*  44 */     this.panel.refresh();
/*  45 */     if (animated) {
/*  46 */       AbstractDungeon.overlayMenu.cancelButton.show(TEXT[4]);
/*     */     } else {
/*  48 */       AbstractDungeon.overlayMenu.cancelButton.showInstantly(TEXT[4]);
/*     */     }
/*  50 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("UI_CLICK_1");
/*  51 */     AbstractDungeon.isScreenUp = true;
/*  52 */     AbstractDungeon.overlayMenu.showBlackScreen();
/*  53 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/*  54 */     AbstractDungeon.overlayMenu.hideCombatPanels();
/*  55 */     AbstractDungeon.screen = com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen.SETTINGS;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  64 */     this.panel.render(sb);
/*  65 */     this.exitPopup.render(sb);
/*  66 */     this.abandonPopup.render(sb);
/*     */   }
/*     */   
/*     */   public void popup(ConfirmPopup.ConfirmType type) {
/*  70 */     if (AbstractDungeon.overlayMenu != null) {
/*  71 */       AbstractDungeon.overlayMenu.cancelButton.hide();
/*     */     }
/*     */     
/*  74 */     switch (type) {
/*     */     case ABANDON: 
/*  76 */       this.abandonPopup.show();
/*  77 */       break;
/*     */     case EXIT: 
/*  79 */       this.exitPopup.desc = TEXT[1];
/*  80 */       switch (AbstractDungeon.player.chosenClass) {
/*     */       case IRONCLAD: 
/*  82 */         if (!Gdx.files.local(SaveAndContinue.IRONCLAD_FILE).exists()) {
/*  83 */           this.exitPopup.desc = NOT_SAVED_MSG;
/*     */         }
/*     */         break;
/*     */       case THE_SILENT: 
/*  87 */         if (!Gdx.files.local(SaveAndContinue.THE_SILENT_FILE).exists()) {
/*  88 */           this.exitPopup.desc = NOT_SAVED_MSG;
/*     */         }
/*     */         break;
/*     */       case DEFECT: 
/*  92 */         if (!Gdx.files.local(SaveAndContinue.DEFECT_FILE).exists()) {
/*  93 */           this.exitPopup.desc = NOT_SAVED_MSG;
/*     */         }
/*     */         break;
/*     */       default: 
/*  97 */         System.out.print(AbstractDungeon.player.chosenClass
/*  98 */           .toString() + " NOT FOUND IN CONFIRMPOPUP()");
/*     */       }
/*     */       
/* 101 */       this.exitPopup.show();
/* 102 */       break;
/*     */     default: 
/* 104 */       logger.info("Unspecified case: " + type.name());
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\SettingsScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */