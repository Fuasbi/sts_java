package com.megacrit.cardcrawl.screens.options;

public abstract interface DropdownMenuListener
{
  public abstract void changedSelectionTo(DropdownMenu paramDropdownMenu, int paramInt, String paramString);
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\screens\options\DropdownMenuListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */