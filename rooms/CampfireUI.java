/*     */ package com.megacrit.cardcrawl.rooms;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.ui.campfire.AbstractCampfireOption;
/*     */ import com.megacrit.cardcrawl.ui.campfire.DigOption;
/*     */ import com.megacrit.cardcrawl.ui.campfire.RestOption;
/*     */ import com.megacrit.cardcrawl.ui.campfire.SmithOption;
/*     */ import com.megacrit.cardcrawl.ui.campfire.TokeOption;
/*     */ import com.megacrit.cardcrawl.ui.panels.PotionPopUp;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import com.megacrit.cardcrawl.vfx.BobEffect;
/*     */ import com.megacrit.cardcrawl.vfx.campfire.CampfireBubbleEffect;
/*     */ import com.megacrit.cardcrawl.vfx.campfire.CampfireBurningEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class CampfireUI
/*     */ {
/*  35 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("CampfireUI");
/*  36 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*  39 */   public static boolean hidden = false;
/*  40 */   public boolean somethingSelected = false;
/*  41 */   private float hideStuffTimer = 0.5F; private float charAnimTimer = 2.0F;
/*     */   private static final float HIDE_TIME = 0.5F;
/*  43 */   private ArrayList<AbstractCampfireOption> buttons = new ArrayList();
/*  44 */   private ArrayList<CampfireBubbleEffect> bubbles = new ArrayList();
/*  45 */   private float fireTimer = 0.0F;
/*     */   private static final float FIRE_INTERVAL = 0.05F;
/*  47 */   private ArrayList<CampfireBurningEffect> fires = new ArrayList();
/*     */   private int bubbleAmt;
/*     */   private String bubbleMsg;
/*  50 */   private BobEffect effect = new BobEffect(2.0F);
/*     */   
/*     */   public CampfireUI() {
/*  53 */     hidden = false;
/*  54 */     initializeButtons();
/*  55 */     if (this.buttons.size() > 2) {
/*  56 */       this.bubbleAmt = 60;
/*     */     } else {
/*  58 */       this.bubbleAmt = 40;
/*     */     }
/*  60 */     this.bubbleMsg = getCampMessage();
/*     */   }
/*     */   
/*     */   private void initializeButtons()
/*     */   {
/*  65 */     if (!AbstractDungeon.player.hasRelic("Coffee Dripper")) {
/*  66 */       this.buttons.add(new RestOption());
/*     */     }
/*     */     
/*  69 */     if ((!((Boolean)com.megacrit.cardcrawl.daily.DailyMods.negativeMods.get("Midas")).booleanValue()) && (!AbstractDungeon.player.hasRelic("Fusion Hammer"))) {
/*  70 */       this.buttons.add(new SmithOption(AbstractDungeon.player.masterDeck.getUpgradableCards().size() > 0));
/*     */     }
/*     */     
/*  73 */     if (AbstractDungeon.player.hasRelic("Peace Pipe")) {
/*  74 */       this.buttons.add(new TokeOption(!AbstractDungeon.player.masterDeck.getPurgeableCards().isEmpty()));
/*     */     }
/*     */     
/*  77 */     if (AbstractDungeon.player.hasRelic("Shovel")) {
/*  78 */       this.buttons.add(new DigOption());
/*     */     }
/*     */     
/*  81 */     if ((AbstractDungeon.player.hasRelic("Girya")) && 
/*  82 */       (AbstractDungeon.player.getRelic("Girya").counter < 3)) {
/*  83 */       this.buttons.add(new com.megacrit.cardcrawl.ui.campfire.LiftOption());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  88 */     switch (this.buttons.size()) {
/*     */     case 0: 
/*  90 */       AbstractRoom.waitTimer = 0.0F;
/*  91 */       AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/*  92 */       break;
/*     */     case 1: 
/*  94 */       ((AbstractCampfireOption)this.buttons.get(0)).setPosition(950.0F * Settings.scale, 720.0F * Settings.scale);
/*  95 */       break;
/*     */     case 2: 
/*  97 */       ((AbstractCampfireOption)this.buttons.get(0)).setPosition(800.0F * Settings.scale, 720.0F * Settings.scale);
/*  98 */       ((AbstractCampfireOption)this.buttons.get(1)).setPosition(1110.0F * Settings.scale, 720.0F * Settings.scale);
/*  99 */       break;
/*     */     case 3: 
/* 101 */       ((AbstractCampfireOption)this.buttons.get(0)).setPosition(800.0F * Settings.scale, 720.0F * Settings.scale);
/* 102 */       ((AbstractCampfireOption)this.buttons.get(1)).setPosition(1110.0F * Settings.scale, 720.0F * Settings.scale);
/* 103 */       ((AbstractCampfireOption)this.buttons.get(2)).setPosition(950.0F * Settings.scale, 450.0F * Settings.scale);
/* 104 */       break;
/*     */     case 4: 
/* 106 */       ((AbstractCampfireOption)this.buttons.get(0)).setPosition(800.0F * Settings.scale, 720.0F * Settings.scale);
/* 107 */       ((AbstractCampfireOption)this.buttons.get(1)).setPosition(1110.0F * Settings.scale, 720.0F * Settings.scale);
/* 108 */       ((AbstractCampfireOption)this.buttons.get(2)).setPosition(800.0F * Settings.scale, 450.0F * Settings.scale);
/* 109 */       ((AbstractCampfireOption)this.buttons.get(3)).setPosition(1110.0F * Settings.scale, 450.0F * Settings.scale);
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 114 */     boolean cannotProceed = true;
/* 115 */     for (AbstractCampfireOption opt : this.buttons) {
/* 116 */       if (opt.usable) {
/* 117 */         cannotProceed = false;
/* 118 */         break;
/*     */       }
/*     */     }
/*     */     
/* 122 */     if (cannotProceed) {
/* 123 */       AbstractRoom.waitTimer = 0.0F;
/* 124 */       AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.COMPLETE;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/* 129 */     updateCharacterPosition();
/* 130 */     updateControllerInput();
/*     */     
/* 132 */     this.effect.update();
/* 133 */     if (!hidden) {
/* 134 */       updateBubbles();
/* 135 */       updateFire();
/* 136 */       for (AbstractCampfireOption o : this.buttons) {
/* 137 */         o.update();
/*     */       }
/*     */     }
/*     */     
/* 141 */     if (this.somethingSelected) {
/* 142 */       this.hideStuffTimer -= Gdx.graphics.getDeltaTime();
/* 143 */       if (this.hideStuffTimer < 0.0F) {
/* 144 */         hidden = true;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/* 150 */     if ((!Settings.isControllerMode) || (AbstractDungeon.player.viewingRelics) || (AbstractDungeon.topPanel.selectPotionMode) || (!AbstractDungeon.topPanel.potionUi.isHidden) || (this.somethingSelected) || 
/*     */     
/* 152 */       (this.buttons.isEmpty())) {
/* 153 */       return;
/*     */     }
/*     */     
/* 156 */     boolean anyHovered = false;
/* 157 */     int index = 0;
/* 158 */     for (AbstractCampfireOption o : this.buttons) {
/* 159 */       if (o.hb.hovered) {
/* 160 */         anyHovered = true;
/* 161 */         break;
/*     */       }
/* 163 */       index++;
/*     */     }
/*     */     
/* 166 */     if (!anyHovered) {
/* 167 */       Gdx.input.setCursorPosition((int)((AbstractCampfireOption)this.buttons.get(0)).hb.cX, Settings.HEIGHT - (int)((AbstractCampfireOption)this.buttons.get(0)).hb.cY);
/*     */     }
/* 169 */     else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 170 */       index--;
/* 171 */       if (index < 0) {
/* 172 */         if (this.buttons.size() == 2) {
/* 173 */           index = 1;
/*     */         } else {
/* 175 */           index = 0;
/*     */         }
/* 177 */       } else if (index == 1) {
/* 178 */         if (this.buttons.size() == 4) {
/* 179 */           index = 3;
/*     */         } else {
/* 181 */           index = 2;
/*     */         }
/*     */       }
/* 184 */       Gdx.input.setCursorPosition(
/* 185 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cX, Settings.HEIGHT - 
/* 186 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cY);
/* 187 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 188 */       index++;
/* 189 */       if (index > this.buttons.size() - 1) {
/* 190 */         if (this.buttons.size() == 2) {
/* 191 */           index = 0;
/* 192 */         } else if (index == 3) {
/* 193 */           index = 2;
/*     */         } else {
/* 195 */           index = 0;
/*     */         }
/*     */       }
/* 198 */       Gdx.input.setCursorPosition(
/* 199 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cX, Settings.HEIGHT - 
/* 200 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cY);
/* 201 */     } else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 202 */       (this.buttons.size() > 2)) {
/* 203 */       if (this.buttons.size() == 3) {
/* 204 */         if (index == 0) {
/* 205 */           index = 2;
/* 206 */         } else if (index == 2) {
/* 207 */           index = 0;
/*     */         }
/*     */       }
/* 210 */       else if (index == 0) {
/* 211 */         index = 2;
/* 212 */       } else if (index == 2) {
/* 213 */         index = 0;
/* 214 */       } else if (index == 3) {
/* 215 */         index = 1;
/*     */       } else {
/* 217 */         index = 3;
/*     */       }
/*     */       
/* 220 */       Gdx.input.setCursorPosition(
/* 221 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cX, Settings.HEIGHT - 
/* 222 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cY);
/* 223 */     } else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/* 224 */       (this.buttons.size() > 2)) {
/* 225 */       if (this.buttons.size() == 4) {
/* 226 */         if (index > 2) {
/* 227 */           index -= 2;
/*     */         } else {
/* 229 */           index += 2;
/*     */         }
/*     */       }
/* 232 */       else if ((index == 0) || (index == 1)) {
/* 233 */         index = 2;
/*     */       } else {
/* 235 */         index = 0;
/*     */       }
/*     */       
/* 238 */       Gdx.input.setCursorPosition(
/* 239 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cX, Settings.HEIGHT - 
/* 240 */         (int)((AbstractCampfireOption)this.buttons.get(index)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateCharacterPosition()
/*     */   {
/* 246 */     this.charAnimTimer -= Gdx.graphics.getDeltaTime();
/* 247 */     if (this.charAnimTimer < 0.0F) {
/* 248 */       this.charAnimTimer = 0.0F;
/*     */     }
/* 250 */     AbstractDungeon.player.animX = com.badlogic.gdx.math.Interpolation.exp10In.apply(0.0F, -300.0F * Settings.scale, this.charAnimTimer / 2.0F);
/*     */   }
/*     */   
/*     */   private void updateBubbles() {
/* 254 */     if (this.bubbles.size() < this.bubbleAmt) {
/* 255 */       int s = this.bubbleAmt - this.bubbles.size();
/* 256 */       for (int i = 0; i < s; i++) {
/* 257 */         this.bubbles.add(new CampfireBubbleEffect(this.bubbleAmt == 60));
/*     */       }
/*     */     }
/*     */     
/* 261 */     for (Iterator<CampfireBubbleEffect> i = this.bubbles.iterator(); i.hasNext();) {
/* 262 */       CampfireBubbleEffect bubble = (CampfireBubbleEffect)i.next();
/* 263 */       bubble.update();
/* 264 */       if (bubble.isDone) {
/* 265 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFire() {
/* 271 */     this.fireTimer -= Gdx.graphics.getDeltaTime();
/* 272 */     if (this.fireTimer < 0.0F) {
/* 273 */       this.fireTimer = 0.05F;
/* 274 */       this.fires.add(new CampfireBurningEffect());
/* 275 */       this.fires.add(new CampfireBurningEffect());
/*     */     }
/*     */     
/* 278 */     for (Iterator<CampfireBurningEffect> i = this.fires.iterator(); i.hasNext();) {
/* 279 */       CampfireBurningEffect fires = (CampfireBurningEffect)i.next();
/* 280 */       fires.update();
/* 281 */       if (fires.isDone) {
/* 282 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void reopen()
/*     */   {
/* 291 */     hidden = false;
/* 292 */     this.hideStuffTimer = 0.5F;
/* 293 */     this.somethingSelected = false;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 297 */     if (!hidden) {
/* 298 */       renderFire(sb);
/* 299 */       AbstractDungeon.player.render(sb);
/*     */       
/* 301 */       for (CampfireBubbleEffect e : this.bubbles) {
/* 302 */         e.render(sb, 950.0F * Settings.scale, 600.0F * Settings.scale + this.effect.y / 4.0F);
/*     */       }
/*     */       
/* 305 */       FontHelper.renderFontCentered(sb, FontHelper.bannerFont, this.bubbleMsg, 950.0F * Settings.scale, 850.0F * Settings.scale + this.effect.y / 3.0F, Settings.CREAM_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 313 */       for (AbstractCampfireOption o : this.buttons) {
/* 314 */         o.render(sb);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private String getCampMessage() {
/* 320 */     ArrayList<String> msgs = new ArrayList();
/* 321 */     msgs.add(TEXT[0]);
/* 322 */     msgs.add(TEXT[1]);
/* 323 */     msgs.add(TEXT[2]);
/* 324 */     msgs.add(TEXT[3]);
/* 325 */     if (this.buttons.size() > 2) {
/* 326 */       msgs.add(TEXT[4]);
/*     */     }
/* 328 */     if (AbstractDungeon.player.currentHealth < AbstractDungeon.player.maxHealth / 2) {
/* 329 */       msgs.add(TEXT[5]);
/* 330 */       msgs.add(TEXT[6]);
/*     */     }
/* 332 */     return (String)msgs.get(MathUtils.random(msgs.size() - 1));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderFire(SpriteBatch sb)
/*     */   {
/* 341 */     for (CampfireBurningEffect e : this.fires) {
/* 342 */       e.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\CampfireUI.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */