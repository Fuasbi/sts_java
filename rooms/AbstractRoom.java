/*     */ package com.megacrit.cardcrawl.rooms;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.DrawCardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.EndTurnAction;
/*     */ import com.megacrit.cardcrawl.actions.common.MonsterStartTurnAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.SoulGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.dungeons.Exordium;
/*     */ import com.megacrit.cardcrawl.dungeons.TheBeyond;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*     */ import com.megacrit.cardcrawl.events.GenericEventDialog;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.DevInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem.RewardType;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*     */ import com.megacrit.cardcrawl.screens.options.SettingsScreen;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.LargeDialogOptionButton;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.ui.panels.PotionPopUp;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.GameSavedEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public abstract class AbstractRoom
/*     */ {
/*  68 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractRoom");
/*  69 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  71 */   private static final Logger logger = LogManager.getLogger(AbstractRoom.class.getName());
/*  72 */   public ArrayList<AbstractPotion> potions = new ArrayList();
/*  73 */   public ArrayList<AbstractRelic> relics = new ArrayList();
/*  74 */   public ArrayList<RewardItem> rewards = new ArrayList();
/*  75 */   public SoulGroup souls = new SoulGroup();
/*     */   public RoomPhase phase;
/*  77 */   public AbstractEvent event = null;
/*     */   public MonsterGroup monsters;
/*  79 */   private float endBattleTimer = 0.0F;
/*  80 */   public float rewardPopOutTimer = 1.0F;
/*     */   private static final float END_TURN_WAIT_DURATION = 1.2F;
/*     */   protected String mapSymbol;
/*     */   protected Texture mapImg;
/*  84 */   protected Texture mapImgOutline; public boolean isBattleOver = false;
/*  85 */   public boolean cannotLose = false;
/*  86 */   public boolean eliteTrigger = false;
/*  87 */   public static int blizzardPotionMod = 0;
/*     */   private static final int BLIZZARD_POTION_MOD_AMT = 10;
/*  89 */   public boolean mugged = false; public boolean smoked = false;
/*  90 */   public boolean combatEvent = false;
/*  91 */   public boolean rewardAllowed = true;
/*     */   
/*  93 */   public static float waitTimer = 0.0F;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final Texture getMapImg()
/*     */   {
/* 101 */     return this.mapImg;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final Texture getMapImgOutline()
/*     */   {
/* 110 */     return this.mapImgOutline;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final void setMapImg(Texture img, Texture imgOutline)
/*     */   {
/* 119 */     this.mapImg = img;
/* 120 */     this.mapImgOutline = imgOutline;
/*     */   }
/*     */   
/*     */   public abstract void onPlayerEntry();
/*     */   
/*     */   public static enum RoomPhase
/*     */   {
/* 127 */     COMBAT,  EVENT,  COMPLETE,  INCOMPLETE;
/*     */     
/*     */     private RoomPhase() {}
/*     */   }
/*     */   
/*     */   public static enum RoomType
/*     */   {
/* 134 */     SHOP,  MONSTER,  SHRINE,  TREASURE,  EVENT,  BOSS;
/*     */     
/*     */ 
/*     */     private RoomType() {}
/*     */   }
/*     */   
/*     */ 
/*     */   public void playBGM(String key)
/*     */   {
/* 143 */     CardCrawlGame.music.playTempBGM(key);
/*     */   }
/*     */   
/*     */   public void playBgmInstantly(String key) {
/* 147 */     CardCrawlGame.music.playTempBgmInstantly(key);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public final String getMapSymbol()
/*     */   {
/* 154 */     return this.mapSymbol;
/*     */   }
/*     */   
/*     */   public final void setMapSymbol(String newSymbol) {
/* 158 */     this.mapSymbol = newSymbol;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public abstract com.megacrit.cardcrawl.cards.AbstractCard.CardRarity getCardRarity(int paramInt);
/*     */   
/*     */ 
/*     */ 
/*     */   public void updateObjects()
/*     */   {
/* 169 */     this.souls.update();
/*     */     
/*     */ 
/* 172 */     for (Iterator<AbstractPotion> i = this.potions.iterator(); i.hasNext();) {
/* 173 */       AbstractPotion tmpPotion = (AbstractPotion)i.next();
/* 174 */       tmpPotion.update();
/* 175 */       if (tmpPotion.isObtained) {
/* 176 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 182 */     for (Iterator<AbstractRelic> i = this.relics.iterator(); i.hasNext();) {
/* 183 */       AbstractRelic relic = (AbstractRelic)i.next();
/* 184 */       relic.update();
/* 185 */       if (relic.isDone) {
/* 186 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 195 */     if ((!AbstractDungeon.isScreenUp) && (InputHelper.pressedEscape) && (AbstractDungeon.overlayMenu.cancelButton.current_x == CancelButton.HIDE_X))
/*     */     {
/* 197 */       AbstractDungeon.settingsScreen.open();
/*     */     }
/*     */     
/* 200 */     if (Settings.isDebug) {
/* 201 */       if (InputHelper.justClickedRight) {
/* 202 */         AbstractDungeon.player.obtainPotion(AbstractDungeon.returnRandomPotion());
/*     */         
/*     */ 
/* 205 */         AbstractDungeon.scene.randomizeScene();
/*     */       }
/*     */       
/* 208 */       if (Gdx.input.isKeyJustPressed(49)) {
/* 209 */         AbstractDungeon.player.increaseMaxOrbSlots(1, true);
/*     */       }
/*     */       
/* 212 */       if (DevInputActionSet.gainGold.isJustPressed()) {
/* 213 */         AbstractDungeon.player.gainGold(100);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 218 */     switch (this.phase) {
/*     */     case EVENT: 
/* 220 */       this.event.updateDialog();
/* 221 */       break;
/*     */     case COMBAT: 
/* 223 */       this.monsters.update();
/* 224 */       if (waitTimer > 0.0F) {
/* 225 */         if ((AbstractDungeon.actionManager.currentAction != null) || 
/* 226 */           (!AbstractDungeon.actionManager.isEmpty())) {
/* 227 */           AbstractDungeon.actionManager.update();
/*     */         } else {
/* 229 */           waitTimer -= Gdx.graphics.getDeltaTime();
/*     */         }
/* 231 */         if (waitTimer <= 0.0F)
/*     */         {
/* 233 */           AbstractDungeon.actionManager.turnHasEnded = true;
/* 234 */           if (!AbstractDungeon.isScreenUp) {
/* 235 */             AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.combat.BattleStartEffect(false));
/*     */           }
/* 237 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAndEnableControlsAction(AbstractDungeon.player.energy.energyMaster));
/*     */           
/* 239 */           AbstractDungeon.player.applyStartOfCombatPreDrawLogic();
/* 240 */           AbstractDungeon.actionManager.addToBottom(new DrawCardAction(AbstractDungeon.player, AbstractDungeon.player.gameHandSize));
/*     */           
/* 242 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.EnableEndTurnButtonAction());
/* 243 */           AbstractDungeon.overlayMenu.showCombatPanels();
/* 244 */           AbstractDungeon.player.applyStartOfCombatLogic();
/* 245 */           AbstractDungeon.player.applyStartOfTurnRelics();
/* 246 */           AbstractDungeon.player.applyStartOfTurnPostDrawRelics();
/* 247 */           AbstractDungeon.player.applyStartOfTurnCards();
/* 248 */           AbstractDungeon.player.applyStartOfTurnPowers();
/* 249 */           AbstractDungeon.player.applyStartOfTurnOrbs();
/* 250 */           AbstractDungeon.actionManager.useNextCombatActions();
/*     */         }
/*     */       } else {
/* 253 */         if ((Settings.isDebug) && (DevInputActionSet.drawCard.isJustPressed())) {
/* 254 */           AbstractDungeon.actionManager.addToTop(new DrawCardAction(AbstractDungeon.player, 1));
/*     */         }
/*     */         
/* 257 */         if (!AbstractDungeon.isScreenUp) {
/* 258 */           AbstractDungeon.actionManager.update();
/*     */           
/* 260 */           if ((!this.monsters.areMonstersBasicallyDead()) && (AbstractDungeon.player.currentHealth > 0)) {
/* 261 */             AbstractDungeon.player.updateInput();
/*     */           }
/*     */         }
/*     */         
/* 265 */         if (!AbstractDungeon.screen.equals(AbstractDungeon.CurrentScreen.HAND_SELECT)) {
/* 266 */           AbstractDungeon.player.combatUpdate();
/*     */         }
/*     */         
/* 269 */         if (AbstractDungeon.player.isEndingTurn) {
/* 270 */           endTurn();
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 275 */       if ((this.isBattleOver) && (AbstractDungeon.actionManager.actions.isEmpty())) {
/* 276 */         this.endBattleTimer -= Gdx.graphics.getDeltaTime();
/* 277 */         if (this.endBattleTimer < 0.0F) {
/* 278 */           this.phase = RoomPhase.COMPLETE;
/*     */           
/* 280 */           if ((!(AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss)) || (!(CardCrawlGame.dungeon instanceof TheBeyond)) || (Settings.isEndless))
/*     */           {
/*     */ 
/*     */ 
/* 284 */             CardCrawlGame.sound.play("VICTORY");
/*     */           }
/*     */           
/* 287 */           this.endBattleTimer = 0.0F;
/*     */           
/* 289 */           if (((this instanceof MonsterRoomBoss)) && (!AbstractDungeon.loading_post_combat)) {
/* 290 */             if (!CardCrawlGame.loadingSave) {
/* 291 */               if (Settings.isDailyRun) {
/* 292 */                 addGoldToRewards(100);
/*     */               } else {
/* 294 */                 int tmp = 100 + AbstractDungeon.miscRng.random(-5, 5);
/*     */                 
/*     */ 
/*     */ 
/* 298 */                 if (AbstractDungeon.ascensionLevel >= 13) {
/* 299 */                   addGoldToRewards(com.badlogic.gdx.math.MathUtils.round(tmp * 0.75F));
/*     */                 } else {
/* 301 */                   addGoldToRewards(tmp);
/*     */                 }
/*     */               }
/*     */             }
/*     */             
/*     */ 
/* 307 */             if (((Boolean)DailyMods.negativeMods.get("Cursed Run")).booleanValue()) {
/* 308 */               AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*     */               
/* 310 */                 AbstractDungeon.returnRandomCurse(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */             }
/*     */             
/*     */ 
/*     */           }
/* 315 */           else if (((this instanceof MonsterRoomElite)) && (!AbstractDungeon.loading_post_combat))
/*     */           {
/* 317 */             if ((CardCrawlGame.dungeon instanceof Exordium)) {
/* 318 */               CardCrawlGame.elites1Slain += 1;
/* 319 */               logger.info("ELITES SLAIN " + CardCrawlGame.elites1Slain);
/* 320 */             } else if ((CardCrawlGame.dungeon instanceof com.megacrit.cardcrawl.dungeons.TheCity)) {
/* 321 */               CardCrawlGame.elites2Slain += 1;
/* 322 */               logger.info("ELITES SLAIN " + CardCrawlGame.elites2Slain);
/*     */             } else {
/* 324 */               CardCrawlGame.elites3Slain += 1;
/* 325 */               logger.info("ELITES SLAIN " + CardCrawlGame.elites3Slain);
/*     */             }
/*     */             
/* 328 */             if (!CardCrawlGame.loadingSave) {
/* 329 */               if (Settings.isDailyRun) {
/* 330 */                 addGoldToRewards(30);
/*     */               } else {
/* 332 */                 addGoldToRewards(AbstractDungeon.treasureRng.random(25, 35));
/*     */               }
/*     */             }
/* 335 */           } else if (((this instanceof MonsterRoom)) && 
/* 336 */             (!AbstractDungeon.getMonsters().haveMonstersEscaped())) {
/* 337 */             CardCrawlGame.monstersSlain += 1;
/* 338 */             logger.info("MONSTERS SLAIN " + CardCrawlGame.monstersSlain);
/*     */             
/* 340 */             if (Settings.isDailyRun) {
/* 341 */               addGoldToRewards(15);
/*     */             } else {
/* 343 */               addGoldToRewards(AbstractDungeon.treasureRng.random(10, 20));
/*     */             }
/*     */           }
/*     */           
/*     */ 
/* 348 */           if ((!(AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss)) || (!(CardCrawlGame.dungeon instanceof TheBeyond)) || (Settings.isEndless))
/*     */           {
/*     */ 
/*     */ 
/* 352 */             if (!AbstractDungeon.loading_post_combat) {
/* 353 */               dropReward();
/* 354 */               addPotionToRewards();
/*     */             }
/* 356 */             int card_seed_before_roll = AbstractDungeon.cardRng.counter;
/*     */             
/* 358 */             if (this.rewardAllowed) {
/* 359 */               if (this.mugged) {
/* 360 */                 AbstractDungeon.combatRewardScreen.openCombat(TEXT[0]);
/* 361 */               } else if (this.smoked) {
/* 362 */                 AbstractDungeon.combatRewardScreen.openCombat(TEXT[1], true);
/*     */               } else {
/* 364 */                 AbstractDungeon.combatRewardScreen.open();
/*     */               }
/*     */               
/* 367 */               if ((!CardCrawlGame.loadingSave) && (!AbstractDungeon.loading_post_combat)) {
/* 368 */                 SaveFile saveFile = new SaveFile(com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType.POST_COMBAT);
/* 369 */                 saveFile.card_seed_count = card_seed_before_roll;
/* 370 */                 if (this.combatEvent) {
/* 371 */                   saveFile.event_seed_count -= 1;
/*     */                 }
/* 373 */                 SaveAndContinue.save(saveFile);
/* 374 */                 AbstractDungeon.effectList.add(new GameSavedEffect());
/*     */               } else {
/* 376 */                 CardCrawlGame.loadingSave = false;
/*     */               }
/* 378 */               AbstractDungeon.loading_post_combat = false;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 384 */       this.monsters.updateAnimations();
/* 385 */       break;
/*     */     case COMPLETE: 
/* 387 */       if (!AbstractDungeon.isScreenUp) {
/* 388 */         AbstractDungeon.actionManager.update();
/* 389 */         if (this.event != null) {
/* 390 */           this.event.updateDialog();
/*     */         }
/* 392 */         if ((AbstractDungeon.actionManager.isEmpty()) && (!AbstractDungeon.isFadingOut))
/*     */         {
/* 394 */           if (this.rewardPopOutTimer > 1.0F) {
/* 395 */             this.rewardPopOutTimer = 1.0F;
/*     */           }
/* 397 */           this.rewardPopOutTimer -= Gdx.graphics.getDeltaTime();
/*     */           
/* 399 */           if (this.rewardPopOutTimer < 0.0F) {
/* 400 */             if (this.event == null) {
/* 401 */               AbstractDungeon.overlayMenu.proceedButton.show();
/* 402 */             } else if ((!(this.event instanceof AbstractImageEvent)) && (!this.event.hasFocus)) {
/* 403 */               AbstractDungeon.overlayMenu.proceedButton.show();
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */       break;
/*     */     case INCOMPLETE: 
/*     */       break;
/*     */     default: 
/* 412 */       logger.info("MISSING PHASE, bro");
/*     */     }
/*     */     
/*     */     
/* 416 */     AbstractDungeon.player.update();
/* 417 */     AbstractDungeon.player.updateAnimations();
/*     */   }
/*     */   
/*     */ 
/*     */   public void endTurn()
/*     */   {
/* 423 */     AbstractDungeon.player.applyEndOfTurnTriggers();
/*     */     
/* 425 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.ClearCardQueueAction());
/* 426 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DiscardAtEndOfTurnAction());
/*     */     
/*     */ 
/* 429 */     for (AbstractCard c : AbstractDungeon.player.drawPile.group) {
/* 430 */       c.resetAttributes();
/*     */     }
/* 432 */     for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/* 433 */       c.resetAttributes();
/*     */     }
/* 435 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 436 */       c.resetAttributes();
/*     */     }
/*     */     
/* 439 */     if (AbstractDungeon.player.hoveredCard != null) {
/* 440 */       AbstractDungeon.player.hoveredCard.resetAttributes();
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 447 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.AbstractGameAction()
/*     */     {
/*     */       public void update() {
/* 450 */         AbstractDungeon.actionManager.addToBottom(new EndTurnAction());
/* 451 */         if (Settings.FAST_MODE) {
/* 452 */           AbstractDungeon.actionManager.addToBottom(new WaitAction(0.1F));
/*     */         } else {
/* 454 */           AbstractDungeon.actionManager.addToBottom(new WaitAction(1.2F));
/*     */         }
/* 456 */         AbstractDungeon.actionManager.addToBottom(new MonsterStartTurnAction());
/* 457 */         AbstractDungeon.actionManager.monsterAttacksQueued = false;
/* 458 */         this.isDone = true;
/*     */       }
/*     */       
/* 461 */     });
/* 462 */     AbstractDungeon.player.isEndingTurn = false;
/*     */   }
/*     */   
/*     */   public void endBattle() {
/* 466 */     this.isBattleOver = true;
/*     */     
/*     */ 
/* 469 */     if (AbstractDungeon.player.currentHealth == 1) {
/* 470 */       UnlockTracker.unlockAchievement("SHRUG_IT_OFF");
/*     */     }
/*     */     
/* 473 */     if (AbstractDungeon.player.hasRelic("Meat on the Bone")) {
/* 474 */       AbstractDungeon.player.getRelic("Meat on the Bone").onTrigger();
/*     */     }
/*     */     
/* 477 */     AbstractDungeon.player.onVictory();
/* 478 */     this.endBattleTimer = 0.25F;
/*     */     
/* 480 */     int attackCount = 0;int skillCount = 0;
/* 481 */     for (AbstractCard c : AbstractDungeon.actionManager.cardsPlayedThisCombat) {
/* 482 */       if (c.type == AbstractCard.CardType.ATTACK) {
/* 483 */         attackCount++;
/* 484 */         break; }
/* 485 */       if (c.type == AbstractCard.CardType.SKILL) {
/* 486 */         skillCount++;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 491 */     if ((attackCount == 0) && (this.smoked != true)) {
/* 492 */       UnlockTracker.unlockAchievement("COME_AT_ME");
/*     */     }
/*     */     
/*     */ 
/* 496 */     if ((skillCount != 0) || 
/*     */     
/*     */ 
/* 499 */       (this.smoked != true))
/*     */     {
/* 501 */       if ((GameActionManager.damageReceivedThisCombat - GameActionManager.hpLossThisCombat <= 0) && ((this instanceof MonsterRoomElite)))
/*     */       {
/* 503 */         CardCrawlGame.champion += 1;
/*     */       }
/*     */     }
/*     */     
/* 507 */     CardCrawlGame.metricData.addEncounterData();
/*     */     
/* 509 */     AbstractDungeon.actionManager.clear();
/* 510 */     AbstractDungeon.player.inSingleTargetMode = false;
/* 511 */     AbstractDungeon.player.releaseCard();
/* 512 */     AbstractDungeon.player.hand.refreshHandLayout();
/* 513 */     AbstractDungeon.player.resetControllerValues();
/* 514 */     AbstractDungeon.overlayMenu.hideCombatPanels();
/*     */   }
/*     */   
/*     */ 
/*     */   public void dropReward() {}
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 522 */     if (((this instanceof EventRoom)) || ((this instanceof VictoryRoom)))
/*     */     {
/* 524 */       if ((this.event != null) && ((!(this.event instanceof AbstractImageEvent)) || (this.event.combatTime))) {
/* 525 */         this.event.renderRoomEventPanel(sb);
/* 526 */         AbstractDungeon.player.render(sb);
/*     */       }
/* 528 */     } else if (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.BOSS_REWARD)
/*     */     {
/* 530 */       AbstractDungeon.player.render(sb);
/*     */     }
/*     */     
/* 533 */     if (!(AbstractDungeon.getCurrRoom() instanceof RestRoom)) {
/* 534 */       if ((this.monsters != null) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.DEATH)) {
/* 535 */         this.monsters.render(sb);
/*     */       }
/*     */       
/*     */ 
/* 539 */       if (this.phase == RoomPhase.COMBAT) {
/* 540 */         AbstractDungeon.player.renderPlayerBattleUi(sb);
/*     */       }
/*     */       
/* 543 */       for (AbstractPotion i : this.potions) {
/* 544 */         if (!i.isObtained) {
/* 545 */           i.render(sb);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 550 */     for (AbstractRelic r : this.relics) {
/* 551 */       r.render(sb);
/*     */     }
/*     */     
/* 554 */     renderTips(sb);
/*     */   }
/*     */   
/*     */   public void renderAboveTopPanel(SpriteBatch sb) {
/* 558 */     for (AbstractPotion i : this.potions) {
/* 559 */       if (i.isObtained) {
/* 560 */         i.render(sb);
/*     */       }
/*     */     }
/*     */     
/* 564 */     this.souls.render(sb);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 569 */     if (Settings.isInfo)
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 583 */       String msg = "[GAME MODE DATA]\n isDaily: " + Settings.isDailyRun + "\n isSpecialSeed: " + Settings.isTrial + "\n isAscension: " + AbstractDungeon.isAscensionMode + "\n\n[CARDGROUPS]\n Deck: " + AbstractDungeon.player.masterDeck.size() + "\n Draw Pile: " + AbstractDungeon.player.drawPile.size() + "\n Discard Pile: " + AbstractDungeon.player.discardPile.size() + "\n Exhaust Pile: " + AbstractDungeon.player.exhaustPile.size() + "\n\n[ACTION MANAGER]\n Phase: " + AbstractDungeon.actionManager.phase.name() + "\n turnEnded: " + AbstractDungeon.actionManager.turnHasEnded + "\n numTurns: " + GameActionManager.turn + "\n\n[Misc]\n Steam Connection: " + ((SteamSaveSync.steamUser != null) && (SteamSaveSync.steamStats != null)) + "\n CUR_SCREEN: " + AbstractDungeon.screen.name() + "\n Controller Mode: " + Settings.isControllerMode + "\n isFadingOut: " + AbstractDungeon.isFadingOut + "\n isScreenUp: " + AbstractDungeon.isScreenUp + "\n Particle Count: " + AbstractDungeon.effectList.size();
/*     */       
/* 585 */       FontHelper.renderFontCenteredHeight(sb, FontHelper.tipBodyFont, msg, 30.0F, Settings.HEIGHT * 0.5F, com.badlogic.gdx.graphics.Color.WHITE);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderTips(SpriteBatch sb) {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void spawnRelicAndObtain(float x, float y, AbstractRelic relic)
/*     */   {
/* 599 */     if ((relic.relicId == "Circlet") && (AbstractDungeon.player.hasRelic("Circlet"))) {
/* 600 */       AbstractRelic circ = AbstractDungeon.player.getRelic("Circlet");
/* 601 */       circ.counter += 1;
/* 602 */       circ.flash();
/*     */     } else {
/* 604 */       relic.spawn(x, y);
/* 605 */       this.relics.add(relic);
/* 606 */       relic.obtain();
/* 607 */       relic.isObtained = true;
/* 608 */       relic.isAnimating = false;
/* 609 */       relic.isDone = false;
/* 610 */       relic.flash();
/*     */     }
/*     */   }
/*     */   
/*     */   public void spawnBlightAndObtain(float x, float y, AbstractBlight blight) {
/* 615 */     blight.spawn(x, y);
/* 616 */     blight.obtain();
/* 617 */     blight.isObtained = true;
/* 618 */     blight.isAnimating = false;
/* 619 */     blight.isDone = false;
/* 620 */     blight.flash();
/*     */   }
/*     */   
/*     */   public void applyEndOfTurnRelics()
/*     */   {
/* 625 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 626 */       r.onPlayerEndTurn();
/*     */     }
/*     */     
/* 629 */     for (AbstractBlight b : AbstractDungeon.player.blights) {
/* 630 */       b.onPlayerEndTurn();
/*     */     }
/*     */   }
/*     */   
/*     */   public void addRelicToRewards(AbstractRelic.RelicTier tier) {
/* 635 */     this.rewards.add(new RewardItem(AbstractDungeon.returnRandomRelic(tier)));
/*     */   }
/*     */   
/*     */   public void removeRelicFromRewards() {
/* 639 */     this.rewards.clear();
/*     */   }
/*     */   
/*     */   public void addNoncampRelicToRewards(AbstractRelic.RelicTier tier) {
/* 643 */     this.rewards.add(new RewardItem(AbstractDungeon.returnRandomNonCampfireRelic(tier)));
/*     */   }
/*     */   
/*     */   public void addRelicToRewards(AbstractRelic relic) {
/* 647 */     this.rewards.add(new RewardItem(relic));
/*     */   }
/*     */   
/*     */   public void addPotionToRewards(AbstractPotion potion) {
/* 651 */     this.rewards.add(new RewardItem(potion));
/*     */   }
/*     */   
/*     */   public void addCardToRewards() {
/* 655 */     RewardItem cardReward = new RewardItem();
/* 656 */     if (cardReward.cards.size() > 0) {
/* 657 */       this.rewards.add(cardReward);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addPotionToRewards()
/*     */   {
/* 666 */     int chance = 0;
/* 667 */     if ((this instanceof MonsterRoomElite)) {
/* 668 */       chance = 40;
/* 669 */       chance += blizzardPotionMod;
/* 670 */     } else if ((this instanceof MonsterRoom)) {
/* 671 */       if (!AbstractDungeon.getMonsters().haveMonstersEscaped()) {
/* 672 */         chance = 40;
/* 673 */         chance += blizzardPotionMod;
/*     */       }
/* 675 */     } else if ((this instanceof MonsterRoomBoss)) {
/* 676 */       chance = 0;
/* 677 */     } else if ((this instanceof EventRoom)) {
/* 678 */       chance = 40;
/* 679 */       chance += blizzardPotionMod;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 688 */     if (AbstractDungeon.player.hasRelic("White Beast Statue")) {
/* 689 */       chance = 100;
/*     */     }
/*     */     
/* 692 */     logger.info("POTION CHANCE: " + chance);
/*     */     
/*     */ 
/* 695 */     if ((AbstractDungeon.potionRng.random(0, 99) < chance) || (Settings.isDebug)) {
/* 696 */       CardCrawlGame.metricData.potions_floor_spawned.add(Integer.valueOf(AbstractDungeon.floorNum));
/* 697 */       this.rewards.add(new RewardItem(AbstractDungeon.returnRandomPotion()));
/* 698 */       blizzardPotionMod -= 10;
/*     */     } else {
/* 700 */       blizzardPotionMod += 10;
/*     */     }
/*     */   }
/*     */   
/*     */   public void addGoldToRewards(int gold) {
/* 705 */     for (RewardItem i : this.rewards) {
/* 706 */       if (i.type == RewardItem.RewardType.GOLD) {
/* 707 */         i.incrementGold(gold);
/* 708 */         return;
/*     */       }
/*     */     }
/*     */     
/* 712 */     this.rewards.add(new RewardItem(gold));
/*     */   }
/*     */   
/*     */   public void addStolenGoldToRewards(int gold) {
/* 716 */     for (RewardItem i : this.rewards) {
/* 717 */       if (i.type == RewardItem.RewardType.STOLEN_GOLD) {
/* 718 */         i.incrementGold(gold);
/* 719 */         return;
/*     */       }
/*     */     }
/* 722 */     this.rewards.add(new RewardItem(gold, true));
/*     */   }
/*     */   
/*     */   public boolean isBattleEnding() {
/* 726 */     if (this.isBattleOver) {
/* 727 */       return true;
/*     */     }
/* 729 */     if (this.monsters != null) {
/* 730 */       return this.monsters.areMonstersBasicallyDead();
/*     */     }
/* 732 */     return false;
/*     */   }
/*     */   
/*     */   public void renderEventTexts(SpriteBatch sb) {
/* 736 */     if (this.event != null) {
/* 737 */       this.event.renderText(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   public void clearEvent() {
/* 742 */     if (this.event != null) {
/* 743 */       this.event.imageEventText.clear();
/* 744 */       this.event.roomEventText.clear();
/*     */     }
/*     */   }
/*     */   
/*     */   public void eventControllerInput() {
/* 749 */     if (!Settings.isControllerMode) {
/* 750 */       return;
/*     */     }
/*     */     
/* 753 */     if ((AbstractDungeon.getCurrRoom().event != null) && (AbstractDungeon.getCurrRoom().phase != RoomPhase.COMBAT) && (!AbstractDungeon.topPanel.selectPotionMode) && (AbstractDungeon.topPanel.potionUi.isHidden) && (!AbstractDungeon.topPanel.potionUi.targetMode) && (!AbstractDungeon.player.viewingRelics))
/*     */     {
/*     */ 
/* 756 */       if (!RoomEventDialog.optionList.isEmpty()) {
/* 757 */         boolean anyHovered = false;
/* 758 */         int index = 0;
/* 759 */         for (LargeDialogOptionButton o : RoomEventDialog.optionList) {
/* 760 */           if (o.hb.hovered) {
/* 761 */             anyHovered = true;
/* 762 */             break;
/*     */           }
/* 764 */           index++;
/*     */         }
/*     */         
/* 767 */         if (!anyHovered) {
/* 768 */           Gdx.input.setCursorPosition(
/* 769 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(0)).hb.cX, Settings.HEIGHT - 
/* 770 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(0)).hb.cY);
/*     */         }
/* 772 */         else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 773 */           index++;
/* 774 */           if (index > RoomEventDialog.optionList.size() - 1) {
/* 775 */             index = 0;
/*     */           }
/* 777 */           Gdx.input.setCursorPosition(
/* 778 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(index)).hb.cX, Settings.HEIGHT - 
/* 779 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(index)).hb.cY);
/* 780 */         } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 781 */           index--;
/* 782 */           if (index < 0) {
/* 783 */             index = RoomEventDialog.optionList.size() - 1;
/*     */           }
/* 785 */           Gdx.input.setCursorPosition(
/* 786 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(index)).hb.cX, Settings.HEIGHT - 
/* 787 */             (int)((LargeDialogOptionButton)RoomEventDialog.optionList.get(index)).hb.cY);
/*     */         }
/*     */       }
/* 790 */       else if (!this.event.imageEventText.optionList.isEmpty()) {
/* 791 */         boolean anyHovered = false;
/* 792 */         int index = 0;
/* 793 */         for (LargeDialogOptionButton o : this.event.imageEventText.optionList) {
/* 794 */           if (o.hb.hovered) {
/* 795 */             anyHovered = true;
/* 796 */             break;
/*     */           }
/* 798 */           index++;
/*     */         }
/*     */         
/* 801 */         if (!anyHovered) {
/* 802 */           Gdx.input.setCursorPosition(
/* 803 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(0)).hb.cX, Settings.HEIGHT - 
/* 804 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(0)).hb.cY);
/*     */         }
/* 806 */         else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/* 807 */           index++;
/* 808 */           if (index > this.event.imageEventText.optionList.size() - 1) {
/* 809 */             index = 0;
/*     */           }
/* 811 */           Gdx.input.setCursorPosition(
/* 812 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(index)).hb.cX, Settings.HEIGHT - 
/* 813 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(index)).hb.cY);
/* 814 */         } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 815 */           index--;
/* 816 */           if (index < 0) {
/* 817 */             index = this.event.imageEventText.optionList.size() - 1;
/*     */           }
/* 819 */           Gdx.input.setCursorPosition(
/* 820 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(index)).hb.cX, Settings.HEIGHT - 
/* 821 */             (int)((LargeDialogOptionButton)this.event.imageEventText.optionList.get(index)).hb.cY);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void addCardReward(RewardItem rewardItem)
/*     */   {
/* 830 */     if (!rewardItem.cards.isEmpty()) {
/* 831 */       this.rewards.add(rewardItem);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\AbstractRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */