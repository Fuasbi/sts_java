/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.shop.Merchant;
/*    */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*    */ 
/*    */ public class ShopRoom extends AbstractRoom
/*    */ {
/* 13 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ShopRoom");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/* 16 */   public int shopRarityBonus = 6;
/*    */   public Merchant merchant;
/*    */   
/*    */   public ShopRoom() {
/* 20 */     this.phase = AbstractRoom.RoomPhase.COMPLETE;
/* 21 */     this.merchant = null;
/* 22 */     this.mapSymbol = "$";
/* 23 */     this.mapImg = ImageMaster.MAP_NODE_MERCHANT;
/* 24 */     this.mapImgOutline = ImageMaster.MAP_NODE_MERCHANT_OUTLINE;
/*    */   }
/*    */   
/*    */   public void setMerchant(Merchant merc) {
/* 28 */     this.merchant = merc;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 33 */     playBGM("SHOP");
/* 34 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[0]);
/* 35 */     setMerchant(new Merchant());
/*    */   }
/*    */   
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/* 40 */     if (roll < 3 + this.shopRarityBonus)
/* 41 */       return AbstractCard.CardRarity.RARE;
/* 42 */     if (roll < 40 + this.shopRarityBonus) {
/* 43 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 45 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 53 */     super.update();
/* 54 */     if (this.merchant != null) {
/* 55 */       this.merchant.update();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 64 */     if (this.merchant != null) {
/* 65 */       this.merchant.render(sb);
/*    */     }
/*    */     
/* 68 */     super.render(sb);
/* 69 */     renderTips(sb);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\ShopRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */