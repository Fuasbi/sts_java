/*     */ package com.megacrit.cardcrawl.rooms;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.rewards.chests.AbstractChest;
/*     */ import com.megacrit.cardcrawl.rewards.chests.BossChest;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.vfx.scene.SpookierChestEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class TreasureRoomBoss extends AbstractRoom
/*     */ {
/*  22 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("TreasureRoomBoss");
/*  23 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public AbstractChest chest;
/*     */   
/*  27 */   private float shinyTimer = 0.0F;
/*     */   private static final float SHINY_INTERVAL = 0.02F;
/*  29 */   public boolean choseRelic = false;
/*     */   
/*     */   public TreasureRoomBoss()
/*     */   {
/*  33 */     CardCrawlGame.nextDungeon = getNextDungeonName();
/*  34 */     if ((AbstractDungeon.actNum < 4) || (!AbstractPlayer.customMods.contains("Blight Chests"))) {
/*  35 */       this.phase = AbstractRoom.RoomPhase.COMPLETE;
/*     */     } else {
/*  37 */       this.phase = AbstractRoom.RoomPhase.INCOMPLETE;
/*     */     }
/*  39 */     this.mapImg = ImageMaster.MAP_NODE_TREASURE;
/*  40 */     this.mapImgOutline = ImageMaster.MAP_NODE_TREASURE_OUTLINE;
/*     */   }
/*     */   
/*     */   private String getNextDungeonName() {
/*  44 */     switch (AbstractDungeon.id) {
/*     */     case "Exordium": 
/*  46 */       return "TheCity";
/*     */     case "TheCity": 
/*  48 */       return "TheBeyond";
/*     */     case "TheBeyond": 
/*  50 */       if (com.megacrit.cardcrawl.core.Settings.isEndless) {
/*  51 */         return "Exordium";
/*     */       }
/*  53 */       return null;
/*     */     }
/*  55 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public void onPlayerEntry()
/*     */   {
/*  61 */     CardCrawlGame.music.silenceBGM();
/*  62 */     if ((AbstractDungeon.actNum < 4) || (!AbstractPlayer.customMods.contains("Blight Chests"))) {
/*  63 */       AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[0]);
/*     */     }
/*  65 */     playBGM("SHRINE");
/*  66 */     this.chest = new BossChest();
/*     */   }
/*     */   
/*     */ 
/*     */   public AbstractCard.CardRarity getCardRarity(int roll)
/*     */   {
/*     */     int rareRate;
/*     */     int rareRate;
/*  74 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/*  75 */       rareRate = 9;
/*     */     } else {
/*  77 */       rareRate = 3;
/*     */     }
/*     */     
/*     */ 
/*  81 */     if (roll < rareRate)
/*  82 */       return AbstractCard.CardRarity.RARE;
/*  83 */     if (roll < 40) {
/*  84 */       return AbstractCard.CardRarity.UNCOMMON;
/*     */     }
/*  86 */     return AbstractCard.CardRarity.COMMON;
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/*  92 */     super.update();
/*  93 */     this.chest.update();
/*  94 */     updateShiny();
/*     */   }
/*     */   
/*     */   private void updateShiny() {
/*  98 */     if (!this.chest.isOpen) {
/*  99 */       this.shinyTimer -= Gdx.graphics.getDeltaTime();
/* 100 */       if (this.shinyTimer < 0.0F) {
/* 101 */         this.shinyTimer = 0.02F;
/* 102 */         AbstractDungeon.effectList.add(new SpookierChestEffect());
/* 103 */         AbstractDungeon.effectList.add(new SpookierChestEffect());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void renderAboveTopPanel(SpriteBatch sb)
/*     */   {
/* 110 */     super.renderAboveTopPanel(sb);
/*     */   }
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 116 */     this.chest.render(sb);
/* 117 */     super.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\TreasureRoomBoss.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */