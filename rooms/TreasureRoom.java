/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.rewards.chests.AbstractChest;
/*    */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*    */ import com.megacrit.cardcrawl.vfx.scene.SpookyChestEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class TreasureRoom extends AbstractRoom
/*    */ {
/* 17 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("TreasureRoom");
/* 18 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public AbstractChest chest;
/*    */   
/* 22 */   private float shinyTimer = 0.0F;
/*    */   private static final float SHINY_INTERVAL = 0.2F;
/*    */   
/*    */   public TreasureRoom()
/*    */   {
/* 27 */     this.phase = AbstractRoom.RoomPhase.COMPLETE;
/* 28 */     this.mapSymbol = "T";
/* 29 */     this.mapImg = ImageMaster.MAP_NODE_TREASURE;
/* 30 */     this.mapImgOutline = ImageMaster.MAP_NODE_TREASURE_OUTLINE;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 35 */     playBGM(null);
/* 36 */     this.chest = AbstractDungeon.getRandomChest();
/* 37 */     AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/*    */     int rareRate;
/*    */     
/*    */     int rareRate;
/* 46 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/* 47 */       rareRate = 9;
/*    */     } else {
/* 49 */       rareRate = 3;
/*    */     }
/*    */     
/*    */ 
/* 53 */     if (roll < rareRate) {
/* 54 */       if ((AbstractDungeon.player.hasRelic("Nloth's Gift")) && (roll > 3)) {
/* 55 */         AbstractDungeon.player.getRelic("Nloth's Gift").flash();
/*    */       }
/* 57 */       return AbstractCard.CardRarity.RARE; }
/* 58 */     if (roll < 40) {
/* 59 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 61 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 67 */     super.update();
/* 68 */     if (this.chest != null) {
/* 69 */       this.chest.update();
/*    */     }
/* 71 */     updateShiny();
/*    */   }
/*    */   
/*    */   private void updateShiny() {
/* 75 */     if (!this.chest.isOpen) {
/* 76 */       this.shinyTimer -= Gdx.graphics.getDeltaTime();
/* 77 */       if (this.shinyTimer < 0.0F) {
/* 78 */         this.shinyTimer = 0.2F;
/* 79 */         AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.ChestShineEffect());
/* 80 */         AbstractDungeon.effectList.add(new SpookyChestEffect());
/* 81 */         AbstractDungeon.effectList.add(new SpookyChestEffect());
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void renderAboveTopPanel(SpriteBatch sb)
/*    */   {
/* 88 */     super.renderAboveTopPanel(sb);
/*    */   }
/*    */   
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 94 */     if (this.chest != null) {
/* 95 */       this.chest.render(sb);
/*    */     }
/* 97 */     super.render(sb);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\TreasureRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */