/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class RestRoom extends AbstractRoom
/*    */ {
/* 16 */   private static final Logger logger = LogManager.getLogger(RestRoom.class.getName());
/*    */   public long fireSoundId;
/*    */   public CampfireUI campfireUI;
/*    */   
/*    */   public RestRoom()
/*    */   {
/* 22 */     this.phase = AbstractRoom.RoomPhase.INCOMPLETE;
/* 23 */     this.mapSymbol = "R";
/* 24 */     this.mapImg = ImageMaster.MAP_NODE_REST;
/* 25 */     this.mapImgOutline = ImageMaster.MAP_NODE_REST_OUTLINE;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 30 */     CardCrawlGame.music.silenceBGM();
/* 31 */     this.fireSoundId = CardCrawlGame.sound.playAndLoop("REST_FIRE_WET");
/* 32 */     this.campfireUI = new CampfireUI();
/*    */     
/* 34 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 35 */       r.onEnterRestRoom();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/* 41 */     logger.info("This should only be called by DreamCatcher!");
/* 42 */     if (roll < 3)
/* 43 */       return AbstractCard.CardRarity.RARE;
/* 44 */     if (roll < 40) {
/* 45 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 47 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 52 */     super.update();
/* 53 */     if (this.campfireUI != null) {
/* 54 */       this.campfireUI.update();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void fadeIn()
/*    */   {
/* 62 */     CardCrawlGame.music.unsilenceBGM();
/*    */   }
/*    */   
/*    */   public void cutFireSound() {
/* 66 */     CardCrawlGame.sound.fadeOut("REST_FIRE_WET", ((RestRoom)AbstractDungeon.getCurrRoom()).fireSoundId);
/*    */   }
/*    */   
/*    */   public void updateAmbience() {
/* 70 */     CardCrawlGame.sound.adjustVolume("REST_FIRE_WET", this.fireSoundId);
/*    */   }
/*    */   
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 76 */     if (this.campfireUI != null) {
/* 77 */       this.campfireUI.render(sb);
/*    */     }
/* 79 */     super.render(sb);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\RestRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */