/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*    */ import com.megacrit.cardcrawl.screens.DiscardPileViewScreen;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class MonsterRoom extends AbstractRoom
/*    */ {
/*    */   public DiscardPileViewScreen discardPileViewScreen;
/*    */   public static final float COMBAT_WAIT_TIME = 0.1F;
/*    */   
/*    */   public MonsterRoom()
/*    */   {
/* 21 */     this.phase = AbstractRoom.RoomPhase.COMBAT;
/*    */     
/* 23 */     this.mapSymbol = "M";
/* 24 */     this.mapImg = ImageMaster.MAP_NODE_ENEMY;
/* 25 */     this.mapImgOutline = ImageMaster.MAP_NODE_ENEMY_OUTLINE;
/*    */     
/*    */ 
/* 28 */     this.discardPileViewScreen = new DiscardPileViewScreen();
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/*    */     int rareRate;
/*    */     int rareRate;
/* 36 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/* 37 */       rareRate = 9;
/*    */     } else {
/* 39 */       rareRate = 3;
/*    */     }
/*    */     
/*    */ 
/* 43 */     if (roll < rareRate) {
/* 44 */       if ((AbstractDungeon.player.hasRelic("Nloth's Gift")) && (roll > 3)) {
/* 45 */         AbstractDungeon.player.getRelic("Nloth's Gift").flash();
/*    */       }
/* 47 */       return AbstractCard.CardRarity.RARE; }
/* 48 */     if (roll < 40) {
/* 49 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 51 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */   public void dropReward()
/*    */   {
/* 56 */     if (((Boolean)com.megacrit.cardcrawl.daily.DailyMods.negativeMods.get("Vintage")).booleanValue()) {
/* 57 */       AbstractRelic.RelicTier tier = returnRandomRelicTier();
/* 58 */       addRelicToRewards(tier);
/*    */     }
/*    */   }
/*    */   
/*    */   private AbstractRelic.RelicTier returnRandomRelicTier() {
/* 63 */     int roll = AbstractDungeon.relicRng.random(0, 99);
/*    */     
/*    */ 
/* 66 */     if (roll < 50) {
/* 67 */       return AbstractRelic.RelicTier.COMMON;
/*    */     }
/* 69 */     if (roll > 85) {
/* 70 */       return AbstractRelic.RelicTier.RARE;
/*    */     }
/*    */     
/*    */ 
/* 74 */     return AbstractRelic.RelicTier.UNCOMMON;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 79 */     playBGM(null);
/* 80 */     if (this.monsters == null) {
/* 81 */       this.monsters = AbstractDungeon.getMonsterForRoomCreation();
/* 82 */       this.monsters.init();
/*    */     }
/*    */     
/* 85 */     waitTimer = 0.1F;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setMonster(MonsterGroup m)
/*    */   {
/* 92 */     this.monsters = m;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 99 */     super.render(sb);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\MonsterRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */