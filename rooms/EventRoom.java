/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ 
/*    */ public class EventRoom extends AbstractRoom
/*    */ {
/*    */   public EventRoom()
/*    */   {
/* 13 */     this.phase = AbstractRoom.RoomPhase.EVENT;
/* 14 */     this.mapSymbol = "?";
/* 15 */     this.mapImg = com.megacrit.cardcrawl.helpers.ImageMaster.MAP_NODE_EVENT;
/* 16 */     this.mapImgOutline = com.megacrit.cardcrawl.helpers.ImageMaster.MAP_NODE_EVENT_OUTLINE;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 21 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 22 */     this.event = AbstractDungeon.generateEvent();
/* 23 */     this.event.onEnterRoom();
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/*    */     int rareRate;
/*    */     int rareRate;
/* 31 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/* 32 */       rareRate = 9;
/*    */     } else {
/* 34 */       rareRate = 3;
/*    */     }
/*    */     
/*    */ 
/* 38 */     if (roll < rareRate) {
/* 39 */       if ((AbstractDungeon.player.hasRelic("Nloth's Gift")) && (roll > 3)) {
/* 40 */         AbstractDungeon.player.getRelic("Nloth's Gift").flash();
/*    */       }
/* 42 */       return AbstractCard.CardRarity.RARE; }
/* 43 */     if (roll < 40) {
/* 44 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 46 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 51 */     super.update();
/* 52 */     if (!AbstractDungeon.isScreenUp) {
/* 53 */       this.event.update();
/*    */     }
/*    */     
/*    */ 
/* 57 */     if ((this.event.waitTimer == 0.0F) && (!this.event.hasFocus) && 
/* 58 */       (this.phase != AbstractRoom.RoomPhase.COMBAT)) {
/* 59 */       this.phase = AbstractRoom.RoomPhase.COMPLETE;
/* 60 */       this.event.reopen();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 68 */     if (this.event != null) {
/* 69 */       this.event.render(sb);
/*    */     }
/* 71 */     super.render(sb);
/*    */   }
/*    */   
/*    */   public void renderAboveTopPanel(SpriteBatch sb)
/*    */   {
/* 76 */     super.renderAboveTopPanel(sb);
/* 77 */     if (this.event != null) {
/* 78 */       this.event.renderAboveTopPanel(sb);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\EventRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */