/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ 
/*    */ public class VictoryRoom extends AbstractRoom
/*    */ {
/*    */   public VictoryRoom()
/*    */   {
/* 11 */     this.phase = AbstractRoom.RoomPhase.EVENT;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 16 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/* 17 */     this.event = new com.megacrit.cardcrawl.events.beyond.SpireHeart();
/* 18 */     this.event.onEnterRoom();
/*    */   }
/*    */   
/*    */   public com.megacrit.cardcrawl.cards.AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/* 23 */     return null;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     super.update();
/* 29 */     if (!AbstractDungeon.isScreenUp) {
/* 30 */       this.event.update();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 37 */     if (this.event != null) {
/* 38 */       this.event.renderRoomEventPanel(sb);
/* 39 */       this.event.render(sb);
/*    */     }
/* 41 */     super.render(sb);
/*    */   }
/*    */   
/*    */   public void renderAboveTopPanel(SpriteBatch sb)
/*    */   {
/* 46 */     super.renderAboveTopPanel(sb);
/* 47 */     if (this.event != null) {
/* 48 */       this.event.renderAboveTopPanel(sb);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\VictoryRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */