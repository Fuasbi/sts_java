/*    */ package com.megacrit.cardcrawl.rooms;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.random.Random;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class MonsterRoomElite extends MonsterRoom
/*    */ {
/*    */   public MonsterRoomElite()
/*    */   {
/* 18 */     this.mapSymbol = "E";
/* 19 */     this.mapImg = ImageMaster.MAP_NODE_ELITE;
/* 20 */     this.mapImgOutline = ImageMaster.MAP_NODE_ELITE_OUTLINE;
/* 21 */     this.eliteTrigger = true;
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 26 */     playBGM(null);
/* 27 */     if (this.monsters == null) {
/* 28 */       this.monsters = AbstractDungeon.getEliteMonsterForRoomCreation();
/* 29 */       this.monsters.init();
/*    */     }
/*    */     
/* 32 */     waitTimer = 0.1F;
/*    */   }
/*    */   
/*    */   public void dropReward()
/*    */   {
/* 37 */     AbstractRelic.RelicTier tier = returnRandomRelicTier();
/* 38 */     if ((com.megacrit.cardcrawl.core.Settings.isEndless) && (AbstractDungeon.player.hasBlight("MimicInfestation")))
/*    */     {
/* 40 */       AbstractDungeon.player.getBlight("MimicInfestation").flash();
/*    */     } else {
/* 42 */       addRelicToRewards(tier);
/* 43 */       if (AbstractDungeon.player.hasRelic("Black Star")) {
/* 44 */         addNoncampRelicToRewards(tier);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   private AbstractRelic.RelicTier returnRandomRelicTier()
/*    */   {
/* 55 */     int roll = AbstractDungeon.relicRng.random(0, 99);
/*    */     
/* 57 */     if (((Boolean)DailyMods.negativeMods.get("Elite Swarm")).booleanValue()) {
/* 58 */       roll += 10;
/*    */     }
/*    */     
/*    */ 
/* 62 */     if (roll < 50) {
/* 63 */       return AbstractRelic.RelicTier.COMMON;
/*    */     }
/* 65 */     if (roll > 82) {
/* 66 */       return AbstractRelic.RelicTier.RARE;
/*    */     }
/*    */     
/*    */ 
/* 70 */     return AbstractRelic.RelicTier.UNCOMMON;
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/* 76 */     if (((Boolean)DailyMods.negativeMods.get("Elite Swarm")).booleanValue()) {
/* 77 */       return AbstractCard.CardRarity.RARE;
/*    */     }
/*    */     
/*    */     int rareRate;
/*    */     int rareRate;
/* 82 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/* 83 */       rareRate = 30;
/*    */     } else {
/* 85 */       rareRate = 10;
/*    */     }
/*    */     
/*    */ 
/* 89 */     if (roll < rareRate) {
/* 90 */       if ((AbstractDungeon.player.hasRelic("Nloth's Gift")) && (roll > 3)) {
/* 91 */         AbstractDungeon.player.getRelic("Nloth's Gift").flash();
/*    */       }
/* 93 */       return AbstractCard.CardRarity.RARE; }
/* 94 */     if (roll < 50) {
/* 95 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 97 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rooms\MonsterRoomElite.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */