/*    */ package com.megacrit.cardcrawl.daily;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Net.HttpRequest;
/*    */ import com.badlogic.gdx.Net.HttpResponse;
/*    */ import com.badlogic.gdx.net.HttpRequestBuilder;
/*    */ import com.badlogic.gdx.net.HttpStatus;
/*    */ import java.util.concurrent.atomic.AtomicInteger;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class TimeLookup
/*    */ {
/* 13 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(TimeLookup.class.getName());
/*    */   
/* 15 */   public static volatile boolean isDone = false;
/*    */   
/*    */   private static final String timeServer = "https://hyi3lwrhf5.execute-api.us-east-1.amazonaws.com/prod/time";
/* 18 */   private static volatile AtomicInteger retryCount = new AtomicInteger(1);
/*    */   
/*    */   private static final int MAX_RETRY = 2;
/*    */   
/*    */   private static final int WAIT_TIME_CAP = 2;
/*    */   
/*    */   private static void makeHTTPReq(String url)
/*    */   {
/* 26 */     HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     Net.HttpRequest httpRequest = requestBuilder.newRequest().method("GET").url(url).header("Content-Type", "application/json").header("Accept", "application/json").header("User-Agent", "curl/7.43.0").timeout(5000).build();
/* 36 */     Gdx.net.sendHttpRequest(httpRequest, new com.badlogic.gdx.Net.HttpResponseListener()
/*    */     {
/*    */       public void handleHttpResponse(Net.HttpResponse httpResponse) {
/* 39 */         String status = String.valueOf(httpResponse.getStatus().getStatusCode());
/* 40 */         String result = httpResponse.getResultAsString();
/* 41 */         if (!status.startsWith("2")) {
/* 42 */           TimeLookup.logger.info("Query to sts-time-server failed: status_code=" + status + " result=" + result);
/*    */         }
/* 44 */         TimeLookup.logger.info("Time server response: " + result);
/* 45 */         long serverTime = Long.parseLong(result);
/* 46 */         TimeLookup.isDone = true;
/* 47 */         TimeHelper.setTime(serverTime, false);
/*    */       }
/*    */       
/*    */       public void failed(Throwable t)
/*    */       {
/* 52 */         TimeLookup.logger.info("http request failed: " + t.toString());
/* 53 */         TimeLookup.logger.info("retry count: " + TimeLookup.retryCount);
/*    */         
/* 55 */         if (TimeLookup.retryCount.get() > 2) {
/* 56 */           TimeLookup.logger.info("Failed to lookup time. Switching to OFFLINE MODE!");
/* 57 */           long localTime = System.currentTimeMillis() / 1000L;
/* 58 */           TimeHelper.setTime(localTime, true);
/* 59 */           return;
/*    */         }
/* 61 */         long waitTime = Math.pow(2.0D, TimeLookup.retryCount.get());
/* 62 */         TimeLookup.logger.info("wait time: " + waitTime);
/* 63 */         if (waitTime > 2L) {
/* 64 */           waitTime = 2L;
/*    */         }
/* 66 */         TimeLookup.logger.info("Retry " + TimeLookup.retryCount.get() + ": waiting " + waitTime + " seconds for time lookup");
/* 67 */         TimeLookup.retryCount.getAndIncrement();
/*    */         try {
/* 69 */           Thread.sleep(waitTime * 1000L);
/*    */         } catch (InterruptedException ex) {
/* 71 */           Thread.currentThread().interrupt();
/* 72 */           TimeLookup.logger.info("Thread interrupted!");
/*    */         }
/* 74 */         TimeLookup.makeHTTPReq("https://hyi3lwrhf5.execute-api.us-east-1.amazonaws.com/prod/time");
/*    */       }
/*    */       
/*    */       public void cancelled()
/*    */       {
/* 79 */         TimeLookup.logger.info("http request cancelled.");
/*    */       }
/*    */     });
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void fetchDailyTimeAsync()
/*    */   {
/* 90 */     makeHTTPReq("https://hyi3lwrhf5.execute-api.us-east-1.amazonaws.com/prod/time");
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\TimeLookup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */