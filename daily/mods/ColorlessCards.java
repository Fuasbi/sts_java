/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class ColorlessCards extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Colorless Cards";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Colorless Cards");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public ColorlessCards() {
/* 12 */     super("Colorless Cards", NAME, DESC, "diverse.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\ColorlessCards.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */