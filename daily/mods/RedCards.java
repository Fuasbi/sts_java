/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class RedCards extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Red Cards";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Red Cards");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public RedCards() {
/* 12 */     super("Red Cards", NAME, DESC, "diverse.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\RedCards.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */