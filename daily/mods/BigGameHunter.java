/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class BigGameHunter extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Elite Swarm";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Elite Swarm");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public BigGameHunter() {
/* 12 */     super("Elite Swarm", NAME, DESC, "elite_swarm.png", false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\BigGameHunter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */