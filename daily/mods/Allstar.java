/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class Allstar extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Allstar";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Allstar");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public Allstar() {
/* 12 */     super("Allstar", NAME, DESC, "all_star.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\Allstar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */