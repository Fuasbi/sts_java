/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class TimeDilation extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Time Dilation";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Time Dilation");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public TimeDilation() {
/* 12 */     super("Time Dilation", NAME, DESC, "slow_start.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\TimeDilation.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */