/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.helpers.GameDataStringBuilder;
/*    */ 
/*    */ public class AbstractDailyMod {
/*    */   public String name;
/*    */   public String description;
/*    */   public String modID;
/*    */   public com.badlogic.gdx.graphics.Texture img;
/*    */   public boolean positive;
/*    */   private static final String IMG_DIR = "images/ui/run_mods/";
/*    */   
/*    */   public AbstractDailyMod(String setId, String name, String description, String imgUrl, boolean positive) {
/* 14 */     this.modID = setId;
/* 15 */     this.name = name;
/* 16 */     this.description = description;
/* 17 */     this.positive = positive;
/* 18 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/ui/run_mods/" + imgUrl);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void effect() {}
/*    */   
/*    */ 
/*    */   public static String gameDataUploadHeader()
/*    */   {
/* 28 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 29 */     sb.addFieldData("name");
/* 30 */     sb.addFieldData("text");
/* 31 */     return sb.toString();
/*    */   }
/*    */   
/*    */   public String gameDataUploadData() {
/* 35 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 36 */     sb.addFieldData(this.name);
/* 37 */     sb.addFieldData(this.description);
/* 38 */     return sb.toString();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\AbstractDailyMod.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */