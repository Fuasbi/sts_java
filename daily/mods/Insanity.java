/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class Insanity extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Insanity";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Insanity");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public Insanity() {
/* 12 */     super("Insanity", NAME, DESC, "restless_journey.png", false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\Insanity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */