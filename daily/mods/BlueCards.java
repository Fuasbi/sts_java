/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class BlueCards extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Blue Cards";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Blue Cards");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public BlueCards() {
/* 12 */     super("Blue Cards", NAME, DESC, "diverse.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\BlueCards.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */