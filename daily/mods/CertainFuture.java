/*    */ package com.megacrit.cardcrawl.daily.mods;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.RunModStrings;
/*    */ 
/*    */ public class CertainFuture extends AbstractDailyMod
/*    */ {
/*    */   public static final String ID = "Uncertain Future";
/*  8 */   private static final RunModStrings modStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getRunModString("Uncertain Future");
/*  9 */   public static final String NAME = modStrings.NAME; public static final String DESC = modStrings.DESCRIPTION;
/*    */   
/*    */   public CertainFuture() {
/* 12 */     super("Uncertain Future", NAME, DESC, "certain_future.png", false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\mods\CertainFuture.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */