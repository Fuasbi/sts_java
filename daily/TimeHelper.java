/*    */ package com.megacrit.cardcrawl.daily;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import java.text.DateFormat;
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.Date;
/*    */ import java.util.TimeZone;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class TimeHelper
/*    */ {
/* 14 */   private static final Logger logger = LogManager.getLogger(TimeHelper.class.getName());
/* 15 */   public static boolean isTimeSet = false;
/*    */   private static long daySince1970;
/*    */   private static long timeServerTime;
/*    */   private static long endTimeMs;
/*    */   private static final float UPDATE_FREQ = -1.0F;
/* 20 */   private static float updateTimer = -1.0F;
/* 21 */   private static boolean offlineMode = false;
/* 22 */   private static DateFormat format = new SimpleDateFormat("MMMM dd, yyyy");
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void setTime(long unixTime, boolean isOfflineMode)
/*    */   {
/* 30 */     if (!isTimeSet) {
/* 31 */       offlineMode = isOfflineMode;
/* 32 */       timeServerTime = unixTime;
/* 33 */       daySince1970 = timeServerTime / 86400L;
/* 34 */       logger.info("Setting time to: " + timeServerTime);
/* 35 */       endTimeMs = (daySince1970 + 1L) * 86400L * 1000L;
/* 36 */       logger.info("Day was set!");
/* 37 */       isTimeSet = true;
/* 38 */       format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
/*    */     }
/*    */   }
/*    */   
/*    */   public static boolean isOfflineMode() {
/* 43 */     return offlineMode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public static long daySince1970()
/*    */   {
/* 50 */     return daySince1970;
/*    */   }
/*    */   
/*    */   public static void update() {
/* 54 */     if (isTimeSet) {
/* 55 */       updateTimer -= Gdx.graphics.getDeltaTime();
/* 56 */       if (updateTimer < 0.0F) {
/* 57 */         updateTimer = -1.0F;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public static String getTodayDate() {
/* 63 */     return format.format(new Date(System.currentTimeMillis()));
/*    */   }
/*    */   
/*    */ 
/*    */   public static String getDate(long numDaysSince1970)
/*    */   {
/* 69 */     long unixTimestampMs = (daySince1970 - numDaysSince1970) * 86400L * 1000L;
/* 70 */     return format.format(new Date(System.currentTimeMillis() - unixTimestampMs));
/*    */   }
/*    */   
/*    */ 
/*    */   public static String getTimeLeft()
/*    */   {
/* 76 */     if (endTimeMs - System.currentTimeMillis() < 0L) {
/* 77 */       endTimeMs += 86400000L;
/* 78 */       daySince1970 += 1L;
/*    */     }
/*    */     
/* 81 */     long remainingSec = (endTimeMs - System.currentTimeMillis()) / 1000L;
/* 82 */     long hours = remainingSec / 3600L;
/* 83 */     remainingSec %= 3600L;
/* 84 */     long minutes = remainingSec / 60L;
/* 85 */     return String.format("%02d:%02d:%02d", new Object[] { Long.valueOf(hours), Long.valueOf(minutes), Long.valueOf(remainingSec % 60L) });
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\TimeHelper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */