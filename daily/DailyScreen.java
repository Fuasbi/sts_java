/*     */ package com.megacrit.cardcrawl.daily;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.mods.AbstractDailyMod;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.BadWordChecker;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.SeedHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.screens.leaderboards.LeaderboardEntry;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuCancelButton;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MenuPanelScreen;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ConfirmButton;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.TimeZone;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class DailyScreen
/*     */ {
/*  39 */   private static final Logger logger = LogManager.getLogger(DailyScreen.class.getName());
/*  40 */   public static final String[] TEXT = CardCrawlGame.languagePack.getUIString("DailyScreen").TEXT;
/*  41 */   public static final String[] TEXT_2 = CardCrawlGame.languagePack.getUIString("LeaderboardsScreen").TEXT;
/*  42 */   private long lastDaily = 0L;
/*  43 */   public AbstractPlayer.PlayerClass todaysChar = null;
/*     */   
/*     */ 
/*  46 */   private Color screenColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
/*  47 */   private float alphaTarget = 0.0F;
/*  48 */   private MenuCancelButton cancelButton = new MenuCancelButton();
/*  49 */   private ConfirmButton confirmButton = new ConfirmButton(TEXT[4]);
/*  50 */   private float screenX = -1100.0F * Settings.scale; private float targetX = -1100.0F * Settings.scale;
/*     */   
/*     */ 
/*  53 */   private static final float HEADER_X = 186.0F * Settings.scale;
/*     */   private static float DATE_X;
/*  55 */   private static final float BODY_X = 208.0F * Settings.scale;
/*  56 */   private static final float CHAR_X = 304.0F * Settings.scale;
/*  57 */   private static final float MOD_DESC_X = 264.0F * Settings.scale; private static final float MOD_ICON_X = 200.0F * Settings.scale;
/*     */   private static final int CHAR_W = 128;
/*  59 */   private static final int MOD_W = 64; private static final float TITLE_Y = Settings.HEIGHT / 2.0F + 350.0F * Settings.scale;
/*  60 */   private static final float TIME_LEFT_Y = Settings.HEIGHT / 2.0F + 310.0F * Settings.scale;
/*  61 */   private static final float CHAR_IMAGE_Y = Settings.HEIGHT / 2.0F + 160.0F * Settings.scale;
/*  62 */   private static final float CHAR_HEADER_Y = Settings.HEIGHT / 2.0F + 250.0F * Settings.scale;
/*  63 */   private static final float MOD_HEADER_Y = Settings.HEIGHT / 2.0F + 136.0F * Settings.scale;
/*  64 */   private static final float MOD_LINE_W = 500.0F * Settings.scale;
/*  65 */   private static final float MOD_LINE_SPACING = 30.0F * Settings.scale;
/*  66 */   private static final float MOD_SECTION_SPACING = 60.0F * Settings.scale;
/*     */   
/*     */   private DateFormat dFormat;
/*     */   
/*  70 */   private boolean timeLookupActive = false; private boolean timeUpdated = false;
/*     */   
/*     */   private Random todayRng;
/*     */   
/*  74 */   private long currentDay = 0L;
/*     */   
/*  76 */   public boolean waiting = true; public boolean viewMyScore = false;
/*  77 */   public int currentStartIndex = 1; public int currentEndIndex = 20;
/*  78 */   public ArrayList<LeaderboardEntry> entries = new ArrayList();
/*  79 */   private Hitbox prevHb; private Hitbox nextHb; private Hitbox viewMyScoreHb = new Hitbox(250.0F * Settings.scale, 80.0F * Settings.scale);
/*     */   private Hitbox prevDayHb;
/*  81 */   private Hitbox nextDayHb; public static final float RANK_X = 1000.0F * Settings.scale; public static final float NAME_X = 1160.0F * Settings.scale; public static final float SCORE_X = 1500.0F * Settings.scale;
/*     */   
/*  83 */   private float lineFadeInTimer = 0.0F;
/*  84 */   private static final float LINE_THICKNESS = 4.0F * Settings.scale;
/*     */   
/*     */   public DailyScreen() {
/*  87 */     DATE_X = HEADER_X + FontHelper.getWidth(FontHelper.charTitleFont, TEXT[0], 1.0F) + 12.0F * Settings.scale;
/*  88 */     this.prevHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  89 */     this.prevHb.move(880.0F * Settings.scale, 530.0F * Settings.scale);
/*  90 */     this.nextHb = new Hitbox(110.0F * Settings.scale, 110.0F * Settings.scale);
/*  91 */     this.nextHb.move(1740.0F * Settings.scale, 530.0F * Settings.scale);
/*  92 */     this.prevDayHb = new Hitbox(80.0F * Settings.scale, 80.0F * Settings.scale);
/*  93 */     this.prevDayHb.move(1320.0F * Settings.scale, 900.0F * Settings.scale);
/*  94 */     this.nextDayHb = new Hitbox(80.0F * Settings.scale, 80.0F * Settings.scale);
/*  95 */     this.nextDayHb.move(1600.0F * Settings.scale, 900.0F * Settings.scale);
/*  96 */     this.viewMyScoreHb.move(1300.0F * Settings.scale, 80.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/* 100 */     this.cancelButton.update();
/* 101 */     if (this.cancelButton.hb.clicked) {
/* 102 */       hide();
/* 103 */       CardCrawlGame.mainMenuScreen.panelScreen.refresh();
/*     */     }
/*     */     
/* 106 */     this.confirmButton.update();
/*     */     
/* 108 */     this.screenColor.a = MathHelper.popLerpSnap(this.screenColor.a, this.alphaTarget);
/* 109 */     this.screenX = MathHelper.uiLerpSnap(this.screenX, this.targetX);
/* 110 */     pingTimeServer();
/*     */     
/* 112 */     if (this.confirmButton.hb.clicked) {
/* 113 */       this.confirmButton.hb.clicked = false;
/* 114 */       CardCrawlGame.chosenCharacter = this.todaysChar;
/* 115 */       CardCrawlGame.mainMenuScreen.isFadingOut = true;
/* 116 */       hide();
/* 117 */       Settings.isTrial = false;
/* 118 */       Settings.isDailyRun = true;
/* 119 */       Settings.dailyDate = TimeHelper.daySince1970();
/* 120 */       Settings.isEndless = false;
/* 121 */       CardCrawlGame.mainMenuScreen.fadeOutMusic();
/*     */     }
/*     */     
/* 124 */     updateLeaderboardSection();
/*     */   }
/*     */   
/*     */   private void updateLeaderboardSection() {
/* 128 */     if ((this.waiting) && (this.currentDay == 0L) && 
/* 129 */       (TimeHelper.daySince1970() != 0L)) {
/* 130 */       this.currentDay = TimeHelper.daySince1970();
/* 131 */       getData(1, 20);
/*     */     }
/*     */     
/*     */ 
/* 135 */     if ((!this.entries.isEmpty()) && (!this.waiting)) {
/* 136 */       this.lineFadeInTimer = MathHelper.slowColorLerpSnap(this.lineFadeInTimer, 1.0F);
/* 137 */       updateEntries();
/*     */     }
/*     */     
/* 140 */     updateDateChangeArrows();
/* 141 */     updateArrows();
/* 142 */     updateViewMyScore();
/*     */   }
/*     */   
/*     */   private void updateDateChangeArrows() {
/* 146 */     if (this.waiting) {
/* 147 */       return;
/*     */     }
/*     */     
/* 150 */     this.prevDayHb.update();
/* 151 */     if (this.prevDayHb.justHovered) {
/* 152 */       CardCrawlGame.sound.play("UI_HOVER");
/* 153 */     } else if ((this.prevDayHb.hovered) && (InputHelper.justClickedLeft)) {
/* 154 */       this.prevDayHb.clickStarted = true;
/* 155 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 156 */     } else if ((this.prevDayHb.clicked) || (CInputActionSet.drawPile.isJustPressed())) {
/* 157 */       if (this.currentDay == 0L) {
/* 158 */         this.currentDay = TimeHelper.daySince1970();
/*     */       }
/* 160 */       this.currentDay -= 1L;
/* 161 */       this.prevDayHb.clicked = false;
/* 162 */       this.waiting = true;
/* 163 */       getData(1, 20);
/*     */     }
/*     */     
/* 166 */     if ((this.currentDay != 0L) && (this.currentDay < TimeHelper.daySince1970())) {
/* 167 */       this.nextDayHb.update();
/*     */     }
/* 169 */     if (this.nextDayHb.justHovered) {
/* 170 */       CardCrawlGame.sound.play("UI_HOVER");
/* 171 */     } else if ((this.nextDayHb.hovered) && (InputHelper.justClickedLeft)) {
/* 172 */       this.nextDayHb.clickStarted = true;
/* 173 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 174 */     } else if ((this.nextDayHb.clicked) || ((CInputActionSet.discardPile.isJustPressed()) && (this.currentDay != 0L) && 
/* 175 */       (this.currentDay < TimeHelper.daySince1970()))) {
/* 176 */       if (this.currentDay == 0L) {
/* 177 */         this.currentDay = TimeHelper.daySince1970();
/*     */       }
/* 179 */       this.currentDay += 1L;
/* 180 */       this.nextDayHb.clicked = false;
/* 181 */       this.waiting = true;
/* 182 */       getData(1, 20);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateArrows() {
/* 187 */     if (this.waiting) {
/* 188 */       return;
/*     */     }
/*     */     
/* 191 */     if (this.entries.size() == 20) {
/* 192 */       this.nextHb.update();
/* 193 */       if (this.nextHb.justHovered) {
/* 194 */         CardCrawlGame.sound.play("UI_HOVER");
/* 195 */       } else if ((this.nextHb.hovered) && (InputHelper.justClickedLeft)) {
/* 196 */         this.nextHb.clickStarted = true;
/* 197 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 198 */       } else if ((this.nextHb.clicked) || (CInputActionSet.pageRightViewExhaust.isJustPressed())) {
/* 199 */         this.nextHb.clicked = false;
/* 200 */         this.currentStartIndex = (this.currentEndIndex + 1);
/* 201 */         this.currentEndIndex = (this.currentStartIndex + 19);
/* 202 */         this.waiting = true;
/* 203 */         getData(this.currentStartIndex, this.currentEndIndex);
/*     */       }
/*     */     }
/*     */     
/* 207 */     if (this.currentStartIndex != 1) {
/* 208 */       this.prevHb.update();
/* 209 */       if (this.prevHb.justHovered) {
/* 210 */         CardCrawlGame.sound.play("UI_HOVER");
/* 211 */       } else if ((this.prevHb.hovered) && (InputHelper.justClickedLeft)) {
/* 212 */         this.prevHb.clickStarted = true;
/* 213 */         CardCrawlGame.sound.play("UI_CLICK_1");
/* 214 */       } else if ((this.prevHb.clicked) || (CInputActionSet.pageLeftViewDeck.isJustPressed())) {
/* 215 */         this.prevHb.clicked = false;
/* 216 */         this.currentStartIndex -= 20;
/* 217 */         if (this.currentStartIndex < 1) {
/* 218 */           this.currentStartIndex = 1;
/*     */         }
/* 220 */         this.currentEndIndex = (this.currentStartIndex + 19);
/* 221 */         this.waiting = true;
/* 222 */         getData(this.currentStartIndex, this.currentEndIndex);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateViewMyScore() {
/* 228 */     if (this.waiting) {
/* 229 */       return;
/*     */     }
/*     */     
/* 232 */     this.viewMyScoreHb.update();
/*     */     
/* 234 */     if (this.viewMyScoreHb.justHovered) {
/* 235 */       CardCrawlGame.sound.play("UI_HOVER");
/* 236 */     } else if ((this.viewMyScoreHb.hovered) && (InputHelper.justClickedLeft)) {
/* 237 */       this.viewMyScoreHb.clickStarted = true;
/* 238 */       CardCrawlGame.sound.play("UI_CLICK_1");
/* 239 */     } else if ((this.viewMyScoreHb.clicked) || (CInputActionSet.topPanel.isJustPressed())) {
/* 240 */       this.viewMyScoreHb.clicked = false;
/* 241 */       this.viewMyScore = true;
/* 242 */       this.waiting = true;
/* 243 */       getData(this.currentStartIndex, this.currentEndIndex);
/*     */     }
/*     */   }
/*     */   
/*     */   private void getData(int startIndex, int endIndex) {
/* 248 */     if (this.currentDay != 0L) {
/* 249 */       SteamSaveSync.getDailyLeaderboard(this.currentDay, startIndex, endIndex);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateEntries() {
/* 254 */     for (LeaderboardEntry e : this.entries) {
/* 255 */       e.update();
/*     */     }
/*     */   }
/*     */   
/*     */   private void pingTimeServer() {
/* 260 */     if ((TimeHelper.isTimeSet) && (!this.timeUpdated)) {
/* 261 */       this.timeUpdated = true;
/* 262 */       this.dFormat = new SimpleDateFormat(TEXT[6]);
/* 263 */       this.dFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
/* 264 */       determineLoadout();
/* 265 */       getData(1, 20);
/* 266 */     } else if (!this.timeLookupActive) {
/* 267 */       TimeLookup.fetchDailyTimeAsync();
/* 268 */       this.timeLookupActive = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void determineLoadout() {
/* 273 */     long todaySeed = TimeHelper.daySince1970();
/*     */     
/* 275 */     Settings.specialSeed = Long.valueOf(todaySeed);
/* 276 */     Settings.dailyMods.initialize(todaySeed);
/* 277 */     logger.info("Today's mods: " + Settings.dailyMods.getEnabledModIDs().toString());
/*     */     
/* 279 */     AbstractDungeon.isAscensionMode = false;
/*     */     
/* 281 */     this.todayRng = new Random(Long.valueOf(todaySeed));
/* 282 */     int loadout = this.todayRng.random(2);
/* 283 */     Settings.seed = Long.valueOf(this.todayRng.randomLong());
/*     */     
/* 285 */     String seedText = SeedHelper.getString(Settings.seed.longValue());
/* 286 */     if (BadWordChecker.containsBadWord(seedText)) {
/* 287 */       Settings.seed = Long.valueOf(SeedHelper.generateUnoffensiveSeed(this.todayRng));
/*     */     }
/* 289 */     AbstractDungeon.generateSeeds();
/*     */     
/* 291 */     switch (loadout) {
/*     */     case 0: 
/* 293 */       this.todaysChar = AbstractPlayer.PlayerClass.IRONCLAD;
/* 294 */       break;
/*     */     case 1: 
/* 296 */       this.todaysChar = AbstractPlayer.PlayerClass.THE_SILENT;
/* 297 */       break;
/*     */     case 2: 
/* 299 */       this.todaysChar = AbstractPlayer.PlayerClass.DEFECT;
/* 300 */       break;
/*     */     default: 
/* 302 */       this.todaysChar = AbstractPlayer.PlayerClass.IRONCLAD;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 310 */     this.confirmButton.isDisabled = false;
/* 311 */     this.confirmButton.show();
/*     */     
/* 313 */     logger.info(TEXT[5] + loadout);
/*     */   }
/*     */   
/*     */   public void open() {
/* 317 */     CardCrawlGame.mainMenuScreen.screen = MainMenuScreen.CurScreen.DAILY;
/* 318 */     CardCrawlGame.mainMenuScreen.darken();
/* 319 */     this.alphaTarget = 0.8F;
/* 320 */     this.cancelButton.show(TEXT[3]);
/* 321 */     this.targetX = (100.0F * Settings.scale);
/*     */     
/* 323 */     this.confirmButton.isDisabled = true;
/* 324 */     this.confirmButton.hide();
/*     */     
/*     */ 
/* 327 */     this.waiting = true;
/* 328 */     this.viewMyScore = false;
/* 329 */     this.currentStartIndex = 1;
/* 330 */     this.currentEndIndex = 20;
/* 331 */     this.entries.clear();
/*     */   }
/*     */   
/*     */   public void hide() {
/* 335 */     this.alphaTarget = 0.0F;
/* 336 */     this.cancelButton.hide();
/* 337 */     this.targetX = (-1100.0F * Settings.scale);
/* 338 */     this.confirmButton.hide();
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 342 */     renderTitle(sb);
/*     */     
/* 344 */     if (TimeHelper.isTimeSet) {
/* 345 */       renderTimeLeft(sb);
/* 346 */       renderCharacterAndNotice(sb);
/* 347 */       renderMods(sb);
/* 348 */       this.confirmButton.render(sb);
/*     */     } else {
/* 350 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, TEXT[1], this.screenX + 50.0F * Settings.scale, 786.0F * Settings.scale, 9999.0F, MOD_LINE_SPACING, Settings.CREAM_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 361 */     renderScoreHeaders(sb);
/* 362 */     renderScoreDateToggler(sb);
/* 363 */     renderScores(sb);
/* 364 */     renderViewMyScoreBox(sb);
/* 365 */     renderArrows(sb);
/* 366 */     renderDateToggleArrows(sb);
/* 367 */     renderDisclaimer(sb);
/*     */     
/* 369 */     this.cancelButton.render(sb);
/*     */   }
/*     */   
/*     */   private void renderDateToggleArrows(SpriteBatch sb) {
/* 373 */     sb.draw(ImageMaster.CF_LEFT_ARROW, this.prevDayHb.cX - 24.0F, this.prevDayHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 391 */     if (Settings.isControllerMode) {
/* 392 */       sb.draw(ImageMaster.CONTROLLER_LT, this.prevDayHb.cX - 32.0F - 70.0F * Settings.scale, this.prevDayHb.cY - 32.0F + 0.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 411 */     if ((this.currentDay != 0L) && (this.currentDay < TimeHelper.daySince1970())) {
/* 412 */       sb.draw(ImageMaster.CF_RIGHT_ARROW, this.nextDayHb.cX - 24.0F, this.nextDayHb.cY - 24.0F, 24.0F, 24.0F, 48.0F, 48.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 48, 48, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 429 */       this.nextDayHb.render(sb);
/* 430 */       if (Settings.isControllerMode) {
/* 431 */         sb.draw(ImageMaster.CONTROLLER_RT, this.nextDayHb.cX - 32.0F + 70.0F * Settings.scale, this.nextDayHb.cY - 32.0F + 0.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 450 */     this.prevDayHb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderScoreDateToggler(SpriteBatch sb)
/*     */   {
/* 455 */     if (this.currentDay == 0L) {
/* 456 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, 
/*     */       
/*     */ 
/* 459 */         TimeHelper.getTodayDate(), 1380.0F * Settings.scale, 910.0F * Settings.scale, new Color(0.53F, 0.808F, 0.92F, this.lineFadeInTimer));
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 464 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, 
/*     */       
/*     */ 
/* 467 */         TimeHelper.getDate(this.currentDay), 1380.0F * Settings.scale, 910.0F * Settings.scale, new Color(0.53F, 0.808F, 0.92F, this.lineFadeInTimer));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderTitle(SpriteBatch sb)
/*     */   {
/* 476 */     FontHelper.renderFontLeftDownAligned(sb, FontHelper.charTitleFont, TEXT[0], HEADER_X, TITLE_Y, Settings.GOLD_COLOR);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 484 */     String offlineModeNotice = "";
/* 485 */     if (TimeHelper.isOfflineMode()) {
/* 486 */       offlineModeNotice = TEXT[16];
/*     */     }
/*     */     
/* 489 */     if (TimeHelper.isTimeSet)
/*     */     {
/* 491 */       FontHelper.renderFontLeftDownAligned(sb, FontHelper.charDescFont, 
/*     */       
/*     */ 
/* 494 */         TimeHelper.getTodayDate() + offlineModeNotice, DATE_X, TITLE_Y, Color.SKY);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void renderTimeLeft(SpriteBatch sb)
/*     */   {
/* 502 */     FontHelper.renderFontLeftDownAligned(sb, FontHelper.charDescFont, TEXT[7] + 
/*     */     
/*     */ 
/* 505 */       TimeHelper.getTimeLeft(), BODY_X, TIME_LEFT_Y, Settings.CREAM_COLOR);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void renderCharacterAndNotice(SpriteBatch sb)
/*     */   {
/* 512 */     if (this.todaysChar == null) {
/* 513 */       return;
/*     */     }
/*     */     
/* 516 */     com.badlogic.gdx.graphics.Texture img = null;
/* 517 */     StringBuilder builder = new StringBuilder("#y");
/* 518 */     builder.append(TEXT_2[2]);
/* 519 */     builder.append(" NL ");
/*     */     
/* 521 */     switch (this.todaysChar) {
/*     */     case IRONCLAD: 
/* 523 */       img = ImageMaster.FILTER_IRONCLAD;
/* 524 */       if (this.lastDaily != TimeHelper.daySince1970()) {
/* 525 */         builder.append(com.megacrit.cardcrawl.characters.Ironclad.NAMES[0]);
/*     */       }
/*     */       break;
/*     */     case THE_SILENT: 
/* 529 */       img = ImageMaster.FILTER_SILENT;
/* 530 */       if (this.lastDaily != TimeHelper.daySince1970()) {
/* 531 */         builder.append(com.megacrit.cardcrawl.characters.TheSilent.NAMES[0]);
/*     */       }
/*     */       break;
/*     */     case DEFECT: 
/* 535 */       img = ImageMaster.FILTER_DEFECT;
/* 536 */       if (this.lastDaily != TimeHelper.daySince1970()) {
/* 537 */         builder.append(com.megacrit.cardcrawl.characters.Defect.NAMES[0]);
/*     */       }
/*     */       
/*     */       break;
/*     */     }
/*     */     
/*     */     
/* 544 */     if (img != null) {
/* 545 */       sb.draw(img, HEADER_X, CHAR_IMAGE_Y, 128.0F * Settings.scale, 128.0F * Settings.scale);
/*     */     }
/*     */     
/* 548 */     if (this.lastDaily == TimeHelper.daySince1970()) {
/* 549 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, TEXT[2], CHAR_X, CHAR_HEADER_Y, 9999.0F, MOD_LINE_SPACING, Settings.CREAM_COLOR);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 559 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, builder
/*     */       
/*     */ 
/* 562 */         .toString(), CHAR_X, CHAR_HEADER_Y, 9999.0F, MOD_LINE_SPACING, Settings.CREAM_COLOR);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderMods(SpriteBatch sb)
/*     */   {
/* 572 */     FontHelper.renderFont(sb, FontHelper.charDescFont, TEXT[13], HEADER_X, MOD_HEADER_Y, Settings.GOLD_COLOR);
/* 573 */     float y = MOD_HEADER_Y - MOD_SECTION_SPACING;
/*     */     
/* 575 */     for (AbstractDailyMod mod : DailyMods.enabledMods) {
/* 576 */       StringBuilder builder = new StringBuilder();
/*     */       
/* 578 */       if (mod.positive) {
/* 579 */         builder.append(FontHelper.colorString(mod.name, "g"));
/*     */       } else {
/* 581 */         builder.append(FontHelper.colorString(mod.name, "r"));
/*     */       }
/*     */       
/* 584 */       builder.append(": ");
/* 585 */       builder.append(mod.description);
/*     */       
/* 587 */       FontHelper.renderSmartText(sb, FontHelper.charDescFont, builder
/*     */       
/*     */ 
/* 590 */         .toString(), MOD_DESC_X, y, MOD_LINE_W, MOD_LINE_SPACING, Settings.CREAM_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 598 */       sb.draw(mod.img, MOD_ICON_X, y - 44.0F * Settings.scale, 64.0F * Settings.scale, 64.0F * Settings.scale);
/*     */       
/* 600 */       y += FontHelper.getSmartHeight(FontHelper.charDescFont, builder.toString(), MOD_LINE_W, MOD_LINE_SPACING) - MOD_SECTION_SPACING;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private void renderArrows(SpriteBatch sb)
/*     */   {
/* 607 */     boolean renderLeftArrow = true;
/*     */     
/* 609 */     if ((this.currentStartIndex != 1) && (renderLeftArrow)) {
/* 610 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer));
/* 611 */       sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 629 */       if (this.prevHb.hovered) {
/* 630 */         sb.setBlendFunction(770, 1);
/* 631 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer / 2.0F));
/* 632 */         sb.draw(ImageMaster.POPUP_ARROW, this.prevHb.cX - 128.0F, this.prevHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 649 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */       
/* 652 */       if (Settings.isControllerMode) {
/* 653 */         sb.draw(CInputActionSet.pageLeftViewDeck
/* 654 */           .getKeyImg(), this.prevHb.cX - 32.0F + 0.0F * Settings.scale, this.prevHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 672 */       this.prevHb.render(sb);
/*     */     }
/*     */     
/*     */ 
/* 676 */     if (this.entries.size() == 20) {
/* 677 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer));
/* 678 */       sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 696 */       if (this.nextHb.hovered) {
/* 697 */         sb.setBlendFunction(770, 1);
/* 698 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.lineFadeInTimer / 2.0F));
/* 699 */         sb.draw(ImageMaster.POPUP_ARROW, this.nextHb.cX - 128.0F, this.nextHb.cY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 0.75F, Settings.scale * 0.75F, 0.0F, 0, 0, 256, 256, true, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 716 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */       
/* 719 */       if (Settings.isControllerMode) {
/* 720 */         sb.draw(CInputActionSet.pageRightViewExhaust
/* 721 */           .getKeyImg(), this.nextHb.cX - 32.0F + 0.0F * Settings.scale, this.nextHb.cY - 32.0F - 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 739 */       this.nextHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderDisclaimer(SpriteBatch sb) {
/* 744 */     FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT[15], Settings.WIDTH * 0.26F, 80.0F * Settings.scale * this.lineFadeInTimer, new Color(1.0F, 0.3F, 0.3F, this.lineFadeInTimer));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderScoreHeaders(SpriteBatch sb)
/*     */   {
/* 754 */     Color creamColor = new Color(1.0F, 0.965F, 0.886F, this.lineFadeInTimer);
/*     */     
/*     */ 
/* 757 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.charTitleFont, TEXT[14], 960.0F * Settings.scale, 920.0F * Settings.scale, new Color(0.937F, 0.784F, 0.317F, this.lineFadeInTimer));
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 766 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT_2[6], RANK_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 774 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT_2[7], NAME_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 783 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, TEXT_2[8], SCORE_X, 860.0F * Settings.scale, creamColor);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 791 */     sb.setColor(creamColor);
/* 792 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 1138.0F * Settings.scale, 168.0F * Settings.scale, LINE_THICKNESS, 692.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 798 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 1480.0F * Settings.scale, 168.0F * Settings.scale, LINE_THICKNESS, 692.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 804 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.lineFadeInTimer * 0.75F));
/* 805 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 982.0F * Settings.scale, 814.0F * Settings.scale, 630.0F * Settings.scale, 16.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 811 */     sb.setColor(creamColor);
/* 812 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 982.0F * Settings.scale, 820.0F * Settings.scale, 630.0F * Settings.scale, LINE_THICKNESS);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderViewMyScoreBox(SpriteBatch sb)
/*     */   {
/* 821 */     if (this.waiting) {
/* 822 */       return;
/*     */     }
/*     */     
/* 825 */     if (this.viewMyScoreHb.hovered) {
/* 826 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_2[5], 1310.0F * Settings.scale, 80.0F * Settings.scale, Settings.GREEN_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 834 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_N, TEXT_2[5], 1310.0F * Settings.scale, 80.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 843 */     if (Settings.isControllerMode) {
/* 844 */       sb.draw(CInputActionSet.topPanel
/* 845 */         .getKeyImg(), Settings.WIDTH / 2.0F - 32.0F + 210.0F * Settings.scale, -32.0F + 80.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 863 */     this.viewMyScoreHb.render(sb);
/*     */   }
/*     */   
/*     */   private void renderScores(SpriteBatch sb) {
/* 867 */     if (!this.waiting) {
/* 868 */       if (this.entries.isEmpty()) {
/* 869 */         FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT_2[12], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/* 877 */         for (int i = 0; i < this.entries.size(); i++) {
/* 878 */           ((LeaderboardEntry)this.entries.get(i)).render(sb, i);
/*     */         }
/*     */       }
/*     */     }
/* 882 */     else if (SteamSaveSync.steamUser == null) {
/* 883 */       FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT_2[11], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.RED_TEXT_COLOR);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 891 */       FontHelper.renderFontCentered(sb, FontHelper.eventBodyText, TEXT_2[9], 1300.0F * Settings.scale, 540.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\DailyScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */