/*    */ package com.megacrit.cardcrawl.daily;
/*    */ 
/*    */ import com.megacrit.cardcrawl.daily.mods.AbstractDailyMod;
/*    */ import com.megacrit.cardcrawl.helpers.ModHelper;
/*    */ import java.util.ArrayList;
/*    */ import java.util.HashMap;
/*    */ import java.util.List;
/*    */ import java.util.Map.Entry;
/*    */ 
/*    */ public class DailyMods
/*    */ {
/* 12 */   public static HashMap<String, Boolean> negativeMods = new HashMap();
/* 13 */   public static HashMap<String, Boolean> cardMods = new HashMap();
/* 14 */   public static ArrayList<AbstractDailyMod> enabledMods = new ArrayList();
/*    */   
/*    */   public void initialize(long daysSince1970) {
/* 17 */     setModsFalse();
/* 18 */     negativeMods = ModHelper.getNegativeMods(daysSince1970);
/* 19 */     cardMods = ModHelper.getCardMods(daysSince1970);
/* 20 */     updateEnabledModList();
/*    */   }
/*    */   
/*    */   public boolean areEnabled() {
/* 24 */     return !enabledMods.isEmpty();
/*    */   }
/*    */   
/*    */   private void updateEnabledModList() {
/* 28 */     enabledMods.clear();
/* 29 */     for (Map.Entry<String, Boolean> m : cardMods.entrySet()) {
/* 30 */       if (((Boolean)m.getValue()).booleanValue()) {
/* 31 */         enabledMods.add(ModHelper.getCardMod((String)m.getKey()));
/*    */       }
/*    */     }
/*    */     
/* 35 */     for (Map.Entry<String, Boolean> m : negativeMods.entrySet()) {
/* 36 */       if (((Boolean)m.getValue()).booleanValue()) {
/* 37 */         enabledMods.add(ModHelper.getNegativeMod((String)m.getKey()));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public ArrayList<String> getEnabledModIDs() {
/* 43 */     ArrayList<String> enabled = new ArrayList();
/* 44 */     for (AbstractDailyMod m : enabledMods) {
/* 45 */       enabled.add(m.modID);
/*    */     }
/* 47 */     return enabled;
/*    */   }
/*    */   
/*    */   public static void setModsFalse() {
/* 51 */     enabledMods.clear();
/* 52 */     cardMods = ModHelper.noPositiveMods();
/* 53 */     negativeMods = ModHelper.noNegativeMods();
/*    */   }
/*    */   
/*    */   public void setMods(List<String> modIDs) {
/* 57 */     setModsFalse();
/* 58 */     negativeMods = ModHelper.getNegativeMods(modIDs);
/* 59 */     cardMods = ModHelper.getCardMods(modIDs);
/* 60 */     updateEnabledModList();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\daily\DailyMods.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */