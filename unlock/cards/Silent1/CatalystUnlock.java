/*    */ package com.megacrit.cardcrawl.unlock.cards.Silent1;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.unlock.AbstractUnlock.UnlockType;
/*    */ 
/*    */ public class CatalystUnlock extends com.megacrit.cardcrawl.unlock.AbstractUnlock
/*    */ {
/*    */   public CatalystUnlock()
/*    */   {
/* 10 */     this.type = AbstractUnlock.UnlockType.CARD;
/* 11 */     this.card = com.megacrit.cardcrawl.helpers.CardLibrary.getCard("Catalyst");
/* 12 */     this.key = this.card.cardID;
/* 13 */     this.title = this.card.name;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\cards\Silent1\CatalystUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */