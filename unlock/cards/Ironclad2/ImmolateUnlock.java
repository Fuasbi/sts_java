/*    */ package com.megacrit.cardcrawl.unlock.cards.Ironclad2;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.unlock.AbstractUnlock.UnlockType;
/*    */ 
/*    */ public class ImmolateUnlock extends com.megacrit.cardcrawl.unlock.AbstractUnlock
/*    */ {
/*    */   public ImmolateUnlock()
/*    */   {
/* 10 */     this.type = AbstractUnlock.UnlockType.CARD;
/* 11 */     this.card = com.megacrit.cardcrawl.helpers.CardLibrary.getCard("Immolate");
/* 12 */     this.key = this.card.cardID;
/* 13 */     this.title = this.card.name;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\cards\Ironclad2\ImmolateUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */