/*    */ package com.megacrit.cardcrawl.unlock.relics.relic2;
/*    */ 
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import com.megacrit.cardcrawl.unlock.AbstractUnlock.UnlockType;
/*    */ 
/*    */ public class CourierUnlock extends com.megacrit.cardcrawl.unlock.AbstractUnlock
/*    */ {
/*    */   public CourierUnlock()
/*    */   {
/* 10 */     this.type = AbstractUnlock.UnlockType.RELIC;
/* 11 */     this.relic = com.megacrit.cardcrawl.helpers.RelicLibrary.getRelic("The Courier");
/* 12 */     this.key = this.relic.relicId;
/* 13 */     this.title = this.relic.name;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\relics\relic2\CourierUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */