/*    */ package com.megacrit.cardcrawl.unlock.relics.defectrelics;
/*    */ 
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class EmotionChipUnlock extends com.megacrit.cardcrawl.unlock.AbstractUnlock
/*    */ {
/*    */   public EmotionChipUnlock()
/*    */   {
/*  9 */     this.type = com.megacrit.cardcrawl.unlock.AbstractUnlock.UnlockType.RELIC;
/* 10 */     this.relic = com.megacrit.cardcrawl.helpers.RelicLibrary.getRelic("Emotion Chip");
/* 11 */     this.key = this.relic.relicId;
/* 12 */     this.title = this.relic.name;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\relics\defectrelics\EmotionChipUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */