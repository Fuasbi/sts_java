/*    */ package com.megacrit.cardcrawl.unlock;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class AbstractUnlock implements Comparable<AbstractUnlock>
/*    */ {
/*    */   public String title;
/*    */   public String key;
/*    */   public UnlockType type;
/* 11 */   public com.megacrit.cardcrawl.characters.AbstractPlayer player = null;
/* 12 */   public AbstractCard card = null;
/* 13 */   public AbstractRelic relic = null;
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */   
/* 16 */   public static enum UnlockType { CARD,  RELIC,  LOADOUT,  CHARACTER,  MISC;
/*    */     
/*    */ 
/*    */     private UnlockType() {}
/*    */   }
/*    */   
/*    */   public int compareTo(AbstractUnlock u)
/*    */   {
/* 24 */     switch (this.type) {
/*    */     case CARD: 
/* 26 */       return this.card.cardID.compareTo(u.card.cardID);
/*    */     case RELIC: 
/* 28 */       return this.relic.relicId.compareTo(u.relic.relicId);
/*    */     }
/* 30 */     return this.title.compareTo(u.title);
/*    */   }
/*    */   
/*    */   public void onUnlockScreenOpen() {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\AbstractUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */