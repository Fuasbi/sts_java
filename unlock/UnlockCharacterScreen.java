/*     */ package com.megacrit.cardcrawl.unlock;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.ui.buttons.DynamicBanner;
/*     */ import com.megacrit.cardcrawl.ui.buttons.UnlockConfirmButton;
/*     */ import com.megacrit.cardcrawl.vfx.ConeEffect;
/*     */ import com.megacrit.cardcrawl.vfx.RoomShineEffect;
/*     */ import com.megacrit.cardcrawl.vfx.RoomShineEffect2;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class UnlockCharacterScreen
/*     */ {
/*  26 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("UnlockCharacterScreen");
/*  27 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public AbstractUnlock unlock;
/*  30 */   private ArrayList<ConeEffect> cones = new ArrayList();
/*     */   
/*     */   private static final int CONE_AMT = 30;
/*  33 */   private float shinyTimer = 0.0F;
/*     */   private static final float SHINY_INTERVAL = 0.2F;
/*  35 */   public UnlockConfirmButton button = new UnlockConfirmButton();
/*     */   public long id;
/*     */   
/*     */   public void open(AbstractUnlock unlock)
/*     */   {
/*  40 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.UNLOCK;
/*  41 */     this.unlock = unlock;
/*  42 */     this.id = CardCrawlGame.sound.play("UNLOCK_SCREEN");
/*     */     
/*  44 */     this.button.show();
/*     */     
/*  46 */     this.cones.clear();
/*  47 */     for (int i = 0; i < 30; i++) {
/*  48 */       this.cones.add(new ConeEffect());
/*     */     }
/*     */     
/*  51 */     unlock.onUnlockScreenOpen();
/*  52 */     UnlockTracker.hardUnlockOverride(unlock.key);
/*  53 */     UnlockTracker.lockedCharacters.remove(unlock.key);
/*  54 */     AbstractDungeon.dynamicBanner.appearInstantly(TEXT[3]);
/*     */   }
/*     */   
/*     */   public void update() {
/*  58 */     if (InputHelper.justClickedRight) {
/*  59 */       this.button.show();
/*     */     }
/*  61 */     this.shinyTimer -= Gdx.graphics.getDeltaTime();
/*  62 */     if (this.shinyTimer < 0.0F) {
/*  63 */       this.shinyTimer = 0.2F;
/*  64 */       AbstractDungeon.topLevelEffects.add(new RoomShineEffect());
/*  65 */       AbstractDungeon.topLevelEffects.add(new RoomShineEffect());
/*  66 */       AbstractDungeon.topLevelEffects.add(new RoomShineEffect2());
/*     */     }
/*     */     
/*  69 */     updateConeEffect();
/*  70 */     this.unlock.player.update();
/*  71 */     this.button.update();
/*     */   }
/*     */   
/*     */   private void updateConeEffect() {
/*  75 */     for (Iterator<ConeEffect> e = this.cones.iterator(); e.hasNext();) {
/*  76 */       ConeEffect d = (ConeEffect)e.next();
/*  77 */       d.update();
/*  78 */       if (d.isDone) {
/*  79 */         e.remove();
/*     */       }
/*     */     }
/*     */     
/*  83 */     if (this.cones.size() < 30) {
/*  84 */       this.cones.add(new ConeEffect());
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/*  89 */     sb.setColor(new Color(0.1F, 0.2F, 0.25F, 1.0F));
/*  90 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/*  92 */     sb.setBlendFunction(770, 1);
/*  93 */     for (ConeEffect e : this.cones) {
/*  94 */       e.render(sb);
/*     */     }
/*     */     
/*  97 */     sb.setBlendFunction(770, 771);
/*  98 */     this.unlock.render(sb);
/*  99 */     this.unlock.player.renderPlayerImage(sb);
/* 100 */     this.button.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\UnlockCharacterScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */