/*     */ package com.megacrit.cardcrawl.unlock;
/*     */ 
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.stats.AchievementGrid;
/*     */ import com.megacrit.cardcrawl.screens.stats.AchievementItem;
/*     */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*     */ import com.megacrit.cardcrawl.steam.SteamSaveSync;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.EchoFormUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.HyperbeamUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.MeteorStrikeUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.NovaUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.ReboundUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.RecycleUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.SunderUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.TurboUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.DefectCards.UndoUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad1.HeavyBladeUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad1.LimitBreakUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad1.SpotWeaknessUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad2.EvolveUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad2.ImmolateUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad2.WildStrikeUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad3.ExhumeUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad3.HavocUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Ironclad3.SentinelUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent1.BaneUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent1.CatalystUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent1.CorpseExplosionUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent3.AccuracyUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent3.CloakAndDaggerUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent3.StormOfSteelUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent4.ConcentrateUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent4.GrandFinaleUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.cards.Silent4.SetupUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.CablesUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.DataDiskUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.EmotionChipUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.RunicCapacitorUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.TurnipUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.defectrelics.VirusUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic1.OmamoriUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic1.PrayerWheelUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic1.ShovelUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic2.ArtOfWarUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic2.CourierUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic2.PandorasBoxUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic3.BlueCandleUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic3.DeadBranchUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic3.SingingBowlUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic4.DuvuDollUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic4.SmilingMaskUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.relics.relic4.TinyChestUnlock;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Set;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UnlockTracker
/*     */ {
/* 117 */   private static final Logger logger = LogManager.getLogger(UnlockTracker.class.getName());
/*     */   public static Prefs unlockPref;
/* 119 */   public static Prefs seenPref; public static Prefs bossSeenPref; public static Prefs relicSeenPref; public static Prefs achievementPref; public static Prefs unlockProgress; public static HashMap<String, String> unlockReqs = new HashMap();
/* 120 */   public static ArrayList<String> lockedCards = new ArrayList();
/* 121 */   public static ArrayList<String> lockedCharacters = new ArrayList();
/* 122 */   public static ArrayList<String> lockedLoadouts = new ArrayList();
/* 123 */   public static ArrayList<String> lockedRelics = new ArrayList();
/*     */   
/*     */ 
/* 126 */   public static HashMap<String, AbstractUnlock> cardUnlocks = new HashMap();
/* 127 */   public static HashMap<String, AbstractUnlock> relicUnlocks = new HashMap();
/*     */   
/*     */   public static int lockedRedCardCount;
/*     */   
/*     */   public static int unlockedRedCardCount;
/*     */   public static int lockedGreenCardCount;
/*     */   public static int unlockedGreenCardCount;
/*     */   public static int lockedBlueCardCount;
/*     */   public static int unlockedBlueCardCount;
/*     */   
/*     */   public static void initialize()
/*     */   {
/* 139 */     achievementPref = SaveHelper.getPrefs("STSAchievements");
/* 140 */     unlockPref = SaveHelper.getPrefs("STSUnlocks");
/* 141 */     unlockProgress = SaveHelper.getPrefs("STSUnlockProgress");
/* 142 */     seenPref = SaveHelper.getPrefs("STSSeenCards");
/* 143 */     bossSeenPref = SaveHelper.getPrefs("STSSeenBosses");
/* 144 */     relicSeenPref = SaveHelper.getPrefs("STSSeenRelics");
/* 145 */     refresh();
/*     */   }
/*     */   
/*     */   public static void retroactiveUnlock() {
/* 149 */     logger.info("Retroactive Unlocks: ");
/* 150 */     AbstractPlayer.PlayerClass c = AbstractPlayer.PlayerClass.IRONCLAD;
/* 151 */     int lvl = unlockProgress.getInteger(c.toString() + "UnlockLevel", 0);
/* 152 */     logger.info(c.toString() + " unlock level: " + lvl);
/*     */     
/* 154 */     while (lvl != 0) {
/* 155 */       ArrayList<AbstractUnlock> list = getUnlockBundle(c, lvl - 1);
/* 156 */       for (AbstractUnlock u : list) {
/* 157 */         hardUnlockOverride(u.key);
/*     */       }
/* 159 */       lvl--;
/*     */     }
/*     */     
/* 162 */     c = AbstractPlayer.PlayerClass.THE_SILENT;
/* 163 */     lvl = unlockProgress.getInteger(c.toString() + "UnlockLevel", 0);
/* 164 */     logger.info(c.toString() + " unlock level: " + lvl);
/*     */     
/* 166 */     while (lvl != 0) {
/* 167 */       ArrayList<AbstractUnlock> list = getUnlockBundle(c, lvl - 1);
/* 168 */       for (AbstractUnlock u : list) {
/* 169 */         hardUnlockOverride(u.key);
/*     */       }
/* 171 */       lvl--;
/*     */     }
/*     */     
/* 174 */     c = AbstractPlayer.PlayerClass.DEFECT;
/* 175 */     lvl = unlockProgress.getInteger(c.toString() + "UnlockLevel", 0);
/* 176 */     logger.info(c.toString() + " unlock level: " + lvl);
/*     */     
/* 178 */     while (lvl != 0) {
/* 179 */       ArrayList<AbstractUnlock> list = getUnlockBundle(c, lvl - 1);
/* 180 */       for (AbstractUnlock u : list) {
/* 181 */         hardUnlockOverride(u.key);
/*     */       }
/* 183 */       lvl--;
/*     */     }
/*     */   }
/*     */   
/*     */   public static void refresh() {
/* 188 */     lockedCards.clear();
/* 189 */     lockedCharacters.clear();
/* 190 */     lockedLoadouts.clear();
/* 191 */     lockedRelics.clear();
/*     */     
/* 193 */     addCard("Havoc");
/* 194 */     addCard("Sentinel");
/* 195 */     addCard("Exhume");
/*     */     
/* 197 */     addCard("Wild Strike");
/* 198 */     addCard("Evolve");
/* 199 */     addCard("Immolate");
/*     */     
/* 201 */     addCard("Heavy Blade");
/* 202 */     addCard("Spot Weakness");
/* 203 */     addCard("Limit Break");
/*     */     
/*     */ 
/* 206 */     addCard("Concentrate");
/* 207 */     addCard("Setup");
/* 208 */     addCard("Grand Finale");
/*     */     
/* 210 */     addCard("Cloak And Dagger");
/* 211 */     addCard("Accuracy");
/* 212 */     addCard("Storm of Steel");
/*     */     
/* 214 */     addCard("Bane");
/* 215 */     addCard("Catalyst");
/* 216 */     addCard("Corpse Explosion");
/*     */     
/*     */ 
/* 219 */     addCard("Rebound");
/* 220 */     addCard("Undo");
/* 221 */     addCard("Echo Form");
/*     */     
/* 223 */     addCard("Turbo");
/* 224 */     addCard("Sunder");
/* 225 */     addCard("Meteor Strike");
/*     */     
/* 227 */     addCard("Hyperbeam");
/* 228 */     addCard("Recycle");
/* 229 */     addCard("Core Surge");
/*     */     
/*     */ 
/* 232 */     addCharacter("The Silent");
/* 233 */     addCharacter("Defect");
/*     */     
/*     */ 
/* 236 */     addRelic("Omamori");
/* 237 */     addRelic("Prayer Wheel");
/* 238 */     addRelic("Shovel");
/*     */     
/* 240 */     addRelic("Art of War");
/* 241 */     addRelic("The Courier");
/* 242 */     addRelic("Pandora's Box");
/*     */     
/* 244 */     addRelic("Blue Candle");
/* 245 */     addRelic("Dead Branch");
/* 246 */     addRelic("Singing Bowl");
/*     */     
/* 248 */     addRelic("Du-Vu Doll");
/* 249 */     addRelic("Smiling Mask");
/* 250 */     addRelic("Tiny Chest");
/*     */     
/* 252 */     addRelic("Cables");
/* 253 */     addRelic("DataDisk");
/* 254 */     addRelic("Emotion Chip");
/*     */     
/* 256 */     addRelic("Runic Capacitor");
/* 257 */     addRelic("Turnip");
/* 258 */     addRelic("Symbiotic Virus");
/*     */     
/* 260 */     countUnlockedCards();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int incrementUnlockRamp(int currentCost)
/*     */   {
/* 270 */     switch (currentCost) {
/*     */     case 300: 
/* 272 */       return 750;
/*     */     
/*     */     case 500: 
/* 275 */       return 1000;
/*     */     case 750: 
/* 277 */       return 1000;
/*     */     case 1000: 
/* 279 */       return 1500;
/*     */     case 1500: 
/* 281 */       return 2000;
/*     */     case 2000: 
/* 283 */       return 2500;
/*     */     case 2500: 
/* 285 */       return 3000;
/*     */     case 3000: 
/* 287 */       return 3000;
/*     */     case 4000: 
/* 289 */       return 4000;
/*     */     }
/* 291 */     return currentCost + 250;
/*     */   }
/*     */   
/*     */   public static void resetUnlockProgress(AbstractPlayer.PlayerClass c)
/*     */   {
/* 296 */     unlockProgress.putInteger(c.toString() + "UnlockLevel", 0);
/* 297 */     unlockProgress.putInteger(c.toString() + "Progress", 0);
/* 298 */     unlockProgress.putInteger(c.toString() + "CurrentCost", 300);
/* 299 */     unlockProgress.putInteger(c.toString() + "TotalScore", 0);
/* 300 */     unlockProgress.putInteger(c.toString() + "HighScore", 0);
/*     */   }
/*     */   
/*     */   public static int getUnlockLevel(AbstractPlayer.PlayerClass c) {
/* 304 */     return unlockProgress.getInteger(c.toString() + "UnlockLevel", 0);
/*     */   }
/*     */   
/*     */   public static int getCurrentProgress(AbstractPlayer.PlayerClass c) {
/* 308 */     return unlockProgress.getInteger(c.toString() + "Progress", 0);
/*     */   }
/*     */   
/*     */   public static int getCurrentScoreCost(AbstractPlayer.PlayerClass c) {
/* 312 */     return unlockProgress.getInteger(c.toString() + "CurrentCost", 300);
/*     */   }
/*     */   
/*     */   public static int getCurrentScoreTotal(AbstractPlayer.PlayerClass c) {
/* 316 */     return unlockProgress.getInteger(c.toString() + "TotalScore", 0);
/*     */   }
/*     */   
/*     */   public static int getHighscore(AbstractPlayer.PlayerClass c) {
/* 320 */     return unlockProgress.getInteger(c.toString() + "HighScore", 0);
/*     */   }
/*     */   
/*     */   public static int seenRedCardCount;
/*     */   public static int seenGreenCardCount;
/*     */   public static int seenBlueCardCount;
/*     */   public static int lockedRelicCount;
/*     */   public static int unlockedRelicCount;
/*     */   private static final int STARTING_UNLOCK_COST = 300;
/*     */   public static void addScore(AbstractPlayer.PlayerClass c, int scoreGained)
/*     */   {
/* 331 */     String key_unlock_level = c.toString() + "UnlockLevel";
/* 332 */     String key_progress = c.toString() + "Progress";
/* 333 */     String key_current_cost = c.toString() + "CurrentCost";
/* 334 */     String key_total_score = c.toString() + "TotalScore";
/* 335 */     String key_high_score = c.toString() + "HighScore";
/* 336 */     logger.info("Keys");
/* 337 */     logger.info(key_unlock_level);
/* 338 */     logger.info(key_progress);
/* 339 */     logger.info(key_current_cost);
/* 340 */     logger.info(key_total_score);
/* 341 */     logger.info(key_high_score);
/*     */     
/*     */ 
/* 344 */     int p = unlockProgress.getInteger(key_progress, 0);
/* 345 */     p += scoreGained;
/*     */     
/* 347 */     if (p >= unlockProgress.getInteger(key_current_cost, 300)) {
/* 348 */       logger.info("[DEBUG] Level up!");
/*     */       
/*     */ 
/* 351 */       int lvl = unlockProgress.getInteger(key_unlock_level, 0);
/* 352 */       lvl++;
/* 353 */       unlockProgress.putInteger(key_unlock_level, lvl);
/*     */       
/*     */ 
/* 356 */       p -= unlockProgress.getInteger(key_current_cost, 300);
/* 357 */       unlockProgress.putInteger(key_progress, p);
/* 358 */       logger.info("[DEBUG] Score Progress: " + key_progress);
/*     */       
/*     */ 
/* 361 */       int current_cost = unlockProgress.getInteger(key_current_cost, 300);
/* 362 */       unlockProgress.putInteger(key_current_cost, incrementUnlockRamp(current_cost));
/*     */       
/* 364 */       if (p > unlockProgress.getInteger(key_current_cost, 300)) {
/* 365 */         unlockProgress.putInteger(key_progress, unlockProgress
/*     */         
/* 367 */           .getInteger(key_current_cost, 300) - 1);
/* 368 */         logger.info("Overfloat maxes out next level");
/*     */       }
/*     */     }
/*     */     else {
/* 372 */       unlockProgress.putInteger(key_progress, p);
/*     */     }
/*     */     
/*     */ 
/* 376 */     int total = unlockProgress.getInteger(key_total_score, 0);
/* 377 */     total += scoreGained;
/* 378 */     unlockProgress.putInteger(key_total_score, total);
/* 379 */     logger.info("[DEBUG] Total score: " + total);
/*     */     
/*     */ 
/* 382 */     int highscore = unlockProgress.getInteger(key_high_score, 0);
/* 383 */     if (scoreGained > highscore) {
/* 384 */       unlockProgress.putInteger(key_high_score, scoreGained);
/* 385 */       logger.info("[DEBUG] New high score: " + scoreGained);
/*     */     }
/*     */     
/*     */ 
/* 389 */     unlockProgress.flush();
/*     */   }
/*     */   
/*     */   public static void countUnlockedCards() {
/* 393 */     ArrayList<String> tmp = new ArrayList();
/* 394 */     int count = 0;
/*     */     
/*     */ 
/*     */ 
/* 398 */     tmp.add("Havoc");
/* 399 */     tmp.add("Sentinel");
/* 400 */     tmp.add("Exhume");
/*     */     
/* 402 */     tmp.add("Wild Strike");
/* 403 */     tmp.add("Evolve");
/* 404 */     tmp.add("Immolate");
/*     */     
/* 406 */     tmp.add("Heavy Blade");
/* 407 */     tmp.add("Spot Weakness");
/* 408 */     tmp.add("Limit Break");
/*     */     
/* 410 */     for (String s : tmp) {
/* 411 */       if (!isCardLocked(s)) {
/* 412 */         count++;
/*     */       }
/*     */     }
/*     */     
/* 416 */     lockedRedCardCount = tmp.size();
/* 417 */     unlockedRedCardCount = count;
/* 418 */     tmp.clear();
/* 419 */     count = 0;
/*     */     
/*     */ 
/* 422 */     tmp.add("Concentrate");
/* 423 */     tmp.add("Setup");
/* 424 */     tmp.add("Grand Finale");
/*     */     
/* 426 */     tmp.add("Cloak And Dagger");
/* 427 */     tmp.add("Accuracy");
/* 428 */     tmp.add("Storm of Steel");
/*     */     
/* 430 */     tmp.add("Bane");
/* 431 */     tmp.add("Catalyst");
/* 432 */     tmp.add("Corpse Explosion");
/*     */     
/* 434 */     for (String s : tmp) {
/* 435 */       if (!isCardLocked(s)) {
/* 436 */         count++;
/*     */       }
/*     */     }
/*     */     
/* 440 */     lockedGreenCardCount = tmp.size();
/* 441 */     unlockedGreenCardCount = count;
/* 442 */     tmp.clear();
/* 443 */     count = 0;
/*     */     
/*     */ 
/* 446 */     tmp.add("Rebound");
/* 447 */     tmp.add("Undo");
/* 448 */     tmp.add("Echo Form");
/*     */     
/* 450 */     tmp.add("Turbo");
/* 451 */     tmp.add("Sunder");
/* 452 */     tmp.add("Meteor Strike");
/*     */     
/* 454 */     tmp.add("Hyperbeam");
/* 455 */     tmp.add("Recycle");
/* 456 */     tmp.add("Core Surge");
/*     */     
/* 458 */     for (String s : tmp) {
/* 459 */       if (!isCardLocked(s)) {
/* 460 */         count++;
/*     */       }
/*     */     }
/*     */     
/* 464 */     lockedBlueCardCount = tmp.size();
/* 465 */     unlockedBlueCardCount = count;
/* 466 */     tmp.clear();
/* 467 */     count = 0;
/*     */     
/*     */ 
/*     */ 
/* 471 */     tmp.add("Omamori");
/* 472 */     tmp.add("Prayer Wheel");
/* 473 */     tmp.add("Shovel");
/*     */     
/* 475 */     tmp.add("Art of War");
/* 476 */     tmp.add("The Courier");
/* 477 */     tmp.add("Pandora's Box");
/*     */     
/* 479 */     tmp.add("Blue Candle");
/* 480 */     tmp.add("Dead Branch");
/* 481 */     tmp.add("Singing Bowl");
/*     */     
/* 483 */     tmp.add("Du-Vu Doll");
/* 484 */     tmp.add("Smiling Mask");
/* 485 */     tmp.add("Tiny Chest");
/*     */     
/*     */ 
/* 488 */     tmp.add("Cables");
/* 489 */     tmp.add("DataDisk");
/* 490 */     tmp.add("Emotion Chip");
/* 491 */     tmp.add("Runic Capacitor");
/* 492 */     tmp.add("Turnip");
/* 493 */     tmp.add("Symbiotic Virus");
/*     */     
/* 495 */     for (String s : tmp) {
/* 496 */       if (!isRelicLocked(s)) {
/* 497 */         count++;
/*     */       }
/*     */     }
/*     */     
/* 501 */     lockedRelicCount = tmp.size();
/* 502 */     unlockedRelicCount = count;
/*     */     
/* 504 */     logger.info("RED UNLOCKS:   " + unlockedRedCardCount + "/" + lockedRedCardCount);
/* 505 */     logger.info("GREEN UNLOCKS: " + unlockedGreenCardCount + "/" + lockedGreenCardCount);
/* 506 */     logger.info("BLUE UNLOCKS: " + unlockedBlueCardCount + "/" + lockedBlueCardCount);
/* 507 */     logger.info("RELIC UNLOCKS: " + unlockedRelicCount + "/" + lockedRelicCount);
/* 508 */     logger.info("CARDS SEEN:    " + seenPref.get().keySet().size() + "/" + CardLibrary.totalCardCount);
/* 509 */     logger.info("RELICS SEEN:   " + relicSeenPref.get().keySet().size() + "/" + RelicLibrary.totalRelicCount);
/*     */   }
/*     */   
/*     */   public static String getCardsSeenString() {
/* 513 */     return CardLibrary.seenRedCards + CardLibrary.seenGreenCards + CardLibrary.seenBlueCards + CardLibrary.seenColorlessCards + CardLibrary.seenCurseCards + "/" + CardLibrary.totalCardCount;
/*     */   }
/*     */   
/*     */   public static String getRelicsSeenString()
/*     */   {
/* 518 */     return RelicLibrary.seenRelics + "/" + RelicLibrary.totalRelicCount;
/*     */   }
/*     */   
/*     */   public static void addCard(String key) {
/* 522 */     if (unlockPref.getString(key).equals("true")) {
/* 523 */       unlockPref.putInteger(key, 2);
/* 524 */       logger.info("Converting " + key + " from bool to int");
/* 525 */       unlockPref.flush();
/* 526 */     } else if (unlockPref.getString(key).equals("false")) {
/* 527 */       unlockPref.putInteger(key, 0);
/* 528 */       logger.info("Converting " + key + " from bool to int");
/* 529 */       unlockPref.flush();
/*     */     }
/*     */     
/* 532 */     if (unlockPref.getInteger(key, 0) != 2) {
/* 533 */       lockedCards.add(key);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void addCharacter(String key) {
/* 538 */     if (unlockPref.getString(key).equals("true")) {
/* 539 */       unlockPref.putInteger(key, 2);
/* 540 */       logger.info("Converting " + key + " from bool to int");
/* 541 */       unlockPref.flush();
/* 542 */     } else if (unlockPref.getString(key).equals("false")) {
/* 543 */       unlockPref.putInteger(key, 0);
/* 544 */       logger.info("Converting " + key + " from bool to int");
/* 545 */       unlockPref.flush();
/*     */     }
/*     */     
/* 548 */     if (unlockPref.getInteger(key, 0) != 2) {
/* 549 */       lockedCharacters.add(key);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void addRelic(String key) {
/* 554 */     if (unlockPref.getInteger(key, 0) != 2) {
/* 555 */       lockedRelics.add(key);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void unlockAchievement(String key)
/*     */   {
/* 565 */     if ((Settings.isShowBuild) || (Settings.seedSet) || (!Settings.isStandardRun())) {
/* 566 */       return;
/*     */     }
/*     */     
/* 569 */     SteamSaveSync.unlockAchievement(key);
/*     */     
/* 571 */     if (!achievementPref.getBoolean(key, false)) {
/* 572 */       achievementPref.putBoolean(key, true);
/* 573 */       achievementPref.flush();
/* 574 */       logger.info("Achievement Unlocked: " + key);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void unlockLuckyDay() {
/* 579 */     String key = "LUCKY_DAY";
/* 580 */     SteamSaveSync.unlockAchievement(key);
/*     */     
/* 582 */     if (!achievementPref.getBoolean(key, false)) {
/* 583 */       achievementPref.putBoolean(key, true);
/* 584 */       achievementPref.flush();
/* 585 */       logger.info("Achievement Unlocked: " + key);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void hardUnlock(String key)
/*     */   {
/* 595 */     if (Settings.isShowBuild) {
/* 596 */       return;
/*     */     }
/*     */     
/* 599 */     if (unlockPref.getInteger(key, 0) == 1) {
/* 600 */       unlockPref.putInteger(key, 2);
/* 601 */       unlockPref.flush();
/* 602 */       logger.info("Hard Unlock: " + key);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void hardUnlockOverride(String key)
/*     */   {
/* 612 */     if (Settings.isShowBuild) {
/* 613 */       return;
/*     */     }
/*     */     
/* 616 */     unlockPref.putInteger(key, 2);
/* 617 */     unlockPref.flush();
/* 618 */     logger.info("Hard Unlock: " + key);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isCardLocked(String key)
/*     */   {
/* 628 */     return lockedCards.contains(key);
/*     */   }
/*     */   
/*     */   public static void unlockCard(String key) {
/* 632 */     seenPref.putInteger(key, 1);
/* 633 */     seenPref.flush();
/* 634 */     unlockPref.putInteger(key, 2);
/* 635 */     unlockPref.flush();
/* 636 */     lockedCards.remove(key);
/*     */     
/* 638 */     if (CardLibrary.getCard(key) != null) {
/* 639 */       CardLibrary.getCard(key).isSeen = true;
/* 640 */       CardLibrary.getCard(key).unlock();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isCharacterLocked(String key)
/*     */   {
/* 651 */     if ((key.equals("The Silent")) && (Settings.isDemo)) {
/* 652 */       return false;
/*     */     }
/* 654 */     return lockedCharacters.contains(key);
/*     */   }
/*     */   
/*     */   public static boolean isAscensionUnlocked(AbstractPlayer.PlayerClass chosenClass)
/*     */   {
/* 659 */     int victories = StatsScreen.getVictory(chosenClass);
/*     */     
/* 661 */     if (victories > 0)
/*     */     {
/* 663 */       if (!achievementPref.getBoolean("ASCEND_0", false)) {
/* 664 */         unlockAchievement("ASCEND_0");
/*     */       }
/*     */       
/*     */ 
/* 668 */       if (!achievementPref.getBoolean("ASCEND_10", false)) {
/* 669 */         StatsScreen.retroactiveAscend10Unlock();
/*     */       }
/*     */       
/*     */ 
/* 673 */       if (!achievementPref.getBoolean("ASCEND_20", false)) {
/* 674 */         StatsScreen.retroactiveAscend20Unlock();
/*     */       }
/* 676 */       return true;
/*     */     }
/*     */     
/* 679 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isRelicLocked(String key)
/*     */   {
/* 689 */     return lockedRelics.contains(key);
/*     */   }
/*     */   
/*     */   public static void markCardAsSeen(String key) {
/* 693 */     if ((CardLibrary.getCard(key) != null) && (!CardLibrary.getCard(key).isSeen)) {
/* 694 */       CardLibrary.getCard(key).isSeen = true;
/* 695 */       seenPref.putInteger(key, 1);
/* 696 */       seenPref.flush();
/*     */     } else {
/* 698 */       logger.info("Already seen: " + key);
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean isCardSeen(String key) {
/* 703 */     return seenPref.getInteger(key, 0) != 0;
/*     */   }
/*     */   
/*     */   public static void markRelicAsSeen(String key) {
/* 707 */     if ((RelicLibrary.getRelic(key) != null) && (!RelicLibrary.getRelic(key).isSeen)) {
/* 708 */       RelicLibrary.getRelic(key).isSeen = true;
/* 709 */       relicSeenPref.putInteger(key, 1);
/* 710 */       relicSeenPref.flush();
/*     */     } else {
/* 712 */       logger.info("Already seen: " + key);
/*     */     }
/*     */   }
/*     */   
/*     */   public static boolean isRelicSeen(String key) {
/* 717 */     return relicSeenPref.getInteger(key, 0) == 1;
/*     */   }
/*     */   
/*     */   public static void markBossAsSeen(String originalName) {
/* 721 */     bossSeenPref.putInteger(originalName, 1);
/* 722 */     bossSeenPref.flush();
/*     */   }
/*     */   
/*     */   public static boolean isBossSeen(String key) {
/* 726 */     if (bossSeenPref.getInteger(key, 0) == 1) {
/* 727 */       return true;
/*     */     }
/* 729 */     return false;
/*     */   }
/*     */   
/*     */   public static ArrayList<AbstractUnlock> getUnlockBundle(AbstractPlayer.PlayerClass c, int unlockLevel) {
/* 733 */     ArrayList<AbstractUnlock> tmpBundle = new ArrayList();
/* 734 */     switch (c) {
/*     */     case IRONCLAD: 
/* 736 */       switch (unlockLevel) {
/*     */       case 0: 
/* 738 */         tmpBundle.add(new HeavyBladeUnlock());
/* 739 */         tmpBundle.add(new SpotWeaknessUnlock());
/* 740 */         tmpBundle.add(new LimitBreakUnlock());
/* 741 */         break;
/*     */       case 1: 
/* 743 */         tmpBundle.add(new OmamoriUnlock());
/* 744 */         tmpBundle.add(new PrayerWheelUnlock());
/* 745 */         tmpBundle.add(new ShovelUnlock());
/* 746 */         break;
/*     */       case 2: 
/* 748 */         tmpBundle.add(new WildStrikeUnlock());
/* 749 */         tmpBundle.add(new EvolveUnlock());
/* 750 */         tmpBundle.add(new ImmolateUnlock());
/* 751 */         break;
/*     */       case 3: 
/* 753 */         tmpBundle.add(new HavocUnlock());
/* 754 */         tmpBundle.add(new SentinelUnlock());
/* 755 */         tmpBundle.add(new ExhumeUnlock());
/* 756 */         break;
/*     */       case 4: 
/* 758 */         tmpBundle.add(new BlueCandleUnlock());
/* 759 */         tmpBundle.add(new DeadBranchUnlock());
/* 760 */         tmpBundle.add(new SingingBowlUnlock());
/*     */       }
/*     */       
/* 763 */       break;
/*     */     
/*     */ 
/*     */     case THE_SILENT: 
/* 767 */       switch (unlockLevel) {
/*     */       case 0: 
/* 769 */         tmpBundle.add(new BaneUnlock());
/* 770 */         tmpBundle.add(new CatalystUnlock());
/* 771 */         tmpBundle.add(new CorpseExplosionUnlock());
/* 772 */         break;
/*     */       case 1: 
/* 774 */         tmpBundle.add(new DuvuDollUnlock());
/* 775 */         tmpBundle.add(new SmilingMaskUnlock());
/* 776 */         tmpBundle.add(new TinyChestUnlock());
/* 777 */         break;
/*     */       case 2: 
/* 779 */         tmpBundle.add(new CloakAndDaggerUnlock());
/* 780 */         tmpBundle.add(new AccuracyUnlock());
/* 781 */         tmpBundle.add(new StormOfSteelUnlock());
/* 782 */         break;
/*     */       case 3: 
/* 784 */         tmpBundle.add(new ArtOfWarUnlock());
/* 785 */         tmpBundle.add(new CourierUnlock());
/* 786 */         tmpBundle.add(new PandorasBoxUnlock());
/* 787 */         break;
/*     */       case 4: 
/* 789 */         tmpBundle.add(new ConcentrateUnlock());
/* 790 */         tmpBundle.add(new SetupUnlock());
/* 791 */         tmpBundle.add(new GrandFinaleUnlock());
/*     */       }
/*     */       
/* 794 */       break;
/*     */     
/*     */ 
/*     */     case DEFECT: 
/* 798 */       switch (unlockLevel) {
/*     */       case 0: 
/* 800 */         tmpBundle.add(new ReboundUnlock());
/* 801 */         tmpBundle.add(new UndoUnlock());
/* 802 */         tmpBundle.add(new EchoFormUnlock());
/* 803 */         break;
/*     */       case 1: 
/* 805 */         tmpBundle.add(new TurboUnlock());
/* 806 */         tmpBundle.add(new SunderUnlock());
/* 807 */         tmpBundle.add(new MeteorStrikeUnlock());
/* 808 */         break;
/*     */       case 2: 
/* 810 */         tmpBundle.add(new HyperbeamUnlock());
/* 811 */         tmpBundle.add(new RecycleUnlock());
/* 812 */         tmpBundle.add(new NovaUnlock());
/* 813 */         break;
/*     */       case 3: 
/* 815 */         tmpBundle.add(new CablesUnlock());
/* 816 */         tmpBundle.add(new TurnipUnlock());
/* 817 */         tmpBundle.add(new RunicCapacitorUnlock());
/* 818 */         break;
/*     */       case 4: 
/* 820 */         tmpBundle.add(new EmotionChipUnlock());
/* 821 */         tmpBundle.add(new VirusUnlock());
/* 822 */         tmpBundle.add(new DataDiskUnlock());
/*     */       }
/*     */       
/* 825 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/* 831 */     return tmpBundle;
/*     */   }
/*     */   
/*     */   public static void addCardUnlockToList(HashMap<String, AbstractUnlock> map, String key, AbstractUnlock unlock) {
/* 835 */     if (isCardLocked(key)) {
/* 836 */       map.put(key, unlock);
/*     */     }
/*     */   }
/*     */   
/*     */   public static void addRelicUnlockToList(HashMap<String, AbstractUnlock> map, String key, AbstractUnlock unlock) {
/* 841 */     if (isRelicLocked(key)) {
/* 842 */       map.put(key, unlock);
/*     */     }
/*     */   }
/*     */   
/*     */   public static float getCompletionPercentage() {
/* 847 */     float totalPercent = 0.0F;
/* 848 */     totalPercent += getAscensionProgress() * 0.3F;
/* 849 */     totalPercent += getUnlockProgress() * 0.25F;
/* 850 */     totalPercent += getAchievementProgress() * 0.35F;
/* 851 */     totalPercent += getSeenCardsProgress() * 0.05F;
/* 852 */     totalPercent += getSeenRelicsProgress() * 0.05F;
/* 853 */     return totalPercent * 100.0F;
/*     */   }
/*     */   
/*     */   private static float getAscensionProgress() {
/* 857 */     int sum = CardCrawlGame.ironcladPrefs.getInteger("ASCENSION_LEVEL", 0);
/* 858 */     sum += CardCrawlGame.silentPrefs.getInteger("ASCENSION_LEVEL", 0);
/* 859 */     sum += CardCrawlGame.defectPrefs.getInteger("ASCENSION_LEVEL", 0);
/*     */     
/* 861 */     float retVal = sum / 60.0F;
/* 862 */     logger.info("Ascension Progress: " + retVal);
/* 863 */     if (retVal > 1.0F) {
/* 864 */       retVal = 1.0F;
/*     */     }
/* 866 */     return retVal;
/*     */   }
/*     */   
/*     */   private static float getUnlockProgress() {
/* 870 */     int sum = Math.min(getUnlockLevel(AbstractPlayer.PlayerClass.IRONCLAD), 5);
/* 871 */     sum += Math.min(getUnlockLevel(AbstractPlayer.PlayerClass.THE_SILENT), 5);
/* 872 */     sum += Math.min(getUnlockLevel(AbstractPlayer.PlayerClass.DEFECT), 5);
/*     */     
/* 874 */     float retVal = sum / 15.0F;
/* 875 */     logger.info("Unlock IC: " + getUnlockLevel(AbstractPlayer.PlayerClass.IRONCLAD));
/* 876 */     logger.info("Unlock Silent: " + getUnlockLevel(AbstractPlayer.PlayerClass.THE_SILENT));
/* 877 */     logger.info("Unlock Defect: " + getUnlockLevel(AbstractPlayer.PlayerClass.DEFECT));
/* 878 */     logger.info("Unlock Progress: " + retVal);
/* 879 */     if (retVal > 1.0F) {
/* 880 */       retVal = 1.0F;
/*     */     }
/* 882 */     return retVal;
/*     */   }
/*     */   
/*     */   private static float getAchievementProgress() {
/* 886 */     int sum = 0;
/* 887 */     for (AchievementItem item : StatsScreen.achievements.items) {
/* 888 */       if (item.isUnlocked) {
/* 889 */         sum++;
/*     */       }
/*     */     }
/*     */     
/* 893 */     float retVal = sum / StatsScreen.achievements.items.size();
/* 894 */     logger.info("Achievement Progress: " + retVal);
/* 895 */     if (retVal > 1.0F) {
/* 896 */       retVal = 1.0F;
/*     */     }
/* 898 */     return retVal;
/*     */   }
/*     */   
/*     */   private static float getSeenCardsProgress() {
/* 902 */     int sum = 0;
/* 903 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 904 */       if (((AbstractCard)c.getValue()).isSeen) {
/* 905 */         sum++;
/*     */       }
/*     */     }
/*     */     
/* 909 */     float retVal = sum / CardLibrary.cards.size();
/* 910 */     logger.info("Seen Cards Progress: " + retVal);
/* 911 */     if (retVal > 1.0F) {
/* 912 */       retVal = 1.0F;
/*     */     }
/* 914 */     return retVal;
/*     */   }
/*     */   
/*     */   private static float getSeenRelicsProgress() {
/* 918 */     float retVal = RelicLibrary.seenRelics / RelicLibrary.totalRelicCount;
/* 919 */     logger.info("Seen Relics Progress: " + retVal);
/* 920 */     if (retVal > 1.0F) {
/* 921 */       retVal = 1.0F;
/*     */     }
/* 923 */     return retVal;
/*     */   }
/*     */   
/*     */   public static long getTotalPlaytime() {
/* 927 */     return Settings.totalPlayTime;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\UnlockTracker.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */