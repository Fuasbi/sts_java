/*    */ package com.megacrit.cardcrawl.unlock.misc;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class TheSilentUnlock extends com.megacrit.cardcrawl.unlock.AbstractUnlock
/*    */ {
/*    */   public static final String KEY = "The Silent";
/*    */   
/*    */   public TheSilentUnlock()
/*    */   {
/* 12 */     this.type = com.megacrit.cardcrawl.unlock.AbstractUnlock.UnlockType.CHARACTER;
/* 13 */     this.key = "The Silent";
/* 14 */     this.title = "The Silent";
/*    */   }
/*    */   
/*    */   public void onUnlockScreenOpen()
/*    */   {
/* 19 */     this.player = new com.megacrit.cardcrawl.characters.TheSilent("Unlock", AbstractPlayer.PlayerClass.THE_SILENT);
/* 20 */     this.player.drawX = (Settings.WIDTH / 2.0F - 20.0F * Settings.scale);
/* 21 */     this.player.drawY = (Settings.HEIGHT / 2.0F - 118.0F * Settings.scale);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\unlock\misc\TheSilentUnlock.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */