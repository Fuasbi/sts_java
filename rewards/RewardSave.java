/*    */ package com.megacrit.cardcrawl.rewards;
/*    */ 
/*    */ public class RewardSave {
/*    */   public String type;
/*    */   public String id;
/*    */   public int amount;
/*    */   public int bonusGold;
/*    */   
/*  9 */   public RewardSave(String type, String id, int amount, int bonusGold) { this.type = type;
/* 10 */     this.id = id;
/* 11 */     this.amount = amount;
/*    */   }
/*    */   
/*    */   public RewardSave(String type, String id) {
/* 15 */     this(type, id, 0, 0);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rewards\RewardSave.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */