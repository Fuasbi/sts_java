/*    */ package com.megacrit.cardcrawl.rewards.chests;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class CursedChest extends AbstractChest
/*    */ {
/*    */   public CursedChest()
/*    */   {
/* 10 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/npcs/redChest.png");
/* 11 */     this.openedImg = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/npcs/redChestOpened.png");
/*    */     
/* 13 */     this.hb = new Hitbox(256.0F * Settings.scale, 200.0F * Settings.scale);
/* 14 */     this.hb.move(CHEST_LOC_X, CHEST_LOC_Y - 100.0F * Settings.scale);
/*    */     
/* 16 */     this.COMMON_CHANCE = 0;
/* 17 */     this.UNCOMMON_CHANCE = 0;
/* 18 */     this.RARE_CHANCE = 100;
/* 19 */     this.GOLD_CHANCE = 15;
/* 20 */     this.GOLD_AMT = 100;
/* 21 */     this.cursed = true;
/*    */     
/* 23 */     randomizeReward();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rewards\chests\CursedChest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */