/*     */ package com.megacrit.cardcrawl.rewards.chests;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public abstract class AbstractChest
/*     */ {
/*  21 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractChest");
/*  22 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  24 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(AbstractChest.class.getName());
/*  25 */   public static final float CHEST_LOC_X = 1308.0F * Settings.scale; public static final float CHEST_LOC_Y = AbstractDungeon.floorY + 192.0F * Settings.scale;
/*     */   private static final int RAW_W = 512;
/*     */   protected Hitbox hb;
/*     */   protected com.badlogic.gdx.graphics.Texture img;
/*     */   protected com.badlogic.gdx.graphics.Texture openedImg;
/*  30 */   public boolean isOpen = false;
/*     */   public int COMMON_CHANCE;
/*     */   public int UNCOMMON_CHANCE;
/*     */   public int RARE_CHANCE;
/*     */   public int GOLD_CHANCE;
/*     */   public int GOLD_AMT;
/*     */   public RelicReward relicReward;
/*  37 */   public boolean goldReward = false;
/*  38 */   public boolean cursed = false;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected boolean keyRequirement()
/*     */   {
/*  52 */     this.isOpen = true;
/*  53 */     return true;
/*     */   }
/*     */   
/*     */   public void randomizeReward()
/*     */   {
/*  58 */     int roll = AbstractDungeon.treasureRng.random(0, 99);
/*     */     
/*     */ 
/*  61 */     if (roll < this.GOLD_CHANCE) {
/*  62 */       this.goldReward = true;
/*     */     }
/*     */     
/*     */ 
/*  66 */     if (roll < this.COMMON_CHANCE) {
/*  67 */       this.relicReward = RelicReward.COMMON_RELIC;
/*  68 */     } else if (roll < this.UNCOMMON_CHANCE + this.COMMON_CHANCE) {
/*  69 */       this.relicReward = RelicReward.UNCOMMON_RELIC;
/*     */     } else {
/*  71 */       this.relicReward = RelicReward.RARE_RELIC;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void open(boolean bossChest)
/*     */   {
/*  79 */     AbstractDungeon.overlayMenu.proceedButton.setLabel(TEXT[0]);
/*  80 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  81 */       r.onChestOpen(bossChest);
/*     */     }
/*  83 */     CardCrawlGame.sound.play("CHEST_OPEN");
/*     */     
/*  85 */     if (this.goldReward) {
/*  86 */       if (Settings.isDailyRun) {
/*  87 */         AbstractDungeon.getCurrRoom().addGoldToRewards(this.GOLD_AMT);
/*     */       } else {
/*  89 */         AbstractDungeon.getCurrRoom().addGoldToRewards(
/*  90 */           Math.round(AbstractDungeon.treasureRng.random(this.GOLD_AMT * 0.9F, this.GOLD_AMT * 1.1F)));
/*     */       }
/*     */     }
/*     */     
/*  94 */     if (this.cursed) {
/*  95 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect(
/*  96 */         AbstractDungeon.returnRandomCurse(), this.hb.cX, this.hb.cY));
/*     */     }
/*     */     
/*  99 */     switch (this.relicReward) {
/*     */     case COMMON_RELIC: 
/* 101 */       AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.COMMON);
/* 102 */       break;
/*     */     case UNCOMMON_RELIC: 
/* 104 */       AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.UNCOMMON);
/* 105 */       break;
/*     */     case RARE_RELIC: 
/* 107 */       AbstractDungeon.getCurrRoom().addRelicToRewards(AbstractRelic.RelicTier.RARE);
/* 108 */       break;
/*     */     default: 
/* 110 */       logger.info("ERROR: Unspecified reward: " + this.relicReward.name());
/*     */     }
/*     */     
/*     */     
/* 114 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 115 */       r.onChestOpenAfter(bossChest);
/*     */     }
/*     */     
/* 118 */     AbstractDungeon.combatRewardScreen.open();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static enum RelicReward
/*     */   {
/* 125 */     COMMON_RELIC,  UNCOMMON_RELIC,  RARE_RELIC;
/*     */     
/*     */     private RelicReward() {}
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 132 */     this.hb.update();
/* 133 */     if (((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) || ((CInputActionSet.select.isJustPressed()) && (!AbstractDungeon.isScreenUp) && (!this.isOpen) && 
/* 134 */       (keyRequirement()))) {
/* 135 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 136 */       open(false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void close() {}
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 145 */     sb.setColor(Color.WHITE);
/* 146 */     float angle = 0.0F;
/* 147 */     if ((this.isOpen) && (this.openedImg == null)) {
/* 148 */       angle = 180.0F;
/*     */     }
/*     */     
/* 151 */     if ((!this.isOpen) || (angle == 180.0F)) {
/* 152 */       sb.draw(this.img, CHEST_LOC_X - 256.0F, CHEST_LOC_Y - 256.0F + AbstractDungeon.sceneOffsetY, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, angle, 0, 0, 512, 512, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 170 */       if (this.hb.hovered) {
/* 171 */         sb.setBlendFunction(770, 1);
/* 172 */         sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.3F));
/* 173 */         sb.draw(this.img, CHEST_LOC_X - 256.0F, CHEST_LOC_Y - 256.0F + AbstractDungeon.sceneOffsetY, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, angle, 0, 0, 512, 512, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 190 */         sb.setBlendFunction(770, 771);
/*     */       }
/*     */     } else {
/* 193 */       sb.draw(this.openedImg, CHEST_LOC_X - 256.0F, CHEST_LOC_Y - 256.0F + AbstractDungeon.sceneOffsetY, 256.0F, 256.0F, 512.0F, 512.0F, Settings.scale, Settings.scale, angle, 0, 0, 512, 512, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 212 */     if ((Settings.isControllerMode) && (!this.isOpen)) {
/* 213 */       sb.setColor(Color.WHITE);
/* 214 */       sb.draw(CInputActionSet.select
/* 215 */         .getKeyImg(), CHEST_LOC_X - 32.0F - 150.0F * Settings.scale, CHEST_LOC_Y - 32.0F - 210.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 233 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rewards\chests\AbstractChest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */