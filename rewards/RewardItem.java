/*     */ package com.megacrit.cardcrawl.rewards;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*     */ import com.megacrit.cardcrawl.rooms.TreasureRoom;
/*     */ import com.megacrit.cardcrawl.screens.CardRewardScreen;
/*     */ import com.megacrit.cardcrawl.ui.FtueTip;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.RewardGlowEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class RewardItem
/*     */ {
/*  47 */   private static final Logger logger = LogManager.getLogger(RewardItem.class.getName());
/*  48 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("RewardItem");
/*  49 */   public static final String[] TEXT = uiStrings.TEXT;
/*  50 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Potion Tip");
/*  51 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  52 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*     */   
/*     */   public RewardType type;
/*  55 */   public int goldAmt = 0;
/*  56 */   public int bonusGold = 0;
/*     */   public String text;
/*     */   public AbstractRelic relic;
/*     */   public AbstractPotion potion;
/*     */   public ArrayList<AbstractCard> cards;
/*  61 */   private ArrayList<AbstractGameEffect> effects = new ArrayList();
/*     */   private boolean isBoss;
/*  63 */   public Hitbox hb = new Hitbox(460.0F * Settings.scale, 90.0F * Settings.scale);
/*  64 */   public float y; public float flashTimer = 0.0F;
/*  65 */   public boolean isDone = false;
/*     */   private static final float FLASH_DUR = 0.5F;
/*     */   private static final int ITEM_W = 464;
/*     */   private static final int ITEM_H = 98;
/*  69 */   public static final float REWARD_ITEM_X = 786.0F * Settings.scale;
/*  70 */   private static final float REWARD_TEXT_X = 833.0F * Settings.scale;
/*     */   
/*     */   public static enum RewardType {
/*  73 */     CARD,  GOLD,  RELIC,  POTION,  STOLEN_GOLD;
/*     */     
/*     */     private RewardType() {} }
/*     */   
/*  77 */   public RewardItem(int gold) { this.type = RewardType.GOLD;
/*  78 */     this.goldAmt = gold;
/*  79 */     applyGoldBonus(false);
/*     */   }
/*     */   
/*     */   public RewardItem(int gold, boolean theft) {
/*  83 */     this.type = RewardType.STOLEN_GOLD;
/*  84 */     this.goldAmt = gold;
/*  85 */     applyGoldBonus(theft);
/*     */   }
/*     */   
/*     */   private void applyGoldBonus(boolean theft) {
/*  89 */     int tmp = this.goldAmt;
/*  90 */     this.bonusGold = 0;
/*     */     
/*  92 */     if (theft) {
/*  93 */       this.text = (this.goldAmt + TEXT[0]);
/*     */     } else {
/*  95 */       if (!(AbstractDungeon.getCurrRoom() instanceof TreasureRoom))
/*     */       {
/*  97 */         if (AbstractDungeon.player.hasRelic("Golden Idol")) {
/*  98 */           this.bonusGold += MathUtils.round(tmp * 0.25F);
/*     */         }
/*     */         
/* 101 */         if (((Boolean)DailyMods.negativeMods.get("Midas")).booleanValue()) {
/* 102 */           this.bonusGold += MathUtils.round(tmp * 2.0F);
/*     */         }
/*     */       }
/*     */       
/* 106 */       if (this.bonusGold == 0) {
/* 107 */         this.text = (this.goldAmt + TEXT[1]);
/*     */       } else {
/* 109 */         this.text = (this.goldAmt + TEXT[1] + " (" + this.bonusGold + ")");
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public RewardItem(AbstractRelic relic) {
/* 115 */     this.type = RewardType.RELIC;
/* 116 */     this.relic = relic;
/* 117 */     relic.hb = new Hitbox(80.0F * Settings.scale, 80.0F * Settings.scale);
/* 118 */     relic.hb.move(-1000.0F, -1000.0F);
/* 119 */     this.text = relic.name;
/*     */   }
/*     */   
/*     */   public RewardItem(AbstractPotion potion) {
/* 123 */     this.type = RewardType.POTION;
/* 124 */     this.potion = potion;
/* 125 */     this.text = potion.name;
/*     */   }
/*     */   
/*     */   public RewardItem() {
/* 129 */     this.type = RewardType.CARD;
/* 130 */     this.isBoss = (AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss);
/* 131 */     this.cards = AbstractDungeon.getRewardCards();
/* 132 */     this.text = TEXT[2];
/*     */   }
/*     */   
/*     */   public RewardItem(AbstractCard.CardColor colorType) {
/* 136 */     this.type = RewardType.CARD;
/* 137 */     this.isBoss = (AbstractDungeon.getCurrRoom() instanceof MonsterRoomBoss);
/* 138 */     if (colorType == AbstractCard.CardColor.COLORLESS) {
/* 139 */       this.cards = AbstractDungeon.getColorlessRewardCards();
/*     */     } else {
/* 141 */       this.cards = AbstractDungeon.getRewardCards();
/*     */     }
/* 143 */     this.text = TEXT[2];
/*     */     
/* 145 */     for (AbstractCard c : this.cards) {
/* 146 */       if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/* 147 */         c.upgrade();
/* 148 */       } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2"))) {
/* 149 */         c.upgrade();
/* 150 */       } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2"))) {
/* 151 */         c.upgrade();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void incrementGold(int gold)
/*     */   {
/* 162 */     if (this.type == RewardType.GOLD) {
/* 163 */       this.goldAmt += gold;
/* 164 */       applyGoldBonus(false);
/* 165 */     } else if (this.type == RewardType.STOLEN_GOLD) {
/* 166 */       this.goldAmt += gold;
/* 167 */       applyGoldBonus(true);
/*     */     } else {
/* 169 */       logger.info("ERROR: Using increment() wrong for RewardItem");
/*     */     }
/*     */   }
/*     */   
/*     */   public void move(float y) {
/* 174 */     this.y = y;
/* 175 */     this.hb.move(Settings.WIDTH / 2.0F, y);
/* 176 */     if (this.type == RewardType.POTION) {
/* 177 */       this.potion.move(REWARD_ITEM_X, y);
/* 178 */     } else if (this.type == RewardType.RELIC) {
/* 179 */       this.relic.currentX = REWARD_ITEM_X;
/* 180 */       this.relic.currentY = y;
/* 181 */       this.relic.targetX = REWARD_ITEM_X;
/* 182 */       this.relic.targetY = y;
/*     */     }
/*     */   }
/*     */   
/*     */   public void flash() {
/* 187 */     this.flashTimer = 0.5F;
/*     */   }
/*     */   
/*     */   public void update() {
/* 191 */     if (this.flashTimer > 0.0F) {
/* 192 */       this.flashTimer -= Gdx.graphics.getDeltaTime();
/* 193 */       if (this.flashTimer < 0.0F) {
/* 194 */         this.flashTimer = 0.0F;
/*     */       }
/*     */     }
/* 197 */     this.hb.update();
/* 198 */     if (this.effects.size() == 0) {
/* 199 */       this.effects.add(new RewardGlowEffect(this.hb.cX, this.hb.cY));
/*     */     }
/* 201 */     for (Iterator<AbstractGameEffect> i = this.effects.iterator(); i.hasNext();) {
/* 202 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 203 */       e.update();
/* 204 */       if (e.isDone) {
/* 205 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 209 */     if (this.hb.hovered) {
/* 210 */       switch (this.type) {
/*     */       case POTION: 
/* 212 */         if (!AbstractDungeon.topPanel.potionCombine) {
/* 213 */           TipHelper.renderGenericTip(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY, this.potion.name, this.potion.description);
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         break;
/*     */       case RELIC: 
/*     */         break;
/*     */       }
/*     */       
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 234 */     if (this.hb.justHovered) {
/* 235 */       CardCrawlGame.sound.play("UI_HOVER");
/*     */     }
/*     */     
/* 238 */     if ((this.hb.hovered) && (InputHelper.justClickedLeft) && (!this.isDone)) {
/* 239 */       CardCrawlGame.sound.playA("UI_CLICK_1", 0.1F);
/* 240 */       this.hb.clickStarted = true;
/*     */     }
/*     */     
/* 243 */     if ((this.hb.hovered) && (CInputActionSet.select.isJustPressed()) && (!this.isDone)) {
/* 244 */       this.hb.clicked = true;
/* 245 */       CardCrawlGame.sound.playA("UI_CLICK_1", 0.1F);
/*     */     }
/*     */     
/* 248 */     if (this.hb.clicked) {
/* 249 */       this.hb.clicked = false;
/* 250 */       this.isDone = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean claimReward() {
/* 255 */     switch (this.type) {
/*     */     case GOLD: 
/* 257 */       CardCrawlGame.sound.play("GOLD_GAIN");
/* 258 */       if (this.bonusGold == 0) {
/* 259 */         AbstractDungeon.player.gainGold(this.goldAmt);
/*     */       } else {
/* 261 */         AbstractDungeon.player.gainGold(this.goldAmt + this.bonusGold);
/*     */       }
/* 263 */       return true;
/*     */     case STOLEN_GOLD: 
/* 265 */       CardCrawlGame.sound.play("GOLD_GAIN");
/* 266 */       if (this.bonusGold == 0) {
/* 267 */         AbstractDungeon.player.gainGold(this.goldAmt);
/*     */       } else {
/* 269 */         AbstractDungeon.player.gainGold(this.goldAmt + this.bonusGold);
/*     */       }
/* 271 */       return true;
/*     */     case POTION: 
/* 273 */       if (AbstractDungeon.player.hasRelic("Sozu")) {
/* 274 */         AbstractDungeon.player.getRelic("Sozu").flash();
/* 275 */         return true;
/*     */       }
/* 277 */       if (AbstractDungeon.player.obtainPotion(this.potion)) {
/* 278 */         if (!((Boolean)TipTracker.tips.get("POTION_TIP")).booleanValue()) {
/* 279 */           AbstractDungeon.ftue = new FtueTip(LABEL[0], MSG[0], Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, this.potion);
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 285 */           TipTracker.neverShowAgain("POTION_TIP");
/*     */         }
/* 287 */         CardCrawlGame.metricData.addPotionObtainData(this.potion);
/* 288 */         return true;
/*     */       }
/*     */       
/* 291 */       return false;
/*     */     case RELIC: 
/* 293 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.GRID) {
/* 294 */         return false;
/*     */       }
/*     */       
/* 297 */       this.relic.instantObtain();
/*     */       
/* 299 */       CardCrawlGame.metricData.addRelicObtainData(this.relic);
/* 300 */       return true;
/*     */     case CARD: 
/* 302 */       if (AbstractDungeon.player.hasRelic("Question Card")) {
/* 303 */         AbstractDungeon.player.getRelic("Question Card").flash();
/*     */       }
/*     */       
/* 306 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.COMBAT_REWARD) {
/* 307 */         AbstractDungeon.cardRewardScreen.open(this.cards, this, TEXT[4]);
/* 308 */         AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.COMBAT_REWARD;
/*     */       }
/* 310 */       return false;
/*     */     }
/*     */     
/* 313 */     logger.info("ERROR: Claim Reward() failed");
/* 314 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 321 */     if (this.hb.hovered) {
/* 322 */       sb.setColor(new Color(0.4F, 0.6F, 0.6F, 1.0F));
/*     */     } else {
/* 324 */       sb.setColor(new Color(0.5F, 0.6F, 0.6F, 0.8F));
/*     */     }
/* 326 */     if (this.hb.clickStarted) {
/* 327 */       sb.draw(ImageMaster.REWARD_SCREEN_ITEM, Settings.WIDTH / 2.0F - 232.0F, this.y - 49.0F, 232.0F, 49.0F, 464.0F, 98.0F, Settings.scale * 0.98F, Settings.scale * 0.98F, 0.0F, 0, 0, 464, 98, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 345 */       sb.draw(ImageMaster.REWARD_SCREEN_ITEM, Settings.WIDTH / 2.0F - 232.0F, this.y - 49.0F, 232.0F, 49.0F, 464.0F, 98.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 464, 98, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 364 */     if (this.flashTimer != 0.0F) {
/* 365 */       sb.setColor(0.6F, 1.0F, 1.0F, this.flashTimer * 1.5F);
/* 366 */       sb.setBlendFunction(770, 1);
/* 367 */       sb.draw(ImageMaster.REWARD_SCREEN_ITEM, Settings.WIDTH / 2.0F - 232.0F, this.y - 49.0F, 232.0F, 49.0F, 464.0F, 98.0F, Settings.scale * 1.03F, Settings.scale * 1.15F, 0.0F, 0, 0, 464, 98, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 384 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 387 */     sb.setColor(Color.WHITE);
/*     */     
/* 389 */     switch (this.type) {
/*     */     case CARD: 
/* 391 */       if (this.isBoss) {
/* 392 */         sb.draw(ImageMaster.REWARD_CARD_BOSS, REWARD_ITEM_X - 32.0F, this.y - 32.0F - 2.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 410 */         sb.draw(ImageMaster.REWARD_CARD_NORMAL, REWARD_ITEM_X - 32.0F, this.y - 32.0F - 2.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 428 */       break;
/*     */     case GOLD: 
/* 430 */       sb.draw(ImageMaster.UI_GOLD, REWARD_ITEM_X - 32.0F, this.y - 32.0F - 2.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 447 */       break;
/*     */     case STOLEN_GOLD: 
/* 449 */       sb.draw(ImageMaster.UI_GOLD, REWARD_ITEM_X - 32.0F, this.y - 32.0F - 2.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 466 */       break;
/*     */     case RELIC: 
/* 468 */       this.relic.renderOutline(sb, false);
/* 469 */       this.relic.render(sb);
/* 470 */       if (this.hb.hovered) {
/* 471 */         this.relic.renderTip(sb);
/*     */       }
/*     */       break;
/*     */     case POTION: 
/* 475 */       this.potion.renderLightOutline(sb);
/* 476 */       this.potion.render(sb);
/* 477 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 484 */     if (this.hb.hovered) {
/* 485 */       Color color = Settings.GOLD_COLOR;
/* 486 */       FontHelper.renderSmartText(sb, FontHelper.rewardTipFont, this.text, REWARD_TEXT_X, this.y + 5.0F * Settings.scale, 1000.0F * Settings.scale, 0.0F, color);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 496 */       Color color = Settings.CREAM_COLOR;
/* 497 */       FontHelper.renderSmartText(sb, FontHelper.rewardTipFont, this.text, REWARD_TEXT_X, this.y + 5.0F * Settings.scale, 1000.0F * Settings.scale, 0.0F, color);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 508 */     if (!this.hb.hovered) {
/* 509 */       for (AbstractGameEffect e : this.effects) {
/* 510 */         e.render(sb);
/*     */       }
/*     */     }
/* 513 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void recordCardSkipMetrics() {
/* 517 */     if ((this.cards != null) && (!this.cards.isEmpty())) {
/* 518 */       HashMap<String, Object> choice = new HashMap();
/* 519 */       ArrayList<String> notpicked = new ArrayList();
/* 520 */       for (AbstractCard card : this.cards) {
/* 521 */         notpicked.add(card.getMetricID());
/*     */       }
/* 523 */       choice.put("picked", "SKIP");
/* 524 */       choice.put("not_picked", notpicked);
/* 525 */       choice.put("floor", Integer.valueOf(AbstractDungeon.floorNum));
/* 526 */       CardCrawlGame.metricData.card_choices.add(choice);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\rewards\RewardItem.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */