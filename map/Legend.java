/*     */ package com.megacrit.cardcrawl.map;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class Legend
/*     */ {
/*  20 */   private static final float X = 1670.0F * Settings.scale; private static final float Y = 500.0F * Settings.scale;
/*     */   
/*  22 */   public Color c = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  23 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("Legend");
/*  24 */   public static final String[] TEXT = uiStrings.TEXT;
/*  25 */   private ArrayList<LegendItem> items = new ArrayList();
/*  26 */   public boolean isLegendHighlighted = false;
/*  27 */   private static com.badlogic.gdx.graphics.Texture img = null;
/*     */   private static final int LW = 512;
/*     */   
/*  30 */   public Legend() { this.items.add(new LegendItem(TEXT[0], ImageMaster.MAP_NODE_EVENT, TEXT[1], TEXT[2], 0));
/*  31 */     this.items.add(new LegendItem(TEXT[3], ImageMaster.MAP_NODE_MERCHANT, TEXT[4], TEXT[5], 1));
/*  32 */     this.items.add(new LegendItem(TEXT[6], ImageMaster.MAP_NODE_TREASURE, TEXT[7], TEXT[8], 2));
/*  33 */     this.items.add(new LegendItem(TEXT[9], ImageMaster.MAP_NODE_REST, TEXT[10], TEXT[11], 3));
/*  34 */     this.items.add(new LegendItem(TEXT[12], ImageMaster.MAP_NODE_ENEMY, TEXT[13], TEXT[14], 4));
/*  35 */     this.items.add(new LegendItem(TEXT[15], ImageMaster.MAP_NODE_ELITE, TEXT[16], TEXT[17], 5));
/*  36 */     if (img == null) {
/*  37 */       img = ImageMaster.loadImage("images/ui/map/selectBox.png");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update(float mapAlpha, boolean isMapScreen) {
/*  42 */     if ((mapAlpha >= 0.8F) && (isMapScreen)) {
/*  43 */       updateControllerInput();
/*  44 */       this.c.a = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.c.a, 1.0F);
/*  45 */       for (LegendItem i : this.items) {
/*  46 */         i.update();
/*     */       }
/*     */     } else {
/*  49 */       this.c.a = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.c.a, 0.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateControllerInput() {
/*  54 */     if (!Settings.isControllerMode) {
/*  55 */       return;
/*     */     }
/*     */     
/*  58 */     if (this.isLegendHighlighted) {
/*  59 */       if ((CInputActionSet.proceed.isJustPressed()) || (CInputActionSet.cancel.isJustPressed()) || 
/*  60 */         (CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  61 */         CInputActionSet.cancel.unpress();
/*  62 */         this.isLegendHighlighted = false;
/*     */       }
/*     */       
/*     */     }
/*  66 */     else if (CInputActionSet.proceed.isJustPressed()) {
/*  67 */       this.isLegendHighlighted = true;
/*  68 */       return;
/*     */     }
/*     */     
/*     */ 
/*  72 */     if (!this.isLegendHighlighted) {
/*  73 */       return;
/*     */     }
/*     */     
/*  76 */     boolean anyHovered = false;
/*  77 */     int index = 0;
/*  78 */     for (LegendItem i : this.items) {
/*  79 */       if (i.hb.hovered) {
/*  80 */         anyHovered = true;
/*  81 */         break;
/*     */       }
/*  83 */       index++;
/*     */     }
/*     */     
/*  86 */     if (!anyHovered) {
/*  87 */       Gdx.input.setCursorPosition((int)((LegendItem)this.items.get(0)).hb.cX, Settings.HEIGHT - (int)((LegendItem)this.items.get(0)).hb.cY);
/*     */     }
/*  89 */     else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  90 */       index++;
/*  91 */       if (index > this.items.size() - 1) {
/*  92 */         index = 0;
/*     */       }
/*  94 */       Gdx.input.setCursorPosition(
/*  95 */         (int)((LegendItem)this.items.get(index)).hb.cX, Settings.HEIGHT - 
/*  96 */         (int)((LegendItem)this.items.get(index)).hb.cY);
/*  97 */     } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  98 */       index--;
/*  99 */       if (index < 0) {
/* 100 */         index = this.items.size() - 1;
/*     */       }
/* 102 */       Gdx.input.setCursorPosition(
/* 103 */         (int)((LegendItem)this.items.get(index)).hb.cX, Settings.HEIGHT - 
/* 104 */         (int)((LegendItem)this.items.get(index)).hb.cY);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 110 */     sb.setColor(this.c);
/* 111 */     sb.draw(ImageMaster.MAP_LEGEND, X - 256.0F, Y - 400.0F - DungeonMapScreen.offsetY / 50.0F, 256.0F, 400.0F, 512.0F, 800.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 512, 800, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */     Color c2 = new Color(MapRoomNode.AVAILABLE_COLOR.r, MapRoomNode.AVAILABLE_COLOR.g, MapRoomNode.AVAILABLE_COLOR.b, this.c.a);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 135 */     FontHelper.renderFontCentered(sb, FontHelper.menuBannerFont, TEXT[18], X, Y + 170.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F, c2);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 143 */     sb.setColor(c2);
/*     */     
/* 145 */     for (LegendItem i : this.items) {
/* 146 */       i.render(sb, c2);
/*     */     }
/*     */     
/* 149 */     if (Settings.isControllerMode) {
/* 150 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, c2.a));
/* 151 */       sb.draw(CInputActionSet.proceed
/* 152 */         .getKeyImg(), 1570.0F * Settings.scale - 32.0F, 670.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 169 */       if (this.isLegendHighlighted) {
/* 170 */         sb.setColor(new Color(1.0F, 0.9F, 0.5F, 0.6F + 
/* 171 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 172 */         float doop = 1.0F + (1.0F + MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L))) / 50.0F;
/* 173 */         sb.draw(img, 1670.0F * Settings.scale - 160.0F, Settings.HEIGHT - Gdx.input
/*     */         
/*     */ 
/* 176 */           .getY() - 52.0F + 4.0F * Settings.scale, 160.0F, 52.0F, 320.0F, 104.0F, Settings.scale * doop, Settings.scale * doop, 0.0F, 0, 0, 320, 104, false, false);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\map\Legend.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */