/*     */ package com.megacrit.cardcrawl.map;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DungeonMap
/*     */ {
/*     */   private static Texture top;
/*     */   private static Texture mid;
/*     */   private static Texture bot;
/*     */   private static Texture blend;
/*     */   public static Texture boss;
/*     */   public static Texture bossOutline;
/*  23 */   public float targetAlpha = 0.0F;
/*  24 */   private static final Color NOT_TAKEN_COLOR = new Color(0.34F, 0.34F, 0.34F, 1.0F);
/*  25 */   private Color bossNodeColor = NOT_TAKEN_COLOR.cpy();
/*  26 */   private Color baseMapColor = Color.WHITE.cpy();
/*     */   private float mapMidDist;
/*     */   private static float mapOffsetY;
/*  29 */   private static final float BOSS_W = 512.0F * Settings.scale;
/*  30 */   private static final float BOSS_OFFSET_Y = 1416.0F * Settings.scale;
/*     */   
/*  32 */   private static final float H = 1020.0F * Settings.scale;
/*  33 */   private static final float BLEND_H = 512.0F * Settings.scale;
/*     */   public Hitbox bossHb;
/*  35 */   public boolean atBoss = false;
/*     */   
/*     */ 
/*  38 */   public Legend legend = new Legend();
/*     */   
/*     */   public DungeonMap() {
/*  41 */     if (top == null) {
/*  42 */       top = ImageMaster.loadImage("images/ui/map/mapTop.png");
/*  43 */       mid = ImageMaster.loadImage("images/ui/map/mapMid.png");
/*  44 */       bot = ImageMaster.loadImage("images/ui/map/mapBot.png");
/*  45 */       blend = ImageMaster.loadImage("images/ui/map/mapBlend.png");
/*     */     }
/*  47 */     this.bossHb = new Hitbox(400.0F * Settings.scale, 360.0F * Settings.scale);
/*     */   }
/*     */   
/*     */   public void update() {
/*  51 */     this.legend.update(this.baseMapColor.a, AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP);
/*  52 */     this.baseMapColor.a = com.megacrit.cardcrawl.helpers.MathHelper.fadeLerpSnap(this.baseMapColor.a, this.targetAlpha);
/*     */     
/*  54 */     this.bossHb.move(Settings.WIDTH / 2.0F, DungeonMapScreen.offsetY + mapOffsetY + BOSS_OFFSET_Y + BOSS_W / 2.0F);
/*  55 */     this.bossHb.update();
/*     */     
/*  57 */     if ((AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && ((Settings.isDebug) || 
/*  58 */       (AbstractDungeon.getCurrMapNode().y == 14)))
/*     */     {
/*  60 */       if ((this.bossHb.hovered) && ((com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) || (com.megacrit.cardcrawl.helpers.controller.CInputActionSet.select.isJustPressed())))
/*     */       {
/*  62 */         AbstractDungeon.getCurrMapNode().taken = true;
/*  63 */         MapRoomNode node2 = AbstractDungeon.getCurrMapNode();
/*  64 */         for (MapEdge e : node2.getEdges()) {
/*  65 */           if (e != null) {
/*  66 */             e.markAsTaken();
/*     */           }
/*     */         }
/*     */         
/*  70 */         com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/*  71 */         com.megacrit.cardcrawl.core.CardCrawlGame.music.fadeOutTempBGM();
/*  72 */         MapRoomNode node = new MapRoomNode(-1, 15);
/*  73 */         node.room = new com.megacrit.cardcrawl.rooms.MonsterRoomBoss();
/*  74 */         AbstractDungeon.nextRoom = node;
/*     */         
/*  76 */         if (AbstractDungeon.pathY.size() > 1) {
/*  77 */           AbstractDungeon.pathX.add(AbstractDungeon.pathX.get(AbstractDungeon.pathX.size() - 1));
/*  78 */           AbstractDungeon.pathY.add(Integer.valueOf(((Integer)AbstractDungeon.pathY.get(AbstractDungeon.pathY.size() - 1)).intValue() + 1));
/*     */         } else {
/*  80 */           AbstractDungeon.pathX.add(Integer.valueOf(1));
/*  81 */           AbstractDungeon.pathY.add(Integer.valueOf(15));
/*     */         }
/*  83 */         AbstractDungeon.nextRoomTransitionStart();
/*  84 */         this.bossHb.hovered = false;
/*     */       }
/*     */     }
/*     */     
/*  88 */     if ((this.bossHb.hovered) || (this.atBoss)) {
/*  89 */       this.bossNodeColor = MapRoomNode.AVAILABLE_COLOR.cpy();
/*     */     } else {
/*  91 */       this.bossNodeColor.lerp(NOT_TAKEN_COLOR, com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 8.0F);
/*     */     }
/*  93 */     this.bossNodeColor.a = this.baseMapColor.a;
/*     */   }
/*     */   
/*     */   private float calculateMapSize() {
/*  97 */     return Settings.MAP_DST_Y * 16.0F - 1380.0F * Settings.scale;
/*     */   }
/*     */   
/*     */   public void show() {
/* 101 */     this.targetAlpha = 1.0F;
/* 102 */     this.mapMidDist = calculateMapSize();
/* 103 */     mapOffsetY = this.mapMidDist - 120.0F * Settings.scale;
/*     */   }
/*     */   
/*     */   public void hide() {
/* 107 */     this.targetAlpha = 0.0F;
/*     */   }
/*     */   
/*     */   public void hideInstantly() {
/* 111 */     this.targetAlpha = 0.0F;
/* 112 */     this.baseMapColor.a = 0.0F;
/* 113 */     this.legend.c.a = 0.0F;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 118 */     sb.setColor(this.baseMapColor);
/* 119 */     sb.draw(top, 0.0F, H + DungeonMapScreen.offsetY + mapOffsetY, Settings.WIDTH, Settings.HEIGHT);
/* 120 */     renderMapCenters(sb);
/* 121 */     sb.draw(bot, 0.0F, -this.mapMidDist + DungeonMapScreen.offsetY + mapOffsetY + 1.0F, Settings.WIDTH, Settings.HEIGHT);
/* 122 */     renderMapBlender(sb);
/* 123 */     this.legend.render(sb);
/*     */   }
/*     */   
/*     */   public void renderBossIcon(SpriteBatch sb)
/*     */   {
/* 128 */     if (boss != null) {
/* 129 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.bossNodeColor.a));
/* 130 */       sb.draw(bossOutline, Settings.WIDTH / 2.0F - BOSS_W / 2.0F, DungeonMapScreen.offsetY + mapOffsetY + BOSS_OFFSET_Y, BOSS_W, BOSS_W);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 136 */       sb.setColor(this.bossNodeColor);
/* 137 */       sb.draw(boss, Settings.WIDTH / 2.0F - BOSS_W / 2.0F, DungeonMapScreen.offsetY + mapOffsetY + BOSS_OFFSET_Y, BOSS_W, BOSS_W);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 145 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) {
/* 146 */       this.bossHb.render(sb);
/*     */     }
/*     */   }
/*     */   
/*     */   private void renderMapCenters(SpriteBatch sb) {
/* 151 */     sb.draw(mid, 0.0F, DungeonMapScreen.offsetY + mapOffsetY, Settings.WIDTH, Settings.HEIGHT);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderMapBlender(SpriteBatch sb)
/*     */   {
/* 160 */     if (!Settings.isSixteenByTen) {
/* 161 */       sb.draw(blend, 0.0F, DungeonMapScreen.offsetY + mapOffsetY + 800.0F * Settings.scale, Settings.WIDTH, BLEND_H);
/* 162 */       sb.draw(blend, 0.0F, DungeonMapScreen.offsetY + mapOffsetY - 200.0F * Settings.scale, Settings.WIDTH, BLEND_H);
/*     */     } else {
/* 164 */       sb.draw(blend, 0.0F, DungeonMapScreen.offsetY + mapOffsetY + 900.0F * Settings.scale, Settings.WIDTH, BLEND_H);
/* 165 */       sb.draw(blend, 0.0F, DungeonMapScreen.offsetY + mapOffsetY - 100.0F * Settings.scale, Settings.WIDTH, BLEND_H);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\map\DungeonMap.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */