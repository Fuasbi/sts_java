/*    */ package com.megacrit.cardcrawl.map;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*    */ 
/*    */ public class LegendItem
/*    */ {
/* 13 */   private static final float ICON_X = 1575.0F * Settings.scale;
/* 14 */   private static final float TEXT_X = 1670.0F * Settings.scale;
/* 15 */   private static final float Y = 500.0F * Settings.scale;
/* 16 */   private static final float SPACE_Y = 58.0F * Settings.scale;
/*    */   private Texture img;
/*    */   private static final int W = 128;
/*    */   private int index;
/*    */   private String label;
/*    */   private String header;
/*    */   private String body;
/* 23 */   public Hitbox hb = new Hitbox(230.0F * Settings.scale, 54.0F * Settings.scale);
/*    */   
/*    */   public LegendItem(String label, Texture img, String tipHeader, String tipBody, int index) {
/* 26 */     this.label = label;
/* 27 */     this.img = img;
/* 28 */     this.header = tipHeader;
/* 29 */     this.body = tipBody;
/* 30 */     this.index = index;
/*    */   }
/*    */   
/*    */   public void update() {
/* 34 */     this.hb.update();
/* 35 */     if (this.hb.hovered) {
/* 36 */       com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(ICON_X - 380.0F * Settings.scale, this.hb.cY, this.header, this.body);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb, Color c)
/*    */   {
/* 42 */     sb.setColor(c);
/*    */     
/* 44 */     if (this.hb.hovered) {
/* 45 */       sb.draw(this.img, ICON_X - 64.0F, Y - SPACE_Y * this.index + 100.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale / 1.2F, Settings.scale / 1.2F, 0.0F, 0, 0, 128, 128, false, false);
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 63 */       sb.draw(this.img, ICON_X - 64.0F, Y - SPACE_Y * this.index + 100.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale / 1.65F, Settings.scale / 1.65F, 0.0F, 0, 0, 128, 128, false, false);
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 82 */     FontHelper.renderFontLeftTopAligned(sb, FontHelper.eventBodyText, this.label, TEXT_X - 50.0F * Settings.scale, Y - SPACE_Y * this.index + 113.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F, c);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 90 */     this.hb.move(TEXT_X, Y - SPACE_Y * this.index + 100.0F * Settings.scale - DungeonMapScreen.offsetY / 50.0F);
/* 91 */     this.hb.render(sb);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\map\LegendItem.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */