/*     */ package com.megacrit.cardcrawl.map;
/*     */ 
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.EventRoom;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoom;
/*     */ import com.megacrit.cardcrawl.rooms.MonsterRoomElite;
/*     */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*     */ import com.megacrit.cardcrawl.rooms.ShopRoom;
/*     */ import com.megacrit.cardcrawl.rooms.TreasureRoom;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.Collections;
/*     */ import java.util.List;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RoomTypeAssigner
/*     */ {
/*  24 */   private static final Logger logger = LogManager.getLogger(RoomTypeAssigner.class.getName());
/*     */   
/*     */ 
/*     */ 
/*     */   public static void assignRowAsRoomType(ArrayList<MapRoomNode> row, Class<? extends AbstractRoom> c)
/*     */   {
/*  30 */     for (MapRoomNode n : row) {
/*  31 */       if (n.getRoom() == null) {
/*     */         try {
/*  33 */           n.setRoom((AbstractRoom)c.newInstance());
/*     */         } catch (InstantiationException|IllegalAccessException e) {
/*  35 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static int getConnectedNonAssignedNodeCount(List<ArrayList<MapRoomNode>> map)
/*     */   {
/*  48 */     int count = 0;
/*  49 */     for (ArrayList<MapRoomNode> row : map) {
/*  50 */       for (MapRoomNode node : row) {
/*  51 */         if ((node.hasEdges()) && (node.getRoom() == null))
/*     */         {
/*  53 */           count++;
/*     */         }
/*     */       }
/*     */     }
/*  57 */     return count;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static ArrayList<MapRoomNode> getSiblings(List<ArrayList<MapRoomNode>> map, ArrayList<MapRoomNode> parents, MapRoomNode n)
/*     */   {
/*  73 */     ArrayList<MapRoomNode> siblings = new ArrayList();
/*  74 */     for (MapRoomNode parent : parents) {
/*  75 */       for (MapEdge parentEdge : parent.getEdges()) {
/*  76 */         MapRoomNode siblingNode = (MapRoomNode)((ArrayList)map.get(parentEdge.dstY)).get(parentEdge.dstX);
/*  77 */         if (!siblingNode.equals(n)) {
/*  78 */           siblings.add(siblingNode);
/*     */         }
/*     */       }
/*     */     }
/*  82 */     return siblings;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean ruleSiblingMatches(ArrayList<MapRoomNode> siblings, AbstractRoom roomToBeSet)
/*     */   {
/*  96 */     List<Class<? extends AbstractRoom>> applicableRooms = Arrays.asList(new Class[] { RestRoom.class, MonsterRoom.class, EventRoom.class, MonsterRoomElite.class, ShopRoom.class });
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 102 */     for (MapRoomNode siblingNode : siblings) {
/* 103 */       if ((siblingNode.getRoom() != null) && 
/* 104 */         (applicableRooms.contains(roomToBeSet.getClass())) && (roomToBeSet.getClass().equals(siblingNode
/* 105 */         .getRoom().getClass()))) {
/* 106 */         return true;
/*     */       }
/*     */     }
/*     */     
/* 110 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean ruleParentMatches(ArrayList<MapRoomNode> parents, AbstractRoom roomToBeSet)
/*     */   {
/* 122 */     List<Class<? extends AbstractRoom>> applicableRooms = Arrays.asList(new Class[] { RestRoom.class, TreasureRoom.class, ShopRoom.class, MonsterRoomElite.class });
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 127 */     for (MapRoomNode parentNode : parents) {
/* 128 */       AbstractRoom parentRoom = parentNode.getRoom();
/* 129 */       if ((parentRoom != null) && 
/* 130 */         (applicableRooms.contains(roomToBeSet.getClass())) && (roomToBeSet.getClass().equals(parentRoom
/* 131 */         .getClass()))) {
/* 132 */         return true;
/*     */       }
/*     */     }
/*     */     
/* 136 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean ruleAssignableToRow(MapRoomNode n, AbstractRoom roomToBeSet)
/*     */   {
/* 150 */     List<Class<? extends AbstractRoom>> applicableRooms = Arrays.asList(new Class[] { RestRoom.class, MonsterRoomElite.class });
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 155 */     List<Class<RestRoom>> applicableRooms2 = Collections.singletonList(RestRoom.class);
/*     */     
/*     */ 
/* 158 */     if ((n.y <= 4) && (applicableRooms.contains(roomToBeSet.getClass()))) {
/* 159 */       return false;
/*     */     }
/*     */     
/*     */ 
/* 163 */     if ((n.y >= 13) && (applicableRooms2.contains(roomToBeSet.getClass()))) {
/* 164 */       return false;
/*     */     }
/*     */     
/* 167 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static AbstractRoom getNextRoomTypeAccordingToRules(ArrayList<ArrayList<MapRoomNode>> map, MapRoomNode n, ArrayList<AbstractRoom> roomList)
/*     */   {
/* 185 */     ArrayList<MapRoomNode> parents = n.getParents();
/* 186 */     ArrayList<MapRoomNode> siblings = getSiblings(map, parents, n);
/*     */     
/* 188 */     for (AbstractRoom roomToBeSet : roomList) {
/* 189 */       if (ruleAssignableToRow(n, roomToBeSet))
/*     */       {
/*     */ 
/* 192 */         if ((!ruleParentMatches(parents, roomToBeSet)) && (!ruleSiblingMatches(siblings, roomToBeSet)))
/* 193 */           return roomToBeSet;
/* 194 */         if (n.y == 0) {
/* 195 */           return roomToBeSet;
/*     */         }
/*     */       }
/*     */     }
/* 199 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void lastMinuteNodeChecker(ArrayList<ArrayList<MapRoomNode>> map, MapRoomNode n)
/*     */   {
/* 212 */     for (ArrayList<MapRoomNode> row : map) {
/* 213 */       for (MapRoomNode node : row) {
/* 214 */         if ((node != null) && (node.hasEdges()) && (node.getRoom() == null)) {
/* 215 */           logger.info("INFO: Node=" + node.toString() + " was null. Changed to a MonsterRoom.");
/* 216 */           node.setRoom(new MonsterRoom());
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void assignRoomsToNodes(ArrayList<ArrayList<MapRoomNode>> map, ArrayList<AbstractRoom> roomList)
/*     */   {
/* 231 */     for (ArrayList<MapRoomNode> row : map) {
/* 232 */       for (MapRoomNode node : row)
/*     */       {
/* 234 */         if ((node != null) && (node.hasEdges()) && (node.getRoom() == null)) {
/* 235 */           AbstractRoom roomToBeSet = getNextRoomTypeAccordingToRules(map, node, roomList);
/* 236 */           if (roomToBeSet != null)
/*     */           {
/* 238 */             node.setRoom((AbstractRoom)roomList.remove(roomList.indexOf(roomToBeSet)));
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ArrayList<ArrayList<MapRoomNode>> distributeRoomsAcrossMap(Random rng, ArrayList<ArrayList<MapRoomNode>> map, ArrayList<AbstractRoom> roomList)
/*     */   {
/* 250 */     int nodeCount = getConnectedNonAssignedNodeCount(map);
/* 251 */     while (roomList.size() < nodeCount) {
/* 252 */       roomList.add(new MonsterRoom());
/*     */     }
/* 254 */     if (roomList.size() > nodeCount) {
/* 255 */       logger.info("WARNING: the roomList is larger than the number of connected nodes. Not all desired roomTypes will be used.");
/*     */     }
/*     */     
/* 258 */     Collections.shuffle(roomList, rng.random);
/*     */     
/*     */ 
/* 261 */     assignRoomsToNodes(map, roomList);
/*     */     
/* 263 */     logger.info("#### Unassigned Rooms:");
/* 264 */     for (AbstractRoom r : roomList) {
/* 265 */       logger.info(r.getClass());
/*     */     }
/*     */     
/* 268 */     lastMinuteNodeChecker(map, null);
/* 269 */     return map;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\map\RoomTypeAssigner.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */