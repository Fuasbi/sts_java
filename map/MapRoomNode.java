/*     */ package com.megacrit.cardcrawl.map;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AnimatedNpc;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.rooms.EventRoom;
/*     */ import com.megacrit.cardcrawl.rooms.ShopRoom;
/*     */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*     */ import com.megacrit.cardcrawl.vfx.FadeWipeParticle;
/*     */ import com.megacrit.cardcrawl.vfx.MapCircleEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class MapRoomNode
/*     */ {
/*  30 */   private static final int IMG_WIDTH = (int)(Settings.scale * 64.0F);
/*  31 */   private static final float OFFSET_X = 560.0F * Settings.scale;
/*  32 */   private static final float OFFSET_Y = 180.0F * Settings.scale;
/*  33 */   private static final float SPACING_X = IMG_WIDTH * 2.0F;
/*  34 */   private static final float JITTER_X = 27.0F * Settings.scale;
/*  35 */   private static final float JITTER_Y = 37.0F * Settings.scale;
/*  36 */   public float offsetX = (int)MathUtils.random(-JITTER_X, JITTER_X);
/*  37 */   public float offsetY = (int)MathUtils.random(-JITTER_Y, JITTER_Y);
/*  38 */   public static final Color AVAILABLE_COLOR = new Color(0.09F, 0.13F, 0.17F, 1.0F);
/*  39 */   private static final Color NOT_TAKEN_COLOR = new Color(0.34F, 0.34F, 0.34F, 1.0F);
/*     */   
/*  41 */   private static final Color OUTLINE_COLOR = Color.valueOf("8c8c80ff");
/*  42 */   public Color color = NOT_TAKEN_COLOR.cpy();
/*  43 */   private float oscillateTimer = MathUtils.random(0.0F, 6.28F);
/*  44 */   public Hitbox hb = null;
/*     */   private static final int W = 128;
/*  46 */   private static final int O_W = 192; private static final float HITBOX_W = 64.0F * Settings.scale;
/*  47 */   private float scale = 0.5F;
/*  48 */   private float angle = MathUtils.random(360.0F);
/*  49 */   private ArrayList<MapRoomNode> parents = new ArrayList();
/*     */   
/*     */   public int x;
/*     */   public int y;
/*  53 */   public AbstractRoom room = null;
/*  54 */   private ArrayList<MapEdge> edges = new ArrayList();
/*  55 */   public boolean taken = false; public boolean highlighted = false;
/*  56 */   private float animWaitTimer = 0.0F;
/*     */   private static final float ANIM_WAIT_TIME = 0.25F;
/*     */   
/*     */   public MapRoomNode(int x, int y) {
/*  60 */     this.x = x;
/*  61 */     this.y = y;
/*  62 */     this.hb = new Hitbox(HITBOX_W, HITBOX_W);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public boolean hasEdges()
/*     */   {
/*  69 */     return !this.edges.isEmpty();
/*     */   }
/*     */   
/*     */   public void addEdge(MapEdge e) {
/*  73 */     Boolean unique = Boolean.valueOf(true);
/*  74 */     for (MapEdge otherEdge : this.edges) {
/*  75 */       if (e.compareTo(otherEdge) == 0) {
/*  76 */         unique = Boolean.valueOf(false);
/*     */       }
/*     */     }
/*     */     
/*  80 */     if (unique.booleanValue()) {
/*  81 */       this.edges.add(e);
/*     */     }
/*     */   }
/*     */   
/*     */   public void delEdge(MapEdge e) {
/*  86 */     this.edges.remove(e);
/*     */   }
/*     */   
/*     */   public ArrayList<MapEdge> getEdges() {
/*  90 */     return this.edges;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isConnectedTo(MapRoomNode node)
/*     */   {
/* 100 */     for (MapEdge edge : this.edges) {
/* 101 */       if ((node.x == edge.dstX) && (node.y == edge.dstY)) {
/* 102 */         return true;
/*     */       }
/*     */     }
/* 105 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MapEdge getEdgeConnectedTo(MapRoomNode node)
/*     */   {
/* 115 */     for (MapEdge edge : this.edges) {
/* 116 */       if ((node.x == edge.dstX) && (node.y == edge.dstY)) {
/* 117 */         return edge;
/*     */       }
/*     */     }
/* 120 */     return null;
/*     */   }
/*     */   
/*     */   public void setRoom(AbstractRoom room) {
/* 124 */     this.room = room;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean leftNodeAvailable()
/*     */   {
/* 133 */     for (MapEdge edge : this.edges) {
/* 134 */       if (edge.dstX < this.x) {
/* 135 */         return true;
/*     */       }
/*     */     }
/* 138 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean centerNodeAvailable()
/*     */   {
/* 147 */     for (MapEdge edge : this.edges) {
/* 148 */       if (edge.dstX == this.x) {
/* 149 */         return true;
/*     */       }
/*     */     }
/* 152 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean rightNodeAvailable()
/*     */   {
/* 161 */     for (MapEdge edge : this.edges) {
/* 162 */       if (edge.dstX > this.x) {
/* 163 */         return true;
/*     */       }
/*     */     }
/* 166 */     return false;
/*     */   }
/*     */   
/*     */   public void addParent(MapRoomNode parent) {
/* 170 */     this.parents.add(parent);
/*     */   }
/*     */   
/*     */   public ArrayList<MapRoomNode> getParents() {
/* 174 */     return this.parents;
/*     */   }
/*     */   
/*     */   public String getRoomSymbol(Boolean showSpecificRoomSymbol) {
/* 178 */     if ((this.room == null) || (!showSpecificRoomSymbol.booleanValue())) {
/* 179 */       return "*";
/*     */     }
/* 181 */     return this.room.getMapSymbol();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 188 */     return "(" + this.x + "," + this.y + "):" + this.edges.toString();
/*     */   }
/*     */   
/*     */ 
/* 192 */   public AbstractRoom getRoom() { return this.room; }
/*     */   
/*     */   public void update() {
/*     */     MapEdge connectedEdge;
/* 196 */     if (this.animWaitTimer != 0.0F) {
/* 197 */       this.animWaitTimer -= Gdx.graphics.getDeltaTime();
/* 198 */       if (this.animWaitTimer < 0.0F) {
/* 199 */         if (!AbstractDungeon.firstRoomChosen) {
/* 200 */           AbstractDungeon.setCurrMapNode(this);
/*     */         } else {
/* 202 */           AbstractDungeon.getCurrMapNode().taken = true;
/*     */         }
/* 204 */         connectedEdge = AbstractDungeon.getCurrMapNode().getEdgeConnectedTo(this);
/* 205 */         if (connectedEdge != null) {
/* 206 */           connectedEdge.markAsTaken();
/*     */         }
/* 208 */         this.animWaitTimer = 0.0F;
/*     */         
/* 210 */         if ((AbstractDungeon.nextRoom != null) && ((AbstractDungeon.nextRoom.room instanceof ShopRoom))) {
/* 211 */           ((ShopRoom)AbstractDungeon.nextRoom.room).merchant.anim.dispose();
/*     */         }
/*     */         
/* 214 */         AbstractDungeon.nextRoom = this;
/* 215 */         AbstractDungeon.pathX.add(Integer.valueOf(this.x));
/* 216 */         AbstractDungeon.pathY.add(Integer.valueOf(this.y));
/* 217 */         CardCrawlGame.metricData.path_taken.add(AbstractDungeon.nextRoom.getRoom().getMapSymbol());
/* 218 */         if (!AbstractDungeon.isDungeonBeaten) {
/* 219 */           AbstractDungeon.nextRoomTransitionStart();
/* 220 */           CardCrawlGame.music.fadeOutTempBGM();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 225 */     this.highlighted = false;
/* 226 */     this.scale = MathHelper.scaleLerpSnap(this.scale, 0.5F);
/*     */     
/*     */ 
/* 229 */     this.hb.move(this.x * SPACING_X + OFFSET_X + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY + this.offsetY);
/*     */     
/*     */ 
/* 232 */     this.hb.update();
/*     */     
/*     */ 
/* 235 */     for (MapEdge edge : this.edges) {
/* 236 */       if (!edge.taken) {
/* 237 */         edge.color = NOT_TAKEN_COLOR;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 242 */     if (AbstractDungeon.getCurrRoom().phase.equals(AbstractRoom.RoomPhase.COMPLETE))
/*     */     {
/* 244 */       if (equals(AbstractDungeon.getCurrMapNode()))
/*     */       {
/* 246 */         for (MapEdge edge : this.edges) {
/* 247 */           edge.color = AVAILABLE_COLOR;
/*     */         }
/*     */       }
/*     */       
/* 251 */       if ((AbstractDungeon.getCurrMapNode().isConnectedTo(this)) || (Settings.isDebug)) {
/* 252 */         if (this.hb.hovered) {
/* 253 */           if (this.hb.justHovered) {
/* 254 */             playNodeHoveredSound();
/*     */           }
/*     */           
/*     */ 
/* 258 */           if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && (AbstractDungeon.dungeonMapScreen.clicked) && (this.animWaitTimer <= 0.0F))
/*     */           {
/* 260 */             playNodeSelectedSound();
/* 261 */             AbstractDungeon.dungeonMapScreen.clicked = false;
/* 262 */             AbstractDungeon.dungeonMapScreen.clickTimer = 0.0F;
/*     */             
/*     */ 
/* 265 */             AbstractDungeon.topLevelEffects.add(new MapCircleEffect(this.x * SPACING_X + OFFSET_X + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY + this.offsetY, this.angle));
/*     */             
/*     */ 
/*     */ 
/*     */ 
/* 270 */             if (!Settings.FAST_MODE) {
/* 271 */               AbstractDungeon.topLevelEffects.add(new FadeWipeParticle());
/*     */             }
/* 273 */             this.animWaitTimer = 0.25F;
/*     */             
/* 275 */             if ((this.room instanceof EventRoom)) {
/* 276 */               CardCrawlGame.mysteryMachine += 1;
/*     */             }
/*     */           }
/* 279 */           this.highlighted = true;
/*     */         } else {
/* 281 */           this.color = AVAILABLE_COLOR.cpy();
/*     */         }
/* 283 */         oscillateColor();
/*     */       }
/* 285 */       else if ((this.hb.hovered) && (!this.taken)) {
/* 286 */         this.scale = 1.0F;
/* 287 */         this.color = AVAILABLE_COLOR.cpy();
/*     */       } else {
/* 289 */         this.color = NOT_TAKEN_COLOR.cpy();
/*     */       }
/*     */     }
/* 292 */     else if (this.hb.hovered) {
/* 293 */       this.scale = 1.0F;
/* 294 */       this.color = AVAILABLE_COLOR.cpy();
/*     */     } else {
/* 296 */       this.color = NOT_TAKEN_COLOR.cpy();
/*     */     }
/*     */     
/* 299 */     if ((!AbstractDungeon.firstRoomChosen) && (this.y == 0) && (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMPLETE)) {
/* 300 */       if (this.hb.hovered) {
/* 301 */         if (this.hb.justHovered) {
/* 302 */           playNodeHoveredSound();
/*     */         }
/*     */         
/* 305 */         if ((AbstractDungeon.screen == AbstractDungeon.CurrentScreen.MAP) && ((CInputActionSet.select.isJustPressed()) || (AbstractDungeon.dungeonMapScreen.clicked)))
/*     */         {
/* 307 */           playNodeSelectedSound();
/* 308 */           AbstractDungeon.dungeonMapScreen.clicked = false;
/* 309 */           AbstractDungeon.dungeonMapScreen.clickTimer = 0.0F;
/* 310 */           AbstractDungeon.dungeonMapScreen.dismissable = true;
/* 311 */           if (!AbstractDungeon.firstRoomChosen) {
/* 312 */             AbstractDungeon.firstRoomChosen = true;
/*     */           }
/* 314 */           AbstractDungeon.topLevelEffects.add(new MapCircleEffect(this.x * SPACING_X + OFFSET_X + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY + this.offsetY, this.angle));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/* 319 */           AbstractDungeon.topLevelEffects.add(new FadeWipeParticle());
/* 320 */           this.animWaitTimer = 0.25F;
/*     */         }
/* 322 */         this.highlighted = true;
/* 323 */       } else if (this.y != 0) {
/* 324 */         this.highlighted = true;
/* 325 */         this.scale = 1.0F;
/*     */       } else {
/* 327 */         this.color = AVAILABLE_COLOR.cpy();
/*     */       }
/* 329 */       oscillateColor();
/*     */     }
/*     */     
/* 332 */     if (equals(AbstractDungeon.getCurrMapNode())) {
/* 333 */       this.color = AVAILABLE_COLOR.cpy();
/* 334 */       this.scale = MathHelper.scaleLerpSnap(this.scale, 0.5F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void playNodeHoveredSound() {
/* 339 */     int roll = MathUtils.random(3);
/* 340 */     switch (roll) {
/*     */     case 0: 
/* 342 */       CardCrawlGame.sound.play("MAP_HOVER_1");
/* 343 */       break;
/*     */     case 1: 
/* 345 */       CardCrawlGame.sound.play("MAP_HOVER_2");
/* 346 */       break;
/*     */     case 2: 
/* 348 */       CardCrawlGame.sound.play("MAP_HOVER_3");
/* 349 */       break;
/*     */     default: 
/* 351 */       CardCrawlGame.sound.play("MAP_HOVER_4");
/*     */     }
/*     */   }
/*     */   
/*     */   private void playNodeSelectedSound()
/*     */   {
/* 357 */     int roll = MathUtils.random(3);
/* 358 */     switch (roll) {
/*     */     case 0: 
/* 360 */       CardCrawlGame.sound.play("MAP_SELECT_1");
/* 361 */       break;
/*     */     case 1: 
/* 363 */       CardCrawlGame.sound.play("MAP_SELECT_2");
/* 364 */       break;
/*     */     case 2: 
/* 366 */       CardCrawlGame.sound.play("MAP_SELECT_3");
/* 367 */       break;
/*     */     default: 
/* 369 */       CardCrawlGame.sound.play("MAP_SELECT_4");
/*     */     }
/*     */   }
/*     */   
/*     */   private void oscillateColor()
/*     */   {
/* 375 */     if (!this.taken) {
/* 376 */       this.oscillateTimer += Gdx.graphics.getDeltaTime() * 5.0F;
/* 377 */       this.color.a = (0.66F + (MathUtils.cos(this.oscillateTimer) + 1.0F) / 6.0F);
/* 378 */       this.scale = (0.25F + this.color.a);
/*     */     } else {
/* 380 */       this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 386 */     for (MapEdge edge : this.edges) {
/* 387 */       edge.render(sb);
/*     */     }
/*     */     
/* 390 */     if (this.highlighted) {
/* 391 */       sb.setColor(Color.LIGHT_GRAY);
/*     */     } else {
/* 393 */       sb.setColor(OUTLINE_COLOR);
/*     */     }
/* 395 */     sb.draw(this.room
/* 396 */       .getMapImgOutline(), this.x * SPACING_X + OFFSET_X - 64.0F + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * Settings.scale, this.scale * Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 413 */     if (this.taken) {
/* 414 */       sb.setColor(AVAILABLE_COLOR);
/*     */     } else {
/* 416 */       sb.setColor(this.color);
/*     */     }
/*     */     
/* 419 */     sb.draw(this.room
/* 420 */       .getMapImg(), this.x * SPACING_X + OFFSET_X - 64.0F + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * Settings.scale, this.scale * Settings.scale, 0.0F, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 438 */     if ((this.taken) || ((AbstractDungeon.firstRoomChosen) && (equals(AbstractDungeon.getCurrMapNode())))) {
/* 439 */       sb.setColor(AVAILABLE_COLOR);
/* 440 */       sb.draw(ImageMaster.MAP_CIRCLE_5, this.x * SPACING_X + OFFSET_X - 96.0F + this.offsetX, this.y * Settings.MAP_DST_Y + OFFSET_Y + DungeonMapScreen.offsetY - 96.0F + this.offsetY, 96.0F, 96.0F, 192.0F, 192.0F, (this.scale * 0.95F + 0.2F) * Settings.scale, (this.scale * 0.95F + 0.2F) * Settings.scale, this.angle, 0, 0, 192, 192, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 459 */     if (this.hb != null) {
/* 460 */       this.hb.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\map\MapRoomNode.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */