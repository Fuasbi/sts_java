/*    */ package com.megacrit.cardcrawl.steam;
/*    */ 
/*    */ import com.badlogic.gdx.Files;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.files.FileHandle;
/*    */ import java.io.File;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CloudFileManifest
/*    */ {
/* 13 */   public static final String IRONCLAD_SAVE_FILEPATH = "saves" + File.separator + "IRONCLAD.autosave";
/* 14 */   public static final String SILENT_SAVE_FILEPATH = "saves" + File.separator + "SILENT.autosave";
/*    */   public static final String PREF_ACHIEVEMENTS = "STSAchievements";
/*    */   public static final String PREF_SILENT = "STSDataTheSilent";
/*    */   public static final String PREF_IRONCLAD = "STSDataVagabond";
/*    */   public static final String PREF_DEFECT = "STSDataDefect";
/*    */   public static final String PREF_GAMEPLAY = "STSGameplaySettings";
/*    */   public static final String PREF_PLAYER = "STSPlayer";
/*    */   public static final String PREF_BOSSES = "STSSeenBosses";
/*    */   public static final String PREF_CARDS = "STSSeenCards";
/*    */   public static final String PREF_RELICS = "STSSeenRelics";
/*    */   public static final String PREF_SOUND = "STSSound";
/*    */   public static final String PREF_TIPS = "STSTips";
/*    */   public static final String PREF_UNLOCKS = "STSUnlocks";
/*    */   public static final String PREF_UNLOCK_PROGRESS = "STSUnlockProgress";
/*    */   
/*    */   public ArrayList<String> getFileNames(String prefDir) {
/* 30 */     ArrayList<String> names = new ArrayList();
/* 31 */     names.add(IRONCLAD_SAVE_FILEPATH);
/* 32 */     names.add(SILENT_SAVE_FILEPATH);
/* 33 */     String prefFilePath = Gdx.files.getExternalStoragePath() + Gdx.files.external(prefDir).path() + File.separator;
/* 34 */     names.add(prefFilePath + "STSAchievements");
/* 35 */     names.add(prefFilePath + "STSDataTheSilent");
/* 36 */     names.add(prefFilePath + "STSDataVagabond");
/* 37 */     names.add(prefFilePath + "STSGameplaySettings");
/* 38 */     names.add(prefFilePath + "STSPlayer");
/* 39 */     names.add(prefFilePath + "STSSeenBosses");
/* 40 */     names.add(prefFilePath + "STSSeenCards");
/* 41 */     names.add(prefFilePath + "STSSeenRelics");
/* 42 */     names.add(prefFilePath + "STSSound");
/* 43 */     names.add(prefFilePath + "STSTips");
/* 44 */     names.add(prefFilePath + "STSUnlocks");
/* 45 */     names.add(prefFilePath + "STSUnlockProgress");
/*    */     
/* 47 */     return names;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\CloudFileManifest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */