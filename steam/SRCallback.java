/*    */ package com.megacrit.cardcrawl.steam;
/*    */ 
/*    */ import com.codedisaster.steamworks.SteamPublishedFileID;
/*    */ import com.codedisaster.steamworks.SteamRemoteStorageCallback;
/*    */ import com.codedisaster.steamworks.SteamResult;
/*    */ import com.codedisaster.steamworks.SteamUGCHandle;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class SRCallback implements SteamRemoteStorageCallback
/*    */ {
/* 11 */   private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(SRCallback.class.getName());
/*    */   
/*    */   public void onFileShareResult(SteamUGCHandle fileHandle, String fileName, SteamResult result)
/*    */   {
/* 15 */     LOGGER.info("The 'onFileShareResult' callback was called and returns: fileHandle=" + fileHandle
/* 16 */       .toString() + ", fileName=" + fileName + ", result=" + result
/* 17 */       .toString());
/*    */   }
/*    */   
/*    */   public void onDownloadUGCResult(SteamUGCHandle fileHandle, SteamResult result)
/*    */   {
/* 22 */     LOGGER.info("The 'onDownloadUGCResult' callback was called and returns: fileHandle=" + fileHandle
/* 23 */       .toString() + ", result=" + result
/* 24 */       .toString());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onPublishFileResult(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA, SteamResult result)
/*    */   {
/* 32 */     LOGGER.info("The 'onPublishFileResult' callback was called and returns: publishedFileID=" + publishedFileID
/* 33 */       .toString() + ", needsToAcceptWLA=" + needsToAcceptWLA + ", result=" + result
/* 34 */       .toString());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onUpdatePublishedFileResult(SteamPublishedFileID publishedFileID, boolean needsToAcceptWLA, SteamResult result)
/*    */   {
/* 42 */     LOGGER.info("The 'onUpdatePublishedFileResult' callback was called and returns: publishedFileID=" + publishedFileID
/*    */     
/* 44 */       .toString() + ", needsToAcceptWLA=" + needsToAcceptWLA + ", result=" + result.toString());
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\SRCallback.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */