package com.megacrit.cardcrawl.steam;

import com.codedisaster.steamworks.SteamAuth.AuthSessionResponse;
import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamUserCallback;

public class SUCallback
  implements SteamUserCallback
{
  public void onValidateAuthTicket(SteamID steamID, SteamAuth.AuthSessionResponse authSessionResponse, SteamID ownerSteamID) {}
  
  public void onMicroTxnAuthorization(int appID, long orderID, boolean authorized) {}
}


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\SUCallback.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */