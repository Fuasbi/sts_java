/*     */ package com.megacrit.cardcrawl.steam;
/*     */ 
/*     */ import com.codedisaster.steamworks.SteamID;
/*     */ import com.codedisaster.steamworks.SteamLeaderboardEntry;
/*     */ import com.codedisaster.steamworks.SteamLeaderboardHandle;
/*     */ import com.codedisaster.steamworks.SteamResult;
/*     */ import com.codedisaster.steamworks.SteamUserStats;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.daily.DailyScreen;
/*     */ import com.megacrit.cardcrawl.screens.leaderboards.LeaderboardEntry;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SSCallback implements com.codedisaster.steamworks.SteamUserStatsCallback
/*     */ {
/*  17 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SSCallback.class.getName());
/*     */   
/*     */   public void onGlobalStatsReceived(long arg0, SteamResult arg1)
/*     */   {
/*  21 */     logger.info("1Bloop: " + arg0);
/*     */   }
/*     */   
/*     */ 
/*     */   public void onLeaderboardFindResult(SteamLeaderboardHandle handle, boolean found)
/*     */   {
/*  27 */     logger.info("onLeaderboardFindResult");
/*  28 */     if (found) {
/*  29 */       switch (SteamSaveSync.task) {
/*     */       case UPLOAD_DAILY: 
/*  31 */         SteamSaveSync.lbHandle = handle;
/*  32 */         SteamSaveSync.uploadDailyLeaderboardHelper();
/*  33 */         break;
/*     */       case UPLOAD: 
/*  35 */         SteamSaveSync.lbHandle = handle;
/*  36 */         SteamSaveSync.uploadLeaderboardHelper();
/*  37 */         break;
/*     */       case RETRIEVE_DAILY: 
/*  39 */         SteamSaveSync.lbHandle = handle;
/*  40 */         SteamSaveSync.getLeaderboardEntryHelper();
/*  41 */         break;
/*     */       case RETRIEVE: 
/*  43 */         SteamSaveSync.lbHandle = handle;
/*  44 */         SteamSaveSync.getLeaderboardEntryHelper();
/*  45 */         break;
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onLeaderboardScoreUploaded(boolean success, SteamLeaderboardHandle handle, int score, boolean changed, int globalRankNew, int globalRankPrevious)
/*     */   {
/*  61 */     if (!success) {
/*  62 */       logger.info("Failed to upload leaderboard data: " + score);
/*  63 */     } else if (!changed) {
/*  64 */       logger.info("Leaderboard data not changed for data: " + score);
/*     */     } else {
/*  66 */       logger.info("Successfully uploaded leaderboard data: " + score);
/*     */     }
/*  68 */     SteamSaveSync.didCompleteCallback(success);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onLeaderboardScoresDownloaded(SteamLeaderboardHandle handle, com.codedisaster.steamworks.SteamLeaderboardEntriesHandle entries, int numEntries)
/*     */   {
/*  77 */     if (SteamSaveSync.task == SteamSaveSync.LeaderboardTask.RETRIEVE) {
/*  78 */       logger.info("Downloaded " + numEntries + " entries");
/*  79 */       int[] details = new int[16];
/*  80 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.entries.clear();
/*     */       
/*  82 */       for (int i = 0; i < numEntries; i++) {
/*  83 */         SteamLeaderboardEntry entry = new SteamLeaderboardEntry();
/*  84 */         if (SteamSaveSync.steamStats.getDownloadedLeaderboardEntry(entries, i, entry, details))
/*     */         {
/*  86 */           int rTemp = entry.getGlobalRank();
/*     */           
/*  88 */           if (i == 0) {
/*  89 */             CardCrawlGame.mainMenuScreen.leaderboardsScreen.currentStartIndex = rTemp;
/*  90 */           } else if (i == numEntries) {
/*  91 */             CardCrawlGame.mainMenuScreen.leaderboardsScreen.currentEndIndex = rTemp;
/*     */           }
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  98 */           if (SteamSaveSync.accountId != -1) {}
/*  94 */           CardCrawlGame.mainMenuScreen.leaderboardsScreen.entries.add(new LeaderboardEntry(rTemp, SteamSaveSync.steamFriends
/*     */           
/*     */ 
/*  97 */             .getFriendPersonaName(entry.getSteamIDUser()), entry
/*  98 */             .getScore(), SteamSaveSync.gettingTime, SteamSaveSync.accountId == entry
/*     */             
/*     */ 
/* 101 */             .getSteamIDUser()
/* 102 */             .getAccountID()));
/*     */         } else {
/* 104 */           logger.info("FAILED TO GET LEADERBOARD ENTRY: " + i);
/*     */         }
/*     */       }
/* 107 */       CardCrawlGame.mainMenuScreen.leaderboardsScreen.waiting = false;
/* 108 */     } else if (SteamSaveSync.task == SteamSaveSync.LeaderboardTask.RETRIEVE_DAILY) {
/* 109 */       logger.info("[DAILY] Downloaded " + numEntries + " entries");
/* 110 */       int[] details = new int[16];
/* 111 */       CardCrawlGame.mainMenuScreen.dailyScreen.entries.clear();
/*     */       
/* 113 */       for (int i = 0; i < numEntries; i++) {
/* 114 */         SteamLeaderboardEntry entry = new SteamLeaderboardEntry();
/* 115 */         if (SteamSaveSync.steamStats.getDownloadedLeaderboardEntry(entries, i, entry, details))
/*     */         {
/* 117 */           int rTemp = entry.getGlobalRank();
/*     */           
/* 119 */           if (i == 0) {
/* 120 */             CardCrawlGame.mainMenuScreen.dailyScreen.currentStartIndex = rTemp;
/* 121 */           } else if (i == numEntries) {
/* 122 */             CardCrawlGame.mainMenuScreen.dailyScreen.currentEndIndex = rTemp;
/*     */           }
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */           if (SteamSaveSync.accountId != -1) {}
/* 125 */           CardCrawlGame.mainMenuScreen.dailyScreen.entries.add(new LeaderboardEntry(rTemp, SteamSaveSync.steamFriends
/*     */           
/*     */ 
/* 128 */             .getFriendPersonaName(entry.getSteamIDUser()), entry
/* 129 */             .getScore(), SteamSaveSync.gettingTime, SteamSaveSync.accountId == entry
/*     */             
/*     */ 
/* 132 */             .getSteamIDUser()
/* 133 */             .getAccountID()));
/*     */         } else {
/* 135 */           logger.info("FAILED TO GET LEADERBOARD ENTRY: " + i);
/*     */         }
/*     */       }
/* 138 */       CardCrawlGame.mainMenuScreen.dailyScreen.waiting = false;
/*     */     }
/*     */   }
/*     */   
/*     */   public void onUserAchievementStored(long arg0, boolean arg1, String arg2, int arg3, int arg4)
/*     */   {
/* 144 */     logger.info("Achievement Stored");
/*     */   }
/*     */   
/*     */ 
/*     */   public void onUserStatsReceived(long arg0, SteamID arg1, SteamResult arg2)
/*     */   {
/* 150 */     logger.info("SteamID: " + arg1.getAccountID());
/* 151 */     logger.info("APPID: " + arg0);
/*     */   }
/*     */   
/*     */   public void onUserStatsStored(long arg0, SteamResult arg1)
/*     */   {
/* 156 */     logger.info("Stat Stored");
/*     */   }
/*     */   
/*     */   public void onUserStatsUnloaded(SteamID arg0)
/*     */   {
/* 161 */     logger.info("8Bloop");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\SSCallback.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */