/*    */ package com.megacrit.cardcrawl.steam;
/*    */ 
/*    */ import com.codedisaster.steamworks.SteamAPI;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class SteamTicker implements Runnable
/*    */ {
/*  9 */   private static final Logger logger = LogManager.getLogger(SteamTicker.class.getName());
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void run()
/*    */   {
/* 16 */     logger.info("Steam Ticker initialized.");
/* 17 */     while (SteamAPI.isSteamRunning()) {
/*    */       try {
/* 19 */         Thread.sleep(66L);
/*    */       } catch (InterruptedException e) {
/* 21 */         e.printStackTrace();
/*    */       }
/*    */       
/*    */ 
/* 25 */       SteamAPI.runCallbacks();
/*    */     }
/*    */     
/* 28 */     logger.info("[ERROR] SteamAPI stopped running.");
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\SteamTicker.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */