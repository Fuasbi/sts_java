/*     */ package com.megacrit.cardcrawl.steam;
/*     */ 
/*     */ import com.codedisaster.steamworks.SteamAPI;
/*     */ import com.codedisaster.steamworks.SteamApps;
/*     */ import com.codedisaster.steamworks.SteamException;
/*     */ import com.codedisaster.steamworks.SteamFriends;
/*     */ import com.codedisaster.steamworks.SteamID;
/*     */ import com.codedisaster.steamworks.SteamLeaderboardHandle;
/*     */ import com.codedisaster.steamworks.SteamRemoteStorage;
/*     */ import com.codedisaster.steamworks.SteamUser;
/*     */ import com.codedisaster.steamworks.SteamUserStats;
/*     */ import com.codedisaster.steamworks.SteamUserStats.LeaderboardDataRequest;
/*     */ import com.codedisaster.steamworks.SteamUserStats.LeaderboardDisplayType;
/*     */ import com.codedisaster.steamworks.SteamUserStats.LeaderboardSortMethod;
/*     */ import com.codedisaster.steamworks.SteamUserStats.LeaderboardUploadScoreMethod;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyScreen;
/*     */ import com.megacrit.cardcrawl.daily.TimeHelper;
/*     */ import com.megacrit.cardcrawl.screens.leaderboards.FilterButton.LeaderboardType;
/*     */ import com.megacrit.cardcrawl.screens.leaderboards.FilterButton.RegionSetting;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import java.nio.file.Path;
/*     */ import java.nio.file.Paths;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Queue;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class SteamSaveSync
/*     */ {
/*  33 */   private static final Logger logger = LogManager.getLogger(SteamSaveSync.class.getName());
/*     */   public static SteamUserStats steamStats;
/*     */   public static SteamUser steamUser;
/*     */   private static SteamApps steamApps;
/*     */   public static SteamFriends steamFriends;
/*     */   private static Thread thread;
/*  39 */   public static int accountId = -1;
/*     */   
/*     */ 
/*  42 */   public static SteamLeaderboardHandle lbHandle = null;
/*  43 */   public static LeaderboardTask task = null;
/*  44 */   public static boolean retrieveGlobal = true; public static boolean gettingTime = false;
/*  45 */   public static int lbScore = 0; public static int startIndex = 0; public static int endIndex = 0;
/*  46 */   public static boolean isUploadingScore = false;
/*     */   
/*  48 */   public static Queue<StatTuple> statsToUpload = new java.util.LinkedList();
/*     */   
/*     */   public static enum LeaderboardTask {
/*  51 */     RETRIEVE,  RETRIEVE_DAILY,  UPLOAD,  UPLOAD_DAILY;
/*     */     
/*     */     private LeaderboardTask() {}
/*     */   }
/*     */   
/*     */   private static class StatTuple { String stat;
/*     */     int score;
/*     */     
/*  59 */     StatTuple(String statName, int scoreVal) { this.stat = statName;
/*  60 */       this.score = scoreVal;
/*     */     }
/*     */   }
/*     */   
/*     */   public static void initializeSteamApi() {
/*  65 */     if (!Settings.isDemo) {
/*     */       try {
/*  67 */         if (SteamAPI.init()) {
/*  68 */           logger.info("[SUCCESS] Steam API initialized successfully.");
/*  69 */           steamStats = new SteamUserStats(new SSCallback());
/*  70 */           steamUser = new SteamUser(new SUCallback());
/*  71 */           steamApps = new SteamApps();
/*  72 */           steamFriends = new SteamFriends(new SFCallback());
/*     */           
/*  74 */           logger.info("BUILD ID: " + steamApps.getAppBuildId());
/*  75 */           logger.info("CURRENT LANG: " + steamApps.getCurrentGameLanguage());
/*  76 */           SteamID id = steamApps.getAppOwner();
/*     */           
/*  78 */           accountId = id.getAccountID();
/*  79 */           logger.info("ACCOUNT ID: " + accountId);
/*  80 */           thread = new Thread(new SteamTicker());
/*  81 */           thread.setName("SteamTicker");
/*  82 */           thread.start();
/*     */         } else {
/*  84 */           logger.info("[FAILURE] Steam API failed to initialize correctly.");
/*     */         }
/*     */       } catch (SteamException e) {
/*  87 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ArrayList<String> getAllCloudFiles()
/*     */   {
/*  98 */     SteamRemoteStorage remoteStorage = new SteamRemoteStorage(new SRCallback());
/*  99 */     int numFiles = remoteStorage.getFileCount();
/* 100 */     logger.info("Num of files: " + numFiles);
/* 101 */     ArrayList<String> files = new ArrayList();
/* 102 */     for (int i = 0; i < numFiles; i++) {
/* 103 */       int[] sizes = new int[1];
/* 104 */       String file = remoteStorage.getFileNameAndSize(i, sizes);
/* 105 */       boolean exists = remoteStorage.fileExists(file);
/* 106 */       if (exists) {
/* 107 */         files.add(file);
/*     */       }
/* 109 */       logger.info("# " + i + " : name=" + file + ", size=" + sizes[0] + ", exists=" + (exists ? "yes" : "no"));
/*     */     }
/* 111 */     remoteStorage.dispose();
/* 112 */     return files;
/*     */   }
/*     */   
/*     */   public static void deleteAllCloudFiles(ArrayList<String> files) {
/* 116 */     SteamRemoteStorage remoteStorage = new SteamRemoteStorage(new SRCallback());
/* 117 */     for (String file : files) {
/* 118 */       logger.info("Deleting file: " + file);
/* 119 */       remoteStorage.fileDelete(file);
/*     */     }
/* 121 */     remoteStorage.dispose();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String basename(String path)
/*     */   {
/* 129 */     Path p = Paths.get(path, new String[0]);
/* 130 */     return p.getFileName().toString();
/*     */   }
/*     */   
/*     */   public static void unlockAchievement(String id) {
/* 134 */     logger.info("unlockAchievement: " + id);
/* 135 */     if (steamStats != null) {
/* 136 */       if (steamStats.setAchievement(id)) {
/* 137 */         steamStats.storeStats();
/*     */       } else {
/* 139 */         logger.info("[ERROR] Could not find achievement " + id);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void removeAllAchievementsBeCarefulNotToPush()
/*     */   {
/* 148 */     if ((Settings.isDev) && (Settings.isBeta) && 
/* 149 */       (steamStats != null) && 
/* 150 */       (steamStats.resetAllStats(true))) {
/* 151 */       steamStats.storeStats();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public static boolean incrementStat(String id, int incrementAmt)
/*     */   {
/* 158 */     logger.info("incrementStat: " + id);
/* 159 */     if (steamStats != null) {
/* 160 */       if (steamStats.setStatI(id, getStat(id) + incrementAmt)) {
/* 161 */         return true;
/*     */       }
/* 163 */       logger.info("Stat: " + id + " not found.");
/* 164 */       return false;
/*     */     }
/*     */     
/* 167 */     logger.info("[ERROR] Could not find stat " + id);
/*     */     
/* 169 */     return false;
/*     */   }
/*     */   
/*     */   public static int getStat(String id) {
/* 173 */     logger.info("getStat: " + id);
/* 174 */     if (steamStats != null) {
/* 175 */       return steamStats.getStatI(id, 0);
/*     */     }
/* 177 */     return -1;
/*     */   }
/*     */   
/*     */   public static boolean setStat(String id, int value) {
/* 181 */     logger.info("setStat: " + id);
/* 182 */     if (steamStats != null) {
/* 183 */       if (steamStats.setStatI(id, value)) {
/* 184 */         logger.info(id + " stat set to " + value);
/* 185 */         return true;
/*     */       }
/* 187 */       logger.info("Stat: " + id + " not found.");
/* 188 */       return false;
/*     */     }
/*     */     
/* 191 */     logger.info("[ERROR] Could not find stat " + id);
/*     */     
/* 193 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long getGlobalStat(String id)
/*     */   {
/* 203 */     logger.info("getGlobalStat");
/* 204 */     if (steamStats != null) {
/* 205 */       return steamStats.getGlobalStat(id, 0L);
/*     */     }
/* 207 */     return -1L;
/*     */   }
/*     */   
/*     */   public static void requestGlobalStats(int i) {
/* 211 */     logger.info("requestGlobalStats");
/* 212 */     if (steamStats != null) {
/* 213 */       steamStats.requestGlobalStats(i);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void getLeaderboardEntries(AbstractPlayer.PlayerClass pClass, FilterButton.RegionSetting rSetting, FilterButton.LeaderboardType lType, int start, int end)
/*     */   {
/* 224 */     task = LeaderboardTask.RETRIEVE;
/* 225 */     startIndex = start;
/* 226 */     endIndex = end;
/*     */     
/* 228 */     if (lType == FilterButton.LeaderboardType.FASTEST_WIN) {
/* 229 */       gettingTime = true;
/*     */     } else {
/* 231 */       gettingTime = false;
/*     */     }
/*     */     
/* 234 */     if (rSetting == FilterButton.RegionSetting.GLOBAL) {
/* 235 */       retrieveGlobal = true;
/*     */     } else {
/* 237 */       retrieveGlobal = false;
/*     */     }
/*     */     
/* 240 */     if (steamStats != null) {
/* 241 */       steamStats.findLeaderboard(createGetLeaderboardString(pClass, lType));
/*     */     }
/*     */   }
/*     */   
/*     */   public static void getDailyLeaderboard(long date, int start, int end) {
/* 246 */     task = LeaderboardTask.RETRIEVE_DAILY;
/* 247 */     startIndex = start;
/* 248 */     endIndex = end;
/* 249 */     retrieveGlobal = true;
/* 250 */     gettingTime = false;
/*     */     
/* 252 */     if (steamStats != null) {
/* 253 */       StringBuilder leaderboardRetrieveString = new StringBuilder("DAILY_");
/* 254 */       leaderboardRetrieveString.append(Long.toString(date));
/* 255 */       if (Settings.isBeta) {
/* 256 */         leaderboardRetrieveString.append("_BETA");
/*     */       }
/* 258 */       steamStats.findOrCreateLeaderboard(leaderboardRetrieveString
/* 259 */         .toString(), SteamUserStats.LeaderboardSortMethod.Descending, SteamUserStats.LeaderboardDisplayType.Numeric);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static String createGetLeaderboardString(AbstractPlayer.PlayerClass pClass, FilterButton.LeaderboardType lType)
/*     */   {
/* 267 */     String retVal = "";
/* 268 */     switch (pClass) {
/*     */     case IRONCLAD: 
/* 270 */       retVal = retVal + "IRONCLAD";
/* 271 */       break;
/*     */     case THE_SILENT: 
/* 273 */       retVal = retVal + "SILENT";
/* 274 */       break;
/*     */     case DEFECT: 
/* 276 */       retVal = retVal + "DEFECT";
/* 277 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 282 */     switch (lType) {
/*     */     case AVG_FLOOR: 
/* 284 */       retVal = retVal + "_AVG_FLOOR";
/* 285 */       break;
/*     */     case AVG_SCORE: 
/* 287 */       retVal = retVal + "_AVG_SCORE";
/* 288 */       break;
/*     */     case CONSECUTIVE_WINS: 
/* 290 */       retVal = retVal + "_CONSECUTIVE_WINS";
/* 291 */       break;
/*     */     case FASTEST_WIN: 
/* 293 */       retVal = retVal + "_FASTEST_WIN";
/* 294 */       break;
/*     */     case HIGH_SCORE: 
/* 296 */       retVal = retVal + "_HIGH_SCORE";
/* 297 */       break;
/*     */     case SPIRE_LEVEL: 
/* 299 */       retVal = retVal + "_SPIRE_LEVEL";
/* 300 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 305 */     if (Settings.isBeta) {
/* 306 */       retVal = retVal + "_BETA";
/*     */     }
/*     */     
/* 309 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void uploadLeaderboardScore(String name, int score)
/*     */   {
/* 320 */     if ((steamUser == null) || (steamStats == null)) {
/* 321 */       return;
/*     */     }
/*     */     
/* 324 */     if (isUploadingScore) {
/* 325 */       statsToUpload.add(new StatTuple(name, score));
/*     */     } else {
/* 327 */       logger.info(String.format("Uploading Steam Leaderboard score (%s: %d)", new Object[] { name, Integer.valueOf(score) }));
/* 328 */       isUploadingScore = true;
/* 329 */       task = LeaderboardTask.UPLOAD;
/* 330 */       lbScore = score;
/* 331 */       steamStats.findLeaderboard(name);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void uploadDailyLeaderboardScore(String name, int score)
/*     */   {
/* 342 */     if (!TimeHelper.isOfflineMode())
/*     */     {
/* 344 */       if ((steamUser == null) || (steamStats == null)) {
/* 345 */         logger.info("User is NOT connected to Steam, unable to upload daily score.");
/* 346 */         return;
/*     */       }
/*     */       
/* 349 */       if (isUploadingScore) {
/* 350 */         statsToUpload.add(new StatTuple(name, score));
/*     */       } else {
/* 352 */         logger.info(String.format("Uploading [DAILY] Steam Leaderboard score (%s: %d)", new Object[] { name, Integer.valueOf(score) }));
/* 353 */         isUploadingScore = true;
/* 354 */         task = LeaderboardTask.UPLOAD_DAILY;
/* 355 */         lbScore = score;
/* 356 */         steamStats.findOrCreateLeaderboard(name, SteamUserStats.LeaderboardSortMethod.Descending, SteamUserStats.LeaderboardDisplayType.Numeric);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void didCompleteCallback(boolean success)
/*     */   {
/* 365 */     logger.info("didCompleteCallback");
/* 366 */     isUploadingScore = false;
/* 367 */     if (statsToUpload.size() > 0) {
/* 368 */       StatTuple uploadMe = (StatTuple)statsToUpload.remove();
/* 369 */       uploadLeaderboardScore(uploadMe.stat, uploadMe.score);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void uploadLeaderboardHelper()
/*     */   {
/* 377 */     logger.info("uploadLeaderboardHelper");
/* 378 */     steamStats.uploadLeaderboardScore(lbHandle, SteamUserStats.LeaderboardUploadScoreMethod.KeepBest, lbScore, new int[0]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void uploadDailyLeaderboardHelper()
/*     */   {
/* 385 */     logger.info("uploadDailyLeaderboardHelper");
/* 386 */     steamStats.uploadLeaderboardScore(lbHandle, SteamUserStats.LeaderboardUploadScoreMethod.KeepBest, lbScore, new int[0]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void getLeaderboardEntryHelper()
/*     */   {
/* 394 */     if (task == LeaderboardTask.RETRIEVE) {
/* 395 */       if (retrieveGlobal) {
/* 396 */         logger.info("Downloading GLOBAL entries: " + startIndex + " - " + endIndex);
/* 397 */         if (CardCrawlGame.mainMenuScreen.leaderboardsScreen.viewMyScore) {
/* 398 */           steamStats.downloadLeaderboardEntries(lbHandle, SteamUserStats.LeaderboardDataRequest.GlobalAroundUser, -9, 10);
/* 399 */           CardCrawlGame.mainMenuScreen.leaderboardsScreen.viewMyScore = false;
/*     */         } else {
/* 401 */           steamStats.downloadLeaderboardEntries(lbHandle, SteamUserStats.LeaderboardDataRequest.Global, startIndex, endIndex);
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/* 408 */         logger.info("Downloading FRIEND entries: " + startIndex + " - " + endIndex);
/* 409 */         steamStats.downloadLeaderboardEntries(lbHandle, SteamUserStats.LeaderboardDataRequest.Friends, startIndex, endIndex);
/*     */       }
/*     */       
/*     */     }
/* 413 */     else if (task == LeaderboardTask.RETRIEVE_DAILY) {
/* 414 */       if (CardCrawlGame.mainMenuScreen.dailyScreen.viewMyScore) {
/* 415 */         steamStats.downloadLeaderboardEntries(lbHandle, SteamUserStats.LeaderboardDataRequest.GlobalAroundUser, -9, 10);
/* 416 */         CardCrawlGame.mainMenuScreen.dailyScreen.viewMyScore = false;
/*     */       } else {
/* 418 */         logger.info("Downloading GLOBAL entries: " + startIndex + " - " + endIndex);
/* 419 */         steamStats.downloadLeaderboardEntries(lbHandle, SteamUserStats.LeaderboardDataRequest.Global, startIndex, endIndex);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steam\SteamSaveSync.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */