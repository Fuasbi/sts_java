/*     */ package com.megacrit.cardcrawl.dungeons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterInfo;
/*     */ import com.megacrit.cardcrawl.rooms.EmptyRoom;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*     */ import com.megacrit.cardcrawl.scenes.TheCityScene;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TheCity
/*     */   extends AbstractDungeon
/*     */ {
/*  45 */   private static final Logger logger = LogManager.getLogger(TheCity.class.getName());
/*  46 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("TheCity");
/*  47 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  49 */   public static final String NAME = TEXT[0];
/*     */   public static final String ID = "TheCity";
/*     */   
/*     */   public TheCity(AbstractPlayer p, ArrayList<String> theList) {
/*  53 */     super(NAME, "TheCity", p, theList);
/*     */     
/*     */ 
/*  56 */     removeRelicFromPool(bossRelicPool, "Ectoplasm");
/*     */     
/*  58 */     if (scene != null) {
/*  59 */       scene.dispose();
/*     */     }
/*  61 */     scene = new TheCityScene();
/*  62 */     fadeColor = Color.valueOf("0a1e1eff");
/*     */     
/*  64 */     initializeLevelSpecificChances();
/*  65 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + AbstractDungeon.actNum * 100));
/*  66 */     generateMap();
/*     */     
/*  68 */     CardCrawlGame.music.changeBGM(id);
/*  69 */     AbstractDungeon.currMapNode = new MapRoomNode(0, -1);
/*  70 */     AbstractDungeon.currMapNode.room = new EmptyRoom();
/*     */   }
/*     */   
/*     */   public TheCity(AbstractPlayer p, SaveFile saveFile) {
/*  74 */     super(NAME, p, saveFile);
/*     */     
/*  76 */     if (scene != null) {
/*  77 */       scene.dispose();
/*     */     }
/*  79 */     scene = new TheCityScene();
/*  80 */     fadeColor = Color.valueOf("0a1e1eff");
/*     */     
/*  82 */     initializeLevelSpecificChances();
/*  83 */     miscRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.floor_num));
/*  84 */     CardCrawlGame.music.changeBGM(id);
/*  85 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.act_num * 100));
/*  86 */     generateMap();
/*  87 */     firstRoomChosen = true;
/*     */     
/*  89 */     populatePathTaken(saveFile);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeLevelSpecificChances()
/*     */   {
/*  95 */     shopRoomChance = 0.05F;
/*  96 */     restRoomChance = 0.12F;
/*  97 */     treasureRoomChance = 0.0F;
/*  98 */     eventRoomChance = 0.22F;
/*  99 */     eliteRoomChance = 0.08F;
/*     */     
/*     */ 
/* 102 */     smallChestChance = 50;
/* 103 */     mediumChestChance = 33;
/* 104 */     largeChestChance = 17;
/*     */     
/*     */ 
/* 107 */     commonRelicChance = 50;
/* 108 */     uncommonRelicChance = 33;
/* 109 */     rareRelicChance = 17;
/*     */     
/*     */ 
/* 112 */     colorlessRareChance = 0.3F;
/* 113 */     if (AbstractDungeon.ascensionLevel >= 12) {
/* 114 */       cardUpgradedChance = 0.125F;
/*     */     } else {
/* 116 */       cardUpgradedChance = 0.25F;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void generateMonsters()
/*     */   {
/* 123 */     ArrayList<MonsterInfo> monsters = new ArrayList();
/* 124 */     monsters.add(new MonsterInfo("Spheric Guardian", 2.0F));
/* 125 */     monsters.add(new MonsterInfo("Chosen", 2.0F));
/* 126 */     monsters.add(new MonsterInfo("Shell Parasite", 2.0F));
/* 127 */     monsters.add(new MonsterInfo("3 Byrds", 2.0F));
/* 128 */     monsters.add(new MonsterInfo("2 Thieves", 2.0F));
/* 129 */     MonsterInfo.normalizeWeights(monsters);
/* 130 */     populateMonsterList(monsters, 2, false);
/*     */     
/*     */ 
/* 133 */     monsters.clear();
/* 134 */     monsters.add(new MonsterInfo("Chosen and Byrds", 2.0F));
/* 135 */     monsters.add(new MonsterInfo("Sentry and Sphere", 2.0F));
/* 136 */     monsters.add(new MonsterInfo("Snake Plant", 6.0F));
/* 137 */     monsters.add(new MonsterInfo("Snecko", 4.0F));
/* 138 */     monsters.add(new MonsterInfo("Centurion and Healer", 6.0F));
/* 139 */     monsters.add(new MonsterInfo("Cultist and Chosen", 3.0F));
/* 140 */     monsters.add(new MonsterInfo("3 Cultists", 3.0F));
/* 141 */     monsters.add(new MonsterInfo("Shelled Parasite and Fungi", 3.0F));
/* 142 */     MonsterInfo.normalizeWeights(monsters);
/* 143 */     populateFirstStrongEnemy(monsters, generateExclusions());
/* 144 */     populateMonsterList(monsters, 12, false);
/*     */     
/*     */ 
/* 147 */     monsters.clear();
/* 148 */     monsters.add(new MonsterInfo("Gremlin Leader", 1.0F));
/* 149 */     monsters.add(new MonsterInfo("Slavers", 1.0F));
/* 150 */     monsters.add(new MonsterInfo("Book of Stabbing", 1.0F));
/* 151 */     MonsterInfo.normalizeWeights(monsters);
/* 152 */     populateMonsterList(monsters, 10, true);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 159 */     logger.info("Random counter: " + monsterRng.counter);
/*     */   }
/*     */   
/*     */   protected ArrayList<String> generateExclusions()
/*     */   {
/* 164 */     ArrayList<String> retVal = new ArrayList();
/* 165 */     switch ((String)monsterList.get(monsterList.size() - 1)) {
/*     */     case "Spheric Guardian": 
/* 167 */       retVal.add("Sentry and Sphere");
/* 168 */       break;
/*     */     case "3 Byrds": 
/* 170 */       retVal.add("Chosen and Byrds");
/* 171 */       break;
/*     */     case "Chosen": 
/* 173 */       retVal.add("Chosen and Byrds");
/* 174 */       retVal.add("Cultist and Chosen");
/* 175 */       break;
/*     */     }
/*     */     
/*     */     
/* 179 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void initializeBoss()
/*     */   {
/* 186 */     if (Settings.isDailyRun) {
/* 187 */       bossList.add("Automaton");
/* 188 */       bossList.add("Collector");
/* 189 */       bossList.add("Champ");
/* 190 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/* 192 */     else if (!UnlockTracker.isBossSeen("CHAMP")) {
/* 193 */       bossList.add("Champ");
/* 194 */     } else if (!UnlockTracker.isBossSeen("AUTOMATON")) {
/* 195 */       bossList.add("Automaton");
/* 196 */     } else if (!UnlockTracker.isBossSeen("COLLECTOR")) {
/* 197 */       bossList.add("Collector");
/*     */     } else {
/* 199 */       bossList.add("Automaton");
/* 200 */       bossList.add("Collector");
/* 201 */       bossList.add("Champ");
/* 202 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeEventList()
/*     */   {
/* 209 */     eventList.add("Addict");
/* 210 */     eventList.add("Back to Basics");
/* 211 */     eventList.add("Beggar");
/* 212 */     eventList.add("Colosseum");
/* 213 */     eventList.add("Cursed Tome");
/* 214 */     eventList.add("Drug Dealer");
/* 215 */     eventList.add("Forgotten Altar");
/* 216 */     eventList.add("Ghosts");
/* 217 */     eventList.add("Masked Bandits");
/* 218 */     eventList.add("Nest");
/* 219 */     eventList.add("The Library");
/* 220 */     eventList.add("The Mausoleum");
/* 221 */     eventList.add("Vampires");
/*     */   }
/*     */   
/*     */   protected void initializeEventImg()
/*     */   {
/* 226 */     if (eventBackgroundImg != null) {
/* 227 */       eventBackgroundImg.dispose();
/* 228 */       eventBackgroundImg = null;
/*     */     }
/* 230 */     eventBackgroundImg = ImageMaster.loadImage("images/ui/event/panel.png");
/*     */   }
/*     */   
/*     */   protected void initializeShrineList()
/*     */   {
/* 235 */     shrineList.add("Match and Keep!");
/* 236 */     shrineList.add("Wheel of Change");
/* 237 */     shrineList.add("Golden Shrine");
/* 238 */     shrineList.add("Transmorgrifier");
/* 239 */     shrineList.add("Purifier");
/* 240 */     shrineList.add("Upgrade Shrine");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\dungeons\TheCity.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */