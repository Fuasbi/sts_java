/*      */ package com.megacrit.cardcrawl.dungeons;
/*      */ 
/*      */ import com.badlogic.gdx.Application;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.Interpolation;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.badlogic.gdx.math.RandomXS128;
/*      */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*      */ import com.megacrit.cardcrawl.cards.SoulGroup;
/*      */ import com.megacrit.cardcrawl.cards.colorless.SwiftStrike;
/*      */ import com.megacrit.cardcrawl.cards.curses.AscendersBane;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.ExceptionHandler;
/*      */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.credits.CreditsScreen;
/*      */ import com.megacrit.cardcrawl.daily.DailyMods;
/*      */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*      */ import com.megacrit.cardcrawl.events.AbstractImageEvent;
/*      */ import com.megacrit.cardcrawl.events.shrines.NoteForYourself;
/*      */ import com.megacrit.cardcrawl.helpers.BlightHelper;
/*      */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*      */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.EventHelper;
/*      */ import com.megacrit.cardcrawl.helpers.EventHelper.RoomResult;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*      */ import com.megacrit.cardcrawl.helpers.PotionHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Prefs;
/*      */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*      */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.map.DungeonMap;
/*      */ import com.megacrit.cardcrawl.map.MapEdge;
/*      */ import com.megacrit.cardcrawl.map.MapGenerator;
/*      */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*      */ import com.megacrit.cardcrawl.map.RoomTypeAssigner;
/*      */ import com.megacrit.cardcrawl.metrics.MetricData;
/*      */ import com.megacrit.cardcrawl.metrics.Metrics;
/*      */ import com.megacrit.cardcrawl.metrics.Metrics.MetricRequestType;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterInfo;
/*      */ import com.megacrit.cardcrawl.neow.NeowRoom;
/*      */ import com.megacrit.cardcrawl.neow.NeowUnlockScreen;
/*      */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*      */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*      */ import com.megacrit.cardcrawl.potions.AbstractPotion.PotionRarity;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*      */ import com.megacrit.cardcrawl.relics.Girya;
/*      */ import com.megacrit.cardcrawl.relics.PeacePipe;
/*      */ import com.megacrit.cardcrawl.relics.Shovel;
/*      */ import com.megacrit.cardcrawl.rewards.chests.AbstractChest;
/*      */ import com.megacrit.cardcrawl.rewards.chests.CursedChest;
/*      */ import com.megacrit.cardcrawl.rewards.chests.LargeChest;
/*      */ import com.megacrit.cardcrawl.rewards.chests.MediumChest;
/*      */ import com.megacrit.cardcrawl.rewards.chests.SmallChest;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*      */ import com.megacrit.cardcrawl.rooms.EmptyRoom;
/*      */ import com.megacrit.cardcrawl.rooms.EventRoom;
/*      */ import com.megacrit.cardcrawl.rooms.MonsterRoom;
/*      */ import com.megacrit.cardcrawl.rooms.MonsterRoomBoss;
/*      */ import com.megacrit.cardcrawl.rooms.MonsterRoomElite;
/*      */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*      */ import com.megacrit.cardcrawl.rooms.ShopRoom;
/*      */ import com.megacrit.cardcrawl.rooms.TreasureRoom;
/*      */ import com.megacrit.cardcrawl.rooms.TreasureRoomBoss;
/*      */ import com.megacrit.cardcrawl.rooms.VictoryRoom;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveAndContinue;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*      */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType;
/*      */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*      */ import com.megacrit.cardcrawl.screens.CardRewardScreen;
/*      */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*      */ import com.megacrit.cardcrawl.screens.DeathScreen;
/*      */ import com.megacrit.cardcrawl.screens.DiscardPileViewScreen;
/*      */ import com.megacrit.cardcrawl.screens.DrawPileViewScreen;
/*      */ import com.megacrit.cardcrawl.screens.DungeonMapScreen;
/*      */ import com.megacrit.cardcrawl.screens.ExhaustPileViewScreen;
/*      */ import com.megacrit.cardcrawl.screens.MasterDeckViewScreen;
/*      */ import com.megacrit.cardcrawl.screens.options.ConfirmPopup;
/*      */ import com.megacrit.cardcrawl.screens.options.InputSettingsScreen;
/*      */ import com.megacrit.cardcrawl.screens.options.SettingsScreen;
/*      */ import com.megacrit.cardcrawl.screens.select.BossRelicSelectScreen;
/*      */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*      */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*      */ import com.megacrit.cardcrawl.screens.stats.StatsScreen;
/*      */ import com.megacrit.cardcrawl.shop.ShopScreen;
/*      */ import com.megacrit.cardcrawl.ui.FtueTip;
/*      */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*      */ import com.megacrit.cardcrawl.ui.buttons.DynamicBanner;
/*      */ import com.megacrit.cardcrawl.ui.buttons.DynamicButton;
/*      */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*      */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*      */ import com.megacrit.cardcrawl.unlock.AbstractUnlock;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockCharacterScreen;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*      */ import com.megacrit.cardcrawl.vfx.PlayerTurnEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Collections;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import java.util.Map.Entry;
/*      */ import java.util.Objects;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public abstract class AbstractDungeon
/*      */ {
/*  173 */   private static final Logger logger = LogManager.getLogger(AbstractDungeon.class.getName());
/*      */   
/*  175 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractDungeon");
/*  176 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   public static String name;
/*      */   public static String levelNum;
/*      */   public static String id;
/*  180 */   public static int floorNum = 0;
/*  181 */   public static int actNum = 0;
/*      */   public static AbstractPlayer player;
/*  183 */   public static ArrayList<AbstractUnlock> unlocks = new ArrayList();
/*  184 */   protected static float shrineChance = 0.25F;
/*  185 */   private static boolean firstChest = true; private static boolean encounteredCursedChest = false;
/*      */   protected static float cardUpgradedChance;
/*      */   public static AbstractCard transformedCard;
/*  188 */   public static boolean loading_post_combat = false;
/*      */   
/*      */   public static Texture eventBackgroundImg;
/*      */   
/*      */   public static com.megacrit.cardcrawl.random.Random monsterRng;
/*      */   
/*      */   public static com.megacrit.cardcrawl.random.Random mapRng;
/*      */   
/*      */   public static com.megacrit.cardcrawl.random.Random eventRng;
/*      */   
/*      */   public static com.megacrit.cardcrawl.random.Random merchantRng;
/*      */   
/*      */   public static com.megacrit.cardcrawl.random.Random cardRng;
/*      */   public static com.megacrit.cardcrawl.random.Random treasureRng;
/*      */   public static com.megacrit.cardcrawl.random.Random relicRng;
/*      */   public static com.megacrit.cardcrawl.random.Random potionRng;
/*      */   public static com.megacrit.cardcrawl.random.Random monsterHpRng;
/*      */   public static com.megacrit.cardcrawl.random.Random aiRng;
/*      */   public static com.megacrit.cardcrawl.random.Random shuffleRng;
/*      */   public static com.megacrit.cardcrawl.random.Random cardRandomRng;
/*      */   public static com.megacrit.cardcrawl.random.Random miscRng;
/*  209 */   public static CardGroup srcColorlessCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  210 */   public static CardGroup srcCurseCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  211 */   public static CardGroup srcCommonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  212 */   public static CardGroup srcUncommonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  213 */   public static CardGroup srcRareCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  214 */   public static CardGroup colorlessCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  215 */   public static CardGroup curseCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  216 */   public static CardGroup commonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  217 */   public static CardGroup uncommonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*  218 */   public static CardGroup rareCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*      */   
/*      */ 
/*  221 */   public static ArrayList<String> commonRelicPool = new ArrayList();
/*  222 */   public static ArrayList<String> uncommonRelicPool = new ArrayList();
/*  223 */   public static ArrayList<String> rareRelicPool = new ArrayList();
/*  224 */   public static ArrayList<String> shopRelicPool = new ArrayList();
/*  225 */   public static ArrayList<String> bossRelicPool = new ArrayList();
/*      */   
/*      */ 
/*  228 */   public static String lastCombatMetricKey = null;
/*  229 */   public static ArrayList<String> monsterList = new ArrayList();
/*  230 */   public static ArrayList<String> eliteMonsterList = new ArrayList();
/*  231 */   public static ArrayList<String> bossList = new ArrayList();
/*      */   
/*      */   public static String bossKey;
/*      */   
/*  235 */   public static ArrayList<String> eventList = new ArrayList();
/*  236 */   public static ArrayList<String> shrineList = new ArrayList();
/*  237 */   public static ArrayList<String> specialOneTimeEventList = new ArrayList();
/*  238 */   public static GameActionManager actionManager = new GameActionManager();
/*  239 */   public static ArrayList<AbstractGameEffect> topLevelEffects = new ArrayList();
/*  240 */   public static ArrayList<AbstractGameEffect> topLevelEffectsQueue = new ArrayList();
/*  241 */   public static ArrayList<AbstractGameEffect> effectList = new ArrayList();
/*  242 */   public static ArrayList<AbstractGameEffect> effectsQueue = new ArrayList();
/*  243 */   public static boolean turnPhaseEffectActive = false;
/*      */   public static float colorlessRareChance;
/*      */   protected static float shopRoomChance;
/*      */   protected static float restRoomChance;
/*      */   protected static float eventRoomChance;
/*      */   protected static float eliteRoomChance;
/*      */   protected static float treasureRoomChance;
/*      */   protected static int smallChestChance;
/*      */   protected static int mediumChestChance;
/*      */   protected static int largeChestChance;
/*      */   protected static int commonRelicChance;
/*  254 */   protected static int uncommonRelicChance; protected static int rareRelicChance; public static AbstractScene scene; public static MapRoomNode currMapNode; public static ArrayList<ArrayList<MapRoomNode>> map; public static boolean leftRoomAvailable; public static boolean centerRoomAvailable; public static boolean rightRoomAvailable; public static boolean firstRoomChosen = false;
/*      */   public static final int MAP_HEIGHT = 15;
/*  256 */   public static final int MAP_WIDTH = 7; public static final int MAP_DENSITY = 6; public static RenderScene rs = RenderScene.NORMAL;
/*  257 */   public static ArrayList<Integer> pathX = new ArrayList();
/*  258 */   public static ArrayList<Integer> pathY = new ArrayList();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*  263 */   public static Color topGradientColor = new Color(1.0F, 1.0F, 1.0F, 0.25F);
/*  264 */   public static Color botGradientColor = new Color(1.0F, 1.0F, 1.0F, 0.25F);
/*  265 */   public static float floorY = 340.0F * Settings.scale;
/*  266 */   public static TopPanel topPanel = new TopPanel();
/*  267 */   public static CardRewardScreen cardRewardScreen = new CardRewardScreen();
/*  268 */   public static CombatRewardScreen combatRewardScreen = new CombatRewardScreen();
/*  269 */   public static BossRelicSelectScreen bossRelicScreen = new BossRelicSelectScreen();
/*  270 */   public static MasterDeckViewScreen deckViewScreen = new MasterDeckViewScreen();
/*  271 */   public static DiscardPileViewScreen discardPileViewScreen = new DiscardPileViewScreen();
/*  272 */   public static DrawPileViewScreen gameDeckViewScreen = new DrawPileViewScreen();
/*  273 */   public static ExhaustPileViewScreen exhaustPileViewScreen = new ExhaustPileViewScreen();
/*  274 */   public static SettingsScreen settingsScreen = new SettingsScreen();
/*  275 */   public static InputSettingsScreen inputSettingsScreen = new InputSettingsScreen();
/*  276 */   public static DungeonMapScreen dungeonMapScreen = new DungeonMapScreen();
/*  277 */   public static GridCardSelectScreen gridSelectScreen = new GridCardSelectScreen();
/*  278 */   public static HandCardSelectScreen handCardSelectScreen = new HandCardSelectScreen();
/*  279 */   public static ShopScreen shopScreen = new ShopScreen();
/*  280 */   public static CreditsScreen creditsScreen = null;
/*  281 */   public static FtueTip ftue = null;
/*      */   public static DeathScreen deathScreen;
/*  283 */   public static UnlockCharacterScreen unlockScreen = new UnlockCharacterScreen();
/*  284 */   public static NeowUnlockScreen gUnlockScreen = new NeowUnlockScreen();
/*  285 */   public static boolean isScreenUp = false;
/*      */   public static OverlayMenu overlayMenu;
/*      */   public static CurrentScreen screen;
/*      */   public static CurrentScreen previousScreen;
/*      */   public static DynamicBanner dynamicBanner;
/*  290 */   public static DynamicButton dynamicButton; public static boolean screenSwap = false;
/*      */   
/*      */ 
/*      */   public static boolean isDungeonBeaten;
/*      */   
/*      */ 
/*  296 */   public static int cardBlizzStartOffset = 5;
/*  297 */   public static int cardBlizzRandomizer = cardBlizzStartOffset;
/*  298 */   public static int cardBlizzGrowth = 1;
/*  299 */   public static int cardBlizzMaxOffset = -40;
/*      */   
/*      */   public static boolean isFadingIn;
/*      */   public static boolean isFadingOut;
/*      */   public static boolean waitingOnFadeOut;
/*      */   protected static float fadeTimer;
/*      */   public static Color fadeColor;
/*      */   public static MapRoomNode nextRoom;
/*      */   public static float sceneOffsetY;
/*      */   public static float sceneOffsetTimer;
/*  309 */   public static ArrayList<String> relicsToRemoveOnStart = new ArrayList();
/*  310 */   public static int bossCount = 0;
/*      */   
/*      */   public static final float SCENE_OFFSET_TIME = 1.3F;
/*      */   
/*  314 */   public static boolean isAscensionMode = false;
/*  315 */   public static int ascensionLevel = 0;
/*      */   
/*      */ 
/*  318 */   public static ArrayList<AbstractBlight> blightPool = new ArrayList();
/*      */   
/*      */   public static boolean ascensionCheck;
/*      */   
/*  322 */   private static final Logger LOGGER = LogManager.getLogger(AbstractDungeon.class.getName());
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AbstractDungeon(String name, String levelId, AbstractPlayer p, ArrayList<String> newSpecialOneTimeEventList)
/*      */   {
/*  330 */     ascensionCheck = UnlockTracker.isAscensionUnlocked(p.chosenClass);
/*  331 */     CardCrawlGame.dungeon = this;
/*  332 */     long startTime = System.currentTimeMillis();
/*  333 */     name = name;
/*  334 */     id = levelId;
/*  335 */     player = p;
/*  336 */     topPanel.setPlayerName();
/*  337 */     actionManager = new GameActionManager();
/*  338 */     overlayMenu = new OverlayMenu(p);
/*  339 */     dynamicBanner = new DynamicBanner();
/*  340 */     dynamicButton = new DynamicButton();
/*  341 */     unlocks.clear();
/*      */     
/*  343 */     specialOneTimeEventList = newSpecialOneTimeEventList;
/*      */     
/*      */ 
/*  346 */     isFadingIn = false;
/*  347 */     isFadingOut = false;
/*  348 */     waitingOnFadeOut = false;
/*  349 */     fadeTimer = 1.0F;
/*  350 */     isDungeonBeaten = false;
/*  351 */     isScreenUp = false;
/*      */     
/*  353 */     dungeonTransitionSetup();
/*  354 */     generateMonsters();
/*  355 */     initializeBoss();
/*  356 */     setBoss((String)bossList.get(0));
/*  357 */     initializeEventList();
/*  358 */     initializeEventImg();
/*  359 */     initializeShrineList();
/*  360 */     initializeCardPools();
/*  361 */     initializePotions();
/*  362 */     BlightHelper.initialize();
/*      */     
/*  364 */     if (id.equals("Exordium")) {
/*  365 */       screen = CurrentScreen.NONE;
/*  366 */       isScreenUp = false;
/*      */     } else {
/*  368 */       screen = CurrentScreen.MAP;
/*  369 */       isScreenUp = true;
/*      */     }
/*      */     
/*  372 */     logger.info("Content generation time: " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */   public AbstractDungeon(String name, AbstractPlayer p, SaveFile saveFile)
/*      */   {
/*  377 */     ascensionCheck = UnlockTracker.isAscensionUnlocked(p.chosenClass);
/*  378 */     id = saveFile.level_name;
/*  379 */     CardCrawlGame.dungeon = this;
/*  380 */     long startTime = System.currentTimeMillis();
/*  381 */     name = name;
/*  382 */     player = p;
/*  383 */     topPanel.setPlayerName();
/*  384 */     actionManager = new GameActionManager();
/*  385 */     overlayMenu = new OverlayMenu(p);
/*  386 */     dynamicBanner = new DynamicBanner();
/*  387 */     dynamicButton = new DynamicButton();
/*  388 */     isFadingIn = false;
/*  389 */     isFadingOut = false;
/*  390 */     waitingOnFadeOut = false;
/*  391 */     fadeTimer = 1.0F;
/*  392 */     isDungeonBeaten = false;
/*  393 */     isScreenUp = false;
/*  394 */     firstRoomChosen = true;
/*  395 */     unlocks.clear();
/*      */     try
/*      */     {
/*  398 */       loadSave(saveFile);
/*      */     } catch (Exception e) {
/*  400 */       logger.info("Deleting save due to crash...");
/*  401 */       SaveAndContinue.deleteSave(player.chosenClass);
/*  402 */       ExceptionHandler.handleException(e, LOGGER);
/*  403 */       LogManager.shutdown();
/*  404 */       Gdx.app.exit();
/*      */     }
/*      */     
/*  407 */     initializeEventImg();
/*  408 */     initializeShrineList();
/*  409 */     initializeCardPools();
/*  410 */     initializePotions();
/*  411 */     BlightHelper.initialize();
/*  412 */     screen = CurrentScreen.NONE;
/*  413 */     isScreenUp = false;
/*      */     
/*  415 */     logger.info("Dungeon load time: " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */   private void setBoss(String key) {
/*  419 */     bossKey = key;
/*      */     
/*  421 */     if (key.equals("The Guardian")) {
/*  422 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/guardian.png");
/*  423 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/guardian.png");
/*  424 */     } else if (key.equals("Hexaghost")) {
/*  425 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/hexaghost.png");
/*  426 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/hexaghost.png");
/*  427 */     } else if (key.equals("Slime Boss")) {
/*  428 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/slime.png");
/*  429 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/slime.png");
/*  430 */     } else if (key.equals("Collector")) {
/*  431 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/collector.png");
/*  432 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/collector.png");
/*  433 */     } else if (key.equals("Automaton")) {
/*  434 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/automaton.png");
/*  435 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/automaton.png");
/*  436 */     } else if (key.equals("Champ")) {
/*  437 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/champ.png");
/*  438 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/champ.png");
/*  439 */     } else if (key.equals("Awakened One")) {
/*  440 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/awakened.png");
/*  441 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/awakened.png");
/*  442 */     } else if (key.equals("Time Eater")) {
/*  443 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/timeeater.png");
/*  444 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/timeeater.png");
/*  445 */     } else if (key.equals("Donu and Deca")) {
/*  446 */       DungeonMap.boss = ImageMaster.loadImage("images/ui/map/boss/donu.png");
/*  447 */       DungeonMap.bossOutline = ImageMaster.loadImage("images/ui/map/bossOutline/donu.png");
/*      */     } else {
/*  449 */       logger.info("WARNING: UNKNOWN BOSS ICON: " + key);
/*  450 */       DungeonMap.boss = null;
/*      */     }
/*  452 */     logger.info("[BOSS] " + (String)bossList.get(0));
/*      */   }
/*      */   
/*      */   protected abstract void initializeLevelSpecificChances();
/*      */   
/*      */   public static void generateSeeds() {
/*  458 */     logger.info("Generating seeds: " + Settings.seed);
/*  459 */     monsterRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  460 */     eventRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  461 */     merchantRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  462 */     cardRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  463 */     treasureRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  464 */     relicRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  465 */     monsterHpRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  466 */     potionRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  467 */     aiRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  468 */     shuffleRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  469 */     cardRandomRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*  470 */     miscRng = new com.megacrit.cardcrawl.random.Random(Settings.seed);
/*      */   }
/*      */   
/*      */   public static void loadSeeds(SaveFile save) {
/*  474 */     if ((save.is_daily) || (save.is_trial)) {
/*  475 */       Settings.isDailyRun = save.is_daily;
/*  476 */       Settings.isTrial = save.is_trial;
/*  477 */       Settings.specialSeed = Long.valueOf(save.special_seed);
/*  478 */       if (save.is_daily) {
/*  479 */         Settings.dailyMods.initialize(save.special_seed);
/*      */       } else {
/*  481 */         Settings.dailyMods.initialize(save.seed);
/*      */       }
/*      */     }
/*  484 */     monsterRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.monster_seed_count);
/*  485 */     eventRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.event_seed_count);
/*  486 */     merchantRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.merchant_seed_count);
/*  487 */     cardRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.card_seed_count);
/*  488 */     cardBlizzRandomizer = save.card_random_seed_randomizer;
/*  489 */     treasureRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.treasure_seed_count);
/*  490 */     relicRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.relic_seed_count);
/*  491 */     potionRng = new com.megacrit.cardcrawl.random.Random(Settings.seed, save.potion_seed_count);
/*      */     
/*      */ 
/*  494 */     logger.info("Loading seeds: " + Settings.seed);
/*  495 */     logger.info("Monster seed:  " + monsterRng.counter);
/*  496 */     logger.info("Event seed:    " + eventRng.counter);
/*  497 */     logger.info("Merchant seed: " + merchantRng.counter);
/*  498 */     logger.info("Card seed:     " + cardRng.counter);
/*  499 */     logger.info("Treasure seed: " + treasureRng.counter);
/*  500 */     logger.info("Relic seed:    " + relicRng.counter);
/*  501 */     logger.info("Potion seed:   " + potionRng.counter);
/*      */   }
/*      */   
/*      */   public void populatePathTaken(SaveFile saveFile) {
/*  505 */     MapRoomNode node = null;
/*      */     
/*  507 */     if (saveFile.current_room.equals(MonsterRoomBoss.class.getName())) {
/*  508 */       node = new MapRoomNode(-1, 15);
/*  509 */       node.room = new MonsterRoomBoss();
/*  510 */       nextRoom = node;
/*  511 */     } else if (saveFile.current_room.equals(TreasureRoomBoss.class.getName())) {
/*  512 */       node = new MapRoomNode(-1, 15);
/*  513 */       node.room = new TreasureRoomBoss();
/*  514 */       nextRoom = node;
/*      */ 
/*      */     }
/*  517 */     else if ((saveFile.room_y == 15) && (saveFile.room_x == -1)) {
/*  518 */       node = new MapRoomNode(-1, 15);
/*  519 */       node.room = new VictoryRoom();
/*  520 */       nextRoom = node;
/*      */ 
/*      */     }
/*  523 */     else if (saveFile.current_room.equals(NeowRoom.class.getName())) {
/*  524 */       nextRoom = null;
/*      */     } else {
/*  526 */       nextRoom = (MapRoomNode)((ArrayList)map.get(saveFile.room_y)).get(saveFile.room_x);
/*      */     }
/*      */     
/*      */ 
/*  530 */     for (int i = 0; i < pathX.size(); i++) {
/*  531 */       if (((Integer)pathY.get(i)).intValue() == 14) {
/*  532 */         MapRoomNode node2 = (MapRoomNode)((ArrayList)map.get(((Integer)pathY.get(i)).intValue())).get(((Integer)pathX.get(i)).intValue());
/*  533 */         for (MapEdge e : node2.getEdges()) {
/*  534 */           if (e != null) {
/*  535 */             e.markAsTaken();
/*      */           }
/*      */         }
/*      */       }
/*  539 */       if (((Integer)pathY.get(i)).intValue() < 15) {
/*  540 */         ((MapRoomNode)((ArrayList)map.get(((Integer)pathY.get(i)).intValue())).get(((Integer)pathX.get(i)).intValue())).taken = true;
/*  541 */         if (node != null) {
/*  542 */           MapEdge connectedEdge = node.getEdgeConnectedTo(
/*  543 */             (MapRoomNode)((ArrayList)map.get(((Integer)pathY.get(i)).intValue())).get(((Integer)pathX.get(i)).intValue()));
/*  544 */           if (connectedEdge != null) {
/*  545 */             connectedEdge.markAsTaken();
/*      */           }
/*      */         }
/*  548 */         node = (MapRoomNode)((ArrayList)map.get(((Integer)pathY.get(i)).intValue())).get(((Integer)pathX.get(i)).intValue());
/*      */       }
/*      */     }
/*      */     
/*  552 */     if (isLoadingIntoNeow(saveFile))
/*      */     {
/*  554 */       logger.info("Loading into Neow");
/*  555 */       currMapNode = new MapRoomNode(0, -1);
/*  556 */       currMapNode.room = new EmptyRoom();
/*  557 */       nextRoom = null;
/*      */     }
/*      */     else {
/*  560 */       logger.info("Loading into: " + saveFile.room_x + "," + saveFile.room_y);
/*  561 */       currMapNode = new MapRoomNode(0, -1);
/*  562 */       currMapNode.room = new EmptyRoom();
/*      */     }
/*      */     
/*  565 */     nextRoomTransition();
/*      */     
/*      */ 
/*  568 */     if (isLoadingIntoNeow(saveFile)) {
/*  569 */       if (saveFile.chose_neow_reward) {
/*  570 */         currMapNode.room = new NeowRoom(true);
/*      */       } else {
/*  572 */         currMapNode.room = new NeowRoom(false);
/*  573 */         CardCrawlGame.playerPref.putInteger(player.chosenClass.name() + "_SPIRITS", 1);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   protected boolean isLoadingIntoNeow(SaveFile saveFile) {
/*  579 */     return (floorNum == 0) || (saveFile.current_room.equals(NeowRoom.class.getName()));
/*      */   }
/*      */   
/*      */   public static AbstractChest getRandomChest() {
/*  583 */     int roll = treasureRng.random(0, 99);
/*      */     
/*  585 */     if (roll < smallChestChance) {
/*  586 */       firstChest = false;
/*  587 */       return new SmallChest(); }
/*  588 */     if (roll < mediumChestChance + smallChestChance) {
/*  589 */       firstChest = false;
/*  590 */       return new MediumChest(); }
/*  591 */     if (roll < largeChestChance + mediumChestChance + smallChestChance) {
/*  592 */       firstChest = false;
/*  593 */       return new LargeChest();
/*      */     }
/*  595 */     if (firstChest) {
/*  596 */       firstChest = false;
/*  597 */       return new SmallChest();
/*      */     }
/*  599 */     if (!encounteredCursedChest) {
/*  600 */       encounteredCursedChest = true;
/*  601 */       return new CursedChest();
/*      */     }
/*  603 */     return new SmallChest();
/*      */   }
/*      */   
/*      */ 
/*      */   protected static void generateMap()
/*      */   {
/*  609 */     if (player.hasRelic("Membership Card")) {
/*  610 */       shopRoomChance *= 1.5F;
/*      */     }
/*      */     
/*  613 */     long startTime = System.currentTimeMillis();
/*      */     
/*  615 */     int mapHeight = 15;
/*  616 */     int mapWidth = 7;
/*  617 */     int mapPathDensity = 6;
/*      */     
/*  619 */     ArrayList<AbstractRoom> roomList = new ArrayList();
/*  620 */     map = MapGenerator.generateDungeon(mapHeight, mapWidth, mapPathDensity, mapRng);
/*      */     
/*      */ 
/*  623 */     int count = 0;
/*  624 */     for (ArrayList<MapRoomNode> a : map) {
/*  625 */       for (MapRoomNode n : a) {
/*  626 */         if ((n.hasEdges()) && 
/*  627 */           (n.y != map.size() - 2))
/*      */         {
/*      */ 
/*  630 */           count++;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  636 */     generateRoomTypes(roomList, count);
/*      */     
/*  638 */     RoomTypeAssigner.assignRowAsRoomType((ArrayList)map.get(map.size() - 1), RestRoom.class);
/*  639 */     RoomTypeAssigner.assignRowAsRoomType((ArrayList)map.get(0), MonsterRoom.class);
/*  640 */     if ((Settings.isEndless) && (player.hasBlight("MimicInfestation"))) {
/*  641 */       RoomTypeAssigner.assignRowAsRoomType((ArrayList)map.get(8), MonsterRoomElite.class);
/*      */     } else {
/*  643 */       RoomTypeAssigner.assignRowAsRoomType((ArrayList)map.get(8), TreasureRoom.class);
/*      */     }
/*  645 */     map = RoomTypeAssigner.distributeRoomsAcrossMap(mapRng, map, roomList);
/*      */     
/*  647 */     logger.info("Generated the following dungeon map:");
/*  648 */     logger.info(MapGenerator.toString(map, Boolean.valueOf(true)));
/*  649 */     logger.info("Game Seed: " + Settings.seed);
/*  650 */     logger.info("Map generation time: " + (System.currentTimeMillis() - startTime) + "ms");
/*  651 */     firstRoomChosen = false;
/*      */     
/*  653 */     fadeIn();
/*      */   }
/*      */   
/*      */   private static void generateRoomTypes(ArrayList<AbstractRoom> roomList, int availableRoomCount) {
/*  657 */     logger.info("Generating Room Types! There are " + availableRoomCount + " rooms:");
/*  658 */     int shopCount = Math.round(availableRoomCount * shopRoomChance);
/*  659 */     logger.info(" SHOP (" + toPercentage(shopRoomChance) + "): " + shopCount);
/*  660 */     int restCount = Math.round(availableRoomCount * restRoomChance);
/*  661 */     logger.info(" REST (" + toPercentage(restRoomChance) + "): " + restCount);
/*  662 */     int treasureCount = Math.round(availableRoomCount * treasureRoomChance);
/*  663 */     logger.info(" TRSRE (" + toPercentage(treasureRoomChance) + "): " + treasureCount);
/*      */     
/*      */     int eliteCount;
/*      */     
/*  667 */     if (((Boolean)DailyMods.negativeMods.get("Elite Swarm")).booleanValue()) {
/*  668 */       int eliteCount = Math.round(availableRoomCount * (eliteRoomChance * 2.5F));
/*  669 */       logger.info(" ELITE (" + toPercentage(eliteRoomChance) + "): " + eliteCount);
/*  670 */     } else if (ascensionLevel >= 1) {
/*  671 */       int eliteCount = Math.round(availableRoomCount * eliteRoomChance * 1.6F);
/*  672 */       logger.info(" ELITE (" + toPercentage(eliteRoomChance) + "): " + eliteCount);
/*      */     }
/*      */     else {
/*  675 */       eliteCount = Math.round(availableRoomCount * eliteRoomChance);
/*  676 */       logger.info(" ELITE (" + toPercentage(eliteRoomChance) + "): " + eliteCount);
/*      */     }
/*      */     
/*      */ 
/*  680 */     int eventCount = Math.round(availableRoomCount * eventRoomChance);
/*  681 */     logger.info(" EVNT (" + toPercentage(eventRoomChance) + "): " + eventCount);
/*  682 */     int monsterCount = availableRoomCount - shopCount - restCount - treasureCount - eliteCount - eventCount;
/*  683 */     logger.info(" MSTR (" + 
/*  684 */       toPercentage(1.0F - shopRoomChance - restRoomChance - treasureRoomChance - eliteRoomChance - eventRoomChance) + "): " + monsterCount);
/*      */     
/*      */ 
/*      */ 
/*  688 */     for (int i = 0; i < shopCount; i++) {
/*  689 */       roomList.add(new ShopRoom());
/*      */     }
/*  691 */     for (int i = 0; i < restCount; i++) {
/*  692 */       roomList.add(new RestRoom());
/*      */     }
/*  694 */     for (int i = 0; i < eliteCount; i++) {
/*  695 */       roomList.add(new MonsterRoomElite());
/*      */     }
/*  697 */     for (int i = 0; i < eventCount; i++) {
/*  698 */       roomList.add(new EventRoom());
/*      */     }
/*      */   }
/*      */   
/*      */   private static String toPercentage(float n) {
/*  703 */     return String.format("%.0f", new Object[] { Float.valueOf(n * 100.0F) }) + "%";
/*      */   }
/*      */   
/*      */   private static void firstRoomLogic()
/*      */   {
/*  708 */     initializeFirstRoom();
/*      */     
/*      */ 
/*      */ 
/*  712 */     leftRoomAvailable = currMapNode.leftNodeAvailable();
/*  713 */     centerRoomAvailable = currMapNode.centerNodeAvailable();
/*  714 */     rightRoomAvailable = currMapNode.rightNodeAvailable();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private boolean passesDonutCheck(ArrayList<ArrayList<MapRoomNode>> map)
/*      */   {
/*  725 */     logger.info("CASEY'S DONUT CHECK: ");
/*  726 */     int width = ((ArrayList)map.get(0)).size();
/*  727 */     int height = map.size();
/*  728 */     logger.info(" HEIGHT: " + height);
/*  729 */     logger.info(" WIDTH:  " + width);
/*      */     
/*      */ 
/*  732 */     int nodeCount = 0;
/*  733 */     boolean[] roomHasNode = new boolean[width];
/*  734 */     for (int i = 0; i < width; i++) {
/*  735 */       roomHasNode[i] = false;
/*      */     }
/*  737 */     ArrayList<MapRoomNode> secondToLastRow = (ArrayList)map.get(map.size() - 2);
/*  738 */     for (Iterator localIterator1 = secondToLastRow.iterator(); localIterator1.hasNext();) { n = (MapRoomNode)localIterator1.next();
/*  739 */       for (localIterator2 = n.getEdges().iterator(); localIterator2.hasNext();) { e = (MapEdge)localIterator2.next();
/*  740 */         roomHasNode[e.dstX] = true; } }
/*      */     MapRoomNode n;
/*      */     Iterator localIterator2;
/*  743 */     MapEdge e; for (int i = 0; i < width - 1; i++) {
/*  744 */       if (roomHasNode[i] != 0) {
/*  745 */         nodeCount++;
/*      */       }
/*      */     }
/*  748 */     if (nodeCount == 1) {
/*  749 */       logger.info(" [SUCCESS] " + nodeCount + " NODE IN LAST ROW");
/*      */     } else {
/*  751 */       logger.info(" [FAIL] " + nodeCount + " NODES IN LAST ROW");
/*  752 */       return false;
/*      */     }
/*      */     
/*      */ 
/*  756 */     int roomCount = 0;
/*  757 */     for (Object rows : map) {
/*  758 */       for (MapRoomNode n : (ArrayList)rows) {
/*  759 */         if (n.room != null) {
/*  760 */           roomCount++;
/*      */         }
/*      */       }
/*      */     }
/*  764 */     logger.info(" ROOM COUNT: " + roomCount);
/*      */     
/*      */ 
/*      */ 
/*  768 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractRoom getCurrRoom()
/*      */   {
/*  778 */     return currMapNode.getRoom();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static MapRoomNode getCurrMapNode()
/*      */   {
/*  787 */     return currMapNode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void setCurrMapNode(MapRoomNode currMapNode)
/*      */   {
/*  796 */     SoulGroup group = currMapNode.room.souls;
/*  797 */     currMapNode = currMapNode;
/*      */     
/*  799 */     if (currMapNode.room == null) {
/*  800 */       logger.warn("This played loaded into a room that no longer exists (due to a new map gen?)");
/*  801 */       for (int i = 0; i < 5; i++) {
/*  802 */         if (((MapRoomNode)((ArrayList)map.get(currMapNode.y)).get(i)).room != null) {
/*  803 */           currMapNode = (MapRoomNode)((ArrayList)map.get(currMapNode.y)).get(i);
/*  804 */           currMapNode.room = ((MapRoomNode)((ArrayList)map.get(currMapNode.y)).get(i)).room;
/*  805 */           nextRoom.room = ((MapRoomNode)((ArrayList)map.get(currMapNode.y)).get(i)).room;
/*  806 */           break;
/*      */         }
/*      */       }
/*      */     } else {
/*  810 */       currMapNode.room.souls = group;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static ArrayList<MapRoomNode> identifyAvailableNodes(ArrayList<MapRoomNode> nodes)
/*      */   {
/*  821 */     ArrayList<MapRoomNode> nodesWithEdges = new ArrayList();
/*  822 */     for (MapRoomNode node : nodes) {
/*  823 */       if (node.hasEdges()) {
/*  824 */         nodesWithEdges.add(node);
/*      */       }
/*      */     }
/*  827 */     return nodesWithEdges;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static MapRoomNode getRandomNodeWithEdges(com.megacrit.cardcrawl.random.Random rng, ArrayList<MapRoomNode> nodes)
/*      */   {
/*      */     MapRoomNode node;
/*      */     
/*      */     MapRoomNode node;
/*      */     
/*  838 */     if (nodes.size() == 1) {
/*  839 */       node = (MapRoomNode)nodes.get(0);
/*      */     } else {
/*  841 */       node = (MapRoomNode)nodes.get(rng.random.nextInt(nodes.size()));
/*      */     }
/*  843 */     if (node.hasEdges()) {
/*  844 */       return node;
/*      */     }
/*  846 */     return null;
/*      */   }
/*      */   
/*      */   public ArrayList<ArrayList<MapRoomNode>> getMap()
/*      */   {
/*  851 */     return map;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractRelic returnRandomRelic(AbstractRelic.RelicTier tier)
/*      */   {
/*  861 */     logger.info("Returning " + tier.name() + " relic");
/*  862 */     return RelicLibrary.getRelic(returnRandomRelicKey(tier)).makeCopy();
/*      */   }
/*      */   
/*      */   public static AbstractRelic returnRandomScreenlessRelic(AbstractRelic.RelicTier tier) {
/*  866 */     logger.info("Returning " + tier.name() + " relic");
/*  867 */     AbstractRelic tmpRelic = RelicLibrary.getRelic(returnRandomRelicKey(tier)).makeCopy();
/*  868 */     while ((Objects.equals(tmpRelic.relicId, "Bottled Flame")) || (Objects.equals(tmpRelic.relicId, "Bottled Lightning")) || 
/*      */     
/*  870 */       (Objects.equals(tmpRelic.relicId, "Bottled Tornado")) || (Objects.equals(tmpRelic.relicId, "Whetstone")))
/*      */     {
/*      */ 
/*      */ 
/*  874 */       tmpRelic = RelicLibrary.getRelic(returnRandomRelicKey(tier)).makeCopy();
/*      */     }
/*  876 */     return tmpRelic;
/*      */   }
/*      */   
/*      */   public static AbstractRelic returnRandomNonCampfireRelic(AbstractRelic.RelicTier tier) {
/*  880 */     logger.info("Returning " + tier.name() + " relic");
/*  881 */     AbstractRelic tmpRelic = RelicLibrary.getRelic(returnRandomRelicKey(tier)).makeCopy();
/*  882 */     while ((Objects.equals(tmpRelic.relicId, "Peace Pipe")) || (Objects.equals(tmpRelic.relicId, "Shovel")) || 
/*  883 */       (Objects.equals(tmpRelic.relicId, "Girya")))
/*      */     {
/*  885 */       tmpRelic = RelicLibrary.getRelic(returnRandomRelicKey(tier)).makeCopy();
/*      */     }
/*  887 */     return tmpRelic;
/*      */   }
/*      */   
/*      */   public static AbstractRelic returnRandomRelicEnd(AbstractRelic.RelicTier tier) {
/*  891 */     logger.info("Returning " + tier.name() + " relic");
/*  892 */     return RelicLibrary.getRelic(returnEndRandomRelicKey(tier)).makeCopy();
/*      */   }
/*      */   
/*      */   public static String returnEndRandomRelicKey(AbstractRelic.RelicTier tier) {
/*  896 */     String retVal = null;
/*  897 */     switch (tier) {
/*      */     case COMMON: 
/*  899 */       if (commonRelicPool.isEmpty()) {
/*  900 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.UNCOMMON);
/*      */       } else {
/*  902 */         retVal = (String)commonRelicPool.remove(commonRelicPool.size() - 1);
/*      */       }
/*  904 */       break;
/*      */     case UNCOMMON: 
/*  906 */       if (uncommonRelicPool.isEmpty()) {
/*  907 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.RARE);
/*      */       } else {
/*  909 */         retVal = (String)uncommonRelicPool.remove(uncommonRelicPool.size() - 1);
/*      */       }
/*  911 */       break;
/*      */     case RARE: 
/*  913 */       if (rareRelicPool.isEmpty()) {
/*  914 */         retVal = "Circlet";
/*      */       } else {
/*  916 */         retVal = (String)rareRelicPool.remove(rareRelicPool.size() - 1);
/*      */       }
/*  918 */       break;
/*      */     case SHOP: 
/*  920 */       if (shopRelicPool.isEmpty()) {
/*  921 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.UNCOMMON);
/*      */       } else {
/*  923 */         retVal = (String)shopRelicPool.remove(shopRelicPool.size() - 1);
/*      */       }
/*  925 */       break;
/*      */     case BOSS: 
/*  927 */       if (bossRelicPool.isEmpty()) {
/*  928 */         retVal = "Red Circlet";
/*      */       } else {
/*  930 */         retVal = (String)bossRelicPool.remove(0);
/*      */       }
/*  932 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*      */ 
/*  938 */     if ((retVal.equals("Peace Pipe")) || (retVal.equals("Shovel")) || (retVal.equals("Girya"))) {
/*  939 */       int campfireCount = 0;
/*      */       
/*  941 */       for (AbstractRelic r : player.relics) {
/*  942 */         if (((r instanceof PeacePipe)) || ((r instanceof Shovel)) || ((r instanceof Girya))) {
/*  943 */           campfireCount++;
/*      */         }
/*      */       }
/*      */       
/*  947 */       if (campfireCount >= 2) {
/*  948 */         return returnEndRandomRelicKey(tier);
/*      */       }
/*      */     }
/*      */     
/*  952 */     if ((retVal.equals("Bottled Flame")) && 
/*  953 */       (!CardHelper.hasCardType(AbstractCard.CardType.ATTACK))) {
/*  954 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/*  958 */     if ((retVal.equals("Bottled Lightning")) && 
/*  959 */       (!CardHelper.hasCardType(AbstractCard.CardType.SKILL))) {
/*  960 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/*  964 */     if ((retVal.equals("Bottled Tornado")) && 
/*  965 */       (!CardHelper.hasCardType(AbstractCard.CardType.POWER))) {
/*  966 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  971 */     if ((retVal.equals("Black Blood")) && 
/*  972 */       (!player.hasRelic("Burning Blood"))) {
/*  973 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*  976 */     if ((retVal.equals("Ring of the Serpent")) && 
/*  977 */       (!player.hasRelic("Ring of the Snake"))) {
/*  978 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*  981 */     if ((retVal.equals("FrozenCore")) && 
/*  982 */       (!player.hasRelic("Cracked Core"))) {
/*  983 */       return returnEndRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/*  987 */     return retVal;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String returnRandomRelicKey(AbstractRelic.RelicTier tier)
/*      */   {
/*  997 */     String retVal = null;
/*  998 */     switch (tier) {
/*      */     case COMMON: 
/* 1000 */       if (commonRelicPool.isEmpty()) {
/* 1001 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.UNCOMMON);
/*      */       } else {
/* 1003 */         retVal = (String)commonRelicPool.remove(0);
/*      */       }
/* 1005 */       break;
/*      */     case UNCOMMON: 
/* 1007 */       if (uncommonRelicPool.isEmpty()) {
/* 1008 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.RARE);
/*      */       } else {
/* 1010 */         retVal = (String)uncommonRelicPool.remove(0);
/*      */       }
/* 1012 */       break;
/*      */     case RARE: 
/* 1014 */       if (rareRelicPool.isEmpty()) {
/* 1015 */         retVal = "Circlet";
/*      */       } else {
/* 1017 */         retVal = (String)rareRelicPool.remove(0);
/*      */       }
/* 1019 */       break;
/*      */     case SHOP: 
/* 1021 */       if (shopRelicPool.isEmpty()) {
/* 1022 */         retVal = returnRandomRelicKey(AbstractRelic.RelicTier.UNCOMMON);
/*      */       } else {
/* 1024 */         retVal = (String)shopRelicPool.remove(0);
/*      */       }
/* 1026 */       break;
/*      */     case BOSS: 
/* 1028 */       if (bossRelicPool.isEmpty()) {
/* 1029 */         retVal = "Red Circlet";
/*      */       } else {
/* 1031 */         retVal = (String)bossRelicPool.remove(0);
/*      */       }
/* 1033 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1040 */     if ((retVal.equals("Peace Pipe")) || (retVal.equals("Shovel")) || (retVal.equals("Girya"))) {
/* 1041 */       int campfireCount = 0;
/*      */       
/* 1043 */       for (AbstractRelic r : player.relics) {
/* 1044 */         if (((r instanceof PeacePipe)) || ((r instanceof Shovel)) || ((r instanceof Girya))) {
/* 1045 */           campfireCount++;
/*      */         }
/*      */       }
/*      */       
/* 1049 */       if (campfireCount >= 2) {
/* 1050 */         return returnRandomRelicKey(tier);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1055 */     if ((retVal.equals("Bottled Flame")) && 
/* 1056 */       (!CardHelper.hasCardType(AbstractCard.CardType.ATTACK))) {
/* 1057 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/* 1061 */     if ((retVal.equals("Bottled Lightning")) && 
/* 1062 */       (!CardHelper.hasCardType(AbstractCard.CardType.SKILL))) {
/* 1063 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/* 1067 */     if ((retVal.equals("Bottled Tornado")) && 
/* 1068 */       (!CardHelper.hasCardType(AbstractCard.CardType.POWER))) {
/* 1069 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1074 */     if ((retVal.equals("Black Blood")) && 
/* 1075 */       (!player.hasRelic("Burning Blood"))) {
/* 1076 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/* 1079 */     if ((retVal.equals("Ring of the Serpent")) && 
/* 1080 */       (!player.hasRelic("Ring of the Snake"))) {
/* 1081 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/* 1084 */     if ((retVal.equals("FrozenCore")) && 
/* 1085 */       (!player.hasRelic("Cracked Core"))) {
/* 1086 */       return returnRandomRelicKey(tier);
/*      */     }
/*      */     
/*      */ 
/* 1090 */     return retVal;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static AbstractRelic.RelicTier returnRandomRelicTier()
/*      */   {
/* 1097 */     int roll = relicRng.random(0, 99);
/* 1098 */     if (roll < commonRelicChance)
/* 1099 */       return AbstractRelic.RelicTier.COMMON;
/* 1100 */     if (roll < commonRelicChance + uncommonRelicChance) {
/* 1101 */       return AbstractRelic.RelicTier.UNCOMMON;
/*      */     }
/* 1103 */     return AbstractRelic.RelicTier.RARE;
/*      */   }
/*      */   
/*      */   public static AbstractPotion returnTotallyRandomPotion() {
/* 1107 */     return PotionHelper.getRandomPotion();
/*      */   }
/*      */   
/*      */   public static AbstractPotion returnRandomPotion() {
/* 1111 */     return returnRandomPotion(false);
/*      */   }
/*      */   
/*      */   public static AbstractPotion returnRandomPotion(boolean limited) {
/* 1115 */     int roll = potionRng.random(0, 99);
/* 1116 */     if (roll < PotionHelper.POTION_COMMON_CHANCE)
/* 1117 */       return returnRandomPotion(AbstractPotion.PotionRarity.COMMON, limited);
/* 1118 */     if (roll < PotionHelper.POTION_UNCOMMON_CHANCE + PotionHelper.POTION_COMMON_CHANCE) {
/* 1119 */       return returnRandomPotion(AbstractPotion.PotionRarity.UNCOMMON, limited);
/*      */     }
/* 1121 */     return returnRandomPotion(AbstractPotion.PotionRarity.RARE, limited);
/*      */   }
/*      */   
/*      */   public static AbstractPotion returnRandomPotion(AbstractPotion.PotionRarity rarity, boolean limited) {
/* 1125 */     AbstractPotion temp = PotionHelper.getRandomPotion();
/* 1126 */     boolean spamCheck = limited;
/* 1127 */     while ((temp.rarity != rarity) || (spamCheck)) {
/* 1128 */       spamCheck = limited;
/* 1129 */       temp = PotionHelper.getRandomPotion();
/* 1130 */       if (temp.ID != "Fruit Juice") {
/* 1131 */         spamCheck = false;
/*      */       }
/*      */     }
/* 1134 */     return temp;
/*      */   }
/*      */   
/*      */   public static void transformCard(AbstractCard c) {
/* 1138 */     transformCard(c, false);
/*      */   }
/*      */   
/*      */   public static void transformCard(AbstractCard c, boolean autoUpgrade) {
/* 1142 */     transformCard(c, autoUpgrade, new com.megacrit.cardcrawl.random.Random());
/*      */   }
/*      */   
/*      */   public static void transformCard(AbstractCard c, boolean autoUpgrade, com.megacrit.cardcrawl.random.Random rng) {
/* 1146 */     switch (c.color) {
/*      */     case COLORLESS: 
/* 1148 */       transformedCard = returnTrulyRandomColorlessCardFromAvailable(c, rng).makeCopy();
/* 1149 */       break;
/*      */     case CURSE: 
/* 1151 */       transformedCard = CardLibrary.getCurse(c, rng).makeCopy();
/* 1152 */       break;
/*      */     default: 
/* 1154 */       transformedCard = returnTrulyRandomCardFromAvailable(c, rng).makeCopy();
/*      */     }
/*      */     
/*      */     
/* 1158 */     UnlockTracker.markCardAsSeen(transformedCard.cardID);
/*      */     
/* 1160 */     if ((autoUpgrade) && (transformedCard.canUpgrade())) {
/* 1161 */       transformedCard.upgrade();
/*      */     }
/*      */   }
/*      */   
/*      */   public static void srcTransformCard(AbstractCard c) {
/* 1166 */     logger.info("Transform using SRC pool...");
/* 1167 */     switch (c.rarity) {
/*      */     case BASIC: 
/* 1169 */       transformedCard = srcCommonCardPool.getRandomCard(false).makeCopy();
/* 1170 */       break;
/*      */     case COMMON: 
/* 1172 */       srcCommonCardPool.removeCard(c.cardID);
/* 1173 */       transformedCard = srcCommonCardPool.getRandomCard(false).makeCopy();
/* 1174 */       srcCommonCardPool.addToTop(c.makeCopy());
/* 1175 */       break;
/*      */     case UNCOMMON: 
/* 1177 */       srcUncommonCardPool.removeCard(c.cardID);
/* 1178 */       transformedCard = srcUncommonCardPool.getRandomCard(false).makeCopy();
/* 1179 */       srcUncommonCardPool.addToTop(c.makeCopy());
/* 1180 */       break;
/*      */     case RARE: 
/* 1182 */       srcRareCardPool.removeCard(c.cardID);
/* 1183 */       if (srcRareCardPool.isEmpty()) {
/* 1184 */         transformedCard = srcUncommonCardPool.getRandomCard(false).makeCopy();
/*      */       } else {
/* 1186 */         transformedCard = srcRareCardPool.getRandomCard(false).makeCopy();
/*      */       }
/* 1188 */       srcRareCardPool.addToTop(c.makeCopy());
/* 1189 */       break;
/*      */     case CURSE: 
/* 1191 */       if (!srcRareCardPool.isEmpty()) {
/* 1192 */         transformedCard = srcRareCardPool.getRandomCard(false).makeCopy();
/*      */       } else
/* 1194 */         transformedCard = srcUncommonCardPool.getRandomCard(false).makeCopy();
/*      */       break;
/*      */     }
/* 1197 */     logger.info("Transform called on a strange card type: " + c.type.name());
/* 1198 */     transformedCard = srcCommonCardPool.getRandomCard(false).makeCopy();
/*      */   }
/*      */   
/*      */   public static AbstractCard returnRandomCard()
/*      */   {
/* 1203 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1204 */     AbstractCard.CardRarity rarity = rollRarity();
/* 1205 */     if (rarity.equals(AbstractCard.CardRarity.COMMON)) {
/* 1206 */       list.addAll(srcCommonCardPool.group);
/* 1207 */     } else if (rarity.equals(AbstractCard.CardRarity.UNCOMMON)) {
/* 1208 */       list.addAll(srcUncommonCardPool.group);
/*      */     } else {
/* 1210 */       list.addAll(srcRareCardPool.group);
/*      */     }
/*      */     
/* 1213 */     return (AbstractCard)list.get(cardRandomRng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard returnTrulyRandomCard()
/*      */   {
/* 1222 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1223 */     list.addAll(srcCommonCardPool.group);
/* 1224 */     list.addAll(srcUncommonCardPool.group);
/* 1225 */     list.addAll(srcRareCardPool.group);
/* 1226 */     return (AbstractCard)list.get(cardRandomRng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard returnTrulyRandomCard(com.megacrit.cardcrawl.random.Random rng)
/*      */   {
/* 1235 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1236 */     list.addAll(srcCommonCardPool.group);
/* 1237 */     list.addAll(srcUncommonCardPool.group);
/* 1238 */     list.addAll(srcRareCardPool.group);
/* 1239 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomCard(AbstractCard.CardType type) {
/* 1243 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1244 */     for (AbstractCard c : srcCommonCardPool.group) {
/* 1245 */       if (c.type == type) {
/* 1246 */         list.add(c);
/*      */       }
/*      */     }
/* 1249 */     for (AbstractCard c : srcUncommonCardPool.group) {
/* 1250 */       if (c.type == type) {
/* 1251 */         list.add(c);
/*      */       }
/*      */     }
/* 1254 */     for (AbstractCard c : srcRareCardPool.group) {
/* 1255 */       if (c.type == type) {
/* 1256 */         list.add(c);
/*      */       }
/*      */     }
/* 1259 */     return (AbstractCard)list.get(MathUtils.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomCard(AbstractCard.CardType type, com.megacrit.cardcrawl.random.Random rng) {
/* 1263 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1264 */     for (AbstractCard c : srcCommonCardPool.group) {
/* 1265 */       if (c.type == type) {
/* 1266 */         list.add(c);
/*      */       }
/*      */     }
/* 1269 */     for (AbstractCard c : srcUncommonCardPool.group) {
/* 1270 */       if (c.type == type) {
/* 1271 */         list.add(c);
/*      */       }
/*      */     }
/* 1274 */     for (AbstractCard c : srcRareCardPool.group) {
/* 1275 */       if (c.type == type) {
/* 1276 */         list.add(c);
/*      */       }
/*      */     }
/* 1279 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomCard(String prohibitedCardId, AbstractCard.CardType type, com.megacrit.cardcrawl.random.Random rng) {
/* 1283 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1284 */     for (AbstractCard c : srcCommonCardPool.group) {
/* 1285 */       if ((c.type == type) && (!c.cardID.equals(prohibitedCardId))) {
/* 1286 */         list.add(c);
/*      */       }
/*      */     }
/* 1289 */     for (AbstractCard c : srcUncommonCardPool.group) {
/* 1290 */       if ((c.type == type) && (!c.cardID.equals(prohibitedCardId))) {
/* 1291 */         list.add(c);
/*      */       }
/*      */     }
/* 1294 */     for (AbstractCard c : srcRareCardPool.group) {
/* 1295 */       if ((c.type == type) && (!c.cardID.equals(prohibitedCardId))) {
/* 1296 */         list.add(c);
/*      */       }
/*      */     }
/* 1299 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCard(AbstractCard.CardType type, com.megacrit.cardcrawl.random.Random rng) {
/* 1303 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1304 */     for (AbstractCard c : srcColorlessCardPool.group) {
/* 1305 */       if (c.type == type) {
/* 1306 */         list.add(c);
/*      */       }
/*      */     }
/* 1309 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCard(AbstractCard prohibited, AbstractCard.CardType type, com.megacrit.cardcrawl.random.Random rng) {
/* 1313 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1314 */     for (AbstractCard c : srcColorlessCardPool.group) {
/* 1315 */       if ((c.type == type) && (!c.cardID.equals(prohibited.cardID))) {
/* 1316 */         list.add(c);
/*      */       }
/*      */     }
/* 1319 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCard() {
/* 1323 */     return returnTrulyRandomColorlessCard(cardRandomRng);
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCard(String prohibitedID) {
/* 1327 */     return returnTrulyRandomColorlessCardFromAvailable(prohibitedID, cardRandomRng);
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCard(com.megacrit.cardcrawl.random.Random rng) {
/* 1331 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1332 */     list.addAll(srcColorlessCardPool.group);
/* 1333 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCardFromAvailable(String prohibited, com.megacrit.cardcrawl.random.Random rng) {
/* 1337 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1338 */     for (AbstractCard c : srcColorlessCardPool.group) {
/* 1339 */       if (c.cardID != prohibited) {
/* 1340 */         list.add(c);
/*      */       }
/*      */     }
/* 1343 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomColorlessCardFromAvailable(AbstractCard prohibited, com.megacrit.cardcrawl.random.Random rng) {
/* 1347 */     ArrayList<AbstractCard> list = new ArrayList();
/* 1348 */     for (AbstractCard c : srcColorlessCardPool.group) {
/* 1349 */       if (!Objects.equals(c.cardID, prohibited.cardID)) {
/* 1350 */         list.add(c);
/*      */       }
/*      */     }
/* 1353 */     return (AbstractCard)list.get(rng.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomCardFromAvailable(AbstractCard prohibited, com.megacrit.cardcrawl.random.Random rng) {
/* 1357 */     ArrayList<AbstractCard> list = new ArrayList();
/*      */     
/* 1359 */     switch (prohibited.color) {
/*      */     case COLORLESS: 
/* 1361 */       for (AbstractCard c : colorlessCardPool.group) {
/* 1362 */         if (!Objects.equals(c.cardID, prohibited.cardID)) {
/* 1363 */           list.add(c);
/*      */         }
/*      */       }
/* 1366 */       break;
/*      */     case CURSE: 
/* 1368 */       return CardLibrary.getCurse();
/*      */     default: 
/* 1370 */       for (AbstractCard c : commonCardPool.group) {
/* 1371 */         if (!Objects.equals(c.cardID, prohibited.cardID)) {
/* 1372 */           list.add(c);
/*      */         }
/*      */       }
/* 1375 */       for (AbstractCard c : srcUncommonCardPool.group) {
/* 1376 */         if (!Objects.equals(c.cardID, prohibited.cardID)) {
/* 1377 */           list.add(c);
/*      */         }
/*      */       }
/* 1380 */       for (AbstractCard c : srcRareCardPool.group) {
/* 1381 */         if (!Objects.equals(c.cardID, prohibited.cardID)) {
/* 1382 */           list.add(c);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */     
/* 1388 */     return ((AbstractCard)list.get(rng.random(list.size() - 1))).makeCopy();
/*      */   }
/*      */   
/*      */   public static AbstractCard returnTrulyRandomCardFromAvailable(AbstractCard prohibited) {
/* 1392 */     return returnTrulyRandomCardFromAvailable(prohibited, new com.megacrit.cardcrawl.random.Random());
/*      */   }
/*      */   
/*      */   public static AbstractCard getTransformedCard() {
/* 1396 */     AbstractCard retVal = transformedCard;
/* 1397 */     transformedCard = null;
/* 1398 */     return retVal;
/*      */   }
/*      */   
/*      */   public void populateFirstStrongEnemy(ArrayList<MonsterInfo> monsters, ArrayList<String> exclusions) {
/*      */     for (;;) {
/* 1403 */       String m = MonsterInfo.roll(monsters, monsterRng.random());
/* 1404 */       if (!exclusions.contains(m)) {
/* 1405 */         monsterList.add(m);
/* 1406 */         return;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void populateMonsterList(ArrayList<MonsterInfo> monsters, int numMonsters, boolean elites)
/*      */   {
/* 1413 */     if (elites) {
/* 1414 */       for (int i = 0; i < numMonsters; i++) {
/* 1415 */         if (eliteMonsterList.isEmpty()) {
/* 1416 */           eliteMonsterList.add(MonsterInfo.roll(monsters, monsterRng.random()));
/*      */         } else {
/* 1418 */           String toAdd = MonsterInfo.roll(monsters, monsterRng.random());
/* 1419 */           if (!toAdd.equals(eliteMonsterList.get(eliteMonsterList.size() - 1))) {
/* 1420 */             eliteMonsterList.add(toAdd);
/*      */           }
/*      */           else {
/* 1423 */             i--;
/*      */           }
/*      */         }
/*      */       }
/*      */     } else {
/* 1428 */       for (int i = 0; i < numMonsters; i++) {
/* 1429 */         if (monsterList.isEmpty()) {
/* 1430 */           monsterList.add(MonsterInfo.roll(monsters, monsterRng.random()));
/*      */         } else {
/* 1432 */           String toAdd = MonsterInfo.roll(monsters, monsterRng.random());
/* 1433 */           if (!toAdd.equals(monsterList.get(monsterList.size() - 1))) {
/* 1434 */             if ((monsterList.size() > 1) && (toAdd.equals(monsterList.get(monsterList.size() - 2))))
/*      */             {
/* 1436 */               i--;
/*      */             } else {
/* 1438 */               monsterList.add(toAdd);
/*      */             }
/*      */           }
/*      */           else {
/* 1442 */             i--;
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected abstract ArrayList<String> generateExclusions();
/*      */   
/*      */ 
/*      */ 
/*      */   public static AbstractCard returnColorlessCard(AbstractCard.CardRarity rarity)
/*      */   {
/* 1458 */     Collections.shuffle(colorlessCardPool.group, new java.util.Random(shuffleRng.randomLong()));
/*      */     
/*      */ 
/* 1461 */     for (AbstractCard c : colorlessCardPool.group) {
/* 1462 */       if (c.rarity == rarity) {
/* 1463 */         return c.makeCopy();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1468 */     if (rarity == AbstractCard.CardRarity.RARE) {
/* 1469 */       for (AbstractCard c : colorlessCardPool.group) {
/* 1470 */         if (c.rarity == AbstractCard.CardRarity.UNCOMMON) {
/* 1471 */           return c.makeCopy();
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1477 */     return new SwiftStrike();
/*      */   }
/*      */   
/*      */   public static AbstractCard returnColorlessCard()
/*      */   {
/* 1482 */     Collections.shuffle(colorlessCardPool.group);
/*      */     
/*      */ 
/* 1485 */     Iterator localIterator = colorlessCardPool.group.iterator(); if (localIterator.hasNext()) { AbstractCard c = (AbstractCard)localIterator.next();
/* 1486 */       return c.makeCopy();
/*      */     }
/*      */     
/*      */ 
/* 1490 */     return new SwiftStrike();
/*      */   }
/*      */   
/*      */   public static AbstractCard returnRandomCurse() {
/* 1494 */     UnlockTracker.markCardAsSeen(CardLibrary.getCurse().cardID);
/* 1495 */     return CardLibrary.getCurse().makeCopy();
/*      */   }
/*      */   
/*      */   public void initializePotions() {
/* 1499 */     PotionHelper.initialize(player.chosenClass);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void initializeCardPools()
/*      */   {
/* 1506 */     logger.info("INIT CARD POOL");
/* 1507 */     long startTime = System.currentTimeMillis();
/*      */     
/*      */ 
/* 1510 */     commonCardPool.clear();
/* 1511 */     uncommonCardPool.clear();
/* 1512 */     rareCardPool.clear();
/* 1513 */     colorlessCardPool.clear();
/* 1514 */     curseCardPool.clear();
/*      */     
/* 1516 */     ArrayList<AbstractCard> tmpPool = new ArrayList();
/* 1517 */     if (((Boolean)DailyMods.cardMods.get("Diverse")).booleanValue()) {
/* 1518 */       addRedCards(tmpPool);
/* 1519 */       addGreenCards(tmpPool);
/* 1520 */       addBlueCards(tmpPool);
/*      */     } else {
/* 1522 */       switch (player.chosenClass) {
/*      */       case IRONCLAD: 
/* 1524 */         addRedCards(tmpPool);
/* 1525 */         if ((Settings.isTrial) && (AbstractPlayer.customMods != null)) {
/* 1526 */           if (AbstractPlayer.customMods.contains("Green Cards")) {
/* 1527 */             addGreenCards(tmpPool);
/*      */           }
/* 1529 */           if (AbstractPlayer.customMods.contains("Blue Cards")) {
/* 1530 */             addBlueCards(tmpPool);
/*      */           }
/* 1532 */           if (AbstractPlayer.customMods.contains("Colorless Cards")) {
/* 1533 */             addColorlessCards(tmpPool);
/*      */           }
/*      */         }
/*      */         break;
/*      */       case THE_SILENT: 
/* 1538 */         addGreenCards(tmpPool);
/* 1539 */         if ((Settings.isTrial) && (AbstractPlayer.customMods != null)) {
/* 1540 */           if (AbstractPlayer.customMods.contains("Red Cards")) {
/* 1541 */             addRedCards(tmpPool);
/*      */           }
/* 1543 */           if (AbstractPlayer.customMods.contains("Blue Cards")) {
/* 1544 */             addBlueCards(tmpPool);
/*      */           }
/* 1546 */           if (AbstractPlayer.customMods.contains("Colorless Cards")) {
/* 1547 */             addColorlessCards(tmpPool);
/*      */           }
/*      */         }
/*      */         break;
/*      */       case DEFECT: 
/* 1552 */         addBlueCards(tmpPool);
/* 1553 */         if ((Settings.isTrial) && (AbstractPlayer.customMods != null)) {
/* 1554 */           if (AbstractPlayer.customMods.contains("Red Cards")) {
/* 1555 */             addRedCards(tmpPool);
/*      */           }
/* 1557 */           if (AbstractPlayer.customMods.contains("Green Cards")) {
/* 1558 */             addGreenCards(tmpPool);
/*      */           }
/* 1560 */           if (AbstractPlayer.customMods.contains("Colorless Cards")) {
/* 1561 */             addColorlessCards(tmpPool);
/*      */           }
/*      */         }
/*      */         
/*      */ 
/*      */         break;
/*      */       }
/*      */       
/*      */     }
/*      */     
/* 1571 */     addColorlessCards();
/*      */     
/* 1573 */     addCurseCards();
/*      */     
/*      */ 
/* 1576 */     for (AbstractCard c : tmpPool) {
/* 1577 */       switch (c.rarity) {
/*      */       case COMMON: 
/* 1579 */         commonCardPool.addToTop(c);
/* 1580 */         break;
/*      */       case UNCOMMON: 
/* 1582 */         uncommonCardPool.addToTop(c);
/* 1583 */         break;
/*      */       case RARE: 
/* 1585 */         rareCardPool.addToTop(c);
/* 1586 */         break;
/*      */       case CURSE: 
/* 1588 */         curseCardPool.addToTop(c);
/* 1589 */         break;
/*      */       default: 
/* 1591 */         logger.info("Unspecified rarity: " + c.rarity
/* 1592 */           .name() + " when creating pools! AbstractDungeon: Line 827");
/*      */       }
/*      */       
/*      */     }
/*      */     
/*      */ 
/* 1598 */     srcColorlessCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/* 1599 */     srcCurseCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/* 1600 */     srcRareCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/* 1601 */     srcUncommonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/* 1602 */     srcCommonCardPool = new CardGroup(CardGroup.CardGroupType.CARD_POOL);
/*      */     
/* 1604 */     for (AbstractCard c : colorlessCardPool.group) {
/* 1605 */       srcColorlessCardPool.addToBottom(c);
/*      */     }
/* 1607 */     for (AbstractCard c : curseCardPool.group) {
/* 1608 */       srcCurseCardPool.addToBottom(c);
/*      */     }
/* 1610 */     for (AbstractCard c : rareCardPool.group) {
/* 1611 */       srcRareCardPool.addToBottom(c);
/*      */     }
/* 1613 */     for (AbstractCard c : uncommonCardPool.group) {
/* 1614 */       srcUncommonCardPool.addToBottom(c);
/*      */     }
/* 1616 */     for (AbstractCard c : commonCardPool.group) {
/* 1617 */       srcCommonCardPool.addToBottom(c);
/*      */     }
/*      */     
/* 1620 */     logger.info("Cardpool load time: " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addColorlessCards()
/*      */   {
/* 1628 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1629 */       AbstractCard card = (AbstractCard)c.getValue();
/* 1630 */       if ((card.color == AbstractCard.CardColor.COLORLESS) && (card.rarity != AbstractCard.CardRarity.BASIC) && (card.rarity != AbstractCard.CardRarity.SPECIAL) && (card.type != AbstractCard.CardType.STATUS))
/*      */       {
/* 1632 */         colorlessCardPool.addToTop(card);
/*      */       }
/*      */     }
/* 1635 */     logger.info("COLORLESS CARDS: " + colorlessCardPool.size());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addCurseCards()
/*      */   {
/* 1643 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1644 */       AbstractCard card = (AbstractCard)c.getValue();
/* 1645 */       if ((card.type == AbstractCard.CardType.CURSE) && (!Objects.equals(card.cardID, "Necronomicurse")) && (!Objects.equals(card.cardID, "AscendersBane")))
/*      */       {
/* 1647 */         if (!Objects.equals(card.cardID, "Pride"))
/* 1648 */           curseCardPool.addToTop(card);
/*      */       }
/*      */     }
/* 1651 */     logger.info("CURSE CARDS: " + curseCardPool.size());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addRedCards(ArrayList<AbstractCard> tmpPool)
/*      */   {
/* 1660 */     logger.info("[INFO] Adding red cards into card pool.");
/* 1661 */     AbstractCard card = null;
/* 1662 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1663 */       card = (AbstractCard)c.getValue();
/* 1664 */       if ((card.color == AbstractCard.CardColor.RED) && (card.rarity != AbstractCard.CardRarity.BASIC) && (
/* 1665 */         (!UnlockTracker.isCardLocked((String)c.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 1666 */         tmpPool.add(card);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addGreenCards(ArrayList<AbstractCard> tmpPool)
/*      */   {
/* 1678 */     logger.info("[INFO] Adding green cards into card pool.");
/* 1679 */     AbstractCard card = null;
/* 1680 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1681 */       card = (AbstractCard)c.getValue();
/* 1682 */       if ((card.color == AbstractCard.CardColor.GREEN) && (card.rarity != AbstractCard.CardRarity.BASIC) && (
/* 1683 */         (!UnlockTracker.isCardLocked((String)c.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 1684 */         tmpPool.add(card);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addBlueCards(ArrayList<AbstractCard> tmpPool)
/*      */   {
/* 1696 */     logger.info("[INFO] Adding blue cards into card pool.");
/* 1697 */     AbstractCard card = null;
/* 1698 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1699 */       card = (AbstractCard)c.getValue();
/* 1700 */       if ((card.color == AbstractCard.CardColor.BLUE) && (card.rarity != AbstractCard.CardRarity.BASIC) && (
/* 1701 */         (!UnlockTracker.isCardLocked((String)c.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 1702 */         tmpPool.add(card);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void addColorlessCards(ArrayList<AbstractCard> tmpPool)
/*      */   {
/* 1714 */     logger.info("[INFO] Adding colorless cards into card pool.");
/* 1715 */     AbstractCard card = null;
/* 1716 */     for (Map.Entry<String, AbstractCard> c : CardLibrary.cards.entrySet()) {
/* 1717 */       card = (AbstractCard)c.getValue();
/* 1718 */       if ((card.color == AbstractCard.CardColor.COLORLESS) && (card.type != AbstractCard.CardType.STATUS) && (
/* 1719 */         (!UnlockTracker.isCardLocked((String)c.getKey())) || (Settings.treatEverythingAsUnlocked()))) {
/* 1720 */         tmpPool.add(card);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void initializeRelicList()
/*      */   {
/* 1730 */     commonRelicPool.clear();
/* 1731 */     uncommonRelicPool.clear();
/* 1732 */     rareRelicPool.clear();
/* 1733 */     shopRelicPool.clear();
/* 1734 */     bossRelicPool.clear();
/*      */     
/* 1736 */     RelicLibrary.populateRelicPool(commonRelicPool, AbstractRelic.RelicTier.COMMON, player.chosenClass);
/* 1737 */     RelicLibrary.populateRelicPool(uncommonRelicPool, AbstractRelic.RelicTier.UNCOMMON, player.chosenClass);
/* 1738 */     RelicLibrary.populateRelicPool(rareRelicPool, AbstractRelic.RelicTier.RARE, player.chosenClass);
/* 1739 */     RelicLibrary.populateRelicPool(shopRelicPool, AbstractRelic.RelicTier.SHOP, player.chosenClass);
/* 1740 */     RelicLibrary.populateRelicPool(bossRelicPool, AbstractRelic.RelicTier.BOSS, player.chosenClass);
/*      */     
/* 1742 */     if (floorNum >= 1) {
/* 1743 */       for (AbstractRelic r : player.relics) {
/* 1744 */         relicsToRemoveOnStart.add(r.relicId);
/*      */       }
/*      */     }
/*      */     
/* 1748 */     Collections.shuffle(commonRelicPool, new java.util.Random(relicRng.randomLong()));
/* 1749 */     Collections.shuffle(uncommonRelicPool, new java.util.Random(relicRng.randomLong()));
/* 1750 */     Collections.shuffle(rareRelicPool, new java.util.Random(relicRng.randomLong()));
/* 1751 */     Collections.shuffle(shopRelicPool, new java.util.Random(relicRng.randomLong()));
/* 1752 */     Collections.shuffle(bossRelicPool, new java.util.Random(relicRng.randomLong()));
/*      */     
/* 1754 */     if (((Boolean)DailyMods.cardMods.get("Brewmaster")).booleanValue()) {
/* 1755 */       relicsToRemoveOnStart.add("White Beast Statue");
/*      */     }
/*      */     
/*      */ 
/* 1759 */     for (??? = relicsToRemoveOnStart.iterator(); ???.hasNext();) { remove = (String)???.next();
/* 1760 */       for (Iterator<String> s = commonRelicPool.iterator(); s.hasNext();) {
/* 1761 */         String derp = (String)s.next();
/* 1762 */         if (derp.equals(remove)) {
/* 1763 */           s.remove();
/* 1764 */           logger.info(derp + " removed.");
/* 1765 */           break;
/*      */         }
/*      */       }
/* 1768 */       for (Iterator<String> s = uncommonRelicPool.iterator(); s.hasNext();) {
/* 1769 */         String derp = (String)s.next();
/* 1770 */         if (derp.equals(remove)) {
/* 1771 */           s.remove();
/* 1772 */           logger.info(derp + " removed.");
/* 1773 */           break;
/*      */         }
/*      */       }
/* 1776 */       for (Iterator<String> s = rareRelicPool.iterator(); s.hasNext();) {
/* 1777 */         String derp = (String)s.next();
/* 1778 */         if (derp.equals(remove)) {
/* 1779 */           s.remove();
/* 1780 */           logger.info(derp + " removed.");
/* 1781 */           break;
/*      */         }
/*      */       }
/* 1784 */       for (Iterator<String> s = bossRelicPool.iterator(); s.hasNext();) {
/* 1785 */         String derp = (String)s.next();
/* 1786 */         if (derp.equals(remove)) {
/* 1787 */           s.remove();
/* 1788 */           logger.info(derp + " removed.");
/* 1789 */           break;
/*      */         }
/*      */       }
/* 1792 */       for (s = shopRelicPool.iterator(); s.hasNext();) {
/* 1793 */         String derp = (String)s.next();
/* 1794 */         if (derp.equals(remove)) {
/* 1795 */           s.remove();
/* 1796 */           logger.info(derp + " removed.");
/* 1797 */           break;
/*      */         }
/*      */       } }
/*      */     String remove;
/*      */     Iterator<String> s;
/* 1802 */     if (Settings.isDebug) {
/* 1803 */       logger.info("Relic (Common):");
/* 1804 */       for (String s : commonRelicPool) {
/* 1805 */         logger.info(" " + s);
/*      */       }
/*      */       
/* 1808 */       logger.info("Relic (Uncommon):");
/* 1809 */       for (String s : uncommonRelicPool) {
/* 1810 */         logger.info(" " + s);
/*      */       }
/*      */       
/* 1813 */       logger.info("Relic (Rare):");
/* 1814 */       for (String s : rareRelicPool) {
/* 1815 */         logger.info(" " + s);
/*      */       }
/*      */       
/* 1818 */       logger.info("Relic (Shop):");
/* 1819 */       for (String s : shopRelicPool) {
/* 1820 */         logger.info(" " + s);
/*      */       }
/*      */       
/* 1823 */       logger.info("Relic (Boss):");
/* 1824 */       for (String s : bossRelicPool) {
/* 1825 */         logger.info(" " + s);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected abstract void generateMonsters();
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   protected abstract void initializeBoss();
/*      */   
/*      */ 
/*      */ 
/*      */   protected abstract void initializeEventList();
/*      */   
/*      */ 
/*      */ 
/*      */   protected abstract void initializeEventImg();
/*      */   
/*      */ 
/*      */ 
/*      */   protected abstract void initializeShrineList();
/*      */   
/*      */ 
/*      */ 
/*      */   public void initializeSpecialOneTimeEventList()
/*      */   {
/* 1856 */     specialOneTimeEventList.clear();
/* 1857 */     specialOneTimeEventList.add("Accursed Blacksmith");
/* 1858 */     specialOneTimeEventList.add("Bonfire Elementals");
/* 1859 */     specialOneTimeEventList.add("Designer");
/* 1860 */     specialOneTimeEventList.add("Duplicator");
/* 1861 */     specialOneTimeEventList.add("FaceTrader");
/* 1862 */     specialOneTimeEventList.add("Fountain of Cleansing");
/* 1863 */     specialOneTimeEventList.add("Knowing Skull");
/* 1864 */     specialOneTimeEventList.add("Lab");
/* 1865 */     specialOneTimeEventList.add("N'loth");
/* 1866 */     specialOneTimeEventList.add("SecretPortal");
/* 1867 */     specialOneTimeEventList.add("The Joust");
/* 1868 */     specialOneTimeEventList.add("WeMeetAgain");
/* 1869 */     specialOneTimeEventList.add("The Woman in Blue");
/*      */     
/* 1871 */     if (Settings.isBeta)
/*      */     {
/* 1873 */       if ((!Settings.isDailyRun) && (!isAscensionMode)) {
/* 1874 */         specialOneTimeEventList.add("NoteForYourself");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public static ArrayList<AbstractCard> getBossRewardCards() {
/* 1880 */     ArrayList<AbstractCard> retVal = new ArrayList();
/*      */     
/* 1882 */     int numCards = 3;
/* 1883 */     if (player.hasRelic("Question Card")) {
/* 1884 */       numCards++;
/*      */     }
/*      */     
/* 1887 */     if (player.hasRelic("Busted Crown")) {
/* 1888 */       numCards -= 2;
/*      */     }
/*      */     
/* 1891 */     if (((Boolean)DailyMods.negativeMods.get("Binary")).booleanValue()) {
/* 1892 */       numCards--;
/*      */     }
/*      */     
/*      */     AbstractCard.CardRarity rarity;
/* 1896 */     for (int i = 0; i < numCards; i++) {
/* 1897 */       rarity = AbstractCard.CardRarity.RARE;
/* 1898 */       AbstractCard card = null;
/* 1899 */       switch (rarity) {
/*      */       case RARE: 
/* 1901 */         card = getCard(rarity);
/* 1902 */         cardBlizzRandomizer = cardBlizzStartOffset;
/* 1903 */         break;
/*      */       case UNCOMMON: 
/* 1905 */         card = getCard(rarity);
/* 1906 */         break;
/*      */       case COMMON: 
/* 1908 */         card = getCard(rarity);
/* 1909 */         cardBlizzRandomizer -= cardBlizzGrowth;
/* 1910 */         if (cardBlizzRandomizer <= cardBlizzMaxOffset) {
/* 1911 */           cardBlizzRandomizer = cardBlizzMaxOffset;
/*      */         }
/*      */         break;
/*      */       default: 
/* 1915 */         logger.info("WTF?");
/*      */       }
/*      */       
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1922 */       while (retVal.contains(card)) {
/* 1923 */         if (card != null) {
/* 1924 */           logger.info("DUPE: " + card.originalName);
/*      */         }
/* 1926 */         card = getCard(rarity);
/*      */       }
/* 1928 */       retVal.add(card);
/*      */     }
/*      */     
/* 1931 */     ArrayList<AbstractCard> retVal2 = new ArrayList();
/* 1932 */     for (AbstractCard c : retVal) {
/* 1933 */       retVal2.add(c.makeCopy());
/*      */     }
/*      */     
/* 1936 */     return retVal2;
/*      */   }
/*      */   
/*      */   public static ArrayList<AbstractCard> getColorlessRewardCards()
/*      */   {
/* 1941 */     ArrayList<AbstractCard> retVal = new ArrayList();
/*      */     
/* 1943 */     int numCards = 3;
/* 1944 */     if (player.hasRelic("Question Card")) {
/* 1945 */       numCards++;
/*      */     }
/*      */     
/* 1948 */     if (player.hasRelic("Busted Crown")) {
/* 1949 */       numCards -= 2;
/*      */     }
/*      */     
/* 1952 */     if (((Boolean)DailyMods.negativeMods.get("Binary")).booleanValue()) {
/* 1953 */       numCards--;
/*      */     }
/*      */     
/*      */     AbstractCard.CardRarity rarity;
/* 1957 */     for (int i = 0; i < numCards; i++) {
/* 1958 */       rarity = rollRareOrUncommon(colorlessRareChance);
/* 1959 */       AbstractCard card = null;
/* 1960 */       switch (rarity) {
/*      */       case RARE: 
/* 1962 */         card = getColorlessCardFromPool(rarity);
/* 1963 */         cardBlizzRandomizer = cardBlizzStartOffset;
/* 1964 */         break;
/*      */       case UNCOMMON: 
/* 1966 */         card = getColorlessCardFromPool(rarity);
/* 1967 */         break;
/*      */       default: 
/* 1969 */         logger.info("WTF?");
/*      */       }
/*      */       
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1976 */       int dupeCount = 0;
/* 1977 */       while (retVal.contains(card)) {
/* 1978 */         if (card != null) {
/* 1979 */           logger.info("DUPE: " + card.originalName);
/*      */         }
/* 1981 */         if (dupeCount < 4) {
/* 1982 */           card = getColorlessCardFromPool(rarity);
/*      */         } else {
/* 1984 */           logger.info("FALLBACK FOR CARD RARITY HAS OCCURRED");
/*      */           
/* 1986 */           switch (rarity) {
/*      */           case RARE: 
/* 1988 */             card = getColorlessCardFromPool(AbstractCard.CardRarity.UNCOMMON);
/* 1989 */             break;
/*      */           case UNCOMMON: 
/* 1991 */             card = getColorlessCardFromPool(AbstractCard.CardRarity.RARE);
/*      */           }
/*      */           
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 1998 */       if (card != null) {
/* 1999 */         retVal.add(card);
/*      */       }
/*      */     }
/*      */     
/* 2003 */     ArrayList<AbstractCard> retVal2 = new ArrayList();
/* 2004 */     for (AbstractCard c : retVal) {
/* 2005 */       retVal2.add(c.makeCopy());
/*      */     }
/*      */     
/* 2008 */     return retVal2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static ArrayList<AbstractCard> getRewardCards()
/*      */   {
/* 2015 */     ArrayList<AbstractCard> retVal = new ArrayList();
/*      */     
/* 2017 */     int numCards = 3;
/* 2018 */     if (player.hasRelic("Question Card")) {
/* 2019 */       numCards++;
/*      */     }
/*      */     
/* 2022 */     if (player.hasRelic("Busted Crown")) {
/* 2023 */       numCards -= 2;
/*      */     }
/*      */     
/* 2026 */     if (((Boolean)DailyMods.negativeMods.get("Binary")).booleanValue()) {
/* 2027 */       numCards--;
/*      */     }
/*      */     
/*      */     AbstractCard.CardRarity rarity;
/* 2031 */     for (int i = 0; i < numCards; i++) {
/* 2032 */       rarity = rollRarity();
/* 2033 */       AbstractCard card = null;
/* 2034 */       switch (rarity) {
/*      */       case RARE: 
/* 2036 */         card = getCard(rarity);
/* 2037 */         cardBlizzRandomizer = cardBlizzStartOffset;
/* 2038 */         break;
/*      */       case UNCOMMON: 
/* 2040 */         card = getCard(rarity);
/* 2041 */         break;
/*      */       case COMMON: 
/* 2043 */         card = getCard(rarity);
/* 2044 */         cardBlizzRandomizer -= cardBlizzGrowth;
/* 2045 */         if (cardBlizzRandomizer <= cardBlizzMaxOffset) {
/* 2046 */           cardBlizzRandomizer = cardBlizzMaxOffset;
/*      */         }
/*      */         break;
/*      */       default: 
/* 2050 */         logger.info("WTF?");
/*      */       }
/*      */       
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 2057 */       int dupeCount = 0;
/* 2058 */       while (retVal.contains(card)) {
/* 2059 */         if (card != null) {
/* 2060 */           logger.info("DUPE: " + card.originalName);
/*      */         }
/* 2062 */         if (dupeCount < 4) {
/* 2063 */           card = getCard(rarity);
/*      */         } else {
/* 2065 */           logger.info("FALLBACK FOR CARD RARITY HAS OCCURRED");
/*      */           
/* 2067 */           switch (rarity) {
/*      */           case RARE: 
/* 2069 */             card = getCard(AbstractCard.CardRarity.UNCOMMON);
/* 2070 */             break;
/*      */           case UNCOMMON: 
/* 2072 */             card = getCard(AbstractCard.CardRarity.COMMON);
/* 2073 */             break;
/*      */           case COMMON: 
/* 2075 */             card = getCard(AbstractCard.CardRarity.UNCOMMON);
/* 2076 */             break;
/*      */           default: 
/* 2078 */             card = getCard(AbstractCard.CardRarity.COMMON);
/*      */           }
/*      */           
/*      */         }
/*      */       }
/* 2083 */       if (card != null) {
/* 2084 */         retVal.add(card);
/*      */       }
/*      */     }
/*      */     
/* 2088 */     ArrayList<AbstractCard> retVal2 = new ArrayList();
/* 2089 */     for (AbstractCard c : retVal) {
/* 2090 */       retVal2.add(c.makeCopy());
/*      */     }
/*      */     
/* 2093 */     for (AbstractCard c : retVal2) {
/* 2094 */       if ((c.rarity != AbstractCard.CardRarity.RARE) && (cardRng.randomBoolean(cardUpgradedChance)) && (c.canUpgrade())) {
/* 2095 */         c.upgrade();
/*      */       }
/* 2097 */       else if ((c.type == AbstractCard.CardType.ATTACK) && (player.hasRelic("Molten Egg 2"))) {
/* 2098 */         c.upgrade();
/* 2099 */       } else if ((c.type == AbstractCard.CardType.SKILL) && (player.hasRelic("Toxic Egg 2"))) {
/* 2100 */         c.upgrade();
/* 2101 */       } else if ((c.type == AbstractCard.CardType.POWER) && (player.hasRelic("Frozen Egg 2"))) {
/* 2102 */         c.upgrade();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2107 */     return retVal2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard getCard(AbstractCard.CardRarity rarity)
/*      */   {
/* 2117 */     switch (rarity) {
/*      */     case RARE: 
/* 2119 */       return rareCardPool.getRandomCard(true);
/*      */     case UNCOMMON: 
/* 2121 */       return uncommonCardPool.getRandomCard(true);
/*      */     case COMMON: 
/* 2123 */       return commonCardPool.getRandomCard(true);
/*      */     case CURSE: 
/* 2125 */       return curseCardPool.getRandomCard(true);
/*      */     }
/* 2127 */     logger.info("No rarity on getCard in Abstract Dungeon");
/* 2128 */     return null;
/*      */   }
/*      */   
/*      */   public static AbstractCard getCard(AbstractCard.CardRarity rarity, com.megacrit.cardcrawl.random.Random rng)
/*      */   {
/* 2133 */     switch (rarity) {
/*      */     case RARE: 
/* 2135 */       return rareCardPool.getRandomCard(rng);
/*      */     case UNCOMMON: 
/* 2137 */       return uncommonCardPool.getRandomCard(rng);
/*      */     case COMMON: 
/* 2139 */       return commonCardPool.getRandomCard(rng);
/*      */     case CURSE: 
/* 2141 */       return curseCardPool.getRandomCard(rng);
/*      */     }
/* 2143 */     logger.info("No rarity on getCard in Abstract Dungeon");
/* 2144 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard getCardWithoutRng(AbstractCard.CardRarity rarity)
/*      */   {
/* 2155 */     switch (rarity) {
/*      */     case RARE: 
/* 2157 */       return rareCardPool.getRandomCard(false);
/*      */     case UNCOMMON: 
/* 2159 */       return uncommonCardPool.getRandomCard(false);
/*      */     case COMMON: 
/* 2161 */       return commonCardPool.getRandomCard(false);
/*      */     case CURSE: 
/* 2163 */       return returnRandomCurse();
/*      */     }
/* 2165 */     logger.info("Check getCardWithoutRng");
/* 2166 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractCard getCardFromPool(AbstractCard.CardRarity rarity, AbstractCard.CardType type, boolean useRng)
/*      */   {
/* 2178 */     switch (rarity) {
/*      */     case RARE: 
/* 2180 */       AbstractCard retVal = rareCardPool.getRandomCard(type, useRng);
/* 2181 */       if (retVal != null) {
/* 2182 */         return retVal;
/*      */       }
/* 2184 */       logger.info("ERROR: Could not find Rare card of type: " + type.name());
/*      */     case UNCOMMON: 
/* 2186 */       AbstractCard retVal = uncommonCardPool.getRandomCard(type, useRng);
/* 2187 */       if (retVal != null) {
/* 2188 */         return retVal;
/*      */       }
/*      */       
/*      */ 
/* 2192 */       if (type == AbstractCard.CardType.POWER) {
/* 2193 */         return getCardFromPool(AbstractCard.CardRarity.RARE, type, useRng);
/*      */       }
/*      */       
/* 2196 */       logger.info("ERROR: Could not find Uncommon card of type: " + type.name());
/*      */     case COMMON: 
/* 2198 */       AbstractCard retVal = commonCardPool.getRandomCard(type, useRng);
/* 2199 */       if (retVal != null) {
/* 2200 */         return retVal;
/*      */       }
/*      */       
/*      */ 
/* 2204 */       if (type == AbstractCard.CardType.POWER) {
/* 2205 */         return getCardFromPool(AbstractCard.CardRarity.UNCOMMON, type, useRng);
/*      */       }
/*      */       
/* 2208 */       logger.info("ERROR: Could not find Common card of type: " + type.name());
/*      */     case CURSE: 
/* 2210 */       AbstractCard retVal = curseCardPool.getRandomCard(type, useRng);
/* 2211 */       if (retVal != null) {
/* 2212 */         return retVal;
/*      */       }
/*      */       
/* 2215 */       logger.info("ERROR: Could not find Curse card of type: " + type.name());
/*      */     }
/* 2217 */     logger.info("ERROR: Default in getCardFromPool");
/* 2218 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */   public static AbstractCard getColorlessCardFromPool(AbstractCard.CardRarity rarity)
/*      */   {
/* 2224 */     switch (rarity) {
/*      */     case RARE: 
/* 2226 */       AbstractCard retVal = colorlessCardPool.getRandomCard(true, rarity);
/* 2227 */       if (retVal != null) {
/* 2228 */         return retVal;
/*      */       }
/*      */     case UNCOMMON: 
/* 2231 */       AbstractCard retVal = colorlessCardPool.getRandomCard(true, rarity);
/* 2232 */       if (retVal != null)
/* 2233 */         return retVal;
/*      */       break;
/*      */     }
/* 2236 */     logger.info("ERROR: getColorlessCardFromPool");
/* 2237 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void removeCardFromPool(String id, String name, AbstractCard.CardRarity rarity, AbstractCard.CardColor color)
/*      */   {
/* 2249 */     if ((color != AbstractCard.CardColor.COLORLESS) && (color != AbstractCard.CardColor.CURSE)) {}
/* 2250 */     Iterator<AbstractCard> i; Iterator<AbstractCard> i; Iterator<AbstractCard> i; switch (rarity) {
/*      */     case RARE: 
/* 2252 */       for (i = rareCardPool.group.iterator(); i.hasNext();) {
/* 2253 */         AbstractCard e = (AbstractCard)i.next();
/* 2254 */         if (e.cardID.equals(id)) {
/* 2255 */           i.remove();
/* 2256 */           logger.info(name + " removed from pool.");
/* 2257 */           return;
/*      */         }
/*      */       }
/* 2260 */       break;
/*      */     case UNCOMMON: 
/* 2262 */       for (i = uncommonCardPool.group.iterator(); i.hasNext();) {
/* 2263 */         AbstractCard e = (AbstractCard)i.next();
/* 2264 */         if (e.cardID.equals(id)) {
/* 2265 */           i.remove();
/* 2266 */           logger.info(name + " removed from pool.");
/* 2267 */           return;
/*      */         }
/*      */       }
/* 2270 */       break;
/*      */     case COMMON: 
/* 2272 */       for (i = commonCardPool.group.iterator(); i.hasNext();) {
/* 2273 */         AbstractCard e = (AbstractCard)i.next();
/* 2274 */         if (e.cardID.equals(id)) {
/* 2275 */           i.remove();
/* 2276 */           logger.info(name + " removed from pool.");
/* 2277 */           return;
/*      */         }
/*      */       }
/* 2280 */       break;
/*      */     default: 
/* 2282 */       logger.info("ERROR: Rarity incorrectly specified: " + rarity.name());
/* 2283 */       break;
/*      */       Iterator<AbstractCard> i;
/* 2285 */       if (color == AbstractCard.CardColor.COLORLESS) {
/* 2286 */         for (i = colorlessCardPool.group.iterator(); i.hasNext();) {
/* 2287 */           AbstractCard e = (AbstractCard)i.next();
/* 2288 */           if (e.cardID.equals(id)) {
/* 2289 */             i.remove();
/* 2290 */             logger.info(name + " removed from pool."); return;
/*      */           }
/*      */         }
/*      */       } else { Iterator<AbstractCard> i;
/* 2294 */         if (color == AbstractCard.CardColor.CURSE) {
/* 2295 */           for (i = curseCardPool.group.iterator(); i.hasNext();) {
/* 2296 */             AbstractCard e = (AbstractCard)i.next();
/* 2297 */             if (e.cardID.equals(id)) {
/* 2298 */               i.remove();
/* 2299 */               logger.info(name + " removed from pool."); return;
/*      */             }
/*      */             
/*      */           }
/*      */         } else
/* 2304 */           logger.info("ERROR: Somebody used removeCardFromPool() incorrectly!!");
/*      */       }
/*      */       break; }
/*      */   }
/*      */   
/* 2309 */   public static AbstractCard.CardRarity rollRarity(com.megacrit.cardcrawl.random.Random rng) { int roll = cardRng.random(99);
/* 2310 */     roll += cardBlizzRandomizer;
/* 2311 */     return getCurrRoom().getCardRarity(roll);
/*      */   }
/*      */   
/*      */   public static AbstractCard.CardRarity rollRarity() {
/* 2315 */     return rollRarity(cardRng);
/*      */   }
/*      */   
/*      */   public static AbstractCard.CardRarity rollRareOrUncommon(float rareChance) {
/* 2319 */     if (cardRng.randomBoolean(rareChance)) {
/* 2320 */       return AbstractCard.CardRarity.RARE;
/*      */     }
/* 2322 */     return AbstractCard.CardRarity.UNCOMMON;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractMonster getRandomMonster()
/*      */   {
/* 2332 */     return getRandomMonster(null);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractMonster getRandomMonster(AbstractMonster except)
/*      */   {
/* 2342 */     return currMapNode.room.monsters.getRandomMonster(except, true);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static void nextRoomTransitionStart()
/*      */   {
/* 2349 */     fadeOut();
/* 2350 */     waitingOnFadeOut = true;
/* 2351 */     overlayMenu.proceedButton.hide();
/* 2352 */     if (((Boolean)DailyMods.negativeMods.get("Terminal")).booleanValue()) {
/* 2353 */       player.decreaseMaxHealth(1);
/*      */     }
/*      */   }
/*      */   
/*      */   public static void initializeFirstRoom() {
/* 2358 */     fadeIn();
/*      */     
/*      */ 
/* 2361 */     floorNum += 1;
/* 2362 */     if ((currMapNode.room instanceof MonsterRoom)) {
/* 2363 */       if (!CardCrawlGame.loadingSave) {
/* 2364 */         if (SaveHelper.shouldSave()) {
/* 2365 */           SaveHelper.saveIfAppropriate(SaveFile.SaveType.ENTER_ROOM);
/*      */         }
/*      */         else {
/* 2368 */           Metrics metrics = new Metrics();
/* 2369 */           metrics.setValues(false, null, Metrics.MetricRequestType.NONE);
/* 2370 */           metrics.gatherAllDataAndSave(false, null);
/*      */         }
/*      */       }
/* 2373 */       floorNum -= 1;
/*      */     }
/*      */     
/*      */ 
/* 2377 */     scene.nextRoom(currMapNode.room);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void nextRoomTransition()
/*      */   {
/* 2384 */     overlayMenu.proceedButton.setLabel(TEXT[0]);
/* 2385 */     sceneOffsetTimer = 1.3F;
/* 2386 */     combatRewardScreen.clear();
/*      */     
/* 2388 */     if ((nextRoom != null) && (nextRoom.room != null)) {
/* 2389 */       nextRoom.room.rewards.clear();
/*      */     }
/*      */     AbstractCard tmpCard;
/* 2392 */     if ((getCurrRoom() instanceof MonsterRoomElite)) {
/* 2393 */       eliteMonsterList.remove(0);
/* 2394 */     } else if ((getCurrRoom() instanceof MonsterRoom)) {
/* 2395 */       monsterList.remove(0);
/* 2396 */     } else if (((getCurrRoom() instanceof EventRoom)) && 
/* 2397 */       ((getCurrRoom().event instanceof NoteForYourself))) {
/* 2398 */       tmpCard = ((NoteForYourself)getCurrRoom().event).saveCard;
/* 2399 */       if (tmpCard != null) {
/* 2400 */         CardCrawlGame.playerPref.putString("NOTE_CARD", tmpCard.cardID);
/* 2401 */         CardCrawlGame.playerPref.putInteger("NOTE_UPGRADE", tmpCard.timesUpgraded);
/* 2402 */         CardCrawlGame.playerPref.flush();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2407 */     getCurrRoom().clearEvent();
/* 2408 */     gridSelectScreen.upgradePreviewCard = null;
/* 2409 */     previousScreen = null;
/* 2410 */     dynamicBanner.hide();
/* 2411 */     dungeonMapScreen.closeInstantly();
/* 2412 */     closeCurrentScreen();
/* 2413 */     topPanel.unhoverHitboxes();
/* 2414 */     fadeIn();
/* 2415 */     player.resetControllerValues();
/* 2416 */     effectList.clear();
/* 2417 */     topLevelEffects.clear();
/* 2418 */     topLevelEffectsQueue.clear();
/* 2419 */     effectsQueue.clear();
/* 2420 */     dungeonMapScreen.dismissable = true;
/* 2421 */     dungeonMapScreen.map.legend.isLegendHighlighted = false;
/*      */     
/*      */ 
/* 2424 */     player.orbs.clear();
/* 2425 */     player.animX = 0.0F;
/* 2426 */     player.animY = 0.0F;
/* 2427 */     player.hideHealthBar();
/* 2428 */     player.hand.clear();
/* 2429 */     player.powers.clear();
/* 2430 */     player.drawPile.clear();
/* 2431 */     player.discardPile.clear();
/* 2432 */     player.exhaustPile.clear();
/* 2433 */     player.limbo.clear();
/* 2434 */     player.loseBlock(true);
/* 2435 */     player.damagedThisCombat = 0;
/* 2436 */     GameActionManager.turn = 1;
/*      */     
/* 2438 */     if (!CardCrawlGame.loadingSave) {
/* 2439 */       incrementFloorBasedMetrics();
/* 2440 */       floorNum += 1;
/* 2441 */       if ((!((Boolean)TipTracker.tips.get("INTENT_TIP")).booleanValue()) && (floorNum == 6)) {
/* 2442 */         TipTracker.neverShowAgain("INTENT_TIP");
/*      */       }
/*      */       
/* 2445 */       StatsScreen.incrementFloorClimbed();
/* 2446 */       SaveHelper.saveIfAppropriate(SaveFile.SaveType.ENTER_ROOM);
/*      */     }
/*      */     
/* 2449 */     monsterHpRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + floorNum));
/* 2450 */     aiRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + floorNum));
/* 2451 */     shuffleRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + floorNum));
/* 2452 */     cardRandomRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + floorNum));
/* 2453 */     miscRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + floorNum));
/*      */     
/* 2455 */     if (nextRoom != null) {
/* 2456 */       for (AbstractRelic r : player.relics) {
/* 2457 */         r.onEnterRoom(nextRoom.room);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2462 */     if (!actionManager.actions.isEmpty()) {
/* 2463 */       logger.info("[WARNING] Line:1904: Action Manager was NOT clear! Clearing");
/* 2464 */       actionManager.clear();
/*      */     }
/*      */     
/* 2467 */     if (nextRoom != null) {
/* 2468 */       if ((nextRoom.room instanceof EventRoom)) {
/* 2469 */         nextRoom.room = generateRoom(EventHelper.roll());
/* 2470 */         if (((nextRoom.room instanceof MonsterRoom)) || ((nextRoom.room instanceof MonsterRoomElite))) {
/* 2471 */           nextRoom.room.combatEvent = true;
/*      */         }
/* 2473 */         nextRoom.room.setMapImg(ImageMaster.MAP_NODE_EVENT, ImageMaster.MAP_NODE_EVENT_OUTLINE);
/* 2474 */       } else if ((nextRoom.room instanceof MonsterRoomBoss))
/*      */       {
/* 2476 */         logger.info("WALKING INTO BOSS ROOM");
/*      */       }
/*      */       
/* 2479 */       setCurrMapNode(nextRoom);
/*      */     }
/* 2481 */     if (getCurrRoom() != null) {
/* 2482 */       for (AbstractRelic r : player.relics) {
/* 2483 */         r.justEnteredRoom(getCurrRoom());
/*      */       }
/*      */     }
/* 2486 */     getCurrRoom().onPlayerEntry();
/*      */     
/* 2488 */     player.drawX = (Settings.WIDTH * 0.25F);
/* 2489 */     player.drawY = floorY;
/*      */     
/*      */ 
/* 2492 */     if ((currMapNode.room instanceof MonsterRoom)) {
/* 2493 */       player.preBattlePrep();
/*      */     }
/*      */     
/*      */ 
/* 2497 */     scene.nextRoom(currMapNode.room);
/*      */     
/* 2499 */     if ((currMapNode.room instanceof RestRoom)) {
/* 2500 */       rs = RenderScene.CAMPFIRE;
/* 2501 */     } else if ((currMapNode.room.event instanceof AbstractImageEvent)) {
/* 2502 */       rs = RenderScene.EVENT;
/*      */     } else {
/* 2504 */       rs = RenderScene.NORMAL;
/*      */     }
/*      */   }
/*      */   
/*      */   private void incrementFloorBasedMetrics() {
/* 2509 */     if (floorNum != 0) {
/* 2510 */       CardCrawlGame.metricData.current_hp_per_floor.add(Integer.valueOf(player.currentHealth));
/* 2511 */       CardCrawlGame.metricData.max_hp_per_floor.add(Integer.valueOf(player.maxHealth));
/* 2512 */       CardCrawlGame.metricData.gold_per_floor.add(Integer.valueOf(player.gold));
/* 2513 */       CardCrawlGame.metricData.path_per_floor.add(getCurrMapNode().getRoomSymbol(Boolean.valueOf(true)));
/*      */     }
/*      */   }
/*      */   
/*      */   private AbstractRoom generateRoom(EventHelper.RoomResult roomType) {
/* 2518 */     logger.info("GENERATING ROOM: " + roomType.name());
/* 2519 */     switch (roomType) {
/*      */     case ELITE: 
/* 2521 */       return new MonsterRoomElite();
/*      */     case MONSTER: 
/* 2523 */       return new MonsterRoom();
/*      */     case SHOP: 
/* 2525 */       return new ShopRoom();
/*      */     case TREASURE: 
/* 2527 */       return new TreasureRoom();
/*      */     }
/* 2529 */     return new EventRoom();
/*      */   }
/*      */   
/*      */   public static MonsterGroup getMonsters()
/*      */   {
/* 2534 */     return getCurrRoom().monsters;
/*      */   }
/*      */   
/*      */   public static MonsterGroup getMonsterForRoomCreation() {
/* 2538 */     lastCombatMetricKey = (String)monsterList.get(0);
/* 2539 */     logger.info("Monster Removed");
/* 2540 */     return MonsterHelper.getEncounter((String)monsterList.get(0));
/*      */   }
/*      */   
/*      */   public static MonsterGroup getEliteMonsterForRoomCreation() {
/* 2544 */     lastCombatMetricKey = (String)eliteMonsterList.get(0);
/* 2545 */     return MonsterHelper.getEncounter((String)eliteMonsterList.get(0));
/*      */   }
/*      */   
/*      */   public static AbstractEvent generateEvent()
/*      */   {
/* 2550 */     if (eventRng.random(1.0F) < shrineChance) {
/* 2551 */       if ((!shrineList.isEmpty()) || (!specialOneTimeEventList.isEmpty()))
/* 2552 */         return getShrine();
/* 2553 */       if (!eventList.isEmpty()) {
/* 2554 */         return getEvent();
/*      */       }
/* 2556 */       logger.info("No events or shrines left");
/* 2557 */       return null;
/*      */     }
/*      */     
/*      */ 
/* 2561 */     AbstractEvent retVal = getEvent();
/* 2562 */     if (retVal == null) {
/* 2563 */       return getShrine();
/*      */     }
/* 2565 */     return retVal;
/*      */   }
/*      */   
/*      */ 
/*      */   public static AbstractEvent getShrine()
/*      */   {
/* 2571 */     ArrayList<String> tmp = new ArrayList();
/* 2572 */     tmp.addAll(shrineList);
/*      */     
/*      */ 
/* 2575 */     for (String e : specialOneTimeEventList) {
/* 2576 */       switch (e) {
/*      */       case "Fountain of Cleansing": 
/* 2578 */         if (player.isCursed()) {
/* 2579 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "Designer": 
/* 2583 */         if (((id.equals("TheCity")) || (id.equals("TheBeyond"))) && (player.gold >= 75)) {
/* 2584 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "Duplicator": 
/* 2588 */         if ((id.equals("TheCity")) || (id.equals("TheBeyond"))) {
/* 2589 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "Knowing Skull": 
/* 2593 */         if ((id.equals("TheCity")) && (player.currentHealth > player.maxHealth / 2)) {
/* 2594 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "N'loth": 
/* 2598 */         if (((id.equals("TheCity")) || (id.equals("TheCity"))) && (player.relics.size() >= 2)) {
/* 2599 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "The Joust": 
/* 2603 */         if ((id.equals("TheCity")) && (player.gold >= 50)) {
/* 2604 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "The Woman in Blue": 
/* 2608 */         if (player.gold >= 50) {
/* 2609 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "SecretPortal": 
/* 2613 */         if ((CardCrawlGame.playtime >= 800.0F) && (id.equals("TheBeyond"))) {
/* 2614 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       default: 
/* 2618 */         tmp.add(e);
/*      */       }
/*      */       
/*      */     }
/*      */     
/* 2623 */     String tmpKey = (String)tmp.get(eventRng.random(tmp.size() - 1));
/* 2624 */     shrineList.remove(tmpKey);
/* 2625 */     specialOneTimeEventList.remove(tmpKey);
/* 2626 */     logger.info("Removed event: " + tmpKey + " from pool.");
/*      */     
/* 2628 */     return EventHelper.getEvent(tmpKey);
/*      */   }
/*      */   
/*      */   public static AbstractEvent getEvent() {
/* 2632 */     ArrayList<String> tmp = new ArrayList();
/* 2633 */     for (String e : eventList) {
/* 2634 */       switch (e) {
/*      */       case "Dead Adventurer": 
/* 2636 */         if (floorNum > 6) {
/* 2637 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "Mushrooms": 
/* 2641 */         if (floorNum > 6) {
/* 2642 */           tmp.add(e);
/*      */         }
/*      */         break;
/*      */       case "The Moai Head": 
/* 2646 */         if ((player.hasRelic("Golden Idol")) || (player.currentHealth / player.maxHealth <= 0.5F))
/*      */         {
/*      */ 
/*      */ 
/* 2650 */           tmp.add(e);
/*      */         }
/* 2652 */         break;
/*      */       default: 
/* 2654 */         tmp.add(e);
/*      */       }
/*      */       
/*      */     }
/*      */     
/* 2659 */     if (tmp.isEmpty()) {
/* 2660 */       return getShrine();
/*      */     }
/*      */     
/* 2663 */     String tmpKey = (String)tmp.get(eventRng.random(tmp.size() - 1));
/* 2664 */     eventList.remove(tmpKey);
/* 2665 */     logger.info("Removed event: " + tmpKey + " from pool.");
/*      */     
/* 2667 */     return EventHelper.getEvent(tmpKey);
/*      */   }
/*      */   
/*      */   public MonsterGroup getBoss() {
/* 2671 */     lastCombatMetricKey = bossKey;
/* 2672 */     dungeonMapScreen.map.atBoss = true;
/* 2673 */     return MonsterHelper.getEncounter(bossKey);
/*      */   }
/*      */   
/*      */   public void update()
/*      */   {
/* 2678 */     if (!CardCrawlGame.stopClock) {
/* 2679 */       CardCrawlGame.playtime += Gdx.graphics.getDeltaTime();
/*      */     }
/*      */     
/* 2682 */     if (CardCrawlGame.screenTimer > 0.0F) {
/* 2683 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 2684 */       CInputActionSet.select.unpress();
/*      */     }
/*      */     
/* 2687 */     updateSceneOffset();
/* 2688 */     topPanel.update();
/* 2689 */     dynamicButton.update();
/* 2690 */     dynamicBanner.update();
/* 2691 */     updateFading();
/* 2692 */     currMapNode.room.updateObjects();
/* 2693 */     if (isScreenUp) {
/* 2694 */       topGradientColor.a = MathHelper.fadeLerpSnap(topGradientColor.a, 0.25F);
/* 2695 */       botGradientColor.a = MathHelper.fadeLerpSnap(botGradientColor.a, 0.25F);
/*      */     } else {
/* 2697 */       topGradientColor.a = MathHelper.fadeLerpSnap(topGradientColor.a, 0.1F);
/* 2698 */       botGradientColor.a = MathHelper.fadeLerpSnap(botGradientColor.a, 0.1F);
/*      */     }
/*      */     
/* 2701 */     switch (screen) {
/*      */     case NONE: 
/* 2703 */       dungeonMapScreen.update();
/* 2704 */       currMapNode.room.update();
/* 2705 */       scene.update();
/* 2706 */       currMapNode.room.eventControllerInput();
/* 2707 */       break;
/*      */     case FTUE: 
/* 2709 */       ftue.update();
/* 2710 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedRight = false;
/* 2711 */       com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft = false;
/* 2712 */       currMapNode.room.update();
/* 2713 */       break;
/*      */     case MASTER_DECK_VIEW: 
/* 2715 */       deckViewScreen.update();
/* 2716 */       break;
/*      */     case GAME_DECK_VIEW: 
/* 2718 */       gameDeckViewScreen.update();
/* 2719 */       break;
/*      */     case DISCARD_VIEW: 
/* 2721 */       discardPileViewScreen.update();
/* 2722 */       break;
/*      */     case EXHAUST_VIEW: 
/* 2724 */       exhaustPileViewScreen.update();
/* 2725 */       break;
/*      */     case SETTINGS: 
/* 2727 */       settingsScreen.update();
/* 2728 */       break;
/*      */     case INPUT_SETTINGS: 
/* 2730 */       inputSettingsScreen.update();
/* 2731 */       break;
/*      */     case MAP: 
/* 2733 */       dungeonMapScreen.update();
/* 2734 */       break;
/*      */     case GRID: 
/* 2736 */       gridSelectScreen.update();
/* 2737 */       break;
/*      */     case CARD_REWARD: 
/* 2739 */       cardRewardScreen.update();
/* 2740 */       break;
/*      */     case COMBAT_REWARD: 
/* 2742 */       combatRewardScreen.update();
/* 2743 */       break;
/*      */     case BOSS_REWARD: 
/* 2745 */       bossRelicScreen.update();
/* 2746 */       currMapNode.room.update();
/* 2747 */       break;
/*      */     case HAND_SELECT: 
/* 2749 */       handCardSelectScreen.update();
/* 2750 */       currMapNode.room.update();
/* 2751 */       break;
/*      */     case SHOP: 
/* 2753 */       shopScreen.update();
/* 2754 */       break;
/*      */     case DEATH: 
/* 2756 */       deathScreen.update();
/* 2757 */       break;
/*      */     case UNLOCK: 
/* 2759 */       unlockScreen.update();
/* 2760 */       break;
/*      */     case NEOW_UNLOCK: 
/* 2762 */       gUnlockScreen.update();
/* 2763 */       break;
/*      */     case CREDITS: 
/* 2765 */       creditsScreen.update();
/* 2766 */       break;
/*      */     default: 
/* 2768 */       logger.info("ERROR: UNKNOWN SCREEN TO UPDATE: " + screen.name());
/*      */     }
/*      */     
/* 2771 */     turnPhaseEffectActive = false;
/*      */     
/*      */ 
/* 2774 */     for (Iterator<AbstractGameEffect> i = topLevelEffects.iterator(); i.hasNext();) {
/* 2775 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 2776 */       e.update();
/* 2777 */       if (e.isDone) {
/* 2778 */         i.remove();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2783 */     for (Iterator<AbstractGameEffect> i = effectList.iterator(); i.hasNext();) {
/* 2784 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 2785 */       e.update();
/* 2786 */       if ((e instanceof PlayerTurnEffect)) {
/* 2787 */         turnPhaseEffectActive = true;
/*      */       }
/* 2789 */       if (e.isDone) {
/* 2790 */         i.remove();
/*      */       }
/*      */     }
/*      */     
/* 2794 */     for (Iterator<AbstractGameEffect> i = effectsQueue.iterator(); i.hasNext();) {
/* 2795 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 2796 */       effectList.add(e);
/* 2797 */       i.remove();
/*      */     }
/*      */     
/* 2800 */     for (Iterator<AbstractGameEffect> i = topLevelEffectsQueue.iterator(); i.hasNext();) {
/* 2801 */       AbstractGameEffect e = (AbstractGameEffect)i.next();
/* 2802 */       topLevelEffects.add(e);
/* 2803 */       i.remove();
/*      */     }
/*      */     
/* 2806 */     overlayMenu.update();
/*      */   }
/*      */   
/*      */   private void updateSceneOffset() {
/* 2810 */     if (sceneOffsetTimer != 0.0F) {
/* 2811 */       sceneOffsetTimer -= Gdx.graphics.getDeltaTime();
/* 2812 */       if (sceneOffsetTimer < 0.0F) {
/* 2813 */         sceneOffsetY = 0.0F;
/* 2814 */         sceneOffsetTimer = 0.0F;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void render(SpriteBatch sb)
/*      */   {
/* 2826 */     switch (rs) {
/*      */     case NORMAL: 
/* 2828 */       scene.renderCombatRoomBg(sb);
/* 2829 */       break;
/*      */     case CAMPFIRE: 
/* 2831 */       scene.renderCampfireRoom(sb);
/* 2832 */       break;
/*      */     case EVENT: 
/* 2834 */       scene.renderEventRoom(sb);
/*      */     }
/*      */     
/*      */     
/* 2838 */     dynamicButton.render(sb);
/*      */     
/*      */ 
/* 2841 */     for (Iterator localIterator = effectList.iterator(); localIterator.hasNext();) { e = (AbstractGameEffect)localIterator.next();
/* 2842 */       if (e.renderBehind) {
/* 2843 */         e.render(sb);
/*      */       }
/*      */     }
/*      */     AbstractGameEffect e;
/* 2847 */     currMapNode.room.render(sb);
/* 2848 */     overlayMenu.renderBgPanels(sb);
/* 2849 */     switch (rs) {
/*      */     case NORMAL: 
/* 2851 */       scene.renderCombatRoomFg(sb);
/* 2852 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 2857 */     if (rs == RenderScene.NORMAL) {
/* 2858 */       scene.renderCombatRoomFg(sb);
/*      */     }
/*      */     
/* 2861 */     AbstractRoom room = getCurrRoom();
/*      */     
/* 2863 */     if (((room instanceof EventRoom)) || ((room instanceof NeowRoom)) || ((room instanceof VictoryRoom))) {
/* 2864 */       room.renderEventTexts(sb);
/*      */     }
/*      */     
/*      */ 
/* 2868 */     for (AbstractGameEffect e : effectList) {
/* 2869 */       if (!e.renderBehind) {
/* 2870 */         e.render(sb);
/*      */       }
/*      */     }
/*      */     
/* 2874 */     overlayMenu.render(sb);
/* 2875 */     overlayMenu.renderBlackScreen(sb);
/*      */     
/* 2877 */     switch (screen) {
/*      */     case NONE: 
/* 2879 */       dungeonMapScreen.render(sb);
/* 2880 */       break;
/*      */     case MASTER_DECK_VIEW: 
/* 2882 */       deckViewScreen.render(sb);
/* 2883 */       break;
/*      */     case DISCARD_VIEW: 
/* 2885 */       discardPileViewScreen.render(sb);
/* 2886 */       break;
/*      */     case GAME_DECK_VIEW: 
/* 2888 */       gameDeckViewScreen.render(sb);
/* 2889 */       break;
/*      */     case EXHAUST_VIEW: 
/* 2891 */       exhaustPileViewScreen.render(sb);
/* 2892 */       break;
/*      */     case SETTINGS: 
/* 2894 */       settingsScreen.render(sb);
/* 2895 */       break;
/*      */     case INPUT_SETTINGS: 
/* 2897 */       inputSettingsScreen.render(sb);
/* 2898 */       break;
/*      */     case MAP: 
/* 2900 */       dungeonMapScreen.render(sb);
/* 2901 */       break;
/*      */     case GRID: 
/* 2903 */       gridSelectScreen.render(sb);
/* 2904 */       break;
/*      */     case CARD_REWARD: 
/* 2906 */       cardRewardScreen.render(sb);
/* 2907 */       break;
/*      */     case COMBAT_REWARD: 
/* 2909 */       combatRewardScreen.render(sb);
/* 2910 */       break;
/*      */     case BOSS_REWARD: 
/* 2912 */       bossRelicScreen.render(sb);
/* 2913 */       break;
/*      */     case HAND_SELECT: 
/* 2915 */       handCardSelectScreen.render(sb);
/* 2916 */       break;
/*      */     case SHOP: 
/* 2918 */       shopScreen.render(sb);
/* 2919 */       break;
/*      */     case DEATH: 
/* 2921 */       deathScreen.render(sb);
/* 2922 */       break;
/*      */     case UNLOCK: 
/* 2924 */       unlockScreen.render(sb);
/* 2925 */       break;
/*      */     case NEOW_UNLOCK: 
/* 2927 */       gUnlockScreen.render(sb);
/* 2928 */       break;
/*      */     case CREDITS: 
/* 2930 */       creditsScreen.render(sb);
/*      */     case FTUE: 
/*      */       break;
/*      */     default: 
/* 2934 */       logger.info("ERROR: UNKNOWN SCREEN TO RENDER: " + screen.name());
/*      */     }
/*      */     
/*      */     
/* 2938 */     if (screen != CurrentScreen.UNLOCK) {
/* 2939 */       sb.setColor(topGradientColor);
/* 2940 */       if (!Settings.hideTopBar) {
/* 2941 */         sb.draw(ImageMaster.SCROLL_GRADIENT, 0.0F, Settings.HEIGHT - 128.0F * Settings.scale, Settings.WIDTH, 64.0F * Settings.scale);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2948 */       sb.setColor(botGradientColor);
/* 2949 */       if (!Settings.hideTopBar) {
/* 2950 */         sb.draw(ImageMaster.SCROLL_GRADIENT, 0.0F, 64.0F * Settings.scale, Settings.WIDTH, -64.0F * Settings.scale);
/*      */       }
/*      */     }
/*      */     
/* 2954 */     if (screen == CurrentScreen.FTUE) {
/* 2955 */       ftue.render(sb);
/*      */     }
/*      */     
/* 2958 */     overlayMenu.cancelButton.render(sb);
/* 2959 */     dynamicBanner.render(sb);
/* 2960 */     if (screen != CurrentScreen.UNLOCK) {
/* 2961 */       topPanel.render(sb);
/*      */     }
/* 2963 */     currMapNode.room.renderAboveTopPanel(sb);
/*      */     
/* 2965 */     for (AbstractGameEffect e : topLevelEffects) {
/* 2966 */       if (!e.renderBehind) {
/* 2967 */         e.render(sb);
/*      */       }
/*      */     }
/*      */     
/* 2971 */     sb.setColor(fadeColor);
/* 2972 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*      */   }
/*      */   
/*      */   public void updateFading()
/*      */   {
/* 2977 */     if (isFadingIn)
/*      */     {
/* 2979 */       fadeTimer -= Gdx.graphics.getDeltaTime();
/* 2980 */       fadeColor.a = Interpolation.fade.apply(0.0F, 1.0F, fadeTimer / 0.8F);
/* 2981 */       if (fadeTimer < 0.0F) {
/* 2982 */         isFadingIn = false;
/* 2983 */         fadeColor.a = 0.0F;
/* 2984 */         fadeTimer = 0.0F;
/*      */       }
/* 2986 */     } else if (isFadingOut)
/*      */     {
/* 2988 */       fadeTimer -= Gdx.graphics.getDeltaTime();
/* 2989 */       fadeColor.a = Interpolation.fade.apply(1.0F, 0.0F, fadeTimer / 0.8F);
/* 2990 */       if (fadeTimer < 0.0F) {
/* 2991 */         fadeTimer = 0.0F;
/* 2992 */         isFadingOut = false;
/* 2993 */         fadeColor.a = 1.0F;
/* 2994 */         if (!isDungeonBeaten) {
/* 2995 */           nextRoomTransition();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static void closeCurrentScreen()
/*      */   {
/* 3005 */     switch (screen) {
/*      */     case MASTER_DECK_VIEW: 
/* 3007 */       overlayMenu.cancelButton.hide();
/* 3008 */       genericScreenOverlayReset();
/* 3009 */       for (AbstractCard c : player.masterDeck.group) {
/* 3010 */         c.unhover();
/* 3011 */         c.untip();
/*      */       }
/* 3013 */       break;
/*      */     case DISCARD_VIEW: 
/* 3015 */       overlayMenu.cancelButton.hide();
/* 3016 */       genericScreenOverlayReset();
/* 3017 */       for (AbstractCard c : player.discardPile.group) {
/* 3018 */         c.drawScale = 0.12F;
/* 3019 */         c.targetDrawScale = 0.12F;
/* 3020 */         c.teleportToDiscardPile();
/* 3021 */         c.darken(true);
/* 3022 */         c.unhover();
/*      */       }
/* 3024 */       break;
/*      */     case FTUE: 
/* 3026 */       genericScreenOverlayReset();
/* 3027 */       break;
/*      */     case GAME_DECK_VIEW: 
/* 3029 */       overlayMenu.cancelButton.hide();
/* 3030 */       genericScreenOverlayReset();
/* 3031 */       break;
/*      */     case EXHAUST_VIEW: 
/* 3033 */       overlayMenu.cancelButton.hide();
/* 3034 */       genericScreenOverlayReset();
/* 3035 */       break;
/*      */     case SETTINGS: 
/* 3037 */       overlayMenu.cancelButton.hide();
/* 3038 */       genericScreenOverlayReset();
/* 3039 */       dynamicButton.hide();
/* 3040 */       settingsScreen.abandonPopup.hide();
/* 3041 */       settingsScreen.exitPopup.hide();
/* 3042 */       break;
/*      */     case INPUT_SETTINGS: 
/* 3044 */       overlayMenu.cancelButton.hide();
/* 3045 */       genericScreenOverlayReset();
/* 3046 */       dynamicButton.hide();
/* 3047 */       settingsScreen.abandonPopup.hide();
/* 3048 */       settingsScreen.exitPopup.hide();
/* 3049 */       break;
/*      */     case NEOW_UNLOCK: 
/* 3051 */       genericScreenOverlayReset();
/* 3052 */       dynamicButton.hide();
/* 3053 */       CardCrawlGame.sound.stop("UNLOCK_SCREEN", gUnlockScreen.id);
/* 3054 */       break;
/*      */     case GRID: 
/* 3056 */       genericScreenOverlayReset();
/* 3057 */       if (!combatRewardScreen.rewards.isEmpty()) {
/* 3058 */         previousScreen = CurrentScreen.COMBAT_REWARD;
/*      */       }
/*      */       break;
/*      */     case CARD_REWARD: 
/* 3062 */       overlayMenu.cancelButton.hide();
/* 3063 */       dynamicBanner.hide();
/* 3064 */       genericScreenOverlayReset();
/* 3065 */       if (!screenSwap) {
/* 3066 */         cardRewardScreen.onClose();
/*      */       }
/*      */       break;
/*      */     case COMBAT_REWARD: 
/* 3070 */       dynamicButton.hide();
/* 3071 */       dynamicBanner.hide();
/* 3072 */       genericScreenOverlayReset();
/* 3073 */       break;
/*      */     case BOSS_REWARD: 
/* 3075 */       genericScreenOverlayReset();
/* 3076 */       dynamicBanner.hide();
/* 3077 */       break;
/*      */     case HAND_SELECT: 
/* 3079 */       genericScreenOverlayReset();
/* 3080 */       overlayMenu.showCombatPanels();
/* 3081 */       break;
/*      */     case MAP: 
/* 3083 */       genericScreenOverlayReset();
/* 3084 */       dungeonMapScreen.close();
/* 3085 */       if ((!firstRoomChosen) && (nextRoom != null) && (!dungeonMapScreen.dismissable)) {
/* 3086 */         firstRoomChosen = true;
/* 3087 */         firstRoomLogic();
/*      */       }
/*      */       break;
/*      */     case SHOP: 
/* 3091 */       CardCrawlGame.sound.play("SHOP_CLOSE");
/* 3092 */       genericScreenOverlayReset();
/* 3093 */       overlayMenu.cancelButton.hide();
/* 3094 */       break;
/*      */     case TRANSFORM: 
/* 3096 */       CardCrawlGame.sound.play("ATTACK_MAGIC_SLOW_1");
/* 3097 */       genericScreenOverlayReset();
/* 3098 */       overlayMenu.cancelButton.hide();
/* 3099 */       break;
/*      */     case DEATH: case UNLOCK: case CREDITS: default: 
/* 3101 */       logger.info("UNSPECIFIED CASE: " + screen.name());
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 3106 */     if (previousScreen == null) {
/* 3107 */       screen = CurrentScreen.NONE;
/*      */     }
/* 3109 */     else if (screenSwap) {
/* 3110 */       screenSwap = false;
/*      */     }
/*      */     else {
/* 3113 */       screen = previousScreen;
/* 3114 */       previousScreen = null;
/* 3115 */       isScreenUp = true;
/* 3116 */       openPreviousScreen(screen);
/*      */     }
/*      */   }
/*      */   
/*      */   private static void openPreviousScreen(CurrentScreen s) {
/* 3121 */     switch (s) {
/*      */     case DEATH: 
/* 3123 */       deathScreen.reopen();
/* 3124 */       break;
/*      */     case MASTER_DECK_VIEW: 
/* 3126 */       deckViewScreen.open();
/* 3127 */       break;
/*      */     case CARD_REWARD: 
/* 3129 */       cardRewardScreen.reopen();
/*      */       
/*      */ 
/* 3132 */       if (cardRewardScreen.rItem != null) {
/* 3133 */         previousScreen = CurrentScreen.COMBAT_REWARD;
/*      */       }
/*      */       break;
/*      */     case DISCARD_VIEW: 
/* 3137 */       discardPileViewScreen.reopen();
/* 3138 */       break;
/*      */     case EXHAUST_VIEW: 
/* 3140 */       exhaustPileViewScreen.reopen();
/* 3141 */       break;
/*      */     case GAME_DECK_VIEW: 
/* 3143 */       gameDeckViewScreen.reopen();
/* 3144 */       break;
/*      */     case HAND_SELECT: 
/* 3146 */       overlayMenu.hideBlackScreen();
/* 3147 */       handCardSelectScreen.reopen();
/* 3148 */       break;
/*      */     case COMBAT_REWARD: 
/* 3150 */       combatRewardScreen.reopen();
/* 3151 */       break;
/*      */     case BOSS_REWARD: 
/* 3153 */       bossRelicScreen.reopen();
/* 3154 */       break;
/*      */     case SHOP: 
/* 3156 */       shopScreen.open();
/* 3157 */       break;
/*      */     case GRID: 
/* 3159 */       overlayMenu.hideBlackScreen();
/* 3160 */       gridSelectScreen.reopen();
/* 3161 */       break;
/*      */     case NEOW_UNLOCK: 
/* 3163 */       gUnlockScreen.reOpen();
/* 3164 */       break;
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void genericScreenOverlayReset()
/*      */   {
/* 3174 */     if (previousScreen == null) {
/* 3175 */       if (player.isDead) {
/* 3176 */         previousScreen = CurrentScreen.DEATH;
/*      */       } else {
/* 3178 */         isScreenUp = false;
/* 3179 */         overlayMenu.hideBlackScreen();
/*      */       }
/*      */     }
/*      */     
/* 3183 */     if ((getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) && (!player.isDead)) {
/* 3184 */       overlayMenu.showCombatPanels();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void fadeIn()
/*      */   {
/* 3193 */     if (fadeColor.a != 1.0F) {
/* 3194 */       logger.info("WARNING: Attempting to fade in even though screen is not black");
/*      */     }
/* 3196 */     isFadingIn = true;
/* 3197 */     if (Settings.FAST_MODE) {
/* 3198 */       fadeTimer = 0.001F;
/*      */     } else {
/* 3200 */       fadeTimer = 0.8F;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static void fadeOut()
/*      */   {
/* 3208 */     if (fadeTimer == 0.0F) {
/* 3209 */       if (fadeColor.a != 0.0F) {
/* 3210 */         logger.info("WARNING: Attempting to fade out even though screen is not transparent");
/*      */       }
/* 3212 */       isFadingOut = true;
/* 3213 */       if (Settings.FAST_MODE) {
/* 3214 */         fadeTimer = 0.001F;
/*      */       } else {
/* 3216 */         fadeTimer = 0.8F;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public static enum CurrentScreen
/*      */   {
/* 3226 */     NONE,  MASTER_DECK_VIEW,  SETTINGS,  INPUT_SETTINGS,  GRID,  MAP,  FTUE,  CHOOSE_ONE,  HAND_SELECT,  SHOP,  COMBAT_REWARD,  DISCARD_VIEW,  EXHAUST_VIEW,  GAME_DECK_VIEW,  BOSS_REWARD,  DEATH,  CARD_REWARD,  TRANSFORM,  VICTORY,  UNLOCK,  CREDITS,  NEOW_UNLOCK;
/*      */     
/*      */     private CurrentScreen() {} }
/*      */   
/* 3230 */   public static enum RenderScene { NORMAL,  EVENT,  CAMPFIRE;
/*      */     
/*      */     private RenderScene() {}
/*      */   }
/*      */   
/*      */   public static void dungeonTransitionSetup()
/*      */   {
/* 3237 */     actNum += 1;
/*      */     
/*      */ 
/* 3240 */     if ((cardRng.counter > 0) && (cardRng.counter < 250)) {
/* 3241 */       cardRng.setCounter(250);
/* 3242 */     } else if ((cardRng.counter > 250) && (cardRng.counter < 500)) {
/* 3243 */       cardRng.setCounter(500);
/* 3244 */     } else if ((cardRng.counter > 500) && (cardRng.counter < 750)) {
/* 3245 */       cardRng.setCounter(750);
/*      */     }
/* 3247 */     logger.info("CardRng Counter: " + cardRng.counter);
/*      */     
/* 3249 */     topPanel.unhoverHitboxes();
/*      */     
/* 3251 */     pathX.clear();
/* 3252 */     pathY.clear();
/*      */     
/*      */ 
/* 3255 */     EventHelper.resetProbabilities();
/* 3256 */     eventList.clear();
/* 3257 */     shrineList.clear();
/*      */     
/*      */ 
/* 3260 */     monsterList.clear();
/* 3261 */     eliteMonsterList.clear();
/* 3262 */     bossList.clear();
/*      */     
/*      */ 
/* 3265 */     AbstractRoom.blizzardPotionMod = 0;
/*      */     
/*      */ 
/*      */ 
/* 3269 */     if (ascensionLevel >= 5) {
/* 3270 */       player.heal(MathUtils.round((player.maxHealth - player.currentHealth) * 0.75F), false);
/*      */     } else {
/* 3272 */       player.heal(player.maxHealth, false);
/*      */     }
/*      */     
/*      */ 
/* 3276 */     if (floorNum <= 1)
/*      */     {
/* 3278 */       if ((CardCrawlGame.dungeon instanceof Exordium)) {
/* 3279 */         if (ascensionLevel >= 14) {
/* 3280 */           switch (player.chosenClass) {
/*      */           case IRONCLAD: 
/* 3282 */             player.decreaseMaxHealth(5);
/* 3283 */             break;
/*      */           case THE_SILENT: 
/* 3285 */             player.decreaseMaxHealth(4);
/* 3286 */             break;
/*      */           case DEFECT: 
/* 3288 */             player.decreaseMaxHealth(4);
/* 3289 */             break;
/*      */           }
/*      */           
/*      */         }
/*      */         
/* 3294 */         if (ascensionLevel >= 6) {
/* 3295 */           player.currentHealth = MathUtils.round(player.maxHealth * 0.9F);
/*      */         }
/* 3297 */         if (ascensionLevel >= 10) {
/* 3298 */           player.masterDeck.addToTop(new AscendersBane());
/* 3299 */           UnlockTracker.markCardAsSeen("AscendersBane");
/*      */         }
/* 3301 */         CardCrawlGame.playtime = 0.0F;
/*      */       }
/*      */     }
/*      */     
/* 3305 */     dungeonMapScreen.map.atBoss = false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public static void reset()
/*      */   {
/* 3312 */     logger.info("Resetting variables...");
/* 3313 */     CardCrawlGame.resetScoreVars();
/* 3314 */     DailyMods.setModsFalse();
/* 3315 */     floorNum = 0;
/* 3316 */     actNum = 0;
/*      */     
/* 3318 */     if ((currMapNode != null) && (getCurrRoom().monsters != null)) {
/* 3319 */       for (AbstractMonster m : getCurrRoom().monsters.monsters) {
/* 3320 */         m.dispose();
/*      */       }
/*      */     }
/*      */     
/* 3324 */     shrineList.clear();
/* 3325 */     relicsToRemoveOnStart.clear();
/* 3326 */     previousScreen = null;
/* 3327 */     actionManager.clear();
/* 3328 */     actionManager.clearNextRoomCombatActions();
/* 3329 */     combatRewardScreen.clear();
/* 3330 */     cardRewardScreen.reset();
/* 3331 */     effectList.clear();
/* 3332 */     effectsQueue.clear();
/* 3333 */     topLevelEffectsQueue.clear();
/* 3334 */     topLevelEffects.clear();
/* 3335 */     firstChest = true;
/* 3336 */     encounteredCursedChest = false;
/* 3337 */     cardBlizzRandomizer = cardBlizzStartOffset;
/*      */     
/* 3339 */     if (player != null) {
/* 3340 */       player.relics.clear();
/*      */     }
/*      */     
/* 3343 */     rs = RenderScene.NORMAL;
/* 3344 */     blightPool.clear();
/*      */   }
/*      */   
/*      */   protected void removeRelicFromPool(ArrayList<String> pool, String name)
/*      */   {
/* 3349 */     for (Iterator<String> i = pool.iterator(); i.hasNext();) {
/* 3350 */       String s = (String)i.next();
/* 3351 */       if (s.equals(name)) {
/* 3352 */         i.remove();
/* 3353 */         logger.info("Relic" + s + " removed from relic pool.");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public static void onModifyPower() {
/* 3359 */     if (player != null) {
/* 3360 */       player.hand.applyPowers();
/*      */       
/* 3362 */       if (player.hasPower("Focus")) {
/* 3363 */         for (AbstractOrb o : player.orbs) {
/* 3364 */           o.updateDescription();
/*      */         }
/*      */       }
/*      */     }
/* 3368 */     if (getCurrRoom().monsters != null) {
/* 3369 */       for (AbstractMonster m : getCurrRoom().monsters.monsters) {
/* 3370 */         m.applyPowers();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void checkForPactAchievement() {
/* 3376 */     if (player != null)
/*      */     {
/* 3378 */       if (player.exhaustPile.size() >= 20) {
/* 3379 */         UnlockTracker.unlockAchievement("THE_PACT");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void loadSave(SaveFile saveFile) {
/* 3385 */     floorNum = saveFile.floor_num;
/* 3386 */     actNum = saveFile.act_num;
/* 3387 */     Settings.seed = Long.valueOf(saveFile.seed);
/* 3388 */     loadSeeds(saveFile);
/* 3389 */     monsterList = saveFile.monster_list;
/* 3390 */     eliteMonsterList = saveFile.elite_monster_list;
/* 3391 */     bossList = saveFile.boss_list;
/* 3392 */     setBoss(saveFile.boss);
/* 3393 */     commonRelicPool = saveFile.common_relics;
/* 3394 */     uncommonRelicPool = saveFile.uncommon_relics;
/* 3395 */     rareRelicPool = saveFile.rare_relics;
/* 3396 */     shopRelicPool = saveFile.shop_relics;
/* 3397 */     bossRelicPool = saveFile.boss_relics;
/* 3398 */     pathX = saveFile.path_x;
/* 3399 */     pathY = saveFile.path_y;
/* 3400 */     bossCount = saveFile.spirit_count;
/* 3401 */     eventList = saveFile.event_list;
/* 3402 */     specialOneTimeEventList = saveFile.one_time_event_list;
/* 3403 */     EventHelper.setChances(saveFile.event_chances);
/* 3404 */     AbstractRoom.blizzardPotionMod = saveFile.potion_chance;
/* 3405 */     ShopScreen.purgeCost = saveFile.purgeCost;
/* 3406 */     CardHelper.obtainedCards = saveFile.obtained_cards;
/*      */     
/* 3408 */     if (saveFile.daily_mods != null) {
/* 3409 */       Settings.dailyMods.setMods(saveFile.daily_mods);
/*      */     }
/*      */   }
/*      */   
/*      */   public static AbstractBlight getBlight(String targetID)
/*      */   {
/* 3415 */     for (AbstractBlight b : blightPool) {
/* 3416 */       if (b.blightID.equals(targetID)) {
/* 3417 */         return b;
/*      */       }
/*      */     }
/* 3420 */     return null;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\dungeons\AbstractDungeon.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */