/*     */ package com.megacrit.cardcrawl.dungeons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.map.MapRoomNode;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterInfo;
/*     */ import com.megacrit.cardcrawl.neow.NeowRoom;
/*     */ import com.megacrit.cardcrawl.rooms.EmptyRoom;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType;
/*     */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*     */ import com.megacrit.cardcrawl.scenes.TheBottomScene;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Exordium
/*     */   extends AbstractDungeon
/*     */ {
/*  43 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("Exordium");
/*  44 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  46 */   public static final String NAME = TEXT[0];
/*     */   public static final String ID = "Exordium";
/*     */   
/*     */   public Exordium(AbstractPlayer p, ArrayList<String> emptyList) {
/*  50 */     super(NAME, "Exordium", p, emptyList);
/*     */     
/*  52 */     initializeRelicList();
/*     */     
/*  54 */     if (Settings.isEndless) {
/*  55 */       if (floorNum <= 1) {
/*  56 */         blightPool.clear();
/*  57 */         blightPool = new ArrayList();
/*     */       }
/*     */     } else {
/*  60 */       blightPool.clear();
/*     */     }
/*     */     
/*  63 */     if (scene != null) {
/*  64 */       scene.dispose();
/*     */     }
/*  66 */     scene = new TheBottomScene();
/*  67 */     scene.randomizeScene();
/*  68 */     fadeColor = Color.valueOf("1e0f0aff");
/*     */     
/*     */ 
/*  71 */     initializeSpecialOneTimeEventList();
/*     */     
/*     */ 
/*  74 */     initializeLevelSpecificChances();
/*  75 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + AbstractDungeon.actNum));
/*  76 */     generateMap();
/*     */     
/*  78 */     CardCrawlGame.music.changeBGM(id);
/*  79 */     AbstractDungeon.currMapNode = new MapRoomNode(0, -1);
/*  80 */     if ((Settings.isShowBuild) || (!((Boolean)TipTracker.tips.get("NEOW_SKIP")).booleanValue())) {
/*  81 */       AbstractDungeon.currMapNode.room = new EmptyRoom();
/*     */     } else {
/*  83 */       AbstractDungeon.currMapNode.room = new NeowRoom(false);
/*     */       
/*     */ 
/*  86 */       if (AbstractDungeon.floorNum > 1) {
/*  87 */         SaveHelper.saveIfAppropriate(SaveFile.SaveType.ENDLESS_NEOW);
/*     */       } else {
/*  89 */         SaveHelper.saveIfAppropriate(SaveFile.SaveType.ENTER_ROOM);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public Exordium(AbstractPlayer p, SaveFile saveFile) {
/*  95 */     super(NAME, p, saveFile);
/*  96 */     CardCrawlGame.dungeon = this;
/*  97 */     if (scene != null) {
/*  98 */       scene.dispose();
/*     */     }
/* 100 */     scene = new TheBottomScene();
/* 101 */     fadeColor = Color.valueOf("1e0f0aff");
/*     */     
/*     */ 
/* 104 */     initializeLevelSpecificChances();
/* 105 */     miscRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.floor_num));
/* 106 */     CardCrawlGame.music.changeBGM(id);
/* 107 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.act_num));
/* 108 */     generateMap();
/* 109 */     firstRoomChosen = true;
/* 110 */     populatePathTaken(saveFile);
/*     */     
/*     */ 
/* 113 */     if (isLoadingIntoNeow(saveFile)) {
/* 114 */       AbstractDungeon.firstRoomChosen = false;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeLevelSpecificChances()
/*     */   {
/* 121 */     shopRoomChance = 0.05F;
/* 122 */     restRoomChance = 0.12F;
/* 123 */     treasureRoomChance = 0.0F;
/* 124 */     eventRoomChance = 0.22F;
/* 125 */     eliteRoomChance = 0.08F;
/*     */     
/*     */ 
/* 128 */     smallChestChance = 50;
/* 129 */     mediumChestChance = 33;
/* 130 */     largeChestChance = 17;
/*     */     
/*     */ 
/* 133 */     commonRelicChance = 50;
/* 134 */     uncommonRelicChance = 33;
/* 135 */     rareRelicChance = 17;
/*     */     
/*     */ 
/* 138 */     colorlessRareChance = 0.3F;
/* 139 */     cardUpgradedChance = 0.0F;
/*     */   }
/*     */   
/*     */   protected void generateMonsters()
/*     */   {
/* 144 */     ArrayList<MonsterInfo> monsters = new ArrayList();
/* 145 */     monsters.add(new MonsterInfo("Cultist", 2.0F));
/* 146 */     monsters.add(new MonsterInfo("Jaw Worm", 2.0F));
/* 147 */     monsters.add(new MonsterInfo("2 Louse", 2.0F));
/* 148 */     monsters.add(new MonsterInfo("Small Slimes", 2.0F));
/* 149 */     MonsterInfo.normalizeWeights(monsters);
/* 150 */     populateMonsterList(monsters, 3, false);
/*     */     
/*     */ 
/* 153 */     monsters.clear();
/* 154 */     monsters.add(new MonsterInfo("Blue Slaver", 2.0F));
/* 155 */     monsters.add(new MonsterInfo("Gremlin Gang", 1.0F));
/* 156 */     monsters.add(new MonsterInfo("Looter", 2.0F));
/* 157 */     monsters.add(new MonsterInfo("Large Slime", 2.0F));
/* 158 */     monsters.add(new MonsterInfo("Lots of Slimes", 1.0F));
/* 159 */     monsters.add(new MonsterInfo("Exordium Thugs", 1.5F));
/* 160 */     monsters.add(new MonsterInfo("Exordium Wildlife", 1.5F));
/* 161 */     monsters.add(new MonsterInfo("Red Slaver", 1.0F));
/* 162 */     monsters.add(new MonsterInfo("3 Louse", 2.0F));
/* 163 */     monsters.add(new MonsterInfo("2 Fungi Beasts", 2.0F));
/* 164 */     MonsterInfo.normalizeWeights(monsters);
/* 165 */     populateFirstStrongEnemy(monsters, generateExclusions());
/* 166 */     populateMonsterList(monsters, 12, false);
/*     */     
/*     */ 
/* 169 */     monsters.clear();
/* 170 */     monsters.add(new MonsterInfo("Gremlin Nob", 1.0F));
/* 171 */     monsters.add(new MonsterInfo("Lagavulin", 1.0F));
/* 172 */     monsters.add(new MonsterInfo("3 Sentries", 1.0F));
/* 173 */     MonsterInfo.normalizeWeights(monsters);
/* 174 */     populateMonsterList(monsters, 10, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected ArrayList<String> generateExclusions()
/*     */   {
/* 181 */     ArrayList<String> retVal = new ArrayList();
/* 182 */     switch ((String)monsterList.get(monsterList.size() - 1)) {
/*     */     case "Looter": 
/* 184 */       retVal.add("Exordium Thugs");
/* 185 */       break;
/*     */     case "Jaw Worm": 
/*     */       break;
/*     */     case "Cultist": 
/*     */       break;
/*     */     case "Blue Slaver": 
/* 191 */       retVal.add("Red Slaver");
/* 192 */       retVal.add("Exordium Thugs");
/* 193 */       break;
/*     */     case "2 Louse": 
/* 195 */       retVal.add("3 Louse");
/* 196 */       break;
/*     */     case "Small Slimes": 
/* 198 */       retVal.add("Large Slime");
/* 199 */       retVal.add("Lots of Slimes");
/* 200 */       break;
/*     */     }
/*     */     
/*     */     
/* 204 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void initializeBoss()
/*     */   {
/* 211 */     if (Settings.isDailyRun) {
/* 212 */       bossList.add("The Guardian");
/* 213 */       bossList.add("Hexaghost");
/* 214 */       bossList.add("Slime Boss");
/* 215 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/* 217 */     else if (!UnlockTracker.isBossSeen("GUARDIAN")) {
/* 218 */       bossList.add("The Guardian");
/* 219 */     } else if (!UnlockTracker.isBossSeen("GHOST")) {
/* 220 */       bossList.add("Hexaghost");
/* 221 */     } else if (!UnlockTracker.isBossSeen("SLIME")) {
/* 222 */       bossList.add("Slime Boss");
/*     */     } else {
/* 224 */       bossList.add("The Guardian");
/* 225 */       bossList.add("Hexaghost");
/* 226 */       bossList.add("Slime Boss");
/* 227 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 232 */     if (Settings.isDemo) {
/* 233 */       bossList.clear();
/* 234 */       bossList.add("Hexaghost");
/*     */     }
/*     */   }
/*     */   
/*     */   protected void initializeEventList()
/*     */   {
/* 240 */     eventList.add("Big Fish");
/* 241 */     eventList.add("The Cleric");
/* 242 */     eventList.add("Dead Adventurer");
/* 243 */     eventList.add("Golden Idol");
/* 244 */     eventList.add("Golden Wing");
/* 245 */     eventList.add("World of Goop");
/* 246 */     eventList.add("Liars Game");
/* 247 */     eventList.add("Living Wall");
/* 248 */     eventList.add("Mushrooms");
/* 249 */     eventList.add("Scrap Ooze");
/* 250 */     eventList.add("Shining Light");
/*     */   }
/*     */   
/*     */   protected void initializeShrineList()
/*     */   {
/* 255 */     shrineList.add("Match and Keep!");
/* 256 */     shrineList.add("Golden Shrine");
/* 257 */     shrineList.add("Transmorgrifier");
/* 258 */     shrineList.add("Purifier");
/* 259 */     shrineList.add("Upgrade Shrine");
/* 260 */     shrineList.add("Wheel of Change");
/*     */   }
/*     */   
/*     */   protected void initializeEventImg()
/*     */   {
/* 265 */     if (eventBackgroundImg != null) {
/* 266 */       eventBackgroundImg.dispose();
/* 267 */       eventBackgroundImg = null;
/*     */     }
/* 269 */     eventBackgroundImg = ImageMaster.loadImage("images/ui/event/panel.png");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\dungeons\Exordium.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */