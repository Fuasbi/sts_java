/*     */ package com.megacrit.cardcrawl.dungeons;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.megacrit.cardcrawl.audio.MusicMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterInfo;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile;
/*     */ import com.megacrit.cardcrawl.scenes.AbstractScene;
/*     */ import com.megacrit.cardcrawl.scenes.TheBeyondScene;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TheBeyond
/*     */   extends AbstractDungeon
/*     */ {
/*  36 */   private static final Logger logger = LogManager.getLogger(TheBeyond.class.getName());
/*  37 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("TheBeyond");
/*  38 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*  40 */   public static final String NAME = TEXT[0];
/*     */   public static final String ID = "TheBeyond";
/*     */   
/*     */   public TheBeyond(AbstractPlayer p, ArrayList<String> theList) {
/*  44 */     super(NAME, "TheBeyond", p, theList);
/*     */     
/*  46 */     if (scene != null) {
/*  47 */       scene.dispose();
/*     */     }
/*  49 */     scene = new TheBeyondScene();
/*  50 */     fadeColor = Color.valueOf("140a1eff");
/*     */     
/*  52 */     initializeLevelSpecificChances();
/*  53 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + AbstractDungeon.actNum * 200));
/*  54 */     generateMap();
/*     */     
/*  56 */     CardCrawlGame.music.changeBGM(id);
/*     */   }
/*     */   
/*     */   public TheBeyond(AbstractPlayer p, SaveFile saveFile) {
/*  60 */     super(NAME, p, saveFile);
/*  61 */     CardCrawlGame.dungeon = this;
/*     */     
/*  63 */     if (scene != null) {
/*  64 */       scene.dispose();
/*     */     }
/*  66 */     scene = new TheBeyondScene();
/*  67 */     fadeColor = Color.valueOf("140a1eff");
/*     */     
/*  69 */     initializeLevelSpecificChances();
/*  70 */     miscRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.floor_num));
/*  71 */     CardCrawlGame.music.changeBGM(id);
/*  72 */     mapRng = new com.megacrit.cardcrawl.random.Random(Long.valueOf(Settings.seed.longValue() + saveFile.act_num * 200));
/*  73 */     generateMap();
/*  74 */     firstRoomChosen = true;
/*     */     
/*  76 */     populatePathTaken(saveFile);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeLevelSpecificChances()
/*     */   {
/*  82 */     shopRoomChance = 0.05F;
/*  83 */     restRoomChance = 0.12F;
/*  84 */     treasureRoomChance = 0.0F;
/*  85 */     eventRoomChance = 0.22F;
/*  86 */     eliteRoomChance = 0.08F;
/*     */     
/*     */ 
/*  89 */     smallChestChance = 50;
/*  90 */     mediumChestChance = 33;
/*  91 */     largeChestChance = 17;
/*     */     
/*     */ 
/*  94 */     commonRelicChance = 50;
/*  95 */     uncommonRelicChance = 33;
/*  96 */     rareRelicChance = 17;
/*     */     
/*     */ 
/*  99 */     colorlessRareChance = 0.3F;
/* 100 */     if (AbstractDungeon.ascensionLevel >= 12) {
/* 101 */       cardUpgradedChance = 0.25F;
/*     */     } else {
/* 103 */       cardUpgradedChance = 0.5F;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void generateMonsters()
/*     */   {
/* 110 */     ArrayList<MonsterInfo> monsters = new ArrayList();
/* 111 */     monsters.add(new MonsterInfo("3 Darklings", 2.0F));
/* 112 */     monsters.add(new MonsterInfo("Orb Walker", 2.0F));
/* 113 */     monsters.add(new MonsterInfo("3 Shapes", 2.0F));
/* 114 */     MonsterInfo.normalizeWeights(monsters);
/* 115 */     populateMonsterList(monsters, 2, false);
/*     */     
/*     */ 
/* 118 */     monsters.clear();
/* 119 */     monsters.add(new MonsterInfo("Reptomancer", 1.0F));
/* 120 */     monsters.add(new MonsterInfo("Spire Growth", 1.0F));
/* 121 */     monsters.add(new MonsterInfo("Transient", 1.0F));
/* 122 */     monsters.add(new MonsterInfo("4 Shapes", 1.0F));
/* 123 */     monsters.add(new MonsterInfo("Maw", 1.0F));
/* 124 */     monsters.add(new MonsterInfo("Sphere and 2 Shapes", 1.0F));
/* 125 */     if (Settings.isBeta) {
/* 126 */       monsters.add(new MonsterInfo("Jaw Worm Horde", 1.0F));
/*     */     }
/* 128 */     monsters.add(new MonsterInfo("3 Darklings", 1.0F));
/* 129 */     MonsterInfo.normalizeWeights(monsters);
/* 130 */     populateFirstStrongEnemy(monsters, generateExclusions());
/* 131 */     populateMonsterList(monsters, 12, false);
/*     */     
/*     */ 
/* 134 */     monsters.clear();
/* 135 */     monsters.add(new MonsterInfo("Giant Head", 2.0F));
/* 136 */     monsters.add(new MonsterInfo("Nemesis", 2.0F));
/* 137 */     monsters.add(new MonsterInfo("2 Orb Walkers", 2.0F));
/* 138 */     MonsterInfo.normalizeWeights(monsters);
/*     */     
/*     */ 
/* 141 */     populateMonsterList(monsters, 10, true);
/*     */     
/*     */ 
/*     */ 
/* 145 */     logger.info("Random counter: " + monsterRng.counter);
/*     */   }
/*     */   
/*     */   protected ArrayList<String> generateExclusions()
/*     */   {
/* 150 */     ArrayList<String> retVal = new ArrayList();
/* 151 */     switch ((String)monsterList.get(monsterList.size() - 1)) {
/*     */     case "3 Darklings": 
/* 153 */       retVal.add("3 Darklings");
/* 154 */       break;
/*     */     case "Orb Walker": 
/* 156 */       retVal.add("Orb Walker");
/* 157 */       break;
/*     */     case "3 Shapes": 
/* 159 */       retVal.add("4 Shapes");
/* 160 */       break;
/*     */     }
/*     */     
/*     */     
/* 164 */     return retVal;
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeBoss()
/*     */   {
/* 170 */     if (Settings.isDailyRun) {
/* 171 */       bossList.add("Awakened One");
/* 172 */       bossList.add("Time Eater");
/* 173 */       bossList.add("Donu and Deca");
/* 174 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/* 176 */     else if (!UnlockTracker.isBossSeen("CROW")) {
/* 177 */       bossList.add("Awakened One");
/* 178 */     } else if (!UnlockTracker.isBossSeen("DONUT")) {
/* 179 */       bossList.add("Donu and Deca");
/* 180 */     } else if (!UnlockTracker.isBossSeen("WIZARD")) {
/* 181 */       bossList.add("Time Eater");
/*     */     } else {
/* 183 */       bossList.add("Awakened One");
/* 184 */       bossList.add("Time Eater");
/* 185 */       bossList.add("Donu and Deca");
/* 186 */       Collections.shuffle(bossList, new java.util.Random(monsterRng.randomLong()));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   protected void initializeEventList()
/*     */   {
/* 193 */     eventList.add("Falling");
/* 194 */     eventList.add("MindBloom");
/* 195 */     eventList.add("The Moai Head");
/* 196 */     eventList.add("Mysterious Sphere");
/* 197 */     eventList.add("SensoryStone");
/* 198 */     eventList.add("Tomb of Lord Red Mask");
/* 199 */     eventList.add("Winding Halls");
/*     */   }
/*     */   
/*     */   protected void initializeEventImg()
/*     */   {
/* 204 */     if (eventBackgroundImg != null) {
/* 205 */       eventBackgroundImg.dispose();
/* 206 */       eventBackgroundImg = null;
/*     */     }
/* 208 */     eventBackgroundImg = ImageMaster.loadImage("images/ui/event/panel.png");
/*     */   }
/*     */   
/*     */   protected void initializeShrineList()
/*     */   {
/* 213 */     shrineList.add("Match and Keep!");
/* 214 */     shrineList.add("Wheel of Change");
/* 215 */     shrineList.add("Golden Shrine");
/* 216 */     shrineList.add("Transmorgrifier");
/* 217 */     shrineList.add("Purifier");
/* 218 */     shrineList.add("Upgrade Shrine");
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\dungeons\TheBeyond.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */