/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class RewardGlowEffect extends AbstractGameEffect
/*    */ {
/*    */   private static final int W = 64;
/*    */   private static final float DURATION = 1.1F;
/*    */   private float scale;
/*    */   private float x;
/*    */   private float y;
/*    */   private float angle;
/*    */   
/*    */   public RewardGlowEffect(float x, float y)
/*    */   {
/* 17 */     this.x = x;
/* 18 */     this.y = y;
/* 19 */     this.color = com.badlogic.gdx.graphics.Color.WHITE.cpy();
/* 20 */     this.duration = 1.1F;
/* 21 */     this.scale = Settings.scale;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 27 */     if (this.duration < 0.0F) {
/* 28 */       this.isDone = true;
/*    */     }
/*    */     
/* 31 */     this.scale += Settings.scale * com.badlogic.gdx.Gdx.graphics.getDeltaTime() / 20.0F;
/*    */     
/* 33 */     this.color.a = (com.badlogic.gdx.math.Interpolation.fade.apply(this.duration / 1.1F) / 12.0F);
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 38 */     sb.setColor(this.color);
/* 39 */     sb.setBlendFunction(770, 1);
/* 40 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.REWARD_SCREEN_ITEM, this.x - 232.0F, this.y - 49.0F, 232.0F, 49.0F, 464.0F, 98.0F, this.scale, this.scale + Settings.scale * 0.05F, 0.0F, 0, 0, 464, 98, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 57 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb, com.badlogic.gdx.graphics.Color tint) {
/* 61 */     sb.setColor(this.color);
/* 62 */     sb.setBlendFunction(770, 1);
/* 63 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale * Settings.scale / 2.0F, this.scale * Settings.scale / 2.0F, this.angle, 0, 0, 64, 64, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 80 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\RewardGlowEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */