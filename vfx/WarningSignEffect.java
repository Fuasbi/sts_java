/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class WarningSignEffect extends AbstractGameEffect
/*    */ {
/* 13 */   private static com.badlogic.gdx.graphics.Texture img = null;
/*    */   private static final float DUR = 1.0F;
/*    */   float x;
/*    */   float y;
/*    */   
/* 18 */   public WarningSignEffect(float x, float y) { this.duration = 1.0F;
/* 19 */     this.color = Color.SCARLET.cpy();
/* 20 */     this.color.a = 0.0F;
/* 21 */     this.x = x;
/* 22 */     this.y = y;
/*    */     
/* 24 */     if (img == null) {
/* 25 */       img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/vfx/ui/warning.png");
/*    */     }
/*    */   }
/*    */   
/*    */   public void update() {
/* 30 */     if (1.0F - this.duration < 0.1F) {
/* 31 */       this.color.a = Interpolation.fade.apply(0.0F, 1.0F, (1.0F - this.duration) * 10.0F);
/*    */     } else {
/* 33 */       this.color.a = Interpolation.pow2Out.apply(0.0F, 1.0F, this.duration);
/*    */     }
/*    */     
/* 36 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 37 */     if (this.duration < 0.0F) {
/* 38 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 44 */     sb.setBlendFunction(770, 1);
/* 45 */     sb.setColor(this.color);
/* 46 */     sb.draw(img, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 64, 64, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 63 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\WarningSignEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */