/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ 
/*     */ public class ShopSpeechBubble extends AbstractGameEffect
/*     */ {
/*     */   private static final int RAW_W = 512;
/*  14 */   private float shadow_offset = 0.0F;
/*  15 */   private static final float SHADOW_OFFSET = 16.0F * Settings.scale;
/*     */   private float x;
/*     */   private float y;
/*  18 */   private float scale_x; private float scale_y; private float wavy_y; private float wavyHelper; private static final float WAVY_SPEED = 6.0F * Settings.scale;
/*  19 */   private static final float WAVY_DISTANCE = 2.0F * Settings.scale;
/*     */   private static final float SCALE_TIME = 0.3F;
/*  21 */   private float scaleTimer = 0.3F;
/*  22 */   private static final float ADJUST_X = 170.0F * Settings.scale;
/*  23 */   private static final float ADJUST_Y = 116.0F * Settings.scale;
/*     */   private boolean facingRight;
/*     */   private static final float DEFAULT_DURATION = 2.0F;
/*     */   public static final float FADE_TIME = 0.3F;
/*     */   public Hitbox hb;
/*     */   
/*     */   public ShopSpeechBubble(float x, float y, String msg, boolean isPlayer) {
/*  30 */     this(x, y, 2.0F, msg, isPlayer);
/*     */   }
/*     */   
/*     */   public ShopSpeechBubble(float x, float y, float duration, String msg, boolean isPlayer) {
/*  34 */     if (isPlayer) {
/*  35 */       this.x = (x + ADJUST_X);
/*     */     } else {
/*  37 */       this.x = (x - ADJUST_X);
/*     */     }
/*     */     
/*  40 */     this.y = (y + ADJUST_Y);
/*  41 */     this.scale_x = (Settings.scale * 0.7F);
/*  42 */     this.scale_y = (Settings.scale * 0.7F);
/*  43 */     this.scaleTimer = 0.3F;
/*  44 */     this.color = new Color(0.8F, 0.9F, 0.9F, 0.0F);
/*  45 */     this.duration = duration;
/*  46 */     this.facingRight = (!isPlayer);
/*     */     
/*  48 */     if (!this.facingRight) {
/*  49 */       this.hb = new Hitbox(x, y, 350.0F * Settings.scale, 270.0F * Settings.scale);
/*     */     } else {
/*  51 */       this.hb = new Hitbox(x - 350.0F * Settings.scale, y, 350.0F * Settings.scale, 270.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/*  56 */     updateScale();
/*  57 */     this.hb.update();
/*     */     
/*  59 */     this.wavyHelper += Gdx.graphics.getDeltaTime() * WAVY_SPEED;
/*  60 */     this.wavy_y = (MathUtils.sin(this.wavyHelper) * WAVY_DISTANCE);
/*     */     
/*  62 */     this.duration -= Gdx.graphics.getDeltaTime();
/*  63 */     if (this.duration < 0.0F) {
/*  64 */       this.isDone = true;
/*     */     }
/*     */     
/*  67 */     if (this.duration > 0.3F) {
/*  68 */       this.color.a = MathUtils.lerp(this.color.a, 1.0F, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     } else {
/*  70 */       this.color.a = MathUtils.lerp(this.color.a, 0.0F, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/*  72 */     this.shadow_offset = MathUtils.lerp(this.shadow_offset, SHADOW_OFFSET, Gdx.graphics.getDeltaTime() * 4.0F);
/*     */   }
/*     */   
/*     */   private void updateScale() {
/*  76 */     this.scaleTimer -= Gdx.graphics.getDeltaTime();
/*  77 */     if (this.scaleTimer < 0.0F) {
/*  78 */       this.scaleTimer = 0.0F;
/*     */     }
/*  80 */     this.scale_x = com.badlogic.gdx.math.Interpolation.swingIn.apply(this.scale_x, Settings.scale, this.scaleTimer / 0.3F);
/*  81 */     this.scale_y = com.badlogic.gdx.math.Interpolation.swingIn.apply(this.scale_y, Settings.scale, this.scaleTimer / 0.3F);
/*     */   }
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  87 */     this.hb.render(sb);
/*     */     
/*     */ 
/*  90 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.color.a / 4.0F));
/*  91 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.SHOP_SPEECH_BUBBLE_IMG, this.x - 256.0F + this.shadow_offset, this.y - 256.0F - this.shadow_offset + this.wavy_y, 256.0F, 256.0F, 512.0F, 512.0F, this.scale_x, this.scale_y, this.rotation, 0, 0, 512, 512, this.facingRight, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 109 */     sb.setColor(this.color);
/* 110 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.SHOP_SPEECH_BUBBLE_IMG, this.x - 256.0F, this.y - 256.0F + this.wavy_y, 256.0F, 256.0F, 512.0F, 512.0F, this.scale_x, this.scale_y, this.rotation, 0, 0, 512, 512, this.facingRight, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\ShopSpeechBubble.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */