/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class RainingGoldEffect extends AbstractGameEffect
/*    */ {
/*    */   private int amount;
/*    */   private int min;
/*    */   private int max;
/* 10 */   private float staggerTimer = 0.0F;
/*    */   
/*    */   public RainingGoldEffect(int amount) {
/* 13 */     this.amount = amount;
/*    */     
/* 15 */     if (amount < 100) {
/* 16 */       this.min = 1;
/* 17 */       this.max = 7;
/*    */     } else {
/* 19 */       this.min = 3;
/* 20 */       this.max = 18;
/*    */     }
/*    */   }
/*    */   
/*    */   public void update() {
/* 25 */     this.staggerTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 26 */     if (this.staggerTimer < 0.0F) {
/* 27 */       int goldToSpawn = MathUtils.random(this.min, this.max);
/*    */       
/* 29 */       if (goldToSpawn <= this.amount) {
/* 30 */         this.amount -= goldToSpawn;
/*    */       } else {
/* 32 */         goldToSpawn = this.amount;
/* 33 */         this.isDone = true;
/*    */       }
/*    */       
/* 36 */       for (int i = 0; i < goldToSpawn; i++) {
/* 37 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new TouchPickupGold());
/*    */       }
/*    */       
/* 40 */       this.staggerTimer = MathUtils.random(0.3F);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\RainingGoldEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */