/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ 
/*     */ public class TouchPickupGold extends AbstractGameEffect
/*     */ {
/*     */   private static final float RAW_IMG_WIDTH = 32.0F;
/*  16 */   private static final float IMG_WIDTH = 32.0F * Settings.scale;
/*     */   private TextureAtlas.AtlasRegion img;
/*  18 */   private boolean isPickupable = false;
/*  19 */   public boolean pickedup = false;
/*     */   private float x;
/*     */   private float y;
/*  22 */   private float landingY; private boolean willBounce = false; private boolean hasBounced = true;
/*     */   private float bounceY;
/*  24 */   private float bounceX; private float vY = -0.2F; private float vX = 0.0F;
/*  25 */   private float gravity = -0.3F;
/*     */   private Hitbox hitbox;
/*     */   
/*     */   public TouchPickupGold() {
/*  29 */     if (MathUtils.randomBoolean()) {
/*  30 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.COPPER_COIN_1;
/*     */     } else {
/*  32 */       this.img = com.megacrit.cardcrawl.helpers.ImageMaster.COPPER_COIN_2;
/*     */     }
/*     */     
/*  35 */     this.willBounce = (MathUtils.random(3) != 0);
/*  36 */     if (this.willBounce) {
/*  37 */       this.hasBounced = false;
/*  38 */       this.bounceY = MathUtils.random(1.0F, 4.0F);
/*  39 */       this.bounceX = MathUtils.random(-3.0F, 3.0F);
/*     */     }
/*     */     
/*  42 */     this.y = (Settings.HEIGHT * MathUtils.random(1.1F, 1.3F) - this.img.packedHeight / 2.0F);
/*  43 */     this.x = (MathUtils.random(Settings.WIDTH * 0.3F, Settings.WIDTH * 0.95F) - this.img.packedWidth / 2.0F);
/*  44 */     this.landingY = MathUtils.random(AbstractDungeon.floorY - Settings.HEIGHT * 0.05F, AbstractDungeon.floorY + Settings.HEIGHT * 0.08F);
/*     */     
/*     */ 
/*  47 */     this.rotation = MathUtils.random(360.0F);
/*  48 */     this.scale = Settings.scale;
/*     */   }
/*     */   
/*     */   public void update() {
/*  52 */     if (!this.isPickupable) {
/*  53 */       this.x += this.vX * Gdx.graphics.getDeltaTime() * 60.0F;
/*  54 */       this.y += this.vY * Gdx.graphics.getDeltaTime() * 60.0F;
/*  55 */       this.vY += this.gravity;
/*     */       
/*  57 */       if (this.y < this.landingY) {
/*  58 */         if (this.hasBounced) {
/*  59 */           this.y = this.landingY;
/*  60 */           this.isPickupable = true;
/*  61 */           this.hitbox = new Hitbox(this.x - IMG_WIDTH * 2.0F, this.y - IMG_WIDTH * 2.0F, IMG_WIDTH * 4.0F, IMG_WIDTH * 4.0F);
/*     */         } else {
/*  63 */           if (MathUtils.random(1) == 0) {
/*  64 */             this.hasBounced = true;
/*     */           }
/*  66 */           this.y = this.landingY;
/*  67 */           this.vY = this.bounceY;
/*  68 */           this.vX = this.bounceX;
/*  69 */           this.bounceY *= 0.5F;
/*  70 */           this.bounceX *= 0.3F;
/*     */         }
/*     */       }
/*     */     }
/*  74 */     else if (!this.pickedup) {
/*  75 */       this.pickedup = true;
/*  76 */       this.isDone = true;
/*  77 */       playGainGoldSFX();
/*     */       
/*  79 */       AbstractDungeon.effectsQueue.add(new ShineLinesEffect(this.x, this.y));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void playGainGoldSFX()
/*     */   {
/* 113 */     int roll = MathUtils.random(2);
/* 114 */     switch (roll) {
/*     */     case 0: 
/* 116 */       CardCrawlGame.sound.play("GOLD_GAIN", 0.1F);
/* 117 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     case 1: 
/* 122 */       CardCrawlGame.sound.play("GOLD_GAIN_3", 0.1F);
/* 123 */       break;
/*     */     
/*     */ 
/*     */ 
/*     */     default: 
/* 128 */       CardCrawlGame.sound.play("GOLD_GAIN_5", 0.1F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 134 */     sb.setColor(com.badlogic.gdx.graphics.Color.WHITE);
/*     */     
/* 136 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 148 */     if (this.hitbox != null) {
/* 149 */       this.hitbox.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\TouchPickupGold.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */