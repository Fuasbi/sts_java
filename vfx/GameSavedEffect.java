/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GameSavedEffect extends AbstractGameEffect
/*    */ {
/* 11 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("GameSavedEffect");
/* 12 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/* 14 */   private static final float X = 1400.0F * Settings.scale;
/* 15 */   private static final float DRAW_X = 1540.0F * Settings.scale;
/* 16 */   private static final float Y = Settings.HEIGHT - 26.0F * Settings.scale;
/*    */   
/*    */ 
/*    */   private static final float DUR = 2.0F;
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 24 */     float drawX = Settings.dailyModsEnabled() ? DRAW_X : X;
/* 25 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.topLevelEffects.add(new SpeechTextEffect(drawX, Y, 2.0F, TEXT[0], com.megacrit.cardcrawl.ui.DialogWord.AppearEffect.FADE_IN));
/* 26 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\GameSavedEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */