/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class UpgradeHammerImprintEffect extends AbstractGameEffect
/*    */ {
/* 13 */   private TextureAtlas.AtlasRegion img = com.megacrit.cardcrawl.helpers.ImageMaster.UPGRADE_HAMMER_IMPACT;
/* 14 */   private boolean generatedLines = false;
/*    */   private static final float DUR = 0.7F;
/*    */   private float x;
/*    */   private float y;
/*    */   private float hammerGlowScale;
/*    */   
/* 20 */   public UpgradeHammerImprintEffect(float x, float y) { this.x = (x - this.img.packedWidth / 2);
/* 21 */     this.y = (y - this.img.packedHeight / 2);
/* 22 */     this.color = Color.WHITE.cpy();
/* 23 */     this.color.a = 0.7F;
/* 24 */     this.duration = 0.7F;
/* 25 */     this.scale = (Settings.scale / MathUtils.random(1.0F, 1.5F));
/* 26 */     this.rotation = MathUtils.random(0.0F, 360.0F);
/* 27 */     this.hammerGlowScale = (1.0F - this.duration);
/* 28 */     this.hammerGlowScale *= this.hammerGlowScale;
/*    */   }
/*    */   
/*    */   public void update() {
/* 32 */     if (!this.generatedLines) {}
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 42 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 43 */     if (this.duration < 0.0F) {
/* 44 */       this.isDone = true;
/*    */     }
/* 46 */     this.color.a = this.duration;
/*    */     
/* 48 */     this.hammerGlowScale = (1.7F - this.duration);
/* 49 */     this.hammerGlowScale *= this.hammerGlowScale * this.hammerGlowScale;
/* 50 */     this.scale += Gdx.graphics.getDeltaTime() / 20.0F;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 55 */     sb.setBlendFunction(770, 1);
/* 56 */     sb.setColor(this.color);
/* 57 */     sb.draw(this.img, this.x + 
/*    */     
/* 59 */       MathUtils.random(-2.0F, 2.0F) * Settings.scale, this.y + 
/* 60 */       MathUtils.random(-2.0F, 2.0F) * Settings.scale, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 68 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.color.a / 10.0F));
/* 69 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.hammerGlowScale, this.hammerGlowScale, this.rotation);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 80 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\UpgradeHammerImprintEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */