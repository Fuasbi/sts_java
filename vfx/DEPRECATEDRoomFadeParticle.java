/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class DEPRECATEDRoomFadeParticle extends AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private TextureAtlas.AtlasRegion img;
/*    */   private static final float DUR = 1.0F;
/*    */   
/*    */   public DEPRECATEDRoomFadeParticle(float y)
/*    */   {
/* 19 */     this.y = y;
/* 20 */     this.x = (Settings.WIDTH + this.img.packedWidth * 1.5F);
/* 21 */     y -= this.img.packedHeight / 2;
/* 22 */     this.duration = 1.0F;
/* 23 */     this.startingDuration = 1.0F;
/* 24 */     this.color = com.megacrit.cardcrawl.dungeons.AbstractDungeon.fadeColor.cpy();
/* 25 */     this.color.a = 1.0F;
/* 26 */     this.scale *= 2.0F;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 48 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 49 */     if (this.duration < 0.0F) {
/* 50 */       this.isDone = true;
/*    */     }
/* 52 */     this.x = Interpolation.pow2Out.apply(0.0F - this.img.packedWidth * 1.5F, Settings.WIDTH + this.img.packedWidth * 1.5F, this.duration / 1.0F);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 60 */     sb.setColor(this.color);
/* 61 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\DEPRECATEDRoomFadeParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */