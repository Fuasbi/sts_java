/*     */ package com.megacrit.cardcrawl.vfx.cardManip;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.BounceIn;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class CardFlashVfx extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*     */   private AbstractCard card;
/*     */   private com.badlogic.gdx.graphics.Texture img;
/*  17 */   private float yScale = 0.0F;
/*  18 */   private boolean isSuper = false;
/*     */   
/*     */   public CardFlashVfx(AbstractCard card, boolean isSuper) {
/*  21 */     this(card, new Color(1.0F, 0.8F, 0.2F, 0.0F), isSuper);
/*     */   }
/*     */   
/*     */   public CardFlashVfx(AbstractCard card, Color c, boolean isSuper) {
/*  25 */     this.card = card;
/*  26 */     this.isSuper = isSuper;
/*  27 */     this.duration = 0.5F;
/*     */     
/*  29 */     if (isSuper) {
/*  30 */       this.img = ImageMaster.CARD_FLASH_VFX;
/*     */     } else {
/*  32 */       switch (card.type) {
/*     */       case POWER: 
/*  34 */         this.img = ImageMaster.CARD_POWER_BG_SILHOUETTE;
/*  35 */         break;
/*     */       case ATTACK: 
/*  37 */         this.img = ImageMaster.CARD_ATTACK_BG_SILHOUETTE;
/*  38 */         break;
/*     */       default: 
/*  40 */         this.img = ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*     */       }
/*     */       
/*     */     }
/*  44 */     this.color = c;
/*     */   }
/*     */   
/*     */   public CardFlashVfx(AbstractCard card) {
/*  48 */     this(card, new Color(1.0F, 0.8F, 0.2F, 0.0F), false);
/*     */   }
/*     */   
/*     */   public CardFlashVfx(AbstractCard card, Color c) {
/*  52 */     this.card = card;
/*  53 */     this.duration = 0.5F;
/*     */     
/*  55 */     switch (card.type) {
/*     */     case POWER: 
/*  57 */       this.img = ImageMaster.CARD_POWER_BG_SILHOUETTE;
/*  58 */       break;
/*     */     case ATTACK: 
/*  60 */       this.img = ImageMaster.CARD_ATTACK_BG_SILHOUETTE;
/*  61 */       break;
/*     */     default: 
/*  63 */       this.img = ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*     */     }
/*     */     
/*  66 */     this.color = c;
/*  67 */     this.isSuper = false;
/*     */   }
/*     */   
/*     */   public void update() {
/*  71 */     this.duration -= Gdx.graphics.getDeltaTime();
/*  72 */     if (this.duration < 0.0F) {
/*  73 */       this.isDone = true;
/*     */     } else {
/*  75 */       this.yScale = Interpolation.bounceIn.apply(1.2F, 1.1F, this.duration * 2.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  81 */     sb.setBlendFunction(770, 1);
/*  82 */     this.color.a = this.duration;
/*  83 */     sb.setColor(this.color);
/*  84 */     if (this.isSuper) {
/*  85 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.52F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.53F) * Settings.scale, this.card.angle, 0, 0, 512, 800, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 103 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.55F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.57F) * Settings.scale, this.card.angle, 0, 0, 512, 800, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 121 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 400.0F, 256.0F, 400.0F, 512.0F, 800.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.58F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.6F) * Settings.scale, this.card.angle, 0, 0, 512, 800, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 139 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.52F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.52F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 157 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.55F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.55F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 175 */       sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.58F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.58F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 193 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\CardFlashVfx.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */