/*     */ package com.megacrit.cardcrawl.vfx.cardManip;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ 
/*     */ public class ShowCardAndAddToHandEffect extends AbstractGameEffect
/*     */ {
/*     */   private static final float EFFECT_DUR = 0.8F;
/*     */   private AbstractCard card;
/*  18 */   private static final float PADDING = 25.0F * Settings.scale;
/*     */   
/*     */   public ShowCardAndAddToHandEffect(AbstractCard card, float offsetX, float offsetY) {
/*  21 */     this.card = card;
/*  22 */     UnlockTracker.markCardAsSeen(card.cardID);
/*  23 */     card.current_x = (Settings.WIDTH / 2.0F);
/*  24 */     card.current_y = (Settings.HEIGHT / 2.0F);
/*  25 */     card.target_x = offsetX;
/*  26 */     card.target_y = offsetY;
/*  27 */     this.duration = 0.8F;
/*  28 */     card.drawScale = 0.75F;
/*  29 */     card.targetDrawScale = 0.75F;
/*  30 */     card.transparency = 0.01F;
/*  31 */     card.targetTransparency = 1.0F;
/*  32 */     card.fadingOut = false;
/*  33 */     playCardObtainSfx();
/*     */     
/*  35 */     AbstractDungeon.player.hand.addToHand(card);
/*  36 */     card.triggerWhenCopied();
/*  37 */     AbstractDungeon.player.hand.refreshHandLayout();
/*  38 */     AbstractDungeon.player.hand.applyPowers();
/*  39 */     AbstractDungeon.player.onCardDrawOrDiscard();
/*     */     
/*  41 */     if ((AbstractDungeon.player.hasPower("Corruption")) && (card.type == AbstractCard.CardType.SKILL)) {
/*  42 */       card.setCostForTurn(-9);
/*     */     }
/*     */   }
/*     */   
/*     */   public ShowCardAndAddToHandEffect(AbstractCard card) {
/*  47 */     this.card = card;
/*  48 */     identifySpawnLocation(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F);
/*  49 */     this.duration = 0.8F;
/*  50 */     card.drawScale = 0.75F;
/*  51 */     card.targetDrawScale = 0.75F;
/*  52 */     card.transparency = 0.01F;
/*  53 */     card.targetTransparency = 1.0F;
/*  54 */     card.fadingOut = false;
/*     */     
/*  56 */     AbstractDungeon.player.hand.addToHand(card);
/*  57 */     card.triggerWhenCopied();
/*  58 */     AbstractDungeon.player.hand.refreshHandLayout();
/*  59 */     AbstractDungeon.player.hand.applyPowers();
/*  60 */     AbstractDungeon.player.onCardDrawOrDiscard();
/*     */     
/*  62 */     if ((AbstractDungeon.player.hasPower("Corruption")) && (card.type == AbstractCard.CardType.SKILL)) {
/*  63 */       card.setCostForTurn(-9);
/*     */     }
/*     */   }
/*     */   
/*     */   private void playCardObtainSfx() {
/*  68 */     int effectCount = 0;
/*  69 */     for (AbstractGameEffect e : AbstractDungeon.effectList) {
/*  70 */       if ((e instanceof ShowCardAndAddToHandEffect)) {
/*  71 */         effectCount++;
/*     */       }
/*     */     }
/*  74 */     if (effectCount < 2) {
/*  75 */       com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CARD_OBTAIN");
/*     */     }
/*     */   }
/*     */   
/*     */   private void identifySpawnLocation(float x, float y) {
/*  80 */     int effectCount = 0;
/*     */     
/*  82 */     for (AbstractGameEffect e : AbstractDungeon.effectList) {
/*  83 */       if ((e instanceof ShowCardAndAddToHandEffect)) {
/*  84 */         effectCount++;
/*     */       }
/*     */     }
/*     */     
/*  88 */     this.card.target_y = (Settings.HEIGHT * 0.5F);
/*     */     
/*  90 */     switch (effectCount) {
/*     */     case 0: 
/*  92 */       this.card.target_x = (Settings.WIDTH * 0.5F);
/*  93 */       break;
/*     */     case 1: 
/*  95 */       this.card.target_x = (Settings.WIDTH * 0.5F - PADDING - AbstractCard.IMG_WIDTH);
/*  96 */       break;
/*     */     case 2: 
/*  98 */       this.card.target_x = (Settings.WIDTH * 0.5F + PADDING + AbstractCard.IMG_WIDTH);
/*  99 */       break;
/*     */     case 3: 
/* 101 */       this.card.target_x = (Settings.WIDTH * 0.5F - (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/* 102 */       break;
/*     */     case 4: 
/* 104 */       this.card.target_x = (Settings.WIDTH * 0.5F + (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/* 105 */       break;
/*     */     default: 
/* 107 */       this.card.target_x = MathUtils.random(Settings.WIDTH * 0.1F, Settings.WIDTH * 0.9F);
/* 108 */       this.card.target_y = MathUtils.random(Settings.HEIGHT * 0.2F, Settings.HEIGHT * 0.8F);
/*     */     }
/*     */     
/*     */     
/* 112 */     this.card.current_x = this.card.target_x;
/* 113 */     this.card.current_y = (this.card.target_y - 200.0F * Settings.scale);
/* 114 */     AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.CardPoofEffect(this.card.target_x, this.card.target_y));
/*     */   }
/*     */   
/*     */   public void update() {
/* 118 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 119 */     this.card.update();
/*     */     
/* 121 */     if (this.duration < 0.0F) {
/* 122 */       this.isDone = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 129 */     if (!this.isDone) {
/* 130 */       this.card.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\ShowCardAndAddToHandEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */