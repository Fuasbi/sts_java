/*    */ package com.megacrit.cardcrawl.vfx.cardManip;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*    */ 
/*    */ public class PurgeCardEffect
/*    */   extends AbstractGameEffect
/*    */ {
/*    */   private static final float EFFECT_DUR = 2.5F;
/*    */   private AbstractCard card;
/* 19 */   private static final float PADDING = 30.0F * Settings.scale;
/*    */   
/*    */   public PurgeCardEffect(AbstractCard card) {
/* 22 */     this(card, Settings.WIDTH - 96.0F * Settings.scale, Settings.HEIGHT - 32.0F * Settings.scale);
/*    */   }
/*    */   
/*    */   public PurgeCardEffect(AbstractCard card, float x, float y) {
/* 26 */     this.card = card;
/* 27 */     this.duration = 2.5F;
/* 28 */     this.startingDuration = 2.5F;
/* 29 */     identifySpawnLocation(x, y);
/* 30 */     card.drawScale = 0.01F;
/* 31 */     card.targetDrawScale = 1.0F;
/* 32 */     CardCrawlGame.sound.play("CARD_BURN");
/*    */   }
/*    */   
/*    */   private void identifySpawnLocation(float x, float y) {
/* 36 */     int effectCount = 0;
/* 37 */     for (AbstractGameEffect e : AbstractDungeon.effectList) {
/* 38 */       if ((e instanceof PurgeCardEffect)) {
/* 39 */         effectCount++;
/*    */       }
/*    */     }
/* 42 */     for (AbstractGameEffect e : AbstractDungeon.topLevelEffects) {
/* 43 */       if ((e instanceof PurgeCardEffect)) {
/* 44 */         effectCount++;
/*    */       }
/*    */     }
/*    */     
/* 48 */     this.card.current_x = x;
/* 49 */     this.card.current_y = y;
/* 50 */     this.card.target_y = (Settings.HEIGHT * 0.5F);
/*    */     
/* 52 */     switch (effectCount) {
/*    */     case 0: 
/* 54 */       this.card.target_x = (Settings.WIDTH * 0.5F);
/* 55 */       break;
/*    */     case 1: 
/* 57 */       this.card.target_x = (Settings.WIDTH * 0.5F - PADDING - AbstractCard.IMG_WIDTH);
/* 58 */       break;
/*    */     case 2: 
/* 60 */       this.card.target_x = (Settings.WIDTH * 0.5F + PADDING + AbstractCard.IMG_WIDTH);
/* 61 */       break;
/*    */     case 3: 
/* 63 */       this.card.target_x = (Settings.WIDTH * 0.5F - (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/* 64 */       break;
/*    */     case 4: 
/* 66 */       this.card.target_x = (Settings.WIDTH * 0.5F + (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/* 67 */       break;
/*    */     default: 
/* 69 */       this.card.target_x = MathUtils.random(Settings.WIDTH * 0.1F, Settings.WIDTH * 0.9F);
/* 70 */       this.card.target_y = MathUtils.random(Settings.HEIGHT * 0.2F, Settings.HEIGHT * 0.8F);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 77 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 78 */     if (this.duration < 0.6F) {
/* 79 */       this.card.fadingOut = true;
/*    */     }
/* 81 */     this.card.update();
/*    */     
/* 83 */     if (this.duration < 0.0F) {
/* 84 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 90 */     if (!this.isDone) {
/* 91 */       this.card.render(sb);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\PurgeCardEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */