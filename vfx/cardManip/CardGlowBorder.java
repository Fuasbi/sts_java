/*    */ package com.megacrit.cardcrawl.vfx.cardManip;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*    */ 
/*    */ public class CardGlowBorder extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private AbstractCard card;
/*    */   private com.badlogic.gdx.graphics.Texture img;
/*    */   private float scale;
/*    */   
/*    */   public CardGlowBorder(AbstractCard card)
/*    */   {
/* 21 */     this.card = card;
/*    */     
/* 23 */     switch (card.type) {
/*    */     case POWER: 
/* 25 */       this.img = ImageMaster.CARD_POWER_BG_SILHOUETTE;
/* 26 */       break;
/*    */     case ATTACK: 
/* 28 */       this.img = ImageMaster.CARD_ATTACK_BG_SILHOUETTE;
/* 29 */       break;
/*    */     default: 
/* 31 */       this.img = ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*    */     }
/*    */     
/*    */     
/* 35 */     this.duration = 1.2F;
/* 36 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 37 */       this.color = Color.valueOf("30c8dcff");
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/* 42 */       this.color = Color.GREEN.cpy();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 53 */     this.scale = ((1.0F + com.badlogic.gdx.math.Interpolation.pow2Out.apply(0.03F, 0.11F, 1.0F - this.duration)) * this.card.drawScale * com.megacrit.cardcrawl.core.Settings.scale);
/*    */     
/*    */ 
/* 56 */     this.color.a = (this.duration / 3.5F);
/* 57 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 58 */     if (this.duration < 0.0F) {
/* 59 */       this.isDone = true;
/* 60 */       this.duration = 0.0F;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 66 */     sb.setColor(this.color);
/* 67 */     sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.scale, this.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\CardGlowBorder.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */