/*     */ package com.megacrit.cardcrawl.vfx.cardManip;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.vfx.combat.CardPoofEffect;
/*     */ 
/*     */ public class ShowCardAndAddToDrawPileEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*     */   private static final float EFFECT_DUR = 1.5F;
/*     */   private AbstractCard card;
/*  16 */   private static final float PADDING = 30.0F * Settings.scale;
/*  17 */   private boolean randomSpot = false;
/*  18 */   private boolean cardOffset = false;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ShowCardAndAddToDrawPileEffect(AbstractCard srcCard, float x, float y, boolean randomSpot, boolean cardOffset)
/*     */   {
/*  27 */     this.card = srcCard.makeStatEquivalentCopy();
/*  28 */     this.cardOffset = cardOffset;
/*  29 */     this.duration = 1.5F;
/*  30 */     this.randomSpot = randomSpot;
/*  31 */     identifySpawnLocation(x, y);
/*  32 */     AbstractDungeon.effectsQueue.add(new CardPoofEffect(this.card.target_x, this.card.target_y));
/*  33 */     this.card.drawScale = 0.01F;
/*  34 */     this.card.targetDrawScale = 1.0F;
/*  35 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CARD_OBTAIN");
/*  36 */     if (randomSpot) {
/*  37 */       AbstractDungeon.player.drawPile.addToRandomSpot(srcCard);
/*     */     } else {
/*  39 */       AbstractDungeon.player.drawPile.addToTop(srcCard);
/*     */     }
/*     */   }
/*     */   
/*     */   public ShowCardAndAddToDrawPileEffect(AbstractCard srcCard, float x, float y, boolean randomSpot) {
/*  44 */     this(srcCard, x, y, randomSpot, false);
/*     */   }
/*     */   
/*     */   public ShowCardAndAddToDrawPileEffect(AbstractCard srcCard, boolean randomSpot) {
/*  48 */     this.card = srcCard.makeStatEquivalentCopy();
/*  49 */     this.duration = 1.5F;
/*  50 */     this.randomSpot = randomSpot;
/*  51 */     this.card.target_x = MathUtils.random(Settings.WIDTH * 0.1F, Settings.WIDTH * 0.9F);
/*  52 */     this.card.target_y = MathUtils.random(Settings.HEIGHT * 0.8F, Settings.HEIGHT * 0.2F);
/*  53 */     AbstractDungeon.effectsQueue.add(new CardPoofEffect(this.card.target_x, this.card.target_y));
/*  54 */     this.card.drawScale = 0.01F;
/*  55 */     this.card.targetDrawScale = 1.0F;
/*  56 */     if (randomSpot) {
/*  57 */       AbstractDungeon.player.drawPile.addToRandomSpot(srcCard);
/*     */     } else {
/*  59 */       AbstractDungeon.player.drawPile.addToTop(srcCard);
/*     */     }
/*     */   }
/*     */   
/*     */   private void identifySpawnLocation(float x, float y) {
/*  64 */     int effectCount = 0;
/*  65 */     if (this.cardOffset) {
/*  66 */       effectCount = 1;
/*     */     }
/*  68 */     for (com.megacrit.cardcrawl.vfx.AbstractGameEffect e : AbstractDungeon.effectList) {
/*  69 */       if ((e instanceof ShowCardAndAddToDrawPileEffect)) {
/*  70 */         effectCount++;
/*     */       }
/*     */     }
/*     */     
/*  74 */     this.card.current_x = x;
/*  75 */     this.card.current_y = y;
/*  76 */     this.card.target_y = (Settings.HEIGHT * 0.5F);
/*     */     
/*  78 */     switch (effectCount) {
/*     */     case 0: 
/*  80 */       this.card.target_x = (Settings.WIDTH * 0.5F);
/*  81 */       break;
/*     */     case 1: 
/*  83 */       this.card.target_x = (Settings.WIDTH * 0.5F - PADDING - AbstractCard.IMG_WIDTH);
/*  84 */       break;
/*     */     case 2: 
/*  86 */       this.card.target_x = (Settings.WIDTH * 0.5F + PADDING + AbstractCard.IMG_WIDTH);
/*  87 */       break;
/*     */     case 3: 
/*  89 */       this.card.target_x = (Settings.WIDTH * 0.5F - (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/*  90 */       break;
/*     */     case 4: 
/*  92 */       this.card.target_x = (Settings.WIDTH * 0.5F + (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/*  93 */       break;
/*     */     default: 
/*  95 */       this.card.target_x = MathUtils.random(Settings.WIDTH * 0.1F, Settings.WIDTH * 0.9F);
/*  96 */       this.card.target_y = MathUtils.random(Settings.HEIGHT * 0.2F, Settings.HEIGHT * 0.8F);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 103 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 104 */     this.card.update();
/*     */     
/* 106 */     if (this.duration < 0.0F) {
/* 107 */       this.isDone = true;
/* 108 */       this.card.shrink();
/* 109 */       AbstractDungeon.getCurrRoom().souls.onToDeck(this.card, this.randomSpot, true);
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 115 */     if (!this.isDone) {
/* 116 */       this.card.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\ShowCardAndAddToDrawPileEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */