/*     */ package com.megacrit.cardcrawl.vfx.cardManip;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.Omamori;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ 
/*     */ public class ShowCardAndObtainEffect extends AbstractGameEffect
/*     */ {
/*     */   private static final float EFFECT_DUR = 2.0F;
/*     */   private static final float FAST_DUR = 0.5F;
/*     */   private AbstractCard card;
/*  19 */   private static final float PADDING = 30.0F * Settings.scale;
/*     */   private boolean converge;
/*     */   
/*     */   public ShowCardAndObtainEffect(AbstractCard card, float x, float y, boolean convergeCards) {
/*  23 */     if ((card.color == com.megacrit.cardcrawl.cards.AbstractCard.CardColor.CURSE) && (AbstractDungeon.player.hasRelic("Omamori")) && 
/*  24 */       (AbstractDungeon.player.getRelic("Omamori").counter != 0)) {
/*  25 */       ((Omamori)AbstractDungeon.player.getRelic("Omamori")).use();
/*  26 */       this.duration = 0.0F;
/*  27 */       this.isDone = true;
/*  28 */       this.converge = convergeCards;
/*     */     }
/*     */     
/*  31 */     CardHelper.obtain(card.cardID, card.rarity, card.color);
/*     */     
/*  33 */     this.card = card;
/*  34 */     if (Settings.FAST_MODE) {
/*  35 */       this.duration = 0.5F;
/*     */     } else {
/*  37 */       this.duration = 2.0F;
/*     */     }
/*  39 */     identifySpawnLocation(x, y);
/*  40 */     AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.CardPoofEffect(card.target_x, card.target_y));
/*  41 */     card.drawScale = 0.01F;
/*  42 */     card.targetDrawScale = 1.0F;
/*     */   }
/*     */   
/*     */   public ShowCardAndObtainEffect(AbstractCard card, float x, float y) {
/*  46 */     this(card, x, y, true);
/*     */   }
/*     */   
/*     */   private void identifySpawnLocation(float x, float y) {
/*  50 */     int effectCount = 0;
/*  51 */     for (AbstractGameEffect e : AbstractDungeon.effectList) {
/*  52 */       if ((e instanceof ShowCardAndObtainEffect)) {
/*  53 */         effectCount++;
/*     */       }
/*     */     }
/*     */     
/*  57 */     this.card.current_x = x;
/*  58 */     this.card.current_y = y;
/*     */     
/*  60 */     if (this.converge) {
/*  61 */       this.card.target_y = (Settings.HEIGHT * 0.5F);
/*  62 */       switch (effectCount) {
/*     */       case 0: 
/*  64 */         this.card.target_x = (Settings.WIDTH * 0.5F);
/*  65 */         break;
/*     */       case 1: 
/*  67 */         this.card.target_x = (Settings.WIDTH * 0.5F - PADDING - AbstractCard.IMG_WIDTH);
/*  68 */         break;
/*     */       case 2: 
/*  70 */         this.card.target_x = (Settings.WIDTH * 0.5F + PADDING + AbstractCard.IMG_WIDTH);
/*  71 */         break;
/*     */       case 3: 
/*  73 */         this.card.target_x = (Settings.WIDTH * 0.5F - (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/*  74 */         break;
/*     */       case 4: 
/*  76 */         this.card.target_x = (Settings.WIDTH * 0.5F + (PADDING + AbstractCard.IMG_WIDTH) * 2.0F);
/*  77 */         break;
/*     */       default: 
/*  79 */         this.card.target_x = MathUtils.random(Settings.WIDTH * 0.1F, Settings.WIDTH * 0.9F);
/*  80 */         this.card.target_y = MathUtils.random(Settings.HEIGHT * 0.2F, Settings.HEIGHT * 0.8F);
/*  81 */         break;
/*     */       }
/*     */     } else {
/*  84 */       this.card.target_x = this.card.current_x;
/*  85 */       this.card.target_y = this.card.current_y;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  91 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  92 */     this.card.update();
/*     */     
/*  94 */     if (this.duration < 0.0F)
/*     */     {
/*     */ 
/*  97 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  98 */         r.onObtainCard(this.card);
/*     */       }
/*     */       
/* 101 */       this.isDone = true;
/* 102 */       this.card.shrink();
/* 103 */       AbstractDungeon.getCurrRoom().souls.obtain(this.card, true);
/* 104 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 105 */         r.onMasterDeckChange();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 112 */     if (!this.isDone) {
/* 113 */       this.card.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\ShowCardAndObtainEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */