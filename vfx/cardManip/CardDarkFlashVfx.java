/*    */ package com.megacrit.cardcrawl.vfx.cardManip;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class CardDarkFlashVfx extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private AbstractCard card;
/*    */   private com.badlogic.gdx.graphics.Texture img;
/* 16 */   private float yScale = 0.0F;
/*    */   
/*    */   public CardDarkFlashVfx(AbstractCard card) {
/* 19 */     this(card, new Color(1.0F, 0.8F, 0.2F, 0.0F));
/*    */   }
/*    */   
/*    */   public CardDarkFlashVfx(AbstractCard card, Color c) {
/* 23 */     this.card = card;
/* 24 */     this.duration = 0.5F;
/*    */     
/* 26 */     switch (card.type) {
/*    */     case POWER: 
/* 28 */       this.img = ImageMaster.CARD_POWER_BG_SILHOUETTE;
/* 29 */       break;
/*    */     case ATTACK: 
/* 31 */       this.img = ImageMaster.CARD_ATTACK_BG_SILHOUETTE;
/* 32 */       break;
/*    */     default: 
/* 34 */       this.img = ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*    */     }
/*    */     
/* 37 */     this.color = c;
/*    */   }
/*    */   
/*    */   public void update() {
/* 41 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 42 */     if (this.duration < 0.0F) {
/* 43 */       this.isDone = true;
/*    */     } else {
/* 45 */       this.yScale = Interpolation.bounceIn.apply(1.2F, 1.1F, this.duration * 2.0F);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 51 */     this.color.a = this.duration;
/* 52 */     sb.setColor(this.color);
/* 53 */     sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.52F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.52F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 71 */     sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.55F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.55F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 89 */     sb.draw(this.img, this.card.current_x - 256.0F, this.card.current_y - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.card.drawScale * ((this.yScale + 1.0F) * 0.58F) * Settings.scale, this.card.drawScale * ((this.yScale + 1.0F) * 0.58F) * Settings.scale, this.card.angle, 0, 0, 512, 512, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\cardManip\CardDarkFlashVfx.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */