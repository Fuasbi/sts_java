/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ 
/*    */ public class TrailRelicParticle extends AbstractGameEffect
/*    */ {
/*    */   private static final float DURATION = 1.0F;
/* 12 */   private float scale = 0.01F;
/*    */   private static final int IMG_W = 128;
/*    */   private Texture img;
/*    */   private float x;
/*    */   private float y;
/*    */   
/* 18 */   public TrailRelicParticle(Texture img, float x, float y) { this.duration = 1.0F;
/* 19 */     this.img = img;
/* 20 */     this.x = x;
/* 21 */     this.y = y;
/* 22 */     this.renderBehind = true;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 27 */     this.scale = Interpolation.linear.apply(0.0F, com.megacrit.cardcrawl.core.Settings.scale, this.duration);
/* 28 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 29 */     if (this.duration < 0.0F) {
/* 30 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 36 */     sb.setColor(new com.badlogic.gdx.graphics.Color(0.6F, 1.0F, 1.0F, this.duration / 2.0F));
/*    */     
/* 38 */     sb.draw(this.img, this.x - 64.0F, this.y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, 0.0F, 0, 0, 128, 128, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\TrailRelicParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */