/*    */ package com.megacrit.cardcrawl.vfx.scene;
/*    */ 
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class TorchParticleXLEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float vY;
/* 16 */   public static TextureAtlas.AtlasRegion[] imgs = new TextureAtlas.AtlasRegion[3];
/*    */   private TextureAtlas.AtlasRegion img;
/* 18 */   public static boolean renderGreen = false;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public TorchParticleXLEffect(float x, float y)
/*    */   {
/* 27 */     if (imgs[0] == null)
/*    */     {
/* 29 */       imgs[0] = ImageMaster.vfxAtlas.findRegion("env/fire1");
/* 30 */       imgs[1] = ImageMaster.vfxAtlas.findRegion("env/fire2");
/* 31 */       imgs[2] = ImageMaster.vfxAtlas.findRegion("env/fire3");
/*    */     }
/*    */     
/* 34 */     this.duration = MathUtils.random(0.5F, 1.0F);
/* 35 */     this.startingDuration = this.duration;
/* 36 */     this.img = imgs[MathUtils.random(imgs.length - 1)];
/* 37 */     this.x = (x - this.img.packedWidth / 2 + MathUtils.random(-3.0F, 3.0F) * Settings.scale);
/* 38 */     this.y = (y - this.img.packedHeight / 2);
/* 39 */     this.scale = (Settings.scale * MathUtils.random(1.0F, 3.0F));
/* 40 */     this.vY = (MathUtils.random(1.0F, 10.0F) * Settings.scale);
/* 41 */     this.vY *= this.vY;
/* 42 */     this.rotation = MathUtils.random(-20.0F, 20.0F);
/*    */     
/* 44 */     if (!renderGreen)
/*    */     {
/*    */ 
/*    */ 
/* 48 */       this.color = new com.badlogic.gdx.graphics.Color(MathUtils.random(0.6F, 1.0F), MathUtils.random(0.3F, 0.6F), MathUtils.random(0.0F, 0.3F), 0.01F);
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/*    */ 
/* 54 */       this.color = new com.badlogic.gdx.graphics.Color(MathUtils.random(0.1F, 0.3F), MathUtils.random(0.5F, 0.9F), MathUtils.random(0.1F, 0.3F), 0.01F);
/*    */     }
/*    */     
/* 57 */     this.renderBehind = false;
/*    */   }
/*    */   
/*    */   public void update() {
/* 61 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 62 */     if (this.duration < 0.0F) {
/* 63 */       this.isDone = true;
/*    */     }
/* 65 */     this.color.a = com.badlogic.gdx.math.Interpolation.fade.apply(0.0F, 1.0F, this.duration / this.startingDuration);
/* 66 */     this.y += this.vY * com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 2.0F;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 71 */     sb.setBlendFunction(770, 1);
/* 72 */     sb.setColor(this.color);
/* 73 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 84 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\scene\TorchParticleXLEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */