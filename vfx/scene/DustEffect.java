/*    */ package com.megacrit.cardcrawl.vfx.scene;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class DustEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float vX;
/*    */   private float vY;
/*    */   private float aV;
/*    */   private float baseAlpha;
/*    */   private TextureAtlas.AtlasRegion img;
/*    */   
/*    */   public DustEffect()
/*    */   {
/* 20 */     this.startingDuration = MathUtils.random(5.0F, 14.0F);
/* 21 */     this.duration = this.startingDuration;
/*    */     
/*    */ 
/* 24 */     this.img = getImg();
/* 25 */     this.scale = (Settings.scale * MathUtils.random(0.1F, 0.8F));
/*    */     
/*    */ 
/* 28 */     this.x = MathUtils.random(0, Settings.WIDTH);
/* 29 */     this.y = (MathUtils.random(-100.0F, 400.0F) * Settings.scale + com.megacrit.cardcrawl.dungeons.AbstractDungeon.floorY);
/* 30 */     this.vX = (MathUtils.random(-12.0F, 12.0F) * Settings.scale);
/* 31 */     this.vY = (MathUtils.random(-12.0F, 30.0F) * Settings.scale);
/*    */     
/*    */ 
/* 34 */     float colorTmp = MathUtils.random(0.1F, 0.7F);
/* 35 */     this.color = new com.badlogic.gdx.graphics.Color(colorTmp, colorTmp, colorTmp, 0.0F);
/* 36 */     this.baseAlpha = (1.0F - colorTmp);
/* 37 */     this.color.a = 0.0F;
/*    */     
/* 39 */     this.rotation = MathUtils.random(0.0F, 360.0F);
/* 40 */     this.aV = MathUtils.random(-120.0F, 120.0F);
/*    */   }
/*    */   
/*    */   private TextureAtlas.AtlasRegion getImg() {
/* 44 */     switch (MathUtils.random(0, 5)) {
/*    */     case 0: 
/* 46 */       return ImageMaster.DUST_1;
/*    */     case 1: 
/* 48 */       return ImageMaster.DUST_2;
/*    */     case 2: 
/* 50 */       return ImageMaster.DUST_3;
/*    */     case 3: 
/* 52 */       return ImageMaster.DUST_4;
/*    */     case 4: 
/* 54 */       return ImageMaster.DUST_5;
/*    */     }
/* 56 */     return ImageMaster.DUST_6;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 61 */     this.rotation += this.aV * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 62 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 63 */     this.x += this.vX * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 64 */     this.y += this.vY * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     
/* 66 */     if (this.duration < 0.0F) {
/* 67 */       this.isDone = true;
/*    */     }
/*    */     
/* 70 */     if (this.duration > this.startingDuration / 2.0F) {
/* 71 */       float tmp = this.duration - this.startingDuration / 2.0F;
/* 72 */       this.color.a = (com.badlogic.gdx.math.Interpolation.fade.apply(0.0F, 1.0F, this.startingDuration / 2.0F - tmp) * this.baseAlpha);
/*    */     } else {
/* 74 */       this.color.a = (com.badlogic.gdx.math.Interpolation.fade.apply(0.0F, 1.0F, this.duration / (this.startingDuration / 2.0F)) * this.baseAlpha);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb)
/*    */   {
/* 80 */     sb.setColor(this.color);
/* 81 */     sb.draw(this.img, this.x, this.y, this.img.offsetX, this.img.offsetY, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\scene\DustEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */