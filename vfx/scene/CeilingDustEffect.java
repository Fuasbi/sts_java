/*    */ package com.megacrit.cardcrawl.vfx.scene;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CeilingDustEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final int DUST_COUNT = 20;
/*    */   private int count;
/*    */   private float x;
/*    */   
/*    */   public CeilingDustEffect()
/*    */   {
/* 16 */     this.count = 20;
/* 17 */     setPosition();
/*    */   }
/*    */   
/*    */   private void setPosition() {
/* 21 */     this.x = (MathUtils.random(0.0F, 1870.0F) * Settings.scale);
/*    */   }
/*    */   
/*    */   public void update() {
/* 25 */     if (this.count != 0) {
/* 26 */       int num = MathUtils.random(0, 8);
/* 27 */       this.count -= num;
/*    */       
/* 29 */       for (int i = 0; i < num; i++) {
/* 30 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.FallingDustEffect(this.x, 980.0F * Settings.scale));
/* 31 */         if (MathUtils.randomBoolean(0.8F)) {
/* 32 */           com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new CeilingDustCloudEffect(this.x, 980.0F * Settings.scale));
/*    */         }
/*    */       }
/*    */       
/* 36 */       if (this.count <= 0) {
/* 37 */         this.isDone = true;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\scene\CeilingDustEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */