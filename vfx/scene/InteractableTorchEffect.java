/*     */ package com.megacrit.cardcrawl.vfx.scene;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class InteractableTorchEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*     */   private float x;
/*     */   private float y;
/*     */   private Hitbox hb;
/*  18 */   private boolean activated = true;
/*  19 */   private float particleTimer1 = 0.0F;
/*     */   private static final float PARTICLE_EMIT_INTERVAL = 0.1F;
/*     */   private static TextureAtlas.AtlasRegion img;
/*  22 */   private TorchSize size = TorchSize.M;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public InteractableTorchEffect(float x, float y, TorchSize size)
/*     */   {
/*  31 */     if (img == null) {
/*  32 */       img = ImageMaster.vfxAtlas.findRegion("env/torch");
/*     */     }
/*  34 */     this.size = size;
/*  35 */     this.x = x;
/*  36 */     this.y = y;
/*  37 */     this.hb = new Hitbox(50.0F * Settings.scale, 60.0F * Settings.scale);
/*  38 */     this.hb.move(x, y);
/*  39 */     this.color = new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, 0.4F);
/*     */     
/*  41 */     switch (size) {
/*     */     case S: 
/*  43 */       this.scale = (Settings.scale * 0.6F);
/*  44 */       break;
/*     */     case M: 
/*  46 */       this.scale = Settings.scale;
/*  47 */       break;
/*     */     case L: 
/*  49 */       this.scale = (Settings.scale * 1.4F);
/*  50 */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public InteractableTorchEffect(float x, float y)
/*     */   {
/*  63 */     this(x, y, TorchSize.M);
/*     */   }
/*     */   
/*     */   public static enum TorchSize {
/*  67 */     S,  M,  L;
/*     */     
/*     */     private TorchSize() {} }
/*     */   
/*  71 */   public void update() { this.hb.update();
/*  72 */     if ((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) {
/*  73 */       this.activated = (!this.activated);
/*  74 */       if (this.activated) {
/*  75 */         CardCrawlGame.sound.playA("ATTACK_FIRE", 0.4F);
/*     */       } else {
/*  77 */         CardCrawlGame.sound.playA("ATTACK_FIRE", -0.4F);
/*     */       }
/*     */     }
/*     */     
/*  81 */     if (this.activated) {
/*  82 */       this.particleTimer1 -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*  83 */       if (this.particleTimer1 < 0.0F) {
/*  84 */         this.particleTimer1 = 0.1F;
/*  85 */         switch (this.size) {
/*     */         case S: 
/*  87 */           AbstractDungeon.effectsQueue.add(new TorchParticleSEffect(this.x, this.y - 10.0F * Settings.scale));
/*  88 */           AbstractDungeon.effectsQueue.add(new LightFlareSEffect(this.x, this.y - 10.0F * Settings.scale));
/*  89 */           break;
/*     */         case M: 
/*  91 */           AbstractDungeon.effectsQueue.add(new TorchParticleMEffect(this.x, this.y));
/*  92 */           AbstractDungeon.effectsQueue.add(new LightFlareMEffect(this.x, this.y));
/*  93 */           break;
/*     */         case L: 
/*  95 */           AbstractDungeon.effectsQueue.add(new TorchParticleLEffect(this.x, this.y + 14.0F * Settings.scale));
/*  96 */           AbstractDungeon.effectsQueue.add(new LightFlareLEffect(this.x, this.y + 14.0F * Settings.scale));
/*  97 */           break;
/*     */         }
/*     */         
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 108 */     sb.setColor(this.color);
/* 109 */     sb.draw(img, this.x - img.packedWidth / 2, this.y - img.packedHeight / 2 - 24.0F * Settings.scale, img.packedWidth / 2.0F, img.packedHeight / 2.0F, img.packedWidth, img.packedHeight, this.scale, this.scale, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 120 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\scene\InteractableTorchEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */