/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class ClangClangClangEffect extends AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public ClangClangClangEffect(float x, float y)
/*    */   {
/* 12 */     this.x = x;
/* 13 */     this.y = y;
/*    */   }
/*    */   
/*    */   public void update() {
/* 17 */     for (int i = 0; i < 30; i++) {
/* 18 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new UpgradeShineParticleEffect(this.x + 
/*    */       
/* 20 */         MathUtils.random(-10.0F, 10.0F) * com.megacrit.cardcrawl.core.Settings.scale, this.y + 
/* 21 */         MathUtils.random(-10.0F, 10.0F) * com.megacrit.cardcrawl.core.Settings.scale));
/*    */     }
/* 23 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\ClangClangClangEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */