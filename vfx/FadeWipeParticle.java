/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class FadeWipeParticle extends AbstractGameEffect
/*    */ {
/*    */   private static final float DUR = 1.0F;
/*    */   private float y;
/*    */   private float lerpTimer;
/*    */   private float delayTimer;
/*    */   private TextureAtlas.AtlasRegion img;
/*    */   private com.badlogic.gdx.graphics.Texture flatImg;
/*    */   
/*    */   public FadeWipeParticle()
/*    */   {
/* 20 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.SCENE_TRANSITION_FADER;
/* 21 */     this.flatImg = com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG;
/* 22 */     this.color = com.megacrit.cardcrawl.dungeons.AbstractDungeon.fadeColor.cpy();
/* 23 */     this.color.a = 0.0F;
/* 24 */     this.duration = 1.0F;
/* 25 */     this.startingDuration = 1.0F;
/* 26 */     this.y = (Settings.HEIGHT + this.img.packedHeight);
/* 27 */     this.delayTimer = 0.1F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 31 */     if (this.delayTimer > 0.0F) {
/* 32 */       this.delayTimer -= Gdx.graphics.getDeltaTime();
/* 33 */       return;
/*    */     }
/*    */     
/* 36 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 37 */     if (this.duration < 0.0F) {
/* 38 */       this.duration = 0.0F;
/* 39 */       this.lerpTimer += Gdx.graphics.getDeltaTime();
/* 40 */       if (this.lerpTimer > 0.5F) {
/* 41 */         this.color.a = com.megacrit.cardcrawl.helpers.MathHelper.slowColorLerpSnap(this.color.a, 0.0F);
/* 42 */         if (this.color.a == 0.0F) {
/* 43 */           this.isDone = true;
/*    */         }
/*    */       }
/*    */     } else {
/* 47 */       this.color.a = com.badlogic.gdx.math.Interpolation.pow5In.apply(1.0F, 0.0F, this.duration / 1.0F);
/* 48 */       this.y = com.badlogic.gdx.math.Interpolation.pow3In.apply(0.0F - this.img.packedHeight, Settings.HEIGHT + this.img.packedHeight, this.duration / 1.0F);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 54 */     sb.setColor(this.color);
/* 55 */     sb.draw(this.img, 0.0F, this.y, Settings.WIDTH, this.img.packedHeight);
/* 56 */     sb.draw(this.flatImg, 0.0F, this.y + this.img.packedHeight - 1.0F * Settings.scale, Settings.WIDTH, Settings.HEIGHT);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\FadeWipeParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */