/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CollectorCurseEffect extends AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private int count;
/* 15 */   private float stakeTimer = 0.0F;
/*    */   
/*    */   public CollectorCurseEffect(float x, float y) {
/* 18 */     this.x = x;
/* 19 */     this.y = y;
/* 20 */     this.count = 13;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     this.stakeTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 26 */     if (this.stakeTimer < 0.0F) {
/* 27 */       if (this.count == 13) {
/* 28 */         com.megacrit.cardcrawl.core.CardCrawlGame.sound.playA("ATTACK_HEAVY", -0.5F);
/*    */         
/*    */ 
/* 31 */         AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.combat.RoomTintEffect(Color.BLACK.cpy(), 0.8F));
/* 32 */         AbstractDungeon.effectsQueue.add(new BorderLongFlashEffect(new Color(1.0F, 0.0F, 1.0F, 0.7F)));
/*    */       }
/*    */       
/*    */ 
/* 36 */       AbstractDungeon.effectsQueue.add(new CollectorStakeEffect(this.x + 
/*    */       
/* 38 */         MathUtils.random(-50.0F, 50.0F) * Settings.scale, this.y + 
/* 39 */         MathUtils.random(-60.0F, 60.0F) * Settings.scale));
/* 40 */       this.stakeTimer = 0.04F;
/* 41 */       this.count -= 1;
/* 42 */       if (this.count == 0) {
/* 43 */         this.isDone = true;
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\CollectorCurseEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */