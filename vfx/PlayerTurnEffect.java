/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.vfx.combat.BattleStartEffect;
/*     */ 
/*     */ public class PlayerTurnEffect extends AbstractGameEffect
/*     */ {
/*     */   private static final float DUR = 2.0F;
/*  21 */   private static final float HEIGHT_DIV_2 = Settings.HEIGHT / 2.0F;
/*  22 */   private static final float WIDTH_DIV_2 = Settings.WIDTH / 2.0F;
/*     */   
/*     */   private Color bgColor;
/*     */   
/*  26 */   private static final float TARGET_HEIGHT = 150.0F * Settings.scale;
/*     */   private static final float BG_RECT_EXPAND_SPEED = 3.0F;
/*  28 */   private float currentHeight = 0.0F;
/*     */   
/*     */   private String turnMessage;
/*     */   
/*  32 */   private static final float MAIN_MSG_OFFSET_Y = 20.0F * Settings.scale;
/*  33 */   private static final float TURN_MSG_OFFSET_Y = -30.0F * Settings.scale;
/*  34 */   private Color turnMessageColor = new Color(0.7F, 0.7F, 0.7F, 0.0F);
/*     */   
/*     */   public PlayerTurnEffect() {
/*  37 */     this.duration = 2.0F;
/*  38 */     this.startingDuration = 2.0F;
/*     */     
/*  40 */     this.bgColor = new Color(AbstractDungeon.fadeColor.r / 2.0F, AbstractDungeon.fadeColor.g / 2.0F, AbstractDungeon.fadeColor.b / 2.0F, 0.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  46 */     this.color = Settings.GOLD_COLOR.cpy();
/*  47 */     this.color.a = 0.0F;
/*  48 */     if (Settings.usesOrdinal) {
/*  49 */       this.turnMessage = (Integer.toString(GameActionManager.turn) + getOrdinalNaming(GameActionManager.turn) + BattleStartEffect.TURN_TXT);
/*     */     }
/*     */     else {
/*  52 */       this.turnMessage = (Integer.toString(GameActionManager.turn) + BattleStartEffect.TURN_TXT);
/*     */     }
/*  54 */     AbstractDungeon.player.energy.recharge();
/*  55 */     for (com.megacrit.cardcrawl.relics.AbstractRelic r : AbstractDungeon.player.relics) {
/*  56 */       r.onEnergyRecharge();
/*     */     }
/*  58 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/*  59 */       p.onEnergyRecharge();
/*     */     }
/*  61 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("TURN_EFFECT");
/*  62 */     AbstractDungeon.getMonsters().showIntent();
/*  63 */     this.scale = 1.0F;
/*     */   }
/*     */   
/*     */   public static String getOrdinalNaming(int i) {
/*  67 */     return (i % 100 == 11) || (i % 100 == 12) || (i % 100 == 13) ? "th" : new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" }[(i % 10)];
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/*  73 */     if (this.duration > 1.5F) {
/*  74 */       this.currentHeight = MathUtils.lerp(this.currentHeight, TARGET_HEIGHT, Gdx.graphics
/*     */       
/*     */ 
/*  77 */         .getDeltaTime() * 3.0F);
/*  78 */     } else if (this.duration < 0.5F) {
/*  79 */       this.currentHeight = MathUtils.lerp(this.currentHeight, 0.0F, Gdx.graphics.getDeltaTime() * 3.0F);
/*     */     }
/*     */     
/*     */ 
/*  83 */     if (this.duration > 1.5F) {
/*  84 */       this.scale = Interpolation.exp10In.apply(1.0F, 3.0F, (this.duration - 1.5F) * 2.0F);
/*  85 */       this.color.a = Interpolation.exp10In.apply(1.0F, 0.0F, (this.duration - 1.5F) * 2.0F);
/*  86 */     } else if (this.duration < 0.5F) {
/*  87 */       this.scale = Interpolation.pow3In.apply(0.9F, 1.0F, this.duration * 2.0F);
/*  88 */       this.color.a = Interpolation.pow3In.apply(0.0F, 1.0F, this.duration * 2.0F);
/*     */     }
/*  90 */     this.bgColor.a = (this.color.a * 0.75F);
/*  91 */     this.turnMessageColor.a = this.color.a;
/*     */     
/*     */ 
/*  94 */     if (Settings.FAST_MODE) {
/*  95 */       this.duration -= Gdx.graphics.getDeltaTime();
/*     */     }
/*  97 */     this.duration -= Gdx.graphics.getDeltaTime();
/*  98 */     if (this.duration < 0.0F) {
/*  99 */       this.isDone = true;
/* 100 */       for (AbstractPower p : AbstractDungeon.player.powers) {
/* 101 */         p.atEnergyGain();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 108 */     if (!this.isDone) {
/* 109 */       sb.setColor(this.bgColor);
/* 110 */       sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG, 0.0F, HEIGHT_DIV_2 - this.currentHeight / 2.0F, Settings.WIDTH, this.currentHeight);
/*     */       
/* 112 */       sb.setBlendFunction(770, 1);
/* 113 */       FontHelper.renderFontCentered(sb, FontHelper.bannerNameFont, BattleStartEffect.PLAYER_TURN_MSG, WIDTH_DIV_2, HEIGHT_DIV_2 + MAIN_MSG_OFFSET_Y, this.color, this.scale);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 122 */       FontHelper.renderFontCentered(sb, FontHelper.turnNumFont, this.turnMessage, WIDTH_DIV_2, HEIGHT_DIV_2 + TURN_MSG_OFFSET_Y, this.turnMessageColor, this.scale);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 130 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\PlayerTurnEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */