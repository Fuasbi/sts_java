/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ 
/*    */ public class TintEffect
/*    */ {
/*    */   public Color color;
/*    */   private Color targetColor;
/*    */   private float lerpSpeed;
/*    */   private static final float DEFAULT_LERP_SPEED = 3.0F;
/*    */   
/*    */   public TintEffect() {
/* 13 */     this.color = Color.WHITE.cpy();
/* 14 */     this.targetColor = this.color;
/* 15 */     this.lerpSpeed = 3.0F;
/*    */   }
/*    */   
/*    */   public void changeColor(Color targetColor, float lerpSpeed) {
/* 19 */     this.targetColor = targetColor;
/* 20 */     this.lerpSpeed = lerpSpeed;
/*    */   }
/*    */   
/*    */   public void changeColor(Color targetColor) {
/* 24 */     changeColor(targetColor, 3.0F);
/*    */   }
/*    */   
/*    */   public void fadeOut() {
/* 28 */     this.targetColor = new Color(this.color.r, this.color.g, this.color.b, 0.0F);
/* 29 */     this.lerpSpeed = 3.0F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 33 */     this.color.lerp(this.targetColor, com.badlogic.gdx.Gdx.graphics.getDeltaTime() * this.lerpSpeed);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\TintEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */