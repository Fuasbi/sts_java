/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class CardTrailEffect extends AbstractGameEffect
/*    */ {
/*    */   private static final float EFFECT_DUR = 0.5F;
/*    */   private static final float DUR_DIV_2 = 0.25F;
/*    */   private float x;
/*    */   private float y;
/* 17 */   private static TextureAtlas.AtlasRegion img = null;
/*    */   private static final int W = 12;
/* 19 */   private static final int W_DIV_2 = 6; private static final float SCALE_MULTI = com.megacrit.cardcrawl.core.Settings.scale * 22.0F;
/*    */   
/*    */   public CardTrailEffect(float x, float y) {
/* 22 */     if (img == null) {
/* 23 */       img = ImageMaster.vfxAtlas.findRegion("combat/blurDot2");
/*    */     }
/*    */     
/* 26 */     this.renderBehind = false;
/* 27 */     this.duration = 0.5F;
/* 28 */     this.startingDuration = 0.5F;
/*    */     
/* 30 */     this.x = (x - 6.0F);
/* 31 */     this.y = (y - 6.0F);
/* 32 */     this.rotation = 0.0F;
/*    */     
/* 34 */     switch (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.chosenClass) {
/*    */     case IRONCLAD: 
/* 36 */       this.color = new Color(1.0F, 0.4F, 0.1F, 1.0F);
/* 37 */       break;
/*    */     case THE_SILENT: 
/* 39 */       this.color = Color.CHARTREUSE.cpy();
/* 40 */       break;
/*    */     case DEFECT: 
/* 42 */       this.color = Color.SKY.cpy();
/* 43 */       break;
/*    */     }
/*    */     
/*    */     
/*    */ 
/* 48 */     this.scale = 0.01F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 52 */     this.duration -= Gdx.graphics.getDeltaTime();
/*    */     
/* 54 */     if (this.duration < 0.25F) {
/* 55 */       this.scale = (this.duration * SCALE_MULTI);
/*    */     } else {
/* 57 */       this.scale = ((this.duration - 0.25F) * SCALE_MULTI);
/*    */     }
/*    */     
/* 60 */     if (this.duration < 0.0F) {
/* 61 */       this.isDone = true;
/*    */     } else {
/* 63 */       this.color.a = Interpolation.fade.apply(0.0F, 0.18F, this.duration / 0.5F);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 69 */     sb.setBlendFunction(770, 1);
/* 70 */     sb.setColor(this.color);
/* 71 */     sb.draw(img, this.x, this.y, 6.0F, 6.0F, 12.0F, 12.0F, this.scale, this.scale, this.rotation);
/* 72 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\CardTrailEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */