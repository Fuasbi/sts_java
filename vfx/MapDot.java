/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class MapDot
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float rotation;
/*    */   private static final int RAW_W = 16;
/* 12 */   private static final float DIST_JITTER = 4.0F * Settings.scale;
/* 13 */   private static final float OFFSET_Y = 172.0F * Settings.scale;
/*    */   
/*    */   public MapDot(float x, float y, float rotation, boolean jitter) {
/* 16 */     if (jitter) {
/* 17 */       this.x = (x + MathUtils.random(-DIST_JITTER, DIST_JITTER));
/* 18 */       this.y = (y + MathUtils.random(-DIST_JITTER, DIST_JITTER));
/* 19 */       this.rotation = (rotation + MathUtils.random(-20.0F, 20.0F));
/*    */     } else {
/* 21 */       this.x = x;
/* 22 */       this.y = y;
/* 23 */       this.rotation = rotation;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {
/* 28 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.MAP_DOT_1, this.x - 8.0F, this.y - 8.0F + com.megacrit.cardcrawl.screens.DungeonMapScreen.offsetY + OFFSET_Y, 8.0F, 8.0F, 16.0F, 16.0F, Settings.scale, Settings.scale, this.rotation, 0, 0, 16, 16, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\MapDot.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */