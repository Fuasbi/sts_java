/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.SoulGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import com.megacrit.cardcrawl.relics.Omamori;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class FastCardObtainEffect
/*    */   extends AbstractGameEffect
/*    */ {
/*    */   private AbstractCard card;
/*    */   
/*    */   public FastCardObtainEffect(AbstractCard card, float x, float y)
/*    */   {
/* 25 */     if ((card.color == AbstractCard.CardColor.CURSE) && (AbstractDungeon.player.hasRelic("Omamori")) && 
/* 26 */       (AbstractDungeon.player.getRelic("Omamori").counter != 0)) {
/* 27 */       ((Omamori)AbstractDungeon.player.getRelic("Omamori")).use();
/* 28 */       this.duration = 0.0F;
/* 29 */       this.isDone = true;
/*    */     }
/*    */     
/* 32 */     CardHelper.obtain(card.cardID, card.rarity, card.color);
/*    */     
/* 34 */     this.card = card;
/* 35 */     this.duration = 0.01F;
/* 36 */     card.current_x = x;
/* 37 */     card.current_y = y;
/* 38 */     CardCrawlGame.sound.play("CARD_SELECT");
/*    */   }
/*    */   
/*    */   public void update() {
/* 42 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 43 */     this.card.update();
/*    */     
/* 45 */     if (this.duration < 0.0F)
/*    */     {
/*    */ 
/* 48 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 49 */         r.onObtainCard(this.card);
/*    */       }
/*    */       
/* 52 */       this.isDone = true;
/* 53 */       this.card.shrink();
/* 54 */       AbstractDungeon.getCurrRoom().souls.obtain(this.card, true);
/* 55 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 56 */         r.onMasterDeckChange();
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 63 */     if (!this.isDone) {
/* 64 */       this.card.render(sb);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\FastCardObtainEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */