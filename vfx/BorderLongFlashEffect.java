/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class BorderLongFlashEffect extends AbstractGameEffect
/*    */ {
/* 13 */   private TextureAtlas.AtlasRegion img = com.megacrit.cardcrawl.helpers.ImageMaster.BORDER_GLOW_2;
/*    */   
/*    */ 
/*    */   private static final float DUR = 2.0F;
/*    */   
/*    */ 
/*    */   private float startAlpha;
/*    */   
/*    */ 
/*    */   public BorderLongFlashEffect(Color color)
/*    */   {
/* 24 */     this.duration = 2.0F;
/* 25 */     this.startAlpha = color.a;
/* 26 */     this.color = color.cpy();
/* 27 */     this.color.a = 0.0F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 31 */     if (2.0F - this.duration < 0.2F) {
/* 32 */       this.color.a = Interpolation.fade.apply(0.0F, this.startAlpha, (2.0F - this.duration) * 10.0F);
/*    */     } else {
/* 34 */       this.color.a = Interpolation.pow2Out.apply(0.0F, this.startAlpha, this.duration / 2.0F);
/*    */     }
/*    */     
/* 37 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 38 */     if (this.duration < 0.0F) {
/* 39 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 45 */     sb.setBlendFunction(770, 1);
/* 46 */     sb.setColor(this.color);
/* 47 */     sb.draw(this.img, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/* 48 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\BorderLongFlashEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */