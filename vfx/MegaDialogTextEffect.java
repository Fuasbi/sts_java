/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.AppearEffect;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordColor;
/*     */ import com.megacrit.cardcrawl.ui.DialogWord.WordEffect;
/*     */ import com.megacrit.cardcrawl.ui.SpeechWord;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Scanner;
/*     */ 
/*     */ public class MegaDialogTextEffect extends AbstractGameEffect
/*     */ {
/*     */   private static GlyphLayout gl;
/*     */   private com.badlogic.gdx.graphics.g2d.BitmapFont font;
/*     */   private DialogWord.AppearEffect a_effect;
/*  21 */   private static final float DEFAULT_WIDTH = 320.0F * Settings.scale;
/*  22 */   private static final float LINE_SPACING = 30.0F * Settings.scale;
/*  23 */   private static final float CHAR_SPACING = 8.0F * Settings.scale;
/*     */   private static final float WORD_TIME = 0.03F;
/*  25 */   private float wordTimer = 0.0F;
/*  26 */   private boolean textDone = false;
/*     */   private float x;
/*  28 */   private float y; private ArrayList<SpeechWord> words = new ArrayList();
/*  29 */   private int curLine = 0;
/*     */   private Scanner s;
/*  31 */   private float curLineWidth = 0.0F;
/*     */   private static final float FADE_TIME = 0.3F;
/*     */   
/*     */   public MegaDialogTextEffect(float x, float y, float duration, String msg, DialogWord.AppearEffect a_effect) {
/*  35 */     if (gl == null) {
/*  36 */       gl = new GlyphLayout();
/*     */     }
/*     */     
/*  39 */     this.duration = duration;
/*  40 */     this.x = x;
/*  41 */     this.y = y;
/*  42 */     this.font = FontHelper.panelNameFont;
/*  43 */     this.a_effect = a_effect;
/*  44 */     this.s = new Scanner(msg);
/*     */   }
/*     */   
/*     */   public void update() {
/*  48 */     this.wordTimer -= Gdx.graphics.getDeltaTime();
/*  49 */     if ((this.wordTimer < 0.0F) && (!this.textDone)) {
/*  50 */       this.wordTimer = 0.03F;
/*  51 */       if (Settings.lineBreakViaCharacter) {
/*  52 */         addWordCN();
/*     */       } else {
/*  54 */         addWord();
/*     */       }
/*     */     }
/*     */     
/*  58 */     for (SpeechWord w : this.words) {
/*  59 */       w.update();
/*     */     }
/*     */     
/*     */ 
/*  63 */     this.duration -= Gdx.graphics.getDeltaTime();
/*  64 */     if (this.duration < 0.0F) {
/*  65 */       this.words.clear();
/*  66 */       this.isDone = true;
/*     */     }
/*     */     
/*  69 */     if (this.duration < 0.3F) {
/*  70 */       for (SpeechWord w : this.words) {
/*  71 */         w.fadeOut();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void addWord() {
/*  77 */     this.wordTimer = 0.03F;
/*     */     
/*  79 */     if (this.s.hasNext()) {
/*  80 */       String word = this.s.next();
/*     */       
/*  82 */       if (word.equals("NL")) {
/*  83 */         this.curLine += 1;
/*  84 */         for (SpeechWord w : this.words) {
/*  85 */           w.shiftY(LINE_SPACING);
/*     */         }
/*  87 */         this.curLineWidth = 0.0F;
/*  88 */         return;
/*     */       }
/*     */       
/*  91 */       DialogWord.WordColor color = SpeechWord.identifyWordColor(word);
/*  92 */       if (color != DialogWord.WordColor.DEFAULT) {
/*  93 */         word = word.substring(2, word.length());
/*     */       }
/*     */       
/*  96 */       DialogWord.WordEffect effect = SpeechWord.identifyWordEffect(word);
/*  97 */       if (effect != DialogWord.WordEffect.NONE) {
/*  98 */         word = word.substring(1, word.length() - 1);
/*     */       }
/*     */       
/* 101 */       gl.setText(this.font, word);
/* 102 */       float temp = 0.0F;
/*     */       
/*     */ 
/* 105 */       if (this.curLineWidth + gl.width > DEFAULT_WIDTH) {
/* 106 */         this.curLine += 1;
/* 107 */         for (SpeechWord w : this.words) {
/* 108 */           w.shiftY(LINE_SPACING);
/*     */         }
/* 110 */         this.curLineWidth = (gl.width + CHAR_SPACING);
/* 111 */         temp = -this.curLineWidth / 2.0F;
/*     */       }
/*     */       else
/*     */       {
/* 115 */         this.curLineWidth += gl.width;
/* 116 */         temp = -this.curLineWidth / 2.0F;
/*     */         
/* 118 */         for (SpeechWord w : this.words) {
/* 119 */           if (w.line == this.curLine) {
/* 120 */             w.setX(this.x + temp);
/* 121 */             gl.setText(this.font, w.word);
/* 122 */             temp += gl.width + CHAR_SPACING;
/*     */           }
/*     */         }
/*     */         
/* 126 */         this.curLineWidth += CHAR_SPACING;
/* 127 */         gl.setText(this.font, word + " ");
/*     */       }
/*     */       
/* 130 */       this.words.add(new SpeechWord(this.font, word, this.a_effect, effect, color, this.x + temp, this.y - LINE_SPACING * this.curLine, this.curLine));
/*     */     }
/*     */     else
/*     */     {
/* 134 */       this.textDone = true;
/* 135 */       this.s.close();
/*     */     }
/*     */   }
/*     */   
/*     */   private void addWordCN() {
/* 140 */     this.wordTimer = 0.03F;
/*     */     
/* 142 */     if (this.s.hasNext()) {
/* 143 */       String word = this.s.next();
/*     */       
/* 145 */       if (word.equals("NL")) {
/* 146 */         return;
/*     */       }
/*     */       
/* 149 */       DialogWord.WordColor color = SpeechWord.identifyWordColor(word);
/* 150 */       if (color != DialogWord.WordColor.DEFAULT) {
/* 151 */         word = word.substring(2, word.length());
/*     */       }
/*     */       
/* 154 */       DialogWord.WordEffect effect = SpeechWord.identifyWordEffect(word);
/* 155 */       if (effect != DialogWord.WordEffect.NONE) {
/* 156 */         word = word.substring(1, word.length() - 1);
/*     */       }
/*     */       
/*     */ 
/* 160 */       for (int i = 0; i < word.length(); i++) {
/* 161 */         String tmp = Character.toString(word.charAt(i));
/*     */         
/* 163 */         gl.setText(this.font, tmp);
/* 164 */         float temp = 0.0F;
/*     */         
/*     */ 
/* 167 */         if (this.curLineWidth + gl.width > DEFAULT_WIDTH) {
/* 168 */           this.curLine += 1;
/* 169 */           for (SpeechWord w : this.words) {
/* 170 */             w.shiftY(LINE_SPACING);
/*     */           }
/* 172 */           this.curLineWidth = gl.width;
/* 173 */           temp = -this.curLineWidth / 2.0F;
/*     */         }
/*     */         else
/*     */         {
/* 177 */           this.curLineWidth += gl.width;
/* 178 */           temp = -this.curLineWidth / 2.0F;
/*     */           
/* 180 */           for (SpeechWord w : this.words) {
/* 181 */             if (w.line == this.curLine) {
/* 182 */               w.setX(this.x + temp);
/* 183 */               gl.setText(this.font, w.word);
/* 184 */               temp += gl.width;
/*     */             }
/*     */           }
/* 187 */           gl.setText(this.font, tmp + " ");
/*     */         }
/*     */         
/* 190 */         this.words.add(new SpeechWord(this.font, tmp, this.a_effect, effect, color, this.x + temp, this.y - LINE_SPACING * this.curLine, this.curLine));
/*     */       }
/*     */     }
/*     */     else {
/* 194 */       this.textDone = true;
/* 195 */       this.s.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 200 */     for (SpeechWord w : this.words) {
/* 201 */       w.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\MegaDialogTextEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */