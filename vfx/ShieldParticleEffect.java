/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class ShieldParticleEffect extends AbstractGameEffect
/*    */ {
/*    */   private com.badlogic.gdx.graphics.Texture img;
/*    */   private static final int RAW_W = 64;
/*    */   private static final float DURATION = 2.0F;
/*    */   private float x;
/*    */   private float y;
/* 17 */   private float scale = Settings.scale / 2.0F;
/*    */   
/*    */   public ShieldParticleEffect(float x, float y) {
/* 20 */     this.duration = 2.0F;
/*    */     
/* 22 */     this.x = x;
/* 23 */     this.y = y;
/* 24 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.INTENT_DEFEND;
/* 25 */     this.renderBehind = true;
/* 26 */     this.color = new Color(1.0F, 1.0F, 1.0F, 0.0F);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 31 */     this.scale += Gdx.graphics.getDeltaTime() * Settings.scale * 1.1F;
/*    */     
/*    */ 
/* 34 */     if (this.duration > 1.0F) {
/* 35 */       this.color.a = Interpolation.fade.apply(0.0F, 0.3F, 1.0F - (this.duration - 1.0F));
/*    */     } else {
/* 37 */       this.color.a = Interpolation.fade.apply(0.3F, 0.0F, 1.0F - this.duration);
/*    */     }
/*    */     
/* 40 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 41 */     if (this.duration < 0.0F) {
/* 42 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 48 */     sb.setBlendFunction(770, 1);
/* 49 */     sb.setColor(this.color);
/* 50 */     sb.draw(this.img, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, 0.0F, 0, 0, 64, 64, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 67 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\ShieldParticleEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */