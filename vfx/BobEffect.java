/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class BobEffect
/*    */ {
/*  8 */   public float y = 0.0F;
/*  9 */   public float speed; public float dist; private float timer = MathUtils.random(0.0F, 359.0F);
/* 10 */   private static final float DEFAULT_DIST = 5.0F * com.megacrit.cardcrawl.core.Settings.scale;
/*    */   private static final float DEFAULT_SPEED = 4.0F;
/*    */   
/*    */   public BobEffect() {
/* 14 */     this(DEFAULT_DIST, 4.0F);
/*    */   }
/*    */   
/*    */   public BobEffect(float speed) {
/* 18 */     this(DEFAULT_DIST, speed);
/*    */   }
/*    */   
/*    */   public BobEffect(float dist, float speed) {
/* 22 */     this.speed = speed;
/* 23 */     this.dist = dist;
/*    */   }
/*    */   
/*    */   public void update() {
/* 27 */     this.y = (MathUtils.sin(this.timer) * this.dist);
/* 28 */     this.timer += com.badlogic.gdx.Gdx.graphics.getDeltaTime() * this.speed;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\BobEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */