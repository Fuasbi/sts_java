/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public abstract class AbstractGameEffect
/*    */ {
/*    */   public float duration;
/*    */   public float startingDuration;
/*    */   protected Color color;
/* 14 */   public boolean isDone = false;
/* 15 */   protected float scale = Settings.scale; protected float rotation = 0.0F;
/* 16 */   public boolean renderBehind = false;
/*    */   
/*    */   public void update() {
/* 19 */     if (this.duration == this.startingDuration) {}
/*    */     
/*    */ 
/*    */ 
/* 23 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 24 */     if (this.duration < this.startingDuration / 2.0F) {
/* 25 */       this.color.a = (this.duration / (this.startingDuration / 2.0F));
/*    */     }
/*    */     
/* 28 */     if (this.duration < 0.0F) {
/* 29 */       this.isDone = true;
/* 30 */       this.color.a = 0.0F;
/*    */     }
/*    */   }
/*    */   
/*    */   public abstract void render(SpriteBatch paramSpriteBatch);
/*    */   
/*    */   public void render(SpriteBatch sb, float x, float y) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\AbstractGameEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */