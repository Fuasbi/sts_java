/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class DebuffParticleEffect extends AbstractGameEffect
/*    */ {
/*    */   private com.badlogic.gdx.graphics.Texture img;
/* 14 */   private static int IMG_NUM = 0;
/*    */   private static final int RAW_W = 32;
/*    */   private static final float DURATION = 4.0F;
/*    */   private static final float SPIN_VELOCITY = 120.0F;
/*    */   private boolean spinClockwise;
/*    */   private float x;
/* 20 */   private float y; private float scale = 0.0F;
/*    */   
/*    */   public DebuffParticleEffect(float x, float y) {
/* 23 */     this.x = (x + MathUtils.random(-36.0F, 36.0F) * Settings.scale);
/* 24 */     this.y = (y + MathUtils.random(-36.0F, 36.0F) * Settings.scale);
/* 25 */     this.duration = 4.0F;
/* 26 */     this.rotation = MathUtils.random(360.0F);
/* 27 */     this.spinClockwise = MathUtils.randomBoolean();
/*    */     
/* 29 */     if (IMG_NUM == 0) {
/* 30 */       this.renderBehind = true;
/* 31 */       this.img = ImageMaster.loadImage("images/ui/intent/debuffVFX1.png");
/* 32 */       IMG_NUM += 1;
/* 33 */     } else if (IMG_NUM == 1) {
/* 34 */       this.img = ImageMaster.loadImage("images/ui/intent/debuffVFX3.png");
/* 35 */       IMG_NUM += 1;
/*    */     } else {
/* 37 */       this.img = ImageMaster.loadImage("images/ui/intent/debuffVFX2.png");
/* 38 */       IMG_NUM = 0;
/*    */     }
/*    */     
/* 41 */     this.color = new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, 0.0F);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 49 */     if (this.spinClockwise) {
/* 50 */       this.rotation += Gdx.graphics.getDeltaTime() * 120.0F;
/*    */     } else {
/* 52 */       this.rotation -= Gdx.graphics.getDeltaTime() * 120.0F;
/*    */     }
/*    */     
/*    */ 
/* 56 */     if (this.duration > 3.0F) {
/* 57 */       this.color.a = Interpolation.fade.apply(0.0F, 1.0F, 1.0F - (this.duration - 3.0F));
/* 58 */     } else if (this.duration <= 1.0F)
/*    */     {
/*    */ 
/* 61 */       this.color.a = Interpolation.fade.apply(1.0F, 0.0F, 1.0F - this.duration);
/* 62 */       this.scale = Interpolation.fade.apply(Settings.scale, 0.0F, 1.0F - this.duration);
/*    */     }
/*    */     
/* 65 */     if (this.duration > 2.0F) {
/* 66 */       this.scale = Interpolation.bounceOut.apply(0.0F, Settings.scale, 2.0F - (this.duration - 2.0F));
/*    */     }
/*    */     
/* 69 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 70 */     if (this.duration < 0.0F) {
/* 71 */       this.isDone = true;
/* 72 */       this.img.dispose();
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 78 */     sb.setColor(this.color);
/* 79 */     sb.draw(this.img, this.x - 16.0F, this.y - 16.0F, 16.0F, 16.0F, 32.0F, 32.0F, this.scale, this.scale, this.rotation, 0, 0, 32, 32, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\DebuffParticleEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */