/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class InfiniteSpeechBubble extends AbstractGameEffect
/*     */ {
/*     */   private static final int RAW_W = 512;
/*  15 */   private float shadow_offset = 0.0F;
/*  16 */   private static final float SHADOW_OFFSET = 16.0F * Settings.scale;
/*     */   private float x;
/*     */   private float y;
/*  19 */   private float scale_x; private float scale_y; private float wavy_y; private float wavyHelper; private static final float WAVY_SPEED = 6.0F * Settings.scale;
/*  20 */   private static final float WAVY_DISTANCE = 2.0F * Settings.scale;
/*     */   private static final float SCALE_TIME = 0.3F;
/*  22 */   private float scaleTimer = 0.3F;
/*  23 */   private static final float ADJUST_X = 170.0F * Settings.scale;
/*  24 */   private static final float ADJUST_Y = 116.0F * Settings.scale;
/*     */   
/*     */ 
/*     */   private boolean facingRight;
/*     */   
/*     */ 
/*     */   private static final float FADE_TIME = 0.3F;
/*     */   
/*     */   private SpeechTextEffect textEffect;
/*     */   
/*     */ 
/*     */   public InfiniteSpeechBubble(float x, float y, String msg)
/*     */   {
/*  37 */     this.textEffect = new SpeechTextEffect(x - 170.0F * Settings.scale, y + 124.0F * Settings.scale, Float.MAX_VALUE, msg, com.megacrit.cardcrawl.ui.DialogWord.AppearEffect.BUMP_IN);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  43 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(this.textEffect);
/*     */     
/*  45 */     this.x = (x - ADJUST_X);
/*  46 */     this.y = (y + ADJUST_Y);
/*  47 */     this.scaleTimer = 0.3F;
/*  48 */     this.color = new Color(0.8F, 0.9F, 0.9F, 0.0F);
/*  49 */     this.duration = Float.MAX_VALUE;
/*  50 */     this.facingRight = true;
/*     */   }
/*     */   
/*     */   public void dismiss() {
/*  54 */     this.duration = 0.3F;
/*  55 */     this.textEffect.duration = 0.3F;
/*     */   }
/*     */   
/*     */   public void update() {
/*  59 */     updateScale();
/*     */     
/*  61 */     this.wavyHelper += Gdx.graphics.getDeltaTime() * WAVY_SPEED;
/*  62 */     this.wavy_y = (MathUtils.sin(this.wavyHelper) * WAVY_DISTANCE);
/*     */     
/*  64 */     this.duration -= Gdx.graphics.getDeltaTime();
/*  65 */     if (this.duration < 0.0F) {
/*  66 */       this.isDone = true;
/*     */     }
/*     */     
/*  69 */     if (this.duration > 0.3F) {
/*  70 */       this.color.a = MathUtils.lerp(this.color.a, 1.0F, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     } else {
/*  72 */       this.color.a = MathUtils.lerp(this.color.a, 0.0F, Gdx.graphics.getDeltaTime() * 12.0F);
/*     */     }
/*  74 */     this.shadow_offset = MathUtils.lerp(this.shadow_offset, SHADOW_OFFSET, Gdx.graphics.getDeltaTime() * 4.0F);
/*     */   }
/*     */   
/*     */   private void updateScale() {
/*  78 */     this.scaleTimer -= Gdx.graphics.getDeltaTime();
/*  79 */     if (this.scaleTimer < 0.0F) {
/*  80 */       this.scaleTimer = 0.0F;
/*     */     }
/*     */     
/*  83 */     this.scale_x = Interpolation.circleIn.apply(Settings.scale, Settings.scale * 0.5F, this.scaleTimer / 0.3F);
/*  84 */     this.scale_y = Interpolation.swingIn.apply(Settings.scale, Settings.scale * 0.8F, this.scaleTimer / 0.3F);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  89 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.color.a / 4.0F));
/*  90 */     sb.draw(ImageMaster.SPEECH_BUBBLE_IMG, this.x - 256.0F + this.shadow_offset, this.y - 256.0F + this.wavy_y - this.shadow_offset, 256.0F, 256.0F, 512.0F, 512.0F, this.scale_x, this.scale_y, this.rotation, 0, 0, 512, 512, this.facingRight, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 108 */     sb.setColor(this.color);
/* 109 */     sb.draw(ImageMaster.SPEECH_BUBBLE_IMG, this.x - 256.0F, this.y - 256.0F + this.wavy_y, 256.0F, 256.0F, 512.0F, 512.0F, this.scale_x, this.scale_y, this.rotation, 0, 0, 512, 512, this.facingRight, false);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\InfiniteSpeechBubble.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */