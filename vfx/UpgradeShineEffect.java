/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*    */ 
/*    */ public class UpgradeShineEffect extends AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private static final float DUR = 0.8F;
/* 15 */   private boolean clang1 = false;
/* 16 */   private boolean clang2 = false;
/*    */   
/*    */   public UpgradeShineEffect(float x, float y) {
/* 19 */     this.x = x;
/* 20 */     this.y = y;
/* 21 */     this.duration = 0.8F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 25 */     if ((this.duration < 0.6F) && (!this.clang1)) {
/* 26 */       CardCrawlGame.sound.play("CARD_UPGRADE");
/* 27 */       this.clang1 = true;
/* 28 */       clank(this.x - 80.0F * Settings.scale, this.y + 0.0F * Settings.scale);
/* 29 */       CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/*    */     }
/*    */     
/* 32 */     if ((this.duration < 0.2F) && (!this.clang2)) {
/* 33 */       this.clang2 = true;
/* 34 */       clank(this.x + 90.0F * Settings.scale, this.y - 110.0F * Settings.scale);
/* 35 */       CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/*    */     }
/*    */     
/* 38 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 39 */     if (this.duration < 0.0F) {
/* 40 */       clank(this.x + 30.0F * Settings.scale, this.y + 120.0F * Settings.scale);
/* 41 */       this.isDone = true;
/* 42 */       CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/*    */     }
/*    */   }
/*    */   
/*    */   private void clank(float x, float y) {
/* 47 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.topLevelEffectsQueue.add(new UpgradeHammerImprintEffect(x, y));
/* 48 */     for (int i = 0; i < 30; i++) {
/* 49 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.topLevelEffectsQueue.add(new UpgradeShineParticleEffect(x + 
/*    */       
/* 51 */         MathUtils.random(-10.0F, 10.0F) * Settings.scale, y + 
/* 52 */         MathUtils.random(-10.0F, 10.0F) * Settings.scale));
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\UpgradeShineEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */