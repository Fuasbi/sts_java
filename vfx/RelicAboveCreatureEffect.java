/*     */ package com.megacrit.cardcrawl.vfx;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ 
/*     */ public class RelicAboveCreatureEffect extends AbstractGameEffect
/*     */ {
/*     */   private static final float TEXT_DURATION = 1.5F;
/*  16 */   private static final float STARTING_OFFSET_Y = 0.0F * Settings.scale;
/*  17 */   private static final float TARGET_OFFSET_Y = 60.0F * Settings.scale;
/*     */   private static final float LERP_RATE = 5.0F;
/*     */   private static final int W = 128;
/*     */   private float x;
/*     */   
/*     */   public RelicAboveCreatureEffect(float x, float y, AbstractRelic relic)
/*     */   {
/*  24 */     this.duration = 1.5F;
/*  25 */     this.startingDuration = 1.5F;
/*  26 */     this.relic = relic;
/*  27 */     this.x = x;
/*  28 */     this.y = y;
/*  29 */     this.color = Color.WHITE.cpy();
/*  30 */     this.offsetY = STARTING_OFFSET_Y;
/*  31 */     this.scale = Settings.scale; }
/*     */   
/*     */   private float y;
/*     */   private float offsetY;
/*     */   private AbstractRelic relic;
/*  36 */   public void update() { if (this.duration > 1.0F) {
/*  37 */       this.color.a = com.badlogic.gdx.math.Interpolation.exp5In.apply(1.0F, 0.0F, (this.duration - 1.0F) * 2.0F);
/*     */     }
/*  39 */     super.update();
/*  40 */     if (AbstractDungeon.player.chosenClass == com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass.DEFECT) {
/*  41 */       this.offsetY = MathUtils.lerp(this.offsetY, TARGET_OFFSET_Y + 80.0F * Settings.scale, Gdx.graphics
/*     */       
/*     */ 
/*  44 */         .getDeltaTime() * 5.0F);
/*     */     } else {
/*  46 */       this.offsetY = MathUtils.lerp(this.offsetY, TARGET_OFFSET_Y, Gdx.graphics.getDeltaTime() * 5.0F);
/*     */     }
/*  48 */     this.y += Gdx.graphics.getDeltaTime() * 12.0F * Settings.scale;
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  53 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, this.color.a / 2.0F));
/*  54 */     sb.draw(this.relic.outlineImg, this.x - 64.0F, this.y - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * (2.5F - this.duration), this.scale * (2.5F - this.duration), this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  72 */     sb.setColor(this.color);
/*  73 */     sb.draw(this.relic.img, this.x - 64.0F, this.y - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * (2.5F - this.duration), this.scale * (2.5F - this.duration), this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  90 */     sb.setBlendFunction(770, 1);
/*     */     
/*  92 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.color.a / 4.0F));
/*  93 */     sb.draw(this.relic.img, this.x - 64.0F, this.y - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * (2.7F - this.duration), this.scale * (2.7F - this.duration), this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 111 */     sb.draw(this.relic.img, this.x - 64.0F, this.y - 64.0F + this.offsetY, 64.0F, 64.0F, 128.0F, 128.0F, this.scale * (3.0F - this.duration), this.scale * (3.0F - this.duration), this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 129 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\RelicAboveCreatureEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */