/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ 
/*    */ public class PotionTrailEffect extends AbstractGameEffect
/*    */ {
/*    */   private static final int W = 64;
/*    */   private static final float DURATION = 1.0F;
/*    */   private float scale;
/*    */   private float x;
/*    */   private float y;
/*    */   private float angle;
/*    */   
/*    */   public PotionTrailEffect(float x, float y, float angle) {
/* 15 */     this.x = x;
/* 16 */     this.y = y;
/* 17 */     this.angle = angle;
/* 18 */     this.color = com.badlogic.gdx.graphics.Color.WHITE.cpy();
/* 19 */     this.duration = 1.0F;
/* 20 */     this.renderBehind = true;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 26 */     if (this.duration < 0.0F) {
/* 27 */       this.isDone = true;
/*    */     }
/*    */     
/* 30 */     this.scale = this.duration;
/* 31 */     this.color.a = (this.duration / 2.0F);
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 36 */     sb.setColor(this.color);
/* 37 */     sb.draw(com.megacrit.cardcrawl.helpers.ImageMaster.WHITE_SQUARE_IMG, this.x - 32.0F, this.y - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale * com.megacrit.cardcrawl.core.Settings.scale / 2.0F, this.scale * com.megacrit.cardcrawl.core.Settings.scale / 2.0F, this.angle, 0, 0, 64, 64, false, false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\PotionTrailEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */