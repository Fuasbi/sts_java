/*    */ package com.megacrit.cardcrawl.vfx;
/*    */ 
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class CrystalTrailEffect extends AbstractGameEffect
/*    */ {
/*    */   private static final float EFFECT_DUR = 0.4F;
/*    */   private float x;
/*    */   private float y;
/* 15 */   private static TextureAtlas.AtlasRegion img = null;
/*    */   
/*    */   public CrystalTrailEffect(float x, float y, float angle) {
/* 18 */     if (img == null) {
/* 19 */       img = com.megacrit.cardcrawl.helpers.ImageMaster.vfxAtlas.findRegion("combat/blurDot");
/*    */     }
/* 21 */     this.renderBehind = false;
/* 22 */     this.duration = 0.4F;
/* 23 */     this.startingDuration = 0.4F;
/*    */     
/* 25 */     this.x = (x - img.packedWidth / 2.0F);
/* 26 */     this.y = (y - img.packedHeight / 2.0F);
/* 27 */     this.rotation = 0.0F;
/*    */     
/*    */ 
/* 30 */     this.color = Color.VIOLET.cpy();
/*    */     
/* 32 */     this.scale = 0.01F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 36 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     
/*    */ 
/* 39 */     if (this.duration < 0.2F) {
/* 40 */       this.scale = (this.duration * Settings.scale * 11.0F);
/*    */     } else {
/* 42 */       this.scale = ((this.duration - 0.2F) * Settings.scale * 11.0F);
/*    */     }
/*    */     
/* 45 */     if (this.duration < 0.0F) {
/* 46 */       this.isDone = true;
/*    */     } else {
/* 48 */       this.color.a = Interpolation.fade.apply(0.0F, 0.15F, this.duration / 0.4F);
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 54 */     sb.setColor(Color.BLACK);
/* 55 */     sb.draw(img, this.x, this.y, img.packedWidth / 2.0F, img.packedHeight / 2.0F, img.packedWidth, img.packedHeight, this.scale, this.scale, this.rotation);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 66 */     sb.setBlendFunction(770, 1);
/* 67 */     sb.setColor(this.color);
/* 68 */     sb.draw(img, this.x, this.y, img.packedWidth / 2.0F, img.packedHeight / 2.0F, img.packedWidth, img.packedHeight, this.scale * 1.7F, this.scale * 1.7F, this.rotation);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 79 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\CrystalTrailEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */