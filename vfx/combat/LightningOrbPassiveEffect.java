/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LightningOrbPassiveEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/* 15 */   public static ArrayList<Texture> img = null;
/* 16 */   private Texture currentImg = null;
/* 17 */   private int index = 0;
/*    */   public static final int W = 122;
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public LightningOrbPassiveEffect(float x, float y)
/*    */   {
/* 24 */     if (img == null) {
/* 25 */       img = new ArrayList();
/* 26 */       img.add(ImageMaster.loadImage("images/vfx/defect/lightning_passive_1.png"));
/* 27 */       img.add(ImageMaster.loadImage("images/vfx/defect/lightning_passive_2.png"));
/* 28 */       img.add(ImageMaster.loadImage("images/vfx/defect/lightning_passive_3.png"));
/* 29 */       img.add(ImageMaster.loadImage("images/vfx/defect/lightning_passive_4.png"));
/* 30 */       img.add(ImageMaster.loadImage("images/vfx/defect/lightning_passive_5.png"));
/*    */     }
/*    */     
/* 33 */     this.renderBehind = MathUtils.randomBoolean();
/* 34 */     this.x = x;
/* 35 */     this.y = y;
/* 36 */     this.color = Settings.LIGHT_YELLOW_COLOR.cpy();
/* 37 */     this.currentImg = ((Texture)img.get(this.index));
/* 38 */     this.scale = (MathUtils.random(0.6F, 1.0F) * Settings.scale);
/* 39 */     this.rotation = MathUtils.random(360.0F);
/* 40 */     if (this.rotation < 120.0F) {
/* 41 */       this.renderBehind = true;
/*    */     }
/* 43 */     this.flipX = MathUtils.randomBoolean();
/* 44 */     this.flipY = MathUtils.randomBoolean();
/* 45 */     this.intervalDuration = MathUtils.random(0.03F, 0.06F);
/* 46 */     this.duration = this.intervalDuration; }
/*    */   
/*    */   private boolean flipX;
/*    */   private boolean flipY;
/*    */   private float intervalDuration;
/* 51 */   public void update() { this.duration -= Gdx.graphics.getDeltaTime();
/* 52 */     if (this.duration < 0.0F) {
/* 53 */       this.index += 1;
/* 54 */       if (this.index > img.size() - 1) {
/* 55 */         this.isDone = true;
/* 56 */         return;
/*    */       }
/* 58 */       this.currentImg = ((Texture)img.get(this.index));
/*    */       
/* 60 */       this.duration = this.intervalDuration;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 66 */     sb.setColor(this.color);
/* 67 */     sb.setBlendFunction(770, 1);
/* 68 */     sb.draw(this.currentImg, this.x - 61.0F, this.y - 61.0F, 61.0F, 61.0F, 122.0F, 122.0F, this.scale, this.scale, this.rotation, 0, 0, 122, 122, this.flipX, this.flipY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 85 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\LightningOrbPassiveEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */