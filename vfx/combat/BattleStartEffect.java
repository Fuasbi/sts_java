/*     */ package com.megacrit.cardcrawl.vfx.combat;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.vfx.UpgradeShineParticleEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class BattleStartEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*  24 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("BattleStartEffect");
/*  25 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   private static final float EFFECT_DUR = 4.0F;
/*  28 */   private static final float HEIGHT_DIV_2 = Settings.HEIGHT / 2.0F;
/*  29 */   private static final float WIDTH_DIV_2 = Settings.WIDTH / 2.0F;
/*  30 */   private boolean surpriseAttack; private boolean soundPlayed = false;
/*  31 */   private boolean bossFight = false;
/*     */   
/*     */   private Color bgColor;
/*     */   
/*  35 */   private static final float TARGET_HEIGHT = 150.0F * Settings.scale;
/*     */   private static final float BG_RECT_EXPAND_SPEED = 3.0F;
/*  37 */   private float currentHeight = 0.0F;
/*     */   
/*     */   private String battleStartMessage;
/*     */   
/*  41 */   private static final String BATTLE_START_MSG = TEXT[0];
/*  42 */   public static final String PLAYER_TURN_MSG = TEXT[1];
/*  43 */   public static final String ENEMY_TURN_MSG = TEXT[2];
/*  44 */   public static final String TURN_TXT = TEXT[3];
/*     */   private String turnMsg;
/*     */   private static final float TEXT_FADE_SPEED = 5.0F;
/*  47 */   private static final float MAIN_MSG_OFFSET_Y = 20.0F * Settings.scale;
/*  48 */   private static final float TURN_MSG_OFFSET_Y = -30.0F * Settings.scale;
/*  49 */   private Color turnMessageColor = new Color(0.7F, 0.7F, 0.7F, 0.0F);
/*  50 */   private float timer1 = 1.0F; private float timer2 = 1.0F;
/*  51 */   private static final float MSG_VANISH_X = -Settings.WIDTH * 0.25F;
/*  52 */   private float firstMessageX = Settings.WIDTH / 2.0F;
/*  53 */   private float secondMessageX = Settings.WIDTH * 1.5F;
/*  54 */   private boolean showHb = false;
/*     */   
/*     */ 
/*  57 */   private static TextureAtlas.AtlasRegion img = null;
/*     */   private static final float SWORD_ANIM_TIME = 0.5F;
/*  59 */   private float swordTimer = 0.5F;
/*  60 */   private static final float SWORD_START_X = -50.0F * Settings.scale;
/*  61 */   private static final float SWORD_DEST_X = Settings.WIDTH / 2.0F + 0.0F * Settings.scale;
/*     */   private float swordX;
/*  63 */   private float swordY; private float swordAngle; private boolean swordSound1 = false;
/*  64 */   private Color swordColor = new Color(0.9F, 0.9F, 0.85F, 0.0F);
/*     */   
/*     */   public BattleStartEffect(boolean surpriseAttack) {
/*  67 */     this.duration = 4.0F;
/*  68 */     this.startingDuration = 4.0F;
/*  69 */     this.surpriseAttack = surpriseAttack;
/*     */     
/*  71 */     this.bgColor = new Color(AbstractDungeon.fadeColor.r / 2.0F, AbstractDungeon.fadeColor.g / 2.0F, AbstractDungeon.fadeColor.b / 2.0F, 0.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  77 */     if (img == null) {
/*  78 */       img = ImageMaster.vfxAtlas.findRegion("combat/battleStartSword");
/*     */     }
/*     */     
/*  81 */     this.scale = Settings.scale;
/*  82 */     this.swordY = (Settings.HEIGHT / 2.0F - img.packedHeight / 2.0F + 20.0F * Settings.scale);
/*     */     
/*  84 */     if (surpriseAttack) {
/*  85 */       this.turnMsg = ENEMY_TURN_MSG;
/*     */     } else {
/*  87 */       this.turnMsg = PLAYER_TURN_MSG;
/*     */     }
/*     */     
/*  90 */     this.color = Settings.GOLD_COLOR.cpy();
/*  91 */     this.color.a = 0.0F;
/*  92 */     if (Settings.usesOrdinal) {
/*  93 */       this.battleStartMessage = (Integer.toString(GameActionManager.turn) + getOrdinalNaming(GameActionManager.turn) + TURN_TXT);
/*     */     }
/*     */     else {
/*  96 */       this.battleStartMessage = (Integer.toString(GameActionManager.turn) + TURN_TXT);
/*     */     }
/*     */     
/*  99 */     if ((AbstractDungeon.getCurrRoom() instanceof com.megacrit.cardcrawl.rooms.MonsterRoomBoss)) {
/* 100 */       this.bossFight = true;
/* 101 */       CardCrawlGame.sound.play("BATTLE_START_BOSS");
/*     */     }
/* 103 */     else if (MathUtils.randomBoolean()) {
/* 104 */       CardCrawlGame.sound.play("BATTLE_START_1");
/*     */     } else {
/* 106 */       CardCrawlGame.sound.play("BATTLE_START_2");
/*     */     }
/*     */   }
/*     */   
/*     */   public static String getOrdinalNaming(int i)
/*     */   {
/* 112 */     return (i % 100 == 11) || (i % 100 == 12) || (i % 100 == 13) ? "th" : new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" }[(i % 10)];
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 117 */     if (!this.showHb) {
/* 118 */       AbstractDungeon.player.showHealthBar();
/* 119 */       for (com.megacrit.cardcrawl.monsters.AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 120 */         m.showHealthBar();
/*     */       }
/* 122 */       this.showHb = true;
/*     */     }
/*     */     
/*     */ 
/* 126 */     if (this.duration > 3.0F) {
/* 127 */       this.currentHeight = MathUtils.lerp(this.currentHeight, TARGET_HEIGHT, Gdx.graphics
/*     */       
/*     */ 
/* 130 */         .getDeltaTime() * 3.0F);
/* 131 */     } else if (this.duration < 0.5F) {
/* 132 */       this.currentHeight = MathUtils.lerp(this.currentHeight, 0.0F, Gdx.graphics.getDeltaTime() * 3.0F);
/*     */     }
/*     */     
/*     */ 
/* 136 */     if ((this.duration < 3.0F) && (this.timer1 != 0.0F)) {
/* 137 */       this.timer1 -= Gdx.graphics.getDeltaTime();
/* 138 */       if (this.timer1 < 0.0F) {
/* 139 */         this.timer1 = 0.0F;
/*     */       }
/* 141 */       this.firstMessageX = Interpolation.pow2In.apply(this.firstMessageX, MSG_VANISH_X, 1.0F - this.timer1);
/* 142 */     } else if ((this.duration < 3.0F) && (this.timer2 != 0.0F)) {
/* 143 */       if (!this.soundPlayed) {
/* 144 */         CardCrawlGame.sound.play("TURN_EFFECT");
/* 145 */         AbstractDungeon.getMonsters().showIntent();
/* 146 */         this.soundPlayed = true;
/*     */       }
/* 148 */       this.timer2 -= Gdx.graphics.getDeltaTime();
/* 149 */       if (this.timer2 < 0.0F) {
/* 150 */         this.timer2 = 0.0F;
/*     */       }
/* 152 */       this.secondMessageX = Interpolation.pow2In.apply(this.secondMessageX, WIDTH_DIV_2, 1.0F - this.timer2);
/*     */     }
/*     */     
/*     */ 
/* 156 */     if (this.duration > 1.0F) {
/* 157 */       this.color.a = MathUtils.lerp(this.color.a, 1.0F, Gdx.graphics.getDeltaTime() * 5.0F);
/*     */     } else {
/* 159 */       this.color.a = MathUtils.lerp(this.color.a, 0.0F, Gdx.graphics.getDeltaTime() * 5.0F);
/*     */     }
/* 161 */     this.bgColor.a = (this.color.a * 0.75F);
/* 162 */     this.turnMessageColor.a = this.color.a;
/*     */     
/*     */ 
/* 165 */     if (Settings.FAST_MODE) {
/* 166 */       this.duration -= Gdx.graphics.getDeltaTime();
/*     */     }
/* 168 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 169 */     if (this.duration < 0.0F) {
/* 170 */       this.isDone = true;
/*     */     }
/*     */     
/* 173 */     updateSwords();
/*     */   }
/*     */   
/*     */   private void updateSwords() {
/* 177 */     this.swordTimer -= Gdx.graphics.getDeltaTime();
/* 178 */     if (this.swordTimer < 0.0F) {
/* 179 */       this.swordTimer = 0.0F;
/*     */     }
/*     */     
/* 182 */     this.swordColor.a = Interpolation.fade.apply(1.0F, 0.01F, this.swordTimer / 0.5F);
/*     */     
/* 184 */     if (this.bossFight) {
/* 185 */       if ((this.swordTimer < 0.1F) && (!this.swordSound1)) {
/* 186 */         this.swordSound1 = true;
/*     */         
/* 188 */         CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.MED, com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur.SHORT, false);
/* 189 */         for (int i = 0; i < 30; i++) {
/* 190 */           if (MathUtils.randomBoolean()) {
/* 191 */             AbstractDungeon.effectsQueue.add(new UpgradeShineParticleEffect(Settings.WIDTH / 2.0F + 
/*     */             
/* 193 */               MathUtils.random(-150.0F, 150.0F) * Settings.scale, Settings.HEIGHT / 2.0F + 
/* 194 */               MathUtils.random(-10.0F, 50.0F) * Settings.scale));
/*     */           } else {
/* 196 */             AbstractDungeon.topLevelEffectsQueue.add(new UpgradeShineParticleEffect(Settings.WIDTH / 2.0F + 
/*     */             
/* 198 */               MathUtils.random(-150.0F, 150.0F) * Settings.scale, Settings.HEIGHT / 2.0F + 
/* 199 */               MathUtils.random(-10.0F, 50.0F) * Settings.scale));
/*     */           }
/*     */         }
/*     */       }
/*     */       
/* 204 */       this.swordX = Interpolation.pow3Out.apply(SWORD_DEST_X, SWORD_START_X, this.swordTimer / 0.5F);
/* 205 */       this.swordAngle = Interpolation.pow3Out.apply(-50.0F, 500.0F, this.swordTimer / 0.5F);
/*     */     } else {
/* 207 */       this.swordX = SWORD_DEST_X;
/* 208 */       this.swordAngle = -50.0F;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 214 */     sb.setColor(this.bgColor);
/* 215 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, HEIGHT_DIV_2 - this.currentHeight / 2.0F, Settings.WIDTH, this.currentHeight);
/*     */     
/* 217 */     renderSwords(sb);
/*     */     
/*     */ 
/* 220 */     FontHelper.renderFontCentered(sb, FontHelper.bannerNameFont, BATTLE_START_MSG, this.firstMessageX, HEIGHT_DIV_2 + MAIN_MSG_OFFSET_Y, this.color, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 230 */     FontHelper.renderFontCentered(sb, FontHelper.bannerNameFont, this.turnMsg, this.secondMessageX, HEIGHT_DIV_2 + MAIN_MSG_OFFSET_Y, this.color, 1.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 239 */     if (!this.surpriseAttack) {
/* 240 */       FontHelper.renderFontCentered(sb, FontHelper.turnNumFont, this.battleStartMessage, this.secondMessageX, HEIGHT_DIV_2 + TURN_MSG_OFFSET_Y, this.turnMessageColor);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void renderSwords(SpriteBatch sb)
/*     */   {
/* 251 */     sb.setColor(this.swordColor);
/* 252 */     sb.draw(img, Settings.WIDTH - this.swordX - img.packedWidth / 2.0F + this.firstMessageX - Settings.WIDTH / 2.0F, this.swordY, img.packedWidth / 2.0F, img.packedHeight / 2.0F, img.packedWidth, img.packedHeight, -this.scale, -this.scale, -this.swordAngle + 180.0F);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 263 */     sb.draw(img, this.swordX - img.packedWidth / 2.0F + this.firstMessageX - Settings.WIDTH / 2.0F, this.swordY, img.packedWidth / 2.0F, img.packedHeight / 2.0F, img.packedWidth, img.packedHeight, this.scale, this.scale, this.swordAngle);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\BattleStartEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */