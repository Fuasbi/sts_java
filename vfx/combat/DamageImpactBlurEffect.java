/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class DamageImpactBlurEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float vX;
/*    */   private float vY;
/*    */   private float startScale;
/*    */   private TextureAtlas.AtlasRegion img;
/*    */   
/*    */   public DamageImpactBlurEffect(float x, float y)
/*    */   {
/* 17 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.STRIKE_BLUR;
/* 18 */     this.duration = MathUtils.random(0.5F, 0.75F);
/* 19 */     this.startingDuration = this.duration;
/* 20 */     this.x = (x - this.img.packedWidth / 2.0F);
/* 21 */     this.y = (y - this.img.packedHeight / 2.0F);
/* 22 */     this.rotation = 0.0F;
/* 23 */     this.vX = MathUtils.random(-42000.0F * com.megacrit.cardcrawl.core.Settings.scale, 42000.0F * com.megacrit.cardcrawl.core.Settings.scale);
/* 24 */     this.vY = MathUtils.random(-42000.0F * com.megacrit.cardcrawl.core.Settings.scale, 42000.0F * com.megacrit.cardcrawl.core.Settings.scale);
/* 25 */     this.startScale = MathUtils.random(4.0F, 8.0F);
/* 26 */     this.renderBehind = true;
/*    */     
/* 28 */     float tmp = MathUtils.random(0.1F, 0.3F);
/* 29 */     this.color = new com.badlogic.gdx.graphics.Color(tmp, tmp, tmp, 1.0F);
/*    */   }
/*    */   
/*    */   public void update() {
/* 33 */     this.x += this.vX * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 34 */     this.y += this.vY * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 35 */     this.vX *= 56.0F * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 36 */     this.vY *= 56.0F * com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     
/* 38 */     this.scale = (com.megacrit.cardcrawl.core.Settings.scale * (this.duration / this.startingDuration * 2.0F + this.startScale));
/* 39 */     this.color.a = this.duration;
/*    */     
/* 41 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 42 */     if (this.duration < 0.0F) {
/* 43 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb)
/*    */   {
/* 49 */     sb.setColor(this.color);
/* 50 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DamageImpactBlurEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */