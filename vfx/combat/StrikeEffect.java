/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class StrikeEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   public StrikeEffect(AbstractCreature target, float x, float y, int number)
/*    */   {
/* 14 */     AbstractDungeon.effectsQueue.add(new DamageNumberEffect(target, x, y, number));
/*    */     
/* 16 */     for (int i = 0; i < 18; i++) {
/* 17 */       AbstractDungeon.effectsQueue.add(new DamageImpactLineEffect(x, y));
/*    */     }
/* 19 */     for (int i = 0; i < 5; i++) {
/* 20 */       AbstractDungeon.effectsQueue.add(new DamageImpactCurvyEffect(x, y));
/*    */     }
/*    */     
/* 23 */     if (number < 5) {
/* 24 */       CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT, false);
/* 25 */     } else if (number < 20) {
/* 26 */       CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/*    */     } else {
/* 28 */       CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/*    */     }
/*    */   }
/*    */   
/*    */   public StrikeEffect(AbstractCreature target, float x, float y, String msg) {
/* 33 */     AbstractDungeon.effectsQueue.add(new BlockedWordEffect(target, x, y, msg));
/*    */     
/* 35 */     for (int i = 0; i < 18; i++) {
/* 36 */       AbstractDungeon.effectsQueue.add(new BlockImpactLineEffect(x, y));
/*    */     }
/*    */     
/* 39 */     CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 44 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\StrikeEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */