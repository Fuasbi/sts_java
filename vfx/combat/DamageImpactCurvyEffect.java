/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DamageImpactCurvyEffect extends AbstractGameEffect
/*    */ {
/* 18 */   private Vector2 pos = new Vector2();
/*    */   private float speed;
/*    */   private float speedStart;
/* 21 */   private float speedTarget; private float waveIntensity; private float waveSpeed; private TextureAtlas.AtlasRegion img; private ArrayList<Vector2> positions = new ArrayList();
/*    */   
/*    */   public DamageImpactCurvyEffect(float x, float y) {
/* 24 */     this.img = ImageMaster.STRIKE_LINE_2;
/* 25 */     this.duration = MathUtils.random(0.8F, 1.1F);
/* 26 */     this.startingDuration = this.duration;
/* 27 */     this.pos.x = (x - this.img.packedWidth / 2.0F);
/* 28 */     this.pos.y = (y - this.img.packedHeight / 2.0F);
/* 29 */     this.speed = (MathUtils.random(400.0F, 900.0F) * Settings.scale);
/* 30 */     this.speedStart = this.speed;
/* 31 */     this.speedTarget = (MathUtils.random(200.0F, 300.0F) * Settings.scale);
/* 32 */     this.color = new Color(1.0F, 1.0F, 1.0F, 0.5F);
/* 33 */     this.renderBehind = true;
/* 34 */     this.rotation = MathUtils.random(360.0F);
/* 35 */     this.waveIntensity = MathUtils.random(5.0F, 30.0F);
/* 36 */     this.waveSpeed = MathUtils.random(-20.0F, 20.0F);
/* 37 */     this.speedTarget = MathUtils.random(0.1F, 0.5F);
/*    */   }
/*    */   
/*    */   public void update() {
/* 41 */     this.positions.add(this.pos);
/* 42 */     Vector2 tmp = new Vector2(MathUtils.cosDeg(this.rotation), MathUtils.sinDeg(this.rotation));
/* 43 */     tmp.x *= this.speed * Gdx.graphics.getDeltaTime();
/* 44 */     tmp.y *= this.speed * Gdx.graphics.getDeltaTime();
/* 45 */     this.speed = Interpolation.pow2OutInverse.apply(this.speedStart, this.speedTarget, 1.0F - this.duration / this.startingDuration);
/* 46 */     this.pos.x += tmp.x;
/* 47 */     this.pos.y += tmp.y;
/* 48 */     this.rotation += MathUtils.cos(this.duration * this.waveSpeed) * this.waveIntensity * Gdx.graphics.getDeltaTime() * 60.0F;
/* 49 */     this.scale = (Settings.scale * this.duration / this.startingDuration * 0.75F);
/* 50 */     super.update();
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 55 */     sb.setBlendFunction(770, 1);
/* 56 */     Color tmp = Color.GOLDENROD.cpy();
/* 57 */     tmp.a = 0.25F;
/*    */     
/* 59 */     for (int i = this.positions.size() - 1; i > 0; i--) {
/* 60 */       sb.setColor(tmp);
/* 61 */       tmp.a *= 0.95F;
/* 62 */       if (tmp.a > 0.05F) {
/* 63 */         sb.draw(this.img, 
/*    */         
/* 65 */           ((Vector2)this.positions.get(i)).x, 
/* 66 */           ((Vector2)this.positions.get(i)).y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale * 2.0F, this.scale * 2.0F, this.rotation);
/*    */       }
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 77 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DamageImpactCurvyEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */