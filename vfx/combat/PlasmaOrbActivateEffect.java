/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ 
/*    */ public class PlasmaOrbActivateEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public PlasmaOrbActivateEffect(float x, float y) {
/* 11 */     this.x = x;
/* 12 */     this.y = y;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     for (int i = 0; i < 12; i++) {
/* 18 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new PlasmaOrbActivateParticle(this.x, this.y));
/*    */     }
/* 20 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\PlasmaOrbActivateEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */