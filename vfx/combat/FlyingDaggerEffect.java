/*     */ package com.megacrit.cardcrawl.vfx.combat;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ 
/*     */ public class FlyingDaggerEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*     */   private float x;
/*     */   private float y;
/*     */   private float destY;
/*     */   private static final float DUR = 0.6F;
/*     */   private TextureAtlas.AtlasRegion img;
/*  20 */   private boolean playedSound = false;
/*     */   
/*     */   public FlyingDaggerEffect(float x, float y, float fAngle) {
/*  23 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.DAGGER_STREAK;
/*  24 */     this.x = (x - 600.0F - this.img.packedWidth / 2.0F);
/*  25 */     this.destY = y;
/*  26 */     this.y = (this.destY - this.img.packedHeight / 2.0F);
/*  27 */     this.startingDuration = 0.6F;
/*  28 */     this.duration = 0.6F;
/*  29 */     this.scale = (MathUtils.random(0.2F, 0.4F) * com.megacrit.cardcrawl.core.Settings.scale);
/*  30 */     this.rotation = fAngle;
/*  31 */     this.color = com.badlogic.gdx.graphics.Color.CHARTREUSE.cpy();
/*     */   }
/*     */   
/*     */   private void playRandomSfX() {
/*  35 */     int roll = MathUtils.random(5);
/*  36 */     switch (roll) {
/*     */     case 0: 
/*  38 */       CardCrawlGame.sound.play("ATTACK_DAGGER_1");
/*  39 */       break;
/*     */     case 1: 
/*  41 */       CardCrawlGame.sound.play("ATTACK_DAGGER_2");
/*  42 */       break;
/*     */     case 2: 
/*  44 */       CardCrawlGame.sound.play("ATTACK_DAGGER_3");
/*  45 */       break;
/*     */     case 3: 
/*  47 */       CardCrawlGame.sound.play("ATTACK_DAGGER_4");
/*  48 */       break;
/*     */     case 4: 
/*  50 */       CardCrawlGame.sound.play("ATTACK_DAGGER_5");
/*  51 */       break;
/*     */     default: 
/*  53 */       CardCrawlGame.sound.play("ATTACK_DAGGER_6");
/*     */     }
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  59 */     if (!this.playedSound) {
/*  60 */       playRandomSfX();
/*  61 */       this.playedSound = true;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  67 */     this.duration -= Gdx.graphics.getDeltaTime();
/*     */     
/*     */ 
/*  70 */     Vector2 derp = new Vector2(MathUtils.cos(0.017453292F * this.rotation), MathUtils.sin(0.017453292F * this.rotation));
/*  71 */     this.x += derp.x * Gdx.graphics.getDeltaTime() * 3500.0F * com.megacrit.cardcrawl.core.Settings.scale;
/*  72 */     this.y += derp.y * Gdx.graphics.getDeltaTime() * 3500.0F * com.megacrit.cardcrawl.core.Settings.scale;
/*  73 */     if (this.duration < 0.0F) {
/*  74 */       this.isDone = true;
/*     */     }
/*  76 */     if (this.duration > 0.3F) {
/*  77 */       this.color.a = Interpolation.fade.apply(1.0F, 0.0F, (this.duration - 0.3F) * 3.3F);
/*     */     } else {
/*  79 */       this.color.a = Interpolation.fade.apply(0.0F, 1.0F, this.duration * 3.3F);
/*     */     }
/*  81 */     this.scale += Gdx.graphics.getDeltaTime();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  88 */     sb.setColor(this.color);
/*  89 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale * 1.5F, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 101 */     sb.setBlendFunction(770, 1);
/* 102 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale * 0.75F, this.scale * 0.75F, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 113 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\FlyingDaggerEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */