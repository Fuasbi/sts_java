/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class InflameEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   float x;
/*    */   float y;
/*    */   
/*    */   public InflameEffect(AbstractCreature creature)
/*    */   {
/* 15 */     this.x = creature.hb.cX;
/* 16 */     this.y = creature.hb.cY;
/*    */   }
/*    */   
/*    */   public void update() {
/* 20 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("ATTACK_FIRE");
/* 21 */     for (int i = 0; i < 75; i++) {
/* 22 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FlameParticleEffect(this.x, this.y));
/*    */     }
/* 24 */     for (int i = 0; i < 20; i++) {
/* 25 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.ExhaustEmberEffect(this.x, this.y));
/*    */     }
/* 27 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\InflameEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */