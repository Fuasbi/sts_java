/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class DarkOrbActivateParticle extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float scaleY;
/*    */   private float aV;
/* 16 */   private static com.badlogic.gdx.graphics.Texture img = null;
/*    */   private static final int W = 140;
/*    */   private boolean flipHorizontal;
/*    */   private boolean flipVertical;
/*    */   
/* 21 */   public DarkOrbActivateParticle(float x, float y) { this.x = x;
/* 22 */     this.y = y;
/* 23 */     if (img == null) {
/* 24 */       img = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/orbs/darkEvoke2.png");
/*    */     }
/* 26 */     this.renderBehind = true;
/* 27 */     this.duration = 0.25F;
/* 28 */     this.startingDuration = 0.25F;
/* 29 */     this.color = new com.badlogic.gdx.graphics.Color(MathUtils.random(0.5F, 1.0F), MathUtils.random(0.6F, 1.0F), 1.0F, 0.5F);
/* 30 */     this.scale = (MathUtils.random(1.0F, 2.0F) * Settings.scale);
/* 31 */     this.rotation = MathUtils.random(-8.0F, 8.0F);
/* 32 */     this.flipHorizontal = MathUtils.randomBoolean();
/* 33 */     this.flipVertical = MathUtils.randomBoolean();
/* 34 */     this.scale = Settings.scale;
/* 35 */     this.scaleY = (2.0F * Settings.scale);
/* 36 */     this.aV = MathUtils.random(-100.0F, 100.0F);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 41 */     this.rotation += Gdx.graphics.getDeltaTime() * this.aV;
/* 42 */     this.scale = (Interpolation.pow4Out.apply(5.0F, 1.0F, this.duration * 4.0F) * Settings.scale);
/* 43 */     this.scaleY = (Interpolation.bounceOut.apply(0.2F, 2.0F, this.duration * 4.0F) * Settings.scale);
/* 44 */     this.color.a = Interpolation.pow5Out.apply(0.01F, 0.5F, this.duration * 4.0F);
/* 45 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 46 */     if (this.duration < 0.0F) {
/* 47 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 53 */     sb.setColor(this.color);
/* 54 */     sb.setBlendFunction(770, 1);
/* 55 */     sb.draw(img, this.x - 70.0F, this.y - 70.0F, 70.0F, 70.0F, 140.0F, 140.0F, this.scale, this.scaleY, this.rotation, 0, 0, 140, 140, this.flipHorizontal, this.flipVertical);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 72 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DarkOrbActivateParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */