/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LoseHPEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   public LoseHPEffect(AbstractCreature target, float x, float y, int number)
/*    */   {
/* 14 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new DamageNumberEffect(target, x, y, number));
/* 15 */     CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.MED, ScreenShake.ShakeDur.SHORT, false);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\LoseHPEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */