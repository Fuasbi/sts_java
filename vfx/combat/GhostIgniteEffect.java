/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GhostIgniteEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final int COUNT = 25;
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public GhostIgniteEffect(float x, float y)
/*    */   {
/* 14 */     this.x = x;
/* 15 */     this.y = y;
/*    */   }
/*    */   
/*    */   public void update() {
/* 19 */     for (int i = 0; i < 25; i++) {
/* 20 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.FireBurstParticleEffect(this.x, this.y));
/* 21 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new LightFlareParticleEffect(this.x, this.y, com.badlogic.gdx.graphics.Color.CHARTREUSE));
/*    */     }
/* 23 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\GhostIgniteEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */