/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DeckPoofEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   public DeckPoofEffect(float x, float y, boolean isDeck)
/*    */   {
/* 10 */     for (int i = 0; i < 70; i++) {
/* 11 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new DeckPoofParticle(x, y, isDeck));
/*    */     }
/*    */   }
/*    */   
/*    */   public void update() {
/* 16 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DeckPoofEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */