/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ 
/*    */ public class VerticalAuraEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float NUM_PARTICLES = 20.0F;
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public VerticalAuraEffect(com.badlogic.gdx.graphics.Color c, float x, float y)
/*    */   {
/* 13 */     this.color = c;
/* 14 */     this.x = x;
/* 15 */     this.y = y;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     for (int i = 0; i < 20.0F; i++) {
/* 21 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new VerticalAuraParticle(this.color, this.x, this.y));
/*    */     }
/* 23 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\VerticalAuraEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */