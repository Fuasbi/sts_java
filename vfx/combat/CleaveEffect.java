/*     */ package com.megacrit.cardcrawl.vfx.combat;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class CleaveEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*     */   private TextureAtlas.AtlasRegion img;
/*     */   private float stallTimer;
/*  18 */   private float fadeOutTimer = 0.4F; private float fadeInTimer = 0.05F;
/*     */   private static final float FADE_OUT_TIME = 0.4F;
/*     */   private static final float FADE_IN_TIME = 0.05F;
/*     */   
/*     */   public CleaveEffect(boolean usedByMonster) {
/*  23 */     this.img = ImageMaster.vfxAtlas.findRegion("combat/cleave");
/*  24 */     this.color = new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  25 */     this.x = (Settings.WIDTH * 0.3F - this.img.packedWidth / 2.0F);
/*  26 */     this.y = (AbstractDungeon.floorY + 100.0F * Settings.scale - this.img.packedHeight / 2.0F);
/*  27 */     this.vX = (100.0F * Settings.scale);
/*  28 */     this.stallTimer = MathUtils.random(0.0F, 0.2F);
/*  29 */     this.scale = (1.2F * Settings.scale);
/*  30 */     this.rotation = MathUtils.random(-5.0F, 1.0F); }
/*     */   
/*     */   private float vX;
/*     */   
/*  34 */   public CleaveEffect() { this.img = ImageMaster.vfxAtlas.findRegion("combat/cleave");
/*  35 */     this.color = new com.badlogic.gdx.graphics.Color(1.0F, 1.0F, 1.0F, 0.0F);
/*  36 */     this.x = (Settings.WIDTH * 0.7F - this.img.packedWidth / 2.0F);
/*  37 */     this.y = (AbstractDungeon.floorY + 100.0F * Settings.scale - this.img.packedHeight / 2.0F);
/*  38 */     this.vX = (100.0F * Settings.scale);
/*  39 */     this.stallTimer = MathUtils.random(0.0F, 0.2F);
/*  40 */     this.scale = (1.2F * Settings.scale);
/*  41 */     this.rotation = MathUtils.random(-5.0F, 1.0F);
/*     */   }
/*     */   
/*     */   private float y;
/*     */   private float x;
/*  46 */   public void update() { if (this.stallTimer > 0.0F) {
/*  47 */       this.stallTimer -= Gdx.graphics.getDeltaTime();
/*  48 */       return;
/*     */     }
/*     */     
/*     */ 
/*  52 */     this.x += this.vX * Gdx.graphics.getDeltaTime();
/*  53 */     this.rotation += MathUtils.random(-0.5F, 0.5F);
/*  54 */     this.scale += 0.005F * Settings.scale;
/*     */     
/*     */ 
/*  57 */     if (this.fadeInTimer != 0.0F) {
/*  58 */       this.fadeInTimer -= Gdx.graphics.getDeltaTime();
/*  59 */       if (this.fadeInTimer < 0.0F) {
/*  60 */         this.fadeInTimer = 0.0F;
/*     */       }
/*  62 */       this.color.a = Interpolation.fade.apply(1.0F, 0.0F, this.fadeInTimer / 0.05F);
/*  63 */     } else if (this.fadeOutTimer != 0.0F) {
/*  64 */       this.fadeOutTimer -= Gdx.graphics.getDeltaTime();
/*  65 */       if (this.fadeOutTimer < 0.0F) {
/*  66 */         this.fadeOutTimer = 0.0F;
/*     */       }
/*  68 */       this.color.a = Interpolation.pow2.apply(0.0F, 1.0F, this.fadeOutTimer / 0.4F);
/*     */     } else {
/*  70 */       this.isDone = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/*  76 */     sb.setColor(this.color);
/*  77 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  89 */     sb.setBlendFunction(770, 1);
/*  90 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 101 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\CleaveEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */