/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class FlashIntentParticle extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float DURATION = 1.0F;
/* 15 */   private static final float START_SCALE = 5.0F * com.megacrit.cardcrawl.core.Settings.scale;
/* 16 */   private float scale = 0.01F;
/*    */   private static int W;
/*    */   private Texture img;
/*    */   private float x;
/*    */   private float y;
/*    */   
/* 22 */   public FlashIntentParticle(Texture img, AbstractMonster m) { this.duration = 1.0F;
/* 23 */     this.img = img;
/* 24 */     W = img.getWidth();
/* 25 */     this.x = (m.intentHb.cX - W / 2.0F);
/* 26 */     this.y = (m.intentHb.cY - W / 2.0F);
/* 27 */     this.renderBehind = true;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 32 */     this.scale = Interpolation.fade.apply(START_SCALE, 0.01F, this.duration);
/* 33 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 34 */     if (this.duration < 0.0F) {
/* 35 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb, float x, float y) {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 49 */     sb.setBlendFunction(770, 1);
/* 50 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.duration / 2.0F));
/* 51 */     sb.draw(this.img, this.x, this.y, W / 2.0F, W / 2.0F, W, W, this.scale, this.scale, 0.0F, 0, 0, W, W, false, false);
/* 52 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\FlashIntentParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */