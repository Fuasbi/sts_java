/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ 
/*    */ public class PowerIconShowEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float DUR = 2.2F;
/*    */   private float x;
/*    */   private float y;
/*    */   private float offsetY;
/*    */   private com.badlogic.gdx.graphics.Texture img;
/* 17 */   private static final float STARTING_OFFSET_Y = 130.0F * Settings.scale;
/* 18 */   private static final float TARGET_OFFSET_Y = 170.0F * Settings.scale;
/*    */   private static final float LERP_RATE = 5.0F;
/*    */   private static final int W = 32;
/*    */   
/*    */   public PowerIconShowEffect(AbstractPower power) {
/* 23 */     if (!power.owner.isDeadOrEscaped()) {
/* 24 */       this.x = power.owner.hb.cX;
/* 25 */       this.y = (power.owner.hb.cY + power.owner.hb.height / 2.0F);
/*    */     }
/* 27 */     this.img = power.img;
/* 28 */     this.duration = 2.2F;
/* 29 */     this.startingDuration = 2.2F;
/* 30 */     this.offsetY = STARTING_OFFSET_Y;
/* 31 */     this.color = Color.WHITE.cpy();
/* 32 */     this.renderBehind = true;
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 38 */     super.update();
/* 39 */     this.offsetY = com.badlogic.gdx.math.MathUtils.lerp(this.offsetY, TARGET_OFFSET_Y, com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 5.0F);
/* 40 */     this.y += com.badlogic.gdx.Gdx.graphics.getDeltaTime() * 12.0F * Settings.scale;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 45 */     if (this.img != null) {
/* 46 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.color.a / 2.0F));
/* 47 */       sb.setBlendFunction(770, 1);
/* 48 */       sb.draw(this.img, this.x - 16.0F, this.y - 16.0F + this.offsetY, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale * 2.5F, Settings.scale * 2.5F, 0.0F, 0, 0, 32, 32, false, false);
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 65 */       sb.setBlendFunction(770, 771);
/* 66 */       sb.setColor(this.color);
/* 67 */       sb.draw(this.img, this.x - 16.0F, this.y - 16.0F + this.offsetY, 16.0F, 16.0F, 32.0F, 32.0F, Settings.scale * 2.0F, Settings.scale * 2.0F, 0.0F, 0, 0, 32, 32, false, false);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\PowerIconShowEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */