/*     */ package com.megacrit.cardcrawl.vfx.combat;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ 
/*     */ public class DamageHeartEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*     */ {
/*  17 */   private static int blockSound = 0;
/*     */   public TextureAtlas.AtlasRegion img;
/*     */   private float x;
/*     */   private float y;
/*     */   private float sY;
/*  22 */   private float tY; private static final float DURATION = 0.6F; private AbstractGameAction.AttackEffect effect; private boolean triggered = false;
/*  23 */   private float delayTimer = 2.0F;
/*     */   private int damage;
/*     */   
/*     */   public DamageHeartEffect(float delay, float x, float y, AbstractGameAction.AttackEffect effect, int damage) {
/*  27 */     this.duration = 0.6F;
/*  28 */     this.delayTimer = delay;
/*  29 */     this.startingDuration = 0.6F;
/*  30 */     this.effect = effect;
/*  31 */     this.img = loadImage();
/*  32 */     this.color = com.badlogic.gdx.graphics.Color.WHITE.cpy();
/*  33 */     this.scale = Settings.scale;
/*  34 */     this.damage = damage;
/*     */     
/*  36 */     this.x = (x - this.img.packedWidth / 2.0F + MathUtils.random(-150.0F, 150.0F) * Settings.scale);
/*  37 */     y -= this.img.packedHeight / 2.0F + MathUtils.random(-150.0F, 150.0F) * Settings.scale;
/*     */     
/*  39 */     switch (effect) {
/*     */     case SHIELD: 
/*  41 */       this.y = (y + 80.0F * Settings.scale);
/*  42 */       this.sY = this.y;
/*  43 */       this.tY = y;
/*  44 */       break;
/*     */     default: 
/*  46 */       this.y = y;
/*  47 */       this.sY = y;
/*  48 */       this.tY = y;
/*     */     }
/*     */   }
/*     */   
/*     */   private TextureAtlas.AtlasRegion loadImage()
/*     */   {
/*  54 */     if (MathUtils.randomBoolean()) {
/*  55 */       if (MathUtils.randomBoolean()) {
/*  56 */         return ImageMaster.ATK_SLASH_HEAVY;
/*     */       }
/*  58 */       return ImageMaster.ATK_SLASH_D;
/*     */     }
/*     */     
/*  61 */     if (MathUtils.randomBoolean()) {
/*  62 */       return ImageMaster.ATK_FIRE;
/*     */     }
/*  64 */     return ImageMaster.ATK_BLUNT_LIGHT;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void playSound(AbstractGameAction.AttackEffect effect)
/*     */   {
/*  96 */     switch (effect) {
/*     */     case SLASH_HEAVY: 
/*  98 */       CardCrawlGame.sound.play("ATTACK_HEAVY");
/*  99 */       break;
/*     */     case BLUNT_LIGHT: 
/* 101 */       CardCrawlGame.sound.play("BLUNT_FAST");
/* 102 */       break;
/*     */     case BLUNT_HEAVY: 
/* 104 */       CardCrawlGame.sound.play("BLUNT_HEAVY");
/* 105 */       break;
/*     */     case FIRE: 
/* 107 */       CardCrawlGame.sound.play("ATTACK_FIRE");
/* 108 */       break;
/*     */     case POISON: 
/* 110 */       CardCrawlGame.sound.play("ATTACK_POISON");
/* 111 */       break;
/*     */     case SHIELD: 
/* 113 */       playBlockSound();
/* 114 */       break;
/*     */     case NONE: 
/*     */       break;
/*     */     default: 
/* 118 */       CardCrawlGame.sound.play("ATTACK_FAST");
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void playBlockSound()
/*     */   {
/* 127 */     if (blockSound == 0) {
/* 128 */       CardCrawlGame.sound.play("BLOCK_GAIN_1");
/* 129 */     } else if (blockSound == 1) {
/* 130 */       CardCrawlGame.sound.play("BLOCK_GAIN_2");
/*     */     } else {
/* 132 */       CardCrawlGame.sound.play("BLOCK_GAIN_3");
/*     */     }
/*     */     
/* 135 */     blockSound += 1;
/* 136 */     if (blockSound > 2) {
/* 137 */       blockSound = 0;
/*     */     }
/*     */   }
/*     */   
/*     */   public void update() {
/* 142 */     if (this.delayTimer > 0.0F) {
/* 143 */       this.delayTimer -= Gdx.graphics.getDeltaTime();
/* 144 */       if (this.delayTimer < 0.0F) {
/* 145 */         playSound(this.effect);
/* 146 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new StrikeEffect(null, this.x + this.img.packedWidth / 2.0F, this.y + this.img.packedHeight / 2.0F, this.damage));
/*     */       }
/*     */       
/* 149 */       return;
/*     */     }
/*     */     
/* 152 */     switch (this.effect) {
/*     */     case SHIELD: 
/* 154 */       this.duration -= Gdx.graphics.getDeltaTime();
/* 155 */       if (this.duration < 0.0F) {
/* 156 */         this.isDone = true;
/* 157 */         this.color.a = 0.0F;
/* 158 */       } else if (this.duration < 0.2F) {
/* 159 */         this.color.a = (this.duration * 5.0F);
/*     */       } else {
/* 161 */         this.color.a = Interpolation.fade.apply(1.0F, 0.0F, this.duration * 0.75F / 0.6F);
/*     */       }
/*     */       
/* 164 */       this.y = Interpolation.exp10In.apply(this.tY, this.sY, this.duration / 0.6F);
/*     */       
/* 166 */       if ((this.duration < 0.4F) && (!this.triggered)) {
/* 167 */         this.triggered = true;
/*     */       }
/*     */       break;
/*     */     default: 
/* 171 */       super.update();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 178 */     if (this.delayTimer < 0.0F) {
/* 179 */       sb.setColor(this.color);
/* 180 */       sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale, this.scale, this.rotation);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DamageHeartEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */