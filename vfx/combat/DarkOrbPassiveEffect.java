/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class DarkOrbPassiveEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private float startingScale;
/*    */   private float rotationSpeed;
/*    */   private com.badlogic.gdx.graphics.Texture img;
/*    */   private static com.badlogic.gdx.graphics.Texture img1;
/*    */   private static com.badlogic.gdx.graphics.Texture img2;
/*    */   private static com.badlogic.gdx.graphics.Texture img3;
/*    */   private static final int W = 74;
/*    */   
/*    */   public DarkOrbPassiveEffect(float x, float y)
/*    */   {
/* 21 */     if (img1 == null) {
/* 22 */       img1 = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/orbs/d1.png");
/* 23 */       img2 = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/orbs/d2.png");
/* 24 */       img3 = com.megacrit.cardcrawl.helpers.ImageMaster.loadImage("images/orbs/d3.png");
/*    */     }
/*    */     
/* 27 */     int roll = MathUtils.random(2);
/* 28 */     switch (roll) {
/*    */     case 0: 
/* 30 */       this.img = img1;
/* 31 */       break;
/*    */     case 1: 
/* 33 */       this.img = img2;
/* 34 */       break;
/*    */     default: 
/* 36 */       this.img = img3;
/*    */     }
/*    */     
/*    */     
/* 40 */     this.color = new com.badlogic.gdx.graphics.Color(MathUtils.random(0.0F, 1.0F), 0.3F, MathUtils.random(0.7F, 1.0F), 0.01F);
/* 41 */     this.renderBehind = false;
/* 42 */     this.duration = 2.0F;
/* 43 */     this.startingDuration = 2.0F;
/* 44 */     this.x = x;
/* 45 */     this.y = y;
/* 46 */     this.rotation = MathUtils.random(360.0F);
/* 47 */     this.startingScale = (MathUtils.random(1.2F, 1.8F) * com.megacrit.cardcrawl.core.Settings.scale);
/* 48 */     this.scale = this.startingScale;
/* 49 */     this.rotationSpeed = MathUtils.random(100.0F, 360.0F);
/*    */   }
/*    */   
/*    */   public void update() {
/* 53 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 54 */     this.rotation += com.badlogic.gdx.Gdx.graphics.getDeltaTime() * this.rotationSpeed;
/*    */     
/* 56 */     if (this.duration > 1.0F) {
/* 57 */       this.color.a = Interpolation.fade.apply(1.0F, 0.0F, this.duration - 1.0F);
/*    */     } else {
/* 59 */       this.color.a = Interpolation.fade.apply(0.0F, 1.0F, this.duration);
/*    */     }
/*    */     
/* 62 */     this.scale = (Interpolation.swingOut.apply(0.01F, this.startingScale, this.duration / 2.0F) * com.megacrit.cardcrawl.core.Settings.scale);
/*    */     
/* 64 */     if ((this.scale < 0.0F) || (this.duration < 0.0F)) {
/* 65 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 71 */     sb.setColor(this.color);
/* 72 */     sb.setBlendFunction(770, 1);
/* 73 */     sb.draw(this.img, this.x - 37.0F, this.y - 37.0F, 37.0F, 37.0F, 74.0F, 74.0F, this.scale, this.scale, this.rotation, 0, 0, 74, 74, false, false);
/* 74 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DarkOrbPassiveEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */