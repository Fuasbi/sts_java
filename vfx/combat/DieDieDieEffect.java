/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class DieDieDieEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/* 11 */   private float interval = 0.0F;
/*    */   
/*    */   public DieDieDieEffect() {
/* 14 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 18 */     this.interval -= Gdx.graphics.getDeltaTime();
/* 19 */     if (this.interval < 0.0F) {
/* 20 */       this.interval = MathUtils.random(0.02F, 0.05F);
/* 21 */       int derp = MathUtils.random(1, 4);
/*    */       
/* 23 */       for (int i = 0; i < derp; i++) {
/* 24 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new ThrowShivEffect(
/*    */         
/* 26 */           MathUtils.random(1200.0F, 2000.0F) * Settings.scale, com.megacrit.cardcrawl.dungeons.AbstractDungeon.floorY + 
/* 27 */           MathUtils.random(-100.0F, 500.0F) * Settings.scale));
/*    */       }
/*    */     }
/*    */     
/* 31 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 32 */     if (this.duration < 0.0F) {
/* 33 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DieDieDieEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */