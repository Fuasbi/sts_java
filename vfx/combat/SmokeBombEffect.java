/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ 
/*    */ public class SmokeBombEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public SmokeBombEffect(float x, float y)
/*    */   {
/* 13 */     this.x = x;
/* 14 */     this.y = y;
/* 15 */     this.duration = 0.2F;
/*    */   }
/*    */   
/*    */   public void update() {
/* 19 */     if (this.duration == 0.2F) {
/* 20 */       CardCrawlGame.sound.play("ATTACK_WHIFF_2");
/* 21 */       for (int i = 0; i < 90; i++) {
/* 22 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new SmokeBlurEffect(this.x, this.y));
/*    */       }
/*    */     }
/* 25 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 26 */     if (this.duration < 0.0F) {
/* 27 */       CardCrawlGame.sound.play("APPEAR");
/* 28 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\SmokeBombEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */