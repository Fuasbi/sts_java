/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class FrostOrbActivateEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   
/*    */   public FrostOrbActivateEffect(float x, float y) {
/* 11 */     this.x = x;
/* 12 */     this.y = y;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FrostOrbActivateParticle(0, this.x, this.y));
/* 18 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FrostOrbActivateParticle(1, this.x, this.y));
/* 19 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FrostOrbActivateParticle(2, this.x, this.y));
/* 20 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\FrostOrbActivateEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */