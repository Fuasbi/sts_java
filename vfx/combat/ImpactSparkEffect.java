/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class ImpactSparkEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private TextureAtlas.AtlasRegion img;
/*    */   private static final float DUR = 1.0F;
/*    */   private float x;
/*    */   private float y;
/*    */   private float vX;
/*    */   private float vY;
/*    */   private float floor;
/* 19 */   private static final float GRAVITY = 20.0F * Settings.scale;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public ImpactSparkEffect(float x, float y)
/*    */   {
/* 28 */     this.img = com.megacrit.cardcrawl.helpers.ImageMaster.GLOW_SPARK_2;
/*    */     
/* 30 */     this.duration = MathUtils.random(0.5F, 1.0F);
/* 31 */     this.x = (x - this.img.packedWidth / 2);
/* 32 */     this.y = (y - this.img.packedHeight / 2);
/* 33 */     this.color = com.badlogic.gdx.graphics.Color.WHITE.cpy();
/* 34 */     this.rotation = MathUtils.random(0.0F, 360.0F);
/* 35 */     this.scale = (MathUtils.random(0.2F, 1.0F) * Settings.scale);
/* 36 */     this.vX = (MathUtils.random(-1500.0F, 1500.0F) * Settings.scale);
/* 37 */     this.vY = (MathUtils.random(-0.0F, 1000.0F) * Settings.scale);
/* 38 */     this.floor = (MathUtils.random(100.0F, 250.0F) * Settings.scale);
/*    */   }
/*    */   
/*    */   public void update() {
/* 42 */     this.vY -= GRAVITY / this.scale;
/* 43 */     this.x += this.vX * Gdx.graphics.getDeltaTime();
/* 44 */     this.y += this.vY * Gdx.graphics.getDeltaTime();
/* 45 */     com.badlogic.gdx.math.Vector2 test = new com.badlogic.gdx.math.Vector2(this.vX, this.vY);
/* 46 */     this.rotation = test.angle();
/*    */     
/*    */ 
/* 49 */     if (this.y < this.floor) {
/* 50 */       this.vY = (-this.vY * 0.75F);
/* 51 */       this.y = (this.floor + 0.1F * Settings.scale);
/* 52 */       this.vX *= 1.1F;
/*    */     }
/*    */     
/* 55 */     if (1.0F - this.duration < 0.1F) {
/* 56 */       this.color.a = com.badlogic.gdx.math.Interpolation.fade.apply(0.0F, 0.5F, (1.0F - this.duration) * 10.0F);
/*    */     } else {
/* 58 */       this.color.a = com.badlogic.gdx.math.Interpolation.pow2Out.apply(0.0F, 0.5F, this.duration);
/*    */     }
/*    */     
/* 61 */     this.duration -= Gdx.graphics.getDeltaTime();
/* 62 */     if (this.duration < 0.0F) {
/* 63 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 69 */     sb.setBlendFunction(770, 1);
/* 70 */     sb.setColor(this.color);
/* 71 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale * 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 79 */       MathUtils.random(0.7F, 1.3F), this.scale * 
/* 80 */       MathUtils.random(0.7F, 1.3F), this.rotation);
/*    */     
/* 82 */     sb.draw(this.img, this.x, this.y, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale * 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 90 */       MathUtils.random(0.7F, 1.3F), this.scale * 
/* 91 */       MathUtils.random(0.7F, 1.3F), this.rotation);
/*    */     
/* 93 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\ImpactSparkEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */