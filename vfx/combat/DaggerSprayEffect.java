/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class DaggerSprayEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   public void update()
/*    */   {
/* 14 */     this.isDone = true;
/* 15 */     for (int i = 0; i < 12; i++) {
/* 16 */       float x = AbstractDungeon.player.hb.cX;
/* 17 */       x += MathUtils.random(900.0F, 1800.0F) * Settings.scale;
/* 18 */       AbstractDungeon.effectsQueue.add(new FlyingDaggerEffect(x, AbstractDungeon.player.hb.cY - 100.0F * Settings.scale + i * 18.0F * Settings.scale, i * 4 - 20.0F));
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\DaggerSprayEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */