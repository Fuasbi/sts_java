/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowOut;
/*    */ import com.badlogic.gdx.math.Interpolation.SwingIn;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class FrostOrbActivateParticle extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/* 15 */   private Texture img = null;
/*    */   public static Texture img1;
/*    */   public static Texture img2;
/*    */   public static Texture img3;
/*    */   public static final int W = 64;
/*    */   private float cX;
/*    */   
/* 22 */   public FrostOrbActivateParticle(int index, float x, float y) { if (img1 == null) {
/* 23 */       img1 = ImageMaster.loadImage("images/orbs/f1.png");
/* 24 */       img2 = ImageMaster.loadImage("images/orbs/f2.png");
/* 25 */       img3 = ImageMaster.loadImage("images/orbs/f3.png");
/*    */     }
/*    */     
/* 28 */     this.cX = x;
/* 29 */     this.cY = y;
/* 30 */     this.sX = this.cX;
/* 31 */     this.sY = this.cY;
/*    */     
/* 33 */     this.rotation = MathUtils.random(-5.0F, 5.0F);
/* 34 */     switch (index) {
/*    */     case 0: 
/* 36 */       this.tX = this.sX;
/* 37 */       this.tY = (this.sY + 5.0F * Settings.scale);
/* 38 */       this.img = img1;
/* 39 */       this.tRotation = MathUtils.random(-5.0F, 5.0F);
/* 40 */       break;
/*    */     case 1: 
/* 42 */       this.tX = (this.sX - 10.0F * Settings.scale);
/* 43 */       this.tY = (this.sY - 5.0F * Settings.scale);
/* 44 */       this.img = img2;
/* 45 */       this.tRotation = (this.rotation + MathUtils.random(-30.0F, 30.0F));
/* 46 */       break;
/*    */     default: 
/* 48 */       this.tX = (this.sX + 10.0F * Settings.scale);
/* 49 */       this.tY = (this.sY - 5.0F * Settings.scale);
/* 50 */       this.tRotation = (this.rotation - MathUtils.random(-30.0F, 30.0F));
/* 51 */       this.img = img3;
/*    */     }
/*    */     
/*    */     
/* 55 */     this.renderBehind = false;
/* 56 */     this.color = new Color(0.5F, 0.95F, 1.0F, 0.9F);
/* 57 */     this.scale = (2.0F * Settings.scale);
/* 58 */     this.startingDuration = 0.3F;
/* 59 */     this.duration = this.startingDuration; }
/*    */   
/*    */   private float cY;
/*    */   private float sX;
/*    */   private float sY;
/*    */   
/* 65 */   public void update() { this.color.a = Interpolation.pow2Out.apply(0.2F, 0.9F, this.duration / this.startingDuration);
/* 66 */     if (this.color.a < 0.0F) {
/* 67 */       this.color.a = 0.0F;
/*    */     }
/*    */     
/* 70 */     this.cX = Interpolation.swingIn.apply(this.tX, this.sX, this.duration / this.startingDuration);
/* 71 */     this.cY = Interpolation.swingIn.apply(this.tY, this.sY, this.duration / this.startingDuration);
/* 72 */     this.rotation = Interpolation.swingIn.apply(this.tRotation, 0.0F, this.duration / this.startingDuration);
/* 73 */     this.scale = (Interpolation.fade.apply(2.4F, 2.0F, this.duration / this.startingDuration) * Settings.scale);
/*    */     
/* 75 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 76 */     if (this.duration < 0.0F)
/* 77 */       this.isDone = true;
/*    */   }
/*    */   
/*    */   private float tX;
/*    */   private float tY;
/*    */   private float tRotation;
/* 83 */   public void render(SpriteBatch sb) { sb.setColor(this.color);
/* 84 */     sb.setBlendFunction(770, 1);
/* 85 */     sb.draw(this.img, this.cX - 32.0F, this.cY - 32.0F, 32.0F, 32.0F, 64.0F, 64.0F, this.scale, this.scale, this.rotation, 0, 0, 64, 64, false, false);
/* 86 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\FrostOrbActivateParticle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */