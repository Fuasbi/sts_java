/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ShockWaveEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float x;
/*    */   private float y;
/*    */   private ShockWaveType type;
/*    */   private Color color;
/*    */   
/*    */   public ShockWaveEffect(float x, float y, Color color, ShockWaveType type)
/*    */   {
/* 19 */     this.x = x;
/* 20 */     this.y = y;
/* 21 */     this.type = type;
/* 22 */     this.color = color;
/*    */   }
/*    */   
/*    */   public void update() {
/* 26 */     float speed = MathUtils.random(1000.0F, 1200.0F) * com.megacrit.cardcrawl.core.Settings.scale;
/* 27 */     switch (this.type) {
/*    */     case ADDITIVE: 
/* 29 */       for (int i = 0; i < 40; i++) {
/* 30 */         AbstractDungeon.effectsQueue.add(new BlurWaveAdditiveEffect(this.x, this.y, this.color.cpy(), speed));
/*    */       }
/* 32 */       break;
/*    */     case NORMAL: 
/* 34 */       for (int i = 0; i < 40; i++) {
/* 35 */         AbstractDungeon.effectsQueue.add(new BlurWaveNormalEffect(this.x, this.y, this.color.cpy(), speed));
/*    */       }
/* 37 */       break;
/*    */     case CHAOTIC: 
/* 39 */       com.megacrit.cardcrawl.core.CardCrawlGame.screenShake.shake(com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.HIGH, ScreenShake.ShakeDur.SHORT, false);
/* 40 */       for (int i = 0; i < 40; i++) {
/* 41 */         AbstractDungeon.effectsQueue.add(new BlurWaveChaoticEffect(this.x, this.y, this.color.cpy(), speed));
/*    */       }
/*    */     }
/*    */     
/* 45 */     this.isDone = true; }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */   
/* 49 */   public static enum ShockWaveType { ADDITIVE,  NORMAL,  CHAOTIC;
/*    */     
/*    */     private ShockWaveType() {}
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\ShockWaveEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */