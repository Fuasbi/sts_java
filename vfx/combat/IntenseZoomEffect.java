/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class IntenseZoomEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private boolean isBlack;
/*    */   private float x;
/*    */   private float y;
/*    */   private static final int AMT = 10;
/*    */   
/*    */   public IntenseZoomEffect(float x, float y, boolean isBlack)
/*    */   {
/* 16 */     this.x = x;
/* 17 */     this.y = y;
/* 18 */     this.isBlack = isBlack;
/*    */   }
/*    */   
/*    */   public void update() {
/* 22 */     if (this.isBlack) {
/* 23 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.BorderFlashEffect(com.badlogic.gdx.graphics.Color.BLACK, this.isBlack));
/*    */     } else {
/* 25 */       AbstractDungeon.effectsQueue.add(new com.megacrit.cardcrawl.vfx.BorderFlashEffect(com.megacrit.cardcrawl.core.Settings.GOLD_COLOR, this.isBlack));
/*    */     }
/* 27 */     for (int i = 0; i < 10; i++) {
/* 28 */       AbstractDungeon.effectsQueue.add(new IntenseZoomParticle(this.x, this.y, this.isBlack));
/*    */     }
/* 30 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\IntenseZoomEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */