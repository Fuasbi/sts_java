/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class FlashIntentEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float DURATION = 1.0F;
/*    */   private static final float FLASH_INTERVAL = 0.17F;
/* 13 */   private float intervalTimer = 0.0F;
/*    */   private Texture img;
/*    */   private AbstractMonster m;
/*    */   
/*    */   public FlashIntentEffect(Texture img, AbstractMonster m) {
/* 18 */     this.duration = 1.0F;
/* 19 */     this.img = img;
/* 20 */     this.m = m;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     this.intervalTimer -= Gdx.graphics.getDeltaTime();
/* 26 */     if ((this.intervalTimer < 0.0F) && 
/* 27 */       (!this.m.isDying)) {
/* 28 */       this.intervalTimer = 0.17F;
/* 29 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FlashIntentParticle(this.img, this.m));
/*    */     }
/*    */     
/*    */ 
/* 33 */     this.duration -= Gdx.graphics.getDeltaTime();
/*    */     
/* 35 */     if (this.duration < 0.0F) {
/* 36 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\FlashIntentEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */