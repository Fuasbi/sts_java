/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CardPoofEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   public CardPoofEffect(float x, float y)
/*    */   {
/* 10 */     for (int i = 0; i < 50; i++) {
/* 11 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new CardPoofParticle(x, y));
/*    */     }
/*    */   }
/*    */   
/*    */   public void update() {
/* 16 */     this.isDone = true;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\CardPoofEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */