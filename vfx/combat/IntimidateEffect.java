/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ 
/*    */ public class IntimidateEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float EFFECT_DUR = 1.0F;
/*    */   private float x;
/*    */   private float y;
/*    */   private float vfxTimer;
/*    */   private static final float VFX_INTERVAL = 0.016F;
/*    */   
/*    */   public IntimidateEffect(float newX, float newY)
/*    */   {
/* 15 */     this.duration = 1.0F;
/* 16 */     this.x = newX;
/* 17 */     this.y = newY;
/*    */   }
/*    */   
/*    */   public void update() {
/* 21 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 22 */     this.vfxTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 23 */     if (this.vfxTimer < 0.0F) {
/* 24 */       this.vfxTimer = 0.016F;
/* 25 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new WobblyLineEffect(this.x, this.y, com.megacrit.cardcrawl.core.Settings.CREAM_COLOR.cpy()));
/*    */     }
/* 27 */     if (this.duration < 0.0F) {
/* 28 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\IntimidateEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */