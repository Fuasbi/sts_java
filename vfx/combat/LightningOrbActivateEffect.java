/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Graphics;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LightningOrbActivateEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/* 14 */   public static ArrayList<Texture> img = null;
/* 15 */   private Texture currentImg = null;
/* 16 */   private int index = 0;
/*    */   public static final int W = 157;
/*    */   public static final int H = 45;
/*    */   
/*    */   public LightningOrbActivateEffect(float x, float y) {
/* 21 */     if (img == null) {
/* 22 */       img = new ArrayList();
/* 23 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb1.png"));
/* 24 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb2.png"));
/* 25 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb3.png"));
/* 26 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb4.png"));
/* 27 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb5.png"));
/* 28 */       img.add(ImageMaster.loadImage("images/vfx/defect/l_orb6.png"));
/*    */     }
/*    */     
/* 31 */     this.renderBehind = false;
/* 32 */     this.x = x;
/* 33 */     this.y = y;
/* 34 */     this.color = Settings.LIGHT_YELLOW_COLOR.cpy();
/* 35 */     this.currentImg = ((Texture)img.get(this.index));
/* 36 */     this.scale = (2.0F * Settings.scale);
/* 37 */     this.duration = 0.03F;
/*    */   }
/*    */   
/*    */   private float x;
/*    */   private float y;
/* 42 */   public void update() { this.duration -= Gdx.graphics.getDeltaTime();
/* 43 */     if (this.duration < 0.0F) {
/* 44 */       this.index += 1;
/* 45 */       if (this.index > img.size() - 1) {
/* 46 */         this.isDone = true;
/* 47 */         return;
/*    */       }
/* 49 */       this.currentImg = ((Texture)img.get(this.index));
/*    */       
/* 51 */       this.duration = 0.03F;
/*    */     }
/* 53 */     this.color.a -= Gdx.graphics.getDeltaTime() * 2.0F;
/* 54 */     if (this.color.a < 0.0F) {
/* 55 */       this.color.a = 0.0F;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 61 */     sb.setColor(this.color);
/* 62 */     sb.setBlendFunction(770, 1);
/* 63 */     sb.draw(this.currentImg, this.x - 78.5F, this.y - 22.5F, 78.5F, 22.5F, 157.0F, 45.0F, this.scale, this.scale, this.rotation, 0, 0, 157, 45, false, false);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 80 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\LightningOrbActivateEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */