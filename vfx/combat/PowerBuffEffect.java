/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Interpolation;
/*    */ import com.badlogic.gdx.math.Interpolation.PowIn;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class PowerBuffEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private static final float TEXT_DURATION = 2.0F;
/* 15 */   private static final float STARTING_OFFSET_Y = 60.0F * Settings.scale;
/* 16 */   private static final float TARGET_OFFSET_Y = 100.0F * Settings.scale;
/*    */   private float x;
/*    */   private float y;
/*    */   private float offsetY;
/*    */   private String msg;
/*    */   private Color targetColor;
/*    */   
/* 23 */   public PowerBuffEffect(float x, float y, String msg) { this.duration = 2.0F;
/* 24 */     this.startingDuration = 2.0F;
/* 25 */     this.msg = msg;
/* 26 */     this.x = x;
/* 27 */     this.y = y;
/* 28 */     this.targetColor = Settings.GREEN_TEXT_COLOR;
/* 29 */     this.color = Color.WHITE.cpy();
/* 30 */     this.offsetY = STARTING_OFFSET_Y;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 35 */     if (this.duration == this.startingDuration) {
/* 36 */       for (int i = 0; i < 12; i++) {
/* 37 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FlyingSpikeEffect(this.x - 
/*    */         
/* 39 */           MathUtils.random(-120.0F, 120.0F) * Settings.scale, this.y + 
/* 40 */           MathUtils.random(90.0F, 110.0F) * Settings.scale, -90.0F, 0.0F, 
/*    */           
/*    */ 
/* 43 */           MathUtils.random(-200.0F, -50.0F) * Settings.scale, Settings.GREEN_TEXT_COLOR));
/*    */       }
/*    */       
/* 46 */       for (int i = 0; i < 12; i++) {
/* 47 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectsQueue.add(new FlyingSpikeEffect(this.x - 
/*    */         
/* 49 */           MathUtils.random(-120.0F, 120.0F) * Settings.scale, this.y + 
/* 50 */           MathUtils.random(90.0F, 110.0F) * Settings.scale, 90.0F, 0.0F, 
/*    */           
/*    */ 
/* 53 */           MathUtils.random(200.0F, 50.0F) * Settings.scale, Settings.GREEN_TEXT_COLOR));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 58 */     this.offsetY = Interpolation.exp10In.apply(TARGET_OFFSET_Y, STARTING_OFFSET_Y, this.duration / 2.0F);
/* 59 */     this.color.r = Interpolation.pow2In.apply(this.targetColor.r, 1.0F, this.duration / this.startingDuration);
/* 60 */     this.color.g = Interpolation.pow2In.apply(this.targetColor.g, 1.0F, this.duration / this.startingDuration);
/* 61 */     this.color.b = Interpolation.pow2In.apply(this.targetColor.b, 1.0F, this.duration / this.startingDuration);
/* 62 */     this.color.a = Interpolation.exp10Out.apply(0.0F, 1.0F, this.duration / 2.0F);
/*    */     
/*    */ 
/* 65 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 66 */     if (this.duration < 0.0F) {
/* 67 */       this.isDone = true;
/* 68 */       this.duration = 0.0F;
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 74 */     FontHelper.renderFontCentered(sb, FontHelper.applyPowerFont, this.msg, this.x, this.y + this.offsetY, this.color);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\PowerBuffEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */