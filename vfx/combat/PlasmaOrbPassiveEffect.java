/*    */ package com.megacrit.cardcrawl.vfx.combat;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ 
/*    */ public class PlasmaOrbPassiveEffect extends com.megacrit.cardcrawl.vfx.AbstractGameEffect
/*    */ {
/*    */   private float effectDuration;
/*    */   private float x;
/*    */   private float y;
/*    */   private float sX;
/*    */   private float sY;
/*    */   private float tX;
/*    */   private float tY;
/* 16 */   private TextureAtlas.AtlasRegion img = com.megacrit.cardcrawl.helpers.ImageMaster.GLOW_SPARK_2;
/*    */   
/*    */   public PlasmaOrbPassiveEffect(float x, float y) {
/* 19 */     this.effectDuration = 1.0F;
/* 20 */     this.duration = this.effectDuration;
/* 21 */     this.startingDuration = this.effectDuration;
/*    */     
/* 23 */     this.x = (x + MathUtils.random(-30.0F, 30.0F) * com.megacrit.cardcrawl.core.Settings.scale);
/* 24 */     this.y = (y + MathUtils.random(-30.0F, 30.0F) * com.megacrit.cardcrawl.core.Settings.scale);
/* 25 */     this.sX = this.x;
/* 26 */     this.sY = this.y;
/* 27 */     this.tX = x;
/* 28 */     this.tY = y;
/*    */     
/* 30 */     int tmp = MathUtils.random(2);
/* 31 */     if (tmp == 0) {
/* 32 */       this.color = com.megacrit.cardcrawl.core.Settings.LIGHT_YELLOW_COLOR.cpy();
/* 33 */     } else if (tmp == 1) {
/* 34 */       this.color = com.badlogic.gdx.graphics.Color.CYAN.cpy();
/*    */     } else {
/* 36 */       this.color = com.badlogic.gdx.graphics.Color.SALMON.cpy();
/*    */     }
/*    */     
/* 39 */     this.scale = (MathUtils.random(0.3F, 1.2F) * com.megacrit.cardcrawl.core.Settings.scale);
/* 40 */     this.renderBehind = true;
/*    */   }
/*    */   
/*    */   public void update() {
/* 44 */     this.x = com.badlogic.gdx.math.Interpolation.swingOut.apply(this.tX, this.sX, this.duration);
/* 45 */     this.y = com.badlogic.gdx.math.Interpolation.swingOut.apply(this.tY, this.sY, this.duration);
/*    */     
/* 47 */     super.update();
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 52 */     sb.setColor(this.color);
/* 53 */     sb.setBlendFunction(770, 1);
/* 54 */     sb.draw(this.img, this.x - this.img.packedWidth / 2.0F, this.y - this.img.packedWidth / 2.0F, this.img.packedWidth / 2.0F, this.img.packedHeight / 2.0F, this.img.packedWidth, this.img.packedHeight, this.scale * 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 62 */       MathUtils.random(0.7F, 1.4F), this.scale * 
/* 63 */       MathUtils.random(0.7F, 1.4F), this.rotation);
/*    */     
/* 65 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\vfx\combat\PlasmaOrbPassiveEffect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */