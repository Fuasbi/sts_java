/*     */ package com.megacrit.cardcrawl.actions.unique;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class PoisonLoseHpAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*  27 */   private static final Logger logger = LogManager.getLogger(PoisonLoseHpAction.class.getName());
/*     */   private static final float DURATION = 0.33F;
/*     */   
/*     */   public PoisonLoseHpAction(AbstractCreature target, AbstractCreature source, int amount, AbstractGameAction.AttackEffect effect) {
/*  31 */     setValues(target, source, amount);
/*  32 */     this.actionType = AbstractGameAction.ActionType.DAMAGE;
/*  33 */     this.attackEffect = effect;
/*  34 */     this.duration = 0.33F;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  39 */     if (AbstractDungeon.getCurrRoom().phase != AbstractRoom.RoomPhase.COMBAT) {
/*  40 */       this.isDone = true;
/*  41 */       return;
/*     */     }
/*     */     
/*  44 */     if ((this.duration == 0.33F) && (this.target.currentHealth > 0)) {
/*  45 */       logger.info(this.target.name + " HAS " + this.target.currentHealth + " HP.");
/*  46 */       this.target.damageFlash = true;
/*  47 */       this.target.damageFlashFrames = 4;
/*  48 */       AbstractDungeon.effectList.add(new FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*     */     }
/*     */     
/*     */ 
/*  52 */     tickDuration();
/*     */     
/*  54 */     if (this.isDone)
/*     */     {
/*  56 */       if ((AbstractDungeon.player.hasRelic("The Specimen")) && (!this.target.isPlayer) && (this.target.currentHealth <= this.amount) && 
/*  57 */         (this.target.getPower("Poison") != null)) {
/*  58 */         int potency = this.target.getPower("Poison").amount;
/*  59 */         if ((potency == 1) && (!AbstractDungeon.player.hasPower("Venomology"))) {
/*  60 */           this.target.powers.remove(this.target.getPower("Poison"));
/*  61 */         } else if ((AbstractDungeon.getRandomMonster((AbstractMonster)this.target) != null) && 
/*  62 */           (!AbstractDungeon.player.hasPower("Venomology"))) {
/*  63 */           this.target.getPower("Poison").amount -= 1;
/*     */         }
/*  65 */         else if ((AbstractDungeon.getRandomMonster((AbstractMonster)this.target) != null) && 
/*  66 */           (AbstractDungeon.player.hasPower("Venomology"))) {
/*  67 */           this.target.getPower("Poison").amount += AbstractDungeon.player.getPower("Venomology").amount;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  74 */       if (this.target.currentHealth > 0) {
/*  75 */         this.target.tint.color = Color.CHARTREUSE.cpy();
/*  76 */         this.target.tint.changeColor(Color.WHITE.cpy());
/*  77 */         this.target.damage(new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.amount, DamageInfo.DamageType.HP_LOSS));
/*     */         
/*     */ 
/*  80 */         if (this.target.isDying) {
/*  81 */           AbstractPlayer.poisonKillCount += 1;
/*  82 */           if ((AbstractPlayer.poisonKillCount == 3) && (AbstractDungeon.player.chosenClass == AbstractPlayer.PlayerClass.THE_SILENT))
/*     */           {
/*  84 */             UnlockTracker.unlockAchievement("PLAGUE");
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*  89 */       if ((!AbstractDungeon.player.hasPower("Venomology")) || (this.target.isPlayer)) {
/*  90 */         AbstractPower p = this.target.getPower("Poison");
/*  91 */         if (p != null) {
/*  92 */           p.amount -= 1;
/*  93 */           if (p.amount == 0) {
/*  94 */             this.target.powers.remove(p);
/*     */           } else {
/*  96 */             p.updateDescription();
/*     */           }
/*     */         }
/*  99 */       } else if ((AbstractDungeon.player.hasPower("Venomology")) && (!this.target.isPlayer)) {
/* 100 */         AbstractPower p = this.target.getPower("Poison");
/* 101 */         if (p != null) {
/* 102 */           p.amount += AbstractDungeon.player.getPower("Venomology").amount;
/*     */         }
/*     */       }
/*     */       
/* 106 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 107 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*     */       }
/* 109 */       AbstractDungeon.actionManager.addToTop(new WaitAction(0.1F));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\PoisonLoseHpAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */