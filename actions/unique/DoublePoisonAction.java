/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ import com.megacrit.cardcrawl.powers.PoisonPower;
/*    */ 
/*    */ public class DoublePoisonAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private float startingDuration;
/*    */   
/*    */   public DoublePoisonAction(AbstractCreature target, AbstractCreature source)
/*    */   {
/* 14 */     this.target = target;
/* 15 */     this.source = source;
/* 16 */     this.startingDuration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 17 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DEBUFF;
/* 18 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE;
/* 19 */     this.duration = this.startingDuration;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 24 */     if ((this.duration == this.startingDuration) && 
/* 25 */       (this.target != null) && (this.target.hasPower("Poison"))) {
/* 26 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.target, this.source, new PoisonPower(this.target, this.source, 
/*    */       
/*    */ 
/*    */ 
/* 30 */         this.target.getPower("Poison").amount), 
/* 31 */         this.target.getPower("Poison").amount));
/*    */     }
/*    */     
/*    */ 
/* 35 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DoublePoisonAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */