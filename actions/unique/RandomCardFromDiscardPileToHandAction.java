/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class RandomCardFromDiscardPileToHandAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public RandomCardFromDiscardPileToHandAction()
/*    */   {
/* 12 */     this.p = com.megacrit.cardcrawl.dungeons.AbstractDungeon.player;
/* 13 */     setValues(this.p, com.megacrit.cardcrawl.dungeons.AbstractDungeon.player, this.amount);
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (this.p.discardPile.size() > 0) {
/* 20 */       com.megacrit.cardcrawl.cards.AbstractCard card = this.p.discardPile.getRandomCard(com.megacrit.cardcrawl.dungeons.AbstractDungeon.cardRandomRng);
/* 21 */       this.p.hand.addToHand(card);
/* 22 */       card.lighten(false);
/* 23 */       this.p.discardPile.removeCard(card);
/* 24 */       this.p.hand.refreshHandLayout();
/*    */     }
/* 26 */     tickDuration();
/* 27 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RandomCardFromDiscardPileToHandAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */