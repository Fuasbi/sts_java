/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MindBlastAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public MindBlastAction(AbstractCreature target)
/*    */   {
/* 13 */     setValues(target, AbstractDungeon.player);
/* 14 */     this.duration = Settings.ACTION_DUR_FAST;
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if ((this.duration == Settings.ACTION_DUR_FAST) && 
/* 21 */       (this.target != null)) {
/* 22 */       DamageInfo info = new DamageInfo(this.source, AbstractDungeon.player.drawPile.size());
/* 23 */       info.applyPowers(this.source, this.target);
/* 24 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*    */     }
/*    */     
/*    */ 
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\MindBlastAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */