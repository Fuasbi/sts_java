/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class LimitBreakAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public LimitBreakAction()
/*    */   {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 15 */     this.p = AbstractDungeon.player;
/* 16 */     this.duration = Settings.ACTION_DUR_XFAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if ((this.duration == Settings.ACTION_DUR_XFAST) && 
/* 22 */       (this.p.hasPower("Strength"))) {
/* 23 */       int strAmt = this.p.getPower("Strength").amount;
/* 24 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.p, this.p, new com.megacrit.cardcrawl.powers.StrengthPower(this.p, strAmt), strAmt));
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 29 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\LimitBreakAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */