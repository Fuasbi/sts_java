/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ 
/*    */ public class DeckToHandAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public DeckToHandAction(int amount)
/*    */   {
/* 13 */     this.p = AbstractDungeon.player;
/* 14 */     setValues(this.p, AbstractDungeon.player, amount);
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 16 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_MED;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_MED) {
/* 22 */       AbstractDungeon.gridSelectScreen.open(this.p.drawPile, this.amount, "Select a card to add to your hand.", false);
/* 23 */       tickDuration();
/* 24 */       return;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 29 */     if (AbstractDungeon.gridSelectScreen.selectedCards.size() != 0) {
/* 30 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.gridSelectScreen.selectedCards) {
/* 31 */         this.p.hand.addToHand(c);
/* 32 */         this.p.drawPile.removeCard(c);
/* 33 */         c.unhover();
/*    */       }
/* 35 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 36 */       this.p.hand.refreshHandLayout();
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 45 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DeckToHandAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */