/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.CardRewardScreen;
/*    */ 
/*    */ public class CodexAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public static int numPlaced;
/* 11 */   private boolean retrieveCard = false;
/*    */   
/*    */   public CodexAction() {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 15 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 21 */       AbstractDungeon.cardRewardScreen.codexOpen();
/* 22 */       tickDuration();
/* 23 */       return;
/*    */     }
/*    */     
/* 26 */     if (!this.retrieveCard) {
/* 27 */       if (AbstractDungeon.cardRewardScreen.codexCard != null) {
/* 28 */         AbstractCard codexCard = AbstractDungeon.cardRewardScreen.codexCard.makeStatEquivalentCopy();
/* 29 */         codexCard.current_x = (-1000.0F * Settings.scale);
/* 30 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDrawPileEffect(codexCard, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, true));
/*    */         
/* 32 */         AbstractDungeon.cardRewardScreen.codexCard = null;
/*    */       }
/* 34 */       this.retrieveCard = true;
/*    */     }
/*    */     
/* 37 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\CodexAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */