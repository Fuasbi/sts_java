/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class MetallicizeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private float startingDuration;
/*    */   
/*    */   public MetallicizeAction(AbstractCreature target, int amount)
/*    */   {
/* 14 */     this.target = target;
/* 15 */     this.amount = amount;
/* 16 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 17 */     this.startingDuration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 18 */     this.duration = this.startingDuration;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == this.startingDuration) {
/* 24 */       int count = AbstractDungeon.player.hand.size();
/*    */       
/* 26 */       for (int i = 0; i < count; i++) {
/* 27 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this.target, this.target, this.amount));
/*    */       }
/* 29 */       for (int i = 0; i < count; i++) {
/* 30 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustAction(AbstractDungeon.player, AbstractDungeon.player, 1, true, true));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 35 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\MetallicizeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */