/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class MadnessAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public MadnessAction()
/*    */   {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 15 */     this.p = com.megacrit.cardcrawl.dungeons.AbstractDungeon.player;
/* 16 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 22 */       boolean betterPossible = false;
/* 23 */       boolean possible = false;
/* 24 */       for (AbstractCard c : this.p.hand.group) {
/* 25 */         if (c.costForTurn > 0) {
/* 26 */           betterPossible = true;
/* 27 */         } else if (c.cost > 0) {
/* 28 */           possible = true;
/*    */         }
/*    */       }
/* 31 */       if ((betterPossible) || (possible)) {
/* 32 */         findAndModifyCard(betterPossible);
/*    */       }
/*    */     }
/*    */     
/* 36 */     tickDuration();
/*    */   }
/*    */   
/*    */   private void findAndModifyCard(boolean better) {
/* 40 */     AbstractCard c = this.p.hand.getRandomCard(false);
/* 41 */     if (better) {
/* 42 */       if (c.costForTurn > 0)
/*    */       {
/* 44 */         c.cost = 0;
/* 45 */         c.costForTurn = 0;
/* 46 */         c.isCostModified = true;
/* 47 */         c.superFlash(Color.GOLD.cpy());
/*    */       }
/*    */       else {
/* 50 */         findAndModifyCard(better);
/*    */       }
/*    */     }
/* 53 */     else if (c.cost > 0)
/*    */     {
/* 55 */       c.cost = 0;
/* 56 */       c.costForTurn = 0;
/* 57 */       c.isCostModified = true;
/* 58 */       c.superFlash(Color.GOLD.cpy());
/*    */     }
/*    */     else {
/* 61 */       findAndModifyCard(better);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\MadnessAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */