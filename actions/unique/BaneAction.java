/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class BaneAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.01F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*    */   private AbstractMonster m;
/*    */   
/*    */   public BaneAction(AbstractMonster target, DamageInfo info)
/*    */   {
/* 19 */     this.info = info;
/* 20 */     setValues(target, info);
/* 21 */     this.m = target;
/* 22 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 23 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_VERTICAL;
/* 24 */     this.duration = 0.01F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 29 */     if (this.target == null) {
/* 30 */       this.isDone = true;
/* 31 */       return;
/*    */     }
/* 33 */     if (this.m.hasPower("Poison")) {
/* 34 */       if ((this.duration == 0.01F) && (this.target != null) && (this.target.currentHealth > 0))
/*    */       {
/* 36 */         if ((this.info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && 
/* 37 */           (this.info.owner.isDying)) {
/* 38 */           this.isDone = true;
/* 39 */           return;
/*    */         }
/*    */         
/* 42 */         this.target.damageFlash = true;
/* 43 */         this.target.damageFlashFrames = 4;
/* 44 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*    */       }
/*    */       
/* 47 */       tickDuration();
/*    */       
/* 49 */       if ((this.isDone) && (this.target != null) && (this.target.currentHealth > 0)) {
/* 50 */         this.target.damage(this.info);
/*    */         
/* 52 */         if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 53 */           AbstractDungeon.actionManager.clearPostCombatActions();
/*    */         }
/* 55 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */       }
/*    */     }
/*    */     else {
/* 59 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\BaneAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */