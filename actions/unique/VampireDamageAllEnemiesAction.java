/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class VampireDamageAllEnemiesAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public int[] damage;
/*    */   
/*    */   public VampireDamageAllEnemiesAction(AbstractCreature source, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect effect)
/*    */   {
/* 18 */     setValues(null, source, amount[0]);
/* 19 */     this.damage = amount;
/* 20 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 21 */     this.damageType = type;
/* 22 */     this.attackEffect = effect;
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     boolean playedMusic;
/* 27 */     if (this.duration == 0.5F) {
/* 28 */       playedMusic = false;
/* 29 */       int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/* 30 */       for (int i = 0; i < temp; i++) {
/* 31 */         if ((!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDying) && 
/* 32 */           (((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).currentHealth > 0) && 
/* 33 */           (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isEscaping)) {
/* 34 */           if (playedMusic) {
/* 35 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*    */             
/* 37 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/* 38 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect, true));
/*    */           }
/*    */           else
/*    */           {
/* 42 */             playedMusic = true;
/* 43 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*    */             
/* 45 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/* 46 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect));
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 53 */     tickDuration();
/*    */     
/* 55 */     if (this.isDone) {
/* 56 */       heal();
/* 57 */       for (com.megacrit.cardcrawl.powers.AbstractPower p : AbstractDungeon.player.powers) {
/* 58 */         p.onDamageAllEnemies(this.damage);
/*    */       }
/*    */       
/* 61 */       for (int i = 0; i < AbstractDungeon.getCurrRoom().monsters.monsters.size(); i++) {
/* 62 */         AbstractMonster target = (AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i);
/* 63 */         if ((!target.isDying) && (target.currentHealth > 0) && (!target.isEscaping)) {
/* 64 */           target.damage(new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.damage[i], this.damageType));
/*    */         }
/*    */       }
/*    */       
/* 68 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 69 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/* 71 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */     }
/*    */   }
/*    */   
/*    */   private void heal() {
/* 76 */     int healAmount = 0;
/* 77 */     AbstractMonster[] monsters = new AbstractMonster[AbstractDungeon.getCurrRoom().monsters.monsters.size()];
/* 78 */     AbstractDungeon.getCurrRoom().monsters.monsters.toArray(monsters);
/*    */     
/* 80 */     for (int i = 0; i < monsters.length; i++) {
/* 81 */       if ((monsters[i] != null) && (!monsters[i].isDying) && (!monsters[i].isEscaping))
/*    */       {
/*    */ 
/*    */ 
/* 85 */         int tmp = this.damage[i];
/* 86 */         tmp -= monsters[i].currentBlock;
/*    */         
/* 88 */         if (tmp > monsters[i].currentHealth) {
/* 89 */           tmp = monsters[i].currentHealth;
/*    */         }
/*    */         
/* 92 */         if (tmp > 0) {
/* 93 */           healAmount += tmp;
/*    */         }
/*    */       }
/*    */     }
/* 97 */     if (healAmount > 0) {
/* 98 */       this.source.heal(healAmount);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\VampireDamageAllEnemiesAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */