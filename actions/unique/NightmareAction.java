/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.powers.NightmarePower;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ 
/*    */ public class NightmareAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 15 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("CopyAction");
/* 16 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   private AbstractPlayer p;
/*    */   public static int numDiscarded;
/* 20 */   private static final float DURATION = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*    */   
/*    */   public NightmareAction(AbstractCreature target, AbstractCreature source, int amount) {
/* 23 */     setValues(target, source, amount);
/* 24 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 25 */     this.duration = DURATION;
/* 26 */     this.p = ((AbstractPlayer)target);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 31 */     if (this.duration == DURATION) {
/* 32 */       if (this.p.hand.isEmpty()) {
/* 33 */         this.isDone = true;
/* 34 */         return; }
/* 35 */       if (this.p.hand.size() == 1) {
/* 36 */         AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.p, this.p, new NightmarePower(this.p, this.amount, this.p.hand
/* 37 */           .getBottomCard())));
/* 38 */         this.isDone = true;
/* 39 */         return;
/*    */       }
/* 41 */       AbstractDungeon.handCardSelectScreen.open(TEXT[0], 1, false, false);
/* 42 */       tickDuration();
/* 43 */       return;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 48 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
/* 49 */       com.megacrit.cardcrawl.cards.AbstractCard tmpCard = AbstractDungeon.handCardSelectScreen.selectedCards.getBottomCard();
/* 50 */       AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.p, this.p, new NightmarePower(this.p, this.amount, tmpCard)));
/* 51 */       AbstractDungeon.player.hand.addToHand(tmpCard);
/*    */       
/* 53 */       AbstractDungeon.handCardSelectScreen.selectedCards.clear();
/* 54 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*    */     }
/*    */     
/* 57 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\NightmareAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */