/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class LoseEnergyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int energyLoss;
/*    */   
/*    */   public LoseEnergyAction(int amount)
/*    */   {
/* 11 */     setValues(AbstractDungeon.player, AbstractDungeon.player, 0);
/* 12 */     this.energyLoss = amount;
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 19 */       AbstractDungeon.player.loseEnergy(this.energyLoss);
/*    */     }
/*    */     
/* 22 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\LoseEnergyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */