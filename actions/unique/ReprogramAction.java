/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ReprogramAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("ReprogramAction");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private float startingDuration;
/*    */   
/*    */   public ReprogramAction(int numCards) {
/* 18 */     this.amount = numCards;
/* 19 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 20 */     this.startingDuration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 21 */     this.duration = this.startingDuration;
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     CardGroup tmpGroup;
/* 26 */     if (this.duration == this.startingDuration) {
/* 27 */       if (AbstractDungeon.player.drawPile.isEmpty()) {
/* 28 */         this.isDone = true;
/* 29 */         return;
/*    */       }
/* 31 */       tmpGroup = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/* 32 */       for (int i = 0; i < Math.min(this.amount, AbstractDungeon.player.drawPile.size()); i++) {
/* 33 */         tmpGroup.addToTop(
/* 34 */           (AbstractCard)AbstractDungeon.player.drawPile.group.get(AbstractDungeon.player.drawPile.size() - i - 1));
/*    */       }
/*    */       
/* 37 */       AbstractDungeon.gridSelectScreen.open(tmpGroup, this.amount, true, TEXT[0]);
/*    */     }
/* 39 */     else if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/* 40 */       for (AbstractCard c : AbstractDungeon.gridSelectScreen.selectedCards) {
/* 41 */         AbstractDungeon.player.drawPile.moveToDiscardPile(c);
/*    */       }
/* 43 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*    */     }
/* 45 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ReprogramAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */