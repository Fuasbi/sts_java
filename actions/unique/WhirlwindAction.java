/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ import com.megacrit.cardcrawl.vfx.combat.CleaveEffect;
/*    */ 
/*    */ public class WhirlwindAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public int[] multiDamage;
/* 17 */   private boolean freeToPlayOnce = false;
/*    */   private DamageInfo.DamageType damageType;
/*    */   private AbstractPlayer p;
/* 20 */   private int energyOnUse = -1;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public WhirlwindAction(AbstractPlayer p, int[] multiDamage, DamageInfo.DamageType damageType, boolean freeToPlayOnce, int energyOnUse)
/*    */   {
/* 28 */     this.multiDamage = multiDamage;
/* 29 */     this.damageType = damageType;
/* 30 */     this.p = p;
/* 31 */     this.freeToPlayOnce = freeToPlayOnce;
/* 32 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 33 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 34 */     this.energyOnUse = energyOnUse;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 39 */     int effect = EnergyPanel.totalCount;
/* 40 */     if (this.energyOnUse != -1) {
/* 41 */       effect = this.energyOnUse;
/*    */     }
/*    */     
/* 44 */     if (this.p.hasRelic("Chemical X")) {
/* 45 */       effect += 2;
/* 46 */       this.p.getRelic("Chemical X").flash();
/*    */     }
/*    */     
/* 49 */     if (effect > 0) {
/* 50 */       for (int i = 0; i < effect; i++) {
/* 51 */         if (i == 0) {
/* 52 */           AbstractDungeon.actionManager.addToBottom(new SFXAction("ATTACK_WHIRLWIND"));
/*    */         }
/*    */         
/* 55 */         AbstractDungeon.actionManager.addToBottom(new SFXAction("ATTACK_HEAVY"));
/* 56 */         AbstractDungeon.actionManager.addToBottom(new VFXAction(this.p, new CleaveEffect(), 0.0F));
/* 57 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(this.p, this.multiDamage, this.damageType, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE, true));
/*    */       }
/*    */       
/*    */ 
/* 61 */       if (!this.freeToPlayOnce) {
/* 62 */         this.p.energy.use(EnergyPanel.totalCount);
/*    */       }
/*    */     }
/* 65 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\WhirlwindAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */