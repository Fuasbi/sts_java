/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class TumbleAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public TumbleAction(AbstractCreature source, int amount)
/*    */   {
/* 15 */     this.source = source;
/* 16 */     this.amount = amount;
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 23 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 24 */         if (c.type == AbstractCard.CardType.ATTACK) {
/* 25 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this.source, this.source, this.amount));
/* 26 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DiscardSpecificCardAction(c));
/*    */         }
/*    */       }
/*    */     }
/* 30 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\TumbleAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */