/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class MalaiseAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 15 */   private boolean freeToPlayOnce = false; private boolean upgraded = false;
/*    */   
/*    */ 
/* 18 */   private int energyOnUse = -1;
/*    */   
/*    */   private AbstractPlayer p;
/*    */   
/*    */   private AbstractMonster m;
/*    */   
/*    */   public MalaiseAction(AbstractPlayer p, AbstractMonster m, boolean upgraded, boolean freeToPlayOnce, int energyOnUse)
/*    */   {
/* 26 */     this.p = p;
/* 27 */     this.m = m;
/* 28 */     this.freeToPlayOnce = freeToPlayOnce;
/* 29 */     this.upgraded = upgraded;
/* 30 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 31 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 32 */     this.energyOnUse = energyOnUse;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 37 */     int effect = EnergyPanel.totalCount;
/* 38 */     if (this.energyOnUse != -1) {
/* 39 */       effect = this.energyOnUse;
/*    */     }
/*    */     
/* 42 */     if (this.p.hasRelic("Chemical X")) {
/* 43 */       effect += 2;
/* 44 */       this.p.getRelic("Chemical X").flash();
/*    */     }
/*    */     
/* 47 */     if (this.upgraded) {
/* 48 */       effect++;
/*    */     }
/*    */     
/* 51 */     if (effect > 0) {
/* 52 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.p, new StrengthPower(this.m, -effect), -effect));
/*    */       
/* 54 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.p, new com.megacrit.cardcrawl.powers.WeakPower(this.m, effect, false), effect));
/*    */       
/*    */ 
/* 57 */       if (!this.freeToPlayOnce) {
/* 58 */         this.p.energy.use(EnergyPanel.totalCount);
/*    */       }
/*    */     }
/* 61 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\MalaiseAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */