/*     */ package com.megacrit.cardcrawl.actions.unique;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class ImmolateAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*  17 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("ImmolateAction");
/*  18 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   public int[] damage;
/*     */   
/*     */   public ImmolateAction(com.megacrit.cardcrawl.core.AbstractCreature source, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type)
/*     */   {
/*  23 */     setValues(null, source, amount[0]);
/*  24 */     this.damage = amount;
/*  25 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*  26 */     this.damageType = type;
/*  27 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE;
/*  28 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*     */     AbstractCard card;
/*  34 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST)
/*     */     {
/*     */ 
/*  37 */       if (AbstractDungeon.player.hand.size() == 0) {
/*  38 */         this.isDone = true;
/*  39 */         return;
/*     */       }
/*     */       
/*  42 */       if (AbstractDungeon.player.hand.size() == 1) {
/*  43 */         card = AbstractDungeon.player.hand.getBottomCard();
/*  44 */         if ((card.type == AbstractCard.CardType.CURSE) || (card.type == AbstractCard.CardType.STATUS)) {
/*  45 */           dealDamage();
/*     */         }
/*  47 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(card, AbstractDungeon.player.hand));
/*     */         
/*  49 */         this.isDone = true;
/*  50 */         return;
/*     */       }
/*     */       
/*  53 */       AbstractDungeon.handCardSelectScreen.open(TEXT[0], 1, false);
/*  54 */       tickDuration();
/*  55 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  61 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
/*  62 */       for (AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/*  63 */         if ((c.type == AbstractCard.CardType.CURSE) || (c.type == AbstractCard.CardType.STATUS)) {
/*  64 */           dealDamage();
/*     */         }
/*  66 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(c, AbstractDungeon.handCardSelectScreen.selectedCards));
/*     */       }
/*     */       
/*  69 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*     */     }
/*     */     
/*  72 */     tickDuration();
/*     */   }
/*     */   
/*     */   public void dealDamage()
/*     */   {
/*  77 */     boolean playedMusic = false;
/*  78 */     int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/*  79 */     for (int i = 0; i < temp; i++) {
/*  80 */       if ((!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDying) && 
/*  81 */         (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isEscaping)) {
/*  82 */         if (playedMusic) {
/*  83 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*     */           
/*  85 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/*  86 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect, true));
/*     */         }
/*     */         else
/*     */         {
/*  90 */           playedMusic = true;
/*  91 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*     */           
/*  93 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/*  94 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect));
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 100 */     for (com.megacrit.cardcrawl.powers.AbstractPower p : AbstractDungeon.player.powers) {
/* 101 */       p.onDamageAllEnemies(this.damage);
/*     */     }
/*     */     
/* 104 */     int temp2 = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/* 105 */     for (int i = 0; i < temp2; i++) {
/* 106 */       if ((!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDying) && 
/* 107 */         (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isEscaping)) {
/* 108 */         ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.color = Color.RED.cpy();
/* 109 */         ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.changeColor(Color.WHITE.cpy());
/* 110 */         ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).damage(new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.damage[i], this.damageType));
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 115 */     if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 116 */       AbstractDungeon.actionManager.clearPostCombatActions();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ImmolateAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */