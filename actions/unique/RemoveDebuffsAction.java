/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ 
/*    */ public class RemoveDebuffsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCreature c;
/*    */   
/*    */   public RemoveDebuffsAction(AbstractCreature c)
/*    */   {
/* 14 */     this.c = c;
/* 15 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     for (AbstractPower p : this.c.powers) {
/* 21 */       if (p.type == com.megacrit.cardcrawl.powers.AbstractPower.PowerType.DEBUFF) {
/* 22 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.c, this.c, p.ID));
/*    */       }
/*    */     }
/*    */     
/* 26 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RemoveDebuffsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */