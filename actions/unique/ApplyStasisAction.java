/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ApplyStasisAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCreature owner;
/*    */   private float startingDuration;
/* 15 */   private AbstractCard card = null;
/*    */   
/*    */   public ApplyStasisAction(AbstractCreature owner) {
/* 18 */     this.owner = owner;
/* 19 */     this.duration = Settings.ACTION_DUR_LONG;
/* 20 */     this.startingDuration = Settings.ACTION_DUR_LONG;
/* 21 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if ((AbstractDungeon.player.drawPile.isEmpty()) && (AbstractDungeon.player.discardPile.isEmpty())) {
/* 27 */       this.isDone = true;
/* 28 */       return;
/*    */     }
/*    */     
/* 31 */     if (this.duration == this.startingDuration) {
/* 32 */       if (AbstractDungeon.player.drawPile.isEmpty()) {
/* 33 */         this.card = AbstractDungeon.player.discardPile.getRandomCard(false, AbstractCard.CardRarity.RARE);
/* 34 */         if (this.card == null) {
/* 35 */           this.card = AbstractDungeon.player.discardPile.getRandomCard(false, AbstractCard.CardRarity.UNCOMMON);
/* 36 */           if (this.card == null) {
/* 37 */             this.card = AbstractDungeon.player.discardPile.getRandomCard(false, AbstractCard.CardRarity.COMMON);
/* 38 */             if (this.card == null) {
/* 39 */               this.card = AbstractDungeon.player.discardPile.getRandomCard(false);
/*    */             }
/*    */           }
/*    */         }
/* 43 */         AbstractDungeon.player.discardPile.removeCard(this.card);
/*    */       } else {
/* 45 */         this.card = AbstractDungeon.player.drawPile.getRandomCard(false, AbstractCard.CardRarity.RARE);
/* 46 */         if (this.card == null) {
/* 47 */           this.card = AbstractDungeon.player.drawPile.getRandomCard(false, AbstractCard.CardRarity.UNCOMMON);
/* 48 */           if (this.card == null) {
/* 49 */             this.card = AbstractDungeon.player.drawPile.getRandomCard(false, AbstractCard.CardRarity.COMMON);
/* 50 */             if (this.card == null) {
/* 51 */               this.card = AbstractDungeon.player.drawPile.getRandomCard(false);
/*    */             }
/*    */           }
/*    */         }
/* 55 */         AbstractDungeon.player.drawPile.removeCard(this.card);
/*    */       }
/*    */       
/* 58 */       AbstractDungeon.player.limbo.addToBottom(this.card);
/* 59 */       this.card.setAngle(0.0F);
/* 60 */       this.card.targetDrawScale = 0.75F;
/* 61 */       this.card.target_x = (Settings.WIDTH / 2.0F);
/* 62 */       this.card.target_y = (Settings.HEIGHT / 2.0F);
/* 63 */       this.card.lighten(false);
/* 64 */       this.card.unfadeOut();
/* 65 */       this.card.unhover();
/* 66 */       this.card.untip();
/* 67 */       this.card.stopGlowing();
/*    */     }
/*    */     
/* 70 */     tickDuration();
/* 71 */     if ((this.isDone) && (this.card != null)) {
/* 72 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.owner, this.owner, new com.megacrit.cardcrawl.powers.StasisPower(this.owner, this.card)));
/* 73 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.ShowCardAction(this.card));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ApplyStasisAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */