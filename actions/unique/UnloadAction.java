/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class UnloadAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public UnloadAction(AbstractCreature source)
/*    */   {
/* 14 */     this.source = source;
/* 15 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 21 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 22 */         if (c.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 23 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DiscardSpecificCardAction(c));
/*    */         }
/*    */       }
/* 26 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\UnloadAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */