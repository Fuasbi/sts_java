/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class PatientMissileAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public PatientMissileAction(AbstractCreature target)
/*    */   {
/* 13 */     setValues(target, AbstractDungeon.player);
/* 14 */     this.duration = Settings.ACTION_DUR_FAST;
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 21 */       DamageInfo info = new DamageInfo(this.source, AbstractDungeon.player.discardPile.size());
/* 22 */       info.applyPowers(this.source, this.target);
/*    */       
/* 24 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */     }
/*    */     
/* 27 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\PatientMissileAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */