/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BlockPerNonAttackAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 10 */   private int BLOCKGAIN_AMOUNT = 0;
/*    */   
/*    */   public BlockPerNonAttackAction(int blockAmount) {
/* 13 */     this.BLOCKGAIN_AMOUNT = blockAmount;
/* 14 */     setValues(AbstractDungeon.player, AbstractDungeon.player);
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.BLOCK;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (!this.isDone) {
/* 21 */       this.isDone = true;
/* 22 */       int total = 0;
/* 23 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 24 */         if (c.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 25 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(c, AbstractDungeon.player.hand));
/*    */           
/* 27 */           total += this.BLOCKGAIN_AMOUNT;
/*    */         }
/*    */       }
/* 30 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, total));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\BlockPerNonAttackAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */