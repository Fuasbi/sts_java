/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ 
/*    */ public class RetainCardsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("RetainCardsAction");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   
/*    */   public RetainCardsAction(AbstractCreature source, int amount) {
/* 17 */     setValues(AbstractDungeon.player, source, amount);
/* 18 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 24 */     if (this.duration == 0.5F) {
/* 25 */       AbstractDungeon.handCardSelectScreen.open(TEXT[0], this.amount, false, true, false, false, true);
/* 26 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.25F));
/* 27 */       tickDuration();
/* 28 */       return;
/*    */     }
/*    */     
/*    */ 
/* 32 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved)
/*    */     {
/* 34 */       for (AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/* 35 */         if (!c.isEthereal) {
/* 36 */           c.retain = true;
/*    */         }
/* 38 */         AbstractDungeon.player.hand.addToTop(c);
/*    */       }
/* 40 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*    */     }
/*    */     
/* 43 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RetainCardsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */