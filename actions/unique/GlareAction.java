/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class GlareAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public GlareAction()
/*    */   {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 15 */     this.p = com.megacrit.cardcrawl.dungeons.AbstractDungeon.player;
/* 16 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 22 */       for (AbstractCard c : this.p.hand.group) {
/* 23 */         if (c.cost >= 0) {
/* 24 */           int newCost = MathUtils.random(1, 3);
/* 25 */           if (c.cost != newCost) {
/* 26 */             c.cost = newCost;
/* 27 */             c.costForTurn = c.cost;
/* 28 */             c.isCostModified = true;
/*    */           }
/*    */         }
/*    */       }
/*    */       
/* 33 */       for (AbstractCard c : this.p.drawPile.group) {
/* 34 */         if (c.cost >= 0) {
/* 35 */           int newCost = MathUtils.random(1, 3);
/* 36 */           if (c.cost != newCost) {
/* 37 */             c.cost = newCost;
/* 38 */             c.costForTurn = c.cost;
/* 39 */             c.isCostModified = true;
/*    */           }
/*    */         }
/*    */       }
/*    */       
/* 44 */       for (AbstractCard c : this.p.discardPile.group) {
/* 45 */         if (c.cost >= 0) {
/* 46 */           int newCost = MathUtils.random(1, 3);
/* 47 */           if (c.cost != newCost) {
/* 48 */             c.cost = newCost;
/* 49 */             c.costForTurn = c.cost;
/* 50 */             c.isCostModified = true;
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 56 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\GlareAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */