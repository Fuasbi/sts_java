/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class UndoAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public UndoAction()
/*    */   {
/* 16 */     this.p = AbstractDungeon.player;
/* 17 */     this.duration = Settings.ACTION_DUR_MED;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     if (this.duration == Settings.ACTION_DUR_MED)
/*    */     {
/*    */ 
/* 25 */       if (GameActionManager.turn == 1) {
/* 26 */         this.isDone = true;
/* 27 */         return;
/*    */       }
/*    */       
/* 30 */       if (this.p.currentHealth < GameActionManager.playerHpLastTurn)
/*    */       {
/* 32 */         this.p.heal(GameActionManager.playerHpLastTurn - this.p.currentHealth, true);
/*    */       }
/* 34 */       else if (this.p.currentHealth > GameActionManager.playerHpLastTurn)
/*    */       {
/* 36 */         AbstractDungeon.actionManager.addToTop(new DamageAction(this.p, new com.megacrit.cardcrawl.cards.DamageInfo(this.p, this.p.currentHealth - GameActionManager.playerHpLastTurn, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS), AbstractGameAction.AttackEffect.FIRE));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 46 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\UndoAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */