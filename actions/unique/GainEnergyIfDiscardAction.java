/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class GainEnergyIfDiscardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int energyGain;
/*    */   
/*    */   public GainEnergyIfDiscardAction(int amount)
/*    */   {
/* 13 */     setValues(AbstractDungeon.player, AbstractDungeon.player, 0);
/* 14 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */     
/* 16 */     this.energyGain = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST)
/*    */     {
/* 23 */       if (GameActionManager.totalDiscardedThisTurn > 0) {
/* 24 */         AbstractDungeon.player.gainEnergy(this.energyGain);
/* 25 */         AbstractDungeon.actionManager.updateEnergyGain(this.energyGain);
/* 26 */         for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.player.hand.group) {
/* 27 */           c.triggerOnGainEnergy(this.energyGain, true);
/*    */         }
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 33 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\GainEnergyIfDiscardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */