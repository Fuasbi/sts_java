/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class CalculatedGambleAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private float startingDuration;
/*    */   private boolean isUpgraded;
/*    */   
/*    */   public CalculatedGambleAction(boolean upgraded)
/*    */   {
/* 14 */     this.target = AbstractDungeon.player;
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 16 */     this.startingDuration = Settings.ACTION_DUR_FAST;
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/* 18 */     this.isUpgraded = upgraded;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == this.startingDuration) {
/* 24 */       int count = AbstractDungeon.player.hand.size();
/* 25 */       if (this.isUpgraded) {
/* 26 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.target, count + 1));
/* 27 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DiscardAction(this.target, this.target, count, true));
/*    */       }
/* 29 */       else if (count != 0) {
/* 30 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.target, count));
/* 31 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DiscardAction(this.target, this.target, count, true));
/*    */       }
/*    */       
/* 34 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\CalculatedGambleAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */