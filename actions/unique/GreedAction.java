/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GreedAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int increaseGold;
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.1F;
/*    */   
/*    */   public GreedAction(AbstractCreature target, DamageInfo info, int goldAmount)
/*    */   {
/* 21 */     this.info = info;
/* 22 */     setValues(target, info);
/* 23 */     this.increaseGold = goldAmount;
/* 24 */     this.actionType = AbstractGameAction.ActionType.DAMAGE;
/* 25 */     this.duration = 0.1F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 30 */     if ((this.duration == 0.1F) && 
/* 31 */       (this.target != null)) {
/* 32 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       
/* 34 */       this.target.damage(this.info);
/*    */       
/* 36 */       if (((((AbstractMonster)this.target).isDying) || (this.target.currentHealth <= 0)) && (!this.target.halfDead) && 
/* 37 */         (!this.target.hasPower("Minion"))) {
/* 38 */         for (int i = 0; i < this.increaseGold; i++) {
/* 39 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.GainPennyEffect(this.source, this.target.hb.cX, this.target.hb.cY, this.source.hb.cX, this.source.hb.cY, true));
/*    */         }
/*    */       }
/*    */       
/*    */ 
/* 44 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 45 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 50 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\GreedAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */