/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ public class WraithFormAction extends AbstractGameAction
/*    */ {
/*    */   public WraithFormAction()
/*    */   {
/* 20 */     this.duration = Settings.ACTION_DUR_MED;
/* 21 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == Settings.ACTION_DUR_MED) {
/* 27 */       AbstractPlayer p = AbstractDungeon.player;
/*    */       
/* 29 */       moveStrikeAndDefendToLimbo(p.hand);
/* 30 */       moveStrikeAndDefendToLimbo(p.drawPile);
/* 31 */       moveStrikeAndDefendToLimbo(p.discardPile);
/*    */       
/* 33 */       AbstractDungeon.actionManager.addToTop(new WaitAction(1.5F));
/*    */       
/* 35 */       for (AbstractCard c : p.limbo.group) {
/* 36 */         c.target_x = MathUtils.random(0.4F * Settings.WIDTH, 0.6F * Settings.WIDTH);
/* 37 */         c.target_y = MathUtils.random(0.3F * Settings.HEIGHT, 0.7F * Settings.HEIGHT);
/* 38 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(c, p.limbo, true));
/*    */       }
/* 40 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   private void moveStrikeAndDefendToLimbo(CardGroup cardGroup) {
/* 45 */     AbstractPlayer p = AbstractDungeon.player;
/*    */     
/* 47 */     for (Iterator<AbstractCard> i = cardGroup.group.iterator(); i.hasNext();) {
/* 48 */       AbstractCard e = (AbstractCard)i.next();
/* 49 */       if (((e instanceof com.megacrit.cardcrawl.cards.green.Strike_Green)) || ((e instanceof com.megacrit.cardcrawl.cards.green.Defend_Green))) {
/* 50 */         i.remove();
/* 51 */         p.limbo.group.add(e);
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\WraithFormAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */