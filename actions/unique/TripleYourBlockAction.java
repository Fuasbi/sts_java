/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class TripleYourBlockAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public TripleYourBlockAction(AbstractCreature target)
/*    */   {
/* 11 */     this.duration = 0.5F;
/* 12 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 13 */     this.target = target;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if ((this.duration == 0.5F) && 
/* 19 */       (this.target != null) && (this.target.currentBlock > 0)) {
/* 20 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SHIELD));
/* 21 */       this.target.addBlock(this.target.currentBlock * 2);
/*    */     }
/*    */     
/*    */ 
/* 25 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\TripleYourBlockAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */