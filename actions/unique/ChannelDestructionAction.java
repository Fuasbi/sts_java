/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ChannelDestructionAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ChannelDestructionAction(AbstractCreature target)
/*    */   {
/* 13 */     this.target = target;
/* 14 */     this.source = AbstractDungeon.player;
/* 15 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 21 */       DamageInfo info = new DamageInfo(this.source, AbstractDungeon.player.hand.size() * 2);
/* 22 */       info.applyPowers(AbstractDungeon.player, this.target);
/* 23 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */     }
/*    */     
/* 26 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ChannelDestructionAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */