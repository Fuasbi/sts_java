/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class ApplyBulletTimeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ApplyBulletTimeAction()
/*    */   {
/* 11 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.group) {
/* 17 */       c.setCostForTurn(-9);
/*    */     }
/* 19 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ApplyBulletTimeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */