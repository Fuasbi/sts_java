/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ 
/*    */ public class SpotWeaknessAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("OpeningAction");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private int damageIncrease;
/*    */   private AbstractMonster targetMonster;
/*    */   
/*    */   public SpotWeaknessAction(int damageIncrease, AbstractMonster m)
/*    */   {
/* 20 */     this.duration = 0.0F;
/* 21 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/* 22 */     this.damageIncrease = damageIncrease;
/* 23 */     this.targetMonster = m;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if ((this.targetMonster != null) && ((this.targetMonster.intent == AbstractMonster.Intent.ATTACK) || (this.targetMonster.intent == AbstractMonster.Intent.ATTACK_BUFF) || (this.targetMonster.intent == AbstractMonster.Intent.ATTACK_DEBUFF) || (this.targetMonster.intent == AbstractMonster.Intent.ATTACK_DEFEND)))
/*    */     {
/*    */ 
/*    */ 
/* 32 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.StrengthPower(AbstractDungeon.player, this.damageIncrease), this.damageIncrease));
/*    */ 
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/*    */ 
/* 39 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.ThoughtBubble(AbstractDungeon.player.dialogX, AbstractDungeon.player.dialogY, 3.0F, TEXT[0], true));
/*    */     }
/*    */     
/* 42 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\SpotWeaknessAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */