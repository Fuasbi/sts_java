/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class TempestAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 14 */   private boolean freeToPlayOnce = false;
/*    */   private AbstractPlayer p;
/* 16 */   private int energyOnUse = -1;
/*    */   private boolean upgraded;
/*    */   
/*    */   public TempestAction(AbstractPlayer p, int energyOnUse, boolean upgraded, boolean freeToPlayOnce) {
/* 20 */     this.p = p;
/* 21 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 22 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 23 */     this.energyOnUse = energyOnUse;
/* 24 */     this.upgraded = upgraded;
/* 25 */     this.freeToPlayOnce = freeToPlayOnce;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 30 */     int effect = EnergyPanel.totalCount;
/* 31 */     if (this.energyOnUse != -1) {
/* 32 */       effect = this.energyOnUse;
/*    */     }
/*    */     
/* 35 */     if (this.p.hasRelic("Chemical X")) {
/* 36 */       effect += 2;
/* 37 */       this.p.getRelic("Chemical X").flash();
/*    */     }
/*    */     
/* 40 */     if (this.upgraded) {
/* 41 */       effect++;
/*    */     }
/*    */     
/* 44 */     if (effect > 0) {
/* 45 */       for (int i = 0; i < effect; i++) {
/* 46 */         AbstractOrb orb = new com.megacrit.cardcrawl.orbs.Lightning();
/* 47 */         AbstractDungeon.actionManager.addToBottom(new ChannelAction(orb));
/*    */       }
/*    */       
/* 50 */       if (!this.freeToPlayOnce) {
/* 51 */         this.p.energy.use(EnergyPanel.totalCount);
/*    */       }
/*    */     }
/* 54 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\TempestAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */