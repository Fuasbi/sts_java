/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class BouncingFlaskAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private static final float DURATION = 0.01F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*    */   private int numTimes;
/*    */   private int amount;
/*    */   
/*    */   public BouncingFlaskAction(AbstractCreature target, int amount, int numTimes)
/*    */   {
/* 19 */     this.target = target;
/* 20 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DEBUFF;
/* 21 */     this.duration = 0.01F;
/* 22 */     this.numTimes = numTimes;
/* 23 */     this.amount = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if (this.target == null) {
/* 29 */       this.isDone = true;
/* 30 */       return;
/*    */     }
/*    */     
/* 33 */     if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 34 */       AbstractDungeon.actionManager.clearPostCombatActions();
/* 35 */       this.isDone = true;
/* 36 */       return;
/*    */     }
/*    */     
/* 39 */     if (this.target.currentHealth > 0) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.target, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.PoisonPower(this.target, AbstractDungeon.player, this.amount), this.amount, true, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.POISON));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 48 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */     }
/*    */     
/*    */ 
/* 52 */     if ((this.numTimes > 1) && (!AbstractDungeon.getMonsters().areMonstersBasicallyDead())) {
/* 53 */       this.numTimes -= 1;
/* 54 */       AbstractMonster randomMonster = AbstractDungeon.getMonsters().getRandomMonster(true);
/* 55 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.PotionBounceEffect(this.target.hb.cX, this.target.hb.cY, randomMonster.hb.cX, randomMonster.hb.cY), 0.4F));
/*    */       
/*    */ 
/*    */ 
/* 59 */       AbstractDungeon.actionManager.addToBottom(new BouncingFlaskAction(randomMonster, this.amount, this.numTimes));
/*    */     }
/*    */     
/* 62 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\BouncingFlaskAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */