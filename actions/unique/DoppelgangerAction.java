/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.powers.EnergizedPower;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class DoppelgangerAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private boolean freeToPlayOnce;
/*    */   private boolean upgraded;
/*    */   private AbstractPlayer p;
/*    */   private int energyOnUse;
/*    */   
/*    */   public DoppelgangerAction(AbstractPlayer p, boolean upgraded, boolean freeToPlayOnce, int energyOnUse)
/*    */   {
/* 20 */     this.p = p;
/* 21 */     this.upgraded = upgraded;
/* 22 */     this.freeToPlayOnce = freeToPlayOnce;
/* 23 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 24 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 25 */     this.energyOnUse = energyOnUse;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 30 */     int effect = EnergyPanel.totalCount;
/* 31 */     if (this.energyOnUse != -1) {
/* 32 */       effect = this.energyOnUse;
/*    */     }
/*    */     
/* 35 */     if (this.p.hasRelic("Chemical X")) {
/* 36 */       effect += 2;
/* 37 */       this.p.getRelic("Chemical X").flash();
/*    */     }
/*    */     
/* 40 */     if (this.upgraded) {
/* 41 */       effect++;
/*    */     }
/*    */     
/* 44 */     if (effect > 0) {
/* 45 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.p, this.p, new EnergizedPower(this.p, effect), effect));
/*    */       
/* 47 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.p, this.p, new com.megacrit.cardcrawl.powers.DrawCardNextTurnPower(this.p, effect), effect));
/*    */       
/*    */ 
/* 50 */       if (!this.freeToPlayOnce) {
/* 51 */         this.p.energy.use(EnergyPanel.totalCount);
/*    */       }
/*    */     }
/* 54 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DoppelgangerAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */