/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ExpertiseAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ExpertiseAction(AbstractCreature source, int amount)
/*    */   {
/* 11 */     setValues(this.target, source, amount);
/* 12 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     int toDraw = this.amount - AbstractDungeon.player.hand.size();
/* 18 */     if (toDraw > 0) {
/* 19 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.source, toDraw));
/*    */     }
/*    */     
/*    */ 
/* 23 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ExpertiseAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */