/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.LoseHPAction;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class StepThroughTimeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public StepThroughTimeAction()
/*    */   {
/* 13 */     this.source = AbstractDungeon.player;
/* 14 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 20 */       int diff = GameActionManager.playerHpLastTurn - AbstractDungeon.player.currentHealth;
/* 21 */       if (diff > 0) {
/* 22 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.HealAction(this.source, this.source, diff));
/* 23 */       } else if (diff < 0) {
/* 24 */         AbstractDungeon.actionManager.addToTop(new LoseHPAction(this.source, this.source, diff));
/*    */       }
/*    */     }
/*    */     
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\StepThroughTimeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */