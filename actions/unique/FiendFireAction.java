/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class FiendFireAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private float startingDuration;
/*    */   
/*    */   public FiendFireAction(AbstractCreature target, DamageInfo info)
/*    */   {
/* 16 */     this.info = info;
/* 17 */     setValues(target, info);
/* 18 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 19 */     this.attackEffect = AbstractGameAction.AttackEffect.FIRE;
/* 20 */     this.startingDuration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 21 */     this.duration = this.startingDuration;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == this.startingDuration) {
/* 27 */       int count = AbstractDungeon.player.hand.size();
/*    */       
/* 29 */       for (int i = 0; i < count; i++) {
/* 30 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, this.info, AbstractGameAction.AttackEffect.FIRE));
/*    */       }
/* 32 */       for (int i = 0; i < count; i++) {
/* 33 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustAction(AbstractDungeon.player, AbstractDungeon.player, 1, true, true));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 38 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\FiendFireAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */