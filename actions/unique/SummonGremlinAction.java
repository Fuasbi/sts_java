/*     */ package com.megacrit.cardcrawl.actions.unique;
/*     */ 
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.MonsterHelper;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.MinionPower;
/*     */ import com.megacrit.cardcrawl.powers.SlowPower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SummonGremlinAction
/*     */   extends AbstractGameAction
/*     */ {
/*  34 */   private static final Logger logger = LogManager.getLogger(SummonGremlinAction.class.getName());
/*     */   private AbstractMonster m;
/*  36 */   private int slotToFill = 0;
/*     */   
/*     */   public SummonGremlinAction(AbstractMonster[] gremlins) {
/*  39 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/*  40 */     if (Settings.FAST_MODE) {
/*  41 */       this.startDuration = Settings.ACTION_DUR_FAST;
/*     */     } else {
/*  43 */       this.startDuration = Settings.ACTION_DUR_LONG;
/*     */     }
/*  45 */     this.duration = this.startDuration;
/*  46 */     int slot = identifySlot(gremlins);
/*     */     
/*  48 */     if (slot == -1) {
/*  49 */       logger.info("INCORRECTLY ATTEMPTED TO CHANNEL GREMLIN.");
/*  50 */       return;
/*     */     }
/*  52 */     this.slotToFill = slot;
/*     */     
/*  54 */     this.m = getRandomGremlin(slot);
/*  55 */     gremlins[slot] = this.m;
/*     */     
/*  57 */     if (AbstractDungeon.player.hasRelic("Philosopher's Stone")) {
/*  58 */       this.m.addPower(new StrengthPower(this.m, 2));
/*     */     }
/*     */   }
/*     */   
/*     */   private int identifySlot(AbstractMonster[] gremlins) {
/*  63 */     for (int i = 0; i < gremlins.length; i++) {
/*  64 */       if ((gremlins[i] == null) || (gremlins[i].isDying)) {
/*  65 */         return i;
/*     */       }
/*     */     }
/*  68 */     return -1;
/*     */   }
/*     */   
/*     */   private AbstractMonster getRandomGremlin(int slot) {
/*  72 */     ArrayList<String> pool = new ArrayList();
/*  73 */     pool.add("GremlinWarrior");
/*  74 */     pool.add("GremlinWarrior");
/*  75 */     pool.add("GremlinThief");
/*  76 */     pool.add("GremlinThief");
/*  77 */     pool.add("GremlinFat");
/*  78 */     pool.add("GremlinFat");
/*  79 */     pool.add("GremlinTsundere");
/*  80 */     pool.add("GremlinWizard");
/*     */     float y;
/*     */     float y;
/*  83 */     float y; float x; float y; switch (slot) {
/*     */     case 0: 
/*  85 */       float x = -366.0F;
/*  86 */       y = -4.0F;
/*  87 */       break;
/*     */     case 1: 
/*  89 */       float x = -170.0F;
/*  90 */       y = 6.0F;
/*  91 */       break;
/*     */     case 2: 
/*  93 */       float x = -532.0F;
/*  94 */       y = 0.0F;
/*  95 */       break;
/*     */     default: 
/*  97 */       x = -366.0F;
/*  98 */       y = -4.0F;
/*     */     }
/*     */     
/*     */     
/* 102 */     return MonsterHelper.getGremlin((String)pool.get(AbstractDungeon.aiRng.random(0, pool.size() - 1)), x, y);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 107 */     if (this.duration == this.startDuration) {
/* 108 */       this.m.animX = (1200.0F * Settings.scale);
/* 109 */       this.m.init();
/* 110 */       this.m.applyPowers();
/* 111 */       AbstractDungeon.getCurrRoom().monsters.addMonster(this.slotToFill, this.m);
/*     */       
/* 113 */       if (((Boolean)DailyMods.negativeMods.get("Lethality")).booleanValue()) {
/* 114 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.m, new StrengthPower(this.m, 3), 3));
/*     */       }
/*     */       
/*     */ 
/* 118 */       if (((Boolean)DailyMods.negativeMods.get("Time Dilation")).booleanValue()) {
/* 119 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.m, new SlowPower(this.m, 0)));
/*     */       }
/*     */       
/* 122 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.m, new MinionPower(this.m)));
/*     */     }
/*     */     
/* 125 */     tickDuration();
/*     */     
/* 127 */     if (this.isDone) {
/* 128 */       this.m.animX = 0.0F;
/* 129 */       this.m.showHealthBar();
/* 130 */       this.m.usePreBattleAction();
/*     */     } else {
/* 132 */       this.m.animX = Interpolation.fade.apply(0.0F, 1200.0F * Settings.scale, this.duration);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\SummonGremlinAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */