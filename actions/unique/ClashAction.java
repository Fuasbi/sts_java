/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class ClashAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int blockAmount;
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.1F;
/*    */   
/*    */   public ClashAction(AbstractCreature target, DamageInfo info, int blockAmount)
/*    */   {
/* 17 */     this.info = info;
/* 18 */     setValues(target, info);
/* 19 */     this.blockAmount = blockAmount;
/* 20 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 21 */     this.duration = 0.1F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == 0.1F) {
/* 27 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/* 28 */       this.target.damage(this.info);
/*    */       
/* 30 */       if ((((AbstractMonster)this.target).isDying) || (this.target.currentHealth <= 0)) {
/* 31 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.blockAmount));
/*    */       }
/*    */       
/*    */ 
/* 35 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 36 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/*    */     }
/*    */     
/* 40 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ClashAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */