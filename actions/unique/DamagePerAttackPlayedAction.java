/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class DamagePerAttackPlayedAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   
/*    */   public DamagePerAttackPlayedAction(AbstractCreature target, DamageInfo info, AbstractGameAction.AttackEffect effect)
/*    */   {
/* 15 */     this.info = info;
/* 16 */     setValues(target, info);
/* 17 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 18 */     this.attackEffect = effect;
/*    */   }
/*    */   
/*    */   public DamagePerAttackPlayedAction(AbstractCreature target, DamageInfo info) {
/* 22 */     this(target, info, AbstractGameAction.AttackEffect.NONE);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 27 */     this.isDone = true;
/* 28 */     if ((this.target != null) && (this.target.currentHealth > 0)) {
/* 29 */       int count = 0;
/* 30 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.actionManager.cardsPlayedThisTurn) {
/* 31 */         if (c.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 32 */           count++;
/*    */         }
/*    */       }
/*    */       
/* 36 */       count--;
/* 37 */       for (int i = 0; i < count; i++) {
/* 38 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, this.info, this.attackEffect));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DamagePerAttackPlayedAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */