/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class EscapePlanAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int blockGain;
/*    */   
/*    */   public EscapePlanAction(int blockGain)
/*    */   {
/* 14 */     this.duration = 0.0F;
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 16 */     this.blockGain = blockGain;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (AbstractDungeon.player.drawPile.isEmpty()) {
/* 22 */       this.isDone = true;
/* 23 */       return;
/*    */     }
/*    */     
/* 26 */     AbstractCard card = AbstractDungeon.player.drawPile.getTopCard();
/* 27 */     if (card.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL) {
/* 28 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.blockGain));
/*    */     }
/*    */     
/* 31 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\EscapePlanAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */