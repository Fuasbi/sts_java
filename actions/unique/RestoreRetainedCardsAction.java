/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ public class RestoreRetainedCardsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private CardGroup group;
/*    */   
/*    */   public RestoreRetainedCardsAction(CardGroup group)
/*    */   {
/* 14 */     setValues(AbstractDungeon.player, this.source, -1);
/* 15 */     this.group = group;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     this.isDone = true;
/*    */     
/* 22 */     for (Iterator<AbstractCard> c = this.group.group.iterator(); c.hasNext();) {
/* 23 */       AbstractCard e = (AbstractCard)c.next();
/* 24 */       if (e.retain) {
/* 25 */         AbstractDungeon.player.hand.addToTop(e);
/* 26 */         e.retain = false;
/* 27 */         c.remove();
/*    */       }
/*    */     }
/* 30 */     AbstractDungeon.player.hand.refreshHandLayout();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RestoreRetainedCardsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */