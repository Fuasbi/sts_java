/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.CardRewardScreen;
/*    */ 
/*    */ public class DiscoveryAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 12 */   private boolean retrieveCard = false;
/* 13 */   private AbstractCard.CardType cardType = null;
/*    */   
/*    */   public DiscoveryAction() {
/* 16 */     this.actionType = AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public DiscoveryAction(AbstractCard.CardType type) {
/* 21 */     this.actionType = AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 22 */     this.duration = Settings.ACTION_DUR_FAST;
/* 23 */     this.cardType = type;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 29 */       if (this.cardType == null) {
/* 30 */         AbstractDungeon.cardRewardScreen.discoveryOpen();
/*    */       } else {
/* 32 */         AbstractDungeon.cardRewardScreen.discoveryOpen(this.cardType);
/*    */       }
/*    */       
/* 35 */       tickDuration();
/* 36 */       return;
/*    */     }
/*    */     
/* 39 */     if (!this.retrieveCard) {
/* 40 */       if (AbstractDungeon.cardRewardScreen.discoveryCard != null) {
/* 41 */         AbstractCard disCard = AbstractDungeon.cardRewardScreen.discoveryCard.makeStatEquivalentCopy();
/* 42 */         disCard.current_x = (-1000.0F * Settings.scale);
/* 43 */         if (AbstractDungeon.player.hand.size() < 10) {
/* 44 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToHandEffect(disCard, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */         }
/*    */         else
/*    */         {
/* 48 */           AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDiscardEffect(disCard, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*    */         }
/*    */         
/* 51 */         disCard.setCostForTurn(0);
/* 52 */         AbstractDungeon.cardRewardScreen.discoveryCard = null;
/*    */       }
/* 54 */       this.retrieveCard = true;
/*    */     }
/*    */     
/* 57 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DiscoveryAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */