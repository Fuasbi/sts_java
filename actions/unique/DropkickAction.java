/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class DropkickAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public DropkickAction(AbstractCreature target)
/*    */   {
/* 14 */     this.duration = Settings.ACTION_DUR_XFAST;
/* 15 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 16 */     this.target = target;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if ((this.target != null) && (this.target.hasPower("Vulnerable"))) {
/* 22 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/* 23 */       AbstractDungeon.actionManager.addToTop(new GainEnergyAction(1));
/*    */     }
/* 25 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DropkickAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */