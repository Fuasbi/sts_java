/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Transmutev2Action extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public Transmutev2Action()
/*    */   {
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 16 */     this.p = AbstractDungeon.player;
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 24 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 25 */       AbstractDungeon.actionManager.cleanCardQueue();
/* 26 */       if (this.p.hand.group.isEmpty()) {
/* 27 */         this.isDone = true;
/* 28 */         return;
/*    */       }
/* 30 */       CardGroup tmp = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/* 31 */       tmp.group.addAll(this.p.hand.group);
/* 32 */       this.p.hand.clear();
/* 33 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : tmp.group) {
/* 34 */         AbstractDungeon.transformCard(c);
/* 35 */         com.megacrit.cardcrawl.cards.AbstractCard transformedCard = AbstractDungeon.getTransformedCard();
/* 36 */         this.p.hand.addToTop(transformedCard);
/*    */       }
/*    */       
/* 39 */       tickDuration();
/* 40 */       return;
/*    */     }
/*    */     
/* 43 */     this.p.hand.refreshHandLayout();
/* 44 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\Transmutev2Action.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */