/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class SwordBoomerangAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.01F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.2F;
/*    */   private int numTimes;
/*    */   
/*    */   public SwordBoomerangAction(AbstractCreature target, DamageInfo info, int numTimes)
/*    */   {
/* 17 */     this.info = info;
/* 18 */     this.target = target;
/* 19 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 20 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL;
/* 21 */     this.duration = 0.01F;
/* 22 */     this.numTimes = numTimes;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 27 */     if (this.target == null) {
/* 28 */       this.isDone = true;
/* 29 */       return;
/*    */     }
/*    */     
/* 32 */     if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 33 */       AbstractDungeon.actionManager.clearPostCombatActions();
/* 34 */       this.isDone = true;
/* 35 */       return;
/*    */     }
/*    */     
/* 38 */     if (this.target.currentHealth > 0) {
/* 39 */       this.target.damageFlash = true;
/* 40 */       this.target.damageFlashFrames = 4;
/* 41 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/* 42 */       this.info.applyPowers(this.info.owner, this.target);
/* 43 */       this.target.damage(this.info);
/*    */       
/*    */ 
/* 46 */       if ((this.numTimes > 1) && (!AbstractDungeon.getMonsters().areMonstersBasicallyDead())) {
/* 47 */         this.numTimes -= 1;
/* 48 */         AbstractDungeon.actionManager.addToTop(new SwordBoomerangAction(
/* 49 */           AbstractDungeon.getMonsters().getRandomMonster(true), this.info, this.numTimes));
/*    */       }
/*    */       
/* 52 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.2F));
/*    */     }
/*    */     
/* 55 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\SwordBoomerangAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */