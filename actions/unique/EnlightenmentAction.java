/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class EnlightenmentAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/* 11 */   private boolean forCombat = false;
/*    */   
/*    */   public EnlightenmentAction(boolean forRestOfCombat) {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 15 */     this.p = com.megacrit.cardcrawl.dungeons.AbstractDungeon.player;
/* 16 */     this.duration = Settings.ACTION_DUR_FAST;
/* 17 */     this.forCombat = forRestOfCombat;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 23 */       for (AbstractCard c : this.p.hand.group) {
/* 24 */         if (c.costForTurn > 1) {
/* 25 */           c.costForTurn = 1;
/* 26 */           c.isCostModifiedForTurn = true;
/*    */           
/* 28 */           if (this.forCombat) {
/* 29 */             c.cost = 1;
/* 30 */             c.isCostModified = true;
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 36 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\EnlightenmentAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */