/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class FlechetteAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   
/*    */   public FlechetteAction(AbstractCreature target, DamageInfo info)
/*    */   {
/* 18 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 19 */     this.info = info;
/* 20 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 21 */     this.target = target;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 27 */       if (c.type == AbstractCard.CardType.SKILL) {
/* 28 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, this.info, true));
/* 29 */         if ((this.target != null) && (this.target.hb != null)) {
/* 30 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.ThrowDaggerEffect(this.target.hb.cX, this.target.hb.cY)));
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 35 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\FlechetteAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */