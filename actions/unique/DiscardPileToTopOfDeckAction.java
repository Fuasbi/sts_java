/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DiscardPileToTopOfDeckAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("DiscardPileToTopOfDeckAction");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public DiscardPileToTopOfDeckAction(AbstractCreature source) {
/* 18 */     this.p = AbstractDungeon.player;
/* 19 */     setValues(null, source, this.amount);
/* 20 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 21 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FASTER;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (AbstractDungeon.getCurrRoom().isBattleEnding()) {
/* 27 */       this.isDone = true; return;
/*    */     }
/*    */     AbstractCard tmp;
/* 30 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FASTER) {
/* 31 */       if (this.p.discardPile.isEmpty()) {
/* 32 */         this.isDone = true;
/* 33 */         return; }
/* 34 */       if (this.p.discardPile.size() == 1) {
/* 35 */         tmp = this.p.discardPile.getTopCard();
/* 36 */         this.p.discardPile.removeCard(tmp);
/* 37 */         this.p.discardPile.moveToDeck(tmp, false);
/*    */       }
/*    */       
/* 40 */       if (this.p.discardPile.group.size() > this.amount) {
/* 41 */         AbstractDungeon.gridSelectScreen.open(this.p.discardPile, 1, TEXT[0], false, false, false, false);
/* 42 */         tickDuration();
/* 43 */         return;
/*    */       }
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 49 */     if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/* 50 */       for (AbstractCard c : AbstractDungeon.gridSelectScreen.selectedCards) {
/* 51 */         this.p.discardPile.removeCard(c);
/* 52 */         this.p.hand.moveToDeck(c, false);
/*    */       }
/* 54 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 55 */       AbstractDungeon.player.hand.refreshHandLayout();
/*    */     }
/*    */     
/* 58 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\DiscardPileToTopOfDeckAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */