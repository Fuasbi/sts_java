/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class InspirationAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public InspirationAction(int drawAmt)
/*    */   {
/* 11 */     this.source = AbstractDungeon.player;
/* 12 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 13 */     this.amount = drawAmt;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if ((this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) && 
/* 19 */       (this.amount - AbstractDungeon.player.hand.size() > 0)) {
/* 20 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.source, this.amount - AbstractDungeon.player.hand
/* 21 */         .size()));
/*    */     }
/*    */     
/*    */ 
/* 25 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\InspirationAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */