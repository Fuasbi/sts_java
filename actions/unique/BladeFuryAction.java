/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
/*    */ import com.megacrit.cardcrawl.cards.colorless.Shiv;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class BladeFuryAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private boolean upgrade;
/*    */   
/*    */   public BladeFuryAction(boolean upgraded)
/*    */   {
/* 15 */     this.duration = Settings.ACTION_DUR_FAST;
/* 16 */     this.upgrade = upgraded;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 22 */       int theSize = AbstractDungeon.player.hand.size();
/*    */       
/* 24 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DiscardAction(AbstractDungeon.player, AbstractDungeon.player, theSize, false));
/*    */       
/* 26 */       if (this.upgrade) {
/* 27 */         com.megacrit.cardcrawl.cards.AbstractCard s = new Shiv().makeCopy();
/* 28 */         s.upgrade();
/* 29 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInHandAction(s, theSize));
/*    */       } else {
/* 31 */         AbstractDungeon.actionManager.addToBottom(new MakeTempCardInHandAction(new Shiv(), theSize));
/*    */       }
/*    */     }
/* 34 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\BladeFuryAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */