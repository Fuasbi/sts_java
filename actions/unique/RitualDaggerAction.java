/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class RitualDaggerAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int increaseAmount;
/*    */   private int miscVal;
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.1F;
/*    */   
/*    */   public RitualDaggerAction(AbstractCreature target, DamageInfo info, int incAmount, int miscValueCheck)
/*    */   {
/* 17 */     this.info = info;
/* 18 */     this.miscVal = miscValueCheck;
/* 19 */     setValues(target, info);
/* 20 */     this.increaseAmount = incAmount;
/* 21 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 22 */     this.duration = 0.1F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 27 */     if ((this.duration == 0.1F) && 
/* 28 */       (this.target != null)) {
/* 29 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*    */       
/* 31 */       this.target.damage(this.info);
/*    */       
/* 33 */       if (((this.target.isDying) || (this.target.currentHealth <= 0)) && (!this.target.halfDead)) {
/* 34 */         for (AbstractCard c : AbstractDungeon.player.masterDeck.group) {
/* 35 */           if ((c.cardID.equals("RitualDagger")) && (c.misc == this.miscVal)) {
/* 36 */             c.misc += this.increaseAmount;
/* 37 */             c.applyPowers();
/* 38 */             c.baseDamage = c.misc;
/* 39 */             c.isDamageModified = false;
/* 40 */             break;
/*    */           }
/*    */         }
/*    */       }
/*    */       
/*    */ 
/* 46 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 47 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 52 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RitualDaggerAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */