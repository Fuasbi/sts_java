/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class SacrificeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public SacrificeAction()
/*    */   {
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 15 */     this.p = AbstractDungeon.player;
/* 16 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 24 */       if (this.p.hand.group.isEmpty()) {
/* 25 */         this.isDone = true;
/* 26 */         return; }
/* 27 */       if (this.p.hand.size() == 1) {
/* 28 */         AbstractDungeon.srcTransformCard((com.megacrit.cardcrawl.cards.AbstractCard)this.p.hand.group.get(0));
/* 29 */         this.p.hand.clear();
/*    */       } else {
/* 31 */         AbstractDungeon.handCardSelectScreen.open("sacrifice", 1, false, false, true);
/*    */       }
/* 33 */       tickDuration();
/* 34 */       return;
/*    */     }
/*    */     
/*    */ 
/* 38 */     com.megacrit.cardcrawl.cards.AbstractCard transformedCard = AbstractDungeon.getTransformedCard();
/* 39 */     transformedCard.upgrade();
/*    */     
/* 41 */     this.p.hand.addToTop(transformedCard);
/* 42 */     AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/* 43 */     AbstractDungeon.handCardSelectScreen.selectedCards.group.clear();
/* 44 */     this.p.hand.refreshHandLayout();
/* 45 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\SacrificeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */