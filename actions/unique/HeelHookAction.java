/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class HeelHookAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public HeelHookAction(AbstractCreature target)
/*    */   {
/* 14 */     this.duration = Settings.ACTION_DUR_XFAST;
/* 15 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 16 */     this.target = target;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if ((this.duration == Settings.ACTION_DUR_XFAST) && 
/* 22 */       (this.target != null) && (this.target.hasPower("Weakened"))) {
/* 23 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/* 24 */       AbstractDungeon.actionManager.addToTop(new GainEnergyAction(1));
/*    */     }
/*    */     
/*    */ 
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\HeelHookAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */