/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class GamblingChipAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/* 17 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("GamblingChipAction");
/* 18 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private boolean notchip;
/*    */   
/*    */   public GamblingChipAction(AbstractCreature source) {
/* 22 */     setValues(AbstractDungeon.player, source, -1);
/* 23 */     this.actionType = AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 24 */     this.notchip = false;
/*    */   }
/*    */   
/*    */   public GamblingChipAction(AbstractCreature source, boolean notChip) {
/* 28 */     setValues(AbstractDungeon.player, source, -1);
/* 29 */     this.actionType = AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 30 */     this.notchip = notChip;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 35 */     if (this.duration == 0.5F) {
/* 36 */       if (this.notchip) {
/* 37 */         AbstractDungeon.handCardSelectScreen.open(TEXT[1], 99, true, true);
/*    */       } else {
/* 39 */         AbstractDungeon.handCardSelectScreen.open(TEXT[0], 99, true, true);
/*    */       }
/* 41 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.25F));
/* 42 */       tickDuration();
/* 43 */       return;
/*    */     }
/*    */     
/*    */ 
/* 47 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved)
/*    */     {
/*    */ 
/* 50 */       if (!AbstractDungeon.handCardSelectScreen.selectedCards.group.isEmpty()) {
/* 51 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.p, AbstractDungeon.handCardSelectScreen.selectedCards.group
/* 52 */           .size()));
/*    */         
/*    */ 
/* 55 */         for (AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/* 56 */           AbstractDungeon.player.hand.moveToDiscardPile(c);
/* 57 */           GameActionManager.incrementDiscard(false);
/* 58 */           c.triggerOnManualDiscard();
/*    */         }
/*    */       }
/*    */       
/* 62 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*    */     }
/*    */     
/* 65 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\GamblingChipAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */