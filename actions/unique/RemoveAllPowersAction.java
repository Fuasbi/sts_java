/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ 
/*    */ public class RemoveAllPowersAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private boolean debuffsOnly;
/*    */   private AbstractCreature c;
/*    */   
/*    */   public RemoveAllPowersAction(AbstractCreature c, boolean debuffsOnly)
/*    */   {
/* 15 */     this.debuffsOnly = debuffsOnly;
/* 16 */     this.c = c;
/* 17 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     for (AbstractPower p : this.c.powers) {
/* 23 */       if ((p.type == com.megacrit.cardcrawl.powers.AbstractPower.PowerType.DEBUFF) || (!this.debuffsOnly)) {
/* 24 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.c, this.c, p.ID));
/*    */       }
/*    */     }
/*    */     
/* 28 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\RemoveAllPowersAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */