/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class ExhaustAllNonAttackAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private float startingDuration;
/*    */   
/*    */   public ExhaustAllNonAttackAction()
/*    */   {
/* 16 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 17 */     this.startingDuration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 18 */     this.duration = this.startingDuration;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == this.startingDuration) {
/* 24 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 25 */         if (c.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK) {
/* 26 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(c, AbstractDungeon.player.hand));
/*    */         }
/*    */       }
/*    */       
/* 30 */       this.isDone = true;
/*    */       
/*    */ 
/* 33 */       if (AbstractDungeon.player.exhaustPile.size() >= 20) {
/* 34 */         UnlockTracker.unlockAchievement("THE_PACT");
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\ExhaustAllNonAttackAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */