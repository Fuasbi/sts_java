/*    */ package com.megacrit.cardcrawl.actions.unique;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.actions.common.SpawnMonsterAction;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.beyond.SnakeDagger;
/*    */ import com.megacrit.cardcrawl.powers.SlowPower;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class SpawnDaggerAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public static final float pos0X = 210.0F;
/*    */   public static final float pos0Y = 50.0F;
/*    */   public static final float pos1X = -220.0F;
/*    */   public static final float pos1Y = 90.0F;
/*    */   private static final float pos2X = 180.0F;
/*    */   private static final float pos2Y = 320.0F;
/*    */   private static final float pos3X = -250.0F;
/*    */   private static final float pos3Y = 310.0F;
/*    */   
/*    */   public SpawnDaggerAction(AbstractMonster monster)
/*    */   {
/* 28 */     this.source = monster;
/* 29 */     this.duration = Settings.ACTION_DUR_XFAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 34 */     if (this.duration == Settings.ACTION_DUR_XFAST) {
/* 35 */       int count = 0;
/* 36 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 37 */         if (m != this.source) {
/* 38 */           if (m.isDying) {
/* 39 */             AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.MinionPower(this.source)));
/* 40 */             AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ReviveMonsterAction(m, this.source, false));
/*    */             
/* 42 */             if (AbstractDungeon.player.hasRelic("Philosopher's Stone")) {
/* 43 */               m.addPower(new StrengthPower(m, 2));
/*    */             }
/* 45 */             if (((Boolean)DailyMods.negativeMods.get("Lethality")).booleanValue()) {
/* 46 */               AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new StrengthPower(m, 3), 3));
/*    */             }
/*    */             
/* 49 */             if (((Boolean)DailyMods.negativeMods.get("Time Dilation")).booleanValue()) {
/* 50 */               AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new SlowPower(m, 0)));
/*    */             }
/* 52 */             tickDuration();
/* 53 */             return;
/*    */           }
/* 55 */           count++;
/*    */         }
/*    */       }
/*    */       
/* 59 */       if (count == 1) {
/* 60 */         AbstractDungeon.actionManager.addToTop(new SpawnMonsterAction(new SnakeDagger(-220.0F, 90.0F), true));
/* 61 */       } else if (count == 2) {
/* 62 */         AbstractDungeon.actionManager.addToTop(new SpawnMonsterAction(new SnakeDagger(180.0F, 320.0F), true));
/* 63 */       } else if (count == 3) {
/* 64 */         AbstractDungeon.actionManager.addToTop(new SpawnMonsterAction(new SnakeDagger(-250.0F, 310.0F), true));
/*    */       }
/*    */     }
/* 67 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\unique\SpawnDaggerAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */