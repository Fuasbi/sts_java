/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class UnlimboAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard card;
/*    */   private boolean exhaust;
/*    */   
/*    */   public UnlimboAction(AbstractCard card, boolean exhaust)
/*    */   {
/* 14 */     this.duration = Settings.ACTION_DUR_XFAST;
/* 15 */     this.card = card;
/* 16 */     this.exhaust = exhaust;
/*    */   }
/*    */   
/*    */   public UnlimboAction(AbstractCard card) {
/* 20 */     this(card, false);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     if (this.duration == Settings.ACTION_DUR_XFAST) {
/* 26 */       if (!this.exhaust) {}
/*    */       
/*    */ 
/* 29 */       AbstractDungeon.player.limbo.removeCard(this.card);
/* 30 */       if (this.exhaust) {
/* 31 */         AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect(this.card));
/*    */       }
/* 33 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\UnlimboAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */