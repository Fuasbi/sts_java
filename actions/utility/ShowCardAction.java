/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ShowCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*  8 */   private AbstractCard card = null;
/*    */   private static final float PURGE_DURATION = 0.2F;
/*    */   
/*    */   public ShowCardAction(AbstractCard card) {
/* 12 */     setValues(AbstractDungeon.player, null, 1);
/* 13 */     this.card = card;
/* 14 */     this.duration = 0.2F;
/* 15 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SPECIAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == 0.2F)
/*    */     {
/* 22 */       if (AbstractDungeon.player.limbo.contains(this.card)) {
/* 23 */         AbstractDungeon.player.limbo.removeCard(this.card);
/*    */       }
/* 25 */       AbstractDungeon.player.cardInUse = null;
/*    */     }
/* 27 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\ShowCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */