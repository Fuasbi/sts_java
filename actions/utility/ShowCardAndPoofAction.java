/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ShowCardAndPoofAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*  9 */   private AbstractCard card = null;
/*    */   private static final float PURGE_DURATION = 0.2F;
/*    */   
/*    */   public ShowCardAndPoofAction(AbstractCard card) {
/* 13 */     setValues(AbstractDungeon.player, null, 1);
/* 14 */     this.card = card;
/* 15 */     this.duration = 0.2F;
/* 16 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SPECIAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 21 */     if (this.duration == 0.2F) {
/* 22 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect(this.card));
/* 23 */       if (AbstractDungeon.player.limbo.contains(this.card)) {
/* 24 */         AbstractDungeon.player.limbo.removeCard(this.card);
/*    */       }
/* 26 */       AbstractDungeon.player.cardInUse = null;
/*    */     }
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\ShowCardAndPoofAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */