/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ExhaustAllEtherealAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ExhaustAllEtherealAction()
/*    */   {
/* 12 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void update()
/*    */   {
/* 20 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 21 */       if (c.isEthereal) {
/* 22 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction(c, AbstractDungeon.player.hand));
/*    */       }
/*    */     }
/* 25 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\ExhaustAllEtherealAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */