/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class QueueCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard card;
/*    */   
/*    */   public QueueCardAction()
/*    */   {
/* 15 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public QueueCardAction(AbstractCard card, AbstractCreature target) {
/* 19 */     this.duration = Settings.ACTION_DUR_FAST;
/* 20 */     this.card = card;
/* 21 */     this.target = target;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 27 */       if (this.card == null)
/*    */       {
/* 29 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem());
/*    */       }
/*    */       else {
/* 32 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(this.card, (com.megacrit.cardcrawl.monsters.AbstractMonster)this.target));
/*    */       }
/* 34 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\QueueCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */