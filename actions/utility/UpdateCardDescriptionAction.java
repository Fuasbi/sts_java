/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class UpdateCardDescriptionAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard targetCard;
/*    */   
/*    */   public UpdateCardDescriptionAction(AbstractCard targetCard) {
/* 10 */     this.targetCard = targetCard;
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.TEXT;
/* 12 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     if (this.duration == 0.5F) {
/* 18 */       this.targetCard.initializeDescription();
/*    */     }
/*    */     
/* 21 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\UpdateCardDescriptionAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */