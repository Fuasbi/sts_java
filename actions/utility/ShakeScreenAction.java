/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity;
/*    */ 
/*    */ public class ShakeScreenAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private float startDur;
/*    */   ScreenShake.ShakeDur shakeDur;
/*    */   ScreenShake.ShakeIntensity intensity;
/*    */   
/*    */   public ShakeScreenAction(float duration, ScreenShake.ShakeDur dur, ScreenShake.ShakeIntensity intensity)
/*    */   {
/* 14 */     this.duration = duration;
/* 15 */     this.startDur = duration;
/* 16 */     this.shakeDur = dur;
/* 17 */     this.intensity = intensity;
/* 18 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == this.startDur) {
/* 24 */       com.megacrit.cardcrawl.core.CardCrawlGame.screenShake.shake(this.intensity, this.shakeDur, false);
/*    */     }
/* 26 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\ShakeScreenAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */