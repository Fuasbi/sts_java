/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class ConditionalDrawAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard.CardType restrictedType;
/*    */   
/*    */   public ConditionalDrawAction(int newAmount, AbstractCard.CardType restrictedType)
/*    */   {
/* 13 */     this.duration = Settings.ACTION_DUR_FAST;
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 15 */     this.source = AbstractDungeon.player;
/* 16 */     this.target = AbstractDungeon.player;
/* 17 */     this.amount = newAmount;
/* 18 */     this.restrictedType = restrictedType;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == Settings.ACTION_DUR_FAST)
/*    */     {
/* 25 */       if (checkCondition()) {
/* 26 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.source, this.amount));
/*    */       }
/*    */       
/* 29 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */   private boolean checkCondition()
/*    */   {
/* 35 */     for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.player.hand.group) {
/* 36 */       if (c.type == this.restrictedType) {
/* 37 */         return false;
/*    */       }
/*    */     }
/*    */     
/* 41 */     return true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\ConditionalDrawAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */