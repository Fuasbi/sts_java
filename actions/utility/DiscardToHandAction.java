/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ 
/*    */ public class DiscardToHandAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard card;
/*    */   
/*    */   public DiscardToHandAction(AbstractCard card)
/*    */   {
/* 12 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 13 */     this.card = card;
/* 14 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 20 */       if ((com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.discardPile.contains(this.card)) && 
/* 21 */         (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.size() < 10)) {
/* 22 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.addToHand(this.card);
/* 23 */         this.card.unhover();
/* 24 */         this.card.setAngle(0.0F, true);
/* 25 */         this.card.lighten(false);
/* 26 */         this.card.drawScale = 0.12F;
/* 27 */         this.card.targetDrawScale = 0.75F;
/* 28 */         this.card.applyPowers();
/* 29 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.discardPile.removeCard(this.card);
/*    */       }
/*    */       
/* 32 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.refreshHandLayout();
/* 33 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.glowCheck();
/*    */     }
/*    */     
/* 36 */     tickDuration();
/* 37 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\DiscardToHandAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */