/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class LoseBlockAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public LoseBlockAction(AbstractCreature target, AbstractCreature source, int amount)
/*    */   {
/*  9 */     setValues(target, source, amount);
/* 10 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.BLOCK;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 15 */     if (this.duration == 0.5F) {
/* 16 */       if (this.target.currentBlock == 0) {
/* 17 */         this.isDone = true;
/* 18 */         return;
/*    */       }
/*    */       
/* 21 */       this.target.loseBlock(this.amount);
/*    */     }
/*    */     
/* 24 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\LoseBlockAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */