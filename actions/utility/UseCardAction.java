/*     */ package com.megacrit.cardcrawl.actions.utility;
/*     */ 
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ 
/*     */ public class UseCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*     */   private AbstractCard targetCard;
/*  17 */   public AbstractCreature target = null;
/*     */   public boolean exhaustCard;
/*  19 */   public boolean reboundCard = false;
/*     */   private static final float DUR = 0.15F;
/*     */   
/*     */   public UseCardAction(AbstractCard card, AbstractCreature target) {
/*  23 */     this.targetCard = card;
/*  24 */     this.target = target;
/*  25 */     if ((card.exhaustOnUseOnce) || (card.exhaust)) {
/*  26 */       this.exhaustCard = true;
/*     */     }
/*     */     
/*  29 */     setValues(AbstractDungeon.player, null, 1);
/*  30 */     this.duration = 0.15F;
/*     */     
/*  32 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/*  33 */       if (!card.dontTriggerOnUseCard) {
/*  34 */         p.onUseCard(card, this);
/*     */       }
/*     */     }
/*     */     
/*  38 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  39 */       if (!card.dontTriggerOnUseCard) {
/*  40 */         r.onUseCard(card, this);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  45 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/*  46 */       if (!card.dontTriggerOnUseCard) {
/*  47 */         c.triggerOnCardPlayed(card);
/*     */       }
/*     */     }
/*  50 */     for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/*  51 */       if (!card.dontTriggerOnUseCard) {
/*  52 */         c.triggerOnCardPlayed(card);
/*     */       }
/*     */     }
/*  55 */     for (AbstractCard c : AbstractDungeon.player.drawPile.group) {
/*  56 */       if (!card.dontTriggerOnUseCard) {
/*  57 */         c.triggerOnCardPlayed(card);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  62 */     for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/*  63 */       for (AbstractPower p : m.powers) {
/*  64 */         if (!card.dontTriggerOnUseCard) {
/*  65 */           p.onUseCard(card, this);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*  70 */     if (this.exhaustCard) {
/*  71 */       this.actionType = AbstractGameAction.ActionType.EXHAUST;
/*     */     } else {
/*  73 */       this.actionType = AbstractGameAction.ActionType.USE;
/*     */     }
/*     */   }
/*     */   
/*     */   public UseCardAction(AbstractCard targetCard) {
/*  78 */     this(targetCard, null);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  83 */     if (this.duration == 0.15F) {
/*  84 */       for (AbstractPower p : AbstractDungeon.player.powers) {
/*  85 */         if (!this.targetCard.dontTriggerOnUseCard) {
/*  86 */           p.onAfterUseCard(this.targetCard, this);
/*     */         }
/*     */       }
/*     */       
/*  90 */       for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  91 */         for (AbstractPower p : m.powers) {
/*  92 */           if (!this.targetCard.dontTriggerOnUseCard) {
/*  93 */             p.onAfterUseCard(this.targetCard, this);
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*  98 */       this.targetCard.freeToPlayOnce = false;
/*  99 */       if (this.targetCard.purgeOnUse) {
/* 100 */         AbstractDungeon.actionManager.addToTop(new ShowCardAndPoofAction(this.targetCard));
/* 101 */         this.isDone = true;
/* 102 */         AbstractDungeon.player.cardInUse = null;
/* 103 */         return;
/*     */       }
/*     */       
/*     */ 
/* 107 */       if (this.targetCard.type == com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER) {
/* 108 */         AbstractDungeon.actionManager.addToTop(new ShowCardAction(this.targetCard));
/* 109 */         if (com.megacrit.cardcrawl.core.Settings.FAST_MODE) {
/* 110 */           AbstractDungeon.actionManager.addToTop(new WaitAction(0.1F));
/*     */         } else {
/* 112 */           AbstractDungeon.actionManager.addToTop(new WaitAction(0.7F));
/*     */         }
/* 114 */         AbstractDungeon.player.hand.empower(this.targetCard);
/* 115 */         this.isDone = true;
/* 116 */         AbstractDungeon.player.hand.applyPowers();
/* 117 */         AbstractDungeon.player.hand.glowCheck();
/* 118 */         AbstractDungeon.player.cardInUse = null;
/* 119 */         return;
/*     */       }
/*     */       
/* 122 */       AbstractDungeon.player.cardInUse = null;
/* 123 */       if (!this.exhaustCard) {
/* 124 */         if (this.reboundCard) {
/* 125 */           AbstractDungeon.player.hand.moveToDeck(this.targetCard, false);
/*     */         } else {
/* 127 */           AbstractDungeon.player.hand.moveToDiscardPile(this.targetCard);
/*     */         }
/*     */       } else {
/* 130 */         this.targetCard.exhaustOnUseOnce = false;
/*     */         
/*     */ 
/* 133 */         if ((AbstractDungeon.player.hasRelic("Strange Spoon")) && (this.targetCard.type != com.megacrit.cardcrawl.cards.AbstractCard.CardType.POWER)) {
/* 134 */           if (AbstractDungeon.cardRandomRng.randomBoolean()) {
/* 135 */             AbstractDungeon.player.getRelic("Strange Spoon").flash();
/* 136 */             AbstractDungeon.player.hand.moveToDiscardPile(this.targetCard);
/*     */           } else {
/* 138 */             AbstractDungeon.player.hand.moveToExhaustPile(this.targetCard);
/* 139 */             com.megacrit.cardcrawl.core.CardCrawlGame.dungeon.checkForPactAchievement();
/*     */           }
/*     */         }
/*     */         else
/*     */         {
/* 144 */           AbstractDungeon.player.hand.moveToExhaustPile(this.targetCard);
/* 145 */           com.megacrit.cardcrawl.core.CardCrawlGame.dungeon.checkForPactAchievement();
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 150 */     tickDuration();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\UseCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */