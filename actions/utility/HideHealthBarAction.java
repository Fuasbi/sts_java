/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class HideHealthBarAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public HideHealthBarAction(AbstractCreature owner) {
/*  8 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/*  9 */     this.source = owner;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 14 */     this.source.hideHealthBar();
/* 15 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\HideHealthBarAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */