/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ 
/*    */ public class SFXAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private String key;
/*  8 */   private float pitchVar = 0.0F;
/*    */   
/*    */   public SFXAction(String key) {
/* 11 */     this.key = key;
/* 12 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public SFXAction(String key, float pitchVar) {
/* 16 */     this.key = key;
/* 17 */     this.pitchVar = pitchVar;
/* 18 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play(this.key, this.pitchVar);
/* 24 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\SFXAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */