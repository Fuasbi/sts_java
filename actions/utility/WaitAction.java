/*    */ package com.megacrit.cardcrawl.actions.utility;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ 
/*    */ public class WaitAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public WaitAction(float setDur)
/*    */   {
/*  9 */     setValues(null, null, 0);
/* 10 */     if ((com.megacrit.cardcrawl.core.Settings.FAST_MODE) && (setDur > 0.1F)) {
/* 11 */       this.duration = 0.1F;
/*    */     } else {
/* 13 */       this.duration = setDur;
/*    */     }
/* 15 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\utility\WaitAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */