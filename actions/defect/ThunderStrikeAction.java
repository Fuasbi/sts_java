/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ThunderStrikeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.01F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.2F;
/*    */   private int numTimes;
/*    */   
/*    */   public ThunderStrikeAction(AbstractCreature target, DamageInfo info, int numTimes)
/*    */   {
/* 19 */     this.info = info;
/* 20 */     this.target = target;
/* 21 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 22 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE;
/* 23 */     this.duration = 0.01F;
/* 24 */     this.numTimes = numTimes;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 29 */     if (this.target == null) {
/* 30 */       this.isDone = true;
/* 31 */       return;
/*    */     }
/*    */     
/* 34 */     if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 35 */       AbstractDungeon.actionManager.clearPostCombatActions();
/* 36 */       this.isDone = true;
/* 37 */       return;
/*    */     }
/*    */     
/* 40 */     if (this.target.currentHealth > 0) {
/* 41 */       this.target.damageFlash = true;
/* 42 */       this.target.damageFlashFrames = 4;
/* 43 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/* 44 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.LightningEffect(this.target.drawX, this.target.drawY));
/* 45 */       com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("ORB_LIGHTNING_EVOKE", 0.1F);
/*    */       
/* 47 */       this.info.applyPowers(this.info.owner, this.target);
/* 48 */       this.target.damage(this.info);
/*    */       
/*    */ 
/* 51 */       if ((this.numTimes > 1) && (!AbstractDungeon.getMonsters().areMonstersBasicallyDead())) {
/* 52 */         this.numTimes -= 1;
/* 53 */         AbstractDungeon.actionManager.addToTop(new ThunderStrikeAction(
/* 54 */           AbstractDungeon.getMonsters().getRandomMonster(true), this.info, this.numTimes));
/*    */       }
/*    */       
/* 57 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.2F));
/*    */     }
/*    */     
/* 60 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\ThunderStrikeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */