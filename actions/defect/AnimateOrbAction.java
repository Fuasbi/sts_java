/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class AnimateOrbAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int orbCount;
/*    */   
/*    */   public AnimateOrbAction(int amount) {
/* 10 */     this.orbCount = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 15 */     for (int i = 0; i < this.orbCount; i++) {
/* 16 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.triggerEvokeAnimation(i);
/*    */     }
/* 18 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\AnimateOrbAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */