/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class DoubleEnergyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public DoubleEnergyAction()
/*    */   {
/* 11 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 17 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainEnergy(EnergyPanel.totalCount);
/*    */     }
/*    */     
/* 20 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\DoubleEnergyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */