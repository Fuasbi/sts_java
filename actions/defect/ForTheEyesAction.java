/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ 
/*    */ public class ForTheEyesAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractMonster m;
/*    */   
/*    */   public ForTheEyesAction(int weakAmt, AbstractMonster m)
/*    */   {
/* 13 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 14 */     this.amount = weakAmt;
/* 15 */     this.m = m;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if ((this.m != null) && ((this.m.intent == AbstractMonster.Intent.ATTACK) || (this.m.intent == AbstractMonster.Intent.ATTACK_BUFF) || (this.m.intent == AbstractMonster.Intent.ATTACK_DEBUFF) || (this.m.intent == AbstractMonster.Intent.ATTACK_DEFEND)))
/*    */     {
/*    */ 
/* 23 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.m, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.WeakPower(this.m, this.amount, false), this.amount));
/*    */     }
/*    */     
/* 26 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\ForTheEyesAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */