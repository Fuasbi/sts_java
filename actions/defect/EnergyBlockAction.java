/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class EnergyBlockAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 10 */   private boolean upg = false;
/*    */   
/*    */   public EnergyBlockAction(boolean upgraded) {
/* 13 */     this.duration = Settings.ACTION_DUR_FAST;
/* 14 */     this.upg = upgraded;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 20 */       if (this.upg) {
/* 21 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, EnergyPanel.totalCount * 2));
/*    */       }
/*    */       else {
/* 24 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, EnergyPanel.totalCount));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 29 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\EnergyBlockAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */