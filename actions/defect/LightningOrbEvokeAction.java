/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.actions.utility.SFXAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LightningOrbEvokeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private boolean hitAll;
/*    */   
/*    */   public LightningOrbEvokeAction(DamageInfo info, boolean hitAll)
/*    */   {
/* 22 */     this.info = info;
/* 23 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 24 */     this.attackEffect = AbstractGameAction.AttackEffect.NONE;
/* 25 */     this.hitAll = hitAll;
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     float speedTime;
/* 30 */     if (!this.hitAll) {
/* 31 */       AbstractCreature m = AbstractDungeon.getRandomMonster();
/*    */       
/* 33 */       if (m != null) {
/* 34 */         speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 35 */         if (Settings.FAST_MODE) {
/* 36 */           speedTime = 0.0F;
/*    */         }
/*    */         
/* 39 */         this.info.output = com.megacrit.cardcrawl.orbs.AbstractOrb.applyLockOn(m, this.info.base);
/* 40 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(m, this.info, AbstractGameAction.AttackEffect.NONE, true));
/* 41 */         AbstractDungeon.actionManager.addToTop(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.LightningEffect(m.drawX, m.drawY), speedTime));
/* 42 */         AbstractDungeon.actionManager.addToTop(new SFXAction("ORB_LIGHTNING_EVOKE"));
/*    */       }
/*    */     } else {
/* 45 */       float speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 46 */       if (Settings.FAST_MODE) {
/* 47 */         speedTime = 0.0F;
/*    */       }
/* 49 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 52 */         DamageInfo.createDamageMatrix(this.info.base, true, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, AbstractGameAction.AttackEffect.NONE));
/*    */       
/*    */ 
/*    */ 
/* 56 */       for (AbstractMonster m3 : AbstractDungeon.getMonsters().monsters) {
/* 57 */         if ((!m3.isDeadOrEscaped()) && (!m3.halfDead)) {
/* 58 */           AbstractDungeon.actionManager.addToTop(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.LightningEffect(m3.drawX, m3.drawY), speedTime));
/*    */         }
/*    */       }
/*    */       
/* 62 */       AbstractDungeon.actionManager.addToTop(new SFXAction("ORB_LIGHTNING_EVOKE"));
/*    */     }
/*    */     
/* 65 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\LightningOrbEvokeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */