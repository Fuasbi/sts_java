/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect;
/*    */ import com.megacrit.cardcrawl.vfx.combat.OrbFlareEffect.OrbFlareColor;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class LightningOrbPassiveAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private AbstractOrb orb;
/*    */   private boolean hitAll;
/*    */   
/*    */   public LightningOrbPassiveAction(DamageInfo info, AbstractOrb orb, boolean hitAll)
/*    */   {
/* 25 */     this.info = info;
/* 26 */     this.orb = orb;
/* 27 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 28 */     this.attackEffect = AbstractGameAction.AttackEffect.NONE;
/* 29 */     this.hitAll = hitAll;
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     float speedTime;
/* 34 */     if (!this.hitAll) {
/* 35 */       AbstractCreature m = AbstractDungeon.getRandomMonster();
/*    */       
/* 37 */       if (m != null) {
/* 38 */         speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 39 */         if (Settings.FAST_MODE) {
/* 40 */           speedTime = 0.0F;
/*    */         }
/*    */         
/* 43 */         this.info.output = AbstractOrb.applyLockOn(m, this.info.base);
/* 44 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(m, this.info, AbstractGameAction.AttackEffect.NONE, true));
/* 45 */         AbstractDungeon.actionManager.addToTop(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.LightningEffect(m.drawX, m.drawY), speedTime));
/* 46 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.SFXAction("ORB_LIGHTNING_EVOKE"));
/* 47 */         if (this.orb != null) {
/* 48 */           AbstractDungeon.actionManager.addToTop(new VFXAction(new OrbFlareEffect(this.orb, OrbFlareEffect.OrbFlareColor.LIGHTNING), speedTime));
/*    */         }
/*    */       }
/*    */     }
/*    */     else {
/* 53 */       float speedTime = 0.2F / AbstractDungeon.player.orbs.size();
/* 54 */       if (Settings.FAST_MODE) {
/* 55 */         speedTime = 0.0F;
/*    */       }
/* 57 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 60 */         DamageInfo.createDamageMatrix(this.info.base, true, true), com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS, AbstractGameAction.AttackEffect.NONE));
/*    */       
/*    */ 
/* 63 */       for (AbstractMonster m3 : AbstractDungeon.getMonsters().monsters) {
/* 64 */         if ((!m3.isDeadOrEscaped()) && (!m3.halfDead)) {
/* 65 */           AbstractDungeon.actionManager.addToTop(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.LightningEffect(m3.drawX, m3.drawY), speedTime));
/*    */         }
/*    */       }
/*    */       
/* 69 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.SFXAction("ORB_LIGHTNING_EVOKE"));
/* 70 */       if (this.orb != null) {
/* 71 */         AbstractDungeon.actionManager.addToTop(new VFXAction(new OrbFlareEffect(this.orb, OrbFlareEffect.OrbFlareColor.LIGHTNING), speedTime));
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 76 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\LightningOrbPassiveAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */