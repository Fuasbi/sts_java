/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class RecycleAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 14 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("RecycleAction");
/* 15 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public RecycleAction() {
/* 19 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 20 */     this.p = AbstractDungeon.player;
/* 21 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 27 */       if (this.p.hand.isEmpty()) {
/* 28 */         this.isDone = true;
/* 29 */         return; }
/* 30 */       if (this.p.hand.size() == 1) {
/* 31 */         if (this.p.hand.getBottomCard().costForTurn == -1) {
/* 32 */           AbstractDungeon.actionManager.addToTop(new GainEnergyAction(EnergyPanel.getCurrentEnergy()));
/* 33 */         } else if (this.p.hand.getBottomCard().costForTurn > 0) {
/* 34 */           AbstractDungeon.actionManager.addToTop(new GainEnergyAction(this.p.hand.getBottomCard().costForTurn));
/*    */         }
/* 36 */         this.p.hand.moveToExhaustPile(this.p.hand.getBottomCard());
/* 37 */         tickDuration();
/* 38 */         return;
/*    */       }
/* 40 */       AbstractDungeon.handCardSelectScreen.open(TEXT[0], 1, false);
/* 41 */       tickDuration();
/* 42 */       return;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 47 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
/* 48 */       for (AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/* 49 */         if (c.costForTurn == -1) {
/* 50 */           AbstractDungeon.actionManager.addToTop(new GainEnergyAction(EnergyPanel.getCurrentEnergy()));
/* 51 */         } else if (c.costForTurn > 0) {
/* 52 */           AbstractDungeon.actionManager.addToTop(new GainEnergyAction(c.costForTurn));
/*    */         }
/* 54 */         this.p.hand.moveToExhaustPile(c);
/*    */       }
/* 56 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/* 57 */       AbstractDungeon.handCardSelectScreen.selectedCards.group.clear();
/*    */     }
/*    */     
/* 60 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\RecycleAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */