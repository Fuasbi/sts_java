/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import com.megacrit.cardcrawl.orbs.EmptyOrbSlot;
/*    */ 
/*    */ public class BlasterAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public BlasterAction()
/*    */   {
/* 13 */     this.duration = Settings.ACTION_DUR_FAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 19 */       int counter = 0;
/* 20 */       for (AbstractOrb o : AbstractDungeon.player.orbs) {
/* 21 */         if (!(o instanceof EmptyOrbSlot)) {
/* 22 */           counter++;
/*    */         }
/*    */       }
/*    */       
/* 26 */       if (counter != 0) {
/* 27 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(counter));
/*    */       }
/*    */     }
/* 30 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\BlasterAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */