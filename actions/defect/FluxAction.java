/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import com.megacrit.cardcrawl.orbs.Plasma;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class FluxAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public FluxAction()
/*    */   {
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SPECIAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 20 */       for (int i = 0; i < AbstractDungeon.player.orbs.size(); i++) {
/* 21 */         if ((!(AbstractDungeon.player.orbs.get(i) instanceof com.megacrit.cardcrawl.orbs.EmptyOrbSlot)) && (!(AbstractDungeon.player.orbs.get(i) instanceof Plasma)))
/*    */         {
/* 23 */           AbstractOrb plasma = new Plasma();
/* 24 */           plasma.cX = ((AbstractOrb)AbstractDungeon.player.orbs.get(i)).cX;
/* 25 */           plasma.cY = ((AbstractOrb)AbstractDungeon.player.orbs.get(i)).cY;
/* 26 */           plasma.setSlot(i, AbstractDungeon.player.maxOrbs);
/* 27 */           AbstractDungeon.player.orbs.set(i, plasma);
/*    */         }
/*    */       }
/*    */     }
/* 31 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\FluxAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */