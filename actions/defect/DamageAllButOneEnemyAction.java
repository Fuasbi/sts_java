/*     */ package com.megacrit.cardcrawl.actions.defect;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DamageAllButOneEnemyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*     */   public int[] damage;
/*  16 */   private boolean firstFrame = true;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DamageAllButOneEnemyAction(AbstractCreature source, AbstractCreature target, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type, AbstractGameAction.AttackEffect effect, boolean isFast)
/*     */   {
/*  25 */     setValues(target, source, amount[0]);
/*  26 */     this.damage = amount;
/*  27 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*  28 */     this.damageType = type;
/*  29 */     this.attackEffect = effect;
/*  30 */     if (isFast) {
/*  31 */       this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*     */     } else {
/*  33 */       this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DamageAllButOneEnemyAction(AbstractCreature source, AbstractCreature target, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type, AbstractGameAction.AttackEffect effect)
/*     */   {
/*  43 */     this(source, target, amount, type, effect, false);
/*     */   }
/*     */   
/*     */   public void update() {
/*     */     boolean playedMusic;
/*  48 */     if (this.firstFrame) {
/*  49 */       playedMusic = false;
/*  50 */       int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/*     */       
/*  52 */       for (int i = 0; i < temp; i++) {
/*  53 */         if ((AbstractDungeon.getCurrRoom().monsters.monsters.get(i) != this.target) && 
/*  54 */           (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDying) && 
/*  55 */           (((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).currentHealth > 0) && (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isEscaping))
/*     */         {
/*  57 */           if (playedMusic) {
/*  58 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*     */             
/*  60 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/*  61 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect, true));
/*     */           }
/*     */           else
/*     */           {
/*  65 */             playedMusic = true;
/*  66 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*     */             
/*  68 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/*  69 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect));
/*     */           }
/*     */         }
/*     */       }
/*     */       
/*  74 */       this.firstFrame = false;
/*     */     }
/*     */     
/*  77 */     tickDuration();
/*     */     
/*  79 */     if (this.isDone) {
/*  80 */       for (com.megacrit.cardcrawl.powers.AbstractPower p : AbstractDungeon.player.powers) {
/*  81 */         p.onDamageAllEnemies(this.damage);
/*     */       }
/*     */       
/*  84 */       int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/*  85 */       for (int i = 0; i < temp; i++) {
/*  86 */         if ((AbstractDungeon.getCurrRoom().monsters.monsters.get(i) != this.target) && 
/*  87 */           (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDeadOrEscaped())) {
/*  88 */           if (this.attackEffect == AbstractGameAction.AttackEffect.POISON) {
/*  89 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.color = Color.CHARTREUSE.cpy();
/*  90 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.changeColor(Color.WHITE.cpy());
/*  91 */           } else if (this.attackEffect == AbstractGameAction.AttackEffect.FIRE) {
/*  92 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.color = Color.RED.cpy();
/*  93 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.changeColor(Color.WHITE.cpy());
/*     */           }
/*  95 */           com.megacrit.cardcrawl.cards.DamageInfo info = new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.damage[i], this.damageType);
/*  96 */           info.applyPowers(this.source, (AbstractCreature)AbstractDungeon.getCurrRoom().monsters.monsters.get(i));
/*  97 */           ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).damage(info);
/*     */         }
/*     */         
/*     */ 
/* 101 */         if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 102 */           AbstractDungeon.actionManager.clearPostCombatActions();
/*     */         }
/* 104 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\DamageAllButOneEnemyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */