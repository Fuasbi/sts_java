/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*    */ 
/*    */ public class DiscardPileToHandAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 11 */   public static final String[] TEXT = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("DiscardPileToHandAction").TEXT;
/*    */   private AbstractPlayer p;
/*    */   
/*    */   public DiscardPileToHandAction(int amount) {
/* 15 */     this.p = AbstractDungeon.player;
/* 16 */     setValues(this.p, AbstractDungeon.player, amount);
/* 17 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     if (this.p.hand.size() >= 10) {
/* 23 */       this.isDone = true; return;
/*    */     }
/*    */     AbstractCard card;
/* 26 */     if (this.p.discardPile.size() == 1) {
/* 27 */       card = (AbstractCard)this.p.discardPile.group.get(0);
/* 28 */       this.p.hand.addToHand(card);
/* 29 */       card.lighten(false);
/* 30 */       this.p.discardPile.removeCard(card);
/* 31 */       this.p.hand.refreshHandLayout();
/* 32 */       this.isDone = true;
/* 33 */       return;
/*    */     }
/*    */     
/* 36 */     if (this.duration == 0.5F) {
/* 37 */       AbstractDungeon.gridSelectScreen.open(this.p.discardPile, this.amount, TEXT[0], false);
/* 38 */       tickDuration();
/* 39 */       return;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 44 */     if (AbstractDungeon.gridSelectScreen.selectedCards.size() != 0) {
/* 45 */       for (AbstractCard c : AbstractDungeon.gridSelectScreen.selectedCards) {
/* 46 */         this.p.hand.addToHand(c);
/* 47 */         this.p.discardPile.removeCard(c);
/* 48 */         c.lighten(false);
/* 49 */         c.unhover();
/*    */       }
/* 51 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 52 */       this.p.hand.refreshHandLayout();
/*    */       
/* 54 */       for (AbstractCard c : this.p.discardPile.group) {
/* 55 */         c.unhover();
/* 56 */         c.target_x = CardGroup.DISCARD_PILE_X;
/* 57 */         c.target_y = 0.0F;
/*    */       }
/* 59 */       this.isDone = true;
/*    */     }
/*    */     
/* 62 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\DiscardPileToHandAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */