/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class ScrapeFollowUpAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ScrapeFollowUpAction()
/*    */   {
/* 12 */     this.duration = Settings.ACTION_DUR_FASTER;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     if (this.duration == Settings.ACTION_DUR_FASTER) {
/* 18 */       for (AbstractCard c : ScrapeAction.scrapedCards) {
/* 19 */         if ((c.costForTurn != 0) && (!c.freeToPlayOnce)) {
/* 20 */           com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.moveToDiscardPile(c);
/* 21 */           c.triggerOnManualDiscard();
/* 22 */           GameActionManager.incrementDiscard(false);
/*    */         }
/*    */       }
/* 25 */       ScrapeAction.scrapedCards.clear();
/*    */     }
/*    */     
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\ScrapeFollowUpAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */