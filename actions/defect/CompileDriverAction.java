/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class CompileDriverAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public CompileDriverAction(AbstractPlayer source, int amount)
/*    */   {
/* 14 */     setValues(this.target, source, amount);
/* 15 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     ArrayList<String> orbList = new ArrayList();
/* 21 */     for (AbstractOrb o : AbstractDungeon.player.orbs) {
/* 22 */       if ((o.ID != null) && (!o.ID.equals("Empty")) && (!orbList.contains(o.ID))) {
/* 23 */         orbList.add(o.ID);
/*    */       }
/*    */     }
/* 26 */     int toDraw = orbList.size() * this.amount;
/* 27 */     if (toDraw > 0) {
/* 28 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(this.source, toDraw));
/*    */     }
/* 30 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\CompileDriverAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */