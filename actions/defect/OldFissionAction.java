/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class OldFissionAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public OldFissionAction()
/*    */   {
/* 10 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 17 */       int orbCount = AbstractDungeon.player.filledOrbCount();
/*    */       
/* 19 */       for (int i = 0; i < orbCount; i++) {
/* 20 */         AbstractDungeon.actionManager.addToBottom(new AnimateOrbAction(1));
/* 21 */         AbstractDungeon.actionManager.addToBottom(new EvokeOrbAction(1));
/*    */       }
/*    */       
/* 24 */       AbstractDungeon.actionManager.addToBottom(new IncreaseMaxOrbAction(orbCount));
/*    */     }
/*    */     
/* 27 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\OldFissionAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */