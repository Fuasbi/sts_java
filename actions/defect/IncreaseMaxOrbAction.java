/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class IncreaseMaxOrbAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public IncreaseMaxOrbAction(int slotIncrease)
/*    */   {
/* 10 */     this.duration = Settings.ACTION_DUR_FAST;
/* 11 */     this.amount = slotIncrease;
/* 12 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 18 */       for (int i = 0; i < this.amount; i++) {
/* 19 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.increaseMaxOrbSlots(1, true);
/*    */       }
/*    */     }
/*    */     
/* 23 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\IncreaseMaxOrbAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */