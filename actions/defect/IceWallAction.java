/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class IceWallAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int perOrbAmt;
/*    */   
/*    */   public IceWallAction(int blockAmt, int perOrbAmt)
/*    */   {
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 14 */     this.amount = blockAmt;
/* 15 */     this.perOrbAmt = perOrbAmt;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 21 */       int count = 0;
/* 22 */       for (int i = 0; i < AbstractDungeon.player.orbs.size(); i++) {
/* 23 */         if ((AbstractDungeon.player.orbs.get(i) instanceof com.megacrit.cardcrawl.orbs.Frost)) {
/* 24 */           count++;
/*    */         }
/*    */       }
/* 27 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, this.amount + count * this.perOrbAmt));
/*    */     }
/*    */     
/*    */ 
/* 31 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\IceWallAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */