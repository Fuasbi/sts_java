/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class FTLAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private AbstractCreature target;
/* 13 */   private int cardPlayCount = 0;
/*    */   
/*    */   public FTLAction(AbstractCreature target, DamageInfo info, int cardPlayCount) {
/* 16 */     this.info = info;
/* 17 */     this.target = target;
/* 18 */     this.cardPlayCount = cardPlayCount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(this.target, this.info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/* 24 */     if (AbstractDungeon.actionManager.cardsPlayedThisTurn.size() - 1 < this.cardPlayCount) {
/* 25 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 1));
/*    */     }
/* 27 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\FTLAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */