/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class SunderAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int energyGainAmt;
/*    */   private DamageInfo info;
/*    */   
/*    */   public SunderAction(AbstractCreature target, DamageInfo info, int energyAmt)
/*    */   {
/* 20 */     this.info = info;
/* 21 */     setValues(target, info);
/* 22 */     this.energyGainAmt = energyAmt;
/* 23 */     this.actionType = AbstractGameAction.ActionType.DAMAGE;
/* 24 */     this.duration = Settings.ACTION_DUR_FASTER;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 29 */     if ((this.duration == Settings.ACTION_DUR_FASTER) && 
/* 30 */       (this.target != null)) {
/* 31 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       
/* 33 */       this.target.damage(this.info);
/*    */       
/* 35 */       if ((((AbstractMonster)this.target).isDying) || (this.target.currentHealth <= 0)) {
/* 36 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.energyGainAmt));
/*    */       }
/*    */       
/* 39 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 40 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 45 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\SunderAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */