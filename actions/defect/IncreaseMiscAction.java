/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class IncreaseMiscAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int miscVal;
/*    */   private int miscIncrease;
/*    */   private String id;
/*    */   
/*    */   public IncreaseMiscAction(String id, int miscValue, int miscIncrease) {
/* 12 */     this.miscIncrease = miscIncrease;
/* 13 */     this.miscVal = miscValue;
/* 14 */     this.id = id;
/*    */   }
/*    */   
/*    */ 
/*    */   public void update()
/*    */   {
/* 20 */     boolean success = false;
/* 21 */     for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.masterDeck.group) {
/* 22 */       if ((c.cardID.equals(this.id)) && (c.misc == this.miscVal)) {
/* 23 */         c.misc += this.miscIncrease;
/* 24 */         c.applyPowers();
/* 25 */         c.baseBlock = c.misc;
/* 26 */         c.isBlockModified = false;
/* 27 */         success = true;
/* 28 */         break;
/*    */       }
/*    */     }
/*    */     
/* 32 */     if (success) {
/* 33 */       System.out.println("Success!");
/*    */     }
/*    */     
/* 36 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\IncreaseMiscAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */