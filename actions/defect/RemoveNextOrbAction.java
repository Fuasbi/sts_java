/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class RemoveNextOrbAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public RemoveNextOrbAction()
/*    */   {
/* 10 */     this.duration = Settings.ACTION_DUR_FAST;
/* 11 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 17 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.removeNextOrb();
/*    */     }
/*    */     
/* 20 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\RemoveNextOrbAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */