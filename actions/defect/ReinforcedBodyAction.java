/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class ReinforcedBodyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public int[] multiDamage;
/* 13 */   private boolean freeToPlayOnce = false;
/*    */   private AbstractPlayer p;
/* 15 */   private int energyOnUse = -1;
/*    */   
/*    */   public ReinforcedBodyAction(AbstractPlayer p, int amount, boolean freeToPlayOnce, int energyOnUse) {
/* 18 */     this.amount = amount;
/* 19 */     this.p = p;
/* 20 */     this.freeToPlayOnce = freeToPlayOnce;
/* 21 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/* 22 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 23 */     this.energyOnUse = energyOnUse;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     int effect = EnergyPanel.totalCount;
/* 29 */     if (this.energyOnUse != -1) {
/* 30 */       effect = this.energyOnUse;
/*    */     }
/*    */     
/* 33 */     if (this.p.hasRelic("Chemical X")) {
/* 34 */       effect += 2;
/* 35 */       this.p.getRelic("Chemical X").flash();
/*    */     }
/*    */     
/* 38 */     if (effect > 0) {
/* 39 */       for (int i = 0; i < effect; i++) {
/* 40 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(this.p, this.p, this.amount));
/*    */       }
/*    */       
/* 43 */       if (!this.freeToPlayOnce) {
/* 44 */         this.p.energy.use(EnergyPanel.totalCount);
/*    */       }
/*    */     }
/* 47 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\ReinforcedBodyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */