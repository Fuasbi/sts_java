/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class AllCostToHandAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractPlayer p;
/*    */   private int costTarget;
/*    */   
/*    */   public AllCostToHandAction(int costToTarget)
/*    */   {
/* 14 */     this.p = AbstractDungeon.player;
/* 15 */     setValues(this.p, AbstractDungeon.player, this.amount);
/* 16 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 17 */     this.costTarget = costToTarget;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 22 */     if (this.p.discardPile.size() > 0) {
/* 23 */       for (AbstractCard card : this.p.discardPile.group) {
/* 24 */         if ((card.cost == this.costTarget) || (card.freeToPlayOnce)) {
/* 25 */           AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.DiscardToHandAction(card));
/*    */         }
/*    */       }
/*    */     }
/* 29 */     tickDuration();
/* 30 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\AllCostToHandAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */