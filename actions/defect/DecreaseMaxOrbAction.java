/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class DecreaseMaxOrbAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public DecreaseMaxOrbAction(int slotDecrease)
/*    */   {
/* 10 */     this.duration = Settings.ACTION_DUR_FAST;
/* 11 */     this.amount = slotDecrease;
/* 12 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 18 */       for (int i = 0; i < this.amount; i++) {
/* 19 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.decreaseMaxOrbSlots(1);
/*    */       }
/*    */     }
/*    */     
/* 23 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\DecreaseMaxOrbAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */