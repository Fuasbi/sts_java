/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class EvokeAllOrbsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public EvokeAllOrbsAction()
/*    */   {
/* 10 */     this.duration = Settings.ACTION_DUR_FAST;
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 17 */       for (int i = 0; i < com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.orbs.size(); i++) {
/* 18 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.evokeOrb();
/*    */       }
/*    */     }
/*    */     
/* 22 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\EvokeAllOrbsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */