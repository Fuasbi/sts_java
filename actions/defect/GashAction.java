/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.blue.Claw;
/*    */ 
/*    */ public class GashAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard card;
/*    */   
/*    */   public GashAction(AbstractCard card, int amount)
/*    */   {
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 14 */     this.card = card;
/* 15 */     this.amount = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 21 */       this.card.baseDamage += this.amount;
/* 22 */       this.card.applyPowers();
/*    */       
/* 24 */       for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.discardPile.group) {
/* 25 */         if ((c instanceof Claw)) {
/* 26 */           c.baseDamage += this.amount;
/* 27 */           c.applyPowers();
/*    */         }
/*    */       }
/*    */       
/* 31 */       for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.group) {
/* 32 */         if ((c instanceof Claw)) {
/* 33 */           c.baseDamage += this.amount;
/* 34 */           c.applyPowers();
/*    */         }
/*    */       }
/*    */       
/* 38 */       for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.group) {
/* 39 */         if ((c instanceof Claw)) {
/* 40 */           c.baseDamage += this.amount;
/* 41 */           c.applyPowers();
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 46 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\GashAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */