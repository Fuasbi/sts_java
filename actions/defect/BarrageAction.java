/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class BarrageAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 12 */   private DamageInfo info = null;
/*    */   private AbstractCreature target;
/*    */   
/*    */   public BarrageAction(AbstractCreature m, DamageInfo info) {
/* 16 */     this.duration = Settings.ACTION_DUR_FAST;
/* 17 */     this.info = info;
/* 18 */     this.target = m;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 24 */       for (int i = 0; i < AbstractDungeon.player.orbs.size(); i++) {
/* 25 */         if (!(AbstractDungeon.player.orbs.get(i) instanceof com.megacrit.cardcrawl.orbs.EmptyOrbSlot)) {
/* 26 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, this.info, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT, true));
/*    */         }
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 32 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\BarrageAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */