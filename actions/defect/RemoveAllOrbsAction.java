/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class RemoveAllOrbsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public RemoveAllOrbsAction()
/*    */   {
/* 10 */     this.duration = Settings.ACTION_DUR_FAST;
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SPECIAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 17 */       while (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.filledOrbCount() > 0) {
/* 18 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.removeNextOrb();
/*    */       }
/*    */     }
/*    */     
/* 22 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\RemoveAllOrbsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */