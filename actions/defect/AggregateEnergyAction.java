/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class AggregateEnergyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int divideAmount;
/*    */   
/*    */   public AggregateEnergyAction(int divideAmountNum)
/*    */   {
/* 12 */     this.duration = Settings.ACTION_DUR_FAST;
/* 13 */     this.divideAmount = divideAmountNum;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 19 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.gainEnergy(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.size() / this.divideAmount);
/*    */     }
/*    */     
/* 22 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\AggregateEnergyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */