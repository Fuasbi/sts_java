/*     */ package com.megacrit.cardcrawl.actions.defect;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.common.EmptyDeckShuffleAction;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.SoulGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.vfx.PlayerTurnEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class ScrapeAction extends AbstractGameAction
/*     */ {
/*  21 */   private boolean shuffleCheck = false;
/*  22 */   private static final Logger logger = LogManager.getLogger(com.megacrit.cardcrawl.actions.common.DrawCardAction.class.getName());
/*  23 */   public static ArrayList<com.megacrit.cardcrawl.cards.AbstractCard> scrapedCards = new ArrayList();
/*     */   
/*     */   public ScrapeAction(AbstractCreature source, int amount, boolean endTurnDraw) {
/*  26 */     if (endTurnDraw) {
/*  27 */       AbstractDungeon.topLevelEffects.add(new PlayerTurnEffect());
/*  28 */     } else if (AbstractDungeon.player.hasPower("No Draw")) {
/*  29 */       AbstractDungeon.player.getPower("No Draw").flash();
/*  30 */       setValues(AbstractDungeon.player, source, amount);
/*  31 */       this.isDone = true;
/*  32 */       this.duration = 0.0F;
/*  33 */       this.actionType = AbstractGameAction.ActionType.WAIT;
/*  34 */       return;
/*     */     }
/*     */     
/*  37 */     setValues(AbstractDungeon.player, source, amount);
/*  38 */     this.actionType = AbstractGameAction.ActionType.DRAW;
/*  39 */     if (Settings.FAST_MODE) {
/*  40 */       this.duration = Settings.ACTION_DUR_XFAST;
/*     */     } else {
/*  42 */       this.duration = Settings.ACTION_DUR_FASTER;
/*     */     }
/*     */   }
/*     */   
/*     */   public ScrapeAction(AbstractCreature source, int amount) {
/*  47 */     this(source, amount, false);
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/*  53 */     if (this.amount <= 0) {
/*  54 */       this.isDone = true;
/*  55 */       return;
/*     */     }
/*     */     
/*  58 */     int deckSize = AbstractDungeon.player.drawPile.size();
/*  59 */     int discardSize = AbstractDungeon.player.discardPile.size();
/*     */     
/*     */ 
/*  62 */     if (SoulGroup.isActive()) {
/*  63 */       return;
/*     */     }
/*     */     
/*     */ 
/*  67 */     if (deckSize + discardSize == 0) {
/*  68 */       this.isDone = true;
/*  69 */       return;
/*     */     }
/*     */     
/*  72 */     if (AbstractDungeon.player.hand.size() == 10) {
/*  73 */       AbstractDungeon.player.createHandIsFullDialog();
/*  74 */       this.isDone = true;
/*  75 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  81 */     if (!this.shuffleCheck) {
/*  82 */       if (this.amount > deckSize) {
/*  83 */         int tmp = this.amount - deckSize;
/*  84 */         AbstractDungeon.actionManager.addToTop(new ScrapeAction(AbstractDungeon.player, tmp));
/*  85 */         AbstractDungeon.actionManager.addToTop(new EmptyDeckShuffleAction());
/*  86 */         if (deckSize != 0) {
/*  87 */           AbstractDungeon.actionManager.addToTop(new ScrapeAction(AbstractDungeon.player, deckSize));
/*     */         }
/*  89 */         this.amount = 0;
/*  90 */         this.isDone = true;
/*     */       }
/*  92 */       this.shuffleCheck = true;
/*     */     }
/*     */     
/*  95 */     this.duration -= Gdx.graphics.getDeltaTime();
/*     */     
/*  97 */     if ((this.amount != 0) && (this.duration < 0.0F)) {
/*  98 */       if (Settings.FAST_MODE) {
/*  99 */         this.duration = Settings.ACTION_DUR_XFAST;
/*     */       } else {
/* 101 */         this.duration = Settings.ACTION_DUR_FASTER;
/*     */       }
/* 103 */       this.amount -= 1;
/*     */       
/* 105 */       if (!AbstractDungeon.player.drawPile.isEmpty()) {
/* 106 */         scrapedCards.add(AbstractDungeon.player.drawPile.getTopCard());
/* 107 */         AbstractDungeon.player.draw();
/* 108 */         AbstractDungeon.player.hand.refreshHandLayout();
/*     */       } else {
/* 110 */         logger.warn("Player attempted to draw from an empty drawpile mid-DrawAction?MASTER DECK: " + AbstractDungeon.player.masterDeck
/*     */         
/* 112 */           .getCardNames());
/* 113 */         this.isDone = true;
/*     */       }
/*     */       
/* 116 */       if (this.amount == 0) {
/* 117 */         this.isDone = true;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\ScrapeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */