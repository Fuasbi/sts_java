/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class FissionAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public FissionAction()
/*    */   {
/* 12 */     this.duration = Settings.ACTION_DUR_XFAST;
/* 13 */     this.actionType = AbstractGameAction.ActionType.ENERGY;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == Settings.ACTION_DUR_XFAST) {
/* 19 */       int orbCount = AbstractDungeon.player.filledOrbCount();
/* 20 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, orbCount));
/* 21 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(orbCount));
/* 22 */       AbstractDungeon.actionManager.addToTop(new RemoveAllOrbsAction());
/*    */     }
/*    */     
/* 25 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\FissionAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */