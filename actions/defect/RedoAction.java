/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ 
/*    */ public class RedoAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractOrb orb;
/*    */   
/*    */   public RedoAction()
/*    */   {
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if ((this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) && 
/* 20 */       (!AbstractDungeon.player.orbs.isEmpty())) {
/* 21 */       this.orb = ((AbstractOrb)AbstractDungeon.player.orbs.get(0));
/* 22 */       if ((this.orb instanceof com.megacrit.cardcrawl.orbs.EmptyOrbSlot)) {
/* 23 */         this.isDone = true;
/*    */       } else {
/* 25 */         AbstractDungeon.player.evokeOrb();
/* 26 */         AbstractDungeon.actionManager.addToTop(new ChannelAction(this.orb, false));
/*    */       }
/*    */     }
/*    */     
/* 30 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\RedoAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */