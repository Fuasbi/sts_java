/*    */ package com.megacrit.cardcrawl.actions.defect;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class EvokeOrbAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int orbCount;
/*    */   
/*    */   public EvokeOrbAction(int amount)
/*    */   {
/* 11 */     this.duration = Settings.ACTION_DUR_FAST;
/* 12 */     this.orbCount = amount;
/* 13 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 19 */       for (int i = 0; i < this.orbCount; i++) {
/* 20 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.evokeOrb();
/*    */       }
/*    */     }
/*    */     
/* 24 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\defect\EvokeOrbAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */