/*    */ package com.megacrit.cardcrawl.actions.animations;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class AnimateHopAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*  8 */   private boolean called = false;
/*    */   
/*    */   public AnimateHopAction(AbstractCreature owner) {
/* 11 */     setValues(null, owner, 0);
/* 12 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 13 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (!this.called) {
/* 19 */       this.source.useHopAnimation();
/* 20 */       this.called = true;
/*    */     }
/* 22 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\animations\AnimateHopAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */