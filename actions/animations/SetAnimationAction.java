/*    */ package com.megacrit.cardcrawl.actions.animations;
/*    */ 
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class SetAnimationAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*  8 */   private boolean called = false;
/*    */   private String animation;
/*    */   
/*    */   public SetAnimationAction(AbstractCreature owner, String animationName) {
/* 12 */     setValues(null, owner, 0);
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 15 */     this.animation = animationName;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (!this.called) {
/* 21 */       this.source.state.setAnimation(0, this.animation, false);
/* 22 */       this.called = true;
/* 23 */       this.source.state.addAnimation(0, "idle", true, 0.0F);
/*    */     }
/* 25 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\animations\SetAnimationAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */