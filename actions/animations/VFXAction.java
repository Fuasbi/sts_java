/*    */ package com.megacrit.cardcrawl.actions.animations;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*    */ 
/*    */ public class VFXAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractGameEffect effect;
/*    */   private float startingDuration;
/* 11 */   private boolean isTopLevelEffect = false;
/*    */   
/*    */   public VFXAction(AbstractGameEffect effect) {
/* 14 */     this(null, effect, 0.0F);
/*    */   }
/*    */   
/*    */   public VFXAction(AbstractGameEffect effect, float duration) {
/* 18 */     this(null, effect, duration);
/*    */   }
/*    */   
/*    */   public VFXAction(AbstractCreature source, AbstractGameEffect effect, float duration) {
/* 22 */     setValues(source, source);
/* 23 */     this.effect = effect;
/* 24 */     this.duration = duration;
/* 25 */     this.startingDuration = duration;
/* 26 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/*    */   }
/*    */   
/*    */   public VFXAction(AbstractCreature source, AbstractGameEffect effect, float duration, boolean topLevel) {
/* 30 */     setValues(source, source);
/* 31 */     this.effect = effect;
/* 32 */     this.duration = duration;
/* 33 */     this.startingDuration = duration;
/* 34 */     this.actionType = AbstractGameAction.ActionType.WAIT;
/* 35 */     this.isTopLevelEffect = topLevel;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 40 */     if (this.duration == this.startingDuration) {
/* 41 */       if (this.isTopLevelEffect) {
/* 42 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.topLevelEffects.add(this.effect);
/*    */       } else {
/* 44 */         com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(this.effect);
/*    */       }
/*    */     }
/*    */     
/* 48 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\animations\VFXAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */