/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDiscardEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class MakeTempCardInDiscardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard cardToMake;
/*    */   private int numCards;
/*    */   
/*    */   public MakeTempCardInDiscardAction(AbstractCard card, int amount)
/*    */   {
/* 16 */     com.megacrit.cardcrawl.unlock.UnlockTracker.markCardAsSeen(card.cardID);
/* 17 */     this.numCards = amount;
/* 18 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 19 */     this.duration = Settings.ACTION_DUR_FAST;
/* 20 */     this.cardToMake = card;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     if (this.duration == Settings.ACTION_DUR_FAST)
/*    */     {
/*    */ 
/* 28 */       if (this.numCards < 6) {
/* 29 */         for (int i = 0; i < this.numCards; i++) {
/* 30 */           AbstractCard c = this.cardToMake.makeStatEquivalentCopy();
/* 31 */           AbstractDungeon.effectList.add(new ShowCardAndAddToDiscardEffect(c));
/*    */         }
/*    */       } else {
/* 34 */         for (int i = 0; i < this.numCards; i++) {
/* 35 */           AbstractCard c = this.cardToMake.makeStatEquivalentCopy();
/* 36 */           AbstractDungeon.effectList.add(new ShowCardAndAddToDiscardEffect(c));
/*    */         }
/*    */       }
/* 39 */       this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     }
/*    */     
/* 42 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\MakeTempCardInDiscardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */