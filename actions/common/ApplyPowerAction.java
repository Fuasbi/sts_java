/*     */ package com.megacrit.cardcrawl.actions.common;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower.PowerType;
/*     */ import com.megacrit.cardcrawl.powers.DexterityPower;
/*     */ import com.megacrit.cardcrawl.powers.NoDrawPower;
/*     */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.PowerBuffEffect;
/*     */ import com.megacrit.cardcrawl.vfx.combat.PowerDebuffEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class ApplyPowerAction
/*     */   extends AbstractGameAction
/*     */ {
/*  41 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ApplyPowerAction");
/*  42 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */ 
/*     */ 
/*     */   private AbstractPower powerToApply;
/*     */   
/*     */ 
/*     */   private float startingDuration;
/*     */   
/*     */ 
/*     */ 
/*     */   public ApplyPowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerToApply, int stackAmount, boolean isFast, AbstractGameAction.AttackEffect effect)
/*     */   {
/*  55 */     if (Settings.FAST_MODE) {
/*  56 */       this.startingDuration = 0.1F;
/*     */     }
/*  58 */     else if (isFast) {
/*  59 */       this.startingDuration = Settings.ACTION_DUR_FASTER;
/*     */     } else {
/*  61 */       this.startingDuration = Settings.ACTION_DUR_FAST;
/*     */     }
/*     */     
/*     */ 
/*  65 */     setValues(target, source, stackAmount);
/*     */     
/*  67 */     this.duration = this.startingDuration;
/*  68 */     this.powerToApply = powerToApply;
/*     */     
/*     */ 
/*  71 */     if ((AbstractDungeon.player.hasRelic("Snake Skull")) && (source != null) && (source.isPlayer) && (target != source) && 
/*  72 */       (powerToApply.ID.equals("Poison"))) {
/*  73 */       AbstractDungeon.player.getRelic("Snake Skull").flash();
/*  74 */       this.powerToApply.amount += 1;
/*  75 */       this.amount += 1;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  80 */     if (powerToApply.ID.equals("Corruption")) {
/*  81 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/*  82 */         if (c.type == AbstractCard.CardType.SKILL)
/*  83 */           c.modifyCostForCombat(-9);
/*     */       }
/*  85 */       for (AbstractCard c : AbstractDungeon.player.drawPile.group) {
/*  86 */         if (c.type == AbstractCard.CardType.SKILL)
/*  87 */           c.modifyCostForCombat(-9);
/*     */       }
/*  89 */       for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/*  90 */         if (c.type == AbstractCard.CardType.SKILL)
/*  91 */           c.modifyCostForCombat(-9);
/*     */       }
/*  93 */       for (AbstractCard c : AbstractDungeon.player.exhaustPile.group) {
/*  94 */         if (c.type == AbstractCard.CardType.SKILL) {
/*  95 */           c.modifyCostForCombat(-9);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 100 */     this.actionType = AbstractGameAction.ActionType.POWER;
/* 101 */     this.attackEffect = effect;
/*     */     
/* 103 */     if (AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 104 */       this.duration = 0.0F;
/* 105 */       this.startingDuration = 0.0F;
/* 106 */       this.isDone = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ApplyPowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerToApply, int stackAmount, boolean isFast)
/*     */   {
/* 116 */     this(target, source, powerToApply, stackAmount, isFast, AbstractGameAction.AttackEffect.NONE);
/*     */   }
/*     */   
/*     */   public ApplyPowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerToApply) {
/* 120 */     this(target, source, powerToApply, -1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ApplyPowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerToApply, int stackAmount)
/*     */   {
/* 128 */     this(target, source, powerToApply, stackAmount, false, AbstractGameAction.AttackEffect.NONE);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ApplyPowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerToApply, int stackAmount, AbstractGameAction.AttackEffect effect)
/*     */   {
/* 137 */     this(target, source, powerToApply, stackAmount, false, effect);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 142 */     if (shouldCancelAction()) {
/* 143 */       this.isDone = true;
/* 144 */       return;
/*     */     }
/*     */     
/* 147 */     if (this.duration == this.startingDuration) {
/* 148 */       if (((this.powerToApply instanceof NoDrawPower)) && (this.target.hasPower(this.powerToApply.ID))) {
/* 149 */         this.isDone = true; return;
/*     */       }
/*     */       
/*     */       Iterator localIterator;
/* 153 */       if (this.source != null) {
/* 154 */         for (localIterator = this.source.powers.iterator(); localIterator.hasNext();) { pow = (AbstractPower)localIterator.next();
/* 155 */           pow.onApplyPower(this.powerToApply, this.target, this.source);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 160 */       if ((AbstractDungeon.player.hasRelic("Champion Belt")) && (this.source != null) && (this.source.isPlayer) && (this.target != this.source) && 
/* 161 */         (this.powerToApply.ID.equals("Vulnerable")) && (!this.target.hasPower("Artifact")))
/*     */       {
/* 163 */         AbstractDungeon.player.getRelic("Champion Belt").onTrigger(this.target);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 168 */       if (((this.target instanceof AbstractMonster)) && 
/* 169 */         (this.target.isDeadOrEscaped())) {
/* 170 */         this.duration = 0.0F;
/* 171 */         this.isDone = true;
/* 172 */         return;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 177 */       if ((this.target.hasPower("Artifact")) && 
/* 178 */         (this.powerToApply.type == AbstractPower.PowerType.DEBUFF)) {
/* 179 */         AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(this.target, TEXT[0]));
/* 180 */         this.duration -= Gdx.graphics.getDeltaTime();
/* 181 */         CardCrawlGame.sound.play("NULLIFY_SFX");
/* 182 */         this.target.getPower("Artifact").flashWithoutSound();
/* 183 */         this.target.getPower("Artifact").onSpecificTrigger();
/* 184 */         return;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 190 */       if ((AbstractDungeon.player.hasRelic("Ginger")) && (this.target.isPlayer) && (this.powerToApply.ID.equals("Weakened")))
/*     */       {
/* 192 */         AbstractDungeon.player.getRelic("Ginger").flash();
/* 193 */         AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(this.target, TEXT[1]));
/* 194 */         this.duration -= Gdx.graphics.getDeltaTime();
/* 195 */         return;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 200 */       if ((AbstractDungeon.player.hasRelic("Turnip")) && (this.target.isPlayer) && (this.powerToApply.ID.equals("Frail")))
/*     */       {
/* 202 */         AbstractDungeon.player.getRelic("Turnip").flash();
/* 203 */         AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(this.target, TEXT[1]));
/* 204 */         this.duration -= Gdx.graphics.getDeltaTime();
/* 205 */         return;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 210 */       if ((AbstractDungeon.player.hasRelic("Nullstone Periapt")) && (this.target.isPlayer) && (this.source != null) && (!this.source.isPlayer) && (this.powerToApply.type == AbstractPower.PowerType.DEBUFF) && 
/* 211 */         (AbstractDungeon.player.getRelic("Nullstone Periapt").counter == -1))
/*     */       {
/* 213 */         AbstractDungeon.player.getRelic("Nullstone Periapt").setCounter(0);
/* 214 */         AbstractDungeon.player.getRelic("Nullstone Periapt").flash();
/* 215 */         AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(this.target, TEXT[2]));
/* 216 */         this.duration -= Gdx.graphics.getDeltaTime();
/* 217 */         return;
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 222 */       AbstractDungeon.effectList.add(new FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*     */       
/*     */ 
/*     */ 
/* 226 */       boolean hasBuffAlready = false;
/* 227 */       for (AbstractPower pow = this.target.powers.iterator(); pow.hasNext();) { p = (AbstractPower)pow.next();
/* 228 */         if ((p.ID.equals(this.powerToApply.ID)) && (!p.ID.equals("Night Terror"))) {
/* 229 */           p.stackPower(this.amount);
/* 230 */           p.flash();
/*     */           
/* 232 */           if ((((p instanceof StrengthPower)) || ((p instanceof DexterityPower))) && (this.amount <= 0)) {
/* 233 */             AbstractDungeon.effectList.add(new PowerDebuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name + TEXT[3]));
/*     */ 
/*     */ 
/*     */ 
/*     */           }
/* 238 */           else if (this.amount > 0) {
/* 239 */             if ((p.type == AbstractPower.PowerType.BUFF) || ((p instanceof StrengthPower)) || ((p instanceof DexterityPower))) {
/* 240 */               AbstractDungeon.effectList.add(new PowerBuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, "+" + 
/*     */               
/*     */ 
/*     */ 
/* 244 */                 Integer.toString(this.amount) + " " + this.powerToApply.name));
/*     */             } else {
/* 246 */               AbstractDungeon.effectList.add(new PowerDebuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, "+" + 
/*     */               
/*     */ 
/*     */ 
/* 250 */                 Integer.toString(this.amount) + " " + this.powerToApply.name));
/*     */             }
/*     */           }
/* 253 */           else if (p.type == AbstractPower.PowerType.BUFF) {
/* 254 */             AbstractDungeon.effectList.add(new PowerBuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name + TEXT[3]));
/*     */ 
/*     */           }
/*     */           else
/*     */           {
/*     */ 
/* 260 */             AbstractDungeon.effectList.add(new PowerDebuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name + TEXT[3]));
/*     */           }
/*     */           
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 268 */           p.updateDescription();
/* 269 */           hasBuffAlready = true;
/* 270 */           AbstractDungeon.onModifyPower();
/*     */         }
/*     */       }
/*     */       AbstractPower p;
/* 274 */       if (this.powerToApply.type == AbstractPower.PowerType.DEBUFF) {
/* 275 */         this.target.useFastShakeAnimation(0.5F);
/*     */       }
/*     */       
/* 278 */       if (!hasBuffAlready) {
/* 279 */         this.target.powers.add(this.powerToApply);
/* 280 */         Collections.sort(this.target.powers);
/* 281 */         this.powerToApply.onInitialApplication();
/* 282 */         this.powerToApply.flash();
/*     */         
/* 284 */         if ((this.amount < 0) && ((this.powerToApply.ID.equals("Strength")) || (this.powerToApply.ID.equals("Dexterity")) || 
/* 285 */           (this.powerToApply.ID.equals("Focus")))) {
/* 286 */           AbstractDungeon.effectList.add(new PowerDebuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name + TEXT[3]));
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         }
/* 292 */         else if (this.powerToApply.type == AbstractPower.PowerType.BUFF) {
/* 293 */           AbstractDungeon.effectList.add(new PowerBuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name));
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/* 299 */           AbstractDungeon.effectList.add(new PowerDebuffEffect(this.target.hb.cX - this.target.animX, this.target.hb.cY + this.target.hb.height / 2.0F, this.powerToApply.name));
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 307 */         AbstractDungeon.onModifyPower();
/*     */         
/*     */ 
/* 310 */         if (this.target.isPlayer) {
/* 311 */           int buffCount = 0;
/* 312 */           for (AbstractPower p : this.target.powers) {
/* 313 */             if (p.type == AbstractPower.PowerType.BUFF) {
/* 314 */               buffCount++;
/*     */             }
/*     */           }
/* 317 */           if (buffCount >= 10) {
/* 318 */             UnlockTracker.unlockAchievement("POWERFUL");
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 324 */     tickDuration();
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ApplyPowerAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */