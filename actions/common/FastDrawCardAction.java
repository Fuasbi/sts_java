/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class FastDrawCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private boolean shuffleCheck = false;
/*    */   
/*    */   public FastDrawCardAction(AbstractCreature source, int amount, boolean endTurnDraw) {
/* 16 */     if (endTurnDraw) {
/* 17 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.PlayerTurnEffect());
/* 18 */     } else if (AbstractDungeon.player.hasPower("No Draw")) {
/* 19 */       AbstractDungeon.player.getPower("No Draw").flash();
/* 20 */       setValues(AbstractDungeon.player, source, amount);
/* 21 */       this.isDone = true;
/* 22 */       this.duration = 0.0F;
/* 23 */       this.actionType = AbstractGameAction.ActionType.WAIT;
/* 24 */       return;
/*    */     }
/*    */     
/* 27 */     setValues(AbstractDungeon.player, source, amount);
/* 28 */     this.actionType = AbstractGameAction.ActionType.DRAW;
/* 29 */     this.duration = Settings.ACTION_DUR_XFAST;
/*    */   }
/*    */   
/*    */   public FastDrawCardAction(AbstractCreature source, int amount) {
/* 33 */     this(source, amount, false);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 38 */     int deckSize = AbstractDungeon.player.drawPile.size();
/* 39 */     int discardSize = AbstractDungeon.player.discardPile.size();
/*    */     
/*    */ 
/* 42 */     if (com.megacrit.cardcrawl.cards.SoulGroup.isActive()) {
/* 43 */       return;
/*    */     }
/*    */     
/*    */ 
/* 47 */     if (deckSize + discardSize == 0) {
/* 48 */       this.isDone = true;
/* 49 */       return;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 55 */     if (!this.shuffleCheck) {
/* 56 */       if (this.amount > deckSize) {
/* 57 */         int tmp = this.amount - deckSize;
/* 58 */         AbstractDungeon.actionManager.addToTop(new FastDrawCardAction(AbstractDungeon.player, tmp));
/* 59 */         AbstractDungeon.actionManager.addToTop(new EmptyDeckShuffleAction());
/* 60 */         if (deckSize != 0) {
/* 61 */           AbstractDungeon.actionManager.addToTop(new FastDrawCardAction(AbstractDungeon.player, deckSize));
/*    */         }
/* 63 */         this.amount = 0;
/* 64 */         this.isDone = true;
/*    */       }
/* 66 */       this.shuffleCheck = true;
/*    */     }
/*    */     
/* 69 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     
/* 71 */     if ((this.amount != 0) && (this.duration < 0.0F)) {
/* 72 */       this.duration = Settings.ACTION_DUR_XFAST;
/* 73 */       this.amount -= 1;
/* 74 */       AbstractDungeon.player.draw();
/* 75 */       AbstractDungeon.player.hand.refreshHandLayout();
/*    */       
/* 77 */       if (this.amount == 0) {
/* 78 */         this.isDone = true;
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\FastDrawCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */