/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class ModifyBlockAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   AbstractCard cardToModify;
/*    */   
/*    */   public ModifyBlockAction(AbstractCard card, int amount) {
/* 10 */     setValues(this.target, this.source, amount);
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 12 */     this.cardToModify = card;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 17 */     this.cardToModify.baseBlock += this.amount;
/* 18 */     if (this.cardToModify.baseDamage < 0) {
/* 19 */       this.cardToModify.baseDamage = 0;
/*    */     }
/* 21 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ModifyBlockAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */