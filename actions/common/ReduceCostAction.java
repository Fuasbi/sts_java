/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ 
/*    */ public class ReduceCostAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard card;
/*    */   
/*    */   public ReduceCostAction(AbstractCard card, int amount)
/*    */   {
/* 11 */     this.card = card;
/* 12 */     this.amount = amount;
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST) {
/* 19 */       this.card.modifyCostForCombat(-1);
/*    */     }
/* 21 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ReduceCostAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */