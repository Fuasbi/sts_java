/*     */ package com.megacrit.cardcrawl.actions.common;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.vfx.GainPennyEffect;
/*     */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class DamageAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*     */   private DamageInfo info;
/*  17 */   private int goldAmount = 0;
/*     */   private static final float DURATION = 0.1F;
/*     */   private static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*  20 */   private boolean skipWait = false;
/*     */   
/*     */   public DamageAction(AbstractCreature target, DamageInfo info, AbstractGameAction.AttackEffect effect) {
/*  23 */     this.info = info;
/*  24 */     setValues(target, info);
/*  25 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/*  26 */     this.attackEffect = effect;
/*  27 */     this.duration = 0.1F;
/*     */   }
/*     */   
/*     */   public DamageAction(AbstractCreature target, DamageInfo info, int stealGoldAmount) {
/*  31 */     this(target, info, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
/*     */   }
/*     */   
/*     */   public DamageAction(AbstractCreature target, DamageInfo info)
/*     */   {
/*  36 */     this(target, info, AbstractGameAction.AttackEffect.NONE);
/*     */   }
/*     */   
/*     */   public DamageAction(AbstractCreature target, DamageInfo info, boolean superFast) {
/*  40 */     this(target, info, AbstractGameAction.AttackEffect.NONE);
/*  41 */     this.skipWait = true;
/*     */   }
/*     */   
/*     */   public DamageAction(AbstractCreature target, DamageInfo info, AbstractGameAction.AttackEffect effect, boolean superFast) {
/*  45 */     this(target, info, effect);
/*  46 */     this.skipWait = true;
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  51 */     if ((shouldCancelAction()) && (this.info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS)) {
/*  52 */       this.isDone = true;
/*  53 */       return;
/*     */     }
/*     */     
/*  56 */     if (this.duration == 0.1F)
/*     */     {
/*  58 */       if ((this.info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && (
/*  59 */         (this.info.owner.isDying) || (this.info.owner.halfDead))) {
/*  60 */         this.isDone = true;
/*  61 */         return;
/*     */       }
/*     */       
/*  64 */       this.target.damageFlash = true;
/*  65 */       this.target.damageFlashFrames = 4;
/*  66 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*     */       
/*  68 */       if (this.goldAmount != 0) {
/*  69 */         stealGold();
/*     */       }
/*     */     }
/*     */     
/*  73 */     tickDuration();
/*     */     
/*  75 */     if (this.isDone) {
/*  76 */       if (this.attackEffect == AbstractGameAction.AttackEffect.POISON) {
/*  77 */         this.target.tint.color = Color.CHARTREUSE.cpy();
/*  78 */         this.target.tint.changeColor(Color.WHITE.cpy());
/*  79 */       } else if (this.attackEffect == AbstractGameAction.AttackEffect.FIRE) {
/*  80 */         this.target.tint.color = Color.RED.cpy();
/*  81 */         this.target.tint.changeColor(Color.WHITE.cpy());
/*     */       }
/*  83 */       this.target.damage(this.info);
/*     */       
/*  85 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/*  86 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*     */       }
/*  88 */       if ((!this.skipWait) && (!com.megacrit.cardcrawl.core.Settings.FAST_MODE)) {
/*  89 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void stealGold()
/*     */   {
/*  98 */     if (this.target.gold == 0)
/*     */     {
/* 100 */       return;
/*     */     }
/*     */     
/* 103 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("GOLD_JINGLE");
/* 104 */     if (this.target.gold < this.goldAmount) {
/* 105 */       this.goldAmount = this.target.gold;
/*     */     }
/*     */     
/* 108 */     this.target.gold -= this.goldAmount;
/* 109 */     for (int i = 0; i < this.goldAmount; i++) {
/* 110 */       if (this.source.isPlayer) {
/* 111 */         AbstractDungeon.effectList.add(new GainPennyEffect(this.target.hb.cX, this.target.hb.cY));
/*     */       } else {
/* 113 */         AbstractDungeon.effectList.add(new GainPennyEffect(this.source, this.target.hb.cX, this.target.hb.cY, this.source.hb.cX, this.source.hb.cY, false));
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\DamageAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */