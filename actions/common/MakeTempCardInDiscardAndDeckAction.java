/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class MakeTempCardInDiscardAndDeckAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard cardToMake;
/*    */   
/*    */   public MakeTempCardInDiscardAndDeckAction(AbstractCard card)
/*    */   {
/* 15 */     UnlockTracker.markCardAsSeen(card.cardID);
/* 16 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/* 18 */     this.cardToMake = card;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 23 */     if (this.duration == Settings.ACTION_DUR_FAST)
/*    */     {
/*    */ 
/* 26 */       AbstractCard tmp = this.cardToMake.makeStatEquivalentCopy();
/* 27 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDrawPileEffect(tmp, 0.0F, 0.0F, true));
/* 28 */       tmp.current_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH / 2.0F - 10.0F * Settings.scale);
/* 29 */       tmp.target_x = tmp.current_x;
/*    */       
/*    */ 
/* 32 */       tmp = this.cardToMake.makeStatEquivalentCopy();
/* 33 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDiscardEffect(tmp));
/* 34 */       tmp.current_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH / 2.0F + 10.0F * Settings.scale);
/* 35 */       tmp.target_x = tmp.current_x;
/*    */     }
/*    */     
/* 38 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\MakeTempCardInDiscardAndDeckAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */