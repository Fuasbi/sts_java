/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class GainBlockAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private static final float DUR = 0.25F;
/*    */   
/*    */   public GainBlockAction(AbstractCreature target, AbstractCreature source, int amount)
/*    */   {
/* 13 */     setValues(target, source, amount);
/* 14 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 15 */     this.duration = 0.25F;
/* 16 */     this.startDuration = 0.25F;
/*    */   }
/*    */   
/*    */   public GainBlockAction(AbstractCreature target, AbstractCreature source, int amount, boolean superFast) {
/* 20 */     setValues(target, source, amount);
/* 21 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/* 22 */     this.duration = 0.1F;
/* 23 */     this.startDuration = 0.1F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if ((!this.target.isDying) && (!this.target.isDead) && 
/* 29 */       (this.duration == this.startDuration)) {
/* 30 */       com.megacrit.cardcrawl.dungeons.AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SHIELD));
/* 31 */       this.target.addBlock(this.amount);
/*    */       
/*    */ 
/* 34 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hand.group) {
/* 35 */         c.applyPowers();
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 40 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\GainBlockAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */