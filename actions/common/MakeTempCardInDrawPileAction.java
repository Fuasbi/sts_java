/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndAddToDrawPileEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class MakeTempCardInDrawPileAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private AbstractCard cardToMake;
/*    */   private boolean randomSpot;
/*    */   private boolean cardOffset;
/*    */   
/*    */   public MakeTempCardInDrawPileAction(AbstractCard card, int amount, boolean randomSpot, boolean cardOffset)
/*    */   {
/* 17 */     com.megacrit.cardcrawl.unlock.UnlockTracker.markCardAsSeen(card.cardID);
/* 18 */     setValues(this.target, this.source, amount);
/* 19 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 20 */     this.duration = 0.5F;
/* 21 */     this.cardToMake = card;
/* 22 */     this.randomSpot = randomSpot;
/* 23 */     this.cardOffset = cardOffset;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if (this.duration == 0.5F)
/*    */     {
/*    */ 
/* 31 */       if (this.amount < 6) {
/* 32 */         for (int i = 0; i < this.amount; i++) {
/* 33 */           AbstractCard c = this.cardToMake.makeStatEquivalentCopy();
/* 34 */           AbstractDungeon.effectList.add(new ShowCardAndAddToDrawPileEffect(c, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, this.randomSpot, this.cardOffset));
/*    */ 
/*    */         }
/*    */         
/*    */ 
/*    */       }
/*    */       else
/*    */       {
/*    */ 
/* 43 */         for (int i = 0; i < this.amount; i++) {
/* 44 */           AbstractCard c = this.cardToMake.makeStatEquivalentCopy();
/* 45 */           AbstractDungeon.effectList.add(new ShowCardAndAddToDrawPileEffect(c, this.randomSpot));
/*    */         }
/*    */       }
/* 48 */       this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*    */     }
/*    */     
/* 51 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\MakeTempCardInDrawPileAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */