/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*    */ import com.megacrit.cardcrawl.monsters.beyond.SnakeDagger;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class ReviveMonsterAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 14 */   private boolean healingEffect = false;
/*    */   
/*    */   public ReviveMonsterAction(AbstractMonster target, AbstractCreature source, boolean healEffect) {
/* 17 */     setValues(target, source, 0);
/* 18 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 19 */     if (com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.hasRelic("Philosopher's Stone")) {
/* 20 */       target.addPower(new StrengthPower(target, 2));
/*    */     }
/*    */     
/* 23 */     this.healingEffect = healEffect;
/*    */   }
/*    */   
/*    */   public ReviveMonsterAction(AbstractMonster target, AbstractCreature source) {
/* 27 */     this(target, source, true);
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 32 */     if ((this.duration == 0.5F) && 
/* 33 */       ((this.target instanceof AbstractMonster))) {
/* 34 */       this.target.isDying = false;
/* 35 */       this.target.heal(this.target.maxHealth, this.healingEffect);
/* 36 */       this.target.healthBarRevivedEvent();
/* 37 */       ((AbstractMonster)this.target).deathTimer = 0.0F;
/* 38 */       ((AbstractMonster)this.target).tint = new com.megacrit.cardcrawl.vfx.TintEffect();
/* 39 */       ((AbstractMonster)this.target).tintFadeOutCalled = false;
/* 40 */       ((AbstractMonster)this.target).isDead = false;
/* 41 */       this.target.powers.clear();
/*    */       
/* 43 */       if ((this.target instanceof SnakeDagger)) {
/* 44 */         ((SnakeDagger)this.target).firstMove = true;
/* 45 */         ((SnakeDagger)this.target).initializeAnimation();
/*    */       }
/*    */       
/* 48 */       ((AbstractMonster)this.target).intent = AbstractMonster.Intent.NONE;
/* 49 */       ((AbstractMonster)this.target).rollMove();
/*    */     }
/*    */     
/*    */ 
/* 53 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ReviveMonsterAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */