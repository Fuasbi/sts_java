/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*    */ import com.megacrit.cardcrawl.ui.buttons.EndTurnButton;
/*    */ 
/*    */ public class EnableEndTurnButtonAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public void update()
/*    */   {
/* 10 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.overlayMenu.endTurnButton.enable();
/* 11 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\EnableEndTurnButtonAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */