/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class ShuffleAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private CardGroup group;
/*    */   
/*    */   public ShuffleAction(CardGroup theGroup)
/*    */   {
/* 12 */     setValues(null, null, 0);
/* 13 */     this.duration = 0.0F;
/* 14 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SHUFFLE;
/* 15 */     this.group = theGroup;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     for (AbstractRelic r : com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.relics) {
/* 21 */       r.onShuffle();
/*    */     }
/* 23 */     this.group.shuffle();
/* 24 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ShuffleAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */