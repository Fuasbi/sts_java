/*     */ package com.megacrit.cardcrawl.actions.common;
/*     */ 
/*     */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.SoulGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.vfx.PlayerTurnEffect;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class DrawCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*     */ {
/*  16 */   private boolean shuffleCheck = false;
/*  17 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DrawCardAction.class.getName());
/*     */   
/*     */   public DrawCardAction(AbstractCreature source, int amount, boolean endTurnDraw) {
/*  20 */     if (endTurnDraw) {
/*  21 */       AbstractDungeon.topLevelEffects.add(new PlayerTurnEffect());
/*  22 */     } else if (AbstractDungeon.player.hasPower("No Draw")) {
/*  23 */       AbstractDungeon.player.getPower("No Draw").flash();
/*  24 */       setValues(AbstractDungeon.player, source, amount);
/*  25 */       this.isDone = true;
/*  26 */       this.duration = 0.0F;
/*  27 */       this.actionType = AbstractGameAction.ActionType.WAIT;
/*  28 */       return;
/*     */     }
/*     */     
/*  31 */     setValues(AbstractDungeon.player, source, amount);
/*  32 */     this.actionType = AbstractGameAction.ActionType.DRAW;
/*  33 */     if (Settings.FAST_MODE) {
/*  34 */       this.duration = Settings.ACTION_DUR_XFAST;
/*     */     } else {
/*  36 */       this.duration = Settings.ACTION_DUR_FASTER;
/*     */     }
/*     */   }
/*     */   
/*     */   public DrawCardAction(AbstractCreature source, int amount) {
/*  41 */     this(source, amount, false);
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/*  47 */     if (this.amount <= 0) {
/*  48 */       this.isDone = true;
/*  49 */       return;
/*     */     }
/*     */     
/*  52 */     int deckSize = AbstractDungeon.player.drawPile.size();
/*  53 */     int discardSize = AbstractDungeon.player.discardPile.size();
/*     */     
/*     */ 
/*  56 */     if (SoulGroup.isActive()) {
/*  57 */       return;
/*     */     }
/*     */     
/*     */ 
/*  61 */     if (deckSize + discardSize == 0) {
/*  62 */       this.isDone = true;
/*  63 */       return;
/*     */     }
/*     */     
/*  66 */     if (AbstractDungeon.player.hand.size() == 10) {
/*  67 */       AbstractDungeon.player.createHandIsFullDialog();
/*  68 */       this.isDone = true;
/*  69 */       return;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  75 */     if (!this.shuffleCheck) {
/*  76 */       if (this.amount > deckSize) {
/*  77 */         int tmp = this.amount - deckSize;
/*  78 */         AbstractDungeon.actionManager.addToTop(new DrawCardAction(AbstractDungeon.player, tmp));
/*  79 */         AbstractDungeon.actionManager.addToTop(new EmptyDeckShuffleAction());
/*  80 */         if (deckSize != 0) {
/*  81 */           AbstractDungeon.actionManager.addToTop(new DrawCardAction(AbstractDungeon.player, deckSize));
/*     */         }
/*  83 */         this.amount = 0;
/*  84 */         this.isDone = true;
/*     */       }
/*  86 */       this.shuffleCheck = true;
/*     */     }
/*     */     
/*  89 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/*     */     
/*  91 */     if ((this.amount != 0) && (this.duration < 0.0F)) {
/*  92 */       if (Settings.FAST_MODE) {
/*  93 */         this.duration = Settings.ACTION_DUR_XFAST;
/*     */       } else {
/*  95 */         this.duration = Settings.ACTION_DUR_FASTER;
/*     */       }
/*  97 */       this.amount -= 1;
/*     */       
/*  99 */       if (!AbstractDungeon.player.drawPile.isEmpty()) {
/* 100 */         AbstractDungeon.player.draw();
/* 101 */         AbstractDungeon.player.hand.refreshHandLayout();
/*     */       } else {
/* 103 */         logger.warn("Player attempted to draw from an empty drawpile mid-DrawAction?MASTER DECK: " + AbstractDungeon.player.masterDeck
/*     */         
/* 105 */           .getCardNames());
/* 106 */         this.isDone = true;
/*     */       }
/*     */       
/* 109 */       if (this.amount == 0) {
/* 110 */         this.isDone = true;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\DrawCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */