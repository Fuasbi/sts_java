/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class GainEnergyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int energyGain;
/*    */   
/*    */   public GainEnergyAction(int amount)
/*    */   {
/* 12 */     setValues(AbstractDungeon.player, AbstractDungeon.player, 0);
/* 13 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */     
/* 15 */     this.energyGain = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST) {
/* 21 */       AbstractDungeon.player.gainEnergy(this.energyGain);
/* 22 */       AbstractDungeon.actionManager.updateEnergyGain(this.energyGain);
/* 23 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.player.hand.group) {
/* 24 */         c.triggerOnGainEnergy(this.energyGain, true);
/*    */       }
/*    */     }
/*    */     
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\GainEnergyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */