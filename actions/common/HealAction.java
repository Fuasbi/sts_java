/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ 
/*    */ public class HealAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public HealAction(AbstractCreature target, AbstractCreature source, int amount)
/*    */   {
/*  9 */     setValues(target, source, amount);
/* 10 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.HEAL;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 15 */     if (this.duration == 0.5F) {
/* 16 */       this.target.heal(this.amount);
/*    */     }
/*    */     
/* 19 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\HealAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */