/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ReducePowerAction
/*    */   extends AbstractGameAction
/*    */ {
/*    */   private String powerID;
/*    */   private AbstractPower powerInstance;
/*    */   
/*    */   public ReducePowerAction(AbstractCreature target, AbstractCreature source, String power, int amount)
/*    */   {
/* 23 */     setValues(target, source, amount);
/* 24 */     this.duration = Settings.ACTION_DUR_FAST;
/* 25 */     this.powerID = power;
/* 26 */     this.actionType = AbstractGameAction.ActionType.REDUCE_POWER;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ReducePowerAction(AbstractCreature target, AbstractCreature source, AbstractPower powerInstance, int amount)
/*    */   {
/* 34 */     setValues(target, source, amount);
/* 35 */     this.duration = Settings.ACTION_DUR_FAST;
/* 36 */     this.powerInstance = powerInstance;
/* 37 */     this.actionType = AbstractGameAction.ActionType.REDUCE_POWER;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 42 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 43 */       AbstractPower reduceMe = null;
/* 44 */       if (this.powerID != null) {
/* 45 */         reduceMe = this.target.getPower(this.powerID);
/* 46 */       } else if (this.powerInstance != null) {
/* 47 */         reduceMe = this.powerInstance;
/*    */       }
/*    */       
/* 50 */       if (reduceMe != null) {
/* 51 */         if (this.amount < reduceMe.amount) {
/* 52 */           reduceMe.reducePower(this.amount);
/* 53 */           reduceMe.updateDescription();
/* 54 */           AbstractDungeon.onModifyPower();
/*    */         } else {
/* 56 */           AbstractDungeon.actionManager.addToTop(new RemoveSpecificPowerAction(this.target, this.source, reduceMe));
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 61 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ReducePowerAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */