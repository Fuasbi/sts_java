/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.vfx.combat.MoveNameEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ShowMoveNameAction
/*    */   extends AbstractGameAction
/*    */ {
/*    */   private String msg;
/*    */   
/*    */   public ShowMoveNameAction(AbstractMonster source, String msg)
/*    */   {
/* 22 */     setValues(source, source);
/* 23 */     this.msg = msg;
/* 24 */     this.actionType = AbstractGameAction.ActionType.TEXT;
/*    */   }
/*    */   
/*    */   public ShowMoveNameAction(AbstractMonster source) {
/* 28 */     setValues(source, source);
/* 29 */     this.msg = source.moveName;
/* 30 */     source.moveName = null;
/* 31 */     this.actionType = AbstractGameAction.ActionType.TEXT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 36 */     if ((this.source != null) && (!this.source.isDying)) {
/* 37 */       AbstractDungeon.effectList.add(new MoveNameEffect(this.source.hb.cX - this.source.animX, this.source.hb.cY + this.source.hb.height / 2.0F, this.msg));
/*    */     }
/*    */     
/* 40 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ShowMoveNameAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */