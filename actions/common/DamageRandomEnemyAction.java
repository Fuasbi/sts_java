/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class DamageRandomEnemyAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.1F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*    */   
/*    */   public DamageRandomEnemyAction(DamageInfo info, AbstractGameAction.AttackEffect effect)
/*    */   {
/* 17 */     this.info = info;
/* 18 */     setValues(AbstractDungeon.getMonsters().getRandomMonster(true), info);
/* 19 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 20 */     this.attackEffect = effect;
/* 21 */     this.duration = 0.1F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (shouldCancelAction()) {
/* 27 */       this.isDone = true;
/* 28 */       return;
/*    */     }
/*    */     
/* 31 */     if (this.duration == 0.1F) {
/* 32 */       this.target.damageFlash = true;
/* 33 */       this.target.damageFlashFrames = 4;
/* 34 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*    */     }
/*    */     
/* 37 */     tickDuration();
/*    */     
/* 39 */     if (this.isDone) {
/* 40 */       if (this.attackEffect == AbstractGameAction.AttackEffect.POISON) {
/* 41 */         this.target.tint.color = Color.CHARTREUSE.cpy();
/* 42 */         this.target.tint.changeColor(Color.WHITE.cpy());
/* 43 */       } else if (this.attackEffect == AbstractGameAction.AttackEffect.FIRE) {
/* 44 */         this.target.tint.color = Color.RED.cpy();
/* 45 */         this.target.tint.changeColor(Color.WHITE.cpy());
/*    */       }
/* 47 */       this.target.damage(this.info);
/*    */       
/* 49 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 50 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/* 52 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\DamageRandomEnemyAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */