/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.UIStrings;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ 
/*    */ public class PutOnDeckAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 12 */   private static final UIStrings uiStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getUIString("PutOnDeckAction");
/* 13 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private AbstractPlayer p;
/*    */   private boolean isRandom;
/*    */   public static int numPlaced;
/*    */   
/*    */   public PutOnDeckAction(AbstractCreature target, AbstractCreature source, int amount, boolean isRandom)
/*    */   {
/* 20 */     this.target = target;
/* 21 */     this.p = ((AbstractPlayer)target);
/* 22 */     setValues(target, source, amount);
/* 23 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/* 24 */     this.isRandom = isRandom;
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     int i;
/* 29 */     if (this.duration == 0.5F) {
/* 30 */       if (this.p.hand.size() < this.amount) {
/* 31 */         this.amount = this.p.hand.size();
/*    */       }
/*    */       
/* 34 */       if (this.isRandom) {
/* 35 */         for (int i = 0; i < this.amount; i++) {
/* 36 */           this.p.hand.moveToDeck(this.p.hand.getRandomCard(false), false);
/*    */         }
/*    */       } else {
/* 39 */         if (this.p.hand.group.size() > this.amount) {
/* 40 */           numPlaced = this.amount;
/* 41 */           AbstractDungeon.handCardSelectScreen.open(TEXT[0], this.amount, false);
/* 42 */           tickDuration();
/* 43 */           return;
/*    */         }
/* 45 */         for (i = 0; i < this.p.hand.size(); i++) {
/* 46 */           this.p.hand.moveToDeck(this.p.hand.getRandomCard(false), this.isRandom);
/*    */         }
/*    */       }
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 54 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
/* 55 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/* 56 */         this.p.hand.moveToDeck(c, false);
/*    */       }
/* 58 */       AbstractDungeon.player.hand.refreshHandLayout();
/* 59 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*    */     }
/*    */     
/* 62 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\PutOnDeckAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */