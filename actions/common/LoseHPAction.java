/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class LoseHPAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private static final float DURATION = 0.33F;
/*    */   
/*    */   public LoseHPAction(AbstractCreature target, AbstractCreature source, int amount)
/*    */   {
/* 16 */     this(target, source, amount, AbstractGameAction.AttackEffect.NONE);
/*    */   }
/*    */   
/*    */   public LoseHPAction(AbstractCreature target, AbstractCreature source, int amount, AbstractGameAction.AttackEffect effect) {
/* 20 */     setValues(target, source, amount);
/* 21 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 22 */     this.attackEffect = effect;
/* 23 */     this.duration = 0.33F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if ((this.duration == 0.33F) && (this.target.currentHealth > 0)) {
/* 29 */       this.target.damageFlash = true;
/* 30 */       this.target.damageFlashFrames = 4;
/* 31 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, this.attackEffect));
/*    */     }
/*    */     
/*    */ 
/* 35 */     tickDuration();
/*    */     
/* 37 */     if (this.isDone) {
/* 38 */       this.target.damage(new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.HP_LOSS));
/*    */       
/* 40 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 41 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/* 43 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\LoseHPAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */