/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ 
/*    */ public class PlayTopCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private boolean exhaustCards;
/*    */   
/*    */   public PlayTopCardAction(com.megacrit.cardcrawl.core.AbstractCreature target, boolean exhausts)
/*    */   {
/* 17 */     this.duration = Settings.ACTION_DUR_FAST;
/* 18 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.WAIT;
/* 19 */     this.source = AbstractDungeon.player;
/* 20 */     this.target = target;
/* 21 */     this.exhaustCards = exhausts;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 26 */     if (this.duration == Settings.ACTION_DUR_FAST) {
/* 27 */       if (AbstractDungeon.player.drawPile.size() + AbstractDungeon.player.discardPile.size() == 0) {
/* 28 */         this.isDone = true;
/* 29 */         return;
/*    */       }
/*    */       
/* 32 */       if (AbstractDungeon.player.drawPile.isEmpty()) {
/* 33 */         AbstractDungeon.actionManager.addToTop(new PlayTopCardAction(this.target, this.exhaustCards));
/* 34 */         AbstractDungeon.actionManager.addToTop(new EmptyDeckShuffleAction());
/* 35 */         this.isDone = true;
/* 36 */         return;
/*    */       }
/*    */       
/* 39 */       if (!AbstractDungeon.player.drawPile.isEmpty()) {
/* 40 */         AbstractCard card = AbstractDungeon.player.drawPile.getTopCard();
/* 41 */         AbstractDungeon.player.drawPile.group.remove(card);
/* 42 */         AbstractDungeon.getCurrRoom().souls.remove(card);
/* 43 */         card.freeToPlayOnce = true;
/* 44 */         card.exhaustOnUseOnce = this.exhaustCards;
/* 45 */         AbstractDungeon.player.limbo.group.add(card);
/* 46 */         card.current_y = (-200.0F * Settings.scale);
/* 47 */         card.target_x = (Settings.WIDTH / 2.0F + 200.0F * Settings.scale);
/* 48 */         card.target_y = (Settings.HEIGHT / 2.0F);
/* 49 */         card.targetAngle = 0.0F;
/* 50 */         card.lighten(false);
/* 51 */         card.drawScale = 0.12F;
/* 52 */         card.targetDrawScale = 0.75F;
/*    */         
/* 54 */         if (!card.canUse(AbstractDungeon.player, (com.megacrit.cardcrawl.monsters.AbstractMonster)this.target)) {
/* 55 */           if (this.exhaustCards) {
/* 56 */             AbstractDungeon.actionManager.addToTop(new ExhaustSpecificCardAction(card, AbstractDungeon.player.limbo));
/*    */           }
/*    */           else
/*    */           {
/* 60 */             AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.UnlimboAction(card));
/* 61 */             AbstractDungeon.actionManager.addToTop(new DiscardSpecificCardAction(card, AbstractDungeon.player.limbo));
/*    */             
/* 63 */             AbstractDungeon.actionManager.addToTop(new WaitAction(0.4F));
/*    */           }
/*    */         } else {
/* 66 */           card.applyPowers();
/* 67 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.QueueCardAction(card, this.target));
/* 68 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.UnlimboAction(card));
/* 69 */           if (!Settings.FAST_MODE) {
/* 70 */             AbstractDungeon.actionManager.addToTop(new WaitAction(Settings.ACTION_DUR_MED));
/*    */           } else {
/* 72 */             AbstractDungeon.actionManager.addToTop(new WaitAction(Settings.ACTION_DUR_FASTER));
/*    */           }
/*    */         }
/*    */       }
/* 76 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\PlayTopCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */