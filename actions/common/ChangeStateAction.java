/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class ChangeStateAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*  7 */   private boolean called = false;
/*    */   private AbstractMonster m;
/*    */   private String stateName;
/*    */   
/*    */   public ChangeStateAction(AbstractMonster monster, String stateName) {
/* 12 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.SPECIAL;
/* 13 */     this.m = monster;
/* 14 */     this.stateName = stateName;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 19 */     if (!this.called) {
/* 20 */       this.m.changeState(this.stateName);
/* 21 */       this.called = true;
/* 22 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ChangeStateAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */