/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class DamageAllEnemiesAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public int[] damage;
/* 16 */   private boolean firstFrame = true;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public DamageAllEnemiesAction(AbstractCreature source, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type, AbstractGameAction.AttackEffect effect, boolean isFast)
/*    */   {
/* 24 */     setValues(null, source, amount[0]);
/* 25 */     this.damage = amount;
/* 26 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 27 */     this.damageType = type;
/* 28 */     this.attackEffect = effect;
/* 29 */     if (isFast) {
/* 30 */       this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*    */     } else {
/* 32 */       this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/*    */     }
/*    */   }
/*    */   
/*    */   public DamageAllEnemiesAction(AbstractCreature source, int[] amount, com.megacrit.cardcrawl.cards.DamageInfo.DamageType type, AbstractGameAction.AttackEffect effect) {
/* 37 */     this(source, amount, type, effect, false);
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     boolean playedMusic;
/* 42 */     if (this.firstFrame) {
/* 43 */       playedMusic = false;
/* 44 */       int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/* 45 */       for (int i = 0; i < temp; i++) {
/* 46 */         if ((!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDying) && 
/* 47 */           (((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).currentHealth > 0) && 
/* 48 */           (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isEscaping)) {
/* 49 */           if (playedMusic) {
/* 50 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*    */             
/* 52 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/* 53 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect, true));
/*    */           }
/*    */           else
/*    */           {
/* 57 */             playedMusic = true;
/* 58 */             AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(
/*    */             
/* 60 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cX, 
/* 61 */               ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).hb.cY, this.attackEffect));
/*    */           }
/*    */         }
/*    */       }
/*    */       
/* 66 */       this.firstFrame = false;
/*    */     }
/*    */     
/* 69 */     tickDuration();
/*    */     
/* 71 */     if (this.isDone) {
/* 72 */       for (com.megacrit.cardcrawl.powers.AbstractPower p : AbstractDungeon.player.powers) {
/* 73 */         p.onDamageAllEnemies(this.damage);
/*    */       }
/*    */       
/* 76 */       int temp = AbstractDungeon.getCurrRoom().monsters.monsters.size();
/* 77 */       for (int i = 0; i < temp; i++) {
/* 78 */         if (!((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).isDeadOrEscaped()) {
/* 79 */           if (this.attackEffect == AbstractGameAction.AttackEffect.POISON) {
/* 80 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.color = Color.CHARTREUSE.cpy();
/* 81 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.changeColor(Color.WHITE.cpy());
/* 82 */           } else if (this.attackEffect == AbstractGameAction.AttackEffect.FIRE) {
/* 83 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.color = Color.RED.cpy();
/* 84 */             ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).tint.changeColor(Color.WHITE.cpy());
/*    */           }
/* 86 */           ((AbstractMonster)AbstractDungeon.getCurrRoom().monsters.monsters.get(i)).damage(new com.megacrit.cardcrawl.cards.DamageInfo(this.source, this.damage[i], this.damageType));
/*    */         }
/*    */       }
/*    */       
/*    */ 
/* 91 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 92 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/* 94 */       if (!com.megacrit.cardcrawl.core.Settings.FAST_MODE) {
/* 95 */         AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\DamageAllEnemiesAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */