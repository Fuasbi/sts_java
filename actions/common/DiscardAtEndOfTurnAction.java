/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.RestoreRetainedCardsAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Collections;
/*    */ import java.util.Iterator;
/*    */ 
/*    */ public class DiscardAtEndOfTurnAction extends AbstractGameAction
/*    */ {
/* 16 */   private static final float DURATION = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_XFAST;
/*    */   
/*    */   public DiscardAtEndOfTurnAction() {
/* 19 */     this.duration = DURATION;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 24 */     if (this.duration == DURATION)
/*    */     {
/*    */ 
/* 27 */       for (Iterator<AbstractCard> c = AbstractDungeon.player.hand.group.iterator(); c.hasNext();) {
/* 28 */         AbstractCard e = (AbstractCard)c.next();
/* 29 */         if (e.retain) {
/* 30 */           AbstractDungeon.player.limbo.addToTop(e);
/* 31 */           c.remove();
/*    */         }
/*    */       }
/* 34 */       AbstractDungeon.actionManager.addToTop(new RestoreRetainedCardsAction(AbstractDungeon.player.limbo));
/*    */       
/*    */       int i;
/* 37 */       if ((!AbstractDungeon.player.hasRelic("Runic Pyramid")) && (!AbstractDungeon.player.hasPower("Equilibrium")))
/*    */       {
/* 39 */         int tempSize = AbstractDungeon.player.hand.size();
/*    */         
/* 41 */         for (i = 0; i < tempSize; i++) {
/* 42 */           AbstractDungeon.actionManager.addToTop(new DiscardAction(AbstractDungeon.player, null, AbstractDungeon.player.hand
/*    */           
/*    */ 
/*    */ 
/* 46 */             .size(), true, true));
/*    */         }
/*    */       }
/*    */       
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 54 */       ArrayList<AbstractCard> cards = (ArrayList)AbstractDungeon.player.hand.group.clone();
/* 55 */       Collections.shuffle(cards);
/* 56 */       for (AbstractCard c : cards) {
/* 57 */         c.triggerOnEndOfPlayerTurn();
/*    */       }
/* 59 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\DiscardAtEndOfTurnAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */