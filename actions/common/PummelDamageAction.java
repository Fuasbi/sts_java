/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class PummelDamageAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private DamageInfo info;
/*    */   private static final float DURATION = 0.01F;
/*    */   private static final float POST_ATTACK_WAIT_DUR = 0.1F;
/*    */   
/*    */   public PummelDamageAction(AbstractCreature target, DamageInfo info)
/*    */   {
/* 19 */     this.info = info;
/* 20 */     setValues(target, info);
/* 21 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.DAMAGE;
/* 22 */     this.attackEffect = com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_LIGHT;
/* 23 */     this.duration = 0.01F;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 28 */     if ((this.duration == 0.01F) && (this.target != null) && (this.target.currentHealth > 0))
/*    */     {
/* 30 */       if ((this.info.type != com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS) && 
/* 31 */         (this.info.owner.isDying)) {
/* 32 */         this.isDone = true;
/* 33 */         return;
/*    */       }
/*    */       
/* 36 */       this.target.damageFlash = true;
/* 37 */       this.target.damageFlashFrames = 4;
/* 38 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX + 
/*    */       
/* 40 */         MathUtils.random(-100.0F * Settings.scale, 100.0F * Settings.scale), this.target.hb.cY + 
/* 41 */         MathUtils.random(-100.0F * Settings.scale, 100.0F * Settings.scale), this.attackEffect));
/*    */     }
/*    */     
/*    */ 
/* 45 */     tickDuration();
/*    */     
/* 47 */     if ((this.isDone) && (this.target != null) && (this.target.currentHealth > 0)) {
/* 48 */       this.target.damage(this.info);
/*    */       
/* 50 */       if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
/* 51 */         AbstractDungeon.actionManager.clearPostCombatActions();
/*    */       }
/* 53 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.WaitAction(0.1F));
/*    */     }
/*    */     
/* 56 */     if (this.target == null) {
/* 57 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\PummelDamageAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */