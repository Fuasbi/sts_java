/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.screens.select.HandCardSelectScreen;
/*    */ 
/*    */ public class ExhaustAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 13 */   private static final com.megacrit.cardcrawl.localization.UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("ExhaustAction");
/* 14 */   public static final String[] TEXT = uiStrings.TEXT;
/*    */   private AbstractPlayer p;
/*    */   private boolean isRandom;
/* 17 */   private boolean anyNumber; private boolean canPickZero = false;
/*    */   public static int numExhausted;
/*    */   
/*    */   public ExhaustAction(AbstractCreature target, AbstractCreature source, int amount, boolean isRandom) {
/* 21 */     this(target, source, amount, isRandom, false, false);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public ExhaustAction(AbstractCreature target, AbstractCreature source, int amount, boolean isRandom, boolean anyNumber, boolean canPickZero)
/*    */   {
/* 31 */     this.anyNumber = anyNumber;
/* 32 */     this.canPickZero = canPickZero;
/* 33 */     this.p = ((AbstractPlayer)target);
/* 34 */     this.isRandom = isRandom;
/* 35 */     setValues(target, source, amount);
/* 36 */     this.duration = com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST;
/* 37 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.EXHAUST;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public ExhaustAction(AbstractCreature target, AbstractCreature source, int amount, boolean isRandom, boolean anyNumber)
/*    */   {
/* 46 */     this(target, source, amount, isRandom, anyNumber, false);
/*    */   }
/*    */   
/*    */   public void update() {
/*    */     int i;
/* 51 */     if (this.duration == com.megacrit.cardcrawl.core.Settings.ACTION_DUR_FAST)
/*    */     {
/*    */ 
/* 54 */       if (this.p.hand.size() == 0) {
/* 55 */         this.isDone = true;
/* 56 */         return;
/*    */       }
/*    */       
/* 59 */       if ((!this.anyNumber) && 
/* 60 */         (this.p.hand.size() <= this.amount)) {
/* 61 */         this.amount = this.p.hand.size();
/* 62 */         numExhausted = this.amount;
/* 63 */         int tmp = this.p.hand.size();
/* 64 */         for (int i = 0; i < tmp; i++) {
/* 65 */           AbstractCard c = this.p.hand.getTopCard();
/* 66 */           this.p.hand.moveToExhaustPile(c);
/*    */         }
/* 68 */         CardCrawlGame.dungeon.checkForPactAchievement();
/* 69 */         return;
/*    */       }
/*    */       
/*    */ 
/* 73 */       if (this.isRandom) {
/* 74 */         for (i = 0; i < this.amount; i++) {
/* 75 */           this.p.hand.moveToExhaustPile(this.p.hand.getRandomCard(true));
/*    */         }
/* 77 */         CardCrawlGame.dungeon.checkForPactAchievement();
/*    */       } else {
/* 79 */         numExhausted = this.amount;
/* 80 */         AbstractDungeon.handCardSelectScreen.open(TEXT[0], this.amount, this.anyNumber, this.canPickZero);
/* 81 */         tickDuration();
/* 82 */         return;
/*    */       }
/*    */     }
/*    */     
/*    */ 
/*    */ 
/* 88 */     if (!AbstractDungeon.handCardSelectScreen.wereCardsRetrieved) {
/* 89 */       for (AbstractCard c : AbstractDungeon.handCardSelectScreen.selectedCards.group) {
/* 90 */         this.p.hand.moveToExhaustPile(c);
/*    */       }
/* 92 */       CardCrawlGame.dungeon.checkForPactAchievement();
/* 93 */       AbstractDungeon.handCardSelectScreen.wereCardsRetrieved = true;
/*    */     }
/*    */     
/* 96 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\ExhaustAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */