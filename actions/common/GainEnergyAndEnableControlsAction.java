/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*    */ 
/*    */ public class GainEnergyAndEnableControlsAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   private int energyGain;
/*    */   
/*    */   public GainEnergyAndEnableControlsAction(int amount)
/*    */   {
/* 13 */     setValues(AbstractDungeon.player, AbstractDungeon.player, 0);
/*    */     
/* 15 */     this.energyGain = amount;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 20 */     if (this.duration == 0.5F) {
/* 21 */       AbstractDungeon.player.gainEnergy(this.energyGain);
/* 22 */       AbstractDungeon.actionManager.updateEnergyGain(this.energyGain);
/* 23 */       for (com.megacrit.cardcrawl.cards.AbstractCard c : AbstractDungeon.player.hand.group) {
/* 24 */         c.triggerOnGainEnergy(this.energyGain, false);
/*    */       }
/* 26 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 27 */         r.onEnergyRecharge();
/*    */       }
/* 29 */       for (com.megacrit.cardcrawl.powers.AbstractPower p : AbstractDungeon.player.powers) {
/* 30 */         p.onEnergyRecharge();
/*    */       }
/* 32 */       AbstractDungeon.actionManager.turnHasEnded = false;
/*    */     }
/*    */     
/* 35 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\GainEnergyAndEnableControlsAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */