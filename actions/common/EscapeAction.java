/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class EscapeAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public EscapeAction(AbstractMonster source)
/*    */   {
/*  9 */     setValues(source, source);
/* 10 */     this.duration = 0.5F;
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.TEXT;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == 0.5F) {
/* 17 */       AbstractMonster m = (AbstractMonster)this.source;
/* 18 */       m.escape();
/*    */     }
/*    */     
/* 21 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\EscapeAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */