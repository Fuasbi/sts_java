/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.powers.MinionPower;
/*    */ import com.megacrit.cardcrawl.powers.SlowPower;
/*    */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ public class SpawnMonsterAction extends AbstractGameAction
/*    */ {
/* 19 */   private boolean used = false;
/*    */   private static final float DURATION = 0.1F;
/*    */   private AbstractMonster m;
/*    */   private boolean minion;
/*    */   private int targetSlot;
/*    */   
/*    */   public SpawnMonsterAction(AbstractMonster m, boolean isMinion) {
/* 26 */     this(m, isMinion, -99);
/*    */   }
/*    */   
/*    */   public SpawnMonsterAction(AbstractMonster m, boolean isMinion, int slot) {
/* 30 */     this.actionType = AbstractGameAction.ActionType.SPECIAL;
/* 31 */     this.duration = 0.1F;
/* 32 */     this.m = m;
/* 33 */     this.minion = isMinion;
/* 34 */     this.targetSlot = slot;
/*    */     
/* 36 */     if (AbstractDungeon.player.hasRelic("Philosopher's Stone")) {
/* 37 */       m.addPower(new StrengthPower(m, 2));
/*    */     }
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 43 */     if (!this.used)
/*    */     {
/* 45 */       this.m.init();
/* 46 */       this.m.applyPowers();
/*    */       
/* 48 */       if (this.targetSlot < 0) {
/* 49 */         AbstractDungeon.getCurrRoom().monsters.addSpawnedMonster(this.m);
/*    */       } else {
/* 51 */         AbstractDungeon.getCurrRoom().monsters.addMonster(this.targetSlot, this.m);
/*    */       }
/* 53 */       this.m.showHealthBar();
/* 54 */       if (((Boolean)DailyMods.negativeMods.get("Lethality")).booleanValue()) {
/* 55 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.m, new StrengthPower(this.m, 3), 3));
/*    */       }
/*    */       
/*    */ 
/* 59 */       if (((Boolean)DailyMods.negativeMods.get("Time Dilation")).booleanValue()) {
/* 60 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this.m, this.m, new SlowPower(this.m, 0)));
/*    */       }
/*    */       
/* 63 */       if (this.minion) {
/* 64 */         AbstractDungeon.actionManager.addToTop(new ApplyPowerAction(this.m, this.m, new MinionPower(this.m)));
/*    */       }
/* 66 */       this.used = true;
/*    */     }
/*    */     
/* 69 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\SpawnMonsterAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */