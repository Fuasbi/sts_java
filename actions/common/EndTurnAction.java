/*    */ package com.megacrit.cardcrawl.actions.common;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.vfx.EnemyTurnEffect;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class EndTurnAction
/*    */   extends AbstractGameAction
/*    */ {
/*    */   public void update()
/*    */   {
/* 14 */     AbstractDungeon.actionManager.endTurn();
/* 15 */     AbstractDungeon.topLevelEffects.add(new EnemyTurnEffect());
/* 16 */     this.isDone = true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\common\EndTurnAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */