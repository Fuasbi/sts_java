/*     */ package com.megacrit.cardcrawl.actions;
/*     */ 
/*     */ import com.megacrit.cardcrawl.actions.common.DrawCardAction;
/*     */ import com.megacrit.cardcrawl.actions.common.EnableEndTurnButtonAction;
/*     */ import com.megacrit.cardcrawl.actions.common.HealAction;
/*     */ import com.megacrit.cardcrawl.actions.common.ShowMoveNameAction;
/*     */ import com.megacrit.cardcrawl.actions.defect.TriggerEndOfTurnOrbsAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*     */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardTarget;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.CardQueueItem;
/*     */ import com.megacrit.cardcrawl.cards.colorless.Shiv;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster.Intent;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterQueueItem;
/*     */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.UnceasingTop;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.ThoughtBubble;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ public class GameActionManager
/*     */ {
/*  42 */   private static final Logger logger = LogManager.getLogger(GameActionManager.class.getName());
/*     */   
/*  44 */   private ArrayList<AbstractGameAction> nextCombatActions = new ArrayList();
/*  45 */   public ArrayList<AbstractGameAction> actions = new ArrayList();
/*  46 */   public ArrayList<AbstractGameAction> preTurnActions = new ArrayList();
/*  47 */   public ArrayList<CardQueueItem> cardQueue = new ArrayList();
/*  48 */   public ArrayList<MonsterQueueItem> monsterQueue = new ArrayList();
/*  49 */   public ArrayList<AbstractCard> cardsPlayedThisTurn = new ArrayList();
/*  50 */   public ArrayList<AbstractCard> cardsPlayedThisCombat = new ArrayList();
/*  51 */   public ArrayList<AbstractOrb> orbsChanneledThisCombat = new ArrayList();
/*  52 */   public ArrayList<AbstractOrb> orbsChanneledThisTurn = new ArrayList();
/*     */   public AbstractGameAction currentAction;
/*  54 */   public AbstractGameAction previousAction; public AbstractGameAction turnStartCurrentAction; public AbstractCard lastCard = null;
/*  55 */   public Phase phase = Phase.WAITING_ON_USER;
/*  56 */   public boolean hasControl = true;
/*  57 */   public boolean turnHasEnded = false; public boolean usingCard = false; public boolean monsterAttacksQueued = true;
/*  58 */   public static int totalDiscardedThisTurn = 0; public static int damageReceivedThisTurn = 0; public static int damageReceivedThisCombat = 0;
/*  59 */   public static int hpLossThisCombat = 0;
/*  60 */   public static int playerHpLastTurn; public static int energyGainedThisCombat; public static int turn = 0;
/*     */   
/*     */   public static enum Phase {
/*  63 */     WAITING_ON_USER,  EXECUTING_ACTIONS;
/*     */     
/*     */ 
/*     */     private Phase() {}
/*     */   }
/*     */   
/*     */ 
/*     */   public void addToNextCombat(AbstractGameAction action)
/*     */   {
/*  72 */     this.nextCombatActions.add(action);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void useNextCombatActions()
/*     */   {
/*  79 */     for (AbstractGameAction a : this.nextCombatActions) {
/*  80 */       addToBottom(a);
/*     */     }
/*  82 */     this.nextCombatActions.clear();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addToBottom(AbstractGameAction action)
/*     */   {
/*  92 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/*  93 */       this.actions.add(action);
/*     */     }
/*     */   }
/*     */   
/*     */   public void removeFromQueue(AbstractCard c) {
/*  98 */     int index = -1;
/*  99 */     for (int i = 0; i < this.cardQueue.size(); i++) {
/* 100 */       if ((((CardQueueItem)this.cardQueue.get(i)).card != null) && (((CardQueueItem)this.cardQueue.get(i)).card.equals(c))) {
/* 101 */         index = i;
/* 102 */         break;
/*     */       }
/*     */     }
/*     */     
/* 106 */     if (index != -1) {
/* 107 */       this.cardQueue.remove(index);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void clearPostCombatActions()
/*     */   {
/* 115 */     for (Iterator<AbstractGameAction> i = this.actions.iterator(); i.hasNext();) {
/* 116 */       AbstractGameAction e = (AbstractGameAction)i.next();
/* 117 */       if ((!(e instanceof HealAction)) && (!(e instanceof UseCardAction)) && (e.actionType != AbstractGameAction.ActionType.DAMAGE))
/*     */       {
/* 119 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addToTop(AbstractGameAction action)
/*     */   {
/* 131 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 132 */       this.actions.add(0, action);
/*     */     }
/*     */   }
/*     */   
/*     */   public void addToTurnStart(AbstractGameAction action) {
/* 137 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 138 */       this.preTurnActions.add(0, action);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 146 */     switch (this.phase) {
/*     */     case WAITING_ON_USER: 
/* 148 */       getNextAction();
/* 149 */       break;
/*     */     case EXECUTING_ACTIONS: 
/* 151 */       if ((this.currentAction != null) && (!this.currentAction.isDone)) {
/* 152 */         this.currentAction.update();
/*     */       } else {
/* 154 */         this.previousAction = this.currentAction;
/* 155 */         this.currentAction = null;
/* 156 */         getNextAction();
/*     */         
/*     */ 
/* 159 */         if ((this.currentAction == null) && (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) && (!this.usingCard))
/*     */         {
/* 161 */           this.phase = Phase.WAITING_ON_USER;
/* 162 */           AbstractDungeon.player.hand.refreshHandLayout();
/* 163 */           this.hasControl = false;
/*     */         }
/* 165 */         this.usingCard = false;
/*     */       }
/* 167 */       break;
/*     */     default: 
/* 169 */       logger.info("This should never be called");
/*     */     }
/*     */   }
/*     */   
/*     */   public void endTurn()
/*     */   {
/* 175 */     this.turnHasEnded = true;
/* 176 */     playerHpLastTurn = AbstractDungeon.player.currentHealth;
/*     */   }
/*     */   
/*     */   private void getNextAction()
/*     */   {
/* 181 */     if (!this.actions.isEmpty()) {
/* 182 */       this.currentAction = ((AbstractGameAction)this.actions.remove(0));
/* 183 */       this.phase = Phase.EXECUTING_ACTIONS;
/* 184 */       this.hasControl = true;
/*     */ 
/*     */     }
/* 187 */     else if (!this.preTurnActions.isEmpty()) {
/* 188 */       this.currentAction = ((AbstractGameAction)this.preTurnActions.remove(0));
/* 189 */       this.phase = Phase.EXECUTING_ACTIONS;
/* 190 */       this.hasControl = true;
/*     */     }
/* 192 */     else if (!this.cardQueue.isEmpty()) {
/* 193 */       this.usingCard = true;
/* 194 */       AbstractCard c = ((CardQueueItem)this.cardQueue.get(0)).card;
/*     */       
/*     */ 
/* 197 */       if (c == null) {
/* 198 */         callEndOfTurnActions();
/* 199 */       } else if (c.equals(this.lastCard)) {
/* 200 */         logger.info("Last card! " + c.name);
/* 201 */         this.lastCard = null;
/*     */       }
/*     */       
/*     */       AbstractRelic top;
/* 205 */       if ((this.cardQueue.size() == 1) && (((CardQueueItem)this.cardQueue.get(0)).isEndTurnAutoPlay)) {
/* 206 */         top = AbstractDungeon.player.getRelic("Unceasing Top");
/* 207 */         if (top != null) {
/* 208 */           ((UnceasingTop)top).disableUntilTurnEnds();
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 213 */       if ((((CardQueueItem)this.cardQueue.get(0)).card != null) && ((c.canUse(AbstractDungeon.player, ((CardQueueItem)this.cardQueue.get(0)).monster)) || 
/* 214 */         (((CardQueueItem)this.cardQueue.get(0)).card.dontTriggerOnUseCard))) {
/* 215 */         AbstractDungeon.player.cardsPlayedThisTurn += 1;
/* 216 */         ((CardQueueItem)this.cardQueue.get(0)).card.energyOnUse = ((CardQueueItem)this.cardQueue.get(0)).energyOnUse;
/*     */         AbstractCard card;
/* 218 */         if (!((CardQueueItem)this.cardQueue.get(0)).card.dontTriggerOnUseCard) {
/* 219 */           for (AbstractPower p : AbstractDungeon.player.powers) {
/* 220 */             p.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/*     */           
/* 223 */           for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 224 */             r.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/*     */           
/* 227 */           for (AbstractBlight b : AbstractDungeon.player.blights) {
/* 228 */             b.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/*     */           
/* 231 */           for (AbstractCard card : AbstractDungeon.player.hand.group) {
/* 232 */             card.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/* 234 */           for (AbstractCard card : AbstractDungeon.player.discardPile.group) {
/* 235 */             card.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/*     */           
/* 238 */           for (top = AbstractDungeon.player.drawPile.group.iterator(); top.hasNext();) { card = (AbstractCard)top.next();
/* 239 */             card.onPlayCard(((CardQueueItem)this.cardQueue.get(0)).card, ((CardQueueItem)this.cardQueue.get(0)).monster);
/*     */           }
/*     */           
/* 242 */           this.cardsPlayedThisTurn.add(((CardQueueItem)this.cardQueue.get(0)).card);
/*     */         }
/*     */         
/*     */ 
/* 246 */         if (this.cardsPlayedThisTurn.size() == 25) {
/* 247 */           UnlockTracker.unlockAchievement("INFINITY");
/*     */         }
/*     */         
/*     */ 
/* 251 */         if ((this.cardsPlayedThisTurn.size() >= 20) && (!CardCrawlGame.combo)) {
/* 252 */           CardCrawlGame.combo = true;
/*     */         }
/*     */         
/*     */         int shivCount;
/* 256 */         if ((((CardQueueItem)this.cardQueue.get(0)).card instanceof Shiv)) {
/* 257 */           shivCount = 0;
/* 258 */           for (AbstractCard i : this.cardsPlayedThisTurn) {
/* 259 */             if ((i instanceof Shiv)) {
/* 260 */               shivCount++;
/* 261 */               if (shivCount == 10) {
/* 262 */                 UnlockTracker.unlockAchievement("NINJA");
/* 263 */                 break;
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */         
/*     */ 
/* 270 */         if (((CardQueueItem)this.cardQueue.get(0)).card != null) {
/* 271 */           if ((((CardQueueItem)this.cardQueue.get(0)).card.target == AbstractCard.CardTarget.ENEMY) && ((((CardQueueItem)this.cardQueue.get(0)).monster == null) || 
/* 272 */             (((CardQueueItem)this.cardQueue.get(0)).monster.isDying)))
/*     */           {
/* 274 */             for (Iterator<AbstractCard> i = AbstractDungeon.player.limbo.group.iterator(); i.hasNext();) {
/* 275 */               AbstractCard e = (AbstractCard)i.next();
/* 276 */               if (e == ((CardQueueItem)this.cardQueue.get(0)).card) {
/* 277 */                 ((CardQueueItem)this.cardQueue.get(0)).card.fadingOut = true;
/* 278 */                 AbstractDungeon.effectList.add(new ExhaustCardEffect(((CardQueueItem)this.cardQueue.get(0)).card));
/* 279 */                 i.remove();
/*     */               }
/*     */             }
/*     */             
/* 283 */             if (((CardQueueItem)this.cardQueue.get(0)).monster == null) {
/* 284 */               ((CardQueueItem)this.cardQueue.get(0)).card.drawScale = ((CardQueueItem)this.cardQueue.get(0)).card.targetDrawScale;
/* 285 */               ((CardQueueItem)this.cardQueue.get(0)).card.angle = ((CardQueueItem)this.cardQueue.get(0)).card.targetAngle;
/* 286 */               ((CardQueueItem)this.cardQueue.get(0)).card.current_x = ((CardQueueItem)this.cardQueue.get(0)).card.target_x;
/* 287 */               ((CardQueueItem)this.cardQueue.get(0)).card.current_y = ((CardQueueItem)this.cardQueue.get(0)).card.target_y;
/* 288 */               AbstractDungeon.effectList.add(new ExhaustCardEffect(((CardQueueItem)this.cardQueue.get(0)).card));
/*     */             }
/*     */           }
/*     */           else
/*     */           {
/* 293 */             this.cardsPlayedThisCombat.add(((CardQueueItem)this.cardQueue.get(0)).card);
/* 294 */             AbstractDungeon.player.useCard(
/* 295 */               ((CardQueueItem)this.cardQueue.get(0)).card, 
/* 296 */               ((CardQueueItem)this.cardQueue.get(0)).monster, 
/* 297 */               ((CardQueueItem)this.cardQueue.get(0)).energyOnUse);
/*     */           }
/*     */         }
/*     */       } else {
/* 301 */         for (Iterator<AbstractCard> i = AbstractDungeon.player.limbo.group.iterator(); i.hasNext();) {
/* 302 */           AbstractCard e = (AbstractCard)i.next();
/* 303 */           if (e == c) {
/* 304 */             c.fadingOut = true;
/* 305 */             AbstractDungeon.effectList.add(new ExhaustCardEffect(c));
/* 306 */             i.remove();
/*     */           }
/*     */         }
/*     */         
/* 310 */         if (c != null) {
/* 311 */           AbstractDungeon.effectList.add(new ThoughtBubble(AbstractDungeon.player.dialogX, AbstractDungeon.player.dialogY, 3.0F, c.cantUseMessage, true));
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 321 */       this.cardQueue.remove(0);
/* 322 */     } else if (!this.monsterAttacksQueued) {
/* 323 */       this.monsterAttacksQueued = true;
/* 324 */       AbstractDungeon.getCurrRoom().monsters.queueMonsters();
/* 325 */     } else if (!this.monsterQueue.isEmpty()) {
/* 326 */       AbstractMonster m = ((MonsterQueueItem)this.monsterQueue.get(0)).monster;
/* 327 */       if ((!m.isDeadOrEscaped()) || (m.halfDead)) {
/* 328 */         if (m.intent != AbstractMonster.Intent.NONE) {
/* 329 */           AbstractDungeon.actionManager.addToBottom(new ShowMoveNameAction(m));
/* 330 */           AbstractDungeon.actionManager.addToBottom(new IntentFlashAction(m));
/*     */         }
/*     */         
/*     */ 
/* 334 */         if ((!((Boolean)TipTracker.tips.get("INTENT_TIP")).booleanValue()) && (AbstractDungeon.player.currentBlock == 0) && ((m.intent == AbstractMonster.Intent.ATTACK) || (m.intent == AbstractMonster.Intent.ATTACK_DEBUFF) || (m.intent == AbstractMonster.Intent.ATTACK_BUFF) || (m.intent == AbstractMonster.Intent.ATTACK_DEFEND)))
/*     */         {
/*     */ 
/* 337 */           if (AbstractDungeon.floorNum <= 5) {
/* 338 */             TipTracker.blockCounter += 1;
/*     */           } else {
/* 340 */             TipTracker.neverShowAgain("INTENT_TIP");
/*     */           }
/*     */         }
/*     */         
/*     */ 
/* 345 */         m.takeTurn();
/* 346 */         m.applyTurnPowers();
/*     */       }
/*     */       
/* 349 */       this.monsterQueue.remove(0);
/* 350 */       if (this.monsterQueue.isEmpty()) {
/* 351 */         AbstractDungeon.actionManager.addToBottom(new WaitAction(1.5F));
/*     */       }
/*     */       
/*     */     }
/* 355 */     else if ((this.turnHasEnded) && (!AbstractDungeon.getMonsters().areMonstersBasicallyDead())) {
/* 356 */       AbstractDungeon.getCurrRoom().monsters.applyEndOfTurnPowers();
/* 357 */       AbstractDungeon.player.cardsPlayedThisTurn = 0;
/* 358 */       this.orbsChanneledThisTurn.clear();
/* 359 */       AbstractDungeon.player.applyStartOfTurnRelics();
/* 360 */       AbstractDungeon.player.applyStartOfTurnCards();
/* 361 */       AbstractDungeon.player.applyStartOfTurnPowers();
/* 362 */       AbstractDungeon.player.applyStartOfTurnOrbs();
/* 363 */       turn += 1;
/* 364 */       this.turnHasEnded = false;
/* 365 */       totalDiscardedThisTurn = 0;
/* 366 */       this.cardsPlayedThisTurn.clear();
/* 367 */       damageReceivedThisTurn = 0;
/*     */       
/* 369 */       if ((!AbstractDungeon.player.hasPower("Barricade")) && (!AbstractDungeon.player.hasPower("Blur")))
/*     */       {
/* 371 */         if (!AbstractDungeon.player.hasRelic("Calipers")) {
/* 372 */           AbstractDungeon.player.loseBlock();
/*     */         } else {
/* 374 */           AbstractDungeon.player.loseBlock(15);
/*     */         }
/*     */       }
/*     */       
/* 378 */       if (!AbstractDungeon.getCurrRoom().isBattleOver) {
/* 379 */         AbstractDungeon.actionManager.addToBottom(new DrawCardAction(null, AbstractDungeon.player.gameHandSize, true));
/*     */         
/* 381 */         AbstractDungeon.player.applyStartOfTurnPostDrawRelics();
/* 382 */         AbstractDungeon.player.applyStartOfTurnPostDrawPowers();
/* 383 */         AbstractDungeon.actionManager.addToBottom(new EnableEndTurnButtonAction());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void callEndOfTurnActions()
/*     */   {
/* 392 */     AbstractDungeon.getCurrRoom().applyEndOfTurnRelics();
/* 393 */     AbstractDungeon.actionManager.addToBottom(new TriggerEndOfTurnOrbsAction());
/*     */     
/* 395 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 396 */       c.triggerOnEndOfTurnForPlayingCard();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void cleanCardQueue()
/*     */   {
/* 404 */     for (Iterator<CardQueueItem> i = this.cardQueue.iterator(); i.hasNext();) {
/* 405 */       CardQueueItem e = (CardQueueItem)i.next();
/* 406 */       if (AbstractDungeon.player.hand.contains(e.card)) {
/* 407 */         i.remove();
/*     */       }
/*     */     }
/*     */     
/* 411 */     for (Iterator<AbstractCard> i = AbstractDungeon.player.limbo.group.iterator(); i.hasNext();) {
/* 412 */       AbstractCard e = (AbstractCard)i.next();
/* 413 */       e.fadingOut = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean isEmpty() {
/* 418 */     return this.actions.isEmpty();
/*     */   }
/*     */   
/*     */   public void clearNextRoomCombatActions() {
/* 422 */     this.nextCombatActions.clear();
/*     */   }
/*     */   
/*     */   public void clear()
/*     */   {
/* 427 */     this.actions.clear();
/* 428 */     this.preTurnActions.clear();
/* 429 */     this.currentAction = null;
/* 430 */     this.previousAction = null;
/* 431 */     this.turnStartCurrentAction = null;
/*     */     
/*     */ 
/* 434 */     this.cardsPlayedThisCombat.clear();
/* 435 */     this.cardsPlayedThisTurn.clear();
/* 436 */     this.orbsChanneledThisCombat.clear();
/* 437 */     this.orbsChanneledThisTurn.clear();
/* 438 */     this.cardQueue.clear();
/*     */     
/* 440 */     energyGainedThisCombat = 0;
/* 441 */     damageReceivedThisCombat = 0;
/* 442 */     hpLossThisCombat = 0;
/* 443 */     this.turnHasEnded = false;
/* 444 */     turn = 1;
/* 445 */     this.phase = Phase.WAITING_ON_USER;
/* 446 */     totalDiscardedThisTurn = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void incrementDiscard(boolean endOfTurn)
/*     */   {
/* 453 */     totalDiscardedThisTurn += 1;
/*     */     
/* 455 */     if ((!AbstractDungeon.actionManager.turnHasEnded) && (!endOfTurn)) {
/* 456 */       AbstractDungeon.player.updateCardsOnDiscard();
/* 457 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 458 */         r.onManualDiscard();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateEnergyGain(int energyGain) {
/* 464 */     energyGainedThisCombat += energyGain;
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\GameActionManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */