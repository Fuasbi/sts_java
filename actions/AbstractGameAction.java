/*    */ package com.megacrit.cardcrawl.actions;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public abstract class AbstractGameAction
/*    */ {
/*    */   protected static final float DEFAULT_DURATION = 0.5F;
/*    */   protected float duration;
/*    */   protected float startDuration;
/*    */   public ActionType actionType;
/* 13 */   public AttackEffect attackEffect = AttackEffect.NONE;
/*    */   public com.megacrit.cardcrawl.cards.DamageInfo.DamageType damageType;
/* 15 */   public boolean isDone = false;
/*    */   public int amount;
/*    */   public AbstractCreature target;
/*    */   public AbstractCreature source;
/*    */   
/* 20 */   public static enum AttackEffect { BLUNT_LIGHT,  BLUNT_HEAVY,  SLASH_DIAGONAL,  SMASH,  SLASH_HEAVY,  SLASH_HORIZONTAL,  SLASH_VERTICAL,  NONE,  FIRE,  POISON,  SHIELD;
/*    */     
/*    */     private AttackEffect() {} }
/*    */   
/* 24 */   public static enum ActionType { BLOCK,  POWER,  CARD_MANIPULATION,  DAMAGE,  DEBUFF,  DISCARD,  DRAW,  EXHAUST,  HEAL,  ENERGY,  TEXT,  USE,  CLEAR_CARD_QUEUE,  DIALOG,  SPECIAL,  WAIT,  SHUFFLE,  REDUCE_POWER;
/*    */     
/*    */     private ActionType() {} }
/*    */   
/* 28 */   protected void setValues(AbstractCreature target, DamageInfo info) { this.target = target;
/* 29 */     this.source = info.owner;
/* 30 */     this.amount = info.output;
/* 31 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   protected void setValues(AbstractCreature target, AbstractCreature source, int amount) {
/* 35 */     this.target = target;
/* 36 */     this.source = source;
/* 37 */     this.amount = amount;
/* 38 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   protected void setValues(AbstractCreature target, AbstractCreature source) {
/* 42 */     this.target = target;
/* 43 */     this.source = source;
/* 44 */     this.amount = 0;
/* 45 */     this.duration = 0.5F;
/*    */   }
/*    */   
/*    */   protected boolean isDeadOrEscaped(AbstractCreature target) {
/* 49 */     if ((target.isDying) || (target.halfDead)) {
/* 50 */       return true;
/*    */     }
/* 52 */     if (!target.isPlayer) {
/* 53 */       AbstractMonster m = (AbstractMonster)target;
/* 54 */       if (m.isEscaping) {
/* 55 */         return true;
/*    */       }
/*    */     }
/* 58 */     return false;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public abstract void update();
/*    */   
/*    */ 
/*    */ 
/*    */   protected void tickDuration()
/*    */   {
/* 69 */     this.duration -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 70 */     if (this.duration < 0.0F) {
/* 71 */       this.isDone = true;
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   protected boolean shouldCancelAction()
/*    */   {
/* 79 */     return (this.target == null) || ((this.source != null) && (this.source.isDying)) || (this.target.isDeadOrEscaped());
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\AbstractGameAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */