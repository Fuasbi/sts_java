/*    */ package com.megacrit.cardcrawl.actions.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ 
/*    */ public class MillAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public MillAction(com.megacrit.cardcrawl.core.AbstractCreature target, com.megacrit.cardcrawl.core.AbstractCreature source, int amount)
/*    */   {
/* 10 */     setValues(target, source, amount);
/* 11 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.CARD_MANIPULATION;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 16 */     if (this.duration == 0.5F) {
/* 17 */       if (this.amount <= com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.size()) {
/* 18 */         for (int i = 0; i < this.amount; i++) {
/* 19 */           com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.moveToDiscardPile(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.getTopCard());
/*    */         }
/*    */       } else {
/* 22 */         for (int i = 0; i < com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.size(); i++) {
/* 23 */           com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.moveToDiscardPile(com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.drawPile.getTopCard());
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 28 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\deprecated\MillAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */