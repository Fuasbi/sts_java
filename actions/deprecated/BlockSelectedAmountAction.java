/*    */ package com.megacrit.cardcrawl.actions.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ 
/*    */ public class BlockSelectedAmountAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public BlockSelectedAmountAction(AbstractCreature target, AbstractCreature source, int multiplier)
/*    */   {
/* 12 */     setValues(target, source, multiplier);
/* 13 */     this.actionType = AbstractGameAction.ActionType.BLOCK;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == 0.5F) {
/* 19 */       AbstractDungeon.effectList.add(new com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect(this.target.hb.cX, this.target.hb.cY, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SHIELD));
/* 20 */       this.amount *= AbstractDungeon.handCardSelectScreen.numSelected;
/* 21 */       this.target.addBlock(this.amount);
/*    */     }
/*    */     
/* 24 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\deprecated\BlockSelectedAmountAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */