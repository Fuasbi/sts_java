/*    */ package com.megacrit.cardcrawl.actions.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.powers.MetallicizePower;
/*    */ 
/*    */ public class ArmorSelectedAmountAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/*    */   public ArmorSelectedAmountAction(AbstractCreature target, AbstractCreature source, int multiplier)
/*    */   {
/* 12 */     setValues(target, source, multiplier);
/* 13 */     this.actionType = com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType.POWER;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 18 */     if (this.duration == 0.5F) {
/* 19 */       this.amount = (AbstractDungeon.handCardSelectScreen.numSelected * this.amount);
/* 20 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(this.target, this.target, new MetallicizePower(this.target, this.amount), this.amount));
/*    */     }
/*    */     
/*    */ 
/* 24 */     tickDuration();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\deprecated\ArmorSelectedAmountAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */