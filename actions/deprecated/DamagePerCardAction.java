/*    */ package com.megacrit.cardcrawl.actions.deprecated;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.ActionType;
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class DamagePerCardAction extends com.megacrit.cardcrawl.actions.AbstractGameAction
/*    */ {
/* 14 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DamagePerCardAction.class.getName());
/*    */   private DamageInfo info;
/*    */   private String cardName;
/*    */   
/*    */   public DamagePerCardAction(AbstractCreature target, DamageInfo info, String cardName, AbstractGameAction.AttackEffect effect) {
/* 19 */     this.info = info;
/* 20 */     this.cardName = cardName;
/* 21 */     this.attackEffect = effect;
/*    */     
/* 23 */     setValues(target, info);
/* 24 */     this.actionType = AbstractGameAction.ActionType.DAMAGE;
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 29 */     if (!this.isDone) {
/* 30 */       this.isDone = true;
/* 31 */       for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 32 */         if (c.originalName.equals(this.cardName)) {
/* 33 */           logger.info("QUEUED DAMAGE...");
/* 34 */           AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.DamageAction(this.target, this.info, this.attackEffect));
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\actions\deprecated\DamagePerCardAction.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */