/*     */ package com.megacrit.cardcrawl.steamController;
/*     */ 
/*     */ import com.codedisaster.steamworks.SteamController;
/*     */ import com.codedisaster.steamworks.SteamController.Pad;
/*     */ import com.codedisaster.steamworks.SteamController.SourceMode;
/*     */ import com.codedisaster.steamworks.SteamControllerAnalogActionData;
/*     */ import com.codedisaster.steamworks.SteamControllerAnalogActionHandle;
/*     */ import com.codedisaster.steamworks.SteamControllerDigitalActionData;
/*     */ import com.codedisaster.steamworks.SteamControllerHandle;
/*     */ import com.codedisaster.steamworks.SteamException;
/*     */ import com.codedisaster.steamworks.SteamNativeHandle;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ public class SteamControllerTest extends SteamTestApp
/*     */ {
/*     */   private SteamController controller;
/*  17 */   private SteamControllerHandle[] controllerHandles = new SteamControllerHandle[0];
/*  18 */   private int numControllers = 0;
/*     */   
/*     */   private com.codedisaster.steamworks.SteamControllerActionSetHandle setHandle;
/*     */   
/*     */   private com.codedisaster.steamworks.SteamControllerDigitalActionHandle digitalActionHandle;
/*  23 */   private SteamControllerDigitalActionData digitalActionData = new SteamControllerDigitalActionData();
/*     */   
/*     */   private SteamControllerAnalogActionHandle analogActionHandle;
/*  26 */   private SteamControllerAnalogActionData analogActionData = new SteamControllerAnalogActionData();
/*     */   
/*     */   public static void main(String[] arguments) {
/*  29 */     new SteamControllerTest().clientMain(arguments);
/*     */   }
/*     */   
/*     */   protected void registerInterfaces()
/*     */   {
/*  34 */     System.out.println("Register controller API ...");
/*  35 */     this.controller = new SteamController();
/*  36 */     this.controller.init();
/*     */     try
/*     */     {
/*  39 */       processInput("c list");
/*     */     } catch (SteamException e) {
/*  41 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */   protected void unregisterInterfaces()
/*     */   {
/*  47 */     this.controller.shutdown();
/*     */   }
/*     */   
/*     */   protected void processUpdate()
/*     */     throws SteamException
/*     */   {
/*  53 */     if ((this.setHandle == null) || (SteamNativeHandle.getNativeHandle(this.setHandle) == 0L)) {
/*  54 */       return;
/*     */     }
/*     */     
/*  57 */     for (int i = 0; i < this.numControllers; i++) {
/*  58 */       SteamControllerHandle handle = this.controllerHandles[i];
/*  59 */       this.controller.activateActionSet(handle, this.setHandle);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  73 */       if (this.digitalActionHandle != null) {
/*  74 */         this.controller.getDigitalActionData(handle, this.digitalActionHandle, this.digitalActionData);
/*  75 */         if ((this.digitalActionData.getActive()) && (this.digitalActionData.getState())) {
/*  76 */           System.out.println("  digital action: " + SteamNativeHandle.getNativeHandle(this.digitalActionHandle));
/*     */         }
/*     */       }
/*     */       
/*  80 */       if (this.analogActionHandle != null) {
/*  81 */         this.controller.getAnalogActionData(handle, this.analogActionHandle, this.analogActionData);
/*  82 */         if (this.analogActionData.getActive()) {
/*  83 */           float x = this.analogActionData.getX();
/*  84 */           float y = this.analogActionData.getY();
/*  85 */           SteamController.SourceMode mode = this.analogActionData.getMode();
/*  86 */           if ((Math.abs(x) > 1.0E-4F) && (Math.abs(y) > 0.001F)) {
/*  87 */             System.out.println("  analog action: " + this.analogActionData.getX() + ", " + this.analogActionData
/*  88 */               .getY() + ", " + mode.name());
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   protected void processInput(String input) throws SteamException
/*     */   {
/*  97 */     if (input.equals("c list")) {
/*  98 */       this.controllerHandles = new SteamControllerHandle[16];
/*  99 */       this.numControllers = this.controller.getConnectedControllers(this.controllerHandles);
/*     */       
/* 101 */       System.out.println(this.numControllers + " controllers found");
/* 102 */       for (int i = 0; i < this.numControllers; i++) {
/* 103 */         System.out.println("  " + i + ": " + this.controllerHandles[i]);
/*     */       }
/* 105 */     } else if (input.startsWith("c pulse ")) {
/* 106 */       String[] params = input.substring("controller pulse ".length()).split(" ");
/* 107 */       if (params.length > 1) {
/* 108 */         SteamController.Pad pad = "left".equals(params[0]) ? SteamController.Pad.Left : SteamController.Pad.Right;
/*     */         
/* 110 */         if (params.length == 2) {
/* 111 */           this.controller.triggerHapticPulse(this.controllerHandles[0], pad, Short.parseShort(params[1]));
/* 112 */         } else if (params.length == 4) {
/* 113 */           this.controller.triggerRepeatedHapticPulse(this.controllerHandles[0], pad, Short.parseShort(params[1]), 
/* 114 */             Short.parseShort(params[2]), Short.parseShort(params[3]), 0);
/*     */         }
/*     */       }
/* 117 */     } else if (input.startsWith("c set ")) {
/* 118 */       String setName = input.substring("c set ".length());
/* 119 */       this.setHandle = this.controller.getActionSetHandle(setName);
/* 120 */       System.out.println("  handle for set '" + setName + "': " + SteamNativeHandle.getNativeHandle(this.setHandle));
/* 121 */     } else if (input.startsWith("c d action ")) {
/* 122 */       String actionName = input.substring("c d action ".length());
/* 123 */       this.digitalActionHandle = this.controller.getDigitalActionHandle(actionName);
/* 124 */       System.out.println("  handle for digital '" + actionName + "': " + SteamNativeHandle.getNativeHandle(this.digitalActionHandle));
/* 125 */     } else if (input.startsWith("c a action ")) {
/* 126 */       String actionName = input.substring("c a action ".length());
/* 127 */       this.analogActionHandle = this.controller.getAnalogActionHandle(actionName);
/* 128 */       System.out.println("  handle for analog '" + actionName + "': " + SteamNativeHandle.getNativeHandle(this.analogActionHandle));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steamController\SteamControllerTest.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */