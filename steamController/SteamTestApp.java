/*     */ package com.megacrit.cardcrawl.steamController;
/*     */ 
/*     */ import com.codedisaster.steamworks.SteamAPI;
/*     */ import com.codedisaster.steamworks.SteamAPIWarningMessageHook;
/*     */ import com.codedisaster.steamworks.SteamException;
/*     */ import com.codedisaster.steamworks.SteamGameServerAPI;
/*     */ import com.codedisaster.steamworks.SteamGameServerAPI.ServerMode;
/*     */ import com.codedisaster.steamworks.SteamUtils;
/*     */ import com.codedisaster.steamworks.SteamUtilsCallback;
/*     */ import java.io.PrintStream;
/*     */ import java.util.Scanner;
/*     */ 
/*     */ public abstract class SteamTestApp
/*     */ {
/*     */   protected SteamUtils clientUtils;
/*     */   protected static final int MS_PER_TICK = 66;
/*  17 */   private long timer = 5000L;
/*  18 */   private long prevTime = 0L;
/*     */   
/*  20 */   private SteamAPIWarningMessageHook clMessageHook = new SteamAPIWarningMessageHook()
/*     */   {
/*     */     public void onWarningMessage(int severity, String message) {
/*  23 */       System.err.println("[client debug message] (" + severity + ") " + message);
/*     */     }
/*     */   };
/*     */   
/*  27 */   private SteamUtilsCallback clUtilsCallback = new SteamUtilsCallback()
/*     */   {
/*     */ 
/*  30 */     public void onSteamShutdown() { System.err.println("Steam client requested to shut down!"); } };
/*     */   protected abstract void registerInterfaces() throws SteamException;
/*     */   
/*     */   protected abstract void unregisterInterfaces() throws SteamException;
/*     */   
/*     */   protected abstract void processUpdate() throws SteamException;
/*     */   
/*     */   protected abstract void processInput(String paramString) throws SteamException;
/*     */   
/*     */   private class InputHandler implements Runnable { private volatile boolean alive;
/*     */     
/*  41 */     public InputHandler(Thread mainThread) { this.alive = true;
/*  42 */       this.mainThread = mainThread;
/*     */       
/*  44 */       this.scanner = new Scanner(System.in, "UTF-8");
/*  45 */       this.scanner.useDelimiter("[\r\n\t]");
/*     */     }
/*     */     
/*     */     public void run()
/*     */     {
/*     */       try {
/*  51 */         while ((this.alive) && (this.mainThread.isAlive())) {
/*  52 */           if (this.scanner.hasNext()) {
/*  53 */             String input = this.scanner.next();
/*  54 */             if ((input.equals("quit")) || (input.equals("exit"))) {
/*  55 */               this.alive = false;
/*     */             } else {
/*     */               try {
/*  58 */                 SteamTestApp.this.processInput(input);
/*     */               } catch (NumberFormatException e) {
/*  60 */                 e.printStackTrace();
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */       } catch (SteamException e) {
/*  66 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */     
/*     */     public boolean alive() {
/*  71 */       return this.alive;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */     private Thread mainThread;
/*     */     
/*     */ 
/*     */     private Scanner scanner;
/*     */   }
/*     */   
/*     */ 
/*     */   private boolean runAsClient(String[] arguments)
/*     */     throws SteamException
/*     */   {
/*  86 */     System.out.println("Initialise Steam client API ...");
/*     */     
/*  88 */     if (!SteamAPI.init()) {
/*  89 */       SteamAPI.printDebugInfo(System.err);
/*  90 */       return false;
/*     */     }
/*     */     
/*  93 */     SteamAPI.printDebugInfo(System.out);
/*  94 */     registerInterfaces();
/*     */     
/*  96 */     this.clientUtils = new SteamUtils(this.clUtilsCallback);
/*  97 */     this.clientUtils.setWarningMessageHook(this.clMessageHook);
/*     */     
/*     */ 
/*     */ 
/* 101 */     if (SteamAPI.restartAppIfNecessary(this.clientUtils.getAppID())) {
/* 102 */       System.out.println("SteamAPI_RestartAppIfNecessary returned 'false'");
/*     */     }
/*     */     
/* 105 */     InputHandler inputHandler = new InputHandler(Thread.currentThread());
/*     */     
/* 107 */     Thread inputThread = new Thread(inputHandler);
/* 108 */     inputThread.start();
/*     */     
/* 110 */     while ((inputHandler.alive()) && (SteamAPI.isSteamRunning())) {
/* 111 */       SteamAPI.runCallbacks();
/* 112 */       timer();
/* 113 */       processUpdate();
/*     */       
/*     */       try
/*     */       {
/* 117 */         Thread.sleep(66L);
/*     */       }
/*     */       catch (InterruptedException localInterruptedException1) {}
/*     */     }
/*     */     
/*     */ 
/* 123 */     System.out.println("Shutting down Steam client API ...");
/*     */     try
/*     */     {
/* 126 */       inputThread.join();
/*     */     } catch (InterruptedException e) {
/* 128 */       throw new SteamException(e);
/*     */     }
/*     */     
/* 131 */     this.clientUtils.dispose();
/* 132 */     unregisterInterfaces();
/* 133 */     SteamAPI.shutdown();
/*     */     
/* 135 */     return true;
/*     */   }
/*     */   
/*     */   private void timer() {
/* 139 */     if (this.prevTime == 0L) {
/* 140 */       this.prevTime = System.currentTimeMillis();
/*     */     }
/*     */     
/* 143 */     long currentTime = System.currentTimeMillis();
/* 144 */     this.timer -= currentTime - this.prevTime;
/* 145 */     if (this.timer < 0L)
/*     */     {
/* 147 */       this.timer = 5000L;
/*     */     }
/*     */     
/* 150 */     this.prevTime = currentTime;
/*     */   }
/*     */   
/*     */ 
/*     */   protected void clientMain(String[] arguments)
/*     */   {
/* 156 */     System.setProperty("com.codedisaster.steamworks.Debug", "true");
/* 157 */     arguments = new String[1];
/* 158 */     arguments[0] = "-forcecontrollerappid 646570";
/*     */     try
/*     */     {
/* 161 */       if (!runAsClient(arguments)) {
/* 162 */         System.exit(-1);
/*     */       }
/*     */       
/* 165 */       System.out.println("Bye!");
/*     */     }
/*     */     catch (Exception e) {
/* 168 */       e.printStackTrace();
/* 169 */       System.exit(-1);
/*     */     }
/*     */   }
/*     */   
/*     */   private boolean runAsGameServer(String[] arguments) throws SteamException
/*     */   {
/* 175 */     boolean dedicated = false;
/*     */     
/* 177 */     for (String arg : arguments) {
/* 178 */       if (arg.equals("--dedicated")) {
/* 179 */         dedicated = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 186 */     if (!dedicated)
/*     */     {
/* 188 */       System.out.println("Initialise Steam client API ...");
/* 189 */       if (!SteamAPI.init()) {
/* 190 */         SteamAPI.printDebugInfo(System.err);
/* 191 */         return false;
/*     */       }
/*     */     }
/*     */     
/* 195 */     System.out.println("Initialise Steam GameServer API ...");
/*     */     
/* 197 */     if (!SteamGameServerAPI.init(2130706433, (short)27015, (short)27016, (short)27017, SteamGameServerAPI.ServerMode.NoAuthentication, "0.0.1"))
/*     */     {
/* 199 */       System.err.println("SteamGameServerAPI.init() failed");
/* 200 */       return false;
/*     */     }
/*     */     
/* 203 */     registerInterfaces();
/*     */     
/* 205 */     InputHandler inputHandler = new InputHandler(Thread.currentThread());
/*     */     
/* 207 */     Thread inputThread = new Thread(inputHandler);
/* 208 */     inputThread.start();
/*     */     
/* 210 */     while (inputHandler.alive())
/*     */     {
/* 212 */       if (!dedicated) {
/* 213 */         SteamAPI.runCallbacks();
/*     */       }
/*     */       
/* 216 */       SteamGameServerAPI.runCallbacks();
/*     */       
/* 218 */       processUpdate();
/*     */       
/*     */       try
/*     */       {
/* 222 */         Thread.sleep(66L);
/*     */       }
/*     */       catch (InterruptedException localInterruptedException2) {}
/*     */     }
/*     */     
/*     */ 
/* 228 */     System.out.println("Shutting down Steam GameServer API ...");
/*     */     try
/*     */     {
/* 231 */       inputThread.join();
/*     */     } catch (InterruptedException e) {
/* 233 */       throw new SteamException(e);
/*     */     }
/*     */     
/* 236 */     unregisterInterfaces();
/*     */     
/* 238 */     SteamGameServerAPI.shutdown();
/*     */     
/* 240 */     if (!dedicated) {
/* 241 */       System.out.println("Shutting down Steam client API ...");
/* 242 */       SteamAPI.shutdown();
/*     */     }
/*     */     
/* 245 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   protected void serverMain(String[] arguments)
/*     */   {
/* 251 */     System.setProperty("com.codedisaster.steamworks.Debug", "true");
/*     */     
/*     */     try
/*     */     {
/* 255 */       if (!runAsGameServer(arguments)) {
/* 256 */         System.exit(-1);
/*     */       }
/*     */       
/* 259 */       System.out.println("Bye!");
/*     */     }
/*     */     catch (Exception e) {
/* 262 */       e.printStackTrace();
/* 263 */       System.exit(-1);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\steamController\SteamTestApp.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */