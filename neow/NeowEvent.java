/*     */ package com.megacrit.cardcrawl.neow;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*     */ import com.megacrit.cardcrawl.blights.GrotesqueTrophy;
/*     */ import com.megacrit.cardcrawl.blights.Muzzle;
/*     */ import com.megacrit.cardcrawl.blights.Spear;
/*     */ import com.megacrit.cardcrawl.blights.TimeMaze;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.characters.AnimatedNpc;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.daily.DailyMods;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*     */ import com.megacrit.cardcrawl.events.RoomEventDialog;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*     */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.WhiteBeast;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*     */ import com.megacrit.cardcrawl.vfx.InfiniteSpeechBubble;
/*     */ import com.megacrit.cardcrawl.vfx.scene.LevelTransitionTextOverlayEffect;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class NeowEvent extends AbstractEvent
/*     */ {
/*  44 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Neow Event");
/*     */   
/*  46 */   public static final String[] NAMES = characterStrings.NAMES;
/*  47 */   public static final String[] TEXT = characterStrings.TEXT;
/*  48 */   public static final String[] OPTIONS = characterStrings.OPTIONS;
/*     */   
/*     */   private AnimatedNpc npc;
/*  51 */   public static final String NAME = NAMES[0];
/*  52 */   private int screenNum = 2;
/*     */   private int bossCount;
/*  54 */   private boolean setPhaseToEvent = false;
/*  55 */   private ArrayList<NeowReward> rewards = new ArrayList();
/*  56 */   public static Random rng = null;
/*  57 */   public static boolean waitingToSave = false;
/*     */   
/*  59 */   private static final float DIALOG_X = 1100.0F * Settings.scale; private static final float DIALOG_Y = 400.0F * Settings.scale;
/*     */   
/*     */   public NeowEvent(boolean isDone)
/*     */   {
/*  63 */     waitingToSave = false;
/*  64 */     this.npc = new AnimatedNpc(1534.0F * Settings.scale, 280.0F * Settings.scale, "images/npcs/neow/skeleton.atlas", "images/npcs/neow/skeleton.json", "idle");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  71 */     this.roomEventText.clear();
/*  72 */     playSfx();
/*     */     
/*  74 */     if ((!Settings.isEndless) || (AbstractDungeon.floorNum <= 1))
/*     */     {
/*  76 */       if ((Settings.isStandardRun()) || ((Settings.isEndless) && (AbstractDungeon.floorNum <= 1))) {
/*  77 */         this.bossCount = CardCrawlGame.playerPref.getInteger(AbstractDungeon.player.chosenClass.name() + "_SPIRITS", 0);
/*  78 */         AbstractDungeon.bossCount = this.bossCount;
/*     */       } else {
/*  80 */         this.bossCount = 0;
/*     */       }
/*     */     }
/*  83 */     this.body = "";
/*     */     
/*  85 */     if ((Settings.isEndless) && (AbstractDungeon.floorNum > 1))
/*     */     {
/*  87 */       talk(TEXT[com.badlogic.gdx.math.MathUtils.random(12, 14)]);
/*  88 */       this.screenNum = 999;
/*  89 */       this.roomEventText.addDialogOption(OPTIONS[0]);
/*  90 */     } else if (shouldSkipNeowDialog()) {
/*  91 */       this.screenNum = 10;
/*  92 */       talk(TEXT[10]);
/*  93 */       this.roomEventText.addDialogOption(OPTIONS[1]);
/*     */     }
/*  95 */     else if (!isDone)
/*     */     {
/*  97 */       if (!((Boolean)TipTracker.tips.get("NEOW_INTRO")).booleanValue()) {
/*  98 */         this.screenNum = 0;
/*  99 */         TipTracker.neverShowAgain("NEOW_INTRO");
/* 100 */         talk(TEXT[0]);
/* 101 */         this.roomEventText.addDialogOption(OPTIONS[1]);
/*     */       }
/*     */       else {
/* 104 */         this.screenNum = 1;
/* 105 */         talk(TEXT[com.badlogic.gdx.math.MathUtils.random(1, 3)]);
/* 106 */         this.roomEventText.addDialogOption(OPTIONS[1]);
/*     */       }
/* 108 */       AbstractDungeon.topLevelEffects.add(new LevelTransitionTextOverlayEffect(AbstractDungeon.name, AbstractDungeon.levelNum, true));
/*     */     }
/*     */     else {
/* 111 */       this.screenNum = 99;
/* 112 */       talk(TEXT[8]);
/* 113 */       this.roomEventText.addDialogOption(OPTIONS[3]);
/*     */     }
/*     */     
/* 116 */     this.hasDialog = true;
/* 117 */     this.hasFocus = true;
/*     */   }
/*     */   
/*     */   public NeowEvent() {
/* 121 */     this(false);
/*     */   }
/*     */   
/*     */   private boolean shouldSkipNeowDialog() {
/* 125 */     return !Settings.isStandardRun();
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 130 */     super.update();
/*     */     
/* 132 */     for (Iterator localIterator = this.rewards.iterator(); localIterator.hasNext();) { r = (NeowReward)localIterator.next();
/* 133 */       r.update();
/*     */     }
/*     */     NeowReward r;
/* 136 */     if (!this.setPhaseToEvent) {
/* 137 */       AbstractDungeon.getCurrRoom().phase = AbstractRoom.RoomPhase.EVENT;
/* 138 */       this.setPhaseToEvent = true;
/*     */     }
/*     */     
/*     */ 
/* 142 */     if (!RoomEventDialog.waitForInput) {
/* 143 */       buttonEffect(this.roomEventText.getSelectedOption());
/*     */     }
/*     */     
/*     */ 
/* 147 */     if ((waitingToSave) && (!AbstractDungeon.isScreenUp) && (AbstractDungeon.topLevelEffects.isEmpty()) && 
/* 148 */       (AbstractDungeon.player.relicsDoneAnimating())) {
/* 149 */       boolean doneAnims = true;
/*     */       
/* 151 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/* 152 */         if (!r.isDone) {
/* 153 */           doneAnims = false;
/* 154 */           break;
/*     */         }
/*     */       }
/*     */       
/* 158 */       if (doneAnims) {
/* 159 */         waitingToSave = false;
/* 160 */         com.megacrit.cardcrawl.helpers.SaveHelper.saveIfAppropriate(SaveFile.SaveType.POST_NEOW);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void talk(String msg) {
/* 166 */     AbstractDungeon.effectList.add(new InfiniteSpeechBubble(DIALOG_X, DIALOG_Y, msg));
/*     */   }
/*     */   
/*     */   protected void buttonEffect(int buttonPressed)
/*     */   {
/* 171 */     switch (this.screenNum)
/*     */     {
/*     */     case 0: 
/* 174 */       dismissBubble();
/* 175 */       talk(TEXT[4]);
/* 176 */       this.roomEventText.updateDialogOption(0, OPTIONS[0]);
/* 177 */       if ((this.bossCount != 0) || (Settings.isTestingNeow)) {
/* 178 */         blessing();
/*     */       } else {
/* 180 */         this.screenNum = 99;
/*     */       }
/* 182 */       break;
/*     */     
/*     */ 
/*     */     case 1: 
/* 186 */       dismissBubble();
/*     */       
/* 188 */       if ((this.bossCount == 0) && (!Settings.isTestingNeow)) {
/* 189 */         talk(TEXT[com.badlogic.gdx.math.MathUtils.random(4, 6)]);
/* 190 */         this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/* 191 */         this.screenNum = 99;
/*     */       } else {
/* 193 */         blessing();
/*     */       }
/* 195 */       break;
/*     */     
/*     */ 
/*     */     case 2: 
/* 199 */       if (buttonPressed == 0) {
/* 200 */         blessing();
/*     */       } else {
/* 202 */         openMap();
/*     */       }
/* 204 */       break;
/*     */     
/*     */ 
/*     */     case 3: 
/* 208 */       dismissBubble();
/* 209 */       this.roomEventText.clearRemainingOptions();
/*     */       
/* 211 */       switch (buttonPressed) {
/*     */       case 0: 
/* 213 */         ((NeowReward)this.rewards.get(0)).activate();
/* 214 */         talk(TEXT[8]);
/* 215 */         break;
/*     */       case 1: 
/* 217 */         ((NeowReward)this.rewards.get(1)).activate();
/* 218 */         talk(TEXT[8]);
/* 219 */         break;
/*     */       case 2: 
/* 221 */         ((NeowReward)this.rewards.get(2)).activate();
/* 222 */         talk(TEXT[9]);
/* 223 */         break;
/*     */       case 3: 
/* 225 */         ((NeowReward)this.rewards.get(3)).activate();
/* 226 */         talk(TEXT[9]);
/* 227 */         break;
/*     */       }
/*     */       
/*     */       
/*     */ 
/*     */ 
/* 233 */       this.screenNum = 99;
/* 234 */       this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/* 235 */       this.roomEventText.clearRemainingOptions();
/* 236 */       waitingToSave = true;
/* 237 */       break;
/*     */     
/*     */ 
/*     */     case 10: 
/* 241 */       dailyBlessing();
/* 242 */       this.roomEventText.clearRemainingOptions();
/* 243 */       this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/* 244 */       this.screenNum = 99;
/* 245 */       break;
/*     */     
/*     */ 
/*     */     case 999: 
/* 249 */       endlessBlight();
/* 250 */       this.roomEventText.clearRemainingOptions();
/* 251 */       this.roomEventText.updateDialogOption(0, OPTIONS[3]);
/* 252 */       this.screenNum = 99;
/* 253 */       break;
/*     */     default: 
/* 255 */       openMap();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   private void endlessBlight()
/*     */   {
/* 262 */     if (AbstractDungeon.player.hasBlight("DeadlyEnemies")) {
/* 263 */       AbstractBlight tmp = AbstractDungeon.player.getBlight("DeadlyEnemies");
/* 264 */       tmp.incrementUp();
/* 265 */       tmp.flash();
/*     */     } else {
/* 267 */       AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new Spear());
/*     */     }
/*     */     
/* 270 */     if (AbstractDungeon.player.hasBlight("ToughEnemies")) {
/* 271 */       AbstractBlight tmp = AbstractDungeon.player.getBlight("ToughEnemies");
/* 272 */       tmp.incrementUp();
/* 273 */       tmp.flash();
/*     */     } else {
/* 275 */       AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new com.megacrit.cardcrawl.blights.Shield());
/*     */     }
/*     */     
/*     */ 
/* 279 */     uniqueBlight();
/*     */   }
/*     */   
/*     */   private void uniqueBlight()
/*     */   {
/* 284 */     AbstractBlight temp = AbstractDungeon.player.getBlight("MimicInfestation");
/* 285 */     if (temp != null) {
/* 286 */       temp = AbstractDungeon.player.getBlight("TimeMaze");
/* 287 */       if (temp != null) {
/* 288 */         temp = AbstractDungeon.player.getBlight("FullBelly");
/* 289 */         if (temp != null) {
/* 290 */           temp = AbstractDungeon.player.getBlight("GrotesqueTrophy");
/* 291 */           if (temp != null)
/*     */           {
/* 293 */             AbstractDungeon.player.getBlight("GrotesqueTrophy").stack();
/*     */           }
/*     */           else {
/* 296 */             AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new GrotesqueTrophy());
/*     */           }
/*     */           
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/* 303 */           AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new Muzzle());
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/* 310 */         AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new TimeMaze());
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 317 */       AbstractDungeon.getCurrRoom().spawnBlightAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, new com.megacrit.cardcrawl.blights.MimicInfestation());
/*     */       
/*     */ 
/*     */ 
/* 321 */       return;
/*     */     }
/*     */   }
/*     */   
/*     */   private void dailyBlessing() {
/* 326 */     rng = new Random(Settings.seed);
/* 327 */     dismissBubble();
/* 328 */     talk(TEXT[8]);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 333 */     if (((Boolean)DailyMods.cardMods.get("Heirloom")).booleanValue()) {
/* 334 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F, 
/*     */       
/*     */ 
/* 337 */         AbstractDungeon.returnRandomRelic(com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier.RARE));
/*     */     }
/*     */     
/*     */ 
/* 341 */     boolean addedCards = false;
/* 342 */     CardGroup group = new CardGroup(com.megacrit.cardcrawl.cards.CardGroup.CardGroupType.UNSPECIFIED);
/*     */     
/*     */ 
/* 345 */     if (((Boolean)DailyMods.cardMods.get("Allstar")).booleanValue()) {
/* 346 */       addedCards = true;
/* 347 */       for (int i = 0; i < 5; i++) {
/* 348 */         AbstractCard colorlessCard = AbstractDungeon.getColorlessCardFromPool(
/* 349 */           AbstractDungeon.rollRareOrUncommon(0.5F));
/* 350 */         UnlockTracker.markCardAsSeen(colorlessCard.cardID);
/* 351 */         group.addToBottom(colorlessCard.makeCopy());
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 356 */     if (((Boolean)DailyMods.cardMods.get("Specialized")).booleanValue()) {
/* 357 */       addedCards = true;
/* 358 */       AbstractCard rareCard = AbstractDungeon.getCard(AbstractCard.CardRarity.RARE);
/* 359 */       UnlockTracker.markCardAsSeen(rareCard.cardID);
/* 360 */       group.addToBottom(rareCard.makeCopy());
/* 361 */       group.addToBottom(rareCard.makeCopy());
/* 362 */       group.addToBottom(rareCard.makeCopy());
/* 363 */       group.addToBottom(rareCard.makeCopy());
/* 364 */       group.addToBottom(rareCard.makeCopy());
/*     */     }
/*     */     
/*     */ 
/* 368 */     if (((Boolean)DailyMods.cardMods.get("Brewmaster")).booleanValue()) {
/* 369 */       addedCards = true;
/* 370 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, new WhiteBeast()
/*     */       
/*     */ 
/* 373 */         .makeCopy());
/*     */       
/* 375 */       AbstractCard rareCard = new com.megacrit.cardcrawl.cards.green.Alchemize();
/* 376 */       UnlockTracker.markCardAsSeen(rareCard.cardID);
/* 377 */       group.addToBottom(rareCard.makeCopy());
/* 378 */       group.addToBottom(rareCard.makeCopy());
/* 379 */       group.addToBottom(rareCard.makeCopy());
/* 380 */       group.addToBottom(rareCard.makeCopy());
/* 381 */       group.addToBottom(rareCard.makeCopy());
/*     */     }
/*     */     
/*     */ 
/* 385 */     if (addedCards) {
/* 386 */       AbstractDungeon.gridSelectScreen.openConfirmationGrid(group, TEXT[11]);
/*     */     }
/*     */     
/*     */ 
/* 390 */     if (((Boolean)DailyMods.cardMods.get("Draft")).booleanValue())
/*     */     {
/* 392 */       AbstractDungeon.cardRewardScreen.draftOpen();
/*     */     }
/*     */     
/* 395 */     this.roomEventText.clearRemainingOptions();
/* 396 */     this.screenNum = 99;
/*     */   }
/*     */   
/*     */   private void blessing() {
/* 400 */     System.out.println("BLESSING");
/* 401 */     rng = new Random(Settings.seed);
/* 402 */     System.out.println("COUNTER: " + rng.counter);
/* 403 */     AbstractDungeon.bossCount = 0;
/* 404 */     dismissBubble();
/* 405 */     talk(TEXT[7]);
/*     */     
/* 407 */     this.rewards.add(new NeowReward(0));
/* 408 */     this.rewards.add(new NeowReward(1));
/* 409 */     this.rewards.add(new NeowReward(2));
/* 410 */     this.rewards.add(new NeowReward(3));
/*     */     
/* 412 */     this.roomEventText.clearRemainingOptions();
/* 413 */     this.roomEventText.updateDialogOption(0, ((NeowReward)this.rewards.get(0)).optionLabel);
/* 414 */     this.roomEventText.addDialogOption(((NeowReward)this.rewards.get(1)).optionLabel);
/* 415 */     this.roomEventText.addDialogOption(((NeowReward)this.rewards.get(2)).optionLabel);
/* 416 */     this.roomEventText.addDialogOption(((NeowReward)this.rewards.get(3)).optionLabel);
/*     */     
/*     */ 
/* 419 */     if (!Settings.isDailyRun) {
/* 420 */       CardCrawlGame.playerPref.putInteger(AbstractDungeon.player.chosenClass.name() + "_SPIRITS", 0);
/* 421 */       CardCrawlGame.playerPref.flush();
/* 422 */       AbstractDungeon.bossCount = 0;
/*     */     }
/* 424 */     this.screenNum = 3;
/*     */   }
/*     */   
/*     */   private void dismissBubble() {
/* 428 */     for (AbstractGameEffect e : AbstractDungeon.effectList) {
/* 429 */       if ((e instanceof InfiniteSpeechBubble)) {
/* 430 */         ((InfiniteSpeechBubble)e).dismiss();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void playSfx() {
/* 436 */     int roll = com.badlogic.gdx.math.MathUtils.random(3);
/* 437 */     if (roll == 0) {
/* 438 */       CardCrawlGame.sound.play("VO_NEOW_1A");
/* 439 */     } else if (roll == 1) {
/* 440 */       CardCrawlGame.sound.play("VO_NEOW_1B");
/* 441 */     } else if (roll == 2) {
/* 442 */       CardCrawlGame.sound.play("VO_NEOW_2A");
/*     */     } else {
/* 444 */       CardCrawlGame.sound.play("VO_NEOW_2B");
/*     */     }
/*     */   }
/*     */   
/*     */   public void logMetric(String actionTaken) {
/* 449 */     AbstractEvent.logMetric(NAME, actionTaken);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 454 */     this.npc.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\neow\NeowEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */