/*    */ package com.megacrit.cardcrawl.neow;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.events.AbstractEvent;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ 
/*    */ public class NeowRoom extends AbstractRoom
/*    */ {
/*    */   public NeowRoom(boolean isDone)
/*    */   {
/* 13 */     this.phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.EVENT;
/* 14 */     this.event = new NeowEvent(isDone);
/* 15 */     this.event.onEnterRoom();
/*    */   }
/*    */   
/*    */   public void onPlayerEntry()
/*    */   {
/* 20 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/*    */   }
/*    */   
/*    */   public void update()
/*    */   {
/* 25 */     super.update();
/* 26 */     if (!AbstractDungeon.isScreenUp) {
/* 27 */       this.event.update();
/*    */     }
/*    */     
/*    */ 
/* 31 */     if ((this.event.waitTimer == 0.0F) && (!this.event.hasFocus) && 
/* 32 */       (this.phase != com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT)) {
/* 33 */       this.phase = com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMPLETE;
/* 34 */       this.event.reopen();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard.CardRarity getCardRarity(int roll)
/*    */   {
/*    */     int rareRate;
/*    */     
/*    */     int rareRate;
/* 44 */     if (AbstractDungeon.player.hasRelic("Nloth's Gift")) {
/* 45 */       rareRate = 9;
/*    */     } else {
/* 47 */       rareRate = 3;
/*    */     }
/*    */     
/*    */ 
/* 51 */     if (roll < rareRate) {
/* 52 */       if ((AbstractDungeon.player.hasRelic("Nloth's Gift")) && (roll > 3)) {
/* 53 */         AbstractDungeon.player.getRelic("Nloth's Gift").flash();
/*    */       }
/* 55 */       return AbstractCard.CardRarity.RARE; }
/* 56 */     if (roll < 40) {
/* 57 */       return AbstractCard.CardRarity.UNCOMMON;
/*    */     }
/* 59 */     return AbstractCard.CardRarity.COMMON;
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 64 */     super.render(sb);
/* 65 */     this.event.render(sb);
/*    */   }
/*    */   
/*    */   public void renderAboveTopPanel(SpriteBatch sb)
/*    */   {
/* 70 */     super.renderAboveTopPanel(sb);
/* 71 */     if (this.event != null) {
/* 72 */       this.event.renderAboveTopPanel(sb);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\neow\NeowRoom.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */