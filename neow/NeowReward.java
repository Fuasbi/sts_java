/*     */ package com.megacrit.cardcrawl.neow;
/*     */ 
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.CardGroup;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*     */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*     */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*     */ import com.megacrit.cardcrawl.relics.NeowsLament;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem;
/*     */ import com.megacrit.cardcrawl.rewards.RewardItem.RewardType;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.saveAndContinue.SaveFile.SaveType;
/*     */ import com.megacrit.cardcrawl.screens.CardRewardScreen;
/*     */ import com.megacrit.cardcrawl.screens.CombatRewardScreen;
/*     */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*     */ import com.megacrit.cardcrawl.ui.buttons.CancelButton;
/*     */ import com.megacrit.cardcrawl.vfx.UpgradeShineEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardAndObtainEffect;
/*     */ import com.megacrit.cardcrawl.vfx.cardManip.ShowCardBrieflyEffect;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class NeowReward
/*     */ {
/*     */   public static class NeowRewardDef
/*     */   {
/*     */     public NeowReward.NeowRewardType type;
/*     */     public String desc;
/*     */     
/*     */     public NeowRewardDef(NeowReward.NeowRewardType type, String desc)
/*     */     {
/*  47 */       this.type = type;
/*  48 */       this.desc = desc;
/*     */     }
/*     */   }
/*     */   
/*     */   public static class NeowRewardDrawbackDef
/*     */   {
/*     */     public NeowReward.NeowRewardDrawback type;
/*     */     public String desc;
/*     */     
/*     */     public NeowRewardDrawbackDef(NeowReward.NeowRewardDrawback type, String desc) {
/*  58 */       this.type = type;
/*  59 */       this.desc = desc;
/*     */     }
/*     */   }
/*     */   
/*     */   public NeowReward(int category) {
/*  64 */     this.optionLabel = "";
/*  65 */     this.drawback = NeowRewardDrawback.NONE;
/*  66 */     this.activated = false;
/*  67 */     this.hp_bonus = 0;
/*  68 */     this.cursed = false;
/*  69 */     this.hp_bonus = ((int)(AbstractDungeon.player.maxHealth * 0.1F));
/*     */     
/*     */ 
/*  72 */     ArrayList<NeowRewardDef> possibleRewards = getRewardOptions(category);
/*     */     
/*     */ 
/*  75 */     NeowRewardDef reward = (NeowRewardDef)possibleRewards.get(NeowEvent.rng.random(0, possibleRewards.size() - 1));
/*  76 */     if ((this.drawback != NeowRewardDrawback.NONE) && (this.drawbackDef != null)) {
/*  77 */       this.optionLabel += this.drawbackDef.desc;
/*     */     }
/*  79 */     this.optionLabel += reward.desc;
/*  80 */     this.type = reward.type;
/*     */   }
/*     */   
/*     */ 
/*     */   private ArrayList<NeowRewardDrawbackDef> getRewardDrawbackOptions()
/*     */   {
/*  86 */     ArrayList<NeowRewardDrawbackDef> drawbackOptions = new ArrayList();
/*  87 */     drawbackOptions.add(new NeowRewardDrawbackDef(NeowRewardDrawback.TEN_PERCENT_HP_LOSS, TEXT[17] + this.hp_bonus + TEXT[18]));
/*     */     
/*     */ 
/*     */ 
/*  91 */     drawbackOptions.add(new NeowRewardDrawbackDef(NeowRewardDrawback.NO_GOLD, TEXT[19]));
/*  92 */     drawbackOptions.add(new NeowRewardDrawbackDef(NeowRewardDrawback.CURSE, TEXT[20]));
/*  93 */     drawbackOptions.add(new NeowRewardDrawbackDef(NeowRewardDrawback.PERCENT_DAMAGE, TEXT[21] + AbstractDungeon.player.currentHealth / 10 * 3 + TEXT[29] + " "));
/*     */     
/*     */ 
/*     */ 
/*  97 */     return drawbackOptions;
/*     */   }
/*     */   
/*     */ 
/*     */   private ArrayList<NeowRewardDef> getRewardOptions(int category)
/*     */   {
/* 103 */     ArrayList<NeowRewardDef> rewardOptions = new ArrayList();
/* 104 */     switch (category) {
/*     */     case 0: 
/* 106 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.THREE_CARDS, TEXT[0]));
/* 107 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.ONE_RANDOM_RARE_CARD, TEXT[1]));
/* 108 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.REMOVE_CARD, TEXT[2]));
/* 109 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.UPGRADE_CARD, TEXT[3]));
/* 110 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.TRANSFORM_CARD, TEXT[4]));
/* 111 */       break;
/*     */     case 1: 
/* 113 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.THREE_SMALL_POTIONS, TEXT[5]));
/* 114 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.RANDOM_COMMON_RELIC, TEXT[6]));
/* 115 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.TEN_PERCENT_HP_BONUS, TEXT[7] + this.hp_bonus + " ]"));
/*     */       
/* 117 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.THREE_ENEMY_KILL, TEXT[28]));
/* 118 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.HUNDRED_GOLD, TEXT[8] + 100 + TEXT[9]));
/*     */       
/*     */ 
/*     */ 
/* 122 */       break;
/*     */     
/*     */     case 2: 
/* 125 */       ArrayList<NeowRewardDrawbackDef> drawbackOptions = getRewardDrawbackOptions();
/* 126 */       this.drawbackDef = ((NeowRewardDrawbackDef)drawbackOptions.get(NeowEvent.rng.random(0, drawbackOptions.size() - 1)));
/* 127 */       this.drawback = this.drawbackDef.type;
/*     */       
/* 129 */       if (this.drawback != NeowRewardDrawback.CURSE) {
/* 130 */         rewardOptions.add(new NeowRewardDef(NeowRewardType.REMOVE_TWO, TEXT[10]));
/*     */       }
/* 132 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.ONE_RARE_RELIC, TEXT[11]));
/* 133 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.THREE_RARE_CARDS, TEXT[12]));
/* 134 */       if (this.drawback != NeowRewardDrawback.NO_GOLD) {
/* 135 */         rewardOptions.add(new NeowRewardDef(NeowRewardType.TWO_FIFTY_GOLD, TEXT[13] + 250 + TEXT[14]));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 140 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.TRANSFORM_TWO_CARDS, TEXT[15]));
/* 141 */       if (this.drawback != NeowRewardDrawback.TEN_PERCENT_HP_LOSS) {
/* 142 */         rewardOptions.add(new NeowRewardDef(NeowRewardType.TWENTY_PERCENT_HP_BONUS, TEXT[16] + this.hp_bonus * 2 + " ]"));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       break;
/*     */     case 3: 
/* 149 */       rewardOptions.add(new NeowRewardDef(NeowRewardType.BOSS_RELIC, UNIQUE_REWARDS[0]));
/*     */     }
/*     */     
/* 152 */     return rewardOptions;
/*     */   }
/*     */   
/*     */ 
/*     */   public void update()
/*     */   {
/* 158 */     if (this.activated) {
/* 159 */       if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/* 160 */         switch (this.type) {
/*     */         case UPGRADE_CARD: 
/* 162 */           AbstractCard c = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 163 */           c.upgrade();
/* 164 */           AbstractDungeon.topLevelEffects.add(new ShowCardBrieflyEffect(c.makeStatEquivalentCopy()));
/* 165 */           AbstractDungeon.topLevelEffects.add(new UpgradeShineEffect(Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */           
/* 167 */           break;
/*     */         case REMOVE_CARD: 
/* 169 */           CardCrawlGame.sound.play("CARD_EXHAUST");
/* 170 */           AbstractDungeon.topLevelEffects.add(new PurgeCardEffect(
/*     */           
/* 172 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0), Settings.WIDTH / 2, Settings.HEIGHT / 2));
/*     */           
/*     */ 
/* 175 */           AbstractDungeon.player.masterDeck.removeCard(
/* 176 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 177 */           break;
/*     */         case REMOVE_TWO: 
/* 179 */           CardCrawlGame.sound.play("CARD_EXHAUST");
/* 180 */           AbstractCard c2 = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 181 */           AbstractCard c3 = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(1);
/* 182 */           AbstractDungeon.topLevelEffects.add(new PurgeCardEffect(c2, Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH / 2.0F - 30.0F * Settings.scale, Settings.HEIGHT / 2));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/* 187 */           AbstractDungeon.topLevelEffects.add(new PurgeCardEffect(c3, Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH / 2.0F + 30.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/*     */ 
/*     */ 
/* 192 */           AbstractDungeon.player.masterDeck.removeCard(c2);
/* 193 */           AbstractDungeon.player.masterDeck.removeCard(c3);
/* 194 */           break;
/*     */         case TRANSFORM_CARD: 
/* 196 */           AbstractDungeon.transformCard((AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 197 */           AbstractDungeon.player.masterDeck.removeCard(
/* 198 */             (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0));
/* 199 */           AbstractDungeon.topLevelEffects.add(new ShowCardAndObtainEffect(
/*     */           
/* 201 */             AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/* 204 */           break;
/*     */         case TRANSFORM_TWO_CARDS: 
/* 206 */           AbstractCard t1 = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(0);
/* 207 */           AbstractCard t2 = (AbstractCard)AbstractDungeon.gridSelectScreen.selectedCards.get(1);
/* 208 */           AbstractDungeon.player.masterDeck.removeCard(t1);
/* 209 */           AbstractDungeon.player.masterDeck.removeCard(t2);
/* 210 */           AbstractDungeon.transformCard(t1);
/* 211 */           AbstractDungeon.topLevelEffects.add(new ShowCardAndObtainEffect(
/*     */           
/* 213 */             AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH / 2.0F - 30.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/* 216 */           AbstractDungeon.transformCard(t2);
/* 217 */           AbstractDungeon.topLevelEffects.add(new ShowCardAndObtainEffect(
/*     */           
/* 219 */             AbstractDungeon.getTransformedCard(), Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH / 2.0F + 30.0F * Settings.scale, Settings.HEIGHT / 2.0F));
/*     */           
/*     */ 
/* 222 */           break;
/*     */         default: 
/* 224 */           logger.info("[ERROR] Missing Neow Reward Type: " + this.type.name());
/*     */         }
/*     */         
/* 227 */         AbstractDungeon.gridSelectScreen.selectedCards.clear();
/* 228 */         AbstractDungeon.overlayMenu.cancelButton.hide();
/* 229 */         SaveHelper.saveIfAppropriate(SaveFile.SaveType.POST_NEOW);
/* 230 */         this.activated = false;
/*     */       }
/* 232 */       if (this.cursed) {
/* 233 */         this.cursed = (!this.cursed);
/* 234 */         AbstractDungeon.topLevelEffects.add(new ShowCardAndObtainEffect(
/*     */         
/* 236 */           AbstractDungeon.getCardWithoutRng(AbstractCard.CardRarity.CURSE), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void activate()
/*     */   {
/* 244 */     this.activated = true;
/* 245 */     switch (this.type) {
/*     */     case THREE_RARE_CARDS: 
/* 247 */       AbstractDungeon.cardRewardScreen.open(getRewardCards(true), null, TEXT[22]);
/* 248 */       break;
/*     */     case HUNDRED_GOLD: 
/* 250 */       CardCrawlGame.sound.play("GOLD_JINGLE");
/* 251 */       AbstractDungeon.player.gainGold(100);
/* 252 */       break;
/*     */     case ONE_RANDOM_RARE_CARD: 
/* 254 */       AbstractDungeon.topLevelEffects.add(new ShowCardAndObtainEffect(
/*     */       
/* 256 */         AbstractDungeon.getCard(AbstractCard.CardRarity.RARE, NeowEvent.rng).makeCopy(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*     */       
/*     */ 
/* 259 */       break;
/*     */     case RANDOM_COMMON_RELIC: 
/* 261 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */       
/*     */ 
/* 264 */         AbstractDungeon.returnRandomRelic(AbstractRelic.RelicTier.COMMON));
/* 265 */       break;
/*     */     case ONE_RARE_RELIC: 
/* 267 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */       
/*     */ 
/* 270 */         AbstractDungeon.returnRandomRelic(AbstractRelic.RelicTier.RARE));
/* 271 */       break;
/*     */     case BOSS_RELIC: 
/* 273 */       AbstractDungeon.player.loseRelic(((AbstractRelic)AbstractDungeon.player.relics.get(0)).relicId);
/* 274 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, 
/*     */       
/*     */ 
/* 277 */         AbstractDungeon.returnRandomRelic(AbstractRelic.RelicTier.BOSS));
/* 278 */       break;
/*     */     case THREE_ENEMY_KILL: 
/* 280 */       AbstractDungeon.getCurrRoom().spawnRelicAndObtain(Settings.WIDTH / 2, Settings.HEIGHT / 2, new NeowsLament());
/*     */       
/*     */ 
/*     */ 
/* 284 */       break;
/*     */     case REMOVE_CARD: 
/* 286 */       AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 287 */         .getPurgeableCards(), 1, TEXT[23], false, false, false, true);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 294 */       break;
/*     */     case REMOVE_TWO: 
/* 296 */       AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 297 */         .getPurgeableCards(), 2, TEXT[24], false, false, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 304 */       break;
/*     */     case TEN_PERCENT_HP_BONUS: 
/* 306 */       AbstractDungeon.player.increaseMaxHp(this.hp_bonus, true);
/* 307 */       break;
/*     */     case THREE_CARDS: 
/* 309 */       AbstractDungeon.cardRewardScreen.open(
/* 310 */         getRewardCards(false), null, 
/*     */         
/* 312 */         CardCrawlGame.languagePack.getUIString("CardRewardScreen").TEXT[1]);
/* 313 */       break;
/*     */     case THREE_SMALL_POTIONS: 
/* 315 */       CardCrawlGame.sound.play("POTION_1");
/* 316 */       for (int i = 0; i < 3; i++) {
/* 317 */         AbstractDungeon.getCurrRoom().addPotionToRewards(com.megacrit.cardcrawl.helpers.PotionHelper.getRandomPotion());
/*     */       }
/* 319 */       AbstractDungeon.combatRewardScreen.open();
/* 320 */       AbstractDungeon.getCurrRoom().rewardPopOutTimer = 0.0F;
/* 321 */       int remove = -1;
/* 322 */       for (int j = 0; j < AbstractDungeon.combatRewardScreen.rewards.size(); j++) {
/* 323 */         if (((RewardItem)AbstractDungeon.combatRewardScreen.rewards.get(j)).type == RewardItem.RewardType.CARD) {
/* 324 */           remove = j;
/* 325 */           break;
/*     */         }
/*     */       }
/* 328 */       if (remove != -1)
/* 329 */         AbstractDungeon.combatRewardScreen.rewards.remove(remove);
/* 330 */       break;
/*     */     
/*     */ 
/*     */     case TRANSFORM_CARD: 
/* 334 */       AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 335 */         .getPurgeableCards(), 1, TEXT[25], false, true, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 342 */       break;
/*     */     case TRANSFORM_TWO_CARDS: 
/* 344 */       AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 345 */         .getPurgeableCards(), 2, TEXT[26], false, false, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 352 */       break;
/*     */     case TWENTY_PERCENT_HP_BONUS: 
/* 354 */       AbstractDungeon.player.increaseMaxHp(this.hp_bonus * 2, true);
/* 355 */       break;
/*     */     case TWO_FIFTY_GOLD: 
/* 357 */       CardCrawlGame.sound.play("GOLD_JINGLE");
/* 358 */       AbstractDungeon.player.gainGold(250);
/* 359 */       break;
/*     */     case UPGRADE_CARD: 
/* 361 */       AbstractDungeon.gridSelectScreen.open(AbstractDungeon.player.masterDeck
/* 362 */         .getUpgradableCards(), 1, TEXT[27], true, false, false, false);
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 371 */     switch (this.drawback) {
/*     */     case CURSE: 
/* 373 */       this.cursed = true;
/* 374 */       break;
/*     */     case NO_GOLD: 
/* 376 */       AbstractDungeon.player.loseGold(AbstractDungeon.player.gold);
/* 377 */       break;
/*     */     case TEN_PERCENT_HP_LOSS: 
/* 379 */       AbstractDungeon.player.decreaseMaxHealth(this.hp_bonus);
/* 380 */       break;
/*     */     case PERCENT_DAMAGE: 
/* 382 */       AbstractDungeon.player.damage(new DamageInfo(null, AbstractDungeon.player.currentHealth / 10 * 3, DamageInfo.DamageType.HP_LOSS));
/*     */       
/* 384 */       break;
/*     */     default: 
/* 386 */       logger.info("[ERROR] Missing Neow Reward Drawback: " + this.drawback.name());
/*     */     }
/*     */     
/* 389 */     CardCrawlGame.metricData.addNeowData(this.type.name(), this.drawback.name());
/*     */   }
/*     */   
/*     */   public ArrayList<AbstractCard> getRewardCards(boolean rareOnly) {
/* 393 */     ArrayList<AbstractCard> retVal = new ArrayList();
/* 394 */     int numCards = 3; for (int i = 0; i < numCards; i++) {
/* 395 */       AbstractCard.CardRarity rarity = rollRarity();
/* 396 */       if (rareOnly) {
/* 397 */         rarity = AbstractCard.CardRarity.RARE;
/*     */       }
/* 399 */       AbstractCard card = null;
/* 400 */       switch (rarity) {
/*     */       case RARE: 
/* 402 */         card = getCard(rarity);
/* 403 */         break;
/*     */       case UNCOMMON: 
/* 405 */         card = getCard(rarity);
/* 406 */         break;
/*     */       case COMMON: 
/* 408 */         card = getCard(rarity);
/* 409 */         break;
/*     */       default: 
/* 411 */         logger.info("WTF?");
/*     */       }
/*     */       
/*     */       
/* 415 */       while (retVal.contains(card)) {
/* 416 */         card = getCard(rarity);
/*     */       }
/* 418 */       retVal.add(card);
/*     */     }
/* 420 */     ArrayList<AbstractCard> retVal2 = new ArrayList();
/* 421 */     for (AbstractCard c : retVal) {
/* 422 */       retVal2.add(c.makeCopy());
/*     */     }
/* 424 */     return retVal2;
/*     */   }
/*     */   
/*     */   public AbstractCard.CardRarity rollRarity() {
/* 428 */     if (NeowEvent.rng.randomBoolean(0.33F)) {
/* 429 */       return AbstractCard.CardRarity.UNCOMMON;
/*     */     }
/* 431 */     return AbstractCard.CardRarity.COMMON;
/*     */   }
/*     */   
/*     */   public AbstractCard getCard(AbstractCard.CardRarity rarity) {
/* 435 */     switch (rarity) {
/*     */     case RARE: 
/* 437 */       return AbstractDungeon.rareCardPool.getRandomCard(NeowEvent.rng);
/*     */     case UNCOMMON: 
/* 439 */       return AbstractDungeon.uncommonCardPool.getRandomCard(NeowEvent.rng);
/*     */     case COMMON: 
/* 441 */       return AbstractDungeon.commonCardPool.getRandomCard(NeowEvent.rng);
/*     */     }
/* 443 */     logger.info("Error in getCard in Neow Reward");
/* 444 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/* 449 */   private static final Logger logger = LogManager.getLogger(NeowReward.class.getName());
/* 450 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Neow Reward");
/* 451 */   public static final String[] NAMES = characterStrings.NAMES;
/* 452 */   public static final String[] TEXT = characterStrings.TEXT;
/* 453 */   public static final String[] UNIQUE_REWARDS = characterStrings.UNIQUE_REWARDS;
/*     */   public String optionLabel;
/*     */   public NeowRewardType type;
/*     */   
/* 457 */   public static enum NeowRewardType { THREE_CARDS,  ONE_RANDOM_RARE_CARD,  REMOVE_CARD,  UPGRADE_CARD,  TRANSFORM_CARD,  THREE_SMALL_POTIONS,  RANDOM_COMMON_RELIC,  TEN_PERCENT_HP_BONUS,  HUNDRED_GOLD,  THREE_ENEMY_KILL,  REMOVE_TWO,  TRANSFORM_TWO_CARDS,  ONE_RARE_RELIC,  THREE_RARE_CARDS,  TWO_FIFTY_GOLD,  TWENTY_PERCENT_HP_BONUS,  BOSS_RELIC;
/*     */     
/*     */     private NeowRewardType() {} }
/*     */   
/* 461 */   public static enum NeowRewardDrawback { NONE,  TEN_PERCENT_HP_LOSS,  NO_GOLD,  CURSE,  PERCENT_DAMAGE;
/*     */     
/*     */     private NeowRewardDrawback() {}
/*     */   }
/*     */   
/*     */   public NeowRewardDrawback drawback;
/*     */   private boolean activated;
/*     */   private int hp_bonus;
/*     */   private boolean cursed;
/*     */   private static final int GOLD_BONUS = 100;
/*     */   private static final int LARGE_GOLD_BONUS = 250;
/*     */   private NeowRewardDrawbackDef drawbackDef;
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\neow\NeowReward.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */