/*     */ package com.megacrit.cardcrawl.neow;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.ui.buttons.DynamicBanner;
/*     */ import com.megacrit.cardcrawl.ui.buttons.UnlockConfirmButton;
/*     */ import com.megacrit.cardcrawl.unlock.AbstractUnlock;
/*     */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*     */ import com.megacrit.cardcrawl.vfx.ConeEffect;
/*     */ import com.megacrit.cardcrawl.vfx.RoomShineEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ 
/*     */ public class NeowUnlockScreen
/*     */ {
/*     */   public ArrayList<AbstractUnlock> unlockBundle;
/*  30 */   private ArrayList<ConeEffect> cones = new ArrayList();
/*     */   private static final int CONE_AMT = 30;
/*  32 */   private static final Color RED_OUTLINE_COLOR = new Color(-10132568);
/*  33 */   private static final Color GREEN_OUTLINE_COLOR = new Color(2147418280);
/*  34 */   private static final Color BLUE_OUTLINE_COLOR = new Color(-2016482392);
/*  35 */   private static final Color BLACK_OUTLINE_COLOR = new Color(168);
/*  36 */   private float shinyTimer = 0.0F;
/*     */   private static final float SHINY_INTERVAL = 0.2F;
/*  38 */   public UnlockConfirmButton button = new UnlockConfirmButton();
/*     */   
/*     */   public long id;
/*     */   
/*  42 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("UnlockScreen");
/*  43 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public void open(ArrayList<AbstractUnlock> unlock) {
/*  46 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.NEOW_UNLOCK;
/*  47 */     this.unlockBundle = unlock;
/*  48 */     this.button.show();
/*  49 */     this.id = CardCrawlGame.sound.play("UNLOCK_SCREEN");
/*     */     
/*  51 */     this.cones.clear();
/*  52 */     for (int i = 0; i < 30; i++) {
/*  53 */       this.cones.add(new ConeEffect());
/*     */     }
/*     */     
/*  56 */     switch (((AbstractUnlock)this.unlockBundle.get(0)).type) {
/*     */     case CARD: 
/*  58 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/*  59 */         UnlockTracker.unlockCard(((AbstractUnlock)this.unlockBundle.get(i)).card.cardID);
/*  60 */         AbstractDungeon.dynamicBanner.appearInstantly(TEXT[0]);
/*  61 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.targetDrawScale = 1.0F;
/*  62 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.drawScale = 0.01F;
/*  63 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.current_x = (Settings.WIDTH * (0.25F * (i + 1)));
/*  64 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.current_y = (Settings.HEIGHT / 2.0F);
/*  65 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.target_x = (Settings.WIDTH * (0.25F * (i + 1)));
/*  66 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.target_y = (Settings.HEIGHT / 2.0F - 30.0F * Settings.scale);
/*     */       }
/*  68 */       break;
/*     */     case RELIC: 
/*  70 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/*  71 */         UnlockTracker.hardUnlockOverride(((AbstractUnlock)this.unlockBundle.get(i)).relic.relicId);
/*  72 */         UnlockTracker.markRelicAsSeen(((AbstractUnlock)this.unlockBundle.get(i)).relic.relicId);
/*  73 */         AbstractDungeon.dynamicBanner.appearInstantly(TEXT[1]);
/*  74 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentX = (Settings.WIDTH * (0.25F * (i + 1)));
/*  75 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentY = (Settings.HEIGHT / 2.0F);
/*  76 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.hb.move(
/*  77 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentX, 
/*  78 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentY);
/*     */       }
/*  80 */       break;
/*     */     case CHARACTER: 
/*  82 */       ((AbstractUnlock)this.unlockBundle.get(0)).onUnlockScreenOpen();
/*  83 */       AbstractDungeon.dynamicBanner.appearInstantly(TEXT[2]);
/*  84 */       break;
/*     */     case MISC: 
/*     */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void reOpen()
/*     */   {
/*  93 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.NEOW_UNLOCK;
/*  94 */     this.button.show();
/*  95 */     this.id = CardCrawlGame.sound.play("UNLOCK_SCREEN");
/*     */     
/*  97 */     this.cones.clear();
/*  98 */     for (int i = 0; i < 30; i++) {
/*  99 */       this.cones.add(new ConeEffect());
/*     */     }
/*     */     
/* 102 */     switch (((AbstractUnlock)this.unlockBundle.get(0)).type) {
/*     */     case CARD: 
/* 104 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 105 */         UnlockTracker.unlockCard(((AbstractUnlock)this.unlockBundle.get(i)).card.cardID);
/* 106 */         AbstractDungeon.dynamicBanner.appearInstantly(TEXT[0]);
/* 107 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.targetDrawScale = 1.0F;
/* 108 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.drawScale = 0.01F;
/* 109 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.current_x = (Settings.WIDTH * (0.25F * (i + 1)));
/* 110 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.current_y = (Settings.HEIGHT / 2.0F);
/* 111 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.target_x = (Settings.WIDTH * (0.25F * (i + 1)));
/* 112 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.target_y = (Settings.HEIGHT / 2.0F - 30.0F * Settings.scale);
/*     */       }
/* 114 */       break;
/*     */     case RELIC: 
/* 116 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 117 */         UnlockTracker.hardUnlockOverride(((AbstractUnlock)this.unlockBundle.get(i)).relic.relicId);
/* 118 */         UnlockTracker.markRelicAsSeen(((AbstractUnlock)this.unlockBundle.get(i)).relic.relicId);
/* 119 */         AbstractDungeon.dynamicBanner.appearInstantly(TEXT[1]);
/* 120 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentX = (Settings.WIDTH * (0.25F * (i + 1)));
/* 121 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentY = (Settings.HEIGHT / 2.0F);
/* 122 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.hb.move(
/* 123 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentX, 
/* 124 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.currentY);
/*     */       }
/* 126 */       break;
/*     */     case CHARACTER: 
/* 128 */       ((AbstractUnlock)this.unlockBundle.get(0)).onUnlockScreenOpen();
/* 129 */       AbstractDungeon.dynamicBanner.appearInstantly(TEXT[2]);
/* 130 */       break;
/*     */     case MISC: 
/*     */       break;
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/* 139 */     this.shinyTimer -= com.badlogic.gdx.Gdx.graphics.getDeltaTime();
/* 140 */     if (this.shinyTimer < 0.0F) {
/* 141 */       this.shinyTimer = 0.2F;
/* 142 */       AbstractDungeon.topLevelEffects.add(new RoomShineEffect());
/* 143 */       AbstractDungeon.topLevelEffects.add(new RoomShineEffect());
/* 144 */       AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.RoomShineEffect2());
/*     */     }
/*     */     
/* 147 */     switch (((AbstractUnlock)this.unlockBundle.get(0)).type) {
/*     */     case CARD: 
/* 149 */       updateConeEffect();
/* 150 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 151 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.update();
/* 152 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.updateHoverLogic();
/* 153 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.targetDrawScale = 1.0F;
/*     */       }
/* 155 */       break;
/*     */     case RELIC: 
/* 157 */       updateConeEffect();
/* 158 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 159 */         ((AbstractUnlock)this.unlockBundle.get(i)).relic.update();
/*     */       }
/* 161 */       break;
/*     */     case CHARACTER: 
/* 163 */       updateConeEffect();
/* 164 */       ((AbstractUnlock)this.unlockBundle.get(0)).player.update();
/* 165 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 170 */     this.button.update();
/*     */   }
/*     */   
/*     */   private void updateConeEffect() {
/* 174 */     for (Iterator<ConeEffect> e = this.cones.iterator(); e.hasNext();) {
/* 175 */       ConeEffect d = (ConeEffect)e.next();
/* 176 */       d.update();
/* 177 */       if (d.isDone) {
/* 178 */         e.remove();
/*     */       }
/*     */     }
/*     */     
/* 182 */     if (this.cones.size() < 30) {
/* 183 */       this.cones.add(new ConeEffect());
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 188 */     sb.setColor(new Color(0.05F, 0.15F, 0.18F, 1.0F));
/* 189 */     sb.draw(ImageMaster.WHITE_SQUARE_IMG, 0.0F, 0.0F, Settings.WIDTH, Settings.HEIGHT);
/*     */     
/* 191 */     sb.setBlendFunction(770, 1);
/* 192 */     for (ConeEffect e : this.cones) {
/* 193 */       e.render(sb);
/*     */     }
/* 195 */     sb.setBlendFunction(770, 771);
/*     */     
/* 197 */     switch (((AbstractUnlock)this.unlockBundle.get(0)).type) {
/*     */     case CARD: 
/* 199 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 200 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.renderHoverShadow(sb);
/* 201 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.render(sb);
/* 202 */         ((AbstractUnlock)this.unlockBundle.get(i)).card.renderCardTip(sb);
/*     */       }
/*     */       
/* 205 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.66F));
/* 206 */       sb.draw(ImageMaster.UNLOCK_TEXT_BG, Settings.WIDTH / 2.0F - 500.0F, 274.0F * Settings.scale - 130.0F, 500.0F, 130.0F, 1000.0F, 130.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1000, 260, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 224 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[3], Settings.WIDTH / 2.0F, 210.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 231 */       break;
/*     */     case CHARACTER: 
/* 233 */       ((AbstractUnlock)this.unlockBundle.get(0)).render(sb);
/* 234 */       ((AbstractUnlock)this.unlockBundle.get(0)).player.renderPlayerImage(sb);
/* 235 */       break;
/*     */     case RELIC: 
/* 237 */       for (int i = 0; i < this.unlockBundle.size(); i++) {
/* 238 */         if (RelicLibrary.redList.contains(((AbstractUnlock)this.unlockBundle.get(i)).relic)) {
/* 239 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.render(sb, false, RED_OUTLINE_COLOR);
/* 240 */         } else if (RelicLibrary.greenList.contains(((AbstractUnlock)this.unlockBundle.get(i)).relic)) {
/* 241 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.render(sb, false, GREEN_OUTLINE_COLOR);
/* 242 */         } else if (RelicLibrary.blueList.contains(((AbstractUnlock)this.unlockBundle.get(i)).relic)) {
/* 243 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.render(sb, false, BLUE_OUTLINE_COLOR);
/*     */         } else {
/* 245 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.render(sb, false, BLACK_OUTLINE_COLOR);
/*     */         }
/*     */         
/* 248 */         sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.66F));
/* 249 */         sb.draw(ImageMaster.UNLOCK_TEXT_BG, Settings.WIDTH / 2.0F - 500.0F, 280.0F * Settings.scale - 130.0F, 500.0F, 130.0F, 1000.0F, 130.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 1000, 260, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 267 */         FontHelper.renderFontCentered(sb, FontHelper.bannerFont, 
/*     */         
/*     */ 
/* 270 */           ((AbstractUnlock)this.unlockBundle.get(i)).relic.name, Settings.WIDTH * (0.25F * (i + 1)), Settings.HEIGHT / 2.0F - 150.0F * Settings.scale, Settings.GOLD_COLOR);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 275 */       FontHelper.renderFontCentered(sb, FontHelper.cardTitleFont_small_N, TEXT[4], Settings.WIDTH / 2.0F, 220.0F * Settings.scale, Settings.CREAM_COLOR);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 282 */       break;
/*     */     }
/*     */     
/*     */     
/*     */ 
/* 287 */     this.button.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\neow\NeowUnlockScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */