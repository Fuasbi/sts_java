/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.ArtifactPower;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class AncientAugmentation extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "MetallicRebirth";
/* 15 */   private static final BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("MetallicRebirth");
/* 16 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public AncientAugmentation() {
/* 19 */     super("MetallicRebirth", NAME, DESC[0] + 1 + DESC[1] + 10 + DESC[2] + 10 + DESC[3], "ancient.png", false);
/* 20 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 25 */     this.counter += 1;
/* 26 */     updateDescription();
/* 27 */     flash();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 32 */     this.description = (DESC[0] + this.counter + DESC[1] + this.counter * 10 + DESC[2] + this.counter * 10 + DESC[3]);
/* 33 */     this.tips.clear();
/* 34 */     this.tips.add(new PowerTip(this.name, this.description));
/* 35 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onCreateEnemy(AbstractMonster m)
/*    */   {
/* 40 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new ArtifactPower(m, this.counter), this.counter));
/* 41 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.PlatedArmorPower(m, this.counter * 10), this.counter * 10));
/*    */     
/* 43 */     AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.RegenerateMonsterPower(m, this.counter * 10), this.counter * 10));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\AncientAugmentation.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */