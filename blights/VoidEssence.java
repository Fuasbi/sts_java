/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class VoidEssence extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "VoidEssence";
/* 11 */   private static final com.megacrit.cardcrawl.localization.BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("VoidEssence");
/* 12 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public VoidEssence() {
/* 15 */     super("VoidEssence", NAME, DESC[0], "void.png", false);
/* 16 */     this.counter = 1;
/* 17 */     updateDescription();
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 22 */     this.counter += 1;
/* 23 */     updateDescription();
/* 24 */     if (AbstractDungeon.player.energy.energyMaster > 0) {
/* 25 */       AbstractDungeon.player.energy.energyMaster -= 1;
/*    */     }
/* 27 */     flash();
/*    */   }
/*    */   
/*    */   public void updateDescription(com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass c)
/*    */   {
/* 32 */     switch (c) {
/*    */     case IRONCLAD: 
/* 34 */       this.description = (DESC[0] + this.counter + DESC[1]);
/* 35 */       break;
/*    */     case THE_SILENT: 
/* 37 */       this.description = (DESC[0] + this.counter + DESC[2]);
/* 38 */       break;
/*    */     case DEFECT: 
/* 40 */       this.description = (DESC[0] + this.counter + DESC[3]);
/* 41 */       break;
/*    */     }
/*    */     
/*    */     
/*    */ 
/* 46 */     this.tips.clear();
/* 47 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 48 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 53 */     if (AbstractDungeon.player != null) {
/* 54 */       switch (AbstractDungeon.player.chosenClass) {
/*    */       case IRONCLAD: 
/* 56 */         this.description = (DESC[0] + this.counter + DESC[1]);
/* 57 */         break;
/*    */       case THE_SILENT: 
/* 59 */         this.description = (DESC[0] + this.counter + DESC[2]);
/* 60 */         break;
/*    */       case DEFECT: 
/* 62 */         this.description = (DESC[0] + this.counter + DESC[3]);
/* 63 */         break;
/*    */       }
/*    */       
/*    */     }
/*    */     
/*    */ 
/* 69 */     this.tips.clear();
/* 70 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 71 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 76 */     if (AbstractDungeon.player.energy.energyMaster > 0) {
/* 77 */       AbstractDungeon.player.energy.energyMaster -= 1;
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\VoidEssence.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */