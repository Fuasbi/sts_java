/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Hauntings extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "GraspOfShadows";
/* 13 */   private static final BlightStrings blightStrings = CardCrawlGame.languagePack.getBlightString("GraspOfShadows");
/* 14 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public Hauntings() {
/* 17 */     super("GraspOfShadows", NAME, DESC[0] + 1 + DESC[1], "hauntings.png", false);
/* 18 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 23 */     this.counter += 1;
/* 24 */     updateDescription();
/* 25 */     flash();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 30 */     this.description = (DESC[0] + this.counter + DESC[1]);
/* 31 */     this.tips.clear();
/* 32 */     this.tips.add(new PowerTip(this.name, this.description));
/* 33 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onCreateEnemy(AbstractMonster m)
/*    */   {
/* 38 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, m, new com.megacrit.cardcrawl.powers.IntangiblePlayerPower(m, this.counter), this.counter));
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\Hauntings.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */