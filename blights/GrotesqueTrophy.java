/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ 
/*    */ public class GrotesqueTrophy extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "GrotesqueTrophy";
/* 13 */   private static final BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("GrotesqueTrophy");
/* 14 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public GrotesqueTrophy() {
/* 17 */     super("GrotesqueTrophy", NAME, DESC[0] + 3 + DESC[1], "trophy.png", false);
/* 18 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 23 */     CardGroup group = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*    */     
/* 25 */     for (int i = 0; i < 3; i++) {
/* 26 */       AbstractCard bloatCard = new com.megacrit.cardcrawl.cards.curses.Pride();
/* 27 */       UnlockTracker.markCardAsSeen(bloatCard.cardID);
/* 28 */       group.addToBottom(bloatCard.makeCopy());
/*    */     }
/*    */     
/* 31 */     AbstractDungeon.gridSelectScreen.openConfirmationGrid(group, DESC[2]);
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 36 */     this.counter += 1;
/* 37 */     flash();
/*    */     
/* 39 */     CardGroup group = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*    */     
/* 41 */     for (int i = 0; i < 3; i++) {
/* 42 */       AbstractCard bloatCard = new com.megacrit.cardcrawl.cards.curses.Pride();
/* 43 */       UnlockTracker.markCardAsSeen(bloatCard.cardID);
/* 44 */       group.addToBottom(bloatCard.makeCopy());
/*    */     }
/*    */     
/* 47 */     AbstractDungeon.gridSelectScreen.openConfirmationGrid(group, DESC[2]);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\GrotesqueTrophy.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */