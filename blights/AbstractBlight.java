/*     */ package com.megacrit.cardcrawl.blights;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.badlogic.gdx.math.Interpolation.ExpIn;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer.PlayerClass;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.GameDataStringBuilder;
/*     */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*     */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
/*     */ import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen.CurScreen;
/*     */ import com.megacrit.cardcrawl.ui.panels.PotionPopUp;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ import com.megacrit.cardcrawl.vfx.FloatyEffect;
/*     */ import com.megacrit.cardcrawl.vfx.GlowRelicParticle;
/*     */ import com.megacrit.cardcrawl.vfx.SmokePuffEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Scanner;
/*     */ import java.util.TreeMap;
/*     */ 
/*     */ 
/*     */ public class AbstractBlight
/*     */ {
/*  45 */   public boolean isDone = false; public boolean isAnimating = false; public boolean isObtained = false;
/*     */   
/*  47 */   public int counter = -1;
/*  48 */   public ArrayList<PowerTip> tips = new ArrayList();
/*  49 */   public String imgUrl = "";
/*     */   
/*     */ 
/*     */ 
/*  53 */   private static final float START_X = 64.0F * Settings.scale; private static final float START_Y = Settings.HEIGHT - 176.0F * Settings.scale;
/*  54 */   private static final Color PASSIVE_OUTLINE_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.33F);
/*  55 */   public boolean isSeen = false;
/*  56 */   public float scale = Settings.scale;
/*     */   
/*     */ 
/*  59 */   public Hitbox hb = new Hitbox(AbstractRelic.PAD_X, AbstractRelic.PAD_X);
/*     */   
/*     */ 
/*  62 */   private float rotation = 0.0F;
/*  63 */   public boolean discarded = false;
/*     */   
/*     */ 
/*  66 */   protected boolean pulse = false;
/*  67 */   private float animationTimer = 0.0F;
/*  68 */   public float flashTimer = 0.0F;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  73 */   private float glowTimer = 0.0F;
/*  74 */   private FloatyEffect f_effect = new FloatyEffect(10.0F, 0.2F);
/*     */   
/*     */ 
/*  77 */   private static float offsetX = 0.0F;
/*     */   public String name;
/*     */   
/*  80 */   public AbstractBlight(String setId, String name, String description, String imgName, boolean unique) { this.blightID = setId;
/*  81 */     this.name = name;
/*  82 */     this.description = description;
/*  83 */     this.unique = unique;
/*  84 */     this.img = ImageMaster.loadImage("images/blights/" + imgName);
/*  85 */     this.outlineImg = ImageMaster.loadImage("images/blights/outline/" + imgName);
/*  86 */     this.increment = 0;
/*  87 */     this.tips.add(new PowerTip(name, description));
/*     */   }
/*     */   
/*     */   public String description;
/*  91 */   public void spawn(float x, float y) { AbstractDungeon.effectsQueue.add(new SmokePuffEffect(x, y));
/*  92 */     this.currentX = x;
/*  93 */     this.currentY = y;
/*  94 */     this.isAnimating = true;
/*  95 */     this.isObtained = false;
/*  96 */     this.f_effect.x = 0.0F;
/*  97 */     this.f_effect.y = 0.0F;
/*  98 */     this.hb = new Hitbox(AbstractRelic.PAD_X, AbstractRelic.PAD_X);
/*     */   }
/*     */   
/*     */   public void renderInTopPanel(SpriteBatch sb) {
/* 102 */     if (Settings.hideRelics) {
/* 103 */       return;
/*     */     }
/*     */     
/* 106 */     renderOutline(sb, true);
/* 107 */     sb.setColor(Color.WHITE);
/* 108 */     sb.draw(this.img, this.currentX - 64.0F + offsetX, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 125 */     renderCounter(sb, true);
/* 126 */     renderFlash(sb, true);
/* 127 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 131 */     if (Settings.hideRelics) {
/* 132 */       return;
/*     */     }
/*     */     
/*     */ 
/* 136 */     if (this.isDone) {
/* 137 */       renderOutline(sb, false);
/*     */     }
/*     */     
/* 140 */     if ((!this.isObtained) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.SHOP)))
/*     */     {
/*     */ 
/*     */ 
/* 144 */       if (this.hb.hovered) {
/* 145 */         renderTip(sb);
/*     */       }
/*     */       
/* 148 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/* 149 */         if (this.hb.hovered) {
/* 150 */           sb.setColor(PASSIVE_OUTLINE_COLOR);
/* 151 */           sb.draw(this.outlineImg, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 169 */           sb.setColor(PASSIVE_OUTLINE_COLOR);
/* 170 */           sb.draw(this.outlineImg, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 191 */     if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/* 192 */       if (!this.isObtained) {
/* 193 */         sb.setColor(Color.WHITE);
/* 194 */         sb.draw(this.img, this.currentX - 64.0F + this.f_effect.x, this.currentY - 64.0F + this.f_effect.y, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 212 */         sb.setColor(Color.WHITE);
/* 213 */         sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 230 */         renderCounter(sb, false);
/*     */       }
/*     */     } else {
/* 233 */       sb.setColor(Color.WHITE);
/* 234 */       sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 252 */       renderCounter(sb, false);
/*     */     }
/*     */     
/* 255 */     if (this.isDone) {
/* 256 */       renderFlash(sb, false);
/*     */     }
/*     */     
/* 259 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   protected void updateAnimation() {
/* 263 */     if (this.animationTimer != 0.0F) {
/* 264 */       this.animationTimer -= Gdx.graphics.getDeltaTime();
/* 265 */       if (this.animationTimer < 0.0F) {
/* 266 */         this.animationTimer = 0.0F;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb, boolean renderAmount, Color outlineColor) {
/* 272 */     if (this.isSeen) {
/* 273 */       renderOutline(outlineColor, sb, false);
/*     */     } else {
/* 275 */       renderOutline(Color.LIGHT_GRAY, sb, false);
/*     */     }
/*     */     
/* 278 */     if (this.isSeen) {
/* 279 */       sb.setColor(Color.WHITE);
/*     */     }
/* 281 */     else if (this.hb.hovered) {
/* 282 */       sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
/*     */     } else {
/* 284 */       sb.setColor(Color.BLACK);
/*     */     }
/*     */     
/*     */ 
/* 288 */     if ((AbstractDungeon.screen != null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NEOW_UNLOCK)) {
/* 289 */       if (this.largeImg == null) {
/* 290 */         sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F + 
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 298 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, Settings.scale * 2.0F + 
/* 299 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, this.rotation, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/* 308 */         sb.draw(this.largeImg, this.currentX - 128.0F, this.currentY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, Settings.scale * 1.0F + 
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 316 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 30.0F, Settings.scale * 1.0F + 
/* 317 */           MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 30.0F, this.rotation, 0, 0, 256, 256, false, false);
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 327 */       sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 346 */     if (this.hb.hovered) {
/* 347 */       renderTip(sb);
/*     */     }
/*     */     
/* 350 */     this.hb.render(sb);
/*     */   }
/*     */   
/*     */   public void renderCounter(SpriteBatch sb, boolean inTopPanel) {
/* 354 */     if (this.counter > -1) {
/* 355 */       FontHelper.renderFontRightTopAligned(sb, FontHelper.topPanelInfoFont, 
/*     */       
/*     */ 
/* 358 */         Integer.toString(this.counter), this.currentX + 30.0F * Settings.scale, this.currentY - 7.0F * Settings.scale, Color.WHITE);
/*     */     }
/*     */   }
/*     */   
/*     */   public String blightID;
/*     */   public Texture img;
/*     */   public Texture outlineImg;
/*     */   public void renderOutline(Color c, SpriteBatch sb, boolean inTopPanel) {
/* 366 */     sb.setColor(c);
/* 367 */     if ((AbstractDungeon.screen != null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NEOW_UNLOCK)) {
/* 368 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, Settings.scale * 2.0F + 
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 376 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, Settings.scale * 2.0F + 
/* 377 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) / 15.0F, this.rotation, 0, 0, 128, 128, false, false);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 386 */     else if ((this.hb.hovered) && (Settings.isControllerMode)) {
/* 387 */       sb.setBlendFunction(770, 1);
/* 388 */       sb.setColor(new Color(1.0F, 0.9F, 0.4F, 0.6F + 
/* 389 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 390 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 407 */       sb.setBlendFunction(770, 771);
/*     */     } else {
/* 409 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */   public Texture largeImg;
/*     */   public boolean unique;
/*     */   public int increment;
/*     */   private static final String IMG_DIR = "images/blights/";
/*     */   private static final String OUTLINE_DIR = "images/blights/outline/";
/*     */   public float floatModAmount;
/*     */   public int cost;
/*     */   public static final int RAW_W = 128;
/*     */   public float currentX;
/*     */   public float currentY;
/*     */   public float targetX;
/*     */   public float targetY;
/*     */   public static final int MAX_BLIGHTS_PER_PAGE = 25;
/*     */   private static final float OBTAIN_SPEED = 6.0F;
/*     */   private static final float OBTAIN_THRESHOLD = 0.5F;
/*     */   private static final float FLASH_ANIM_TIME = 2.0F;
/*     */   private static final float DEFAULT_ANIM_SCALE = 4.0F;
/*     */   public void renderOutline(SpriteBatch sb, boolean inTopPanel) {
/* 431 */     if ((this.hb.hovered) && (Settings.isControllerMode)) {
/* 432 */       sb.setBlendFunction(770, 1);
/* 433 */       sb.setColor(new Color(1.0F, 0.9F, 0.4F, 0.6F + 
/* 434 */         MathUtils.cosDeg((float)(System.currentTimeMillis() / 2L % 360L)) / 5.0F));
/* 435 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 452 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     else {
/* 455 */       sb.setColor(PASSIVE_OUTLINE_COLOR);
/* 456 */       sb.draw(this.outlineImg, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale, this.scale, this.rotation, 0, 0, 128, 128, false, false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void renderFlash(SpriteBatch sb, boolean inTopPanel)
/*     */   {
/* 477 */     float tmp = Interpolation.exp10In.apply(0.0F, 4.0F, this.flashTimer / 2.0F);
/*     */     
/* 479 */     sb.setBlendFunction(770, 1);
/* 480 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, this.flashTimer * 0.2F));
/* 481 */     sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp, this.scale + tmp, this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 499 */     sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp * 0.66F, this.scale + tmp * 0.66F, this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 517 */     sb.draw(this.img, this.currentX - 64.0F, this.currentY - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, this.scale + tmp / 3.0F, this.scale + tmp / 3.0F, this.rotation, 0, 0, 128, 128, false, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 535 */     sb.setBlendFunction(770, 771);
/*     */   }
/*     */   
/*     */   public void flash() {
/* 539 */     this.flashTimer = 2.0F;
/*     */   }
/*     */   
/*     */   public void renderTip(SpriteBatch sb) {
/* 543 */     if (InputHelper.mX < 1400.0F * Settings.scale) {
/* 544 */       if (CardCrawlGame.mainMenuScreen.screen == MainMenuScreen.CurScreen.RELIC_VIEW) {
/* 545 */         TipHelper.queuePowerTips(180.0F * Settings.scale, Settings.HEIGHT * 0.7F, this.tips);
/*     */       } else {
/* 547 */         TipHelper.queuePowerTips(InputHelper.mX + 50.0F * Settings.scale, InputHelper.mY + 50.0F * Settings.scale, this.tips);
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else {
/* 553 */       TipHelper.queuePowerTips(InputHelper.mX - 350.0F * Settings.scale, InputHelper.mY - 50.0F * Settings.scale, this.tips);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected void initializeTips()
/*     */   {
/* 561 */     Scanner desc = new Scanner(this.description);
/*     */     
/* 563 */     while (desc.hasNext()) {
/* 564 */       String s = desc.next();
/* 565 */       if (s.charAt(0) == '#') {
/* 566 */         s = s.substring(2);
/*     */       }
/*     */       
/* 569 */       s = s.replace(',', ' ');
/* 570 */       s = s.replace('.', ' ');
/* 571 */       s = s.trim();
/* 572 */       s = s.toLowerCase();
/*     */       
/* 574 */       boolean alreadyExists = false;
/* 575 */       if (GameDictionary.keywords.containsKey(s)) {
/* 576 */         s = (String)GameDictionary.parentWord.get(s);
/* 577 */         for (PowerTip t : this.tips) {
/* 578 */           if (t.header.toLowerCase().equals(s)) {
/* 579 */             alreadyExists = true;
/* 580 */             break;
/*     */           }
/*     */         }
/* 583 */         if (!alreadyExists) {
/* 584 */           this.tips.add(new PowerTip(TipHelper.capitalize(s), (String)GameDictionary.keywords.get(s)));
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 589 */     desc.close();
/*     */   }
/*     */   
/*     */   public void obtain() {
/* 593 */     this.hb.hovered = false;
/* 594 */     int slot = AbstractDungeon.player.blights.size();
/* 595 */     this.targetX = (START_X + slot * AbstractRelic.PAD_X);
/* 596 */     this.targetY = START_Y;
/* 597 */     AbstractDungeon.player.blights.add(this);
/*     */   }
/*     */   
/*     */   public void instantObtain(AbstractPlayer p, int slot, boolean callOnEquip) {
/* 601 */     this.isDone = true;
/* 602 */     this.isObtained = true;
/*     */     
/* 604 */     if (slot >= p.blights.size()) {
/* 605 */       p.blights.add(this);
/*     */     } else {
/* 607 */       p.blights.set(slot, this);
/*     */     }
/*     */     
/* 610 */     this.currentX = (START_X + slot * AbstractRelic.PAD_X);
/* 611 */     this.currentY = START_Y;
/* 612 */     this.targetX = this.currentX;
/* 613 */     this.targetY = this.currentY;
/* 614 */     this.hb.move(this.currentX, this.currentY);
/*     */     
/* 616 */     if (callOnEquip) {
/* 617 */       onEquip();
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateFlash() {
/* 622 */     if (this.flashTimer != 0.0F) {
/* 623 */       this.flashTimer -= Gdx.graphics.getDeltaTime();
/* 624 */       if (this.flashTimer < 0.0F) {
/* 625 */         if (this.pulse) {
/* 626 */           this.flashTimer = 1.0F;
/*     */         } else {
/* 628 */           this.flashTimer = 0.0F;
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void setCounter(int counter) {
/* 635 */     this.counter = counter;
/*     */   }
/*     */   
/*     */   public void update() {
/* 639 */     updateFlash();
/*     */     
/* 641 */     if (!this.isDone)
/*     */     {
/* 643 */       if (this.isAnimating) {
/* 644 */         this.glowTimer -= Gdx.graphics.getDeltaTime();
/* 645 */         if (this.glowTimer < 0.0F) {
/* 646 */           this.glowTimer = 0.5F;
/* 647 */           AbstractDungeon.effectList.add(new GlowRelicParticle(this.img, this.currentX + this.f_effect.x, this.currentY + this.f_effect.y, this.rotation));
/*     */         }
/*     */         
/* 650 */         this.f_effect.update();
/* 651 */         if (this.hb.hovered) {
/* 652 */           this.scale = (Settings.scale * 1.5F);
/*     */         } else {
/* 654 */           this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale * 1.1F);
/*     */         }
/*     */       }
/* 657 */       else if (this.hb.hovered) {
/* 658 */         this.scale = (Settings.scale * 1.25F);
/*     */       } else {
/* 660 */         this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale);
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 665 */       if (this.isObtained) {
/* 666 */         if (this.rotation != 0.0F) {
/* 667 */           this.rotation = MathUtils.lerp(this.rotation, 0.0F, Gdx.graphics.getDeltaTime() * 6.0F * 2.0F);
/*     */         }
/*     */         
/* 670 */         if (this.currentX != this.targetX) {
/* 671 */           this.currentX = MathUtils.lerp(this.currentX, this.targetX, Gdx.graphics.getDeltaTime() * 6.0F);
/* 672 */           if (Math.abs(this.currentX - this.targetX) < 0.5F) {
/* 673 */             this.currentX = this.targetX;
/*     */           }
/*     */         }
/* 676 */         if (this.currentY != this.targetY) {
/* 677 */           this.currentY = MathUtils.lerp(this.currentY, this.targetY, Gdx.graphics.getDeltaTime() * 6.0F);
/* 678 */           if (Math.abs(this.currentY - this.targetY) < 0.5F) {
/* 679 */             this.currentY = this.targetY;
/*     */           }
/*     */         }
/*     */         
/* 683 */         if ((this.currentY == this.targetY) && (this.currentX == this.targetX)) {
/* 684 */           this.isDone = true;
/* 685 */           onEquip();
/* 686 */           this.hb.move(this.currentX, this.currentY);
/*     */         }
/* 688 */         this.scale = Settings.scale;
/*     */       }
/*     */       
/* 691 */       if (this.hb != null) {
/* 692 */         this.hb.update();
/*     */         
/* 694 */         if ((this.hb.hovered) && ((!AbstractDungeon.isScreenUp) || (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD)) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.NEOW_UNLOCK))
/*     */         {
/*     */ 
/* 697 */           if (((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) && (!this.isObtained)) {
/* 698 */             if (AbstractDungeon.player.hasBlight(this.blightID)) {
/* 699 */               AbstractDungeon.player.getBlight(this.blightID).stack();
/* 700 */               this.isObtained = true;
/*     */             } else {
/* 702 */               obtain();
/* 703 */               InputHelper.justClickedLeft = false;
/* 704 */               this.isObtained = true;
/* 705 */               this.f_effect.x = 0.0F;
/* 706 */               this.f_effect.y = 0.0F;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/* 711 */       if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.BOSS_REWARD) {
/* 712 */         updateAnimation();
/*     */       }
/*     */     } else {
/* 715 */       this.hb.update();
/*     */       
/* 717 */       if ((this.hb.hovered) && (AbstractDungeon.topPanel.potionUi.isHidden)) {
/* 718 */         this.scale = (Settings.scale * 1.25F);
/*     */       }
/*     */       else {
/* 721 */         this.scale = MathHelper.scaleLerpSnap(this.scale, Settings.scale);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void onPlayCard(AbstractCard card, AbstractMonster m) {}
/*     */   
/*     */ 
/*     */   public boolean canPlay(AbstractCard card)
/*     */   {
/* 732 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void onVictory() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void atBattleStart() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void atTurnStart() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onPlayerEndTurn() {}
/*     */   
/*     */ 
/*     */ 
/*     */   public void onBossDefeat() {}
/*     */   
/*     */ 
/*     */   public void onCreateEnemy(AbstractMonster m) {}
/*     */   
/*     */ 
/*     */   public void effect() {}
/*     */   
/*     */ 
/*     */   public void onEquip() {}
/*     */   
/*     */ 
/*     */   public float effectFloat()
/*     */   {
/* 767 */     return this.floatModAmount;
/*     */   }
/*     */   
/*     */   public void incrementUp() {}
/*     */   
/*     */   public void setIncrement(int newInc)
/*     */   {
/* 774 */     this.increment = newInc;
/*     */   }
/*     */   
/*     */   public static String gameDataUploadHeader() {
/* 778 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 779 */     sb.addFieldData("name");
/* 780 */     sb.addFieldData("text");
/* 781 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public String gameDataUploadData() {
/* 785 */     GameDataStringBuilder sb = new GameDataStringBuilder();
/* 786 */     sb.addFieldData(this.name);
/* 787 */     sb.addFieldData(this.description);
/* 788 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public void stack() {}
/*     */   
/*     */   public void updateDescription(AbstractPlayer.PlayerClass c) {}
/*     */   
/*     */   public void updateDescription() {}
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\AbstractBlight.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */