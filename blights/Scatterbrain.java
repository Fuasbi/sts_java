/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Scatterbrain extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "Scatterbrain";
/* 10 */   private static final BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("Scatterbrain");
/* 11 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public Scatterbrain() {
/* 14 */     super("Scatterbrain", NAME, DESC[0] + 1 + DESC[1], "scatter.png", false);
/* 15 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 20 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.masterHandSize -= 1;
/* 21 */     this.counter += 1;
/* 22 */     updateDescription();
/* 23 */     flash();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 28 */     this.description = (DESC[0] + this.counter + DESC[1]);
/* 29 */     this.tips.clear();
/* 30 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 31 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onEquip()
/*    */   {
/* 36 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.player.masterHandSize -= 1;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\Scatterbrain.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */