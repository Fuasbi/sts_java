/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*    */ import com.megacrit.cardcrawl.cards.status.Burn;
/*    */ import com.megacrit.cardcrawl.cards.status.Slimed;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.PowerTip;
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class TwistingMind extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "TwistingMind";
/* 17 */   private static final BlightStrings blightStrings = CardCrawlGame.languagePack.getBlightString("TwistingMind");
/* 18 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public TwistingMind() {
/* 21 */     super("TwistingMind", NAME, DESC[0] + 1 + DESC[1], "twist.png", false);
/* 22 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void stack()
/*    */   {
/* 27 */     this.counter += 1;
/* 28 */     updateDescription();
/* 29 */     flash();
/*    */   }
/*    */   
/*    */   public void updateDescription()
/*    */   {
/* 34 */     this.description = (DESC[0] + this.counter + DESC[1]);
/* 35 */     this.tips.clear();
/* 36 */     this.tips.add(new PowerTip(this.name, this.description));
/* 37 */     initializeTips();
/*    */   }
/*    */   
/*    */   public void onPlayerEndTurn()
/*    */   {
/* 42 */     flash();
/* 43 */     CardGroup group = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*    */     
/* 45 */     group.addToBottom(new Slimed());
/* 46 */     group.addToBottom(new com.megacrit.cardcrawl.cards.status.Void());
/* 47 */     group.addToBottom(new Burn());
/* 48 */     group.addToBottom(new com.megacrit.cardcrawl.cards.status.Dazed());
/* 49 */     group.addToBottom(new com.megacrit.cardcrawl.cards.status.Wound());
/* 50 */     group.shuffle();
/*    */     
/* 52 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(group
/* 53 */       .getBottomCard(), this.counter, false, true));
/*    */     
/* 55 */     group.clear();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\TwistingMind.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */