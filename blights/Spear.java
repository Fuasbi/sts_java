/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Spear extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "DeadlyEnemies";
/*  9 */   private static final BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("DeadlyEnemies");
/* 10 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/* 11 */   public float damageMod = 2.0F;
/*    */   
/*    */   public Spear() {
/* 14 */     super("DeadlyEnemies", NAME, DESC[0] + 100 + DESC[1], "spear.png", true);
/* 15 */     this.counter = 1;
/*    */   }
/*    */   
/*    */   public void incrementUp()
/*    */   {
/* 20 */     this.damageMod += 0.75F;
/* 21 */     this.increment += 1;
/* 22 */     this.counter += 1;
/* 23 */     this.description = (DESC[0] + (int)((this.damageMod - 1.0F) * 100.0F) + DESC[1]);
/* 24 */     this.tips.clear();
/* 25 */     this.tips.add(new com.megacrit.cardcrawl.helpers.PowerTip(this.name, this.description));
/* 26 */     initializeTips();
/*    */   }
/*    */   
/*    */   public float effectFloat()
/*    */   {
/* 31 */     return this.damageMod;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\Spear.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */