/*    */ package com.megacrit.cardcrawl.blights;
/*    */ 
/*    */ import com.megacrit.cardcrawl.localization.BlightStrings;
/*    */ 
/*    */ public class MimicInfestation extends AbstractBlight
/*    */ {
/*    */   public static final String ID = "MimicInfestation";
/*  8 */   private static final BlightStrings blightStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getBlightString("MimicInfestation");
/*  9 */   public static final String NAME = blightStrings.NAME; public static final String[] DESC = blightStrings.DESCRIPTION;
/*    */   
/*    */   public MimicInfestation() {
/* 12 */     super("MimicInfestation", NAME, DESC[0], "mimic.png", true);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\blights\MimicInfestation.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */