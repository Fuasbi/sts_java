/*     */ package com.megacrit.cardcrawl.shop;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.UIStrings;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*     */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*     */ 
/*     */ public class StorePotion
/*     */ {
/*  21 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("StorePotion");
/*  22 */   public static final String[] TEXT = uiStrings.TEXT;
/*     */   
/*     */   public AbstractPotion potion;
/*     */   private ShopScreen shopScreen;
/*     */   public int price;
/*     */   private int slot;
/*  28 */   public boolean isPurchased = false;
/*  29 */   private static final float RELIC_GOLD_OFFSET_X = -56.0F * Settings.scale;
/*  30 */   private static final float RELIC_GOLD_OFFSET_Y = -100.0F * Settings.scale;
/*  31 */   private static final float RELIC_PRICE_OFFSET_X = 14.0F * Settings.scale;
/*  32 */   private static final float RELIC_PRICE_OFFSET_Y = -62.0F * Settings.scale;
/*  33 */   private static final float GOLD_IMG_WIDTH = ImageMaster.UI_GOLD.getWidth() * Settings.scale;
/*     */   
/*     */   public StorePotion(AbstractPotion potion, int slot, ShopScreen screenRef) {
/*  36 */     this.potion = potion;
/*  37 */     this.price = potion.getPrice();
/*  38 */     this.slot = slot;
/*  39 */     this.shopScreen = screenRef;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update(float rugY)
/*     */   {
/*  48 */     if (this.potion != null)
/*     */     {
/*  50 */       this.potion.posX = (1000.0F * Settings.scale + 150.0F * this.slot * Settings.scale);
/*  51 */       this.potion.posY = (rugY + 200.0F * Settings.scale);
/*  52 */       this.potion.hb.move(this.potion.posX, this.potion.posY);
/*     */       
/*     */ 
/*  55 */       this.potion.hb.update();
/*  56 */       if (this.potion.hb.hovered) {
/*  57 */         this.shopScreen.moveHand(this.potion.posX - 190.0F * Settings.scale, this.potion.posY - 70.0F * Settings.scale);
/*  58 */         if (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft) {
/*  59 */           this.potion.hb.clickStarted = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  64 */       if ((this.potion.hb.clicked) || ((this.potion.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/*  65 */         this.potion.hb.clicked = false;
/*     */         
/*  67 */         if (AbstractDungeon.player.hasRelic("Sozu")) {
/*  68 */           AbstractDungeon.player.getRelic("Sozu").flash();
/*  69 */           return;
/*     */         }
/*     */         
/*  72 */         if (AbstractDungeon.player.gold >= this.price) {
/*  73 */           if (AbstractDungeon.player.obtainPotion(this.potion)) {
/*  74 */             AbstractDungeon.player.loseGold(this.price);
/*  75 */             CardCrawlGame.sound.play("SHOP_PURCHASE", 0.1F);
/*  76 */             CardCrawlGame.metricData.addShopPurchaseData(this.potion.ID);
/*  77 */             this.shopScreen.playBuySfx();
/*  78 */             this.shopScreen.createSpeech(ShopScreen.getBuyMsg());
/*     */             
/*     */ 
/*  81 */             if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  82 */               this.potion = AbstractDungeon.returnRandomPotion();
/*  83 */               this.price = this.potion.getPrice();
/*  84 */               this.shopScreen.getNewPrice(this);
/*     */             } else {
/*  86 */               this.isPurchased = true;
/*     */             }
/*  88 */             return;
/*     */           }
/*  90 */           this.shopScreen.createSpeech(TEXT[0]);
/*  91 */           AbstractDungeon.topPanel.flashRed();
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*  96 */           this.shopScreen.playCantBuySfx();
/*  97 */           this.shopScreen.createSpeech(ShopScreen.getCantBuyMsg());
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/* 104 */     if (this.potion != null) {
/* 105 */       this.potion.posY = (Settings.HEIGHT + 200.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 115 */     if (this.potion != null) {
/* 116 */       this.potion.shopRender(sb);
/*     */       
/*     */ 
/* 119 */       sb.setColor(Color.WHITE);
/* 120 */       sb.draw(ImageMaster.UI_GOLD, this.potion.posX + RELIC_GOLD_OFFSET_X, this.potion.posY + RELIC_GOLD_OFFSET_Y, GOLD_IMG_WIDTH, GOLD_IMG_WIDTH);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 127 */       Color color = Color.WHITE;
/* 128 */       if (this.price > AbstractDungeon.player.gold) {
/* 129 */         color = Color.SALMON;
/*     */       }
/* 131 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*     */       
/*     */ 
/* 134 */         Integer.toString(this.price), this.potion.posX + RELIC_PRICE_OFFSET_X, this.potion.posY + RELIC_PRICE_OFFSET_Y, color);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\shop\StorePotion.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */