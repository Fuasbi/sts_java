/*    */ package com.megacrit.cardcrawl.shop;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*    */ 
/*    */ public class OnSaleTag
/*    */ {
/*    */   public AbstractCard card;
/* 14 */   private static final float W = 128.0F * Settings.scale;
/* 15 */   public static Texture img = null;
/*    */   
/*    */   public OnSaleTag(AbstractCard c) {
/* 18 */     this.card = c;
/* 19 */     if (img == null) {
/* 20 */       switch (Settings.language) {
/*    */       case DEU: 
/* 22 */         img = ImageMaster.loadImage("images/npcs/sale_tag/deu.png");
/* 23 */         break;
/*    */       case FRA: 
/* 25 */         img = ImageMaster.loadImage("images/npcs/sale_tag/fra.png");
/* 26 */         break;
/*    */       case ITA: 
/* 28 */         img = ImageMaster.loadImage("images/npcs/sale_tag/ita.png");
/* 29 */         break;
/*    */       case KOR: 
/* 31 */         img = ImageMaster.loadImage("images/npcs/sale_tag/kor.png");
/* 32 */         break;
/*    */       case RUS: 
/* 34 */         img = ImageMaster.loadImage("images/npcs/sale_tag/rus.png");
/* 35 */         break;
/*    */       case UKR: 
/* 37 */         img = ImageMaster.loadImage("images/npcs/sale_tag/ukr.png");
/* 38 */         break;
/*    */       case ZHS: 
/* 40 */         img = ImageMaster.loadImage("images/npcs/sale_tag/zhs.png");
/* 41 */         break;
/*    */       default: 
/* 43 */         img = ImageMaster.loadImage("images/npcs/sale_tag/eng.png");
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb)
/*    */   {
/* 50 */     sb.setColor(Color.WHITE);
/* 51 */     sb.draw(img, this.card.current_x + 30.0F * Settings.scale + (this.card.drawScale - 0.75F) * 60.0F * Settings.scale, this.card.current_y + 70.0F * Settings.scale + (this.card.drawScale - 0.75F) * 90.0F * Settings.scale, W * this.card.drawScale, W * this.card.drawScale);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 57 */     sb.setBlendFunction(770, 1);
/* 58 */     sb.setColor(new Color(1.0F, 1.0F, 1.0F, (MathUtils.cosDeg((float)(System.currentTimeMillis() / 5L % 360L)) + 1.25F) / 3.0F));
/* 59 */     sb.draw(img, this.card.current_x + 30.0F * Settings.scale + (this.card.drawScale - 0.75F) * 60.0F * Settings.scale, this.card.current_y + 70.0F * Settings.scale + (this.card.drawScale - 0.75F) * 90.0F * Settings.scale, W * this.card.drawScale, W * this.card.drawScale);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 65 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\shop\OnSaleTag.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */