/*      */ package com.megacrit.cardcrawl.shop;
/*      */ 
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.Input;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*      */ import com.megacrit.cardcrawl.metrics.MetricData;
/*      */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*      */ import com.megacrit.cardcrawl.random.Random;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic.RelicTier;
/*      */ import com.megacrit.cardcrawl.relics.OldCoin;
/*      */ import com.megacrit.cardcrawl.screens.select.GridCardSelectScreen;
/*      */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.FloatyEffect;
/*      */ import com.megacrit.cardcrawl.vfx.ShopSpeechBubble;
/*      */ import com.megacrit.cardcrawl.vfx.SpeechTextEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Iterator;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ public class ShopScreen
/*      */ {
/*   51 */   private static final Logger logger = LogManager.getLogger(ShopScreen.class.getName());
/*   52 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Shop Tip");
/*   53 */   public static final String[] MSG = tutorialStrings.TEXT;
/*   54 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*      */   
/*   56 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Shop Screen");
/*      */   
/*   58 */   public static final String[] NAMES = characterStrings.NAMES;
/*   59 */   public static final String[] TEXT = characterStrings.TEXT;
/*      */   
/*      */ 
/*   62 */   public boolean isActive = true;
/*      */   
/*      */ 
/*   65 */   private static Texture rugImg = null; private static Texture removeServiceImg = null; private static Texture soldOutImg = null; private static Texture handImg = null;
/*   66 */   private float rugY = Settings.HEIGHT;
/*      */   
/*      */   private static final float RUG_SPEED = 5.0F;
/*      */   
/*   70 */   private ArrayList<AbstractCard> coloredCards = new ArrayList();
/*   71 */   private ArrayList<AbstractCard> colorlessCards = new ArrayList();
/*   72 */   private static final float DRAW_START_X = Settings.WIDTH * 0.16F;
/*      */   private static final int NUM_CARDS_PER_LINE = 5;
/*      */   private static final float CARD_PRICE_JITTER = 0.1F;
/*   75 */   private static final float TOP_ROW_Y = 730.0F * Settings.scale;
/*   76 */   private static final float BOTTOM_ROW_Y = 307.0F * Settings.scale;
/*      */   
/*      */ 
/*   79 */   private float speechTimer = 0.0F;
/*      */   private static final float MIN_IDLE_MSG_TIME = 40.0F;
/*      */   private static final float MAX_IDLE_MSG_TIME = 60.0F;
/*      */   private static final float SPEECH_DURATION = 4.0F;
/*   83 */   private static final float SPEECH_TEXT_R_X = 164.0F * Settings.scale;
/*   84 */   private static final float SPEECH_TEXT_L_X = -166.0F * Settings.scale;
/*   85 */   private static final float SPEECH_TEXT_Y = 126.0F * Settings.scale;
/*   86 */   private ShopSpeechBubble speechBubble = null;
/*   87 */   private SpeechTextEffect dialogTextEffect = null;
/*   88 */   private static final String WELCOME_MSG = NAMES[0];
/*   89 */   private ArrayList<String> idleMessages = new ArrayList();
/*   90 */   private boolean saidWelcome = false; private boolean somethingHovered = false;
/*      */   
/*      */ 
/*   93 */   private ArrayList<StoreRelic> relics = new ArrayList();
/*      */   
/*      */   private static final float RELIC_PRICE_JITTER = 0.05F;
/*      */   
/*   97 */   private ArrayList<StorePotion> potions = new ArrayList();
/*      */   
/*      */   private static final float POTION_PRICE_JITTER = 0.05F;
/*      */   
/*  101 */   public boolean purgeAvailable = false;
/*  102 */   public static int purgeCost = 75; public static int actualPurgeCost = 75;
/*      */   private static final int PURGE_COST_RAMP = 25;
/*  104 */   private boolean purgeHovered = false;
/*  105 */   private float purgeCardX; private float purgeCardY; private float purgeCardScale = 1.0F;
/*      */   
/*      */ 
/*  108 */   private FloatyEffect f_effect = new FloatyEffect(20.0F, 0.1F);
/*  109 */   private float handTimer = 1.0F;
/*  110 */   private float handX = Settings.WIDTH / 2.0F;
/*  111 */   private float handY = Settings.HEIGHT;
/*  112 */   private float handTargetX = 0.0F;
/*  113 */   private float handTargetY = Settings.HEIGHT;
/*      */   private static final float HAND_SPEED = 6.0F;
/*      */   private static float HAND_W;
/*  116 */   private static float HAND_H; private float notHoveredTimer = 0.0F;
/*      */   
/*      */ 
/*  119 */   private static final float GOLD_IMG_WIDTH = ImageMaster.UI_GOLD.getWidth() * Settings.scale;
/*      */   private static final float COLORLESS_PRICE_BUMP = 1.3F;
/*      */   private OnSaleTag saleTag;
/*  122 */   private static final float GOLD_IMG_OFFSET_X = -50.0F * Settings.scale;
/*  123 */   private static final float GOLD_IMG_OFFSET_Y = -215.0F * Settings.scale;
/*  124 */   private static final float PRICE_TEXT_OFFSET_X = 16.0F * Settings.scale;
/*  125 */   private static final float PRICE_TEXT_OFFSET_Y = -180.0F * Settings.scale;
/*      */   
/*      */ 
/*      */   public void init(ArrayList<AbstractCard> coloredCards, ArrayList<AbstractCard> colorlessCards)
/*      */   {
/*  130 */     java.util.Collections.addAll(this.idleMessages, TEXT);
/*      */     
/*  132 */     if (rugImg == null) {
/*  133 */       switch (Settings.language) {
/*      */       case DEU: 
/*  135 */         rugImg = ImageMaster.loadImage("images/npcs/rug/deu.png");
/*  136 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/deu.png");
/*  137 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/deu.png");
/*  138 */         break;
/*      */       case FRA: 
/*  140 */         rugImg = ImageMaster.loadImage("images/npcs/rug/fra.png");
/*  141 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/fra.png");
/*  142 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/fra.png");
/*  143 */         break;
/*      */       case ITA: 
/*  145 */         rugImg = ImageMaster.loadImage("images/npcs/rug/ita.png");
/*  146 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/ita.png");
/*  147 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/ita.png");
/*  148 */         break;
/*      */       case KOR: 
/*  150 */         rugImg = ImageMaster.loadImage("images/npcs/rug/kor.png");
/*  151 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/kor.png");
/*  152 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/kor.png");
/*  153 */         break;
/*      */       case RUS: 
/*  155 */         rugImg = ImageMaster.loadImage("images/npcs/rug/rus.png");
/*  156 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/rus.png");
/*  157 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/rus.png");
/*  158 */         break;
/*      */       case UKR: 
/*  160 */         rugImg = ImageMaster.loadImage("images/npcs/rug/ukr.png");
/*  161 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/ukr.png");
/*  162 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/ukr.png");
/*  163 */         break;
/*      */       case ZHS: 
/*  165 */         rugImg = ImageMaster.loadImage("images/npcs/rug/zhs.png");
/*  166 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/zhs.png");
/*  167 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/zhs.png");
/*  168 */         break;
/*      */       default: 
/*  170 */         rugImg = ImageMaster.loadImage("images/npcs/rug/eng.png");
/*  171 */         removeServiceImg = ImageMaster.loadImage("images/npcs/purge/eng.png");
/*  172 */         soldOutImg = ImageMaster.loadImage("images/npcs/sold_out/eng.png");
/*      */       }
/*      */       
/*  175 */       handImg = ImageMaster.loadImage("images/npcs/merchantHand.png");
/*      */     }
/*      */     
/*  178 */     HAND_W = handImg.getWidth() * Settings.scale;
/*  179 */     HAND_H = handImg.getHeight() * Settings.scale;
/*      */     
/*      */ 
/*  182 */     this.coloredCards.clear();
/*  183 */     this.colorlessCards.clear();
/*  184 */     this.coloredCards = coloredCards;
/*  185 */     this.colorlessCards = colorlessCards;
/*  186 */     initCards();
/*      */     
/*      */ 
/*  189 */     initRelics();
/*      */     
/*      */ 
/*  192 */     initPotions();
/*      */     
/*      */ 
/*  195 */     this.purgeAvailable = true;
/*  196 */     this.purgeCardY = -1000.0F;
/*  197 */     this.purgeCardX = (1400.0F * Settings.scale);
/*  198 */     this.purgeCardScale = 0.7F;
/*  199 */     actualPurgeCost = purgeCost;
/*      */     
/*      */ 
/*  202 */     if (AbstractDungeon.ascensionLevel >= 16) {
/*  203 */       applyDiscount(1.15F, false);
/*      */     }
/*      */     
/*  206 */     if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  207 */       applyDiscount(0.8F, true);
/*      */     }
/*  209 */     if (AbstractDungeon.player.hasRelic("Membership Card")) {
/*  210 */       applyDiscount(0.5F, true);
/*      */     }
/*      */     
/*  213 */     if (AbstractDungeon.player.hasRelic("Smiling Mask")) {
/*  214 */       actualPurgeCost = 50;
/*      */     }
/*      */   }
/*      */   
/*      */   public static void resetPurgeCost() {
/*  219 */     purgeCost = 75;
/*  220 */     actualPurgeCost = 75;
/*      */   }
/*      */   
/*      */   private void initCards() {
/*  224 */     int tmp = (int)(Settings.WIDTH - DRAW_START_X * 2.0F - AbstractCard.IMG_WIDTH_S * 5.0F) / 4;
/*      */     
/*  226 */     float padX = (int)(tmp + AbstractCard.IMG_WIDTH_S);
/*      */     
/*      */ 
/*  229 */     for (int i = 0; i < this.coloredCards.size(); i++) {
/*  230 */       float tmpPrice = AbstractCard.getPrice(((AbstractCard)this.coloredCards.get(i)).rarity) * AbstractDungeon.merchantRng.random(0.9F, 1.1F);
/*      */       
/*      */ 
/*      */ 
/*  234 */       AbstractCard c = (AbstractCard)this.coloredCards.get(i);
/*  235 */       c.price = ((int)tmpPrice);
/*  236 */       c.current_x = (Settings.WIDTH / 2);
/*  237 */       c.target_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*      */       
/*  239 */       if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/*  240 */         c.upgrade();
/*  241 */       } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2"))) {
/*  242 */         c.upgrade();
/*  243 */       } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2"))) {
/*  244 */         c.upgrade();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  249 */     for (int i = 0; i < this.colorlessCards.size(); i++) {
/*  250 */       float tmpPrice = AbstractCard.getPrice(((AbstractCard)this.colorlessCards.get(i)).rarity) * AbstractDungeon.merchantRng.random(0.9F, 1.1F);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*  255 */       tmpPrice *= 1.3F;
/*      */       
/*  257 */       AbstractCard c = (AbstractCard)this.colorlessCards.get(i);
/*      */       
/*  259 */       c.price = ((int)tmpPrice);
/*  260 */       c.current_x = (Settings.WIDTH / 2);
/*  261 */       c.target_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*      */       
/*  263 */       if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/*  264 */         c.upgrade();
/*  265 */       } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2"))) {
/*  266 */         c.upgrade();
/*  267 */       } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2"))) {
/*  268 */         c.upgrade();
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  273 */     AbstractCard saleCard = (AbstractCard)this.coloredCards.get(AbstractDungeon.merchantRng.random(0, 4));
/*  274 */     saleCard.price /= 2;
/*  275 */     this.saleTag = new OnSaleTag(saleCard);
/*  276 */     setStartingCardPositions();
/*      */   }
/*      */   
/*      */   public static void purgeCard() {
/*  280 */     AbstractDungeon.player.loseGold(actualPurgeCost);
/*  281 */     CardCrawlGame.sound.play("SHOP_PURCHASE", 0.1F);
/*  282 */     purgeCost += 25;
/*  283 */     actualPurgeCost = purgeCost;
/*      */     
/*  285 */     if (AbstractDungeon.player.hasRelic("Smiling Mask")) {
/*  286 */       actualPurgeCost = 50;
/*      */     }
/*  288 */     else if ((AbstractDungeon.player.hasRelic("The Courier")) && (AbstractDungeon.player.hasRelic("Membership Card"))) {
/*  289 */       actualPurgeCost = MathUtils.round(purgeCost * 0.8F * 0.5F);
/*  290 */     } else if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  291 */       actualPurgeCost = MathUtils.round(purgeCost * 0.8F);
/*  292 */     } else if (AbstractDungeon.player.hasRelic("Membership Card")) {
/*  293 */       actualPurgeCost = MathUtils.round(purgeCost * 0.5F);
/*      */     }
/*      */   }
/*      */   
/*      */   public void updatePurge()
/*      */   {
/*  299 */     if (!AbstractDungeon.gridSelectScreen.selectedCards.isEmpty()) {
/*  300 */       purgeCard();
/*  301 */       for (AbstractCard card : AbstractDungeon.gridSelectScreen.selectedCards) {
/*  302 */         CardCrawlGame.metricData.addPurgedItem(card.getMetricID());
/*  303 */         AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect(card, Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*      */         
/*  305 */         AbstractDungeon.player.masterDeck.removeCard(card);
/*      */       }
/*  307 */       AbstractDungeon.gridSelectScreen.selectedCards.clear();
/*  308 */       AbstractDungeon.shopScreen.purgeAvailable = false;
/*      */     }
/*      */   }
/*      */   
/*      */   public static String getCantBuyMsg() {
/*  313 */     ArrayList<String> list = new ArrayList();
/*  314 */     list.add(NAMES[1]);
/*  315 */     list.add(NAMES[2]);
/*  316 */     list.add(NAMES[3]);
/*  317 */     list.add(NAMES[4]);
/*  318 */     list.add(NAMES[5]);
/*  319 */     list.add(NAMES[6]);
/*      */     
/*  321 */     return (String)list.get(MathUtils.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public static String getBuyMsg() {
/*  325 */     ArrayList<String> list = new ArrayList();
/*  326 */     list.add(NAMES[7]);
/*  327 */     list.add(NAMES[8]);
/*  328 */     list.add(NAMES[9]);
/*  329 */     list.add(NAMES[10]);
/*  330 */     list.add(NAMES[11]);
/*      */     
/*  332 */     return (String)list.get(MathUtils.random(list.size() - 1));
/*      */   }
/*      */   
/*      */   public void applyUpgrades(AbstractCard.CardType type) {
/*  336 */     for (AbstractCard c : this.coloredCards) {
/*  337 */       if (c.type == type) {
/*  338 */         c.upgrade();
/*      */       }
/*      */     }
/*  341 */     for (AbstractCard c : this.colorlessCards) {
/*  342 */       if (c.type == type) {
/*  343 */         c.upgrade();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyDiscount(float multiplier, boolean affectPurge) {
/*  349 */     for (StoreRelic r : this.relics) {
/*  350 */       r.price = MathUtils.round(r.price * multiplier);
/*      */     }
/*  352 */     for (StorePotion p : this.potions) {
/*  353 */       p.price = MathUtils.round(p.price * multiplier);
/*      */     }
/*  355 */     for (AbstractCard c : this.coloredCards) {
/*  356 */       c.price = MathUtils.round(c.price * multiplier);
/*      */     }
/*  358 */     for (AbstractCard c : this.colorlessCards) {
/*  359 */       c.price = MathUtils.round(c.price * multiplier);
/*      */     }
/*  361 */     if (AbstractDungeon.player.hasRelic("Smiling Mask")) {
/*  362 */       actualPurgeCost = 50;
/*      */     }
/*  364 */     else if (affectPurge) {
/*  365 */       actualPurgeCost = MathUtils.round(purgeCost * multiplier);
/*      */     }
/*      */   }
/*      */   
/*      */   private void initRelics()
/*      */   {
/*  371 */     this.relics.clear();
/*  372 */     this.relics = new ArrayList();
/*  373 */     for (int i = 0; i < 3; i++) {
/*  374 */       AbstractRelic tempRelic = null;
/*      */       
/*      */ 
/*  377 */       if (i != 2) {
/*  378 */         tempRelic = AbstractDungeon.returnRandomRelicEnd(rollRelicTier());
/*      */         
/*  380 */         while (((tempRelic instanceof OldCoin)) || ((tempRelic instanceof com.megacrit.cardcrawl.relics.SmilingMask)) || ((tempRelic instanceof com.megacrit.cardcrawl.relics.Courier)))
/*      */         {
/*  382 */           tempRelic = AbstractDungeon.returnRandomRelicEnd(rollRelicTier());
/*      */         }
/*      */       }
/*      */       
/*  386 */       tempRelic = AbstractDungeon.returnRandomRelicEnd(AbstractRelic.RelicTier.SHOP);
/*      */       
/*      */ 
/*  389 */       StoreRelic relic = new StoreRelic(tempRelic, i, this);
/*  390 */       if (!Settings.isDailyRun) {
/*  391 */         relic.price = MathUtils.round(relic.price * AbstractDungeon.merchantRng
/*  392 */           .random(0.95F, 1.05F));
/*      */       }
/*  394 */       this.relics.add(relic);
/*      */     }
/*      */   }
/*      */   
/*      */   private void initPotions() {
/*  399 */     this.potions.clear();
/*  400 */     this.potions = new ArrayList();
/*  401 */     for (int i = 0; i < 3; i++) {
/*  402 */       StorePotion potion = new StorePotion(AbstractDungeon.returnRandomPotion(), i, this);
/*  403 */       if (!Settings.isDailyRun) {
/*  404 */         potion.price = MathUtils.round(potion.price * AbstractDungeon.merchantRng
/*  405 */           .random(0.95F, 1.05F));
/*      */       }
/*      */       
/*      */ 
/*  409 */       this.potions.add(potion);
/*      */     }
/*      */   }
/*      */   
/*      */   public void getNewPrice(StoreRelic r)
/*      */   {
/*  415 */     int retVal = r.price;
/*      */     
/*      */ 
/*  418 */     if (!Settings.isDailyRun) {
/*  419 */       retVal = MathUtils.round(retVal * AbstractDungeon.merchantRng
/*  420 */         .random(0.95F, 1.05F));
/*      */     }
/*      */     
/*      */ 
/*  424 */     if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  425 */       applyDiscountToRelic(retVal, 0.8F);
/*      */     }
/*  427 */     if (AbstractDungeon.player.hasRelic("Membership Card")) {
/*  428 */       applyDiscountToRelic(retVal, 0.5F);
/*      */     }
/*      */     
/*      */ 
/*  432 */     r.price = retVal;
/*      */   }
/*      */   
/*      */   public void getNewPrice(StorePotion r)
/*      */   {
/*  437 */     int retVal = r.price;
/*      */     
/*      */ 
/*  440 */     if (!Settings.isDailyRun) {
/*  441 */       retVal = MathUtils.round(retVal * AbstractDungeon.merchantRng
/*  442 */         .random(0.95F, 1.05F));
/*      */     }
/*      */     
/*      */ 
/*  446 */     if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  447 */       applyDiscountToRelic(retVal, 0.8F);
/*      */     }
/*  449 */     if (AbstractDungeon.player.hasRelic("Membership Card")) {
/*  450 */       applyDiscountToRelic(retVal, 0.5F);
/*      */     }
/*      */     
/*      */ 
/*  454 */     r.price = retVal;
/*      */   }
/*      */   
/*      */   private int applyDiscountToRelic(int price, float multiplier) {
/*  458 */     return MathUtils.round(price * multiplier);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static AbstractRelic.RelicTier rollRelicTier()
/*      */   {
/*  467 */     int roll = AbstractDungeon.merchantRng.random(99);
/*  468 */     logger.info("ROLL " + roll);
/*      */     
/*      */ 
/*  471 */     if (roll < 48) {
/*  472 */       return AbstractRelic.RelicTier.COMMON;
/*      */     }
/*  474 */     if (roll < 82) {
/*  475 */       return AbstractRelic.RelicTier.UNCOMMON;
/*      */     }
/*      */     
/*  478 */     return AbstractRelic.RelicTier.RARE;
/*      */   }
/*      */   
/*      */   private void setStartingCardPositions()
/*      */   {
/*  483 */     int tmp = (int)(Settings.WIDTH - DRAW_START_X * 2.0F - AbstractCard.IMG_WIDTH_S * 5.0F) / 4;
/*      */     
/*  485 */     float padX = (int)(tmp + AbstractCard.IMG_WIDTH_S) + 10.0F * Settings.scale;
/*      */     
/*  487 */     for (int i = 0; i < this.coloredCards.size(); i++) {
/*  488 */       ((AbstractCard)this.coloredCards.get(i)).updateHoverLogic();
/*  489 */       ((AbstractCard)this.coloredCards.get(i)).targetDrawScale = 0.75F;
/*  490 */       ((AbstractCard)this.coloredCards.get(i)).current_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*  491 */       ((AbstractCard)this.coloredCards.get(i)).target_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*  492 */       ((AbstractCard)this.coloredCards.get(i)).target_y = (9999.0F * Settings.scale);
/*  493 */       ((AbstractCard)this.coloredCards.get(i)).current_y = (9999.0F * Settings.scale);
/*      */     }
/*      */     
/*  496 */     for (int i = 0; i < this.colorlessCards.size(); i++) {
/*  497 */       ((AbstractCard)this.colorlessCards.get(i)).updateHoverLogic();
/*  498 */       ((AbstractCard)this.colorlessCards.get(i)).targetDrawScale = 0.75F;
/*  499 */       ((AbstractCard)this.colorlessCards.get(i)).current_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*  500 */       ((AbstractCard)this.colorlessCards.get(i)).target_x = (DRAW_START_X + AbstractCard.IMG_WIDTH_S / 2.0F + padX * i);
/*  501 */       ((AbstractCard)this.colorlessCards.get(i)).target_y = (9999.0F * Settings.scale);
/*  502 */       ((AbstractCard)this.colorlessCards.get(i)).current_y = (9999.0F * Settings.scale);
/*      */     }
/*      */   }
/*      */   
/*      */   public void open() {
/*  507 */     CardCrawlGame.sound.play("SHOP_OPEN");
/*  508 */     setStartingCardPositions();
/*  509 */     this.purgeCardY = -1000.0F;
/*  510 */     AbstractDungeon.isScreenUp = true;
/*  511 */     AbstractDungeon.screen = AbstractDungeon.CurrentScreen.SHOP;
/*  512 */     AbstractDungeon.overlayMenu.proceedButton.hide();
/*  513 */     AbstractDungeon.overlayMenu.cancelButton.show(NAMES[12]);
/*  514 */     for (StoreRelic r : this.relics) {
/*  515 */       r.hide();
/*      */     }
/*  517 */     for (StorePotion p : this.potions) {
/*  518 */       p.hide();
/*      */     }
/*  520 */     this.rugY = Settings.HEIGHT;
/*  521 */     this.handX = (Settings.WIDTH / 2.0F);
/*  522 */     this.handY = Settings.HEIGHT;
/*  523 */     this.handTargetX = this.handX;
/*  524 */     this.handTargetY = this.handY;
/*  525 */     this.handTimer = 1.0F;
/*  526 */     this.speechTimer = 1.5F;
/*  527 */     this.speechBubble = null;
/*  528 */     this.dialogTextEffect = null;
/*  529 */     AbstractDungeon.overlayMenu.showBlackScreen();
/*      */     
/*      */ 
/*  532 */     for (AbstractCard c : this.coloredCards) {
/*  533 */       UnlockTracker.markCardAsSeen(c.cardID);
/*      */     }
/*  535 */     for (AbstractCard c : this.colorlessCards) {
/*  536 */       UnlockTracker.markCardAsSeen(c.cardID);
/*      */     }
/*  538 */     for (StoreRelic r : this.relics) {
/*  539 */       if (r.relic != null) {
/*  540 */         UnlockTracker.markRelicAsSeen(r.relic.relicId);
/*      */       }
/*      */     }
/*  543 */     if (((Boolean)com.megacrit.cardcrawl.daily.DailyMods.negativeMods.get("Hoarder")).booleanValue()) {
/*  544 */       this.purgeAvailable = false;
/*      */     }
/*      */   }
/*      */   
/*      */   public void update() {
/*  549 */     if (this.handTimer != 0.0F) {
/*  550 */       this.handTimer -= Gdx.graphics.getDeltaTime();
/*  551 */       if (this.handTimer < 0.0F) {
/*  552 */         this.handTimer = 0.0F;
/*      */       }
/*      */     }
/*  555 */     this.f_effect.update();
/*      */     
/*  557 */     this.somethingHovered = false;
/*  558 */     updateControllerInput();
/*  559 */     updatePurgeCard();
/*  560 */     updatePurge();
/*  561 */     updateRelics();
/*  562 */     updatePotions();
/*  563 */     updateRug();
/*  564 */     updateSpeech();
/*  565 */     updateCards();
/*      */     
/*      */ 
/*  568 */     updateHand();
/*  569 */     AbstractCard hoveredCard = null;
/*  570 */     for (AbstractCard c : this.coloredCards) {
/*  571 */       if (c.hb.hovered) {
/*  572 */         hoveredCard = c;
/*  573 */         this.somethingHovered = true;
/*  574 */         moveHand(c.current_x - AbstractCard.IMG_WIDTH / 2.0F, c.current_y);
/*  575 */         break;
/*      */       }
/*      */     }
/*      */     
/*  579 */     for (AbstractCard c : this.colorlessCards) {
/*  580 */       if (c.hb.hovered) {
/*  581 */         hoveredCard = c;
/*  582 */         this.somethingHovered = true;
/*  583 */         moveHand(c.current_x - AbstractCard.IMG_WIDTH / 2.0F, c.current_y);
/*  584 */         break;
/*      */       }
/*      */     }
/*      */     
/*  588 */     if (!this.somethingHovered) {
/*  589 */       this.notHoveredTimer += Gdx.graphics.getDeltaTime();
/*  590 */       if (this.notHoveredTimer > 1.0F) {
/*  591 */         this.handTargetY = Settings.HEIGHT;
/*      */       }
/*      */     } else {
/*  594 */       this.notHoveredTimer = 0.0F;
/*      */     }
/*      */     
/*      */ 
/*  598 */     if ((hoveredCard != null) && (InputHelper.justClickedLeft)) {
/*  599 */       hoveredCard.hb.clickStarted = true;
/*      */     }
/*      */     
/*  602 */     if ((hoveredCard != null) && ((InputHelper.justClickedRight) || (CInputActionSet.proceed.isJustPressed())))
/*      */     {
/*  604 */       CardCrawlGame.cardPopup.open(hoveredCard);
/*      */     }
/*      */     
/*  607 */     if ((hoveredCard != null) && ((hoveredCard.hb.clicked) || (CInputActionSet.select.isJustPressed()))) {
/*  608 */       hoveredCard.hb.clicked = false;
/*      */       
/*  610 */       if (AbstractDungeon.player.gold >= hoveredCard.price) {
/*  611 */         CardCrawlGame.metricData.addShopPurchaseData(hoveredCard.getMetricID());
/*  612 */         this.coloredCards.remove(hoveredCard);
/*  613 */         this.colorlessCards.remove(hoveredCard);
/*  614 */         AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.FastCardObtainEffect(hoveredCard, hoveredCard.current_x, hoveredCard.current_y));
/*      */         
/*  616 */         AbstractDungeon.player.loseGold(hoveredCard.price);
/*  617 */         CardCrawlGame.sound.play("SHOP_PURCHASE", 0.1F);
/*      */         
/*  619 */         if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  620 */           if (hoveredCard.color == AbstractCard.CardColor.COLORLESS) {
/*  621 */             AbstractCard.CardRarity tempRarity = AbstractCard.CardRarity.UNCOMMON;
/*  622 */             if (AbstractDungeon.merchantRng.random() < AbstractDungeon.colorlessRareChance) {
/*  623 */               tempRarity = AbstractCard.CardRarity.RARE;
/*      */             }
/*  625 */             AbstractCard c = AbstractDungeon.getColorlessCardFromPool(tempRarity).makeCopy();
/*  626 */             if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/*  627 */               c.upgrade();
/*  628 */             } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2")))
/*      */             {
/*  630 */               c.upgrade();
/*  631 */             } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2")))
/*      */             {
/*  633 */               c.upgrade();
/*      */             }
/*  635 */             c.current_x = hoveredCard.current_x;
/*  636 */             c.current_y = hoveredCard.current_y;
/*  637 */             c.target_x = c.current_x;
/*  638 */             c.target_y = c.current_y;
/*  639 */             setPrice(c);
/*      */             
/*  641 */             this.colorlessCards.add(c);
/*      */           }
/*      */           else
/*      */           {
/*  645 */             AbstractCard c = AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), hoveredCard.type, false).makeCopy();
/*  646 */             if ((c.type == AbstractCard.CardType.ATTACK) && (AbstractDungeon.player.hasRelic("Molten Egg 2"))) {
/*  647 */               c.upgrade();
/*  648 */             } else if ((c.type == AbstractCard.CardType.SKILL) && (AbstractDungeon.player.hasRelic("Toxic Egg 2")))
/*      */             {
/*  650 */               c.upgrade();
/*  651 */             } else if ((c.type == AbstractCard.CardType.POWER) && (AbstractDungeon.player.hasRelic("Frozen Egg 2")))
/*      */             {
/*  653 */               c.upgrade();
/*      */             }
/*  655 */             c.current_x = hoveredCard.current_x;
/*  656 */             c.current_y = hoveredCard.current_y;
/*  657 */             c.target_x = c.current_x;
/*  658 */             c.target_y = c.current_y;
/*  659 */             setPrice(c);
/*      */             
/*  661 */             this.coloredCards.add(c);
/*      */           }
/*      */         }
/*      */         
/*  665 */         hoveredCard = null;
/*  666 */         InputHelper.justClickedLeft = false;
/*  667 */         this.notHoveredTimer = 1.0F;
/*  668 */         this.speechTimer = MathUtils.random(40.0F, 60.0F);
/*  669 */         playBuySfx();
/*  670 */         createSpeech(getBuyMsg());
/*      */       } else {
/*  672 */         this.speechTimer = MathUtils.random(40.0F, 60.0F);
/*  673 */         playCantBuySfx();
/*  674 */         createSpeech(getCantBuyMsg());
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateCards()
/*      */   {
/*  681 */     for (int i = 0; i < this.coloredCards.size(); i++) {
/*  682 */       ((AbstractCard)this.coloredCards.get(i)).update();
/*  683 */       ((AbstractCard)this.coloredCards.get(i)).updateHoverLogic();
/*  684 */       ((AbstractCard)this.coloredCards.get(i)).current_y = (this.rugY + TOP_ROW_Y);
/*  685 */       ((AbstractCard)this.coloredCards.get(i)).target_y = ((AbstractCard)this.coloredCards.get(i)).current_y;
/*      */     }
/*      */     
/*      */ 
/*  689 */     for (int i = 0; i < this.colorlessCards.size(); i++) {
/*  690 */       ((AbstractCard)this.colorlessCards.get(i)).update();
/*  691 */       ((AbstractCard)this.colorlessCards.get(i)).updateHoverLogic();
/*  692 */       ((AbstractCard)this.colorlessCards.get(i)).current_y = (this.rugY + BOTTOM_ROW_Y);
/*  693 */       ((AbstractCard)this.colorlessCards.get(i)).target_y = ((AbstractCard)this.colorlessCards.get(i)).current_y;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void setPrice(AbstractCard card)
/*      */   {
/*  703 */     float tmpPrice = AbstractCard.getPrice(card.rarity) * AbstractDungeon.merchantRng.random(0.9F, 1.1F);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  708 */     if (card.color == AbstractCard.CardColor.COLORLESS) {
/*  709 */       tmpPrice *= 1.3F;
/*      */     }
/*      */     
/*      */ 
/*  713 */     if (AbstractDungeon.player.hasRelic("The Courier")) {
/*  714 */       tmpPrice *= 0.8F;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  719 */     if (AbstractDungeon.player.hasRelic("Membership Card")) {
/*  720 */       tmpPrice *= 0.5F;
/*      */     }
/*      */     
/*      */ 
/*  724 */     card.price = ((int)tmpPrice);
/*      */   }
/*      */   
/*      */   public void moveHand(float x, float y) {
/*  728 */     this.handTargetX = (x - 50.0F * Settings.scale);
/*  729 */     this.handTargetY = (y + 90.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   private static enum StoreSelectionType {
/*  733 */     RELIC,  COLOR_CARD,  COLORLESS_CARD,  POTION,  PURGE;
/*      */     
/*      */     private StoreSelectionType() {} }
/*      */   
/*  737 */   private void updateControllerInput() { if ((!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode) || (!AbstractDungeon.topPanel.potionUi.isHidden))
/*      */     {
/*  739 */       return;
/*      */     }
/*      */     
/*  742 */     StoreSelectionType type = null;
/*  743 */     int index = 0;
/*      */     
/*      */ 
/*  746 */     for (AbstractCard c : this.coloredCards) {
/*  747 */       if (c.hb.hovered) {
/*  748 */         type = StoreSelectionType.COLOR_CARD;
/*  749 */         break;
/*      */       }
/*  751 */       index++;
/*      */     }
/*      */     
/*      */ 
/*  755 */     if (type == null) {
/*  756 */       index = 0;
/*  757 */       for (StoreRelic r : this.relics) {
/*  758 */         if (r.relic.hb.hovered) {
/*  759 */           type = StoreSelectionType.RELIC;
/*  760 */           break;
/*      */         }
/*  762 */         index++;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  767 */     if (type == null) {
/*  768 */       index = 0;
/*  769 */       for (AbstractCard c : this.colorlessCards) {
/*  770 */         if (c.hb.hovered) {
/*  771 */           type = StoreSelectionType.COLORLESS_CARD;
/*  772 */           break;
/*      */         }
/*  774 */         index++;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  779 */     if (type == null) {
/*  780 */       index = 0;
/*  781 */       for (StorePotion p : this.potions) {
/*  782 */         if (p.potion.hb.hovered) {
/*  783 */           type = StoreSelectionType.POTION;
/*  784 */           break;
/*      */         }
/*  786 */         index++;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  791 */     if ((type == null) && 
/*  792 */       (this.purgeHovered)) {
/*  793 */       type = StoreSelectionType.PURGE;
/*      */     }
/*      */     
/*      */ 
/*  797 */     if (type == null) {
/*  798 */       if (!this.coloredCards.isEmpty()) {
/*  799 */         Gdx.input.setCursorPosition(
/*  800 */           (int)((AbstractCard)this.coloredCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  801 */           (int)((AbstractCard)this.coloredCards.get(0)).hb.cY);
/*  802 */       } else if (!this.colorlessCards.isEmpty()) {
/*  803 */         Gdx.input.setCursorPosition(
/*  804 */           (int)((AbstractCard)this.colorlessCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  805 */           (int)((AbstractCard)this.colorlessCards.get(0)).hb.cY);
/*  806 */       } else if (!this.relics.isEmpty()) {
/*  807 */         Gdx.input.setCursorPosition(
/*  808 */           (int)((StoreRelic)this.relics.get(0)).relic.hb.cX, Settings.HEIGHT - 
/*  809 */           (int)((StoreRelic)this.relics.get(0)).relic.hb.cY);
/*  810 */       } else if (!this.potions.isEmpty()) {
/*  811 */         Gdx.input.setCursorPosition(
/*  812 */           (int)((StorePotion)this.potions.get(0)).potion.hb.cX, Settings.HEIGHT - 
/*  813 */           (int)((StorePotion)this.potions.get(0)).potion.hb.cY);
/*  814 */       } else if (this.purgeAvailable) {
/*  815 */         Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*      */       }
/*      */     } else {
/*  818 */       switch (type) {
/*      */       case COLOR_CARD: 
/*  820 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  821 */           index--;
/*  822 */           if (index < 0) {
/*  823 */             index = 0;
/*      */           }
/*  825 */           Gdx.input.setCursorPosition(
/*  826 */             (int)((AbstractCard)this.coloredCards.get(index)).hb.cX, Settings.HEIGHT - 
/*  827 */             (int)((AbstractCard)this.coloredCards.get(index)).hb.cY);
/*  828 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  829 */           index++;
/*  830 */           if (index > this.coloredCards.size() - 1) {
/*  831 */             index--;
/*      */           }
/*  833 */           Gdx.input.setCursorPosition(
/*  834 */             (int)((AbstractCard)this.coloredCards.get(index)).hb.cX, Settings.HEIGHT - 
/*  835 */             (int)((AbstractCard)this.coloredCards.get(index)).hb.cY);
/*      */ 
/*      */         }
/*  838 */         else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  839 */           if ((((AbstractCard)this.coloredCards.get(index)).hb.cX < 550.0F * Settings.scale) && 
/*  840 */             (!this.colorlessCards.isEmpty())) {
/*  841 */             Gdx.input.setCursorPosition(
/*  842 */               (int)((AbstractCard)this.colorlessCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  843 */               (int)((AbstractCard)this.colorlessCards.get(0)).hb.cY);
/*  844 */             return;
/*      */           }
/*      */           
/*      */ 
/*  848 */           if (((AbstractCard)this.coloredCards.get(index)).hb.cX < 850.0F * Settings.scale) {
/*  849 */             if (!this.colorlessCards.isEmpty()) {
/*  850 */               if (this.colorlessCards.size() > 1) {
/*  851 */                 Gdx.input.setCursorPosition(
/*  852 */                   (int)((AbstractCard)this.colorlessCards.get(1)).hb.cX, Settings.HEIGHT - 
/*  853 */                   (int)((AbstractCard)this.colorlessCards.get(1)).hb.cY);
/*      */               } else {
/*  855 */                 Gdx.input.setCursorPosition(
/*  856 */                   (int)((AbstractCard)this.colorlessCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  857 */                   (int)((AbstractCard)this.colorlessCards.get(0)).hb.cY);
/*      */               }
/*  859 */               return; }
/*  860 */             if (!this.relics.isEmpty()) {
/*  861 */               Gdx.input.setCursorPosition(
/*  862 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cX, Settings.HEIGHT - 
/*  863 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cY);
/*  864 */               return; }
/*  865 */             if (!this.potions.isEmpty()) {
/*  866 */               Gdx.input.setCursorPosition(
/*  867 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cX, Settings.HEIGHT - 
/*  868 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cY);
/*  869 */             } else if (this.purgeAvailable) {
/*  870 */               Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*  871 */               return;
/*      */             }
/*      */           }
/*      */           
/*  875 */           if (((AbstractCard)this.coloredCards.get(index)).hb.cX < 1400.0F * Settings.scale) {
/*  876 */             if (!this.relics.isEmpty()) {
/*  877 */               Gdx.input.setCursorPosition(
/*  878 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cX, Settings.HEIGHT - 
/*  879 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cY);
/*  880 */               return; }
/*  881 */             if (!this.potions.isEmpty()) {
/*  882 */               Gdx.input.setCursorPosition(
/*  883 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cX, Settings.HEIGHT - 
/*  884 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cY);
/*      */             }
/*      */           }
/*      */           
/*  888 */           Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*      */         }
/*      */         break;
/*      */       case COLORLESS_CARD: 
/*  892 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  893 */           index--;
/*  894 */           if (index < 0) {
/*  895 */             index = 0;
/*      */           }
/*  897 */           Gdx.input.setCursorPosition(
/*  898 */             (int)((AbstractCard)this.colorlessCards.get(index)).hb.cX, Settings.HEIGHT - 
/*  899 */             (int)((AbstractCard)this.colorlessCards.get(index)).hb.cY);
/*  900 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  901 */           index++;
/*  902 */           if (index > this.colorlessCards.size() - 1) {
/*  903 */             if (!this.relics.isEmpty()) {
/*  904 */               Gdx.input.setCursorPosition(
/*  905 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cX, Settings.HEIGHT - 
/*  906 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cY);
/*  907 */             } else if (!this.potions.isEmpty()) {
/*  908 */               Gdx.input.setCursorPosition(
/*  909 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cX, Settings.HEIGHT - 
/*  910 */                 (int)((StorePotion)this.potions.get(0)).potion.hb.cY);
/*  911 */             } else if (this.purgeAvailable) {
/*  912 */               Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*      */             } else {
/*  914 */               index = 0;
/*      */             }
/*      */           } else {
/*  917 */             Gdx.input.setCursorPosition(
/*  918 */               (int)((AbstractCard)this.colorlessCards.get(index)).hb.cX, Settings.HEIGHT - 
/*  919 */               (int)((AbstractCard)this.colorlessCards.get(index)).hb.cY);
/*      */           }
/*      */           
/*      */         }
/*  923 */         else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/*  924 */           (!this.coloredCards.isEmpty())) {
/*  925 */           if (((AbstractCard)this.colorlessCards.get(index)).hb.cX < 550.0F * Settings.scale) {
/*  926 */             Gdx.input.setCursorPosition(
/*  927 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  928 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cY);
/*      */           }
/*  930 */           else if (this.coloredCards.size() > 1) {
/*  931 */             Gdx.input.setCursorPosition(
/*  932 */               (int)((AbstractCard)this.coloredCards.get(1)).hb.cX, Settings.HEIGHT - 
/*  933 */               (int)((AbstractCard)this.coloredCards.get(1)).hb.cY);
/*      */           } else {
/*  935 */             Gdx.input.setCursorPosition(
/*  936 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  937 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cY);
/*      */           }
/*      */         }
/*      */         
/*      */         break;
/*      */       case RELIC: 
/*  943 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  944 */           index--;
/*  945 */           if ((index < 0) && (!this.colorlessCards.isEmpty())) {
/*  946 */             Gdx.input.setCursorPosition(
/*  947 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cX, Settings.HEIGHT - 
/*  948 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cY);
/*      */           } else {
/*  950 */             Gdx.input.setCursorPosition(
/*  951 */               (int)((StoreRelic)this.relics.get(index)).relic.hb.cX, Settings.HEIGHT - 
/*  952 */               (int)((StoreRelic)this.relics.get(index)).relic.hb.cY);
/*      */           }
/*  954 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  955 */           index++;
/*  956 */           if ((index > this.relics.size() - 1) && (this.purgeAvailable)) {
/*  957 */             Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*      */           }
/*  959 */           else if (index <= this.relics.size() - 1) {
/*  960 */             Gdx.input.setCursorPosition(
/*  961 */               (int)((StoreRelic)this.relics.get(index)).relic.hb.cX, Settings.HEIGHT - 
/*  962 */               (int)((StoreRelic)this.relics.get(index)).relic.hb.cY);
/*      */           }
/*      */         }
/*  965 */         else if (((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) && 
/*  966 */           (!this.potions.isEmpty())) {
/*  967 */           System.out.println("SIZE: " + (this.potions.size() - 1) + " INDEX: " + index);
/*  968 */           if (this.potions.size() - 1 >= index) {
/*  969 */             Gdx.input.setCursorPosition(
/*  970 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cX, Settings.HEIGHT - 
/*  971 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cY);
/*      */           } else {
/*  973 */             Gdx.input.setCursorPosition(
/*  974 */               (int)((StorePotion)this.potions.get(0)).potion.hb.cX, Settings.HEIGHT - 
/*  975 */               (int)((StorePotion)this.potions.get(0)).potion.hb.cY);
/*      */           }
/*  977 */         } else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/*  978 */           (!this.coloredCards.isEmpty())) {
/*  979 */           if (this.coloredCards.size() > 3) {
/*  980 */             Gdx.input.setCursorPosition(
/*  981 */               (int)((AbstractCard)this.coloredCards.get(2)).hb.cX, Settings.HEIGHT - 
/*  982 */               (int)((AbstractCard)this.coloredCards.get(2)).hb.cY);
/*      */           } else {
/*  984 */             Gdx.input.setCursorPosition(
/*  985 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cX, Settings.HEIGHT - 
/*  986 */               (int)((AbstractCard)this.coloredCards.get(0)).hb.cY);
/*      */           }
/*      */         }
/*      */         break;
/*      */       case POTION: 
/*  991 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  992 */           index--;
/*  993 */           if ((index < 0) && (!this.colorlessCards.isEmpty())) {
/*  994 */             Gdx.input.setCursorPosition(
/*  995 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cX, Settings.HEIGHT - 
/*  996 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cY);
/*      */           } else {
/*  998 */             Gdx.input.setCursorPosition(
/*  999 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cX, Settings.HEIGHT - 
/* 1000 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cY);
/*      */           }
/* 1002 */         } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/* 1003 */           index++;
/* 1004 */           if ((index > this.potions.size() - 1) && (this.purgeAvailable)) {
/* 1005 */             Gdx.input.setCursorPosition((int)this.purgeCardX, Settings.HEIGHT - (int)this.purgeCardY);
/*      */           }
/* 1007 */           else if (index <= this.potions.size() - 1) {
/* 1008 */             Gdx.input.setCursorPosition(
/* 1009 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cX, Settings.HEIGHT - 
/* 1010 */               (int)((StorePotion)this.potions.get(index)).potion.hb.cY);
/*      */           }
/*      */         }
/* 1013 */         else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/* 1014 */           if (!this.relics.isEmpty()) {
/* 1015 */             if (this.relics.size() - 1 >= index) {
/* 1016 */               Gdx.input.setCursorPosition(
/* 1017 */                 (int)((StoreRelic)this.relics.get(index)).relic.hb.cX, Settings.HEIGHT - 
/* 1018 */                 (int)((StoreRelic)this.relics.get(index)).relic.hb.cY);
/*      */             } else {
/* 1020 */               Gdx.input.setCursorPosition(
/* 1021 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cX, Settings.HEIGHT - 
/* 1022 */                 (int)((StoreRelic)this.relics.get(0)).relic.hb.cY);
/*      */             }
/* 1024 */           } else if (!this.coloredCards.isEmpty()) {
/* 1025 */             if (this.coloredCards.size() > 3) {
/* 1026 */               Gdx.input.setCursorPosition(
/* 1027 */                 (int)((AbstractCard)this.coloredCards.get(2)).hb.cX, Settings.HEIGHT - 
/* 1028 */                 (int)((AbstractCard)this.coloredCards.get(2)).hb.cY);
/*      */             } else {
/* 1030 */               Gdx.input.setCursorPosition(
/* 1031 */                 (int)((AbstractCard)this.coloredCards.get(0)).hb.cX, Settings.HEIGHT - 
/* 1032 */                 (int)((AbstractCard)this.coloredCards.get(0)).hb.cY);
/*      */             }
/*      */           }
/*      */         }
/*      */         break;
/*      */       case PURGE: 
/* 1038 */         if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/* 1039 */           if (!this.relics.isEmpty()) {
/* 1040 */             Gdx.input.setCursorPosition(
/* 1041 */               (int)((StoreRelic)this.relics.get(this.relics.size() - 1)).relic.hb.cX, Settings.HEIGHT - 
/* 1042 */               (int)((StoreRelic)this.relics.get(this.relics.size() - 1)).relic.hb.cY);
/* 1043 */           } else if (!this.potions.isEmpty()) {
/* 1044 */             Gdx.input.setCursorPosition(
/* 1045 */               (int)((StorePotion)this.potions.get(this.potions.size() - 1)).potion.hb.cX, Settings.HEIGHT - 
/* 1046 */               (int)((StorePotion)this.potions.get(this.potions.size() - 1)).potion.hb.cY);
/* 1047 */           } else if (this.colorlessCards.isEmpty()) {
/* 1048 */             Gdx.input.setCursorPosition(
/* 1049 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cX, Settings.HEIGHT - 
/* 1050 */               (int)((AbstractCard)this.colorlessCards.get(this.colorlessCards.size() - 1)).hb.cY);
/*      */           }
/* 1052 */         } else if (((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/* 1053 */           (!this.coloredCards.isEmpty())) {
/* 1054 */           Gdx.input.setCursorPosition(
/* 1055 */             (int)((AbstractCard)this.coloredCards.get(this.coloredCards.size() - 1)).hb.cX, Settings.HEIGHT - 
/* 1056 */             (int)((AbstractCard)this.coloredCards.get(this.coloredCards.size() - 1)).hb.cY);
/*      */         }
/*      */         
/*      */         break;
/*      */       }
/*      */       
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private void updatePurgeCard()
/*      */   {
/* 1068 */     this.purgeCardX = (1554.0F * Settings.scale);
/* 1069 */     this.purgeCardY = (this.rugY + BOTTOM_ROW_Y);
/*      */     
/* 1071 */     if (this.purgeAvailable) {
/* 1072 */       float CARD_W = 110.0F * Settings.scale;
/* 1073 */       float CARD_H = 150.0F * Settings.scale;
/*      */       
/* 1075 */       if ((InputHelper.mX > this.purgeCardX - CARD_W) && (InputHelper.mX < this.purgeCardX + CARD_W) && (InputHelper.mY > this.purgeCardY - CARD_H) && (InputHelper.mY < this.purgeCardY + CARD_H))
/*      */       {
/* 1077 */         this.purgeHovered = true;
/* 1078 */         moveHand(this.purgeCardX - AbstractCard.IMG_WIDTH / 2.0F, this.purgeCardY);
/* 1079 */         this.somethingHovered = true;
/* 1080 */         this.purgeCardScale = Settings.scale;
/*      */       } else {
/* 1082 */         this.purgeHovered = false;
/*      */       }
/*      */       
/* 1085 */       if (!this.purgeHovered) {
/* 1086 */         this.purgeCardScale = MathHelper.cardScaleLerpSnap(this.purgeCardScale, 0.75F * Settings.scale);
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/* 1091 */         if ((InputHelper.justClickedLeft) || (CInputActionSet.select.isJustPressed())) {
/* 1092 */           CInputActionSet.select.unpress();
/* 1093 */           this.purgeHovered = false;
/* 1094 */           if (AbstractDungeon.player.gold >= actualPurgeCost) {
/* 1095 */             AbstractDungeon.previousScreen = AbstractDungeon.CurrentScreen.SHOP;
/*      */             
/* 1097 */             AbstractDungeon.gridSelectScreen.open(
/* 1098 */               CardGroup.getGroupWithoutBottledCards(AbstractDungeon.player.masterDeck
/* 1099 */               .getPurgeableCards()), 1, NAMES[13], false, false, true, true);
/*      */ 
/*      */ 
/*      */           }
/*      */           else
/*      */           {
/*      */ 
/*      */ 
/* 1107 */             playCantBuySfx();
/* 1108 */             createSpeech(getCantBuyMsg());
/*      */           }
/*      */         }
/* 1111 */         com.megacrit.cardcrawl.helpers.TipHelper.renderGenericTip(InputHelper.mX - 360.0F * Settings.scale, InputHelper.mY - 70.0F * Settings.scale, LABEL[0], MSG[0] + 25 + MSG[1]);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 1118 */       this.purgeCardScale = MathHelper.cardScaleLerpSnap(this.purgeCardScale, 0.75F * Settings.scale);
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateRelics() {
/* 1123 */     for (Iterator<StoreRelic> i = this.relics.iterator(); i.hasNext();) {
/* 1124 */       StoreRelic r = (StoreRelic)i.next();
/* 1125 */       r.update(this.rugY);
/* 1126 */       if (r.isPurchased) {
/* 1127 */         i.remove();
/* 1128 */         break;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updatePotions() {
/* 1134 */     for (Iterator<StorePotion> i = this.potions.iterator(); i.hasNext();) {
/* 1135 */       StorePotion p = (StorePotion)i.next();
/* 1136 */       p.update(this.rugY);
/* 1137 */       if (p.isPurchased) {
/* 1138 */         i.remove();
/* 1139 */         break;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void createSpeech(String msg) {
/* 1145 */     boolean isRight = MathUtils.randomBoolean();
/* 1146 */     float x = MathUtils.random(660.0F, 1260.0F) * Settings.scale;
/* 1147 */     float y = Settings.HEIGHT - 380.0F * Settings.scale;
/* 1148 */     this.speechBubble = new ShopSpeechBubble(x, y, 4.0F, msg, isRight);
/* 1149 */     float offset_x = 0.0F;
/* 1150 */     if (isRight) {
/* 1151 */       offset_x = SPEECH_TEXT_R_X;
/*      */     } else {
/* 1153 */       offset_x = SPEECH_TEXT_L_X;
/*      */     }
/* 1155 */     this.dialogTextEffect = new SpeechTextEffect(x + offset_x, y + SPEECH_TEXT_Y, 4.0F, msg, com.megacrit.cardcrawl.ui.DialogWord.AppearEffect.BUMP_IN);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void updateSpeech()
/*      */   {
/* 1165 */     if (this.speechBubble != null) {
/* 1166 */       this.speechBubble.update();
/* 1167 */       if ((this.speechBubble.hb.hovered) && (this.speechBubble.duration > 0.3F)) {
/* 1168 */         this.speechBubble.duration = 0.3F;
/* 1169 */         this.dialogTextEffect.duration = 0.3F;
/*      */       }
/* 1171 */       if (this.speechBubble.isDone) {
/* 1172 */         this.speechBubble = null;
/*      */       }
/*      */     }
/* 1175 */     if (this.dialogTextEffect != null) {
/* 1176 */       this.dialogTextEffect.update();
/* 1177 */       if (this.dialogTextEffect.isDone) {
/* 1178 */         this.dialogTextEffect = null;
/*      */       }
/*      */     }
/*      */     
/* 1182 */     this.speechTimer -= Gdx.graphics.getDeltaTime();
/* 1183 */     if ((this.speechBubble == null) && (this.dialogTextEffect == null) && (this.speechTimer <= 0.0F)) {
/* 1184 */       this.speechTimer = MathUtils.random(40.0F, 60.0F);
/* 1185 */       if (!this.saidWelcome) {
/* 1186 */         createSpeech(WELCOME_MSG);
/* 1187 */         this.saidWelcome = true;
/* 1188 */         welcomeSfx();
/*      */       } else {
/* 1190 */         playMiscSfx();
/* 1191 */         createSpeech(getIdleMsg());
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void welcomeSfx() {
/* 1197 */     int roll = MathUtils.random(2);
/* 1198 */     if (roll == 0) {
/* 1199 */       CardCrawlGame.sound.play("VO_MERCHANT_3A");
/* 1200 */     } else if (roll == 1) {
/* 1201 */       CardCrawlGame.sound.play("VO_MERCHANT_3B");
/*      */     } else {
/* 1203 */       CardCrawlGame.sound.play("VO_MERCHANT_3C");
/*      */     }
/*      */   }
/*      */   
/*      */   private void playMiscSfx() {
/* 1208 */     int roll = MathUtils.random(5);
/* 1209 */     if (roll == 0) {
/* 1210 */       CardCrawlGame.sound.play("VO_MERCHANT_MA");
/* 1211 */     } else if (roll == 1) {
/* 1212 */       CardCrawlGame.sound.play("VO_MERCHANT_MB");
/* 1213 */     } else if (roll == 2) {
/* 1214 */       CardCrawlGame.sound.play("VO_MERCHANT_MC");
/* 1215 */     } else if (roll == 3) {
/* 1216 */       CardCrawlGame.sound.play("VO_MERCHANT_3A");
/* 1217 */     } else if (roll == 4) {
/* 1218 */       CardCrawlGame.sound.play("VO_MERCHANT_3B");
/*      */     } else {
/* 1220 */       CardCrawlGame.sound.play("VO_MERCHANT_3C");
/*      */     }
/*      */   }
/*      */   
/*      */   public void playBuySfx() {
/* 1225 */     int roll = MathUtils.random(2);
/* 1226 */     if (roll == 0) {
/* 1227 */       CardCrawlGame.sound.play("VO_MERCHANT_KA");
/* 1228 */     } else if (roll == 1) {
/* 1229 */       CardCrawlGame.sound.play("VO_MERCHANT_KB");
/*      */     } else {
/* 1231 */       CardCrawlGame.sound.play("VO_MERCHANT_KC");
/*      */     }
/*      */   }
/*      */   
/*      */   public void playCantBuySfx() {
/* 1236 */     int roll = MathUtils.random(2);
/* 1237 */     if (roll == 0) {
/* 1238 */       CardCrawlGame.sound.play("VO_MERCHANT_2A");
/* 1239 */     } else if (roll == 1) {
/* 1240 */       CardCrawlGame.sound.play("VO_MERCHANT_2B");
/*      */     } else {
/* 1242 */       CardCrawlGame.sound.play("VO_MERCHANT_2C");
/*      */     }
/*      */   }
/*      */   
/*      */   private String getIdleMsg() {
/* 1247 */     return (String)this.idleMessages.get(MathUtils.random(this.idleMessages.size() - 1));
/*      */   }
/*      */   
/*      */   private void updateRug() {
/* 1251 */     if (this.rugY != 0.0F) {
/* 1252 */       this.rugY = MathUtils.lerp(this.rugY, 0.0F, Gdx.graphics.getDeltaTime() * 5.0F);
/* 1253 */       if (Math.abs(this.rugY - 0.0F) < 0.5F) {
/* 1254 */         this.rugY = 0.0F;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateHand() {
/* 1260 */     if (this.handTimer == 0.0F) {
/* 1261 */       if (this.handX != this.handTargetX) {
/* 1262 */         this.handX = MathUtils.lerp(this.handX, this.handTargetX, Gdx.graphics.getDeltaTime() * 6.0F);
/*      */       }
/* 1264 */       if (this.handY != this.handTargetY) {
/* 1265 */         if (this.handY > this.handTargetY) {
/* 1266 */           this.handY = MathUtils.lerp(this.handY, this.handTargetY, Gdx.graphics.getDeltaTime() * 6.0F);
/*      */         } else {
/* 1268 */           this.handY = MathUtils.lerp(this.handY, this.handTargetY, Gdx.graphics.getDeltaTime() * 6.0F / 4.0F);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb)
/*      */   {
/* 1276 */     sb.setColor(Color.WHITE);
/* 1277 */     sb.draw(rugImg, 0.0F, this.rugY, Settings.WIDTH, Settings.HEIGHT);
/*      */     
/* 1279 */     renderCardsAndPrices(sb);
/* 1280 */     renderRelics(sb);
/* 1281 */     renderPotions(sb);
/* 1282 */     renderPurge(sb);
/*      */     
/*      */ 
/* 1285 */     sb.draw(handImg, this.handX + this.f_effect.x, this.handY + this.f_effect.y, HAND_W, HAND_H);
/*      */     
/* 1287 */     if (this.speechBubble != null) {
/* 1288 */       this.speechBubble.render(sb);
/*      */     }
/* 1290 */     if (this.dialogTextEffect != null) {
/* 1291 */       this.dialogTextEffect.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderRelics(SpriteBatch sb) {
/* 1296 */     for (StoreRelic r : this.relics) {
/* 1297 */       r.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderPotions(SpriteBatch sb) {
/* 1302 */     for (StorePotion p : this.potions) {
/* 1303 */       p.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderCardsAndPrices(SpriteBatch sb)
/*      */   {
/* 1314 */     for (AbstractCard c : this.coloredCards) {
/* 1315 */       c.render(sb);
/*      */       
/*      */ 
/* 1318 */       sb.setColor(Color.WHITE);
/* 1319 */       sb.draw(ImageMaster.UI_GOLD, c.current_x + GOLD_IMG_OFFSET_X, c.current_y + GOLD_IMG_OFFSET_Y - (c.drawScale - 0.75F) * 200.0F * Settings.scale, GOLD_IMG_WIDTH, GOLD_IMG_WIDTH);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1327 */       Color color = Color.WHITE.cpy();
/* 1328 */       if (c.price > AbstractDungeon.player.gold) {
/* 1329 */         color = Color.SALMON.cpy();
/* 1330 */       } else if (c.equals(this.saleTag.card)) {
/* 1331 */         color = Color.SKY.cpy();
/*      */       }
/*      */       
/*      */ 
/* 1335 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*      */       
/*      */ 
/* 1338 */         Integer.toString(c.price), c.current_x + PRICE_TEXT_OFFSET_X, c.current_y + PRICE_TEXT_OFFSET_Y - (c.drawScale - 0.75F) * 200.0F * Settings.scale, color);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1345 */     for (AbstractCard c : this.colorlessCards) {
/* 1346 */       c.render(sb);
/*      */       
/*      */ 
/* 1349 */       sb.setColor(Color.WHITE);
/* 1350 */       sb.draw(ImageMaster.UI_GOLD, c.current_x + GOLD_IMG_OFFSET_X, c.current_y + GOLD_IMG_OFFSET_Y - (c.drawScale - 0.75F) * 200.0F * Settings.scale, GOLD_IMG_WIDTH, GOLD_IMG_WIDTH);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1358 */       Color color = Color.WHITE.cpy();
/* 1359 */       if (c.price > AbstractDungeon.player.gold) {
/* 1360 */         color = Color.SALMON.cpy();
/* 1361 */       } else if (c.equals(this.saleTag.card)) {
/* 1362 */         color = Color.SKY.cpy();
/*      */       }
/*      */       
/*      */ 
/* 1366 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*      */       
/*      */ 
/* 1369 */         Integer.toString(c.price), c.current_x + PRICE_TEXT_OFFSET_X, c.current_y + PRICE_TEXT_OFFSET_Y - (c.drawScale - 0.75F) * 200.0F * Settings.scale, color);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1376 */     if (this.coloredCards.contains(this.saleTag.card)) {
/* 1377 */       this.saleTag.render(sb);
/*      */     }
/* 1379 */     if (this.colorlessCards.contains(this.saleTag.card)) {
/* 1380 */       this.saleTag.render(sb);
/*      */     }
/*      */     
/*      */ 
/* 1384 */     for (AbstractCard c : this.coloredCards) {
/* 1385 */       c.renderCardTip(sb);
/*      */     }
/*      */     
/*      */ 
/* 1389 */     for (AbstractCard c : this.colorlessCards) {
/* 1390 */       c.renderCardTip(sb);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderPurge(SpriteBatch sb)
/*      */   {
/* 1401 */     sb.setColor(new Color(0.0F, 0.0F, 0.0F, 0.25F));
/* 1402 */     sb.draw(ImageMaster.CARD_SKILL_BG_SILHOUETTE, this.purgeCardX - 256.0F + 18.0F * Settings.scale, this.purgeCardY - 256.0F - 14.0F * Settings.scale, 256.0F, 256.0F, 512.0F, 512.0F, this.purgeCardScale, this.purgeCardScale, 0.0F, 0, 0, 512, 512, false, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1420 */     sb.setColor(Color.WHITE);
/* 1421 */     if (this.purgeAvailable) {
/* 1422 */       sb.draw(removeServiceImg, this.purgeCardX - 256.0F, this.purgeCardY - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.purgeCardScale, this.purgeCardScale, 0.0F, 0, 0, 512, 512, false, false);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1441 */       sb.draw(ImageMaster.UI_GOLD, this.purgeCardX + GOLD_IMG_OFFSET_X, this.purgeCardY + GOLD_IMG_OFFSET_Y - (this.purgeCardScale / Settings.scale - 0.75F) * 200.0F * Settings.scale, GOLD_IMG_WIDTH, GOLD_IMG_WIDTH);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1451 */       Color color = Color.WHITE;
/* 1452 */       if (actualPurgeCost > AbstractDungeon.player.gold) {
/* 1453 */         color = Color.SALMON;
/*      */       }
/*      */       
/* 1456 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*      */       
/*      */ 
/* 1459 */         Integer.toString(actualPurgeCost), this.purgeCardX + PRICE_TEXT_OFFSET_X, this.purgeCardY + PRICE_TEXT_OFFSET_Y - (this.purgeCardScale / Settings.scale - 0.75F) * 200.0F * Settings.scale, color);
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1466 */       sb.draw(soldOutImg, this.purgeCardX - 256.0F, this.purgeCardY - 256.0F, 256.0F, 256.0F, 512.0F, 512.0F, this.purgeCardScale, this.purgeCardScale, 0.0F, 0, 0, 512, 512, false, false);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\shop\ShopScreen.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */