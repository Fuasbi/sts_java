/*     */ package com.megacrit.cardcrawl.shop;
/*     */ 
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*     */ import com.megacrit.cardcrawl.metrics.MetricData;
/*     */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*     */ import com.megacrit.cardcrawl.relics.Courier;
/*     */ import com.megacrit.cardcrawl.relics.OldCoin;
/*     */ import com.megacrit.cardcrawl.relics.SmilingMask;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.screens.SingleRelicViewPopup;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class StoreRelic
/*     */ {
/*     */   public AbstractRelic relic;
/*     */   private ShopScreen shopScreen;
/*     */   public int price;
/*     */   private int slot;
/*  31 */   public boolean isPurchased = false;
/*  32 */   private static final float RELIC_GOLD_OFFSET_X = -56.0F * Settings.scale;
/*  33 */   private static final float RELIC_GOLD_OFFSET_Y = -100.0F * Settings.scale;
/*  34 */   private static final float RELIC_PRICE_OFFSET_X = 14.0F * Settings.scale;
/*  35 */   private static final float RELIC_PRICE_OFFSET_Y = -62.0F * Settings.scale;
/*  36 */   private static final float GOLD_IMG_WIDTH = ImageMaster.UI_GOLD.getWidth() * Settings.scale;
/*     */   
/*     */   public StoreRelic(AbstractRelic relic, int slot, ShopScreen screenRef) {
/*  39 */     this.relic = relic;
/*  40 */     this.price = relic.getPrice();
/*  41 */     this.slot = slot;
/*  42 */     this.shopScreen = screenRef;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void update(float rugY)
/*     */   {
/*  51 */     if (this.relic != null)
/*     */     {
/*  53 */       this.relic.currentX = (1000.0F * Settings.scale + 150.0F * this.slot * Settings.scale);
/*  54 */       this.relic.currentY = (rugY + 400.0F * Settings.scale);
/*  55 */       this.relic.hb.move(this.relic.currentX, this.relic.currentY);
/*     */       
/*     */ 
/*  58 */       this.relic.hb.update();
/*  59 */       if (this.relic.hb.hovered) {
/*  60 */         this.shopScreen.moveHand(this.relic.currentX - 190.0F * Settings.scale, this.relic.currentY - 70.0F * Settings.scale);
/*  61 */         if (InputHelper.justClickedLeft) {
/*  62 */           this.relic.hb.clickStarted = true;
/*     */         }
/*  64 */         this.relic.scale = (Settings.scale * 1.25F);
/*     */       } else {
/*  66 */         this.relic.scale = MathHelper.scaleLerpSnap(this.relic.scale, Settings.scale);
/*     */       }
/*     */       
/*  69 */       if ((this.relic.hb.hovered) && (InputHelper.justClickedRight))
/*     */       {
/*  71 */         CardCrawlGame.relicPopup.open(this.relic);
/*     */       }
/*     */       
/*     */ 
/*  75 */       if ((this.relic.hb.clicked) || ((this.relic.hb.hovered) && (CInputActionSet.select.isJustPressed()))) {
/*  76 */         this.relic.hb.clicked = false;
/*  77 */         if (AbstractDungeon.player.gold >= this.price) {
/*  78 */           AbstractDungeon.player.loseGold(this.price);
/*  79 */           CardCrawlGame.sound.play("SHOP_PURCHASE", 0.1F);
/*  80 */           CardCrawlGame.metricData.addShopPurchaseData(this.relic.relicId);
/*  81 */           AbstractDungeon.getCurrRoom().relics.add(this.relic);
/*  82 */           this.relic.instantObtain(AbstractDungeon.player, AbstractDungeon.player.relics.size(), true);
/*  83 */           this.relic.flash();
/*     */           
/*  85 */           if (this.relic.relicId.equals("Membership Card")) {
/*  86 */             this.shopScreen.applyDiscount(0.5F, true);
/*     */           }
/*  88 */           if (this.relic.relicId.equals("Smiling Mask")) {
/*  89 */             ShopScreen.actualPurgeCost = 50;
/*     */           }
/*     */           
/*  92 */           if (this.relic.relicId.equals("Toxic Egg 2")) {
/*  93 */             this.shopScreen.applyUpgrades(AbstractCard.CardType.SKILL);
/*     */           }
/*     */           
/*  96 */           if (this.relic.relicId.equals("Molten Egg 2")) {
/*  97 */             this.shopScreen.applyUpgrades(AbstractCard.CardType.ATTACK);
/*     */           }
/*     */           
/* 100 */           if (this.relic.relicId.equals("Frozen Egg 2")) {
/* 101 */             this.shopScreen.applyUpgrades(AbstractCard.CardType.POWER);
/*     */           }
/*     */           
/* 104 */           this.shopScreen.playBuySfx();
/* 105 */           this.shopScreen.createSpeech(ShopScreen.getBuyMsg());
/*     */           
/*     */ 
/* 108 */           if ((this.relic.relicId.equals("The Courier")) || (AbstractDungeon.player.hasRelic("The Courier"))) {
/* 109 */             AbstractRelic tempRelic = AbstractDungeon.returnRandomRelicEnd(ShopScreen.rollRelicTier());
/*     */             
/* 111 */             while (((tempRelic instanceof OldCoin)) || ((tempRelic instanceof SmilingMask)) || ((tempRelic instanceof Courier)))
/*     */             {
/* 113 */               tempRelic = AbstractDungeon.returnRandomRelicEnd(ShopScreen.rollRelicTier());
/*     */             }
/* 115 */             this.relic = tempRelic;
/* 116 */             this.price = this.relic.getPrice();
/* 117 */             this.shopScreen.getNewPrice(this);
/*     */           } else {
/* 119 */             this.isPurchased = true;
/*     */           }
/*     */         }
/*     */         else
/*     */         {
/* 124 */           this.shopScreen.playCantBuySfx();
/* 125 */           this.shopScreen.createSpeech(ShopScreen.getCantBuyMsg());
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void hide() {
/* 132 */     if (this.relic != null) {
/* 133 */       this.relic.currentY = (Settings.HEIGHT + 200.0F * Settings.scale);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void render(SpriteBatch sb)
/*     */   {
/* 143 */     if (this.relic != null) {
/* 144 */       this.relic.renderWithoutAmount(sb, new Color(0.0F, 0.0F, 0.0F, 0.25F));
/*     */       
/*     */ 
/* 147 */       sb.setColor(Color.WHITE);
/* 148 */       sb.draw(ImageMaster.UI_GOLD, this.relic.currentX + RELIC_GOLD_OFFSET_X, this.relic.currentY + RELIC_GOLD_OFFSET_Y, GOLD_IMG_WIDTH, GOLD_IMG_WIDTH);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 155 */       Color color = Color.WHITE;
/* 156 */       if (this.price > AbstractDungeon.player.gold) {
/* 157 */         color = Color.SALMON;
/*     */       }
/* 159 */       FontHelper.renderFontLeftTopAligned(sb, FontHelper.tipHeaderFont, 
/*     */       
/*     */ 
/* 162 */         Integer.toString(this.price), this.relic.currentX + RELIC_PRICE_OFFSET_X, this.relic.currentY + RELIC_PRICE_OFFSET_Y, color);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\shop\StoreRelic.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */