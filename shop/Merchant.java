/*     */ package com.megacrit.cardcrawl.shop;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.characters.AnimatedNpc;
/*     */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*     */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*     */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*     */ import com.megacrit.cardcrawl.ui.buttons.ProceedButton;
/*     */ import com.megacrit.cardcrawl.vfx.SpeechBubble;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Objects;
/*     */ 
/*     */ public class Merchant
/*     */ {
/*  28 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Merchant");
/*  29 */   public static final String[] NAMES = characterStrings.NAMES;
/*  30 */   public static final String[] TEXT = characterStrings.TEXT;
/*     */   
/*     */   public AnimatedNpc anim;
/*  33 */   public static final float DRAW_X = Settings.WIDTH * 0.5F + 34.0F * Settings.scale; public static final float DRAW_Y = AbstractDungeon.floorY - 109.0F * Settings.scale;
/*     */   
/*  35 */   public Hitbox hb = new Hitbox(360.0F * Settings.scale, 300.0F * Settings.scale);
/*  36 */   private ArrayList<AbstractCard> cards1 = new ArrayList();
/*  37 */   private ArrayList<AbstractCard> cards2 = new ArrayList();
/*     */   
/*     */ 
/*  40 */   private ArrayList<String> idleMessages = new ArrayList();
/*  41 */   private float speechTimer = 1.5F;
/*  42 */   private boolean saidWelcome = false;
/*     */   private static final float MIN_IDLE_MSG_TIME = 40.0F;
/*     */   private static final float MAX_IDLE_MSG_TIME = 60.0F;
/*     */   private static final float SPEECH_DURATION = 3.0F;
/*  46 */   private int shopScreen = 1;
/*     */   private float modX;
/*     */   private float modY;
/*     */   
/*  50 */   public Merchant() { this(0.0F, 0.0F, 1); }
/*     */   
/*     */   public Merchant(float x, float y, int newShopScreen)
/*     */   {
/*  54 */     this.anim = new AnimatedNpc(1260.0F * Settings.scale, 370.0F * Settings.scale, "images/npcs/merchant/skeleton.atlas", "images/npcs/merchant/skeleton.json", "idle");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  62 */     this.cards1.add(AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.ATTACK, true).makeCopy());
/*     */     
/*     */ 
/*     */ 
/*  66 */     AbstractCard addCard = AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.ATTACK, true).makeCopy();
/*  67 */     while (Objects.equals(addCard.cardID, ((AbstractCard)this.cards1.get(this.cards1.size() - 1)).cardID)) {
/*  68 */       addCard = AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.ATTACK, true).makeCopy();
/*     */     }
/*  70 */     this.cards1.add(addCard);
/*     */     
/*  72 */     this.cards1.add(AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.SKILL, true).makeCopy());
/*  73 */     addCard = AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.SKILL, true).makeCopy();
/*  74 */     while (Objects.equals(addCard.cardID, ((AbstractCard)this.cards1.get(this.cards1.size() - 1)).cardID)) {
/*  75 */       addCard = AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.SKILL, true).makeCopy();
/*     */     }
/*  77 */     this.cards1.add(addCard);
/*     */     
/*  79 */     this.cards1.add(AbstractDungeon.getCardFromPool(AbstractDungeon.rollRarity(), AbstractCard.CardType.POWER, true).makeCopy());
/*     */     
/*     */ 
/*  82 */     this.cards2.add(AbstractDungeon.getColorlessCardFromPool(AbstractCard.CardRarity.UNCOMMON).makeCopy());
/*  83 */     this.cards2.add(AbstractDungeon.getColorlessCardFromPool(AbstractCard.CardRarity.RARE).makeCopy());
/*     */     
/*     */ 
/*  86 */     java.util.Collections.addAll(this.idleMessages, TEXT);
/*     */     
/*  88 */     this.speechTimer = 1.5F;
/*  89 */     this.modX = x;
/*  90 */     this.modY = y;
/*  91 */     this.hb.move(DRAW_X + (250.0F + x) * Settings.scale, DRAW_Y + (130.0F + y) * Settings.scale);
/*  92 */     this.shopScreen = newShopScreen;
/*  93 */     AbstractDungeon.shopScreen.init(this.cards1, this.cards2);
/*     */   }
/*     */   
/*     */   public void update()
/*     */   {
/*  98 */     this.hb.update();
/*  99 */     if (((this.hb.hovered) && (com.megacrit.cardcrawl.helpers.input.InputHelper.justClickedLeft)) || ((CInputActionSet.select.isJustPressed()) && (!AbstractDungeon.isScreenUp) && (!AbstractDungeon.isFadingOut) && (!AbstractDungeon.player.viewingRelics)))
/*     */     {
/*     */ 
/* 102 */       AbstractDungeon.overlayMenu.proceedButton.setLabel(NAMES[0]);
/* 103 */       this.saidWelcome = true;
/* 104 */       AbstractDungeon.shopScreen.open();
/*     */       
/* 106 */       this.hb.hovered = false;
/*     */     }
/*     */     
/* 109 */     this.speechTimer -= Gdx.graphics.getDeltaTime();
/*     */     
/* 111 */     if ((this.speechTimer < 0.0F) && (this.shopScreen == 1)) {
/* 112 */       String msg = (String)this.idleMessages.get(MathUtils.random(0, this.idleMessages.size() - 1));
/* 113 */       if (!this.saidWelcome) {
/* 114 */         this.saidWelcome = true;
/* 115 */         welcomeSfx();
/* 116 */         msg = NAMES[1];
/*     */       } else {
/* 118 */         playMiscSfx();
/*     */       }
/*     */       
/* 121 */       if (MathUtils.randomBoolean()) {
/* 122 */         AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX - 50.0F * Settings.scale, this.hb.cY + 70.0F * Settings.scale, 3.0F, msg, false));
/*     */ 
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/*     */ 
/*     */ 
/* 130 */         AbstractDungeon.effectList.add(new SpeechBubble(this.hb.cX + 50.0F * Settings.scale, this.hb.cY + 70.0F * Settings.scale, 3.0F, msg, true));
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 138 */       this.speechTimer = MathUtils.random(40.0F, 60.0F);
/*     */     }
/*     */   }
/*     */   
/*     */   private void welcomeSfx() {
/* 143 */     int roll = MathUtils.random(2);
/* 144 */     if (roll == 0) {
/* 145 */       CardCrawlGame.sound.play("VO_MERCHANT_3A");
/* 146 */     } else if (roll == 1) {
/* 147 */       CardCrawlGame.sound.play("VO_MERCHANT_3B");
/*     */     } else {
/* 149 */       CardCrawlGame.sound.play("VO_MERCHANT_3C");
/*     */     }
/*     */   }
/*     */   
/*     */   private void playMiscSfx() {
/* 154 */     int roll = MathUtils.random(5);
/* 155 */     if (roll == 0) {
/* 156 */       CardCrawlGame.sound.play("VO_MERCHANT_MA");
/* 157 */     } else if (roll == 1) {
/* 158 */       CardCrawlGame.sound.play("VO_MERCHANT_MB");
/* 159 */     } else if (roll == 2) {
/* 160 */       CardCrawlGame.sound.play("VO_MERCHANT_MC");
/* 161 */     } else if (roll == 3) {
/* 162 */       CardCrawlGame.sound.play("VO_MERCHANT_3A");
/* 163 */     } else if (roll == 4) {
/* 164 */       CardCrawlGame.sound.play("VO_MERCHANT_3B");
/*     */     } else {
/* 166 */       CardCrawlGame.sound.play("VO_MERCHANT_3C");
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 171 */     sb.setColor(Color.WHITE);
/* 172 */     sb.draw(ImageMaster.MERCHANT_RUG_IMG, DRAW_X + this.modX, DRAW_Y + this.modY, 512.0F * Settings.scale, 512.0F * Settings.scale);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 179 */     if (this.hb.hovered) {
/* 180 */       sb.setBlendFunction(770, 1);
/* 181 */       sb.setColor(new Color(1.0F, 1.0F, 1.0F, 0.3F));
/* 182 */       sb.draw(ImageMaster.MERCHANT_RUG_IMG, DRAW_X + this.modX, DRAW_Y + this.modY, 512.0F * Settings.scale, 512.0F * Settings.scale);
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 188 */       sb.setBlendFunction(770, 771);
/*     */     }
/*     */     
/* 191 */     if (Settings.isControllerMode) {
/* 192 */       sb.setColor(Color.WHITE);
/* 193 */       sb.draw(CInputActionSet.select
/* 194 */         .getKeyImg(), DRAW_X - 32.0F + 150.0F * Settings.scale, DRAW_Y - 32.0F + 100.0F * Settings.scale, 32.0F, 32.0F, 64.0F, 64.0F, Settings.scale, Settings.scale, 0.0F, 0, 0, 64, 64, false, false);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 211 */     this.anim.render(sb);
/* 212 */     this.hb.render(sb);
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\shop\Merchant.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */