/*    */ package com.megacrit.cardcrawl.characters;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import java.util.ArrayList;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Ironclad
/*    */   extends AbstractPlayer
/*    */ {
/* 23 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Ironclad");
/* 24 */   public static final String[] NAMES = characterStrings.NAMES;
/* 25 */   public static final String[] TEXT = characterStrings.TEXT;
/*    */   public static final int START_HP = 80;
/*    */   
/*    */   public Ironclad(String name, AbstractPlayer.PlayerClass setClass) {
/* 29 */     super(name, setClass);
/*    */     
/* 31 */     this.dialogX = (this.drawX + 0.0F * Settings.scale);
/* 32 */     this.dialogY = (this.drawY + 220.0F * Settings.scale);
/*    */     
/* 34 */     initializeClass(null, "images/characters/ironclad/shoulder2.png", "images/characters/ironclad/shoulder.png", "images/characters/ironclad/corpse.png", 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 39 */       getLoadout(), 20.0F, -10.0F, 220.0F, 290.0F, new EnergyManager(3));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 46 */     loadAnimation("images/characters/ironclad/idle/skeleton.atlas", "images/characters/ironclad/idle/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 51 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "animation", true);
/* 52 */     e.setTime(e.getEndTime() * MathUtils.random());
/*    */     
/* 54 */     if (((Settings.dailyModsEnabled()) && (((Boolean)DailyMods.cardMods.get("Diverse")).booleanValue())) || ((Settings.isTrial) && (customMods != null) && 
/* 55 */       (customMods.contains("Blue Cards")))) {
/* 56 */       this.masterMaxOrbs = 1;
/*    */     }
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingDeck() {
/* 61 */     ArrayList<String> retVal = new ArrayList();
/* 62 */     retVal.add("Strike_R");
/* 63 */     retVal.add("Strike_R");
/* 64 */     retVal.add("Strike_R");
/* 65 */     retVal.add("Strike_R");
/* 66 */     retVal.add("Strike_R");
/* 67 */     retVal.add("Defend_R");
/* 68 */     retVal.add("Defend_R");
/* 69 */     retVal.add("Defend_R");
/* 70 */     retVal.add("Defend_R");
/* 71 */     retVal.add("Bash");
/* 72 */     return retVal;
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingRelics() {
/* 76 */     ArrayList<String> retVal = new ArrayList();
/* 77 */     retVal.add("Burning Blood");
/* 78 */     UnlockTracker.markRelicAsSeen("Burning Blood");
/* 79 */     return retVal;
/*    */   }
/*    */   
/*    */   public static CharSelectInfo getLoadout() {
/* 83 */     return new CharSelectInfo(NAMES[0], TEXT[0], 80, 80, 0, 99, 5, AbstractPlayer.PlayerClass.IRONCLAD, 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 92 */       getStartingRelics(), 
/* 93 */       getStartingDeck(), false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\characters\Ironclad.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */