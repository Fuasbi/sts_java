/*    */ package com.megacrit.cardcrawl.characters;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Defect
/*    */   extends AbstractPlayer
/*    */ {
/* 19 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Defect");
/* 20 */   public static final String[] NAMES = characterStrings.NAMES;
/* 21 */   public static final String[] TEXT = characterStrings.TEXT;
/*    */   
/*    */   public Defect(String name, AbstractPlayer.PlayerClass setClass) {
/* 24 */     super(name, setClass);
/* 25 */     this.drawX += 5.0F * Settings.scale;
/* 26 */     this.drawY += 7.0F * Settings.scale;
/* 27 */     this.dialogX = (this.drawX + 0.0F * Settings.scale);
/* 28 */     this.dialogY = (this.drawY + 170.0F * Settings.scale);
/*    */     
/* 30 */     initializeClass(null, "images/characters/defect/shoulder.png", "images/characters/defect/shoulder.png", "images/characters/defect/corpse.png", 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 35 */       getLoadout(), 0.0F, -5.0F, 240.0F, 244.0F, new EnergyManager(3));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 42 */     loadAnimation("images/characters/defect/idle/skeleton.atlas", "images/characters/defect/idle/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 47 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "Idle", true);
/* 48 */     e.setTime(e.getEndTime() * MathUtils.random());
/* 49 */     e.setTimeScale(0.9F);
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingRelics() {
/* 53 */     ArrayList<String> retVal = new ArrayList();
/* 54 */     retVal.add("Cracked Core");
/* 55 */     return retVal;
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingDeck() {
/* 59 */     ArrayList<String> retVal = new ArrayList();
/* 60 */     retVal.add("Strike_B");
/* 61 */     retVal.add("Strike_B");
/* 62 */     retVal.add("Strike_B");
/* 63 */     retVal.add("Strike_B");
/* 64 */     retVal.add("Defend_B");
/* 65 */     retVal.add("Defend_B");
/* 66 */     retVal.add("Defend_B");
/* 67 */     retVal.add("Defend_B");
/* 68 */     retVal.add("Zap");
/* 69 */     retVal.add("Dualcast");
/* 70 */     return retVal;
/*    */   }
/*    */   
/*    */   public static CharSelectInfo getLoadout() {
/* 74 */     return new CharSelectInfo(NAMES[0], TEXT[0], 75, 75, 3, 99, 5, AbstractPlayer.PlayerClass.DEFECT, 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 83 */       getStartingRelics(), 
/* 84 */       getStartingDeck(), false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\characters\Defect.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */