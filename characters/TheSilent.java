/*    */ package com.megacrit.cardcrawl.characters;
/*    */ 
/*    */ import com.badlogic.gdx.math.MathUtils;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationState.TrackEntry;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.core.EnergyManager;
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ import com.megacrit.cardcrawl.daily.DailyMods;
/*    */ import com.megacrit.cardcrawl.localization.CharacterStrings;
/*    */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*    */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*    */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*    */ import java.util.ArrayList;
/*    */ import java.util.HashMap;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TheSilent
/*    */   extends AbstractPlayer
/*    */ {
/* 24 */   private static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString("Silent");
/* 25 */   public static final String[] NAMES = characterStrings.NAMES;
/* 26 */   public static final String[] TEXT = characterStrings.TEXT;
/*    */   public static final int START_HP = 70;
/*    */   
/*    */   public TheSilent(String name, AbstractPlayer.PlayerClass setClass) {
/* 30 */     super(name, setClass);
/*    */     
/* 32 */     this.dialogX = (this.drawX + 0.0F * Settings.scale);
/* 33 */     this.dialogY = (this.drawY + 170.0F * Settings.scale);
/*    */     
/* 35 */     initializeClass(null, "images/characters/theSilent/shoulder2.png", "images/characters/theSilent/shoulder.png", "images/characters/theSilent/corpse.png", 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 40 */       getLoadout(), 0.0F, -20.0F, 240.0F, 240.0F, new EnergyManager(3));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 47 */     loadAnimation("images/characters/theSilent/idle/skeleton.atlas", "images/characters/theSilent/idle/skeleton.json", 1.0F);
/*    */     
/*    */ 
/*    */ 
/* 51 */     AnimationState.TrackEntry e = this.state.setAnimation(0, "idle", true);
/* 52 */     e.setTime(e.getEndTime() * MathUtils.random());
/*    */     
/* 54 */     if (((Settings.dailyModsEnabled()) && (((Boolean)DailyMods.cardMods.get("Diverse")).booleanValue())) || ((Settings.isTrial) && (customMods != null) && 
/* 55 */       (customMods.contains("Blue Cards")))) {
/* 56 */       this.masterMaxOrbs = 1;
/*    */     }
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingDeck() {
/* 61 */     ArrayList<String> retVal = new ArrayList();
/* 62 */     retVal.add("Defend_G");
/* 63 */     retVal.add("Defend_G");
/* 64 */     retVal.add("Defend_G");
/* 65 */     retVal.add("Defend_G");
/* 66 */     retVal.add("Defend_G");
/* 67 */     retVal.add("Strike_G");
/* 68 */     retVal.add("Strike_G");
/* 69 */     retVal.add("Strike_G");
/* 70 */     retVal.add("Strike_G");
/* 71 */     retVal.add("Strike_G");
/* 72 */     retVal.add("Survivor");
/* 73 */     retVal.add("Neutralize");
/* 74 */     return retVal;
/*    */   }
/*    */   
/*    */   public static ArrayList<String> getStartingRelics() {
/* 78 */     ArrayList<String> retVal = new ArrayList();
/* 79 */     retVal.add("Ring of the Snake");
/* 80 */     UnlockTracker.markRelicAsSeen("Ring of the Snake");
/* 81 */     return retVal;
/*    */   }
/*    */   
/*    */   public static CharSelectInfo getLoadout() {
/* 85 */     return new CharSelectInfo(NAMES[0], TEXT[0], 70, 70, 0, 99, 5, AbstractPlayer.PlayerClass.THE_SILENT, 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 94 */       getStartingRelics(), 
/* 95 */       getStartingDeck(), false);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\characters\TheSilent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */