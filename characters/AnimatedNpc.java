/*    */ package com.megacrit.cardcrawl.characters;
/*    */ 
/*    */ import com.badlogic.gdx.Files;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*    */ import com.esotericsoftware.spine.AnimationState;
/*    */ import com.esotericsoftware.spine.AnimationStateData;
/*    */ import com.esotericsoftware.spine.Skeleton;
/*    */ import com.esotericsoftware.spine.SkeletonData;
/*    */ import com.esotericsoftware.spine.SkeletonJson;
/*    */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.helpers.HeartAnimListener;
/*    */ import org.apache.logging.log4j.LogManager;
/*    */ import org.apache.logging.log4j.Logger;
/*    */ 
/*    */ public class AnimatedNpc
/*    */ {
/* 22 */   private static final Logger logger = LogManager.getLogger(AnimatedNpc.class.getName());
/* 23 */   private TextureAtlas atlas = null;
/*    */   private Skeleton skeleton;
/*    */   private AnimationState state;
/*    */   private AnimationStateData stateData;
/*    */   
/*    */   public AnimatedNpc(float x, float y, String atlasUrl, String skeletonUrl, String trackName) {
/* 29 */     loadAnimation(atlasUrl, skeletonUrl, 1.0F);
/* 30 */     this.skeleton.setPosition(x, y);
/* 31 */     this.state.setAnimation(0, trackName, true);
/* 32 */     this.state.setTimeScale(1.0F);
/*    */   }
/*    */   
/*    */   private void loadAnimation(String atlasUrl, String skeletonUrl, float scale) {
/* 36 */     this.atlas = new TextureAtlas(Gdx.files.internal(atlasUrl));
/* 37 */     SkeletonJson json = new SkeletonJson(this.atlas);
/* 38 */     json.setScale(com.megacrit.cardcrawl.core.Settings.scale / scale);
/* 39 */     SkeletonData skeletonData = json.readSkeletonData(Gdx.files.internal(skeletonUrl));
/* 40 */     this.skeleton = new Skeleton(skeletonData);
/* 41 */     this.skeleton.setColor(Color.WHITE);
/* 42 */     this.stateData = new AnimationStateData(skeletonData);
/* 43 */     this.state = new AnimationState(this.stateData);
/*    */   }
/*    */   
/*    */   public void render(SpriteBatch sb) {
/* 47 */     this.state.update(Gdx.graphics.getDeltaTime());
/* 48 */     this.state.apply(this.skeleton);
/* 49 */     this.skeleton.updateWorldTransform();
/* 50 */     this.skeleton.setFlip(false, false);
/* 51 */     this.skeleton.setColor(Color.WHITE);
/* 52 */     sb.end();
/* 53 */     CardCrawlGame.psb.begin();
/* 54 */     AbstractCreature.sr.draw(CardCrawlGame.psb, this.skeleton);
/* 55 */     CardCrawlGame.psb.end();
/* 56 */     sb.begin();
/* 57 */     sb.setBlendFunction(770, 771);
/*    */   }
/*    */   
/*    */   public void dispose() {
/* 61 */     logger.info("Disposing merchant atlas.");
/* 62 */     this.atlas.dispose();
/*    */   }
/*    */   
/*    */   public void setTimeScale(float setScale) {
/* 66 */     this.state.setTimeScale(setScale);
/*    */   }
/*    */   
/*    */   public void addListener(HeartAnimListener listener) {
/* 70 */     this.state.addListener(listener);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\characters\AnimatedNpc.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */