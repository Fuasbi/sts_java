/*      */ package com.megacrit.cardcrawl.characters;
/*      */ 
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.Input;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.Bezier;
/*      */ import com.badlogic.gdx.math.Interpolation;
/*      */ import com.badlogic.gdx.math.Interpolation.ElasticOut;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.badlogic.gdx.math.Vector2;
/*      */ import com.esotericsoftware.spine.AnimationState;
/*      */ import com.esotericsoftware.spine.Skeleton;
/*      */ import com.esotericsoftware.spine.SkeletonMeshRenderer;
/*      */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*      */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*      */ import com.megacrit.cardcrawl.actions.defect.AnimateOrbAction;
/*      */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*      */ import com.megacrit.cardcrawl.actions.defect.EvokeOrbAction;
/*      */ import com.megacrit.cardcrawl.actions.utility.UseCardAction;
/*      */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardTarget;
/*      */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup;
/*      */ import com.megacrit.cardcrawl.cards.CardGroup.CardGroupType;
/*      */ import com.megacrit.cardcrawl.cards.CardQueueItem;
/*      */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*      */ import com.megacrit.cardcrawl.cards.DamageInfo.DamageType;
/*      */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.EnergyManager;
/*      */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.daily.DailyMods;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.helpers.CardLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.RelicLibrary;
/*      */ import com.megacrit.cardcrawl.helpers.ShaderHelper;
/*      */ import com.megacrit.cardcrawl.helpers.ShaderHelper.Shader;
/*      */ import com.megacrit.cardcrawl.helpers.TipTracker;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputAction;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.controller.CInputHelper;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputAction;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputActionSet;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.TutorialStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*      */ import com.megacrit.cardcrawl.orbs.Dark;
/*      */ import com.megacrit.cardcrawl.orbs.EmptyOrbSlot;
/*      */ import com.megacrit.cardcrawl.orbs.Plasma;
/*      */ import com.megacrit.cardcrawl.potions.AbstractPotion;
/*      */ import com.megacrit.cardcrawl.potions.PotionSlot;
/*      */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*      */ import com.megacrit.cardcrawl.powers.PlatedArmorPower;
/*      */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.BottledFlame;
/*      */ import com.megacrit.cardcrawl.relics.BottledLightning;
/*      */ import com.megacrit.cardcrawl.relics.BottledTornado;
/*      */ import com.megacrit.cardcrawl.relics.LizardTail;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*      */ import com.megacrit.cardcrawl.rooms.CampfireUI;
/*      */ import com.megacrit.cardcrawl.rooms.MonsterRoom;
/*      */ import com.megacrit.cardcrawl.rooms.RestRoom;
/*      */ import com.megacrit.cardcrawl.rooms.ShopRoom;
/*      */ import com.megacrit.cardcrawl.screens.CharSelectInfo;
/*      */ import com.megacrit.cardcrawl.screens.DeathScreen;
/*      */ import com.megacrit.cardcrawl.ui.FtueTip;
/*      */ import com.megacrit.cardcrawl.ui.FtueTip.TipType;
/*      */ import com.megacrit.cardcrawl.ui.MultiPageFtue;
/*      */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*      */ import com.megacrit.cardcrawl.ui.panels.PotionPopUp;
/*      */ import com.megacrit.cardcrawl.ui.panels.TopPanel;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.BorderFlashEffect;
/*      */ import com.megacrit.cardcrawl.vfx.ThoughtBubble;
/*      */ import com.megacrit.cardcrawl.vfx.TintEffect;
/*      */ import com.megacrit.cardcrawl.vfx.cardManip.PurgeCardEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.BlockedWordEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.HbBlockBrokenEffect;
/*      */ import com.megacrit.cardcrawl.vfx.combat.StrikeEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Collections;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public abstract class AbstractPlayer
/*      */   extends AbstractCreature
/*      */ {
/*  114 */   private static final Logger logger = LogManager.getLogger(AbstractPlayer.class.getName());
/*  115 */   private static final TutorialStrings tutorialStrings = CardCrawlGame.languagePack.getTutorialString("Player Tips");
/*  116 */   public static final String[] MSG = tutorialStrings.TEXT;
/*  117 */   public static final String[] LABEL = tutorialStrings.LABEL;
/*      */   
/*      */   public PlayerClass chosenClass;
/*      */   
/*      */   public int gameHandSize;
/*      */   
/*      */   public int masterHandSize;
/*  124 */   public CardGroup masterDeck = new CardGroup(CardGroup.CardGroupType.MASTER_DECK); public CardGroup drawPile = new CardGroup(CardGroup.CardGroupType.DRAW_PILE); public CardGroup hand = new CardGroup(CardGroup.CardGroupType.HAND); public CardGroup discardPile = new CardGroup(CardGroup.CardGroupType.DISCARD_PILE); public CardGroup exhaustPile = new CardGroup(CardGroup.CardGroupType.EXHAUST_PILE); public CardGroup limbo = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
/*      */   
/*      */ 
/*      */ 
/*  128 */   public ArrayList<AbstractRelic> relics = new ArrayList();
/*  129 */   public ArrayList<AbstractBlight> blights = new ArrayList();
/*  130 */   public int potionSlots = 3;
/*  131 */   public ArrayList<AbstractPotion> potions = new ArrayList();
/*      */   public EnergyManager energy;
/*  133 */   public boolean isEndingTurn = false; public boolean viewingRelics = false; public boolean inspectMode = false;
/*  134 */   public Hitbox inspectHb = null;
/*  135 */   public static int poisonKillCount = 0;
/*  136 */   public int damagedThisCombat = 0;
/*  137 */   public int cardsPlayedThisTurn = 0;
/*      */   
/*      */   public String title;
/*      */   
/*  141 */   public ArrayList<AbstractOrb> orbs = new ArrayList();
/*      */   
/*      */   public int masterMaxOrbs;
/*      */   
/*      */   public int maxOrbs;
/*      */   
/*  147 */   private boolean isHoveringCard = false;
/*  148 */   public boolean isHoveringDropZone = false;
/*  149 */   private float hoverTimer = 0.0F;
/*  150 */   private float hoverStartLine = 0.0F;
/*  151 */   private boolean passedHesitationLine = false;
/*  152 */   public AbstractCard hoveredCard = null; public AbstractCard toHover = null; public AbstractCard cardInUse = null;
/*  153 */   public boolean isDraggingCard = false;
/*  154 */   private boolean isUsingClickDragControl = false;
/*  155 */   private float clickDragTimer = 0.0F;
/*  156 */   public boolean inSingleTargetMode = false;
/*  157 */   private AbstractMonster hoveredMonster = null;
/*  158 */   public float hoverEnemyWaitTimer = 0.0F;
/*      */   
/*      */   private static final float HOVER_ENEMY_WAIT_TIME = 1.0F;
/*      */   public Texture img;
/*      */   public Texture shoulderImg;
/*      */   public Texture shoulder2Img;
/*      */   public Texture corpseImg;
/*  165 */   private static final Color ARROW_COLOR = new Color(1.0F, 0.2F, 0.3F, 1.0F);
/*  166 */   private float arrowScale; private float arrowScaleTimer = 0.0F;
/*      */   private float arrowX;
/*      */   private float arrowY;
/*  169 */   private static final float ARROW_TARGET_SCALE = 1.2F; private static final int TARGET_ARROW_W = 256; public boolean drawDisabledUI = false;
/*  170 */   public static final float HOVER_CARD_Y_POSITION = 210.0F * Settings.scale;
/*  171 */   public boolean endTurnQueued = false;
/*      */   private static final int SEGMENTS = 20;
/*  173 */   private Vector2[] points = new Vector2[20];
/*      */   private Vector2 controlPoint;
/*  175 */   private boolean renderCorpse = false;
/*      */   
/*      */ 
/*  178 */   private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("AbstractPlayer");
/*  179 */   public static final String BLOCKED_STRING = uiStrings.TEXT[0];
/*  180 */   public static final String IRONCLAD_TITLE_STRING = uiStrings.TEXT[1];
/*  181 */   public static final String SILENT_TITLE_STRING = uiStrings.TEXT[2];
/*  182 */   public static final String DEFECT_TITLE_STRING = uiStrings.TEXT[3];
/*      */   
/*      */   public AbstractPlayer(String name, PlayerClass setClass) {
/*  185 */     this.name = name;
/*  186 */     this.title = getTitle(setClass);
/*  187 */     this.drawX = (Settings.WIDTH * 0.25F);
/*  188 */     this.drawY = AbstractDungeon.floorY;
/*  189 */     this.chosenClass = setClass;
/*  190 */     this.isPlayer = true;
/*  191 */     initializeStarterDeck();
/*  192 */     initializeStarterRelics(setClass);
/*      */     
/*  194 */     if (AbstractDungeon.ascensionLevel >= 11) {
/*  195 */       this.potionSlots -= 1;
/*      */     }
/*      */     
/*  198 */     this.potions.clear();
/*  199 */     for (int i = 0; i < this.potionSlots; i++) {
/*  200 */       this.potions.add(new PotionSlot(i));
/*      */     }
/*      */     
/*  203 */     for (int i = 0; i < this.points.length; i++) {
/*  204 */       this.points[i] = new Vector2();
/*      */     }
/*      */   }
/*      */   
/*      */   public static String getTitle(PlayerClass plyrClass) {
/*  209 */     switch (plyrClass) {
/*      */     case IRONCLAD: 
/*  211 */       return IRONCLAD_TITLE_STRING;
/*      */     case THE_SILENT: 
/*  213 */       return SILENT_TITLE_STRING;
/*      */     case DEFECT: 
/*  215 */       return DEFECT_TITLE_STRING;
/*      */     }
/*  217 */     return "NOT SET, getTitle()";
/*      */   }
/*      */   
/*      */   public void adjustPotionPositions() {
/*  221 */     for (int i = 0; i < this.potions.size() - 1; i++) {
/*  222 */       ((AbstractPotion)this.potions.get(i)).adjustPosition(i);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void initializeClass(String imgUrl, String shoulder2ImgUrl, String shouldImgUrl, String corpseImgUrl, CharSelectInfo info, float hb_x, float hb_y, float hb_w, float hb_h, EnergyManager energy)
/*      */   {
/*  238 */     this.img = ImageMaster.loadImage(imgUrl);
/*  239 */     if (this.img != null) {
/*  240 */       this.atlas = null;
/*      */     }
/*      */     
/*  243 */     this.shoulderImg = ImageMaster.loadImage(shouldImgUrl);
/*  244 */     this.shoulder2Img = ImageMaster.loadImage(shoulder2ImgUrl);
/*  245 */     this.corpseImg = ImageMaster.loadImage(corpseImgUrl);
/*      */     
/*  247 */     this.maxHealth = info.maxHp;
/*  248 */     this.currentHealth = info.currentHp;
/*  249 */     this.masterMaxOrbs = info.maxOrbs;
/*  250 */     this.energy = energy;
/*  251 */     this.masterHandSize = info.cardDraw;
/*  252 */     this.gameHandSize = this.masterHandSize;
/*  253 */     this.gold = info.gold;
/*  254 */     this.displayGold = this.gold;
/*  255 */     this.hb_h = (hb_h * Settings.scale);
/*  256 */     this.hb_w = (hb_w * Settings.scale);
/*  257 */     this.hb_x = (hb_x * Settings.scale);
/*  258 */     this.hb_y = (hb_y * Settings.scale);
/*  259 */     this.hb = new Hitbox(this.hb_w, this.hb_h);
/*  260 */     this.healthHb = new Hitbox(this.hb.width, 72.0F * Settings.scale);
/*  261 */     refreshHitboxLocation();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void initializeStarterDeck()
/*      */   {
/*  270 */     ArrayList<String> cards = null;
/*  271 */     switch (this.chosenClass) {
/*      */     case IRONCLAD: 
/*  273 */       cards = Ironclad.getStartingDeck();
/*  274 */       break;
/*      */     case THE_SILENT: 
/*  276 */       cards = TheSilent.getStartingDeck();
/*  277 */       break;
/*      */     case DEFECT: 
/*  279 */       cards = Defect.getStartingDeck();
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  284 */     boolean addBaseCards = true;
/*      */     
/*  286 */     if (((Boolean)DailyMods.cardMods.get("Insanity")).booleanValue()) {
/*  287 */       addBaseCards = false;
/*  288 */       for (int i = 0; i < 50; i++) {
/*  289 */         this.masterDeck.addToTop(
/*  290 */           CardLibrary.getColorSpecificCard(this.chosenClass, AbstractDungeon.cardRandomRng).makeCopy());
/*      */       }
/*      */     }
/*      */     
/*  294 */     if (((Boolean)DailyMods.cardMods.get("Draft")).booleanValue()) {
/*  295 */       addBaseCards = false;
/*      */     }
/*      */     CardGroup group;
/*  298 */     if (((Boolean)DailyMods.cardMods.get("Shiny")).booleanValue()) {
/*  299 */       addBaseCards = false;
/*  300 */       group = CardLibrary.getEachRare(this.chosenClass);
/*  301 */       for (AbstractCard c : group.group) {
/*  302 */         this.masterDeck.addToTop(c);
/*  303 */         UnlockTracker.markCardAsSeen(c.cardID);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  308 */     if (addBaseCards) {
/*  309 */       for (String s : cards) {
/*  310 */         this.masterDeck.addToTop(CardLibrary.getCard(this.chosenClass, s).makeCopy());
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   protected void initializeStarterRelics(PlayerClass chosenClass)
/*      */   {
/*  319 */     ArrayList<String> relics = null;
/*      */     
/*  321 */     switch (chosenClass) {
/*      */     case IRONCLAD: 
/*  323 */       relics = Ironclad.getStartingRelics();
/*  324 */       break;
/*      */     case THE_SILENT: 
/*  326 */       relics = TheSilent.getStartingRelics();
/*  327 */       break;
/*      */     case DEFECT: 
/*  329 */       relics = Defect.getStartingRelics();
/*      */     }
/*      */     
/*      */     
/*  333 */     if (((Boolean)DailyMods.negativeMods.get("Cursed Run")).booleanValue()) {
/*  334 */       relics.clear();
/*  335 */       relics.add("Cursed Key");
/*  336 */       relics.add("Darkstone Periapt");
/*  337 */       relics.add("Du-Vu Doll");
/*      */     }
/*      */     
/*  340 */     int index = 0;
/*  341 */     for (String s : relics) {
/*  342 */       RelicLibrary.getRelic(s).makeCopy().instantObtain(this, index, false);
/*  343 */       index++;
/*      */     }
/*      */     
/*  346 */     AbstractDungeon.relicsToRemoveOnStart.addAll(relics);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void combatUpdate()
/*      */   {
/*  353 */     if (this.cardInUse != null) {
/*  354 */       this.cardInUse.update();
/*      */     }
/*      */     
/*  357 */     this.limbo.update();
/*  358 */     this.exhaustPile.update();
/*  359 */     for (AbstractPower p : this.powers) {
/*  360 */       p.updateParticles();
/*      */     }
/*  362 */     for (AbstractOrb o : this.orbs) {
/*  363 */       o.update();
/*      */     }
/*      */   }
/*      */   
/*      */   public void update() {
/*  368 */     updateControllerInput();
/*  369 */     this.hb.update();
/*  370 */     updateHealthBar();
/*  371 */     updatePowers();
/*  372 */     this.healthHb.update();
/*  373 */     updateReticle();
/*  374 */     this.tint.update();
/*  375 */     for (AbstractOrb o : this.orbs) {
/*  376 */       o.updateAnimation();
/*      */     }
/*  378 */     updateEscapeAnimation();
/*      */   }
/*      */   
/*      */   private void updateControllerInput() {
/*  382 */     if ((!Settings.isControllerMode) || (AbstractDungeon.topPanel.selectPotionMode)) {
/*  383 */       return;
/*      */     }
/*      */     
/*  386 */     boolean anyHovered = false;
/*  387 */     boolean orbHovered = false;
/*  388 */     int orbIndex = 0;
/*  389 */     for (Iterator localIterator = AbstractDungeon.player.orbs.iterator(); localIterator.hasNext();) { o = (AbstractOrb)localIterator.next();
/*  390 */       if (o.hb.hovered) {
/*  391 */         orbHovered = true;
/*  392 */         break;
/*      */       }
/*  394 */       orbIndex++;
/*      */     }
/*      */     AbstractOrb o;
/*  397 */     if ((orbHovered) && ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed()))) {
/*  398 */       CInputActionSet.up.unpress();
/*  399 */       CInputActionSet.altUp.unpress();
/*      */       
/*  401 */       this.inspectMode = false;
/*  402 */       this.inspectHb = null;
/*  403 */       orbHovered = false;
/*  404 */       this.viewingRelics = true;
/*  405 */       if (!this.blights.isEmpty()) {
/*  406 */         CInputHelper.setCursor(((AbstractBlight)this.blights.get(0)).hb);
/*      */       } else {
/*  408 */         CInputHelper.setCursor(((AbstractRelic)this.relics.get(0)).hb);
/*      */       }
/*  410 */     } else if ((orbHovered) && ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed()))) {
/*  411 */       orbIndex++;
/*  412 */       if (orbIndex > AbstractDungeon.player.orbs.size() - 1) {
/*  413 */         orbIndex = 0;
/*      */       }
/*  415 */       this.inspectHb = ((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb;
/*  416 */       Gdx.input.setCursorPosition(
/*  417 */         (int)((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb.cX, Settings.HEIGHT - 
/*  418 */         (int)((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb.cY);
/*  419 */     } else if ((orbHovered) && ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed()))) {
/*  420 */       orbIndex--;
/*  421 */       if (orbIndex < 0) {
/*  422 */         orbIndex = AbstractDungeon.player.orbs.size() - 1;
/*      */       }
/*  424 */       this.inspectHb = ((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb;
/*  425 */       Gdx.input.setCursorPosition(
/*  426 */         (int)((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb.cX, Settings.HEIGHT - 
/*  427 */         (int)((AbstractOrb)AbstractDungeon.player.orbs.get(orbIndex)).hb.cY);
/*  428 */     } else if ((this.inspectMode) && ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed()))) {
/*  429 */       if (orbHovered) {
/*  430 */         this.inspectHb = AbstractDungeon.player.hb;
/*  431 */         CInputHelper.setCursor(this.inspectHb);
/*      */       } else {
/*  433 */         this.inspectMode = false;
/*  434 */         this.inspectHb = null;
/*  435 */         if (!AbstractDungeon.player.hand.isEmpty()) {
/*  436 */           this.hoveredCard = ((AbstractCard)AbstractDungeon.player.hand.group.get(0));
/*  437 */           hoverCardInHand(this.hoveredCard);
/*  438 */           this.keyboardCardIndex = 0;
/*      */         }
/*      */       }
/*  441 */     } else if ((!this.inspectMode) && ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/*  442 */       (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) && (!AbstractDungeon.isScreenUp))
/*      */     {
/*  444 */       if (Gdx.input.getX() < Settings.WIDTH / 2.0F) {
/*  445 */         this.inspectHb = AbstractDungeon.player.hb;
/*      */       }
/*  447 */       else if (!AbstractDungeon.getMonsters().monsters.isEmpty()) {
/*  448 */         Object hbs = new ArrayList();
/*  449 */         for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/*  450 */           if ((!m.isDying) && (!m.isEscaping)) {
/*  451 */             ((ArrayList)hbs).add(m.hb);
/*      */           }
/*      */         }
/*  454 */         if (!((ArrayList)hbs).isEmpty()) {
/*  455 */           this.inspectHb = ((Hitbox)((ArrayList)hbs).get(0));
/*      */         } else {
/*  457 */           this.inspectHb = AbstractDungeon.player.hb;
/*      */         }
/*      */       } else {
/*  460 */         this.inspectHb = AbstractDungeon.player.hb;
/*      */       }
/*      */       
/*      */ 
/*  464 */       Gdx.input.setCursorPosition((int)this.inspectHb.cX, Settings.HEIGHT - (int)this.inspectHb.cY);
/*  465 */       this.inspectMode = true;
/*  466 */       AbstractDungeon.player.releaseCard(); } else { int index;
/*  467 */       if ((this.inspectMode) && ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) && 
/*  468 */         (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT)) {
/*  469 */         Object hbs = new ArrayList();
/*      */         
/*  471 */         ((ArrayList)hbs).add(AbstractDungeon.player.hb);
/*  472 */         for (o = AbstractDungeon.getMonsters().monsters.iterator(); o.hasNext();) { m = (AbstractMonster)o.next();
/*  473 */           if ((!m.isDying) && (!m.isEscaping)) {
/*  474 */             ((ArrayList)hbs).add(m.hb);
/*      */           }
/*      */         }
/*      */         AbstractMonster m;
/*  478 */         index = 0;
/*  479 */         for (Hitbox h : (ArrayList)hbs) {
/*  480 */           h.update();
/*  481 */           if (h.hovered) {
/*  482 */             anyHovered = true;
/*  483 */             break;
/*      */           }
/*  485 */           index++;
/*      */         }
/*      */         
/*  488 */         if (!anyHovered) {
/*  489 */           Gdx.input.setCursorPosition((int)((Hitbox)((ArrayList)hbs).get(0)).cX, Settings.HEIGHT - (int)((Hitbox)((ArrayList)hbs).get(0)).cY);
/*  490 */           this.inspectHb = ((Hitbox)((ArrayList)hbs).get(0));
/*      */         } else {
/*  492 */           index++;
/*  493 */           if (index > ((ArrayList)hbs).size() - 1) {
/*  494 */             index = 0;
/*      */           }
/*  496 */           Gdx.input.setCursorPosition((int)((Hitbox)((ArrayList)hbs).get(index)).cX, Settings.HEIGHT - (int)((Hitbox)((ArrayList)hbs).get(index)).cY);
/*  497 */           this.inspectHb = ((Hitbox)((ArrayList)hbs).get(index));
/*      */         }
/*      */         
/*  500 */         this.inspectMode = true;
/*  501 */         AbstractDungeon.player.releaseCard();
/*  502 */       } else if ((this.inspectMode) && ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) && 
/*  503 */         (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT)) {
/*  504 */         Object hbs = new ArrayList();
/*      */         
/*  506 */         ((ArrayList)hbs).add(AbstractDungeon.player.hb);
/*  507 */         for (index = AbstractDungeon.getMonsters().monsters.iterator(); index.hasNext();) { m = (AbstractMonster)index.next();
/*  508 */           if ((!m.isDying) && (!m.isEscaping)) {
/*  509 */             ((ArrayList)hbs).add(m.hb);
/*      */           }
/*      */         }
/*      */         AbstractMonster m;
/*  513 */         int index = 0;
/*  514 */         for (Hitbox h : (ArrayList)hbs) {
/*  515 */           if (h.hovered) {
/*  516 */             anyHovered = true;
/*  517 */             break;
/*      */           }
/*  519 */           index++;
/*      */         }
/*      */         
/*  522 */         if (!anyHovered) {
/*  523 */           Gdx.input.setCursorPosition(
/*  524 */             (int)((Hitbox)((ArrayList)hbs).get(((ArrayList)hbs).size() - 1)).cX, Settings.HEIGHT - 
/*  525 */             (int)((Hitbox)((ArrayList)hbs).get(((ArrayList)hbs).size() - 1)).cY);
/*  526 */           this.inspectHb = ((Hitbox)((ArrayList)hbs).get(((ArrayList)hbs).size() - 1));
/*      */         } else {
/*  528 */           index--;
/*  529 */           if (index < 0) {
/*  530 */             index = ((ArrayList)hbs).size() - 1;
/*      */           }
/*  532 */           Gdx.input.setCursorPosition((int)((Hitbox)((ArrayList)hbs).get(index)).cX, Settings.HEIGHT - (int)((Hitbox)((ArrayList)hbs).get(index)).cY);
/*  533 */           this.inspectHb = ((Hitbox)((ArrayList)hbs).get(index));
/*      */         }
/*      */         
/*  536 */         this.inspectMode = true;
/*  537 */         AbstractDungeon.player.releaseCard();
/*  538 */       } else if ((this.inspectMode) && ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) && 
/*  539 */         (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT))
/*      */       {
/*  541 */         CInputActionSet.up.unpress();
/*  542 */         CInputActionSet.altUp.unpress();
/*      */         
/*  544 */         if ((!orbHovered) && (!AbstractDungeon.player.orbs.isEmpty())) {
/*  545 */           Gdx.input.setCursorPosition(
/*  546 */             (int)((AbstractOrb)AbstractDungeon.player.orbs.get(0)).hb.cX, Settings.HEIGHT - 
/*  547 */             (int)((AbstractOrb)AbstractDungeon.player.orbs.get(0)).hb.cY);
/*  548 */           this.inspectHb = ((AbstractOrb)AbstractDungeon.player.orbs.get(0)).hb;
/*      */         } else {
/*  550 */           this.inspectMode = false;
/*  551 */           this.inspectHb = null;
/*  552 */           this.viewingRelics = true;
/*      */           
/*  554 */           if (!this.blights.isEmpty()) {
/*  555 */             CInputHelper.setCursor(((AbstractBlight)this.blights.get(0)).hb);
/*      */           } else
/*  557 */             CInputHelper.setCursor(((AbstractRelic)this.relics.get(0)).hb);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private static enum RHoverType {
/*  564 */     RELIC,  BLIGHT;
/*      */     
/*      */     private RHoverType() {} }
/*      */   
/*  568 */   public void updateViewRelicControls() { int index = 0;
/*  569 */     boolean anyHovered = false;
/*  570 */     RHoverType type = RHoverType.RELIC;
/*      */     
/*  572 */     for (AbstractRelic r : this.relics) {
/*  573 */       if (r.hb.hovered) {
/*  574 */         anyHovered = true;
/*  575 */         break;
/*      */       }
/*  577 */       index++;
/*      */     }
/*      */     
/*  580 */     if (!anyHovered) {
/*  581 */       index = 0;
/*  582 */       for (AbstractBlight b : this.blights) {
/*  583 */         if (b.hb.hovered) {
/*  584 */           anyHovered = true;
/*  585 */           type = RHoverType.BLIGHT;
/*  586 */           break;
/*      */         }
/*  588 */         index++;
/*      */       }
/*      */     }
/*      */     
/*  592 */     if (!anyHovered) {
/*  593 */       CInputHelper.setCursor(((AbstractRelic)this.relics.get(0)).hb);
/*      */     }
/*  595 */     else if ((CInputActionSet.left.isJustPressed()) || (CInputActionSet.altLeft.isJustPressed())) {
/*  596 */       index--;
/*  597 */       if (type == RHoverType.RELIC) {
/*  598 */         if (index < AbstractRelic.relicPage * 25) {
/*  599 */           AbstractRelic.relicPage -= 1;
/*      */           
/*  601 */           if (AbstractRelic.relicPage < 0) {
/*  602 */             if (AbstractDungeon.player.relics.size() <= 25) {
/*  603 */               AbstractRelic.relicPage = 0;
/*      */             } else {
/*  605 */               AbstractRelic.relicPage = AbstractDungeon.player.relics.size() / 25;
/*      */               
/*  607 */               AbstractDungeon.topPanel.adjustRelicHbs();
/*      */             }
/*  609 */             index = AbstractDungeon.player.relics.size() - 1;
/*      */           } else {
/*  611 */             index = (AbstractRelic.relicPage + 1) * 25 - 1;
/*  612 */             AbstractDungeon.topPanel.adjustRelicHbs();
/*      */           }
/*      */         }
/*  615 */         CInputHelper.setCursor(((AbstractRelic)this.relics.get(index)).hb);
/*      */       } else {
/*  617 */         if (index < 0) {
/*  618 */           index = this.blights.size() - 1;
/*      */         }
/*  620 */         CInputHelper.setCursor(((AbstractBlight)this.blights.get(index)).hb);
/*      */       }
/*  622 */     } else if ((CInputActionSet.right.isJustPressed()) || (CInputActionSet.altRight.isJustPressed())) {
/*  623 */       index++;
/*  624 */       if (type == RHoverType.RELIC) {
/*  625 */         if (index > this.relics.size() - 1) {
/*  626 */           index = 0;
/*  627 */         } else if (index > (AbstractRelic.relicPage + 1) * 25 - 1) {
/*  628 */           AbstractRelic.relicPage += 1;
/*  629 */           AbstractDungeon.topPanel.adjustRelicHbs();
/*  630 */           index = AbstractRelic.relicPage * 25;
/*      */         }
/*  632 */         CInputHelper.setCursor(((AbstractRelic)this.relics.get(index)).hb);
/*      */       } else {
/*  634 */         if (index > this.blights.size() - 1) {
/*  635 */           index = 0;
/*      */         }
/*  637 */         CInputHelper.setCursor(((AbstractBlight)this.blights.get(index)).hb);
/*      */       }
/*  639 */     } else if ((CInputActionSet.up.isJustPressed()) || (CInputActionSet.altUp.isJustPressed())) {
/*  640 */       CInputActionSet.up.unpress();
/*  641 */       if (type == RHoverType.RELIC) {
/*  642 */         this.viewingRelics = false;
/*  643 */         AbstractDungeon.topPanel.selectPotionMode = true;
/*  644 */         CInputHelper.setCursor(((AbstractPotion)this.potions.get(0)).hb);
/*      */       } else {
/*  646 */         CInputHelper.setCursor(((AbstractRelic)this.relics.get(0)).hb);
/*      */       }
/*  648 */     } else if (CInputActionSet.cancel.isJustPressed()) {
/*  649 */       this.viewingRelics = false;
/*  650 */       Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/*      */ 
/*      */     }
/*  653 */     else if ((CInputActionSet.down.isJustPressed()) || (CInputActionSet.altDown.isJustPressed())) {
/*  654 */       if (type == RHoverType.RELIC) {
/*  655 */         if (this.blights.isEmpty()) {
/*  656 */           CInputActionSet.down.unpress();
/*  657 */           CInputActionSet.altDown.unpress();
/*  658 */           this.viewingRelics = false;
/*  659 */           this.inspectMode = true;
/*  660 */           if (!AbstractDungeon.player.orbs.isEmpty()) {
/*  661 */             this.inspectHb = ((AbstractOrb)AbstractDungeon.player.orbs.get(0)).hb;
/*      */           } else {
/*  663 */             this.inspectHb = AbstractDungeon.player.hb;
/*      */           }
/*  665 */           CInputHelper.setCursor(this.inspectHb);
/*      */         } else {
/*  667 */           CInputHelper.setCursor(((AbstractBlight)this.blights.get(0)).hb);
/*      */         }
/*      */       } else {
/*  670 */         CInputActionSet.down.unpress();
/*  671 */         CInputActionSet.altDown.unpress();
/*  672 */         this.viewingRelics = false;
/*  673 */         this.inspectMode = true;
/*  674 */         if (!AbstractDungeon.player.orbs.isEmpty()) {
/*  675 */           this.inspectHb = ((AbstractOrb)AbstractDungeon.player.orbs.get(0)).hb;
/*      */         } else {
/*  677 */           this.inspectHb = AbstractDungeon.player.hb;
/*      */         }
/*  679 */         CInputHelper.setCursor(this.inspectHb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void loseGold(int goldAmount)
/*      */   {
/*  688 */     if ((AbstractDungeon.getCurrRoom() instanceof ShopRoom)) {
/*  689 */       for (AbstractRelic r : this.relics) {
/*  690 */         r.onSpendGold();
/*      */       }
/*      */     }
/*  693 */     if ((!(AbstractDungeon.getCurrRoom() instanceof ShopRoom)) && 
/*  694 */       (AbstractDungeon.getCurrRoom().phase != AbstractRoom.RoomPhase.COMBAT)) {
/*  695 */       CardCrawlGame.sound.play("EVENT_PURCHASE");
/*      */     }
/*  697 */     if (goldAmount > 0) {
/*  698 */       this.gold -= goldAmount;
/*  699 */       if (this.gold < 0) {
/*  700 */         this.gold = 0;
/*      */       }
/*  702 */       for (AbstractRelic r : this.relics) {
/*  703 */         r.onLoseGold();
/*      */       }
/*      */     } else {
/*  706 */       logger.info("NEGATIVE MONEY???");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void gainGold(int amount)
/*      */   {
/*  713 */     if (hasRelic("Ectoplasm")) {
/*  714 */       getRelic("Ectoplasm").flash();
/*  715 */       return;
/*      */     }
/*      */     
/*  718 */     if (amount <= 0) {
/*  719 */       logger.info("NEGATIVE MONEY???");
/*      */     } else {
/*  721 */       CardCrawlGame.goldGained += amount;
/*  722 */       this.gold += amount;
/*  723 */       for (AbstractRelic r : this.relics) {
/*  724 */         r.onGainGold();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void playDeathAnimation() {
/*  730 */     this.img = this.corpseImg;
/*  731 */     this.renderCorpse = true;
/*      */   }
/*      */   
/*      */   public boolean isCursed() {
/*  735 */     boolean cursed = false;
/*  736 */     for (AbstractCard c : this.masterDeck.group) {
/*  737 */       if ((c.type == AbstractCard.CardType.CURSE) && (c.cardID != "Necronomicurse") && (c.cardID != "AscendersBane")) {
/*  738 */         cursed = true;
/*      */       }
/*      */     }
/*  741 */     return cursed;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void updateInput()
/*      */   {
/*  748 */     if (this.viewingRelics) {
/*  749 */       return;
/*      */     }
/*      */     
/*  752 */     if (this.hoverEnemyWaitTimer > 0.0F) {
/*  753 */       this.hoverEnemyWaitTimer -= Gdx.graphics.getDeltaTime();
/*      */     }
/*      */     
/*  756 */     if (this.inSingleTargetMode) {
/*  757 */       updateSingleTargetInput();
/*      */     }
/*      */     else {
/*  760 */       int y = InputHelper.mY;
/*      */       
/*  762 */       boolean hMonster = false;
/*  763 */       for (Iterator localIterator1 = AbstractDungeon.getCurrRoom().monsters.monsters.iterator(); localIterator1.hasNext();) { m = (AbstractMonster)localIterator1.next();
/*  764 */         m.hb.update();
/*  765 */         if ((m.hb.hovered) && (!m.isDying) && (!m.isEscaping) && (m.currentHealth > 0)) {
/*  766 */           hMonster = true;
/*  767 */           break;
/*      */         }
/*      */       }
/*      */       AbstractMonster m;
/*  771 */       boolean tmp = this.isHoveringDropZone;
/*  772 */       this.isHoveringDropZone = (((y <= this.hoverStartLine) && (y <= 300.0F * Settings.scale)) || ((y < Settings.CARD_DROP_END_Y) || (hMonster)));
/*      */       
/*      */       int tmpShowCount;
/*  775 */       if ((!tmp) && (this.isHoveringDropZone) && (this.isDraggingCard)) {
/*  776 */         this.hoveredCard.flash(Color.SKY.cpy());
/*  777 */         if (this.hoveredCard.showEvokeValue) {
/*  778 */           if (this.hoveredCard.showEvokeOrbCount == 0) {
/*  779 */             for (AbstractOrb o : this.orbs) {
/*  780 */               o.showEvokeValue();
/*      */             }
/*      */           } else {
/*  783 */             tmpShowCount = this.hoveredCard.showEvokeOrbCount;
/*  784 */             int emptyCount = 0;
/*  785 */             for (AbstractOrb o : this.orbs) {
/*  786 */               if ((o instanceof EmptyOrbSlot)) {
/*  787 */                 emptyCount++;
/*      */               }
/*      */             }
/*  790 */             tmpShowCount -= emptyCount;
/*  791 */             if (tmpShowCount > 0) {
/*  792 */               for (AbstractOrb o : this.orbs) {
/*  793 */                 o.showEvokeValue();
/*  794 */                 tmpShowCount--;
/*  795 */                 if (tmpShowCount <= 0) {
/*      */                   break;
/*      */                 }
/*      */               }
/*      */             }
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*  804 */       if ((this.isDraggingCard) && (this.isHoveringDropZone) && (this.hoveredCard != null)) {
/*  805 */         this.passedHesitationLine = true;
/*      */       }
/*      */       
/*      */ 
/*  809 */       if ((this.isDraggingCard) && ((y < this.hoverStartLine - 100.0F * Settings.scale) || (y < 50.0F * Settings.scale)) && (this.passedHesitationLine))
/*      */       {
/*  811 */         releaseCard();
/*  812 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*      */       }
/*      */       
/*  815 */       updateFullKeyboardCardSelection();
/*      */       
/*  817 */       if ((this.isInKeyboardMode) && (!AbstractDungeon.topPanel.selectPotionMode) && (AbstractDungeon.topPanel.potionUi.isHidden) && (!AbstractDungeon.topPanel.potionUi.targetMode) && (!AbstractDungeon.isScreenUp))
/*      */       {
/*      */ 
/*  820 */         if (this.toHover != null) {
/*  821 */           releaseCard();
/*  822 */           this.hoveredCard = this.toHover;
/*  823 */           this.toHover = null;
/*      */           
/*  825 */           this.isHoveringCard = true;
/*  826 */           this.hoveredCard.current_y = HOVER_CARD_Y_POSITION;
/*  827 */           this.hoveredCard.target_y = HOVER_CARD_Y_POSITION;
/*  828 */           this.hoveredCard.setAngle(0.0F, true);
/*  829 */           this.hand.hoverCardPush(this.hoveredCard);
/*      */         }
/*      */         
/*      */       }
/*  833 */       else if ((this.hoveredCard == null) && (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.NONE) && (!AbstractDungeon.topPanel.selectPotionMode))
/*      */       {
/*  835 */         this.isHoveringCard = false;
/*      */         
/*      */ 
/*  838 */         if (this.toHover != null) {
/*  839 */           this.hoveredCard = this.toHover;
/*  840 */           this.toHover = null;
/*      */         } else {
/*  842 */           this.hoveredCard = this.hand.getHoveredCard();
/*      */         }
/*  844 */         if (this.hoveredCard != null) {
/*  845 */           this.isHoveringCard = true;
/*  846 */           this.hoveredCard.current_y = HOVER_CARD_Y_POSITION;
/*  847 */           this.hoveredCard.target_y = HOVER_CARD_Y_POSITION;
/*  848 */           this.hoveredCard.setAngle(0.0F, true);
/*  849 */           this.hand.hoverCardPush(this.hoveredCard);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*  854 */       if (this.hoveredCard != null) {
/*  855 */         this.hoveredCard.drawScale = 1.0F;
/*  856 */         this.hoveredCard.targetDrawScale = 1.0F;
/*      */       }
/*      */       
/*      */ 
/*  860 */       if ((!this.isDraggingCard) && 
/*  861 */         (hasRelic("Necronomicon"))) {
/*  862 */         getRelic("Necronomicon").stopPulse();
/*      */       }
/*      */       
/*      */ 
/*  866 */       if (!this.endTurnQueued)
/*      */       {
/*  868 */         if ((!AbstractDungeon.actionManager.turnHasEnded) && 
/*  869 */           (clickAndDragCards())) {
/*  870 */           return;
/*      */         }
/*      */         
/*      */ 
/*  874 */         if (!this.isInKeyboardMode)
/*      */         {
/*  876 */           if ((this.isHoveringCard) && (this.hoveredCard != null) && (!this.hoveredCard.isHoveredInHand(1.0F)))
/*      */           {
/*      */ 
/*  879 */             for (int i = 0; i < this.hand.group.size(); i++) {
/*  880 */               if ((this.hand.group.get(i) == this.hoveredCard) && (i != 0) && 
/*  881 */                 (((AbstractCard)this.hand.group.get(i - 1)).isHoveredInHand(1.0F))) {
/*  882 */                 this.toHover = ((AbstractCard)this.hand.group.get(i - 1));
/*  883 */                 break;
/*      */               }
/*      */             }
/*      */             
/*  887 */             releaseCard();
/*      */           }
/*      */         }
/*      */         
/*  891 */         if (this.hoveredCard != null) {
/*  892 */           this.hoveredCard.updateHoverLogic();
/*      */         }
/*      */       }
/*  895 */       else if ((AbstractDungeon.actionManager.cardQueue.isEmpty()) && (!AbstractDungeon.actionManager.hasControl)) {
/*  896 */         this.endTurnQueued = false;
/*  897 */         this.isEndingTurn = true;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateSingleTargetInput()
/*      */   {
/*      */     AbstractCard card;
/*  905 */     if (this.isInKeyboardMode) {
/*  906 */       if ((InputActionSet.releaseCard.isJustPressed()) || (CInputActionSet.cancel.isJustPressed())) {
/*  907 */         card = this.hoveredCard;
/*  908 */         this.inSingleTargetMode = false;
/*  909 */         this.hoveredMonster = null;
/*  910 */         hoverCardInHand(card);
/*      */       } else {
/*  912 */         updateTargetArrowWithKeyboard(false);
/*      */       }
/*      */     } else {
/*  915 */       this.hoveredMonster = null;
/*  916 */       for (AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters) {
/*  917 */         m.hb.update();
/*  918 */         if ((m.hb.hovered) && (!m.isDying) && (!m.isEscaping) && (m.currentHealth > 0)) {
/*  919 */           this.hoveredMonster = m;
/*  920 */           break;
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  926 */     if ((AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) || (InputHelper.justClickedRight) || (InputHelper.mY < this.hoverStartLine - 100.0F * Settings.scale) || (InputHelper.mY < 50.0F * Settings.scale))
/*      */     {
/*  928 */       releaseCard();
/*  929 */       CardCrawlGame.sound.play("UI_CLICK_2");
/*  930 */       this.isUsingClickDragControl = false;
/*  931 */       this.inSingleTargetMode = false;
/*  932 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*  933 */       this.hoveredMonster = null;
/*  934 */       return;
/*      */     }
/*      */     
/*      */ 
/*  938 */     AbstractCard cardFromHotkey = InputHelper.getCardSelectedByHotkey(this.hand);
/*  939 */     if ((cardFromHotkey != null) && (!isCardQueued(cardFromHotkey))) {
/*  940 */       boolean isSameCard = cardFromHotkey == this.hoveredCard;
/*      */       
/*  942 */       releaseCard();
/*  943 */       this.hoveredMonster = null;
/*  944 */       if (isSameCard) {
/*  945 */         com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*      */       }
/*      */       else {
/*  948 */         this.hoveredCard = cardFromHotkey;
/*  949 */         this.hoveredCard.setAngle(0.0F, false);
/*  950 */         this.isUsingClickDragControl = true;
/*  951 */         this.isDraggingCard = true;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  956 */     if ((InputHelper.justClickedLeft) || (InputActionSet.confirm.isJustPressed()) || 
/*  957 */       (CInputActionSet.select.isJustPressed()))
/*      */     {
/*  959 */       InputHelper.justClickedLeft = false;
/*      */       
/*      */ 
/*  962 */       if (this.hoveredMonster == null) {
/*  963 */         CardCrawlGame.sound.play("UI_CLICK_1");
/*  964 */         return;
/*      */       }
/*  966 */       if (this.hoveredCard.canUse(this, this.hoveredMonster)) {
/*  967 */         playCard();
/*      */       } else {
/*  969 */         AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, this.hoveredCard.cantUseMessage, true));
/*      */         
/*  971 */         energyTip(this.hoveredCard);
/*  972 */         releaseCard();
/*      */       }
/*      */       
/*      */ 
/*  976 */       this.isUsingClickDragControl = false;
/*  977 */       this.inSingleTargetMode = false;
/*  978 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*  979 */       this.hoveredMonster = null;
/*  980 */       return;
/*      */     }
/*      */     
/*      */ 
/*  984 */     if ((!this.isUsingClickDragControl) && (InputHelper.justReleasedClickLeft) && (this.hoveredMonster != null)) {
/*  985 */       if (this.hoveredCard.canUse(this, this.hoveredMonster)) {
/*  986 */         playCard();
/*      */       } else {
/*  988 */         AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, this.hoveredCard.cantUseMessage, true));
/*      */         
/*  990 */         energyTip(this.hoveredCard);
/*  991 */         releaseCard();
/*      */       }
/*      */       
/*  994 */       this.inSingleTargetMode = false;
/*  995 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*  996 */       this.hoveredMonster = null;
/*  997 */       return;
/*      */     }
/*      */   }
/*      */   
/*      */   private boolean isCardQueued(AbstractCard card) {
/* 1002 */     for (CardQueueItem item : AbstractDungeon.actionManager.cardQueue) {
/* 1003 */       if (item.card == card) {
/* 1004 */         return true;
/*      */       }
/*      */     }
/* 1007 */     return false;
/*      */   }
/*      */   
/*      */   private void energyTip(AbstractCard cardToCheck)
/*      */   {
/* 1012 */     int availableEnergy = EnergyPanel.totalCount;
/* 1013 */     if (cardToCheck.cost > availableEnergy)
/*      */     {
/* 1015 */       if (!((Boolean)TipTracker.tips.get("ENERGY_USE_TIP")).booleanValue()) {
/* 1016 */         TipTracker.energyUseCounter += 1;
/* 1017 */         if (TipTracker.energyUseCounter >= 2) {
/* 1018 */           AbstractDungeon.ftue = new FtueTip(LABEL[1], MSG[1], 330.0F * Settings.scale, 400.0F * Settings.scale, FtueTip.TipType.ENERGY);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1024 */           TipTracker.neverShowAgain("ENERGY_USE_TIP");
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/* 1030 */   private boolean isInKeyboardMode = false;
/* 1031 */   private boolean skipMouseModeOnce = false;
/* 1032 */   private int keyboardCardIndex = -1;
/* 1033 */   public static ArrayList<String> customMods = null;
/*      */   
/*      */   private boolean updateFullKeyboardCardSelection() {
/* 1036 */     if ((Settings.isControllerMode) || (InputActionSet.left.isJustPressed()) || (InputActionSet.right.isJustPressed()) || 
/* 1037 */       (InputActionSet.confirm.isJustPressed())) {
/* 1038 */       this.isInKeyboardMode = true;
/* 1039 */       this.skipMouseModeOnce = true;
/* 1040 */       com.megacrit.cardcrawl.core.GameCursor.hidden = true;
/*      */     }
/*      */     
/* 1043 */     if ((this.isInKeyboardMode) && (InputHelper.didMoveMouse())) {
/* 1044 */       if (this.skipMouseModeOnce) {
/* 1045 */         this.skipMouseModeOnce = false;
/*      */       } else {
/* 1047 */         this.isInKeyboardMode = false;
/* 1048 */         com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*      */       }
/*      */     }
/*      */     
/* 1052 */     if ((!this.isInKeyboardMode) || (this.hand.isEmpty()) || (this.inspectMode)) {
/* 1053 */       return false;
/*      */     }
/*      */     
/* 1056 */     if (this.keyboardCardIndex == -2) {
/* 1057 */       if ((InputActionSet.left.isJustPressed()) || (CInputActionSet.left.isJustPressed()) || 
/* 1058 */         (CInputActionSet.altLeft.isJustPressed())) {
/* 1059 */         this.keyboardCardIndex = (this.hand.size() - 1);
/* 1060 */       } else if ((InputActionSet.right.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/* 1061 */         (CInputActionSet.altRight.isJustPressed())) {
/* 1062 */         this.keyboardCardIndex = 0;
/*      */       }
/* 1064 */       return false;
/*      */     }
/* 1066 */     if ((InputActionSet.left.isJustPressed()) || (CInputActionSet.left.isJustPressed()) || 
/* 1067 */       (CInputActionSet.altLeft.isJustPressed())) {
/* 1068 */       this.keyboardCardIndex -= 1;
/* 1069 */     } else if ((InputActionSet.right.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/* 1070 */       (CInputActionSet.altRight.isJustPressed())) {
/* 1071 */       this.keyboardCardIndex += 1;
/*      */     }
/*      */     
/*      */ 
/* 1075 */     this.keyboardCardIndex = ((this.keyboardCardIndex + this.hand.size()) % this.hand.size());
/*      */     
/* 1077 */     if ((!AbstractDungeon.topPanel.selectPotionMode) && (AbstractDungeon.topPanel.potionUi.isHidden) && (!AbstractDungeon.topPanel.potionUi.targetMode))
/*      */     {
/* 1079 */       AbstractCard card = (AbstractCard)this.hand.group.get(this.keyboardCardIndex);
/* 1080 */       if (card != this.hoveredCard) {
/* 1081 */         hoverCardInHand(card);
/* 1082 */         return true;
/*      */       }
/*      */     }
/*      */     
/* 1086 */     return false;
/*      */   }
/*      */   
/*      */   private void hoverCardInHand(AbstractCard card) {
/* 1090 */     this.toHover = card;
/* 1091 */     if ((card != null) && (!this.inspectMode)) {
/* 1092 */       Gdx.input.setCursorPosition((int)card.hb.cX, (int)(Settings.HEIGHT - HOVER_CARD_Y_POSITION));
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateTargetArrowWithKeyboard(boolean autoTargetFirst) {
/* 1097 */     int directionIndex = 0;
/*      */     
/* 1099 */     if (autoTargetFirst) {
/* 1100 */       directionIndex++;
/*      */     }
/* 1102 */     if ((InputActionSet.left.isJustPressed()) || (CInputActionSet.left.isJustPressed()) || 
/* 1103 */       (CInputActionSet.altLeft.isJustPressed())) {
/* 1104 */       directionIndex--;
/*      */     }
/* 1106 */     if ((InputActionSet.right.isJustPressed()) || (CInputActionSet.right.isJustPressed()) || 
/* 1107 */       (CInputActionSet.altRight.isJustPressed())) {
/* 1108 */       directionIndex++;
/*      */     }
/*      */     
/* 1111 */     if (directionIndex != 0)
/*      */     {
/*      */ 
/* 1114 */       ArrayList<AbstractMonster> prefiltered = AbstractDungeon.getCurrRoom().monsters.monsters;
/*      */       
/* 1116 */       ArrayList<AbstractMonster> sortedMonsters = new ArrayList(AbstractDungeon.getCurrRoom().monsters.monsters);
/*      */       
/* 1118 */       for (AbstractMonster mons : prefiltered) {
/* 1119 */         if (mons.isDying) {
/* 1120 */           sortedMonsters.remove(mons);
/*      */         }
/*      */       }
/*      */       
/* 1124 */       sortedMonsters.sort(AbstractMonster.sortByHitbox);
/*      */       
/* 1126 */       if (sortedMonsters.isEmpty()) {
/* 1127 */         return;
/*      */       }
/*      */       
/*      */ 
/* 1131 */       for (AbstractMonster m : sortedMonsters) {
/* 1132 */         if (m.hb.hovered) {
/* 1133 */           this.hoveredMonster = m;
/* 1134 */           break;
/*      */         }
/*      */       }
/*      */       
/* 1138 */       AbstractMonster newTarget = null;
/* 1139 */       if (this.hoveredMonster == null)
/*      */       {
/* 1141 */         if (directionIndex == 1)
/*      */         {
/* 1143 */           newTarget = (AbstractMonster)sortedMonsters.get(0);
/*      */         }
/*      */         else {
/* 1146 */           newTarget = (AbstractMonster)sortedMonsters.get(sortedMonsters.size() - 1);
/*      */         }
/*      */       }
/*      */       else {
/* 1150 */         int currentTargetIndex = sortedMonsters.indexOf(this.hoveredMonster);
/* 1151 */         int newTargetIndex = currentTargetIndex + directionIndex;
/* 1152 */         newTargetIndex = (newTargetIndex + sortedMonsters.size()) % sortedMonsters.size();
/* 1153 */         newTarget = (AbstractMonster)sortedMonsters.get(newTargetIndex);
/*      */       }
/*      */       
/* 1156 */       if (newTarget != null) {
/* 1157 */         Hitbox target = newTarget.hb;
/* 1158 */         Gdx.input.setCursorPosition((int)target.cX, Settings.HEIGHT - (int)target.cY);
/* 1159 */         this.hoveredMonster = newTarget;
/* 1160 */         this.isUsingClickDragControl = true;
/* 1161 */         this.isDraggingCard = true;
/*      */       }
/*      */       
/* 1164 */       if (this.hoveredMonster.halfDead) {
/* 1165 */         this.hoveredMonster = null;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderCardHotKeyText(SpriteBatch sb) {
/* 1171 */     int index = 0;
/* 1172 */     for (AbstractCard card : this.hand.group) {
/* 1173 */       if (index < InputActionSet.selectCardActions.length) {
/* 1174 */         float width = AbstractCard.IMG_WIDTH * card.drawScale / 2.0F;
/* 1175 */         float height = AbstractCard.IMG_HEIGHT * card.drawScale / 2.0F;
/*      */         
/* 1177 */         float topOfCard = card.current_y + height;
/* 1178 */         float textSpacing = 50.0F * Settings.scale;
/* 1179 */         float textY = topOfCard + textSpacing;
/*      */         
/* 1181 */         float sin = (float)Math.sin(card.angle / 180.0F * 3.141592653589793D);
/* 1182 */         float xOffset = sin * width;
/*      */         
/* 1184 */         FontHelper.renderFontCentered(sb, FontHelper.buttonLabelFont, InputActionSet.selectCardActions[index]
/*      */         
/*      */ 
/* 1187 */           .getKeyString(), card.current_x - xOffset, textY, Settings.CREAM_COLOR);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 1192 */       index++;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private boolean clickAndDragCards()
/*      */   {
/* 1201 */     boolean simulateRightClickDrop = false;
/* 1202 */     AbstractCard cardFromHotkey = InputHelper.getCardSelectedByHotkey(this.hand);
/* 1203 */     if ((cardFromHotkey != null) && (!isCardQueued(cardFromHotkey))) {
/* 1204 */       if (this.isDraggingCard) {
/* 1205 */         simulateRightClickDrop = cardFromHotkey == this.hoveredCard;
/* 1206 */         CardCrawlGame.sound.play("UI_CLICK_2");
/* 1207 */         releaseCard();
/*      */       }
/*      */       
/* 1210 */       if (!simulateRightClickDrop) {
/* 1211 */         manuallySelectCard(cardFromHotkey);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1216 */     if ((CInputActionSet.select.isJustPressed()) && (this.hoveredCard != null) && (!isCardQueued(this.hoveredCard)) && (!this.isDraggingCard))
/*      */     {
/* 1218 */       manuallySelectCard(this.hoveredCard);
/* 1219 */       if (this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) {
/* 1220 */         updateTargetArrowWithKeyboard(true);
/*      */       } else {
/* 1222 */         Gdx.input.setCursorPosition(Settings.WIDTH / 2, Settings.HEIGHT / 2);
/*      */       }
/* 1224 */       return true;
/*      */     }
/*      */     
/*      */ 
/* 1228 */     if ((InputHelper.justClickedLeft) && (this.isHoveringCard) && (!this.isDraggingCard))
/*      */     {
/* 1230 */       this.hoverStartLine = (InputHelper.mY + 140.0F * Settings.scale);
/* 1231 */       InputHelper.justClickedLeft = false;
/*      */       
/* 1233 */       if (this.hoveredCard != null) {
/* 1234 */         CardCrawlGame.sound.play("CARD_OBTAIN");
/* 1235 */         this.isDraggingCard = true;
/* 1236 */         this.passedHesitationLine = false;
/* 1237 */         this.hoveredCard.targetDrawScale = 0.7F;
/* 1238 */         return true;
/*      */       }
/*      */     }
/*      */     
/* 1242 */     if (InputHelper.isMouseDown) {
/* 1243 */       this.clickDragTimer += Gdx.graphics.getDeltaTime();
/*      */     } else {
/* 1245 */       this.clickDragTimer = 0.0F;
/*      */     }
/*      */     
/*      */ 
/* 1249 */     if (((InputHelper.justClickedLeft) || (InputActionSet.confirm.isJustPressed()) || 
/* 1250 */       (CInputActionSet.select.isJustPressed())) && (this.isUsingClickDragControl))
/*      */     {
/* 1252 */       if ((InputHelper.justClickedRight) || (simulateRightClickDrop)) {
/* 1253 */         CardCrawlGame.sound.play("UI_CLICK_2");
/* 1254 */         releaseCard();
/* 1255 */         return true;
/*      */       }
/*      */       
/* 1258 */       InputHelper.justClickedLeft = false;
/*      */       
/*      */ 
/* 1261 */       if ((this.isHoveringDropZone) && (this.hoveredCard.canUse(this, null)) && (this.hoveredCard.target != AbstractCard.CardTarget.ENEMY) && (this.hoveredCard.target != AbstractCard.CardTarget.SELF_AND_ENEMY))
/*      */       {
/* 1263 */         playCard();
/*      */       } else {
/* 1265 */         CardCrawlGame.sound.play("CARD_OBTAIN");
/* 1266 */         releaseCard();
/*      */       }
/* 1268 */       this.isUsingClickDragControl = false;
/* 1269 */       return true;
/*      */     }
/*      */     
/*      */ 
/* 1273 */     if (this.isInKeyboardMode) {
/* 1274 */       if ((InputActionSet.releaseCard.isJustPressed()) || (CInputActionSet.cancel.isJustPressed())) {
/* 1275 */         hoverCardInHand(this.hoveredCard);
/* 1276 */       } else if (((InputActionSet.confirm.isJustPressed()) || (CInputActionSet.select.isJustPressed())) && (this.hoveredCard != null))
/*      */       {
/*      */ 
/* 1279 */         manuallySelectCard(this.hoveredCard);
/* 1280 */         if (this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) {
/* 1281 */           updateTargetArrowWithKeyboard(true);
/*      */         } else {
/* 1283 */           Gdx.input.setCursorPosition(10, Settings.HEIGHT / 2);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1289 */     if ((this.isDraggingCard) && ((InputHelper.isMouseDown) || (this.isUsingClickDragControl))) {
/* 1290 */       if ((InputHelper.justClickedRight) || (simulateRightClickDrop)) {
/* 1291 */         CardCrawlGame.sound.play("UI_CLICK_2");
/* 1292 */         releaseCard();
/* 1293 */         return true;
/*      */       }
/*      */       
/* 1296 */       this.hoveredCard.target_x = InputHelper.mX;
/* 1297 */       this.hoveredCard.target_y = InputHelper.mY;
/*      */       
/*      */ 
/* 1300 */       if ((!this.hoveredCard.hasEnoughEnergy()) && (this.isHoveringDropZone)) {
/* 1301 */         AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, this.hoveredCard.cantUseMessage, true));
/*      */         
/* 1303 */         energyTip(this.hoveredCard);
/* 1304 */         releaseCard();
/* 1305 */         CardCrawlGame.sound.play("CARD_REJECT");
/* 1306 */         return true; }
/* 1307 */       if ((this.isHoveringDropZone) && ((this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) || (this.hoveredCard.target == AbstractCard.CardTarget.SELF_AND_ENEMY)))
/*      */       {
/* 1309 */         this.inSingleTargetMode = true;
/* 1310 */         this.arrowX = InputHelper.mX;
/* 1311 */         this.arrowY = InputHelper.mY;
/* 1312 */         com.megacrit.cardcrawl.core.GameCursor.hidden = true;
/* 1313 */         this.hoveredCard.untip();
/* 1314 */         this.hand.refreshHandLayout();
/* 1315 */         this.hoveredCard.target_y = (AbstractCard.IMG_HEIGHT * 0.75F / 2.5F);
/* 1316 */         this.hoveredCard.target_x = (Settings.WIDTH / 2.0F);
/* 1317 */         this.isDraggingCard = false;
/*      */       }
/* 1319 */       return true;
/*      */     }
/*      */     
/* 1322 */     if ((this.isDraggingCard) && (InputHelper.justReleasedClickLeft)) {
/* 1323 */       if (this.isHoveringDropZone) {
/* 1324 */         if ((this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) || (this.hoveredCard.target == AbstractCard.CardTarget.SELF_AND_ENEMY)) {
/* 1325 */           this.inSingleTargetMode = true;
/* 1326 */           this.arrowX = InputHelper.mX;
/* 1327 */           this.arrowY = InputHelper.mY;
/* 1328 */           com.megacrit.cardcrawl.core.GameCursor.hidden = true;
/* 1329 */           this.hoveredCard.untip();
/* 1330 */           this.hand.refreshHandLayout();
/* 1331 */           this.hoveredCard.target_y = (AbstractCard.IMG_HEIGHT * 0.75F / 2.5F);
/* 1332 */           this.hoveredCard.target_x = (Settings.WIDTH / 2.0F);
/* 1333 */           this.isDraggingCard = false;
/* 1334 */           return true; }
/* 1335 */         if (this.hoveredCard.canUse(this, null)) {
/* 1336 */           playCard();
/* 1337 */           return true;
/*      */         }
/* 1339 */         AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, this.hoveredCard.cantUseMessage, true));
/*      */         
/* 1341 */         energyTip(this.hoveredCard);
/* 1342 */         releaseCard();
/* 1343 */         return true;
/*      */       }
/*      */       
/*      */ 
/* 1347 */       if (this.clickDragTimer < 0.4F) {
/* 1348 */         this.isUsingClickDragControl = true;
/* 1349 */         return true;
/*      */       }
/* 1351 */       if (AbstractDungeon.actionManager.currentAction == null) {
/* 1352 */         releaseCard();
/* 1353 */         logger.info("DERP?");
/* 1354 */         CardCrawlGame.sound.play("CARD_OBTAIN");
/* 1355 */         return true;
/*      */       }
/*      */     }
/*      */     
/* 1359 */     return false;
/*      */   }
/*      */   
/*      */   private void manuallySelectCard(AbstractCard card) {
/* 1363 */     this.hoveredCard = card;
/* 1364 */     this.hoveredCard.setAngle(0.0F, false);
/* 1365 */     this.isUsingClickDragControl = true;
/* 1366 */     this.isDraggingCard = true;
/*      */     
/* 1368 */     this.hoveredCard.flash(Color.SKY.cpy());
/* 1369 */     int tmpShowCount; if (this.hoveredCard.showEvokeValue) {
/* 1370 */       if (this.hoveredCard.showEvokeOrbCount == 0) {
/* 1371 */         for (AbstractOrb o : this.orbs) {
/* 1372 */           o.showEvokeValue();
/*      */         }
/*      */       } else {
/* 1375 */         tmpShowCount = this.hoveredCard.showEvokeOrbCount;
/* 1376 */         int emptyCount = 0;
/* 1377 */         for (AbstractOrb o : this.orbs) {
/* 1378 */           if ((o instanceof EmptyOrbSlot)) {
/* 1379 */             emptyCount++;
/*      */           }
/*      */         }
/* 1382 */         tmpShowCount -= emptyCount;
/* 1383 */         if (tmpShowCount > 0) {
/* 1384 */           for (AbstractOrb o : this.orbs) {
/* 1385 */             o.showEvokeValue();
/* 1386 */             tmpShowCount--;
/* 1387 */             if (tmpShowCount <= 0) {
/*      */               break;
/*      */             }
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void playCard() {
/* 1397 */     InputHelper.justClickedLeft = false;
/* 1398 */     this.hoverEnemyWaitTimer = 1.0F;
/* 1399 */     this.hoveredCard.unhover();
/*      */     
/* 1401 */     if ((this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) || (this.hoveredCard.target == AbstractCard.CardTarget.SELF_AND_ENEMY)) {
/* 1402 */       AbstractDungeon.actionManager.cardQueue.add(new CardQueueItem(this.hoveredCard, this.hoveredMonster));
/*      */     } else {
/* 1404 */       AbstractDungeon.actionManager.cardQueue.add(new CardQueueItem(this.hoveredCard, null));
/*      */     }
/*      */     
/* 1407 */     this.isUsingClickDragControl = false;
/* 1408 */     this.hoveredCard = null;
/* 1409 */     this.isDraggingCard = false;
/*      */   }
/*      */   
/*      */   public void releaseCard() {
/* 1413 */     for (AbstractOrb o : this.orbs) {
/* 1414 */       o.hideEvokeValues();
/*      */     }
/* 1416 */     this.passedHesitationLine = false;
/* 1417 */     InputHelper.justClickedLeft = false;
/* 1418 */     InputHelper.justReleasedClickLeft = false;
/* 1419 */     InputHelper.isMouseDown = false;
/* 1420 */     this.inSingleTargetMode = false;
/* 1421 */     if (!this.isInKeyboardMode) {
/* 1422 */       com.megacrit.cardcrawl.core.GameCursor.hidden = false;
/*      */     }
/* 1424 */     this.isUsingClickDragControl = false;
/* 1425 */     this.isHoveringDropZone = false;
/* 1426 */     this.isDraggingCard = false;
/* 1427 */     this.isHoveringCard = false;
/*      */     
/* 1429 */     if (this.hoveredCard != null) {
/* 1430 */       if (this.hoveredCard.canUse(this, null)) {
/* 1431 */         this.hoveredCard.beginGlowing();
/*      */       }
/* 1433 */       this.hoveredCard.untip();
/* 1434 */       this.hoveredCard.hoverTimer = 0.25F;
/* 1435 */       this.hoveredCard.unhover();
/*      */     }
/* 1437 */     this.hoveredCard = null;
/* 1438 */     this.hand.refreshHandLayout();
/*      */   }
/*      */   
/*      */   public void onCardDrawOrDiscard() {
/* 1442 */     for (AbstractPower p : this.powers) {
/* 1443 */       p.onDrawOrDiscard();
/*      */     }
/*      */     
/* 1446 */     for (AbstractRelic r : this.relics) {
/* 1447 */       r.onDrawOrDiscard();
/*      */     }
/*      */     
/*      */ 
/* 1451 */     if (hasPower("Corruption")) {
/* 1452 */       for (AbstractCard c : this.hand.group) {
/* 1453 */         if ((c.type == AbstractCard.CardType.SKILL) && (c.costForTurn != 0)) {
/* 1454 */           c.modifyCostForCombat(-9);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1459 */     this.hand.applyPowers();
/* 1460 */     this.hand.glowCheck();
/*      */   }
/*      */   
/*      */   public void useCard(AbstractCard c, AbstractMonster monster, int energyOnUse) {
/* 1464 */     if (c.type == AbstractCard.CardType.ATTACK) {
/* 1465 */       useFastAttackAnimation();
/*      */     }
/*      */     
/* 1468 */     c.calculateCardDamage(monster);
/* 1469 */     c.use(this, monster);
/*      */     
/* 1471 */     AbstractDungeon.actionManager.addToBottom(new UseCardAction(c, monster));
/* 1472 */     if (!c.dontTriggerOnUseCard) {
/* 1473 */       this.hand.triggerOnOtherCardPlayed(c);
/*      */     }
/* 1475 */     this.hand.removeCard(c);
/* 1476 */     this.cardInUse = c;
/*      */     
/* 1478 */     c.target_x = (Settings.WIDTH / 2);
/* 1479 */     c.target_y = (Settings.HEIGHT / 2);
/*      */     
/* 1481 */     if ((c.cost > 0) && (!c.freeToPlayOnce) && (
/* 1482 */       (!hasPower("Corruption")) || (c.type != AbstractCard.CardType.SKILL))) {
/* 1483 */       this.energy.use(c.costForTurn);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1488 */     if ((!this.hand.canUseAnyCard()) && (!this.endTurnQueued)) {
/* 1489 */       AbstractDungeon.overlayMenu.endTurnButton.isGlowing = true;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void damage(DamageInfo info)
/*      */   {
/* 1496 */     int damageAmount = info.output;
/* 1497 */     boolean hadBlock = true;
/* 1498 */     if (this.currentBlock == 0) {
/* 1499 */       hadBlock = false;
/*      */     }
/*      */     
/* 1502 */     if (damageAmount < 0) {
/* 1503 */       damageAmount = 0;
/* 1504 */       logger.info("LINE 726: AbstractPlayer.java: Why is damageAmount less than 0?");
/*      */     }
/*      */     
/* 1507 */     if ((damageAmount > 1) && (hasPower("IntangiblePlayer"))) {
/* 1508 */       damageAmount = 1;
/*      */     }
/*      */     
/*      */ 
/* 1512 */     damageAmount = decrementBlock(info, damageAmount);
/*      */     
/* 1514 */     if (info.owner == this) {
/* 1515 */       for (AbstractRelic r : this.relics) {
/* 1516 */         r.onAttack(info, damageAmount, this);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1521 */     if (info.owner != null) {
/* 1522 */       for (AbstractPower p : info.owner.powers) {
/* 1523 */         p.onAttack(info, damageAmount, this);
/*      */       }
/* 1525 */       for (AbstractPower p : this.powers) {
/* 1526 */         damageAmount = p.onAttacked(info, damageAmount);
/*      */       }
/* 1528 */       for (AbstractRelic r : this.relics) {
/* 1529 */         damageAmount = r.onAttacked(info, damageAmount);
/*      */       }
/*      */     } else {
/* 1532 */       logger.info("NO OWNER, DON'T TRIGGER POWERS");
/*      */     }
/*      */     
/* 1535 */     if (damageAmount > 0) {
/* 1536 */       for (AbstractPower p : this.powers) {
/* 1537 */         damageAmount = p.onLoseHp(damageAmount);
/*      */       }
/* 1539 */       for (AbstractRelic r : this.relics) {
/* 1540 */         r.onLoseHp(damageAmount);
/*      */       }
/*      */       
/* 1543 */       if (info.owner != this) {
/* 1544 */         useStaggerAnimation();
/*      */       }
/*      */       
/* 1547 */       if (info.type == DamageInfo.DamageType.HP_LOSS) {
/* 1548 */         GameActionManager.hpLossThisCombat += damageAmount;
/*      */       }
/* 1550 */       GameActionManager.damageReceivedThisTurn += damageAmount;
/* 1551 */       GameActionManager.damageReceivedThisCombat += damageAmount;
/* 1552 */       updateCardsOnDamage();
/* 1553 */       this.damagedThisCombat += 1;
/*      */       
/* 1555 */       this.currentHealth -= damageAmount;
/* 1556 */       AbstractDungeon.effectList.add(new StrikeEffect(this, this.hb.cX, this.hb.cY, damageAmount));
/* 1557 */       if (this.currentHealth < 0) {
/* 1558 */         this.currentHealth = 0;
/* 1559 */       } else if (this.currentHealth < this.maxHealth / 4) {
/* 1560 */         AbstractDungeon.topLevelEffects.add(new BorderFlashEffect(new Color(1.0F, 0.1F, 0.05F, 0.0F)));
/*      */       }
/* 1562 */       healthBarUpdatedEvent();
/*      */       
/* 1564 */       if ((this.currentHealth <= this.maxHealth / 2.0F) && 
/* 1565 */         (!this.isBloodied)) {
/* 1566 */         this.isBloodied = true;
/* 1567 */         for (AbstractRelic r : this.relics) {
/* 1568 */           if (r != null) {
/* 1569 */             r.onBloodied();
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 1575 */       if (this.currentHealth < 1) {
/* 1576 */         if (!hasRelic("Mark of the Bloom"))
/*      */         {
/* 1578 */           if (hasPotion("FairyPotion"))
/*      */           {
/* 1580 */             for (AbstractPotion p : this.potions) {
/* 1581 */               if (p.ID.equals("FairyPotion")) {
/* 1582 */                 p.flash();
/* 1583 */                 this.currentHealth = 0;
/* 1584 */                 p.use(AbstractDungeon.player);
/* 1585 */                 AbstractDungeon.topPanel.destroyPotion(p.slot);
/* 1586 */                 return;
/*      */               }
/*      */             }
/* 1589 */           } else if (hasRelic("Lizard Tail"))
/*      */           {
/* 1591 */             if (((LizardTail)getRelic("Lizard Tail")).counter == -1) {
/* 1592 */               this.currentHealth = 0;
/* 1593 */               getRelic("Lizard Tail").onTrigger();
/* 1594 */               return;
/*      */             }
/*      */           }
/*      */         }
/*      */         
/* 1599 */         this.isDead = true;
/* 1600 */         AbstractDungeon.deathScreen = new DeathScreen(AbstractDungeon.getMonsters());
/* 1601 */         this.currentHealth = 0;
/* 1602 */         if (this.currentBlock > 0) {
/* 1603 */           loseBlock();
/* 1604 */           AbstractDungeon.effectList.add(new HbBlockBrokenEffect(this.hb.cX - this.hb.width / 2.0F + BLOCK_ICON_X, this.hb.cY - this.hb.height / 2.0F + BLOCK_ICON_Y));
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1611 */     else if (this.currentBlock > 0) {
/* 1612 */       AbstractDungeon.effectList.add(new BlockedWordEffect(this, this.hb.cX, this.hb.cY, BLOCKED_STRING));
/* 1613 */     } else if (!hadBlock) {
/* 1614 */       AbstractDungeon.effectList.add(new StrikeEffect(this, this.hb.cX, this.hb.cY, 0));
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateCardsOnDamage()
/*      */   {
/* 1620 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 1621 */       for (AbstractCard c : this.hand.group) {
/* 1622 */         c.tookDamage();
/*      */       }
/* 1624 */       for (AbstractCard c : this.discardPile.group) {
/* 1625 */         c.tookDamage();
/*      */       }
/* 1627 */       for (AbstractCard c : this.drawPile.group) {
/* 1628 */         c.tookDamage();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateCardsOnDiscard() {
/* 1634 */     for (AbstractCard c : this.hand.group) {
/* 1635 */       c.didDiscard();
/*      */     }
/* 1637 */     for (AbstractCard c : this.discardPile.group) {
/* 1638 */       c.didDiscard();
/*      */     }
/* 1640 */     for (AbstractCard c : this.drawPile.group) {
/* 1641 */       c.didDiscard();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   public void heal(int healAmount)
/*      */   {
/* 1648 */     super.heal(healAmount);
/* 1649 */     if ((this.currentHealth > this.maxHealth / 2.0F) && 
/* 1650 */       (this.isBloodied)) {
/* 1651 */       this.isBloodied = false;
/* 1652 */       for (AbstractRelic r : this.relics) {
/* 1653 */         if (r != null) {
/* 1654 */           r.onNotBloodied();
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void gainEnergy(int e)
/*      */   {
/* 1662 */     EnergyPanel.addEnergy(e);
/* 1663 */     this.hand.glowCheck();
/*      */   }
/*      */   
/*      */   public void loseEnergy(int e) {
/* 1667 */     EnergyPanel.useEnergy(e);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void preBattlePrep()
/*      */   {
/* 1677 */     if (!((Boolean)TipTracker.tips.get("COMBAT_TIP")).booleanValue()) {
/* 1678 */       AbstractDungeon.ftue = new MultiPageFtue();
/* 1679 */       TipTracker.neverShowAgain("COMBAT_TIP");
/*      */     }
/*      */     
/*      */ 
/* 1683 */     AbstractDungeon.actionManager.clear();
/* 1684 */     this.damagedThisCombat = 0;
/* 1685 */     this.cardsPlayedThisTurn = 0;
/*      */     
/*      */ 
/* 1688 */     this.maxOrbs = 0;
/* 1689 */     this.orbs.clear();
/* 1690 */     increaseMaxOrbSlots(this.masterMaxOrbs, false);
/*      */     
/* 1692 */     this.isBloodied = (this.currentHealth <= this.maxHealth / 2);
/*      */     
/* 1694 */     poisonKillCount = 0;
/*      */     
/* 1696 */     GameActionManager.playerHpLastTurn = this.currentHealth;
/* 1697 */     this.endTurnQueued = false;
/* 1698 */     this.gameHandSize = this.masterHandSize;
/* 1699 */     this.isDraggingCard = false;
/* 1700 */     this.isHoveringDropZone = false;
/* 1701 */     this.hoveredCard = null;
/* 1702 */     this.cardInUse = null;
/* 1703 */     this.drawPile.initializeDeck(this.masterDeck);
/* 1704 */     AbstractDungeon.overlayMenu.endTurnButton.enabled = false;
/* 1705 */     this.hand.clear();
/* 1706 */     this.discardPile.clear();
/* 1707 */     this.exhaustPile.clear();
/* 1708 */     this.energy.prep();
/* 1709 */     this.powers.clear();
/* 1710 */     this.isEndingTurn = false;
/* 1711 */     healthBarUpdatedEvent();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1716 */     if (((Boolean)DailyMods.negativeMods.get("Lethality")).booleanValue()) {
/* 1717 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new StrengthPower(this, 3), 3));
/*      */     }
/*      */     
/*      */ 
/* 1721 */     if (((Boolean)DailyMods.negativeMods.get("Terminal")).booleanValue()) {
/* 1722 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(this, this, new PlatedArmorPower(this, 5), 5));
/*      */     }
/*      */     
/* 1725 */     AbstractDungeon.getCurrRoom().monsters.usePreBattleAction();
/* 1726 */     AbstractDungeon.actionManager.addToTop(new WaitAction(1.0F));
/*      */     
/*      */ 
/* 1729 */     applyPreCombatLogic();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ArrayList<String> getRelicNames()
/*      */   {
/* 1738 */     ArrayList<String> arr = new ArrayList();
/* 1739 */     for (AbstractRelic relic : this.relics) {
/* 1740 */       arr.add(relic.relicId);
/*      */     }
/* 1742 */     return arr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getCircletCount()
/*      */   {
/* 1751 */     int count = 0;
/* 1752 */     int counterSum = 0;
/* 1753 */     for (AbstractRelic relic : this.relics) {
/* 1754 */       if (relic.relicId.equals("Circlet")) {
/* 1755 */         count++;
/* 1756 */         counterSum += relic.counter;
/*      */       }
/*      */     }
/* 1759 */     if (counterSum > 0) {
/* 1760 */       return counterSum;
/*      */     }
/* 1762 */     return count;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void draw(int numCards)
/*      */   {
/* 1773 */     for (int i = 0; i < numCards; i++) { AbstractCard c;
/* 1774 */       int newCost; if (!this.drawPile.isEmpty()) {
/* 1775 */         c = this.drawPile.getTopCard();
/* 1776 */         c.current_x = CardGroup.DRAW_PILE_X;
/* 1777 */         c.current_y = CardGroup.DRAW_PILE_Y;
/* 1778 */         c.setAngle(0.0F, true);
/* 1779 */         c.lighten(false);
/* 1780 */         c.drawScale = 0.12F;
/* 1781 */         c.targetDrawScale = 0.75F;
/*      */         
/*      */ 
/* 1784 */         if ((AbstractDungeon.player.hasPower("Confusion")) && (c.cost > -1) && (c.color != AbstractCard.CardColor.CURSE) && (c.type != AbstractCard.CardType.STATUS))
/*      */         {
/* 1786 */           newCost = AbstractDungeon.cardRandomRng.random(3);
/* 1787 */           if (c.cost != newCost) {
/* 1788 */             c.cost = newCost;
/* 1789 */             c.costForTurn = c.cost;
/* 1790 */             c.isCostModified = true;
/*      */           }
/*      */         }
/*      */         
/*      */ 
/* 1795 */         c.triggerWhenDrawn();
/*      */         
/* 1797 */         this.hand.addToHand(c);
/* 1798 */         this.drawPile.removeTopCard();
/*      */         
/* 1800 */         if ((AbstractDungeon.player.hasPower("Corruption")) && (c.type == AbstractCard.CardType.SKILL)) {
/* 1801 */           c.setCostForTurn(-9);
/*      */         }
/*      */         
/* 1804 */         for (AbstractRelic r : this.relics) {
/* 1805 */           r.onCardDraw(c);
/*      */         }
/*      */       }
/*      */       else {
/* 1809 */         logger.info("ERROR: How did this happen? No cards in draw pile?? Player.java");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void draw()
/*      */   {
/* 1818 */     if (this.hand.size() == 10) {
/* 1819 */       AbstractDungeon.player.createHandIsFullDialog();
/* 1820 */       return;
/*      */     }
/* 1822 */     draw(1);
/* 1823 */     onCardDrawOrDiscard();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void render(SpriteBatch sb)
/*      */   {
/* 1831 */     if (((AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) || 
/* 1832 */       ((AbstractDungeon.getCurrRoom() instanceof MonsterRoom))) && (!this.isDead)) {
/* 1833 */       renderHealth(sb);
/*      */       
/* 1835 */       if (!this.orbs.isEmpty()) {
/* 1836 */         for (AbstractOrb o : this.orbs) {
/* 1837 */           o.render(sb);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1842 */     if (!(AbstractDungeon.getCurrRoom() instanceof RestRoom)) {
/* 1843 */       if (this.damageFlash) {
/* 1844 */         ShaderHelper.setShader(sb, ShaderHelper.Shader.WHITE_SILHOUETTE);
/*      */       }
/*      */       
/* 1847 */       if ((this.atlas == null) || (this.renderCorpse)) {
/* 1848 */         sb.setColor(Color.WHITE);
/* 1849 */         sb.draw(this.img, this.drawX - this.img
/*      */         
/* 1851 */           .getWidth() * Settings.scale / 2.0F + this.animX, this.drawY, this.img
/*      */           
/* 1853 */           .getWidth() * Settings.scale, this.img
/* 1854 */           .getHeight() * Settings.scale, 0, 0, this.img
/*      */           
/*      */ 
/* 1857 */           .getWidth(), this.img
/* 1858 */           .getHeight(), this.flipHorizontal, this.flipVertical);
/*      */       }
/*      */       else
/*      */       {
/* 1862 */         renderPlayerImage(sb);
/*      */       }
/*      */       
/* 1865 */       if (this.damageFlash) {
/* 1866 */         ShaderHelper.setShader(sb, ShaderHelper.Shader.DEFAULT);
/* 1867 */         this.damageFlashFrames -= 1;
/* 1868 */         if (this.damageFlashFrames == 0) {
/* 1869 */           this.damageFlash = false;
/*      */         }
/*      */       }
/*      */       
/* 1873 */       this.hb.render(sb);
/* 1874 */       this.healthHb.render(sb);
/*      */     } else {
/* 1876 */       sb.setColor(Color.WHITE);
/* 1877 */       if (CampfireUI.hidden) {
/* 1878 */         sb.draw(this.shoulder2Img, 0.0F, 0.0F, Settings.WIDTH, 1136.0F * Settings.scale);
/*      */       } else {
/* 1880 */         sb.draw(this.shoulderImg, this.animX, 0.0F, Settings.WIDTH, 1136.0F * Settings.scale);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderPlayerImage(SpriteBatch sb) {
/* 1886 */     if (this.atlas != null) {
/* 1887 */       this.state.update(Gdx.graphics.getDeltaTime());
/* 1888 */       this.state.apply(this.skeleton);
/* 1889 */       this.skeleton.updateWorldTransform();
/* 1890 */       this.skeleton.setPosition(this.drawX + this.animX, this.drawY + this.animY + AbstractDungeon.sceneOffsetY);
/*      */       
/*      */ 
/* 1893 */       this.skeleton.setColor(this.tint.color);
/* 1894 */       this.skeleton.setFlip(this.flipHorizontal, this.flipVertical);
/* 1895 */       sb.end();
/* 1896 */       CardCrawlGame.psb.begin();
/* 1897 */       sr.draw(CardCrawlGame.psb, this.skeleton);
/* 1898 */       CardCrawlGame.psb.end();
/* 1899 */       sb.begin();
/*      */     } else {
/* 1901 */       sb.setColor(Color.WHITE);
/* 1902 */       sb.draw(this.img, this.drawX - this.img
/*      */       
/* 1904 */         .getWidth() * Settings.scale / 2.0F + this.animX, this.drawY, this.img
/*      */         
/* 1906 */         .getWidth() * Settings.scale, this.img
/* 1907 */         .getHeight() * Settings.scale, 0, 0, this.img
/*      */         
/*      */ 
/* 1910 */         .getWidth(), this.img
/* 1911 */         .getHeight(), this.flipHorizontal, this.flipVertical);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderPlayerBattleUi(SpriteBatch sb)
/*      */   {
/* 1922 */     if (((this.hb.hovered) || (this.healthHb.hovered)) && (!AbstractDungeon.isScreenUp)) {
/* 1923 */       renderPowerTips(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderHand(SpriteBatch sb) {
/* 1928 */     if (Settings.SHOW_CARD_HOTKEYS) {
/* 1929 */       renderCardHotKeyText(sb);
/*      */     }
/*      */     
/* 1932 */     if ((this.inspectMode) && 
/* 1933 */       (this.inspectHb != null)) {
/* 1934 */       renderReticle(sb, this.inspectHb);
/*      */     }
/*      */     
/*      */ 
/* 1938 */     if (this.hoveredCard != null) {
/* 1939 */       int aliveMonsters = 0;
/* 1940 */       this.hand.renderHand(sb, this.hoveredCard);
/* 1941 */       this.hoveredCard.renderHoverShadow(sb);
/*      */       
/* 1943 */       if (((this.isDraggingCard) || (this.inSingleTargetMode)) && (this.isHoveringDropZone))
/*      */       {
/* 1945 */         if ((this.isDraggingCard) && (!this.inSingleTargetMode)) {
/* 1946 */           AbstractMonster theMonster = null;
/* 1947 */           for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 1948 */             if ((!m.isDying) && (m.currentHealth > 0)) {
/* 1949 */               aliveMonsters++;
/* 1950 */               theMonster = m;
/*      */             }
/*      */           }
/*      */           
/* 1954 */           if ((aliveMonsters == 1) && (this.hoveredMonster == null)) {
/* 1955 */             this.hoveredCard.calculateCardDamage(theMonster);
/* 1956 */             this.hoveredCard.render(sb);
/* 1957 */             this.hoveredCard.applyPowers();
/*      */           } else {
/* 1959 */             this.hoveredCard.render(sb);
/*      */           }
/*      */         }
/* 1962 */         if (!AbstractDungeon.getCurrRoom().isBattleEnding()) {
/* 1963 */           renderHoverReticle(sb);
/*      */         }
/*      */       }
/*      */       
/* 1967 */       if (this.hoveredMonster != null) {
/* 1968 */         this.hoveredCard.calculateCardDamage(this.hoveredMonster);
/* 1969 */         this.hoveredCard.render(sb);
/* 1970 */         this.hoveredCard.applyPowers();
/* 1971 */       } else if (aliveMonsters != 1) {
/* 1972 */         this.hoveredCard.render(sb);
/*      */       }
/*      */     }
/* 1975 */     else if (AbstractDungeon.screen == AbstractDungeon.CurrentScreen.HAND_SELECT) {
/* 1976 */       this.hand.render(sb);
/*      */     } else {
/* 1978 */       this.hand.renderHand(sb, this.cardInUse);
/*      */     }
/*      */     
/*      */ 
/* 1982 */     if ((this.cardInUse != null) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.HAND_SELECT)) {
/* 1983 */       this.cardInUse.render(sb);
/* 1984 */       if (AbstractDungeon.getCurrRoom().phase != AbstractRoom.RoomPhase.COMBAT) {
/* 1985 */         AbstractDungeon.effectList.add(new PurgeCardEffect(this.cardInUse
/* 1986 */           .makeCopy(), this.cardInUse.current_x, this.cardInUse.current_y));
/* 1987 */         this.cardInUse = null;
/*      */       }
/*      */     }
/*      */     
/* 1991 */     this.limbo.render(sb);
/*      */     
/* 1993 */     if ((this.inSingleTargetMode) && (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT))
/*      */     {
/* 1995 */       if (!AbstractDungeon.getCurrRoom().isBattleEnding()) {
/* 1996 */         renderTargetingUi(sb);
/*      */       }
/*      */     }
/* 1999 */     renderHoverNumberLogic(sb);
/*      */   }
/*      */   
/*      */   private void renderHoverNumberLogic(SpriteBatch sb) {
/* 2003 */     if ((this.isHoveringDropZone) && (this.hoveredCard != null) && (Settings.isInfo))
/*      */     {
/*      */ 
/* 2006 */       if ((this.hoveredCard.target == AbstractCard.CardTarget.ENEMY) && (this.hoveredMonster != null)) {
/* 2007 */         this.hoverTimer = MathHelper.fadeLerpSnap(this.hoverTimer, 1.0F);
/* 2008 */         this.hoveredCard.calculateDamageDisplay(this.hoveredMonster);
/* 2009 */         FontHelper.renderFontCentered(sb, FontHelper.bannerFont, 
/*      */         
/*      */ 
/* 2012 */           Integer.toString(this.hoveredCard.damage), this.hoveredMonster.hb.cX, this.hoveredMonster.hb.cY + this.hoveredMonster.hb.height / 2.0F + 80.0F * Settings.scale + this.hoverTimer * 30.0F * Settings.scale, new Color(1.0F, 0.9F, 0.4F, this.hoverTimer));
/*      */       }
/*      */       else
/*      */       {
/*      */         float tmpY;
/*      */         
/*      */         int i;
/* 2019 */         if (this.hoveredCard.target == AbstractCard.CardTarget.ALL_ENEMY) {
/* 2020 */           this.hoverTimer = MathHelper.fadeLerpSnap(this.hoverTimer, 1.0F);
/* 2021 */           this.hoveredCard.calculateDamageDisplay(null);
/* 2022 */           tmpY = 80.0F * Settings.scale + this.hoverTimer * 30.0F * Settings.scale;
/* 2023 */           i = 0;
/* 2024 */           for (AbstractMonster m : AbstractDungeon.getMonsters().monsters) {
/* 2025 */             FontHelper.renderFontCentered(sb, FontHelper.bannerFont, 
/*      */             
/*      */ 
/* 2028 */               Integer.toString(this.hoveredCard.multiDamage[i]), m.hb.cX, m.hb.cY + m.hb.height / 2.0F + tmpY, new Color(1.0F, 0.9F, 0.4F, this.hoverTimer));
/*      */             
/*      */ 
/*      */ 
/* 2032 */             i++;
/*      */           }
/*      */         } else {
/* 2035 */           this.hoverTimer = MathHelper.fadeLerpSnap(this.hoverTimer, 0.0F);
/*      */         }
/*      */       }
/* 2038 */     } else { this.hoverTimer = MathHelper.fadeLerpSnap(this.hoverTimer, 0.0F);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderTargetingUi(SpriteBatch sb)
/*      */   {
/* 2048 */     this.arrowX = MathHelper.mouseLerpSnap(this.arrowX, InputHelper.mX);
/* 2049 */     this.arrowY = MathHelper.mouseLerpSnap(this.arrowY, InputHelper.mY);
/*      */     
/* 2051 */     this.controlPoint = new Vector2(this.hoveredCard.current_x - (this.arrowX - this.hoveredCard.current_x) / 4.0F, this.arrowY + (this.arrowY - this.hoveredCard.current_y) / 2.0F);
/*      */     
/*      */ 
/*      */ 
/* 2055 */     if (this.hoveredMonster == null) {
/* 2056 */       this.arrowScale = Settings.scale;
/* 2057 */       this.arrowScaleTimer = 0.0F;
/* 2058 */       sb.setColor(Color.WHITE);
/*      */     } else {
/* 2060 */       this.arrowScaleTimer += Gdx.graphics.getDeltaTime();
/* 2061 */       if (this.arrowScaleTimer > 1.0F) {
/* 2062 */         this.arrowScaleTimer = 1.0F;
/*      */       }
/*      */       
/* 2065 */       this.arrowScale = Interpolation.elasticOut.apply(Settings.scale, Settings.scale * 1.2F, this.arrowScaleTimer);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 2070 */       sb.setColor(ARROW_COLOR);
/*      */     }
/*      */     
/*      */ 
/* 2074 */     Vector2 tmp = new Vector2(this.controlPoint.x - this.arrowX, this.controlPoint.y - this.arrowY);
/* 2075 */     tmp.nor();
/*      */     
/* 2077 */     drawCurvedLine(sb, new Vector2(this.hoveredCard.current_x, this.hoveredCard.current_y), new Vector2(this.arrowX, this.arrowY), this.controlPoint);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2082 */     sb.draw(ImageMaster.TARGET_UI_ARROW, this.arrowX - 128.0F, this.arrowY - 128.0F, 128.0F, 128.0F, 256.0F, 256.0F, this.arrowScale, this.arrowScale, tmp
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2092 */       .angle() + 90.0F, 0, 0, 256, 256, false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void drawCurvedLine(SpriteBatch sb, Vector2 start, Vector2 end, Vector2 control)
/*      */   {
/* 2102 */     float radius = 7.0F * Settings.scale;
/*      */     
/* 2104 */     for (int i = 0; i < this.points.length - 1; i++) {
/* 2105 */       this.points[i] = ((Vector2)Bezier.quadratic(this.points[i], i / 20.0F, start, control, end, new Vector2()));
/* 2106 */       radius += 0.4F * Settings.scale;
/*      */       
/*      */       float angle;
/*      */       
/*      */       float angle;
/* 2111 */       if (i != 0) {
/* 2112 */         Vector2 tmp = new Vector2(this.points[(i - 1)].x - this.points[i].x, this.points[(i - 1)].y - this.points[i].y);
/* 2113 */         angle = tmp.nor().angle() + 90.0F;
/*      */       } else {
/* 2115 */         Vector2 tmp = new Vector2(this.controlPoint.x - this.points[i].x, this.controlPoint.y - this.points[i].y);
/* 2116 */         angle = tmp.nor().angle() + 270.0F;
/*      */       }
/*      */       
/* 2119 */       sb.draw(ImageMaster.TARGET_UI_CIRCLE, this.points[i].x - 64.0F, this.points[i].y - 64.0F, 64.0F, 64.0F, 128.0F, 128.0F, radius / 18.0F, radius / 18.0F, angle, 0, 0, 128, 128, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void createHandIsFullDialog()
/*      */   {
/* 2140 */     AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, MSG[2], true));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderHoverReticle(SpriteBatch sb)
/*      */   {
/* 2149 */     switch (this.hoveredCard.target) {
/*      */     case ENEMY: 
/* 2151 */       if ((this.inSingleTargetMode) && (this.hoveredMonster != null)) {
/* 2152 */         this.hoveredMonster.renderReticle(sb);
/*      */       }
/*      */       break;
/*      */     case ALL_ENEMY: 
/* 2156 */       AbstractDungeon.getCurrRoom().monsters.renderReticle(sb);
/* 2157 */       break;
/*      */     case SELF: 
/* 2159 */       renderReticle(sb);
/* 2160 */       break;
/*      */     case SELF_AND_ENEMY: 
/* 2162 */       renderReticle(sb);
/* 2163 */       if ((this.inSingleTargetMode) && (this.hoveredMonster != null)) {
/* 2164 */         this.hoveredMonster.renderReticle(sb);
/*      */       }
/*      */       break;
/*      */     case ALL: 
/* 2168 */       renderReticle(sb);
/* 2169 */       AbstractDungeon.getCurrRoom().monsters.renderReticle(sb);
/* 2170 */       break;
/*      */     case NONE: 
/*      */       break;
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */   public void applyPreCombatLogic()
/*      */   {
/* 2180 */     for (AbstractRelic r : this.relics) {
/* 2181 */       if (r != null) {
/* 2182 */         r.atPreBattle();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfCombatLogic()
/*      */   {
/* 2189 */     for (AbstractRelic r : this.relics) {
/* 2190 */       if (r != null) {
/* 2191 */         r.atBattleStart();
/*      */       }
/*      */     }
/*      */     
/* 2195 */     for (AbstractBlight b : this.blights) {
/* 2196 */       if (b != null) {
/* 2197 */         b.atBattleStart();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfCombatPreDrawLogic()
/*      */   {
/* 2204 */     for (AbstractRelic r : this.relics) {
/* 2205 */       if (r != null) {
/* 2206 */         r.atBattleStartPreDraw();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnRelics()
/*      */   {
/* 2213 */     for (AbstractRelic r : this.relics) {
/* 2214 */       if (r != null) {
/* 2215 */         r.atTurnStart();
/*      */       }
/*      */     }
/*      */     
/* 2219 */     for (AbstractBlight b : this.blights) {
/* 2220 */       if (b != null) {
/* 2221 */         b.atTurnStart();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnPostDrawRelics() {
/* 2227 */     for (AbstractRelic r : this.relics) {
/* 2228 */       if (r != null) {
/* 2229 */         r.atTurnStartPostDraw();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnCards()
/*      */   {
/* 2236 */     for (AbstractCard c : this.drawPile.group) {
/* 2237 */       if (c != null) {
/* 2238 */         c.atTurnStart();
/*      */       }
/*      */     }
/* 2241 */     for (AbstractCard c : this.hand.group) {
/* 2242 */       if (c != null) {
/* 2243 */         c.atTurnStart();
/*      */       }
/*      */     }
/* 2246 */     for (AbstractCard c : this.discardPile.group) {
/* 2247 */       if (c != null) {
/* 2248 */         c.atTurnStart();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void onVictory()
/*      */   {
/* 2257 */     if (!this.isDying) {
/* 2258 */       for (AbstractRelic r : this.relics) {
/* 2259 */         r.onVictory();
/*      */       }
/* 2261 */       for (AbstractBlight b : this.blights) {
/* 2262 */         b.onVictory();
/*      */       }
/* 2264 */       for (AbstractPower p : this.powers) {
/* 2265 */         p.onVictory();
/*      */       }
/*      */     }
/* 2268 */     this.damagedThisCombat = 0;
/*      */   }
/*      */   
/*      */   public static enum PlayerClass {
/* 2272 */     IRONCLAD,  THE_SILENT,  DEFECT;
/*      */     
/*      */ 
/*      */ 
/*      */     private PlayerClass() {}
/*      */   }
/*      */   
/*      */ 
/*      */   public boolean hasRelic(String targetID)
/*      */   {
/* 2282 */     for (AbstractRelic r : this.relics) {
/* 2283 */       if (r.relicId.equals(targetID)) {
/* 2284 */         return true;
/*      */       }
/*      */     }
/* 2287 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean hasBlight(String targetID)
/*      */   {
/* 2297 */     for (AbstractBlight b : this.blights) {
/* 2298 */       if (b.blightID.equals(targetID)) {
/* 2299 */         return true;
/*      */       }
/*      */     }
/* 2302 */     return false;
/*      */   }
/*      */   
/*      */   public boolean hasPotion(String id) {
/* 2306 */     for (AbstractPotion p : this.potions) {
/* 2307 */       if (p.ID.equals(id)) {
/* 2308 */         return true;
/*      */       }
/*      */     }
/* 2311 */     return false;
/*      */   }
/*      */   
/*      */   public boolean hasAnyPotions() {
/* 2315 */     for (AbstractPotion p : this.potions) {
/* 2316 */       if (!(p instanceof PotionSlot)) {
/* 2317 */         return true;
/*      */       }
/*      */     }
/* 2320 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void loseRandomRelics(int amount)
/*      */   {
/* 2330 */     if (amount > this.relics.size()) {
/* 2331 */       for (AbstractRelic r : this.relics) {
/* 2332 */         r.onUnequip();
/*      */       }
/* 2334 */       this.relics.clear();
/* 2335 */       return;
/*      */     }
/*      */     
/*      */ 
/* 2339 */     for (int i = 0; i < amount; i++) {
/* 2340 */       int index = MathUtils.random(0, this.relics.size() - 1);
/* 2341 */       ((AbstractRelic)this.relics.get(index)).onUnequip();
/* 2342 */       this.relics.remove(index);
/*      */     }
/*      */     
/* 2345 */     reorganizeRelics();
/*      */   }
/*      */   
/*      */   public boolean loseRelic(String targetID) {
/* 2349 */     if (!hasRelic(targetID)) {
/* 2350 */       return false;
/*      */     }
/*      */     
/*      */ 
/* 2354 */     AbstractRelic toRemove = null;
/* 2355 */     for (AbstractRelic r : this.relics) {
/* 2356 */       if (r.relicId.equals(targetID)) {
/* 2357 */         r.onUnequip();
/* 2358 */         toRemove = r;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2363 */     if (toRemove == null) {
/* 2364 */       logger.info("WHY WAS RELIC: " + this.name + " NOT FOUND???");
/* 2365 */       return false;
/*      */     }
/*      */     
/*      */ 
/* 2369 */     this.relics.remove(toRemove);
/*      */     
/* 2371 */     reorganizeRelics();
/* 2372 */     return true;
/*      */   }
/*      */   
/*      */   public void reorganizeRelics() {
/* 2376 */     logger.info("Reorganizing relics");
/*      */     
/* 2378 */     ArrayList<AbstractRelic> tmpRelics = new ArrayList();
/* 2379 */     tmpRelics.addAll(this.relics);
/* 2380 */     this.relics.clear();
/*      */     
/*      */ 
/* 2383 */     for (int i = 0; i < tmpRelics.size(); i++) {
/* 2384 */       ((AbstractRelic)tmpRelics.get(i)).reorganizeObtain(this, i, false, tmpRelics.size());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public AbstractRelic getRelic(String targetID)
/*      */   {
/* 2392 */     for (AbstractRelic r : this.relics) {
/* 2393 */       if (r.relicId.equals(targetID)) {
/* 2394 */         return r;
/*      */       }
/*      */     }
/* 2397 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public AbstractBlight getBlight(String targetID)
/*      */   {
/* 2404 */     for (AbstractBlight b : this.blights) {
/* 2405 */       if (b.blightID.equals(targetID)) {
/* 2406 */         return b;
/*      */       }
/*      */     }
/* 2409 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void obtainPotion(int slot, AbstractPotion potionToObtain)
/*      */   {
/* 2419 */     if (slot > this.potionSlots) {
/* 2420 */       return;
/*      */     }
/* 2422 */     this.potions.set(slot, potionToObtain);
/* 2423 */     potionToObtain.setAsObtained(slot);
/*      */   }
/*      */   
/*      */   public boolean obtainPotion(AbstractPotion potionToObtain) {
/* 2427 */     int index = 0;
/* 2428 */     for (AbstractPotion p : this.potions) {
/* 2429 */       if ((p instanceof PotionSlot)) {
/*      */         break;
/*      */       }
/* 2432 */       index++;
/*      */     }
/*      */     
/* 2435 */     if (index < this.potionSlots) {
/* 2436 */       this.potions.set(index, potionToObtain);
/* 2437 */       potionToObtain.setAsObtained(index);
/* 2438 */       potionToObtain.flash();
/* 2439 */       AbstractPotion.playPotionSound();
/* 2440 */       return true;
/*      */     }
/*      */     
/*      */ 
/* 2444 */     logger.info("NOT ENOUGH POTION SLOTS");
/* 2445 */     AbstractDungeon.topPanel.flashRed();
/* 2446 */     return false;
/*      */   }
/*      */   
/*      */   public void renderRelics(SpriteBatch sb) {
/* 2450 */     for (int i = 0; i < this.relics.size(); i++) {
/* 2451 */       if (i / 25 == AbstractRelic.relicPage) {
/* 2452 */         ((AbstractRelic)this.relics.get(i)).renderInTopPanel(sb);
/*      */       }
/*      */     }
/*      */     
/* 2456 */     for (AbstractRelic r : this.relics) {
/* 2457 */       if (r.hb.hovered) {
/* 2458 */         r.renderTip(sb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderBlights(SpriteBatch sb) {
/* 2464 */     for (AbstractBlight b : this.blights) {
/* 2465 */       b.renderInTopPanel(sb);
/*      */     }
/*      */     
/* 2468 */     for (AbstractBlight b : this.blights) {
/* 2469 */       if (b.hb.hovered) {
/* 2470 */         b.renderTip(sb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public static String getClass(PlayerClass c) {
/* 2476 */     switch (c) {
/*      */     case IRONCLAD: 
/* 2478 */       return "The Ironclad";
/*      */     case THE_SILENT: 
/* 2480 */       return "The Silent";
/*      */     case DEFECT: 
/* 2482 */       return "The Defect";
/*      */     }
/* 2484 */     return "Unknown";
/*      */   }
/*      */   
/*      */   public void bottledCardUpgradeCheck(AbstractCard c)
/*      */   {
/* 2489 */     if ((c.inBottleFlame) && (AbstractDungeon.player.hasRelic("Bottled Flame"))) {
/* 2490 */       ((BottledFlame)AbstractDungeon.player.getRelic("Bottled Flame")).setDescriptionAfterLoading();
/*      */     }
/*      */     
/* 2493 */     if ((c.inBottleLightning) && (AbstractDungeon.player.hasRelic("Bottled Lightning"))) {
/* 2494 */       ((BottledLightning)AbstractDungeon.player.getRelic("Bottled Lightning")).setDescriptionAfterLoading();
/*      */     }
/*      */     
/* 2497 */     if ((c.inBottleTornado) && (AbstractDungeon.player.hasRelic("Bottled Tornado"))) {
/* 2498 */       ((BottledTornado)AbstractDungeon.player.getRelic("Bottled Tornado")).setDescriptionAfterLoading();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void triggerEvokeAnimation(int slot)
/*      */   {
/* 2506 */     if (this.maxOrbs <= 0) {
/* 2507 */       return;
/*      */     }
/* 2509 */     ((AbstractOrb)this.orbs.get(slot)).triggerEvokeAnimation();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void evokeOrb()
/*      */   {
/* 2516 */     if ((!this.orbs.isEmpty()) && (!(this.orbs.get(0) instanceof EmptyOrbSlot))) {
/* 2517 */       ((AbstractOrb)this.orbs.get(0)).onEvoke();
/* 2518 */       AbstractOrb orbSlot = new EmptyOrbSlot();
/*      */       
/* 2520 */       for (int i = 1; i < this.orbs.size(); i++) {
/* 2521 */         Collections.swap(this.orbs, i, i - 1);
/*      */       }
/*      */       
/* 2524 */       this.orbs.set(this.orbs.size() - 1, orbSlot);
/*      */       
/* 2526 */       for (int i = 0; i < this.orbs.size(); i++) {
/* 2527 */         ((AbstractOrb)this.orbs.get(i)).setSlot(i, this.maxOrbs);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void evokeNewestOrb()
/*      */   {
/* 2536 */     if ((!this.orbs.isEmpty()) && (!(this.orbs.get(this.orbs.size() - 1) instanceof EmptyOrbSlot))) {
/* 2537 */       ((AbstractOrb)this.orbs.get(this.orbs.size() - 1)).onEvoke();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void evokeWithoutLosingOrb()
/*      */   {
/* 2549 */     if ((!this.orbs.isEmpty()) && (!(this.orbs.get(0) instanceof EmptyOrbSlot))) {
/* 2550 */       ((AbstractOrb)this.orbs.get(0)).onEvoke();
/*      */     }
/*      */   }
/*      */   
/*      */   public void removeNextOrb() {
/* 2555 */     if ((!this.orbs.isEmpty()) && (!(this.orbs.get(0) instanceof EmptyOrbSlot))) {
/* 2556 */       AbstractOrb orbSlot = new EmptyOrbSlot(((AbstractOrb)this.orbs.get(0)).cX, ((AbstractOrb)this.orbs.get(0)).cY);
/*      */       
/* 2558 */       for (int i = 1; i < this.orbs.size(); i++) {
/* 2559 */         Collections.swap(this.orbs, i, i - 1);
/*      */       }
/*      */       
/* 2562 */       this.orbs.set(this.orbs.size() - 1, orbSlot);
/*      */       
/* 2564 */       for (int i = 0; i < this.orbs.size(); i++) {
/* 2565 */         ((AbstractOrb)this.orbs.get(i)).setSlot(i, this.maxOrbs);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public boolean hasEmptyOrb() {
/* 2571 */     if (this.orbs.isEmpty()) {
/* 2572 */       return false;
/*      */     }
/*      */     
/* 2575 */     for (AbstractOrb o : this.orbs) {
/* 2576 */       if ((o instanceof EmptyOrbSlot)) {
/* 2577 */         return true;
/*      */       }
/*      */     }
/*      */     
/* 2581 */     return false;
/*      */   }
/*      */   
/*      */   public boolean hasOrb() {
/* 2585 */     if (this.orbs.isEmpty()) {
/* 2586 */       return false;
/*      */     }
/*      */     
/* 2589 */     if ((this.orbs.get(0) instanceof EmptyOrbSlot)) {
/* 2590 */       return false;
/*      */     }
/*      */     
/* 2593 */     return true;
/*      */   }
/*      */   
/*      */   public int filledOrbCount() {
/* 2597 */     int orbCount = 0;
/* 2598 */     for (AbstractOrb o : AbstractDungeon.player.orbs) {
/* 2599 */       if (!(o instanceof EmptyOrbSlot)) {
/* 2600 */         orbCount++;
/*      */       }
/*      */     }
/* 2603 */     return orbCount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void channelOrb(AbstractOrb orbToSet)
/*      */   {
/* 2610 */     if (this.maxOrbs <= 0) {
/* 2611 */       AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, MSG[4], true));
/* 2612 */       return;
/*      */     }
/*      */     
/* 2615 */     if (this.maxOrbs > 0)
/*      */     {
/* 2617 */       if ((AbstractDungeon.player.hasRelic("Dark Core")) && (!(orbToSet instanceof Dark))) {
/* 2618 */         orbToSet = new Dark();
/*      */       }
/*      */       
/* 2621 */       int index = -1;
/* 2622 */       for (int i = 0; i < this.orbs.size(); i++) {
/* 2623 */         if ((this.orbs.get(i) instanceof EmptyOrbSlot)) {
/* 2624 */           index = i;
/* 2625 */           break;
/*      */         }
/*      */       }
/*      */       
/* 2629 */       if (index != -1) {
/* 2630 */         orbToSet.cX = ((AbstractOrb)this.orbs.get(index)).cX;
/* 2631 */         orbToSet.cY = ((AbstractOrb)this.orbs.get(index)).cY;
/* 2632 */         this.orbs.set(index, orbToSet);
/* 2633 */         ((AbstractOrb)this.orbs.get(index)).setSlot(index, this.maxOrbs);
/* 2634 */         orbToSet.playChannelSFX();
/*      */         
/* 2636 */         for (i = this.powers.iterator(); i.hasNext();) { p = (AbstractPower)i.next();
/* 2637 */           p.onChannel(orbToSet);
/*      */         }
/*      */         AbstractPower p;
/* 2640 */         AbstractDungeon.actionManager.orbsChanneledThisCombat.add(orbToSet);
/*      */         
/*      */ 
/* 2643 */         AbstractDungeon.actionManager.orbsChanneledThisTurn.add(orbToSet);
/* 2644 */         int plasmaCount = 0;
/* 2645 */         for (AbstractOrb o : AbstractDungeon.actionManager.orbsChanneledThisTurn) {
/* 2646 */           if ((o instanceof Plasma)) {
/* 2647 */             plasmaCount++;
/*      */           }
/*      */         }
/* 2650 */         if (plasmaCount == 9) {
/* 2651 */           UnlockTracker.unlockAchievement("NEON");
/*      */         }
/*      */         
/*      */ 
/* 2655 */         orbToSet.applyFocus();
/*      */       } else {
/* 2657 */         AbstractDungeon.actionManager.addToTop(new ChannelAction(orbToSet));
/* 2658 */         AbstractDungeon.actionManager.addToTop(new EvokeOrbAction(1));
/* 2659 */         AbstractDungeon.actionManager.addToTop(new AnimateOrbAction(1));
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void increaseMaxOrbSlots(int amount, boolean playSfx)
/*      */   {
/* 2666 */     if (this.maxOrbs == 10) {
/* 2667 */       AbstractDungeon.effectList.add(new ThoughtBubble(this.dialogX, this.dialogY, 3.0F, MSG[3], true));
/* 2668 */       return;
/*      */     }
/*      */     
/* 2671 */     if (playSfx) {
/* 2672 */       CardCrawlGame.sound.play("ORB_SLOT_GAIN", 0.1F);
/*      */     }
/* 2674 */     this.maxOrbs += amount;
/* 2675 */     for (int i = 0; i < amount; i++) {
/* 2676 */       this.orbs.add(new EmptyOrbSlot());
/*      */     }
/* 2678 */     for (int i = 0; i < this.orbs.size(); i++) {
/* 2679 */       ((AbstractOrb)this.orbs.get(i)).setSlot(i, this.maxOrbs);
/*      */     }
/*      */   }
/*      */   
/*      */   public void decreaseMaxOrbSlots(int amount) {
/* 2684 */     if (this.maxOrbs <= 0) {
/* 2685 */       return;
/*      */     }
/*      */     
/* 2688 */     this.maxOrbs -= amount;
/*      */     
/* 2690 */     if (this.maxOrbs < 0) {
/* 2691 */       this.maxOrbs = 0;
/*      */     }
/*      */     
/* 2694 */     if (!this.orbs.isEmpty()) {
/* 2695 */       this.orbs.remove(this.orbs.size() - 1);
/*      */     }
/*      */     
/* 2698 */     for (int i = 0; i < this.orbs.size(); i++) {
/* 2699 */       ((AbstractOrb)this.orbs.get(i)).setSlot(i, this.maxOrbs);
/*      */     }
/*      */   }
/*      */   
/*      */   public void applyStartOfTurnOrbs() {
/* 2704 */     if (!AbstractDungeon.player.orbs.isEmpty()) {
/* 2705 */       for (AbstractOrb o : this.orbs) {
/* 2706 */         o.onStartOfTurn();
/*      */       }
/*      */       
/* 2709 */       if ((AbstractDungeon.player.hasRelic("Cables")) && (!(AbstractDungeon.player.orbs.get(0) instanceof EmptyOrbSlot)))
/*      */       {
/* 2711 */         ((AbstractOrb)AbstractDungeon.player.orbs.get(0)).onStartOfTurn();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void updateEscapeAnimation()
/*      */   {
/* 2720 */     if (this.escapeTimer != 0.0F) {
/* 2721 */       this.flipHorizontal = true;
/* 2722 */       this.escapeTimer -= Gdx.graphics.getDeltaTime();
/* 2723 */       this.drawX -= Gdx.graphics.getDeltaTime() * 400.0F * Settings.scale;
/*      */     }
/* 2725 */     if (this.escapeTimer < 0.0F) {
/* 2726 */       AbstractDungeon.getCurrRoom().endBattle();
/* 2727 */       this.flipHorizontal = false;
/* 2728 */       this.isEscaping = false;
/* 2729 */       this.escapeTimer = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   public boolean relicsDoneAnimating() {
/* 2734 */     for (AbstractRelic r : this.relics) {
/* 2735 */       if (!r.isDone) {
/* 2736 */         return false;
/*      */       }
/*      */     }
/* 2739 */     return true;
/*      */   }
/*      */   
/*      */   public void resetControllerValues() {
/* 2743 */     if (Settings.isControllerMode) {
/* 2744 */       this.hoveredCard = null;
/* 2745 */       this.inspectMode = false;
/* 2746 */       this.inspectHb = null;
/* 2747 */       this.keyboardCardIndex = -1;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AbstractPotion getRandomPotion()
/*      */   {
/* 2757 */     ArrayList<AbstractPotion> list = new ArrayList();
/* 2758 */     for (AbstractPotion p : this.potions) {
/* 2759 */       if (!(p instanceof PotionSlot)) {
/* 2760 */         list.add(p);
/*      */       }
/*      */     }
/*      */     
/* 2764 */     if (list.isEmpty()) {
/* 2765 */       return null;
/*      */     }
/*      */     
/* 2768 */     Collections.shuffle(list, new java.util.Random(AbstractDungeon.miscRng.randomLong()));
/* 2769 */     return (AbstractPotion)list.get(0);
/*      */   }
/*      */   
/*      */   public void removePotion(AbstractPotion potionOption) {
/* 2773 */     int slot = this.potions.indexOf(potionOption);
/* 2774 */     if (slot >= 0) {
/* 2775 */       this.potions.set(slot, new PotionSlot(slot));
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\characters\AbstractPlayer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */