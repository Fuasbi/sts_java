/*     */ package com.megacrit.cardcrawl.audio;
/*     */ 
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.audio.Music;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import com.megacrit.cardcrawl.random.Random;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class MainMusic
/*     */ {
/*  17 */   private static final Logger logger = LogManager.getLogger(MainMusic.class.getName());
/*     */   
/*     */   private Music music;
/*     */   
/*     */   public String key;
/*     */   private static final String DIR = "audio/music/";
/*     */   private static final String TITLE_BGM = "STS_MenuTheme_NewMix_v1.ogg";
/*     */   private static final String LEVEL_1_1_BGM = "STS_Level1_NewMix_v1.ogg";
/*     */   private static final String LEVEL_1_2_BGM = "STS_Level1-2_v2.ogg";
/*     */   private static final String LEVEL_2_1_BGM = "STS_Level2_NewMix_v1.ogg";
/*     */   private static final String LEVEL_2_2_BGM = "STS_Level2-2_v2.ogg";
/*     */   private static final String LEVEL_3_1_BGM = "STS_Level3_v2.ogg";
/*     */   private static final String LEVEL_3_2_BGM = "STS_Level3-2_v2.ogg";
/*  30 */   public boolean isSilenced = false;
/*  31 */   private float silenceTimer = 0.0F; private float silenceTime = 0.0F;
/*     */   
/*     */   private static final float SILENCE_TIME = 4.0F;
/*     */   private static final float FAST_SILENCE_TIME = 0.25F;
/*     */   private float silenceStartVolume;
/*     */   private static final float FADE_IN_TIME = 4.0F;
/*     */   private static final float FADE_OUT_TIME = 4.0F;
/*  38 */   private float fadeTimer = 0.0F;
/*  39 */   public boolean isFadingOut = false;
/*     */   private float fadeOutStartVolume;
/*  41 */   public boolean isDone = false;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MainMusic(String key)
/*     */   {
/*  49 */     this.key = key;
/*  50 */     this.music = getSong(key);
/*  51 */     this.fadeTimer = 4.0F;
/*  52 */     this.music.setLooping(true);
/*  53 */     this.music.play();
/*  54 */     this.music.setVolume(0.0F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Music getSong(String key)
/*     */   {
/*  64 */     switch (key) {
/*     */     case "Exordium": 
/*  66 */       switch (AbstractDungeon.miscRng.random(1)) {
/*     */       case 0: 
/*  68 */         return newMusic("audio/music/STS_Level1_NewMix_v1.ogg");
/*     */       }
/*  70 */       return newMusic("audio/music/STS_Level1-2_v2.ogg");
/*     */     
/*     */     case "TheCity": 
/*  73 */       switch (AbstractDungeon.miscRng.random(1)) {
/*     */       case 0: 
/*  75 */         return newMusic("audio/music/STS_Level2_NewMix_v1.ogg");
/*     */       }
/*  77 */       return newMusic("audio/music/STS_Level2-2_v2.ogg");
/*     */     
/*     */     case "TheBeyond": 
/*  80 */       switch (AbstractDungeon.miscRng.random(1)) {
/*     */       case 0: 
/*  82 */         return newMusic("audio/music/STS_Level3_v2.ogg");
/*     */       }
/*  84 */       return newMusic("audio/music/STS_Level3-2_v2.ogg");
/*     */     
/*     */     case "MENU": 
/*  87 */       return newMusic("audio/music/STS_MenuTheme_NewMix_v1.ogg");
/*     */     }
/*  89 */     logger.info("NO SUCH MAIN BGM (playing level_1 instead): " + key);
/*  90 */     return newMusic("audio/music/STS_Level1_NewMix_v1.ogg");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Music newMusic(String path)
/*     */   {
/* 101 */     if (Gdx.audio == null) {
/* 102 */       logger.info("WARNING: Gdx.audio is null so no Music instance can be initialized.");
/* 103 */       return new MockMusic();
/*     */     }
/* 105 */     return Gdx.audio.newMusic(Gdx.files.internal(path));
/*     */   }
/*     */   
/*     */   public void updateVolume()
/*     */   {
/* 110 */     if ((!this.isFadingOut) && (!this.isSilenced)) {
/* 111 */       this.music.setVolume(Settings.MUSIC_VOLUME * Settings.MASTER_VOLUME);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void fadeOut()
/*     */   {
/* 119 */     this.isFadingOut = true;
/* 120 */     this.fadeOutStartVolume = this.music.getVolume();
/* 121 */     this.fadeTimer = 4.0F;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void silence()
/*     */   {
/* 128 */     this.isSilenced = true;
/* 129 */     this.silenceTimer = 4.0F;
/* 130 */     this.silenceTime = 4.0F;
/* 131 */     this.silenceStartVolume = this.music.getVolume();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void silenceInstantly()
/*     */   {
/* 138 */     this.isSilenced = true;
/* 139 */     this.silenceTimer = 0.25F;
/* 140 */     this.silenceTime = 0.25F;
/* 141 */     this.silenceStartVolume = this.music.getVolume();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void unsilence()
/*     */   {
/* 148 */     if (this.isSilenced) {
/* 149 */       logger.info("Unsilencing " + this.key);
/* 150 */       this.isSilenced = false;
/* 151 */       this.fadeTimer = 4.0F;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void kill()
/*     */   {
/* 159 */     this.music.dispose();
/* 160 */     this.isDone = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 167 */     if (!this.isFadingOut) {
/* 168 */       updateFadeIn();
/*     */     } else {
/* 170 */       updateFadeOut();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFadeIn()
/*     */   {
/* 178 */     if (!this.isSilenced) {
/* 179 */       this.fadeTimer -= Gdx.graphics.getDeltaTime();
/* 180 */       if (this.fadeTimer < 0.0F) {
/* 181 */         this.fadeTimer = 0.0F;
/*     */       }
/*     */       
/* 184 */       if (!Settings.isBackgrounded) {
/* 185 */         this.music.setVolume(Interpolation.fade
/* 186 */           .apply(0.0F, 1.0F, 1.0F - this.fadeTimer / 4.0F) * (Settings.MUSIC_VOLUME * Settings.MASTER_VOLUME));
/*     */       }
/*     */       else {
/* 189 */         this.music.setVolume(MathHelper.slowColorLerpSnap(this.music.getVolume(), 0.0F));
/*     */       }
/*     */     } else {
/* 192 */       this.silenceTimer -= Gdx.graphics.getDeltaTime();
/* 193 */       if (this.silenceTimer < 0.0F) {
/* 194 */         this.silenceTimer = 0.0F;
/*     */       }
/* 196 */       if (!Settings.isBackgrounded) {
/* 197 */         this.music.setVolume(Interpolation.fade.apply(this.silenceStartVolume, 0.0F, 1.0F - this.silenceTimer / this.silenceTime));
/*     */       } else {
/* 199 */         this.music.setVolume(MathHelper.slowColorLerpSnap(this.music.getVolume(), 0.0F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFadeOut()
/*     */   {
/* 208 */     if (!this.isSilenced) {
/* 209 */       this.fadeTimer -= Gdx.graphics.getDeltaTime();
/* 210 */       if (this.fadeTimer < 0.0F) {
/* 211 */         this.fadeTimer = 0.0F;
/* 212 */         this.isDone = true;
/* 213 */         this.music.dispose();
/*     */       } else {
/* 215 */         this.music.setVolume(Interpolation.fade.apply(this.fadeOutStartVolume, 0.0F, 1.0F - this.fadeTimer / 4.0F));
/*     */       }
/*     */     } else {
/* 218 */       this.silenceTimer -= Gdx.graphics.getDeltaTime();
/* 219 */       if (this.silenceTimer < 0.0F) {
/* 220 */         this.silenceTimer = 0.0F;
/*     */       }
/* 222 */       this.music.setVolume(Interpolation.fade.apply(this.silenceStartVolume, 0.0F, 1.0F - this.silenceTimer / this.silenceTime));
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\audio\MainMusic.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */