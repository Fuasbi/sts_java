/*     */ package com.megacrit.cardcrawl.audio;
/*     */ 
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.Prefs;
/*     */ import com.megacrit.cardcrawl.helpers.SaveHelper;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import org.apache.logging.log4j.LogManager;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ 
/*     */ public class MusicMaster
/*     */ {
/*  14 */   private static final Logger logger = LogManager.getLogger(MusicMaster.class.getName());
/*  15 */   private ArrayList<MainMusic> mainTrack = new ArrayList();
/*  16 */   private ArrayList<TempMusic> tempTrack = new ArrayList();
/*     */   
/*     */   public MusicMaster() {
/*  19 */     Prefs prefs = SaveHelper.getPrefs("STSSound");
/*  20 */     Settings.MASTER_VOLUME = prefs.getFloat("Master Volume", 0.5F);
/*  21 */     Settings.MUSIC_VOLUME = prefs.getFloat("Music Volume", 0.5F);
/*  22 */     logger.info("Music Volume: " + Settings.MUSIC_VOLUME);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/*  29 */     updateBGM();
/*  30 */     updateTempBGM();
/*     */   }
/*     */   
/*     */   public void updateVolume() {
/*  34 */     for (MainMusic m : this.mainTrack) {
/*  35 */       m.updateVolume();
/*     */     }
/*  37 */     for (TempMusic m : this.tempTrack) {
/*  38 */       m.updateVolume();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void updateBGM()
/*     */   {
/*  47 */     for (Iterator<MainMusic> i = this.mainTrack.iterator(); i.hasNext();) {
/*  48 */       MainMusic e = (MainMusic)i.next();
/*  49 */       e.update();
/*  50 */       if (e.isDone) {
/*  51 */         logger.info("Properly faded out " + e.key);
/*  52 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void updateTempBGM()
/*     */   {
/*  62 */     for (Iterator<TempMusic> i = this.tempTrack.iterator(); i.hasNext();) {
/*  63 */       TempMusic e = (TempMusic)i.next();
/*  64 */       e.update();
/*  65 */       if (e.isDone) {
/*  66 */         logger.info("Properly faded out " + e.key);
/*  67 */         i.remove();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void fadeOutTempBGM()
/*     */   {
/*  76 */     for (TempMusic m : this.tempTrack) {
/*  77 */       if (!m.isFadingOut) {
/*  78 */         m.fadeOut();
/*     */       }
/*     */     }
/*  81 */     for (MainMusic m : this.mainTrack) {
/*  82 */       m.unsilence();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void playTempBGM(String key)
/*     */   {
/*  92 */     if (key != null) {
/*  93 */       logger.info("Playing " + key);
/*  94 */       this.tempTrack.add(new TempMusic(key, false));
/*     */       
/*  96 */       for (MainMusic m : this.mainTrack) {
/*  97 */         m.silence();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void playTempBgmInstantly(String key)
/*     */   {
/* 106 */     if (key != null) {
/* 107 */       logger.info("Playing " + key);
/* 108 */       this.tempTrack.add(new TempMusic(key, true));
/*     */       
/* 110 */       for (MainMusic m : this.mainTrack) {
/* 111 */         m.silenceInstantly();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void playTempBgmInstantly(String key, boolean loop)
/*     */   {
/* 120 */     if (key != null) {
/* 121 */       logger.info("Playing " + key);
/* 122 */       this.tempTrack.add(new TempMusic(key, true, loop));
/*     */       
/* 124 */       for (MainMusic m : this.mainTrack) {
/* 125 */         m.silenceInstantly();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void changeBGM(String key)
/*     */   {
/* 137 */     this.mainTrack.add(new MainMusic(key));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void fadeOutBGM()
/*     */   {
/* 145 */     for (MainMusic m : this.mainTrack) {
/* 146 */       if (!m.isFadingOut) {
/* 147 */         m.fadeOut();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void silenceBGM() {
/* 153 */     for (MainMusic m : this.mainTrack) {
/* 154 */       m.silence();
/*     */     }
/*     */   }
/*     */   
/*     */   public void silenceBGMInstantly() {
/* 159 */     for (MainMusic m : this.mainTrack) {
/* 160 */       m.silenceInstantly();
/*     */     }
/*     */   }
/*     */   
/*     */   public void silenceTempBgmInstantly() {
/* 165 */     for (TempMusic m : this.tempTrack) {
/* 166 */       m.silenceInstantly();
/*     */     }
/*     */   }
/*     */   
/*     */   public void unsilenceBGM() {
/* 171 */     for (MainMusic m : this.mainTrack) {
/* 172 */       m.unsilence();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 181 */     for (MainMusic m : this.mainTrack) {
/* 182 */       m.kill();
/*     */     }
/*     */     
/*     */ 
/* 186 */     for (TempMusic m : this.tempTrack) {
/* 187 */       m.kill();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void fadeAll()
/*     */   {
/* 196 */     for (MainMusic m : this.mainTrack) {
/* 197 */       m.fadeOut();
/*     */     }
/*     */     
/*     */ 
/* 201 */     for (TempMusic m : this.tempTrack) {
/* 202 */       m.fadeOut();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\audio\MusicMaster.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */