/*     */ package com.megacrit.cardcrawl.audio;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.audio.Music;
/*     */ import com.badlogic.gdx.math.Interpolation;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class TempMusic
/*     */ {
/*  13 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(TempMusic.class.getName());
/*     */   
/*     */   private Music music;
/*     */   
/*     */   public String key;
/*     */   private static final String DIR = "audio/music/";
/*     */   private static final String SHOP_BGM = "STS_Merchant_NewMix_v1.ogg";
/*     */   private static final String SHRINE_BGM = "STS_Shrine_NewMix_v1.ogg";
/*     */   private static final String LEVEL_1_BOSS_BGM = "STS_Boss1_NewMix_v1.ogg";
/*     */   private static final String LEVEL_2_BOSS_BGM = "STS_Boss2_NewMix_v1.ogg";
/*     */   private static final String LEVEL_3_BOSS_BGM = "STS_Boss3_NewMix_v1.ogg";
/*     */   private static final String ELITE_BGM = "STS_EliteBoss_NewMix_v1.ogg";
/*  25 */   public boolean isSilenced = false;
/*  26 */   private float silenceTimer = 0.0F; private float silenceTime = 0.0F;
/*     */   
/*     */   private static final float FAST_SILENCE_TIME = 0.25F;
/*     */   private float silenceStartVolume;
/*     */   private static final float FADE_IN_TIME = 4.0F;
/*     */   private static final float FAST_FADE_IN_TIME = 0.25F;
/*     */   private static final float FADE_OUT_TIME = 4.0F;
/*  33 */   private float fadeTimer = 0.0F; private float fadeTime = 0.0F;
/*  34 */   public boolean isFadingOut = false;
/*     */   private float fadeOutStartVolume;
/*  36 */   public boolean isDone = false;
/*     */   
/*     */   public TempMusic(String key, boolean isFast) {
/*  39 */     this(key, isFast, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TempMusic(String key, boolean isFast, boolean loop)
/*     */   {
/*  48 */     this.key = key;
/*  49 */     this.music = getSong(key);
/*  50 */     if (isFast) {
/*  51 */       this.fadeTimer = 0.25F;
/*  52 */       this.fadeTime = 0.25F;
/*     */     } else {
/*  54 */       this.fadeTimer = 4.0F;
/*  55 */       this.fadeTime = 4.0F;
/*     */     }
/*  57 */     this.music.setLooping(loop);
/*  58 */     this.music.play();
/*  59 */     this.music.setVolume(0.0F);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Music getSong(String key)
/*     */   {
/*  69 */     switch (key) {
/*     */     case "SHOP": 
/*  71 */       return MainMusic.newMusic("audio/music/STS_Merchant_NewMix_v1.ogg");
/*     */     case "SHRINE": 
/*  73 */       return MainMusic.newMusic("audio/music/STS_Shrine_NewMix_v1.ogg");
/*     */     case "BOSS_BOTTOM": 
/*  75 */       return MainMusic.newMusic("audio/music/STS_Boss1_NewMix_v1.ogg");
/*     */     case "BOSS_CITY": 
/*  77 */       return MainMusic.newMusic("audio/music/STS_Boss2_NewMix_v1.ogg");
/*     */     case "BOSS_BEYOND": 
/*  79 */       return MainMusic.newMusic("audio/music/STS_Boss3_NewMix_v1.ogg");
/*     */     case "ELITE": 
/*  81 */       return MainMusic.newMusic("audio/music/STS_EliteBoss_NewMix_v1.ogg");
/*     */     }
/*  83 */     logger.info("ERROR: NO SUCH TEMP BGM: " + key);
/*  84 */     return MainMusic.newMusic("audio/music/" + key);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void fadeOut()
/*     */   {
/*  93 */     this.isFadingOut = true;
/*  94 */     this.fadeOutStartVolume = this.music.getVolume();
/*  95 */     this.fadeTimer = 4.0F;
/*     */   }
/*     */   
/*     */   public void silenceInstantly() {
/*  99 */     this.isSilenced = true;
/* 100 */     this.silenceTimer = 0.25F;
/* 101 */     this.silenceTime = 0.25F;
/* 102 */     this.silenceStartVolume = this.music.getVolume();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void kill()
/*     */   {
/* 109 */     this.music.dispose();
/* 110 */     this.isDone = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void update()
/*     */   {
/* 117 */     if (!this.isFadingOut) {
/* 118 */       updateFadeIn();
/*     */     } else {
/* 120 */       updateFadeOut();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFadeIn()
/*     */   {
/* 128 */     if (!this.isSilenced) {
/* 129 */       this.fadeTimer -= Gdx.graphics.getDeltaTime();
/* 130 */       if (this.fadeTimer < 0.0F) {
/* 131 */         this.fadeTimer = 0.0F;
/*     */       }
/* 133 */       else if (!Settings.isBackgrounded) {
/* 134 */         this.music.setVolume(Interpolation.fade
/* 135 */           .apply(0.0F, 1.0F, 1.0F - this.fadeTimer / this.fadeTime) * (Settings.MUSIC_VOLUME * Settings.MASTER_VOLUME));
/*     */       }
/*     */       else {
/* 138 */         this.music.setVolume(MathHelper.slowColorLerpSnap(this.music.getVolume(), 0.0F));
/*     */       }
/*     */     }
/*     */     else {
/* 142 */       this.silenceTimer -= Gdx.graphics.getDeltaTime();
/* 143 */       if (this.silenceTimer < 0.0F) {
/* 144 */         this.silenceTimer = 0.0F;
/*     */       }
/* 146 */       if (!Settings.isBackgrounded) {
/* 147 */         this.music.setVolume(Interpolation.fade.apply(this.silenceStartVolume, 0.0F, 1.0F - this.silenceTimer / this.silenceTime));
/*     */       } else {
/* 149 */         this.music.setVolume(MathHelper.slowColorLerpSnap(this.music.getVolume(), 0.0F));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void updateFadeOut()
/*     */   {
/* 158 */     this.fadeTimer -= Gdx.graphics.getDeltaTime();
/* 159 */     if (this.fadeTimer < 0.0F) {
/* 160 */       this.fadeTimer = 0.0F;
/* 161 */       this.isDone = true;
/* 162 */       this.music.dispose();
/*     */     } else {
/* 164 */       this.music.setVolume(Interpolation.fade.apply(this.fadeOutStartVolume, 0.0F, 1.0F - this.fadeTimer / 4.0F));
/*     */     }
/*     */   }
/*     */   
/*     */   public void updateVolume() {
/* 169 */     if ((!this.isFadingOut) && (!this.isSilenced)) {
/* 170 */       this.music.setVolume(Settings.MUSIC_VOLUME * Settings.MASTER_VOLUME);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\audio\TempMusic.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */