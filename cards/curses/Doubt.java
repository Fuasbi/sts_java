/*    */ package com.megacrit.cardcrawl.cards.curses;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Doubt extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Doubt";
/* 17 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Doubt");
/* 18 */   public static final String NAME = cardStrings.NAME;
/* 19 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   
/*    */   private static final int COST = -2;
/*    */   private static final int WEAK_AMT = 1;
/*    */   
/*    */   public Doubt()
/*    */   {
/* 26 */     super("Doubt", NAME, null, "curse/doubt", -2, DESCRIPTION, AbstractCard.CardType.CURSE, AbstractCard.CardColor.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     if ((!this.dontTriggerOnUseCard) && (p.hasRelic("Blue Candle"))) {
/* 42 */       useBlueCandle(p);
/*    */     } else {
/* 44 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, new com.megacrit.cardcrawl.powers.WeakPower(AbstractDungeon.player, 1, true), 1));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void triggerWhenDrawn()
/*    */   {
/* 55 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetDontTriggerAction(this, false));
/*    */   }
/*    */   
/*    */   public void triggerOnEndOfTurnForPlayingCard()
/*    */   {
/* 60 */     this.dontTriggerOnUseCard = true;
/* 61 */     AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(this, true));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 66 */     return new Doubt();
/*    */   }
/*    */   
/*    */   public void upgrade() {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\curses\Doubt.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */