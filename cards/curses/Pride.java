/*    */ package com.megacrit.cardcrawl.cards.curses;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Pride extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Pride";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Pride");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Pride()
/*    */   {
/* 22 */     super("Pride", NAME, null, "curse/pride", 1, DESCRIPTION, AbstractCard.CardType.CURSE, AbstractCard.CardColor.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.SPECIAL, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.exhaust = true;
/* 34 */     this.isInnate = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     if ((!this.dontTriggerOnUseCard) && (p.hasRelic("Blue Candle"))) {
/* 40 */       useBlueCandle(p);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void triggerWhenDrawn()
/*    */   {
/* 48 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.SetDontTriggerAction(this, false));
/*    */   }
/*    */   
/*    */   public void triggerOnEndOfTurnForPlayingCard()
/*    */   {
/* 53 */     this.dontTriggerOnUseCard = true;
/* 54 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(
/* 55 */       makeStatEquivalentCopy(), 1, false, true));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 60 */     return new Pride();
/*    */   }
/*    */   
/*    */   public void upgrade() {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\curses\Pride.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */