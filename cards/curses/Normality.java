/*    */ package com.megacrit.cardcrawl.cards.curses;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Normality extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Normality";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Normality");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   
/*    */   private static final int COST = -2;
/*    */   private static final int PLAY_LIMIT = 3;
/*    */   
/*    */   public Normality()
/*    */   {
/* 24 */     super("Normality", NAME, "curse/normality", "curse/normality", -2, DESCRIPTION, AbstractCard.CardType.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.CURSE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean canPlay(AbstractCard card)
/*    */   {
/* 38 */     if (AbstractDungeon.player.cardsPlayedThisTurn >= 3) {
/* 39 */       card.cantUseMessage = EXTENDED_DESCRIPTION[0];
/* 40 */       return false;
/*    */     }
/* 42 */     return true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 47 */     if (p.hasRelic("Blue Candle")) {
/* 48 */       useBlueCandle(p);
/*    */     } else {
/* 50 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.UseCardAction(this));
/*    */     }
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 56 */     super.applyPowers();
/*    */     
/* 58 */     if (AbstractDungeon.player.cardsPlayedThisTurn == 0) {
/* 59 */       this.rawDescription = (EXTENDED_DESCRIPTION[1] + 3 + EXTENDED_DESCRIPTION[2]);
/* 60 */     } else if (AbstractDungeon.player.cardsPlayedThisTurn == 1) {
/* 61 */       this.rawDescription = (EXTENDED_DESCRIPTION[1] + 3 + EXTENDED_DESCRIPTION[3] + AbstractDungeon.player.cardsPlayedThisTurn + EXTENDED_DESCRIPTION[4]);
/*    */     }
/*    */     else {
/* 64 */       this.rawDescription = (EXTENDED_DESCRIPTION[1] + 3 + EXTENDED_DESCRIPTION[3] + AbstractDungeon.player.cardsPlayedThisTurn + EXTENDED_DESCRIPTION[5]);
/*    */     }
/*    */     
/* 67 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 72 */     return new Normality();
/*    */   }
/*    */   
/*    */   public void upgrade() {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\curses\Normality.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */