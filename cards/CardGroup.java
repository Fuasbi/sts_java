/*      */ package com.megacrit.cardcrawl.cards;
/*      */ 
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*      */ import com.megacrit.cardcrawl.actions.common.DrawCardAction;
/*      */ import com.megacrit.cardcrawl.audio.SoundMaster;
/*      */ import com.megacrit.cardcrawl.cards.curses.Necronomicurse;
/*      */ import com.megacrit.cardcrawl.cards.curses.Parasite;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon.CurrentScreen;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*      */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.relics.BottledFlame;
/*      */ import com.megacrit.cardcrawl.relics.BottledLightning;
/*      */ import com.megacrit.cardcrawl.relics.BottledTornado;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*      */ import com.megacrit.cardcrawl.unlock.UnlockTracker;
/*      */ import com.megacrit.cardcrawl.vfx.NecronomicurseEffect;
/*      */ import com.megacrit.cardcrawl.vfx.cardManip.ExhaustCardEffect;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Collections;
/*      */ import java.util.Comparator;
/*      */ import java.util.HashMap;
/*      */ import java.util.HashSet;
/*      */ import java.util.Iterator;
/*      */ import java.util.Map.Entry;
/*      */ import java.util.Set;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class CardGroup
/*      */ {
/*   46 */   private static final Logger logger = LogManager.getLogger(CardGroup.class.getName());
/*   47 */   public ArrayList<AbstractCard> group = new ArrayList();
/*   48 */   public float HAND_START_X = Settings.WIDTH * 0.36F; public float HAND_OFFSET_X = AbstractCard.IMG_WIDTH * 0.35F;
/*      */   private static final float HAND_HOVER_PUSH_AMT = 0.4F;
/*      */   private static final float PUSH_TAPER = 0.25F;
/*   51 */   private static final float TWO_CARD_PUSH_AMT = 0.2F; private static final float THREE_FOUR_CARD_PUSH_AMT = 0.27F; public static final float DRAW_PILE_X = Settings.WIDTH * 0.04F; public static final float DRAW_PILE_Y = 50.0F * Settings.scale;
/*   52 */   public static final int DISCARD_PILE_X = (int)(Settings.WIDTH + AbstractCard.IMG_WIDTH_S / 2.0F + 100.0F * Settings.scale);
/*      */   public static final int DISCARD_PILE_Y = 0;
/*      */   public CardGroupType type;
/*   55 */   public HashMap<Integer, Integer> handPositioningMap = new HashMap();
/*      */   
/*      */   public CardGroup(CardGroupType type) {
/*   58 */     this.type = type;
/*      */   }
/*      */   
/*      */   public CardGroup(CardGroup g, CardGroupType type) {
/*   62 */     this.type = type;
/*   63 */     for (AbstractCard c : g.group) {
/*   64 */       this.group.add(c.makeStatEquivalentCopy());
/*      */     }
/*      */   }
/*      */   
/*      */   public ArrayList<CardSave> getCardDeck() {
/*   69 */     ArrayList<CardSave> retVal = new ArrayList();
/*   70 */     for (AbstractCard card : this.group) {
/*   71 */       retVal.add(new CardSave(card.cardID, card.timesUpgraded, card.misc));
/*      */     }
/*   73 */     return retVal;
/*      */   }
/*      */   
/*      */   public ArrayList<String> getCardNames() {
/*   77 */     ArrayList<String> retVal = new ArrayList();
/*   78 */     for (AbstractCard card : this.group) {
/*   79 */       retVal.add(card.cardID);
/*      */     }
/*   81 */     return retVal;
/*      */   }
/*      */   
/*      */   public ArrayList<String> getCardIdsForMetrics() {
/*   85 */     ArrayList<String> retVal = new ArrayList();
/*   86 */     for (AbstractCard card : this.group) {
/*   87 */       retVal.add(card.getMetricID());
/*      */     }
/*   89 */     return retVal;
/*      */   }
/*      */   
/*      */   public void clear() {
/*   93 */     this.group.clear();
/*      */   }
/*      */   
/*      */   public boolean contains(AbstractCard c) {
/*   97 */     return this.group.contains(c);
/*      */   }
/*      */   
/*      */   public boolean canUseAnyCard() {
/*  101 */     for (AbstractCard c : this.group) {
/*  102 */       if (c.hasEnoughEnergy()) {
/*  103 */         return true;
/*      */       }
/*      */     }
/*      */     
/*  107 */     return false;
/*      */   }
/*      */   
/*      */   public int fullSetCheck() {
/*  111 */     int times = 0;
/*      */     
/*  113 */     ArrayList<String> cardIDS = new ArrayList();
/*  114 */     for (Iterator localIterator = this.group.iterator(); localIterator.hasNext();) { c = (AbstractCard)localIterator.next();
/*  115 */       if (c.rarity != AbstractCard.CardRarity.BASIC) {
/*  116 */         cardIDS.add(c.cardID);
/*      */       }
/*      */     }
/*      */     AbstractCard c;
/*  120 */     Object cardCount = new HashMap();
/*      */     
/*  122 */     for (String cardID : cardIDS) {
/*  123 */       if (((HashMap)cardCount).containsKey(cardID))
/*      */       {
/*  125 */         ((HashMap)cardCount).put(cardID, Integer.valueOf(((Integer)((HashMap)cardCount).get(cardID)).intValue() + 1));
/*      */       }
/*      */       else {
/*  128 */         ((HashMap)cardCount).put(cardID, Integer.valueOf(1));
/*      */       }
/*      */     }
/*      */     
/*  132 */     for (Map.Entry<String, Integer> card : ((HashMap)cardCount).entrySet()) {
/*  133 */       if (((Integer)card.getValue()).intValue() >= 4) {
/*  134 */         times++;
/*      */       }
/*      */     }
/*      */     
/*  138 */     return times;
/*      */   }
/*      */   
/*      */   public boolean pauperCheck() {
/*  142 */     for (AbstractCard c : this.group) {
/*  143 */       if (c.rarity == AbstractCard.CardRarity.RARE) {
/*  144 */         return false;
/*      */       }
/*      */     }
/*  147 */     return true;
/*      */   }
/*      */   
/*      */   public boolean cursedCheck() {
/*  151 */     int count = 0;
/*  152 */     for (AbstractCard c : this.group) {
/*  153 */       if (c.type == AbstractCard.CardType.CURSE) {
/*  154 */         count++;
/*      */       }
/*      */     }
/*  157 */     return count >= 5;
/*      */   }
/*      */   
/*      */   public boolean highlanderCheck()
/*      */   {
/*  162 */     ArrayList<String> cardIDS = new ArrayList();
/*  163 */     for (AbstractCard c : this.group) {
/*  164 */       if (c.rarity != AbstractCard.CardRarity.BASIC) {
/*  165 */         cardIDS.add(c.cardID);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  170 */     Object set = new HashSet(cardIDS);
/*      */     
/*  172 */     if (((Set)set).size() < cardIDS.size())
/*      */     {
/*  174 */       return false;
/*      */     }
/*      */     
/*      */ 
/*  178 */     return true;
/*      */   }
/*      */   
/*      */   public void applyPowers() {
/*  182 */     for (AbstractCard c : this.group) {
/*  183 */       c.applyPowers();
/*      */     }
/*      */   }
/*      */   
/*      */   public void removeCard(AbstractCard c) {
/*  188 */     if (((c instanceof Necronomicurse)) && (this.type == CardGroupType.MASTER_DECK)) {
/*  189 */       if (AbstractDungeon.player.hasRelic("Necronomicon")) {
/*  190 */         AbstractDungeon.player.getRelic("Necronomicon").flash();
/*      */       }
/*  192 */       AbstractDungeon.effectsQueue.add(new NecronomicurseEffect(new Necronomicurse(), Settings.WIDTH / 2.0F, Settings.HEIGHT / 2.0F));
/*      */     }
/*  194 */     else if (((c instanceof Parasite)) && (this.type == CardGroupType.MASTER_DECK)) {
/*  195 */       AbstractDungeon.player.decreaseMaxHealth(3);
/*  196 */       CardCrawlGame.sound.play("BLOOD_SWISH");
/*      */     }
/*  198 */     this.group.remove(c);
/*  199 */     if (this.type == CardGroupType.MASTER_DECK) {
/*  200 */       for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  201 */         r.onMasterDeckChange();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean removeCard(String targetID)
/*      */   {
/*  212 */     for (Iterator<AbstractCard> i = this.group.iterator(); i.hasNext();) {
/*  213 */       AbstractCard e = (AbstractCard)i.next();
/*  214 */       if (e.cardID.equals(targetID)) {
/*  215 */         i.remove();
/*  216 */         return true;
/*      */       }
/*      */     }
/*  219 */     return false;
/*      */   }
/*      */   
/*      */   public void addToHand(AbstractCard c) {
/*  223 */     this.group.add(c);
/*      */   }
/*      */   
/*      */   public void refreshHandLayout() {
/*  227 */     if ((AbstractDungeon.getCurrRoom().monsters != null) && 
/*  228 */       (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead())) {
/*  229 */       return;
/*      */     }
/*      */     
/*  232 */     for (AbstractOrb o : AbstractDungeon.player.orbs) {
/*  233 */       o.hideEvokeValues();
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  238 */     if ((AbstractDungeon.player.hand.size() + AbstractDungeon.player.drawPile.size() + AbstractDungeon.player.discardPile.size() <= 3) && 
/*  239 */       (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) && (AbstractDungeon.getCurrRoom().monsters != null) && 
/*  240 */       (!AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) && (AbstractDungeon.floorNum > 3)) {
/*  241 */       UnlockTracker.unlockAchievement("PURITY");
/*      */     }
/*      */     
/*  244 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  245 */       r.onRefreshHand();
/*      */     }
/*      */     
/*      */ 
/*  249 */     float angleRange = 50.0F - (10 - this.group.size()) * 5.0F;
/*  250 */     float incrementAngle = angleRange / this.group.size();
/*  251 */     float sinkStart = 80.0F * Settings.scale;
/*  252 */     float sinkRange = 300.0F * Settings.scale;
/*  253 */     float incrementSink = sinkRange / this.group.size() / 2.0F;
/*  254 */     int middle = this.group.size() / 2;
/*      */     
/*  256 */     for (int i = 0; i < this.group.size(); i++) {
/*  257 */       ((AbstractCard)this.group.get(i)).setAngle(angleRange / 2.0F - incrementAngle * i - incrementAngle / 2.0F);
/*      */       
/*  259 */       int t = i - middle;
/*  260 */       if (t >= 0) {
/*  261 */         if (this.group.size() % 2 == 0) {
/*  262 */           t++;
/*  263 */           t = -t;
/*      */         } else {
/*  265 */           t = -t;
/*      */         }
/*      */       }
/*      */       
/*  269 */       if (this.group.size() % 2 == 0) {
/*  270 */         t++;
/*      */       }
/*  272 */       t = (int)(t * 1.7F);
/*  273 */       ((AbstractCard)this.group.get(i)).target_y = (sinkStart + incrementSink * t);
/*      */     }
/*      */     
/*  276 */     for (AbstractCard c : this.group) {
/*  277 */       c.targetDrawScale = 0.75F;
/*      */     }
/*      */     AbstractCard c;
/*  280 */     switch (this.group.size()) {
/*      */     case 0: 
/*  282 */       return;
/*      */     case 1: 
/*  284 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F);
/*  285 */       break;
/*      */     case 2: 
/*  287 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.47F);
/*  288 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.53F);
/*  289 */       break;
/*      */     case 3: 
/*  291 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.9F);
/*  292 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F);
/*  293 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.9F);
/*  294 */       ((AbstractCard)this.group.get(0)).target_y += 20.0F * Settings.scale;
/*  295 */       ((AbstractCard)this.group.get(2)).target_y += 20.0F * Settings.scale;
/*  296 */       break;
/*      */     case 4: 
/*  298 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.36F);
/*  299 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.46F);
/*  300 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.46F);
/*  301 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.36F);
/*  302 */       ((AbstractCard)this.group.get(1)).target_y -= 10.0F * Settings.scale;
/*  303 */       ((AbstractCard)this.group.get(2)).target_y -= 10.0F * Settings.scale;
/*  304 */       break;
/*      */     case 5: 
/*  306 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.7F);
/*  307 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.9F);
/*  308 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F);
/*  309 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.9F);
/*  310 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.7F);
/*  311 */       ((AbstractCard)this.group.get(0)).target_y += 25.0F * Settings.scale;
/*  312 */       ((AbstractCard)this.group.get(2)).target_y -= 10.0F * Settings.scale;
/*  313 */       ((AbstractCard)this.group.get(4)).target_y += 25.0F * Settings.scale;
/*  314 */       break;
/*      */     case 6: 
/*  316 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.1F);
/*  317 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.3F);
/*  318 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.43F);
/*  319 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.43F);
/*  320 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.3F);
/*  321 */       ((AbstractCard)this.group.get(5)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.1F);
/*  322 */       ((AbstractCard)this.group.get(0)).target_y += 10.0F * Settings.scale;
/*  323 */       ((AbstractCard)this.group.get(5)).target_y += 10.0F * Settings.scale;
/*  324 */       break;
/*      */     case 7: 
/*  326 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.4F);
/*  327 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.7F);
/*  328 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.9F);
/*  329 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F);
/*  330 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.9F);
/*  331 */       ((AbstractCard)this.group.get(5)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.7F);
/*  332 */       ((AbstractCard)this.group.get(6)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.4F);
/*  333 */       ((AbstractCard)this.group.get(0)).target_y += 25.0F * Settings.scale;
/*  334 */       ((AbstractCard)this.group.get(1)).target_y += 18.0F * Settings.scale;
/*  335 */       ((AbstractCard)this.group.get(3)).target_y -= 6.0F * Settings.scale;
/*  336 */       ((AbstractCard)this.group.get(5)).target_y += 18.0F * Settings.scale;
/*  337 */       ((AbstractCard)this.group.get(6)).target_y += 25.0F * Settings.scale;
/*  338 */       break;
/*      */     case 8: 
/*  340 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.5F);
/*  341 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.82F);
/*  342 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.1F);
/*  343 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.38F);
/*  344 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.38F);
/*  345 */       ((AbstractCard)this.group.get(5)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.1F);
/*  346 */       ((AbstractCard)this.group.get(6)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.77F);
/*  347 */       ((AbstractCard)this.group.get(7)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.5F);
/*  348 */       ((AbstractCard)this.group.get(1)).target_y += 10.0F * Settings.scale;
/*  349 */       ((AbstractCard)this.group.get(6)).target_y += 10.0F * Settings.scale;
/*      */       
/*  351 */       for (AbstractCard c : this.group) {
/*  352 */         c.targetDrawScale = 0.7125F;
/*      */       }
/*  354 */       break;
/*      */     case 9: 
/*  356 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.8F);
/*  357 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.2F);
/*  358 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.53F);
/*  359 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.8F);
/*  360 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.0F);
/*  361 */       ((AbstractCard)this.group.get(5)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.8F);
/*  362 */       ((AbstractCard)this.group.get(6)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.53F);
/*  363 */       ((AbstractCard)this.group.get(7)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.2F);
/*  364 */       ((AbstractCard)this.group.get(8)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.8F);
/*  365 */       ((AbstractCard)this.group.get(1)).target_y += 22.0F * Settings.scale;
/*  366 */       ((AbstractCard)this.group.get(2)).target_y += 18.0F * Settings.scale;
/*  367 */       ((AbstractCard)this.group.get(3)).target_y += 12.0F * Settings.scale;
/*  368 */       ((AbstractCard)this.group.get(5)).target_y += 12.0F * Settings.scale;
/*  369 */       ((AbstractCard)this.group.get(6)).target_y += 18.0F * Settings.scale;
/*  370 */       ((AbstractCard)this.group.get(7)).target_y += 22.0F * Settings.scale;
/*      */       
/*  372 */       for (AbstractCard c : this.group) {
/*  373 */         c.targetDrawScale = 0.67499995F;
/*      */       }
/*  375 */       break;
/*      */     case 10: 
/*  377 */       ((AbstractCard)this.group.get(0)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.9F);
/*  378 */       ((AbstractCard)this.group.get(1)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 2.4F);
/*  379 */       ((AbstractCard)this.group.get(2)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.8F);
/*  380 */       ((AbstractCard)this.group.get(3)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 1.1F);
/*  381 */       ((AbstractCard)this.group.get(4)).target_x = (Settings.WIDTH / 2.0F - AbstractCard.IMG_WIDTH_S * 0.4F);
/*  382 */       ((AbstractCard)this.group.get(5)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 0.4F);
/*  383 */       ((AbstractCard)this.group.get(6)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.1F);
/*  384 */       ((AbstractCard)this.group.get(7)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 1.8F);
/*  385 */       ((AbstractCard)this.group.get(8)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.4F);
/*  386 */       ((AbstractCard)this.group.get(9)).target_x = (Settings.WIDTH / 2.0F + AbstractCard.IMG_WIDTH_S * 2.9F);
/*  387 */       ((AbstractCard)this.group.get(1)).target_y += 20.0F * Settings.scale;
/*  388 */       ((AbstractCard)this.group.get(2)).target_y += 17.0F * Settings.scale;
/*  389 */       ((AbstractCard)this.group.get(3)).target_y += 12.0F * Settings.scale;
/*  390 */       ((AbstractCard)this.group.get(4)).target_y += 5.0F * Settings.scale;
/*  391 */       ((AbstractCard)this.group.get(5)).target_y += 5.0F * Settings.scale;
/*  392 */       ((AbstractCard)this.group.get(6)).target_y += 12.0F * Settings.scale;
/*  393 */       ((AbstractCard)this.group.get(7)).target_y += 17.0F * Settings.scale;
/*  394 */       ((AbstractCard)this.group.get(8)).target_y += 20.0F * Settings.scale;
/*      */       
/*  396 */       for (i = this.group.iterator(); i.hasNext();) { c = (AbstractCard)i.next();
/*  397 */         c.targetDrawScale = 0.63750005F;
/*      */       }
/*  399 */       break;
/*      */     
/*      */ 
/*      */     default: 
/*  403 */       logger.info("WTF MATE, why so many cards");
/*      */     }
/*      */     
/*      */     
/*  407 */     AbstractCard card = AbstractDungeon.player.hoveredCard;
/*  408 */     if (card != null) {
/*  409 */       card.setAngle(0.0F);
/*  410 */       card.target_x = ((card.current_x + card.target_x) / 2.0F);
/*  411 */       card.target_y = card.current_y;
/*      */     }
/*      */     
/*  414 */     for (CardQueueItem q : AbstractDungeon.actionManager.cardQueue) {
/*  415 */       if (q.card != null) {
/*  416 */         q.card.setAngle(0.0F);
/*  417 */         q.card.target_x = q.card.current_x;
/*  418 */         q.card.target_y = q.card.current_y;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void glowCheck() {
/*  424 */     for (AbstractCard c : this.group) {
/*  425 */       if ((c.canUse(AbstractDungeon.player, null)) && (AbstractDungeon.screen != AbstractDungeon.CurrentScreen.HAND_SELECT)) {
/*  426 */         c.beginGlowing();
/*      */       } else {
/*  428 */         c.stopGlowing();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void stopGlowing() {
/*  434 */     for (AbstractCard c : this.group) {
/*  435 */       c.stopGlowing();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void hoverCardPush(AbstractCard c)
/*      */   {
/*  445 */     if (this.group.size() > 1) {
/*  446 */       int cardNum = -1;
/*      */       
/*  448 */       for (int i = 0; i < this.group.size(); i++) {
/*  449 */         if (c.equals(this.group.get(i))) {
/*  450 */           cardNum = i;
/*  451 */           break;
/*      */         }
/*      */       }
/*      */       
/*  455 */       float pushAmt = 0.4F;
/*  456 */       if (this.group.size() == 2) {
/*  457 */         pushAmt = 0.2F;
/*  458 */       } else if ((this.group.size() == 3) || (this.group.size() == 4)) {
/*  459 */         pushAmt = 0.27F;
/*      */       }
/*      */       
/*  462 */       int currentSlot = cardNum + 1;
/*      */       
/*      */ 
/*  465 */       while (currentSlot < this.group.size()) {
/*  466 */         ((AbstractCard)this.group.get(currentSlot)).target_x += AbstractCard.IMG_WIDTH_S * pushAmt;
/*  467 */         pushAmt *= 0.25F;
/*  468 */         currentSlot++;
/*      */       }
/*      */       
/*  471 */       pushAmt = 0.4F;
/*      */       
/*  473 */       if (this.group.size() == 2) {
/*  474 */         pushAmt = 0.2F;
/*  475 */       } else if ((this.group.size() == 3) || (this.group.size() == 4)) {
/*  476 */         pushAmt = 0.27F;
/*      */       }
/*      */       
/*  479 */       currentSlot = cardNum - 1;
/*      */       
/*      */ 
/*  482 */       while ((currentSlot > -1) && (currentSlot < this.group.size())) {
/*  483 */         ((AbstractCard)this.group.get(currentSlot)).target_x -= AbstractCard.IMG_WIDTH_S * pushAmt;
/*  484 */         pushAmt *= 0.25F;
/*  485 */         currentSlot--;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void addToTop(AbstractCard c) {
/*  491 */     this.group.add(c);
/*      */   }
/*      */   
/*      */   public void addToBottom(AbstractCard c) {
/*  495 */     this.group.add(0, c);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void addToRandomSpot(AbstractCard c)
/*      */   {
/*  502 */     if (this.group.size() == 0)
/*      */     {
/*  504 */       this.group.add(c);
/*      */     } else {
/*  506 */       this.group.add(AbstractDungeon.cardRandomRng.random(this.group.size() - 1), c);
/*      */     }
/*      */   }
/*      */   
/*      */   public AbstractCard getTopCard() {
/*  511 */     return (AbstractCard)this.group.get(this.group.size() - 1);
/*      */   }
/*      */   
/*      */   public AbstractCard getNCardFromTop(int num) {
/*  515 */     return (AbstractCard)this.group.get(this.group.size() - 1 - num);
/*      */   }
/*      */   
/*      */   public AbstractCard getBottomCard() {
/*  519 */     return (AbstractCard)this.group.get(0);
/*      */   }
/*      */   
/*      */   public AbstractCard getHoveredCard() {
/*  523 */     for (AbstractCard c : this.group) {
/*  524 */       if (c.isHoveredInHand(0.7F)) {
/*  525 */         boolean success = true;
/*  526 */         for (CardQueueItem q : AbstractDungeon.actionManager.cardQueue) {
/*  527 */           if (q.card == c) {
/*  528 */             success = false;
/*  529 */             break;
/*      */           }
/*      */         }
/*  532 */         if (success) {
/*  533 */           return c;
/*      */         }
/*      */       }
/*      */     }
/*  537 */     return null;
/*      */   }
/*      */   
/*      */   public AbstractCard getRandomCard(com.megacrit.cardcrawl.random.Random rng) {
/*  541 */     return (AbstractCard)this.group.get(rng.random(this.group.size() - 1));
/*      */   }
/*      */   
/*      */   public AbstractCard getRandomCard(boolean useRng) {
/*  545 */     if (useRng) {
/*  546 */       return (AbstractCard)this.group.get(AbstractDungeon.cardRng.random(this.group.size() - 1));
/*      */     }
/*  548 */     return (AbstractCard)this.group.get(MathUtils.random(this.group.size() - 1));
/*      */   }
/*      */   
/*      */   public AbstractCard getRandomCard(boolean useRng, AbstractCard.CardRarity rarity)
/*      */   {
/*  553 */     ArrayList<AbstractCard> tmp = new ArrayList();
/*  554 */     for (AbstractCard c : this.group) {
/*  555 */       if (c.rarity == rarity) {
/*  556 */         tmp.add(c);
/*      */       }
/*      */     }
/*      */     
/*  560 */     if (tmp.isEmpty()) {
/*  561 */       logger.info("ERROR: No cards left for type: " + this.type.name());
/*  562 */       return null;
/*      */     }
/*      */     
/*  565 */     Collections.sort(tmp);
/*  566 */     if (useRng) {
/*  567 */       return (AbstractCard)tmp.get(AbstractDungeon.cardRng.random(tmp.size() - 1));
/*      */     }
/*  569 */     return (AbstractCard)tmp.get(MathUtils.random(tmp.size() - 1));
/*      */   }
/*      */   
/*      */   public AbstractCard getRandomCard(AbstractCard.CardType type, boolean useRng)
/*      */   {
/*  574 */     ArrayList<AbstractCard> tmp = new ArrayList();
/*  575 */     for (AbstractCard c : this.group) {
/*  576 */       if (c.type == type) {
/*  577 */         tmp.add(c);
/*      */       }
/*      */     }
/*      */     
/*  581 */     if (tmp.isEmpty()) {
/*  582 */       logger.info("ERROR: No cards left for type: " + type.name());
/*  583 */       return null;
/*      */     }
/*      */     
/*  586 */     Collections.sort(tmp);
/*  587 */     if (useRng) {
/*  588 */       return (AbstractCard)tmp.get(AbstractDungeon.cardRng.random(tmp.size() - 1));
/*      */     }
/*  590 */     return (AbstractCard)tmp.get(MathUtils.random(tmp.size() - 1));
/*      */   }
/*      */   
/*      */ 
/*      */   public void removeTopCard()
/*      */   {
/*  596 */     this.group.remove(this.group.size() - 1);
/*      */   }
/*      */   
/*      */   public void shuffle() {
/*  600 */     Collections.shuffle(this.group, new java.util.Random(AbstractDungeon.shuffleRng.randomLong()));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void shuffle(com.megacrit.cardcrawl.random.Random rng)
/*      */   {
/*  609 */     Collections.shuffle(this.group, new java.util.Random(rng.randomLong()));
/*      */   }
/*      */   
/*      */   public String toString()
/*      */   {
/*  614 */     StringBuilder sb = new StringBuilder("");
/*  615 */     for (AbstractCard c : this.group) {
/*  616 */       sb.append(c.cardID);
/*  617 */       sb.append("\n");
/*      */     }
/*  619 */     return sb.toString();
/*      */   }
/*      */   
/*      */   public void update() {
/*  623 */     for (AbstractCard c : this.group) {
/*  624 */       c.update();
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateHoverLogic() {
/*  629 */     for (AbstractCard c : this.group) {
/*  630 */       c.updateHoverLogic();
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void render(SpriteBatch sb)
/*      */   {
/*  640 */     for (AbstractCard c : this.group) {
/*  641 */       c.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderMasterDeck(SpriteBatch sb)
/*      */   {
/*  651 */     for (AbstractCard c : this.group) {
/*  652 */       c.render(sb);
/*  653 */       if (c.inBottleFlame) {
/*  654 */         AbstractRelic r = new BottledFlame();
/*  655 */         r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  656 */         r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  657 */         r.renderOutline(sb, false);
/*  658 */         r.render(sb);
/*  659 */       } else if (c.inBottleLightning) {
/*  660 */         AbstractRelic r = new BottledLightning();
/*  661 */         r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  662 */         r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  663 */         r.renderOutline(sb, false);
/*  664 */         r.render(sb);
/*  665 */       } else if (c.inBottleTornado) {
/*  666 */         AbstractRelic r = new BottledTornado();
/*  667 */         r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  668 */         r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  669 */         r.renderOutline(sb, false);
/*  670 */         r.render(sb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderExceptOneCard(SpriteBatch sb, AbstractCard card)
/*      */   {
/*  683 */     for (AbstractCard c : this.group) {
/*  684 */       if (!c.equals(card)) {
/*  685 */         c.render(sb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderMasterDeckExceptOneCard(SpriteBatch sb, AbstractCard card) {
/*  691 */     for (AbstractCard c : this.group) {
/*  692 */       if (!c.equals(card)) {
/*  693 */         c.render(sb);
/*  694 */         if (c.inBottleFlame) {
/*  695 */           AbstractRelic r = new BottledFlame();
/*  696 */           r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  697 */           r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  698 */           r.renderOutline(sb, false);
/*  699 */           r.render(sb);
/*  700 */         } else if (c.inBottleLightning) {
/*  701 */           AbstractRelic r = new BottledLightning();
/*  702 */           r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  703 */           r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  704 */           r.renderOutline(sb, false);
/*  705 */           r.render(sb);
/*  706 */         } else if (c.inBottleTornado) {
/*  707 */           AbstractRelic r = new BottledTornado();
/*  708 */           r.currentX = (c.current_x + 390.0F * c.drawScale / 3.0F * Settings.scale);
/*  709 */           r.currentY = (c.current_y + 546.0F * c.drawScale / 3.0F * Settings.scale);
/*  710 */           r.renderOutline(sb, false);
/*  711 */           r.render(sb);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderHand(SpriteBatch sb, AbstractCard exceptThis) {
/*  718 */     ArrayList<AbstractCard> queued = new ArrayList();
/*  719 */     ArrayList<AbstractCard> inHand = new ArrayList();
/*      */     
/*  721 */     for (AbstractCard c : this.group) {
/*  722 */       if (c != exceptThis) {
/*  723 */         boolean inQueue = false;
/*  724 */         for (CardQueueItem i : AbstractDungeon.actionManager.cardQueue) {
/*  725 */           if ((i.card != null) && (i.card.equals(c))) {
/*  726 */             queued.add(c);
/*  727 */             inQueue = true;
/*  728 */             break;
/*      */           }
/*      */         }
/*  731 */         if (!inQueue) {
/*  732 */           inHand.add(c);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*  737 */     for (AbstractCard c : inHand) {
/*  738 */       c.render(sb);
/*      */     }
/*      */     
/*  741 */     for (AbstractCard c : queued) {
/*  742 */       c.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderInLibrary(SpriteBatch sb) {
/*  747 */     for (AbstractCard c : this.group) {
/*  748 */       c.renderInLibrary(sb);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderTip(SpriteBatch sb)
/*      */   {
/*  758 */     for (AbstractCard c : this.group) {
/*  759 */       c.renderCardTip(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderWithSelections(SpriteBatch sb) {
/*  764 */     for (AbstractCard c : this.group) {
/*  765 */       c.renderWithSelections(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderDiscardPile(SpriteBatch sb) {
/*  770 */     for (AbstractCard c : this.group) {
/*  771 */       c.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void moveToDiscardPile(AbstractCard c) {
/*  776 */     resetCardBeforeMoving(c);
/*  777 */     c.shrink();
/*  778 */     c.darken(false);
/*  779 */     AbstractDungeon.getCurrRoom().souls.discard(c);
/*  780 */     AbstractDungeon.player.onCardDrawOrDiscard();
/*      */   }
/*      */   
/*      */   public void empower(AbstractCard c) {
/*  784 */     resetCardBeforeMoving(c);
/*  785 */     c.shrink();
/*  786 */     AbstractDungeon.getCurrRoom().souls.empower(c);
/*      */   }
/*      */   
/*      */   public void moveToExhaustPile(AbstractCard c) {
/*  790 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  791 */       r.onExhaust(c);
/*      */     }
/*  793 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/*  794 */       p.onExhaust(c);
/*      */     }
/*  796 */     c.triggerOnExhaust();
/*  797 */     resetCardBeforeMoving(c);
/*  798 */     AbstractDungeon.effectList.add(new ExhaustCardEffect(c));
/*  799 */     AbstractDungeon.player.exhaustPile.addToTop(c);
/*  800 */     AbstractDungeon.player.onCardDrawOrDiscard();
/*      */   }
/*      */   
/*      */   public void moveToHand(AbstractCard c, CardGroup group) {
/*  804 */     c.unhover();
/*  805 */     c.lighten(true);
/*  806 */     c.setAngle(0.0F);
/*  807 */     c.drawScale = 0.12F;
/*  808 */     c.targetDrawScale = 0.75F;
/*  809 */     c.current_x = DRAW_PILE_X;
/*  810 */     c.current_y = DRAW_PILE_Y;
/*  811 */     group.removeCard(c);
/*  812 */     AbstractDungeon.player.hand.addToTop(c);
/*  813 */     AbstractDungeon.player.hand.refreshHandLayout();
/*  814 */     AbstractDungeon.player.hand.applyPowers();
/*      */   }
/*      */   
/*      */   public void moveToDeck(AbstractCard c, boolean randomSpot) {
/*  818 */     resetCardBeforeMoving(c);
/*  819 */     c.shrink();
/*  820 */     AbstractDungeon.getCurrRoom().souls.onToDeck(c, randomSpot);
/*      */   }
/*      */   
/*      */   public void moveToBottomOfDeck(AbstractCard c) {
/*  824 */     resetCardBeforeMoving(c);
/*  825 */     c.shrink();
/*  826 */     AbstractDungeon.getCurrRoom().souls.onToBottomOfDeck(c);
/*      */   }
/*      */   
/*      */   private void resetCardBeforeMoving(AbstractCard c) {
/*  830 */     if (AbstractDungeon.player.hoveredCard == c) {
/*  831 */       AbstractDungeon.player.releaseCard();
/*      */     }
/*  833 */     AbstractDungeon.actionManager.removeFromQueue(c);
/*  834 */     c.unhover();
/*  835 */     c.untip();
/*  836 */     c.stopGlowing();
/*  837 */     this.group.remove(c);
/*      */   }
/*      */   
/*      */   public boolean isEmpty() {
/*  841 */     return this.group.isEmpty();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void discardAll(CardGroup discardPile)
/*      */   {
/*  851 */     for (AbstractCard c : this.group) {
/*  852 */       c.target_x = DISCARD_PILE_X;
/*  853 */       c.target_y = 0.0F;
/*  854 */       discardPile.addToTop(c);
/*      */     }
/*  856 */     this.group.clear();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void initializeDeck(CardGroup masterDeck)
/*      */   {
/*  865 */     clear();
/*  866 */     CardGroup copy = new CardGroup(masterDeck, CardGroupType.DRAW_PILE);
/*  867 */     copy.shuffle(AbstractDungeon.shuffleRng);
/*      */     
/*  869 */     ArrayList<AbstractCard> placeOnTop = new ArrayList();
/*      */     
/*  871 */     for (AbstractCard c : copy.group) {
/*  872 */       if (c.isInnate) {
/*  873 */         placeOnTop.add(c);
/*      */ 
/*      */       }
/*  876 */       else if ((c.inBottleFlame) || (c.inBottleLightning) || (c.inBottleTornado)) {
/*  877 */         placeOnTop.add(c);
/*      */       }
/*      */       else
/*      */       {
/*  881 */         c.target_x = DRAW_PILE_X;
/*  882 */         c.target_y = DRAW_PILE_Y;
/*  883 */         c.current_x = DRAW_PILE_X;
/*  884 */         c.current_y = DRAW_PILE_Y;
/*  885 */         addToTop(c);
/*      */       }
/*      */     }
/*  888 */     for (AbstractCard c : placeOnTop) {
/*  889 */       addToTop(c);
/*      */     }
/*      */     
/*      */ 
/*  893 */     if (placeOnTop.size() > AbstractDungeon.player.masterHandSize) {
/*  894 */       AbstractDungeon.actionManager.addToTurnStart(new DrawCardAction(AbstractDungeon.player, placeOnTop
/*  895 */         .size() - AbstractDungeon.player.masterHandSize));
/*      */     }
/*      */     
/*  898 */     placeOnTop.clear();
/*      */   }
/*      */   
/*      */   public int size() {
/*  902 */     return this.group.size();
/*      */   }
/*      */   
/*      */   public CardGroup getUpgradableCards() {
/*  906 */     CardGroup retVal = new CardGroup(CardGroupType.UNSPECIFIED);
/*  907 */     for (AbstractCard c : this.group) {
/*  908 */       if (c.canUpgrade()) {
/*  909 */         retVal.group.add(c);
/*      */       }
/*      */     }
/*  912 */     return retVal;
/*      */   }
/*      */   
/*      */   public Boolean hasUpgradableCards() {
/*  916 */     for (AbstractCard c : this.group) {
/*  917 */       if (c.canUpgrade()) {
/*  918 */         return Boolean.valueOf(true);
/*      */       }
/*      */     }
/*  921 */     return Boolean.valueOf(false);
/*      */   }
/*      */   
/*      */   public CardGroup getPurgeableCards() {
/*  925 */     CardGroup retVal = new CardGroup(CardGroupType.UNSPECIFIED);
/*  926 */     for (AbstractCard c : this.group) {
/*  927 */       if ((!c.cardID.equals("Necronomicurse")) && (!c.cardID.equals("AscendersBane"))) {
/*  928 */         retVal.group.add(c);
/*      */       }
/*      */     }
/*  931 */     return retVal;
/*      */   }
/*      */   
/*      */   public AbstractCard getSpecificCard(AbstractCard targetCard) {
/*  935 */     if (this.group.contains(targetCard)) {
/*  936 */       return targetCard;
/*      */     }
/*  938 */     return null;
/*      */   }
/*      */   
/*      */   public static enum CardGroupType
/*      */   {
/*  943 */     DRAW_PILE,  MASTER_DECK,  HAND,  DISCARD_PILE,  EXHAUST_PILE,  CARD_POOL,  UNSPECIFIED;
/*      */     
/*      */ 
/*      */     private CardGroupType() {}
/*      */   }
/*      */   
/*      */ 
/*      */   public void triggerOnOtherCardPlayed(AbstractCard usedCard)
/*      */   {
/*  952 */     for (AbstractCard c : this.group) {
/*  953 */       if (c != usedCard) {
/*  954 */         c.triggerOnOtherCardPlayed(usedCard);
/*      */       }
/*      */     }
/*  957 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/*  958 */       p.onAfterCardPlayed(usedCard);
/*      */     }
/*      */   }
/*      */   
/*      */   private class CardRarityComparator
/*      */     implements Comparator<AbstractCard>
/*      */   {
/*      */     private CardRarityComparator() {}
/*      */     
/*  967 */     public int compare(AbstractCard c1, AbstractCard c2) { return c1.rarity.compareTo(c2.rarity); }
/*      */   }
/*      */   
/*      */   private class StatusCardsLastComparator implements Comparator<AbstractCard> {
/*      */     private StatusCardsLastComparator() {}
/*      */     
/*      */     public int compare(AbstractCard c1, AbstractCard c2) {
/*  974 */       if ((c1.type == AbstractCard.CardType.STATUS) && (c2.type != AbstractCard.CardType.STATUS))
/*  975 */         return 1;
/*  976 */       if ((c1.type != AbstractCard.CardType.STATUS) && (c2.type == AbstractCard.CardType.STATUS)) {
/*  977 */         return -1;
/*      */       }
/*  979 */       return 0;
/*      */     }
/*      */   }
/*      */   
/*      */   private class CardTypeComparator
/*      */     implements Comparator<AbstractCard> {
/*      */     private CardTypeComparator() {}
/*      */     
/*  987 */     public int compare(AbstractCard c1, AbstractCard c2) { return c1.type.compareTo(c2.type); }
/*      */   }
/*      */   
/*      */   private class CardLockednessComparator implements Comparator<AbstractCard> {
/*      */     private CardLockednessComparator() {}
/*      */     
/*      */     public int compare(AbstractCard c1, AbstractCard c2) {
/*  994 */       int c1Rank = 0;
/*  995 */       if (UnlockTracker.isCardLocked(c1.cardID)) {
/*  996 */         c1Rank = 2;
/*  997 */       } else if (!UnlockTracker.isCardSeen(c1.cardID)) {
/*  998 */         c1Rank = 1;
/*      */       }
/*      */       
/* 1001 */       int c2Rank = 0;
/* 1002 */       if (UnlockTracker.isCardLocked(c2.cardID)) {
/* 1003 */         c2Rank = 2;
/* 1004 */       } else if (!UnlockTracker.isCardSeen(c2.cardID)) {
/* 1005 */         c2Rank = 1;
/*      */       }
/*      */       
/* 1008 */       return c1Rank - c2Rank;
/*      */     }
/*      */   }
/*      */   
/*      */   private class CardNameComparator implements Comparator<AbstractCard> {
/*      */     private CardNameComparator() {}
/*      */     
/* 1015 */     public int compare(AbstractCard c1, AbstractCard c2) { return c1.name.compareTo(c2.name); }
/*      */   }
/*      */   
/*      */   private class CardCostComparator implements Comparator<AbstractCard> {
/*      */     private CardCostComparator() {}
/*      */     
/*      */     public int compare(AbstractCard c1, AbstractCard c2) {
/* 1022 */       return c1.cost - c2.cost;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void sortWithComparator(Comparator<AbstractCard> comp, boolean ascending)
/*      */   {
/* 1033 */     if (ascending) {
/* 1034 */       this.group.sort(comp);
/*      */     } else {
/* 1036 */       this.group.sort(Collections.reverseOrder(comp));
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void sortByRarity(boolean ascending)
/*      */   {
/* 1044 */     sortWithComparator(new CardRarityComparator(null), ascending);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void sortByRarityPlusStatusCardType(boolean ascending)
/*      */   {
/* 1051 */     sortWithComparator(new CardRarityComparator(null), ascending);
/* 1052 */     sortWithComparator(new StatusCardsLastComparator(null), true);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void sortByType(boolean ascending)
/*      */   {
/* 1059 */     sortWithComparator(new CardTypeComparator(null), ascending);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void sortByAcquisition() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void sortByStatus(boolean ascending)
/*      */   {
/* 1072 */     sortWithComparator(new CardLockednessComparator(null), ascending);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void sortAlphabetically(boolean ascending)
/*      */   {
/* 1081 */     sortWithComparator(new CardNameComparator(null), ascending);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void sortByCost(boolean ascending)
/*      */   {
/* 1090 */     sortWithComparator(new CardCostComparator(null), ascending);
/*      */   }
/*      */   
/*      */   public CardGroup getSkills() {
/* 1094 */     return getCardsOfType(AbstractCard.CardType.SKILL);
/*      */   }
/*      */   
/*      */   public CardGroup getAttacks() {
/* 1098 */     return getCardsOfType(AbstractCard.CardType.ATTACK);
/*      */   }
/*      */   
/*      */   public CardGroup getPowers() {
/* 1102 */     return getCardsOfType(AbstractCard.CardType.POWER);
/*      */   }
/*      */   
/*      */   public CardGroup getCardsOfType(AbstractCard.CardType cardType) {
/* 1106 */     CardGroup retVal = new CardGroup(CardGroupType.UNSPECIFIED);
/* 1107 */     for (AbstractCard card : this.group) {
/* 1108 */       if (card.type == cardType) {
/* 1109 */         retVal.addToBottom(card);
/*      */       }
/*      */     }
/*      */     
/* 1113 */     return retVal;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CardGroup getGroupedByColor()
/*      */   {
/* 1122 */     ArrayList<CardGroup> colorGroups = new ArrayList();
/*      */     
/* 1124 */     for (AbstractCard.CardColor color : AbstractCard.CardColor.values()) {
/* 1125 */       colorGroups.add(new CardGroup(CardGroupType.UNSPECIFIED));
/*      */     }
/* 1127 */     for (??? = this.group.iterator(); ((Iterator)???).hasNext();) { card = (AbstractCard)((Iterator)???).next();
/* 1128 */       ((CardGroup)colorGroups.get(((AbstractCard)card).color.ordinal())).addToTop((AbstractCard)card);
/*      */     }
/*      */     
/* 1131 */     CardGroup retVal = new CardGroup(CardGroupType.UNSPECIFIED);
/* 1132 */     for (Object card = colorGroups.iterator(); ((Iterator)card).hasNext();) { CardGroup group = (CardGroup)((Iterator)card).next();
/* 1133 */       retVal.group.addAll(group.group);
/*      */     }
/* 1135 */     return retVal;
/*      */   }
/*      */   
/*      */   public AbstractCard findCardByName(String id) {
/* 1139 */     for (AbstractCard c : this.group) {
/* 1140 */       if (c.cardID.equals(id)) {
/* 1141 */         return c;
/*      */       }
/*      */     }
/* 1144 */     return null;
/*      */   }
/*      */   
/*      */   public static CardGroup getGroupWithoutBottledCards(CardGroup group) {
/* 1148 */     CardGroup retVal = new CardGroup(CardGroupType.UNSPECIFIED);
/* 1149 */     for (AbstractCard c : group.group) {
/* 1150 */       if ((!c.inBottleFlame) && (!c.inBottleLightning) && (!c.inBottleTornado)) {
/* 1151 */         retVal.addToTop(c);
/*      */       }
/*      */     }
/* 1154 */     return retVal;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\CardGroup.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */