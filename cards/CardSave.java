/*    */ package com.megacrit.cardcrawl.cards;
/*    */ 
/*    */ public class CardSave {
/*    */   public int upgrades;
/*    */   public int misc;
/*    */   public String id;
/*    */   
/*  8 */   public CardSave(String cardID, int timesUpgraded, int misc) { this.id = cardID;
/*  9 */     this.upgrades = timesUpgraded;
/* 10 */     this.misc = misc;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\CardSave.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */