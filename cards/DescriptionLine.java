/*    */ package com.megacrit.cardcrawl.cards;
/*    */ 
/*    */ import com.megacrit.cardcrawl.core.Settings;
/*    */ 
/*    */ public class DescriptionLine {
/*    */   public String text;
/*    */   public float width;
/*  8 */   private static final float offsetter = 10.0F * Settings.scale;
/*    */   
/*    */   public DescriptionLine(String text, float width) {
/* 11 */     this.text = text.trim();
/* 12 */     this.width = (width -= offsetter);
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\DescriptionLine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */