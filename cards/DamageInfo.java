/*     */ package com.megacrit.cardcrawl.cards;
/*     */ 
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.AbstractCreature;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*     */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*     */ import java.util.ArrayList;
/*     */ import org.apache.logging.log4j.Logger;
/*     */ 
/*     */ public class DamageInfo
/*     */ {
/*  15 */   private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DamageInfo.class.getName());
/*     */   public AbstractCreature owner;
/*     */   public String name;
/*     */   public DamageType type;
/*     */   public int base;
/*  20 */   public int output; public boolean isModified = false;
/*     */   
/*     */   public DamageInfo(AbstractCreature damageSource, int base, DamageType type) {
/*  23 */     this.owner = damageSource;
/*  24 */     this.type = type;
/*  25 */     this.base = base;
/*  26 */     this.output = base;
/*     */   }
/*     */   
/*     */   public DamageInfo(AbstractCreature owner, int base) {
/*  30 */     this(owner, base, DamageType.NORMAL);
/*     */   }
/*     */   
/*     */   public void applyPowers(AbstractCreature owner, AbstractCreature target) {
/*  34 */     this.output = this.base;
/*  35 */     this.isModified = false;
/*  36 */     float tmp = this.output;
/*     */     float mod;
/*  38 */     if (!owner.isPlayer)
/*     */     {
/*     */ 
/*  41 */       if ((com.megacrit.cardcrawl.core.Settings.isEndless) && (AbstractDungeon.player.hasBlight("DeadlyEnemies")))
/*     */       {
/*  43 */         mod = AbstractDungeon.player.getBlight("DeadlyEnemies").effectFloat();
/*  44 */         tmp *= mod;
/*     */         
/*  46 */         if (this.base != (int)tmp) {
/*  47 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  52 */       for (AbstractPower p : owner.powers) {
/*  53 */         tmp = p.atDamageGive(tmp, this.type);
/*     */         
/*  55 */         if (this.base != (int)tmp) {
/*  56 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  61 */       for (AbstractPower p : target.powers) {
/*  62 */         tmp = p.atDamageReceive(tmp, this.type);
/*  63 */         if (this.base != (int)tmp) {
/*  64 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  69 */       for (AbstractPower p : owner.powers) {
/*  70 */         tmp = p.atDamageFinalGive(tmp, this.type);
/*  71 */         if (this.base != (int)tmp) {
/*  72 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  77 */       for (AbstractPower p : target.powers) {
/*  78 */         tmp = p.atDamageFinalReceive(tmp, this.type);
/*  79 */         if (this.base != (int)tmp) {
/*  80 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*  85 */       this.output = MathUtils.floor(tmp);
/*  86 */       if (this.output < 0) {
/*  87 */         this.output = 0;
/*     */       }
/*     */     }
/*     */     else {
/*  91 */       logger.info("Damage Info calculation for Player is still in test");
/*     */       
/*     */ 
/*  94 */       for (AbstractPower p : owner.powers) {
/*  95 */         tmp = p.atDamageGive(tmp, this.type);
/*  96 */         if (this.base != (int)tmp) {
/*  97 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 102 */       for (AbstractPower p : target.powers) {
/* 103 */         tmp = p.atDamageReceive(tmp, this.type);
/* 104 */         if (this.base != (int)tmp) {
/* 105 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 110 */       for (AbstractPower p : owner.powers) {
/* 111 */         tmp = p.atDamageFinalGive(tmp, this.type);
/* 112 */         if (this.base != (int)tmp) {
/* 113 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 118 */       for (AbstractPower p : target.powers) {
/* 119 */         tmp = p.atDamageFinalReceive(tmp, this.type);
/* 120 */         if (this.base != (int)tmp) {
/* 121 */           this.isModified = true;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 126 */       this.output = MathUtils.floor(tmp);
/* 127 */       if (this.output < 0) {
/* 128 */         this.output = 0;
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void applyEnemyPowersOnly(AbstractCreature target)
/*     */   {
/* 139 */     this.output = this.base;
/* 140 */     this.isModified = false;
/* 141 */     float tmp = this.output;
/*     */     
/*     */ 
/* 144 */     for (AbstractPower p : target.powers) {
/* 145 */       tmp = p.atDamageReceive(this.output, this.type);
/* 146 */       if (this.base != this.output) {
/* 147 */         this.isModified = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 152 */     for (AbstractPower p : target.powers) {
/* 153 */       tmp = p.atDamageFinalReceive(this.output, this.type);
/* 154 */       if (this.base != this.output) {
/* 155 */         this.isModified = true;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 160 */     if (tmp < 0.0F) {
/* 161 */       tmp = 0.0F;
/*     */     }
/* 163 */     this.output = MathUtils.floor(tmp);
/*     */   }
/*     */   
/*     */   public static int[] createDamageMatrix(int baseDamage) {
/* 167 */     return createDamageMatrix(baseDamage, false);
/*     */   }
/*     */   
/*     */   public static int[] createDamageMatrix(int baseDamage, boolean isPureDamage) {
/* 171 */     int[] retVal = new int[AbstractDungeon.getMonsters().monsters.size()];
/* 172 */     for (int i = 0; i < retVal.length; i++) {
/* 173 */       DamageInfo info = new DamageInfo(AbstractDungeon.player, baseDamage);
/* 174 */       if (!isPureDamage) {
/* 175 */         info.applyEnemyPowersOnly((AbstractCreature)AbstractDungeon.getMonsters().monsters.get(i));
/*     */       }
/* 177 */       retVal[i] = info.output;
/*     */     }
/* 179 */     return retVal;
/*     */   }
/*     */   
/*     */   public static int[] createDamageMatrix(int baseDamage, boolean isPureDamage, boolean isOrbDamage) {
/* 183 */     int[] retVal = new int[AbstractDungeon.getMonsters().monsters.size()];
/* 184 */     for (int i = 0; i < retVal.length; i++) {
/* 185 */       DamageInfo info = new DamageInfo(AbstractDungeon.player, baseDamage);
/*     */       
/*     */ 
/*     */ 
/* 189 */       if ((isOrbDamage) && (((AbstractMonster)AbstractDungeon.getMonsters().monsters.get(i)).hasPower("Lockon"))) {
/* 190 */         info.output = ((int)(info.base * 1.5F));
/*     */       }
/* 192 */       retVal[i] = info.output;
/*     */     }
/* 194 */     return retVal;
/*     */   }
/*     */   
/*     */   public static enum DamageType {
/* 198 */     NORMAL,  THORNS,  HP_LOSS;
/*     */     
/*     */     private DamageType() {}
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\DamageInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */