/*    */ package com.megacrit.cardcrawl.cards.status;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.LoseEnergyAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ 
/*    */ public class Void extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Void";
/* 18 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Void");
/* 19 */   public static final String NAME = cardStrings.NAME;
/* 20 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = -2;
/*    */   
/*    */   public Void()
/*    */   {
/* 25 */     super("Void", NAME, null, "status/void", -2, DESCRIPTION, AbstractCard.CardType.STATUS, AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     this.isEthereal = true;
/*    */   }
/*    */   
/*    */ 
/*    */   public void triggerWhenDrawn()
/*    */   {
/* 42 */     AbstractDungeon.actionManager.addToBottom(new LoseEnergyAction(1));
/*    */     
/* 44 */     if ((AbstractDungeon.player.hasPower("Evolve")) && (!AbstractDungeon.player.hasPower("No Draw")))
/*    */     {
/* 46 */       AbstractDungeon.player.getPower("Evolve").flash();
/* 47 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 50 */         AbstractDungeon.player.getPower("Evolve").amount));
/*    */     }
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 56 */     if (p.hasRelic("Medical Kit")) {
/* 57 */       useMedicalKit(p);
/*    */     } else {
/* 59 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.UseCardAction(this));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 65 */     return new Void();
/*    */   }
/*    */   
/*    */   public void upgrade() {}
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\status\Void.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */