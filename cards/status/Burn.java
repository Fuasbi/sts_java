/*    */ package com.megacrit.cardcrawl.cards.status;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.SetDontTriggerAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardRarity;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardTarget;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Burn extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Burn";
/* 22 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Burn");
/* 23 */   public static final String NAME = cardStrings.NAME;
/* 24 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 25 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   
/*    */   private static final int COST = -2;
/*    */   
/* 29 */   public int UPG_DAMAGE = 4;
/* 30 */   private int actualBurnDamage = 2;
/*    */   
/*    */   public Burn() {
/* 33 */     super("Burn", NAME, "status/burn", "status/burn", -2, DESCRIPTION, AbstractCard.CardType.STATUS, AbstractCard.CardColor.COLORLESS, AbstractCard.CardRarity.COMMON, AbstractCard.CardTarget.NONE);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 48 */     if ((!this.dontTriggerOnUseCard) && (p.hasRelic("Medical Kit"))) {
/* 49 */       useMedicalKit(p);
/*    */     } else {
/* 51 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(AbstractDungeon.player, new DamageInfo(AbstractDungeon.player, this.actualBurnDamage, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.THORNS), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */       
/*    */ 
/*    */ 
/*    */ 
/* 56 */       AbstractDungeon.actionManager.addToBottom(new SetDontTriggerAction(this, false));
/*    */     }
/*    */   }
/*    */   
/*    */   public void triggerWhenDrawn()
/*    */   {
/* 62 */     if ((AbstractDungeon.player.hasPower("Evolve")) && (!AbstractDungeon.player.hasPower("No Draw")))
/*    */     {
/* 64 */       AbstractDungeon.player.getPower("Evolve").flash();
/* 65 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, 
/*    */       
/*    */ 
/* 68 */         AbstractDungeon.player.getPower("Evolve").amount));
/*    */     }
/*    */   }
/*    */   
/*    */   public void triggerOnEndOfTurnForPlayingCard()
/*    */   {
/* 74 */     this.dontTriggerOnUseCard = true;
/* 75 */     AbstractDungeon.actionManager.cardQueue.add(new com.megacrit.cardcrawl.cards.CardQueueItem(this, true));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 80 */     Burn retVal = new Burn();
/* 81 */     return retVal;
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 86 */     if (!this.upgraded) {
/* 87 */       upgradeName();
/* 88 */       this.actualBurnDamage = this.UPG_DAMAGE;
/* 89 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 90 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\status\Burn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */