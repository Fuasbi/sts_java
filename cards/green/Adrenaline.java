/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Adrenaline extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Adrenaline";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Adrenaline");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   private static final int DRAW_AMT = 2;
/*    */   private static final int ENERGY_AMT = 1;
/*    */   private static final int UPGRADE_ENERGY_AMT = 2;
/*    */   
/*    */   public Adrenaline() {
/* 24 */     super("Adrenaline", NAME, "green/skill/adrenaline", "green/skill/adrenaline", 0, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 40 */     if (this.upgraded) {
/* 41 */       AbstractDungeon.actionManager.addToBottom(new GainEnergyAction(2));
/*    */     } else {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new GainEnergyAction(1));
/*    */     }
/* 45 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(p, 2));
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 50 */     if (!this.upgraded) {
/* 51 */       upgradeName();
/* 52 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 53 */       initializeDescription();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 59 */     return new Adrenaline();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Adrenaline.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */