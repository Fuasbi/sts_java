/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Flechettes extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Flechettes";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Flechettes");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int DAMAGE = 4;
/*    */   
/*    */   public Flechettes()
/*    */   {
/* 23 */     super("Flechettes", NAME, null, "green/attack/flechettes", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.baseDamage = 4;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.FlechetteAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn)));
/* 40 */     this.rawDescription = DESCRIPTION;
/* 41 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 46 */     super.applyPowers();
/*    */     
/* 48 */     int count = 0;
/* 49 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 50 */       if (c.type == AbstractCard.CardType.SKILL) {
/* 51 */         count++;
/*    */       }
/*    */     }
/*    */     
/* 55 */     this.rawDescription = DESCRIPTION;
/* 56 */     this.rawDescription = (this.rawDescription + EXTENDED_DESCRIPTION[0] + count);
/* 57 */     if (count == 1) {
/* 58 */       this.rawDescription += EXTENDED_DESCRIPTION[1];
/*    */     } else {
/* 60 */       this.rawDescription += EXTENDED_DESCRIPTION[2];
/*    */     }
/* 62 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void onMoveToDiscard()
/*    */   {
/* 67 */     this.rawDescription = DESCRIPTION;
/* 68 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 73 */     if (!this.upgraded) {
/* 74 */       upgradeName();
/* 75 */       upgradeDamage(2);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 81 */     return new Flechettes();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Flechettes.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */