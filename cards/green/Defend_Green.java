/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.GainBlockAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Defend_Green extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Defend_G";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Defend_G");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int BLOCK_INIT_GAINED = 5;
/*    */   
/*    */   public Defend_Green()
/*    */   {
/* 22 */     super("Defend_G", NAME, "green/skill/defend", "green/skill/defend", 1, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.BASIC, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.baseBlock = 5;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     if (com.megacrit.cardcrawl.core.Settings.isDebug) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new GainBlockAction(p, p, 50));
/*    */     } else {
/* 42 */       AbstractDungeon.actionManager.addToBottom(new GainBlockAction(p, p, this.block));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 48 */     return new Defend_Green();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 53 */     if (!this.upgraded) {
/* 54 */       upgradeName();
/* 55 */       upgradeBlock(3);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Defend_Green.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */