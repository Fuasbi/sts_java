/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ 
/*    */ public class CripplingPoison extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Crippling Poison";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Crippling Poison");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   
/*    */   public CripplingPoison()
/*    */   {
/* 22 */     super("Crippling Poison", NAME, "green/skill/cripplingPoison", "green/skill/cripplingPoison", 2, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.baseMagicNumber = 4;
/* 35 */     this.magicNumber = this.baseMagicNumber;
/* 36 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     if (!AbstractDungeon.getMonsters().areMonstersBasicallyDead()) {
/* 42 */       flash();
/* 43 */       for (AbstractMonster monster : AbstractDungeon.getMonsters().monsters) {
/* 44 */         if ((!monster.isDead) && (!monster.isDying)) {
/* 45 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(monster, p, new com.megacrit.cardcrawl.powers.PoisonPower(monster, p, this.magicNumber), this.magicNumber));
/*    */           
/* 47 */           AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(monster, p, new com.megacrit.cardcrawl.powers.WeakPower(monster, 2, false), 2));
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 56 */     return new CripplingPoison();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 61 */     if (!this.upgraded) {
/* 62 */       upgradeName();
/* 63 */       upgradeMagicNumber(3);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\CripplingPoison.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */