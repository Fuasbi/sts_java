/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Tactician extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Tactician";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Tactician");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 16 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   
/*    */   private static final int COST = -2;
/*    */   
/*    */   private static final int ENERGY_AMT = 1;
/* 22 */   private int energy = 1;
/*    */   
/*    */   public Tactician() {
/* 25 */     super("Tactician", NAME, null, "green/skill/tactician", -2, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m) {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 45 */     this.cantUseMessage = EXTENDED_DESCRIPTION[0];
/* 46 */     return false;
/*    */   }
/*    */   
/*    */ 
/*    */   public void triggerOnManualDiscard()
/*    */   {
/* 52 */     if (this.upgraded) {
/* 53 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.energy + 1));
/*    */     } else {
/* 55 */       AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.common.GainEnergyAction(this.energy));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 61 */     return new Tactician();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 66 */     if (!this.upgraded) {
/* 67 */       upgradeName();
/* 68 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 69 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Tactician.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */