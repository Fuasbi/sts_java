/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class GrandFinale extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Grand Finale";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Grand Finale");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   private static final int ATTACK_DMG = 40;
/*    */   
/*    */   public GrandFinale()
/*    */   {
/* 23 */     super("Grand Finale", NAME, "green/attack/grandFinale", "green/attack/grandFinale", 0, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 40;
/* 36 */     this.isMultiDamage = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 47 */     boolean canUse = super.canUse(p, m);
/* 48 */     if (!canUse) {
/* 49 */       return false;
/*    */     }
/*    */     
/* 52 */     if (p.drawPile.size() > 0) {
/* 53 */       this.cantUseMessage = UPGRADE_DESCRIPTION;
/* 54 */       return false;
/*    */     }
/* 56 */     return canUse;
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 61 */     return new GrandFinale();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 66 */     if (!this.upgraded) {
/* 67 */       upgradeName();
/* 68 */       upgradeDamage(10);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\GrandFinale.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */