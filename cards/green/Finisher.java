/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Finisher extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Finisher";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Finisher");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int ATTACK_DMG = 6;
/*    */   
/*    */   public Finisher()
/*    */   {
/* 24 */     super("Finisher", NAME, null, "green/attack/finisher", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 6;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 40 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.DamagePerAttackPlayedAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 45 */     this.rawDescription = DESCRIPTION;
/* 46 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 51 */     super.applyPowers();
/*    */     
/* 53 */     int count = 0;
/* 54 */     for (AbstractCard c : AbstractDungeon.actionManager.cardsPlayedThisTurn) {
/* 55 */       if (c.type == AbstractCard.CardType.ATTACK) {
/* 56 */         count++;
/*    */       }
/*    */     }
/*    */     
/* 60 */     this.rawDescription = DESCRIPTION;
/* 61 */     this.rawDescription = (this.rawDescription + EXTENDED_DESCRIPTION[0] + count);
/*    */     
/* 63 */     if (count == 1) {
/* 64 */       this.rawDescription += EXTENDED_DESCRIPTION[1];
/*    */     } else {
/* 66 */       this.rawDescription += EXTENDED_DESCRIPTION[2];
/*    */     }
/* 68 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void onMoveToDiscard()
/*    */   {
/* 73 */     this.rawDescription = DESCRIPTION;
/* 74 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 79 */     return new Finisher();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 84 */     if (!this.upgraded) {
/* 85 */       upgradeName();
/* 86 */       upgradeDamage(2);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Finisher.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */