/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.ScreenShake.ShakeDur;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.vfx.BorderLongFlashEffect;
/*    */ import com.megacrit.cardcrawl.vfx.combat.DieDieDieEffect;
/*    */ 
/*    */ public class DieDieDie extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Die Die Die";
/* 21 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Die Die Die");
/* 22 */   public static final String NAME = cardStrings.NAME;
/* 23 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int ATTACK_DMG = 13;
/*    */   
/*    */   public DieDieDie()
/*    */   {
/* 29 */     super("Die Die Die", NAME, "green/attack/dieDieDie", "green/attack/dieDieDie", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 40 */     this.exhaust = true;
/* 41 */     this.baseDamage = 13;
/* 42 */     this.isMultiDamage = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 47 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(new BorderLongFlashEffect(Color.LIGHT_GRAY.cpy())));
/* 48 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(new DieDieDieEffect(), 0.7F));
/* 49 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.ShakeScreenAction(0.0F, ScreenShake.ShakeDur.MED, com.megacrit.cardcrawl.helpers.ScreenShake.ShakeIntensity.HIGH));
/* 50 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HORIZONTAL));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 56 */     return new DieDieDie();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 61 */     if (!this.upgraded) {
/* 62 */       upgradeName();
/* 63 */       upgradeDamage(4);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\DieDieDie.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */