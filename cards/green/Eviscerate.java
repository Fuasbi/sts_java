/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Eviscerate extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Eviscerate";
/* 16 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Eviscerate");
/* 17 */   public static final String NAME = cardStrings.NAME;
/* 18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 4;
/*    */   private static final int DAMAGE_AMT = 6;
/*    */   
/*    */   public Eviscerate()
/*    */   {
/* 24 */     super("Eviscerate", NAME, null, "green/attack/eviscerate", 4, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 6;
/*    */   }
/*    */   
/*    */   public void didDiscard()
/*    */   {
/* 40 */     setCostForTurn(this.cost - GameActionManager.totalDiscardedThisTurn);
/*    */   }
/*    */   
/*    */   public void triggerWhenDrawn()
/*    */   {
/* 45 */     super.triggerWhenDrawn();
/* 46 */     setCostForTurn(this.cost - GameActionManager.totalDiscardedThisTurn);
/*    */   }
/*    */   
/*    */   public void triggerWhenCopied()
/*    */   {
/* 51 */     super.triggerWhenDrawn();
/* 52 */     setCostForTurn(this.cost - GameActionManager.totalDiscardedThisTurn);
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 57 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 62 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 67 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 76 */     return new Eviscerate();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 81 */     if (!this.upgraded) {
/* 82 */       upgradeName();
/* 83 */       upgradeDamage(2);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Eviscerate.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */