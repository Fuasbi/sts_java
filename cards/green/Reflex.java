/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Reflex extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Reflex";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Reflex");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 16 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   
/*    */   private static final int COST = -2;
/*    */   private static final int DRAW = 1;
/*    */   
/*    */   public Reflex()
/*    */   {
/* 24 */     super("Reflex", NAME, null, "green/skill/reflex", -2, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     this.baseMagicNumber = 1;
/* 37 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 42 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(p, this.magicNumber));
/*    */   }
/*    */   
/*    */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 47 */     this.cantUseMessage = EXTENDED_DESCRIPTION[0];
/* 48 */     return false;
/*    */   }
/*    */   
/*    */   public void triggerOnManualDiscard()
/*    */   {
/* 53 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(AbstractDungeon.player, this.magicNumber));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 58 */     return new Reflex();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 63 */     if (!this.upgraded) {
/* 64 */       upgradeName();
/* 65 */       upgradeMagicNumber(1);
/* 66 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 67 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Reflex.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */