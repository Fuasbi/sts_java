/*    */ package com.megacrit.cardcrawl.cards.green;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.EnergizedPower;
/*    */ 
/*    */ public class Outmaneuver extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Outmaneuver";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Outmaneuver");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   
/*    */   private static final int COST = 1;
/*    */   private static final int ENERGY_AMT = 2;
/*    */   
/*    */   public Outmaneuver()
/*    */   {
/* 24 */     super("Outmaneuver", NAME, null, "green/skill/outmaneuver", 1, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.GREEN, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(com.megacrit.cardcrawl.characters.AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     if (!this.upgraded) {
/* 40 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(p, p, new EnergizedPower(p, 2), 2));
/*    */     }
/*    */     else {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(p, p, new EnergizedPower(p, 3), 3));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 50 */     return new Outmaneuver();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 55 */     if (!this.upgraded) {
/* 56 */       upgradeName();
/* 57 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 58 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\green\Outmaneuver.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */