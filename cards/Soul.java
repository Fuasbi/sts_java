/*     */ package com.megacrit.cardcrawl.cards;
/*     */ 
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.CatmullRomSpline;
/*     */ import com.badlogic.gdx.math.MathUtils;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*     */ import com.megacrit.cardcrawl.core.Settings;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*     */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*     */ import com.megacrit.cardcrawl.vfx.combat.EmpowerEffect;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ 
/*     */ public class Soul
/*     */ {
/*     */   public AbstractCard card;
/*     */   public CardGroup group;
/*  23 */   private CatmullRomSpline<Vector2> crs = new CatmullRomSpline();
/*  24 */   private ArrayList<Vector2> controlPoints = new ArrayList();
/*     */   private static final int NUM_POINTS = 20;
/*  26 */   private Vector2[] points = new Vector2[20];
/*     */   private Vector2 pos;
/*     */   private Vector2 target;
/*  29 */   private static final float VFX_INTERVAL = 0.015F; private float backUpTimer; private float vfxTimer = 0.015F;
/*     */   private static final float BACK_UP_TIME = 1.5F;
/*  31 */   private float spawnStutterTimer = 0.0F;
/*     */   private static final float STUTTER_TIME_MAX = 0.12F;
/*  33 */   private boolean isInvisible = false;
/*     */   
/*     */ 
/*  36 */   private static final float DISCARD_X = Settings.WIDTH * 0.96F;
/*  37 */   private static final float DISCARD_Y = Settings.HEIGHT * 0.06F;
/*  38 */   private static final float DRAW_PILE_X = Settings.WIDTH * 0.04F;
/*  39 */   private static final float DRAW_PILE_Y = Settings.HEIGHT * 0.06F;
/*  40 */   private static final float MASTER_DECK_X = Settings.WIDTH - 96.0F * Settings.scale;
/*  41 */   private static final float MASTER_DECK_Y = Settings.HEIGHT - 32.0F * Settings.scale;
/*     */   
/*     */ 
/*  44 */   private float currentSpeed = 0.0F;
/*  45 */   private static final float START_VELOCITY = 200.0F * Settings.scale;
/*  46 */   private static final float MAX_VELOCITY = 6000.0F * Settings.scale;
/*  47 */   private static final float VELOCITY_RAMP_RATE = 3000.0F * Settings.scale;
/*     */   public boolean isReadyForReuse;
/*  49 */   public boolean isDone; private static final float DST_THRESHOLD = 36.0F * Settings.scale;
/*  50 */   private static final float HOME_IN_THRESHOLD = 72.0F * Settings.scale;
/*     */   
/*     */   private float rotation;
/*     */   
/*  54 */   private boolean rotateClockwise = true;
/*  55 */   private boolean stopRotating = false;
/*     */   private float rotationRate;
/*  57 */   private static final float ROTATION_RATE = 150.0F * Settings.scale;
/*     */   private static final float ROTATION_RAMP_RATE = 800.0F;
/*     */   
/*     */   public Soul() {
/*  61 */     this.crs.controlPoints = new Vector2[1];
/*  62 */     this.isReadyForReuse = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void discard(AbstractCard card, boolean visualOnly)
/*     */   {
/*  71 */     this.card = card;
/*  72 */     this.group = AbstractDungeon.player.discardPile;
/*  73 */     if (!visualOnly) {
/*  74 */       this.group.addToTop(card);
/*     */     }
/*  76 */     this.pos = new Vector2(card.current_x, card.current_y);
/*  77 */     this.target = new Vector2(DISCARD_X, DISCARD_Y);
/*  78 */     setSharedVariables();
/*  79 */     this.rotation = (card.angle + 270.0F);
/*  80 */     this.rotateClockwise = false;
/*  81 */     if (Settings.FAST_MODE) {
/*  82 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(4.0F, 6.0F));
/*     */     } else {
/*  84 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(1.0F, 4.0F));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void discard(AbstractCard card)
/*     */   {
/*  94 */     discard(card, false);
/*     */   }
/*     */   
/*     */   public void shuffle(AbstractCard card, boolean isInvisible) {
/*  98 */     this.isInvisible = isInvisible;
/*  99 */     this.card = card;
/* 100 */     this.group = AbstractDungeon.player.drawPile;
/* 101 */     this.group.addToTop(card);
/* 102 */     this.pos = new Vector2(DISCARD_X, DISCARD_Y);
/* 103 */     this.target = new Vector2(DRAW_PILE_X, DRAW_PILE_Y);
/* 104 */     setSharedVariables();
/* 105 */     this.rotation = MathUtils.random(260.0F, 310.0F);
/* 106 */     if (Settings.FAST_MODE) {
/* 107 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(8.0F, 12.0F));
/*     */     } else {
/* 109 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(2.0F, 5.0F));
/*     */     }
/* 111 */     this.rotateClockwise = true;
/* 112 */     this.spawnStutterTimer = MathUtils.random(0.0F, 0.12F);
/*     */   }
/*     */   
/*     */   public void onToDeck(AbstractCard card, boolean randomSpot, boolean visualOnly) {
/* 116 */     this.card = card;
/* 117 */     this.group = AbstractDungeon.player.drawPile;
/* 118 */     if (!visualOnly) {
/* 119 */       if (randomSpot) {
/* 120 */         this.group.addToRandomSpot(card);
/*     */       } else {
/* 122 */         this.group.addToTop(card);
/*     */       }
/*     */     }
/* 125 */     this.pos = new Vector2(card.current_x, card.current_y);
/* 126 */     this.target = new Vector2(DRAW_PILE_X, DRAW_PILE_Y);
/* 127 */     setSharedVariables();
/* 128 */     this.rotation = (card.angle + 270.0F);
/* 129 */     this.rotateClockwise = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void onToDeck(AbstractCard card, boolean randomSpot)
/*     */   {
/* 139 */     onToDeck(card, randomSpot, false);
/*     */   }
/*     */   
/*     */   public void onToBottomOfDeck(AbstractCard card) {
/* 143 */     this.card = card;
/* 144 */     this.group = AbstractDungeon.player.drawPile;
/* 145 */     this.group.addToBottom(card);
/* 146 */     this.pos = new Vector2(card.current_x, card.current_y);
/* 147 */     this.target = new Vector2(DRAW_PILE_X, DRAW_PILE_Y);
/* 148 */     setSharedVariables();
/* 149 */     this.rotation = (card.angle + 270.0F);
/* 150 */     this.rotateClockwise = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void empower(AbstractCard card)
/*     */   {
/* 157 */     com.megacrit.cardcrawl.core.CardCrawlGame.sound.play("CARD_POWER_WOOSH", 0.1F);
/* 158 */     this.card = card;
/* 159 */     this.group = null;
/* 160 */     this.pos = new Vector2(card.current_x, card.current_y);
/* 161 */     this.target = new Vector2(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY);
/* 162 */     setSharedVariables();
/*     */   }
/*     */   
/*     */   public void obtain(AbstractCard card) {
/* 166 */     this.card = card;
/* 167 */     this.group = AbstractDungeon.player.masterDeck;
/* 168 */     this.group.addToTop(card);
/* 169 */     if (((Boolean)com.megacrit.cardcrawl.daily.DailyMods.negativeMods.get("Hoarder")).booleanValue()) {
/* 170 */       this.group.addToTop(card.makeStatEquivalentCopy());
/* 171 */       this.group.addToTop(card.makeStatEquivalentCopy());
/*     */     }
/* 173 */     this.pos = new Vector2(card.current_x, card.current_y);
/* 174 */     this.target = new Vector2(MASTER_DECK_X, MASTER_DECK_Y);
/* 175 */     setSharedVariables();
/*     */   }
/*     */   
/*     */   private void setSharedVariables() {
/* 179 */     this.controlPoints.clear();
/* 180 */     if (Settings.FAST_MODE) {
/* 181 */       this.rotationRate = (ROTATION_RATE * MathUtils.random(4.0F, 6.0F));
/* 182 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(1.0F, 1.5F));
/* 183 */       this.backUpTimer = 0.5F;
/*     */     } else {
/* 185 */       this.rotationRate = (ROTATION_RATE * MathUtils.random(1.0F, 2.0F));
/* 186 */       this.currentSpeed = (START_VELOCITY * MathUtils.random(0.2F, 1.0F));
/* 187 */       this.backUpTimer = 1.5F;
/*     */     }
/*     */     
/* 190 */     this.stopRotating = false;
/* 191 */     this.rotateClockwise = MathUtils.randomBoolean();
/* 192 */     this.rotation = MathUtils.random(0, 359);
/* 193 */     this.isReadyForReuse = false;
/* 194 */     this.isDone = false;
/*     */   }
/*     */   
/*     */   public void update() {
/* 198 */     this.card.update();
/* 199 */     this.card.targetAngle = (this.rotation + 90.0F);
/* 200 */     this.card.current_x = this.pos.x;
/* 201 */     this.card.current_y = this.pos.y;
/* 202 */     this.card.target_x = this.card.current_x;
/* 203 */     this.card.target_y = this.card.current_y;
/*     */     
/* 205 */     if (this.spawnStutterTimer > 0.0F) {
/* 206 */       this.spawnStutterTimer -= Gdx.graphics.getDeltaTime();
/* 207 */       return;
/*     */     }
/*     */     
/* 210 */     updateMovement();
/* 211 */     updateBackUpTimer();
/*     */     
/* 213 */     if (this.isDone)
/*     */     {
/* 215 */       if (this.group == null) {
/* 216 */         AbstractDungeon.effectList.add(new EmpowerEffect(AbstractDungeon.player.hb.cX, AbstractDungeon.player.hb.cY));
/*     */         
/* 218 */         this.isReadyForReuse = true;
/* 219 */         return;
/*     */       }
/*     */       
/* 222 */       switch (this.group.type) {
/*     */       case MASTER_DECK: 
/* 224 */         this.card.setAngle(0.0F);
/* 225 */         this.card.targetDrawScale = 0.75F;
/* 226 */         break;
/*     */       case DRAW_PILE: 
/* 228 */         this.card.targetDrawScale = 0.75F;
/* 229 */         this.card.setAngle(0.0F);
/* 230 */         this.card.lighten(false);
/* 231 */         this.card.clearPowers();
/* 232 */         AbstractDungeon.overlayMenu.combatDeckPanel.pop();
/* 233 */         break;
/*     */       case DISCARD_PILE: 
/* 235 */         this.card.targetDrawScale = 0.75F;
/* 236 */         this.card.setAngle(0.0F);
/* 237 */         this.card.lighten(false);
/* 238 */         this.card.clearPowers();
/* 239 */         this.card.teleportToDiscardPile();
/* 240 */         AbstractDungeon.overlayMenu.discardPilePanel.pop();
/* 241 */         break;
/*     */       case EXHAUST_PILE: 
/*     */         break;
/*     */       }
/*     */       
/*     */       
/* 247 */       if (AbstractDungeon.getCurrRoom().phase == com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase.COMBAT) {
/* 248 */         AbstractDungeon.player.hand.applyPowers();
/*     */       }
/* 250 */       this.isReadyForReuse = true;
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateMovement()
/*     */   {
/* 256 */     Vector2 tmp = new Vector2(this.pos.x - this.target.x, this.pos.y - this.target.y);
/* 257 */     tmp.nor();
/* 258 */     float targetAngle = tmp.angle();
/* 259 */     this.rotationRate += Gdx.graphics.getDeltaTime() * 800.0F;
/*     */     
/*     */ 
/* 262 */     if (!this.stopRotating) {
/* 263 */       if (this.rotateClockwise) {
/* 264 */         this.rotation += Gdx.graphics.getDeltaTime() * this.rotationRate;
/*     */       } else {
/* 266 */         this.rotation -= Gdx.graphics.getDeltaTime() * this.rotationRate;
/* 267 */         if (this.rotation < 0.0F) {
/* 268 */           this.rotation += 360.0F;
/*     */         }
/*     */       }
/*     */       
/* 272 */       this.rotation %= 360.0F;
/*     */       
/* 274 */       if (!this.stopRotating) {
/* 275 */         if (this.target.dst(this.pos) < HOME_IN_THRESHOLD) {
/* 276 */           this.rotation = targetAngle;
/* 277 */           this.stopRotating = true;
/* 278 */         } else if (Math.abs(this.rotation - targetAngle) < Gdx.graphics.getDeltaTime() * this.rotationRate) {
/* 279 */           this.rotation = targetAngle;
/* 280 */           this.stopRotating = true;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 286 */     tmp.setAngle(this.rotation);
/*     */     
/*     */ 
/* 289 */     tmp.x *= Gdx.graphics.getDeltaTime() * this.currentSpeed;
/* 290 */     tmp.y *= Gdx.graphics.getDeltaTime() * this.currentSpeed;
/* 291 */     this.pos.sub(tmp);
/*     */     
/* 293 */     if ((this.stopRotating) && (this.backUpTimer < 1.3499999F)) {
/* 294 */       this.currentSpeed += Gdx.graphics.getDeltaTime() * VELOCITY_RAMP_RATE * 3.0F;
/*     */     } else {
/* 296 */       this.currentSpeed += Gdx.graphics.getDeltaTime() * VELOCITY_RAMP_RATE * 1.5F;
/*     */     }
/* 298 */     if (this.currentSpeed > MAX_VELOCITY) {
/* 299 */       this.currentSpeed = MAX_VELOCITY;
/*     */     }
/*     */     
/*     */ 
/* 303 */     if ((this.target.x < Settings.WIDTH / 2.0F) && (this.pos.x < 0.0F)) {
/* 304 */       this.isDone = true;
/* 305 */     } else if ((this.target.x > Settings.WIDTH / 2.0F) && (this.pos.x > Settings.WIDTH)) {
/* 306 */       this.isDone = true;
/*     */     }
/*     */     
/* 309 */     if (this.target.dst(this.pos) < DST_THRESHOLD) {
/* 310 */       this.isDone = true;
/*     */     }
/*     */     
/* 313 */     this.vfxTimer -= Gdx.graphics.getDeltaTime();
/* 314 */     if ((!this.isDone) && (this.vfxTimer < 0.0F)) {
/* 315 */       this.vfxTimer = 0.015F;
/* 316 */       if (!this.controlPoints.isEmpty()) {
/* 317 */         if (!((Vector2)this.controlPoints.get(0)).equals(this.pos)) {
/* 318 */           this.controlPoints.add(this.pos.cpy());
/*     */         }
/*     */       } else {
/* 321 */         this.controlPoints.add(this.pos.cpy());
/*     */       }
/*     */       
/* 324 */       if (this.controlPoints.size() > 10) {
/* 325 */         this.controlPoints.remove(0);
/*     */       }
/*     */       
/* 328 */       if (this.controlPoints.size() > 3) {
/* 329 */         Vector2[] vec2Array = new Vector2[0];
/* 330 */         this.crs.set((com.badlogic.gdx.math.Vector[])this.controlPoints.toArray(vec2Array), false);
/*     */         
/* 332 */         for (int i = 0; i < 20; i++) {
/* 333 */           this.points[i] = new Vector2();
/* 334 */           Vector2 derp = (Vector2)this.crs.valueAt(this.points[i], i / 19.0F);
/* 335 */           AbstractDungeon.topLevelEffects.add(new com.megacrit.cardcrawl.vfx.CardTrailEffect(derp.x, derp.y));
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private void updateBackUpTimer() {
/* 342 */     this.backUpTimer -= Gdx.graphics.getDeltaTime();
/* 343 */     if (this.backUpTimer < 0.0F) {
/* 344 */       this.isDone = true;
/*     */     }
/*     */   }
/*     */   
/*     */   public void render(SpriteBatch sb) {
/* 349 */     if (!this.isInvisible) {
/* 350 */       this.card.renderOuterGlow(sb);
/* 351 */       this.card.render(sb);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\Soul.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */