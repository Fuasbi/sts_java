/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class GhostlyArmor extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Ghostly Armor";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Ghostly Armor");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int BLOCK_AMT = 10;
/*    */   
/*    */   public GhostlyArmor()
/*    */   {
/* 22 */     super("Ghostly Armor", NAME, "red/skill/ghostlyArmor", "red/skill/ghostlyArmor", 1, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.isEthereal = true;
/* 34 */     this.baseBlock = 10;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(p, p, this.block));
/*    */   }
/*    */   
/*    */   public void triggerOnEndOfPlayerTurn()
/*    */   {
/* 44 */     AbstractDungeon.actionManager.addToTop(new com.megacrit.cardcrawl.actions.utility.ExhaustAllEtherealAction());
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 49 */     return new GhostlyArmor();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 54 */     if (!this.upgraded) {
/* 55 */       upgradeName();
/* 56 */       upgradeBlock(3);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\GhostlyArmor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */