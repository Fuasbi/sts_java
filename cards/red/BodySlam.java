/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class BodySlam extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Body Slam";
/* 16 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Body Slam");
/* 17 */   public static final String NAME = cardStrings.NAME;
/* 18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 19 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public BodySlam()
/*    */   {
/* 24 */     super("Body Slam", NAME, "red/attack/bodySlam", "red/attack/bodySlam", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     this.baseDamage = 0;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     this.baseDamage = p.currentBlock;
/* 42 */     calculateCardDamage(m);
/* 43 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */     
/* 45 */     this.rawDescription = DESCRIPTION;
/* 46 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 51 */     this.baseDamage = AbstractDungeon.player.currentBlock;
/* 52 */     super.applyPowers();
/*    */     
/* 54 */     this.rawDescription = DESCRIPTION;
/* 55 */     this.rawDescription += UPGRADE_DESCRIPTION;
/* 56 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void onMoveToDiscard()
/*    */   {
/* 61 */     this.rawDescription = DESCRIPTION;
/* 62 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void calculateCardDamage(AbstractMonster mo)
/*    */   {
/* 67 */     super.calculateCardDamage(mo);
/* 68 */     this.rawDescription = DESCRIPTION;
/* 69 */     this.rawDescription += UPGRADE_DESCRIPTION;
/* 70 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 75 */     return new BodySlam();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 80 */     if (!this.upgraded) {
/* 81 */       upgradeName();
/* 82 */       upgradeBaseCost(0);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\BodySlam.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */