/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Clash extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Clash";
/* 15 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Clash");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   private static final int DAMAGE_AMT = 14;
/*    */   
/*    */   public Clash()
/*    */   {
/* 24 */     super("Clash", NAME, "red/attack/clash", "red/attack/clash", 0, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     this.baseDamage = 14;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 51 */     boolean canUse = super.canUse(p, m);
/* 52 */     if (!canUse) {
/* 53 */       return false;
/*    */     }
/* 55 */     for (AbstractCard c : p.hand.group) {
/* 56 */       if (c.type != AbstractCard.CardType.ATTACK) {
/* 57 */         canUse = false;
/* 58 */         this.cantUseMessage = EXTENDED_DESCRIPTION[0];
/*    */       }
/*    */     }
/*    */     
/* 62 */     return canUse;
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 67 */     return new Clash();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 72 */     if (!this.upgraded) {
/* 73 */       upgradeName();
/* 74 */       upgradeDamage(4);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\Clash.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */