/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Bash extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Bash";
/* 19 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Bash");
/* 20 */   public static final String NAME = cardStrings.NAME;
/* 21 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   private static final int ATTACK_DMG = 8;
/*    */   private static final int DEBUG_DMG = 100;
/*    */   private static final int VULNERABLE_AMT = 2;
/*    */   
/*    */   public Bash() {
/* 28 */     super("Bash", NAME, "red/attack/bash", "red/attack/bash", 2, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.BASIC, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 40 */     this.baseDamage = 8;
/* 41 */     this.baseMagicNumber = 2;
/* 42 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 47 */     if (com.megacrit.cardcrawl.core.Settings.isDebug) {
/* 48 */       this.multiDamage = new int[AbstractDungeon.getCurrRoom().monsters.monsters.size()];
/* 49 */       for (int i = 0; i < AbstractDungeon.getCurrRoom().monsters.monsters.size(); i++) {
/* 50 */         this.multiDamage[i] = 100;
/*    */       }
/* 52 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */     }
/*    */     else {
/* 55 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */     }
/*    */     
/*    */ 
/* 59 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(m, p, new com.megacrit.cardcrawl.powers.VulnerablePower(m, this.magicNumber, false), this.magicNumber));
/*    */   }
/*    */   
/*    */ 
/*    */   public void upgrade()
/*    */   {
/* 65 */     if (!this.upgraded) {
/* 66 */       upgradeName();
/* 67 */       upgradeDamage(2);
/* 68 */       upgradeMagicNumber(1);
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 74 */     return new Bash();
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\Bash.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */