/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*    */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*    */ import java.util.ArrayList;
/*    */ 
/*    */ public class Strike_Red extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Strike_R";
/* 17 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Strike_R");
/* 18 */   public static final String NAME = cardStrings.NAME;
/* 19 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int ATTACK_DMG = 6;
/*    */   
/*    */   public Strike_Red()
/*    */   {
/* 25 */     super("Strike_R", NAME, "red/attack/strike", "red/attack/strike", 1, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.BASIC, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 37 */     this.baseDamage = 6;
/*    */   }
/*    */   
/*    */   public void use(com.megacrit.cardcrawl.characters.AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 42 */     if (com.megacrit.cardcrawl.core.Settings.isDebug) {
/* 43 */       if (com.megacrit.cardcrawl.core.Settings.isInfo) {
/* 44 */         this.multiDamage = new int[AbstractDungeon.getCurrRoom().monsters.monsters.size()];
/* 45 */         for (int i = 0; i < AbstractDungeon.getCurrRoom().monsters.monsters.size(); i++) {
/* 46 */           this.multiDamage[i] = 150;
/*    */         }
/* 48 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */       }
/*    */       else {
/* 51 */         AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new DamageInfo(p, 150, this.damageTypeForTurn), AbstractGameAction.AttackEffect.BLUNT_HEAVY));
/*    */       }
/*    */     }
/*    */     else {
/* 55 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 62 */     return new Strike_Red();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 67 */     if (!this.upgraded) {
/* 68 */       upgradeName();
/* 69 */       upgradeDamage(3);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\Strike_Red.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */