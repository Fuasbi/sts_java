/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect;
/*    */ import com.megacrit.cardcrawl.vfx.combat.ShockWaveEffect.ShockWaveType;
/*    */ 
/*    */ public class Warcry extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Warcry";
/* 18 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Warcry");
/* 19 */   public static final String NAME = cardStrings.NAME;
/* 20 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 21 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   private static final int DRAW_AMT = 1;
/*    */   private static final int UPG_DRAW_AMT = 1;
/*    */   
/*    */   public Warcry()
/*    */   {
/* 28 */     super("Warcry", NAME, "red/skill/warcry", "red/skill/warCry", 0, DESCRIPTION, AbstractCard.CardType.SKILL, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 39 */     this.exhaust = true;
/* 40 */     this.baseMagicNumber = 1;
/* 41 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 46 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(p, new ShockWaveEffect(p.hb.cX, p.hb.cY, com.megacrit.cardcrawl.core.Settings.RED_TEXT_COLOR, ShockWaveEffect.ShockWaveType.ADDITIVE), 0.5F));
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 51 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DrawCardAction(p, this.magicNumber));
/* 52 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.PutOnDeckAction(p, p, 1, false));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 57 */     return new Warcry();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 62 */     if (!this.upgraded) {
/* 63 */       upgradeName();
/* 64 */       upgradeMagicNumber(1);
/* 65 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 66 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\Warcry.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */