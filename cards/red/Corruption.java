/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*    */ import com.megacrit.cardcrawl.vfx.BorderLongFlashEffect;
/*    */ import com.megacrit.cardcrawl.vfx.combat.VerticalAuraEffect;
/*    */ 
/*    */ public class Corruption extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Corruption";
/* 20 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Corruption");
/* 21 */   public static final String NAME = cardStrings.NAME;
/* 22 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int STR = 3;
/*    */   private static final int COST = 3;
/*    */   
/*    */   public Corruption()
/*    */   {
/* 28 */     super("Corruption", NAME, "red/power/corruption", "red/power/corruption", 3, DESCRIPTION, AbstractCard.CardType.POWER, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 39 */     this.baseMagicNumber = 3;
/* 40 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 45 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(p, new VerticalAuraEffect(Color.BLACK, p.hb.cX, p.hb.cY), 0.33F));
/*    */     
/* 47 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.utility.SFXAction("ATTACK_FIRE"));
/* 48 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(p, new VerticalAuraEffect(Color.PURPLE, p.hb.cX, p.hb.cY), 0.33F));
/*    */     
/* 50 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(p, new VerticalAuraEffect(Color.CYAN, p.hb.cX, p.hb.cY), 0.0F));
/*    */     
/* 52 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(p, new BorderLongFlashEffect(Color.MAGENTA), 0.0F, true));
/*    */     
/* 54 */     boolean powerExists = false;
/* 55 */     for (AbstractPower pow : p.powers) {
/* 56 */       if (pow.ID.equals("Corruption")) {
/* 57 */         powerExists = true;
/* 58 */         break;
/*    */       }
/*    */     }
/*    */     
/* 62 */     if (!powerExists) {
/* 63 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(p, p, new com.megacrit.cardcrawl.powers.CorruptionPower(p)));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 69 */     return new Corruption();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 74 */     if (!this.upgraded) {
/* 75 */       upgradeName();
/* 76 */       upgradeBaseCost(2);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\Corruption.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */