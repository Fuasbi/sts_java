/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class SearingBlow extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Searing Blow";
/* 15 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Searing Blow");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   private static final int ATTACK_DMG = 12;
/*    */   
/*    */   public SearingBlow()
/*    */   {
/* 23 */     this(0);
/*    */   }
/*    */   
/*    */   public SearingBlow(int upgrades) {
/* 27 */     super("Searing Blow", NAME, "red/attack/searingBlow", "red/attack/searingBlow", 2, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 38 */     this.baseDamage = 12;
/* 39 */     this.timesUpgraded = upgrades;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 44 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */   }
/*    */   
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 50 */     return new SearingBlow(this.timesUpgraded);
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 55 */     upgradeDamage(4 + this.timesUpgraded);
/* 56 */     this.timesUpgraded += 1;
/* 57 */     this.upgraded = true;
/* 58 */     this.name = (NAME + "+" + this.timesUpgraded);
/* 59 */     initializeTitle();
/*    */   }
/*    */   
/*    */   public boolean canUpgrade()
/*    */   {
/* 64 */     return true;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\SearingBlow.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */