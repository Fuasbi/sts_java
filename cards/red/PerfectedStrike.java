/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.DamageAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.CardGroup;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.cards.blue.Strike_Blue;
/*    */ import com.megacrit.cardcrawl.cards.green.UnderhandedStrike;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class PerfectedStrike extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Perfected Strike";
/* 21 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Perfected Strike");
/* 22 */   public static final String NAME = cardStrings.NAME;
/* 23 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 24 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   private static final int DAMAGE_AMT = 6;
/*    */   public static final int BONUS = 2;
/*    */   public static final int UPG_BONUS = 3;
/*    */   
/*    */   public PerfectedStrike()
/*    */   {
/* 32 */     super("Perfected Strike", NAME, "red/attack/perfectedStrike", "red/attack/perfectedStrike", 2, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 44 */     this.baseDamage = 6;
/* 45 */     this.baseMagicNumber = 2;
/* 46 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public static int countCards() {
/* 50 */     int count = 0;
/* 51 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/* 52 */       if (isStrike(c)) {
/* 53 */         count++;
/*    */       }
/*    */     }
/* 56 */     for (AbstractCard c : AbstractDungeon.player.drawPile.group) {
/* 57 */       if (isStrike(c)) {
/* 58 */         count++;
/*    */       }
/*    */     }
/* 61 */     for (AbstractCard c : AbstractDungeon.player.discardPile.group) {
/* 62 */       if (isStrike(c)) {
/* 63 */         count++;
/*    */       }
/*    */     }
/* 66 */     return count;
/*    */   }
/*    */   
/*    */   public static boolean isStrike(AbstractCard c) {
/* 70 */     return ((c instanceof Strike_Red)) || ((c instanceof Strike_Blue)) || ((c instanceof com.megacrit.cardcrawl.cards.colorless.SwiftStrike)) || ((c instanceof PerfectedStrike)) || ((c instanceof PommelStrike)) || ((c instanceof WildStrike)) || ((c instanceof TwinStrike)) || ((c instanceof com.megacrit.cardcrawl.cards.green.Strike_Green)) || ((c instanceof UnderhandedStrike)) || ((c instanceof com.megacrit.cardcrawl.cards.blue.MeteorStrike)) || ((c instanceof com.megacrit.cardcrawl.cards.blue.ThunderStrike));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 78 */     AbstractDungeon.actionManager.addToBottom(new DamageAction(m, new DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 87 */     return new PerfectedStrike();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 92 */     if (!this.upgraded) {
/* 93 */       upgradeName();
/* 94 */       upgradeMagicNumber(1);
/* 95 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 96 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\PerfectedStrike.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */