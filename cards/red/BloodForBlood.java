/*    */ package com.megacrit.cardcrawl.cards.red;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class BloodForBlood extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Blood for Blood";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Blood for Blood");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 4;
/*    */   private static final int DAMAGE_AMT = 18;
/*    */   
/*    */   public BloodForBlood()
/*    */   {
/* 23 */     super("Blood for Blood", NAME, "red/attack/bloodForBlood", "red/attack/bloodForBlood", 4, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.RED, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.baseDamage = 18;
/*    */   }
/*    */   
/*    */   public void tookDamage()
/*    */   {
/* 39 */     updateCost(-1);
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 44 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 53 */     AbstractCard tmp = new BloodForBlood();
/* 54 */     if (AbstractDungeon.player != null) {
/* 55 */       tmp.updateCost(-AbstractDungeon.player.damagedThisCombat);
/*    */     }
/* 57 */     return tmp;
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 62 */     if (!this.upgraded) {
/* 63 */       upgradeName();
/* 64 */       if (this.cost < 4) {
/* 65 */         upgradeBaseCost(this.cost - 1);
/* 66 */         if (this.cost < 0) {
/* 67 */           this.cost = 0;
/*    */         }
/*    */       }
/*    */       else {
/* 71 */         upgradeBaseCost(3);
/*    */       }
/* 73 */       upgradeDamage(4);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\red\BloodForBlood.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */