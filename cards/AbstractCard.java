/*      */ package com.megacrit.cardcrawl.cards;
/*      */ 
/*      */ import com.badlogic.gdx.Files;
/*      */ import com.badlogic.gdx.Gdx;
/*      */ import com.badlogic.gdx.Graphics;
/*      */ import com.badlogic.gdx.graphics.Color;
/*      */ import com.badlogic.gdx.graphics.Texture;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*      */ import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
/*      */ import com.badlogic.gdx.graphics.g2d.GlyphLayout;
/*      */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*      */ import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
/*      */ import com.badlogic.gdx.math.MathUtils;
/*      */ import com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect;
/*      */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*      */ import com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction;
/*      */ import com.megacrit.cardcrawl.actions.common.LoseHPAction;
/*      */ import com.megacrit.cardcrawl.blights.AbstractBlight;
/*      */ import com.megacrit.cardcrawl.cards.red.HeavyBlade;
/*      */ import com.megacrit.cardcrawl.cards.red.PerfectedStrike;
/*      */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*      */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*      */ import com.megacrit.cardcrawl.core.ExceptionHandler;
/*      */ import com.megacrit.cardcrawl.core.OverlayMenu;
/*      */ import com.megacrit.cardcrawl.core.Settings;
/*      */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*      */ import com.megacrit.cardcrawl.helpers.CardHelper;
/*      */ import com.megacrit.cardcrawl.helpers.FontHelper;
/*      */ import com.megacrit.cardcrawl.helpers.GameDataStringBuilder;
/*      */ import com.megacrit.cardcrawl.helpers.GameDictionary;
/*      */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*      */ import com.megacrit.cardcrawl.helpers.ImageMaster;
/*      */ import com.megacrit.cardcrawl.helpers.MathHelper;
/*      */ import com.megacrit.cardcrawl.helpers.TipHelper;
/*      */ import com.megacrit.cardcrawl.helpers.input.InputHelper;
/*      */ import com.megacrit.cardcrawl.localization.LocalizedStrings;
/*      */ import com.megacrit.cardcrawl.localization.UIStrings;
/*      */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*      */ import com.megacrit.cardcrawl.monsters.MonsterGroup;
/*      */ import com.megacrit.cardcrawl.powers.AbstractPower;
/*      */ import com.megacrit.cardcrawl.powers.StrengthPower;
/*      */ import com.megacrit.cardcrawl.relics.AbstractRelic;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom;
/*      */ import com.megacrit.cardcrawl.rooms.AbstractRoom.RoomPhase;
/*      */ import com.megacrit.cardcrawl.screens.SingleCardViewPopup;
/*      */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*      */ import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
/*      */ import com.megacrit.cardcrawl.vfx.cardManip.CardDarkFlashVfx;
/*      */ import com.megacrit.cardcrawl.vfx.cardManip.CardFlashVfx;
/*      */ import com.megacrit.cardcrawl.vfx.cardManip.CardGlowBorder;
/*      */ import java.io.PrintStream;
/*      */ import java.io.Serializable;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Arrays;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import java.util.Scanner;
/*      */ import java.util.TreeMap;
/*      */ import org.apache.logging.log4j.LogManager;
/*      */ import org.apache.logging.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ public abstract class AbstractCard
/*      */   implements Comparable<AbstractCard>
/*      */ {
/*   68 */   private static final Logger logger = LogManager.getLogger(AbstractCard.class.getName());
/*      */   public CardType type;
/*      */   public int cost;
/*   71 */   public int costForTurn; public int price; public int chargeCost = -1;
/*   72 */   public boolean isCostModified = false;
/*   73 */   public boolean isCostModifiedForTurn = false;
/*   74 */   public boolean retain = false; public boolean dontTriggerOnUseCard = false;
/*      */   public CardRarity rarity;
/*      */   public CardColor color;
/*   77 */   public boolean isInnate = false;
/*   78 */   public boolean isLocked = false;
/*   79 */   public boolean showEvokeValue = false;
/*   80 */   public int showEvokeOrbCount = 0;
/*      */   
/*      */ 
/*   83 */   public ArrayList<String> keywords = new ArrayList();
/*      */   private static final int COMMON_CARD_PRICE = 50;
/*      */   private static final int UNCOMMON_CARD_PRICE = 75;
/*      */   private static final int RARE_CARD_PRICE = 150;
/*   87 */   public boolean isSelected = false; public boolean exhaust = false;
/*   88 */   public boolean isEthereal = false;
/*   89 */   protected boolean isUsed = false;
/*   90 */   public boolean upgraded = false;
/*   91 */   public int timesUpgraded = 0; public int misc = 0;
/*   92 */   public int energyOnUse; public boolean isSeen = true;
/*      */   
/*   94 */   public boolean upgradedCost = false; public boolean upgradedDamage = false; public boolean upgradedBlock = false; public boolean upgradedMagicNumber = false;
/*      */   
/*      */   public int[] multiDamage;
/*      */   
/*   98 */   protected boolean isMultiDamage = false;
/*   99 */   public int baseDamage = -1; public int baseBlock = -1; public int baseMagicNumber = -1; public int baseHeal = -1; public int baseDraw = -1; public int baseDiscard = -1;
/*  100 */   public int damage = -1; public int block = -1; public int magicNumber = -1; public int heal = -1; public int draw = -1; public int discard = -1;
/*  101 */   public boolean isDamageModified = false;
/*  102 */   public boolean isBlockModified = false;
/*  103 */   public boolean isMagicNumberModified = false;
/*      */   protected DamageInfo.DamageType damageType;
/*      */   public DamageInfo.DamageType damageTypeForTurn;
/*  106 */   public CardTarget target = CardTarget.ENEMY;
/*  107 */   public boolean purgeOnUse = false; public boolean exhaustOnUseOnce = false;
/*  108 */   public boolean exhaustOnFire = false;
/*  109 */   public boolean freeToPlayOnce = false;
/*      */   private static TextureAtlas orbAtlas;
/*      */   private static TextureAtlas cardAtlas;
/*      */   private static TextureAtlas oldCardAtlas;
/*  113 */   public static TextureAtlas.AtlasRegion orb_red; public static TextureAtlas.AtlasRegion orb_green; public static TextureAtlas.AtlasRegion orb_blue; private TextureAtlas.AtlasRegion portrait; private TextureAtlas.AtlasRegion jokePortrait; private static final UIStrings uiStrings = CardCrawlGame.languagePack.getUIString("SingleCardViewPopup");
/*  114 */   public static final String[] TEXT = uiStrings.TEXT;
/*      */   
/*      */   public AbstractGameEffect flashVfx;
/*      */   
/*      */   public String assetURL;
/*      */   public String jokeAssetURL;
/*  120 */   public boolean fadingOut = false;
/*  121 */   public float transparency = 1.0F;
/*  122 */   public float targetTransparency = 1.0F;
/*  123 */   private Color goldColor = Settings.GOLD_COLOR.cpy();
/*  124 */   private Color renderColor = Color.WHITE.cpy();
/*  125 */   private Color textColor = Settings.CREAM_COLOR.cpy();
/*  126 */   public float targetAngle = 0.0F;
/*      */   private static final float NAME_OFFSET_Y = 175.0F;
/*      */   private static final float ENERGY_TEXT_OFFSET_X = -132.0F;
/*  129 */   private static final float ENERGY_TEXT_OFFSET_Y = 192.0F; private static final int W = 512; public float angle = 0.0F;
/*  130 */   private ArrayList<CardGlowBorder> glowList = new ArrayList();
/*  131 */   private float glowTimer = 0.0F;
/*  132 */   public boolean isGlowing = false;
/*      */   public static final float SMALL_SCALE = 0.7F;
/*      */   public static final int RAW_W = 300;
/*  135 */   public static final int RAW_H = 420; public static final float IMG_WIDTH = 300.0F * Settings.scale; public static final float IMG_HEIGHT = 420.0F * Settings.scale;
/*  136 */   public static final float IMG_WIDTH_S = 300.0F * Settings.scale * 0.7F; public static final float IMG_HEIGHT_S = 420.0F * Settings.scale * 0.7F;
/*      */   
/*  138 */   private static final float SHADOW_OFFSET_X = 18.0F * Settings.scale;
/*  139 */   private static final float SHADOW_OFFSET_Y = 14.0F * Settings.scale;
/*      */   public float current_x;
/*  141 */   public float current_y; public float target_x; public float target_y; protected Texture portraitImg = null;
/*  142 */   private boolean useSmallTitleFont = false;
/*  143 */   public float drawScale = 0.7F; public float targetDrawScale = 0.7F;
/*      */   private static final int PORTRAIT_WIDTH = 250;
/*      */   private static final int PORTRAIT_HEIGHT = 190;
/*      */   private static final float PORTRAIT_OFFSET_Y = 72.0F;
/*  147 */   private static final float LINE_SPACING = 1.45F; public boolean isFlipped = false;
/*  148 */   public Hitbox hb = new Hitbox(IMG_WIDTH_S, IMG_HEIGHT_S);
/*  149 */   private boolean darken = false;
/*  150 */   private float darkTimer = 0.0F;
/*      */   
/*      */   private static final float DARKEN_TIME = 0.3F;
/*      */   
/*  154 */   private static final float HB_W = 300.0F * Settings.scale; private static final float HB_H = 420.0F * Settings.scale;
/*  155 */   public float hoverTimer = 0.0F;
/*  156 */   private boolean renderTip = false; private boolean hovered = false;
/*  157 */   private float hoverDuration = 0.0F;
/*      */   private static final float HOVER_TIP_TIME = 0.2F;
/*  159 */   private static final GlyphLayout gl = new GlyphLayout();
/*      */   
/*      */ 
/*  162 */   public float newGlowTimer = 0.0F;
/*      */   private static Scanner scanner;
/*      */   public String originalName;
/*      */   public String name;
/*      */   public String rawDescription;
/*  167 */   public String cardID; public ArrayList<DescriptionLine> description = new ArrayList();
/*      */   public String cantUseMessage;
/*      */   private static final float TYPE_OFFSET_Y = -1.0F;
/*  170 */   private static final float DESC_OFFSET_Y = IMG_HEIGHT * 0.255F;
/*  171 */   private static final float DESC_OFFSET_Y2 = -6.0F; private static final float DESC_BOX_WIDTH = IMG_WIDTH * 0.79F;
/*  172 */   private static final float CN_DESC_BOX_WIDTH = IMG_WIDTH * 0.72F;
/*  173 */   private static final float TITLE_BOX_WIDTH = IMG_WIDTH * 0.6F;
/*  174 */   private static final float TITLE_BOX_WIDTH_NO_COST = IMG_WIDTH * 0.7F;
/*  175 */   private static final float CARD_ENERGY_IMG_WIDTH = 24.0F * Settings.scale;
/*  176 */   private static final UIStrings cardRenderStrings = CardCrawlGame.languagePack.getUIString("AbstractCard");
/*  177 */   public static final String LOCKED_STRING = cardRenderStrings.TEXT[0];
/*  178 */   public static final String UNKNOWN_STRING = cardRenderStrings.TEXT[1];
/*      */   private Color bgColor;
/*      */   private Color backColor;
/*      */   private Color frameColor;
/*      */   private Color frameOutlineColor;
/*  183 */   private Color frameShadowColor; private Color imgFrameColor; private Color descBoxColor; private Color bannerColor; private Color tintColor; private static final Color ENERGY_COST_RESTRICTED_COLOR = new Color(1.0F, 0.3F, 0.3F, 1.0F);
/*  184 */   private static final Color ENERGY_COST_MODIFIED_COLOR = new Color(0.4F, 1.0F, 0.4F, 1.0F);
/*  185 */   private static final Color FRAME_SHADOW_COLOR = new Color(0.0F, 0.0F, 0.0F, 0.25F);
/*  186 */   private static final Color IMG_FRAME_COLOR_COMMON = CardHelper.getColor(53.0F, 58.0F, 64.0F);
/*  187 */   private static final Color IMG_FRAME_COLOR_UNCOMMON = CardHelper.getColor(119.0F, 152.0F, 161.0F);
/*  188 */   private static final Color IMG_FRAME_COLOR_RARE = new Color(0.855F, 0.557F, 0.32F, 1.0F);
/*      */   
/*  190 */   private static final Color HOVER_IMG_COLOR = new Color(1.0F, 0.815F, 0.314F, 0.8F);
/*  191 */   private static final Color SELECTED_CARD_COLOR = new Color(0.5F, 0.9F, 0.9F, 1.0F);
/*  192 */   private static final Color BANNER_COLOR_COMMON = CardHelper.getColor(131.0F, 129.0F, 121.0F);
/*  193 */   private static final Color BANNER_COLOR_UNCOMMON = CardHelper.getColor(142.0F, 196.0F, 213.0F);
/*  194 */   private static final Color BANNER_COLOR_RARE = new Color(1.0F, 0.796F, 0.251F, 1.0F);
/*      */   
/*      */ 
/*  197 */   private static final Color CURSE_BG_COLOR = CardHelper.getColor(29.0F, 29.0F, 29.0F);
/*  198 */   private static final Color CURSE_TYPE_BACK_COLOR = new Color(0.23F, 0.23F, 0.23F, 1.0F);
/*  199 */   private static final Color CURSE_FRAME_COLOR = CardHelper.getColor(21.0F, 2.0F, 21.0F);
/*  200 */   private static final Color CURSE_DESC_BOX_COLOR = CardHelper.getColor(52.0F, 58.0F, 64.0F);
/*      */   
/*      */ 
/*  203 */   private static final Color COLORLESS_BG_COLOR = new Color(0.15F, 0.15F, 0.15F, 1.0F);
/*  204 */   private static final Color COLORLESS_TYPE_BACK_COLOR = new Color(0.23F, 0.23F, 0.23F, 1.0F);
/*  205 */   private static final Color COLORLESS_FRAME_COLOR = new Color(0.48F, 0.48F, 0.48F, 1.0F);
/*  206 */   private static final Color COLORLESS_DESC_BOX_COLOR = new Color(0.351F, 0.363F, 0.3745F, 1.0F);
/*      */   
/*      */ 
/*  209 */   private static final Color RED_BG_COLOR = CardHelper.getColor(50.0F, 26.0F, 26.0F);
/*  210 */   private static final Color RED_TYPE_BACK_COLOR = CardHelper.getColor(91.0F, 43.0F, 32.0F);
/*  211 */   private static final Color RED_FRAME_COLOR = CardHelper.getColor(121.0F, 12.0F, 28.0F);
/*  212 */   private static final Color RED_RARE_OUTLINE_COLOR = new Color(1.0F, 0.75F, 0.43F, 1.0F);
/*  213 */   private static final Color RED_DESC_BOX_COLOR = CardHelper.getColor(53.0F, 58.0F, 64.0F);
/*      */   
/*      */ 
/*  216 */   private static final Color GREEN_BG_COLOR = CardHelper.getColor(19.0F, 45.0F, 40.0F);
/*  217 */   private static final Color GREEN_TYPE_BACK_COLOR = CardHelper.getColor(32.0F, 91.0F, 43.0F);
/*  218 */   private static final Color GREEN_FRAME_COLOR = CardHelper.getColor(52.0F, 123.0F, 8.0F);
/*  219 */   private static final Color GREEN_RARE_OUTLINE_COLOR = new Color(1.0F, 0.75F, 0.43F, 1.0F);
/*  220 */   private static final Color GREEN_DESC_BOX_COLOR = CardHelper.getColor(53.0F, 58.0F, 64.0F);
/*      */   
/*      */ 
/*  223 */   private static final Color BLUE_BG_COLOR = CardHelper.getColor(19.0F, 45.0F, 40.0F);
/*  224 */   private static final Color BLUE_TYPE_BACK_COLOR = CardHelper.getColor(32.0F, 91.0F, 43.0F);
/*  225 */   private static final Color BLUE_FRAME_COLOR = CardHelper.getColor(52.0F, 123.0F, 8.0F);
/*  226 */   private static final Color BLUE_RARE_OUTLINE_COLOR = new Color(1.0F, 0.75F, 0.43F, 1.0F);
/*  227 */   private static final Color BLUE_DESC_BOX_COLOR = CardHelper.getColor(53.0F, 58.0F, 64.0F);
/*      */   
/*      */ 
/*  230 */   public boolean inBottleFlame = false; public boolean inBottleLightning = false; public boolean inBottleTornado = false;
/*      */   
/*      */   public static enum CardTarget {
/*  233 */     ENEMY,  ALL_ENEMY,  SELF,  NONE,  SELF_AND_ENEMY,  ALL;
/*      */     
/*      */     private CardTarget() {} }
/*      */   
/*  237 */   public static enum CardColor { RED,  GREEN,  BLUE,  COLORLESS,  CURSE;
/*      */     
/*      */     private CardColor() {} }
/*      */   
/*  241 */   public static enum CardRarity { BASIC,  SPECIAL,  COMMON,  UNCOMMON,  RARE,  CURSE;
/*      */     
/*      */     private CardRarity() {} }
/*      */   
/*  245 */   public static enum CardType { ATTACK,  SKILL,  POWER,  STATUS,  CURSE;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     private CardType() {}
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AbstractCard(String id, String name, String jokeUrl, String imgUrl, int cost, String rawDescription, CardType type, CardColor color, CardRarity rarity, CardTarget target)
/*      */   {
/*  259 */     this(id, name, jokeUrl, imgUrl, cost, rawDescription, type, color, rarity, target, DamageInfo.DamageType.NORMAL);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AbstractCard(String id, String name, String jokeUrl, String imgUrl, int cost, String rawDescription, CardType type, CardColor color, CardRarity rarity, CardTarget target, DamageInfo.DamageType dType)
/*      */   {
/*  275 */     this.originalName = name;
/*  276 */     this.name = name;
/*  277 */     this.cardID = id;
/*      */     
/*      */ 
/*  280 */     this.portrait = cardAtlas.findRegion(imgUrl);
/*  281 */     if ((this.portrait == null) && (Settings.isBeta)) {
/*  282 */       this.portrait = oldCardAtlas.findRegion(jokeUrl);
/*  283 */       if (this.portrait == null) {
/*  284 */         this.portrait = cardAtlas.findRegion("status/beta");
/*      */       }
/*      */     }
/*      */     
/*  288 */     if (this.portrait == null) {
/*  289 */       if (imgUrl != null) {
/*  290 */         logger.info(this.originalName + " url was not found.");
/*      */       }
/*  292 */       this.portrait = cardAtlas.findRegion("status/beta");
/*  293 */       this.portrait = oldCardAtlas.findRegion(jokeUrl);
/*      */     }
/*  295 */     this.jokePortrait = oldCardAtlas.findRegion(jokeUrl);
/*      */     
/*      */ 
/*  298 */     if ((this.portrait == null) && 
/*  299 */       (jokeUrl != null)) {
/*  300 */       logger.info(this.originalName + " joke url was not found.");
/*      */     }
/*      */     
/*      */ 
/*  304 */     if (imgUrl != null) {
/*  305 */       this.assetURL = imgUrl;
/*      */     }
/*  307 */     if (jokeUrl != null) {
/*  308 */       this.jokeAssetURL = jokeUrl;
/*      */     }
/*      */     
/*  311 */     this.cost = cost;
/*  312 */     this.costForTurn = cost;
/*  313 */     this.rawDescription = rawDescription;
/*  314 */     this.type = type;
/*  315 */     this.color = color;
/*  316 */     this.rarity = rarity;
/*  317 */     this.target = target;
/*  318 */     this.damageType = dType;
/*  319 */     this.damageTypeForTurn = dType;
/*  320 */     createCardImage();
/*      */     
/*  322 */     if ((name == null) || (rawDescription == null)) {
/*  323 */       logger.info("Card initialized incorrecty");
/*      */     }
/*      */     
/*  326 */     initializeTitle();
/*  327 */     initializeDescription();
/*  328 */     updateTransparency();
/*      */   }
/*      */   
/*      */   public static void initialize() {
/*  332 */     long startTime = System.currentTimeMillis();
/*  333 */     cardAtlas = new TextureAtlas(Gdx.files.internal("cards/cards.atlas"));
/*  334 */     oldCardAtlas = new TextureAtlas(Gdx.files.internal("oldCards/cards.atlas"));
/*  335 */     orbAtlas = new TextureAtlas(Gdx.files.internal("orbs/orb.atlas"));
/*  336 */     orb_red = orbAtlas.findRegion("red");
/*  337 */     orb_green = orbAtlas.findRegion("green");
/*  338 */     orb_blue = orbAtlas.findRegion("blue");
/*  339 */     logger.info("Card Image load time: " + (System.currentTimeMillis() - startTime) + "ms");
/*      */   }
/*      */   
/*      */   protected void initializeTitle() {
/*  343 */     FontHelper.cardTitleFont_L.getData().setScale(1.0F);
/*  344 */     GlyphLayout gl = new GlyphLayout(FontHelper.cardTitleFont_L, this.name, new Color(), 0.0F, 1, false);
/*  345 */     if ((this.cost > 0) || (this.cost == -1)) {
/*  346 */       if (gl.width > TITLE_BOX_WIDTH) {
/*  347 */         this.useSmallTitleFont = true;
/*      */       }
/*      */     }
/*  350 */     else if (gl.width > TITLE_BOX_WIDTH_NO_COST) {
/*  351 */       this.useSmallTitleFont = true;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void initializeDescription()
/*      */   {
/*  361 */     this.keywords.clear();
/*  362 */     if (Settings.lineBreakViaCharacter) {
/*  363 */       initializeDescriptionCN();
/*  364 */       return;
/*      */     }
/*      */     
/*  367 */     this.description.clear();
/*  368 */     int numLines = 1;
/*      */     
/*      */ 
/*  371 */     scanner = new Scanner(this.rawDescription);
/*      */     
/*  373 */     StringBuilder currentLine = new StringBuilder("");
/*  374 */     float currentWidth = 0.0F;
/*      */     
/*      */ 
/*  377 */     while (scanner.hasNext()) {
/*  378 */       boolean isKeyword = false;
/*  379 */       String word = scanner.next();
/*      */       
/*      */ 
/*  382 */       StringBuilder lastChar = new StringBuilder(" ");
/*  383 */       if ((word.charAt(word.length() - 1) != ']') && (!Character.isLetterOrDigit(word.charAt(word.length() - 1)))) {
/*  384 */         lastChar.insert(0, word.charAt(word.length() - 1));
/*  385 */         word = word.substring(0, word.length() - 1);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  390 */       String keywordTmp = word.toLowerCase();
/*  391 */       keywordTmp = dedupeKeyword(keywordTmp);
/*  392 */       GlyphLayout gl; if (GameDictionary.keywords.containsKey(keywordTmp)) {
/*  393 */         if (!this.keywords.contains(keywordTmp)) {
/*  394 */           this.keywords.add(keywordTmp);
/*      */         }
/*      */         
/*  397 */         GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, lastChar);
/*  398 */         float tmp = gl.width;
/*  399 */         gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*  400 */         gl.width += tmp;
/*  401 */         isKeyword = true;
/*      */       }
/*      */       else {
/*  404 */         if ((word.equals("[R]")) || (word.equals("[G]")) || (word.equals("[B]"))) {
/*  405 */           GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, lastChar);
/*  406 */           gl.width += CARD_ENERGY_IMG_WIDTH;
/*      */         }
/*  408 */         switch (this.color) {
/*      */         case RED: 
/*  410 */           if (!this.keywords.contains("[R]")) {
/*  411 */             this.keywords.add("[R]");
/*      */           }
/*      */           break;
/*      */         case GREEN: 
/*  415 */           if (!this.keywords.contains("[G]")) {
/*  416 */             this.keywords.add("[G]");
/*      */           }
/*      */           break;
/*      */         case BLUE: 
/*  420 */           if (!this.keywords.contains("[B]")) {
/*  421 */             this.keywords.add("[B]");
/*      */           }
/*      */           break;
/*      */         default: 
/*  425 */           logger.info("ERROR: Tried to display an invalid energy type");
/*  426 */           break;
/*      */           
/*      */           GlyphLayout gl;
/*      */           
/*  430 */           if (word.equals("!D")) {
/*  431 */             gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*      */           } else {
/*      */             GlyphLayout gl;
/*  434 */             if (word.equals("!B")) {
/*  435 */               gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*      */             } else {
/*      */               GlyphLayout gl;
/*  438 */               if (word.equals("!M")) {
/*  439 */                 gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*      */ 
/*      */               }
/*  442 */               else if (word.equals("NL")) {
/*  443 */                 GlyphLayout gl = new GlyphLayout();
/*  444 */                 gl.width = 0.0F;
/*  445 */                 word = "";
/*  446 */                 this.description.add(new DescriptionLine(currentLine.toString().trim(), currentWidth));
/*  447 */                 currentWidth = 0.0F;
/*  448 */                 numLines++;
/*  449 */                 currentLine = new StringBuilder("");
/*      */               }
/*      */               else
/*      */               {
/*  453 */                 gl = new GlyphLayout(FontHelper.cardDescFont_N, word + lastChar);
/*      */               }
/*      */             } }
/*      */           break; } }
/*  457 */       if (currentWidth + gl.width > DESC_BOX_WIDTH) {
/*  458 */         this.description.add(new DescriptionLine(currentLine.toString().trim(), currentWidth));
/*  459 */         numLines++;
/*  460 */         currentLine = new StringBuilder("");
/*  461 */         currentWidth = gl.width;
/*      */       } else {
/*  463 */         currentWidth += gl.width;
/*      */       }
/*      */       
/*  466 */       if (isKeyword) {
/*  467 */         currentLine.append('*');
/*      */       }
/*  469 */       currentLine.append(word).append(lastChar.toString());
/*      */     }
/*      */     
/*  472 */     String lastLine = currentLine.toString().trim();
/*  473 */     if (!lastLine.isEmpty()) {
/*  474 */       this.description.add(new DescriptionLine(lastLine.trim(), currentWidth));
/*      */     }
/*      */     
/*  477 */     if (numLines > 5) {
/*  478 */       logger.info("WARNING: Card " + this.name + " has lots of text");
/*      */     }
/*  480 */     scanner.close();
/*      */   }
/*      */   
/*      */   public void initializeDescriptionCN() {
/*  484 */     this.description.clear();
/*  485 */     int numLines = 1;
/*      */     
/*      */ 
/*  488 */     scanner = new Scanner(this.rawDescription);
/*      */     
/*  490 */     StringBuilder currentLine = new StringBuilder("");
/*  491 */     float currentWidth = 0.0F;
/*      */     
/*  493 */     while (scanner.hasNext()) {
/*  494 */       String word = scanner.next();
/*  495 */       word = word.trim();
/*      */       
/*  497 */       if (!word.contains("NL"))
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  503 */         String keywordTmp = word.toLowerCase();
/*  504 */         keywordTmp = dedupeKeyword(keywordTmp);
/*      */         
/*  506 */         if (GameDictionary.keywords.containsKey(keywordTmp)) {
/*  507 */           if (!this.keywords.contains(keywordTmp)) {
/*  508 */             this.keywords.add(keywordTmp);
/*      */           }
/*      */           
/*  511 */           GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*  512 */           if (currentWidth + gl.width > CN_DESC_BOX_WIDTH) {
/*  513 */             numLines++;
/*  514 */             this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  515 */             currentLine = new StringBuilder("");
/*  516 */             currentWidth = gl.width;
/*  517 */             currentLine.append(" *").append(word).append(" ");
/*      */           } else {
/*  519 */             currentLine.append(" *").append(word).append(" ");
/*  520 */             currentWidth += gl.width;
/*      */           }
/*      */           
/*      */         }
/*  524 */         else if ((word.equals("[R]")) || (word.equals("[G]")) || (word.equals("[B]"))) {
/*  525 */           switch (this.color) {
/*      */           case RED: 
/*  527 */             if (!this.keywords.contains("[R]")) {
/*  528 */               this.keywords.add("[R]");
/*      */             }
/*      */             break;
/*      */           case GREEN: 
/*  532 */             if (!this.keywords.contains("[G]")) {
/*  533 */               this.keywords.add("[G]");
/*      */             }
/*      */             break;
/*      */           case BLUE: 
/*  537 */             if (!this.keywords.contains("[B]")) {
/*  538 */               this.keywords.add("[B]");
/*      */             }
/*      */             break;
/*      */           default: 
/*  542 */             logger.info("ERROR: Tried to display an invalid energy type");
/*      */           }
/*      */           
/*      */           
/*  546 */           if (currentWidth + CARD_ENERGY_IMG_WIDTH > CN_DESC_BOX_WIDTH) {
/*  547 */             numLines++;
/*  548 */             this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  549 */             currentLine = new StringBuilder("");
/*  550 */             currentWidth = CARD_ENERGY_IMG_WIDTH;
/*  551 */             currentLine.append(" ").append(word).append(" ");
/*      */           } else {
/*  553 */             currentLine.append(" ").append(word).append(" ");
/*  554 */             currentWidth += CARD_ENERGY_IMG_WIDTH;
/*      */           }
/*      */           
/*      */         }
/*  558 */         else if (word.equals("!D!")) {
/*  559 */           GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*  560 */           if (currentWidth + gl.width > CN_DESC_BOX_WIDTH) {
/*  561 */             numLines++;
/*  562 */             this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  563 */             currentLine = new StringBuilder("");
/*  564 */             currentWidth = gl.width;
/*  565 */             currentLine.append(" D ");
/*      */           } else {
/*  567 */             currentLine.append(" D ");
/*  568 */             currentWidth += gl.width;
/*      */           }
/*      */           
/*      */         }
/*  572 */         else if (word.equals("!B!")) {
/*  573 */           GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*  574 */           if (currentWidth + gl.width > CN_DESC_BOX_WIDTH) {
/*  575 */             numLines++;
/*  576 */             this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  577 */             currentLine = new StringBuilder("");
/*  578 */             currentWidth = gl.width;
/*  579 */             currentLine.append(" ").append(word).append("! ");
/*      */           } else {
/*  581 */             currentLine.append(" ").append(word).append("! ");
/*  582 */             currentWidth += gl.width;
/*      */           }
/*      */           
/*      */         }
/*  586 */         else if (word.equals("!M!")) {
/*  587 */           GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, word);
/*  588 */           if (currentWidth + gl.width > CN_DESC_BOX_WIDTH) {
/*  589 */             numLines++;
/*  590 */             this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  591 */             currentLine = new StringBuilder("");
/*  592 */             currentWidth = gl.width;
/*  593 */             currentLine.append(" ").append(word).append("! ");
/*      */           } else {
/*  595 */             currentLine.append(" ").append(word).append("! ");
/*  596 */             currentWidth += gl.width;
/*      */           }
/*      */         }
/*      */         else
/*      */         {
/*  601 */           word = word.trim();
/*  602 */           for (char c : word.toCharArray()) {
/*  603 */             GlyphLayout gl = new GlyphLayout(FontHelper.cardDescFont_N, Character.toString(c));
/*  604 */             currentLine.append(c);
/*      */             
/*  606 */             if (currentWidth + gl.width > CN_DESC_BOX_WIDTH) {
/*  607 */               numLines++;
/*  608 */               this.description.add(new DescriptionLine(currentLine.toString(), currentWidth));
/*  609 */               currentLine = new StringBuilder("");
/*  610 */               currentWidth = gl.width;
/*      */             } else {
/*  612 */               currentWidth += gl.width;
/*      */             }
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*  618 */     String lastLine = currentLine.toString();
/*  619 */     if (!lastLine.isEmpty()) {
/*  620 */       this.description.add(new DescriptionLine(lastLine, currentWidth));
/*      */     }
/*      */     
/*      */ 
/*  624 */     if (lastLine.equals("。")) {
/*  625 */       this.description.set(this.description
/*  626 */         .size() - 2, new DescriptionLine(
/*      */         
/*  628 */         ((DescriptionLine)this.description.get(this.description.size() - 2)).text += "。", 
/*  629 */         ((DescriptionLine)this.description.get(this.description.size() - 2)).width));
/*  630 */       this.description.remove(this.description.size() - 1);
/*      */     }
/*      */     
/*  633 */     if (numLines > 5) {
/*  634 */       logger.info("WARNING: Card " + this.name + " has lots of text");
/*      */     }
/*  636 */     scanner.close();
/*      */   }
/*      */   
/*      */   public boolean canUpgrade() {
/*  640 */     if (this.type == CardType.CURSE) {
/*  641 */       return false;
/*      */     }
/*  643 */     if (this.type == CardType.STATUS) {
/*  644 */       return false;
/*      */     }
/*  646 */     if (this.upgraded) {
/*  647 */       return false;
/*      */     }
/*  649 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */   public abstract void upgrade();
/*      */   
/*      */ 
/*      */   public void displayUpgrades()
/*      */   {
/*  658 */     if (this.upgradedCost) {
/*  659 */       this.isCostModified = true;
/*      */     }
/*      */     
/*  662 */     if (this.upgradedDamage) {
/*  663 */       this.damage = this.baseDamage;
/*  664 */       this.isDamageModified = true;
/*      */     }
/*      */     
/*  667 */     if (this.upgradedBlock) {
/*  668 */       this.block = this.baseBlock;
/*  669 */       this.isBlockModified = true;
/*      */     }
/*      */     
/*  672 */     if (this.upgradedMagicNumber) {
/*  673 */       this.magicNumber = this.baseMagicNumber;
/*  674 */       this.isMagicNumberModified = true;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void upgradeDamage(int amount)
/*      */   {
/*  684 */     this.baseDamage += amount;
/*  685 */     this.upgradedDamage = true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void upgradeBlock(int amount)
/*      */   {
/*  694 */     this.baseBlock += amount;
/*  695 */     this.upgradedBlock = true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void upgradeMagicNumber(int amount)
/*      */   {
/*  704 */     this.baseMagicNumber += amount;
/*  705 */     this.magicNumber = this.baseMagicNumber;
/*  706 */     this.upgradedMagicNumber = true;
/*      */   }
/*      */   
/*      */   protected void upgradeName() {
/*  710 */     this.timesUpgraded += 1;
/*  711 */     this.upgraded = true;
/*  712 */     this.name += "+";
/*  713 */     initializeTitle();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   protected void upgradeBaseCost(int newBaseCost)
/*      */   {
/*  722 */     int diff = this.cost - this.costForTurn;
/*  723 */     int baseDiff = newBaseCost - this.cost;
/*  724 */     this.cost += baseDiff;
/*  725 */     if (this.cost < 0) {
/*  726 */       this.cost = 0;
/*      */     }
/*  728 */     if (this.costForTurn > 0) {
/*  729 */       this.costForTurn = (this.cost - diff);
/*      */     }
/*  731 */     this.upgradedCost = true;
/*      */   }
/*      */   
/*      */   private String dedupeKeyword(String keyword) {
/*  735 */     String retVal = (String)GameDictionary.parentWord.get(keyword);
/*  736 */     if (retVal != null) {
/*  737 */       return retVal;
/*      */     }
/*  739 */     return keyword;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AbstractCard(AbstractCard c)
/*      */   {
/*  748 */     this.name = c.name;
/*  749 */     this.rawDescription = c.rawDescription;
/*  750 */     this.cost = c.cost;
/*  751 */     this.type = c.type;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void createCardImage()
/*      */   {
/*  758 */     switch (this.color) {
/*      */     case CURSE: 
/*  760 */       this.bgColor = CURSE_BG_COLOR.cpy();
/*  761 */       this.backColor = CURSE_TYPE_BACK_COLOR.cpy();
/*  762 */       this.frameColor = CURSE_FRAME_COLOR.cpy();
/*  763 */       this.descBoxColor = CURSE_DESC_BOX_COLOR.cpy();
/*  764 */       break;
/*      */     case COLORLESS: 
/*  766 */       this.bgColor = COLORLESS_BG_COLOR.cpy();
/*  767 */       this.backColor = COLORLESS_TYPE_BACK_COLOR.cpy();
/*  768 */       this.frameColor = COLORLESS_FRAME_COLOR.cpy();
/*  769 */       this.frameOutlineColor = Color.WHITE.cpy();
/*  770 */       this.descBoxColor = COLORLESS_DESC_BOX_COLOR.cpy();
/*  771 */       break;
/*      */     case RED: 
/*  773 */       this.bgColor = RED_BG_COLOR.cpy();
/*  774 */       this.backColor = RED_TYPE_BACK_COLOR.cpy();
/*  775 */       this.frameColor = RED_FRAME_COLOR.cpy();
/*  776 */       this.frameOutlineColor = RED_RARE_OUTLINE_COLOR.cpy();
/*  777 */       this.descBoxColor = RED_DESC_BOX_COLOR.cpy();
/*  778 */       break;
/*      */     case GREEN: 
/*  780 */       this.bgColor = GREEN_BG_COLOR.cpy();
/*  781 */       this.backColor = GREEN_TYPE_BACK_COLOR.cpy();
/*  782 */       this.frameColor = GREEN_FRAME_COLOR.cpy();
/*  783 */       this.frameOutlineColor = GREEN_RARE_OUTLINE_COLOR.cpy();
/*  784 */       this.descBoxColor = GREEN_DESC_BOX_COLOR.cpy();
/*  785 */       break;
/*      */     case BLUE: 
/*  787 */       this.bgColor = BLUE_BG_COLOR.cpy();
/*  788 */       this.backColor = BLUE_TYPE_BACK_COLOR.cpy();
/*  789 */       this.frameColor = BLUE_FRAME_COLOR.cpy();
/*  790 */       this.frameOutlineColor = BLUE_RARE_OUTLINE_COLOR.cpy();
/*  791 */       this.descBoxColor = BLUE_DESC_BOX_COLOR.cpy();
/*  792 */       break;
/*      */     default: 
/*  794 */       logger.info("ERROR: Card color was NOT set for " + this.name);
/*      */     }
/*      */     
/*      */     
/*      */ 
/*  799 */     if ((this.rarity == CardRarity.COMMON) || (this.rarity == CardRarity.BASIC) || (this.rarity == CardRarity.CURSE)) {
/*  800 */       this.bannerColor = BANNER_COLOR_COMMON.cpy();
/*  801 */       this.imgFrameColor = IMG_FRAME_COLOR_COMMON.cpy();
/*  802 */     } else if (this.rarity == CardRarity.UNCOMMON) {
/*  803 */       this.bannerColor = BANNER_COLOR_UNCOMMON.cpy();
/*  804 */       this.imgFrameColor = IMG_FRAME_COLOR_UNCOMMON.cpy();
/*      */     } else {
/*  806 */       this.bannerColor = BANNER_COLOR_RARE.cpy();
/*  807 */       this.imgFrameColor = IMG_FRAME_COLOR_RARE.cpy();
/*      */     }
/*      */     
/*  810 */     this.tintColor = CardHelper.getColor(43.0F, 37.0F, 65.0F);
/*  811 */     this.tintColor.a = 0.0F;
/*  812 */     this.frameShadowColor = FRAME_SHADOW_COLOR.cpy();
/*      */   }
/*      */   
/*      */   public abstract AbstractCard makeCopy();
/*      */   
/*      */   public AbstractCard makeStatEquivalentCopy() {
/*  818 */     AbstractCard card = makeCopy();
/*      */     
/*  820 */     for (int i = 0; i < this.timesUpgraded; i++) {
/*  821 */       card.upgrade();
/*      */     }
/*      */     
/*  824 */     card.name = this.name;
/*  825 */     card.target = this.target;
/*  826 */     card.upgraded = this.upgraded;
/*  827 */     card.timesUpgraded = this.timesUpgraded;
/*  828 */     card.baseDamage = this.baseDamage;
/*  829 */     card.baseBlock = this.baseBlock;
/*  830 */     card.baseMagicNumber = this.baseMagicNumber;
/*  831 */     card.cost = this.cost;
/*  832 */     card.costForTurn = this.costForTurn;
/*  833 */     card.isCostModified = this.isCostModified;
/*  834 */     card.isCostModifiedForTurn = this.isCostModifiedForTurn;
/*  835 */     card.inBottleLightning = this.inBottleLightning;
/*  836 */     card.inBottleFlame = this.inBottleFlame;
/*  837 */     card.inBottleTornado = this.inBottleTornado;
/*  838 */     card.isSeen = this.isSeen;
/*  839 */     card.isLocked = this.isLocked;
/*  840 */     card.misc = this.misc;
/*  841 */     card.freeToPlayOnce = this.freeToPlayOnce;
/*  842 */     return card;
/*      */   }
/*      */   
/*      */   public boolean cardPlayable(AbstractMonster m) {
/*  846 */     if (((this.target != CardTarget.ENEMY) && (this.target != CardTarget.SELF_AND_ENEMY)) || (((m != null) && (m.isDying)) || 
/*  847 */       (AbstractDungeon.getMonsters().areMonstersBasicallyDead()))) {
/*  848 */       this.cantUseMessage = null;
/*  849 */       return false;
/*      */     }
/*  851 */     return true;
/*      */   }
/*      */   
/*      */   public boolean hasEnoughEnergy() {
/*  855 */     if (AbstractDungeon.actionManager.turnHasEnded) {
/*  856 */       this.cantUseMessage = TEXT[9];
/*  857 */       return false;
/*      */     }
/*      */     
/*  860 */     if ((AbstractDungeon.player.hasPower("Entangled")) && (this.type == CardType.ATTACK)) {
/*  861 */       this.cantUseMessage = TEXT[10];
/*  862 */       return false;
/*      */     }
/*      */     
/*  865 */     if (this.freeToPlayOnce) {
/*  866 */       return true;
/*      */     }
/*      */     
/*  869 */     for (AbstractRelic r : AbstractDungeon.player.relics) {
/*  870 */       if (!r.canPlay(this)) {
/*  871 */         return false;
/*      */       }
/*      */     }
/*      */     
/*  875 */     for (AbstractBlight b : AbstractDungeon.player.blights) {
/*  876 */       if (!b.canPlay(this)) {
/*  877 */         return false;
/*      */       }
/*      */     }
/*      */     
/*  881 */     for (AbstractCard c : AbstractDungeon.player.hand.group) {
/*  882 */       if (!c.canPlay(this)) {
/*  883 */         return false;
/*      */       }
/*      */     }
/*      */     
/*  887 */     if (EnergyPanel.totalCount >= this.costForTurn) {
/*  888 */       return true;
/*      */     }
/*      */     
/*  891 */     this.cantUseMessage = TEXT[11];
/*  892 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void tookDamage() {}
/*      */   
/*      */ 
/*      */ 
/*      */   public void didDiscard() {}
/*      */   
/*      */ 
/*      */   protected void useBlueCandle(AbstractPlayer p)
/*      */   {
/*  906 */     p.getRelic("Blue Candle").flash();
/*  907 */     AbstractDungeon.actionManager.addToBottom(new LoseHPAction(p, p, 1, AbstractGameAction.AttackEffect.FIRE));
/*  908 */     this.exhaust = true;
/*      */   }
/*      */   
/*      */   protected void useMedicalKit(AbstractPlayer p) {
/*  912 */     p.getRelic("Medical Kit").flash();
/*  913 */     this.exhaust = true;
/*      */   }
/*      */   
/*      */   public boolean canPlay(AbstractCard card) {
/*  917 */     return true;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*      */   {
/*  924 */     if (this.type == CardType.STATUS) {
/*  925 */       if (AbstractDungeon.player.hasRelic("Medical Kit")) {
/*  926 */         return true;
/*      */       }
/*  928 */       if (!this.cardID.equals("Slimed")) {
/*  929 */         this.cantUseMessage = getCantPlayMessage();
/*  930 */         return false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  935 */     if (this.type == CardType.CURSE) {
/*  936 */       if (AbstractDungeon.player.hasRelic("Blue Candle")) {
/*  937 */         return true;
/*      */       }
/*  939 */       if (!this.cardID.equals("Pride")) {
/*  940 */         this.cantUseMessage = getCantPlayMessage();
/*  941 */         return false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  946 */     if ((cardPlayable(m)) && (hasEnoughEnergy())) {
/*  947 */       return true;
/*      */     }
/*      */     
/*  950 */     return false;
/*      */   }
/*      */   
/*      */ 
/*      */   public abstract void use(AbstractPlayer paramAbstractPlayer, AbstractMonster paramAbstractMonster);
/*      */   
/*      */ 
/*      */   public void update()
/*      */   {
/*  959 */     updateFlashVfx();
/*      */     
/*  961 */     if (this.hoverTimer != 0.0F) {
/*  962 */       this.hoverTimer -= Gdx.graphics.getDeltaTime();
/*  963 */       if (this.hoverTimer < 0.0F) {
/*  964 */         this.hoverTimer = 0.0F;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  971 */     if ((AbstractDungeon.player != null) && (AbstractDungeon.player.isDraggingCard) && (this == AbstractDungeon.player.hoveredCard))
/*      */     {
/*  973 */       this.current_x = MathHelper.cardLerpSnap(this.current_x, this.target_x);
/*  974 */       this.current_y = MathHelper.cardLerpSnap(this.current_y, this.target_y);
/*      */       
/*      */ 
/*  977 */       if (AbstractDungeon.player.hasRelic("Necronomicon"))
/*      */       {
/*  979 */         if ((this.cost >= 2) && (this.type == CardType.ATTACK))
/*      */         {
/*  981 */           if (AbstractDungeon.player.getRelic("Necronomicon").checkTrigger()) {
/*  982 */             AbstractDungeon.player.getRelic("Necronomicon").beginLongPulse();
/*      */             break label171; } }
/*  984 */         AbstractDungeon.player.getRelic("Necronomicon").stopPulse();
/*      */       }
/*      */     }
/*      */     
/*      */     label171:
/*  989 */     if (Settings.FAST_MODE) {
/*  990 */       this.current_x = MathHelper.cardLerpSnap(this.current_x, this.target_x);
/*  991 */       this.current_y = MathHelper.cardLerpSnap(this.current_y, this.target_y);
/*      */     }
/*  993 */     this.current_x = MathHelper.cardLerpSnap(this.current_x, this.target_x);
/*  994 */     this.current_y = MathHelper.cardLerpSnap(this.current_y, this.target_y);
/*      */     
/*  996 */     this.hb.move(this.current_x, this.current_y);
/*  997 */     this.hb.resize(HB_W * this.drawScale, HB_H * this.drawScale);
/*      */     
/*  999 */     if ((this.hb.clickStarted) && (this.hb.hovered)) {
/* 1000 */       this.drawScale = MathHelper.cardScaleLerpSnap(this.drawScale, this.targetDrawScale * 0.9F);
/* 1001 */       this.drawScale = MathHelper.cardScaleLerpSnap(this.drawScale, this.targetDrawScale * 0.9F);
/*      */     } else {
/* 1003 */       this.drawScale = MathHelper.cardScaleLerpSnap(this.drawScale, this.targetDrawScale);
/*      */     }
/*      */     
/* 1006 */     if (this.angle != this.targetAngle) {
/* 1007 */       this.angle = MathHelper.angleLerpSnap(this.angle, this.targetAngle);
/*      */     }
/*      */     
/* 1010 */     updateTransparency();
/* 1011 */     updateColor();
/*      */   }
/*      */   
/*      */   private void updateFlashVfx() {
/* 1015 */     if (this.flashVfx != null) {
/* 1016 */       this.flashVfx.update();
/* 1017 */       if (this.flashVfx.isDone) {
/* 1018 */         this.flashVfx = null;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateGlow() {
/* 1024 */     if (this.isGlowing) {
/* 1025 */       this.glowTimer -= Gdx.graphics.getDeltaTime();
/* 1026 */       if (this.glowTimer < 0.0F) {
/* 1027 */         this.glowList.add(new CardGlowBorder(this));
/* 1028 */         this.glowTimer = 0.15F;
/*      */       }
/*      */     }
/*      */     
/* 1032 */     for (Iterator<CardGlowBorder> i = this.glowList.iterator(); i.hasNext();) {
/* 1033 */       CardGlowBorder e = (CardGlowBorder)i.next();
/* 1034 */       e.update();
/* 1035 */       if (e.isDone) {
/* 1036 */         i.remove();
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public boolean isHoveredInHand(float scale) {
/* 1042 */     if (this.hoverTimer > 0.0F) {
/* 1043 */       return false;
/*      */     }
/*      */     
/* 1046 */     int x = InputHelper.mX;int y = InputHelper.mY;
/* 1047 */     return (x > this.current_x - IMG_WIDTH * scale / 2.0F) && (x < this.current_x + IMG_WIDTH * scale / 2.0F) && (y > this.current_y - IMG_HEIGHT * scale / 2.0F) && (y < this.current_y + IMG_HEIGHT * scale / 2.0F);
/*      */   }
/*      */   
/*      */   private boolean isOnScreen()
/*      */   {
/* 1052 */     return (this.current_y >= -200.0F * Settings.scale) && (this.current_y <= Settings.HEIGHT + 200.0F * Settings.scale);
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb) {
/* 1056 */     if (!Settings.hideCards) {
/* 1057 */       render(sb, false);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderHoverShadow(SpriteBatch sb) {
/* 1062 */     if (!Settings.hideCards) {
/* 1063 */       renderHelper(sb, new Color(0.0F, 0.0F, 0.0F, 0.66F), ImageMaster.CARD_SUPER_SHADOW, this.current_x - 256.0F, this.current_y - 256.0F, 1.15F);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderInLibrary(SpriteBatch sb)
/*      */   {
/* 1074 */     if (!isOnScreen()) {
/* 1075 */       return;
/*      */     }
/*      */     
/* 1078 */     if ((SingleCardViewPopup.isViewingUpgrade) && (this.isSeen) && (!this.isLocked)) {
/* 1079 */       AbstractCard copy = makeCopy();
/* 1080 */       copy.current_x = this.current_x;
/* 1081 */       copy.current_y = this.current_y;
/* 1082 */       copy.drawScale = this.drawScale;
/* 1083 */       copy.upgrade();
/* 1084 */       copy.displayUpgrades();
/* 1085 */       copy.render(sb);
/*      */     } else {
/* 1087 */       updateGlow();
/* 1088 */       renderGlow(sb);
/* 1089 */       renderImage(sb, this.hovered, false);
/* 1090 */       renderType(sb);
/* 1091 */       renderTitle(sb);
/* 1092 */       if (Settings.lineBreakViaCharacter) {
/* 1093 */         renderDescriptionCN(sb);
/*      */       } else {
/* 1095 */         renderDescription(sb);
/*      */       }
/* 1097 */       renderTint(sb);
/* 1098 */       renderEnergy(sb);
/* 1099 */       this.hb.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void render(SpriteBatch sb, boolean selected)
/*      */   {
/* 1105 */     if (!Settings.hideCards) {
/* 1106 */       if (this.flashVfx != null) {
/* 1107 */         this.flashVfx.render(sb);
/*      */       }
/* 1109 */       renderCard(sb, this.hovered, selected);
/* 1110 */       this.hb.render(sb);
/*      */     }
/*      */   }
/*      */   
/*      */   public void renderUpgradePreview(SpriteBatch sb) {
/* 1115 */     this.upgraded = true;
/* 1116 */     this.name = (this.originalName + "+");
/* 1117 */     initializeTitle();
/* 1118 */     renderCard(sb, this.hovered, false);
/* 1119 */     this.name = this.originalName;
/* 1120 */     initializeTitle();
/* 1121 */     this.upgraded = false;
/* 1122 */     resetAttributes();
/*      */   }
/*      */   
/*      */   public void renderWithSelections(SpriteBatch sb) {
/* 1126 */     renderCard(sb, false, true);
/*      */   }
/*      */   
/*      */   private void renderCard(SpriteBatch sb, boolean hovered, boolean selected)
/*      */   {
/* 1131 */     if (!Settings.hideCards) {
/* 1132 */       if (!isOnScreen()) {
/* 1133 */         return;
/*      */       }
/* 1135 */       if (!this.isFlipped) {
/* 1136 */         updateGlow();
/* 1137 */         renderGlow(sb);
/* 1138 */         renderImage(sb, hovered, selected);
/* 1139 */         renderTitle(sb);
/* 1140 */         renderType(sb);
/* 1141 */         if (Settings.lineBreakViaCharacter) {
/* 1142 */           renderDescriptionCN(sb);
/*      */         } else {
/* 1144 */           renderDescription(sb);
/*      */         }
/* 1146 */         renderTint(sb);
/* 1147 */         renderEnergy(sb);
/*      */       } else {
/* 1149 */         renderBack(sb, hovered, selected);
/* 1150 */         this.hb.render(sb);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderTint(SpriteBatch sb) {
/* 1156 */     if (!Settings.hideCards) {
/* 1157 */       float drawX = this.current_x - 256.0F;
/* 1158 */       float drawY = this.current_y - 256.0F;
/* 1159 */       renderHelper(sb, this.tintColor, getCardBg(), drawX, drawY);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void renderOuterGlow(SpriteBatch sb)
/*      */   {
/* 1167 */     if (AbstractDungeon.player == null) {
/* 1168 */       return;
/*      */     }
/*      */     
/* 1171 */     if (!Settings.hideCards) {
/* 1172 */       float drawX = this.current_x - 256.0F;
/* 1173 */       float drawY = this.current_y - 256.0F;
/*      */       
/* 1175 */       switch (AbstractDungeon.player.chosenClass) {
/*      */       case IRONCLAD: 
/* 1177 */         renderHelper(sb, Color.SCARLET, getCardBg(), drawX, drawY, 1.0F + this.tintColor.a / 5.0F);
/* 1178 */         break;
/*      */       case THE_SILENT: 
/* 1180 */         renderHelper(sb, Color.CHARTREUSE, getCardBg(), drawX, drawY, 1.0F + this.tintColor.a / 5.0F);
/* 1181 */         break;
/*      */       case DEFECT: 
/* 1183 */         renderHelper(sb, Color.SKY, getCardBg(), drawX, drawY, 1.0F + this.tintColor.a / 5.0F);
/* 1184 */         break;
/*      */       }
/*      */       
/*      */     }
/*      */   }
/*      */   
/*      */   public Texture getCardBg()
/*      */   {
/* 1192 */     switch (this.type) {
/*      */     case ATTACK: 
/* 1194 */       return ImageMaster.CARD_ATTACK_BG_SILHOUETTE;
/*      */     case SKILL: 
/* 1196 */       return ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*      */     case POWER: 
/* 1198 */       return ImageMaster.CARD_POWER_BG_SILHOUETTE;
/*      */     case CURSE: 
/* 1200 */       return ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*      */     case STATUS: 
/* 1202 */       return ImageMaster.CARD_SKILL_BG_SILHOUETTE;
/*      */     }
/* 1204 */     return null;
/*      */   }
/*      */   
/*      */   private void renderGlow(SpriteBatch sb)
/*      */   {
/* 1209 */     if (!Settings.hideCards) {
/* 1210 */       sb.setBlendFunction(770, 1);
/* 1211 */       for (AbstractGameEffect e : this.glowList) {
/* 1212 */         e.render(sb);
/*      */       }
/* 1214 */       sb.setBlendFunction(770, 771);
/*      */     }
/*      */   }
/*      */   
/*      */   public void beginGlowing() {
/* 1219 */     this.isGlowing = true;
/*      */   }
/*      */   
/*      */   public void stopGlowing() {
/* 1223 */     this.isGlowing = false;
/* 1224 */     for (CardGlowBorder e : this.glowList) {
/* 1225 */       e.duration /= 5.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderHelper(SpriteBatch sb, Color color, Texture img, float drawX, float drawY) {
/* 1230 */     sb.setColor(color);
/*      */     try {
/* 1232 */       sb.draw(img, drawX, drawY, 256.0F, 256.0F, 512.0F, 512.0F, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle, 0, 0, 512, 512, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1250 */       ExceptionHandler.handleException(e, logger);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderHelper(SpriteBatch sb, Color color, Texture img, float drawX, float drawY, float scale) {
/* 1255 */     sb.setColor(color);
/* 1256 */     sb.draw(img, drawX, drawY, 256.0F, 256.0F, 512.0F, 512.0F, this.drawScale * Settings.scale * scale, this.drawScale * Settings.scale * scale, this.angle, 0, 0, 512, 512, false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderSmallEnergy(SpriteBatch sb, TextureAtlas.AtlasRegion region, float x, float y)
/*      */   {
/* 1276 */     sb.setColor(this.renderColor);
/* 1277 */     sb.draw(region
/* 1278 */       .getTexture(), this.current_x + x * Settings.scale * this.drawScale + region.offsetX * Settings.scale, this.current_y + y * Settings.scale * this.drawScale / 2.0F + region.offsetY * Settings.scale, 0.0F, 0.0F, region.packedWidth, region.packedHeight, this.drawScale * Settings.scale, this.drawScale * Settings.scale, 0.0F, region
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1288 */       .getRegionX(), region
/* 1289 */       .getRegionY(), region
/* 1290 */       .getRegionWidth(), region
/* 1291 */       .getRegionHeight(), false, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderImage(SpriteBatch sb, boolean hovered, boolean selected)
/*      */   {
/* 1304 */     float drawX = this.current_x - 256.0F;
/* 1305 */     float drawY = this.current_y - 256.0F;
/*      */     
/* 1307 */     if (AbstractDungeon.player != null) {
/* 1308 */       if (selected) {
/* 1309 */         renderHelper(sb, Color.SKY, getCardBg(), drawX, drawY, 1.03F);
/*      */       }
/*      */       
/*      */ 
/* 1313 */       renderHelper(sb, this.frameShadowColor, 
/*      */       
/*      */ 
/* 1316 */         getCardBg(), drawX + SHADOW_OFFSET_X * this.drawScale, drawY - SHADOW_OFFSET_Y * this.drawScale);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/* 1321 */       if ((AbstractDungeon.player.hoveredCard == this) && (((AbstractDungeon.player.isDraggingCard) && (AbstractDungeon.player.isHoveringDropZone)) || (AbstractDungeon.player.inSingleTargetMode)))
/*      */       {
/* 1323 */         renderHelper(sb, HOVER_IMG_COLOR, getCardBg(), drawX, drawY);
/* 1324 */       } else if (selected) {
/* 1325 */         renderHelper(sb, SELECTED_CARD_COLOR, getCardBg(), drawX, drawY);
/*      */       }
/*      */     }
/*      */     
/* 1329 */     renderCardBg(sb, drawX, drawY);
/* 1330 */     if ((Settings.PLAYTESTER_ART_MODE) && (Settings.isBeta)) {
/* 1331 */       renderJokePortrait(sb);
/*      */     } else {
/* 1333 */       renderPortrait(sb);
/*      */     }
/* 1335 */     renderPortraitFrame(sb, drawX, drawY);
/* 1336 */     renderBannerImage(sb, drawX, drawY);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderCardBg(SpriteBatch sb, float x, float y)
/*      */   {
/* 1347 */     switch (this.type) {
/*      */     case ATTACK: 
/* 1349 */       renderAttackBg(sb, x, y);
/* 1350 */       break;
/*      */     case SKILL: 
/* 1352 */       renderSkillBg(sb, x, y);
/* 1353 */       break;
/*      */     case POWER: 
/* 1355 */       renderPowerBg(sb, x, y);
/* 1356 */       break;
/*      */     case CURSE: 
/* 1358 */       renderSkillBg(sb, x, y);
/* 1359 */       break;
/*      */     case STATUS: 
/* 1361 */       renderSkillBg(sb, x, y);
/* 1362 */       break;
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */   private void renderAttackBg(SpriteBatch sb, float x, float y)
/*      */   {
/* 1369 */     switch (this.color) {
/*      */     case RED: 
/* 1371 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_ATTACK_BG_RED, x, y);
/* 1372 */       break;
/*      */     case GREEN: 
/* 1374 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_ATTACK_BG_GREEN, x, y);
/* 1375 */       break;
/*      */     case BLUE: 
/* 1377 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_ATTACK_BG_BLUE, x, y);
/* 1378 */       break;
/*      */     case CURSE: 
/* 1380 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/* 1381 */       break;
/*      */     case COLORLESS: 
/* 1383 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_ATTACK_BG_GRAY, x, y);
/* 1384 */       break;
/*      */     default: 
/* 1386 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderSkillBg(SpriteBatch sb, float x, float y) {
/* 1391 */     switch (this.color) {
/*      */     case RED: 
/* 1393 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_RED, x, y);
/* 1394 */       break;
/*      */     case GREEN: 
/* 1396 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_GREEN, x, y);
/* 1397 */       break;
/*      */     case BLUE: 
/* 1399 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLUE, x, y);
/* 1400 */       break;
/*      */     case CURSE: 
/* 1402 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/* 1403 */       break;
/*      */     case COLORLESS: 
/* 1405 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_GRAY, x, y);
/* 1406 */       break;
/*      */     default: 
/* 1408 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/*      */     }
/*      */   }
/*      */   
/*      */   private void renderPowerBg(SpriteBatch sb, float x, float y) {
/* 1413 */     switch (this.color) {
/*      */     case RED: 
/* 1415 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_POWER_BG_RED, x, y);
/* 1416 */       break;
/*      */     case GREEN: 
/* 1418 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_POWER_BG_GREEN, x, y);
/* 1419 */       break;
/*      */     case BLUE: 
/* 1421 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_POWER_BG_BLUE, x, y);
/* 1422 */       break;
/*      */     case CURSE: 
/* 1424 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/* 1425 */       break;
/*      */     case COLORLESS: 
/* 1427 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_POWER_BG_GRAY, x, y);
/* 1428 */       break;
/*      */     default: 
/* 1430 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_SKILL_BG_BLACK, x, y);
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderPortraitFrame(SpriteBatch sb, float x, float y)
/*      */   {
/* 1442 */     switch (this.type) {
/*      */     case ATTACK: 
/* 1444 */       renderAttackPortrait(sb, x, y);
/* 1445 */       break;
/*      */     case SKILL: 
/* 1447 */       renderSkillPortrait(sb, x, y);
/* 1448 */       break;
/*      */     case POWER: 
/* 1450 */       renderPowerPortrait(sb, x, y);
/* 1451 */       break;
/*      */     case STATUS: 
/* 1453 */       renderSkillPortrait(sb, x, y);
/* 1454 */       break;
/*      */     case CURSE: 
/* 1456 */       renderSkillPortrait(sb, x, y);
/* 1457 */       break;
/*      */     }
/*      */     
/*      */   }
/*      */   
/*      */   private void renderAttackPortrait(SpriteBatch sb, float x, float y)
/*      */   {
/* 1464 */     switch (this.rarity) {
/*      */     case BASIC: 
/* 1466 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_COMMON, x, y);
/* 1467 */       return;
/*      */     case COMMON: 
/* 1469 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_COMMON, x, y);
/* 1470 */       return;
/*      */     case UNCOMMON: 
/* 1472 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_UNCOMMON, x, y);
/* 1473 */       return;
/*      */     case RARE: 
/* 1475 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_RARE, x, y);
/* 1476 */       return;
/*      */     case CURSE: 
/* 1478 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_COMMON, x, y);
/* 1479 */       return;
/*      */     }
/* 1481 */     renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_ATTACK_COMMON, x, y);
/*      */   }
/*      */   
/*      */ 
/*      */   private void renderSkillPortrait(SpriteBatch sb, float x, float y)
/*      */   {
/* 1487 */     switch (this.rarity) {
/*      */     case BASIC: 
/* 1489 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_COMMON, x, y);
/* 1490 */       return;
/*      */     case COMMON: 
/* 1492 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_COMMON, x, y);
/* 1493 */       return;
/*      */     case UNCOMMON: 
/* 1495 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_UNCOMMON, x, y);
/* 1496 */       return;
/*      */     case RARE: 
/* 1498 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_RARE, x, y);
/* 1499 */       return;
/*      */     case CURSE: 
/* 1501 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_COMMON, x, y);
/* 1502 */       return;
/*      */     }
/* 1504 */     renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_SKILL_COMMON, x, y);
/*      */   }
/*      */   
/*      */ 
/*      */   private void renderPowerPortrait(SpriteBatch sb, float x, float y)
/*      */   {
/* 1510 */     switch (this.rarity) {
/*      */     case BASIC: 
/* 1512 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_COMMON, x, y);
/* 1513 */       return;
/*      */     case COMMON: 
/* 1515 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_COMMON, x, y);
/* 1516 */       return;
/*      */     case UNCOMMON: 
/* 1518 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_UNCOMMON, x, y);
/* 1519 */       return;
/*      */     case RARE: 
/* 1521 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_RARE, x, y);
/* 1522 */       return;
/*      */     case CURSE: 
/* 1524 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_COMMON, x, y);
/* 1525 */       return;
/*      */     }
/* 1527 */     renderHelper(sb, this.renderColor, ImageMaster.CARD_FRAME_POWER_COMMON, x, y);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBannerImage(SpriteBatch sb, float drawX, float drawY)
/*      */   {
/* 1540 */     switch (this.rarity) {
/*      */     case BASIC: 
/* 1542 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_COMMON, drawX, drawY);
/* 1543 */       return;
/*      */     case COMMON: 
/* 1545 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_COMMON, drawX, drawY);
/* 1546 */       return;
/*      */     case UNCOMMON: 
/* 1548 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_UNCOMMON, drawX, drawY);
/* 1549 */       return;
/*      */     case RARE: 
/* 1551 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_RARE, drawX, drawY);
/* 1552 */       return;
/*      */     case CURSE: 
/* 1554 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_COMMON, drawX, drawY);
/* 1555 */       return;
/*      */     }
/* 1557 */     renderHelper(sb, this.renderColor, ImageMaster.CARD_BANNER_COMMON, drawX, drawY);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderBack(SpriteBatch sb, boolean hovered, boolean selected)
/*      */   {
/* 1570 */     float drawX = this.current_x - 256.0F;
/* 1571 */     float drawY = this.current_y - 256.0F;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1582 */     renderHelper(sb, this.renderColor, ImageMaster.CARD_BACK, drawX, drawY);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderPortrait(SpriteBatch sb)
/*      */   {
/* 1591 */     float drawX = this.current_x - 125.0F;
/* 1592 */     float drawY = this.current_y - 95.0F;
/*      */     
/* 1594 */     Texture img = null;
/* 1595 */     if (this.portraitImg != null) {
/* 1596 */       img = this.portraitImg;
/*      */     }
/*      */     
/* 1599 */     if (!this.isLocked) {
/* 1600 */       if (this.portrait != null) {
/* 1601 */         drawX = this.current_x - this.portrait.packedWidth / 2.0F;
/* 1602 */         drawY = this.current_y - this.portrait.packedHeight / 2.0F;
/* 1603 */         sb.setColor(this.renderColor);
/* 1604 */         sb.draw(this.portrait, drawX, drawY + 72.0F, this.portrait.packedWidth / 2.0F, this.portrait.packedHeight / 2.0F - 72.0F, this.portrait.packedWidth, this.portrait.packedHeight, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/* 1615 */       else if (img != null) {
/* 1616 */         sb.setColor(this.renderColor);
/* 1617 */         sb.draw(img, drawX, drawY + 72.0F, 125.0F, 23.0F, 250.0F, 190.0F, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle, 0, 0, 250, 190, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1637 */       sb.draw(this.portraitImg, drawX, drawY + 72.0F, 125.0F, 23.0F, 250.0F, 190.0F, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle, 0, 0, 250, 190, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderJokePortrait(SpriteBatch sb)
/*      */   {
/* 1663 */     float drawX = this.current_x - 125.0F;
/* 1664 */     float drawY = this.current_y - 95.0F;
/*      */     
/* 1666 */     Texture img = null;
/* 1667 */     if (this.portraitImg != null) {
/* 1668 */       img = this.portraitImg;
/*      */     }
/*      */     
/* 1671 */     if (!this.isLocked) {
/* 1672 */       if (this.jokePortrait != null) {
/* 1673 */         drawX = this.current_x - this.portrait.packedWidth / 2.0F;
/* 1674 */         drawY = this.current_y - this.portrait.packedHeight / 2.0F;
/* 1675 */         sb.setColor(this.renderColor);
/* 1676 */         sb.draw(this.jokePortrait, drawX, drawY + 72.0F, this.jokePortrait.packedWidth / 2.0F, this.jokePortrait.packedHeight / 2.0F - 72.0F, this.jokePortrait.packedWidth, this.jokePortrait.packedHeight, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/* 1687 */       else if (img != null) {
/* 1688 */         sb.setColor(this.renderColor);
/* 1689 */         sb.draw(img, drawX, drawY + 72.0F, 125.0F, 23.0F, 250.0F, 190.0F, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle, 0, 0, 250, 190, false, false);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1708 */       sb.draw(this.portraitImg, drawX, drawY + 72.0F, 125.0F, 23.0F, 250.0F, 190.0F, this.drawScale * Settings.scale, this.drawScale * Settings.scale, this.angle, 0, 0, 250, 190, false, false);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderDescription(SpriteBatch sb)
/*      */   {
/* 1732 */     if ((!this.isSeen) || (this.isLocked)) {
/* 1733 */       FontHelper.menuBannerFont.getData().setScale(this.drawScale * 1.25F);
/* 1734 */       FontHelper.renderRotatedText(sb, FontHelper.menuBannerFont, "? ? ?", this.current_x, this.current_y, 0.0F, -200.0F * Settings.scale * this.drawScale / 2.0F, this.angle, true, this.textColor);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1745 */       FontHelper.menuBannerFont.getData().setScale(1.0F);
/* 1746 */       return;
/*      */     }
/*      */     
/* 1749 */     BitmapFont font = getDescFont();
/* 1750 */     float draw_y = this.current_y - IMG_HEIGHT * this.drawScale / 2.0F + DESC_OFFSET_Y * this.drawScale;
/* 1751 */     draw_y += this.description.size() * font.getCapHeight() * 0.775F - font.getCapHeight() * 0.375F;
/* 1752 */     float spacing = 1.45F * -font.getCapHeight() / Settings.scale / this.drawScale;
/* 1753 */     GlyphLayout gl = new GlyphLayout();
/*      */     
/* 1755 */     for (int i = 0; i < this.description.size(); i++) {
/* 1756 */       float start_x = this.current_x - ((DescriptionLine)this.description.get(i)).width * this.drawScale / 2.0F;
/* 1757 */       scanner = new Scanner(((DescriptionLine)this.description.get(i)).text);
/*      */       
/* 1759 */       while (scanner.hasNext()) {
/* 1760 */         String tmp = scanner.next() + ' ';
/*      */         
/*      */ 
/* 1763 */         if (tmp.charAt(0) == '*') {
/* 1764 */           tmp = tmp.substring(1);
/* 1765 */           String punctuation = "";
/* 1766 */           if ((tmp.length() > 1) && (!Character.isLetter(tmp.charAt(tmp.length() - 2)))) {
/* 1767 */             punctuation = punctuation + tmp.charAt(tmp.length() - 2);
/* 1768 */             tmp = tmp.substring(0, tmp.length() - 2);
/* 1769 */             punctuation = punctuation + ' ';
/*      */           }
/*      */           
/* 1772 */           gl.setText(font, tmp);
/* 1773 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1780 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.goldColor);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/* 1785 */           start_x = Math.round(start_x + gl.width);
/* 1786 */           gl.setText(font, punctuation);
/* 1787 */           FontHelper.renderRotatedText(sb, font, punctuation, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1794 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/* 1799 */           gl.setText(font, punctuation);
/* 1800 */           start_x += gl.width;
/*      */ 
/*      */         }
/* 1803 */         else if (tmp.charAt(0) == '!') {
/* 1804 */           if (tmp.length() == 4) {
/* 1805 */             start_x += renderDynamicVariable(tmp.charAt(1), start_x, draw_y, i, font, sb, null);
/* 1806 */           } else if (tmp.length() == 5) {
/* 1807 */             start_x += renderDynamicVariable(tmp.charAt(1), start_x, draw_y, i, font, sb, Character.valueOf(tmp.charAt(3)));
/*      */           }
/*      */           
/*      */         }
/* 1811 */         else if (tmp.equals("[R] ")) {
/* 1812 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 1813 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1814 */           renderSmallEnergy(sb, orb_red, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1820 */           start_x += gl.width;
/*      */ 
/*      */         }
/* 1823 */         else if (tmp.equals("[R]. ")) {
/* 1824 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale / Settings.scale);
/* 1825 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1826 */           renderSmallEnergy(sb, orb_red, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1832 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + CARD_ENERGY_IMG_WIDTH * this.drawScale, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1839 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/* 1844 */           start_x += gl.width;
/* 1845 */           gl.setText(font, ".");
/* 1846 */           start_x += gl.width;
/*      */         }
/* 1848 */         else if (tmp.equals("[G] ")) {
/* 1849 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 1850 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1851 */           renderSmallEnergy(sb, orb_green, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1857 */           start_x += gl.width;
/*      */         }
/* 1859 */         else if (tmp.equals("[G]. ")) {
/* 1860 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 1861 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1862 */           renderSmallEnergy(sb, orb_green, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1868 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + CARD_ENERGY_IMG_WIDTH * this.drawScale, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1875 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/* 1879 */           start_x += gl.width;
/*      */ 
/*      */         }
/* 1882 */         else if (tmp.equals("[B] ")) {
/* 1883 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 1884 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1885 */           renderSmallEnergy(sb, orb_blue, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1891 */           start_x += gl.width;
/*      */         }
/* 1893 */         else if (tmp.equals("[B]. ")) {
/* 1894 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 1895 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 1896 */           renderSmallEnergy(sb, orb_blue, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1902 */           FontHelper.renderRotatedText(sb, font, ".", this.current_x, this.current_y, start_x - this.current_x + CARD_ENERGY_IMG_WIDTH * this.drawScale, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1909 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/* 1913 */           start_x += gl.width;
/*      */         }
/*      */         else
/*      */         {
/* 1917 */           gl.setText(font, tmp);
/* 1918 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1925 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/* 1929 */           start_x += gl.width;
/*      */         }
/*      */       }
/*      */     }
/* 1933 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */   private String getDynamicValue(char key) {
/* 1937 */     switch (key) {
/*      */     case 'B': 
/* 1939 */       if (this.isBlockModified) {
/* 1940 */         if (this.block >= this.baseBlock) {
/* 1941 */           return "[#7fff00]" + Integer.toString(this.block) + "[]";
/*      */         }
/* 1943 */         return "[#ff6563]" + Integer.toString(this.block) + "[]";
/*      */       }
/*      */       
/* 1946 */       return Integer.toString(this.baseBlock);
/*      */     
/*      */     case 'D': 
/* 1949 */       if (this.isDamageModified) {
/* 1950 */         if (this.damage >= this.baseDamage) {
/* 1951 */           return "[#7fff00]" + Integer.toString(this.damage) + "[]";
/*      */         }
/* 1953 */         return "[#ff6563]" + Integer.toString(this.damage) + "[]";
/*      */       }
/*      */       
/* 1956 */       return Integer.toString(this.baseDamage);
/*      */     
/*      */     case 'M': 
/* 1959 */       if (this.isMagicNumberModified) {
/* 1960 */         if (this.magicNumber >= this.baseMagicNumber) {
/* 1961 */           return "[#7fff00]" + Integer.toString(this.magicNumber) + "[]";
/*      */         }
/* 1963 */         return "[#ff6563]" + Integer.toString(this.magicNumber) + "[]";
/*      */       }
/*      */       
/* 1966 */       return Integer.toString(this.baseMagicNumber);
/*      */     }
/*      */     
/* 1969 */     System.out.println("KEY: " + key);
/* 1970 */     return Integer.toString(-99);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private void renderDescriptionCN(SpriteBatch sb)
/*      */   {
/* 1978 */     if ((!this.isSeen) || (this.isLocked)) {
/* 1979 */       FontHelper.menuBannerFont.getData().setScale(this.drawScale * 1.25F);
/* 1980 */       FontHelper.renderRotatedText(sb, FontHelper.menuBannerFont, "? ? ?", this.current_x, this.current_y, 0.0F, -200.0F * Settings.scale * this.drawScale / 2.0F, this.angle, true, this.textColor);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1991 */       FontHelper.menuBannerFont.getData().setScale(1.0F);
/* 1992 */       return;
/*      */     }
/*      */     
/* 1995 */     BitmapFont font = getDescFont();
/* 1996 */     float draw_y = this.current_y - IMG_HEIGHT * this.drawScale / 2.0F + DESC_OFFSET_Y * this.drawScale;
/* 1997 */     draw_y += this.description.size() * font.getCapHeight() * 0.775F - font.getCapHeight() * 0.375F;
/* 1998 */     float spacing = 1.45F * -font.getCapHeight() / Settings.scale / this.drawScale;
/* 1999 */     GlyphLayout gl = new GlyphLayout();
/*      */     
/* 2001 */     for (int i = 0; i < this.description.size(); i++) {
/* 2002 */       float start_x = this.current_x - ((DescriptionLine)this.description.get(i)).width * this.drawScale / 2.0F - 14.0F * Settings.scale;
/* 2003 */       scanner = new Scanner(((DescriptionLine)this.description.get(i)).text);
/*      */       
/* 2005 */       while (scanner.hasNext()) {
/* 2006 */         String tmp = scanner.next();
/*      */         
/* 2008 */         tmp = tmp.replace("!", "");
/* 2009 */         String updateTmp = null;
/* 2010 */         for (int j = 0; j < tmp.length(); j++) {
/* 2011 */           if ((tmp.charAt(j) == 'D') || ((tmp.charAt(j) == 'B') && (!tmp.contains("[B]"))) || (tmp.charAt(j) == 'M'))
/*      */           {
/* 2013 */             updateTmp = tmp.substring(0, j);
/* 2014 */             updateTmp = updateTmp + getDynamicValue(tmp.charAt(j));
/* 2015 */             updateTmp = updateTmp + tmp.substring(j + 1);
/* 2016 */             break;
/*      */           }
/*      */         }
/*      */         
/* 2020 */         if (updateTmp != null) {
/* 2021 */           tmp = updateTmp;
/*      */         }
/*      */         
/*      */ 
/* 2025 */         for (int j = 0; j < tmp.length(); j++) {
/* 2026 */           if ((tmp.charAt(j) == 'D') || ((tmp.charAt(j) == 'B') && (!tmp.contains("[B]"))) || (tmp.charAt(j) == 'M'))
/*      */           {
/* 2028 */             updateTmp = tmp.substring(0, j);
/* 2029 */             updateTmp = updateTmp + getDynamicValue(tmp.charAt(j));
/* 2030 */             updateTmp = updateTmp + tmp.substring(j + 1);
/* 2031 */             break;
/*      */           }
/*      */         }
/*      */         
/* 2035 */         if (updateTmp != null) {
/* 2036 */           tmp = updateTmp;
/*      */         }
/*      */         
/*      */ 
/* 2040 */         if (tmp.charAt(0) == '*') {
/* 2041 */           tmp = tmp.substring(1);
/* 2042 */           String punctuation = "";
/* 2043 */           if ((tmp.length() > 1) && (!Character.isLetter(tmp.charAt(tmp.length() - 2)))) {
/* 2044 */             punctuation = punctuation + tmp.charAt(tmp.length() - 2);
/* 2045 */             tmp = tmp.substring(0, tmp.length() - 2);
/* 2046 */             punctuation = punctuation + ' ';
/*      */           }
/*      */           
/* 2049 */           gl.setText(font, tmp);
/* 2050 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2057 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.goldColor);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/* 2062 */           start_x = Math.round(start_x + gl.width);
/* 2063 */           gl.setText(font, punctuation);
/* 2064 */           FontHelper.renderRotatedText(sb, font, punctuation, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2071 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/* 2076 */           gl.setText(font, punctuation);
/* 2077 */           start_x += gl.width;
/*      */ 
/*      */         }
/* 2080 */         else if (tmp.equals("[R]")) {
/* 2081 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 2082 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 2083 */           renderSmallEnergy(sb, orb_red, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2089 */           start_x += gl.width;
/* 2090 */         } else if (tmp.equals("[G]")) {
/* 2091 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 2092 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 2093 */           renderSmallEnergy(sb, orb_green, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2099 */           start_x += gl.width;
/* 2100 */         } else if (tmp.equals("[B]")) {
/* 2101 */           gl.width = (CARD_ENERGY_IMG_WIDTH * this.drawScale);
/* 2102 */           float tmp2 = (this.description.size() - 4) * spacing;
/* 2103 */           renderSmallEnergy(sb, orb_blue, (start_x - this.current_x) / Settings.scale / this.drawScale, -tmp2 - 172.0F + CARD_ENERGY_IMG_WIDTH * this.drawScale + i * spacing * 2.0F);
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2109 */           start_x += gl.width;
/*      */         }
/*      */         else
/*      */         {
/* 2113 */           gl.setText(font, tmp);
/* 2114 */           FontHelper.renderRotatedText(sb, font, tmp, this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2121 */             -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, this.textColor);
/*      */           
/*      */ 
/*      */ 
/* 2125 */           start_x += gl.width;
/*      */         }
/*      */       }
/*      */     }
/* 2129 */     font.getData().setScale(1.0F);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private float renderDynamicVariable(char key, float start_x, float draw_y, int i, BitmapFont font, SpriteBatch sb, Character end)
/*      */   {
/* 2141 */     StringBuilder stringBuilder = new StringBuilder();
/* 2142 */     Color c = null;
/* 2143 */     int num = 0;
/*      */     
/* 2145 */     switch (key) {
/*      */     case 'D': 
/* 2147 */       if (this.isDamageModified) {
/* 2148 */         num = this.damage;
/* 2149 */         if (this.damage >= this.baseDamage) {
/* 2150 */           c = Settings.GREEN_TEXT_COLOR;
/*      */         } else {
/* 2152 */           c = Settings.RED_TEXT_COLOR;
/*      */         }
/*      */       } else {
/* 2155 */         c = this.textColor;
/* 2156 */         num = this.baseDamage;
/*      */       }
/* 2158 */       break;
/*      */     case 'B': 
/* 2160 */       if (this.isBlockModified) {
/* 2161 */         num = this.block;
/* 2162 */         if (this.block >= this.baseBlock) {
/* 2163 */           c = Settings.GREEN_TEXT_COLOR;
/*      */         } else {
/* 2165 */           c = Settings.RED_TEXT_COLOR;
/*      */         }
/*      */       } else {
/* 2168 */         c = this.textColor;
/* 2169 */         num = this.baseBlock;
/*      */       }
/* 2171 */       break;
/*      */     case 'M': 
/* 2173 */       if (this.isMagicNumberModified) {
/* 2174 */         num = this.magicNumber;
/* 2175 */         if (this.magicNumber >= this.baseMagicNumber) {
/* 2176 */           c = Settings.GREEN_TEXT_COLOR;
/*      */         } else {
/* 2178 */           c = Settings.RED_TEXT_COLOR;
/*      */         }
/*      */       } else {
/* 2181 */         c = this.textColor;
/* 2182 */         num = this.baseMagicNumber;
/*      */       }
/* 2184 */       break;
/*      */     }
/*      */     
/*      */     
/*      */ 
/* 2189 */     stringBuilder.append(Integer.toString(num));
/* 2190 */     gl.setText(font, stringBuilder.toString());
/* 2191 */     FontHelper.renderRotatedText(sb, font, stringBuilder
/*      */     
/*      */ 
/* 2194 */       .toString(), this.current_x, this.current_y, start_x - this.current_x + gl.width / 2.0F, i * 1.45F * 
/*      */       
/*      */ 
/*      */ 
/* 2198 */       -font.getCapHeight() + draw_y - this.current_y + -6.0F, this.angle, true, c);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2203 */     if (end != null) {
/* 2204 */       FontHelper.renderRotatedText(sb, font, 
/*      */       
/*      */ 
/* 2207 */         Character.toString(end.charValue()), this.current_x, this.current_y, start_x - this.current_x + gl.width + 4.0F * Settings.scale, i * 1.45F * 
/*      */         
/*      */ 
/*      */ 
/* 2211 */         -font.getCapHeight() + draw_y - this.current_y + -6.0F, 0.0F, true, Settings.CREAM_COLOR);
/*      */       
/*      */ 
/*      */ 
/* 2215 */       stringBuilder.append(end);
/*      */     }
/*      */     
/* 2218 */     stringBuilder.append(' ');
/* 2219 */     gl.setText(font, stringBuilder.toString());
/* 2220 */     return gl.width;
/*      */   }
/*      */   
/*      */   private BitmapFont getDescFont() {
/* 2224 */     BitmapFont font = null;
/*      */     
/* 2226 */     if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2227 */       font = FontHelper.cardDescFont_N;
/*      */     } else {
/* 2229 */       font = FontHelper.cardDescFont_L;
/*      */     }
/*      */     
/* 2232 */     font.getData().setScale(this.drawScale);
/* 2233 */     return font;
/*      */   }
/*      */   
/*      */   private void renderTitle(SpriteBatch sb) {
/* 2237 */     BitmapFont font = null;
/*      */     
/*      */ 
/* 2240 */     if (this.isLocked) {
/* 2241 */       if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2242 */         font = FontHelper.cardTitleFont_N;
/*      */       } else {
/* 2244 */         font = FontHelper.cardTitleFont_L;
/*      */       }
/* 2246 */       font.getData().setScale(this.drawScale);
/* 2247 */       FontHelper.renderRotatedText(sb, font, LOCKED_STRING, this.current_x, this.current_y, 0.0F, 175.0F * this.drawScale * Settings.scale, this.angle, false, this.renderColor);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2259 */       return;
/*      */     }
/*      */     
/*      */ 
/* 2263 */     if (!this.isSeen) {
/* 2264 */       if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2265 */         font = FontHelper.cardTitleFont_N;
/*      */       } else {
/* 2267 */         font = FontHelper.cardTitleFont_L;
/*      */       }
/* 2269 */       font.getData().setScale(this.drawScale);
/* 2270 */       FontHelper.renderRotatedText(sb, font, UNKNOWN_STRING, this.current_x, this.current_y, 0.0F, 175.0F * this.drawScale * Settings.scale, this.angle, false, this.renderColor);
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2282 */       return;
/*      */     }
/*      */     
/*      */ 
/* 2286 */     if (!this.useSmallTitleFont) {
/* 2287 */       if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2288 */         font = FontHelper.cardTitleFont_N;
/*      */       } else {
/* 2290 */         font = FontHelper.cardTitleFont_L;
/*      */       }
/*      */     }
/* 2293 */     else if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2294 */       font = FontHelper.cardTitleFont_small_N;
/*      */     } else {
/* 2296 */       font = FontHelper.cardTitleFont_small_L;
/*      */     }
/*      */     
/*      */ 
/* 2300 */     font.getData().setScale(this.drawScale);
/* 2301 */     if (this.upgraded) {
/* 2302 */       Color color = Settings.GREEN_TEXT_COLOR.cpy();
/* 2303 */       color.a = this.renderColor.a;
/* 2304 */       FontHelper.renderRotatedText(sb, font, this.name, this.current_x, this.current_y, 0.0F, 175.0F * this.drawScale * Settings.scale, this.angle, false, color);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2316 */       FontHelper.renderRotatedText(sb, font, this.name, this.current_x, this.current_y, 0.0F, 175.0F * this.drawScale * Settings.scale, this.angle, false, this.renderColor);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */   private void renderType(SpriteBatch sb)
/*      */   {
/*      */     String text;
/*      */     
/*      */     String text;
/*      */     
/*      */     String text;
/*      */     
/*      */     String text;
/*      */     
/*      */     String text;
/*      */     String text;
/* 2333 */     switch (this.type) {
/*      */     case ATTACK: 
/* 2335 */       text = TEXT[0];
/* 2336 */       break;
/*      */     case SKILL: 
/* 2338 */       text = TEXT[1];
/* 2339 */       break;
/*      */     case POWER: 
/* 2341 */       text = TEXT[2];
/* 2342 */       break;
/*      */     case STATUS: 
/* 2344 */       text = TEXT[7];
/* 2345 */       break;
/*      */     case CURSE: 
/* 2347 */       text = TEXT[3];
/* 2348 */       break;
/*      */     default: 
/* 2350 */       text = TEXT[5];
/*      */     }
/*      */     
/*      */     BitmapFont font;
/*      */     BitmapFont font;
/* 2355 */     if ((this.angle == 0.0F) && (this.drawScale == 1.0F)) {
/* 2356 */       font = FontHelper.cardTypeFont_N;
/*      */     } else {
/* 2358 */       font = FontHelper.cardTypeFont_L;
/*      */     }
/* 2360 */     font.getData().setScale(this.drawScale);
/*      */     
/* 2362 */     FontHelper.renderRotatedText(sb, font, text, this.current_x, this.current_y - 22.0F * this.drawScale * Settings.scale, 0.0F, -1.0F * this.drawScale * Settings.scale, this.angle, false, new Color(0.35F, 0.35F, 0.35F, this.renderColor.a));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static int getPrice(CardRarity rarity)
/*      */   {
/* 2376 */     switch (rarity) {
/*      */     case BASIC: 
/* 2378 */       logger.info("ERROR: WHY WE SELLIN' BASIC");
/* 2379 */       return 9999;
/*      */     case COMMON: 
/* 2381 */       return 50;
/*      */     case UNCOMMON: 
/* 2383 */       return 75;
/*      */     case RARE: 
/* 2385 */       return 150;
/*      */     case SPECIAL: 
/* 2387 */       logger.info("ERROR: WHY WE SELLIN' SPECIAL");
/* 2388 */       return 9999;
/*      */     }
/* 2390 */     logger.info("No rarity on this card?");
/* 2391 */     return 0;
/*      */   }
/*      */   
/*      */   private void renderEnergy(SpriteBatch sb)
/*      */   {
/* 2396 */     if ((this.cost <= -2) || (this.darken) || (this.isLocked) || (!this.isSeen)) {
/* 2397 */       return;
/*      */     }
/*      */     
/* 2400 */     float drawX = this.current_x - 256.0F;
/* 2401 */     float drawY = this.current_y - 256.0F;
/*      */     
/*      */ 
/* 2404 */     switch (this.color) {
/*      */     case RED: 
/* 2406 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_RED_ORB, drawX, drawY);
/* 2407 */       break;
/*      */     case GREEN: 
/* 2409 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_GREEN_ORB, drawX, drawY);
/* 2410 */       break;
/*      */     case BLUE: 
/* 2412 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_BLUE_ORB, drawX, drawY);
/* 2413 */       break;
/*      */     case COLORLESS: 
/* 2415 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_COLORLESS_ORB, drawX, drawY);
/*      */     
/*      */     case CURSE: 
/*      */     default: 
/* 2419 */       renderHelper(sb, this.renderColor, ImageMaster.CARD_COLORLESS_ORB, drawX, drawY);
/*      */     }
/*      */     
/*      */     
/* 2423 */     Color costColor = Color.WHITE.cpy();
/* 2424 */     if ((AbstractDungeon.player != null) && (AbstractDungeon.player.hand.contains(this)) && (!hasEnoughEnergy())) {
/* 2425 */       costColor = ENERGY_COST_RESTRICTED_COLOR;
/* 2426 */     } else if ((this.isCostModified) || (this.isCostModifiedForTurn) || (this.freeToPlayOnce)) {
/* 2427 */       costColor = ENERGY_COST_MODIFIED_COLOR;
/*      */     }
/* 2429 */     costColor.a = this.transparency;
/*      */     
/* 2431 */     String text = getCost();
/* 2432 */     BitmapFont font = getEnergyFont();
/*      */     
/* 2434 */     if (((this.type != CardType.STATUS) || (this.cardID.equals("Slimed"))) && ((this.color != CardColor.CURSE) || (this.cardID.equals("Pride"))))
/*      */     {
/* 2436 */       FontHelper.renderRotatedText(sb, font, text, this.current_x, this.current_y, -132.0F * this.drawScale * Settings.scale, 192.0F * this.drawScale * Settings.scale, this.angle, false, costColor);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void updateCost(int amt)
/*      */   {
/* 2457 */     if (((this.color != CardColor.CURSE) || (this.cardID.equals("Pride"))) && ((this.type != CardType.STATUS) || (this.cardID.equals("Slimed"))))
/*      */     {
/* 2459 */       int tmpCost = this.cost;
/* 2460 */       int diff = this.cost - this.costForTurn;
/*      */       
/* 2462 */       if (amt != 0) {
/* 2463 */         tmpCost += amt;
/* 2464 */         if (tmpCost < 0) {
/* 2465 */           tmpCost = 0;
/*      */         }
/*      */         
/* 2468 */         if (tmpCost != this.cost) {
/* 2469 */           this.isCostModified = true;
/* 2470 */           this.cost = tmpCost;
/* 2471 */           this.costForTurn = (this.cost - diff);
/*      */           
/* 2473 */           if (this.costForTurn < 0) {
/* 2474 */             this.costForTurn = 0;
/*      */           }
/*      */         }
/*      */       }
/*      */     } else {
/* 2479 */       logger.info("Curses/Statuses cannot have their costs modified");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCostForTurn(int amt)
/*      */   {
/* 2489 */     if (this.costForTurn > 0) {
/* 2490 */       this.costForTurn = amt;
/* 2491 */       if (this.costForTurn < 0) {
/* 2492 */         this.costForTurn = 0;
/*      */       }
/*      */       
/* 2495 */       if (this.costForTurn != this.cost) {
/* 2496 */         this.isCostModifiedForTurn = true;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void modifyCostForTurn(int amt)
/*      */   {
/* 2507 */     if (this.costForTurn > 0) {
/* 2508 */       this.costForTurn += amt;
/* 2509 */       if (this.costForTurn < 0) {
/* 2510 */         this.costForTurn = 0;
/*      */       }
/*      */       
/* 2513 */       if (this.costForTurn != this.cost) {
/* 2514 */         this.isCostModifiedForTurn = true;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void modifyCostForCombat(int amt)
/*      */   {
/* 2525 */     if (this.costForTurn > 0) {
/* 2526 */       this.costForTurn += amt;
/* 2527 */       if (this.costForTurn < 0) {
/* 2528 */         this.costForTurn = 0;
/*      */       }
/*      */       
/* 2531 */       if (this.cost != this.costForTurn) {
/* 2532 */         this.isCostModified = true;
/*      */       }
/* 2534 */       this.cost = this.costForTurn;
/*      */     } else {
/* 2536 */       this.cost += amt;
/* 2537 */       if (this.cost < 0) {
/* 2538 */         this.cost = 0;
/*      */       }
/* 2540 */       this.costForTurn = 0;
/* 2541 */       if (this.cost != this.costForTurn) {
/* 2542 */         this.isCostModified = true;
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void resetAttributes()
/*      */   {
/* 2553 */     this.block = this.baseBlock;
/* 2554 */     this.isBlockModified = false;
/* 2555 */     this.damage = this.baseDamage;
/* 2556 */     this.isDamageModified = false;
/* 2557 */     this.magicNumber = this.baseMagicNumber;
/* 2558 */     this.isMagicNumberModified = false;
/* 2559 */     this.damageTypeForTurn = this.damageType;
/* 2560 */     this.costForTurn = this.cost;
/* 2561 */     this.isCostModifiedForTurn = false;
/*      */   }
/*      */   
/*      */   private String getCost() {
/* 2565 */     if (this.cost == -1)
/* 2566 */       return "X";
/* 2567 */     if (this.freeToPlayOnce) {
/* 2568 */       return "0";
/*      */     }
/* 2570 */     return Integer.toString(this.costForTurn);
/*      */   }
/*      */   
/*      */   private BitmapFont getEnergyFont()
/*      */   {
/* 2575 */     FontHelper.cardEnergyFont_L.getData().setScale(this.drawScale);
/* 2576 */     return FontHelper.cardEnergyFont_L;
/*      */   }
/*      */   
/*      */   public void hover() {
/* 2580 */     if (!this.hovered) {
/* 2581 */       this.hovered = true;
/* 2582 */       this.drawScale = 1.0F;
/* 2583 */       this.targetDrawScale = 1.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   public void unhover() {
/* 2588 */     if (this.hovered) {
/* 2589 */       this.hovered = false;
/* 2590 */       this.hoverDuration = 0.0F;
/* 2591 */       this.renderTip = false;
/* 2592 */       this.targetDrawScale = 0.75F;
/*      */     }
/*      */   }
/*      */   
/*      */   public void updateHoverLogic() {
/* 2597 */     this.hb.update();
/* 2598 */     if (this.hb.hovered) {
/* 2599 */       hover();
/* 2600 */       this.hoverDuration += Gdx.graphics.getDeltaTime();
/* 2601 */       if ((this.hoverDuration > 0.2F) && 
/* 2602 */         (!Settings.hideCards)) {
/* 2603 */         this.renderTip = true;
/*      */       }
/*      */     }
/*      */     else {
/* 2607 */       unhover();
/*      */     }
/*      */   }
/*      */   
/*      */   public void untip() {
/* 2612 */     this.hoverDuration = 0.0F;
/* 2613 */     this.renderTip = false;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void moveToDiscardPile()
/*      */   {
/* 2620 */     this.target_x = CardGroup.DISCARD_PILE_X;
/* 2621 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 2622 */       this.target_y = 0.0F;
/*      */     } else {
/* 2624 */       this.target_y = (0.0F - OverlayMenu.HAND_HIDE_Y);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void teleportToDiscardPile()
/*      */   {
/* 2632 */     this.current_x = CardGroup.DISCARD_PILE_X;
/* 2633 */     this.target_x = this.current_x;
/* 2634 */     if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
/* 2635 */       this.current_y = 0.0F;
/*      */     } else {
/* 2637 */       this.current_y = (0.0F - OverlayMenu.HAND_HIDE_Y);
/*      */     }
/* 2639 */     this.target_y = this.current_y;
/* 2640 */     onMoveToDiscard();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void onMoveToDiscard() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void renderCardTip(SpriteBatch sb)
/*      */   {
/* 2654 */     if ((!Settings.hideCards) && 
/* 2655 */       (this.renderTip)) {
/* 2656 */       if ((AbstractDungeon.player != null) && (AbstractDungeon.player.isDraggingCard)) {
/* 2657 */         return;
/*      */       }
/*      */       
/* 2660 */       if (this.isLocked) {
/* 2661 */         ArrayList<String> locked = new ArrayList();
/* 2662 */         locked.add(0, "locked");
/* 2663 */         TipHelper.renderTipForCard(this, sb, locked);
/* 2664 */         return; }
/* 2665 */       if (!this.isSeen) {
/* 2666 */         ArrayList<String> unseen = new ArrayList();
/* 2667 */         unseen.add(0, "unseen");
/* 2668 */         TipHelper.renderTipForCard(this, sb, unseen);
/* 2669 */         return;
/*      */       }
/*      */       
/* 2672 */       if ((SingleCardViewPopup.isViewingUpgrade) && (this.isSeen) && (!this.isLocked)) {
/* 2673 */         AbstractCard copy = makeCopy();
/* 2674 */         copy.current_x = this.current_x;
/* 2675 */         copy.current_y = this.current_y;
/* 2676 */         copy.drawScale = this.drawScale;
/* 2677 */         copy.upgrade();
/*      */         
/* 2679 */         TipHelper.renderTipForCard(copy, sb, copy.keywords);
/*      */       } else {
/* 2681 */         TipHelper.renderTipForCard(this, sb, this.keywords);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerWhenDrawn() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerWhenCopied() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnEndOfPlayerTurn()
/*      */   {
/* 2704 */     if (this.isEthereal) {
/* 2705 */       AbstractDungeon.actionManager.addToTop(new ExhaustSpecificCardAction(this, AbstractDungeon.player.hand));
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnEndOfTurnForPlayingCard() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnOtherCardPlayed(AbstractCard c) {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnGainEnergy(int e, boolean dueToCard) {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnManualDiscard() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnCardPlayed(AbstractCard cardPlayed) {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void onPlayCard(AbstractCard c, AbstractMonster m) {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void atTurnStart() {}
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void triggerOnExhaust() {}
/*      */   
/*      */ 
/*      */ 
/*      */   public void applyPowers()
/*      */   {
/* 2753 */     applyPowersToBlock();
/* 2754 */     AbstractPlayer player = AbstractDungeon.player;
/* 2755 */     this.isDamageModified = false;
/*      */     
/*      */ 
/* 2758 */     if (!this.isMultiDamage) {
/* 2759 */       float tmp = this.baseDamage;
/* 2760 */       if ((this instanceof PerfectedStrike)) {
/* 2761 */         if (this.upgraded) {
/* 2762 */           tmp += 3 * PerfectedStrike.countCards();
/*      */         } else {
/* 2764 */           tmp += 2 * PerfectedStrike.countCards();
/*      */         }
/* 2766 */         if (this.baseDamage != (int)tmp) {
/* 2767 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2772 */       if ((AbstractDungeon.player.hasRelic("WristBlade")) && ((this.costForTurn == 0) || (this.freeToPlayOnce))) {
/* 2773 */         tmp += 3.0F;
/* 2774 */         if (this.baseDamage != (int)tmp) {
/* 2775 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 2781 */       for (AbstractPower p : player.powers) {
/* 2782 */         if (((this instanceof HeavyBlade)) && ((p instanceof StrengthPower))) {
/* 2783 */           if (this.upgraded) {
/* 2784 */             tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2785 */             tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/*      */           }
/* 2787 */           tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2788 */           tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/*      */         }
/* 2790 */         tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2791 */         if (this.baseDamage != (int)tmp) {
/* 2792 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2797 */       for (AbstractPower p : player.powers) {
/* 2798 */         tmp = p.atDamageFinalGive(tmp, this.damageTypeForTurn);
/* 2799 */         if (this.baseDamage != (int)tmp) {
/* 2800 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2805 */       if (tmp < 0.0F) {
/* 2806 */         tmp = 0.0F;
/*      */       }
/*      */       
/*      */ 
/* 2810 */       this.damage = MathUtils.floor(tmp);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 2815 */       ArrayList<AbstractMonster> m = AbstractDungeon.getCurrRoom().monsters.monsters;
/* 2816 */       float[] tmp = new float[m.size()];
/* 2817 */       for (int i = 0; i < tmp.length; i++) {
/* 2818 */         tmp[i] = this.baseDamage;
/*      */       }
/*      */       
/*      */ 
/* 2822 */       for (int i = 0; i < tmp.length; i++)
/*      */       {
/* 2824 */         if ((AbstractDungeon.player.hasRelic("WristBlade")) && ((this.costForTurn == 0) || (this.freeToPlayOnce))) {
/* 2825 */           tmp[i] += 3.0F;
/* 2826 */           if (this.baseDamage != (int)tmp[i]) {
/* 2827 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */         
/* 2831 */         for (AbstractPower p : player.powers) {
/* 2832 */           tmp[i] = p.atDamageGive(tmp[i], this.damageTypeForTurn);
/* 2833 */           if (this.baseDamage != (int)tmp[i]) {
/* 2834 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2840 */       for (int i = 0; i < tmp.length; i++) {
/* 2841 */         for (AbstractPower p : player.powers) {
/* 2842 */           tmp[i] = p.atDamageFinalGive(tmp[i], this.damageTypeForTurn);
/* 2843 */           if (this.baseDamage != (int)tmp[i]) {
/* 2844 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2850 */       for (int i = 0; i < tmp.length; i++) {
/* 2851 */         if (tmp[i] < 0.0F) {
/* 2852 */           tmp[i] = 0.0F;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2857 */       this.multiDamage = new int[tmp.length];
/* 2858 */       for (int i = 0; i < tmp.length; i++) {
/* 2859 */         this.multiDamage[i] = MathUtils.floor(tmp[i]);
/*      */       }
/* 2861 */       this.damage = this.multiDamage[0];
/*      */     }
/*      */   }
/*      */   
/*      */   private void applyPowersToBlock() {
/* 2866 */     this.isBlockModified = false;
/* 2867 */     float tmp = this.baseBlock;
/* 2868 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/* 2869 */       tmp = p.modifyBlock(tmp);
/* 2870 */       if (this.baseBlock != MathUtils.floor(tmp))
/*      */       {
/* 2872 */         this.isBlockModified = true; }
/*      */     }
/* 2874 */     if (tmp < 0.0F) {
/* 2875 */       tmp = 0.0F;
/*      */     }
/* 2877 */     this.block = MathUtils.floor(tmp);
/*      */   }
/*      */   
/*      */   public static int applyPowerOnBlockHelper(int base) {
/* 2881 */     float tmp = base;
/* 2882 */     for (AbstractPower p : AbstractDungeon.player.powers) {
/* 2883 */       tmp = p.modifyBlock(tmp);
/*      */     }
/* 2885 */     return MathUtils.floor(tmp);
/*      */   }
/*      */   
/*      */   public void calculateDamageDisplay(AbstractMonster mo) {
/* 2889 */     calculateCardDamage(mo);
/*      */   }
/*      */   
/*      */   public void calculateCardDamage(AbstractMonster mo) {
/* 2893 */     applyPowersToBlock();
/* 2894 */     AbstractPlayer player = AbstractDungeon.player;
/* 2895 */     this.isDamageModified = false;
/*      */     
/*      */ 
/* 2898 */     if ((!this.isMultiDamage) && (mo != null)) {
/* 2899 */       float tmp = this.baseDamage;
/* 2900 */       if ((this instanceof PerfectedStrike)) {
/* 2901 */         if (this.upgraded) {
/* 2902 */           tmp += 3 * PerfectedStrike.countCards();
/*      */         } else {
/* 2904 */           tmp += 2 * PerfectedStrike.countCards();
/*      */         }
/* 2906 */         if (this.baseDamage != (int)tmp) {
/* 2907 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2912 */       if ((AbstractDungeon.player.hasRelic("WristBlade")) && ((this.costForTurn == 0) || (this.freeToPlayOnce))) {
/* 2913 */         tmp += 3.0F;
/* 2914 */         if (this.baseDamage != (int)tmp) {
/* 2915 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/*      */ 
/* 2921 */       for (AbstractPower p : player.powers) {
/* 2922 */         if (((this instanceof HeavyBlade)) && ((p instanceof StrengthPower))) {
/* 2923 */           if (this.upgraded) {
/* 2924 */             tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2925 */             tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/*      */           }
/* 2927 */           tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2928 */           tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/*      */         }
/* 2930 */         tmp = p.atDamageGive(tmp, this.damageTypeForTurn);
/* 2931 */         if (this.baseDamage != (int)tmp) {
/* 2932 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2937 */       if (mo != null) {
/* 2938 */         for (AbstractPower p : mo.powers) {
/* 2939 */           tmp = p.atDamageReceive(tmp, this.damageTypeForTurn);
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2944 */       for (AbstractPower p : player.powers) {
/* 2945 */         tmp = p.atDamageFinalGive(tmp, this.damageTypeForTurn);
/* 2946 */         if (this.baseDamage != (int)tmp) {
/* 2947 */           this.isDamageModified = true;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2952 */       if (mo != null) {
/* 2953 */         for (AbstractPower p : mo.powers) {
/* 2954 */           tmp = p.atDamageFinalReceive(tmp, this.damageTypeForTurn);
/* 2955 */           if (this.baseDamage != (int)tmp) {
/* 2956 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2962 */       if (tmp < 0.0F) {
/* 2963 */         tmp = 0.0F;
/*      */       }
/*      */       
/*      */ 
/* 2967 */       this.damage = MathUtils.floor(tmp);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 2972 */       ArrayList<AbstractMonster> m = AbstractDungeon.getCurrRoom().monsters.monsters;
/* 2973 */       float[] tmp = new float[m.size()];
/* 2974 */       for (int i = 0; i < tmp.length; i++) {
/* 2975 */         tmp[i] = this.baseDamage;
/*      */       }
/*      */       
/*      */ 
/* 2979 */       for (int i = 0; i < tmp.length; i++)
/*      */       {
/*      */ 
/* 2982 */         if ((AbstractDungeon.player.hasRelic("WristBlade")) && ((this.costForTurn == 0) || (this.freeToPlayOnce))) {
/* 2983 */           tmp[i] += 3.0F;
/* 2984 */           if (this.baseDamage != (int)tmp[i]) {
/* 2985 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */         
/*      */ 
/* 2990 */         for (AbstractPower p : player.powers) {
/* 2991 */           tmp[i] = p.atDamageGive(tmp[i], this.damageTypeForTurn);
/* 2992 */           if (this.baseDamage != (int)tmp[i]) {
/* 2993 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 2999 */       for (int i = 0; i < tmp.length; i++) {
/* 3000 */         for (AbstractPower p : ((AbstractMonster)m.get(i)).powers) {
/* 3001 */           if ((!((AbstractMonster)m.get(i)).isDying) && (!((AbstractMonster)m.get(i)).isEscaping)) {
/* 3002 */             tmp[i] = p.atDamageReceive(tmp[i], this.damageTypeForTurn);
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 3008 */       for (int i = 0; i < tmp.length; i++) {
/* 3009 */         for (AbstractPower p : player.powers) {
/* 3010 */           tmp[i] = p.atDamageFinalGive(tmp[i], this.damageTypeForTurn);
/* 3011 */           if (this.baseDamage != (int)tmp[i]) {
/* 3012 */             this.isDamageModified = true;
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 3018 */       for (int i = 0; i < tmp.length; i++) {
/* 3019 */         for (AbstractPower p : ((AbstractMonster)m.get(i)).powers) {
/* 3020 */           if ((!((AbstractMonster)m.get(i)).isDying) && (!((AbstractMonster)m.get(i)).isEscaping)) {
/* 3021 */             tmp[i] = p.atDamageFinalReceive(tmp[i], this.damageTypeForTurn);
/*      */           }
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 3027 */       for (int i = 0; i < tmp.length; i++) {
/* 3028 */         if (tmp[i] < 0.0F) {
/* 3029 */           tmp[i] = 0.0F;
/*      */         }
/*      */       }
/*      */       
/*      */ 
/* 3034 */       this.multiDamage = new int[tmp.length];
/* 3035 */       for (int i = 0; i < tmp.length; i++) {
/* 3036 */         this.multiDamage[i] = MathUtils.floor(tmp[i]);
/*      */       }
/* 3038 */       this.damage = this.multiDamage[0];
/*      */     }
/*      */   }
/*      */   
/*      */   public void setAngle(float degrees, boolean immediate) {
/* 3043 */     this.targetAngle = degrees;
/* 3044 */     if (immediate) {
/* 3045 */       this.angle = this.targetAngle;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void shrink()
/*      */   {
/* 3053 */     this.targetDrawScale = 0.12F;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void shrink(boolean immediate)
/*      */   {
/* 3060 */     this.targetDrawScale = 0.12F;
/* 3061 */     this.drawScale = 0.12F;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void darken(boolean immediate)
/*      */   {
/* 3068 */     this.darken = true;
/* 3069 */     this.darkTimer = 0.3F;
/* 3070 */     if (immediate) {
/* 3071 */       this.tintColor.a = 1.0F;
/* 3072 */       this.darkTimer = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void lighten(boolean immediate)
/*      */   {
/* 3080 */     this.darken = false;
/* 3081 */     this.darkTimer = 0.3F;
/* 3082 */     if (immediate) {
/* 3083 */       this.tintColor.a = 0.0F;
/* 3084 */       this.darkTimer = 0.0F;
/*      */     }
/*      */   }
/*      */   
/*      */   private void updateColor() {
/* 3089 */     if (this.darkTimer != 0.0F) {
/* 3090 */       this.darkTimer -= Gdx.graphics.getDeltaTime();
/* 3091 */       if (this.darkTimer < 0.0F) {
/* 3092 */         this.darkTimer = 0.0F;
/*      */       }
/*      */       
/* 3095 */       if (this.darken) {
/* 3096 */         this.tintColor.a = (1.0F - this.darkTimer * 1.0F / 0.3F);
/*      */       }
/*      */       else {
/* 3099 */         this.tintColor.a = (this.darkTimer * 1.0F / 0.3F);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   public void superFlash(Color c) {
/* 3105 */     this.flashVfx = new CardFlashVfx(this, c, true);
/*      */   }
/*      */   
/*      */   public void superFlash() {
/* 3109 */     this.flashVfx = new CardFlashVfx(this, true);
/*      */   }
/*      */   
/*      */   public void flash() {
/* 3113 */     this.flashVfx = new CardFlashVfx(this);
/*      */   }
/*      */   
/*      */   public void flash(Color c) {
/* 3117 */     this.flashVfx = new CardFlashVfx(this, c);
/*      */   }
/*      */   
/*      */   public void darkFlash(Color c) {
/* 3121 */     this.flashVfx = new CardDarkFlashVfx(this, c);
/*      */   }
/*      */   
/*      */   public void unfadeOut() {
/* 3125 */     this.fadingOut = false;
/* 3126 */     this.transparency = 1.0F;
/* 3127 */     this.targetTransparency = 1.0F;
/*      */     
/* 3129 */     this.bannerColor.a = this.transparency;
/* 3130 */     this.backColor.a = this.transparency;
/* 3131 */     this.frameColor.a = this.transparency;
/* 3132 */     this.bgColor.a = this.transparency;
/* 3133 */     this.descBoxColor.a = this.transparency;
/* 3134 */     this.imgFrameColor.a = this.transparency;
/* 3135 */     this.frameShadowColor.a = (this.transparency / 4.0F);
/* 3136 */     this.renderColor.a = this.transparency;
/* 3137 */     this.goldColor.a = this.transparency;
/*      */     
/* 3139 */     if (this.frameOutlineColor != null) {
/* 3140 */       this.frameOutlineColor.a = this.transparency;
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   private void updateTransparency()
/*      */   {
/* 3148 */     if ((this.fadingOut) && (this.transparency != 0.0F)) {
/* 3149 */       this.transparency -= Gdx.graphics.getDeltaTime() * 2.0F;
/* 3150 */       if (this.transparency < 0.0F) {
/* 3151 */         this.transparency = 0.0F;
/*      */       }
/* 3153 */     } else if (this.transparency != this.targetTransparency) {
/* 3154 */       this.transparency += Gdx.graphics.getDeltaTime() * 1.5F;
/* 3155 */       if (this.transparency > this.targetTransparency) {
/* 3156 */         this.transparency = this.targetTransparency;
/*      */       }
/*      */     }
/*      */     
/* 3160 */     this.bannerColor.a = this.transparency;
/* 3161 */     this.backColor.a = this.transparency;
/* 3162 */     this.frameColor.a = this.transparency;
/* 3163 */     this.bgColor.a = this.transparency;
/* 3164 */     this.descBoxColor.a = this.transparency;
/* 3165 */     this.imgFrameColor.a = this.transparency;
/* 3166 */     this.frameShadowColor.a = (this.transparency / 4.0F);
/* 3167 */     this.renderColor.a = this.transparency;
/* 3168 */     this.textColor.a = this.transparency;
/* 3169 */     this.goldColor.a = this.transparency;
/*      */     
/* 3171 */     if (this.frameOutlineColor != null) {
/* 3172 */       this.frameOutlineColor.a = this.transparency;
/*      */     }
/*      */   }
/*      */   
/*      */   public void setAngle(float degrees) {
/* 3177 */     setAngle(degrees, false);
/*      */   }
/*      */   
/*      */   protected String getCantPlayMessage() {
/* 3181 */     return TEXT[13];
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public void clearPowers()
/*      */   {
/* 3188 */     resetAttributes();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 3200 */     this.isDamageModified = false;
/*      */   }
/*      */   
/*      */   public static void debugPrintDetailedCardDataHeader() {
/* 3204 */     logger.info(gameDataUploadHeader());
/*      */   }
/*      */   
/*      */   public static String gameDataUploadHeader() {
/* 3208 */     GameDataStringBuilder builder = new GameDataStringBuilder();
/*      */     
/*      */ 
/* 3211 */     builder.addFieldData("name");
/* 3212 */     builder.addFieldData("cardID");
/* 3213 */     builder.addFieldData("rawDescription");
/* 3214 */     builder.addFieldData("assetURL");
/* 3215 */     builder.addFieldData("jokeAssetURL");
/*      */     
/*      */ 
/* 3218 */     builder.addFieldData("keywords");
/* 3219 */     builder.addFieldData("color");
/* 3220 */     builder.addFieldData("type");
/* 3221 */     builder.addFieldData("rarity");
/*      */     
/*      */ 
/* 3224 */     builder.addFieldData("cost");
/* 3225 */     builder.addFieldData("target");
/* 3226 */     builder.addFieldData("damageType");
/*      */     
/* 3228 */     builder.addFieldData("baseDamage");
/* 3229 */     builder.addFieldData("baseBlock");
/* 3230 */     builder.addFieldData("baseHeal");
/* 3231 */     builder.addFieldData("baseDraw");
/* 3232 */     builder.addFieldData("baseDiscard");
/* 3233 */     builder.addFieldData("baseMagicNumber");
/* 3234 */     builder.addFieldData("isMultiDamage");
/* 3235 */     return builder.toString();
/*      */   }
/*      */   
/*      */   public void debugPrintDetailedCardData() {
/* 3239 */     logger.info(gameDataUploadData());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public String gameDataUploadData()
/*      */   {
/* 3246 */     GameDataStringBuilder builder = new GameDataStringBuilder();
/*      */     
/*      */ 
/* 3249 */     builder.addFieldData(this.name);
/* 3250 */     builder.addFieldData(this.cardID);
/* 3251 */     builder.addFieldData(this.rawDescription);
/* 3252 */     builder.addFieldData(this.assetURL);
/* 3253 */     builder.addFieldData(this.jokeAssetURL);
/*      */     
/*      */ 
/* 3256 */     builder.addFieldData(Arrays.toString(this.keywords.toArray()));
/* 3257 */     builder.addFieldData(this.color.name());
/* 3258 */     builder.addFieldData(this.type.name());
/* 3259 */     builder.addFieldData(this.rarity.name());
/*      */     
/*      */ 
/* 3262 */     builder.addFieldData(this.cost);
/* 3263 */     builder.addFieldData(this.target.name());
/* 3264 */     builder.addFieldData(this.damageType.name());
/*      */     
/* 3266 */     builder.addFieldData(this.baseDamage);
/* 3267 */     builder.addFieldData(this.baseBlock);
/* 3268 */     builder.addFieldData(this.baseHeal);
/* 3269 */     builder.addFieldData(this.baseDraw);
/* 3270 */     builder.addFieldData(this.baseDiscard);
/* 3271 */     builder.addFieldData(this.baseMagicNumber);
/* 3272 */     builder.addFieldData(this.isMultiDamage);
/*      */     
/* 3274 */     return builder.toString();
/*      */   }
/*      */   
/*      */   public String toString()
/*      */   {
/* 3279 */     return this.name;
/*      */   }
/*      */   
/*      */   public int compareTo(AbstractCard other)
/*      */   {
/* 3284 */     return this.cardID.compareTo(other.cardID);
/*      */   }
/*      */   
/*      */   public void setLocked() {
/* 3288 */     this.isLocked = true;
/* 3289 */     switch (this.type) {
/*      */     case ATTACK: 
/* 3291 */       this.portraitImg = ImageMaster.CARD_LOCKED_ATTACK;
/* 3292 */       break;
/*      */     case POWER: 
/* 3294 */       this.portraitImg = ImageMaster.CARD_LOCKED_POWER;
/* 3295 */       break;
/*      */     default: 
/* 3297 */       this.portraitImg = ImageMaster.CARD_LOCKED_SKILL;
/*      */     }
/*      */     
/* 3300 */     initializeDescription();
/*      */   }
/*      */   
/*      */   public void unlock() {
/* 3304 */     this.isLocked = false;
/*      */     
/* 3306 */     this.portrait = cardAtlas.findRegion(this.assetURL);
/* 3307 */     if (this.portrait == null) {
/* 3308 */       if (this.assetURL != null) {
/* 3309 */         logger.info(this.cardID + " url was not found.");
/*      */       }
/* 3311 */       this.portrait = oldCardAtlas.findRegion(this.jokeAssetURL);
/*      */     }
/*      */   }
/*      */   
/*      */   public HashMap<String, Serializable> getLocStrings()
/*      */   {
/* 3317 */     HashMap<String, Serializable> cardData = new HashMap();
/* 3318 */     initializeDescription();
/* 3319 */     cardData.put("name", this.name);
/* 3320 */     cardData.put("description", this.rawDescription);
/* 3321 */     return cardData;
/*      */   }
/*      */   
/*      */   public String getMetricID() {
/* 3325 */     String id = this.cardID;
/* 3326 */     if (this.upgraded) {
/* 3327 */       id = id + "+";
/* 3328 */       if (this.timesUpgraded > 0) {
/* 3329 */         id = id + this.timesUpgraded;
/*      */       }
/*      */     }
/* 3332 */     return id;
/*      */   }
/*      */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\AbstractCard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */