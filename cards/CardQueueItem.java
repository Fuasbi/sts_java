/*    */ package com.megacrit.cardcrawl.cards;
/*    */ 
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class CardQueueItem
/*    */ {
/*    */   public AbstractCard card;
/*    */   public AbstractMonster monster;
/*  9 */   public int energyOnUse = 0;
/* 10 */   public boolean isEndTurnAutoPlay = false;
/*    */   
/*    */   public CardQueueItem() {
/* 13 */     this.card = null;
/* 14 */     this.monster = null;
/*    */   }
/*    */   
/*    */   public CardQueueItem(AbstractCard card, AbstractMonster monster) {
/* 18 */     this(card, monster, com.megacrit.cardcrawl.ui.panels.EnergyPanel.getCurrentEnergy());
/*    */   }
/*    */   
/*    */   public CardQueueItem(AbstractCard card, AbstractMonster monster, int setEnergyOnUse) {
/* 22 */     this.card = card;
/* 23 */     this.monster = monster;
/* 24 */     this.energyOnUse = setEnergyOnUse;
/*    */   }
/*    */   
/*    */   public CardQueueItem(AbstractCard card, boolean isEndTurnAutoPlay) {
/* 28 */     this(card, null);
/* 29 */     this.isEndTurnAutoPlay = isEndTurnAutoPlay;
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\CardQueueItem.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */