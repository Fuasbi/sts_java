/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class ForceField extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Force Field";
/* 13 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Force Field");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 4;
/*    */   private static final int BLOCK_AMT = 12;
/*    */   
/*    */   public ForceField() {
/* 20 */     super("Force Field", NAME, "blue/skill/forcefield", "blue/skill/forcefield", 4, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 32 */     this.baseBlock = 12;
/* 33 */     if (CardCrawlGame.dungeon != null) {
/* 34 */       configureCostsOnNewCard();
/*    */     }
/*    */   }
/*    */   
/*    */   public void configureCostsOnNewCard() {
/* 39 */     for (AbstractCard c : com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.cardsPlayedThisCombat) {
/* 40 */       if (c.type == AbstractCard.CardType.POWER) {
/* 41 */         updateCost(-1);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void triggerOnCardPlayed(AbstractCard c)
/*    */   {
/* 48 */     if (c.type == AbstractCard.CardType.POWER) {
/* 49 */       updateCost(-1);
/*    */     }
/*    */   }
/*    */   
/*    */   public void use(com.megacrit.cardcrawl.characters.AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 55 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(p, p, this.block));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 60 */     return new ForceField();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 65 */     if (!this.upgraded) {
/* 66 */       upgradeName();
/* 67 */       upgradeBlock(4);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\ForceField.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */