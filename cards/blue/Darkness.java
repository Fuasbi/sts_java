/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Darkness extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Darkness";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Darkness");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Darkness() {
/* 22 */     super("Darkness", NAME, null, "blue/skill/darkness", 1, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.showEvokeValue = true;
/* 35 */     this.showEvokeOrbCount = 1;
/* 36 */     this.baseMagicNumber = 1;
/* 37 */     this.magicNumber = 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Dark()));
/* 44 */     if (this.upgraded) {
/* 45 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.DarkImpulseAction());
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 51 */     return new Darkness();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 56 */     if (!this.upgraded) {
/* 57 */       upgradeName();
/* 58 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 59 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Darkness.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */