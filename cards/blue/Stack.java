/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Stack extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Stack";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Stack");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 16 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Stack()
/*    */   {
/* 22 */     super("Stack", NAME, "blue/skill/stack", "blue/skill/stack", 1, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.baseBlock = 0;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(p, p, this.block));
/*    */     
/* 41 */     if (!this.upgraded) {
/* 42 */       this.rawDescription = DESCRIPTION;
/*    */     } else {
/* 44 */       this.rawDescription = UPGRADE_DESCRIPTION;
/*    */     }
/* 46 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 51 */     return new Stack();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 56 */     this.baseBlock = AbstractDungeon.player.discardPile.size();
/* 57 */     if (this.upgraded) {
/* 58 */       this.baseBlock += 3;
/*    */     }
/* 60 */     super.applyPowers();
/*    */     
/* 62 */     if (!this.upgraded) {
/* 63 */       this.rawDescription = DESCRIPTION;
/*    */     } else {
/* 65 */       this.rawDescription = UPGRADE_DESCRIPTION;
/*    */     }
/* 67 */     this.rawDescription += EXTENDED_DESCRIPTION[0];
/* 68 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 73 */     if (!this.upgraded) {
/* 74 */       upgradeName();
/* 75 */       upgradeBlock(3);
/* 76 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 77 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Stack.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */