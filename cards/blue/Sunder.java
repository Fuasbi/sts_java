/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.utility.WaitAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.helpers.Hitbox;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Sunder extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Sunder";
/* 17 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Sunder");
/* 18 */   public static final String NAME = cardStrings.NAME;
/* 19 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 3;
/*    */   private static final int ATTACK_DMG = 24;
/*    */   
/*    */   public Sunder()
/*    */   {
/* 25 */     super("Sunder", NAME, "blue/attack/sunder", "blue/attack/sunder", 3, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 37 */     this.baseDamage = 24;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 42 */     if (m != null) {
/* 43 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.animations.VFXAction(new com.megacrit.cardcrawl.vfx.combat.WeightyImpactEffect(m.hb.cX, m.hb.cY)));
/* 44 */       AbstractDungeon.actionManager.addToBottom(new WaitAction(0.8F));
/*    */     }
/*    */     
/* 47 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.SunderAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), 3));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 52 */     return new Sunder();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 57 */     if (!this.upgraded) {
/* 58 */       upgradeName();
/* 59 */       upgradeDamage(8);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Sunder.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */