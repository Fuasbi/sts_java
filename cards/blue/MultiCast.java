/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.ui.panels.EnergyPanel;
/*    */ 
/*    */ public class MultiCast extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Multi-Cast";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Multi-Cast");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = -1;
/*    */   
/*    */   public MultiCast() {
/* 21 */     super("Multi-Cast", NAME, null, "blue/skill/multicast", -1, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.showEvokeValue = true;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 40 */     if (this.energyOnUse < EnergyPanel.totalCount) {
/* 41 */       this.energyOnUse = EnergyPanel.totalCount;
/*    */     }
/*    */     
/* 44 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.MulticastAction(p, this.energyOnUse, this.upgraded, this.freeToPlayOnce));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 49 */     return new MultiCast();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 54 */     if (!this.upgraded) {
/* 55 */       upgradeName();
/* 56 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 57 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\MultiCast.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */