/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class ColdSnap extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Cold Snap";
/* 17 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Cold Snap");
/* 18 */   public static final String NAME = cardStrings.NAME;
/* 19 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public ColdSnap() {
/* 23 */     super("Cold Snap", NAME, "blue/attack/coldSnap", "blue/attack/coldSnap", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.showEvokeValue = true;
/* 36 */     this.showEvokeOrbCount = 1;
/* 37 */     this.baseMagicNumber = 1;
/* 38 */     this.magicNumber = this.baseMagicNumber;
/* 39 */     this.baseDamage = 6;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 44 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_HEAVY));
/*    */     
/* 46 */     AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Frost()));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 51 */     return new ColdSnap();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 56 */     if (!this.upgraded) {
/* 57 */       upgradeName();
/* 58 */       upgradeDamage(3);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\ColdSnap.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */