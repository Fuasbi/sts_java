/*     */ package com.megacrit.cardcrawl.cards.blue;
/*     */ 
/*     */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*     */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*     */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*     */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*     */ import com.megacrit.cardcrawl.localization.CardStrings;
/*     */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*     */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*     */ import com.megacrit.cardcrawl.orbs.Lightning;
/*     */ 
/*     */ public class ThunderStrike extends AbstractCard
/*     */ {
/*     */   public static final String ID = "Thunder Strike";
/*  16 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Thunder Strike");
/*  17 */   public static final String NAME = cardStrings.NAME;
/*  18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*  19 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*     */   private static final int COST = 3;
/*     */   
/*     */   public ThunderStrike()
/*     */   {
/*  24 */     super("Thunder Strike", NAME, "blue/attack/thunderStrike", "blue/attack/thunderStrike", 3, DESCRIPTION, AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  36 */     this.baseMagicNumber = 0;
/*  37 */     this.magicNumber = 0;
/*  38 */     this.baseDamage = 7;
/*     */   }
/*     */   
/*     */   public void use(AbstractPlayer p, AbstractMonster m)
/*     */   {
/*  43 */     this.baseMagicNumber = 0;
/*  44 */     for (AbstractOrb o : AbstractDungeon.actionManager.orbsChanneledThisCombat) {
/*  45 */       if ((o instanceof Lightning)) {
/*  46 */         this.baseMagicNumber += 1;
/*     */       }
/*     */     }
/*  49 */     this.magicNumber = this.baseMagicNumber;
/*     */     
/*  51 */     if (this.magicNumber > 0)
/*     */     {
/*  53 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.ThunderStrikeAction(
/*     */       
/*  55 */         AbstractDungeon.getMonsters().getRandomMonster(true), new com.megacrit.cardcrawl.cards.DamageInfo(p, this.baseDamage, this.damageTypeForTurn), this.magicNumber));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void applyPowers()
/*     */   {
/*  63 */     super.applyPowers();
/*     */     
/*  65 */     this.baseMagicNumber = 0;
/*  66 */     this.magicNumber = 0;
/*  67 */     for (AbstractOrb o : AbstractDungeon.actionManager.orbsChanneledThisCombat) {
/*  68 */       if ((o instanceof Lightning)) {
/*  69 */         this.baseMagicNumber += 1;
/*     */       }
/*     */     }
/*     */     
/*  73 */     if (this.baseMagicNumber > 0) {
/*  74 */       this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[0]);
/*  75 */       initializeDescription();
/*     */     }
/*     */   }
/*     */   
/*     */   public void onMoveToDiscard()
/*     */   {
/*  81 */     this.rawDescription = DESCRIPTION;
/*  82 */     initializeDescription();
/*     */   }
/*     */   
/*     */   public void calculateCardDamage(AbstractMonster mo)
/*     */   {
/*  87 */     super.calculateCardDamage(mo);
/*  88 */     if (this.baseMagicNumber > 0) {
/*  89 */       this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[0]);
/*     */     }
/*  91 */     initializeDescription();
/*     */   }
/*     */   
/*     */   public AbstractCard makeCopy()
/*     */   {
/*  96 */     return new ThunderStrike();
/*     */   }
/*     */   
/*     */   public void upgrade()
/*     */   {
/* 101 */     if (!this.upgraded) {
/* 102 */       upgradeName();
/* 103 */       upgradeDamage(2);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\ThunderStrike.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */