/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class EchoForm extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Echo Form";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Echo Form");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 3;
/*    */   
/*    */   public EchoForm() {
/* 21 */     super("Echo Form", NAME, "blue/power/echoForm", "blue/power/echoForm", 3, DESCRIPTION, AbstractCard.CardType.POWER, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.isEthereal = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 38 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.ApplyPowerAction(p, p, new com.megacrit.cardcrawl.powers.EchoPower(p, 1), 1));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 43 */     return new EchoForm();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 48 */     if (!this.upgraded) {
/* 49 */       this.isEthereal = false;
/* 50 */       upgradeName();
/* 51 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 52 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\EchoForm.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */