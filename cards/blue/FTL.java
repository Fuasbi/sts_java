/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.FTLAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class FTL extends AbstractCard
/*    */ {
/*    */   public static final String ID = "FTL";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("FTL");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   
/*    */   public FTL() {
/* 21 */     super("FTL", NAME, "blue/attack/ftl", "blue/attack/ftl", 0, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.baseDamage = 5;
/* 34 */     this.baseMagicNumber = 3;
/* 35 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 40 */     AbstractDungeon.actionManager.addToBottom(new FTLAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), this.magicNumber));
/*    */     
/* 42 */     this.rawDescription = DESCRIPTION;
/* 43 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 48 */     super.applyPowers();
/*    */     
/* 50 */     int count = AbstractDungeon.actionManager.cardsPlayedThisTurn.size();
/*    */     
/* 52 */     this.rawDescription = DESCRIPTION;
/* 53 */     this.rawDescription = (this.rawDescription + EXTENDED_DESCRIPTION[0] + count);
/*    */     
/* 55 */     if (count == 1) {
/* 56 */       this.rawDescription += EXTENDED_DESCRIPTION[1];
/*    */     } else {
/* 58 */       this.rawDescription += EXTENDED_DESCRIPTION[2];
/*    */     }
/* 60 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void onMoveToDiscard()
/*    */   {
/* 65 */     this.rawDescription = DESCRIPTION;
/* 66 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 71 */     return new FTL();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 76 */     if (!this.upgraded) {
/* 77 */       upgradeName();
/* 78 */       upgradeDamage(1);
/* 79 */       upgradeMagicNumber(1);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\FTL.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */