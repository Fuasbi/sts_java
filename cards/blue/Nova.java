/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Nova extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Nova";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Nova");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Nova()
/*    */   {
/* 23 */     super("Nova", NAME, "blue/attack/nova", "blue/attack/nova", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 0;
/* 36 */     this.baseMagicNumber = 3;
/* 37 */     this.magicNumber = this.baseMagicNumber;
/* 38 */     this.isMultiDamage = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     if (this.baseDamage != 0) {
/* 44 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.FIRE));
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void applyPowers()
/*    */   {
/* 51 */     int tmp = 0;
/* 52 */     for (AbstractCard c : AbstractDungeon.actionManager.cardsPlayedThisCombat) {
/* 53 */       if (c.type == AbstractCard.CardType.POWER) {
/* 54 */         tmp++;
/*    */       }
/*    */     }
/*    */     
/* 58 */     if (tmp > 0) {
/* 59 */       this.baseDamage = (tmp * this.magicNumber);
/* 60 */       super.applyPowers();
/* 61 */       this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[1]);
/* 62 */       initializeDescription();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 68 */     return new Nova();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 73 */     if (!this.upgraded) {
/* 74 */       upgradeMagicNumber(1);
/* 75 */       upgradeName();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Nova.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */