/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Rainbow extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Rainbow";
/* 16 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Rainbow");
/* 17 */   public static final String NAME = cardStrings.NAME;
/* 18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 19 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   
/*    */   public Rainbow()
/*    */   {
/* 24 */     super("Rainbow", NAME, "blue/skill/rainbow", "blue/skill/rainbow", 2, DESCRIPTION, AbstractCard.CardType.SKILL, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 36 */     this.showEvokeValue = true;
/* 37 */     this.showEvokeOrbCount = 3;
/* 38 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Lightning()));
/* 44 */     AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Frost()));
/* 45 */     AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Dark()));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 50 */     return new Rainbow();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 55 */     if (!this.upgraded) {
/* 56 */       upgradeName();
/* 57 */       this.exhaust = false;
/* 58 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 59 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Rainbow.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */