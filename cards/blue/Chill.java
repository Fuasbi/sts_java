/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.ChannelAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Chill extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Chill";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Chill");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 17 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   
/*    */   public Chill()
/*    */   {
/* 22 */     super("Chill", NAME, "blue/skill/chill", "blue/skill/chill", 0, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.exhaust = true;
/* 35 */     this.showEvokeValue = true;
/* 36 */     this.showEvokeOrbCount = 3;
/* 37 */     this.baseMagicNumber = 1;
/* 38 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     int count = 0;
/* 44 */     for (AbstractMonster mon : AbstractDungeon.getMonsters().monsters) {
/* 45 */       if (!mon.isDeadOrEscaped()) {
/* 46 */         count++;
/*    */       }
/*    */     }
/* 49 */     for (int i = 0; i < count; i++) {
/* 50 */       AbstractDungeon.actionManager.addToBottom(new ChannelAction(new com.megacrit.cardcrawl.orbs.Frost()));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 56 */     return new Chill();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 61 */     if (!this.upgraded) {
/* 62 */       upgradeName();
/* 63 */       this.isInnate = true;
/* 64 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 65 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Chill.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */