/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.defect.AnimateOrbAction;
/*    */ import com.megacrit.cardcrawl.actions.defect.EvokeWithoutRemovingOrbAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Dualcast extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Dualcast";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Dualcast");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Dualcast() {
/* 22 */     super("Dualcast", NAME, null, "blue/skill/dualcast", 1, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.BASIC, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 34 */     this.showEvokeValue = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 39 */     AbstractDungeon.actionManager.addToBottom(new AnimateOrbAction(1));
/* 40 */     AbstractDungeon.actionManager.addToBottom(new EvokeWithoutRemovingOrbAction(1));
/* 41 */     AbstractDungeon.actionManager.addToBottom(new AnimateOrbAction(1));
/* 42 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.EvokeOrbAction(1));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 47 */     return new Dualcast();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 52 */     if (!this.upgraded) {
/* 53 */       upgradeName();
/* 54 */       upgradeBaseCost(0);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Dualcast.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */