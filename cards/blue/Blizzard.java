/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.orbs.AbstractOrb;
/*    */ import com.megacrit.cardcrawl.orbs.Frost;
/*    */ 
/*    */ public class Blizzard extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Blizzard";
/* 16 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Blizzard");
/* 17 */   public static final String NAME = cardStrings.NAME;
/* 18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 19 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public Blizzard() {
/* 23 */     super("Blizzard", NAME, "blue/attack/blizzard", "blue/attack/blizzard", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ALL_ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 0;
/* 36 */     this.baseMagicNumber = 2;
/* 37 */     this.magicNumber = this.baseMagicNumber;
/* 38 */     this.isMultiDamage = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     int frostCount = 0;
/* 44 */     for (AbstractOrb o : AbstractDungeon.actionManager.orbsChanneledThisCombat) {
/* 45 */       if ((o instanceof Frost)) {
/* 46 */         frostCount++;
/*    */       }
/*    */     }
/*    */     
/* 50 */     this.baseDamage = (frostCount * this.magicNumber);
/* 51 */     calculateCardDamage(null);
/*    */     
/* 53 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction(p, this.multiDamage, this.damageTypeForTurn, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.BLUNT_HEAVY, false));
/*    */   }
/*    */   
/*    */ 
/*    */   public void applyPowers()
/*    */   {
/* 59 */     int frostCount = 0;
/* 60 */     for (AbstractOrb o : AbstractDungeon.actionManager.orbsChanneledThisCombat) {
/* 61 */       if ((o instanceof Frost)) {
/* 62 */         frostCount++;
/*    */       }
/*    */     }
/*    */     
/* 66 */     if (frostCount > 0) {
/* 67 */       this.baseDamage = (frostCount * this.magicNumber);
/* 68 */       super.applyPowers();
/* 69 */       this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[0]);
/* 70 */       initializeDescription();
/*    */     }
/*    */   }
/*    */   
/*    */   public void onMoveToDiscard()
/*    */   {
/* 76 */     this.rawDescription = DESCRIPTION;
/* 77 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void calculateCardDamage(AbstractMonster mo)
/*    */   {
/* 82 */     super.calculateCardDamage(mo);
/*    */     
/* 84 */     this.rawDescription = DESCRIPTION;
/* 85 */     this.rawDescription += EXTENDED_DESCRIPTION[0];
/* 86 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 91 */     return new Blizzard();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 96 */     if (!this.upgraded) {
/* 97 */       upgradeName();
/* 98 */       upgradeMagicNumber(1);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Blizzard.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */