/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Hologram extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Hologram";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Hologram");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   private static final int BLOCK_AMT = 3;
/*    */   
/*    */   public Hologram()
/*    */   {
/* 24 */     super("Hologram", NAME, "blue/skill/hologram", "blue/skill/hologram", 1, DESCRIPTION, AbstractCard.CardType.SKILL, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseBlock = 3;
/* 36 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void upgrade()
/*    */   {
/* 44 */     if (!this.upgraded) {
/* 45 */       upgradeName();
/* 46 */       upgradeBlock(2);
/* 47 */       this.exhaust = false;
/* 48 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 49 */       initializeDescription();
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 55 */     return new Hologram();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 66 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.GainBlockAction(p, p, this.block));
/* 67 */     if (AbstractDungeon.player.discardPile.size() > 0) {
/* 68 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.DiscardPileToHandAction(1));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\Hologram.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */