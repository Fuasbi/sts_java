/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class GoForTheEyes extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Go for the Eyes";
/* 16 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Go for the Eyes");
/* 17 */   public static final String NAME = cardStrings.NAME;
/* 18 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 0;
/*    */   private static final int ATTACK_DMG = 3;
/*    */   
/*    */   public GoForTheEyes() {
/* 23 */     super("Go for the Eyes", NAME, "blue/attack/goForTheEyes", "blue/attack/goForTheEyes", 0, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.COMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseDamage = 3;
/* 36 */     this.baseMagicNumber = 1;
/* 37 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 42 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.SLASH_DIAGONAL));
/*    */     
/* 44 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.defect.ForTheEyesAction(this.magicNumber, m));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 49 */     return new GoForTheEyes();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 54 */     if (!this.upgraded) {
/* 55 */       upgradeDamage(1);
/* 56 */       upgradeMagicNumber(1);
/* 57 */       upgradeName();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\GoForTheEyes.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */