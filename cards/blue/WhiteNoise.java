/*    */ package com.megacrit.cardcrawl.cards.blue;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class WhiteNoise extends AbstractCard
/*    */ {
/*    */   public static final String ID = "White Noise";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("White Noise");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public WhiteNoise()
/*    */   {
/* 20 */     super("White Noise", NAME, "blue/skill/whiteNoise", "blue/skill/whiteNoise", 1, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.BLUE, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 31 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 38 */     AbstractCard c = AbstractDungeon.returnTrulyRandomCard(AbstractCard.CardType.POWER, AbstractDungeon.cardRandomRng).makeCopy();
/* 39 */     c.setCostForTurn(0);
/* 40 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction(c, true));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 45 */     return new WhiteNoise();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 50 */     if (!this.upgraded) {
/* 51 */       upgradeName();
/* 52 */       upgradeBaseCost(0);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\blue\WhiteNoise.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */