/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class SecretTechnique extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Secret Technique";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Secret Technique");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 16 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/* 17 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   
/*    */   public SecretTechnique() {
/* 20 */     super("Secret Technique", NAME, "colorless/skill/secretTechnique", "colorless/skill/secretTechnique", 0, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 31 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 36 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.unique.SkillFromDeckToHandAction(1));
/*    */   }
/*    */   
/*    */   public boolean canUse(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     boolean canUse = super.canUse(p, m);
/* 42 */     if (!canUse) {
/* 43 */       return false;
/*    */     }
/*    */     
/* 46 */     boolean hasSkill = false;
/* 47 */     for (AbstractCard c : p.drawPile.group) {
/* 48 */       if (c.type == AbstractCard.CardType.SKILL) {
/* 49 */         hasSkill = true;
/*    */       }
/*    */     }
/*    */     
/* 53 */     if (!hasSkill) {
/* 54 */       this.cantUseMessage = EXTENDED_DESCRIPTION[0];
/* 55 */       canUse = false;
/*    */     }
/*    */     
/* 58 */     return canUse;
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 63 */     return new SecretTechnique();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 68 */     if (!this.upgraded) {
/* 69 */       upgradeName();
/* 70 */       this.exhaust = false;
/* 71 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 72 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\SecretTechnique.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */