/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.EnlightenmentAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Enlightenment extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Enlightenment";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Enlightenment");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 16 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   
/*    */   public Enlightenment() {
/* 19 */     super("Enlightenment", NAME, "colorless/skill/enlightenment", "colorless/skill/enlightenment", 0, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.SELF);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 34 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new EnlightenmentAction(this.upgraded));
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 39 */     return new Enlightenment();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 44 */     if (!this.upgraded) {
/* 45 */       upgradeName();
/* 46 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 47 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\Enlightenment.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */