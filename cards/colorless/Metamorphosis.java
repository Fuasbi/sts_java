/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class Metamorphosis extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Metamorphosis";
/* 13 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Metamorphosis");
/* 14 */   public static final String NAME = cardStrings.NAME;
/* 15 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   private static final int START_CARDS = 3;
/*    */   private static final int UPGRADE_AMT = 2;
/*    */   
/*    */   public Metamorphosis()
/*    */   {
/* 22 */     super("Metamorphosis", NAME, "colorless/skill/metamorphosis", "colorless/skill/metamorphosis", 2, DESCRIPTION, AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.RARE, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.NONE);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 33 */     this.baseMagicNumber = 3;
/* 34 */     this.magicNumber = this.baseMagicNumber;
/* 35 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 40 */     for (int i = 0; i < this.magicNumber; i++)
/*    */     {
/*    */ 
/* 43 */       AbstractCard card = AbstractDungeon.returnTrulyRandomCard(AbstractCard.CardType.ATTACK, AbstractDungeon.cardRandomRng).makeCopy();
/* 44 */       if (card.cost > 0) {
/* 45 */         card.cost = 0;
/* 46 */         card.costForTurn = 0;
/* 47 */         card.isCostModified = true;
/*    */       }
/* 49 */       AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction(card, 1, true, true));
/*    */     }
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 55 */     return new Metamorphosis();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 60 */     if (!this.upgraded) {
/* 61 */       upgradeName();
/* 62 */       upgradeMagicNumber(2);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\Metamorphosis.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */