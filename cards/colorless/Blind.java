/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardTarget;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ import com.megacrit.cardcrawl.powers.WeakPower;
/*    */ 
/*    */ public class Blind extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Blind";
/* 15 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("Blind");
/* 16 */   public static final String NAME = cardStrings.NAME;
/* 17 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 18 */   public static final String UPGRADE_DESCRIPTION = cardStrings.UPGRADE_DESCRIPTION;
/*    */   private static final int WEAK_AMOUNT = 2;
/*    */   
/*    */   public Blind()
/*    */   {
/* 23 */     super("Blind", NAME, "colorless/skill/blind", "colorless/skill/blind", 0, DESCRIPTION, com.megacrit.cardcrawl.cards.AbstractCard.CardType.SKILL, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 35 */     this.baseMagicNumber = 2;
/* 36 */     this.magicNumber = this.baseMagicNumber;
/*    */   }
/*    */   
/*    */   public void use(com.megacrit.cardcrawl.characters.AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     if (!this.upgraded) {
/* 42 */       AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(m, p, new WeakPower(m, this.magicNumber, false), this.magicNumber));
/*    */     }
/*    */     else {
/* 45 */       for (AbstractMonster mo : AbstractDungeon.getCurrRoom().monsters.monsters) {
/* 46 */         AbstractDungeon.actionManager.addToBottom(new ApplyPowerAction(mo, p, new WeakPower(mo, this.magicNumber, false), this.magicNumber, true, com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 60 */     return new Blind();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 65 */     if (!this.upgraded) {
/* 66 */       upgradeName();
/* 67 */       this.target = AbstractCard.CardTarget.ALL_ENEMY;
/* 68 */       this.rawDescription = UPGRADE_DESCRIPTION;
/* 69 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\Blind.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */