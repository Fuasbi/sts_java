/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.unique.RitualDaggerAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class RitualDagger extends AbstractCard
/*    */ {
/*    */   public static final String ID = "RitualDagger";
/* 14 */   private static final CardStrings cardStrings = com.megacrit.cardcrawl.core.CardCrawlGame.languagePack.getCardStrings("RitualDagger");
/* 15 */   public static final String NAME = cardStrings.NAME;
/* 16 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/*    */   private static final int COST = 1;
/*    */   
/*    */   public RitualDagger() {
/* 20 */     super("RitualDagger", NAME, "colorless/attack/ritualDagger", "colorless/attack/ritualDagger", 1, DESCRIPTION, AbstractCard.CardType.ATTACK, com.megacrit.cardcrawl.cards.AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.SPECIAL, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 32 */     this.misc = 15;
/* 33 */     this.baseMagicNumber = 3;
/* 34 */     this.magicNumber = this.baseMagicNumber;
/* 35 */     this.baseDamage = this.misc;
/* 36 */     this.exhaust = true;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 41 */     com.megacrit.cardcrawl.dungeons.AbstractDungeon.actionManager.addToBottom(new RitualDaggerAction(m, new com.megacrit.cardcrawl.cards.DamageInfo(p, this.damage, this.damageTypeForTurn), this.magicNumber, this.misc));
/*    */   }
/*    */   
/*    */ 
/*    */   public void applyPowers()
/*    */   {
/* 47 */     this.baseBlock = this.misc;
/* 48 */     super.applyPowers();
/* 49 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 54 */     return new RitualDagger();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 59 */     if (!this.upgraded) {
/* 60 */       upgradeMagicNumber(2);
/* 61 */       upgradeName();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\RitualDagger.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */