/*    */ package com.megacrit.cardcrawl.cards.colorless;
/*    */ 
/*    */ import com.megacrit.cardcrawl.actions.GameActionManager;
/*    */ import com.megacrit.cardcrawl.actions.animations.VFXAction;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;
/*    */ import com.megacrit.cardcrawl.cards.AbstractCard.CardType;
/*    */ import com.megacrit.cardcrawl.cards.DamageInfo;
/*    */ import com.megacrit.cardcrawl.characters.AbstractPlayer;
/*    */ import com.megacrit.cardcrawl.core.CardCrawlGame;
/*    */ import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
/*    */ import com.megacrit.cardcrawl.localization.CardStrings;
/*    */ import com.megacrit.cardcrawl.monsters.AbstractMonster;
/*    */ 
/*    */ public class MindBlast extends AbstractCard
/*    */ {
/*    */   public static final String ID = "Mind Blast";
/* 18 */   private static final CardStrings cardStrings = CardCrawlGame.languagePack.getCardStrings("Mind Blast");
/* 19 */   public static final String NAME = cardStrings.NAME;
/* 20 */   public static final String DESCRIPTION = cardStrings.DESCRIPTION;
/* 21 */   public static final String[] EXTENDED_DESCRIPTION = cardStrings.EXTENDED_DESCRIPTION;
/*    */   private static final int COST = 2;
/*    */   
/*    */   public MindBlast()
/*    */   {
/* 26 */     super("Mind Blast", NAME, null, "colorless/attack/mindBlast", 2, DESCRIPTION, AbstractCard.CardType.ATTACK, AbstractCard.CardColor.COLORLESS, com.megacrit.cardcrawl.cards.AbstractCard.CardRarity.UNCOMMON, com.megacrit.cardcrawl.cards.AbstractCard.CardTarget.ENEMY);
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 37 */     this.isInnate = true;
/* 38 */     this.baseDamage = 0;
/*    */   }
/*    */   
/*    */   public void use(AbstractPlayer p, AbstractMonster m)
/*    */   {
/* 43 */     AbstractDungeon.actionManager.addToBottom(new VFXAction(new com.megacrit.cardcrawl.vfx.combat.MindblastEffect(p.dialogX, p.dialogY)));
/* 44 */     AbstractDungeon.actionManager.addToBottom(new com.megacrit.cardcrawl.actions.common.DamageAction(m, new DamageInfo(p, this.damage, com.megacrit.cardcrawl.cards.DamageInfo.DamageType.NORMAL), com.megacrit.cardcrawl.actions.AbstractGameAction.AttackEffect.NONE));
/*    */     
/*    */ 
/* 47 */     this.rawDescription = DESCRIPTION;
/* 48 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public AbstractCard makeCopy()
/*    */   {
/* 53 */     return new MindBlast();
/*    */   }
/*    */   
/*    */   public void applyPowers()
/*    */   {
/* 58 */     this.baseDamage = AbstractDungeon.player.drawPile.size();
/* 59 */     super.applyPowers();
/* 60 */     this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[0]);
/* 61 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void calculateCardDamage(AbstractMonster mo)
/*    */   {
/* 66 */     super.calculateCardDamage(mo);
/* 67 */     this.rawDescription = (DESCRIPTION + EXTENDED_DESCRIPTION[0]);
/* 68 */     initializeDescription();
/*    */   }
/*    */   
/*    */   public void upgrade()
/*    */   {
/* 73 */     if (!this.upgraded) {
/* 74 */       upgradeName();
/* 75 */       upgradeBaseCost(1);
/* 76 */       initializeDescription();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Gavin-Wolfgang\Desktop\slay\!\com\megacrit\cardcrawl\cards\colorless\MindBlast.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */